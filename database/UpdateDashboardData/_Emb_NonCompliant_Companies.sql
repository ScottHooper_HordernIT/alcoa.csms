USE [AuA_CSMS_Dev]
GO

/****** Object:  StoredProcedure [dbo].[_Emb_NonCompliant_Companies]    Script Date: 31/07/2015 12:45:27 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[_Emb_NonCompliant_Companies]                                      
(                                      
 @SiteId int,               
 @Month int,                                      
 @Year int                                      
)                                      
AS                       
DECLARE @Companies VARCHAR(MAX), @SiteIdN int                 
DECLARE @ErrorMsg VARCHAR(1000)        
      
--IF (@SiteId < 0)                                   
-- BEGIN              
--  SELECT @SiteIdN = [dbo].[GetSiteIdByComboRegionId](@SiteId)              
-- END              
--ELSE              
-- BEGIN              
--  SET @SiteIdN = @SiteId              
-- END          
          
SELECT @SiteIdN = [dbo].[GetSiteIdByComboRegionId](@SiteId)          
----------------------------     
SET @Companies = ''    
                   
BEGIN TRY            
     
                 
SELECT @Companies = CompaniesNotCompliant              
 FROM AdHoc_Radar_Items A              
 WHERE SiteId = @SiteIdN AND [Year] = @Year              
 AND AdHoc_RadarId =7                  
            
IF EXISTS(SELECT * FROM dbo.Dashboard_Companies_NonCompliant WHERE SiteId = @SiteId AND [Year] = @Year                       
 AND [Month] = @Month AND [ItemName] = 'Number of Companies:')                      
 UPDATE [dbo].[Dashboard_Companies_NonCompliant]                      
 SET [NonCompliantCompanies] = @Companies                      
 WHERE [SiteId] = @SiteId                      
   AND [Month] = @Month                       
   AND [Year] = @Year                      
   AND [ItemName] = 'Number of Companies:'                      
ELSE                      
 INSERT INTO [dbo].[Dashboard_Companies_NonCompliant]                      
           ([SiteId]                      
           ,[Month]                      
           ,[Year]                      
           ,[ItemName]                      
           ,[NonCompliantCompanies])                      
     VALUES                      
           (@SiteId                      
           ,@Month                     
           ,@Year                      
           ,'Number of Companies:'                      
           ,@Companies)            
                           
END TRY            
BEGIN CATCH            
 SET @ErrorMsg = ERROR_MESSAGE()            
END CATCH             
-------------------------------              
      
SET @Companies = ''       
              
BEGIN TRY                    
  
SELECT @Companies = CompaniesNotCompliant              
 FROM AdHoc_Radar_Items A              
 WHERE SiteId = @SiteIdN AND [Year] = @Year               
 AND AdHoc_RadarId =8                   
                            
                
IF EXISTS(SELECT * FROM dbo.Dashboard_Companies_NonCompliant WHERE SiteId = @SiteId AND [Year] = @Year                       
 AND [Month] = @Month AND [ItemName] = 'Contractor Services Audit')                      
 UPDATE [dbo].[Dashboard_Companies_NonCompliant]                      
 SET [NonCompliantCompanies] = @Companies                      
 WHERE [SiteId] = @SiteId                      
   AND [Month] = @Month                      
   AND [Year] = @Year                      
   AND [ItemName] = 'Contractor Services Audit'                      
ELSE                      
 INSERT INTO [dbo].[Dashboard_Companies_NonCompliant]                      
           ([SiteId]                      
           ,[Month]                      
           ,[Year]                      
           ,[ItemName]                      
           ,[NonCompliantCompanies])                      
     VALUES                      
           (@SiteId                      
           ,@Month                     
           ,@Year                      
           ,'Contractor Services Audit'                      
           ,@Companies)            
                
END TRY            
BEGIN CATCH            
 SET @ErrorMsg = ERROR_MESSAGE()            
END CATCH             
-------------------------------              
      
SET @Companies = ''       
      
BEGIN TRY       
                     
SELECT @Companies = CompaniesNotCompliant              
 FROM AdHoc_Radar_Items A              
 WHERE SiteId = @SiteIdN AND [Year] = @Year         
 AND AdHoc_RadarId =10                   
                
IF EXISTS(SELECT * FROM dbo.Dashboard_Companies_NonCompliant WHERE SiteId = @SiteId AND [Year] = @Year                       
AND [Month] = @Month AND [ItemName] = 'Workplace Safety Compliance')                      
 UPDATE [dbo].[Dashboard_Companies_NonCompliant]                      
 SET [NonCompliantCompanies] = @Companies                      
 WHERE [SiteId] = @SiteId                      
   AND [Month] = @Month                      
   AND [Year] = @Year                      
   AND [ItemName] = 'Workplace Safety Compliance'                      
ELSE                
 INSERT INTO [dbo].[Dashboard_Companies_NonCompliant]                      
           ([SiteId]                      
           ,[Month]                      
           ,[Year]                      
           ,[ItemName]                      
           ,[NonCompliantCompanies])                      
     VALUES                      
           (@SiteId                      
           ,@Month                     
           ,@Year                      
           ,'Workplace Safety Compliance'                      
           ,@Companies)               
                
END TRY            
BEGIN CATCH            
 SET @ErrorMsg = ERROR_MESSAGE()            
END CATCH            
-------------------------------              
      
SET @Companies = ''      
             
BEGIN TRY                      
   
SELECT @Companies = CompaniesNotCompliant              
 FROM AdHoc_Radar_Items A              
 WHERE SiteId = @SiteIdN AND [Year] = @Year               
 AND AdHoc_RadarId =11                   
                
IF EXISTS(SELECT * FROM dbo.Dashboard_Companies_NonCompliant WHERE SiteId = @SiteId AND [Year] = @Year                       
AND [Month] = @Month AND [ItemName] = 'Management Health Safety Work Contacts')                      
 UPDATE [dbo].[Dashboard_Companies_NonCompliant]                      
 SET [NonCompliantCompanies] = @Companies                      
 WHERE [SiteId] = @SiteId                      
   AND [Month] = @Month                      
   AND [Year] = @Year                      
   AND [ItemName] = 'Management Health Safety Work Contacts'                      
ELSE                      
 INSERT INTO [dbo].[Dashboard_Companies_NonCompliant]                      
           ([SiteId]                      
           ,[Month]                      
           ,[Year]                      
           ,[ItemName]                      
           ,[NonCompliantCompanies])                      
     VALUES                      
           (@SiteId                      
           ,@Month                     
           ,@Year                      
           ,'Management Health Safety Work Contacts'                      
           ,@Companies)                  
                
END TRY            
BEGIN CATCH            
 SET @ErrorMsg = ERROR_MESSAGE()            
END CATCH                      
-------------------------------              
      
SET @Companies = ''      
              
BEGIN TRY                   
   
SELECT @Companies = CompaniesNotCompliant              
 FROM AdHoc_Radar_Items A              
 WHERE SiteId = @SiteIdN AND [Year] = @Year             
 AND AdHoc_RadarId =12                  
                
IF EXISTS(SELECT * FROM dbo.Dashboard_Companies_NonCompliant WHERE SiteId = @SiteId AND [Year] = @Year                       
AND [Month] = @Month AND [ItemName] = 'Behavioural Observations')                      
 UPDATE [dbo].[Dashboard_Companies_NonCompliant]                      
 SET [NonCompliantCompanies] = @Companies                      
 WHERE [SiteId] = @SiteId                      
   AND [Month] = @Month                      
   AND [Year] = @Year                      
   AND [ItemName] = 'Behavioural Observations'                      
ELSE                      
 INSERT INTO [dbo].[Dashboard_Companies_NonCompliant]                      
           ([SiteId]                      
           ,[Month]                      
           ,[Year]                      
           ,[ItemName]                      
           ,[NonCompliantCompanies])                      
     VALUES                      
           (@SiteId                      
           ,@Month                     
           ,@Year          
           ,'Behavioural Observations'                      
           ,@Companies)              
                
END TRY            
BEGIN CATCH            
 SET @ErrorMsg = ERROR_MESSAGE()            
END CATCH            
-------------------------------              
      
SET @Companies = ''       
              
BEGIN TRY                    
  
 SELECT @Companies = CompaniesNotCompliant              
 FROM AdHoc_Radar_Items A              
 WHERE SiteId = @SiteIdN AND [Year] = @Year              
 AND AdHoc_RadarId =13             
                
IF EXISTS(SELECT * FROM dbo.Dashboard_Companies_NonCompliant WHERE SiteId = @SiteId AND [Year] = @Year                       
AND [Month] = @Month AND [ItemName] = 'Fatality Prevention Program')                      
 UPDATE [dbo].[Dashboard_Companies_NonCompliant]                      
 SET [NonCompliantCompanies] = @Companies                      
 WHERE [SiteId] = @SiteId                      
   AND [Month] = @Month                      
   AND [Year] = @Year                      
   AND [ItemName] = 'Fatality Prevention Program'                      
ELSE                      
 INSERT INTO [dbo].[Dashboard_Companies_NonCompliant]                      
           ([SiteId]                      
           ,[Month]                      
           ,[Year]                      
           ,[ItemName]                      
           ,[NonCompliantCompanies])                      
     VALUES                      
           (@SiteId                      
           ,@Month                     
           ,@Year                      
           ,'Fatality Prevention Program'                      
           ,@Companies)               
                
END TRY            
BEGIN CATCH            
 SET @ErrorMsg = ERROR_MESSAGE()            
END CATCH            
-------------------------------              
      
SET @Companies = ''       
              
BEGIN TRY                     
SELECT @Companies = CompaniesNotCompliant              
 FROM AdHoc_Radar_Items A              
 WHERE SiteId = @SiteIdN AND [Year] = @Year                
 AND AdHoc_RadarId =14           
      
               
IF EXISTS(SELECT * FROM dbo.Dashboard_Companies_NonCompliant WHERE SiteId = @SiteId AND [Year] = @Year                       
AND [Month] = @Month AND [ItemName] = 'JSA Field Audit Verifications')      
      
                      
 UPDATE [dbo].[Dashboard_Companies_NonCompliant]                      
 SET [NonCompliantCompanies] = @Companies                      
 WHERE [SiteId] = @SiteId                      
   AND [Month] = @Month                      
   AND [Year] = @Year                      
   AND [ItemName] = 'JSA Field Audit Verifications'                     
      
ELSE            
       
 INSERT INTO [dbo].[Dashboard_Companies_NonCompliant]                      
           ([SiteId]                      
           ,[Month]                      
           ,[Year]                   
           ,[ItemName]                      
           ,[NonCompliantCompanies])                      
     VALUES                      
           (@SiteId                      
           ,@Month                     
           ,@Year                      
           ,'JSA Field Audit Verifications'                      
           ,@Companies)         
         
END TRY            
      
BEGIN CATCH            
 SET @ErrorMsg = ERROR_MESSAGE()            
END CATCH           
               
-------------------------------          
      
SET @Companies = ''       
              
BEGIN TRY                   
SELECT @Companies = CompaniesNotCompliant              
 FROM AdHoc_Radar_Items A              
 WHERE SiteId = @SiteIdN AND [Year] = @Year              
 AND AdHoc_RadarId =15                 
                
IF EXISTS(SELECT * FROM dbo.Dashboard_Companies_NonCompliant WHERE SiteId = @SiteId AND [Year] = @Year                       
AND [Month] = @Month AND [ItemName] = 'Toolbox Meetings')                      
 UPDATE [dbo].[Dashboard_Companies_NonCompliant]                      
 SET [NonCompliantCompanies] = @Companies                      
 WHERE [SiteId] = @SiteId                      
   AND [Month] = @Month                      
   AND [Year] = @Year                      
   AND [ItemName] = 'Toolbox Meetings'                      
ELSE                      
 INSERT INTO [dbo].[Dashboard_Companies_NonCompliant]                      
           ([SiteId]                      
           ,[Month]                      
           ,[Year]                      
           ,[ItemName]                      
           ,[NonCompliantCompanies])                      
     VALUES                      
           (@SiteId                      
           ,@Month                     
           ,@Year                      
           ,'Toolbox Meetings'                      
           ,@Companies)               
                
END TRY            
BEGIN CATCH            
 SET @ErrorMsg = ERROR_MESSAGE()            
END CATCH              
-------------------------------              
      
SET @Companies = ''       
              
BEGIN TRY                     
SELECT @Companies = CompaniesNotCompliant              
 FROM AdHoc_Radar_Items A              
 WHERE SiteId = @SiteIdN AND [Year] = @Year              
 AND AdHoc_RadarId =16                  
             
IF EXISTS(SELECT * FROM dbo.Dashboard_Companies_NonCompliant WHERE SiteId = @SiteId AND [Year] = @Year                       
AND [Month] = @Month AND [ItemName] = 'Alcoa Weekly Contractors Meeting')                      
 UPDATE [dbo].[Dashboard_Companies_NonCompliant]                      
 SET [NonCompliantCompanies] = @Companies                      
 WHERE [SiteId] = @SiteId                      
   AND [Month] = @Month                      
   AND [Year] = @Year                      
   AND [ItemName] = 'Alcoa Weekly Contractors Meeting'                      
ELSE                      
 INSERT INTO [dbo].[Dashboard_Companies_NonCompliant]                      
           ([SiteId]                      
           ,[Month]                      
           ,[Year]                      
           ,[ItemName]                      
           ,[NonCompliantCompanies])                      
     VALUES                      
           (@SiteId                      
           ,@Month                     
           ,@Year                      
           ,'Alcoa Weekly Contractors Meeting'                      
           ,@Companies)               
                
END TRY            
BEGIN CATCH            
 SET @ErrorMsg = ERROR_MESSAGE()            
END CATCH              
-------------------------------           
      
SET @Companies = ''          
              
BEGIN TRY                    
SELECT @Companies = CompaniesNotCompliant              
 FROM AdHoc_Radar_Items A              
 WHERE SiteId = @SiteIdN AND [Year] = @Year               
 AND AdHoc_RadarId =17                   
               
IF EXISTS(SELECT * FROM dbo.Dashboard_Companies_NonCompliant WHERE SiteId = @SiteId AND [Year] = @Year                       
AND [Month] = @Month AND [ItemName] = 'Alcoa Monthly Contractors Meeting')                      
 UPDATE [dbo].[Dashboard_Companies_NonCompliant]                      
 SET [NonCompliantCompanies] = @Companies                      
 WHERE [SiteId] = @SiteId                      
  AND [Month] = @Month                      
   AND [Year] = @Year                      
   AND [ItemName] = 'Alcoa Monthly Contractors Meeting'                      
ELSE                      
 INSERT INTO [dbo].[Dashboard_Companies_NonCompliant]                      
           ([SiteId]                      
           ,[Month]                      
           ,[Year]                      
           ,[ItemName]                      
           ,[NonCompliantCompanies])                      
     VALUES                      
           (@SiteId                      
           ,@Month                     
           ,@Year                      
           ,'Alcoa Monthly Contractors Meeting'                      
           ,@Companies)               
                
END TRY            
BEGIN CATCH            
 SET @ErrorMsg = ERROR_MESSAGE()            
END CATCH              
-------------------------------              
      
SET @Companies = ''       
              
BEGIN TRY                    
SELECT @Companies = CompaniesNotCompliant              
 FROM AdHoc_Radar_Items A              
 WHERE SiteId = @SiteIdN AND [Year] = @Year              
 AND AdHoc_RadarId =18                  
               
IF EXISTS(SELECT * FROM dbo.Dashboard_Companies_NonCompliant WHERE SiteId = @SiteId AND [Year] = @Year                       
AND [Month] = @Month AND [ItemName] = 'Safety Plan(s) Submitted')                      
 UPDATE [dbo].[Dashboard_Companies_NonCompliant]                      
 SET [NonCompliantCompanies] = @Companies                      
 WHERE [SiteId] = @SiteId                      
   AND [Month] = @Month                      
   AND [Year] = @Year                      
   AND [ItemName] = 'Safety Plan(s) Submitted'                      
ELSE                      
 INSERT INTO [dbo].[Dashboard_Companies_NonCompliant]                      
           ([SiteId]                      
           ,[Month]                      
           ,[Year]                      
           ,[ItemName]                      
           ,[NonCompliantCompanies])                      
     VALUES                      
           (@SiteId                      
           ,@Month                     
           ,@Year                      
           ,'Safety Plan(s) Submitted'                      
           ,@Companies)               
                
END TRY            
BEGIN CATCH            
 SET @ErrorMsg = ERROR_MESSAGE()            
END CATCH             
-------------------------------              
      
SET @Companies = ''       
              
BEGIN TRY                      
SELECT @Companies = CompaniesNotCompliant              
 FROM AdHoc_Radar_Items A              
 WHERE SiteId = @SiteIdN AND [Year] = @Year             
 AND AdHoc_RadarId =19                
                
IF EXISTS(SELECT * FROM dbo.Dashboard_Companies_NonCompliant WHERE SiteId = @SiteId AND [Year] = @Year                       
AND [Month] = @Month AND [ItemName] = 'Mandated Training')                      
 UPDATE [dbo].[Dashboard_Companies_NonCompliant]                      
 SET [NonCompliantCompanies] = @Companies                      
 WHERE [SiteId] = @SiteId                
   AND [Month] = @Month                      
   AND [Year] = @Year                  
   AND [ItemName] = 'Mandated Training'                      
ELSE                      
 INSERT INTO [dbo].[Dashboard_Companies_NonCompliant]                      
           ([SiteId]                      
           ,[Month]                      
           ,[Year]                      
           ,[ItemName]                      
           ,[NonCompliantCompanies])                      
     VALUES                      
           (@SiteId                      
           ,@Month                     
           ,@Year                      
           ,'Mandated Training'                      
           ,@Companies)             
                
END TRY            
BEGIN CATCH            
 SET @ErrorMsg = ERROR_MESSAGE()            
END CATCH            
----------------------------------------

GO


