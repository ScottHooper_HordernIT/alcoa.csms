using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.Data;
using DevExpress.Web.ASPxPopupControl;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxCallbackPanel;
using DevExpress.Web.ASPxClasses;
using KaiZen.CSMS.Services;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using System.Data.SqlClient;
using DevExpress.XtraCharts;
using System.Threading;

//Added By Bishwajit For Item#31
using System.IO;
using Autofac;
using Autofac.Integration.Web;

using System.Text;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Collections.Generic;
using System.Web.Configuration;
using System.ComponentModel;
//End Added By Bishwajit For Item#31


// excellent use of oo and programming techniques below... lol
// TODO: write properly when time allows = never? lol
public partial class UserControls_ComplianceChart2 : System.Web.UI.UserControl
{
    Auth auth = new Auth();
//Added By Bishwajit For Item#31
    private enum TrafficLightColor
    {
        [Description("0")]
        Grey = 0,
        [Description("1")]
        Red = 1,
        [Description("2")]
        Yellow = 2,
        [Description("3")]
        Green = 3,
    }
	//End Added By Bishwajit For Item#31

    protected void Page_Load(object sender, EventArgs e) {  Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); }

    private void moduleInit(bool postBack)
    {
        var cpa = (Autofac.Integration.Web.IContainerProviderAccessor)HttpContext.Current.ApplicationInstance;
        var cp = cpa.ContainerProvider;

        Reports_DropDownListPicker.btnHandler += new
                 UserControls_Tiny_Reports_DropDownListPicker2.OnButtonClick(DropDownListPicker_btnHandler);

        ASPxGridView.RegisterBaseScript(this.Page);

        SessionHandler.spVar_RadarInternal = "0";

        if (Request.QueryString["id"] != null)
        {
            if (auth.RoleId == (int)RoleList.Contractor &&
                Request.QueryString["id"] == "ComplianceChart2_S_A")
            {
                SessionHandler.spVar_Page = "ComplianceChart2_S_A";
                SessionHandler.spVar_CompanyId = auth.CompanyId.ToString();
                if (String.IsNullOrEmpty(SessionHandler.spVar_SiteId))
                    { SessionHandler.spVar_SiteId = "-1"; };

                if (String.IsNullOrEmpty(SessionHandler.spVar_MonthId))
                    { SessionHandler.spVar_MonthId = "0"; };
                
                if (String.IsNullOrEmpty(SessionHandler.spVar_Year))
                {
                    DataTable dt = ((DataView)sqldsKpi_GetAllYearsSubmitted.Select(DataSourceSelectArguments.Empty)).ToTable();
                    int r = dt.Rows.Count - 1;
                    int latestYear = 0;
                    Int32.TryParse(dt.Rows[r].ItemArray[0].ToString(), out latestYear);

                    if (latestYear == 0)
                    {
                        latestYear = DateTime.Now.Year;
                    }

                    if (DateTime.Now.Year == latestYear)
                    {
                        if (DateTime.Now.Month == 1)
                        {
                            latestYear = DateTime.Now.Year - 1;
                        }
                    }

                    SessionHandler.spVar_Year = latestYear.ToString();
                }
            }
            if (!String.IsNullOrEmpty(Request.QueryString["id"]))
            {
                SessionHandler.spVar_Page = Request.QueryString["id"].ToString();
                int i = 0;
                if (String.IsNullOrEmpty(SessionHandler.spVar_CompanyId)) { i++; };
                if (String.IsNullOrEmpty(SessionHandler.spVar_SiteId)) { i++; };
                if (String.IsNullOrEmpty(SessionHandler.spVar_MonthId)) { i++; };
                if (String.IsNullOrEmpty(SessionHandler.spVar_Year)) { i++; };

                if (!String.IsNullOrEmpty(SessionHandler.spVar_SiteId))
                {
                    if (Convert.ToInt32(SessionHandler.spVar_SiteId) > 0 && i == 0)
                    {
                        SessionHandler.spVar_Page = "ComplianceChart2_S_S";
                        
                    }
                }
        // Added By Bishwajit For Item#31
                if (i > 0 && SessionHandler.spVar_Page == "ComplianceChart2_S_S")
                {

                    ASPxComboBox ddlCompanies = new ASPxComboBox();
                    ASPxComboBox ddlSites = new ASPxComboBox();
                    ASPxComboBox ddlMonth = new ASPxComboBox();   
                    ASPxComboBox ddlYear = new ASPxComboBox();                  

                   ddlCompanies = (ASPxComboBox)Reports_DropDownListPicker.FindControl("ddlCompanies");
                    ddlSites = (ASPxComboBox)Reports_DropDownListPicker.FindControl("ddlSites");
                    ddlMonth = (ASPxComboBox)Reports_DropDownListPicker.FindControl("ddlMonth");   
                    ddlYear = (ASPxComboBox)Reports_DropDownListPicker.FindControl("ddlYear");                  

                    if(ddlSites.SelectedItem == null)
                    {
                        SessionHandler.spVar_SiteId = null;
                    }
                    else
                    {
                        SessionHandler.spVar_SiteId = ddlSites.SelectedItem.Value.ToString();
                    }
                    if (SessionHandler.spVar_CompanyId == null || SessionHandler.spVar_CompanyId.ToString() == "")
                    {
                        SessionHandler.spVar_CompanyId = ddlCompanies.SelectedItem.Value.ToString();
                    }

                    if (SessionHandler.spVar_MonthId == null || SessionHandler.spVar_MonthId.ToString() == "")
                    {
                        if (ddlMonth.SelectedItem != null && ddlMonth.SelectedItem.Value != null)
                        {
                            SessionHandler.spVar_MonthId = ddlMonth.SelectedItem.Value.ToString();
                        }
                    }

                    if (SessionHandler.spVar_Year == null || SessionHandler.spVar_Year.ToString() == "")
                    {
                        if (ddlYear.SelectedItem.Value != null)
                        {
                            SessionHandler.spVar_Year = ddlYear.SelectedItem.Value.ToString();
                        }
                    }

                }

                if ((i == 0 || SessionHandler.spVar_Page == "ComplianceChart2_S_S") && SessionHandler.spVar_SiteId!="")
                {
                    if (SessionHandler.spVar_CompanyId != "-1")
                    {
                        //End Added By Bishwajit For Item#31 
                        // DT 3224 Changes By Ratikanta         
           
                           Control uc = (UserControl) LoadControl(String.Format("~/UserControls/Main/ComplianceCharts2/{0}.ascx", SessionHandler.spVar_Page));
                           if (!IsPostBack || Convert.ToBoolean(Session["IsUserControlToAdd"]) == true)
                           {
                               cp.RequestLifetime.InjectProperties(uc);
                               PlaceHolder1.Controls.Add(uc);
                               Session["IsUserControlToAdd"] = true;
                           }                                                       
                        
                        btnPdf.Visible = true;
                        //Added By Bishwajit For Item#31
                    }
                    else if (SessionHandler.spVar_Page == "ComplianceChart2_A_A")
                    {
                        Control uc = LoadControl(String.Format("~/UserControls/Main/ComplianceCharts2/{0}.ascx", SessionHandler.spVar_Page));
                        cp.RequestLifetime.InjectProperties(uc);
                        PlaceHolder1.Controls.Add(uc);
                        btnPdf.Visible = true;
                    }
                    
                    //End Added By Bishwajit For Item#31
                    else if (SessionHandler.spVar_Page == "ComplianceChart2_S_S" && SessionHandler.spVar_CompanyId=="-1")
                    {
                        Control uc = LoadControl(String.Format("~/UserControls/Main/ComplianceCharts2/{0}.ascx", "ComplianceChart2_A_A"));
                        cp.RequestLifetime.InjectProperties(uc);
                        PlaceHolder1.Controls.Add(uc);
                        btnPdf.Visible = true;
                        SessionHandler.spVar_Page = "ComplianceChart2_A_A";
                    }
                    
                }
            }
        }
        else
        {
            SessionHandler.spVar_Page = "ComplianceChart2_A_A";
            SessionHandler.spVar_CompanyId = "-1";
            RegionsService rService = new RegionsService();
            Regions region_AU = rService.GetByRegionInternalName("AU");
            SessionHandler.spVar_SiteId = "-" + region_AU.RegionId.ToString();

            SessionHandler.spVar_MonthId = "0";

            DataTable dt = ((DataView)sqldsKpi_GetAllYearsSubmitted.Select(DataSourceSelectArguments.Empty)).ToTable();
            int r = dt.Rows.Count - 1;
            int latestYear = 0;
            Int32.TryParse(dt.Rows[r].ItemArray[0].ToString(), out latestYear);

            if (latestYear == 0)
            {
                latestYear = DateTime.Now.Year;
            }

            if (DateTime.Now.Year == latestYear)
            {
                if (DateTime.Now.Month == 1)
                {
                    latestYear = DateTime.Now.Year - 1;
                }
            }

            SessionHandler.spVar_Year = latestYear.ToString();

            Control uc = LoadControl(String.Format("~/UserControls/Main/ComplianceCharts2/{0}.ascx", SessionHandler.spVar_Page));
            cp.RequestLifetime.InjectProperties(uc);
            PlaceHolder1.Controls.Add(uc);
			// Added By Bishwajit For Item#31
            btnPdf.Visible = true;
			//End Added By Bishwajit For Item#31
        }
        
        //Add to show for Legend - Added By Bishwajit For Item#31
        if (SessionHandler.spVar_Page == "ComplianceChart2_A_A")
        {
            divLegend.Style.Add(HtmlTextWriterStyle.Display, "block");
        }
        else
        {
            divLegend.Style.Add(HtmlTextWriterStyle.Display, "none");
        }
		//End Added By Bishwajit For Item#31
        if (!postBack)
        {
            //To check if PDF is will be opened or not
            
            //if (WebChartControl.ProcessImageRequest(this.Page))
            //{
            //    return;
            //}
            //SessionHandler.spVar_CompanyId = "-1";
            switch (auth.RoleId)
            {
                     
                case ((int)RoleList.Reader): //Reader
                    Session["IsTableVisible"] = true;
                    break;
                case ((int)RoleList.Contractor): //Contributor
                    //SessionHandler.spVar_Page = "ComplianceChart_S_A";
                    //SessionHandler.spVar_CompanyId = auth.CompanyId.ToString();
                    //SessionHandler.spVar_SiteId = "-5";
                   // SessionHandler.spVar_ResidentialStatus = "3";
                    //SessionHandler.spVar_Year = DateTime.Now.Year.ToString();
                    ////Response.Redirect("ComplianceChart.aspx?id=" + SessionHandler.spVar_Page, true);
                   // Session["IsReturnedFromSS"] = false;
                    Session["IsTableVisible"] = true;
                    break;
                case ((int)RoleList.Administrator): //Administrator
				//Added By Bishwajit For Item#31
                    if (SessionHandler.spVar_Page != "ComplianceChart2_S_S")
                    {
                        Session["IsTableVisible"] = true;
                        btnPdf.Visible = true;
                    }
					//End Added By Bishwajit For Item#31
                    break;
                default:
                    // do something
                    Response.Redirect("AccessDenied.aspx?error=" + Server.UrlEncode("Unrecognised User."));
                    break;
            }
        }
        //if (Session["IsTableVisible"] != null || Session["IsReturnedFromSS"] != null)
        //{
        //    //btnPdf.Visible = false;
        //    if (Convert.ToBoolean(Session["IsTableVisible"]) || Convert.ToBoolean(Session["IsReturnedFromSS"]))
        //    {
        //        btnPdf.Visible = true;
        //    }
        //    else
        //    {
        //        btnPdf.Visible = false;
        //    }
        //}
        //else
        //{
        //    btnPdf.Visible = false;
        //}
    }

    void DropDownListPicker_btnHandler(String strvalue)
    {
        try
        {

            PlaceHolder1.Controls.Clear();

            ASPxComboBox ddlCompanies = (ASPxComboBox)Reports_DropDownListPicker.FindControl("ddlCompanies");
            ASPxComboBox ddlSites = (ASPxComboBox)Reports_DropDownListPicker.FindControl("ddlSites");
            ASPxComboBox ddlMonth = (ASPxComboBox)Reports_DropDownListPicker.FindControl("ddlMonth");
            ASPxComboBox ddlYear = (ASPxComboBox)Reports_DropDownListPicker.FindControl("ddlYear");

            string sCompany = ddlCompanies.SelectedItem.Value.ToString();
            string sSite = ddlSites.SelectedItem.Value.ToString();
            string sMonth = ddlMonth.SelectedItem.Value.ToString();
            string sYear = ddlYear.SelectedItem.Value.ToString();

            int i = 0;
            if (String.IsNullOrEmpty(sCompany)) { i++; };
            if (String.IsNullOrEmpty(sSite)) { i++; };
            if (String.IsNullOrEmpty(sMonth)) { i++; };
            if (String.IsNullOrEmpty(sYear)) { i++; };
            if ((sCompany) == "0") i++;
            if ((sSite) == "0") i++;
            if (i == 0)
            {
                //To Show Table true from PDF file - //Added By Bishwajit For Item#31
                Session["IsTableVisible"]=true;
				//End Added By Bishwajit For Item#31
                SessionHandler.spVar_CompanyId = sCompany;
                SessionHandler.spVar_SiteId = sSite;

                if (sSite.Contains("-"))
                {
                    SessionHandler.spVar_RegionId = sSite.Replace("-", "");
                }
                else
                {
                    SessionHandler.spVar_RegionId = null;
                }

                SessionHandler.spVar_SiteName = ddlSites.SelectedItem.Text;
                SessionHandler.spVar_Year = sYear;
                SessionHandler.spVar_MonthId = sMonth; //YTD

                SessionHandler.spVar_Page = "ComplianceChart2_A_A";
                if (Convert.ToInt32(sCompany) > 0)
                {
                    if (!String.IsNullOrEmpty(SessionHandler.spVar_RegionId))
                    {
                        SessionHandler.spVar_Page = "ComplianceChart2_S_A";
                    }
                    else
                    {
                        SessionHandler.spVar_Page = "ComplianceChart2_S_S";
                    }
                }

                Response.Redirect("ComplianceChart2.aspx?id=" + SessionHandler.spVar_Page, false);
            }
            else
            {
                //To Show Table true from PDF file - Added By Bishwajit For Item#31
                Session["IsTableVisible"]=false;
				//End Added By Bishwajit For Item#31
                PopUpErrorMessage("Please Select All Search Fields.");
            }
        }
        catch (Exception ex)
        {
            Elmah.ErrorSignal.FromContext(Context).Raise(ex);
            if (ex.Message == "Object reference not set to an instance of an object.")
            {
                PopUpErrorMessage("Please Select All Search Fields.");
            }
            else
            {
                PopUpErrorMessage("Error Occured: " + ex.Message);
            }
        }
    }

    protected void PopUpErrorMessage(string errormsg)
    {
        //ASPxGridView1.Visible = false;
        PopupWindow pcWindow = new PopupWindow(errormsg);
        pcWindow.FooterText = "";
        pcWindow.ShowOnPageLoad = true;
        pcWindow.Modal = true;
        //ASPxPopupControl ASPxPopupControl1 = (ASPxPopupControl)Page.Master.FindControl("ASPxPopupControl1");
        ASPxPopupControl1.Windows.Add(pcWindow);
        //WebChartControl1.Visible = false;
    }





    protected string getBGColor(string value)
    {
        string color = string.Empty;
        try
        {
            decimal intValue = Convert.ToDecimal(value);

            if (intValue == 100)
            {
                color = "Lime";
            }
            else if (intValue >= 80)
            {
                color = "Yellow";
            }
            else if (intValue < 80)
            {
                color = "Red";
            }
        }
        catch (Exception)
        {
            color = "White";
        }
        return color;

    }

    protected string getBGColorCSA(string value, string value2, string value3)
    {
        string color = string.Empty;
        if (value3 != "Alcoa Annual Audit Score (%)"
                       && value3 != "CSA - Self Audit (% Score) - Current Qtr")
        {
            try
            {
                decimal intValue = Convert.ToDecimal(value);
                decimal intValue2 = Convert.ToDecimal(value2);

                if (intValue >= intValue2)
                {
                    color = "Lime";
                }

                else if (intValue < intValue2)
                {
                    color = "Red";
                }
            }
            catch (Exception)
            {
                color = "White";
            }
        }
        else 
        {
            color = "White";
        }
        return color;

    }





    protected void btnPdf_Click(object sender, EventArgs e)
    {

        try
        {
            if (Session["IsTableVisible"] != null ||  Session["IsReturnedFromSS"]!=null)
            {
                //btnPdf.Visible = false;
                if (Convert.ToBoolean(Session["IsTableVisible"]) || Convert.ToBoolean(Session["IsReturnedFromSS"]))
                {
                    Session["IsReturnedFromSS"] = false;
                    string strSummary1 = string.Empty;
                    string strSummary2 = string.Empty;
                    string strSummary3 = string.Empty;
                    string strSummary4 = string.Empty;

                    string strSummarySS = string.Empty;

                    string strEmbSummary = string.Empty;
                    string strNonEmbSummary1 = string.Empty;
                    string strNonEmbSummary2 = string.Empty;
                    string strNonEmbSummary3 = string.Empty;


                    string strEmbYtd = string.Empty;
                    string strNonEmbYtd1 = string.Empty;
                    string strNonEmbYtd2 = string.Empty;
                    string strNonEmbYtd3 = string.Empty;

                    string strIndicator1 = string.Empty;
                    string strIndicator2 = string.Empty;
                    string strIndicator3 = string.Empty;
                    string strIndicator4 = string.Empty;

                    string header = string.Empty;

                    int siteId = 0;
                    int yearId = 0;
                    int monthId = 0;


                    string strFilter = GetFilter();



                    if (Convert.ToString(SessionHandler.spVar_Page) == "ComplianceChart2_A_A")
                    {
                        //if (Session["Radar_Table0"] != null)
                        //{
                        //    DataTable dtYtdSummary = (DataTable)Session["Radar_Table0"];

                        //   strSummary = CreateSummaryTable(dtYtdSummary);
                        //}

                        if (Session["Radar_Table1-Summary"] != null)
                        {
                            siteId = Convert.ToInt32(SessionHandler.spVar_SiteId);
                            yearId = Convert.ToInt32(SessionHandler.spVar_Year);
                            monthId = Convert.ToInt32(SessionHandler.spVar_MonthId);

                            if (siteId < 0) //Region
                            {
                                if (siteId == -2)
                                {
                                    DataSet dsCatSummary = (DataSet)Session["Radar_Table1-Summary"];

                                    if (Convert.ToBoolean(Session["E"]))
                                    {
                                        if (Session["Radar_Table0"] != null)
                                        {
                                            DataTable dtYtdSummary = (DataTable)Session["Radar_Table0"];

                                            //DT 3133 re-changes
                                            //strSummary1 = CreateSummaryTable(dtYtdSummary, "Embedded");
                                            strSummary1 = CreateSummaryTable(dtYtdSummary, "");
                                        }
                                        strEmbSummary = CreateCatSummaryTable(dsCatSummary.Tables[0], "emb1", "Embedded - LWDFR, TRIFR, AIFR and IFE : Injury Ratio");
                                    }
                                    if (Convert.ToBoolean(Session["NE1"]))
                                    {
                                        if (Session["Radar_Table0"] != null)
                                        {
                                            DataTable dtYtdSummary = (DataTable)Session["Radar_Table0"];

                                            strSummary2 = CreateSummaryTable(dtYtdSummary, "Non Embedded 1");
                                        }

                                        strNonEmbSummary1 = CreateCatSummaryTable(dsCatSummary.Tables[1], "nonemb1", "Non Embedded 1 - LWDFR, TRIFR, AIFR and IFE : Injury Ratio ");
                                    }
                                    if (Convert.ToBoolean(Session["NE2"]))
                                    {
                                        if (Session["Radar_Table0"] != null)
                                        {
                                            DataTable dtYtdSummary = (DataTable)Session["Radar_Table0"];

                                            strSummary3 = CreateSummaryTable(dtYtdSummary, "Non Embedded 2");
                                        }

                                        strNonEmbSummary2 = CreateCatSummaryTable(dsCatSummary.Tables[2], "nonemb2", "Non Embedded 2 - LWDFR, TRIFR, AIFR and IFE : Injury Ratio ");
                                    }
                                    if (Convert.ToBoolean(Session["NE3"]))
                                    {
                                        if (Session["Radar_Table0"] != null)
                                        {
                                            DataTable dtYtdSummary = (DataTable)Session["Radar_Table0"];

                                            strSummary4 = CreateSummaryTable(dtYtdSummary, "Non Embedded 3");
                                        }

                                        strNonEmbSummary3 = CreateCatSummaryTable(dsCatSummary.Tables[3], "nonemb3", "Non Embedded 3 - LWDFR, TRIFR, AIFR and IFE : Injury Ratio");
                                    }

                                }
                                else
                                {
                                    if (Convert.ToBoolean(Session["E"]))
                                    {

                                        if (Session["Radar_Table0"] != null)
                                        {
                                            DataTable dtYtdSummary = (DataTable)Session["Radar_Table0"];

                                            //DT 3133 re-changes
                                            //strSummary1 = CreateSummaryTable(dtYtdSummary, "Embedded");
                                            strSummary1 = CreateSummaryTable(dtYtdSummary, "");
                                        }

                                        string[] strArr = GetYtdArrForRegions(1);

                                        strEmbSummary = CreateYtdSummaryforRegions(strArr, "Embedded - LWDFR, TRIFR, AIFR and IFE : Injury Ratio", "E");
                                        strEmbYtd = CreateYtdDetailsforRegions(strArr,"E");

                                        string dataValid = string.Empty;
                                        if (Session["LastViewedAt_E"] != null)
                                        {
                                            dataValid=Session["LastViewedAt_E"].ToString();
                                        }
                                        strIndicator1 = GetIndicator(dataValid);
                                      
                                    }

                                    if (Convert.ToBoolean(Session["NE1"]))
                                    {

                                        if (Session["Radar_Table0"] != null)
                                        {
                                            DataTable dtYtdSummary = (DataTable)Session["Radar_Table0"];

                                            strSummary2 = CreateSummaryTable(dtYtdSummary, "Non Embedded 1");
                                        }
                                        string[] strArr = GetYtdArrForRegions(2);

                                        strNonEmbSummary1 = CreateYtdSummaryforRegions(strArr, "Non Embedded 1 - LWDFR, TRIFR, AIFR and IFE : Injury Ratio", "NE1");
                                        strNonEmbYtd1 = CreateYtdDetailsforRegions(strArr,"NE1");
                                        string dataValid = string.Empty;
                                        if (Session["LastViewedAt_NE1"] != null)
                                        {
                                            dataValid = Session["LastViewedAt_NE1"].ToString();
                                        }
                                        strIndicator2 = GetIndicator(dataValid);
                                        
                                    }

                                    if (Convert.ToBoolean(Session["NE2"]))
                                    {

                                        if (Session["Radar_Table0"] != null)
                                        {
                                            DataTable dtYtdSummary = (DataTable)Session["Radar_Table0"];

                                            strSummary3 = CreateSummaryTable(dtYtdSummary, "Non Embedded 2");
                                        }
                                        string[] strArr = GetYtdArrForRegions(3);

                                        strNonEmbSummary2 = CreateYtdSummaryforRegions(strArr, "Non Embedded 2 - LWDFR, TRIFR, AIFR and IFE : Injury Ratio", "NE2");
                                        strNonEmbYtd2 = CreateYtdDetailsforRegions(strArr, "NE2");
                                        string dataValid = string.Empty;
                                        if (Session["LastViewedAt_NE2"] != null)
                                        {
                                            dataValid = Session["LastViewedAt_NE2"].ToString();
                                        }
                                        strIndicator3 = GetIndicator(dataValid);
                                        strIndicator3 = strIndicator3 + "<br/><br/>";
                                    }
                                    if (Convert.ToBoolean(Session["NE3"]))
                                    {
                                        if (Session["Radar_Table0"] != null)
                                        {
                                            DataTable dtYtdSummary = (DataTable)Session["Radar_Table0"];

                                            strSummary4 = CreateSummaryTable(dtYtdSummary, "Non Embedded 3");
                                        }
                                        string[] strArr = GetYtdArrForRegions(4);

                                        strNonEmbSummary3 = CreateYtdSummaryforRegions(strArr, "Non Embedded 3 - LWDFR, TRIFR, AIFR and IFE : Injury Ratio", "NE3");
                                        strNonEmbYtd3 = CreateYtdDetailsforRegions(strArr, "NE3");
                                        string dataValid = string.Empty;
                                        if (Session["LastViewedAt_NE3"] != null)
                                        {
                                            dataValid = Session["LastViewedAt_NE3"].ToString();
                                        }
                                        strIndicator4 = GetIndicator(dataValid);                                     
                                      
                                    }

                                }
                            }
                            else
                            {
                                DataSet dsCatSummary = (DataSet)Session["Radar_Table1-Summary"];
                                SitesService sService = new SitesService();
                                Sites s = sService.GetBySiteId(siteId);
                                if (s == null) throw new Exception("Error Occured whilst Loading Site Columns 1");
                                string SiteName = s.SiteName;
                                if (SiteName.Length > 1) SiteName = s.SiteAbbrev;
                                if (Convert.ToBoolean(Session["E"]))
                                {
                                    if (Session["Radar_Table0"] != null)
                                    {
                                        DataTable dtYtdSummary = (DataTable)Session["Radar_Table0"];

                                        //DT 3133 re-changes
                                        //strSummary1 = CreateSummaryTable(dtYtdSummary, "Embedded");
                                        strSummary1 = CreateSummaryTable(dtYtdSummary, "");
                                    }

                                    strEmbSummary = CreateCatSummaryBySite(dsCatSummary.Tables[0], "emb1", SiteName, "Embedded - LWDFR, TRIFR, AIFR and IFE : Injury Ratio");
                                }
                                if (Convert.ToBoolean(Session["NE1"]))
                                {
                                    if (Session["Radar_Table0"] != null)
                                    {
                                        DataTable dtYtdSummary = (DataTable)Session["Radar_Table0"];

                                        strSummary2 = CreateSummaryTable(dtYtdSummary, "Non Embedded 1");
                                    }

                                    strNonEmbSummary1 = CreateCatSummaryBySite(dsCatSummary.Tables[1], "nonemb1", SiteName, "Non Embedded 1 - LWDFR, TRIFR, AIFR and IFE : Injury Ratio ");
                                }
                                if (Convert.ToBoolean(Session["NE2"]))
                                {
                                    if (Session["Radar_Table0"] != null)
                                    {
                                        DataTable dtYtdSummary = (DataTable)Session["Radar_Table0"];

                                        strSummary3 = CreateSummaryTable(dtYtdSummary, "Non Embedded 2");
                                    }
                                    strNonEmbSummary2 = CreateCatSummaryBySite(dsCatSummary.Tables[2], "nonemb2", SiteName, "Non Embedded 2 - LWDFR, TRIFR, AIFR and IFE : Injury Ratio ");
                                }
                                if (Convert.ToBoolean(Session["NE3"]))
                                {
                                    if (Session["Radar_Table0"] != null)
                                    {
                                        DataTable dtYtdSummary = (DataTable)Session["Radar_Table0"];

                                        strSummary4 = CreateSummaryTable(dtYtdSummary, "Non Embedded 3");
                                    }
                                    strNonEmbSummary3 = CreateCatSummaryBySite(dsCatSummary.Tables[3], "nonemb3", SiteName, "Non Embedded 3 - LWDFR, TRIFR, AIFR and IFE : Injury Ratio");
                                }
                            }
                        }


                        if (Session["Radar_Table2-Ytd"].ToString() != "")
                        {
                            if (siteId < 0) //Region
                            {
                                if (siteId == -2)
                                {

                                    DataSet dsRadarTbl = (DataSet)Session["Radar_Table2-Ytd"];

                                    if (Convert.ToBoolean(Session["E"]))
                                    {
                                        strEmbYtd = CreateCatYtdTable(dsRadarTbl.Tables[0], "emb1",
                                        "The below table indicates the percentage (%) number of companies compliant per Key Performance Indicator (KPI) measure: ");
                                        string dataValid = string.Empty;
                                        if (Session["LastViewedAt_E"] != null)
                                        {
                                            dataValid = Session["LastViewedAt_E"].ToString();
                                        }
                                        strIndicator1 = GetIndicator(dataValid);                                       
                                                                          
                                    }
                                    if (Convert.ToBoolean(Session["NE1"]))
                                    {
                                        strNonEmbYtd1 = CreateCatYtdTable(dsRadarTbl.Tables[1], "nonemb1",
                                        "The below table indicates the percentage (%) number of companies compliant per Key Performance Indicator (KPI) measure: ");
                                        string dataValid = string.Empty;
                                        if (Session["LastViewedAt_NE1"] != null)
                                        {
                                            dataValid = Session["LastViewedAt_NE1"].ToString();
                                        }
                                        strIndicator2 = GetIndicator(dataValid);                                        
                                        
                                    }
                                    if (Convert.ToBoolean(Session["NE2"]))
                                    {
                                        strNonEmbYtd2 = CreateCatYtdTable(dsRadarTbl.Tables[2], "nonemb2",
                                           "The below table indicates the percentage (%) number of companies compliant per Key Performance Indicator (KPI) measure: ");
                                        string dataValid = string.Empty;
                                        if (Session["LastViewedAt_NE2"] != null)
                                        {
                                            dataValid = Session["LastViewedAt_NE2"].ToString();
                                        }
                                        strIndicator3 = GetIndicator(dataValid);                                       
                                        
                                    }
                                    if (Convert.ToBoolean(Session["NE3"]))
                                    {
                                        strNonEmbYtd3 = CreateCatYtdTable(dsRadarTbl.Tables[3], "nonemb3",
                                               "The below table indicates the percentage (%) number of companies compliant per Key Performance Indicator (KPI) measure: ");
                                        string dataValid = string.Empty;
                                        if (Session["LastViewedAt_NE3"] != null)
                                        {
                                            dataValid = Session["LastViewedAt_NE3"].ToString();
                                        }
                                        strIndicator4 = GetIndicator(dataValid);                                      
                                        
                                    }
                                }
                                else
                                { }
                            }
                            else
                            {
                                DataSet dsRadarTbl = (DataSet)Session["Radar_Table2-Ytd"];


                                SitesService sService = new SitesService();
                                Sites s = sService.GetBySiteId(siteId);
                                if (s == null) throw new Exception("Error Occured whilst Loading Site Columns 1");
                                string SiteName = s.SiteName;
                                if (SiteName.Length > 1) SiteName = s.SiteAbbrev;

                                if (Convert.ToBoolean(Session["E"]))
                                {
                                    strEmbYtd = CreateCatYtdBySite(dsRadarTbl.Tables[0], "emb1", SiteName,
                                        "The below table indicates the percentage (%) number of companies compliant per Key Performance Indicator (KPI) measure: ");
                                    string dataValid = string.Empty;
                                    if (Session["LastViewedAt_E"] != null)
                                    {
                                        dataValid = Session["LastViewedAt_E"].ToString();
                                    }
                                    strIndicator1 = GetIndicator(dataValid);                                  
                                    
                                }
                                if (Convert.ToBoolean(Session["NE1"]))
                                {
                                    strNonEmbYtd1 = CreateCatYtdBySite(dsRadarTbl.Tables[1], "nonemb1", SiteName,
                                        "The below table indicates the percentage (%) number of companies compliant per Key Performance Indicator (KPI) measure: ");
                                    string dataValid = string.Empty;
                                    if (Session["LastViewedAt_NE1"] != null)
                                    {
                                        dataValid = Session["LastViewedAt_NE1"].ToString();
                                    }
                                    strIndicator2 = GetIndicator(dataValid);                                  
                                    
                                }
                                if (Convert.ToBoolean(Session["NE2"]))
                                {
                                    strNonEmbYtd2 = CreateCatYtdBySite(dsRadarTbl.Tables[2], "nonemb2", SiteName,
                                           "The below table indicates the percentage (%) number of companies compliant per Key Performance Indicator (KPI) measure: ");
                                    string dataValid = string.Empty;
                                    if (Session["LastViewedAt_NE2"] != null)
                                    {
                                        dataValid = Session["LastViewedAt_NE2"].ToString();
                                    }
                                    strIndicator3 = GetIndicator(dataValid);                                   
                                }
                                if (Convert.ToBoolean(Session["NE3"]))
                                {
                                    strNonEmbYtd3 = CreateCatYtdBySite(dsRadarTbl.Tables[3], "nonemb3", SiteName,
                                               "The below table indicates the percentage (%) number of companies compliant per Key Performance Indicator (KPI) measure: ");
                                    string dataValid = string.Empty;
                                    if (Session["LastViewedAt_NE3"] != null)
                                    {
                                        dataValid = Session["LastViewedAt_NE3"].ToString();
                                    }
                                    strIndicator4 = GetIndicator(dataValid);
                                    
                                }
                            }

                            //strIndicator = GetIndicator();

                        }


                    }
                    else if (Convert.ToString(SessionHandler.spVar_Page) == "ComplianceChart2_S_S")
                    {
                        if (Session["IsValueTrue"] != null || Convert.ToBoolean(Session["IsValueTrue"]) != false)
                        {
                            strSummarySS = CreateHeaderWithSummaryFor_S_S();
                        }
                    }

                    else if (Convert.ToString(SessionHandler.spVar_Page) == "ComplianceChart2_S_A")
                    {


                        int compid = Convert.ToInt32(SessionHandler.spVar_CompanyId);
                        siteId = Convert.ToInt32(SessionHandler.spVar_SiteId);

                        DataSet dsSummary = (DataSet)Session["Radar_Table1-Summary"];



                        if (Session["IsValueTrue"] != null || Convert.ToBoolean(Session["IsValueTrue"]) != false)
                        {
                            if (Convert.ToBoolean(Session["E"]))
                            {
                                string[] strArr = CreateArrayForSA(compid, siteId, 1, 1);
                                if (strArr[1] != "0")
                                {

                                    strEmbSummary = CreateSummaryforSA(strArr, "Embedded", 0);
                                    strEmbYtd = CreateYtdforSA(strArr, "YTD Averages:", 0);
                                }

                            }
                            if (Convert.ToBoolean(Session["E"]))
                            {
                                string[] strArr = CreateArrayForSA(compid, siteId, 1, 2);
                                if (strArr[1] != "0")
                                {
                                    strNonEmbSummary1 = CreateSummaryforSA(strArr, "Non-Embedded 1", 1);
                                    strNonEmbYtd1 = CreateYtdforSA(strArr, "YTD Averages:", 1);
                                }
                            }
                            if (Convert.ToBoolean(Session["NE2"]))
                            {
                                string[] strArr = CreateArrayForSA(compid, siteId, 1, 3);
                                if (strArr[1] != "0")
                                {
                                    strNonEmbSummary2 = CreateSummaryforSA(strArr, "Non-Embedded 2", 2);
                                    strNonEmbYtd2 = CreateYtdforSA(strArr, "YTD Averages:", 2);
                                }
                            }
                            if (Convert.ToBoolean(Session["NE3"]))
                            {
                                string[] strArr = CreateArrayForSA(compid, siteId, 1, 4);

                                if (strArr[1] != "0")
                                {

                                    strNonEmbSummary3 = CreateSummaryforSA(strArr, "Non-Embedded 3", 3);
                                    strNonEmbYtd3 = CreateYtdforSA(strArr, "YTD Averages:", 3);
                                }
                            }





                            //strSummary = CreateHeaderWithSummaryFor_S_S();
                        }
                    }
                  

                    bool isContent = false;

                    Document document = new Document(PageSize.A4, 50, 50, 25, 25);
                   
                    //PdfWriter.GetInstance(document, new FileStream(@"C:\Bishwajit\radar.pdf", FileMode.Create));

                    MemoryStream output = new MemoryStream();
                    Object writer = PdfWriter.GetInstance(document, output);
                    string contents = string.Empty;

                    contents = File.ReadAllText(Server.MapPath(@"pdfTemplateRadarFilter.htm"));

                    contents = contents.Replace("[Filter]", strFilter);
                    contents = contents.Replace("[SummarySS]", strSummarySS);
                    
                    document.Open();

                    List<IElement> parsedHtmlElements5 = iTextSharp.text.html.simpleparser.HTMLWorker.ParseToList(new StringReader(contents), null);
                    foreach (object htmlElement in parsedHtmlElements5)
                    {                        
                        document.Add((IElement)htmlElement);
                    }

                    contents = File.ReadAllText(Server.MapPath(@"pdfTemplateRadarE.htm"));

                    contents = contents.Replace("[Summary1]", strSummary1);
                    contents = contents.Replace("[EmbeddedSummary1]", strEmbSummary);
                    contents = contents.Replace("[EmbeddedYtd1]", strEmbYtd);
                    contents = contents.Replace("[Indicator1]", strIndicator1);


                    List<IElement> parsedHtmlElements = iTextSharp.text.html.simpleparser.HTMLWorker.ParseToList(new StringReader(contents), null);
                    foreach (object htmlElement in parsedHtmlElements)
                    {
                        isContent = true;
                        document.Add((IElement)htmlElement);
                    }
                    if (isContent)
                    {
                        document.NewPage();
                        isContent = false;
                    }


                    contents = File.ReadAllText(Server.MapPath(@"pdfTemplateRadarNE1.htm"));

                    contents = contents.Replace("[Summary2]", strSummary2);
                    contents = contents.Replace("[NonEmbeddedSummary1]", strNonEmbSummary1);
                    contents = contents.Replace("[NonEmbeddedYtd1]", strNonEmbYtd1);
                    contents = contents.Replace("[Indicator2]", strIndicator2);

                    List<IElement> parsedHtmlElements1 = iTextSharp.text.html.simpleparser.HTMLWorker.ParseToList(new StringReader(contents), null);
                    foreach (object htmlElement in parsedHtmlElements1)
                    {
                        isContent = true;
                        document.Add((IElement)htmlElement); 
                    }
                    if (isContent)
                    {
                        document.NewPage();
                        isContent = false;
                    }


                    contents = File.ReadAllText(Server.MapPath(@"pdfTemplateRadarNE2.htm"));

                    contents = contents.Replace("[Summary3]", strSummary3);
                    contents = contents.Replace("[NonEmbeddedSummary2]", strNonEmbSummary2);
                    contents = contents.Replace("[NonEmbeddedYtd2]", strNonEmbYtd2);
                    contents = contents.Replace("[Indicator3]", strIndicator3);

                    List<IElement> parsedHtmlElements2 = iTextSharp.text.html.simpleparser.HTMLWorker.ParseToList(new StringReader(contents), null);
                    foreach (object htmlElement in parsedHtmlElements2)
                    {
                        isContent = true;
                        document.Add((IElement)htmlElement);
                    }
                    if (isContent)
                    {
                        document.NewPage();
                        isContent = false;
                    }


                    contents = File.ReadAllText(Server.MapPath(@"pdfTemplateRadarNE3.htm"));

                    contents = contents.Replace("[Summary4]", strSummary4);
                    contents = contents.Replace("[NonEmbeddedSummary3]", strNonEmbSummary3);
                    contents = contents.Replace("[NonEmbeddedYtd3]", strNonEmbYtd3);
                    contents = contents.Replace("[Indicator4]", strIndicator4);

                    //if (auth._AlcoaDirectHTTPParameter != null)
                    //{
                    //    contents = contents +"  _AlcoaDirectHTTPParameter : " +  auth._AlcoaDirectHTTPParameter.ToString();
                    //}
                    //if (auth.UserId != null)
                    //{
                    //     contents = contents + "UserId : " + auth.UserId.ToString();
                    //}
                    //if (auth.UserLogon != null)
                    //    contents = contents + " UserLogon : " + auth.UserLogon.ToString();
                    //if (auth.CompanyId != null)
                    //     contents = contents + " CompanyId : "+ auth.CompanyId.ToString();

                    List<IElement> parsedHtmlElements3 = iTextSharp.text.html.simpleparser.HTMLWorker.ParseToList(new StringReader(contents), null);
                    foreach (object htmlElement in parsedHtmlElements3)
                    {
                        document.Add((IElement)htmlElement);
                    }             

                    
                    document.Close();

                    //Commented By Bishwajit
                    
                    //if (Directory.Exists(Server.MapPath(@"~/Images/ChartImages/" + auth.UserId.ToString())))
                    //{
                    //    System.IO.DirectoryInfo directory = new DirectoryInfo(Server.MapPath(@"~/Images/ChartImages/" + auth.UserId.ToString()));
                    //    foreach (FileInfo file in directory.GetFiles())
                    //    {
                    //        file.Delete();
                    //    }
                    //    System.IO.Directory.Delete(Server.MapPath(@"~/Images/ChartImages/" + auth.UserId.ToString()));
                    //}

                    //Commented By Bishwajit

                    btnPdf.Visible = true;                    
                    Response.ClearContent();
                    Response.Clear();

                    Response.ContentType = "application/pdf";
                    Response.AddHeader("Content-Disposition", String.Format("attachment;filename={0}.pdf", "Safety KPI Compliance Chart"));
                    Response.BinaryWrite(output.ToArray());

                    

                    //Auth auth1 = new Auth();
                   // Helper._Auth.PageLoad(auth);

                    //Response.TransmitFile(FilePath);
                    //Response.Flush();
                    // Response.End();
                }
            }
        }
        catch (Exception ex)
        {
            //string strMessage = string.Empty;
            //if (Session["ImagePath"] != null)
            //{
            //    strMessage = Session["ImagePath"].ToString();
            //}
            //Response.Write(ex.Message);
            //throw new Exception(ex.Message);
        }

    }



    protected string CreateHeaderWithSummaryFor_S_S()
    {
        StringBuilder sb = new StringBuilder();
        string headerBgColor = "#D1DBF4";

        //string tableBgColor = "#DDECFF"; ///commented for DT# 2967, item 1 on 08/12/2014
        string tableBgColor = "#FFFFFF"; //Added for DT# 2967, item 1 on 08/12/2014


        string colorGreen = "Lime";
        string colorWhite = "White";
        string colorRed = "Red";
        string RedHeaderFormat = "<h5 style='color:Red'>[Header]</h5>";

        sb.Append("<table width='100%' bgcolor='" + tableBgColor + "' align='center' valign='top' style='font-size:7px; color:Black;'>");
        sb.Append("<tr>");
        sb.Append("<td border='1' width='50%' align='center' valign='top' style='font-weight:bold;'>Time Frame: " + Session["DropDownVal"] + "</td>");
        sb.Append("<td border='1' width='50%' align='center' valign='top'>Last Viewed At: " + DateTime.Now.ToString() + "</td>");
        sb.Append("</tr>");

        sb.Append("<tr>");

        sb.Append("<td border='1' width='50%' align='center' valign='top'>");


        sb.Append("<table width='100%' align='center' valign='top' style='font-size:7px; color:Black;'>");
        sb.Append("<tr>");
        sb.Append("<td border='1' width='100%' align='center' valign='top'>");


        #region Table RNCount
        sb.Append("<table width='100%' align='center' border='0' valign='top' style='font-size:6px; color:Black;'>");
        sb.Append("<tr>");
        sb.Append("<td width='25%' align='center' border='0' valign='top' style='font-size:6px; color:Black;'>");
        sb.Append("<b>LWDFR </b> " + Convert.ToString(Session["WDFR"]) + "&nbsp;&nbsp;");
        sb.Append("</td>");
        sb.Append("<td width='25%' align='center' border='0' valign='top' style='font-size:6px; color:Black;'>");
        sb.Append("<b>TRIFR </b> " + Convert.ToString(Session["TRIFR"]) + "&nbsp;&nbsp;");
        sb.Append("</td>");
        sb.Append("<td width='25%' align='center' border='0' valign='top' style='font-size:6px; color:Black;'>");
        sb.Append("<b>AIFR </b> " + Convert.ToString(Session["AIFR"]) + "&nbsp;&nbsp;");
        sb.Append("</td>");
        sb.Append("<td width='25%' align='left' border='0' valign='top' style='font-size:6px; color:Black;'>");
        sb.Append("<b>DART </b> " + Convert.ToString(Session["DART"]) + "&nbsp;&nbsp;");       
        sb.Append("</td>");
        sb.Append("</tr>");
        sb.Append("</table>");

        sb.Append("</td>");
        sb.Append("</tr>");
        sb.Append("<tr>");
        sb.Append("<td border='1' width='100%' align='center' valign='top'>");

        sb.Append("<table width='100%' align='center' border='0' valign='top' style='font-size:6px; color:Black;'>");
        sb.Append("<tr>");
        sb.Append("<td width='33%' align='center' border='0' valign='top' style='font-size:6px; color:Black;'>");
        sb.Append("<b>IFE COUNT </b> " + Convert.ToString(Session["IFECOUNT"]) + "&nbsp;&nbsp;");
        sb.Append("</td>");
        sb.Append("<td width='33%' align='center' border='0' valign='top' style='font-size:6px; color:Black;'>");
        sb.Append("<b>RN COUNT </b> " + Convert.ToString(Session["RNCOUNT"]) + "&nbsp;&nbsp;");
        sb.Append("</td>");
        sb.Append("<td width='34%' align='center' border='0' valign='top' style='font-size:6px; color:Black;'>");
        sb.Append("<b>IFE+RN COUNT </b> " + Convert.ToString(Session["IFERNCount"]) + "&nbsp;&nbsp;");
        sb.Append("</td>");
        sb.Append("</tr>");
        sb.Append("</table>");


        sb.Append("</td>");
        sb.Append("</tr>");
        sb.Append("<tr>");
        sb.Append("<td border='1' width='100%' align='center' valign='top'>");


        sb.Append("<table width='100%' border='0' align='center' valign='top' style='font-size:6px; color:Black;'>");
        sb.Append("<tr>");
        sb.Append("<td width='32%' align='left' border='0' valign='top' style='font-size:6px; color:Black;'>");
        sb.Append("&nbsp;&nbsp;<b>IFE: Injury Ratio </b> " + Convert.ToString(Session["IFEInjuryRatio"]) + "&nbsp;&nbsp;");
        sb.Append("</td>");
        sb.Append("<td width='32%' align='center' border='0' valign='top' style='font-size:6px; color:Black;'>");
        sb.Append("<b>RN: Injury Ratio </b> " + Convert.ToString(Session["RNInjuryRatio"]) + "&nbsp;&nbsp;");
        sb.Append("</td>");
        sb.Append("<td width='36%' align='center' border='0' valign='top' style='font-size:6px; color:Black;'>");
        sb.Append("<b>IFE+RN: Injury Ratio </b> " + Convert.ToString(Session["IFERNInjuryRatio"]) + "&nbsp;&nbsp;");
        sb.Append("</td>");
        sb.Append("</tr>");
        sb.Append("</table>");


        sb.Append("</td>");
        sb.Append("</tr>");
        sb.Append("</table>");


        #endregion

        if (Session["SpiderIsVisible"] != null)
        {
            if (Convert.ToBoolean(Session["SpiderIsVisible"]))
            {
               
                //string imgChartPath = WebConfigurationManager.AppSettings["URLPath"].ToString();
                string imgChartPath =string.Empty;
                string[] arr =  Request.Url.ToString().Split('/');
                for (int i = 0; i < arr.Length - 1; i++)
                {
                    if (i > 0) imgChartPath = imgChartPath + "/";
                    imgChartPath = imgChartPath+arr[i];
                }
                if (Session["ImagePath"] != null)
                {
                    Session["lblImagePath"] = imgChartPath + "/" + Convert.ToString(Session["ImagePath"]).Replace("~/", "");
                    imgChartPath = imgChartPath + "/" + Convert.ToString(Session["ImagePath"]).Replace("~/", "");
                }
                else 
                {
                    imgChartPath = imgChartPath + "/Images/ArticleIcon.gif";
                }
                //imgChartPath = imgChartPath + "Images/ChartImages/" + auth.UserId.ToString() + "/2.png";
                sb.Append("<img src='" + imgChartPath + "' alt='img'/>");

            }
        }

        sb.Append("</td>");

        sb.Append("<td border='1' width='50%' align='center' valign='top'>");

        if (Session["Radar_Table1-Summary"] != null)
        {
            string siteName = string.Empty;
            DataSet dsSummary = (DataSet)Session["Radar_Table1-Summary"];
            if (dsSummary.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < dsSummary.Tables[0].Rows.Count; i++)
                {
                    int siteId = Convert.ToInt32(SessionHandler.spVar_SiteId);
                    SitesService sService = new SitesService();
                    Sites s = sService.GetBySiteId(siteId);
                    if (s != null)
                    {
                        if (s.SiteName.Length > 10)
                        { siteName = s.SiteAbbrev; }
                        else { siteName = s.SiteName; };
                    }
                    #region Table Summary


                    sb.Append("<table width='100%' align='center' valign='top' border='1' style='font-size:7px; color:Black;'>");

                    sb.Append("<tr>");
                    sb.Append("<td border='1' width='60%' bgcolor='" + headerBgColor + "' align='left' valign='top' style='font-weight:bold;'>Key Performance Indicator (KPI)</td>");
                    sb.Append("<td border='1' width='40%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>" + siteName + "</td>");
                    sb.Append("</tr>");



                    sb.Append("<tr>");
                    sb.Append("<td border='1' bgcolor='" + colorWhite + "' width='60%' align='left' valign='top'>");
                    sb.Append(dsSummary.Tables[0].Rows[i]["KpiMeasure"].ToString());
                    sb.Append("</td>");
                    sb.Append("<td border='1' bgcolor='" + colorWhite + "' width='40%' align='center' valign='top'>");
                    sb.Append(dsSummary.Tables[0].Rows[i]["KpiScoreYtd"].ToString());
                    sb.Append("</td>");
                    sb.Append("</tr>");
                    sb.Append("</table>");
                    #endregion
                }
            }
        }

        if (Session["Radar_Table2-Ytd"] != null)
        {
            DataSet dsYtd = (DataSet)Session["Radar_Table2-Ytd"];
            if (dsYtd.Tables[0].Rows.Count > 0)
            {
                string siteName = string.Empty;
                int siteId = Convert.ToInt32(SessionHandler.spVar_SiteId);
                SitesService sService = new SitesService();
                Sites s = sService.GetBySiteId(siteId);
                if (s != null)
                {
                    if (s.SiteName.Length > 10)
                    { siteName = s.SiteAbbrev; }
                    else { siteName = s.SiteName; };
                }
                int intGreen = 0;
                int intTotal = 0;

                string strHeader = string.Empty;
                strHeader="YTD Averages:";
                if (Session["DropDownVal"] != null)
                {
                    if (Session["DropDownVal"].ToString() != "[YTD]")
                    {
                        strHeader = Session["DropDownVal"].ToString();
                    }
                }

                sb.Append(RedHeaderFormat.Replace("[Header]", "YTD Averages:"));

                #region Table Ytd
                sb.Append("<table width='100%' align='center' valign='top' border='1' style='font-size:7px; color:Black;'>");

                sb.Append("<tr>");


               


                sb.Append("<td border='1' width='45%' bgcolor='" + headerBgColor + "' align='left' valign='top' style='font-weight:bold;'>Key Performance Indicator (KPI)</td>");
                sb.Append("<td border='1' width='20%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>Ytd Plan</td>");
                sb.Append("<td border='1' width='15%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>" + siteName + "</td>");
                sb.Append("<td border='1' width='10%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>%</td>");
                sb.Append("</tr>");

                for (int i = 0; i < dsYtd.Tables[0].Rows.Count; i++)
                {

                    sb.Append("<tr>");

                    sb.Append("<td border='1' bgcolor='" + colorWhite + "' width='45%' align='left' valign='top'>");
                    sb.Append(dsYtd.Tables[0].Rows[i]["KpiMeasure"].ToString());
                    sb.Append("</td>");

                    sb.Append("<td border='1' bgcolor='" + colorWhite + "' width='20%' align='center' valign='top'>");
                    sb.Append(dsYtd.Tables[0].Rows[i]["PlanYtd"].ToString());
                    sb.Append("</td>");
                    if (dsYtd.Tables[0].Rows[i]["KpiMeasure"].ToString() == "Alcoa Annual Audit Score (%)")
                    {
                        if ((dsYtd.Tables[0].Rows[i]["PlanYtd"].ToString() != "" && dsYtd.Tables[0].Rows[i]["KpiScoreYtd"].ToString() != "") && Convert.ToInt32(dsYtd.Tables[0].Rows[i]["PlanYtd"]) > Convert.ToInt32(dsYtd.Tables[0].Rows[i]["KpiScoreYtd"]))
                        {
                            sb.Append("<td border='1' bgcolor='" + colorRed + "' width='15%' align='center' valign='top'>");
                        }
                        else if ((dsYtd.Tables[0].Rows[i]["PlanYtd"].ToString() != "" && dsYtd.Tables[0].Rows[i]["KpiScoreYtd"].ToString() != "") && Convert.ToInt32(dsYtd.Tables[0].Rows[i]["PlanYtd"]) <= Convert.ToInt32(dsYtd.Tables[0].Rows[i]["KpiScoreYtd"]))
                        {
                            sb.Append("<td border='1' bgcolor='" + colorGreen + "' width='15%' align='center' valign='top'>");
                        }
                        else
                        {
                            sb.Append("<td border='1' bgcolor='" + colorWhite + "' width='15%' align='center' valign='top'>");
                        }
                    }
                    else
                    {
                        sb.Append("<td border='1' bgcolor='" + colorWhite + "' width='15%' align='center' valign='top'>");
                    }
                    sb.Append(dsYtd.Tables[0].Rows[i]["KpiScoreYtd"].ToString());
                    sb.Append("</td>");

                    if (dsYtd.Tables[0].Rows[i]["KpiScoreYtdPercentage"].ToString() == "100")
                    {
                        sb.Append("<td border='1' bgcolor='" + colorGreen + "' width='10%' align='center' valign='top'>");
                        intGreen++;
                        intTotal++;
                    }
                    else if (dsYtd.Tables[0].Rows[i]["KpiScoreYtdPercentage"].ToString() == "-")
                    {
                        sb.Append("<td border='1' bgcolor='" + colorWhite + "' width='10%' align='center' valign='top'>");

                    }
                    else
                    {
                        sb.Append("<td border='1' bgcolor='" + colorRed + "' width='10%' align='center' valign='top'>");
                        intTotal++;
                    }

                    sb.Append(dsYtd.Tables[0].Rows[i]["KpiScoreYtdPercentage"].ToString());
                    sb.Append("</td>");
                    sb.Append("</tr>");

                }
                string percentage = string.Empty;
                if (intTotal == 0)
                {
                    percentage = "n/a";
                }
                else
                {
                    decimal dInt= Math.Round(Convert.ToDecimal(intGreen * 100) / intTotal);
                
                    percentage = dInt.ToString()+"%";
                    if (((intGreen * 100) / intTotal) == 0)
                    {
                        percentage = "n/a";
                    }
                }

                sb.Append("<tr>");
                sb.Append("<td border='1' bgcolor='" + headerBgColor + "' width='45%' align='center' valign='top'>");
                sb.Append("</td>");
                sb.Append("<td border='1' bgcolor='" + headerBgColor + "' width='20%' align='center' valign='top'>");
                sb.Append("</td>");
                sb.Append("<td border='1' bgcolor='" + headerBgColor + "' width='15%' align='center' valign='top'>");
                sb.Append("</td>");
                sb.Append("<td border='1' bgcolor='" + headerBgColor + "' width='10%' align='center' valign='top'>");
                sb.Append(percentage);
                sb.Append("</td>");
                sb.Append("</tr>");


                sb.Append("</table>");
                #endregion
            }
        }
        sb.Append("</td>");
        sb.Append("</tr>");

        sb.Append("<tr>");
        sb.Append("<td colspan='2' border='1' width='100%' align='center' valign='top'>");

        #region Table TraficLight
        //comment part starts for DT# 2967 on 08/12/2014
        //sb.Append("<table width='100%'  border='0' cellpadding='0' cellspacing='0' align='center' bgcolor='" + colorWhite + "' valign='top' style='font-size:7px; color:Black;'>");
        //sb.Append("<tr>");

        //sb.Append("<td width='10%' style='padding:1px 0px 3px 5px' valign='top'>");
        //if (Session["SQColor"] != null)
        //{
        //    sb.Append(GetImage(Convert.ToInt32(Session["SQColor"])));
            
        //    sb.Append("&nbsp;&nbsp;");
            
        //    sb.Append(Session["SQText"].ToString());
        //    //sb.Append("<table width='100%' align='left'><tr><td width='7%' valign='top' align='left'>");
        //    //sb.Append(GetImage(Convert.ToInt32(Session["SQColor"])));
        //    //sb.Append("</td>");
        //    //sb.Append("<td width='93%' align='left' valign='top'>");
        //    //sb.Append("&nbsp;&nbsp;");
        //    //sb.Append(Session["SQText"].ToString());
        //    //sb.Append("</td></tr></table>");
        //}
        //sb.Append("</td>");

        //sb.Append("<td width='50%' style='padding:1px 0px 3px 5px' valign='top' align='left' valign='top'>");
        //if (Session["SCColor"] != null)
        //{
        //    sb.Append(GetImage(Convert.ToInt32(Session["SCColor"])));
        //    sb.Append("&nbsp;&nbsp;");
        //    sb.Append(Session["SCText"].ToString());

        //    //sb.Append("<table width='100%' valign='top' align='left'><tr><td width='7%' valign='bottom' align='left'>");
        //    //sb.Append(GetImage(Convert.ToInt32(Session["SCColor"])));
        //    //sb.Append("</td>");
        //    //sb.Append("<td width='93%' align='left' valign='top'>");
        //    ////sb.Append("&nbsp;&nbsp;");
        //    //sb.Append(Session["SCText"].ToString());
        //    //sb.Append("</td></tr></table>");
        //}
        //sb.Append("</td>");

        //sb.Append("</tr>");

        //sb.Append("<tr>");
        //sb.Append("<td width='50%' style='padding:1px 0px 3px 5px' valign='top' align='left' valign='top'>");
        //if (Session["CCColor"] != null)
        //{
        //    sb.Append(GetImage(Convert.ToInt32(Session["CCColor"])));
        //    sb.Append("&nbsp;&nbsp;");
        //    sb.Append(Session["CCText"].ToString());

        //    //    sb.Append("<table width='100%' align='left'><tr><td width='7%' valign='top' align='left'>");
        //    //    sb.Append(GetImage(Convert.ToInt32(Session["CCColor"])));
        //    //    sb.Append("</td>");
        //    //    sb.Append("<td width='93%' align='left' valign='top'>");
        //    //    sb.Append("&nbsp;&nbsp;");
        //    //    sb.Append(Session["CCText"].ToString());
        //    //    sb.Append("</td></tr></table>");
        //}
        //sb.Append("</td>");
        //sb.Append("<td width='50%' style='padding:0px 0px 3px 5px' align='left' valign='top'>");

        //sb.Append("</td>");
        //sb.Append("</tr>");

        //sb.Append("</table>");
        //comment part ends for DT# 2967 on 08/12/2014

        //Added for DT# 2967, item 3 on 08/12/2014 by Subhro
        sb.Append("<table width='100%'  border='0' cellpadding='0' cellspacing='0' align='center' Font-Names='Tahoma' bgcolor='" + colorWhite + "'  style='font-size:8px; color:Black;'>");
        sb.Append("<tr>");

        sb.Append("<td width='3%' style='padding:0px 0px 3px 3px' valign='Bottom' align='left'>");
        if (Session["SQColor"] != null)
        {
            sb.Append(GetImage(Convert.ToInt32(Session["SQColor"])));
            sb.Append("</td>");
            sb.Append("<td width='40%' style='padding:1px 0px 3px 3px' valign='top' align='left'>");
            sb.Append(Session["SQText"].ToString());
        }
        sb.Append("</td>");

        sb.Append("<td width='3%' style='padding:1px 0px 3px 3px' align='left' valign='Bottom'>");
        if (Session["SCColor"] != null)
        {
            sb.Append(GetImage(Convert.ToInt32(Session["SCColor"])));
            sb.Append("</td>");
            sb.Append("<td width='40%' style='padding:1px 0px 3px 3px' valign='top' align='left' valign='top'>");
            sb.Append(Session["SCText"].ToString());
        }
        sb.Append("</td>");

        sb.Append("</tr>");

        sb.Append("<tr>");
        sb.Append("<td width='3%' style='padding:1px 0px 3px 3px' valign='Bottom' align='left'>");
        if (Session["CCColor"] != null)
        {
            sb.Append(GetImage(Convert.ToInt32(Session["CCColor"])));
            sb.Append("</td>");
            sb.Append("<td width='40%' style='padding:1px 0px 3px 3px' align='left' valign='top'>");
            sb.Append(Session["CCText"].ToString());
        }
        sb.Append("</td>");
        sb.Append("<td width='3%' style='padding:1px 0px 3px 3px' valign='Bottom' align='left'>");
        sb.Append("</td>");
        sb.Append("<td width='40%' style='padding:1px 0px 3px 3px' align='left' valign='top'>");
        sb.Append("</td>");
        sb.Append("</tr>");

        sb.Append("</table>");
        // end

        #endregion

        sb.Append("</td>");

        sb.Append("</tr>");

        sb.Append("<tr>");
        sb.Append("<td border='1' colspan='2' width='100%' align='center' valign='top'>");
        if (Session["Radar_Table2-YtdMonth"] != null)
        {
            DataSet dsYthMon = (DataSet)Session["Radar_Table2-YtdMonth"];
            if (dsYthMon.Tables[0].Rows.Count > 0)
            {
                sb.Append(RedHeaderFormat.Replace("[Header]", "Monthly Progress:"));

                #region Table YTD Month
                sb.Append("<table width='100%' align='center' valign='top' border='1' style='font-size:7px; color:Black;'>");

                sb.Append("<tr>");
                sb.Append("<td border='1' width='42%' bgcolor='" + headerBgColor + "' align='left' valign='top' style='font-weight:bold;'>Key Performance Indicator (KPI)</td>");
                sb.Append("<td border='1' width='10%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>Mth Plan</td>");


                sb.Append("<td border='1' width='4%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>Jan</td>");
                sb.Append("<td border='1' width='4%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>Feb</td>");
                sb.Append("<td border='1' width='4%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>Mar</td>");
                sb.Append("<td border='1' width='4%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>Apr</td>");
                sb.Append("<td border='1' width='4%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>May</td>");
                sb.Append("<td border='1' width='4%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>Jun</td>");

                sb.Append("<td border='1' width='4%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>Jul</td>");
                sb.Append("<td border='1' width='4%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>Aug</td>");
                sb.Append("<td border='1' width='4%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>Sep</td>");
                sb.Append("<td border='1' width='4%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>Oct</td>");
                sb.Append("<td border='1' width='4%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>Nov</td>");
                sb.Append("<td border='1' width='4%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>Dec</td>");
                sb.Append("</tr>");

                for (int i = 0; i < dsYthMon.Tables[0].Rows.Count; i++)
                {

                    sb.Append("<tr>");
                    // sb.Append("<td border='1' bgcolor='" + colorWhite + "' width='42%' align='left' valign='top' style='font-weight:bold;'>"); //commented for DT 2967, item 2 on 08/12/2014 by Subhro
                    sb.Append("<td border='1' bgcolor='" + colorWhite + "' width='42%' align='left' valign='top'>"); // Added for DT# 2967, item 2 on 08/12/2014
                    sb.Append(dsYthMon.Tables[0].Rows[i]["KpiMeasure"].ToString());
                    sb.Append("</td>");
                    //sb.Append("<td border='1' bgcolor='" + colorWhite + "' width='10%' align='center' valign='top' style='font-weight:bold;'>"); //commented for DT 2967, item 2 on 08/12/2014 by Subhro
                    sb.Append("<td border='1' bgcolor='" + colorWhite + "' width='10%' align='center' valign='top'>"); // Added for DT# 2967, item 2 on 08/12/2014
                    sb.Append(dsYthMon.Tables[0].Rows[i]["PlanMonth"].ToString());
                    sb.Append("</td>");

                    if (dsYthMon.Tables[0].Rows[i]["KpiScoreMonth1"] != null)
                    {
                        sb.Append(GetYtdSummaryTD(dsYthMon.Tables[0].Rows[i]["KpiScoreMonth1"].ToString(), dsYthMon.Tables[0].Rows[i]["PlanMonth"].ToString()));

                    }
                    else
                    {
                        sb.Append("<td border='1' bgcolor='" + colorRed + "' width='4%' align='center' valign='top' style='font-weight:bold;'>");
                    }
                    sb.Append(dsYthMon.Tables[0].Rows[i]["KpiScoreMonth1"].ToString());
                    sb.Append("</td>");



                    if (dsYthMon.Tables[0].Rows[i]["KpiScoreMonth2"] != null)
                    {
                        sb.Append(GetYtdSummaryTD(dsYthMon.Tables[0].Rows[i]["KpiScoreMonth2"].ToString(), dsYthMon.Tables[0].Rows[i]["PlanMonth"].ToString()));

                    }
                    else
                    {
                        sb.Append("<td border='1' bgcolor='" + colorRed + "' width='4%' align='center' valign='top' style='font-weight:bold;'>");
                    }
                    sb.Append(dsYthMon.Tables[0].Rows[i]["KpiScoreMonth2"].ToString());
                    sb.Append("</td>");



                    if (dsYthMon.Tables[0].Rows[i]["KpiScoreMonth3"] != null)
                    {
                        sb.Append(GetYtdSummaryTD(dsYthMon.Tables[0].Rows[i]["KpiScoreMonth3"].ToString(), dsYthMon.Tables[0].Rows[i]["PlanMonth"].ToString()));

                    }
                    else
                    {
                        sb.Append("<td border='1' bgcolor='" + colorRed + "' width='4%' align='center' valign='top' style='font-weight:bold;'>");
                    }
                    sb.Append(dsYthMon.Tables[0].Rows[i]["KpiScoreMonth3"].ToString());
                    sb.Append("</td>");



                    if (dsYthMon.Tables[0].Rows[i]["KpiScoreMonth4"] != null)
                    {
                        sb.Append(GetYtdSummaryTD(dsYthMon.Tables[0].Rows[i]["KpiScoreMonth4"].ToString(), dsYthMon.Tables[0].Rows[i]["PlanMonth"].ToString()));

                    }
                    else
                    {
                        sb.Append("<td border='1' bgcolor='" + colorRed + "' width='4%' align='center' valign='top' style='font-weight:bold;'>");
                    }
                    sb.Append(dsYthMon.Tables[0].Rows[i]["KpiScoreMonth4"].ToString());
                    sb.Append("</td>");




                    if (dsYthMon.Tables[0].Rows[i]["KpiScoreMonth5"] != null)
                    {
                        sb.Append(GetYtdSummaryTD(dsYthMon.Tables[0].Rows[i]["KpiScoreMonth5"].ToString(), dsYthMon.Tables[0].Rows[i]["PlanMonth"].ToString()));

                    }
                    else
                    {
                        sb.Append("<td border='1' bgcolor='" + colorRed + "' width='4%' align='center' valign='top' style='font-weight:bold;'>");
                    }
                    sb.Append(dsYthMon.Tables[0].Rows[i]["KpiScoreMonth5"].ToString());
                    sb.Append("</td>");




                    if (dsYthMon.Tables[0].Rows[i]["KpiScoreMonth6"] != null)
                    {
                        sb.Append(GetYtdSummaryTD(dsYthMon.Tables[0].Rows[i]["KpiScoreMonth6"].ToString(), dsYthMon.Tables[0].Rows[i]["PlanMonth"].ToString()));

                    }
                    else
                    {
                        sb.Append("<td border='1' bgcolor='" + colorRed + "' width='4%' align='center' valign='top' style='font-weight:bold;'>");
                    }
                    sb.Append(dsYthMon.Tables[0].Rows[i]["KpiScoreMonth6"].ToString());
                    sb.Append("</td>");




                    if (dsYthMon.Tables[0].Rows[i]["KpiScoreMonth7"] != null)
                    {
                        sb.Append(GetYtdSummaryTD(dsYthMon.Tables[0].Rows[i]["KpiScoreMonth7"].ToString(), dsYthMon.Tables[0].Rows[i]["PlanMonth"].ToString()));

                    }
                    else
                    {
                        sb.Append("<td border='1' bgcolor='" + colorRed + "' width='4%' align='center' valign='top' style='font-weight:bold;'>");
                    }
                    sb.Append(dsYthMon.Tables[0].Rows[i]["KpiScoreMonth7"].ToString());
                    sb.Append("</td>");


                    if (dsYthMon.Tables[0].Rows[i]["KpiScoreMonth8"] != null)
                    {
                        sb.Append(GetYtdSummaryTD(dsYthMon.Tables[0].Rows[i]["KpiScoreMonth8"].ToString(), dsYthMon.Tables[0].Rows[i]["PlanMonth"].ToString()));

                    }
                    else
                    {
                        sb.Append("<td border='1' bgcolor='" + colorRed + "' width='4%' align='center' valign='top' style='font-weight:bold;'>");
                    }
                    sb.Append(dsYthMon.Tables[0].Rows[i]["KpiScoreMonth8"].ToString());
                    sb.Append("</td>");




                    if (dsYthMon.Tables[0].Rows[i]["KpiScoreMonth9"] != null)
                    {
                        sb.Append(GetYtdSummaryTD(dsYthMon.Tables[0].Rows[i]["KpiScoreMonth9"].ToString(), dsYthMon.Tables[0].Rows[i]["PlanMonth"].ToString()));

                    }
                    else
                    {
                        sb.Append("<td border='1' bgcolor='" + colorRed + "' width='4%' align='center' valign='top' style='font-weight:bold;'>");
                    }
                    sb.Append(dsYthMon.Tables[0].Rows[i]["KpiScoreMonth9"].ToString());
                    sb.Append("</td>");



                    if (dsYthMon.Tables[0].Rows[i]["KpiScoreMonth10"] != null)
                    {
                        sb.Append(GetYtdSummaryTD(dsYthMon.Tables[0].Rows[i]["KpiScoreMonth10"].ToString(), dsYthMon.Tables[0].Rows[i]["PlanMonth"].ToString()));

                    }
                    else
                    {
                        sb.Append("<td border='1' bgcolor='" + colorRed + "' width='4%' align='center' valign='top' style='font-weight:bold;'>");
                    }
                    sb.Append(dsYthMon.Tables[0].Rows[i]["KpiScoreMonth10"].ToString());
                    sb.Append("</td>");



                    if (dsYthMon.Tables[0].Rows[i]["KpiScoreMonth11"] != null)
                    {
                        sb.Append(GetYtdSummaryTD(dsYthMon.Tables[0].Rows[i]["KpiScoreMonth11"].ToString(), dsYthMon.Tables[0].Rows[i]["PlanMonth"].ToString()));

                    }
                    else
                    {
                        sb.Append("<td border='1' bgcolor='" + colorRed + "' width='4%' align='center' valign='top' style='font-weight:bold;'>");
                    }
                    sb.Append(dsYthMon.Tables[0].Rows[i]["KpiScoreMonth11"].ToString());
                    sb.Append("</td>");




                    if (dsYthMon.Tables[0].Rows[i]["KpiScoreMonth12"] != null)
                    {
                        sb.Append(GetYtdSummaryTD(dsYthMon.Tables[0].Rows[i]["KpiScoreMonth12"].ToString(), dsYthMon.Tables[0].Rows[i]["PlanMonth"].ToString()));

                    }
                    else
                    {
                        sb.Append("<td border='1' bgcolor='" + colorRed + "' width='4%' align='center' valign='top' style='font-weight:bold;'>");
                    }
                    sb.Append(dsYthMon.Tables[0].Rows[i]["KpiScoreMonth12"].ToString());
                    sb.Append("</td>");
                    sb.Append("</tr>");

                }
                sb.Append("</table>");
                #endregion

                sb.Append("<br/>");
            }

        }
        if (Session["Radar_Table2-YtdQtr"] != null)
        {
            DataSet dsYthQtr = (DataSet)Session["Radar_Table2-YtdQtr"];
            if (dsYthQtr.Tables[0].Rows.Count > 0)
            {
                sb.Append(RedHeaderFormat.Replace("[Header]", "Quarterly Progress:"));

                #region Table Ytd Qtr
                sb.Append("<table width='100%' align='center' valign='top' border='1' style='font-size:7px; color:Black;'>");

                sb.Append("<tr>");
                sb.Append("<td border='1' width='26%' bgcolor='" + headerBgColor + "' align='left' valign='top' style='font-weight:bold;'>Key Performance Indicator (KPI)</td>");
                sb.Append("<td border='1' width='10%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>Qtr Plan</td>");


                sb.Append("<td border='1' width='16%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>Qtr1</td>");
                sb.Append("<td border='1' width='16%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>Qtr2</td>");
                sb.Append("<td border='1' width='16%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>Qtr3</td>");
                sb.Append("<td border='1' width='16%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>Qtr4</td>");
                sb.Append("</tr>");
                for (int i = 0; i < dsYthQtr.Tables[0].Rows.Count; i++)
                {

                    sb.Append("<tr>");
                    sb.Append("<td border='1' bgcolor='" + colorWhite + "' width='26%' align='left' valign='top'>");
                    sb.Append(dsYthQtr.Tables[0].Rows[i]["KpiMeasure"].ToString());
                    sb.Append("</td>");
                    sb.Append("<td border='1' bgcolor='" + colorWhite + "' width='10%' align='center' valign='top'>");
                    sb.Append(dsYthQtr.Tables[0].Rows[i]["PlanMonth"].ToString());
                    sb.Append("</td>");

                    DataSet dsCategory = (DataSet)Session["Radar_Table1-Summary"];
                    bool NE1=false;
                    string sCategory = dsCategory.Tables[0].Rows[0]["KpiScoreYtd"].ToString();
                    if(sCategory=="Non-Embedded 1" && i==0)
                        NE1=true;
                     
                    if (dsYthQtr.Tables[0].Rows[i]["KpiScoreMonth3"] != null)
                    {
                        
                        sb.Append(GetYtdSummaryTDForQuarterData(dsYthQtr.Tables[0].Rows[i]["KpiScoreMonth3"].ToString(), dsYthQtr.Tables[0].Rows[i]["PlanMonth"].ToString(),NE1));

                    }
                    else
                    {
                        if(NE1)
                        sb.Append("<td border='1' bgcolor='" + colorWhite + "' width='16%' align='center' valign='top' colspan='2'>");
                        else
                            sb.Append("<td border='1' bgcolor='" + colorWhite + "' width='16%' align='center' valign='top'>");
                    }

                    sb.Append(dsYthQtr.Tables[0].Rows[i]["KpiScoreMonth3"].ToString());
                    sb.Append("</td>");

                    if (!NE1)
                    {
                        if (dsYthQtr.Tables[0].Rows[i]["KpiScoreMonth6"] != null)
                        {

                            sb.Append(GetYtdSummaryTDForQuarterData(dsYthQtr.Tables[0].Rows[i]["KpiScoreMonth6"].ToString(), dsYthQtr.Tables[0].Rows[i]["PlanMonth"].ToString(), false));

                        }
                        else
                        {
                            sb.Append("<td border='1' bgcolor='" + colorWhite + "' width='16%' align='center' valign='top'>");
                        }

                        sb.Append(dsYthQtr.Tables[0].Rows[i]["KpiScoreMonth6"].ToString());
                        sb.Append("</td>");
                    }


                    if (dsYthQtr.Tables[0].Rows[i]["KpiScoreMonth9"] != null)
                    {

                        sb.Append(GetYtdSummaryTDForQuarterData(dsYthQtr.Tables[0].Rows[i]["KpiScoreMonth9"].ToString(), dsYthQtr.Tables[0].Rows[i]["PlanMonth"].ToString(),NE1));

                    }
                    else
                    {
                        if (NE1)
                            sb.Append("<td border='1' bgcolor='" + colorWhite + "' width='16%' align='center' valign='top' colspan='2'>");
                        else
                            sb.Append("<td border='1' bgcolor='" + colorWhite + "' width='16%' align='center' valign='top'>");
                    }

                    sb.Append(dsYthQtr.Tables[0].Rows[i]["KpiScoreMonth9"].ToString());
                    sb.Append("</td>");

                    if (!NE1)
                    {
                        if (dsYthQtr.Tables[0].Rows[i]["KpiScoreMonth12"] != null)
                        {

                            sb.Append(GetYtdSummaryTDForQuarterData(dsYthQtr.Tables[0].Rows[i]["KpiScoreMonth12"].ToString(), dsYthQtr.Tables[0].Rows[i]["PlanMonth"].ToString(), false));

                        }
                        else
                        {
                            sb.Append("<td border='1' bgcolor='" + colorWhite + "' width='16%' align='center' valign='top'>");
                        }

                        sb.Append(dsYthQtr.Tables[0].Rows[i]["KpiScoreMonth12"].ToString());
                        sb.Append("</td>");

                        sb.Append("</tr>");
                    }

                }
                sb.Append("</table>");
                #endregion

            }
        }


        if (Session["RawVisible"] != null)
        {
            if (Convert.ToBoolean(Session["RawVisible"]))
            {
                DataSet dsYthMon = (DataSet)Session["Radar_Table2-YtdMonth"];
                if (dsYthMon.Tables[1].Rows.Count > 0)
                {
                    sb.Append(RedHeaderFormat.Replace("[Header]", "Raw Scores:"));

                    #region Table YTD Month Raw Score
                    sb.Append("<table width='100%' align='center' valign='top' border='1' style='font-size:7px; color:Black;'>");

                    sb.Append("<tr>");
                    sb.Append("<td border='1' width='30%' bgcolor='" + headerBgColor + "' align='left' valign='top' style='font-weight:bold;'>Key Performance Indicator (KPI)</td>");
                    sb.Append("<td border='1' width='10%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>YTD</td>");


                    sb.Append("<td border='1' width='5%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>Jan</td>");
                    sb.Append("<td border='1' width='5%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>Feb</td>");
                    sb.Append("<td border='1' width='5%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>Mar</td>");
                    sb.Append("<td border='1' width='5%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>Apr</td>");
                    sb.Append("<td border='1' width='5%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>May</td>");
                    sb.Append("<td border='1' width='5%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>Jun</td>");

                    sb.Append("<td border='1' width='5%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>Jul</td>");
                    sb.Append("<td border='1' width='5%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>Aug</td>");
                    sb.Append("<td border='1' width='5%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>Sep</td>");
                    sb.Append("<td border='1' width='5%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>Oct</td>");
                    sb.Append("<td border='1' width='5%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>Nov</td>");
                    sb.Append("<td border='1' width='5%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>Dec</td>");
                    sb.Append("</tr>");

                    for (int i = 0; i < dsYthMon.Tables[1].Rows.Count; i++)
                    {

                        sb.Append("<tr>");
                        sb.Append("<td border='1' bgcolor='" + colorWhite + "' width='30%' align='left' valign='top'>");
                        sb.Append(dsYthMon.Tables[1].Rows[i]["KpiMeasure"].ToString());
                        sb.Append("</td>");
                        sb.Append("<td border='1' bgcolor='" + colorWhite + "' width='10%' align='center' valign='top'>");
                        sb.Append(dsYthMon.Tables[1].Rows[i]["PlanMonth"].ToString());
                        sb.Append("</td>");
                        if (i == 0)
                        {
                            sb.Append("<td border='1' bgcolor='" + colorWhite + "' width='5%' align='center' valign='top'>");
                            sb.Append(dsYthMon.Tables[1].Rows[i]["KpiScoreMonth1"].ToString());
                            sb.Append("</td>");

                            sb.Append("<td border='1' bgcolor='" + colorWhite + "' width='5%' align='center' valign='top'>");
                            sb.Append(dsYthMon.Tables[1].Rows[i]["KpiScoreMonth2"].ToString());
                            sb.Append("</td>");

                            sb.Append("<td border='1' bgcolor='" + colorWhite + "' width='5%' align='center' valign='top'>");
                            sb.Append(dsYthMon.Tables[1].Rows[i]["KpiScoreMonth3"].ToString());
                            sb.Append("</td>");

                            sb.Append("<td border='1' bgcolor='" + colorWhite + "' width='5%' align='center' valign='top'>");
                            sb.Append(dsYthMon.Tables[1].Rows[i]["KpiScoreMonth4"].ToString());
                            sb.Append("</td>");

                            sb.Append("<td border='1' bgcolor='" + colorWhite + "' width='5%' align='center' valign='top'>");
                            sb.Append(dsYthMon.Tables[1].Rows[i]["KpiScoreMonth5"].ToString());
                            sb.Append("</td>");

                            sb.Append("<td border='1' bgcolor='" + colorWhite + "' width='5%' align='center' valign='top'>");
                            sb.Append(dsYthMon.Tables[1].Rows[i]["KpiScoreMonth6"].ToString());
                            sb.Append("</td>");

                            sb.Append("<td border='1' bgcolor='" + colorWhite + "' width='5%' align='center' valign='top'>");
                            sb.Append(dsYthMon.Tables[1].Rows[i]["KpiScoreMonth7"].ToString());
                            sb.Append("</td>");

                            sb.Append("<td border='1' bgcolor='" + colorWhite + "' width='5%' align='center' valign='top'>");
                            sb.Append(dsYthMon.Tables[1].Rows[i]["KpiScoreMonth8"].ToString());
                            sb.Append("</td>");

                            sb.Append("<td border='1' bgcolor='" + colorWhite + "' width='5%' align='center' valign='top'>");
                            sb.Append(dsYthMon.Tables[1].Rows[i]["KpiScoreMonth9"].ToString());
                            sb.Append("</td>");

                            sb.Append("<td border='1' bgcolor='" + colorWhite + "' width='5%' align='center' valign='top'>");
                            sb.Append(dsYthMon.Tables[1].Rows[i]["KpiScoreMonth10"].ToString());
                            sb.Append("</td>");

                            sb.Append("<td border='1' bgcolor='" + colorWhite + "' width='5%' align='center' valign='top'>");
                            sb.Append(dsYthMon.Tables[1].Rows[i]["KpiScoreMonth11"].ToString());
                            sb.Append("</td>");

                            sb.Append("<td border='1' bgcolor='" + colorWhite + "' width='5%' align='center' valign='top'>");
                            sb.Append(dsYthMon.Tables[1].Rows[i]["KpiScoreMonth12"].ToString());
                            sb.Append("</td>");

                        }
                        else if (i == 1)
                        {
                            if (dsYthMon.Tables[1].Rows[1]["KpiScoreMonth1"] != null)
                            {
                                sb.Append(GetYtdSummaryTD(dsYthMon.Tables[1].Rows[1]["KpiScoreMonth1"].ToString(), dsYthMon.Tables[1].Rows[0]["KpiScoreMonth1"].ToString()));
                            }
                            else
                            {
                                sb.Append("<td border='1' bgcolor='" + colorWhite + "' width='5%' align='center' valign='top'>");
                            }
                            sb.Append(dsYthMon.Tables[1].Rows[1]["KpiScoreMonth1"].ToString());
                            sb.Append("</td>");

                            if (dsYthMon.Tables[1].Rows[1]["KpiScoreMonth2"] != null)
                            {
                                sb.Append(GetYtdSummaryTD(dsYthMon.Tables[1].Rows[1]["KpiScoreMonth2"].ToString(), dsYthMon.Tables[1].Rows[0]["KpiScoreMonth2"].ToString()));
                            }
                            else
                            {
                                sb.Append("<td border='1' bgcolor='" + colorWhite + "' width='5%' align='center' valign='top'>");
                            }
                            sb.Append(dsYthMon.Tables[1].Rows[1]["KpiScoreMonth2"].ToString());
                            sb.Append("</td>");

                            if (dsYthMon.Tables[1].Rows[1]["KpiScoreMonth3"] != null)
                            {
                                sb.Append(GetYtdSummaryTD(dsYthMon.Tables[1].Rows[1]["KpiScoreMonth3"].ToString(), dsYthMon.Tables[1].Rows[0]["KpiScoreMonth3"].ToString()));
                            }
                            else
                            {
                                sb.Append("<td border='1' bgcolor='" + colorWhite + "' width='5%' align='center' valign='top'>");
                            }
                            sb.Append(dsYthMon.Tables[1].Rows[1]["KpiScoreMonth3"].ToString());
                            sb.Append("</td>");



                            if (dsYthMon.Tables[1].Rows[1]["KpiScoreMonth4"] != null)
                            {
                                sb.Append(GetYtdSummaryTD(dsYthMon.Tables[1].Rows[1]["KpiScoreMonth4"].ToString(), dsYthMon.Tables[1].Rows[0]["KpiScoreMonth4"].ToString()));
                            }
                            else
                            {
                                sb.Append("<td border='1' bgcolor='" + colorWhite + "' width='5%' align='center' valign='top'>");
                            }
                            sb.Append(dsYthMon.Tables[1].Rows[1]["KpiScoreMonth4"].ToString());
                            sb.Append("</td>");


                            if (dsYthMon.Tables[1].Rows[1]["KpiScoreMonth5"] != null)
                            {
                                sb.Append(GetYtdSummaryTD(dsYthMon.Tables[1].Rows[1]["KpiScoreMonth5"].ToString(), dsYthMon.Tables[1].Rows[0]["KpiScoreMonth5"].ToString()));
                            }
                            else
                            {
                                sb.Append("<td border='1' bgcolor='" + colorWhite + "' width='5%' align='center' valign='top'>");
                            }
                            sb.Append(dsYthMon.Tables[1].Rows[1]["KpiScoreMonth5"].ToString());
                            sb.Append("</td>");

                            if (dsYthMon.Tables[1].Rows[1]["KpiScoreMonth6"] != null)
                            {
                                sb.Append(GetYtdSummaryTD(dsYthMon.Tables[1].Rows[1]["KpiScoreMonth6"].ToString(), dsYthMon.Tables[1].Rows[0]["KpiScoreMonth6"].ToString()));
                            }
                            else
                            {
                                sb.Append("<td border='1' bgcolor='" + colorWhite + "' width='5%' align='center' valign='top'>");
                            }
                            sb.Append(dsYthMon.Tables[1].Rows[1]["KpiScoreMonth6"].ToString());
                            sb.Append("</td>");



                            if (dsYthMon.Tables[1].Rows[1]["KpiScoreMonth7"] != null)
                            {
                                sb.Append(GetYtdSummaryTD(dsYthMon.Tables[1].Rows[1]["KpiScoreMonth7"].ToString(), dsYthMon.Tables[1].Rows[0]["KpiScoreMonth7"].ToString()));
                            }
                            else
                            {
                                sb.Append("<td border='1' bgcolor='" + colorWhite + "' width='5%' align='center' valign='top'>");
                            }
                            sb.Append(dsYthMon.Tables[1].Rows[1]["KpiScoreMonth7"].ToString());
                            sb.Append("</td>");


                            if (dsYthMon.Tables[1].Rows[1]["KpiScoreMonth8"] != null)
                            {
                                sb.Append(GetYtdSummaryTD(dsYthMon.Tables[1].Rows[1]["KpiScoreMonth8"].ToString(), dsYthMon.Tables[1].Rows[0]["KpiScoreMonth8"].ToString()));
                            }
                            else
                            {
                                sb.Append("<td border='1' bgcolor='" + colorWhite + "' width='5%' align='center' valign='top'>");
                            }
                            sb.Append(dsYthMon.Tables[1].Rows[1]["KpiScoreMonth8"].ToString());
                            sb.Append("</td>");

                            if (dsYthMon.Tables[1].Rows[1]["KpiScoreMonth9"] != null)
                            {
                                sb.Append(GetYtdSummaryTD(dsYthMon.Tables[1].Rows[1]["KpiScoreMonth9"].ToString(), dsYthMon.Tables[1].Rows[0]["KpiScoreMonth9"].ToString()));
                            }
                            else
                            {
                                sb.Append("<td border='1' bgcolor='" + colorWhite + "' width='5%' align='center' valign='top'>");
                            }
                            sb.Append(dsYthMon.Tables[1].Rows[1]["KpiScoreMonth9"].ToString());
                            sb.Append("</td>");


                            if (dsYthMon.Tables[1].Rows[1]["KpiScoreMonth10"] != null)
                            {
                                sb.Append(GetYtdSummaryTD(dsYthMon.Tables[1].Rows[1]["KpiScoreMonth10"].ToString(), dsYthMon.Tables[1].Rows[0]["KpiScoreMonth10"].ToString()));
                            }
                            else
                            {
                                sb.Append("<td border='1' bgcolor='" + colorWhite + "' width='5%' align='center' valign='top'>");
                            }
                            sb.Append(dsYthMon.Tables[1].Rows[1]["KpiScoreMonth10"].ToString());
                            sb.Append("</td>");

                            if (dsYthMon.Tables[1].Rows[1]["KpiScoreMonth11"] != null)
                            {
                                sb.Append(GetYtdSummaryTD(dsYthMon.Tables[1].Rows[1]["KpiScoreMonth11"].ToString(), dsYthMon.Tables[1].Rows[0]["KpiScoreMonth11"].ToString()));
                            }
                            else
                            {
                                sb.Append("<td border='1' bgcolor='" + colorWhite + "' width='5%' align='center' valign='top'>");
                            }
                            sb.Append(dsYthMon.Tables[1].Rows[1]["KpiScoreMonth11"].ToString());
                            sb.Append("</td>");

                            if (dsYthMon.Tables[1].Rows[1]["KpiScoreMonth12"] != null)
                            {
                                sb.Append(GetYtdSummaryTD(dsYthMon.Tables[1].Rows[1]["KpiScoreMonth12"].ToString(), dsYthMon.Tables[1].Rows[0]["KpiScoreMonth12"].ToString()));
                            }
                            else
                            {
                                sb.Append("<td border='1' bgcolor='" + colorWhite + "' width='5%' align='center' valign='top'>");
                            }
                            sb.Append(dsYthMon.Tables[1].Rows[1]["KpiScoreMonth12"].ToString());
                            sb.Append("</td>");
                        }


                        sb.Append("</tr>");

                    }
                    sb.Append("</table>");
                    #endregion

                    sb.Append("<br/>");
                }
            }
        }


        sb.Append("</td>");
        sb.Append("</tr>");
        sb.Append("</table>");

        return sb.ToString();

    }


    protected string GetImage(int colorValue)
    {

        string imgURLPath = string.Empty;
        string[] arr = Request.Url.ToString().Split('/');
        for (int i = 0; i < arr.Length - 1; i++)
        {
            if (i > 0) imgURLPath = imgURLPath + "/";
            imgURLPath = imgURLPath + arr[i];
        }

        string imgPath = string.Empty;
       // imgPath = WebConfigurationManager.AppSettings["PDFImagetlGray"].ToString();
        imgPath = imgURLPath + "/" + @"Images/TrafficLights/tlGray.gif";

        if (colorValue != null)
        {
            if (colorValue == Convert.ToInt32(TrafficLightColor.Red))
            {
                //imgPath = WebConfigurationManager.AppSettings["PDFImagetlRed"].ToString();
                //imgPath = imgURLPath + "/" + @"Images/TrafficLights/tlRed.gif";
                imgPath = imgURLPath + "/" + @"Images/TrafficLights/Red.png";
            }
            else if (colorValue == Convert.ToInt32(TrafficLightColor.Yellow))
            {
                //imgPath = WebConfigurationManager.AppSettings["PDFImagetlYellow"].ToString();
               // imgPath = imgURLPath + "/" + @"Images/TrafficLights/tlYellow.gif";
                imgPath = imgURLPath + "/" + @"Images/TrafficLights/Yellow.png";
            }
            else if (colorValue == Convert.ToInt32(TrafficLightColor.Green))
            {
                //imgPath = WebConfigurationManager.AppSettings["PDFImagetlGreen"].ToString();
                //imgPath = imgURLPath + "/" + @"Images/TrafficLights/tlGreen.gif";
                imgPath = imgURLPath + "/" + @"Images/TrafficLights/Green.png";
            }


        }
        return "<img width='7px' height='7px' src='" + imgPath + "' alt='Image1'/>";
    }

    protected string GetYtdSummaryTD(string KpiScoreMonth, string PlanYtd)
    {
        string colorGreen = "Lime";
        string colorWhite = "White";
        string colorRed = "Red";
        string sb = string.Empty;
        sb = "<td border='1' bgcolor='" + colorWhite + "' width='6%' align='center' valign='top'>";
        if (KpiScoreMonth != "")
        {
            if (KpiScoreMonth == "-")
            {
                sb = "<td border='1' bgcolor='" + colorWhite + "' width='6%' align='center' valign='top'>";
            }
            else if (KpiScoreMonth == "Yes")
            {
                sb = "<td border='1' bgcolor='" + colorWhite + "' width='6%' align='center' valign='top'>";
            }
            else if (KpiScoreMonth == "No")
            {
                sb = "<td border='1' bgcolor='" + colorWhite + "' width='6%' align='center' valign='top'>";
            }
            else if (PlanYtd != "" && PlanYtd != "-" && PlanYtd != null)
            {
                 if (Convert.ToDecimal(KpiScoreMonth) >= Convert.ToDecimal(PlanYtd))
                {
                    sb = "<td border='1' bgcolor='" + colorGreen + "' width='6%' align='center' valign='top'>";
                }
                else if (Convert.ToDecimal(KpiScoreMonth) < Convert.ToDecimal(PlanYtd))
                {
                    sb = "<td border='1' bgcolor='" + colorRed + "' width='6%' align='center' valign='top'>";
                }
            }
            else
            {
                sb = "<td border='1' bgcolor='" + colorWhite + "' width='6%' align='center' valign='top'>";
            }
        }
        else
        {
            sb = "<td border='1' bgcolor='" + colorRed + "' width='6%' align='center' valign='top'>";
        }

        return sb;
    }

    protected string GetYtdSummaryTDForQuarterData(string KpiScoreMonth, string PlanYtd,bool NE1)
    {
        string colorGreen = "Lime";
        string colorWhite = "White";
        string colorRed = "Red";
        string sb = string.Empty;
        if(NE1)
        sb = "<td border='1' bgcolor='" + colorWhite + "' width='16%' align='center' valign='top' colspan='2'>";
        else
            sb = "<td border='1' bgcolor='" + colorWhite + "' width='16%' align='center' valign='top'>";

        if (KpiScoreMonth != "")
        {
            if (KpiScoreMonth == "-")
            {
                if(NE1)
                sb = "<td border='1' bgcolor='" + colorWhite + "' width='16%' align='center' valign='top' colspan='2'>";
                else
                    sb = "<td border='1' bgcolor='" + colorWhite + "' width='16%' align='center' valign='top'>";
            }
            else if (KpiScoreMonth == "Yes")
            {
                if(NE1)
                sb = "<td border='1' bgcolor='" + colorWhite + "' width='16%' align='center' valign='top' colspan='2'>";
                else
                    sb = "<td border='1' bgcolor='" + colorWhite + "' width='16%' align='center' valign='top'>";
            }
            else if (KpiScoreMonth == "No")
            {
                if (NE1)
                    sb = "<td border='1' bgcolor='" + colorWhite + "' width='16%' align='center' valign='top' colspan='2'>";
                else
                    sb = "<td border='1' bgcolor='" + colorWhite + "' width='16%' align='center' valign='top'>";
            }
            else if (Convert.ToDecimal(KpiScoreMonth) >= Convert.ToDecimal(PlanYtd))
            {
                if(NE1)
                sb = "<td border='1' bgcolor='" + colorGreen + "' width='16%' align='center' valign='top' colspan='2'>";
                else
                    sb = "<td border='1' bgcolor='" + colorGreen + "' width='16%' align='center' valign='top'>";

            }
            else if (Convert.ToDecimal(KpiScoreMonth) < Convert.ToDecimal(PlanYtd))
            {
                if(NE1)
                sb = "<td border='1' bgcolor='" + colorRed + "' width='16%' align='center' valign='top' colspan='2'>";
                else
                    sb = "<td border='1' bgcolor='" + colorRed + "' width='16%' align='center' valign='top'>";

            }
            else
            {
                if(NE1)
                sb = "<td border='1' bgcolor='" + colorWhite + "' width='16%' align='center' valign='top' colspan='2'>";
                else
                    sb = "<td border='1' bgcolor='" + colorWhite + "' width='16%' align='center' valign='top'>";
            }
        }
        else
        {
            if(NE1)
            sb = "<td border='1' bgcolor='" + colorRed + "' width='16' align='center' valign='top' colspan='2'>";
            else
                sb = "<td border='1' bgcolor='" + colorRed + "' width='16' align='center' valign='top'>";
        }

        return sb;
    }


    protected string CreateCatSummaryBySite(DataTable dtYtdSummary, string embType, string siteName, string header)
    {
        string headerFormat = "<h4 style='color:#110c84'>[Header]</h4><br/>";
        string headerBgColor = "#D1DBF4";
        StringBuilder sbYtdSummary = new StringBuilder();

        string KpiScoreYtd = "KpiScoreYtd" + siteName;

        sbYtdSummary.Append(headerFormat.Replace("[Header]", header));

        sbYtdSummary.Append("<table width='100%' align='center' valign='top' border='1' style='font-size:7px; color:Black;'>");
        sbYtdSummary.Append("<tr>");
        //DT 3133 re-changes
        sbYtdSummary.Append("<td border='1' width='50%' bgcolor='" + headerBgColor + "' align='left' valign='top' style='font-weight:bold;'>Safety Rates</td>");

        sbYtdSummary.Append("<td border='1' width='50%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>" + siteName + "</td>");

        sbYtdSummary.Append("</tr>");

        if (dtYtdSummary != null)
        {
            for (int i = 0; i < dtYtdSummary.Rows.Count; i++)
            {
                sbYtdSummary.Append("<tr>");

                sbYtdSummary.Append("<td width='50%'  border='1' valign='top' align='left'>");
                if (dtYtdSummary.Rows[i]["KpiMeasure"] != null)
                {
                    sbYtdSummary.Append(dtYtdSummary.Rows[i]["KpiMeasure"].ToString());
                }
                sbYtdSummary.Append("</td>");

                sbYtdSummary.Append("<td width='50%'  border='1' valign='top' align='center'>");
                if (dtYtdSummary.Rows[i]["KpiMeasure"] != null)
                {
                    sbYtdSummary.Append(dtYtdSummary.Rows[i][KpiScoreYtd].ToString());
                }
                sbYtdSummary.Append("</td>");
                sbYtdSummary.Append("</tr>");
            }
        }
        sbYtdSummary.Append("</table>");
        return sbYtdSummary.ToString();

    }



    protected string CreateCatYtdBySite(DataTable dtYtdSummary, string embType, string siteName, string header)
    {
        string headerFormat = "<h5 style='color:#110c84'>[Header]</h5><br/>";
        string headerBgColor = "#D1DBF4";
        StringBuilder sbYtdSummary = new StringBuilder();
        bool isColored = false;

        string KpiScoreYtd = "KpiScoreYtd" + siteName;
        string CompaniesNotCompliant = "CompaniesNotCompliant" + siteName;

        sbYtdSummary.Append(headerFormat.Replace("[Header]", header));

        sbYtdSummary.Append("<table width='100%' align='center' valign='top' border='1' style='font-size:7px; color:Black;'>");
        sbYtdSummary.Append("<tr>");


        sbYtdSummary.Append("<td border='1' width='30%' bgcolor='" + headerBgColor + "' align='left' valign='top' style='font-weight:bold;'>Key Performance Indicators(KPI)</td>");
        //DT 3133 re-changes
        sbYtdSummary.Append("<td border='1' width='08%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>" + siteName + "</td>");
        sbYtdSummary.Append("<td border='1' width='62%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>Companies Not Compliant</td>");

        sbYtdSummary.Append("</tr>");

        if (dtYtdSummary != null)
        {
            for (int i = 0; i < dtYtdSummary.Rows.Count; i++)
            {
                sbYtdSummary.Append("<tr>");

                sbYtdSummary.Append("<td width='30%'  border='1' valign='top' align='left'>");
                if (dtYtdSummary.Rows[i]["KpiMeasure"] != null)
                {
                    sbYtdSummary.Append(dtYtdSummary.Rows[i]["KpiMeasure"].ToString());
                }
                sbYtdSummary.Append("</td>");


                if (dtYtdSummary.Rows[i]["KpiMeasure"].ToString() != "Number of Companies:"
                       && dtYtdSummary.Rows[i]["KpiMeasure"].ToString() != "Number of Companies On Site:"
                       && dtYtdSummary.Rows[i]["KpiMeasure"].ToString() != "CSA - Self Audit (Conducted)")
                {
                    isColored = true;
                }
                else
                {
                    isColored = false;
                }

                if (dtYtdSummary.Rows[i][KpiScoreYtd] != null)
                {
                    if (!isColored)
                    {
                        sbYtdSummary.Append("<td width='08%'  border='1' valign='top' align='center'>");
                    }
                    else
                    {
                        sbYtdSummary.Append("<td width='08%' bgcolor='" + getBGColor(dtYtdSummary.Rows[i][KpiScoreYtd].ToString()) +
                                            "'  border='1' valign='top' align='center'>");
                    }
                    sbYtdSummary.Append(dtYtdSummary.Rows[i][KpiScoreYtd].ToString());
                    sbYtdSummary.Append("</td>");
                }
                else
                {
                    sbYtdSummary.Append("<td width='08%'  border='1' valign='top' align='center'>");

                    sbYtdSummary.Append("</td>");
                }

                sbYtdSummary.Append("<td width='62%'  border='1' valign='top' align='left'>");
                if (dtYtdSummary.Rows[i][CompaniesNotCompliant] != null)
                {
                    sbYtdSummary.Append(dtYtdSummary.Rows[i][CompaniesNotCompliant].ToString());
                }
                sbYtdSummary.Append("</td>");

                sbYtdSummary.Append("</tr>");
            }
        }
        sbYtdSummary.Append("</table>");

        return sbYtdSummary.ToString();

    }


    //This function to create summary table for PDF

    protected string CreateSummaryTable(DataTable dtYtdSummary, string header)
    {
        string headerMainFormat = "<h4 style='color:#110c84'>[Header]</h4><br/>";
        string headerFormat = "<h5 style='color:#110c84'>[Header]</h5><br/>";
        string headerBgColor = "#D1DBF4";
        StringBuilder sbYtdSummary = new StringBuilder();


        ////Adding by Pankaj Dikshit for <DT 2823, 19-Feb-2013> - Start
        SitesService sService = new SitesService();
        int yearId = Convert.ToInt32(SessionHandler.spVar_Year);
        int monthId = Convert.ToInt32(SessionHandler.spVar_MonthId);
        DataSet dsGridCols = sService.GetByYearAdHocRadarId(yearId, '%');
        DataRow[] drTmp = dsGridCols.Tables[0].Select("SiteAbbrev = '-AM'");
        ////Adding by Pankaj Dikshit for <DT 2823, 19-Feb-2013> - End


        sbYtdSummary.Append(headerMainFormat.Replace("[Header]", header));

        sbYtdSummary.Append(headerFormat.Replace("[Header]", "Summary"));


        sbYtdSummary.Append("<table width='100%' align='center' valign='top' border='1' style='font-size:7px; color:Black;'>");
        sbYtdSummary.Append("<tr>");


        sbYtdSummary.Append("<td border='1' width='18%' bgcolor='" + headerBgColor + "' align='left' valign='top' style='font-weight:bold;'>Safety Rates</td>");

        sbYtdSummary.Append("<td border='1' width='5%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>WAO</td>");
        sbYtdSummary.Append("<td border='1' width='5%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>KWI</td>");
        sbYtdSummary.Append("<td border='1' width='5%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>PIN</td>");
        sbYtdSummary.Append("<td border='1' width='5%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>WGP</td>");
        if (drTmp.Length > 0)
        {
            sbYtdSummary.Append("<td border='1' width='5%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>WAM</td>");
        }
        sbYtdSummary.Append("<td border='1' width='5%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>HUN</td>");
        sbYtdSummary.Append("<td border='1' width='5%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>WDL</td>");
        sbYtdSummary.Append("<td border='1' width='7%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>FML</td>");

        sbYtdSummary.Append("<td border='1' width='5%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>BUN</td>");
        sbYtdSummary.Append("<td border='1' width='5%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>BGN</td>");
        sbYtdSummary.Append("<td border='1' width='5%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>CE</td>");
        sbYtdSummary.Append("<td border='1' width='5%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>PNJ</td>");

        sbYtdSummary.Append("<td border='1' width='5%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>VIC</td>");
        sbYtdSummary.Append("<td border='1' width='5%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>ANG</td>");
        sbYtdSummary.Append("<td border='1' width='5%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>PTH</td>");
        sbYtdSummary.Append("<td border='1' width='5%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>PTD</td>");


        sbYtdSummary.Append("</tr>");

        if (dtYtdSummary != null)
        {
            for (int i = 0; i < dtYtdSummary.Rows.Count; i++)
            {
                sbYtdSummary.Append("<tr>");

                sbYtdSummary.Append("<td width='18%'  border='1' valign='top' align='left'>");
                if (dtYtdSummary.Rows[i]["KpiMeasure"] != null)
                {
                    sbYtdSummary.Append(dtYtdSummary.Rows[i]["KpiMeasure"].ToString());
                }
                sbYtdSummary.Append("</td>");

                sbYtdSummary.Append("<td width='5%'  border='1' valign='top' align='center'>");
                if (dtYtdSummary.Rows[i]["KpiScoreYtd-WAO"] != null)
                {
                    sbYtdSummary.Append(dtYtdSummary.Rows[i]["KpiScoreYtd-WAO"].ToString());
                }
                sbYtdSummary.Append("</td>");

                sbYtdSummary.Append("<td width='5%'  border='1' valign='top' align='center'>");
                if (dtYtdSummary.Rows[i]["KpiScoreYtdKWI"] != null)
                {
                    sbYtdSummary.Append(dtYtdSummary.Rows[i]["KpiScoreYtdKWI"].ToString());
                }
                sbYtdSummary.Append("</td>");

                sbYtdSummary.Append("<td width='5%'  border='1' valign='top' align='center'>");
                if (dtYtdSummary.Rows[i]["KpiScoreYtdPIN"] != null)
                {
                    sbYtdSummary.Append(dtYtdSummary.Rows[i]["KpiScoreYtdPIN"].ToString());
                }
                sbYtdSummary.Append("</td>");

                sbYtdSummary.Append("<td width='5%'  border='1' valign='top' align='center'>");
                if (dtYtdSummary.Rows[i]["KpiScoreYtdWGP"] != null)
                {
                    sbYtdSummary.Append(dtYtdSummary.Rows[i]["KpiScoreYtdWGP"].ToString());
                }
                sbYtdSummary.Append("</td>");

                if (drTmp.Length > 0)
                {
                    sbYtdSummary.Append("<td width='5%'  border='1' valign='top' align='center'>");
                    if (dtYtdSummary.Rows[i]["KpiScoreYtd-WAM"] != null)
                    {
                        sbYtdSummary.Append(dtYtdSummary.Rows[i]["KpiScoreYtd-WAM"].ToString());
                    }
                    sbYtdSummary.Append("</td>");
                }

                sbYtdSummary.Append("<td width='5%'  border='1' valign='top' align='center'>");
                if (dtYtdSummary.Rows[i]["KpiScoreYtdHUN"] != null)
                {
                    sbYtdSummary.Append(dtYtdSummary.Rows[i]["KpiScoreYtdHUN"].ToString());
                }
                sbYtdSummary.Append("</td>");

                sbYtdSummary.Append("<td width='5%'  border='1' valign='top' align='center'>");
                if (dtYtdSummary.Rows[i]["KpiScoreYtdWDL"] != null)
                {
                    sbYtdSummary.Append(dtYtdSummary.Rows[i]["KpiScoreYtdWDL"].ToString());
                }
                sbYtdSummary.Append("</td>");

                sbYtdSummary.Append("<td width='7%'  border='1' valign='top' align='center'>");
                if (dtYtdSummary.Rows[i]["KpiScoreYtdFML"] != null)
                {
                    sbYtdSummary.Append(dtYtdSummary.Rows[i]["KpiScoreYtdFML"].ToString());
                }
                sbYtdSummary.Append("</td>");

                sbYtdSummary.Append("<td width='5%'  border='1' valign='top' align='center'>");
                if (dtYtdSummary.Rows[i]["KpiScoreYtdBUN"] != null)
                {
                    sbYtdSummary.Append(dtYtdSummary.Rows[i]["KpiScoreYtdBUN"].ToString());
                }
                sbYtdSummary.Append("</td>");

                sbYtdSummary.Append("<td width='5%'  border='1' valign='top' align='center'>");
                if (dtYtdSummary.Rows[i]["KpiScoreYtdBGN"] != null)
                {
                    sbYtdSummary.Append(dtYtdSummary.Rows[i]["KpiScoreYtdBGN"].ToString());
                }
                sbYtdSummary.Append("</td>");


                sbYtdSummary.Append("<td width='5%'  border='1' valign='top' align='center'>");
                if (dtYtdSummary.Rows[i]["KpiScoreYtdCE"] != null)
                {
                    sbYtdSummary.Append(dtYtdSummary.Rows[i]["KpiScoreYtdCE"].ToString());
                }
                sbYtdSummary.Append("</td>");

                sbYtdSummary.Append("<td width='5%'  border='1' valign='top' align='center'>");
                if (dtYtdSummary.Rows[i]["KpiScoreYtdPNJ"] != null)
                {
                    sbYtdSummary.Append(dtYtdSummary.Rows[i]["KpiScoreYtdPNJ"].ToString());
                }
                sbYtdSummary.Append("</td>");

                sbYtdSummary.Append("<td width='5%'  border='1' valign='top' align='center'>");
                if (dtYtdSummary.Rows[i]["KpiScoreYtd-VIC"] != null)
                {
                    sbYtdSummary.Append(dtYtdSummary.Rows[i]["KpiScoreYtd-VIC"].ToString());
                }
                sbYtdSummary.Append("</td>");

                sbYtdSummary.Append("<td width='5%'  border='1' valign='top' align='center'>");
                if (dtYtdSummary.Rows[i]["KpiScoreYtdANG"] != null)
                {
                    sbYtdSummary.Append(dtYtdSummary.Rows[i]["KpiScoreYtdANG"].ToString());
                }
                sbYtdSummary.Append("</td>");

                sbYtdSummary.Append("<td width='5%'  border='1' valign='top' align='center'>");
                if (dtYtdSummary.Rows[i]["KpiScoreYtdPTH"] != null)
                {
                    sbYtdSummary.Append(dtYtdSummary.Rows[i]["KpiScoreYtdPTH"].ToString());
                }
                sbYtdSummary.Append("</td>");

                sbYtdSummary.Append("<td width='5%'  border='1' valign='top' align='center'>");
                if (dtYtdSummary.Rows[i]["KpiScoreYtdPTD"] != null)
                {
                    sbYtdSummary.Append(dtYtdSummary.Rows[i]["KpiScoreYtdPTD"].ToString());
                }
                sbYtdSummary.Append("</td>");

                sbYtdSummary.Append("</tr>");

            }
        }
        else { }
        sbYtdSummary.Append("</table>");
        sbYtdSummary.Append("<br/>");

        return sbYtdSummary.ToString();
    }



    //Creates Summary for E, NE1, NE2, NE3

    protected string CreateCatSummaryTable(DataTable dtYtdSummary, string embType, string header)
    {
        DataSet dsRadarTbl = (DataSet)Session["Radar_Table2-Ytd"];
        string headerFormat = "<h5 style='color:#110c84'>[Header]</h5><br/>";
        string headerBgColor = "#D1DBF4";
        StringBuilder sbYtdSummary = new StringBuilder();

        ////Adding by Pankaj Dikshit for <DT 2823, 19-Feb-2013> - Start
        SitesService sService = new SitesService();
        int yearId = Convert.ToInt32(SessionHandler.spVar_Year);
        int monthId = Convert.ToInt32(SessionHandler.spVar_MonthId);
        DataSet dsGridCols = sService.GetByYearAdHocRadarId(yearId, '%');
        DataRow[] drTmp = dsGridCols.Tables[0].Select("SiteAbbrev = '-AM'");
        ////Adding by Pankaj Dikshit for <DT 2823, 19-Feb-2013> - End

        sbYtdSummary.Append(headerFormat.Replace("[Header]", header));

        sbYtdSummary.Append("<table width='100%' align='center' valign='top' border='1' style='font-size:7px; color:Black;'>");
        sbYtdSummary.Append("<tr>");

        sbYtdSummary.Append("<td border='1' width='18%' bgcolor='" + headerBgColor + "' align='left' valign='top' style='font-weight:bold;'>Safety Rates</td>");

        sbYtdSummary.Append("<td border='1' width='5%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>WAO</td>");
        sbYtdSummary.Append("<td border='1' width='5%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>KWI</td>");
        sbYtdSummary.Append("<td border='1' width='5%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>PIN</td>");
        sbYtdSummary.Append("<td border='1' width='5%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>WGP</td>");
        if (drTmp.Length > 0)
        {
            sbYtdSummary.Append("<td border='1' width='5%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>WAM</td>");
        }
        sbYtdSummary.Append("<td border='1' width='5%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>HUN</td>");
        sbYtdSummary.Append("<td border='1' width='5%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>WDL</td>");

        if (embType == "emb1" || embType == "nonemb1")
        {
            sbYtdSummary.Append("<td border='1' width='5%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>BUN</td>");
            sbYtdSummary.Append("<td border='1' width='5%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>VIC</td>");
        }
        if (embType == "emb1")
        {
            sbYtdSummary.Append("<td border='1' width='5%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>PTH</td>");

        }
        else if (embType == "nonemb1"  && Convert.ToInt32(dsRadarTbl.Tables[1].Rows[0]["KpiScoreYtdANG"])>0)
        {
            sbYtdSummary.Append("<td border='1' width='5%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>ANG</td>");
        }


        if (embType == "nonemb2" || embType == "nonemb3")
        {
            sbYtdSummary.Append("<td border='1' width='7%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>FML</td>");
            sbYtdSummary.Append("<td border='1' width='5%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>BUN</td>");
        }

        if (embType == "nonemb2")
        {
            sbYtdSummary.Append("<td border='1' width='5%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>PNJ</td>");
            sbYtdSummary.Append("<td border='1' width='5%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>VIC</td>");
            sbYtdSummary.Append("<td border='1' width='5%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>ANG</td>");
            sbYtdSummary.Append("<td border='1' width='5%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>PTH</td>");
        }
        if (embType == "nonemb3")
        {
            sbYtdSummary.Append("<td border='1' width='5%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>BGN</td>");
            sbYtdSummary.Append("<td border='1' width='5%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>CE</td>");
            sbYtdSummary.Append("<td border='1' width='5%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>PNJ</td>");
            sbYtdSummary.Append("<td border='1' width='5%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>VIC</td>");

            sbYtdSummary.Append("<td border='1' width='5%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>ANG</td>");
            sbYtdSummary.Append("<td border='1' width='5%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>PTH</td>");
        }

        sbYtdSummary.Append("<td border='1' width='5%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>PTD</td>");


        sbYtdSummary.Append("</tr>");

        if (dtYtdSummary != null)
        {
            

            for (int i = 0; i < dtYtdSummary.Rows.Count; i++)
            {
                sbYtdSummary.Append("<tr>");

                sbYtdSummary.Append("<td width='18%'  border='1' valign='top' align='left'>");
                if (dtYtdSummary.Rows[i]["KpiMeasure"] != null)
                {
                    sbYtdSummary.Append(dtYtdSummary.Rows[i]["KpiMeasure"].ToString());
                }
                sbYtdSummary.Append("</td>");

                sbYtdSummary.Append("<td width='5%'  border='1' valign='top' align='center'>");
                if (dtYtdSummary.Rows[i]["KpiScoreYtd-WAO"] != null)
                {
                    sbYtdSummary.Append(dtYtdSummary.Rows[i]["KpiScoreYtd-WAO"].ToString());
                }
                sbYtdSummary.Append("</td>");

                sbYtdSummary.Append("<td width='5%'  border='1' valign='top' align='center'>");
                if (dtYtdSummary.Rows[i]["KpiScoreYtdKWI"] != null)
                {
                    sbYtdSummary.Append(dtYtdSummary.Rows[i]["KpiScoreYtdKWI"].ToString());
                }
                sbYtdSummary.Append("</td>");

                sbYtdSummary.Append("<td width='5%'  border='1' valign='top' align='center'>");
                if (dtYtdSummary.Rows[i]["KpiScoreYtdPIN"] != null)
                {
                    sbYtdSummary.Append(dtYtdSummary.Rows[i]["KpiScoreYtdPIN"].ToString());
                }
                sbYtdSummary.Append("</td>");

                sbYtdSummary.Append("<td width='5%'  border='1' valign='top' align='center'>");
                if (dtYtdSummary.Rows[i]["KpiScoreYtdWGP"] != null)
                {
                    sbYtdSummary.Append(dtYtdSummary.Rows[i]["KpiScoreYtdWGP"].ToString());
                }
                sbYtdSummary.Append("</td>");

                if (drTmp.Length > 0)
                {
                    sbYtdSummary.Append("<td width='5%'  border='1' valign='top' align='center'>");
                    if (dtYtdSummary.Rows[i]["KpiScoreYtd-AM"] != null)
                    {
                        sbYtdSummary.Append(dtYtdSummary.Rows[i]["KpiScoreYtd-AM"].ToString());
                    }
                    sbYtdSummary.Append("</td>");
                }

                sbYtdSummary.Append("<td width='5%'  border='1' valign='top' align='center'>");
                if (dtYtdSummary.Rows[i]["KpiScoreYtdHUN"] != null)
                {
                    sbYtdSummary.Append(dtYtdSummary.Rows[i]["KpiScoreYtdHUN"].ToString());
                }
                sbYtdSummary.Append("</td>");

                sbYtdSummary.Append("<td width='5%'  border='1' valign='top' align='center'>");
                if (dtYtdSummary.Rows[i]["KpiScoreYtdWDL"] != null)
                {
                    sbYtdSummary.Append(dtYtdSummary.Rows[i]["KpiScoreYtdWDL"].ToString());
                }
                sbYtdSummary.Append("</td>");

                //////////////////////////////////////////////


                if (embType == "emb1" || embType == "nonemb1")
                {

                    sbYtdSummary.Append("<td width='5%'  border='1' valign='top' align='center'>");
                    if (dtYtdSummary.Rows[i]["KpiScoreYtdBUN"] != null)
                    {
                        sbYtdSummary.Append(dtYtdSummary.Rows[i]["KpiScoreYtdBUN"].ToString());
                    }
                    sbYtdSummary.Append("</td>");

                    sbYtdSummary.Append("<td width='5%'  border='1' valign='top' align='center'>");
                    if (dtYtdSummary.Rows[i]["KpiScoreYtd-VIC"] != null)
                    {
                        sbYtdSummary.Append(dtYtdSummary.Rows[i]["KpiScoreYtd-VIC"].ToString());
                    }
                    sbYtdSummary.Append("</td>");

                }


                if (embType == "emb1")
                {
                    //sbYtdSummary.Append("<td border='1' width='5%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>PTH</td>");
                    sbYtdSummary.Append("<td width='5%'  border='1' valign='top' align='center'>");
                    if (dtYtdSummary.Rows[i]["KpiScoreYtdPTH"] != null)
                    {
                        sbYtdSummary.Append(dtYtdSummary.Rows[i]["KpiScoreYtdPTH"].ToString());
                    }
                    sbYtdSummary.Append("</td>");

                }
                else if (embType == "nonemb1" && Convert.ToInt32(dsRadarTbl.Tables[1].Rows[0]["KpiScoreYtdANG"])>0)
                {
                    //sbYtdSummary.Append("<td border='1' width='5%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>ANG</td>");
                    sbYtdSummary.Append("<td width='5%'  border='1' valign='top' align='center'>");
                    if (dtYtdSummary.Rows[i]["KpiScoreYtdANG"] != null)
                    {
                        sbYtdSummary.Append(dtYtdSummary.Rows[i]["KpiScoreYtdANG"].ToString());
                    }
                    sbYtdSummary.Append("</td>");
                }


                if (embType == "nonemb2" || embType == "nonemb3")
                {
                    //sbYtdSummary.Append("<td border='1' width='5%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>FML</td>");
                    //sbYtdSummary.Append("<td border='1' width='5%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>BUN</td>");

                    sbYtdSummary.Append("<td width='7%'  border='1' valign='top' align='center'>");
                    if (dtYtdSummary.Rows[i]["KpiScoreYtdFML"] != null)
                    {
                        sbYtdSummary.Append(dtYtdSummary.Rows[i]["KpiScoreYtdFML"].ToString());
                    }
                    sbYtdSummary.Append("</td>");

                    sbYtdSummary.Append("<td width='5%'  border='1' valign='top' align='center'>");
                    if (dtYtdSummary.Rows[i]["KpiScoreYtdBUN"] != null)
                    {
                        sbYtdSummary.Append(dtYtdSummary.Rows[i]["KpiScoreYtdBUN"].ToString());
                    }
                    sbYtdSummary.Append("</td>");
                }


                if (embType == "nonemb2")
                {
                    //sbYtdSummary.Append("<td border='1' width='5%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>PNJ</td>");
                    //sbYtdSummary.Append("<td border='1' width='5%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>VIC</td>");
                    //sbYtdSummary.Append("<td border='1' width='5%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>ANG</td>");
                    //sbYtdSummary.Append("<td border='1' width='5%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>PTH</td>");



                    sbYtdSummary.Append("<td width='5%'  border='1' valign='top' align='center'>");
                    if (dtYtdSummary.Rows[i]["KpiScoreYtdPNJ"] != null)
                    {
                        sbYtdSummary.Append(dtYtdSummary.Rows[i]["KpiScoreYtdPNJ"].ToString());
                    }
                    sbYtdSummary.Append("</td>");

                    sbYtdSummary.Append("<td width='5%'  border='1' valign='top' align='left'>");
                    if (dtYtdSummary.Rows[i]["KpiScoreYtd-VIC"] != null)
                    {
                        sbYtdSummary.Append(dtYtdSummary.Rows[i]["KpiScoreYtd-VIC"].ToString());
                    }
                    sbYtdSummary.Append("</td>");

                    sbYtdSummary.Append("<td width='5%'  border='1' valign='top' align='center'>");
                    if (dtYtdSummary.Rows[i]["KpiScoreYtdANG"] != null)
                    {
                        sbYtdSummary.Append(dtYtdSummary.Rows[i]["KpiScoreYtdANG"].ToString());
                    }
                    sbYtdSummary.Append("</td>");


                    sbYtdSummary.Append("<td width='5%'  border='1' valign='top' align='center'>");
                    if (dtYtdSummary.Rows[i]["KpiScoreYtdPTH"] != null)
                    {
                        sbYtdSummary.Append(dtYtdSummary.Rows[i]["KpiScoreYtdPTH"].ToString());
                    }
                    sbYtdSummary.Append("</td>");
                }
                if (embType == "nonemb3")
                {
                    //sbYtdSummary.Append("<td border='1' width='5%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>BGN</td>");
                    //sbYtdSummary.Append("<td border='1' width='5%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>CE</td>");
                    //sbYtdSummary.Append("<td border='1' width='5%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>PNJ</td>");
                    //sbYtdSummary.Append("<td border='1' width='5%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>VIC</td>");

                    //sbYtdSummary.Append("<td border='1' width='5%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>ANG</td>");
                    //sbYtdSummary.Append("<td border='1' width='5%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>PTH</td>");




                    sbYtdSummary.Append("<td width='5%'  border='1' valign='top' align='center'>");
                    if (dtYtdSummary.Rows[i]["KpiScoreYtdBGN"] != null)
                    {
                        sbYtdSummary.Append(dtYtdSummary.Rows[i]["KpiScoreYtdBGN"].ToString());
                    }
                    sbYtdSummary.Append("</td>");

                    sbYtdSummary.Append("<td width='5%'  border='1' valign='top' align='center'>");
                    if (dtYtdSummary.Rows[i]["KpiScoreYtdCE"] != null)
                    {
                        sbYtdSummary.Append(dtYtdSummary.Rows[i]["KpiScoreYtdCE"].ToString());
                    }
                    sbYtdSummary.Append("</td>");

                    sbYtdSummary.Append("<td width='5%'  border='1' valign='top' align='center'>");
                    if (dtYtdSummary.Rows[i]["KpiScoreYtdPNJ"] != null)
                    {
                        sbYtdSummary.Append(dtYtdSummary.Rows[i]["KpiScoreYtdPNJ"].ToString());
                    }
                    sbYtdSummary.Append("</td>");

                    sbYtdSummary.Append("<td width='5%'  border='1' valign='top' align='center'>");
                    if (dtYtdSummary.Rows[i]["KpiScoreYtd-VIC"] != null)
                    {
                        sbYtdSummary.Append(dtYtdSummary.Rows[i]["KpiScoreYtd-VIC"].ToString());
                    }
                    sbYtdSummary.Append("</td>");

                    sbYtdSummary.Append("<td width='5%'  border='1' valign='top' align='center'>");
                    if (dtYtdSummary.Rows[i]["KpiScoreYtdANG"] != null)
                    {
                        sbYtdSummary.Append(dtYtdSummary.Rows[i]["KpiScoreYtdANG"].ToString());
                    }
                    sbYtdSummary.Append("</td>");

                    sbYtdSummary.Append("<td width='5%'  border='1' valign='top' align='center'>");
                    if (dtYtdSummary.Rows[i]["KpiScoreYtdPTH"] != null)
                    {
                        sbYtdSummary.Append(dtYtdSummary.Rows[i]["KpiScoreYtdPTH"].ToString());
                    }
                    sbYtdSummary.Append("</td>");

                }


                sbYtdSummary.Append("<td width='5%'  border='1' valign='top' align='center'>");
                if (dtYtdSummary.Rows[i]["KpiScoreYtdPTD"] != null)
                {
                    sbYtdSummary.Append(dtYtdSummary.Rows[i]["KpiScoreYtdPTD"].ToString());
                }
                sbYtdSummary.Append("</td>");

                sbYtdSummary.Append("</tr>");

            }
        }
        else { }
        sbYtdSummary.Append("</table>");
        sbYtdSummary.Append("<br/>");

        return sbYtdSummary.ToString();
    }


    protected string CreateCatYtdTable(DataTable dtYtdSummary, string embType, string header)
    {
        string headerFormat = "<h6 style='color:#110c84'>[Header]</h6><br/>";
        string headerBgColor = "#D1DBF4";
        StringBuilder sbYtdSummary = new StringBuilder();
        bool isColored = false;

        ////Adding by Pankaj Dikshit for <DT 2823, 19-Feb-2013> - Start
        SitesService sService = new SitesService();
        int yearId = Convert.ToInt32(SessionHandler.spVar_Year);
        int monthId = Convert.ToInt32(SessionHandler.spVar_MonthId);
        DataSet dsGridCols = sService.GetByYearAdHocRadarId(yearId, '%');
        DataRow[] drTmp = dsGridCols.Tables[0].Select("SiteAbbrev = '-AM'");
        ////Adding by Pankaj Dikshit for <DT 2823, 19-Feb-2013> - End


        sbYtdSummary.Append(headerFormat.Replace("[Header]", header));

        sbYtdSummary.Append("<table width='100%' align='center' valign='top' border='1' style='font-size:7px; color:Black;'>");
        sbYtdSummary.Append("<tr>");

        sbYtdSummary.Append("<td border='1' width='28%' bgcolor='" + headerBgColor + "' align='left' valign='top' style='font-weight:bold;'>Key Performance Indicators (KPI) </td>");

        sbYtdSummary.Append("<td border='1' width='4%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>WAO</td>");
        sbYtdSummary.Append("<td border='1' width='4%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>KWI</td>");
        sbYtdSummary.Append("<td border='1' width='4%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>PIN</td>");
        sbYtdSummary.Append("<td border='1' width='4%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>WGP</td>");
        if (drTmp.Length > 0)
        {
            sbYtdSummary.Append("<td border='1' width='4%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>WAM</td>");
        }
        sbYtdSummary.Append("<td border='1' width='4%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>HUN</td>");
        sbYtdSummary.Append("<td border='1' width='4%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>WDL</td>");

        if (embType == "emb1" || embType == "nonemb1")
        {
            sbYtdSummary.Append("<td border='1' width='4%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>BUN</td>");
            sbYtdSummary.Append("<td border='1' width='4%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>VIC</td>");
        }
        if (embType == "emb1")
        {
            sbYtdSummary.Append("<td border='1' width='4%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>PTH</td>");

        }
        else if (embType == "nonemb1" && Convert.ToInt32(dtYtdSummary.Rows[0]["KpiScoreYtdANG"])>0)
        {
            sbYtdSummary.Append("<td border='1' width='4%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>ANG</td>");
        }


        if (embType == "nonemb2" || embType == "nonemb3")
        {
            sbYtdSummary.Append("<td border='1' width='4%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>FML</td>");
            sbYtdSummary.Append("<td border='1' width='4%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>BUN</td>");
        }

        if (embType == "nonemb2")
        {
            sbYtdSummary.Append("<td border='1' width='4%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>PNJ</td>");
            sbYtdSummary.Append("<td border='1' width='4%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>VIC</td>");
            sbYtdSummary.Append("<td border='1' width='4%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>ANG</td>");
            sbYtdSummary.Append("<td border='1' width='4%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>PTH</td>");
        }
        if (embType == "nonemb3")
        {
            sbYtdSummary.Append("<td border='1' width='4%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>BGN</td>");
            sbYtdSummary.Append("<td border='1' width='4%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>CE</td>");
            sbYtdSummary.Append("<td border='1' width='4%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>PNJ</td>");
            sbYtdSummary.Append("<td border='1' width='4%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>VIC</td>");

            sbYtdSummary.Append("<td border='1' width='4%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>ANG</td>");
            sbYtdSummary.Append("<td border='1' width='4%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>PTH</td>");
        }

        sbYtdSummary.Append("<td border='1' width='4%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>PTD</td>");


        sbYtdSummary.Append("</tr>");

        if (dtYtdSummary != null)
        {
            for (int i = 0; i < dtYtdSummary.Rows.Count; i++)
            {
                sbYtdSummary.Append("<tr>");



                if (dtYtdSummary.Rows[i]["KpiMeasure"] != null)
                {
                    sbYtdSummary.Append("<td width='28%'  border='1' valign='top' align='left'>");
                    sbYtdSummary.Append(dtYtdSummary.Rows[i]["KpiMeasure"].ToString());
                    sbYtdSummary.Append("</td>");
                }
                else
                {
                    sbYtdSummary.Append("<td width='28%'  border='1' valign='top' align='center'>");
                    sbYtdSummary.Append("</td>");
                }
                if (dtYtdSummary.Rows[i]["KpiMeasure"].ToString() != "Number of Companies:"
                       && dtYtdSummary.Rows[i]["KpiMeasure"].ToString() != "Number of Companies On Site:"
                       && dtYtdSummary.Rows[i]["KpiMeasure"].ToString() != "CSA - Self Audit (Conducted)")
                {
                    isColored = true;
                }
                else
                {
                    isColored = false;
                }


                if (dtYtdSummary.Rows[i]["KpiScoreYtd-WAO"] != null)
                {
                    if (!isColored)
                    {
                        sbYtdSummary.Append("<td width='4%'  border='1' valign='top' align='center'>");
                    }
                    else
                    {
                        sbYtdSummary.Append("<td width='4%' bgcolor='" + getBGColor(dtYtdSummary.Rows[i]["KpiScoreYtd-WAO"].ToString()) +
                                            "'  border='1' valign='top' align='center'>");
                    }
                    sbYtdSummary.Append(dtYtdSummary.Rows[i]["KpiScoreYtd-WAO"].ToString());
                    sbYtdSummary.Append("</td>");
                }
                else
                {
                    sbYtdSummary.Append("<td width='4%'  border='1' valign='top' align='center'>");

                    sbYtdSummary.Append("</td>");
                }


                if (dtYtdSummary.Rows[i]["KpiScoreYtdKWI"] != null)
                {
                    if (!isColored)
                    {
                        sbYtdSummary.Append("<td width='4%'  border='1' valign='top' align='center'>");
                    }
                    else
                    {
                        sbYtdSummary.Append("<td width='4%' bgcolor='" + getBGColor(dtYtdSummary.Rows[i]["KpiScoreYtdKWI"].ToString()) +
                                            "'  border='1' valign='top' align='center'>");
                    }
                    sbYtdSummary.Append(dtYtdSummary.Rows[i]["KpiScoreYtdKWI"].ToString());
                    sbYtdSummary.Append("</td>");
                }
                else
                {
                    sbYtdSummary.Append("<td width='4%'  border='1' valign='top' align='center'>");

                    sbYtdSummary.Append("</td>");
                }

                if (dtYtdSummary.Rows[i]["KpiScoreYtdPIN"] != null)
                {
                    if (!isColored)
                    {
                        sbYtdSummary.Append("<td width='4%'  border='1' valign='top' align='center'>");
                    }
                    else
                    {
                        sbYtdSummary.Append("<td width='4%' bgcolor='" + getBGColor(dtYtdSummary.Rows[i]["KpiScoreYtdPIN"].ToString()) +
                                            "'  border='1' valign='top' align='center'>");
                    }
                    sbYtdSummary.Append(dtYtdSummary.Rows[i]["KpiScoreYtdPIN"].ToString());
                    sbYtdSummary.Append("</td>");
                }
                else
                {
                    sbYtdSummary.Append("<td width='4%'  border='1' valign='top' align='center'>");

                    sbYtdSummary.Append("</td>");
                }


                if (dtYtdSummary.Rows[i]["KpiScoreYtdWGP"] != null)
                {
                    if (!isColored)
                    {
                        sbYtdSummary.Append("<td width='4%'  border='1' valign='top' align='center'>");
                    }
                    else
                    {
                        sbYtdSummary.Append("<td width='4%' bgcolor='" +
                            getBGColor(dtYtdSummary.Rows[i]["KpiScoreYtdWGP"].ToString()) +
                                            "'  border='1' valign='top' align='center'>");
                    }
                    sbYtdSummary.Append(dtYtdSummary.Rows[i]["KpiScoreYtdWGP"].ToString());
                    sbYtdSummary.Append("</td>");
                }
                else
                {
                    sbYtdSummary.Append("<td width='4%'  border='1' valign='top' align='center'>");

                    sbYtdSummary.Append("</td>");
                }

                if (drTmp.Length > 0)
                {
                    if (dtYtdSummary.Rows[i]["KpiScoreYtd-AM"] != null)
                    {
                        if (!isColored)
                        {
                            sbYtdSummary.Append("<td width='4%'  border='1' valign='top' align='center'>");
                        }
                        else
                        {
                            sbYtdSummary.Append("<td width='4%' bgcolor='" +
                                getBGColor(dtYtdSummary.Rows[i]["KpiScoreYtd-AM"].ToString()) +
                                                "'  border='1' valign='top' align='center'>");
                        }
                        sbYtdSummary.Append(dtYtdSummary.Rows[i]["KpiScoreYtd-AM"].ToString());
                        sbYtdSummary.Append("</td>");
                    }
                    else
                    {
                        sbYtdSummary.Append("<td width='4%'  border='1' valign='top' align='center'>");

                        sbYtdSummary.Append("</td>");
                    }
                 }


                if (dtYtdSummary.Rows[i]["KpiScoreYtdHUN"] != null)
                {
                    if (!isColored)
                    {
                        sbYtdSummary.Append("<td width='4%'  border='1' valign='top' align='center'>");
                    }
                    else
                    {
                        sbYtdSummary.Append("<td width='4%' bgcolor='" +
                            getBGColor(dtYtdSummary.Rows[i]["KpiScoreYtdHUN"].ToString()) +
                                            "'  border='1' valign='top' align='center'>");
                    }
                    sbYtdSummary.Append(dtYtdSummary.Rows[i]["KpiScoreYtdHUN"].ToString());
                    sbYtdSummary.Append("</td>");
                }
                else
                {
                    sbYtdSummary.Append("<td width='4%'  border='1' valign='top' align='center'>");

                    sbYtdSummary.Append("</td>");
                }


                if (dtYtdSummary.Rows[i]["KpiScoreYtdWDL"] != null)
                {
                    if (!isColored)
                    {
                        sbYtdSummary.Append("<td width='4%'  border='1' valign='top' align='center'>");
                    }
                    else
                    {
                        sbYtdSummary.Append("<td width='4%' bgcolor='" +
                            getBGColor(dtYtdSummary.Rows[i]["KpiScoreYtdWDL"].ToString()) +
                                            "'  border='1' valign='top' align='center'>");
                    }
                    sbYtdSummary.Append(dtYtdSummary.Rows[i]["KpiScoreYtdWDL"].ToString());
                    sbYtdSummary.Append("</td>");
                }
                else
                {
                    sbYtdSummary.Append("<td width='4%'  border='1' valign='top' align='center'>");

                    sbYtdSummary.Append("</td>");
                }

                //////////////////////////////////////////////


                if (embType == "emb1" || embType == "nonemb1")
                {


                    if (dtYtdSummary.Rows[i]["KpiScoreYtdBUN"] != null)
                    {
                        if (!isColored)
                        {
                            sbYtdSummary.Append("<td width='4%'  border='1' valign='top' align='center'>");
                        }
                        else
                        {
                            sbYtdSummary.Append("<td width='4%' bgcolor='" +
                                getBGColor(dtYtdSummary.Rows[i]["KpiScoreYtdBUN"].ToString()) +
                                                "'  border='1' valign='top' align='center'>");
                        }
                        sbYtdSummary.Append(dtYtdSummary.Rows[i]["KpiScoreYtdBUN"].ToString());
                        sbYtdSummary.Append("</td>");
                    }
                    else
                    {
                        sbYtdSummary.Append("<td width='4%'  border='1' valign='top' align='center'>");

                        sbYtdSummary.Append("</td>");
                    }

                    if (dtYtdSummary.Rows[i]["KpiScoreYtd-VIC"] != null)
                    {
                        if (!isColored)
                        {
                            sbYtdSummary.Append("<td width='4%'  border='1' valign='top' align='center'>");
                        }
                        else
                        {
                            sbYtdSummary.Append("<td width='4%' bgcolor='" +
                                getBGColor(dtYtdSummary.Rows[i]["KpiScoreYtd-VIC"].ToString()) +
                                                "'  border='1' valign='top' align='center'>");
                        }
                        sbYtdSummary.Append(dtYtdSummary.Rows[i]["KpiScoreYtd-VIC"].ToString());
                        sbYtdSummary.Append("</td>");
                    }
                    else
                    {
                        sbYtdSummary.Append("<td width='4%'  border='1' valign='top' align='center'>");
                        sbYtdSummary.Append("</td>");
                    }

                }


                if (embType == "emb1")
                {

                    if (dtYtdSummary.Rows[i]["KpiScoreYtdPTH"] != null)
                    {
                        if (!isColored)
                        {
                            sbYtdSummary.Append("<td width='4%'  border='1' valign='top' align='center'>");
                        }
                        else
                        {
                            sbYtdSummary.Append("<td width='4%' bgcolor='" +
                                getBGColor(dtYtdSummary.Rows[i]["KpiScoreYtdPTH"].ToString()) +
                                                "'  border='1' valign='top' align='center'>");
                        }
                        sbYtdSummary.Append(dtYtdSummary.Rows[i]["KpiScoreYtdPTH"].ToString());
                        sbYtdSummary.Append("</td>");
                    }
                    else
                    {
                        sbYtdSummary.Append("<td width='4%'  border='1' valign='top' align='center'>");
                        sbYtdSummary.Append("</td>");
                    }


                }
                else if (embType == "nonemb1" && Convert.ToInt32(dtYtdSummary.Rows[0]["KpiScoreYtdANG"]) > 0)
                {
                    //sbYtdSummary.Append("<td border='1' width='5%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>ANG</td>");

                    if (dtYtdSummary.Rows[i]["KpiScoreYtdANG"] != null)
                    {
                        if (!isColored)
                        {
                            sbYtdSummary.Append("<td width='4%'  border='1' valign='top' align='center'>");
                        }
                        else
                        {
                            sbYtdSummary.Append("<td width='4%' bgcolor='" +
                                getBGColor(dtYtdSummary.Rows[i]["KpiScoreYtdANG"].ToString()) +
                                                "'  border='1' valign='top' align='center'>");
                        }
                        sbYtdSummary.Append(dtYtdSummary.Rows[i]["KpiScoreYtdANG"].ToString());
                        sbYtdSummary.Append("</td>");
                    }
                    else
                    {
                        sbYtdSummary.Append("<td width='4%'  border='1' valign='top' align='center'>");
                        sbYtdSummary.Append("</td>");
                    }
                }


                if (embType == "nonemb2" || embType == "nonemb3")
                {
                    //sbYtdSummary.Append("<td border='1' width='5%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>FML</td>");
                    //sbYtdSummary.Append("<td border='1' width='5%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>BUN</td>");


                    if (dtYtdSummary.Rows[i]["KpiScoreYtdFML"] != null)
                    {
                        if (!isColored)
                        {
                            sbYtdSummary.Append("<td width='4%'  border='1' valign='top' align='center'>");
                        }
                        else
                        {
                            sbYtdSummary.Append("<td width='4%' bgcolor='" +
                                getBGColor(dtYtdSummary.Rows[i]["KpiScoreYtdFML"].ToString()) +
                                                "'  border='1' valign='top' align='center'>");
                        }
                        sbYtdSummary.Append(dtYtdSummary.Rows[i]["KpiScoreYtdFML"].ToString());
                        sbYtdSummary.Append("</td>");
                    }
                    else
                    {
                        sbYtdSummary.Append("<td width='4%'  border='1' valign='top' align='center'>");
                        sbYtdSummary.Append("</td>");
                    }


                    if (dtYtdSummary.Rows[i]["KpiScoreYtdBUN"] != null)
                    {
                        if (!isColored)
                        {
                            sbYtdSummary.Append("<td width='4%'  border='1' valign='top' align='center'>");
                        }
                        else
                        {
                            sbYtdSummary.Append("<td width='4%' bgcolor='" +
                                getBGColor(dtYtdSummary.Rows[i]["KpiScoreYtdBUN"].ToString()) +
                                                "'  border='1' valign='top' align='center'>");
                        }
                        sbYtdSummary.Append(dtYtdSummary.Rows[i]["KpiScoreYtdBUN"].ToString());
                        sbYtdSummary.Append("</td>");
                    }
                    else
                    {
                        sbYtdSummary.Append("<td width='4%'  border='1' valign='top' align='center'>");
                        sbYtdSummary.Append("</td>");
                    }

                }


                if (embType == "nonemb2")
                {
                    //sbYtdSummary.Append("<td border='1' width='5%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>PNJ</td>");
                    //sbYtdSummary.Append("<td border='1' width='5%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>VIC</td>");
                    //sbYtdSummary.Append("<td border='1' width='5%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>ANG</td>");
                    //sbYtdSummary.Append("<td border='1' width='5%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>PTH</td>");

                    if (dtYtdSummary.Rows[i]["KpiScoreYtdPNJ"] != null)
                    {
                        if (!isColored)
                        {
                            sbYtdSummary.Append("<td width='4%'  border='1' valign='top' align='center'>");
                        }
                        else
                        {
                            sbYtdSummary.Append("<td width='4%' bgcolor='" +
                                getBGColor(dtYtdSummary.Rows[i]["KpiScoreYtdPNJ"].ToString()) +
                                                "'  border='1' valign='top' align='center'>");
                        }
                        sbYtdSummary.Append(dtYtdSummary.Rows[i]["KpiScoreYtdPNJ"].ToString());
                        sbYtdSummary.Append("</td>");
                    }
                    else
                    {
                        sbYtdSummary.Append("<td width='4%'  border='1' valign='top' align='center'>");
                        sbYtdSummary.Append("</td>");
                    }

                    if (dtYtdSummary.Rows[i]["KpiScoreYtd-VIC"] != null)
                    {
                        if (!isColored)
                        {
                            sbYtdSummary.Append("<td width='4%'  border='1' valign='top' align='center'>");
                        }
                        else
                        {
                            sbYtdSummary.Append("<td width='4%' bgcolor='" +
                                getBGColor(dtYtdSummary.Rows[i]["KpiScoreYtd-VIC"].ToString()) +
                                                "'  border='1' valign='top' align='center'>");
                        }
                        sbYtdSummary.Append(dtYtdSummary.Rows[i]["KpiScoreYtd-VIC"].ToString());
                        sbYtdSummary.Append("</td>");
                    }
                    else
                    {
                        sbYtdSummary.Append("<td width='4%'  border='1' valign='top' align='center'>");
                        sbYtdSummary.Append("</td>");
                    }

                    if (dtYtdSummary.Rows[i]["KpiScoreYtdANG"] != null)
                    {
                        if (!isColored)
                        {
                            sbYtdSummary.Append("<td width='4%'  border='1' valign='top' align='center'>");
                        }
                        else
                        {
                            sbYtdSummary.Append("<td width='4%' bgcolor='" +
                                getBGColor(dtYtdSummary.Rows[i]["KpiScoreYtdANG"].ToString()) +
                                                "'  border='1' valign='top' align='center'>");
                        }
                        sbYtdSummary.Append(dtYtdSummary.Rows[i]["KpiScoreYtdANG"].ToString());
                        sbYtdSummary.Append("</td>");
                    }
                    else
                    {
                        sbYtdSummary.Append("<td width='4%'  border='1' valign='top' align='center'>");
                        sbYtdSummary.Append("</td>");
                    }

                    if (dtYtdSummary.Rows[i]["KpiScoreYtdPTH"] != null)
                    {
                        if (!isColored)
                        {
                            sbYtdSummary.Append("<td width='4%'  border='1' valign='top' align='center'>");
                        }
                        else
                        {
                            sbYtdSummary.Append("<td width='4%' bgcolor='" +
                                getBGColor(dtYtdSummary.Rows[i]["KpiScoreYtdPTH"].ToString()) +
                                                "'  border='1' valign='top' align='center'>");
                        }
                        sbYtdSummary.Append(dtYtdSummary.Rows[i]["KpiScoreYtdPTH"].ToString());
                        sbYtdSummary.Append("</td>");
                    }
                    else
                    {
                        sbYtdSummary.Append("<td width='4%'  border='1' valign='top' align='center'>");
                        sbYtdSummary.Append("</td>");
                    }


                }
                if (embType == "nonemb3")
                {
                    //sbYtdSummary.Append("<td border='1' width='5%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>BGN</td>");
                    //sbYtdSummary.Append("<td border='1' width='5%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>CE</td>");
                    //sbYtdSummary.Append("<td border='1' width='5%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>PNJ</td>");
                    //sbYtdSummary.Append("<td border='1' width='5%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>VIC</td>");

                    //sbYtdSummary.Append("<td border='1' width='5%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>ANG</td>");
                    //sbYtdSummary.Append("<td border='1' width='5%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>PTH</td>");
                    if (dtYtdSummary.Rows[i]["KpiScoreYtdBGN"] != null)
                    {
                        if (!isColored)
                        {
                            sbYtdSummary.Append("<td width='4%'  border='1' valign='top' align='center'>");
                        }
                        else
                        {
                            sbYtdSummary.Append("<td width='4%' bgcolor='" +
                                getBGColor(dtYtdSummary.Rows[i]["KpiScoreYtdBGN"].ToString()) +
                                                "'  border='1' valign='top' align='center'>");
                        }
                        sbYtdSummary.Append(dtYtdSummary.Rows[i]["KpiScoreYtdBGN"].ToString());
                        sbYtdSummary.Append("</td>");
                    }
                    else
                    {
                        sbYtdSummary.Append("<td width='4%'  border='1' valign='top' align='center'>");
                        sbYtdSummary.Append("</td>");
                    }


                    if (dtYtdSummary.Rows[i]["KpiScoreYtdCE"] != null)
                    {
                        if (!isColored)
                        {
                            sbYtdSummary.Append("<td width='4%'  border='1' valign='top' align='center'>");
                        }
                        else
                        {
                            sbYtdSummary.Append("<td width='4%' bgcolor='" +
                                getBGColor(dtYtdSummary.Rows[i]["KpiScoreYtdCE"].ToString()) +
                                                "'  border='1' valign='top' align='center'>");
                        }
                        sbYtdSummary.Append(dtYtdSummary.Rows[i]["KpiScoreYtdCE"].ToString());
                        sbYtdSummary.Append("</td>");
                    }
                    else
                    {
                        sbYtdSummary.Append("<td width='4%'  border='1' valign='top' align='center'>");
                        sbYtdSummary.Append("</td>");
                    }

                    if (dtYtdSummary.Rows[i]["KpiScoreYtdPNJ"] != null)
                    {
                        if (!isColored)
                        {
                            sbYtdSummary.Append("<td width='4%'  border='1' valign='top' align='center'>");
                        }
                        else
                        {
                            sbYtdSummary.Append("<td width='4%' bgcolor='" +
                                getBGColor(dtYtdSummary.Rows[i]["KpiScoreYtdPNJ"].ToString()) +
                                                "'  border='1' valign='top' align='center'>");
                        }
                        sbYtdSummary.Append(dtYtdSummary.Rows[i]["KpiScoreYtdPNJ"].ToString());
                        sbYtdSummary.Append("</td>");
                    }
                    else
                    {
                        sbYtdSummary.Append("<td width='4%'  border='1' valign='top' align='center'>");
                        sbYtdSummary.Append("</td>");
                    }

                    if (dtYtdSummary.Rows[i]["KpiScoreYtd-VIC"] != null)
                    {
                        if (!isColored)
                        {
                            sbYtdSummary.Append("<td width='4%'  border='1' valign='top' align='center'>");
                        }
                        else
                        {
                            sbYtdSummary.Append("<td width='4%' bgcolor='" +
                                getBGColor(dtYtdSummary.Rows[i]["KpiScoreYtd-VIC"].ToString()) +
                                                "'  border='1' valign='top' align='center'>");
                        }
                        sbYtdSummary.Append(dtYtdSummary.Rows[i]["KpiScoreYtd-VIC"].ToString());
                        sbYtdSummary.Append("</td>");
                    }
                    else
                    {
                        sbYtdSummary.Append("<td width='4%'  border='1' valign='top' align='center'>");
                        sbYtdSummary.Append("</td>");
                    }

                    if (dtYtdSummary.Rows[i]["KpiScoreYtdANG"] != null)
                    {
                        if (!isColored)
                        {
                            sbYtdSummary.Append("<td width='4%'  border='1' valign='top' align='center'>");
                        }
                        else
                        {
                            sbYtdSummary.Append("<td width='4%' bgcolor='" +
                                getBGColor(dtYtdSummary.Rows[i]["KpiScoreYtdANG"].ToString()) +
                                                "'  border='1' valign='top' align='center'>");
                        }
                        sbYtdSummary.Append(dtYtdSummary.Rows[i]["KpiScoreYtdANG"].ToString());
                        sbYtdSummary.Append("</td>");
                    }
                    else
                    {
                        sbYtdSummary.Append("<td width='4%'  border='1' valign='top' align='center'>");
                        sbYtdSummary.Append("</td>");
                    }

                    if (dtYtdSummary.Rows[i]["KpiScoreYtdPTH"] != null)
                    {
                        if (!isColored)
                        {
                            sbYtdSummary.Append("<td width='4%'  border='1' valign='top' align='center'>");
                        }
                        else
                        {
                            sbYtdSummary.Append("<td width='4%' bgcolor='" +
                                getBGColor(dtYtdSummary.Rows[i]["KpiScoreYtdPTH"].ToString()) +
                                                "'  border='1' valign='top' align='center'>");
                        }
                        sbYtdSummary.Append(dtYtdSummary.Rows[i]["KpiScoreYtdPTH"].ToString());
                        sbYtdSummary.Append("</td>");
                    }
                    else
                    {
                        sbYtdSummary.Append("<td width='4%'  border='1' valign='top' align='center'>");
                        sbYtdSummary.Append("</td>");
                    }



                }


                if (dtYtdSummary.Rows[i]["KpiScoreYtdPTD"] != null)
                {
                    if (!isColored)
                    {
                        sbYtdSummary.Append("<td width='4%'  border='1' valign='top' align='center'>");
                    }
                    else
                    {
                        sbYtdSummary.Append("<td width='4%' bgcolor='" +
                            getBGColor(dtYtdSummary.Rows[i]["KpiScoreYtdPTD"].ToString()) +
                                            "'  border='1' valign='top' align='center'>");
                    }
                    sbYtdSummary.Append(dtYtdSummary.Rows[i]["KpiScoreYtdPTD"].ToString());
                    sbYtdSummary.Append("</td>");
                }
                else
                {
                    sbYtdSummary.Append("<td width='4%'  border='1' valign='top' align='center'>");
                    sbYtdSummary.Append("</td>");
                }


                sbYtdSummary.Append("</tr>");

            }
        }
        else { }
        sbYtdSummary.Append("</table>");




        sbYtdSummary.Append("<br/>");

        return sbYtdSummary.ToString();
    }


    protected string GetIndicator(string dataValid)
    {
        StringBuilder sb = new StringBuilder();
        string headerFormat = "<h5 style='color:#110c84'>[Header]</h5><br/>";
        string headerBgColor = "#D1DBF4";
        string colorGreen = "Lime";
        string colorWhite = "White";
        string colorRed = "Red";
        string colorYellow = "Yellow";


        sb.Append("<table width='100%' valign='top' align='left'>");
        sb.Append("<tr>");
        sb.Append("<td width='50%' valign='top' align='left' style='color:Red; font-size:7px'>");
        sb.Append("<b>The information shown above is a snapshot.</b>");
        sb.Append("</td>");
        sb.Append("<td width='50%' valign='top' align='right' style='color:black; font-size:7px'>");
        sb.Append(dataValid);
        sb.Append("</td>");
        sb.Append("</tr>");
        sb.Append("</table>");





        sb.Append(headerFormat.Replace("[Header]", "Legend"));

        sb.Append("<table width='100%' align='center' valign='top' border='1' style='font-size:7px; color:Black;'>");

        sb.Append("<tr>");
        sb.Append("<td border='1' width='20%' bgcolor='" + colorGreen + "' align='center' valign='top'>");
        sb.Append("100");
        sb.Append("</td>");
        sb.Append("<td border='1' width='80%' bgcolor='" + colorWhite + "' align='left' valign='top'>");
        sb.Append("A Green Color Cell means that <b>100%</b> of all Companies are compliant for the specified KPI Measure.");
        sb.Append("</td>");
        sb.Append("</tr>");


        sb.Append("<tr>");
        sb.Append("<td border='1' width='20%' bgcolor='" + colorYellow + "' align='center' valign='top'>");
        sb.Append("Greater Than 80");
        sb.Append("</td>");
        sb.Append("<td border='1' width='80%' bgcolor='" + colorWhite + "' align='left' valign='top'>");
        sb.Append("A Yellow Colored Cell means that <b>greater than 80%</b> of all Companies are compliant for the specified KPI Measure. ");
        sb.Append("</td>");
        sb.Append("</tr>");


        sb.Append("<tr>");
        sb.Append("<td border='1' width='20%' bgcolor='" + colorRed + "' align='center' valign='top'>");
        sb.Append("Less than 80");
        sb.Append("</td>");
        sb.Append("<td border='1' width='80%' bgcolor='" + colorWhite + "' align='left' valign='top'>");
        sb.Append("A Red Coloured Cell means that <b>less than 80%</b> of all Companies are compliant for the specified KPI Measure. ");
        sb.Append("</td>");
        sb.Append("</tr>");

        sb.Append("</table>");

        return sb.ToString();
    }


    protected string GetFilter()
    {
        StringBuilder sb = new StringBuilder();
        sb.Append("<table width='100%' align='center' valign='top' style='font-size:9px; color:Black;'>");

        sb.Append("<tr>");
        sb.Append("<td width='30%' align='center' valign='top'>");
        sb.Append("<b>Company: </b>");

        int companyId = Convert.ToInt32(SessionHandler.spVar_CompanyId);

        int siteId = Convert.ToInt32(SessionHandler.spVar_SiteId);
        string companyName = string.Empty;
        
        if (companyId == -1)
        {
            companyName = "All";
        }
        else
        {
            CompaniesService compSer = new CompaniesService();
            Companies comp = compSer.GetByCompanyId(companyId);
            if (comp != null)
            {
                companyName = comp.CompanyName;
            }
        }



        string siteName = string.Empty;
        if (siteId > 0)
        {
            SitesService serSite = new SitesService();
            Sites site = serSite.GetBySiteId(siteId);
            if (site != null)
            {
                siteName = site.SiteName;
            }
        }
        else
        {
            if (siteId == -2)
            {
                siteName = "Australia";
            }
            else
            {
                RegionsService regSer = new RegionsService();
                Regions reg = regSer.GetByRegionId(-1 * siteId);
                if (reg != null)
                {
                    siteName = reg.RegionName;
                }
            }
        }

        int yearId = Convert.ToInt32(SessionHandler.spVar_Year);
        int monthId = Convert.ToInt32(SessionHandler.spVar_MonthId);


        sb.Append(companyName);
        sb.Append("</td>");
        sb.Append("<td width='30%' align='center' valign='top'>");
        sb.Append("<b>Site: </b>");
        sb.Append(siteName);
        sb.Append("</td>");
        sb.Append("<td width='30%' align='center' valign='top'>");
        sb.Append("<b>Year: </b>");
        sb.Append(yearId.ToString());
        sb.Append("</td>");
        sb.Append("</tr>");
        sb.Append("</table><br/>");

        return sb.ToString();

    }
    protected string[] CreateArrayForSA(int _CompanyId, int _SiteId, int No, int? _CategoryId)
    {
        string[] strArr = new string[2];
        // DataTable dt = new DataTable();
        StringBuilder sbColumns = new StringBuilder();

        int icount = 0;
        if (_SiteId < 0)
        {
            CompanySiteCategoryStandardService cscsService = new CompanySiteCategoryStandardService();
            RegionsSitesService rService = new RegionsSitesService();
            SitesService sService = new SitesService();

            TList<RegionsSites> rsTlist = rService.GetByRegionId(-1 * _SiteId);
            TList<CompanySiteCategoryStandard> cscsTlist = cscsService.GetByCompanyId(_CompanyId);

            foreach (RegionsSites rs in rsTlist)
            {
                foreach (CompanySiteCategoryStandard cscs in cscsTlist)
                {
                    if (cscs.SiteId == rs.SiteId)
                    {
                        bool cont = true;
                        if (_CategoryId != null)
                        {
                            if (cscs.CompanySiteCategoryId != _CategoryId) cont = false;
                        }

                        if (cont)
                        {
                            Sites s = sService.GetBySiteId(rs.SiteId);
                            if (s == null) throw new Exception("Error Occurred whilst Loading Site Columns");
                            string SiteName = s.SiteName;
                            if (SiteName.Length > 10) SiteName = s.SiteAbbrev;

                            if (icount > 0) sbColumns.Append("|");


                            sbColumns.Append(SiteName);

                            icount++;
                        }
                    }
                }
            }
        }
        else
        {
            //sbColumns.Append("KpiScoreYtd");
            //sbColumns.Append("||");
            //icount++;
            //sbColumns.Append("KpiScoreYtdPercentage");
            //sbColumns.Append("||");
            //icount++;
            //if (No == 2)
            //{
            //    sbColumns.Append("PlanMonth");
            //    sbColumns.Append("||");

            //    for (int i = 1; i <= 12; i++)
            //    {
            //        if (i > 1)
            //        {
            //            sbColumns.Append("||");
            //        }
            //        sbColumns.Append(String.Format("KpiScoreMonth{0}", i));
            //        icount++;

            //    }
            //    for (int i = 1; i <= 12; i++)
            //    {
            //        if (i > 1)
            //        {
            //            sbColumns.Append("||");
            //        }
            //        sbColumns.Append(String.Format("KpiScoreMonth{0}Percentage", i));
            //        icount++;
            //    }

            //}
        }

        strArr[0] = sbColumns.ToString();
        strArr[1] = icount.ToString();

        return strArr;
    }


    protected string CreateSummaryforSA(string[] strArr, string header, int tblNo)
    {
        string[] strArr1 = strArr[0].Split('|');

        DataSet dsSummary = (DataSet)Session["Radar_Table1-Summary"];

        DataTable dtSummary = dsSummary.Tables[tblNo];

        string headerFormat = "<h5 style='color:#110c84'>[Header]</h5><br/>";
        string headerBgColor = "#D1DBF4";
        StringBuilder sbYtdSummary = new StringBuilder();
        bool isColored = false;

        

        sbYtdSummary.Append(headerFormat.Replace("[Header]", header));

        sbYtdSummary.Append("<div align='right' style='width:100%; font-size:7px; color:Black; padding-bottom:6px'>Last Viewed At: " + DateTime.Now.ToString() + "</div><br/>");

        sbYtdSummary.Append("<table width='100%' align='center' valign='top' border='1' style='font-size:7px; color:Black;'>");
        sbYtdSummary.Append("<tr>");

        sbYtdSummary.Append("<td border='1' width='20%' bgcolor='" + headerBgColor + "' align='left' valign='top' style='font-weight:bold;'>Key Performance Indicators (KPI) </td>");


        for (int i = 0; i < strArr1.Length; i++)
        {
            sbYtdSummary.Append("<td border='1' width='20%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>" + strArr1[i] + "</td>");

        }
        sbYtdSummary.Append("</tr>");

        for (int i = 0; i < dtSummary.Rows.Count; i++)
        {
            sbYtdSummary.Append("<tr>");

            sbYtdSummary.Append("<td width='30%'  border='1' valign='top' align='left'>");
            sbYtdSummary.Append(dtSummary.Rows[i]["KpiMeasure"].ToString());
            sbYtdSummary.Append("</td>");

            for (int j = 0; j < strArr1.Length; j++)
            {
                //sbYtdSummary.Append("<td border='1' width='20%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>" + strArr1[i] + "</td>");
                string strRegion = "KpiScoreYtd" + strArr1[j];
                sbYtdSummary.Append("<td width='30%'  border='1' valign='top' align='center'>");
                sbYtdSummary.Append(dtSummary.Rows[i][strRegion].ToString());
                sbYtdSummary.Append("</td>");
            }
            sbYtdSummary.Append("</tr>");
        }
        sbYtdSummary.Append("</table>");

        return sbYtdSummary.ToString();
    }




    protected string CreateYtdforSA(string[] strArr, string header, int tblNo)
    {
        string[] strArr1 = strArr[0].Split('|');

        DataSet dsSummary = (DataSet)Session["Radar_Table2-Ytd"];

        DataTable dtSummary = dsSummary.Tables[tblNo];

        string headerFormat = "<h5 style='color:#110c84'>[Header]</h5><br/>";
        string headerBgColor = "#D1DBF4";
        StringBuilder sbYtdSummary = new StringBuilder();
        bool isColored = false;

        sbYtdSummary.Append(headerFormat.Replace("[Header]", header));

        sbYtdSummary.Append("<table width='100%' align='center' valign='top' border='1' style='font-size:7px; color:Black;'>");
        sbYtdSummary.Append("<tr>");

        sbYtdSummary.Append("<td border='1' width='20%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>Key Performance Indicators (KPI) </td>");


        for (int i = 0; i < strArr1.Length; i++)
        {
            sbYtdSummary.Append("<td border='1' width='20%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>Plan (" + strArr1[i] + ")</td>");
            sbYtdSummary.Append("<td border='1' width='20%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>" + strArr1[i] + "</td>");

        }
        sbYtdSummary.Append("</tr>");

        for (int i = 0; i < dtSummary.Rows.Count; i++)
        {
            sbYtdSummary.Append("<tr>");

            sbYtdSummary.Append("<td width='20%'  border='1' valign='top' align='left'>");
            sbYtdSummary.Append(dtSummary.Rows[i]["KpiMeasure"].ToString());
            sbYtdSummary.Append("</td>");

            for (int j = 0; j < strArr1.Length; j++)
            {
                //sbYtdSummary.Append("<td border='1' width='20%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>" + strArr1[i] + "</td>");
                string strPlanYtd = "PlanYtd" + strArr1[j];
                string strKpiScoreYtd = "KpiScoreYtd" + strArr1[j];

                sbYtdSummary.Append("<td width='20%'  border='1' valign='top' align='center'>");
                sbYtdSummary.Append(dtSummary.Rows[i][strPlanYtd].ToString());
                sbYtdSummary.Append("</td>");

                sbYtdSummary.Append("<td width='20%'  border='1' valign='top' align='center' bgcolor='" + getBGColorCSA(dtSummary.Rows[i][strKpiScoreYtd].ToString(), dtSummary.Rows[i][strPlanYtd].ToString(), dtSummary.Rows[i]["KpiMeasure"].ToString()) + "'>");
                sbYtdSummary.Append(dtSummary.Rows[i][strKpiScoreYtd].ToString());
                sbYtdSummary.Append("</td>");
            }
            sbYtdSummary.Append("</tr>");
        }
        sbYtdSummary.Append("</table>");

        return sbYtdSummary.ToString();
    }




    protected string[] GetYtdArrForRegions(int _CategoryId)
    {
        string[] strArr = new string[2];
        StringBuilder sb = new StringBuilder();
        StringBuilder sb1 = new StringBuilder();

        

        SitesService sService = new SitesService();
        RegionsSitesService rService = new RegionsSitesService();
        CompanySiteCategoryStandardService cscsService = new CompanySiteCategoryStandardService();

        int _SiteId = Convert.ToInt32(SessionHandler.spVar_SiteId);
        int yearId = Convert.ToInt32(SessionHandler.spVar_Year);
        int monthId = Convert.ToInt32(SessionHandler.spVar_MonthId);
       

        DataSet dsSites = sService.CompanySiteCategoryStandard_ByRegionId(-1 * _SiteId);

        if (dsSites.Tables.Count == 0) throw new Exception("Error Occured whilst Loading Site Columns 1");
        if (dsSites.Tables[0].Rows.Count == 0) throw new Exception("Error Occured whilst Loading Site Columns 2");


        RegionsService regionService = new RegionsService();
        Regions rWAO = regionService.GetByRegionInternalName("WAO");
        Regions rVIC = regionService.GetByRegionInternalName("VICOPS");
        Regions rARP = regionService.GetByRegionInternalName("ARP");
        Regions rWAM = regionService.GetByRegionInternalName("Mines");
        //WAO

        string _FieldName = "";
        string _SiteName = "";

        if ((_SiteId * -1) == rWAO.RegionId)
        {
            _FieldName = "-WAO";
            _SiteName = "WAO";
        }
        else if ((_SiteId * -1) == rVIC.RegionId)
        {
            _FieldName = "-VIC";
            _SiteName = "VIC";
        }
        else if ((_SiteId * -1) == rARP.RegionId)
        {
            _FieldName = "-ARP";
            //SiteName = "Victorian Operations";
            _SiteName = "ARP";
        }
        else if ((_SiteId * -1) == rWAM.RegionId)
        {
            _FieldName = "-AM";
            _SiteName = "WAM";
        }

        if (!String.IsNullOrEmpty(_FieldName) && !String.IsNullOrEmpty(_SiteName))
        {
            sb.Append(_FieldName);
            sb.Append("|");
            sb1.Append(_SiteName);
            sb1.Append("|");
            //grid_VisibleIndex = Helper.Radar.AddGridColumn(_grid, String.Format("KpiScoreYtd{0}", _FieldName), _SiteName, _SiteName, 200, grid_VisibleIndex);
            //grid2_VisibleIndex = Helper.Radar.AddGridColumn(_grid2, String.Format("KpiScoreYtd{0}", _FieldName), _SiteName, _SiteName, null, grid2_VisibleIndex);
        }


        //VIC

        //ARP
        int iCount = 0;

        foreach (DataRow dr in dsSites.Tables[0].Rows)
        {
            int _SiteId2 = Convert.ToInt32(dr[0].ToString());
            TList<CompanySiteCategoryStandard> cscs = cscsService.GetBySiteIdCompanySiteCategoryId(_SiteId2, _CategoryId);
            if (cscs != null)
            {
                if (cscs.Count > 0)
                {
                    //site = true;

                    string SiteName = dr[1].ToString(); //SiteName
                    if (SiteName.Length > 1) SiteName = dr[2].ToString(); //SiteAbbrev
                    if (iCount > 0)
                    {
                        sb.Append("|");
                        sb1.Append("|");
                    }
                    sb.Append(SiteName);
                    sb1.Append(SiteName);
                    // grid_VisibleIndex = Helper.Radar.AddGridColumn(_grid, String.Format("KpiScoreYtd{0}", SiteName), SiteName, SiteName, 200, grid_VisibleIndex);
                    // grid2_VisibleIndex = Helper.Radar.AddGridColumn(_grid2, String.Format("KpiScoreYtd{0}", SiteName), SiteName, SiteName, null, grid2_VisibleIndex);
                    iCount++;
                }
            }
        }
        strArr[0] = sb.ToString();
        strArr[1] = sb1.ToString();
        return strArr;
    }

    protected string CreateYtdSummaryforRegions(string[] strArr, string header, string EmbType)
    {
        ////Adding by Pankaj Dikshit for <DT 2823, 19-Feb-2013> - Start
        SitesService sService1 = new SitesService();
        int yearId = Convert.ToInt32(SessionHandler.spVar_Year);
        int monthId = Convert.ToInt32(SessionHandler.spVar_MonthId);
        DataSet dsGridCols = sService1.GetByYearAdHocRadarId(yearId, '%');
        DataRow[] drTmp = dsGridCols.Tables[0].Select("SiteAbbrev = '-AM'");
        ////Adding by Pankaj Dikshit for <DT 2823, 19-Feb-2013> - End


        string[] strArr1 = strArr[0].Split('|');

        string[] strArr2 = strArr[1].Split('|');

        DataSet dsSummary = (DataSet)Session["Radar_Table1-Summary"];
        DataTable dtSummary = new DataTable();

        if (EmbType == "E")
        {
            dtSummary = dsSummary.Tables[0];
        }
        else if (EmbType == "NE1")
        {
            dtSummary = dsSummary.Tables[1];
        }
        else if (EmbType == "NE2")
        {
            dtSummary = dsSummary.Tables[2];
        }
        else if (EmbType == "NE3")
        {
            dtSummary = dsSummary.Tables[3];
        }
        
        string headerFormat = "<h5 style='color:#110c84'>[Header]</h5><br/>";
        string headerBgColor = "#D1DBF4";
        StringBuilder sbYtdSummary = new StringBuilder();
        bool isColored = false;

        sbYtdSummary.Append(headerFormat.Replace("[Header]", header));

        sbYtdSummary.Append("<table width='100%' align='center' valign='top' border='1' style='font-size:7px; color:Black;'>");
        sbYtdSummary.Append("<tr>");

        sbYtdSummary.Append("<td border='1' width='30%' bgcolor='" + headerBgColor + "' align='left' valign='top' style='font-weight:bold;'>Safety Rates</td>");


        for (int i = 0; i < strArr1.Length; i++)
        {
            if (strArr2[i].Contains("WAM"))
            {
                if (drTmp.Length > 0)
                {
                    sbYtdSummary.Append("<td border='1' width='10%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>" + strArr2[i] + "</td>");
                }
            }
            else
            {
                sbYtdSummary.Append("<td border='1' width='10%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>" + strArr2[i] + "</td>");
            }

        }
        sbYtdSummary.Append("</tr>");

        for (int i = 0; i < dtSummary.Rows.Count; i++)
        {
            sbYtdSummary.Append("<tr>");

            sbYtdSummary.Append("<td width='30%'  border='1' valign='top' align='left'>");
            sbYtdSummary.Append(dtSummary.Rows[i]["KpiMeasure"].ToString());
            sbYtdSummary.Append("</td>");

            for (int j = 0; j < strArr1.Length; j++)
            {
                //sbYtdSummary.Append("<td border='1' width='20%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>" + strArr1[i] + "</td>");

                string strKpiScoreYtd = "KpiScoreYtd" + strArr1[j];

                if (strArr1[j].Contains("AM"))
                {
                    if (drTmp.Length > 0)
                    {
                        sbYtdSummary.Append("<td  width='10%' border='1' valign='top' align='center'>");
                        sbYtdSummary.Append(dtSummary.Rows[i][strKpiScoreYtd].ToString());
                        sbYtdSummary.Append("</td>");
                    }
                }
                else
                {
                    sbYtdSummary.Append("<td  width='10%' border='1' valign='top' align='center'>");
                    sbYtdSummary.Append(dtSummary.Rows[i][strKpiScoreYtd].ToString());
                    sbYtdSummary.Append("</td>");
                }
            }
            sbYtdSummary.Append("</tr>");
        }
        sbYtdSummary.Append("</table>");

        return sbYtdSummary.ToString();
    }


    protected string CreateYtdDetailsforRegions(string[] strArr, string EmbType)
    {
        ////Adding by Pankaj Dikshit for <DT 2823, 19-Feb-2013> - Start
        SitesService sService1 = new SitesService();
        int yearId = Convert.ToInt32(SessionHandler.spVar_Year);
        int monthId = Convert.ToInt32(SessionHandler.spVar_MonthId);
        DataSet dsGridCols = sService1.GetByYearAdHocRadarId(yearId, '%');
        DataRow[] drTmp = dsGridCols.Tables[0].Select("SiteAbbrev = '-AM'");
        ////Adding by Pankaj Dikshit for <DT 2823, 19-Feb-2013> - End


        string[] strArr1 = strArr[0].Split('|');

        string[] strArr2 = strArr[1].Split('|');

        DataSet dsSummary = (DataSet)Session["Radar_Table2-Ytd"];

        DataTable dtSummary = new DataTable();

        if (EmbType == "E")
        {
            dtSummary = dsSummary.Tables[0];
        }
        else if (EmbType == "NE1")
        {
            dtSummary = dsSummary.Tables[1];
        }
        else if (EmbType == "NE2")
        {
            dtSummary = dsSummary.Tables[2];
        }
        else if (EmbType == "NE3")
        {
            dtSummary = dsSummary.Tables[3];
        }

        string headerFormat = "<h5 style='color:#110c84'>[Header]</h5><br/>";
        string headerBgColor = "#D1DBF4";
        StringBuilder sbYtdSummary = new StringBuilder();
        bool isColored = false;

        sbYtdSummary.Append(headerFormat.Replace("[Header]", "The below table indicates the percentage (%) number of companies compliant per Key Performance Indicator (KPI) measure: "));

        sbYtdSummary.Append("<table width='100%' align='center' valign='top' border='1' style='font-size:7px; color:Black;'>");
        sbYtdSummary.Append("<tr>");

        sbYtdSummary.Append("<td border='1' width='30%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>Key Performace Indicators (KPI)</td>");


        for (int i = 0; i < strArr1.Length; i++)
        {
            if (strArr2[i].Contains("WAM"))
            {
                if (drTmp.Length > 0)
                {
                    sbYtdSummary.Append("<td border='1' width='10%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>" + strArr2[i] + "</td>");
                }
            }
            else
            {
                sbYtdSummary.Append("<td border='1' width='10%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>" + strArr2[i] + "</td>");
            }

        }
        sbYtdSummary.Append("</tr>");

        for (int i = 0; i < dtSummary.Rows.Count; i++)
        {

            if (dtSummary.Rows[i]["KpiMeasure"].ToString() != "Number of Companies:"
                       && dtSummary.Rows[i]["KpiMeasure"].ToString() != "Number of Companies On Site:"
                       && dtSummary.Rows[i]["KpiMeasure"].ToString() != "CSA - Self Audit (Conducted)")
            {
                isColored = true;
            }
            else
            {
                isColored = false;
            }



            sbYtdSummary.Append("<tr>");

            sbYtdSummary.Append("<td width='30%'  border='1' valign='top' align='left'>");
            sbYtdSummary.Append(dtSummary.Rows[i]["KpiMeasure"].ToString());
            sbYtdSummary.Append("</td>");

            for (int j = 0; j < strArr1.Length; j++)
            {
                if (strArr1[j].Contains("-AM"))
                {
                    if (drTmp.Length > 0)
                    {
                        //sbYtdSummary.Append("<td border='1' width='20%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>" + strArr1[i] + "</td>");

                        string strKpiScoreYtd = "KpiScoreYtd" + strArr1[j];
                        if (isColored)
                        {
                            sbYtdSummary.Append("<td width='10%' bgcolor='" + getBGColor(dtSummary.Rows[i][strKpiScoreYtd].ToString()) + "'  border='1' valign='top' align='center'>");
                        }
                        else
                        {
                            sbYtdSummary.Append("<td width='10%'  border='1' valign='top' align='center'>");
                        }
                        sbYtdSummary.Append(dtSummary.Rows[i][strKpiScoreYtd].ToString());
                        sbYtdSummary.Append("</td>");
                    }
                }
                else
                {
                    string strKpiScoreYtd = "KpiScoreYtd" + strArr1[j];
                    if (isColored)
                    {
                        sbYtdSummary.Append("<td width='10%' bgcolor='" + getBGColor(dtSummary.Rows[i][strKpiScoreYtd].ToString()) + "'  border='1' valign='top' align='center'>");
                    }
                    else
                    {
                        sbYtdSummary.Append("<td width='10%'  border='1' valign='top' align='center'>");
                    }
                    sbYtdSummary.Append(dtSummary.Rows[i][strKpiScoreYtd].ToString());
                    sbYtdSummary.Append("</td>"); 
                }
            }
            sbYtdSummary.Append("</tr>");
        }
        sbYtdSummary.Append("</table>");

        return sbYtdSummary.ToString();
    }

   


    
}

