﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Adminv2_UserControls_Default" Codebehind="default.ascx.cs" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
    


<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxTabControl" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxClasses" tagprefix="dx" %>
    


<style type="text/css">
img 
{
	border-width: 0px;
}

    .style3
    {
        width: 140px;
        height: 84px;
    }

</style>
    


<asp:Label ID="Label1" runat="server" ForeColor="Red"></asp:Label>
<br />
<dx:ASPxPageControl ID="ASPxPageControl1" runat="server" ActiveTabIndex="0" 
    CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
    CssPostfix="Office2003Blue" 
    SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" Visible="False" 
    Width="900px">
    <TabPages>
        <dx:TabPage Name="tabStandard" Text="Standard">
            <ContentCollection>
                <dx:ContentControl runat="server" SupportsDisabledAttribute="True">
                    <dx:ASPxPanel ID="ASPxPanel1" runat="server" Height="440px" Visible="True" 
                        Width="870px">
                        <PanelCollection>
                            <dx:PanelContent runat="server">
                                <table border="0" cellpadding="0" cellspacing="0" 
                                    style="width: 870px">
                                    <tr>
                                        <td style="width: 140px; text-align: center; height: 77px;">
                                            <a href="AdminTasks.aspx">
                                            <dxe:ASPxImage ID="imgAdminTasks" runat="server" 
                                                ImageUrl="~/Images/AdminLogos/AdminTasks.png" IsPng="True">
                                            </dxe:ASPxImage></a>
                                        </td>
                                        <td style="width: 140px; text-align: center; height: 77px;">
                                            <a href="Admin.aspx?s=CsmsFiles">
                                            <dxe:ASPxImage ID="imgCsmsFiles" runat="server" 
                                                ImageUrl="~/Images/AdminLogos/CsmsFiles.png" IsPng="True">
                                            </dxe:ASPxImage></a>
                                        </td>
                                        <td style="width: 140px; text-align: center; height: 77px;">
                                            <a href="Admin.aspx?s=CsmsEmailLogHistory">
                                            <dxe:ASPxImage ID="imgCsmsEmailLogHistory" runat="server" 
                                                ImageUrl="~/Images/AdminLogos/CsmsEmailLogHistory.png" IsPng="True">
                                            </dxe:ASPxImage></a>
                                        </td>
                                        <td style="width: 140px; text-align: center; height: 77px;">
                                            <a href="Admin.aspx?s=News">
                                            <dxe:ASPxImage ID="imgNews" runat="server" 
                                                ImageUrl="~/Images/adminLogos/news.png" IsPng="True">
                                            </dxe:ASPxImage></a>
                                        </td>
                                        <td style="width: 140px; text-align: center; height: 77px;">
                                            <a href="Admin.aspx?s=SystemHealth">
                                            <dxe:ASPxImage ID="imgSystemHealth" runat="server" 
                                                ImageUrl="~/Images/AdminLogos/systemHealth.png" IsPng="True">
                                            </dxe:ASPxImage></a>
                                            </td>
                                        <td style="width: 140px; text-align: center; height: 77px;">
                                            <a href="Admin.aspx?s=Usage">
                                            <dxe:ASPxImage ID="imgViewUsageStats" runat="server" 
                                                ImageUrl="~/Images/adminLogos/viewUsageStats.png" IsPng="True">
                                            </dxe:ASPxImage></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 140px; text-align: center; height: 16px;">
                                            <dxe:ASPxHyperLink ID="hlAdminTasks" runat="server" 
                                                NavigateUrl="~/AdminTasks.aspx" Text="Admin Tasks">
                                            </dxe:ASPxHyperLink>
                                        </td>
                                        <td style="width: 140px; text-align: center; height: 16px;">
                                            <dxe:ASPxHyperLink ID="hlCsmsFiles" runat="server" 
                                                NavigateUrl="~/Admin.aspx?s=CsmsFiles" Text="CSMS Files">
                                            </dxe:ASPxHyperLink>
                                        </td>
                                        <td style="width: 140px; text-align: center; height: 16px;">
                                            <dxe:ASPxHyperLink ID="hlEmailLogs" runat="server" 
                                                NavigateUrl="~/Admin.aspx?s=CsmsEmailLogHistory" Text="Email Logs">
                                            </dxe:ASPxHyperLink>
                                        </td>
                                        <td style="width: 140px; text-align: center; height: 16px;">
                                            <dxe:ASPxHyperLink ID="hlNews" runat="server" 
                                                NavigateUrl="~/Admin.aspx?s=News" Text="News">
                                            </dxe:ASPxHyperLink>
                                        </td>
                                        <td style="width: 140px; text-align: center; height: 16px;">
                                            <dxe:ASPxHyperLink ID="hlSystemHealth" runat="server" 
                                                NavigateUrl="~/Admin.aspx?s=SystemHealth" Text="System Health">
                                            </dxe:ASPxHyperLink>
                                        </td>
                                        <td style="width: 140px; text-align: center; height: 16px;">
                                            <dxe:ASPxHyperLink ID="hlUsage" runat="server" 
                                                NavigateUrl="~/Admin.aspx?s=Usage" Text="View Usage Statistics">
                                            </dxe:ASPxHyperLink>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="style3" style="text-align: center; "></td>
                                        <td class="style3" style="text-align: center; "></td>
                                        <td class="style3" style="text-align: center; "></td>
                                        <td class="style3" style="text-align: center; "></td>
                                        <td class="style3" style="text-align: center; "></td>
                                        <td class="style3" style="text-align: center; "></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 140px; text-align: center; height: 16px;">
                                            <a href="Admin.aspx?s=ConfigSa812">
                                            <dxe:ASPxImage ID="imgConfigSa812" runat="server" 
                                                ImageUrl="~/Images/adminLogos/audit.png" IsPng="True">
                                            </dxe:ASPxImage></a>
                                        </td>
                                        <td style="width: 140px; text-align: center; height: 16px;">
                                            <a href="Admin.aspx?s=ContractorServices">
                                            <dxe:ASPxImage ID="imgContractorServices" runat="server" 
                                                ImageUrl="~/Images/adminLogos/contractorServices.png" IsPng="True">
                                            </dxe:ASPxImage></a>
                                        </td>
                                        <%--<td style="width: 140px; text-align: center; height: 16px;">
                                            <a href="Admin.aspx?s=Ehsims">
                                            <dxe:ASPxImage ID="imgEhsims" runat="server" 
                                                ImageUrl="~/Images/adminLogos/ehsimsCompaniesMap.png" IsPng="True">
                                            </dxe:ASPxImage></a>
                                        </td>--%>
                                        <td style="width: 140px; height: 16px; text-align: center">
                                            <a href="Admin.aspx?s=KpiHelpFiles">
                                            <dxe:ASPxImage ID="ASPxImage7" runat="server" 
                                                ImageUrl="~/Images/AdminLogos/KpiHelpFiles.png" IsPng="True">
                                            </dxe:ASPxImage></a>
                                        </td>
                                        <td style="width: 140px; text-align: center; height: 16px;">
                                            <a href="Admin_Ebi.aspx">
                                            <dxe:ASPxImage ID="imgEbi" runat="server" 
                                                ImageUrl="~/Images/adminLogos/ebi.png" IsPng="True">
                                            </dxe:ASPxImage></a>
                                        </td>
                                        <td style="width: 140px; text-align: center; height: 16px;">
                                            <a href="Admin.aspx?s=QuestionnaireRationale">
                                            <dxe:ASPxImage ID="imgQuestionnaireRationale" runat="server" 
                                                ImageUrl="~/Images/AdminLogos/questionnaireRationale.png" IsPng="True">
                                            </dxe:ASPxImage></a>
                                        </td>
                                        <td style="width: 140px; text-align: center; height: 16px;">
                                            <a href="Admin.aspx?s=residentialNew">
                                            <dxe:ASPxImage ID="imgResidential" runat="server" 
                                                ImageUrl="~/Images/adminLogos/residency.png" IsPng="True">
                                            </dxe:ASPxImage></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 140px; text-align: center; height: 16px;">
                                            <dxe:ASPxHyperLink ID="hlSa812" runat="server" 
                                                NavigateUrl="~/Admin.aspx?s=ConfigSa812" Text="Audit S8.12" Width="50px">
                                            </dxe:ASPxHyperLink>
                                        </td>
                                        <td style="width: 140px; height: 16px; text-align: center">
                                            <dxe:ASPxHyperLink ID="hlContractorServices" runat="server" 
                                                NavigateUrl="~/Admin.aspx?s=ContractorServices" Text="Contractor Services" 
                                                Width="100px">
                                            </dxe:ASPxHyperLink>
                                        </td>
                                        <td style="width: 140px; height: 16px; text-align: center">
                                            <dxe:ASPxHyperLink ID="ASPxHyperLink1" runat="server" 
                                                NavigateUrl="~/Admin.aspx?s=KpiHelpFiles" Text="KPI Help Files" 
                                                Width="100px">
                                            </dxe:ASPxHyperLink>
                                        </td>
                                        <td style="width: 140px; height: 16px; text-align: center">
                                            <dxe:ASPxHyperLink ID="hlEbi2" runat="server" NavigateUrl="~/Admin_Ebi.aspx" 
                                                Text="Honeywell EBI Integration">
                                            </dxe:ASPxHyperLink>
                                        </td>
                                        <td style="width: 140px; height: 16px; text-align: center">
                                            <dxe:ASPxHyperLink ID="hlQuestionnaireRationale" runat="server" 
                                                NavigateUrl="~/Admin.aspx?s=QuestionnaireRationale" 
                                                Text="Questionnaire Rationale" Width="100px">
                                            </dxe:ASPxHyperLink>
                                        </td>
                                        <td style="width: 140px; height: 16px; text-align: center">
                                            <dxe:ASPxHyperLink ID="hlResidency" runat="server" 
                                                NavigateUrl="~/Admin.aspx?s=residentialNew" Text="Safety Compliance Categories">
                                            </dxe:ASPxHyperLink>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 140px; height: 16px; text-align: center">
                                            <a href="Admin_CompareEhsimsIhs.aspx">
                                            <dxe:ASPxImage ID="imgEhsimsCompare" runat="server" 
                                                ImageUrl="~/Images/adminLogos/ehsimsCompare.png" IsPng="True">
                                            </dxe:ASPxImage></a>
                                        </td>
                                        <td style="width: 140px; height: 16px; text-align: center">
                                            <a href="Admin.aspx?s=IhsEmail">
                                            <dxe:ASPxImage ID="imgIhsEmail" runat="server" 
                                                ImageUrl="~/Images/AdminLogos/IhsEmail.png" IsPng="True">
                                            </dxe:ASPxImage></a>
                                        </td>
                                        <td style="width: 140px; height: 16px; text-align: center;">
                                            <a href="Admin.aspx?s=FileVault_HelpFiles">
                                            <dxe:ASPxImage ID="imgCsmsHelpFiles" runat="server" 
                                                ImageUrl="~/Images/AdminLogos/CsmsHelpFiles.png" IsPng="True">
                                            </dxe:ASPxImage></a>
                                        </td>
                                        <td style="width: 140px; height: 16px; text-align: center;">
                                            <a href="Admin.aspx?s=MaintainLabourClassifications">
                                            <dxe:ASPxImage ID="imgMaintainLabourClassifications" runat="server" 
                                                ImageUrl="~/Images/AdminLogos/MaintainLabourClassifications.png" IsPng="True">
                                            </dxe:ASPxImage></a>
                                        </td>
                                        <%--<td style="width: 140px; height: 16px; text-align: center">
                                            <a href="Admin.aspx?s=CompaniesHrMap">
                                            <dxe:ASPxImage ID="imgHrCompaniesMap" runat="server" 
                                                ImageUrl="~/Images/AdminLogos/CompaniesHrMap.png" IsPng="True">
                                            </dxe:ASPxImage></a>
                                        </td>--%>
                                        <td style="width: 140px; height: 16px; text-align: center"></td>
                                        <td style="width: 140px; height: 16px; text-align: center"></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 140px; height: 16px; text-align: center">
                                            <dxe:ASPxHyperLink ID="hlEhsimsCompare" runat="server" 
                                                NavigateUrl="~/Admin_CompareEhsimsIhs.aspx" 
                                                Text="Compare IHS &amp; CSMS" Width="120px">
                                            </dxe:ASPxHyperLink>
                                        </td>
                                        <td style="width: 140px; height: 16px; text-align: center">
                                            <dxe:ASPxHyperLink ID="hlIhsEmail" runat="server" 
                                                NavigateUrl="~/Admin.aspx?s=IhsEmail" Text="E-mail IHS Contractor Hours" 
                                                Width="100px">
                                            </dxe:ASPxHyperLink>
                                        </td>
                                        <td style="width: 140px; height: 16px; text-align: center;">
                                            <dxe:ASPxHyperLink ID="hlCsmsHelpFiles" runat="server" 
                                                NavigateUrl="~/Admin.aspx?s=FileVault_HelpFiles" Text="CSMS Help Files" 
                                                Width="100px">
                                            </dxe:ASPxHyperLink></td>
                                        <td style="width: 140px; height: 16px; text-align: center;">
                                            <dxe:ASPxHyperLink ID="hlMaintainLabourClassifications" runat="server" 
                                                NavigateUrl="~/Admin.aspx?s=MaintainLabourClassifications" Text="Maintain Labour Classifications" 
                                                Width="100px">
                                            </dxe:ASPxHyperLink>
                                        </td>
                                        <%--<td style="width: 140px; height: 16px; text-align: center">
                                            <dxe:ASPxHyperLink ID="hlHrCompaniesMap" runat="server" 
                                                NavigateUrl="~/Admin.aspx?s=CompaniesHrMap" Text="HR Companies Map" 
                                                Width="100px">
                                            </dxe:ASPxHyperLink>
                                        </td>--%>
                                       <td>&nbsp;</td>
                                       <td style="width: 140px; height: 16px; text-align: center">&nbsp;</td>
                                    </tr>
                                </table>
                                <br />
                            </dx:PanelContent>
                        </PanelCollection>
                    </dx:ASPxPanel>
                </dx:ContentControl>
            </ContentCollection>
        </dx:TabPage>
        <dx:TabPage Name="tabAdvanced" Text="Advanced">
            <ContentCollection>
                <dx:ContentControl runat="server" SupportsDisabledAttribute="True">
                    <dx:ASPxPanel ID="ASPxPanel2" runat="server" Height="450px" Visible="True" 
                        Width="870px">
                        <PanelCollection>
                            <dx:PanelContent runat="server">
                                <table border="0" cellpadding="0" cellspacing="0" 
                                    style="width: 870px">
                                    <tr>
                                        <td style="width: 140px; text-align: center; height: 77px;">
                                            <a href="Admin.aspx?s=Companies">
                                            <dxe:ASPxImage ID="imgCompanies" runat="server" 
                                                ImageUrl="~/Images/adminLogos/companies.png" IsPng="True">
                                            </dxe:ASPxImage></a>
                                        </td>
                                        <td style="width: 140px; text-align: center; height: 77px;">
                                            <a href="Admin.aspx?s=Sites">
                                            <dxe:ASPxImage ID="imgSites" runat="server" 
                                                ImageUrl="~/Images/adminLogos/sites.png" IsPng="True">
                                            </dxe:ASPxImage></a>
                                        </td>
                                        <td style="width: 140px; text-align: center; height: 77px;">
                                            <a href="Admin.aspx?s=users">
                                            <dxe:ASPxImage ID="imgUsers" runat="server" 
                                                ImageUrl="~/Images/adminLogos/users.png" IsPng="True">
                                            </dxe:ASPxImage></a>
                                        </td>
                                        <td style="width: 140px; text-align: center; height: 77px;">
                                            <a href="Admin.aspx?s=EmailTemplates">
                                            <dxe:ASPxImage ID="imgEmailTemplates" runat="server" 
                                                ImageUrl="~/Images/AdminLogos/EmailTemplates.png" IsPng="True">
                                            </dxe:ASPxImage></a>
                                            </td>
                                        <td style="width: 140px; text-align: center; height: 77px;"></td>
                                        <td style="width: 140px; text-align: center; height: 77px;">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td style="width: 140px; text-align: center; height: 16px;">
                                            <dxe:ASPxHyperLink ID="hlCompanies" runat="server" 
                                                NavigateUrl="~/Admin.aspx?s=Companies" Text="Companies">
                                            </dxe:ASPxHyperLink>
                                        </td>
                                        <td style="width: 140px; text-align: center; height: 16px;">
                                            <dxe:ASPxHyperLink ID="hlSites" runat="server" 
                                                NavigateUrl="~/Admin.aspx?s=Sites" Text="Sites">
                                            </dxe:ASPxHyperLink>
                                        </td>
                                        <td style="width: 140px; text-align: center; height: 16px;">
                                            <dxe:ASPxHyperLink ID="hlUsers" runat="server" 
                                                NavigateUrl="~/Admin.aspx?s=users" Text="Users">
                                            </dxe:ASPxHyperLink>
                                        </td>
                                        <td style="width: 140px; text-align: center; height: 16px;">
                                            <dxe:ASPxHyperLink ID="hlEmailTemplates" runat="server" 
                                                NavigateUrl="~/Admin.aspx?s=EmailTemplates" Text="Email Templates">
                                            </dxe:ASPxHyperLink>
                                        </td>
                                        <td style="width: 140px; text-align: center; height: 16px;"></td>
                                        <td style="width: 140px; text-align: center; height: 16px;">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td style="width: 140px; text-align: center; height: 57px;">
                                            <a href="Admin.aspx?s=AnnualTargets">
                                            <dxe:ASPxImage ID="imgAnnualTargets" runat="server" 
                                                ImageUrl="~/Images/adminLogos/annualTargets.png" IsPng="True">
                                            </dxe:ASPxImage></a>
                                        </td>
                                        <td style="width: 140px; text-align: center; height: 57px;">
                                            <a href="Admin.aspx?s=ConfigText">
                                            <dxe:ASPxImage ID="imgConfigText" runat="server" 
                                                ImageUrl="~/Images/adminLogos/configurationText.png" IsPng="True">
                                            </dxe:ASPxImage></a>
                                        </td>
                                        <td style="width: 140px; text-align: center; height: 57px;">
                                            <a href="Admin.aspx?s=Config">
                                            <dxe:ASPxImage ID="imgConfig" runat="server" 
                                                ImageUrl="~/Images/adminLogos/configuration.png">
                                            </dxe:ASPxImage></a>
                                        </td>
                                        <td style="width: 140px; text-align: center; height: 57px;">
                                            <a href="Admin.aspx?s=EventLog">
                                            <dxe:ASPxImage ID="imgEventLog" runat="server" 
                                                ImageUrl="~/Images/adminLogos/viewEventLog.png" IsPng="True">
                                            </dxe:ASPxImage></a>
                                        </td>
                                        <td style="width: 140px; text-align: center; height: 57px;">&nbsp;</td>
                                        <td style="width: 140px; text-align: center; height: 57px;">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td style="width: 140px; text-align: center; height: 13px;">
                                            <dxe:ASPxHyperLink ID="hlAnnualTargets" runat="server" 
                                                NavigateUrl="~/Admin.aspx?s=AnnualTargets" Text="Annual Targets">
                                            </dxe:ASPxHyperLink>
                                        </td>
                                        <td style="width: 140px; text-align: center; height: 13px;">
                                            <dxe:ASPxHyperLink ID="hlConfigText" runat="server" 
                                                NavigateUrl="~/Admin.aspx?s=ConfigText" Text="Configuration Text">
                                            </dxe:ASPxHyperLink>
                                        </td>
                                        <td style="width: 140px; text-align: center; height: 13px;">
                                            <dxe:ASPxHyperLink ID="hlConfiguration" runat="server" 
                                                NavigateUrl="~/Admin.aspx?s=Config" Text="Configuration">
                                            </dxe:ASPxHyperLink>
                                        </td>
                                        <td style="width: 140px; text-align: center; height: 13px;">
                                            <dxe:ASPxHyperLink ID="hlEventLog" runat="server" 
                                                NavigateUrl="~/Admin.aspx?s=EventLog" Text="View Event Log">
                                            </dxe:ASPxHyperLink>
                                        </td>
                                        <td style="width: 140px; text-align: center; height: 13px;">&nbsp;</td>
                                        <td style="width: 140px; text-align: center; height: 13px;">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td style="width: 140px; padding-top: 24px; text-align: center">
                                            <a href="Admin.aspx?s=UsersBulkChange">
                                            <dxe:ASPxImage ID="imgUsersBulkChange" runat="server" 
                                                ImageUrl="~/Images/adminLogos/UsersBulkChange.png" IsPng="True">
                                            </dxe:ASPxImage></a>
                                        </td>
                                        <td style="width: 140px; padding-top: 24px; height: 16px; text-align: center">
                                            <a href="Admin.aspx?s=KPI">
                                            <dxe:ASPxImage ID="imgKpiFixer" runat="server" 
                                                ImageUrl="~/Images/adminLogos/kpiFixer.png" IsPng="True">
                                            </dxe:ASPxImage></a>
                                        </td>
                                        <td style="width: 140px; height: 16px; text-align: center; padding-top: 17px">
                                            <a href="Admin.aspx?s=MedicalTS">
                                            <dxe:ASPxImage ID="imgMoveSchedules" runat="server" 
                                                ImageUrl="~/Images/adminLogos/moveSchedules.png" IsPng="True">
                                            </dxe:ASPxImage></a>
                                        </td>
                                        <td style="width: 140px; height: 16px; text-align: center">
                                            <a href="Admin.aspx?s=RoboCopy">
                                            <dxe:ASPxImage ID="imgRoboCopy" runat="server"
                                                ImageUrl="~/Images/adminLogos/robocopy.png" IsPng="True">
                                            </dxe:ASPxImage></a>
                                        </td>
                                        <td style="width: 140px; height: 16px; text-align: center"></td>
                                        <td style="width: 140px; height: 16px; text-align: center"></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 140px; height: 16px; text-align: center">
                                            <dxe:ASPxHyperLink ID="hlUsersBulkChange" runat="server" 
                                                NavigateUrl="~/Admin.aspx?s=UsersBulkChange" Text="Bulk User Change Script">
                                            </dxe:ASPxHyperLink>
                                        </td>
                                        <td style="width: 140px; height: 16px; text-align: center">
                                            <dxe:ASPxHyperLink ID="hlKpiFixer" runat="server" NavigateUrl="~/Admin.aspx?s=KPI" 
                                                Text="KPI Fixer Tool">
                                            </dxe:ASPxHyperLink>
                                        </td>
                                        <td style="width: 140px; height: 16px; text-align: center;">
                                            <dxe:ASPxHyperLink ID="hlMedicalTS" runat="server" 
                                                NavigateUrl="~/Admin.aspx?s=MedicalTS" 
                                                Text="Move Schedules Between Training/Medical Lists">
                                            </dxe:ASPxHyperLink>
                                        </td>
                                        <td style="width: 140px; height: 16px; text-align: center">
                                            <dxe:ASPxHyperLink ID="hlRoboCopy" runat="server" 
                                                NavigateUrl="~/Admin.aspx?s=RoboCopy" Text="RoboCopy">
                                            </dxe:ASPxHyperLink>
                                        </td>
                                        <td style="width: 140px; height: 16px; text-align: center"></td>
                                        <td style="width: 140px; height: 16px; text-align: center"></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 140px; padding-top: 24px; text-align: center">
                                           <%--//Added By Sayani sil for Iuem# 21--%>
                                             <a href="Admin.aspx?s=SafetyDashboardPlan">
                                            <dxe:ASPxImage ID="ASPxImage5" runat="server" 
                                                ImageUrl="~/Images/adminLogos/configuration.png">
                                            </dxe:ASPxImage></a>
                                            <%--End--%>
                                        </td>
                                        <td style="width: 140px; padding-top: 24px; height: 16px; text-align: center">
                                            <%--//Added By Sayani sil for Iuem# 21--%>
                                             <a href="Admin.aspx?s=SafetyDashboard_AuditScore">
                                            <dxe:ASPxImage ID="ASPxImage1" runat="server" 
                                                ImageUrl="~/Images/adminLogos/configuration.png">
                                            </dxe:ASPxImage></a>
                                            <%--End--%>
                                        </td>
                                        <td style="width: 140px; height: 16px; text-align: center; padding-top: 17px">
                                           <a href="Admin.aspx?s=UsersCompaniesMap1">
                                            <dxe:ASPxImage ID="ASPxImage2" runat="server" 
                                                ImageUrl="~/Images/AdminLogos/CompaniesHrMap.png" IsPng="True">
                                            </dxe:ASPxImage></a>
                                        </td>
                                        <td style="width: 140px; height: 16px; text-align: center">
                                           <a href="Admin.aspx?s=LinkManager">
                                            <dxe:ASPxImage ID="ASPxImage3" runat="server" 
                                                ImageUrl="~/Images/AdminLogos/Link.png" IsPng="True">
                                            </dxe:ASPxImage></a>                                            
                                        </td>
                                        <td style="width: 140px; height: 16px; text-align: center"></td>
                                        <td style="width: 140px; height: 16px; text-align: center"></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 140px; height: 16px; text-align: center">
                                        <%--//Added By Sayani sil for Iuem# 21--%>
                                            <dxe:ASPxHyperLink ID="hlDashboardPlan" runat="server" 
                                                NavigateUrl="~/Admin.aspx?s=SafetyDashboardPlan" Text="Safety Dashboard Plan">
                                            </dxe:ASPxHyperLink>
                                            <%--End--%>
                                        </td>
                                        <td style="width: 140px; height: 16px; text-align: center">
                                            <%--//Added By Sayani sil for Iuem# 21--%>
                                            <dxe:ASPxHyperLink ID="hlDashboardAuditScore" runat="server" 
                                                NavigateUrl="~/Admin.aspx?s=SafetyDashboard_AuditScore" Text="Safety Dashboard Audit Score">
                                            </dxe:ASPxHyperLink>
                                            <%--End--%>
                                        </td>
                                        <td style="width: 140px; height: 16px; text-align: center;">
                                           <dxe:ASPxHyperLink ID="hlUsersCompaniesMap" runat="server" 
                                                NavigateUrl="~/Admin.aspx?s=UsersCompaniesMap1" Text="User Companies Map" 
                                                Width="100px">
                                            </dxe:ASPxHyperLink>
                                        </td>
                                        <td style="width: 140px; height: 16px; text-align: center">
                                          <dxe:ASPxHyperLink ID="hlLinkManager" runat="server" 
                                                NavigateUrl="~/Admin.aspx?s=LinkManager" Text="Link Manager" 
                                                Width="100px">
                                            </dxe:ASPxHyperLink>                                            
                                        </td>
                                        <td style="width: 140px; height: 16px; text-align: center"></td>
                                        <td style="width: 140px; height: 16px; text-align: center"></td>
                                    </tr>
                                </table>
                                <br />
                                <br />
                                <br />
                                <br />
                            </dx:PanelContent>
                        </PanelCollection>
                    </dx:ASPxPanel>
                </dx:ContentControl>
            </ContentCollection>
        </dx:TabPage>
    </TabPages>
    <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
    </LoadingPanelImage>
    <LoadingPanelStyle ImageSpacing="6px">
    </LoadingPanelStyle>
    <ContentStyle>
        <Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px" />
    </ContentStyle>
</dx:ASPxPageControl>
