﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.QuestionnaireReportProgressProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[CLSCompliant(true)]
	[Designer(typeof(QuestionnaireReportProgressDataSourceDesigner))]
	public class QuestionnaireReportProgressDataSource : ReadOnlyDataSource<QuestionnaireReportProgress>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportProgressDataSource class.
		/// </summary>
		public QuestionnaireReportProgressDataSource() : base(new QuestionnaireReportProgressService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the QuestionnaireReportProgressDataSourceView used by the QuestionnaireReportProgressDataSource.
		/// </summary>
		protected QuestionnaireReportProgressDataSourceView QuestionnaireReportProgressView
		{
			get { return ( View as QuestionnaireReportProgressDataSourceView ); }
		}
		
		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the QuestionnaireReportProgressDataSourceView class that is to be
		/// used by the QuestionnaireReportProgressDataSource.
		/// </summary>
		/// <returns>An instance of the QuestionnaireReportProgressDataSourceView class.</returns>
		protected override BaseDataSourceView<QuestionnaireReportProgress, Object> GetNewDataSourceView()
		{
			return new QuestionnaireReportProgressDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the QuestionnaireReportProgressDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class QuestionnaireReportProgressDataSourceView : ReadOnlyDataSourceView<QuestionnaireReportProgress>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportProgressDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the QuestionnaireReportProgressDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public QuestionnaireReportProgressDataSourceView(QuestionnaireReportProgressDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal QuestionnaireReportProgressDataSource QuestionnaireReportProgressOwner
		{
			get { return Owner as QuestionnaireReportProgressDataSource; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal QuestionnaireReportProgressService QuestionnaireReportProgressProvider
		{
			get { return Provider as QuestionnaireReportProgressService; }
		}

		#endregion Properties
		
		#region Methods
		
		#endregion Methods
	}

	#region QuestionnaireReportProgressDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the QuestionnaireReportProgressDataSource class.
	/// </summary>
	public class QuestionnaireReportProgressDataSourceDesigner : ReadOnlyDataSourceDesigner<QuestionnaireReportProgress>
	{
	}

	#endregion QuestionnaireReportProgressDataSourceDesigner

	#region QuestionnaireReportProgressFilter

	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireReportProgress"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireReportProgressFilter : SqlFilter<QuestionnaireReportProgressColumn>
	{
	}

	#endregion QuestionnaireReportProgressFilter

	#region QuestionnaireReportProgressExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireReportProgress"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireReportProgressExpressionBuilder : SqlExpressionBuilder<QuestionnaireReportProgressColumn>
	{
	}
	
	#endregion QuestionnaireReportProgressExpressionBuilder		
}

