﻿#region Using directives

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="CurrentCompaniesEhsConsultantIdProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract class CurrentCompaniesEhsConsultantIdProviderBaseCore : EntityViewProviderBase<CurrentCompaniesEhsConsultantId>
	{
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions
		
		/*
		///<summary>
		/// Fill an VList&lt;CurrentCompaniesEhsConsultantId&gt; From a DataSet
		///</summary>
		/// <param name="dataSet">the DataSet</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList&lt;CurrentCompaniesEhsConsultantId&gt;"/></returns>
		protected static VList&lt;CurrentCompaniesEhsConsultantId&gt; Fill(DataSet dataSet, VList<CurrentCompaniesEhsConsultantId> rows, int start, int pagelen)
		{
			if (dataSet.Tables.Count == 1)
			{
				return Fill(dataSet.Tables[0], rows, start, pagelen);
			}
			else
			{
				return new VList<CurrentCompaniesEhsConsultantId>();
			}	
		}
		
		
		///<summary>
		/// Fill an VList&lt;CurrentCompaniesEhsConsultantId&gt; From a DataTable
		///</summary>
		/// <param name="dataTable">the DataTable that hold the data.</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList<CurrentCompaniesEhsConsultantId>"/></returns>
		protected static VList&lt;CurrentCompaniesEhsConsultantId&gt; Fill(DataTable dataTable, VList<CurrentCompaniesEhsConsultantId> rows, int start, int pagelen)
		{
			int recordnum = 0;
			
			System.Collections.IEnumerator dataRows =  dataTable.Rows.GetEnumerator();
			
			while (dataRows.MoveNext() && (pagelen != 0))
			{
				if(recordnum >= start)
				{
					DataRow row = (DataRow)dataRows.Current;
				
					CurrentCompaniesEhsConsultantId c = new CurrentCompaniesEhsConsultantId();
					c.EhsConsultantId = (Convert.IsDBNull(row["EHSConsultantId"]))?(int)0:(System.Int32?)row["EHSConsultantId"];
					c.UserFullName = (Convert.IsDBNull(row["UserFullName"]))?string.Empty:(System.String)row["UserFullName"];
					c.AcceptChanges();
					rows.Add(c);
					pagelen -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		*/	
						
		///<summary>
		/// Fill an <see cref="VList&lt;CurrentCompaniesEhsConsultantId&gt;"/> From a DataReader.
		///</summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pageLength">number of row.</param>
		///<returns>a <see cref="VList&lt;CurrentCompaniesEhsConsultantId&gt;"/></returns>
		protected VList<CurrentCompaniesEhsConsultantId> Fill(IDataReader reader, VList<CurrentCompaniesEhsConsultantId> rows, int start, int pageLength)
		{
			int recordnum = 0;
			while (reader.Read() && (pageLength != 0))
			{
				if(recordnum >= start)
				{
					CurrentCompaniesEhsConsultantId entity = null;
					if (DataRepository.Provider.UseEntityFactory)
					{
						entity = EntityManager.CreateViewEntity<CurrentCompaniesEhsConsultantId>("CurrentCompaniesEhsConsultantId",  DataRepository.Provider.EntityCreationalFactoryType); 
					}
					else
					{
						entity = new CurrentCompaniesEhsConsultantId();
					}
					
					entity.SuppressEntityEvents = true;

					entity.EhsConsultantId = (reader.IsDBNull(((int)CurrentCompaniesEhsConsultantIdColumn.EhsConsultantId)))?null:(System.Int32?)reader[((int)CurrentCompaniesEhsConsultantIdColumn.EhsConsultantId)];
					//entity.EhsConsultantId = (Convert.IsDBNull(reader["EHSConsultantId"]))?(int)0:(System.Int32?)reader["EHSConsultantId"];
					entity.UserFullName = (reader.IsDBNull(((int)CurrentCompaniesEhsConsultantIdColumn.UserFullName)))?null:(System.String)reader[((int)CurrentCompaniesEhsConsultantIdColumn.UserFullName)];
					//entity.UserFullName = (Convert.IsDBNull(reader["UserFullName"]))?string.Empty:(System.String)reader["UserFullName"];
					entity.AcceptChanges();
					entity.SuppressEntityEvents = false;
					
					rows.Add(entity);
					pageLength -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		
		
		/// <summary>
		/// Refreshes the <see cref="CurrentCompaniesEhsConsultantId"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="CurrentCompaniesEhsConsultantId"/> object to refresh.</param>
		protected void RefreshEntity(IDataReader reader, CurrentCompaniesEhsConsultantId entity)
		{
			reader.Read();
			entity.EhsConsultantId = (reader.IsDBNull(((int)CurrentCompaniesEhsConsultantIdColumn.EhsConsultantId)))?null:(System.Int32?)reader[((int)CurrentCompaniesEhsConsultantIdColumn.EhsConsultantId)];
			//entity.EhsConsultantId = (Convert.IsDBNull(reader["EHSConsultantId"]))?(int)0:(System.Int32?)reader["EHSConsultantId"];
			entity.UserFullName = (reader.IsDBNull(((int)CurrentCompaniesEhsConsultantIdColumn.UserFullName)))?null:(System.String)reader[((int)CurrentCompaniesEhsConsultantIdColumn.UserFullName)];
			//entity.UserFullName = (Convert.IsDBNull(reader["UserFullName"]))?string.Empty:(System.String)reader["UserFullName"];
			reader.Close();
	
			entity.AcceptChanges();
		}
		
		/*
		/// <summary>
		/// Refreshes the <see cref="CurrentCompaniesEhsConsultantId"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="CurrentCompaniesEhsConsultantId"/> object.</param>
		protected static void RefreshEntity(DataSet dataSet, CurrentCompaniesEhsConsultantId entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.EhsConsultantId = (Convert.IsDBNull(dataRow["EHSConsultantId"]))?(int)0:(System.Int32?)dataRow["EHSConsultantId"];
			entity.UserFullName = (Convert.IsDBNull(dataRow["UserFullName"]))?string.Empty:(System.String)dataRow["UserFullName"];
			entity.AcceptChanges();
		}
		*/
			
		#endregion Helper Functions
	}//end class

	#region CurrentCompaniesEhsConsultantIdFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CurrentCompaniesEhsConsultantId"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CurrentCompaniesEhsConsultantIdFilterBuilder : SqlFilterBuilder<CurrentCompaniesEhsConsultantIdColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CurrentCompaniesEhsConsultantIdFilterBuilder class.
		/// </summary>
		public CurrentCompaniesEhsConsultantIdFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the CurrentCompaniesEhsConsultantIdFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CurrentCompaniesEhsConsultantIdFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CurrentCompaniesEhsConsultantIdFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CurrentCompaniesEhsConsultantIdFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CurrentCompaniesEhsConsultantIdFilterBuilder

	#region CurrentCompaniesEhsConsultantIdParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CurrentCompaniesEhsConsultantId"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CurrentCompaniesEhsConsultantIdParameterBuilder : ParameterizedSqlFilterBuilder<CurrentCompaniesEhsConsultantIdColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CurrentCompaniesEhsConsultantIdParameterBuilder class.
		/// </summary>
		public CurrentCompaniesEhsConsultantIdParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the CurrentCompaniesEhsConsultantIdParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CurrentCompaniesEhsConsultantIdParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CurrentCompaniesEhsConsultantIdParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CurrentCompaniesEhsConsultantIdParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CurrentCompaniesEhsConsultantIdParameterBuilder
	
	#region CurrentCompaniesEhsConsultantIdSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CurrentCompaniesEhsConsultantId"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class CurrentCompaniesEhsConsultantIdSortBuilder : SqlSortBuilder<CurrentCompaniesEhsConsultantIdColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CurrentCompaniesEhsConsultantIdSqlSortBuilder class.
		/// </summary>
		public CurrentCompaniesEhsConsultantIdSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion CurrentCompaniesEhsConsultantIdSortBuilder

} // end namespace
