using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;

using KaiZen.CSMS.Data;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Services;


public partial class Common_LoadingScreen : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    { 
        Configuration configuration = new Configuration();
        string LoadingScreenRandomMessages = configuration.GetValue(ConfigList.LoadingScreenRandomMessages);
        if (LoadingScreenRandomMessages != "0")
        {
            ConfigTextService ctService = new ConfigTextService();
            DataSet ctDataset = ctService.GetRandom();
            foreach (DataRow dr in ctDataset.Tables[0].Rows)
            {
                Label1.Text = dr[0].ToString();
            }
        }
    }
}
