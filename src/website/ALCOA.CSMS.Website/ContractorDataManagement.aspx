﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/user.master" AutoEventWireup="true" CodeBehind="ContractorDataManagement.aspx.cs" Inherits="ALCOA.CSMS.Website.ContractorDataManagement" %>

<%@ Register src="UserControls/Main/ContractorDataManagement.ascx" tagname="ContractorDataManagement" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <%: System.Web.Optimization.Styles.Render("~/bundles/Office2003BlueOld_styles") %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
    <uc1:ContractorDataManagement ID="ContractorDataManagement1" runat="server" />
</asp:Content>
