﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/popup.master" AutoEventWireup="true" CodeBehind="FatigueMgtPopup.aspx.cs" Inherits="ALCOA.CSMS.Website.PopUps.FatigueMgtPopup" %>
<%@ Register assembly="DevExpress.Web.v14.1" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<dx:ASPxGridView width="100%" ID="gridHourWorked_ByMonthYear" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue" AutoGenerateColumns="False" Settings-ShowGroupPanel="true" DataSourceId="EbiHoursWorkedByClockId">
                    <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                        <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
                        </LoadingPanelOnStatusBar>
                        <LoadingPanel Url="~/App_Themes/Office2003Blue/GridView/Loading.gif">
                        </LoadingPanel>
                    </Images>
                    <ImagesFilterControl>
                        <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
                        </LoadingPanel>
                    </ImagesFilterControl>
                    <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue">
                        <Header ImageSpacing="5px" SortingImageSpacing="5px">
                        </Header>
                        <AlternatingRow Enabled="True">
                    </AlternatingRow>
                        <LoadingPanel ImageSpacing="10px">
                        </LoadingPanel>
                    </Styles>
                    <StylesEditors>
                        <ProgressBar Height="25px">
                        </ProgressBar>
                    </StylesEditors>
                    <Settings ShowFilterRow="true" ShowFilterBar ="Visible"/>
                    <Columns>
                        
                       <dx:GridViewDataDateColumn Caption="Date/Time In" FieldName="EntryDateTime" ReadOnly="True" VisibleIndex="0">
                                <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy HH:mm:ss">
                                </PropertiesDateEdit>
                        </dx:GridViewDataDateColumn>
                         <dx:GridViewDataDateColumn Caption="Date/Time Out" FieldName="ExitDateTime" ReadOnly="True" VisibleIndex="1">
                                <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy HH:mm:ss">
                                </PropertiesDateEdit>
                        </dx:GridViewDataDateColumn>
                        <dx:GridViewDataTextColumn FieldName="TotalTime" Caption="Total Time" VisibleIndex="2">
                        </dx:GridViewDataTextColumn>
                         
                          
                        <dx:GridViewDataTextColumn FieldName="ClockId" Caption="Clock ID" VisibleIndex="5">
                        </dx:GridViewDataTextColumn>
                        
                         <dx:GridViewDataTextColumn  FieldName="FullName" Caption="FullName" VisibleIndex="6">
                        </dx:GridViewDataTextColumn>                       
                        <dx:GridViewDataTextColumn  FieldName="CompanyName" Caption="Company Name" VisibleIndex="7">
                        </dx:GridViewDataTextColumn> 
                        <dx:GridViewDataComboBoxColumn FieldName="Site" VisibleIndex="8" Caption="Site">
                                                <PropertiesComboBox DataSourceID="SitenameEbiDS"  TextField="SiteNameEbi"
                                                     IncrementalFilteringMode="StartsWith">
                                                </PropertiesComboBox>
                                                <Settings SortMode="DisplayText"></Settings>
                                                <CellStyle HorizontalAlign="Left"></CellStyle>
                        </dx:GridViewDataComboBoxColumn>

                        <dx:GridViewDataCheckColumn FieldName="Doubt" Caption="Doubt" VisibleIndex="9">
                        </dx:GridViewDataCheckColumn>
                    </Columns>
                    <SettingsPager AllButton-Visible="true"></SettingsPager>
                        </dx:ASPxGridView>
                        <br />
                        
                        <asp:SqlDataSource ID="EbiHoursWorkedByClockId" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
                        SelectCommand="EbiHoursWorked_GetByClockId" SelectCommandType="StoredProcedure">
                        <SelectParameters>
        <asp:Parameter Name="ClockId" Type="Int32" />
        </SelectParameters>
 </asp:SqlDataSource>
 <asp:SqlDataSource ID="SitenameEbiDS" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand=" Select Distinct SiteNameEbi from Sites where SiteNameEbi Is Not Null order by SiteNameEbi ">
</asp:SqlDataSource>
</asp:Content>
