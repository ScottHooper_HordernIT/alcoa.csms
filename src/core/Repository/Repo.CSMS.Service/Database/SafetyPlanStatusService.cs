﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface ISafetyPlanStatusService
    {
        SafetyPlanStatus Get(Expression<Func<SafetyPlanStatus, bool>> where, List<Expression<Func<SafetyPlanStatus, object>>> includes);
        List<SafetyPlanStatus> GetMany(int? top, Expression<Func<SafetyPlanStatus, bool>> where, Func<IQueryable<SafetyPlanStatus>, IOrderedQueryable<SafetyPlanStatus>> orderBy, List<Expression<Func<SafetyPlanStatus, object>>> includes);
        List<SafetyPlanStatus> GetManyPaged(int pageNumber, int pageSize, Expression<Func<SafetyPlanStatus, bool>> where, Func<IQueryable<SafetyPlanStatus>, IOrderedQueryable<SafetyPlanStatus>> orderBy, List<Expression<Func<SafetyPlanStatus, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<SafetyPlanStatus, bool>> where, List<Expression<Func<SafetyPlanStatus, object>>> includes);
        void Insert(SafetyPlanStatus item, bool saveChanges = true);
        void Update(SafetyPlanStatus item, bool saveChanges = true);
        void Delete(SafetyPlanStatus item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class SafetyPlanStatusService : ISafetyPlanStatusService
    {
        private readonly IRepository<SafetyPlanStatus> _repository;

        public SafetyPlanStatusService(IRepository<SafetyPlanStatus> repository)
        {
            this._repository = repository;
        }

        public SafetyPlanStatus Get(Expression<Func<SafetyPlanStatus, bool>> where, List<Expression<Func<SafetyPlanStatus, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<SafetyPlanStatus> GetMany(int? top, Expression<Func<SafetyPlanStatus, bool>> where, Func<IQueryable<SafetyPlanStatus>, IOrderedQueryable<SafetyPlanStatus>> orderBy, List<Expression<Func<SafetyPlanStatus, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<SafetyPlanStatus> GetManyPaged(int pageNumber, int pageSize, Expression<Func<SafetyPlanStatus, bool>> where, Func<IQueryable<SafetyPlanStatus>, IOrderedQueryable<SafetyPlanStatus>> orderBy, List<Expression<Func<SafetyPlanStatus, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<SafetyPlanStatus, bool>> where, List<Expression<Func<SafetyPlanStatus, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(SafetyPlanStatus item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("SafetyPlanStatus");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(SafetyPlanStatus item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("SafetyPlanStatus");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(SafetyPlanStatus item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("SafetyPlanStatus");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
