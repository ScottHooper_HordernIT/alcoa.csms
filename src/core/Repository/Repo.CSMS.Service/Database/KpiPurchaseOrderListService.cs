﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface IKpiPurchaseOrderListService
    {
        KpiPurchaseOrderList Get(Expression<Func<KpiPurchaseOrderList, bool>> where, List<Expression<Func<KpiPurchaseOrderList, object>>> includes);
        List<KpiPurchaseOrderList> GetMany(int? top, Expression<Func<KpiPurchaseOrderList, bool>> where, Func<IQueryable<KpiPurchaseOrderList>, IOrderedQueryable<KpiPurchaseOrderList>> orderBy, List<Expression<Func<KpiPurchaseOrderList, object>>> includes);
        List<KpiPurchaseOrderList> GetManyPaged(int pageNumber, int pageSize, Expression<Func<KpiPurchaseOrderList, bool>> where, Func<IQueryable<KpiPurchaseOrderList>, IOrderedQueryable<KpiPurchaseOrderList>> orderBy, List<Expression<Func<KpiPurchaseOrderList, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<KpiPurchaseOrderList, bool>> where, List<Expression<Func<KpiPurchaseOrderList, object>>> includes);
        void Insert(KpiPurchaseOrderList item, bool saveChanges = true);
        void Update(KpiPurchaseOrderList item, bool saveChanges = true);
        void Delete(KpiPurchaseOrderList item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class KpiPurchaseOrderListService : IKpiPurchaseOrderListService
    {
        private readonly IRepository<KpiPurchaseOrderList> _repository;

        public KpiPurchaseOrderListService(IRepository<KpiPurchaseOrderList> repository)
        {
            this._repository = repository;
        }

        public KpiPurchaseOrderList Get(Expression<Func<KpiPurchaseOrderList, bool>> where, List<Expression<Func<KpiPurchaseOrderList, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<KpiPurchaseOrderList> GetMany(int? top, Expression<Func<KpiPurchaseOrderList, bool>> where, Func<IQueryable<KpiPurchaseOrderList>, IOrderedQueryable<KpiPurchaseOrderList>> orderBy, List<Expression<Func<KpiPurchaseOrderList, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<KpiPurchaseOrderList> GetManyPaged(int pageNumber, int pageSize, Expression<Func<KpiPurchaseOrderList, bool>> where, Func<IQueryable<KpiPurchaseOrderList>, IOrderedQueryable<KpiPurchaseOrderList>> orderBy, List<Expression<Func<KpiPurchaseOrderList, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<KpiPurchaseOrderList, bool>> where, List<Expression<Func<KpiPurchaseOrderList, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(KpiPurchaseOrderList item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("KpiPurchaseOrderList");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(KpiPurchaseOrderList item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("KpiPurchaseOrderList");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(KpiPurchaseOrderList item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("KpiPurchaseOrderList");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
