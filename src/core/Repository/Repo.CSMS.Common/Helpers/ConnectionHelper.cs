﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Configuration;
using System.Data.Common;
using System.Data.SqlClient;

namespace Repo.CSMS.Common.Helpers
{
    public class ConnectionHelper
    {
        private static string GetConnectionString()
        {
            return ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
        }

        public static string GetCatalogName()
        {
            SqlConnection cn = new SqlConnection(GetConnectionString());
            return cn.Database;
        }

        public static string GetServerName()
        {
            SqlConnection cn = new SqlConnection(GetConnectionString());
            return cn.DataSource;
        }
    }
}
