﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Library_APSS_Top9" Codebehind="APSS_Top9.ascx.cs" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>

<table border="0" cellpadding="2" cellspacing="0" width="900">
    <tr>
        <td class="pageName" colspan="7">
            <span class="title">Library Documents<asp:Button ID="btnEdit" runat="server" BackColor="White"
                BorderColor="White" BorderStyle="None" Font-Names="Verdana" Font-Size="XX-Small"
                Font-Underline="True" ForeColor="Red" OnClick="btnEdit_Click" Text="(Edit)" /></span><br />
            <span class="date">Categories:</span> Must Read these...,
            <asp:LinkButton ID="LinkButton1" runat="server" PostBackUrl="~/APSS_MFU.aspx">Most Frequently Used</asp:LinkButton>,
            <asp:LinkButton ID="LinkButton2" runat="server" PostBackUrl="~/APSS_Search.aspx">Search</asp:LinkButton>.<br />
            <img height="1" src="images/grfc_dottedline.gif" width="24" /><br /></td>
        <td class="pageName" colspan="1">
        </td>
    </tr>
    <tr>
        <td colspan="8" style="height: 110px">
            <dxwgv:ASPxGridView ID="grid" runat="server" AutoGenerateColumns="False" ClientInstanceName="grid"
                CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
                CssPostfix="Office2003Blue" OnRowInserting="grid_OnRowInserting" OnHtmlRowCreated="grid_RowCreated"
                OnInitNewRow="grid_InitNewRow" OnRowUpdating="grid_RowUpdating" 
                DataSourceID="DocumentsDataSource" KeyFieldName="DocumentId">
                <Columns>
                <dxwgv:GridViewCommandColumn Caption="Action" Visible="False" VisibleIndex="0" Width="50px">
                            <EditButton Visible="True">
                            </EditButton>
                            <NewButton Visible="True">
                            </NewButton>
                            <DeleteButton Visible="True">
                            </DeleteButton>
                            <ClearFilterButton Visible="True">
                            </ClearFilterButton>
                        </dxwgv:GridViewCommandColumn>
                        <dxwgv:GridViewDataTextColumn FieldName="DocumentId" ReadOnly="True" Visible="False"
                            VisibleIndex="0">
                            <EditFormSettings Visible="False" />
                            <CellStyle>
                                <BackgroundImage ImageUrl="~/Images/ArticleIcon.gif" Repeat="NoRepeat" />
                            </CellStyle>
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn ReadOnly="True" VisibleIndex="0" Width="8px">
                            <EditFormSettings Visible="False" />
                            <CellStyle>
                                <Paddings Padding="5px" />
                                <BackgroundImage ImageUrl="~/Images/ArticleIcon.gif" Repeat="NoRepeat" />
                            </CellStyle>
                        </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn FieldName="DocumentName" Caption="Document Name" VisibleIndex="2" Width="600px" SortIndex="0" SortOrder="Ascending">
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataHyperLinkColumn FieldName="DocumentFileName" Caption="File Name" VisibleIndex="1" Width="100px" ToolTip="Click to download file.">
                         <DataItemTemplate>
                             <table cellpadding="0" cellspacing="0"><tr>
                             <td style="width:120px">
                                 <dxe:ASPxHyperLink ID="hlFileDownload" runat="server" Text="">
                                 </dxe:ASPxHyperLink>
                             </td>
                             </tr></table>
                        </DataItemTemplate>
                        </dxwgv:GridViewDataHyperLinkColumn>
                    <dxwgv:GridViewDataTextColumn FieldName="DocumentFileDescription" Visible="False" VisibleIndex="3">
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn FieldName="DocumentType" Visible="False" VisibleIndex="3">
                        <EditFormSettings Visible="False" />
                    </dxwgv:GridViewDataTextColumn>
                     <dxwgv:GridViewDataTextColumn Name="Read" Caption="Read" FieldName="DocumentFileName" ReadOnly="True"
                 VisibleIndex="3" Width="120px">
                 <DataItemTemplate>
                     <table cellpadding="0" cellspacing="0"><tr>
                     <td style="width:120px"><asp:Label ID="lblRead" runat="server" Text="" ></asp:Label></td>
                     </tr></table>
                 </DataItemTemplate>
                 <HeaderStyle HorizontalAlign="Center" />
                        <CellStyle HorizontalAlign="Center">
                        </CellStyle>
                         <EditFormSettings Visible="False" />
             </dxwgv:GridViewDataTextColumn>
             
                    <dxwgv:GridViewDataComboBoxColumn Caption="Relevant To" FieldName="RegionId" VisibleIndex="4" Width="220px">
                        <PropertiesComboBox DataSourceID="RegionsDataSource" DropDownHeight="150px" TextField="RegionName"
                            ValueField="RegionId" ValueType="System.Int32">
                        </PropertiesComboBox>
                        <EditFormSettings Visible="False" />
                    </dxwgv:GridViewDataComboBoxColumn>
                    
                </Columns>
                <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                    <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
                    </LoadingPanelOnStatusBar>
                    <CollapsedButton Height="12px"
                        Width="11px" />
                    <ExpandedButton Height="12px"
                        Width="11px" />
                    <DetailCollapsedButton Height="12px"
                        Width="11px" />
                    <DetailExpandedButton Height="12px"
                        Width="11px" />
                    <LoadingPanel Url="~/App_Themes/Office2003BlueOld/GridView/Loading.gif"><%--Enhancement_023:change by debashis for testing platform upgradation--%>
                    </LoadingPanel>
                </Images>
                <ImagesFilterControl>
                    <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
                    </LoadingPanel>
                </ImagesFilterControl>
                <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
                    CssPostfix="Office2003Blue">
                    <Header ImageSpacing="5px" SortingImageSpacing="5px">
                    </Header>
                    <LoadingPanel ImageSpacing="10px">
                    </LoadingPanel>
                </Styles>
                <SettingsBehavior ConfirmDelete="True" />
            </dxwgv:ASPxGridView>
            &nbsp;
            
                <data:RegionsDataSource
                    ID="RegionsDataSource"
                    runat="server"
                    EnableDeepLoad="False"
                    EnableSorting="True"
                    SelectMethod="GetPaged">
                </data:RegionsDataSource>
                
                <data:DocumentsDataSource
                    ID="DocumentsDataSource"
                    runat="server"
                    EnableDeepLoad="False"
                    EnableSorting="true"
                    SelectMethod="GetPaged">
                    <Parameters>
                      <data:SqlParameter Name="WhereClause" UseParameterizedFilters="false">
                         <Filters>
                            <data:DocumentsFilter Column="DocumentType" DefaultValue="MR" SessionField="MR" />
                            <data:DocumentsFilter Column="RegionId" DefaultValue="" SessionField="Documents_Region" />
                         </Filters>
                      </data:SqlParameter>
                   </Parameters>
                </data:DocumentsDataSource>
                <br />
            </td>
    </tr>
</table>
<asp:SqlDataSource ID="sqldsReadCD" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="_DocumentsDownloadLog_MustReadDownloadedFiles_APSS" SelectCommandType="StoredProcedure">
    <SelectParameters>
        <asp:SessionParameter DefaultValue="0" Name="CompanyId" SessionField="spVar_CompanyId"
            Type="Int32" />
    </SelectParameters>
</asp:SqlDataSource>