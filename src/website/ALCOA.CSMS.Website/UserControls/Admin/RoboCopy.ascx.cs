using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Text.RegularExpressions;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Services;
using System.Globalization;
using System.Drawing;
using System.Security;
using System.Security.Principal;

public partial class UserControls_Admin_RoboCopy : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public void Stuff()
    {
        //IMPERSONATION!
        WindowsImpersonationContext impContext = null;
        try
        {
            impContext = Helper.ImpersonateWAOCSMUser();
        }
        catch (ApplicationException ex)
        {
            Trace.Write(ex.Message);
        }

        try
        {
            if (null != impContext)
            {
                using (impContext)
                {
                    try
                    {

                        string listMR = KaiZen.CSMS.Data.DataRepository.DocumentsProvider.GetByDocumentType("MR").ToString();
                        //string listMFU = KaiZen.CSMS.Data.DataRepository.DocumentsProvider.GetByDocumentType("MFU").ToString();
                        //string listTP = KaiZen.CSMS.Data.DataRepository.DocumentsProvider.GetByDocumentType("TP").ToString();
                        //string listMM = KaiZen.CSMS.Data.DataRepository.DocumentsProvider.GetByDocumentType("MM").ToString();
                        string listDocs = KaiZen.CSMS.Data.DataRepository.DocumentsProvider.GetAll().ToString();

                        ASPxMemo1.Text += "Starting...\n";

                        Configuration configuration = new Configuration();
                        //string[] fileEntries = Directory.GetFiles("C:\\WAOCSM");

                        string[] fileEntries = Directory.GetFiles(@configuration.GetValue(ConfigList.RoboCopyLogsDir).ToString());
                        foreach (string fileName in fileEntries)
                        {
                            if (fileName.Contains("WAOCSM_Sync"))
                            {
                                TextReader textr = new StreamReader(fileName);
                                string line;
                                int lineNumber = 0;

                                string TimeStampField = "  Started : ";
                                string TimeStampString = "";
                                DateTime TimeStamp = new DateTime();


                                string divider = "------------------------------------------------------------------------------";
                                int dividerSection = 0;

                                int filesLine = 0;
                                int entries = 0;
                                bool breakOut = false;

                                while ((line = textr.ReadLine()) != null && breakOut == false)
                                {
                                    if (line.Contains(TimeStampField))
                                    {
                                        TimeStampString = line.Replace(TimeStampField, "");
                                        TimeStamp = DateTime.ParseExact(TimeStampString,
                                                        "ddd MMM d HH:mm:ss yyyy",
                                                        new DateTimeFormatInfo());

                                        //check if TimeStamp already exists in database..
                                        int Count = 0;
                                        ApssLogsFilters queryApssLogs = new ApssLogsFilters();
                                        queryApssLogs.AppendEquals(ApssLogsColumn.TimeStamp, TimeStamp.ToString("MM/dd/yyyy HH:mm:ss"));
                                        //queryApssLogs.Append(ApssLogsColumn.TimeStamp, DateTime.Now.ToString("yyyy-MM-dd-HH:mm:ss"));
                                        TList<ApssLogs> apssLogsList = DataRepository.ApssLogsProvider.GetPaged(queryApssLogs.ToString(), null, 0, 100, out Count);
                                        if (Count > 0)
                                        {
                                            ASPxMemo1.Text += "Ignoring File: " + fileName +
                                                            " with TimeStamp: " + TimeStampString + " as it already exists in the Database.\n";
                                            breakOut = true;
                                            break;
                                        }
                                        else
                                        {
                                            ASPxMemo1.Text += "Reading File: " + fileName +
                                                            " with TimeStamp: " + TimeStampString + " as it does not exist in the Database.\n";
                                        }

                                    }
                                    if (line.Contains(divider) == true)
                                    {
                                        dividerSection++;
                                    }
                                    else
                                    {
                                        switch (dividerSection)
                                        {
                                            case 3:
                                                //begin filechanges log
                                                filesLine++;
                                                if ((filesLine >= 3) && line.Length > 0)
                                                {
                                                    string[] ChangedFiles = line.Split('\t');
                                                    string ApssfileTag = TrimString(ChangedFiles[1].ToString());
                                                    string ApssfileName = TrimString(ChangedFiles[4].ToString()).ToLower();
                                                    ApssLogs apssLogsEntry = new ApssLogs();
                                                    apssLogsEntry.FileName = ApssfileName;
                                                    apssLogsEntry.FileTag = ApssfileTag;
                                                    apssLogsEntry.TimeStamp = TimeStamp;

                                                    apssLogsEntry.FileType = "";

                                                    if (listMR.Contains(ApssfileName))
                                                    { apssLogsEntry.FileType = "Must Read"; }
                                                    if (listMR.Contains(ApssfileName))
                                                    { apssLogsEntry.FileType = "Most Frequently Read"; }
                                                    if (listMR.Contains(ApssfileName))
                                                    { apssLogsEntry.FileType = "Training Packages"; }
                                                    if (listMR.Contains(ApssfileName))
                                                    { apssLogsEntry.FileType = "Mines Medical"; }

                                                    if (!String.IsNullOrEmpty(apssLogsEntry.FileType))
                                                    {
                                                        if (apssLogsEntry.FileType == "Must Read")
                                                        {
                                                            ApssLogsService apssLogsService = new ApssLogsService();
                                                            apssLogsService.Insert(apssLogsEntry);
                                                            ASPxMemo1.Text += "Adding Entry... TimeStamp: " + TimeStampString +
                                                                " FileName: " + ApssfileName + " FileTag: " + ApssfileTag + "\n";
                                                        }
                                                        else
                                                        {
                                                            if (TrimString(ApssfileTag).ToLower() == "newer")
                                                            {
                                                                ASPxMemo1.Text += "Ignoring Entry... TimeStamp: " +
                                                                        TimeStampString + " FileName: " + ApssfileName
                                                                        + " FileTag: " + ApssfileTag
                                                                        + " because we are told to ignore file tags of 'Older/Newer (Non MR/MM/TP/MFU Type)'.\n";
                                                            }
                                                            else
                                                            {
                                                                if (TrimString(ApssfileTag) == "Older")
                                                                {
                                                                    ASPxMemo1.Text += "Ignoring Entry... TimeStamp: " +
                                                                            TimeStampString + " FileName: " + ApssfileName
                                                                            + " FileTag: " + ApssfileTag
                                                                            + " because we are told to ignore file tags of 'Older/Newer (Non MR/MM/TP/MFU Type)'.\n";
                                                                }
                                                                else
                                                                {
                                                                    //Add to Database
                                                                    ApssLogsService apssLogsService = new ApssLogsService();
                                                                    apssLogsService.Insert(apssLogsEntry);
                                                                    ASPxMemo1.Text += "Adding Entry... TimeStamp: "
                                                                        + TimeStampString
                                                                        + " FileName: " + ApssfileName + " FileTag: "
                                                                        + ApssfileTag + "\n";
                                                                }
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        ASPxMemo1.Text += "Ignoring Entry... TimeStamp: " +
                                                                        TimeStampString + " FileName: " + ApssfileName
                                                                        + " FileTag: " + ApssfileTag
                                                                        + " because we are told to ignore files which are not of MR/TP/MM/MFU Type.\n";
                                                    }
                                                    entries++;
                                                }

                                                break;
                                            case 4:
                                                //end filechanges log
                                                break;
                                            default:
                                                break;
                                        }
                                    }
                                    lineNumber++;
                                }
                                textr.Close();
                            }
                        }
                        ASPxMemo1.Text += "Finished.\n";
                    }
                    catch (Exception ex)
                    {
                        Elmah.ErrorSignal.FromContext(Context).Raise(ex);
                        lblStatus.Text = "Error: " + ex.Message.ToString();
                    }
                }
            }
        }
        catch (Exception ex)
        {
            Elmah.ErrorSignal.FromContext(Context).Raise(ex);
            lblStatus.Text = "Error: " + ex.Message.ToString();
        }
        finally
        {
            if (impContext != null) impContext.Undo();
        }
    }


    /// <summary>
    /// Method for removing trailing and leadingt whitespace from a string
    /// </summary>
    /// <param name="str">The string to trim</param>
    /// <returns></returns>
    public string TrimString(string str)
    {
        try
        {
            string pattern = @"^[ \t]+|[ \t]+$";
            Regex reg = new Regex(pattern, RegexOptions.IgnoreCase);
            str = reg.Replace(str, "");
            return str;
        }
        catch (Exception ex)
        {
            Elmah.ErrorSignal.FromContext(Context).Raise(ex);
            throw ex;
        }
    }

    protected void cbShowDebug_CheckedChanged(object sender, EventArgs e)
    {
        switch (cbShowDebug.Checked)
        {
            case true:
                ASPxMemo1.Visible = true;
                break;
            case false:
                ASPxMemo1.Visible = false;
                break;
            default:
                break;
        }
    }
    protected void ASPxButton1_Click(object sender, EventArgs e)
    {
        //System.Threading.Thread.Sleep(1000);
        try
        {
            Stuff();
            ApssLogsDataSource.DataBind();
            ASPxGridView1.DataBind();
            lblStatus.Text = "Completed successfully.";
            lblStatus.ForeColor = Color.Green;
        }
        catch (Exception ex)
        {
            Elmah.ErrorSignal.FromContext(Context).Raise(ex);
            lblStatus.Text = "An error occured during processing... (something broke)";
            lblStatus.ForeColor = Color.Red;
        }
    }
}
