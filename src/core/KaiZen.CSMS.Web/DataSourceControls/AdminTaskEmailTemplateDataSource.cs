﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.AdminTaskEmailTemplateProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(AdminTaskEmailTemplateDataSourceDesigner))]
	public class AdminTaskEmailTemplateDataSource : ProviderDataSource<AdminTaskEmailTemplate, AdminTaskEmailTemplateKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailTemplateDataSource class.
		/// </summary>
		public AdminTaskEmailTemplateDataSource() : base(new AdminTaskEmailTemplateService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the AdminTaskEmailTemplateDataSourceView used by the AdminTaskEmailTemplateDataSource.
		/// </summary>
		protected AdminTaskEmailTemplateDataSourceView AdminTaskEmailTemplateView
		{
			get { return ( View as AdminTaskEmailTemplateDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the AdminTaskEmailTemplateDataSource control invokes to retrieve data.
		/// </summary>
		public AdminTaskEmailTemplateSelectMethod SelectMethod
		{
			get
			{
				AdminTaskEmailTemplateSelectMethod selectMethod = AdminTaskEmailTemplateSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (AdminTaskEmailTemplateSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the AdminTaskEmailTemplateDataSourceView class that is to be
		/// used by the AdminTaskEmailTemplateDataSource.
		/// </summary>
		/// <returns>An instance of the AdminTaskEmailTemplateDataSourceView class.</returns>
		protected override BaseDataSourceView<AdminTaskEmailTemplate, AdminTaskEmailTemplateKey> GetNewDataSourceView()
		{
			return new AdminTaskEmailTemplateDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the AdminTaskEmailTemplateDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class AdminTaskEmailTemplateDataSourceView : ProviderDataSourceView<AdminTaskEmailTemplate, AdminTaskEmailTemplateKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailTemplateDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the AdminTaskEmailTemplateDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public AdminTaskEmailTemplateDataSourceView(AdminTaskEmailTemplateDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal AdminTaskEmailTemplateDataSource AdminTaskEmailTemplateOwner
		{
			get { return Owner as AdminTaskEmailTemplateDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal AdminTaskEmailTemplateSelectMethod SelectMethod
		{
			get { return AdminTaskEmailTemplateOwner.SelectMethod; }
			set { AdminTaskEmailTemplateOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal AdminTaskEmailTemplateService AdminTaskEmailTemplateProvider
		{
			get { return Provider as AdminTaskEmailTemplateService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<AdminTaskEmailTemplate> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<AdminTaskEmailTemplate> results = null;
			AdminTaskEmailTemplate item;
			count = 0;
			
			System.Int32 _adminTaskEmailTemplateId;
			System.Int32 _adminTaskTypeId;

			switch ( SelectMethod )
			{
				case AdminTaskEmailTemplateSelectMethod.Get:
					AdminTaskEmailTemplateKey entityKey  = new AdminTaskEmailTemplateKey();
					entityKey.Load(values);
					item = AdminTaskEmailTemplateProvider.Get(entityKey);
					results = new TList<AdminTaskEmailTemplate>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case AdminTaskEmailTemplateSelectMethod.GetAll:
                    results = AdminTaskEmailTemplateProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case AdminTaskEmailTemplateSelectMethod.GetPaged:
					results = AdminTaskEmailTemplateProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case AdminTaskEmailTemplateSelectMethod.Find:
					if ( FilterParameters != null )
						results = AdminTaskEmailTemplateProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = AdminTaskEmailTemplateProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case AdminTaskEmailTemplateSelectMethod.GetByAdminTaskEmailTemplateId:
					_adminTaskEmailTemplateId = ( values["AdminTaskEmailTemplateId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["AdminTaskEmailTemplateId"], typeof(System.Int32)) : (int)0;
					item = AdminTaskEmailTemplateProvider.GetByAdminTaskEmailTemplateId(_adminTaskEmailTemplateId);
					results = new TList<AdminTaskEmailTemplate>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				case AdminTaskEmailTemplateSelectMethod.GetByAdminTaskTypeId:
					_adminTaskTypeId = ( values["AdminTaskTypeId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["AdminTaskTypeId"], typeof(System.Int32)) : (int)0;
					item = AdminTaskEmailTemplateProvider.GetByAdminTaskTypeId(_adminTaskTypeId);
					results = new TList<AdminTaskEmailTemplate>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// FK
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == AdminTaskEmailTemplateSelectMethod.Get || SelectMethod == AdminTaskEmailTemplateSelectMethod.GetByAdminTaskEmailTemplateId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				AdminTaskEmailTemplate entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					AdminTaskEmailTemplateProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<AdminTaskEmailTemplate> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			AdminTaskEmailTemplateProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region AdminTaskEmailTemplateDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the AdminTaskEmailTemplateDataSource class.
	/// </summary>
	public class AdminTaskEmailTemplateDataSourceDesigner : ProviderDataSourceDesigner<AdminTaskEmailTemplate, AdminTaskEmailTemplateKey>
	{
		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailTemplateDataSourceDesigner class.
		/// </summary>
		public AdminTaskEmailTemplateDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public AdminTaskEmailTemplateSelectMethod SelectMethod
		{
			get { return ((AdminTaskEmailTemplateDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new AdminTaskEmailTemplateDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region AdminTaskEmailTemplateDataSourceActionList

	/// <summary>
	/// Supports the AdminTaskEmailTemplateDataSourceDesigner class.
	/// </summary>
	internal class AdminTaskEmailTemplateDataSourceActionList : DesignerActionList
	{
		private AdminTaskEmailTemplateDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailTemplateDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public AdminTaskEmailTemplateDataSourceActionList(AdminTaskEmailTemplateDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public AdminTaskEmailTemplateSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion AdminTaskEmailTemplateDataSourceActionList
	
	#endregion AdminTaskEmailTemplateDataSourceDesigner
	
	#region AdminTaskEmailTemplateSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the AdminTaskEmailTemplateDataSource.SelectMethod property.
	/// </summary>
	public enum AdminTaskEmailTemplateSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByAdminTaskEmailTemplateId method.
		/// </summary>
		GetByAdminTaskEmailTemplateId,
		/// <summary>
		/// Represents the GetByAdminTaskTypeId method.
		/// </summary>
		GetByAdminTaskTypeId
	}
	
	#endregion AdminTaskEmailTemplateSelectMethod

	#region AdminTaskEmailTemplateFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AdminTaskEmailTemplate"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AdminTaskEmailTemplateFilter : SqlFilter<AdminTaskEmailTemplateColumn>
	{
	}
	
	#endregion AdminTaskEmailTemplateFilter

	#region AdminTaskEmailTemplateExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AdminTaskEmailTemplate"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AdminTaskEmailTemplateExpressionBuilder : SqlExpressionBuilder<AdminTaskEmailTemplateColumn>
	{
	}
	
	#endregion AdminTaskEmailTemplateExpressionBuilder	

	#region AdminTaskEmailTemplateProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;AdminTaskEmailTemplateChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="AdminTaskEmailTemplate"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AdminTaskEmailTemplateProperty : ChildEntityProperty<AdminTaskEmailTemplateChildEntityTypes>
	{
	}
	
	#endregion AdminTaskEmailTemplateProperty
}

