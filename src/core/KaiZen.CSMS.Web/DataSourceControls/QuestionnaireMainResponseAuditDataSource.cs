﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.QuestionnaireMainResponseAuditProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(QuestionnaireMainResponseAuditDataSourceDesigner))]
	public class QuestionnaireMainResponseAuditDataSource : ProviderDataSource<QuestionnaireMainResponseAudit, QuestionnaireMainResponseAuditKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainResponseAuditDataSource class.
		/// </summary>
		public QuestionnaireMainResponseAuditDataSource() : base(new QuestionnaireMainResponseAuditService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the QuestionnaireMainResponseAuditDataSourceView used by the QuestionnaireMainResponseAuditDataSource.
		/// </summary>
		protected QuestionnaireMainResponseAuditDataSourceView QuestionnaireMainResponseAuditView
		{
			get { return ( View as QuestionnaireMainResponseAuditDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the QuestionnaireMainResponseAuditDataSource control invokes to retrieve data.
		/// </summary>
		public QuestionnaireMainResponseAuditSelectMethod SelectMethod
		{
			get
			{
				QuestionnaireMainResponseAuditSelectMethod selectMethod = QuestionnaireMainResponseAuditSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (QuestionnaireMainResponseAuditSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the QuestionnaireMainResponseAuditDataSourceView class that is to be
		/// used by the QuestionnaireMainResponseAuditDataSource.
		/// </summary>
		/// <returns>An instance of the QuestionnaireMainResponseAuditDataSourceView class.</returns>
		protected override BaseDataSourceView<QuestionnaireMainResponseAudit, QuestionnaireMainResponseAuditKey> GetNewDataSourceView()
		{
			return new QuestionnaireMainResponseAuditDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the QuestionnaireMainResponseAuditDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class QuestionnaireMainResponseAuditDataSourceView : ProviderDataSourceView<QuestionnaireMainResponseAudit, QuestionnaireMainResponseAuditKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainResponseAuditDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the QuestionnaireMainResponseAuditDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public QuestionnaireMainResponseAuditDataSourceView(QuestionnaireMainResponseAuditDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal QuestionnaireMainResponseAuditDataSource QuestionnaireMainResponseAuditOwner
		{
			get { return Owner as QuestionnaireMainResponseAuditDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal QuestionnaireMainResponseAuditSelectMethod SelectMethod
		{
			get { return QuestionnaireMainResponseAuditOwner.SelectMethod; }
			set { QuestionnaireMainResponseAuditOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal QuestionnaireMainResponseAuditService QuestionnaireMainResponseAuditProvider
		{
			get { return Provider as QuestionnaireMainResponseAuditService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<QuestionnaireMainResponseAudit> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<QuestionnaireMainResponseAudit> results = null;
			QuestionnaireMainResponseAudit item;
			count = 0;
			
			System.Int32 _auditId;
			System.Int32? _questionnaireId_nullable;
			System.Int32? _answerId_nullable;
			System.Int32? _modifiedByUserId_nullable;
			System.Int32? _responseId_nullable;

			switch ( SelectMethod )
			{
				case QuestionnaireMainResponseAuditSelectMethod.Get:
					QuestionnaireMainResponseAuditKey entityKey  = new QuestionnaireMainResponseAuditKey();
					entityKey.Load(values);
					item = QuestionnaireMainResponseAuditProvider.Get(entityKey);
					results = new TList<QuestionnaireMainResponseAudit>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case QuestionnaireMainResponseAuditSelectMethod.GetAll:
                    results = QuestionnaireMainResponseAuditProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case QuestionnaireMainResponseAuditSelectMethod.GetPaged:
					results = QuestionnaireMainResponseAuditProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case QuestionnaireMainResponseAuditSelectMethod.Find:
					if ( FilterParameters != null )
						results = QuestionnaireMainResponseAuditProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = QuestionnaireMainResponseAuditProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case QuestionnaireMainResponseAuditSelectMethod.GetByAuditId:
					_auditId = ( values["AuditId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["AuditId"], typeof(System.Int32)) : (int)0;
					item = QuestionnaireMainResponseAuditProvider.GetByAuditId(_auditId);
					results = new TList<QuestionnaireMainResponseAudit>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				case QuestionnaireMainResponseAuditSelectMethod.GetByQuestionnaireIdAnswerId:
					_questionnaireId_nullable = (System.Int32?) EntityUtil.ChangeType(values["QuestionnaireId"], typeof(System.Int32?));
					_answerId_nullable = (System.Int32?) EntityUtil.ChangeType(values["AnswerId"], typeof(System.Int32?));
					results = QuestionnaireMainResponseAuditProvider.GetByQuestionnaireIdAnswerId(_questionnaireId_nullable, _answerId_nullable, this.StartIndex, this.PageSize, out count);
					break;
				case QuestionnaireMainResponseAuditSelectMethod.GetByModifiedByUserId:
					_modifiedByUserId_nullable = (System.Int32?) EntityUtil.ChangeType(values["ModifiedByUserId"], typeof(System.Int32?));
					results = QuestionnaireMainResponseAuditProvider.GetByModifiedByUserId(_modifiedByUserId_nullable, this.StartIndex, this.PageSize, out count);
					break;
				case QuestionnaireMainResponseAuditSelectMethod.GetByResponseId:
					_responseId_nullable = (System.Int32?) EntityUtil.ChangeType(values["ResponseId"], typeof(System.Int32?));
					results = QuestionnaireMainResponseAuditProvider.GetByResponseId(_responseId_nullable, this.StartIndex, this.PageSize, out count);
					break;
				// FK
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == QuestionnaireMainResponseAuditSelectMethod.Get || SelectMethod == QuestionnaireMainResponseAuditSelectMethod.GetByAuditId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				QuestionnaireMainResponseAudit entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					QuestionnaireMainResponseAuditProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<QuestionnaireMainResponseAudit> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			QuestionnaireMainResponseAuditProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region QuestionnaireMainResponseAuditDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the QuestionnaireMainResponseAuditDataSource class.
	/// </summary>
	public class QuestionnaireMainResponseAuditDataSourceDesigner : ProviderDataSourceDesigner<QuestionnaireMainResponseAudit, QuestionnaireMainResponseAuditKey>
	{
		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainResponseAuditDataSourceDesigner class.
		/// </summary>
		public QuestionnaireMainResponseAuditDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public QuestionnaireMainResponseAuditSelectMethod SelectMethod
		{
			get { return ((QuestionnaireMainResponseAuditDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new QuestionnaireMainResponseAuditDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region QuestionnaireMainResponseAuditDataSourceActionList

	/// <summary>
	/// Supports the QuestionnaireMainResponseAuditDataSourceDesigner class.
	/// </summary>
	internal class QuestionnaireMainResponseAuditDataSourceActionList : DesignerActionList
	{
		private QuestionnaireMainResponseAuditDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainResponseAuditDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public QuestionnaireMainResponseAuditDataSourceActionList(QuestionnaireMainResponseAuditDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public QuestionnaireMainResponseAuditSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion QuestionnaireMainResponseAuditDataSourceActionList
	
	#endregion QuestionnaireMainResponseAuditDataSourceDesigner
	
	#region QuestionnaireMainResponseAuditSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the QuestionnaireMainResponseAuditDataSource.SelectMethod property.
	/// </summary>
	public enum QuestionnaireMainResponseAuditSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByAuditId method.
		/// </summary>
		GetByAuditId,
		/// <summary>
		/// Represents the GetByQuestionnaireIdAnswerId method.
		/// </summary>
		GetByQuestionnaireIdAnswerId,
		/// <summary>
		/// Represents the GetByModifiedByUserId method.
		/// </summary>
		GetByModifiedByUserId,
		/// <summary>
		/// Represents the GetByResponseId method.
		/// </summary>
		GetByResponseId
	}
	
	#endregion QuestionnaireMainResponseAuditSelectMethod

	#region QuestionnaireMainResponseAuditFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireMainResponseAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireMainResponseAuditFilter : SqlFilter<QuestionnaireMainResponseAuditColumn>
	{
	}
	
	#endregion QuestionnaireMainResponseAuditFilter

	#region QuestionnaireMainResponseAuditExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireMainResponseAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireMainResponseAuditExpressionBuilder : SqlExpressionBuilder<QuestionnaireMainResponseAuditColumn>
	{
	}
	
	#endregion QuestionnaireMainResponseAuditExpressionBuilder	

	#region QuestionnaireMainResponseAuditProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;QuestionnaireMainResponseAuditChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireMainResponseAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireMainResponseAuditProperty : ChildEntityProperty<QuestionnaireMainResponseAuditChildEntityTypes>
	{
	}
	
	#endregion QuestionnaireMainResponseAuditProperty
}

