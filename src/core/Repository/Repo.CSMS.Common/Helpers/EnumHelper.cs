﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.ComponentModel;

namespace Repo.CSMS.Common.Helpers
{
    public static class Enums
    {
        public static string GetEnumDescription<TEnum>(TEnum value)
        {
            FieldInfo fi = value.GetType().GetField(value.ToString());
            DescriptionAttribute[] attributes = (DescriptionAttribute[])fi.
                   GetCustomAttributes(typeof(DescriptionAttribute), false);
            if ((attributes != null) && (attributes.Length > 0))
                return attributes[0].Description;
            else
                return value.ToString();
        }
    }

    public enum IsActive
    {
        [Description("Active")]
        Active,
        [Description("In Active")]
        InActive,
        [Description("Both")]
        Both
    }

    public enum Shift
    {
        [Description("Day")]
        Day,
        [Description("Night")]
        Night,
        [Description("Afternoon")]
        Afternoon
    }

    public enum FormMode
    {
        [Description("Read")]
        Read,
        [Description("Create")]
        Create,
        [Description("Edit")]
        Edit,
        [Description("Update")]
        Update,
        [Description("Cancel")]
        Cancel,
        [Description("Delete")]
        Delete
    }
}
