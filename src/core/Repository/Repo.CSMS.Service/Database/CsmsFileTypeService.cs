﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface ICsmsFileTypeService
    {
        CsmsFileType Get(Expression<Func<CsmsFileType, bool>> where, List<Expression<Func<CsmsFileType, object>>> includes);
        List<CsmsFileType> GetMany(int? top, Expression<Func<CsmsFileType, bool>> where, Func<IQueryable<CsmsFileType>, IOrderedQueryable<CsmsFileType>> orderBy, List<Expression<Func<CsmsFileType, object>>> includes);
        List<CsmsFileType> GetManyPaged(int pageNumber, int pageSize, Expression<Func<CsmsFileType, bool>> where, Func<IQueryable<CsmsFileType>, IOrderedQueryable<CsmsFileType>> orderBy, List<Expression<Func<CsmsFileType, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<CsmsFileType, bool>> where, List<Expression<Func<CsmsFileType, object>>> includes);
        void Insert(CsmsFileType item, bool saveChanges = true);
        void Update(CsmsFileType item, bool saveChanges = true);
        void Delete(CsmsFileType item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class CsmsFileTypeService : ICsmsFileTypeService
    {
        private readonly IRepository<CsmsFileType> _repository;

        public CsmsFileTypeService(IRepository<CsmsFileType> repository)
        {
            this._repository = repository;
        }

        public CsmsFileType Get(Expression<Func<CsmsFileType, bool>> where, List<Expression<Func<CsmsFileType, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<CsmsFileType> GetMany(int? top, Expression<Func<CsmsFileType, bool>> where, Func<IQueryable<CsmsFileType>, IOrderedQueryable<CsmsFileType>> orderBy, List<Expression<Func<CsmsFileType, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<CsmsFileType> GetManyPaged(int pageNumber, int pageSize, Expression<Func<CsmsFileType, bool>> where, Func<IQueryable<CsmsFileType>, IOrderedQueryable<CsmsFileType>> orderBy, List<Expression<Func<CsmsFileType, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<CsmsFileType, bool>> where, List<Expression<Func<CsmsFileType, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(CsmsFileType item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("CsmsFileType");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(CsmsFileType item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("CsmsFileType");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(CsmsFileType item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("CsmsFileType");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
