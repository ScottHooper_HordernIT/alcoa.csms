//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Repo.CSMS.DAL.EntityModels
{
    using System;
    using System.Collections.Generic;
    
    public partial class EBI_BACKUP_09092013
    {
        public int EbiId { get; set; }
        public string CompanyName { get; set; }
        public Nullable<int> ClockId { get; set; }
        public string FullName { get; set; }
        public Nullable<int> AccessCardNo { get; set; }
        public string SwipeSite { get; set; }
        public Nullable<bool> DataChecked { get; set; }
        public System.DateTime SwipeDateTime { get; set; }
        public int SwipeYear { get; set; }
        public int SwipeMonth { get; set; }
        public int SwipeDay { get; set; }
        public string Vendor_Number { get; set; }
        public string Source { get; set; }
    }
}
