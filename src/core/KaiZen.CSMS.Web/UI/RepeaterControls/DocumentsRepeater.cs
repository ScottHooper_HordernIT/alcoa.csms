﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.ComponentModel;
using System.Web.UI.Design.WebControls;
using System.Web.UI.Design;
using System.Web.UI.WebControls;

namespace KaiZen.CSMS.Web.UI
{
    /// <summary>
    /// A designer class for a strongly typed repeater <c>DocumentsRepeater</c>
    /// </summary>
	public class DocumentsRepeaterDesigner : System.Web.UI.Design.ControlDesigner
	{
	    /// <summary>
        /// Initializes a new instance of the <see cref="T:DocumentsRepeaterDesigner"/> class.
        /// </summary>
		public DocumentsRepeaterDesigner()
		{
		}

        /// <summary>
        /// Initializes the control designer and loads the specified component.
        /// </summary>
        /// <param name="component">The control being designed.</param>
		public override void Initialize(IComponent component)
		{
			if (!(component is DocumentsRepeater))
			{ 
				throw new ArgumentException("Component is not a DocumentsRepeater."); 
			} 
			base.Initialize(component); 
			base.SetViewFlags(ViewFlags.TemplateEditing, true); 
		}


		/// <summary>
		/// Generate HTML for the designer
		/// </summary>
		/// <returns>a string of design time HTML</returns>
		public override string GetDesignTimeHtml()
		{

			// Get the instance this designer applies to
			//
			DocumentsRepeater z = (DocumentsRepeater)Component;
			z.DataBind();

			return base.GetDesignTimeHtml();

			//return z.RenderAtDesignTime();

			//	ControlCollection c = z.Controls;
			//Totem z = (Totem) Component;
			//Totem z = (Totem) Component;
			//return ("<div style='border: 1px gray dotted; background-color: lightgray'><b>TagStat :</b> zae |  qsdds</div>");

		}
	}

    /// <summary>
    /// A strongly typed repeater control for the <see cref="DocumentsRepeater"/> Type.
    /// </summary>
	[Designer(typeof(DocumentsRepeaterDesigner))]
	[ParseChildren(true)]
	[ToolboxData("<{0}:DocumentsRepeater runat=\"server\"></{0}:DocumentsRepeater>")]
	public class DocumentsRepeater : CompositeDataBoundControl, System.Web.UI.INamingContainer
	{
	    /// <summary>
        /// Initializes a new instance of the <see cref="T:DocumentsRepeater"/> class.
        /// </summary>
		public DocumentsRepeater()
		{
		}

		/// <summary>
        /// Gets a <see cref="T:System.Web.UI.ControlCollection"></see> object that represents the child controls for a specified server control in the UI hierarchy.
        /// </summary>
        /// <value></value>
        /// <returns>The collection of child controls for the specified server control.</returns>
		public override ControlCollection Controls
		{
			get
			{
				this.EnsureChildControls();
				return base.Controls;
			}
		}

		private ITemplate m_headerTemplate;
		/// <summary>
        /// Gets or sets the header template.
        /// </summary>
        /// <value>The header template.</value>
		[Browsable(false)]
		[TemplateContainer(typeof(DocumentsItem))]
		[PersistenceMode(PersistenceMode.InnerDefaultProperty)]
		public ITemplate HeaderTemplate
		{
			get { return m_headerTemplate; }
			set { m_headerTemplate = value; }
		}

		private ITemplate m_itemTemplate;
		/// <summary>
        /// Gets or sets the item template.
        /// </summary>
        /// <value>The item template.</value>
		[Browsable(false)]
		[TemplateContainer(typeof(DocumentsItem))]
		[PersistenceMode(PersistenceMode.InnerDefaultProperty)]
		public ITemplate ItemTemplate
		{
			get { return m_itemTemplate; }
			set { m_itemTemplate = value; }
		}

		private ITemplate m_seperatorTemplate;
        /// <summary>
        /// Gets or sets the Seperator Template
        /// </summary>
        [Browsable(false)]
        [TemplateContainer(typeof(DocumentsItem))]
        [PersistenceMode(PersistenceMode.InnerDefaultProperty)]
        public ITemplate SeperatorTemplate
        {
            get { return m_seperatorTemplate; }
            set { m_seperatorTemplate = value; }
        }
			
		private ITemplate m_altenateItemTemplate;
        /// <summary>
        /// Gets or sets the alternating item template.
        /// </summary>
        /// <value>The alternating item template.</value>
		[Browsable(false)]
		[TemplateContainer(typeof(DocumentsItem))]
		[PersistenceMode(PersistenceMode.InnerDefaultProperty)]
		public ITemplate AlternatingItemTemplate
		{
			get { return m_altenateItemTemplate; }
			set { m_altenateItemTemplate = value; }
		}

		private ITemplate m_footerTemplate;
        /// <summary>
        /// Gets or sets the footer template.
        /// </summary>
        /// <value>The footer template.</value>
		[Browsable(false)]
		[TemplateContainer(typeof(DocumentsItem))]
		[PersistenceMode(PersistenceMode.InnerDefaultProperty)]
		public ITemplate FooterTemplate
		{
			get { return m_footerTemplate; }
			set { m_footerTemplate = value; }
		}

//      /// <summary>
//      /// Called by the ASP.NET page framework to notify server controls that use composition-based implementation to create any child controls they contain in preparation for posting back or rendering.
//      /// </summary>
//		protected override void CreateChildControls()
//      {
//         if (ChildControlsCreated)
//         {
//            return;
//         }

//         Controls.Clear();

//         //Instantiate the Header template (if exists)
//         if (m_headerTemplate != null)
//         {
//            Control headerItem = new Control();
//            m_headerTemplate.InstantiateIn(headerItem);
//            Controls.Add(headerItem);
//         }

//         //Instantiate the Footer template (if exists)
//         if (m_footerTemplate != null)
//         {
//            Control footerItem = new Control();
//            m_footerTemplate.InstantiateIn(footerItem);
//            Controls.Add(footerItem);
//         }
//
//         ChildControlsCreated = true;
//      }
	
		/// <summary>
        /// Overridden and Empty so that span tags are not written
        /// </summary>
        /// <param name="writer"></param>
        public override void RenderBeginTag(HtmlTextWriter writer)
        {
            
        }

        /// <summary>
        /// Overridden and Empty so that span tags are not written
        /// </summary>
        /// <param name="writer"></param>
        public override void RenderEndTag(HtmlTextWriter writer)
        {
                
        }		
		
		/// <summary>
      	/// Called by the ASP.NET page framework to notify server controls that use composition-based implementation to create any child controls they contain in preparation for posting back or rendering.
      	/// </summary>
		protected override int CreateChildControls(System.Collections.IEnumerable dataSource, bool dataBinding)
      	{
         int pos = 0;

         if (dataBinding)
         {
            //Instantiate the Header template (if exists)
            if (m_headerTemplate != null)
            {
                Control headerItem = new Control();
                m_headerTemplate.InstantiateIn(headerItem);
                Controls.Add(headerItem);
            }
			if (dataSource != null)
			{
				foreach (object o in dataSource)
				{
						KaiZen.CSMS.Entities.Documents entity = o as KaiZen.CSMS.Entities.Documents;
						DocumentsItem container = new DocumentsItem(entity);
	
						if (m_itemTemplate != null && (pos % 2) == 0)
						{
							m_itemTemplate.InstantiateIn(container);
							
							if (m_seperatorTemplate != null)
							{
								m_seperatorTemplate.InstantiateIn(container);
							}
						}
						else
						{
							if (m_altenateItemTemplate != null)
							{
								m_altenateItemTemplate.InstantiateIn(container);
								
								if (m_seperatorTemplate != null)
								{
									m_seperatorTemplate.InstantiateIn(container);
								}
								
							}
							else if (m_itemTemplate != null)
							{
								m_itemTemplate.InstantiateIn(container);
								
								if (m_seperatorTemplate != null)
								{
									m_seperatorTemplate.InstantiateIn(container);
								}
							}
							else
							{
								// no template !!!
							}
						}
						Controls.Add(container);
						
						container.DataBind();
						
						pos++;
				}
			}
            //Instantiate the Footer template (if exists)
            if (m_footerTemplate != null)
            {
                Control footerItem = new Control();
                m_footerTemplate.InstantiateIn(footerItem);
                Controls.Add(footerItem);
            }

		}
			
			return pos;
		}

        /// <summary>
        /// Raises the <see cref="E:System.Web.UI.Control.PreRender"></see> event.
        /// </summary>
        /// <param name="e">An <see cref="T:System.EventArgs"></see> object that contains the event data.</param>
		protected override void OnPreRender(EventArgs e)
		{
			base.DataBind();
		}

		#region Design time
        /// <summary>
        /// Renders at design time.
        /// </summary>
        /// <returns>a  string of the Designed HTML</returns>
		internal string RenderAtDesignTime()
		{			
			return "Designer currently not implemented"; 
		}

		#endregion
	}

    /// <summary>
    /// A wrapper type for the entity
    /// </summary>
	[System.ComponentModel.ToolboxItem(false)]
	public class DocumentsItem : System.Web.UI.Control, System.Web.UI.INamingContainer
	{
		private KaiZen.CSMS.Entities.Documents _entity;

        /// <summary>
        /// Initializes a new instance of the <see cref="T:DocumentsItem"/> class.
        /// </summary>
		public DocumentsItem()
			: base()
		{ }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:DocumentsItem"/> class.
        /// </summary>
		public DocumentsItem(KaiZen.CSMS.Entities.Documents entity)
			: base()
		{
			_entity = entity;
		}
		
        /// <summary>
        /// Gets the DocumentId
        /// </summary>
        /// <value>The DocumentId.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32 DocumentId
		{
			get { return _entity.DocumentId; }
		}
        /// <summary>
        /// Gets the RegionId
        /// </summary>
        /// <value>The RegionId.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? RegionId
		{
			get { return _entity.RegionId; }
		}
        /// <summary>
        /// Gets the DocumentName
        /// </summary>
        /// <value>The DocumentName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String DocumentName
		{
			get { return _entity.DocumentName; }
		}
        /// <summary>
        /// Gets the DocumentFileName
        /// </summary>
        /// <value>The DocumentFileName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String DocumentFileName
		{
			get { return _entity.DocumentFileName; }
		}
        /// <summary>
        /// Gets the DocumentFileDescription
        /// </summary>
        /// <value>The DocumentFileDescription.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String DocumentFileDescription
		{
			get { return _entity.DocumentFileDescription; }
		}
        /// <summary>
        /// Gets the DocumentType
        /// </summary>
        /// <value>The DocumentType.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String DocumentType
		{
			get { return _entity.DocumentType; }
		}
        /// <summary>
        /// Gets the RootNode
        /// </summary>
        /// <value>The RootNode.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String RootNode
		{
			get { return _entity.RootNode; }
		}
        /// <summary>
        /// Gets the ParentNode
        /// </summary>
        /// <value>The ParentNode.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String ParentNode
		{
			get { return _entity.ParentNode; }
		}
        /// <summary>
        /// Gets the ChildNode
        /// </summary>
        /// <value>The ChildNode.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String ChildNode
		{
			get { return _entity.ChildNode; }
		}
        /// <summary>
        /// Gets the UploadedFile
        /// </summary>
        /// <value>The UploadedFile.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Byte[] UploadedFile
		{
			get { return _entity.UploadedFile; }
		}
        /// <summary>
        /// Gets the UploadedFile2
        /// </summary>
        /// <value>The UploadedFile2.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Byte[] UploadedFile2
		{
			get { return _entity.UploadedFile2; }
		}
        /// <summary>
        /// Gets the UploadedFile3
        /// </summary>
        /// <value>The UploadedFile3.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Byte[] UploadedFile3
		{
			get { return _entity.UploadedFile3; }
		}
        /// <summary>
        /// Gets the UploadedFileName
        /// </summary>
        /// <value>The UploadedFileName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String UploadedFileName
		{
			get { return _entity.UploadedFileName; }
		}
        /// <summary>
        /// Gets the UploadedFileName2
        /// </summary>
        /// <value>The UploadedFileName2.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String UploadedFileName2
		{
			get { return _entity.UploadedFileName2; }
		}
        /// <summary>
        /// Gets the UploadedFileName3
        /// </summary>
        /// <value>The UploadedFileName3.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String UploadedFileName3
		{
			get { return _entity.UploadedFileName3; }
		}

        /// <summary>
        /// Gets a <see cref="T:KaiZen.CSMS.Entities.Documents"></see> object
        /// </summary>
        /// <value></value>
        [System.ComponentModel.Bindable(true)]
        public KaiZen.CSMS.Entities.Documents Entity
        {
            get { return _entity; }
        }
	}
}
