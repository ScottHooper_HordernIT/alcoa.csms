﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface ITrainingCompetencyService
    {
        TrainingCompetency Get(Expression<Func<TrainingCompetency, bool>> where, List<Expression<Func<TrainingCompetency, object>>> includes);
        List<TrainingCompetency> GetMany(int? top, Expression<Func<TrainingCompetency, bool>> where, Func<IQueryable<TrainingCompetency>, IOrderedQueryable<TrainingCompetency>> orderBy, List<Expression<Func<TrainingCompetency, object>>> includes);
        List<TrainingCompetency> GetManyPaged(int pageNumber, int pageSize, Expression<Func<TrainingCompetency, bool>> where, Func<IQueryable<TrainingCompetency>, IOrderedQueryable<TrainingCompetency>> orderBy, List<Expression<Func<TrainingCompetency, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<TrainingCompetency, bool>> where, List<Expression<Func<TrainingCompetency, object>>> includes);
        void Insert(TrainingCompetency item, bool saveChanges = true);
        void Update(TrainingCompetency item, bool saveChanges = true);
        void Delete(TrainingCompetency item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class TrainingCompetencyService : ITrainingCompetencyService
    {
        private readonly IRepository<TrainingCompetency> _repository;

        public TrainingCompetencyService(IRepository<TrainingCompetency> repository)
        {
            this._repository = repository;
        }

        public TrainingCompetency Get(Expression<Func<TrainingCompetency, bool>> where, List<Expression<Func<TrainingCompetency, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<TrainingCompetency> GetMany(int? top, Expression<Func<TrainingCompetency, bool>> where, Func<IQueryable<TrainingCompetency>, IOrderedQueryable<TrainingCompetency>> orderBy, List<Expression<Func<TrainingCompetency, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<TrainingCompetency> GetManyPaged(int pageNumber, int pageSize, Expression<Func<TrainingCompetency, bool>> where, Func<IQueryable<TrainingCompetency>, IOrderedQueryable<TrainingCompetency>> orderBy, List<Expression<Func<TrainingCompetency, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<TrainingCompetency, bool>> where, List<Expression<Func<TrainingCompetency, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(TrainingCompetency item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("TrainingCompetency");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(TrainingCompetency item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("TrainingCompetency");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(TrainingCompetency item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("TrainingCompetency");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
