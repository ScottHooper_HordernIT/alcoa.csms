﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface IContactsAlcoaService
    {
        ContactsAlcoa Get(Expression<Func<ContactsAlcoa, bool>> where, List<Expression<Func<ContactsAlcoa, object>>> includes);
        List<ContactsAlcoa> GetMany(int? top, Expression<Func<ContactsAlcoa, bool>> where, Func<IQueryable<ContactsAlcoa>, IOrderedQueryable<ContactsAlcoa>> orderBy, List<Expression<Func<ContactsAlcoa, object>>> includes);
        List<ContactsAlcoa> GetManyPaged(int pageNumber, int pageSize, Expression<Func<ContactsAlcoa, bool>> where, Func<IQueryable<ContactsAlcoa>, IOrderedQueryable<ContactsAlcoa>> orderBy, List<Expression<Func<ContactsAlcoa, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<ContactsAlcoa, bool>> where, List<Expression<Func<ContactsAlcoa, object>>> includes);
        void Insert(ContactsAlcoa item, bool saveChanges = true);
        void Update(ContactsAlcoa item, bool saveChanges = true);
        void Delete(ContactsAlcoa item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class ContactsAlcoaService : IContactsAlcoaService
    {
        private readonly IRepository<ContactsAlcoa> _repository;

        public ContactsAlcoaService(IRepository<ContactsAlcoa> repository)
        {
            this._repository = repository;
        }

        public ContactsAlcoa Get(Expression<Func<ContactsAlcoa, bool>> where, List<Expression<Func<ContactsAlcoa, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<ContactsAlcoa> GetMany(int? top, Expression<Func<ContactsAlcoa, bool>> where, Func<IQueryable<ContactsAlcoa>, IOrderedQueryable<ContactsAlcoa>> orderBy, List<Expression<Func<ContactsAlcoa, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<ContactsAlcoa> GetManyPaged(int pageNumber, int pageSize, Expression<Func<ContactsAlcoa, bool>> where, Func<IQueryable<ContactsAlcoa>, IOrderedQueryable<ContactsAlcoa>> orderBy, List<Expression<Func<ContactsAlcoa, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<ContactsAlcoa, bool>> where, List<Expression<Func<ContactsAlcoa, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(ContactsAlcoa item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("ContactsAlcoa");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(ContactsAlcoa item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("ContactsAlcoa");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(ContactsAlcoa item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("ContactsAlcoa");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
