using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using KaiZen.CSMS.Services;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxTabControl;
using DevExpress.Web.Data;
using DevExpress.Web.ASPxGridView;  
using System.Diagnostics;

public partial class UserControls_Admin_EventLog : System.Web.UI.UserControl
{
    Auth auth = new Auth();

    protected void Page_Load(object sender, EventArgs e) { Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); }

    private void moduleInit(bool postBack)
    {
        if (!postBack)
        { //first time load
            if (auth.RoleId == (int)RoleList.Administrator)
            {
                EventLog elog = new EventLog();
                elog.Log = "Application";
                elog.Source = "ASP.NET 4.0.39.0";
                elog.MachineName = ".";



                ASPxGridView1.DataSource = elog.Entries;
                ASPxGridView1.AutoGenerateColumns = true;
                ASPxGridView1.FilterExpression = "Source = 'ASP.NET 4.0.39.0'";
                ASPxGridView1.DataBind();
                UpdatePanel1.Visible = false;
            }
            else
            {
                Response.Redirect("default.aspx");
            }
        }
    }


    protected void ImageButton3_Click(object sender, ImageClickEventArgs e)
    {
        Response.Clear();
        ASPxGridViewExporter1.WriteXlsToResponse();
        Response.Close();
    }
    protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
    {
        Response.Clear();
        ASPxGridViewExporter1.WriteRtfToResponse();
        Response.Close();
    }
    protected void ImageButton2_Click(object sender, ImageClickEventArgs e)
    {
        Response.Clear();
        ASPxGridViewExporter1.WritePdfToResponse();
        Response.Close();
    }
    protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (DropDownList1.SelectedValue.ToString() != "All")
        {
            ASPxGridView1.SettingsPager.Mode = GridViewPagerMode.ShowPager;
            ASPxGridView1.SettingsPager.PageSize = Convert.ToInt32(DropDownList1.SelectedValue.ToString());
        }
        else
        {
            ASPxGridView1.SettingsPager.Mode = GridViewPagerMode.ShowAllRecords;
        }
        ASPxGridView1.DataBind();
    }
}
