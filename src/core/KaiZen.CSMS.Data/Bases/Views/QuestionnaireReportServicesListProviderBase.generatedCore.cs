﻿#region Using directives

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="QuestionnaireReportServicesListProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract class QuestionnaireReportServicesListProviderBaseCore : EntityViewProviderBase<QuestionnaireReportServicesList>
	{
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions
		
		/*
		///<summary>
		/// Fill an VList&lt;QuestionnaireReportServicesList&gt; From a DataSet
		///</summary>
		/// <param name="dataSet">the DataSet</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList&lt;QuestionnaireReportServicesList&gt;"/></returns>
		protected static VList&lt;QuestionnaireReportServicesList&gt; Fill(DataSet dataSet, VList<QuestionnaireReportServicesList> rows, int start, int pagelen)
		{
			if (dataSet.Tables.Count == 1)
			{
				return Fill(dataSet.Tables[0], rows, start, pagelen);
			}
			else
			{
				return new VList<QuestionnaireReportServicesList>();
			}	
		}
		
		
		///<summary>
		/// Fill an VList&lt;QuestionnaireReportServicesList&gt; From a DataTable
		///</summary>
		/// <param name="dataTable">the DataTable that hold the data.</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList<QuestionnaireReportServicesList>"/></returns>
		protected static VList&lt;QuestionnaireReportServicesList&gt; Fill(DataTable dataTable, VList<QuestionnaireReportServicesList> rows, int start, int pagelen)
		{
			int recordnum = 0;
			
			System.Collections.IEnumerator dataRows =  dataTable.Rows.GetEnumerator();
			
			while (dataRows.MoveNext() && (pagelen != 0))
			{
				if(recordnum >= start)
				{
					DataRow row = (DataRow)dataRows.Current;
				
					QuestionnaireReportServicesList c = new QuestionnaireReportServicesList();
					c.QuestionnaireId = (Convert.IsDBNull(row["QuestionnaireId"]))?(int)0:(System.Int32)row["QuestionnaireId"];
					c.QuestionnaireReportServices = (Convert.IsDBNull(row["QuestionnaireReportServices"]))?string.Empty:(System.String)row["QuestionnaireReportServices"];
					c.AcceptChanges();
					rows.Add(c);
					pagelen -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		*/	
						
		///<summary>
		/// Fill an <see cref="VList&lt;QuestionnaireReportServicesList&gt;"/> From a DataReader.
		///</summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pageLength">number of row.</param>
		///<returns>a <see cref="VList&lt;QuestionnaireReportServicesList&gt;"/></returns>
		protected VList<QuestionnaireReportServicesList> Fill(IDataReader reader, VList<QuestionnaireReportServicesList> rows, int start, int pageLength)
		{
			int recordnum = 0;
			while (reader.Read() && (pageLength != 0))
			{
				if(recordnum >= start)
				{
					QuestionnaireReportServicesList entity = null;
					if (DataRepository.Provider.UseEntityFactory)
					{
						entity = EntityManager.CreateViewEntity<QuestionnaireReportServicesList>("QuestionnaireReportServicesList",  DataRepository.Provider.EntityCreationalFactoryType); 
					}
					else
					{
						entity = new QuestionnaireReportServicesList();
					}
					
					entity.SuppressEntityEvents = true;

					entity.QuestionnaireId = (System.Int32)reader[((int)QuestionnaireReportServicesListColumn.QuestionnaireId)];
					//entity.QuestionnaireId = (Convert.IsDBNull(reader["QuestionnaireId"]))?(int)0:(System.Int32)reader["QuestionnaireId"];
					entity.QuestionnaireReportServices = (reader.IsDBNull(((int)QuestionnaireReportServicesListColumn.QuestionnaireReportServices)))?null:(System.String)reader[((int)QuestionnaireReportServicesListColumn.QuestionnaireReportServices)];
					//entity.QuestionnaireReportServices = (Convert.IsDBNull(reader["QuestionnaireReportServices"]))?string.Empty:(System.String)reader["QuestionnaireReportServices"];
					entity.AcceptChanges();
					entity.SuppressEntityEvents = false;
					
					rows.Add(entity);
					pageLength -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		
		
		/// <summary>
		/// Refreshes the <see cref="QuestionnaireReportServicesList"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="QuestionnaireReportServicesList"/> object to refresh.</param>
		protected void RefreshEntity(IDataReader reader, QuestionnaireReportServicesList entity)
		{
			reader.Read();
			entity.QuestionnaireId = (System.Int32)reader[((int)QuestionnaireReportServicesListColumn.QuestionnaireId)];
			//entity.QuestionnaireId = (Convert.IsDBNull(reader["QuestionnaireId"]))?(int)0:(System.Int32)reader["QuestionnaireId"];
			entity.QuestionnaireReportServices = (reader.IsDBNull(((int)QuestionnaireReportServicesListColumn.QuestionnaireReportServices)))?null:(System.String)reader[((int)QuestionnaireReportServicesListColumn.QuestionnaireReportServices)];
			//entity.QuestionnaireReportServices = (Convert.IsDBNull(reader["QuestionnaireReportServices"]))?string.Empty:(System.String)reader["QuestionnaireReportServices"];
			reader.Close();
	
			entity.AcceptChanges();
		}
		
		/*
		/// <summary>
		/// Refreshes the <see cref="QuestionnaireReportServicesList"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="QuestionnaireReportServicesList"/> object.</param>
		protected static void RefreshEntity(DataSet dataSet, QuestionnaireReportServicesList entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.QuestionnaireId = (Convert.IsDBNull(dataRow["QuestionnaireId"]))?(int)0:(System.Int32)dataRow["QuestionnaireId"];
			entity.QuestionnaireReportServices = (Convert.IsDBNull(dataRow["QuestionnaireReportServices"]))?string.Empty:(System.String)dataRow["QuestionnaireReportServices"];
			entity.AcceptChanges();
		}
		*/
			
		#endregion Helper Functions
	}//end class

	#region QuestionnaireReportServicesListFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireReportServicesList"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireReportServicesListFilterBuilder : SqlFilterBuilder<QuestionnaireReportServicesListColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportServicesListFilterBuilder class.
		/// </summary>
		public QuestionnaireReportServicesListFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportServicesListFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireReportServicesListFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportServicesListFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireReportServicesListFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireReportServicesListFilterBuilder

	#region QuestionnaireReportServicesListParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireReportServicesList"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireReportServicesListParameterBuilder : ParameterizedSqlFilterBuilder<QuestionnaireReportServicesListColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportServicesListParameterBuilder class.
		/// </summary>
		public QuestionnaireReportServicesListParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportServicesListParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireReportServicesListParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportServicesListParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireReportServicesListParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireReportServicesListParameterBuilder
	
	#region QuestionnaireReportServicesListSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireReportServicesList"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class QuestionnaireReportServicesListSortBuilder : SqlSortBuilder<QuestionnaireReportServicesListColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportServicesListSqlSortBuilder class.
		/// </summary>
		public QuestionnaireReportServicesListSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion QuestionnaireReportServicesListSortBuilder

} // end namespace
