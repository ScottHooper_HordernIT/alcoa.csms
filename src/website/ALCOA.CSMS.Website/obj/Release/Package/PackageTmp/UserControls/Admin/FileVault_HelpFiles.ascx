﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FileVault_HelpFiles.ascx.cs" Inherits="ALCOA.CSMS.Website.UserControls.Admin.FileVault_HelpFiles" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxUploadControl" tagprefix="dx" %>
<script language="javascript" type="text/javascript">
    function popUpClosed() {
        window.location.reload();
    }
</script>
<p>
    <br />
    List of Files</p>
<dx:ASPxGridView ID="grid" runat="server" AutoGenerateColumns="False" 
    CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
    CssPostfix="Office2003Blue" DataSourceID="FileVaultListHelpFilesDataSource1"
    OnHtmlRowCreated="grid_HtmlRowCreated"
    OnRowInserting="grid_RowInserting" OnRowDeleting="grid_RowDeleting" OnRowUpdating="grid_RowUpdating">
    <Columns>
        <dx:GridViewDataTextColumn FieldName="FileVaultId" 
            VisibleIndex="1" Caption="Action">
            <DataItemTemplate>
                <dx:ASPxHyperLink ID="hlEdit" runat="server" Text="Edit">
                </dx:ASPxHyperLink>
            </DataItemTemplate>
            <EditFormSettings Visible="False" />
            <Settings AllowHeaderFilter="False" />
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn FieldName="FileName" VisibleIndex="2">
            <EditFormSettings Visible="False" />
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn Caption="Size" FieldName="ContentLength" 
            VisibleIndex="3">
            <DataItemTemplate>
                <dx:ASPxLabel ID="lblFileSize" runat="server" Text="-">
                </dx:ASPxLabel>
            </DataItemTemplate>
            <EditFormSettings Visible="False" />
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn Caption="Upload" Visible="False" VisibleIndex="4">
            <EditFormSettings Visible="True" />
            <EditItemTemplate>
                
            </EditItemTemplate>
        </dx:GridViewDataTextColumn>
    </Columns>
    <Settings ShowHeaderFilterButton="true" />
    <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
        <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
        </LoadingPanelOnStatusBar>
        <LoadingPanel Url="~/App_Themes/Office2003BlueOld/GridView/Loading.gif"><%--Enhancement_023:change by debashis for testing platform upgradation--%>
        </LoadingPanel>
    </Images>
    <ImagesFilterControl>
        <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
        </LoadingPanel>
    </ImagesFilterControl>
    <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
        CssPostfix="Office2003Blue">
        <Header ImageSpacing="5px" SortingImageSpacing="5px">
        </Header>
        <LoadingPanel ImageSpacing="10px">
        </LoadingPanel>
    </Styles>
    <StylesEditors>
        <ProgressBar Height="25px">
        </ProgressBar>
    </StylesEditors>
</dx:ASPxGridView>

    <br />
    <a href="javascript:popUp('../PopUps/EditHelpFile.aspx')">Add New Help File</a>
    

<data:FileVaultListHelpFilesDataSource ID="FileVaultListHelpFilesDataSource1" 
    runat="server">
</data:FileVaultListHelpFilesDataSource>

