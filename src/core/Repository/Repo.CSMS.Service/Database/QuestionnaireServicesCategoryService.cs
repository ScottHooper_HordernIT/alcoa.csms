﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface IQuestionnaireServicesCategoryService
    {
        QuestionnaireServicesCategory Get(Expression<Func<QuestionnaireServicesCategory, bool>> where, List<Expression<Func<QuestionnaireServicesCategory, object>>> includes);
        List<QuestionnaireServicesCategory> GetMany(int? top, Expression<Func<QuestionnaireServicesCategory, bool>> where, Func<IQueryable<QuestionnaireServicesCategory>, IOrderedQueryable<QuestionnaireServicesCategory>> orderBy, List<Expression<Func<QuestionnaireServicesCategory, object>>> includes);
        List<QuestionnaireServicesCategory> GetManyPaged(int pageNumber, int pageSize, Expression<Func<QuestionnaireServicesCategory, bool>> where, Func<IQueryable<QuestionnaireServicesCategory>, IOrderedQueryable<QuestionnaireServicesCategory>> orderBy, List<Expression<Func<QuestionnaireServicesCategory, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<QuestionnaireServicesCategory, bool>> where, List<Expression<Func<QuestionnaireServicesCategory, object>>> includes);
        void Insert(QuestionnaireServicesCategory item, bool saveChanges = true);
        void Update(QuestionnaireServicesCategory item, bool saveChanges = true);
        void Delete(QuestionnaireServicesCategory item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class QuestionnaireServicesCategoryService : IQuestionnaireServicesCategoryService
    {
        private readonly IRepository<QuestionnaireServicesCategory> _repository;

        public QuestionnaireServicesCategoryService(IRepository<QuestionnaireServicesCategory> repository)
        {
            this._repository = repository;
        }

        public QuestionnaireServicesCategory Get(Expression<Func<QuestionnaireServicesCategory, bool>> where, List<Expression<Func<QuestionnaireServicesCategory, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<QuestionnaireServicesCategory> GetMany(int? top, Expression<Func<QuestionnaireServicesCategory, bool>> where, Func<IQueryable<QuestionnaireServicesCategory>, IOrderedQueryable<QuestionnaireServicesCategory>> orderBy, List<Expression<Func<QuestionnaireServicesCategory, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<QuestionnaireServicesCategory> GetManyPaged(int pageNumber, int pageSize, Expression<Func<QuestionnaireServicesCategory, bool>> where, Func<IQueryable<QuestionnaireServicesCategory>, IOrderedQueryable<QuestionnaireServicesCategory>> orderBy, List<Expression<Func<QuestionnaireServicesCategory, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<QuestionnaireServicesCategory, bool>> where, List<Expression<Func<QuestionnaireServicesCategory, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(QuestionnaireServicesCategory item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("QuestionnaireServicesCategory");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(QuestionnaireServicesCategory item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("QuestionnaireServicesCategory");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(QuestionnaireServicesCategory item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("QuestionnaireServicesCategory");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
