﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" Inherits="news" Codebehind="news.aspx.cs" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxHtmlEditor.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxHtmlEditor" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxSpellChecker.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxSpellChecker" TagPrefix="dx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Contractor Services Management System | News</title>
    <link rel="shortcut icon" href="~/Images/favicon.ico" type="image/x-icon" />
    <script language="javascript" type="text/javascript">
            function prevent_previous_page_return()
            {
               window.history.forward();
            }
    </script>

    <asp:PlaceHolder ID="phBundling" runat="server">
        <%: System.Web.Optimization.Styles.Render("~/bundles/user_master_styles") %>
    </asp:PlaceHolder>


    <style type="text/css">
        p.MsoListParagraph
        {
            margin-top: 0cm;
            margin-right: 0cm;
            margin-bottom: 0cm;
            margin-left: 36.0pt;
            margin-bottom: .0001pt;
            font-size: 11.0pt;
            font-family: "Calibri" , "sans-serif";
        }
    </style>
</head>
<body onload="prevent_previous_page_return();">
    <form id="form1" runat="server">
    <table align="center" width="900px">
        <tr>
            <td align="center" style="text-align: center; margin-left: auto; margin-right: auto;" width="900px">
                <div style="text-align: center" align="center">
                    <table align="center" cellpadding="0" cellspacing="0" width="740px">
                        <tr>
                            <td align="left" style="text-align: left">
                                <img border="0" height="33" src="Images/top_alcoa_logo_wide.gif" width="153" alt="Alcoa"/>
                            </td>
                            <td align="right" style="text-align: right">
                                <span style="color: #003399">Alcoa World Alumina - Australian Operations<br />
                                    <span class="sitetitleblue"><a class="sitetitleblue" href="default.aspx" onfocus="blur(this)">
                                        Contractor Services Management System</a></span> </span>
                            </td>
                        </tr>
                    </table>
                    <br />
                    <br />
                    <div align="center" style="text-align: center">
                        <dx:ASPxHtmlEditor align="center" ID="ASPxHtmlEditor1" runat="server" ActiveView="Preview" CssFilePath="~/App_Themes/Office2010BlueNew/{0}/styles.css"
                            CssPostfix="Office2010Blue" Width="740px" Height="460px">
                            <Styles CssFilePath="~/App_Themes/Office2010BlueNew/{0}/styles.css" CssPostfix="Office2010Blue">
                                <ViewArea>
                                    <Border BorderColor="#859EBF" />
                                </ViewArea>
                            </Styles>
                            <Images SpriteCssFilePath="~/App_Themes/Office2010BlueNew/{0}/sprite.css">
                                <LoadingPanel Url="~/App_Themes/Office2010BlueNew/HtmlEditor/Loading.gif">
                                </LoadingPanel>
                            </Images>
                            <ImagesFileManager>
                                <FolderContainerNodeLoadingPanel Url="~/App_Themes/Office2010BlueNew/Web/tvNodeLoading.gif">
                                </FolderContainerNodeLoadingPanel>
                                <LoadingPanel Url="~/App_Themes/Office2010BlueNew/Web/Loading.gif">
                                </LoadingPanel>
                            </ImagesFileManager>
                            <SettingsImageSelector>
                                <CommonSettings AllowedFileExtensions=".jpe, .jpeg, .jpg, .gif, .png"></CommonSettings>
                            </SettingsImageSelector>
                            <Settings AllowDesignView="False" AllowHtmlView="False" />
                            <SettingsImageUpload>
                                <ValidationSettings AllowedFileExtensions=".jpe, .jpeg, .jpg, .gif, .png">
                                </ValidationSettings>
                            </SettingsImageUpload>
                            <SettingsDocumentSelector>
                                <CommonSettings AllowedFileExtensions=".rtf, .pdf, .doc, .docx, .odt, .txt, .xls, .xlsx, .ods, .ppt, .pptx, .odp">
                                </CommonSettings>
                            </SettingsDocumentSelector>
                            <StylesEditors ButtonEditCellSpacing="0">
                            </StylesEditors>
                            <StylesStatusBar>
                                <StatusBar TabSpacing="0px">
                                    <Paddings Padding="0px" />
                                </StatusBar>
                            </StylesStatusBar>
                        </dx:ASPxHtmlEditor>
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td align="center" style="text-align: center; margin-left: auto; margin-right: auto;" width="900px">
                <div style="text-align: center" align="center">
                    <dx:ASPxButton ID="ASPxButton1" runat="server" Text="I've Read the News, Continue to the front page."
                        PostBackUrl="default.aspx" CssFilePath="~/App_Themes/Office2010BlueNew/{0}/styles.css"
                        CssPostfix="Office2010Blue" SpriteCssFilePath="~/App_Themes/Office2010BlueNew/{0}/sprite.css">
                    </dx:ASPxButton>
                </div>
            </td>
        </tr>
    </table>
    </form>

    <%: System.Web.Optimization.Scripts.Render("~/bundles/jquery_scripts") %>
    <%: System.Web.Optimization.Scripts.Render("~/bundles/user_master_scripts") %>
</body>
</html>
