using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.Data;

using KaiZen.CSMS.Services;
using KaiZen.CSMS.Entities;

using DevExpress.Web.ASPxEditors;

public partial class Library_APSS_Top9 : System.Web.UI.UserControl
{
    Auth auth = new Auth();
    Hashtable htReadFiles = new Hashtable();
    protected void Page_Load(object sender, EventArgs e) {  Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); }

    private void moduleInit(bool postBack)
    {
        if (!postBack)
        {
            btnEdit.Visible = false;
            SessionHandler.ViewMode = "View";
            Session["MR"] = "MR";
        }

        if (Request.QueryString["i"] != null)
        {
            Session["Documents_Region"] = "%";
            SessionHandler.spVar_CompanyId = auth.CompanyId.ToString();
            DataView dvReadCD = (DataView)sqldsReadCD.Select(DataSourceSelectArguments.Empty);
            foreach (DataRow dr in dvReadCD.Table.Rows)
            {
                htReadFiles.Add(dr["MustReadDownloaded"].ToString(), "1");
            }
        }
        else
        {
            String Region = "";
            if(Request.QueryString["Region"] != null)
            {
                Region = Request.QueryString["Region"];
            }

            RegionsService rService = new RegionsService();
            Regions r = rService.GetByRegionInternalName("WAO");
            Session["Documents_Region"] = (int)r.RegionId;

            if (!String.IsNullOrEmpty(Region))
            {
                if (Region == "VIC")
                {
                    r = rService.GetByRegionInternalName("VICOPS");
                    Session["Documents_Region"] = (int)r.RegionId;
                    //grid.Columns["Read"].Visible = false;
                    LinkButton1.Enabled = false;
                    LinkButton2.Enabled = false;
                    btnEdit.Visible = false;
                }
            }

            SessionHandler.spVar_CompanyId = auth.CompanyId.ToString();
            DataView dvReadCD = (DataView)sqldsReadCD.Select(DataSourceSelectArguments.Empty);
            foreach (DataRow dr in dvReadCD.Table.Rows)
            {
                htReadFiles.Add(dr["MustReadDownloaded"].ToString(), "1");
            }

            switch (auth.RoleId)
            {
                case ((int)RoleList.Reader):
                    break;
                case ((int)RoleList.Contractor):
                    break;
                case ((int)RoleList.Administrator):
                    btnEdit.Visible = true;
                    break;
                default:
                    // do something
                    Response.Redirect("AccessDenied.aspx?error=" + Server.UrlEncode("Unrecognised User."));
                    break;
            }
        }
    }
    protected void btnEdit_Click(object sender, EventArgs e)
    {
        if (SessionHandler.ViewMode == "View")
        {
            grid.Columns[0].Visible = true;
            SessionHandler.ViewMode = "Edit";
            btnEdit.Text = "(View)";
        }
        else if (SessionHandler.ViewMode == "Edit")
        {
            grid.Columns[0].Visible = false;
            SessionHandler.ViewMode = "View";
            btnEdit.Text = "(Edit)";
        }
    }

    protected void grid_RowUpdating(object sender, ASPxDataUpdatingEventArgs e)
    {
        e.NewValues["DocumentType"] = "MR";
    }
    protected void grid_InitNewRow(object sender, ASPxDataInitNewRowEventArgs e) //Default Values 
    {
        e.NewValues["DocumentType"] = "MR";
        RegionsService rService = new RegionsService();
        Regions r = rService.GetByRegionInternalName("WAO");
        e.NewValues["RegionId"] = (int)r.RegionId;
        if (Request.QueryString["Region"] != null)
        {
            if (Request.QueryString["Region"] == "VIC")
            {
                r = rService.GetByRegionInternalName("VICOPS");
                e.NewValues["RegionId"] = (int)r.RegionId;
            }
        }
    }
    protected void grid_OnRowInserting(object sender, ASPxDataInsertingEventArgs e) //Default Values 
    {
        e.NewValues["DocumentType"] = "MR";
        RegionsService rService = new RegionsService();
        Regions r = rService.GetByRegionInternalName("WAO");
        e.NewValues["RegionId"] = (int)r.RegionId;

        if (Request.QueryString["Region"] != null)
        {
            if (Request.QueryString["Region"] == "VIC")
            {
                r = rService.GetByRegionInternalName("VICOPS");
                e.NewValues["RegionId"] = (int)r.RegionId;
            }
        }
    }

    protected void grid_RowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {
        if (e.RowType != GridViewRowType.Data) return;

        string fileName = (string)grid.GetRowValues(e.VisibleIndex, "DocumentFileName");
        int Region = (int)grid.GetRowValues(e.VisibleIndex, "RegionId");

        Label label = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "lblRead") as Label;
        if (label != null)
        {
            

            if (htReadFiles.ContainsKey(fileName))
            {
                label.Text = "Read";
                label.ForeColor = System.Drawing.Color.Green;
            }
            else
            {
                label.Text = "UnRead";
                label.ForeColor = System.Drawing.Color.Red;
            }
        }

        ASPxHyperLink hl = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "hlFileDownload") as ASPxHyperLink;
        if (hl != null)
        {
            hl.Text = fileName;
            hl.NavigateUrl = String.Format("javascript:popUp2('Common/GetFile.aspx{0}')", QueryStringModule.Encrypt("Type=APSS&SubType=MR&File=" + fileName));


             RegionsService rService = new RegionsService();
            Regions r = rService.GetByRegionInternalName("VICOPS");

            if (Region == r.RegionId)
            {
                hl.NavigateUrl = String.Format("javascript:popUp2('Common/GetFile.aspx{0}')", QueryStringModule.Encrypt(String.Format("Type=APSS&SubType=MR&File={0}&Region=VIC", fileName)));
            }
        }
    }
}
