﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.EbiProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(EbiDataSourceDesigner))]
	public class EbiDataSource : ProviderDataSource<Ebi, EbiKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EbiDataSource class.
		/// </summary>
		public EbiDataSource() : base(new EbiService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the EbiDataSourceView used by the EbiDataSource.
		/// </summary>
		protected EbiDataSourceView EbiView
		{
			get { return ( View as EbiDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the EbiDataSource control invokes to retrieve data.
		/// </summary>
		public EbiSelectMethod SelectMethod
		{
			get
			{
				EbiSelectMethod selectMethod = EbiSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (EbiSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the EbiDataSourceView class that is to be
		/// used by the EbiDataSource.
		/// </summary>
		/// <returns>An instance of the EbiDataSourceView class.</returns>
		protected override BaseDataSourceView<Ebi, EbiKey> GetNewDataSourceView()
		{
			return new EbiDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the EbiDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class EbiDataSourceView : ProviderDataSourceView<Ebi, EbiKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EbiDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the EbiDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public EbiDataSourceView(EbiDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal EbiDataSource EbiOwner
		{
			get { return Owner as EbiDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal EbiSelectMethod SelectMethod
		{
			get { return EbiOwner.SelectMethod; }
			set { EbiOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal EbiService EbiProvider
		{
			get { return Provider as EbiService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<Ebi> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<Ebi> results = null;
			Ebi item;
			count = 0;
			
			System.Int32 _ebiId;
			System.Boolean? _dataChecked_nullable;
			System.Int32? _accessCardNo_nullable;
			System.String _companyName_nullable;
			System.String _swipeSite;
			System.Int32 _swipeYear;
			System.Int32 _swipeMonth;
			System.Int32 _swipeDay;

			switch ( SelectMethod )
			{
				case EbiSelectMethod.Get:
					EbiKey entityKey  = new EbiKey();
					entityKey.Load(values);
					item = EbiProvider.Get(entityKey);
					results = new TList<Ebi>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case EbiSelectMethod.GetAll:
                    results = EbiProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case EbiSelectMethod.GetPaged:
					results = EbiProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case EbiSelectMethod.Find:
					if ( FilterParameters != null )
						results = EbiProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = EbiProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case EbiSelectMethod.GetByEbiId:
					_ebiId = ( values["EbiId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["EbiId"], typeof(System.Int32)) : (int)0;
					item = EbiProvider.GetByEbiId(_ebiId);
					results = new TList<Ebi>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				case EbiSelectMethod.GetByDataChecked:
					_dataChecked_nullable = (System.Boolean?) EntityUtil.ChangeType(values["DataChecked"], typeof(System.Boolean?));
					results = EbiProvider.GetByDataChecked(_dataChecked_nullable, this.StartIndex, this.PageSize, out count);
					break;
				case EbiSelectMethod.GetByAccessCardNo:
					_accessCardNo_nullable = (System.Int32?) EntityUtil.ChangeType(values["AccessCardNo"], typeof(System.Int32?));
					results = EbiProvider.GetByAccessCardNo(_accessCardNo_nullable, this.StartIndex, this.PageSize, out count);
					break;
				case EbiSelectMethod.GetByCompanyName:
					_companyName_nullable = (System.String) EntityUtil.ChangeType(values["CompanyName"], typeof(System.String));
					results = EbiProvider.GetByCompanyName(_companyName_nullable, this.StartIndex, this.PageSize, out count);
					break;
				case EbiSelectMethod.GetBySwipeSite:
					_swipeSite = ( values["SwipeSite"] != null ) ? (System.String) EntityUtil.ChangeType(values["SwipeSite"], typeof(System.String)) : string.Empty;
					results = EbiProvider.GetBySwipeSite(_swipeSite, this.StartIndex, this.PageSize, out count);
					break;
				case EbiSelectMethod.GetBySwipeYear:
					_swipeYear = ( values["SwipeYear"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["SwipeYear"], typeof(System.Int32)) : (int)0;
					results = EbiProvider.GetBySwipeYear(_swipeYear, this.StartIndex, this.PageSize, out count);
					break;
				case EbiSelectMethod.GetBySwipeYearSwipeMonth:
					_swipeYear = ( values["SwipeYear"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["SwipeYear"], typeof(System.Int32)) : (int)0;
					_swipeMonth = ( values["SwipeMonth"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["SwipeMonth"], typeof(System.Int32)) : (int)0;
					results = EbiProvider.GetBySwipeYearSwipeMonth(_swipeYear, _swipeMonth, this.StartIndex, this.PageSize, out count);
					break;
				case EbiSelectMethod.GetBySwipeYearSwipeMonthSwipeDay:
					_swipeYear = ( values["SwipeYear"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["SwipeYear"], typeof(System.Int32)) : (int)0;
					_swipeMonth = ( values["SwipeMonth"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["SwipeMonth"], typeof(System.Int32)) : (int)0;
					_swipeDay = ( values["SwipeDay"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["SwipeDay"], typeof(System.Int32)) : (int)0;
					results = EbiProvider.GetBySwipeYearSwipeMonthSwipeDay(_swipeYear, _swipeMonth, _swipeDay, this.StartIndex, this.PageSize, out count);
					break;
				case EbiSelectMethod.GetBySwipeYearSwipeMonthSwipeSiteCompanyName:
					_swipeYear = ( values["SwipeYear"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["SwipeYear"], typeof(System.Int32)) : (int)0;
					_swipeMonth = ( values["SwipeMonth"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["SwipeMonth"], typeof(System.Int32)) : (int)0;
					_swipeSite = ( values["SwipeSite"] != null ) ? (System.String) EntityUtil.ChangeType(values["SwipeSite"], typeof(System.String)) : string.Empty;
					_companyName_nullable = (System.String) EntityUtil.ChangeType(values["CompanyName"], typeof(System.String));
					results = EbiProvider.GetBySwipeYearSwipeMonthSwipeSiteCompanyName(_swipeYear, _swipeMonth, _swipeSite, _companyName_nullable, this.StartIndex, this.PageSize, out count);
					break;
				// FK
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == EbiSelectMethod.Get || SelectMethod == EbiSelectMethod.GetByEbiId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				Ebi entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					EbiProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<Ebi> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			EbiProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region EbiDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the EbiDataSource class.
	/// </summary>
	public class EbiDataSourceDesigner : ProviderDataSourceDesigner<Ebi, EbiKey>
	{
		/// <summary>
		/// Initializes a new instance of the EbiDataSourceDesigner class.
		/// </summary>
		public EbiDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public EbiSelectMethod SelectMethod
		{
			get { return ((EbiDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new EbiDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region EbiDataSourceActionList

	/// <summary>
	/// Supports the EbiDataSourceDesigner class.
	/// </summary>
	internal class EbiDataSourceActionList : DesignerActionList
	{
		private EbiDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the EbiDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public EbiDataSourceActionList(EbiDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public EbiSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion EbiDataSourceActionList
	
	#endregion EbiDataSourceDesigner
	
	#region EbiSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the EbiDataSource.SelectMethod property.
	/// </summary>
	public enum EbiSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByEbiId method.
		/// </summary>
		GetByEbiId,
		/// <summary>
		/// Represents the GetByDataChecked method.
		/// </summary>
		GetByDataChecked,
		/// <summary>
		/// Represents the GetByAccessCardNo method.
		/// </summary>
		GetByAccessCardNo,
		/// <summary>
		/// Represents the GetByCompanyName method.
		/// </summary>
		GetByCompanyName,
		/// <summary>
		/// Represents the GetBySwipeSite method.
		/// </summary>
		GetBySwipeSite,
		/// <summary>
		/// Represents the GetBySwipeYear method.
		/// </summary>
		GetBySwipeYear,
		/// <summary>
		/// Represents the GetBySwipeYearSwipeMonth method.
		/// </summary>
		GetBySwipeYearSwipeMonth,
		/// <summary>
		/// Represents the GetBySwipeYearSwipeMonthSwipeDay method.
		/// </summary>
		GetBySwipeYearSwipeMonthSwipeDay,
		/// <summary>
		/// Represents the GetBySwipeYearSwipeMonthSwipeSiteCompanyName method.
		/// </summary>
		GetBySwipeYearSwipeMonthSwipeSiteCompanyName
	}
	
	#endregion EbiSelectMethod

	#region EbiFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Ebi"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EbiFilter : SqlFilter<EbiColumn>
	{
	}
	
	#endregion EbiFilter

	#region EbiExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Ebi"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EbiExpressionBuilder : SqlExpressionBuilder<EbiColumn>
	{
	}
	
	#endregion EbiExpressionBuilder	

	#region EbiProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;EbiChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="Ebi"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EbiProperty : ChildEntityProperty<EbiChildEntityTypes>
	{
	}
	
	#endregion EbiProperty
}

