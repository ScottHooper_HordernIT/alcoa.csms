﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.ContactsContractorsProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(ContactsContractorsDataSourceDesigner))]
	public class ContactsContractorsDataSource : ProviderDataSource<ContactsContractors, ContactsContractorsKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ContactsContractorsDataSource class.
		/// </summary>
		public ContactsContractorsDataSource() : base(new ContactsContractorsService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the ContactsContractorsDataSourceView used by the ContactsContractorsDataSource.
		/// </summary>
		protected ContactsContractorsDataSourceView ContactsContractorsView
		{
			get { return ( View as ContactsContractorsDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the ContactsContractorsDataSource control invokes to retrieve data.
		/// </summary>
		public ContactsContractorsSelectMethod SelectMethod
		{
			get
			{
				ContactsContractorsSelectMethod selectMethod = ContactsContractorsSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (ContactsContractorsSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the ContactsContractorsDataSourceView class that is to be
		/// used by the ContactsContractorsDataSource.
		/// </summary>
		/// <returns>An instance of the ContactsContractorsDataSourceView class.</returns>
		protected override BaseDataSourceView<ContactsContractors, ContactsContractorsKey> GetNewDataSourceView()
		{
			return new ContactsContractorsDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the ContactsContractorsDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class ContactsContractorsDataSourceView : ProviderDataSourceView<ContactsContractors, ContactsContractorsKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ContactsContractorsDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the ContactsContractorsDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public ContactsContractorsDataSourceView(ContactsContractorsDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal ContactsContractorsDataSource ContactsContractorsOwner
		{
			get { return Owner as ContactsContractorsDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal ContactsContractorsSelectMethod SelectMethod
		{
			get { return ContactsContractorsOwner.SelectMethod; }
			set { ContactsContractorsOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal ContactsContractorsService ContactsContractorsProvider
		{
			get { return Provider as ContactsContractorsService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<ContactsContractors> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<ContactsContractors> results = null;
			ContactsContractors item;
			count = 0;
			
			System.Int32 _contactId;
			System.String _contactLastName_nullable;
			System.String _contactFirstName;
			System.Int32 _companyId;
			System.String _contactRole_nullable;
			System.String _contactTitle_nullable;
			System.String _contactPhone_nullable;
			System.String _contactMobile_nullable;
			System.String _contactEmail_nullable;
			System.Int32? _siteId_nullable;
			System.Int32 _modifiedByUserId;
			System.Int32? _regionId_nullable;

			switch ( SelectMethod )
			{
				case ContactsContractorsSelectMethod.Get:
					ContactsContractorsKey entityKey  = new ContactsContractorsKey();
					entityKey.Load(values);
					item = ContactsContractorsProvider.Get(entityKey);
					results = new TList<ContactsContractors>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case ContactsContractorsSelectMethod.GetAll:
                    results = ContactsContractorsProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case ContactsContractorsSelectMethod.GetPaged:
					results = ContactsContractorsProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case ContactsContractorsSelectMethod.Find:
					if ( FilterParameters != null )
						results = ContactsContractorsProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = ContactsContractorsProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case ContactsContractorsSelectMethod.GetByContactId:
					_contactId = ( values["ContactId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["ContactId"], typeof(System.Int32)) : (int)0;
					item = ContactsContractorsProvider.GetByContactId(_contactId);
					results = new TList<ContactsContractors>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				case ContactsContractorsSelectMethod.GetByContactLastNameContactFirstNameCompanyIdContactRoleContactTitleContactPhoneContactMobileContactEmailSiteId:
					_contactLastName_nullable = (System.String) EntityUtil.ChangeType(values["ContactLastName"], typeof(System.String));
					_contactFirstName = ( values["ContactFirstName"] != null ) ? (System.String) EntityUtil.ChangeType(values["ContactFirstName"], typeof(System.String)) : string.Empty;
					_companyId = ( values["CompanyId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["CompanyId"], typeof(System.Int32)) : (int)0;
					_contactRole_nullable = (System.String) EntityUtil.ChangeType(values["ContactRole"], typeof(System.String));
					_contactTitle_nullable = (System.String) EntityUtil.ChangeType(values["ContactTitle"], typeof(System.String));
					_contactPhone_nullable = (System.String) EntityUtil.ChangeType(values["ContactPhone"], typeof(System.String));
					_contactMobile_nullable = (System.String) EntityUtil.ChangeType(values["ContactMobile"], typeof(System.String));
					_contactEmail_nullable = (System.String) EntityUtil.ChangeType(values["ContactEmail"], typeof(System.String));
					_siteId_nullable = (System.Int32?) EntityUtil.ChangeType(values["SiteId"], typeof(System.Int32?));
					item = ContactsContractorsProvider.GetByContactLastNameContactFirstNameCompanyIdContactRoleContactTitleContactPhoneContactMobileContactEmailSiteId(_contactLastName_nullable, _contactFirstName, _companyId, _contactRole_nullable, _contactTitle_nullable, _contactPhone_nullable, _contactMobile_nullable, _contactEmail_nullable, _siteId_nullable);
					results = new TList<ContactsContractors>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// FK
				case ContactsContractorsSelectMethod.GetByModifiedByUserId:
					_modifiedByUserId = ( values["ModifiedByUserId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["ModifiedByUserId"], typeof(System.Int32)) : (int)0;
					results = ContactsContractorsProvider.GetByModifiedByUserId(_modifiedByUserId, this.StartIndex, this.PageSize, out count);
					break;
				case ContactsContractorsSelectMethod.GetBySiteId:
					_siteId_nullable = (System.Int32?) EntityUtil.ChangeType(values["SiteId"], typeof(System.Int32?));
					results = ContactsContractorsProvider.GetBySiteId(_siteId_nullable, this.StartIndex, this.PageSize, out count);
					break;
				case ContactsContractorsSelectMethod.GetByRegionId:
					_regionId_nullable = (System.Int32?) EntityUtil.ChangeType(values["RegionId"], typeof(System.Int32?));
					results = ContactsContractorsProvider.GetByRegionId(_regionId_nullable, this.StartIndex, this.PageSize, out count);
					break;
				case ContactsContractorsSelectMethod.GetByCompanyId:
					_companyId = ( values["CompanyId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["CompanyId"], typeof(System.Int32)) : (int)0;
					results = ContactsContractorsProvider.GetByCompanyId(_companyId, this.StartIndex, this.PageSize, out count);
					break;
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == ContactsContractorsSelectMethod.Get || SelectMethod == ContactsContractorsSelectMethod.GetByContactId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				ContactsContractors entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					ContactsContractorsProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<ContactsContractors> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			ContactsContractorsProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region ContactsContractorsDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the ContactsContractorsDataSource class.
	/// </summary>
	public class ContactsContractorsDataSourceDesigner : ProviderDataSourceDesigner<ContactsContractors, ContactsContractorsKey>
	{
		/// <summary>
		/// Initializes a new instance of the ContactsContractorsDataSourceDesigner class.
		/// </summary>
		public ContactsContractorsDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public ContactsContractorsSelectMethod SelectMethod
		{
			get { return ((ContactsContractorsDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new ContactsContractorsDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region ContactsContractorsDataSourceActionList

	/// <summary>
	/// Supports the ContactsContractorsDataSourceDesigner class.
	/// </summary>
	internal class ContactsContractorsDataSourceActionList : DesignerActionList
	{
		private ContactsContractorsDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the ContactsContractorsDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public ContactsContractorsDataSourceActionList(ContactsContractorsDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public ContactsContractorsSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion ContactsContractorsDataSourceActionList
	
	#endregion ContactsContractorsDataSourceDesigner
	
	#region ContactsContractorsSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the ContactsContractorsDataSource.SelectMethod property.
	/// </summary>
	public enum ContactsContractorsSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByContactId method.
		/// </summary>
		GetByContactId,
		/// <summary>
		/// Represents the GetByContactLastNameContactFirstNameCompanyIdContactRoleContactTitleContactPhoneContactMobileContactEmailSiteId method.
		/// </summary>
		GetByContactLastNameContactFirstNameCompanyIdContactRoleContactTitleContactPhoneContactMobileContactEmailSiteId,
		/// <summary>
		/// Represents the GetByModifiedByUserId method.
		/// </summary>
		GetByModifiedByUserId,
		/// <summary>
		/// Represents the GetBySiteId method.
		/// </summary>
		GetBySiteId,
		/// <summary>
		/// Represents the GetByRegionId method.
		/// </summary>
		GetByRegionId,
		/// <summary>
		/// Represents the GetByCompanyId method.
		/// </summary>
		GetByCompanyId
	}
	
	#endregion ContactsContractorsSelectMethod

	#region ContactsContractorsFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ContactsContractors"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ContactsContractorsFilter : SqlFilter<ContactsContractorsColumn>
	{
	}
	
	#endregion ContactsContractorsFilter

	#region ContactsContractorsExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ContactsContractors"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ContactsContractorsExpressionBuilder : SqlExpressionBuilder<ContactsContractorsColumn>
	{
	}
	
	#endregion ContactsContractorsExpressionBuilder	

	#region ContactsContractorsProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;ContactsContractorsChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="ContactsContractors"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ContactsContractorsProperty : ChildEntityProperty<ContactsContractorsChildEntityTypes>
	{
	}
	
	#endregion ContactsContractorsProperty
}

