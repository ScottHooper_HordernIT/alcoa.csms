﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.CompanySiteCategoryExceptionProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(CompanySiteCategoryExceptionDataSourceDesigner))]
	public class CompanySiteCategoryExceptionDataSource : ProviderDataSource<CompanySiteCategoryException, CompanySiteCategoryExceptionKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompanySiteCategoryExceptionDataSource class.
		/// </summary>
		public CompanySiteCategoryExceptionDataSource() : base(new CompanySiteCategoryExceptionService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the CompanySiteCategoryExceptionDataSourceView used by the CompanySiteCategoryExceptionDataSource.
		/// </summary>
		protected CompanySiteCategoryExceptionDataSourceView CompanySiteCategoryExceptionView
		{
			get { return ( View as CompanySiteCategoryExceptionDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the CompanySiteCategoryExceptionDataSource control invokes to retrieve data.
		/// </summary>
		public CompanySiteCategoryExceptionSelectMethod SelectMethod
		{
			get
			{
				CompanySiteCategoryExceptionSelectMethod selectMethod = CompanySiteCategoryExceptionSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (CompanySiteCategoryExceptionSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the CompanySiteCategoryExceptionDataSourceView class that is to be
		/// used by the CompanySiteCategoryExceptionDataSource.
		/// </summary>
		/// <returns>An instance of the CompanySiteCategoryExceptionDataSourceView class.</returns>
		protected override BaseDataSourceView<CompanySiteCategoryException, CompanySiteCategoryExceptionKey> GetNewDataSourceView()
		{
			return new CompanySiteCategoryExceptionDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the CompanySiteCategoryExceptionDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class CompanySiteCategoryExceptionDataSourceView : ProviderDataSourceView<CompanySiteCategoryException, CompanySiteCategoryExceptionKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompanySiteCategoryExceptionDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the CompanySiteCategoryExceptionDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public CompanySiteCategoryExceptionDataSourceView(CompanySiteCategoryExceptionDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal CompanySiteCategoryExceptionDataSource CompanySiteCategoryExceptionOwner
		{
			get { return Owner as CompanySiteCategoryExceptionDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal CompanySiteCategoryExceptionSelectMethod SelectMethod
		{
			get { return CompanySiteCategoryExceptionOwner.SelectMethod; }
			set { CompanySiteCategoryExceptionOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal CompanySiteCategoryExceptionService CompanySiteCategoryExceptionProvider
		{
			get { return Provider as CompanySiteCategoryExceptionService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<CompanySiteCategoryException> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<CompanySiteCategoryException> results = null;
			CompanySiteCategoryException item;
			count = 0;
			
			System.Int32 _companySiteCategoryExceptionId;
			System.Int32 _companyId;
			System.Int32 _siteId;
			System.DateTime _monthYear;
			System.Int32 _companySiteCategoryId;

			switch ( SelectMethod )
			{
				case CompanySiteCategoryExceptionSelectMethod.Get:
					CompanySiteCategoryExceptionKey entityKey  = new CompanySiteCategoryExceptionKey();
					entityKey.Load(values);
					item = CompanySiteCategoryExceptionProvider.Get(entityKey);
					results = new TList<CompanySiteCategoryException>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case CompanySiteCategoryExceptionSelectMethod.GetAll:
                    results = CompanySiteCategoryExceptionProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case CompanySiteCategoryExceptionSelectMethod.GetPaged:
					results = CompanySiteCategoryExceptionProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case CompanySiteCategoryExceptionSelectMethod.Find:
					if ( FilterParameters != null )
						results = CompanySiteCategoryExceptionProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = CompanySiteCategoryExceptionProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case CompanySiteCategoryExceptionSelectMethod.GetByCompanySiteCategoryExceptionId:
					_companySiteCategoryExceptionId = ( values["CompanySiteCategoryExceptionId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["CompanySiteCategoryExceptionId"], typeof(System.Int32)) : (int)0;
					item = CompanySiteCategoryExceptionProvider.GetByCompanySiteCategoryExceptionId(_companySiteCategoryExceptionId);
					results = new TList<CompanySiteCategoryException>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				case CompanySiteCategoryExceptionSelectMethod.GetByCompanyIdSiteIdMonthYear:
					_companyId = ( values["CompanyId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["CompanyId"], typeof(System.Int32)) : (int)0;
					_siteId = ( values["SiteId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["SiteId"], typeof(System.Int32)) : (int)0;
					_monthYear = ( values["MonthYear"] != null ) ? (System.DateTime) EntityUtil.ChangeType(values["MonthYear"], typeof(System.DateTime)) : DateTime.MinValue;
					item = CompanySiteCategoryExceptionProvider.GetByCompanyIdSiteIdMonthYear(_companyId, _siteId, _monthYear);
					results = new TList<CompanySiteCategoryException>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case CompanySiteCategoryExceptionSelectMethod.GetByCompanyIdSiteId:
					_companyId = ( values["CompanyId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["CompanyId"], typeof(System.Int32)) : (int)0;
					_siteId = ( values["SiteId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["SiteId"], typeof(System.Int32)) : (int)0;
					results = CompanySiteCategoryExceptionProvider.GetByCompanyIdSiteId(_companyId, _siteId, this.StartIndex, this.PageSize, out count);
					break;
				// FK
				case CompanySiteCategoryExceptionSelectMethod.GetByCompanyId:
					_companyId = ( values["CompanyId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["CompanyId"], typeof(System.Int32)) : (int)0;
					results = CompanySiteCategoryExceptionProvider.GetByCompanyId(_companyId, this.StartIndex, this.PageSize, out count);
					break;
				case CompanySiteCategoryExceptionSelectMethod.GetByCompanySiteCategoryId:
					_companySiteCategoryId = ( values["CompanySiteCategoryId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["CompanySiteCategoryId"], typeof(System.Int32)) : (int)0;
					results = CompanySiteCategoryExceptionProvider.GetByCompanySiteCategoryId(_companySiteCategoryId, this.StartIndex, this.PageSize, out count);
					break;
				case CompanySiteCategoryExceptionSelectMethod.GetBySiteId:
					_siteId = ( values["SiteId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["SiteId"], typeof(System.Int32)) : (int)0;
					results = CompanySiteCategoryExceptionProvider.GetBySiteId(_siteId, this.StartIndex, this.PageSize, out count);
					break;
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == CompanySiteCategoryExceptionSelectMethod.Get || SelectMethod == CompanySiteCategoryExceptionSelectMethod.GetByCompanySiteCategoryExceptionId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				CompanySiteCategoryException entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					CompanySiteCategoryExceptionProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<CompanySiteCategoryException> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			CompanySiteCategoryExceptionProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region CompanySiteCategoryExceptionDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the CompanySiteCategoryExceptionDataSource class.
	/// </summary>
	public class CompanySiteCategoryExceptionDataSourceDesigner : ProviderDataSourceDesigner<CompanySiteCategoryException, CompanySiteCategoryExceptionKey>
	{
		/// <summary>
		/// Initializes a new instance of the CompanySiteCategoryExceptionDataSourceDesigner class.
		/// </summary>
		public CompanySiteCategoryExceptionDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public CompanySiteCategoryExceptionSelectMethod SelectMethod
		{
			get { return ((CompanySiteCategoryExceptionDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new CompanySiteCategoryExceptionDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region CompanySiteCategoryExceptionDataSourceActionList

	/// <summary>
	/// Supports the CompanySiteCategoryExceptionDataSourceDesigner class.
	/// </summary>
	internal class CompanySiteCategoryExceptionDataSourceActionList : DesignerActionList
	{
		private CompanySiteCategoryExceptionDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the CompanySiteCategoryExceptionDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public CompanySiteCategoryExceptionDataSourceActionList(CompanySiteCategoryExceptionDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public CompanySiteCategoryExceptionSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion CompanySiteCategoryExceptionDataSourceActionList
	
	#endregion CompanySiteCategoryExceptionDataSourceDesigner
	
	#region CompanySiteCategoryExceptionSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the CompanySiteCategoryExceptionDataSource.SelectMethod property.
	/// </summary>
	public enum CompanySiteCategoryExceptionSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByCompanySiteCategoryExceptionId method.
		/// </summary>
		GetByCompanySiteCategoryExceptionId,
		/// <summary>
		/// Represents the GetByCompanyIdSiteIdMonthYear method.
		/// </summary>
		GetByCompanyIdSiteIdMonthYear,
		/// <summary>
		/// Represents the GetByCompanyIdSiteId method.
		/// </summary>
		GetByCompanyIdSiteId,
		/// <summary>
		/// Represents the GetByCompanyId method.
		/// </summary>
		GetByCompanyId,
		/// <summary>
		/// Represents the GetByCompanySiteCategoryId method.
		/// </summary>
		GetByCompanySiteCategoryId,
		/// <summary>
		/// Represents the GetBySiteId method.
		/// </summary>
		GetBySiteId
	}
	
	#endregion CompanySiteCategoryExceptionSelectMethod

	#region CompanySiteCategoryExceptionFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CompanySiteCategoryException"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompanySiteCategoryExceptionFilter : SqlFilter<CompanySiteCategoryExceptionColumn>
	{
	}
	
	#endregion CompanySiteCategoryExceptionFilter

	#region CompanySiteCategoryExceptionExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CompanySiteCategoryException"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompanySiteCategoryExceptionExpressionBuilder : SqlExpressionBuilder<CompanySiteCategoryExceptionColumn>
	{
	}
	
	#endregion CompanySiteCategoryExceptionExpressionBuilder	

	#region CompanySiteCategoryExceptionProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;CompanySiteCategoryExceptionChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="CompanySiteCategoryException"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompanySiteCategoryExceptionProperty : ChildEntityProperty<CompanySiteCategoryExceptionChildEntityTypes>
	{
	}
	
	#endregion CompanySiteCategoryExceptionProperty
}

