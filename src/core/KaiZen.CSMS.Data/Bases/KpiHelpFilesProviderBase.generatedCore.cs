﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="FileVaultProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
    public abstract partial class KpiHelpFilesProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.KpiHelpFiles, KaiZen.CSMS.Entities.KpiHelpFilesKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
        public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.KpiHelpFilesKey key)
		{
            return Delete(transactionManager, key.KpiHelpFileId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_fileVaultId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
        public bool Delete(System.Int32 _kpiHelpFileId)
		{
            return Delete(null, _kpiHelpFileId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_fileVaultId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
        public abstract bool Delete(TransactionManager transactionManager, System.Int32 _kpiHelpFileId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_FileVault_Users_ModifiedBy key.
		///		FK_FileVault_Users_ModifiedBy Description: 
		/// </summary>
		/// <param name="_modifiedByUserId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.FileVault objects.</returns>
		public TList<KpiHelpFiles> GetByModifiedByUserId(System.Int32 _modifiedByUserId)
		{
			int count = -1;
			return GetByModifiedByUserId(_modifiedByUserId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_FileVault_Users_ModifiedBy key.
		///		FK_FileVault_Users_ModifiedBy Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_modifiedByUserId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.FileVault objects.</returns>
		/// <remarks></remarks>
        public TList<KpiHelpFiles> GetByModifiedByUserId(TransactionManager transactionManager, System.Int32 _modifiedByUserId)
		{
			int count = -1;
			return GetByModifiedByUserId(transactionManager, _modifiedByUserId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_FileVault_Users_ModifiedBy key.
		///		FK_FileVault_Users_ModifiedBy Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_modifiedByUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.FileVault objects.</returns>
        public TList<KpiHelpFiles> GetByModifiedByUserId(TransactionManager transactionManager, System.Int32 _modifiedByUserId, int start, int pageLength)
		{
			int count = -1;
			return GetByModifiedByUserId(transactionManager, _modifiedByUserId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_FileVault_Users_ModifiedBy key.
		///		fkFileVaultUsersModifiedBy Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_modifiedByUserId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.FileVault objects.</returns>
        public TList<KpiHelpFiles> GetByModifiedByUserId(System.Int32 _modifiedByUserId, int start, int pageLength)
		{
			int count =  -1;
			return GetByModifiedByUserId(null, _modifiedByUserId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_FileVault_Users_ModifiedBy key.
		///		fkFileVaultUsersModifiedBy Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_modifiedByUserId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.FileVault objects.</returns>
        public TList<KpiHelpFiles> GetByModifiedByUserId(System.Int32 _modifiedByUserId, int start, int pageLength, out int count)
		{
			return GetByModifiedByUserId(null, _modifiedByUserId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_FileVault_Users_ModifiedBy key.
		///		FK_FileVault_Users_ModifiedBy Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_modifiedByUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.FileVault objects.</returns>
        public abstract TList<KpiHelpFiles> GetByModifiedByUserId(TransactionManager transactionManager, System.Int32 _modifiedByUserId, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
        public override KaiZen.CSMS.Entities.KpiHelpFiles Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.KpiHelpFilesKey key, int start, int pageLength)
		{
            return GetByKpiHelpFileId(transactionManager, key.KpiHelpFileId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_FileVault index.
		/// </summary>
		/// <param name="_fileVaultId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.FileVault"/> class.</returns>
        public KaiZen.CSMS.Entities.KpiHelpFiles GetByKpiHelpFileId(System.Int32 _kpiHelpFileId)
		{
			int count = -1;
            return GetByKpiHelpFileId(null, _kpiHelpFileId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_FileVault index.
		/// </summary>
		/// <param name="_fileVaultId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.FileVault"/> class.</returns>
        public KaiZen.CSMS.Entities.KpiHelpFiles GetByKpiHelpFileId(System.Int32 _kpiHelpFileId, int start, int pageLength)
		{
			int count = -1;
            return GetByKpiHelpFileId(null, _kpiHelpFileId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_FileVault index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_fileVaultId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.FileVault"/> class.</returns>
        public KaiZen.CSMS.Entities.KpiHelpFiles GetByKpiHelpFileId(TransactionManager transactionManager, System.Int32 _kpiHelpFileId)
		{
			int count = -1;
            return GetByKpiHelpFileId(transactionManager, _kpiHelpFileId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_FileVault index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_fileVaultId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.FileVault"/> class.</returns>
        public KaiZen.CSMS.Entities.KpiHelpFiles GetByKpiHelpFileId(TransactionManager transactionManager, System.Int32 _kpiHelpFileId, int start, int pageLength)
		{
			int count = -1;
            return GetByKpiHelpFileId(transactionManager, _kpiHelpFileId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_FileVault index.
		/// </summary>
		/// <param name="_fileVaultId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.FileVault"/> class.</returns>
        public KaiZen.CSMS.Entities.KpiHelpFiles GetByKpiHelpFileId(System.Int32 _kpiHelpFileId, int start, int pageLength, out int count)
		{
            return GetByKpiHelpFileId(null, _kpiHelpFileId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_FileVault index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_fileVaultId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.FileVault"/> class.</returns>
        public abstract KaiZen.CSMS.Entities.KpiHelpFiles GetByKpiHelpFileId(TransactionManager transactionManager, System.Int32 _kpiHelpFileId, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_FileVaultCategory index.
		/// </summary>
		/// <param name="_fileVaultCategoryId"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;FileVault&gt;"/> class.</returns>
        public TList<KpiHelpFiles> GetByKpiHelpCaption(System.String _kpiHelpCaption)
		{
			int count = -1;
            return GetByKpiHelpCaption(null, _kpiHelpCaption, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_FileVaultCategory index.
		/// </summary>
		/// <param name="_fileVaultCategoryId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;FileVault&gt;"/> class.</returns>
        public TList<KpiHelpFiles> GetByKpiHelpCaption(System.String _kpiHelpCaption, int start, int pageLength)
		{
			int count = -1;
            return GetByKpiHelpCaption(null, _kpiHelpCaption, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_FileVaultCategory index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_fileVaultCategoryId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;FileVault&gt;"/> class.</returns>
        public TList<KpiHelpFiles> GetByKpiHelpCaption(TransactionManager transactionManager, System.String _kpiHelpCaption)
		{
			int count = -1;
            return GetByKpiHelpCaption(transactionManager, _kpiHelpCaption, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_FileVaultCategory index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_fileVaultCategoryId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;FileVault&gt;"/> class.</returns>
        public TList<KpiHelpFiles> GetByKpiHelpCaption(TransactionManager transactionManager, System.String _kpiHelpCaption, int start, int pageLength)
		{
			int count = -1;
            return GetByKpiHelpCaption(transactionManager, _kpiHelpCaption, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_FileVaultCategory index.
		/// </summary>
		/// <param name="_fileVaultCategoryId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;FileVault&gt;"/> class.</returns>
        public TList<KpiHelpFiles> GetByKpiHelpCaption(System.String _kpiHelpCaption, int start, int pageLength, out int count)
		{
            return GetByKpiHelpCaption(null, _kpiHelpCaption, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_FileVaultCategory index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_fileVaultCategoryId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;FileVault&gt;"/> class.</returns>
        public abstract TList<KpiHelpFiles> GetByKpiHelpCaption(TransactionManager transactionManager, System.String _kpiHelpCaption, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;FileVault&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;FileVault&gt;"/></returns>
        public static TList<KpiHelpFiles> Fill(IDataReader reader, TList<KpiHelpFiles> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;

                KaiZen.CSMS.Entities.KpiHelpFiles c = null;
				if (useEntityFactory)
				{
                    key = new System.Text.StringBuilder("KpiHelpFiles")
                    .Append("|").Append((System.Int32)reader[((int)KpiHelpFilesColumn.KpiHelpFileId - 1)]).ToString();
                    c = EntityManager.LocateOrCreate<KpiHelpFiles>(
					key.ToString(), // EntityTrackingKey
                    "KpiHelpFiles",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
                    c = new KaiZen.CSMS.Entities.KpiHelpFiles();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
                    c.KpiHelpFileId = (System.Int32)reader[((int)KpiHelpFilesColumn.KpiHelpFileId - 1)];
                    c.KpiHelpCaption = (System.String)reader[((int)KpiHelpFilesColumn.KpiHelpCaption - 1)];
                    c.FileName = (System.String)reader[((int)KpiHelpFilesColumn.FileName - 1)];
                    c.FileHash = (System.Byte[])reader[((int)KpiHelpFilesColumn.FileHash - 1)];
                    c.ContentLength = (System.Int32)reader[((int)KpiHelpFilesColumn.ContentLength - 1)];
                    c.Content = (System.Byte[])reader[((int)KpiHelpFilesColumn.Content - 1)];
                    c.ModifiedDate = (System.DateTime)reader[((int)KpiHelpFilesColumn.ModifiedDate - 1)];
                    c.ModifiedByUserId = (System.Int32)reader[((int)KpiHelpFilesColumn.ModifiedByUserId - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.FileVault"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.FileVault"/> object to refresh.</param>
        public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.KpiHelpFiles entity)
		{
			if (!reader.Read()) return;

            entity.KpiHelpFileId = (System.Int32)reader[((int)KpiHelpFilesColumn.KpiHelpFileId - 1)];
            entity.KpiHelpCaption = (System.String)reader[((int)KpiHelpFilesColumn.KpiHelpCaption - 1)];
            entity.FileName = (System.String)reader[((int)KpiHelpFilesColumn.FileName - 1)];
            entity.FileHash = (System.Byte[])reader[((int)KpiHelpFilesColumn.FileHash - 1)];
            entity.ContentLength = (System.Int32)reader[((int)KpiHelpFilesColumn.ContentLength - 1)];
            entity.Content = (System.Byte[])reader[((int)KpiHelpFilesColumn.Content - 1)];
            entity.ModifiedDate = (System.DateTime)reader[((int)KpiHelpFilesColumn.ModifiedDate - 1)];
            entity.ModifiedByUserId = (System.Int32)reader[((int)KpiHelpFilesColumn.ModifiedByUserId - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.FileVault"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.FileVault"/> object.</param>
        public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.KpiHelpFiles entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];

            entity.KpiHelpFileId = (System.Int32)dataRow["KpiHelpFileId"];
            entity.KpiHelpCaption = (System.String)dataRow["KpiHelpCaption"];
			entity.FileName = (System.String)dataRow["FileName"];
			entity.FileHash = (System.Byte[])dataRow["FileHash"];
			entity.ContentLength = (System.Int32)dataRow["ContentLength"];
			entity.Content = (System.Byte[])dataRow["Content"];
			entity.ModifiedDate = (System.DateTime)dataRow["ModifiedDate"];
			entity.ModifiedByUserId = (System.Int32)dataRow["ModifiedByUserId"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.FileVault"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.FileVault Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
        public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.KpiHelpFiles entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region ModifiedByUserIdSource	
			if (CanDeepLoad(entity, "Users|ModifiedByUserIdSource", deepLoadType, innerList) 
				&& entity.ModifiedByUserIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.ModifiedByUserId;
				Users tmpEntity = EntityManager.LocateEntity<Users>(EntityLocator.ConstructKeyFromPkItems(typeof(Users), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.ModifiedByUserIdSource = tmpEntity;
				else
					entity.ModifiedByUserIdSource = DataRepository.UsersProvider.GetByUserId(transactionManager, entity.ModifiedByUserId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ModifiedByUserIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.ModifiedByUserIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.UsersProvider.DeepLoad(transactionManager, entity.ModifiedByUserIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion ModifiedByUserIdSource

            //#region FileVaultCategoryIdSource	
            //if (CanDeepLoad(entity, "FileVaultCategory|FileVaultCategoryIdSource", deepLoadType, innerList) 
            //    && entity.FileVaultCategoryIdSource == null)
            //{
            //    object[] pkItems = new object[1];
            //    pkItems[0] = entity.FileVaultCategoryId;
            //    FileVaultCategory tmpEntity = EntityManager.LocateEntity<FileVaultCategory>(EntityLocator.ConstructKeyFromPkItems(typeof(FileVaultCategory), pkItems), DataRepository.Provider.EnableEntityTracking);
            //    if (tmpEntity != null)
            //        entity.FileVaultCategoryIdSource = tmpEntity;
            //    else
            //        entity.FileVaultCategoryIdSource = DataRepository.FileVaultCategoryProvider.GetByFileVaultCategoryId(transactionManager, entity.FileVaultCategoryId);		
				
            //    #if NETTIERS_DEBUG
            //    System.Diagnostics.Debug.WriteLine("- property 'FileVaultCategoryIdSource' loaded. key " + entity.EntityTrackingKey);
            //    #endif 
				
            //    if (deep && entity.FileVaultCategoryIdSource != null)
            //    {
            //        innerList.SkipChildren = true;
            //        DataRepository.FileVaultCategoryProvider.DeepLoad(transactionManager, entity.FileVaultCategoryIdSource, deep, deepLoadType, childTypes, innerList);
            //        innerList.SkipChildren = false;
            //    }
					
            //}
            //#endregion FileVaultCategoryIdSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetByFileVaultId methods when available
			
            //#region FileVaultTableCollection
            ////Relationship Type One : Many
            //if (CanDeepLoad(entity, "List<FileVaultTable>|FileVaultTableCollection", deepLoadType, innerList)) 
            //{
            //    #if NETTIERS_DEBUG
            //    System.Diagnostics.Debug.WriteLine("- property 'FileVaultTableCollection' loaded. key " + entity.EntityTrackingKey);
            //    #endif 

            //    entity.FileVaultTableCollection = DataRepository.FileVaultTableProvider.GetByFileVaultId(transactionManager, entity.FileVaultId);

            //    if (deep && entity.FileVaultTableCollection.Count > 0)
            //    {
            //        deepHandles.Add("FileVaultTableCollection",
            //            new KeyValuePair<Delegate, object>((DeepLoadHandle<FileVaultTable>) DataRepository.FileVaultTableProvider.DeepLoad,
            //            new object[] { transactionManager, entity.FileVaultTableCollection, deep, deepLoadType, childTypes, innerList }
            //        ));
            //    }
            //}		
            //#endregion 
			
			
			#region SqExemptionCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<SqExemption>|SqExemptionCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'SqExemptionCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

                entity.SqExemptionCollection = DataRepository.SqExemptionProvider.GetByFileVaultId(transactionManager, entity.KpiHelpFileId);

				if (deep && entity.SqExemptionCollection.Count > 0)
				{
					deepHandles.Add("SqExemptionCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<SqExemption>) DataRepository.SqExemptionProvider.DeepLoad,
						new object[] { transactionManager, entity.SqExemptionCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.FileVault object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.FileVault instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.FileVault Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.KpiHelpFiles entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region ModifiedByUserIdSource
			if (CanDeepSave(entity, "Users|ModifiedByUserIdSource", deepSaveType, innerList) 
				&& entity.ModifiedByUserIdSource != null)
			{
				DataRepository.UsersProvider.Save(transactionManager, entity.ModifiedByUserIdSource);
				entity.ModifiedByUserId = entity.ModifiedByUserIdSource.UserId;
			}
			#endregion 
			
            //#region FileVaultCategoryIdSource
            //if (CanDeepSave(entity, "FileVaultCategory|FileVaultCategoryIdSource", deepSaveType, innerList) 
            //    && entity.FileVaultCategoryIdSource != null)
            //{
            //    DataRepository.FileVaultCategoryProvider.Save(transactionManager, entity.FileVaultCategoryIdSource);
            //    entity.FileVaultCategoryId = entity.FileVaultCategoryIdSource.FileVaultCategoryId;
            //}
            //#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
            //#region List<FileVaultTable>
            //    if (CanDeepSave(entity.FileVaultTableCollection, "List<FileVaultTable>|FileVaultTableCollection", deepSaveType, innerList)) 
            //    {	
            //        // update each child parent id with the real parent id (mostly used on insert)
            //        foreach(FileVaultTable child in entity.FileVaultTableCollection)
            //        {
            //            if(child.FileVaultIdSource != null)
            //            {
            //                child.FileVaultId = child.FileVaultIdSource.FileVaultId;
            //            }
            //            else
            //            {
            //                child.FileVaultId = entity.FileVaultId;
            //            }

            //        }

            //        if (entity.FileVaultTableCollection.Count > 0 || entity.FileVaultTableCollection.DeletedItems.Count > 0)
            //        {
            //            //DataRepository.FileVaultTableProvider.Save(transactionManager, entity.FileVaultTableCollection);
						
            //            deepHandles.Add("FileVaultTableCollection",
            //            new KeyValuePair<Delegate, object>((DeepSaveHandle< FileVaultTable >) DataRepository.FileVaultTableProvider.DeepSave,
            //                new object[] { transactionManager, entity.FileVaultTableCollection, deepSaveType, childTypes, innerList }
            //            ));
            //        }
            //    } 
            //#endregion 
				
	
			#region List<SqExemption>
                if (CanDeepSave(entity.SqExemptionCollection, "List<SqExemption>|SqExemptionCollection", deepSaveType, innerList)) 
                {	
                //    // update each child parent id with the real parent id (mostly used on insert)
                //    foreach(SqExemption child in entity.SqExemptionCollection)
                //    {
                //        if (child.KpiHelpFileIdSource != null)
                //        {
                //            child.KpiHelpFileId = child.KpiHelpFileIdSource.KpiHelpFileId;
                //        }
                //        else
                //        {
                //            child.KpiHelpFileId = entity.KpiHelpFileId;
                //        }

                //    }

					if (entity.SqExemptionCollection.Count > 0 || entity.SqExemptionCollection.DeletedItems.Count > 0)
					{
						//DataRepository.SqExemptionProvider.Save(transactionManager, entity.SqExemptionCollection);
						
						deepHandles.Add("SqExemptionCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< SqExemption >) DataRepository.SqExemptionProvider.DeepSave,
							new object[] { transactionManager, entity.SqExemptionCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region FileVaultChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.FileVault</c>
	///</summary>
	public enum KpiHelpFilesChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>Users</c> at ModifiedByUserIdSource
		///</summary>
		[ChildEntityType(typeof(Users))]
		Users,
			
		///<summary>
		/// Composite Property for <c>FileVaultCategory</c> at FileVaultCategoryIdSource
		///</summary>
        //[ChildEntityType(typeof(KpiHelpCaption))]
        //KpiHelpCaption,
	
		///<summary>
		/// Collection of <c>FileVault</c> as OneToMany for FileVaultTableCollection
		///</summary>
        //[ChildEntityType(typeof(TList<FileVaultTable>))]
        //FileVaultTableCollection,

		///<summary>
		/// Collection of <c>FileVault</c> as OneToMany for SqExemptionCollection
		///</summary>
		[ChildEntityType(typeof(TList<SqExemption>))]
		SqExemptionCollection,
	}
	
	#endregion FileVaultChildEntityTypes
	
	#region FileVaultFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;FileVaultColumn&gt;"/> class
	/// that is used exclusively with a <see cref="FileVault"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class KpiHelpFilesFilterBuilder : SqlFilterBuilder<KpiHelpFilesColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FileVaultFilterBuilder class.
		/// </summary>
		public KpiHelpFilesFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the FileVaultFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public KpiHelpFilesFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the FileVaultFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
        public KpiHelpFilesFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion FileVaultFilterBuilder
	
	#region FileVaultParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;FileVaultColumn&gt;"/> class
	/// that is used exclusively with a <see cref="FileVault"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class KpiHelpFilesParameterBuilder : ParameterizedSqlFilterBuilder<KpiHelpFilesColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FileVaultParameterBuilder class.
		/// </summary>
		public KpiHelpFilesParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the FileVaultParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public KpiHelpFilesParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the FileVaultParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
        public KpiHelpFilesParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion FileVaultParameterBuilder
	
	#region FileVaultSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;FileVaultColumn&gt;"/> class
	/// that is used exclusively with a <see cref="FileVault"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class KpiHelpFilesSortBuilder : SqlSortBuilder<KpiHelpFilesColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FileVaultSqlSortBuilder class.
		/// </summary>
        public KpiHelpFilesSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion FileVaultSortBuilder
	
} // end namespace
