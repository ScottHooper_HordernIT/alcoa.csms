﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/popup.master" AutoEventWireup="true"
    CodeBehind="EditSqExemption.aspx.cs" Inherits="ALCOA.CSMS.Website.PopUps.EditSqExemption" %>

<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxUploadControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxRoundPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script language="javascript" type="text/javascript">
        function closePopup() { window.close(); }

        window.onunload = function () {
            if (window.opener && !window.opener.closed) {
                window.opener.popUpClosed();
            }
        };
    </script>
    <dx:ASPxRoundPanel ID="ASPxRoundPanel1" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
        CssPostfix="Office2003Blue" EnableDefaultAppearance="False" GroupBoxCaptionOffsetY="-25px"
        HeaderText="Authority to Access Site (Safety Qualification Exemption)" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
        Width="700px">
        <ContentPaddings Padding="2px" PaddingBottom="10px" PaddingTop="10px" />
        <HeaderStyle>
            <Paddings Padding="0px" PaddingBottom="7px" PaddingLeft="2px" PaddingRight="2px" />
        </HeaderStyle>
        <PanelCollection>
            <dx:PanelContent runat="server" SupportsDisabledAttribute="True">
                <dx:ASPxLabel ID="lblStatus" runat="server" ForeColor="Red">
                </dx:ASPxLabel>
                <br />
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="width: 120px; text-align: right;">
                            &nbsp;
                        </td>
                        <td style="width: 190px;">
                            &nbsp;
                        </td>
                        <td style="width: 10px;">
                            &nbsp;
                        </td>
                        <td style="width: 120px; text-align: right;">
                            Date Applied:
                        </td>
                        <td style="width: 190px;">
                            <dx:ASPxDateEdit ID="deDateApplied" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                <ButtonStyle Width="13px">
                                </ButtonStyle>
                                <ValidationSettings CausesValidation="True" Display="Dynamic" ErrorDisplayMode="ImageWithTooltip"
                                    ValidationGroup="AddUpload">
                                    <RequiredField IsRequired="True" />
                                </ValidationSettings>
                            </dx:ASPxDateEdit>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 120px; text-align: right;">
                            &nbsp;
                        </td>
                        <td style="width: 190px;">
                            &nbsp;
                        </td>
                        <td style="width: 10px;">
                            &nbsp;
                        </td>
                        <td style="width: 120px; text-align: right;">
                            &nbsp;
                        </td>
                        <td style="width: 190px;">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 120px; text-align: right;">
                            Valid From:
                        </td>
                        <td style="width: 190px; padding-bottom: 2px;">
                            <dx:ASPxDateEdit ID="deValidFrom" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                <ButtonStyle Width="13px">
                                </ButtonStyle>
                                <ValidationSettings CausesValidation="True" ValidationGroup="AddUpload" ErrorDisplayMode="ImageWithTooltip">
                                    <RequiredField IsRequired="True" />
                                </ValidationSettings>
                            </dx:ASPxDateEdit>
                        </td>
                        <td style="width: 10px;">
                            &nbsp;
                        </td>
                        <td style="width: 120px; text-align: right;">
                            Valid To:
                        </td>
                        <td style="width: 190px; padding-bottom: 2px;">
                            <dx:ASPxDateEdit ID="deValidTo" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                <ButtonStyle Width="13px">
                                </ButtonStyle>
                                <ValidationSettings CausesValidation="True" Display="Dynamic" ErrorDisplayMode="ImageWithTooltip"
                                    ValidationGroup="AddUpload">
                                    <RequiredField IsRequired="True" />
                                </ValidationSettings>
                            </dx:ASPxDateEdit>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 120px; text-align: right;">
                            Company:
                        </td>
                        <td style="width: 190px; padding-bottom: 2px;">
                            <dx:ASPxComboBox ID="cbCompany" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                CssPostfix="Office2003Blue" DataSourceID="dsCompanies" Enabled="False" ReadOnly="True"
                                SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" TextField="CompanyName"
                                ValueField="CompanyId" ValueType="System.Int32">
                                <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                                </LoadingPanelImage>
                                <ButtonStyle Width="13px">
                                </ButtonStyle>
                                <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip">
                                    <RequiredField IsRequired="True" />
                                </ValidationSettings>
                            </dx:ASPxComboBox>
                        </td>
                        <td style="width: 10px;">
                            &nbsp;
                        </td>
                        <td style="width: 120px; text-align: right;">
                            Site:
                        </td>
                        <td style="width: 190px; padding-bottom: 2px;">
                            <dx:ASPxComboBox ID="cbSite" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                CssPostfix="Office2003Blue" DataSourceID="dsSitesFilter" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                TextField="SiteName" ValueField="SiteId" ValueType="System.Int32" EnableIncrementalFiltering="True"
                                IncrementalFilteringMode="StartsWith">
                                <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                                </LoadingPanelImage>
                                <ButtonStyle Width="13px">
                                </ButtonStyle>
                                <ValidationSettings CausesValidation="True" Display="Dynamic" ErrorDisplayMode="ImageWithTooltip"
                                    ValidationGroup="AddUpload">
                                    <RequiredField IsRequired="True" />
                                </ValidationSettings>
                            </dx:ASPxComboBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 120px; text-align: right;">
                            Company Type:
                        </td>
                        <td style="width: 190px;">
                            <dx:ASPxComboBox ID="cbCompanyType" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                CssPostfix="Office2003Blue" DataSourceID="CompanyStatus2DataSource1" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                TextField="CompanyStatusDesc" ValueField="CompanyStatusId" ValueType="System.Int32"
                                EnableIncrementalFiltering="True" IncrementalFilteringMode="StartsWith">
                                <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                                </LoadingPanelImage>
                                <ButtonStyle Width="13px">
                                </ButtonStyle>
                                <ValidationSettings CausesValidation="True" ValidationGroup="AddUpload" ErrorDisplayMode="ImageWithTooltip">
                                    <RequiredField IsRequired="True" />
                                </ValidationSettings>
                            </dx:ASPxComboBox>
                        </td>
                        <td style="width: 10px;">
                            &nbsp;
                        </td>
                        <td style="width: 120px; text-align: right;">
                            Requested By:
                        </td>
                        <td style="width: 190px;">
                            <dx:ASPxComboBox ID="cbRequestingCompany" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                CssPostfix="Office2003Blue" DataSourceID="dsCompanies" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                TextField="CompanyName" ValueField="CompanyId" ValueType="System.Int32" EnableIncrementalFiltering="True"
                                IncrementalFilteringMode="StartsWith">
                                <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                                </LoadingPanelImage>
                                <ButtonStyle Width="13px">
                                </ButtonStyle>
                                <ValidationSettings CausesValidation="True" Display="Dynamic" ErrorDisplayMode="ImageWithTooltip"
                                    ValidationGroup="AddUpload">
                                    <RequiredField IsRequired="True" />
                                </ValidationSettings>
                            </dx:ASPxComboBox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5" style="padding-top: 10px">
                            Uploaded Scanned Copy of Safety Qualification Exemption Form:
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5">
                            <dx:ASPxUploadControl ID="ucNewFile" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                CssPostfix="Office2003Blue" ShowProgressPanel="True" Width="605px">
                                <ValidationSettings AllowedFileExtensions=".txt, .pdf, .xls, .xlsx, .doc, .docx, .ppt, .pptx, .zip, .jpg, .jpeg, .png"
                                    GeneralErrorText="File Upload Failed." MaxFileSize="26252928" MaxFileSizeErrorText="File size exceeds the maximum allowed size of 25Mb (maybe consider ZIPing the file?)"
                                    NotAllowedFileExtensionErrorText="File NOT uploaded. Please upload only files of the following file type. All Other file types will not be uploaded: Text (TXT), Excel (XLS, XLSX), Word Document (DOC, DOCX), PowerPoint (PPT, PPTX), Adobe PDF (PDF), ZIP Compressed File (ZIP), Image (JPG/PNG)">
                                </ValidationSettings>
                            </dx:ASPxUploadControl>
                            <dx:ASPxLabel ID="lblCurrentFile" runat="server" ForeColor="Red" Text="The following file is currently saved. Uploading another file will overwrite this: "
                                Visible="False">
                            </dx:ASPxLabel>
                            <dx:ASPxHyperLink ID="hlCurrentFile" runat="server" Text="-" Visible="False">
                            </dx:ASPxHyperLink>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5">
                            <dx:ASPxLabel ID="lblFileUploadMsg" runat="server" ForeColor="Red" Style="font-weight: 700">
                            </dx:ASPxLabel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5" style="text-align: center;">
                            <div align="center" style="text-align: center">
                                <table cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td>
                                            <dx:ASPxButton ID="btnUpload" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                CssPostfix="Office2003Blue" OnClick="btnUpload_Click" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                Text="Add / Upload" ValidationGroup="AddUpload">
                                            </dx:ASPxButton>
                                        </td>
                                        <td>
                                            <dx:ASPxButton ID="btnDelete" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                CssPostfix="Office2003Blue" Enabled="False" OnClick="btnDelete_Click" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                Text="Delete" Visible="False">
                                                <ClientSideEvents Click="function(s, e) {
	e.processOnServer = confirm('Are you sure you wish to delete?');
}" />
                                            </dx:ASPxButton>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5" style="text-align: right;">
                            <dx:ASPxLabel ID="lblModified" runat="server" Font-Size="Smaller">
                            </dx:ASPxLabel>
                        </td>
                    </tr>
                </table>
                <br />
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxRoundPanel>
    <data:CompanyStatus2DataSource ID="CompanyStatus2DataSource1" runat="server">
    </data:CompanyStatus2DataSource>
    <data:CompaniesDataSource ID="dsCompanies" runat="server" Filter="" Sort="CompanyName ASC">
    </data:CompaniesDataSource>
    <data:SitesDataSource ID="dsSitesFilter" runat="server" Filter="IsVisible = True AND Editable = True"
        InsertMethod="Insert" SelectMethod="GetAll" Sort="SiteName ASC">
    </data:SitesDataSource>
    <br />
</asp:Content>
