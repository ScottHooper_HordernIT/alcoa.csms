﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="CsmsAccessProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class CsmsAccessProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.CsmsAccess, KaiZen.CSMS.Entities.CsmsAccessKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.CsmsAccessKey key)
		{
			return Delete(transactionManager, key.CsmsAccessId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_csmsAccessId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _csmsAccessId)
		{
			return Delete(null, _csmsAccessId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_csmsAccessId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _csmsAccessId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.CsmsAccess Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.CsmsAccessKey key, int start, int pageLength)
		{
			return GetByCsmsAccessId(transactionManager, key.CsmsAccessId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_CsmsAccess index.
		/// </summary>
		/// <param name="_csmsAccessId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CsmsAccess"/> class.</returns>
		public KaiZen.CSMS.Entities.CsmsAccess GetByCsmsAccessId(System.Int32 _csmsAccessId)
		{
			int count = -1;
			return GetByCsmsAccessId(null,_csmsAccessId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CsmsAccess index.
		/// </summary>
		/// <param name="_csmsAccessId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CsmsAccess"/> class.</returns>
		public KaiZen.CSMS.Entities.CsmsAccess GetByCsmsAccessId(System.Int32 _csmsAccessId, int start, int pageLength)
		{
			int count = -1;
			return GetByCsmsAccessId(null, _csmsAccessId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CsmsAccess index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_csmsAccessId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CsmsAccess"/> class.</returns>
		public KaiZen.CSMS.Entities.CsmsAccess GetByCsmsAccessId(TransactionManager transactionManager, System.Int32 _csmsAccessId)
		{
			int count = -1;
			return GetByCsmsAccessId(transactionManager, _csmsAccessId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CsmsAccess index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_csmsAccessId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CsmsAccess"/> class.</returns>
		public KaiZen.CSMS.Entities.CsmsAccess GetByCsmsAccessId(TransactionManager transactionManager, System.Int32 _csmsAccessId, int start, int pageLength)
		{
			int count = -1;
			return GetByCsmsAccessId(transactionManager, _csmsAccessId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CsmsAccess index.
		/// </summary>
		/// <param name="_csmsAccessId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CsmsAccess"/> class.</returns>
		public KaiZen.CSMS.Entities.CsmsAccess GetByCsmsAccessId(System.Int32 _csmsAccessId, int start, int pageLength, out int count)
		{
			return GetByCsmsAccessId(null, _csmsAccessId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CsmsAccess index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_csmsAccessId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CsmsAccess"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.CsmsAccess GetByCsmsAccessId(TransactionManager transactionManager, System.Int32 _csmsAccessId, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key UK_CsmsAccessName index.
		/// </summary>
		/// <param name="_accessName"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CsmsAccess"/> class.</returns>
		public KaiZen.CSMS.Entities.CsmsAccess GetByAccessName(System.String _accessName)
		{
			int count = -1;
			return GetByAccessName(null,_accessName, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the UK_CsmsAccessName index.
		/// </summary>
		/// <param name="_accessName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CsmsAccess"/> class.</returns>
		public KaiZen.CSMS.Entities.CsmsAccess GetByAccessName(System.String _accessName, int start, int pageLength)
		{
			int count = -1;
			return GetByAccessName(null, _accessName, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the UK_CsmsAccessName index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_accessName"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CsmsAccess"/> class.</returns>
		public KaiZen.CSMS.Entities.CsmsAccess GetByAccessName(TransactionManager transactionManager, System.String _accessName)
		{
			int count = -1;
			return GetByAccessName(transactionManager, _accessName, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the UK_CsmsAccessName index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_accessName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CsmsAccess"/> class.</returns>
		public KaiZen.CSMS.Entities.CsmsAccess GetByAccessName(TransactionManager transactionManager, System.String _accessName, int start, int pageLength)
		{
			int count = -1;
			return GetByAccessName(transactionManager, _accessName, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the UK_CsmsAccessName index.
		/// </summary>
		/// <param name="_accessName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CsmsAccess"/> class.</returns>
		public KaiZen.CSMS.Entities.CsmsAccess GetByAccessName(System.String _accessName, int start, int pageLength, out int count)
		{
			return GetByAccessName(null, _accessName, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the UK_CsmsAccessName index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_accessName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CsmsAccess"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.CsmsAccess GetByAccessName(TransactionManager transactionManager, System.String _accessName, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;CsmsAccess&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;CsmsAccess&gt;"/></returns>
		public static TList<CsmsAccess> Fill(IDataReader reader, TList<CsmsAccess> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.CsmsAccess c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("CsmsAccess")
					.Append("|").Append((System.Int32)reader[((int)CsmsAccessColumn.CsmsAccessId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<CsmsAccess>(
					key.ToString(), // EntityTrackingKey
					"CsmsAccess",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.CsmsAccess();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.CsmsAccessId = (System.Int32)reader[((int)CsmsAccessColumn.CsmsAccessId - 1)];
					c.AccessName = (System.String)reader[((int)CsmsAccessColumn.AccessName - 1)];
					c.AccessDesc = (System.String)reader[((int)CsmsAccessColumn.AccessDesc - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.CsmsAccess"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.CsmsAccess"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.CsmsAccess entity)
		{
			if (!reader.Read()) return;
			
			entity.CsmsAccessId = (System.Int32)reader[((int)CsmsAccessColumn.CsmsAccessId - 1)];
			entity.AccessName = (System.String)reader[((int)CsmsAccessColumn.AccessName - 1)];
			entity.AccessDesc = (System.String)reader[((int)CsmsAccessColumn.AccessDesc - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.CsmsAccess"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.CsmsAccess"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.CsmsAccess entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.CsmsAccessId = (System.Int32)dataRow["CsmsAccessId"];
			entity.AccessName = (System.String)dataRow["AccessName"];
			entity.AccessDesc = (System.String)dataRow["AccessDesc"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.CsmsAccess"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.CsmsAccess Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.CsmsAccess entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetByCsmsAccessId methods when available
			
			#region AdminTaskCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<AdminTask>|AdminTaskCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'AdminTaskCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.AdminTaskCollection = DataRepository.AdminTaskProvider.GetByCsmsAccessId(transactionManager, entity.CsmsAccessId);

				if (deep && entity.AdminTaskCollection.Count > 0)
				{
					deepHandles.Add("AdminTaskCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<AdminTask>) DataRepository.AdminTaskProvider.DeepLoad,
						new object[] { transactionManager, entity.AdminTaskCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.CsmsAccess object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.CsmsAccess instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.CsmsAccess Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.CsmsAccess entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<AdminTask>
				if (CanDeepSave(entity.AdminTaskCollection, "List<AdminTask>|AdminTaskCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(AdminTask child in entity.AdminTaskCollection)
					{
						if(child.CsmsAccessIdSource != null)
						{
							child.CsmsAccessId = child.CsmsAccessIdSource.CsmsAccessId;
						}
						else
						{
							child.CsmsAccessId = entity.CsmsAccessId;
						}

					}

					if (entity.AdminTaskCollection.Count > 0 || entity.AdminTaskCollection.DeletedItems.Count > 0)
					{
						//DataRepository.AdminTaskProvider.Save(transactionManager, entity.AdminTaskCollection);
						
						deepHandles.Add("AdminTaskCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< AdminTask >) DataRepository.AdminTaskProvider.DeepSave,
							new object[] { transactionManager, entity.AdminTaskCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region CsmsAccessChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.CsmsAccess</c>
	///</summary>
	public enum CsmsAccessChildEntityTypes
	{

		///<summary>
		/// Collection of <c>CsmsAccess</c> as OneToMany for AdminTaskCollection
		///</summary>
		[ChildEntityType(typeof(TList<AdminTask>))]
		AdminTaskCollection,
	}
	
	#endregion CsmsAccessChildEntityTypes
	
	#region CsmsAccessFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;CsmsAccessColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CsmsAccess"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CsmsAccessFilterBuilder : SqlFilterBuilder<CsmsAccessColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CsmsAccessFilterBuilder class.
		/// </summary>
		public CsmsAccessFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the CsmsAccessFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CsmsAccessFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CsmsAccessFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CsmsAccessFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CsmsAccessFilterBuilder
	
	#region CsmsAccessParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;CsmsAccessColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CsmsAccess"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CsmsAccessParameterBuilder : ParameterizedSqlFilterBuilder<CsmsAccessColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CsmsAccessParameterBuilder class.
		/// </summary>
		public CsmsAccessParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the CsmsAccessParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CsmsAccessParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CsmsAccessParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CsmsAccessParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CsmsAccessParameterBuilder
	
	#region CsmsAccessSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;CsmsAccessColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CsmsAccess"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class CsmsAccessSortBuilder : SqlSortBuilder<CsmsAccessColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CsmsAccessSqlSortBuilder class.
		/// </summary>
		public CsmsAccessSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion CsmsAccessSortBuilder
	
} // end namespace
