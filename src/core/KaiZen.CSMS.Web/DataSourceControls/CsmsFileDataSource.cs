﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.CsmsFileProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(CsmsFileDataSourceDesigner))]
	public class CsmsFileDataSource : ProviderDataSource<CsmsFile, CsmsFileKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CsmsFileDataSource class.
		/// </summary>
		public CsmsFileDataSource() : base(new CsmsFileService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the CsmsFileDataSourceView used by the CsmsFileDataSource.
		/// </summary>
		protected CsmsFileDataSourceView CsmsFileView
		{
			get { return ( View as CsmsFileDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the CsmsFileDataSource control invokes to retrieve data.
		/// </summary>
		public CsmsFileSelectMethod SelectMethod
		{
			get
			{
				CsmsFileSelectMethod selectMethod = CsmsFileSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (CsmsFileSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the CsmsFileDataSourceView class that is to be
		/// used by the CsmsFileDataSource.
		/// </summary>
		/// <returns>An instance of the CsmsFileDataSourceView class.</returns>
		protected override BaseDataSourceView<CsmsFile, CsmsFileKey> GetNewDataSourceView()
		{
			return new CsmsFileDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the CsmsFileDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class CsmsFileDataSourceView : ProviderDataSourceView<CsmsFile, CsmsFileKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CsmsFileDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the CsmsFileDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public CsmsFileDataSourceView(CsmsFileDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal CsmsFileDataSource CsmsFileOwner
		{
			get { return Owner as CsmsFileDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal CsmsFileSelectMethod SelectMethod
		{
			get { return CsmsFileOwner.SelectMethod; }
			set { CsmsFileOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal CsmsFileService CsmsFileProvider
		{
			get { return Provider as CsmsFileService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<CsmsFile> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<CsmsFile> results = null;
			CsmsFile item;
			count = 0;
			
			System.Int32 _csmsFileId;
			System.Int32 _csmsFileTypeId;
			System.Int32 _createdByUserId;
			System.Int32 _modifiedByUserId;

			switch ( SelectMethod )
			{
				case CsmsFileSelectMethod.Get:
					CsmsFileKey entityKey  = new CsmsFileKey();
					entityKey.Load(values);
					item = CsmsFileProvider.Get(entityKey);
					results = new TList<CsmsFile>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case CsmsFileSelectMethod.GetAll:
                    results = CsmsFileProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case CsmsFileSelectMethod.GetPaged:
					results = CsmsFileProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case CsmsFileSelectMethod.Find:
					if ( FilterParameters != null )
						results = CsmsFileProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = CsmsFileProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case CsmsFileSelectMethod.GetByCsmsFileId:
					_csmsFileId = ( values["CsmsFileId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["CsmsFileId"], typeof(System.Int32)) : (int)0;
					item = CsmsFileProvider.GetByCsmsFileId(_csmsFileId);
					results = new TList<CsmsFile>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				// FK
				case CsmsFileSelectMethod.GetByCsmsFileTypeId:
					_csmsFileTypeId = ( values["CsmsFileTypeId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["CsmsFileTypeId"], typeof(System.Int32)) : (int)0;
					results = CsmsFileProvider.GetByCsmsFileTypeId(_csmsFileTypeId, this.StartIndex, this.PageSize, out count);
					break;
				case CsmsFileSelectMethod.GetByCreatedByUserId:
					_createdByUserId = ( values["CreatedByUserId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["CreatedByUserId"], typeof(System.Int32)) : (int)0;
					results = CsmsFileProvider.GetByCreatedByUserId(_createdByUserId, this.StartIndex, this.PageSize, out count);
					break;
				case CsmsFileSelectMethod.GetByModifiedByUserId:
					_modifiedByUserId = ( values["ModifiedByUserId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["ModifiedByUserId"], typeof(System.Int32)) : (int)0;
					results = CsmsFileProvider.GetByModifiedByUserId(_modifiedByUserId, this.StartIndex, this.PageSize, out count);
					break;
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == CsmsFileSelectMethod.Get || SelectMethod == CsmsFileSelectMethod.GetByCsmsFileId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				CsmsFile entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					CsmsFileProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<CsmsFile> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			CsmsFileProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region CsmsFileDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the CsmsFileDataSource class.
	/// </summary>
	public class CsmsFileDataSourceDesigner : ProviderDataSourceDesigner<CsmsFile, CsmsFileKey>
	{
		/// <summary>
		/// Initializes a new instance of the CsmsFileDataSourceDesigner class.
		/// </summary>
		public CsmsFileDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public CsmsFileSelectMethod SelectMethod
		{
			get { return ((CsmsFileDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new CsmsFileDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region CsmsFileDataSourceActionList

	/// <summary>
	/// Supports the CsmsFileDataSourceDesigner class.
	/// </summary>
	internal class CsmsFileDataSourceActionList : DesignerActionList
	{
		private CsmsFileDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the CsmsFileDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public CsmsFileDataSourceActionList(CsmsFileDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public CsmsFileSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion CsmsFileDataSourceActionList
	
	#endregion CsmsFileDataSourceDesigner
	
	#region CsmsFileSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the CsmsFileDataSource.SelectMethod property.
	/// </summary>
	public enum CsmsFileSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByCsmsFileId method.
		/// </summary>
		GetByCsmsFileId,
		/// <summary>
		/// Represents the GetByCsmsFileTypeId method.
		/// </summary>
		GetByCsmsFileTypeId,
		/// <summary>
		/// Represents the GetByCreatedByUserId method.
		/// </summary>
		GetByCreatedByUserId,
		/// <summary>
		/// Represents the GetByModifiedByUserId method.
		/// </summary>
		GetByModifiedByUserId
	}
	
	#endregion CsmsFileSelectMethod

	#region CsmsFileFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CsmsFile"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CsmsFileFilter : SqlFilter<CsmsFileColumn>
	{
	}
	
	#endregion CsmsFileFilter

	#region CsmsFileExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CsmsFile"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CsmsFileExpressionBuilder : SqlExpressionBuilder<CsmsFileColumn>
	{
	}
	
	#endregion CsmsFileExpressionBuilder	

	#region CsmsFileProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;CsmsFileChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="CsmsFile"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CsmsFileProperty : ChildEntityProperty<CsmsFileChildEntityTypes>
	{
	}
	
	#endregion CsmsFileProperty
}

