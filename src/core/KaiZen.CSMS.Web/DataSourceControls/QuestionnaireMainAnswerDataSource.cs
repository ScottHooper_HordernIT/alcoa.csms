﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.QuestionnaireMainAnswerProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(QuestionnaireMainAnswerDataSourceDesigner))]
	public class QuestionnaireMainAnswerDataSource : ProviderDataSource<QuestionnaireMainAnswer, QuestionnaireMainAnswerKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainAnswerDataSource class.
		/// </summary>
		public QuestionnaireMainAnswerDataSource() : base(new QuestionnaireMainAnswerService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the QuestionnaireMainAnswerDataSourceView used by the QuestionnaireMainAnswerDataSource.
		/// </summary>
		protected QuestionnaireMainAnswerDataSourceView QuestionnaireMainAnswerView
		{
			get { return ( View as QuestionnaireMainAnswerDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the QuestionnaireMainAnswerDataSource control invokes to retrieve data.
		/// </summary>
		public QuestionnaireMainAnswerSelectMethod SelectMethod
		{
			get
			{
				QuestionnaireMainAnswerSelectMethod selectMethod = QuestionnaireMainAnswerSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (QuestionnaireMainAnswerSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the QuestionnaireMainAnswerDataSourceView class that is to be
		/// used by the QuestionnaireMainAnswerDataSource.
		/// </summary>
		/// <returns>An instance of the QuestionnaireMainAnswerDataSourceView class.</returns>
		protected override BaseDataSourceView<QuestionnaireMainAnswer, QuestionnaireMainAnswerKey> GetNewDataSourceView()
		{
			return new QuestionnaireMainAnswerDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the QuestionnaireMainAnswerDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class QuestionnaireMainAnswerDataSourceView : ProviderDataSourceView<QuestionnaireMainAnswer, QuestionnaireMainAnswerKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainAnswerDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the QuestionnaireMainAnswerDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public QuestionnaireMainAnswerDataSourceView(QuestionnaireMainAnswerDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal QuestionnaireMainAnswerDataSource QuestionnaireMainAnswerOwner
		{
			get { return Owner as QuestionnaireMainAnswerDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal QuestionnaireMainAnswerSelectMethod SelectMethod
		{
			get { return QuestionnaireMainAnswerOwner.SelectMethod; }
			set { QuestionnaireMainAnswerOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal QuestionnaireMainAnswerService QuestionnaireMainAnswerProvider
		{
			get { return Provider as QuestionnaireMainAnswerService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<QuestionnaireMainAnswer> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<QuestionnaireMainAnswer> results = null;
			QuestionnaireMainAnswer item;
			count = 0;
			
			System.Int32 _answerId;
			System.String _questionNo;
			System.String _answerNo_nullable;

			switch ( SelectMethod )
			{
				case QuestionnaireMainAnswerSelectMethod.Get:
					QuestionnaireMainAnswerKey entityKey  = new QuestionnaireMainAnswerKey();
					entityKey.Load(values);
					item = QuestionnaireMainAnswerProvider.Get(entityKey);
					results = new TList<QuestionnaireMainAnswer>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case QuestionnaireMainAnswerSelectMethod.GetAll:
                    results = QuestionnaireMainAnswerProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case QuestionnaireMainAnswerSelectMethod.GetPaged:
					results = QuestionnaireMainAnswerProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case QuestionnaireMainAnswerSelectMethod.Find:
					if ( FilterParameters != null )
						results = QuestionnaireMainAnswerProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = QuestionnaireMainAnswerProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case QuestionnaireMainAnswerSelectMethod.GetByAnswerId:
					_answerId = ( values["AnswerId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["AnswerId"], typeof(System.Int32)) : (int)0;
					item = QuestionnaireMainAnswerProvider.GetByAnswerId(_answerId);
					results = new TList<QuestionnaireMainAnswer>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				case QuestionnaireMainAnswerSelectMethod.GetByQuestionNoAnswerNo:
					_questionNo = ( values["QuestionNo"] != null ) ? (System.String) EntityUtil.ChangeType(values["QuestionNo"], typeof(System.String)) : string.Empty;
					_answerNo_nullable = (System.String) EntityUtil.ChangeType(values["AnswerNo"], typeof(System.String));
					item = QuestionnaireMainAnswerProvider.GetByQuestionNoAnswerNo(_questionNo, _answerNo_nullable);
					results = new TList<QuestionnaireMainAnswer>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// FK
				case QuestionnaireMainAnswerSelectMethod.GetByQuestionNo:
					_questionNo = ( values["QuestionNo"] != null ) ? (System.String) EntityUtil.ChangeType(values["QuestionNo"], typeof(System.String)) : string.Empty;
					results = QuestionnaireMainAnswerProvider.GetByQuestionNo(_questionNo, this.StartIndex, this.PageSize, out count);
					break;
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == QuestionnaireMainAnswerSelectMethod.Get || SelectMethod == QuestionnaireMainAnswerSelectMethod.GetByAnswerId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				QuestionnaireMainAnswer entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					QuestionnaireMainAnswerProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<QuestionnaireMainAnswer> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			QuestionnaireMainAnswerProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region QuestionnaireMainAnswerDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the QuestionnaireMainAnswerDataSource class.
	/// </summary>
	public class QuestionnaireMainAnswerDataSourceDesigner : ProviderDataSourceDesigner<QuestionnaireMainAnswer, QuestionnaireMainAnswerKey>
	{
		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainAnswerDataSourceDesigner class.
		/// </summary>
		public QuestionnaireMainAnswerDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public QuestionnaireMainAnswerSelectMethod SelectMethod
		{
			get { return ((QuestionnaireMainAnswerDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new QuestionnaireMainAnswerDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region QuestionnaireMainAnswerDataSourceActionList

	/// <summary>
	/// Supports the QuestionnaireMainAnswerDataSourceDesigner class.
	/// </summary>
	internal class QuestionnaireMainAnswerDataSourceActionList : DesignerActionList
	{
		private QuestionnaireMainAnswerDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainAnswerDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public QuestionnaireMainAnswerDataSourceActionList(QuestionnaireMainAnswerDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public QuestionnaireMainAnswerSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion QuestionnaireMainAnswerDataSourceActionList
	
	#endregion QuestionnaireMainAnswerDataSourceDesigner
	
	#region QuestionnaireMainAnswerSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the QuestionnaireMainAnswerDataSource.SelectMethod property.
	/// </summary>
	public enum QuestionnaireMainAnswerSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByAnswerId method.
		/// </summary>
		GetByAnswerId,
		/// <summary>
		/// Represents the GetByQuestionNoAnswerNo method.
		/// </summary>
		GetByQuestionNoAnswerNo,
		/// <summary>
		/// Represents the GetByQuestionNo method.
		/// </summary>
		GetByQuestionNo
	}
	
	#endregion QuestionnaireMainAnswerSelectMethod

	#region QuestionnaireMainAnswerFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireMainAnswer"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireMainAnswerFilter : SqlFilter<QuestionnaireMainAnswerColumn>
	{
	}
	
	#endregion QuestionnaireMainAnswerFilter

	#region QuestionnaireMainAnswerExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireMainAnswer"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireMainAnswerExpressionBuilder : SqlExpressionBuilder<QuestionnaireMainAnswerColumn>
	{
	}
	
	#endregion QuestionnaireMainAnswerExpressionBuilder	

	#region QuestionnaireMainAnswerProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;QuestionnaireMainAnswerChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireMainAnswer"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireMainAnswerProperty : ChildEntityProperty<QuestionnaireMainAnswerChildEntityTypes>
	{
	}
	
	#endregion QuestionnaireMainAnswerProperty
}

