﻿using ALCOA.CSMS.Batch.Common.Log;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Services;
using KaiZen.Library;
using LumenWorks.Framework.IO.Csv;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Web.Mvc;
using model = Repo.CSMS.DAL.EntityModels;
using repo = Repo.CSMS.Service.Database;


namespace ALCOA.CSMS.BatchJobScripts
{

    public class EBI
    {
        public class Metrics
        {
            public static void Update(bool live, DateTime startDate, DateTime endDate) //DT325 - add parameters to allow a date range to be calculated
            {
                LoadData(live, startDate.DateOnly(), endDate.DateOnly());
            }

            public static void Update(bool live) //DT325 - add parameters to allow a date range to be calculated
            {
                LoadData(live, DateTime.Today, DateTime.Now); //new parameters so that code can continue with old params but work for a range behind the scenes

            }

            //DT325 - add parameters to allow a date range to be calculated. If only start date is given, do from 0:00 hours to 23:59.59
            public static void Update(bool live, DateTime startDate)
            {
                LoadData(live, startDate.DateOnly(), startDate.AddDays(1).AddTicks(-1)); //new parameters so that code can continue with old params but work for a range behind the scenes
            }

            static void LoadData(bool live, DateTime startDate, DateTime endDate)
            {
                try
                {
                    var ebiService = DependencyResolver.Current.GetService<repo.IEbiService>();
                    List<model.Ebi> ebiList;
                    if (!live)
                    {
                        //DT325 - changed code to use EBI table rather than CSV file
                        ebiList = ebiService.GetMany(null, w => w.SwipeDateTime >= startDate && w.SwipeDateTime <= endDate, null, null);
                        Log4NetService.LogInfo(String.Format("Got {0} EBI swipe records from {1} to {2}", ebiList.Count, startDate.ToString("yyyy-MM-dd HH:mm:ss"),
                                                                                                                         endDate.ToString("yyyy-MM-dd HH:mm:ss")));
                    }
                    else
                    {
                        ebiList = ebiService.GetMany(null, w => w.SwipeDateTime >= DateTime.Today, null, null); //get records for today
                        Log4NetService.LogInfo(String.Format("Got {0} EBI swipe records from {1}", ebiList.Count, startDate.ToString("yyyy-MM-dd HH:mm:ss")));
                    }

                    //DT236 -if we returned some rows, load the data for each day.
                    if (ebiList.Count > 0)
                    {
                        DateTime current = startDate;
                        while (current <= endDate.DateOnly())
                        {
                            List<model.Ebi> currentEbiList = ebiList.FindAll(w => w.SwipeYear == current.Year && w.SwipeMonth == current.Month && w.SwipeDay == current.Day);

                            if (currentEbiList.Count > 0) // We have some records for the current date
                            {
                                LoadData2(currentEbiList, current);
                            }
                            current = current.AddDays(1);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Log4NetService.LogInfo(ex.Message + "\nServer Configuration Account Error. Contact Administrator.");
                }
                finally
                {
                }
            }

            static model.EbiMetric ebiMetric(DateTime dtNow, int SiteId) //DT325 - changed to new repo
            {
                model.EbiMetric em = new model.EbiMetric();
                em.Date = dtNow;
                em.SiteId = SiteId;
                em.Metric1 = 0;
                em.Metric2 = 0;
                em.Metric3 = 0;
                em.Metric4 = 0;
                em.Metric5 = 0;
                em.Metric6 = 0;
                return em;
            }
            
            static void LoadData2(List<model.Ebi> ebiList, DateTime date)
            {
                TransactionManager transactionManager = null;
                try
                {
                    DateTime dtNow = date; //DateTime.Now; DT325 - use date passed in rather than today to allow flexibility

                    SitesService sService = new SitesService();
                    Sites sBooragoon = sService.GetBySiteAbbrev("BGN");
                    Sites sMccoy = sService.GetBySiteAbbrev("HUN");
                    Sites sPeel = sService.GetBySiteAbbrev("PNJ");
                    Sites sPinjarra = sService.GetBySiteAbbrev("PIN");
                    Sites sWagerup = sService.GetBySiteAbbrev("WGP");
                    Sites sPortland = sService.GetBySiteAbbrev("PTD"); //Portland
                    Sites sKwinana = sService.GetBySiteAbbrev("KWI"); //Kwinana

                    //not yet operational
                    Sites sAnglesea = sService.GetBySiteAbbrev("ANG"); //Anglesea
                    Sites sYennora = sService.GetBySiteAbbrev("YEN"); //Yennora

                    //DT325 - changed to new repo EbiMetric
                    model.EbiMetric emBgn = ebiMetric(dtNow, sBooragoon.SiteId);
                    model.EbiMetric emHun = ebiMetric(dtNow, sMccoy.SiteId);
                    model.EbiMetric emPnj = ebiMetric(dtNow, sPeel.SiteId);
                    model.EbiMetric emPin = ebiMetric(dtNow, sPinjarra.SiteId);
                    model.EbiMetric emWgp = ebiMetric(dtNow, sWagerup.SiteId);
                    model.EbiMetric emPtd = ebiMetric(dtNow, sPortland.SiteId);
                    model.EbiMetric emKwi = ebiMetric(dtNow, sKwinana.SiteId);

                    //not yet operational
                    model.EbiMetric emAng = ebiMetric(dtNow, sAnglesea.SiteId);
                    model.EbiMetric emYen = ebiMetric(dtNow, sYennora.SiteId);

                    transactionManager = ConnectionScope.ValidateOrCreateTransaction(true);
                    List<model.EbiDailyReport> edrList = new List<model.EbiDailyReport>(); //DT325 - change to use Hordern repo

                    DataTable dtEbi = new DataTable();
                    dtEbi.Columns.Add("CompanyName", typeof(string));
                    dtEbi.Columns.Add("ClockId", typeof(string));
                    dtEbi.Columns.Add("FullName", typeof(string));
                    dtEbi.Columns.Add("AccessCardNo", typeof(string));
                    dtEbi.Columns.Add("Site", typeof(string));
                    dtEbi.Columns.Add("SwipeDateTime", typeof(string));
                    dtEbi.Columns.Add("CompanyName-CSMS", typeof(string));

                    CompaniesService cService = new CompaniesService();
                    List<String> CompaniesSitesList = new List<String>();
                    List<String> CompaniesSitesNullList = new List<String>();

                    DataTable dtEbi2 = new DataTable();
                    dtEbi2.Columns.Add("CompanyName", typeof(string));
                    dtEbi2.Columns.Add("CompanyName-CSMS", typeof(string));
                    dtEbi2.Columns.Add("Site", typeof(string));
                    dtEbi2.Columns.Add("ApprovedOnSite", typeof(string));
                    dtEbi2.Columns.Add("IssueId", typeof(int));
                    dtEbi2.Columns.Add("SqValidTill", typeof(string));
                    dtEbi2.Columns.Add("CompanySQStatus", typeof(string));
                    dtEbi2.Columns.Add("QuestionnaireId", typeof(string));
                    dtEbi2.Columns.Add("Validity", typeof(string));
                    dtEbi2.Columns.Add("Type", typeof(string));

                    dtEbi2.Columns.Add("ProcurementContact", typeof(string));
                    dtEbi2.Columns.Add("HSAssessor", typeof(string));

                    if (ebiList.Count > 0)
                    {
                        Log4NetService.LogInfo(String.Format("Retrieving EBI for {0}, number of records: {1}", dtNow.ToShortDateString(), ebiList.Count)); //DT325 Cindi Thornton Log4NetService.LogInfo("CSV File Ok.");

                        TList<Sites> sList = sService.GetAll();
                        foreach (model.Ebi ebi in ebiList)
                        {
                            string CompanyName = ebi.CompanyName;
                            string ClockId = ebi.ClockId.ToString();
                            string FullName = ebi.FullName;
                            string AccessCardNo = ebi.AccessCardNo.ToString();
                            string Site = ebi.SwipeSite;

                            DateTime SwipeDateTime = DateTime.Now;
                            int? Clock = null;
                            try
                            {
                                if (!String.IsNullOrEmpty(ClockId))
                                    Clock = Convert.ToInt32(ClockId);
                            }
                            catch (Exception)
                            {
                                Clock = null;
                            }

                            string live = "live";
                            SwipeDateTime = ebi.SwipeDateTime; //DT325 14/1/15 Cindi Thornton Convert.ToDateTime(csv["SwipeDateTime"]);

                            string CompanyNameCSMS = "";
                            string ProcurementContact = "(n/a)";
                            string HSAssessor = "(n/a)";

                            int CompanyId = -1;
                            int SiteId = -1;
                            string ABN;

                            Sites s = sList.Find(SitesColumn.SiteNameEbi, Site);
                            if (s != null) SiteId = s.SiteId;

                            ConnectionStringSettings conString = ConfigurationManager.ConnectionStrings["ALCOA_WAO_CSMWP_devConnectionString"];
                            using (SqlConnection conn1 = new SqlConnection(conString.ConnectionString))
                            {
                                conn1.Open();
                                SqlCommand cmd2 = new SqlCommand("GetCompanyABNByEbiClockIdSwipeDateTime", conn1);
                                cmd2.CommandType = CommandType.StoredProcedure;
                                cmd2.Parameters.Add(new SqlParameter("@live", live));
                                if (Clock != null)
                                    cmd2.Parameters.Add(new SqlParameter("@ClockId", Clock));
                                else
                                    cmd2.Parameters.Add(new SqlParameter("@ClockId", DBNull.Value));
                                cmd2.Parameters.Add(new SqlParameter("@SwipeDateTime", SwipeDateTime));
                                Object objABN = cmd2.ExecuteScalar();

                                ABN = Convert.ToString(objABN);
                            }

                            Companies cList;
                            if (!String.IsNullOrEmpty(ABN))
                                cList = cService.GetByCompanyAbn(ABN);
                            else
                                cList = null;

                            if (cList != null)
                            {
                                CompanyNameCSMS = cList.CompanyName;
                                CompanyId = cList.CompanyId;
                                if (CompanyId != -1)
                                {
                                    QuestionnaireWithLocationApprovalViewLatestService qwlavlService = new QuestionnaireWithLocationApprovalViewLatestService();
                                    DataSet DSqwlavl = qwlavlService.GetProcurementContactHsAssessor_ByCompanyId(CompanyId);
                                    if (DSqwlavl.Tables.Count > 0)
                                    {
                                        if (DSqwlavl.Tables[0].Rows.Count > 0)
                                        {
                                            foreach (DataRow DRqwlavl in DSqwlavl.Tables[0].Rows)
                                            {
                                                ProcurementContact = DRqwlavl[0].ToString();
                                                HSAssessor = DRqwlavl[1].ToString();
                                            }
                                        }
                                    }
                                }
                            }
                            dtEbi.Rows.Add(new object[] { CompanyName, ClockId, FullName, AccessCardNo, Site, SwipeDateTime, CompanyNameCSMS });
                            if (CompaniesSitesList.Contains(String.Format("{0}|||||{1}", CompanyName, Site)) == false && CompanyId != -1)
                            {
                                CompaniesSitesList.Add(String.Format("{0}|||||{1}", CompanyName, Site));
                                string ApprovedOnSite = "";
                                int IssueId = 0;
                                string SqValidTill = "";
                                string CompanySQStatus = "";
                                string QuestionnaireId = "";
                                string Validity = "?";
                                string Type = "?";
                                if (CompanyId != -1)
                                {
                                    CompaniesService cService2 = new CompaniesService();
                                    Companies c = cService2.GetByCompanyId(CompanyId);
                                    CompanyStatusService css = new CompanyStatusService();
                                    CompanyStatus cs = css.GetByCompanyStatusId(c.CompanyStatusId);
                                    CompanySQStatus = cs.CompanyStatusDesc;

                                    int _count = 0;
                                    VList<QuestionnaireWithLocationApprovalViewLatest> qwlavlList = DataRepository.QuestionnaireWithLocationApprovalViewLatestProvider.GetPaged(
                                                                                String.Format("CompanyId = {0}", CompanyId), "CreatedDate DESC", 0, 9999, out _count);

                                    if (_count == 0)
                                    {
                                        IssueId = (int)EbiIssueList.No_SqNotFound;
                                    }
                                    else
                                    {
                                        QuestionnaireId = qwlavlList[0].QuestionnaireId.ToString();

                                        if (qwlavlList[0].SubContractor == true)
                                            Type = "Sub Contractor";
                                        else
                                            Type = "Contractor";

                                        CompanySiteCategoryStandardService cscsService = new CompanySiteCategoryStandardService();
                                        if (SiteId != -1)
                                        {
                                            CompanySiteCategoryStandard cscs = cscsService.GetByCompanyIdSiteId(qwlavlList[0].CompanyId, SiteId);
                                            if (cscs != null)
                                            {
                                                switch (cscs.Approved)
                                                {
                                                    case true:
                                                        ApprovedOnSite = "Yes";
                                                        break;
                                                    case false:
                                                        ApprovedOnSite = "No";
                                                        break;
                                                    case null:
                                                        ApprovedOnSite = "Tentative";
                                                        break;
                                                    default:
                                                        ApprovedOnSite = "";
                                                        break;
                                                }
                                            }
                                        }

                                        bool CompanyStatusAllowsOnSite = false;
                                        if (Type == "Contractor")
                                        {
                                            if (c.CompanyStatusId == (int)CompanyStatusList.Active) CompanyStatusAllowsOnSite = true;
                                        }
                                        else
                                        {
                                            if (c.CompanyStatusId == (int)CompanyStatusList.SubContractor) CompanyStatusAllowsOnSite = true;
                                        }

                                        if (qwlavlList[0].MainAssessmentValidTo != null)
                                        {
                                            DateTime dtValidTo = Convert.ToDateTime(qwlavlList[0].MainAssessmentValidTo.ToString());
                                            SqValidTill = dtValidTo.ToString();

                                            if (DateTime.Now >= dtValidTo)
                                            {
                                                Validity = "Expired";
                                                IssueId = (int)EbiIssueList.No_ExpiredSqAndOrAccessToSiteNotGranted;
                                            }
                                            else
                                            {
                                                TimeSpan span = dtValidTo - DateTime.Now;
                                                if (span.TotalDays <= 60)
                                                {
                                                    if (c.CompanyStatusId == (int)CompanyStatusList.Requal_Incomplete) CompanyStatusAllowsOnSite = true;

                                                    if (ApprovedOnSite == "Yes" && CompanyStatusAllowsOnSite)
                                                        IssueId = (int)EbiIssueList.Yes_CurrentSqButExpiringAndAccessToSite;
                                                    else
                                                        IssueId = (int)EbiIssueList.No_AccessToSiteNotGranted;
                                                }
                                                else
                                                {
                                                    Validity = "Current";
                                                    if (ApprovedOnSite == "Yes" && CompanyStatusAllowsOnSite)
                                                        IssueId = (int)EbiIssueList.Yes_CurrentSqAndAccessToSite;
                                                    else
                                                        IssueId = (int)EbiIssueList.No_AccessToSiteNotGranted;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            IssueId = (int)EbiIssueList.No_CurrentSqFoundAndOrAccessToSiteNotGranted;
                                        }
                                    }

                                    SqExemptionService sqeService = new SqExemptionService();
                                    TList<SqExemption> sqeList = sqeService.GetByCompanyId(CompanyId);
                                    sqeList = sqeList.FindAll(SqExemptionColumn.SiteId, SiteId);
                                    if (sqeList.Count > 0)
                                    {
                                        foreach (SqExemption sq in sqeList)
                                        {
                                            if (sq.ValidTo > DateTime.Now)
                                            {
                                                IssueId = (int)EbiIssueList.SqExempt;
                                                break;
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    IssueId = (int)EbiIssueList.No_CompanyNotFound;
                                }
                                if (!String.IsNullOrEmpty(CompanyName))
                                {
                                    dtEbi2.Rows.Add(new object[] { CompanyName, CompanyNameCSMS, Site, ApprovedOnSite, IssueId, SqValidTill, CompanySQStatus, QuestionnaireId, Validity, Type,
                                                                    ProcurementContact,  HSAssessor});
                                    model.EbiDailyReport edr = new model.EbiDailyReport(); //DT325 - change to use new repo
                                    edr.DateTime = dtNow;
                                    edr.CompanyName = CompanyName;
                                    edr.CompanyNameCsms = CompanyNameCSMS;
                                    edr.SiteName = Site;
                                    edr.ApprovedOnSite = ApprovedOnSite;
                                    edr.EbiIssueId = IssueId;
                                    edr.SqValidTill = SqValidTill;
                                    edr.CompanySqStatus = CompanySQStatus;
                                    edr.QuestionnaireId = QuestionnaireId;
                                    edr.Validity = Validity;
                                    edr.Type = Type;
                                    edrList.Add(edr);

                                    switch (Site)
                                    {
                                        case "BOORAGOON":
                                            emBgn = AddEbiMetric_Row(emBgn, IssueId);
                                            break;
                                        case "MCCOY":
                                            emHun = AddEbiMetric_Row(emHun, IssueId);
                                            break;
                                        case "PEEL":
                                            emPnj = AddEbiMetric_Row(emPnj, IssueId);
                                            break;
                                        case "PINJARRA":
                                            emPin = AddEbiMetric_Row(emPin, IssueId);
                                            break;
                                        case "WAGERUP":
                                            emWgp = AddEbiMetric_Row(emWgp, IssueId);
                                            break;
                                        case "KWINANA":
                                            emKwi = AddEbiMetric_Row(emKwi, IssueId);
                                            break;
                                        case "PORTLAND":
                                            emPtd = AddEbiMetric_Row(emPtd, IssueId);
                                            break;
                                        case "Other":
                                            break;
                                    }
                                }
                            }
                            else
                            {
                                if (CompanyId == -1 && CompaniesSitesNullList.Contains(String.Format("{0}|||||{1}", CompanyName, Site)) == false)
                                {
                                    CompaniesSitesNullList.Add(String.Format("{0}|||||{1}", CompanyName, Site));
                                }
                            }
                        }

                        foreach (string str in CompaniesSitesNullList)
                        {
                            if (!CompaniesSitesList.Contains(str))
                            {
                                string[] nullList = str.Split('|');
                                string CompanyName = nullList[0];
                                string Site = nullList[5];
                                dtEbi2.Rows.Add(new object[] { CompanyName, "", Site, "", (int)EbiIssueList.No_CompanyNotFound, "", "", "", "", "?",
                                                                        "",  ""});

                                int IssueId = (int)EbiIssueList.No_CompanyNotFound;
                                model.EbiDailyReport edr = new model.EbiDailyReport(); //DT325 - change to use new repo
                                edr.DateTime = dtNow;
                                edr.CompanyName = CompanyName;
                                edr.CompanyNameCsms = "";
                                edr.SiteName = Site;
                                edr.ApprovedOnSite = "";
                                edr.EbiIssueId = (int)EbiIssueList.No_CompanyNotFound;
                                edr.SqValidTill = "";
                                edr.CompanySqStatus = "";
                                edr.QuestionnaireId = "";
                                edr.Validity = "";
                                edr.Type = "?";
                                edrList.Add(edr);

                                switch (Site)
                                {
                                    case "BOORAGOON":
                                        emBgn = AddEbiMetric_Row(emBgn, IssueId);
                                        break;
                                    case "MCCOY":
                                        emHun = AddEbiMetric_Row(emHun, IssueId);
                                        break;
                                    case "PEEL":
                                        emPnj = AddEbiMetric_Row(emPnj, IssueId);
                                        break;
                                    case "PINJARRA":
                                        emPin = AddEbiMetric_Row(emPin, IssueId);
                                        break;
                                    case "WAGERUP":
                                        emWgp = AddEbiMetric_Row(emWgp, IssueId);
                                        break;
                                    case "KWINANA":
                                        emKwi = AddEbiMetric_Row(emKwi, IssueId);
                                        break;
                                    case "PORTLAND":
                                        emPtd = AddEbiMetric_Row(emPtd, IssueId);
                                        break;
                                    case "Other":
                                        break;
                                }
                            }
                        }

                        //DT325 - change to use new repo, update if a record already exists for this date, otherwise insert
                        var ebiDailyReportService = DependencyResolver.Current.GetService<repo.IEbiDailyReportService>();
                        List<model.EbiDailyReport> ebiDailyReportList = ebiDailyReportService.GetMany(null, w => DbFunctions.TruncateTime(w.DateTime) == date.Date, null, null); //get all records for the current day

                        Log4NetService.LogInfo(String.Format("Deleting {0} records from EBIDailyReport for Date {1}", ebiDailyReportList.Count, date.Date));
                        foreach (model.EbiDailyReport edrExisting in ebiDailyReportList)
                            ebiDailyReportService.Delete(edrExisting);

                        foreach (model.EbiDailyReport edr in edrList)
                        {
                                ebiDailyReportService.Insert(edr);
                        }
                        Log4NetService.LogInfo(String.Format("Inserting {0} new records into EBIDailyReport for Date {1}", edrList.Count, date.Date));

                        var ebiMetricService = DependencyResolver.Current.GetService<repo.IEbiMetricService>();
                        List<model.EbiMetric> emList = ebiMetricService.GetMany(null, w => DbFunctions.TruncateTime(w.Date) == date.Date, null, null); //get all records for the current day

                        //if we find a match for the day delete them. Then insert
                        Log4NetService.LogInfo(String.Format("Deleting {0} records from EBIMetric for Date {1}", emList.Count, date.Date));
                        foreach (model.EbiMetric em in emList)
                            ebiMetricService.Delete(em);

                        ebiMetricService.Insert(emBgn);
                        ebiMetricService.Insert(emHun);
                        ebiMetricService.Insert(emPnj);
                        ebiMetricService.Insert(emPin);
                        ebiMetricService.Insert(emWgp);
                        ebiMetricService.Insert(emKwi);
                        ebiMetricService.Insert(emPtd);
                        Log4NetService.LogInfo(String.Format("Inserting new records into EBIMetric for Date {0}", date.Date));
                    }
                }
                catch (Exception ex)
                {
                    if ((transactionManager != null) && (transactionManager.IsOpen))
                        transactionManager.Rollback();
                    throw ex;
                }
            }
            
            static string ReadEbiFile(string FileFullPath)
            {
                return Helper.IO.ReadFile(FileFullPath);
            }
            
            static string ReadEbiFile()
            {
                string myFile = "CompanyName,ClockId,FullName,AccessCardNo,Site,SwipeDateTime\n";

                SitesService sService = new SitesService();
                SitesFilters sFilters = new SitesFilters();
                sFilters.AppendIsNotNull(SitesColumn.EbiServerName);
                sFilters.Junction = SqlUtil.AND;
                sFilters.AppendIsNotNull(SitesColumn.EbiTodayViewName);
                int count = 0;
                TList<Sites> sTlist = sService.GetPaged(sFilters.ToString(), null, 0, 100, out count);

                if (count > 0)
                {
                    ArrayList EbiServers = new ArrayList();
                    foreach (Sites s in sTlist)
                    {
                        if(!String.IsNullOrEmpty(s.EbiServerName) && !String.IsNullOrEmpty(s.EbiTodayViewName))
                        {
                            if (!EbiServers.Contains(s.EbiServerName))
                            {
                                EbiServers.Add(s.EbiServerName);
                            }
                        }
                    }

                    DataTable dtAll = new DataTable();
                    int tmpCnt = 0;

                    while (tmpCnt <= (EbiServers.Count - 1))
                    {
                        ConnectionStringSettings conString = ConfigurationManager.ConnectionStrings[String.Format("Alcoa.Ebi.{0}.ConnectionString", EbiServers[tmpCnt].ToString())];
                        string strConnString = conString.ConnectionString;

                        using (SqlConnection cn = new SqlConnection(strConnString))
                        {
                            try
                            {
                                cn.Open();
                                foreach (Sites s in sTlist)
                                {
                                    if (s.EbiServerName == EbiServers[tmpCnt].ToString())
                                    {
                                        SqlCommand cmd = new SqlCommand("SELECT * FROM " + s.EbiTodayViewName, cn);

                                        SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(cmd);
                                        DataTable dt = new DataTable();
                                        if (dtAll.Rows.Count == 0)
                                        {
                                            sqlDataAdapter.Fill(dtAll);
                                        }
                                        else
                                        {
                                            sqlDataAdapter.Fill(dt);
                                            dtAll.Merge(dt);
                                        }
                                    }
                                }
                            }
                            catch (Exception)
                            {
                            }
                            finally
                            {
                                cn.Close();
                            }
                        }
                        tmpCnt++;
                    }

                    foreach (DataRow r in dtAll.Rows)
                    {
                        string CompanyName = (string)r["Company"];
                        if ((string)r["ClockID"] != "")
                        {
                            try
                            {
                                int test = Convert.ToInt32(r["ClockID"]);
                            }
                            catch (Exception)
                            {
                                continue;
                            }
                        }
                        string ClockId = (string)r["ClockID"];
                        string FullName = (string)r["FullName"];
                        string AccessCardNo = (string)r["CardNumber"];
                        string Site = (string)r["Site"];
                        DateTime SwipeDateTime = (DateTime)r["SwipeDateTime"];

                        myFile += "\"" + CompanyName + "\",\"" + ClockId + "\",\"" + FullName + "\",\"" + AccessCardNo + "\",\"" + Site + "\",\"" + SwipeDateTime.ToString() + "\"\n";
                    }
                }
                return myFile;
            }
            
            static model.EbiMetric AddEbiMetric_Row(model.EbiMetric em, int IssueId) //DT325 - changed to use new repo
            {
                switch (IssueId)
                {
                    case 1:
                        em.Metric1 += 1;
                        break;
                    case 2:
                        em.Metric2 += 1;
                        break;
                    case 3:
                        em.Metric3 += 1;
                        break;
                    case 4:
                        em.Metric4 += 1;
                        break;
                    case 5:
                        em.Metric5 += 1;
                        break;
                    case 6:
                        em.Metric6 += 1;
                        break;
                    case 7:
                        em.Metric7 += 1;
                        break;
                    case 8:
                        em.Metric8 += 1;
                        break;
                    default:
                        break;
                }
                return em;
            }
        }

        public class Statistics
        {
            public static void UpdateEbiSource1(int Year, int Month)
            {
                //Connect to all EBI Servers and dump CSV File :)
                try
                {
                    SitesService sService = new SitesService();
                    TList<Sites> sitesAll = sService.GetAll();
                    foreach (Sites s in sitesAll)
                    {
                        if(!String.IsNullOrEmpty(s.EbiServerName) && !String.IsNullOrEmpty(s.EbiAllViewName))
                        {
                            try
                            {
                                string server = s.EbiServerName;
                                int AccessCardNo = 0;
                                string view = string.Empty;
                                string Source = string.Empty;

                                ConnectionStringSettings conString1 = ConfigurationManager.ConnectionStrings[String.Format("Alcoa.Ebi.{0}.ConnectionString", server)];
                                string strConnString = conString1.ConnectionString;
                                ConnectionStringSettings conString = ConfigurationManager.ConnectionStrings["ALCOA_WAO_CSMWP_devConnectionString"];
                                DataSet ds = new DataSet();

                                Log4NetService.LogInfo("Connecting to: " + server + "...");
                                using (SqlConnection cn = new SqlConnection(conString1.ConnectionString))
                                {
                                    cn.Open();

                                    SqlCommand cmd = new SqlCommand("SELECT CardNumber,SwipeDateTime,Source FROM " + s.EbiAllViewName + " where Year(SwipeDateTime)=" + Year + " And Month(SwipeDateTime)='" + Month + "' Order by SwipeDateTime Asc", cn);
                                    cmd.CommandTimeout = 120000;
                                    Log4NetService.LogInfo("Reading data from  Ebi Server for Site " + s.SiteNameEbi + " and Year=" + Year + " and Month=" + Month);
                                    SqlDataAdapter sqlda = new SqlDataAdapter(cmd);
                                    sqlda.Fill(ds);
                                    Log4NetService.LogInfo("Read...");

                                    foreach (DataRow dr1 in ds.Tables[0].Rows)
                                    {
                                        try
                                        {
                                            Source = (string)dr1["Source"];
                                            AccessCardNo = Convert.ToInt32(dr1["CardNumber"]);
                                            DateTime swipeTime = (DateTime)(dr1["SwipeDateTime"]);

                                            using (SqlConnection conn = new SqlConnection(conString.ConnectionString))
                                            {
                                                if (Source != null && AccessCardNo != null)
                                                {
                                                    using (SqlCommand cmd1 = new SqlCommand("Ebi_UpdateSourceByCardSwipe", conn))
                                                    {
                                                        cmd1.CommandType = CommandType.StoredProcedure;
                                                        cmd1.Parameters.Add(new SqlParameter("@AccessCardNo", AccessCardNo));
                                                        cmd1.Parameters.Add(new SqlParameter("@SwipeDateTime", swipeTime));
                                                        cmd1.Parameters.Add(new SqlParameter("@Source", Source));

                                                        conn.Open();
                                                        cmd1.ExecuteNonQuery();
                                                        conn.Close();
                                                    }
                                                }
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            Log4NetService.LogInfo("Error while updating AccessCardNo= " + AccessCardNo + ": " + ex.Message);
                                        }
                                    }
                                    cn.Close();
                                }
                            }
                            catch (Exception ex)
                            {
                                Log4NetService.LogInfo("Error In " + s.EbiServerName + ": " + ex.Message);
                            }

                            Log4NetService.LogInfo("Finished updating Source for site " + s.SiteNameEbi + " and Year=" + Year + " and Month="+Month+"\n\n");
                        }
                    }
                }
                catch (Exception ex)
                {
                    Log4NetService.LogInfo(ex.Message.ToString() + "\nServer Configuration Account Error. Contact Administrator.");
                }
                finally
                {
                }
            }

            public static void UpdateFatigueManagementTable(bool live)
            {
                string spLive=string.Empty;

                if (live == true)
                    spLive = "_EbiHoursWorked_FatigueManagement_Live";
                else
                    spLive = "_EbiHoursWorked_FatigueManagement";

                // TOOD !####! need to decommission this option as it is calling an old version of FatigueManagement_Insert and WONT work in this form
                try
                {
                    try
                    {
                        ConnectionStringSettings conString = ConfigurationManager.ConnectionStrings["ALCOA_WAO_CSMWP_devConnectionString"];
                        string strConnString = conString.ConnectionString;

                        Log4NetService.LogInfo("Connecting to CSMS");
                        using (SqlConnection cn = new SqlConnection(strConnString))
                        {
                            cn.Open();
                            Log4NetService.LogInfo("Reading from CSMS records violating 16/64 policy: ");
                            SqlCommand cmd = new SqlCommand(spLive, cn);
                            cmd.CommandTimeout = 60000;
                            cmd.CommandType = CommandType.StoredProcedure;
                            SqlDataReader r = cmd.ExecuteReader();

                            while (r.Read())
                            {
                                try
                                {
                                    int inserted=1;
                                    string comments = string.Empty;

                                    if ((decimal)r["RollingWeekHours"] > 64)
                                        comments = "Automatically added due to >64 hours in rolling 7 days";
                                    else
                                        comments = "Automatically added due to >16 hours working day";

                                    try
                                    {
                                        using (SqlConnection conn = new SqlConnection(conString.ConnectionString))
                                        {
                                            using (SqlCommand cmd1 = new SqlCommand("FatigueManagement_Insert", conn))
                                            {
                                                cmd1.CommandType = CommandType.StoredProcedure;
                                                if (Convert.ToInt32(r["ClockId"]) != null)
                                                    cmd1.Parameters.Add(new SqlParameter("@ClockId", Convert.ToInt32(r["ClockId"])));
                                                else
                                                    cmd1.Parameters.Add(new SqlParameter("@ClockId", DBNull.Value));
                                                if (Convert.ToDecimal(r["TotalTime"]) != null)
                                                    cmd1.Parameters.Add(new SqlParameter("@TotalTime", Convert.ToDecimal(r["TotalTime"])));
                                                else
                                                    cmd1.Parameters.Add(new SqlParameter("@TotalTime", DBNull.Value));
                                                if (Convert.ToDateTime(r["EntryDateTime"]) != null)
                                                    cmd1.Parameters.Add(new SqlParameter("@EntryDateTime", Convert.ToDateTime(r["EntryDateTime"])));
                                                else
                                                    cmd1.Parameters.Add(new SqlParameter("@EntryDateTime", DBNull.Value));
                                                if (Convert.ToString(r["FullName"]) != null)
                                                    cmd1.Parameters.Add(new SqlParameter("@FullName", Convert.ToString(r["FullName"])));
                                                else
                                                    cmd1.Parameters.Add(new SqlParameter("@FullName", DBNull.Value));
                                                if (Convert.ToString(r["Vendor_Number"]) != null)
                                                    cmd1.Parameters.Add(new SqlParameter("@Vendor_Number", Convert.ToString(r["Vendor_Number"])));
                                                else
                                                    cmd1.Parameters.Add(new SqlParameter("@Vendor_Number", DBNull.Value));
                                                if (Convert.ToDecimal(r["RollingWeekHours"]) != null)
                                                    cmd1.Parameters.Add(new SqlParameter("@RollingWeekHours", Convert.ToDecimal(r["RollingWeekHours"])));
                                                else
                                                    cmd1.Parameters.Add(new SqlParameter("@RollingWeekHours", DBNull.Value));

                                                cmd1.Parameters.Add(new SqlParameter("@Comments", comments));

                                                if (Convert.ToInt32(r["SiteId"]) != null)
                                                    cmd1.Parameters.Add(new SqlParameter("@SiteId", Convert.ToInt32(r["SiteId"])));
                                                else
                                                    cmd1.Parameters.Add(new SqlParameter("@SiteId", DBNull.Value));

                                                SqlParameter output = new SqlParameter("@result", SqlDbType.Int);
                                                output.Direction = ParameterDirection.Output;
                                                cmd1.Parameters.Add(output);

                                                conn.Open();
                                                cmd1.ExecuteNonQuery();
                                                inserted = Convert.ToInt32(output.Value);
                                                conn.Close();
                                            }
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        Log4NetService.LogInfo("Error occurred while inserting a row in FatigueManagement table:" + ex.Message);
                                        inserted = 0;
                                    }
                                    
                                    if(inserted==1)
                                    {
                                        ConfigService cfgService = new ConfigService();
                                        Config cfg = cfgService.GetByKey("EbiAutomaticEmail");
                                        string CompanyName = string.Empty;
                                        string FullName = string.Empty;
                                        if (cfg.Value == "Yes")
                                        {
                                            ConfigSa812Service cfgSaService = new ConfigSa812Service();
                                            ConfigSa812 cfgSa;
                                            if (Convert.ToInt32(r["SiteId"]) != null)
                                            {
                                                cfgSa = cfgSaService.GetBySiteId(Convert.ToInt32(r["SiteId"]));
                                                UsersService uService = new UsersService();
                                                if (cfgSa != null)
                                                {
                                                    int SaId = Convert.ToInt32(cfgSa.SupervisorUserId);
                                                    if (SaId != null)
                                                    {
                                                        Users u = uService.GetByUserId(SaId);
                                                        if (u != null)
                                                        {
                                                            string emailToName = String.Format("{0} {1}", u.FirstName, u.LastName);
                                                            string[] to = { u.Email };

                                                            string emailSubject = String.Format("Potential Contractor violation of Alcoa 16/64 – {0} – {1}", Convert.ToString(r["CompanyName"]), Convert.ToString(r["FullName"]));
                                                            if (Convert.ToString(r["CompanyName"]) != null)
                                                                CompanyName = Convert.ToString(r["CompanyName"]);
                                                            else
                                                                CompanyName = "";
                                                            if (Convert.ToString(r["FullName"]) != null)
                                                                FullName = Convert.ToString(r["FullName"]);
                                                            else
                                                                FullName = "";

                                                            string emailBody = String.Format("Company:{0}<br/><br/>Full Name: {1}<br/><br/>Date/Time Exceeding Alcoa 16/64 : {2}<br/><br/>Comments: {3}<br/><br/>", CompanyName, FullName, Convert.ToString(r["EntryDateTime"]), comments);
                                                            emailBody += "<a href='http://csm.aua.alcoa.com/FatigueManagement.aspx'>Click here to access the Fatigue Management listing</a>";
                                                            if (!String.IsNullOrEmpty(emailBody))
                                                            {
                                                                //Cindi Thornton 4/9/2015 - removed call to sendEmail, modified call to logEmail for new columns IsBodyHtml & attachmentPath. EmailLog used to queue & log emails.
                                                                //Helper.EmailAgent.sendEmail(to, null, null, emailSubject, emailBody, true, null); //Send E-Mail
                                                                //Helper.EmailAgent.logEmail(to, null, null, emailSubject, emailBody, EmailLogTypeList.General, true, null); //Log
                                                                var emailLogService = DependencyResolver.Current.GetService<repo.IEmailLogService>();
                                                                emailLogService.Insert(to, null, null, emailSubject, emailBody, "General", true, null, null);
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                Log4NetService.LogInfo("No site Information exists");
                                            }
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    Log4NetService.LogInfo("Error " + ex.Message);
                                }
                            }
                            cn.Close();
                        }
                    }
                    catch (Exception ex)
                    {
                        Log4NetService.LogInfo("Error " + ex.Message);
                        throw;
                    }
                }
                catch (Exception ex)
                {
                    Log4NetService.LogInfo(ex.Message.ToString() + "\nServer Configuration Account Error. Contact Administrator.");
                    throw;
                }
                finally
                {
                }
            }

            public static void UpdateEbiSource(int Year)
            {
                //Connect to all EBI Servers and dump CSV File :)
                try
                {
                    SitesService sService = new SitesService();
                    TList<Sites> sitesAll = sService.GetAll();
                    foreach (Sites s in sitesAll)
                    {
                        if(!String.IsNullOrEmpty(s.EbiServerName) && !String.IsNullOrEmpty(s.EbiAllViewName))
                        {
                            try
                            {
                                string server = s.EbiServerName;
                                int EbiId = 0;
                                int AccessCardNo = 0;
                                string view = string.Empty;
                                string Source = string.Empty;

                                ConnectionStringSettings conString1 = ConfigurationManager.ConnectionStrings[String.Format("Alcoa.Ebi.{0}.ConnectionString", server)];
                                string strConnString = conString1.ConnectionString;
                                ConnectionStringSettings conString = ConfigurationManager.ConnectionStrings["ALCOA_WAO_CSMWP_devConnectionString"];
                                DataSet ds = new DataSet();

                                Log4NetService.LogInfo("Connecting to: " + server + "...");
                                using (SqlConnection cn = new SqlConnection(conString.ConnectionString))
                                {
                                    cn.Open();

                                    SqlCommand cmd = new SqlCommand("SELECT EbiId,AccessCardNo,SwipeDateTime FROM Ebi where Source is Null And SwipeYear=" + Year + " And SwipeSite='" + s.SiteNameEbi + "' Order by SwipeDateTime Desc", cn);
                                    cmd.CommandTimeout = 60000;
                                    Log4NetService.LogInfo("Reading data from CSMS Ebi for Site " + s.SiteNameEbi + " and Year=" + Year);
                                    SqlDataAdapter sqlda = new SqlDataAdapter(cmd);
                                    sqlda.Fill(ds);
                                    Log4NetService.LogInfo("Read...");

                                    foreach (DataRow dr1 in ds.Tables[0].Rows)
                                    {
                                        try
                                        {
                                            EbiId = (int)dr1["EbiId"];
                                            AccessCardNo = (int)dr1["AccessCardNo"];
                                            DateTime swipeTime = (DateTime)(dr1["SwipeDateTime"]);

                                            view = s.EbiAllViewName + " WHERE SwipeDateTime = Convert(datetime,'" + swipeTime + "',103) And CardNumber=" + AccessCardNo;

                                            using (SqlConnection conn1 = new SqlConnection(strConnString))
                                            {
                                                conn1.Open();
                                                SqlCommand cmd2 = new SqlCommand("SELECT Top 1 Source FROM " + view, conn1);
                                                Object Descr = cmd2.ExecuteScalar();
                                                Source = Convert.ToString(Descr);
                                            }
                                            using (SqlConnection conn = new SqlConnection(conString.ConnectionString))
                                            {
                                                if (Source != null && EbiId != null)
                                                {
                                                    using (SqlCommand cmd1 = new SqlCommand("Ebi_UpdateSource", conn))
                                                    {

                                                        cmd1.CommandType = CommandType.StoredProcedure;
                                                        cmd1.Parameters.Add(new SqlParameter("@EbiId", EbiId));
                                                        cmd1.Parameters.Add(new SqlParameter("@Source", Source));

                                                        conn.Open();
                                                        cmd1.ExecuteNonQuery();
                                                        conn.Close();
                                                    }
                                                }
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            Log4NetService.LogInfo("Error while updating EbiID= " + EbiId + ": " + ex.Message);
                                        }
                                    }
                                    cn.Close();
                                }
                            }
                            catch (Exception ex)
                            {
                                Log4NetService.LogInfo("Error In " + s.EbiServerName + ": " + ex.Message);
                            }

                            Log4NetService.LogInfo("Finished updating Description for site " + s.SiteNameEbi + " and Year=" + Year + ".\n\n");
                        }
                    }
                }
                catch (Exception ex)
                {
                    Log4NetService.LogInfo(ex.Message.ToString() + "\nServer Configuration Account Error. Contact Administrator.");
                }
                finally
                {
                }
            }

            public static void UpdateCSMSdbFromEbi()
            {
                //Connect to all EBI Servers and dump CSV File :)
                try
                {
                    SitesService sService = new SitesService();
                    TList<Sites> sitesAll = sService.GetAll();
                    foreach (Sites s in sitesAll)
                    {
                        if(!String.IsNullOrEmpty(s.EbiServerName) && !String.IsNullOrEmpty(s.EbiAllViewName))
                        {
                            try
                            {
                                DateTime LatestSwipe = DateTime.Now.AddMinutes(-15);
                                try
                                {
                                    ConnectionStringSettings conString = ConfigurationManager.ConnectionStrings["ALCOA_WAO_CSMWP_devConnectionString"];
                                    using (SqlConnection conn = new SqlConnection(conString.ConnectionString))
                                    {
                                        using (SqlCommand cmd = new SqlCommand("GetMaxSwipeDateTime", conn))
                                        {
                                            Log4NetService.LogInfo("Fetching information from CSMS EBI table...");
                                            cmd.CommandType = CommandType.StoredProcedure;

                                            SqlParameter message = new SqlParameter("@Message", SqlDbType.DateTime);
                                            message.Size = 50;
                                            message.Direction = ParameterDirection.Output; // This is important!
                                            cmd.Parameters.Add(new SqlParameter("@SwipeSite", s.SiteNameEbi));
                                            cmd.Parameters.Add(message);
                                            conn.Open();
                                            cmd.ExecuteNonQuery();
                                            conn.Close();
                                            if ((cmd.Parameters["@Message"].Value) != null)
                                                Log4NetService.LogInfo("Successfully fetched information from EBI table for Site " + s.SiteNameEbi);
                                            LatestSwipe = (DateTime)cmd.Parameters["@Message"].Value;
                                            Log4NetService.LogInfo("Last Swipedatetime for Site-" + s.SiteNameEbi + " =" + LatestSwipe);
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    Log4NetService.LogInfo("Error occurred while fetching information from EBI table: " + ex.Message);
                                }

                                string server = s.EbiServerName;
                                string SwipeDateTime_Formatted = "";
                                String dtFormat = "M/d/yyyy $ h:mm:ss # tt";

                                SwipeDateTime_Formatted = LatestSwipe.ToString(dtFormat, CultureInfo.InvariantCulture);
                                SwipeDateTime_Formatted = SwipeDateTime_Formatted.Replace(" $ ", "   ");

                                SwipeDateTime_Formatted = SwipeDateTime_Formatted.Replace(" # ", "");

                                //TODO Temporary. Next Upgrade, add column 'EbiYesterdayViewName'
                                //Yesterday
                                string view = view = s.EbiAllViewName + " WHERE Cast(SwipeDateTime as datetime) > '" + SwipeDateTime_Formatted + "' Order by SwipeDateTime ASC";

                                ConnectionStringSettings conString1 = ConfigurationManager.ConnectionStrings[String.Format("Alcoa.Ebi.{0}.ConnectionString", server)];
                                string strConnString = conString1.ConnectionString;

                                Log4NetService.LogInfo("Connecting to: " + server + "...");
                                using (SqlConnection cn = new SqlConnection(strConnString))
                                {
                                    cn.Open();
                                    Log4NetService.LogInfo("Reading View: " + view + "...");
                                    SqlCommand cmd = new SqlCommand("SELECT * FROM " + view, cn);
                                    cmd.CommandTimeout = 60000;
                                    SqlDataReader r = cmd.ExecuteReader();
                                    Log4NetService.LogInfo("Reading data for site " + s.SiteNameEbi);
                                    
                                    while (r.Read())
                                    {
                                        try
                                        {
                                            int newEbiId;
                                            int? newClockId;
                                            string source = string.Empty;
                                            DateTime swipetime = DateTime.Now;

                                            EbiService ebiService = new EbiService();
                                            using (Ebi ebi = new Ebi())
                                            {
                                                ebi.CompanyName = (string)r["Company"];
                                                ebi.ClockId = NumberUtilities.Convert.parseStringToNullableInt((string)r["ClockID"]);
                                                ebi.FullName = (string)r["FullName"];
                                                ebi.AccessCardNo = NumberUtilities.Convert.parseStringToNullableInt((string)r["CardNumber"]);
                                                ebi.SwipeSite = (string)r["Site"];
                                                ebi.SwipeDateTime = (DateTime)r["SwipeDateTime"];
                                                ebi.SwipeYear = ((DateTime)r["SwipeDateTime"]).Year;
                                                ebi.SwipeMonth = ((DateTime)r["SwipeDateTime"]).Month;
                                                ebi.SwipeDay = ((DateTime)r["SwipeDateTime"]).Day;
                                                ebi.DataChecked = null;
                                                Ebi newEbi = ebiService.Save(ebi);
                                                newEbiId = newEbi.EbiId;
                                                newClockId = newEbi.ClockId;
                                                source = (string)r["Source"];
                                                swipetime = (DateTime)r["SwipeDateTime"];
                                            }

                                            ConnectionStringSettings conString = ConfigurationManager.ConnectionStrings["ALCOA_WAO_CSMWP_devConnectionString"];
                                            try
                                            {
                                                using (SqlConnection conn = new SqlConnection(conString.ConnectionString))
                                                {
                                                    if (newClockId != null)
                                                    {
                                                        using (SqlCommand cmd1 = new SqlCommand("Ebi_UpdateVendorNoSource", conn))
                                                        {
                                                            cmd1.CommandType = CommandType.StoredProcedure;
                                                            cmd1.Parameters.Add(new SqlParameter("@ClockId", newClockId));
                                                            cmd1.Parameters.Add(new SqlParameter("@EbiId", newEbiId));
                                                            cmd1.Parameters.Add(new SqlParameter("@SwipeDateTime", swipetime));
                                                            cmd1.Parameters.Add(new SqlParameter("@Source", source));

                                                            conn.Open();
                                                            cmd1.ExecuteNonQuery();
                                                            conn.Close();
                                                        }
                                                    }
                                                    else
                                                    {
                                                        using (SqlCommand cmd1 = new SqlCommand("Ebi_UpdateSource", conn))
                                                        {
                                                            cmd1.CommandType = CommandType.StoredProcedure;
                                                            cmd1.Parameters.Add(new SqlParameter("@EbiId", newEbiId));
                                                            cmd1.Parameters.Add(new SqlParameter("@Source", source));

                                                            conn.Open();
                                                            cmd1.ExecuteNonQuery();
                                                            conn.Close();
                                                        }
                                                    }
                                                }
                                            }
                                            catch (Exception ex)
                                            {
                                                Log4NetService.LogInfo("Error occurred while updating Vendor_Number and Source columns for ClockId=" + newClockId + " " + ex.Message);
                                            }
                                        }
                                        catch (Exception)
                                        {
                                        }
                                    }
                                    cn.Close();
                                }
                            }
                            catch (Exception ex)
                            {
                                Log4NetService.LogInfo("Error In " + s.EbiServerName + ": " + ex.Message);
                            }
                            Log4NetService.LogInfo("Finished for Site " + s.SiteNameEbi + ".\n\n");
                        }
                    }
                }
                catch (Exception ex)
                {
                    Log4NetService.LogInfo(ex.Message.ToString() + "\nServer Configuration Account Error. Contact Administrator.");
                }
                finally
                {
                }
            }

            public static void UpdateEbiAllVendorNumber()
            {
                int rows;
                ConnectionStringSettings conString = ConfigurationManager.ConnectionStrings["ALCOA_WAO_CSMWP_devConnectionString"];
                try
                {
                    using (SqlConnection conn = new SqlConnection(conString.ConnectionString))
                    {
                        using (SqlCommand cmd = new SqlCommand("Ebi_UpdateEbiAll", conn))
                        {
                            Log4NetService.LogInfo("Updating EBI Table...");
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.CommandTimeout = 500000000;
                            conn.Open();
                            rows = cmd.ExecuteNonQuery();
                            conn.Close();
                        }
                    }
                    if (rows >= 0)
                        Log4NetService.LogInfo("Successfully updated EBI Table");
                }
                catch (Exception ex)
                {
                    Log4NetService.LogInfo("Error occurred while updating EBI table: " + ex.Message);
                    throw;
                }
            }

            public static void UpdateEbiVendorNumberByYear(int Year)
            {
                int rows;
                ConnectionStringSettings conString = ConfigurationManager.ConnectionStrings["ALCOA_WAO_CSMWP_devConnectionString"];
                try
                {
                    using (SqlConnection conn = new SqlConnection(conString.ConnectionString))
                    {
                        using (SqlCommand cmd = new SqlCommand("Ebi_UpdateEbiVendorByYear", conn))
                        {
                            Log4NetService.LogInfo("Updating EBI Table Vendor Number for Year= "+Year);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.CommandTimeout = 500000000;
                            cmd.Parameters.Add(new SqlParameter("@Year", Year));
                            conn.Open();
                            rows = cmd.ExecuteNonQuery();
                            conn.Close();
                        }
                    }
                    if (rows >= 0)
                        Log4NetService.LogInfo("Successfully updated EBI Table");
                }
                catch (Exception ex)
                {
                    Log4NetService.LogInfo("Error occurred while updating EBI table: " + ex.Message);
                    throw;
                }
            }

            public static void UpdateCSMSEBICompaniesMap(int EbiId)
            {
                int rows;
                ConnectionStringSettings conString = ConfigurationManager.ConnectionStrings["ALCOA_WAO_CSMWP_devConnectionString"];
                try
                {
                    using (SqlConnection conn = new SqlConnection(conString.ConnectionString))
                    {
                        using (SqlCommand cmd = new SqlCommand("EbiCompaniesSitesMap_Insert", conn))
                        {
                            Log4NetService.LogInfo("Updating Ebi Companies Map Table...");
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add(new SqlParameter("@EbiId", EbiId));
                            cmd.CommandTimeout = 60000;
                            conn.Open();
                            rows = cmd.ExecuteNonQuery();
                            conn.Close();
                        }
                    }
                    if (rows > 0)
                        Log4NetService.LogInfo("Successfully updated Ebi Companies Map Table");
                }
                catch (Exception ex)
                {
                    Log4NetService.LogInfo("Error occurred while updating Ebi Companies Map table: " + ex.Message);
                }
            }

            public static void UpdateCSMSEbiHoursWorkedDump()
            {
                int rows;
                ConnectionStringSettings conString = ConfigurationManager.ConnectionStrings["ALCOA_WAO_CSMWP_devConnectionString"];
                try
                {
                    using (SqlConnection conn = new SqlConnection(conString.ConnectionString))
                    {
                        using (SqlCommand cmd = new SqlCommand("EbiHoursWorkedCompanyVendorMap_insert", conn))
                        {
                            Log4NetService.LogInfo("Updating Ebi Hours Worked Map Table...");
                            cmd.CommandType = CommandType.StoredProcedure;
                            conn.Open();
                            rows = cmd.ExecuteNonQuery();
                            conn.Close();
                        }
                    }
                    if (rows > 0)
                        Log4NetService.LogInfo("Successfully updated Ebi Hours Worked Map Table");
                }
                catch (Exception ex)
                {
                    Log4NetService.LogInfo("Error occurred while updating Ebi Hours Worked Map table: " + ex.Message);
                }
            }

            /// <summary>
            /// Updates the db from ebi_ CSV.
            /// </summary>
            /// <param name="myFile">My file.</param>
            static void FromEbi_Csv(string myFile)
            {
                if (!String.IsNullOrEmpty(myFile))
                {
                    Log4NetService.LogInfo("CSV File Ok.");
                    try
                    {
                        int lineCnt = 0;
                        using (CsvReader csv = new CsvReader(new StringReader(myFile), true))
                        {
                            int fieldCount = csv.FieldCount;
                            string[] headers = csv.GetFieldHeaders();
                            Log4NetService.LogInfo("Found Headers: " + String.Join(",", headers));

                            SitesService sService = new SitesService();
                            Sites sBooragoon = sService.GetBySiteAbbrev("BGN");
                            Sites sMccoy = sService.GetBySiteAbbrev("HUN");
                            Sites sPeel = sService.GetBySiteAbbrev("PNJ");
                            Sites sPinjarra = sService.GetBySiteAbbrev("PIN");
                            Sites sWagerup = sService.GetBySiteAbbrev("WGP");

                            CompaniesService cService = new CompaniesService();
                            TList<Companies> cListAll = cService.GetAll();

                            List<String> companiesMatch = new List<String>();
                            List<String> companiesNoMatch = new List<String>();

                            while (csv.ReadNextRecord())
                            {
                                try
                                {
                                    lineCnt++;
                                    string CompanyName = csv["CompanyName"].ToString();
                                    string ClockId = csv["ClockId"].ToString();
                                    string AccessCardNo = csv["AccessCardNo"].ToString();
                                    string Site = csv["Site"].ToString();
                                    string SwipeDateTime = csv["SwipeDateTime"].ToString();

                                    CultureInfo provider = CultureInfo.InvariantCulture;
                                    string datetimeFormat = "M/d/yyyy";
                                    //February2011.csv and ContractorSwipes_21-3-2011.csv onwards = "MM/dd/yyyy";
                                    DateTime dt = DateTime.ParseExact(SwipeDateTime, datetimeFormat, provider);
                                    DateTime _dt = new DateTime(dt.Year, dt.Month, 1);

                                    int SiteId = -1;
                                    if (Site == "BOORAGOON") SiteId = sBooragoon.SiteId;
                                    if (Site == "MCCOY") SiteId = sMccoy.SiteId;
                                    if (Site == "PEEL") SiteId = sPeel.SiteId;
                                    if (Site == "PINJARRA") SiteId = sPinjarra.SiteId;
                                    if (Site == "WAGERUP") SiteId = sWagerup.SiteId;

                                    if (SiteId != -1)
                                    {
                                        Companies c = cListAll.Find(CompaniesColumn.CompanyName, CompanyName, true);
                                        if (c == null)
                                        {
                                            if (companiesNoMatch.Contains(CompanyName))
                                            {
                                                //Already Exists.
                                            }
                                            else
                                            {
                                                companiesNoMatch.Add(CompanyName);
                                            }
                                        }
                                        else
                                        {
                                            if (companiesMatch.Contains(CompanyName))
                                            {
                                                //Already Exists.
                                            }
                                            else
                                            {
                                                companiesMatch.Add(CompanyName);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        //TODO write exception: Site Unknown..
                                    }
                                }
                                catch (Exception ex)
                                {
                                    Log4NetService.LogInfo("[ERROR] " + ex.Message.ToString());
                                }
                            }
                            Log4NetService.LogInfo("\n    Companies Matched: " + companiesMatch.Count.ToString());
                            Log4NetService.LogInfo(String.Format("Companies Not Matched: {0}\n", companiesNoMatch.Count));

                            if (companiesNoMatch.Count > 0)
                            {
                                Log4NetService.LogInfo("The following companies could not be matched up to CSMS company names: ");
                                foreach (string _l in companiesNoMatch)
                                {
                                    Log4NetService.LogInfo(_l);
                                }
                            }
                            Log4NetService.LogInfo(String.Format("\nEOF. Read: {0} Lines.", lineCnt));
                        }
                    }
                    catch (Exception ex)
                    {
                        Log4NetService.LogInfo("[ERROR] " + ex.Message.ToString());
                    }
                }
                else
                {
                    Log4NetService.LogInfo("CSV File Empty.");
                }
            }

            /// <summary>
            /// Updates the db from ebi_ CSV2.
            /// </summary>
            /// <param name="FileFullPath">The file full path.</param>
            static void FromEbi_Csv2(string FileFullPath, bool olddates)
            {
                if (!String.IsNullOrEmpty(FileFullPath))
                {
                    Log4NetService.LogInfo("CSV File Ok. Reading: " + FileFullPath);
                    try
                    {
                        int lineCnt = 0;
                        using (CsvReader csv = new CsvReader(new StreamReader(FileFullPath), true))
                        {
                            int fieldCount = csv.FieldCount;
                            string[] headers = csv.GetFieldHeaders();

                            while (csv.ReadNextRecord())
                            {
                                try
                                {
                                    lineCnt++;
                                    
                                    String dtString = csv[5].ToString();
                                    String dtFormat = "M/d/yyyy h:mm:ss tt";

                                    if (olddates)
                                    {
                                        dtFormat = "d/MM/yyyy h:mm:ss tt";
                                    }

                                    if (dtString.Contains("PM")) dtString = dtString.Replace("PM", " PM");
                                    if (dtString.Contains("AM")) dtString = dtString.Replace("AM", " AM");
                                    if (dtString.Contains("   ")) dtString = dtString.Replace("   ", " ");
                                    if (dtString.Contains("  ")) dtString = dtString.Replace("  ", " ");

                                    DateTime dt = DateTime.ParseExact(dtString, dtFormat, CultureInfo.InvariantCulture);
                                    int newEbiId;
                                    int? newClockId;

                                    EbiService ebiService = new EbiService();
                                    using (Ebi ebi = new Ebi())
                                    {
                                        ebi.CompanyName = csv[0].ToString();
                                        ebi.ClockId = NumberUtilities.Convert.parseStringToNullableInt(csv[1].ToString());
                                        ebi.FullName = csv[2].ToString();
                                        ebi.AccessCardNo = NumberUtilities.Convert.parseStringToNullableInt(csv[3].ToString());
                                        ebi.SwipeSite = csv[4].ToString();
                                        ebi.SwipeDateTime = dt;
                                        ebi.SwipeYear = dt.Year;
                                        ebi.SwipeMonth = dt.Month;
                                        ebi.SwipeDay = dt.Day;
                                        ebi.DataChecked = null;
                                        Ebi newEbi = ebiService.Save(ebi);
                                        newEbiId = newEbi.EbiId;
                                        newClockId = newEbi.ClockId;
                                    }

                                    //Updating Vendor_Number Column for the row from the DB Link
                                    ConnectionStringSettings conString = ConfigurationManager.ConnectionStrings["ALCOA_WAO_CSMWP_devConnectionString"];
                                    try
                                    {
                                        using (SqlConnection conn = new SqlConnection(conString.ConnectionString))
                                        {
                                            using (SqlCommand cmd = new SqlCommand("Ebi_UpdateVendorNo", conn))
                                            {
                                                cmd.CommandType = CommandType.StoredProcedure;
                                                cmd.Parameters.Add(new SqlParameter("@ClockId", newClockId));
                                                cmd.Parameters.Add(new SqlParameter("@EbiId", newEbiId));
                                                cmd.Parameters.Add(new SqlParameter("@SwipeDateTime", dt));
                                                if (newClockId != null)
                                                {
                                                    conn.Open();
                                                    cmd.ExecuteNonQuery();
                                                    conn.Close();
                                                }
                                            }
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        Log4NetService.LogInfo("Error occurred while updating Vendor_Number column for ClockId=" + newClockId + " " + ex.Message);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    Log4NetService.LogInfo(String.Format("[ERROR] Line {0}: {1}", lineCnt, ex.Message));
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Log4NetService.LogInfo("[ERROR] " + ex.Message.ToString());
                    }
                }
            }
            
            /// <summary>
            /// Updates the db from db ebi all.
            /// </summary>
            static void FromDbEbiAll()
            {
                EbiService ebiService = new EbiService();
                TList<Ebi> listEbi = ebiService.GetBySwipeYear(2006);
                foreach (Ebi ebi in listEbi)
                {
                }
            }

            /// <summary>
            /// Updates the db from ebi.
            /// </summary>
            /// <param name="when">The when.</param>
            /// <param name="opt">The opt.</param>
            public static void UpdateCsmsEbi(string when, string opt)
            {
                try
                {
                    string myFile = "CompanyName,ClockId,FullName,AccessCardNo,Site,SwipeDateTime\n";
                    string FileFullPath = "";
                    string sourcePath = "";
                    string r = "";

                    switch (when)
                    {
                        case "File":
                            Log4NetService.LogInfo("Updating CSMS Database from specific EBI Data file...");
                            if (String.IsNullOrEmpty(opt))
                            {
                                Log4NetService.LogInfo("\n No File Specified.");
                            }
                            else
                            {
                                FileFullPath = opt;
                                r = Helper.IO.ReadFile(FileFullPath);
                                if (!String.IsNullOrEmpty(r))
                                {
                                    myFile += r;
                                    FromEbi_Csv(myFile);
                                }
                                else
                                {
                                    Log4NetService.LogInfo("[ERROR] CSV File is Empty.");
                                }
                            }
                            break;
                        case "MostRecent":
                            Log4NetService.LogInfo("Updating CSMS Database from most recent EBI Data available...");
                            DateTime dtYesterday = DateTime.Now.AddDays(-1);
                            DateTime dtToday = DateTime.Now;
                            string MostRecentFile = String.Format("ContractorSwipes_{0}-{1}-{2}.csv", dtToday.Day, dtToday.Month, dtToday.Year);
                            sourcePath = "\\\\auapinebi\\ContractorReports\\CSVOutput"; //TODO: Read from Configuration Table
                            sourcePath = Helper.Configuration.GetValue(ConfigList.EbiYesterdayDir);
                            FileFullPath = String.Format("{0}\\{1}", sourcePath, MostRecentFile);

                            r = Helper.IO.ReadFile(FileFullPath);
                            if (!String.IsNullOrEmpty(r))
                            {
                                myFile += r;
                                FromEbi_Csv(myFile);
                            }
                            else
                            {
                                Log4NetService.LogInfo("[ERROR] CSV File is Empty.");
                            }
                            break;
                        case "AllFromYesterdayFolder":
                            Log4NetService.LogInfo("Updating CSMS Database from all csv files in Yesterday Folder...");
                            sourcePath = Helper.Configuration.GetValue(ConfigList.EbiYesterdayDir);
                            Log4NetService.LogInfo(String.Format("Looking in: \n{0}\n", sourcePath));

                            string[] fileListRecent = Directory.GetFiles(sourcePath, "*", SearchOption.TopDirectoryOnly);

                            Log4NetService.LogInfo(String.Format("Found {0} Files:", fileListRecent.Length));
                            foreach (string file in fileListRecent)
                            {
                                Log4NetService.LogInfo(file.Replace(sourcePath + "\\", ""));
                            }

                            Log4NetService.LogInfo("");

                            //Go thru each Year's File...
                            foreach (string file in fileListRecent)
                            {
                                if (file.Contains(".csv"))
                                {
                                    FromEbi_Csv2(file, false);
                                }
                                //Robocopy does this. Let's temp disable this and let the batch file do its job..
                                //Directory.Move(file, sourcePath + "Read\\" + file.Replace(sourcePath, ""));
                                //Log4NetService.LogInfo("Moving File from: '" + file + "' to '" + file.Replace("EBI\\Latest", "EBI\\Read") + "'");
                                //File.Move(file, file.Replace("EBI\\Latest", "EBI\\Read"));
                                //File.Move(file, file.Replace(".csv", "--Read.csv"));
                            }
                            break;
                        case "AllArchive":
                            Log4NetService.LogInfo("Updating CSMS Database from All EBI Data available (Historical)...");
                            sourcePath = "C:\\Temp\\ebiYear"; //TODO: Read from Configuration Table
                            sourcePath = Helper.Configuration.GetValue(ConfigList.EbiArchiveDir);
                            Log4NetService.LogInfo(String.Format("Looking in: \n{0}\n", sourcePath));

                            string[] fileList = Directory.GetFiles(sourcePath, "*", SearchOption.TopDirectoryOnly);

                            Log4NetService.LogInfo(String.Format("Found {0} Files:", fileList.Length));
                            foreach (string file in fileList)
                            {
                                Log4NetService.LogInfo(file.Replace(sourcePath + "\\", ""));
                            }

                            Log4NetService.LogInfo("");

                            //Go thru each Year's File...
                            foreach (string file in fileList)
                            {
                                r = Helper.IO.ReadFile(file);
                                if (!String.IsNullOrEmpty(r))
                                {
                                    myFile = "CompanyName,ClockId,FullName,AccessCardNo,Site,SwipeDateTime\n";
                                    myFile += r;
                                    FromEbi_Csv2(file, false);
                                }
                                else
                                {
                                    Log4NetService.LogInfo("[ERROR] CSV File is Empty.");
                                }
                            }

                            break;
                        case "AllArchive_olddates":
                            Log4NetService.LogInfo("Updating CSMS Database from All EBI Data available (Historical)...");
                            sourcePath = Helper.Configuration.GetValue(ConfigList.EbiArchiveDir) + "\\DAY-MONTH\\";
                            Log4NetService.LogInfo(String.Format("Looking in: \n{0}\n", sourcePath));

                            string[] fileList2 = Directory.GetFiles(sourcePath, "*", SearchOption.TopDirectoryOnly);

                            Log4NetService.LogInfo(String.Format("Found {0} Files:", fileList2.Length));
                            foreach (string file in fileList2)
                            {
                                Log4NetService.LogInfo(file.Replace(sourcePath + "\\", ""));
                            }

                            Log4NetService.LogInfo("");

                            //Go thru each Year's File...
                            foreach (string file in fileList2)
                            {
                                r = Helper.IO.ReadFile(file);
                                if (!String.IsNullOrEmpty(r))
                                {
                                    myFile = "CompanyName,ClockId,FullName,AccessCardNo,Site,SwipeDateTime\n";
                                    myFile += r;
                                    FromEbi_Csv2(file, true);
                                }
                                else
                                {
                                    Log4NetService.LogInfo("[ERROR] CSV File is Empty.");
                                }
                            }
                            break;
                        case "AllArchive2":
                            Log4NetService.LogInfo("Updating CSMS Database from All EBI Data available (Historical) in Ebi Database Table");
                            FromDbEbiAll();
                            break;
                        default:
                            throw new Exception("--UpdateDbFromEbi [All|MostRecent]: All or MostRecent option must be selected.");
                    }

                    string activity = "UpdateDbFromEbi";
                    if (!string.IsNullOrEmpty(when)) activity += " " + when;
                    HealthCheck.Update(true, activity);
                }
                catch (Exception ex)
                {
                    string activity = "UpdateDbFromEbi";
                    if (!string.IsNullOrEmpty(when)) activity += " " + when;
                    HealthCheck.Update(false, activity, "Exception: " + ex.Message);
                }
            }

            public static void UpdateCsmsKpi(CompanySiteCategoryStandard cscs, int year, int month, int? day)
            {
                //TODO: update ebionsite = null, if ebionsite = true when ebi shows false. (ebi data was updated?!)
                try
                {
                    EbiService ebiService = new EbiService();
                    DataSet ds = ebiService.CompaniesOnSite_ByYearMonthCompanyIdSiteId(year, month, cscs.CompanyId, cscs.SiteId);

                    if (ds.Tables[0] != null)
                    {
                        if (ds.Tables[0].Rows.Count != 0)
                        {
                            if (!String.IsNullOrEmpty(ds.Tables[0].Rows[0].ToString()))
                            {
                                int siteCnt = 0;
                                try
                                {
                                    siteCnt = Convert.ToInt32(ds.Tables[0].Rows[0].ItemArray[0].ToString());
                                }
                                catch (Exception) { }

                                DateTime dtMonth = new DateTime(year, month, 1);
                                KpiService kpiService = new KpiService();
                                Kpi kpi = kpiService.GetByCompanyIdSiteIdKpiDateTime(cscs.CompanyId, cscs.SiteId, dtMonth);

                                if (kpi == null)
                                {
                                    kpi = new Kpi();
                                    kpi.CreatedbyUserId = 0;
                                    kpi.ModifiedbyUserId = 0;
                                    kpi.DateAdded = DateTime.Now;
                                    kpi.Datemodified = DateTime.Now;
                                    kpi.KpiDateTime = dtMonth;
                                    kpi.CompanyId = cscs.CompanyId;
                                    kpi.SiteId = cscs.SiteId;
                                    kpi.KpiGeneral = 0;
                                    kpi.KpiCalcinerCapital = 0;
                                    kpi.KpiCalcinerExpense = 0;
                                    kpi.KpiResidue = 0;
                                    kpi.AheaRefineryWork = 0;
                                    kpi.AheaResidueWork = 0;
                                    kpi.AheaSmeltingWork = 0;
                                    kpi.AheaTotalManHours = 0;
                                    kpi.AheaPeakNopplSiteWeek = 0;
                                    kpi.AheaAvgNopplSiteMonth = 0;
                                    kpi.AheaNoEmpExcessMonth = 0;
                                    kpi.MtOthers = "";

                                    // Safety Plan submitted for year
                                    FileDbFilters query = new FileDbFilters();
                                    query.Append(FileDbColumn.CompanyId, cscs.CompanyId.ToString());
                                    query.Append(FileDbColumn.ModifiedDate, "%" + year.ToString() + "%");

                                    int count = 0;
                                    TList<FileDb> list = DataRepository.FileDbProvider.GetPaged(query.ToString(), null, 0, 100, out count);

                                    if (count > 0)
                                    {
                                        kpi.SafetyPlansSubmitted = true;
                                    }
                                    else
                                    {
                                        kpi.SafetyPlansSubmitted = false;
                                    }
                                }

                                if (siteCnt > 0)
                                {
                                    if (kpi.EbiOnSite != true)
                                    {
                                        kpi.EbiOnSite = true;
                                        kpi.IsSystemEdit = true;
                                        kpiService.Save(kpi);
                                    }
                                }
                                else
                                {
                                    kpi.EbiOnSite = false;
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Log4NetService.LogInfo("error: " + ex.Message);
                }
                month++;
            }
            
            /// <summary>
            /// Updates Kpi.EbiOnSite (bit) dependant on EBI Data... using ALL EBI Data in Database
            /// </summary>
            public static void UpdateCsmsKpi(string when)
            {
                try
                {
                    CompanySiteCategoryStandardService cscsService = new CompanySiteCategoryStandardService();
                    TList<CompanySiteCategoryStandard> cscsAll = cscsService.GetAll();
                    foreach (CompanySiteCategoryStandard cscs in cscsAll)
                    {
                        //do current year only for now...
                        if (cscs.CompanySiteCategoryId != null)
                        {
                            int year = DateTime.Now.Year;
                            int month = 1;

                            if (String.IsNullOrEmpty(when))
                            {
                                while (month <= 12)
                                {
                                    UpdateCsmsKpi(cscs, year, month, null);
                                    month++;
                                }
                            }
                            else
                            {
                                if (when == "month")
                                {
                                    month = DateTime.Now.Month;
                                    UpdateCsmsKpi(cscs, year, month, null);
                                }
                                else if (when == "yesterday")
                                {
                                    //month = DateTime.Now.Month;
                                    //int day = DateTime.Now.AddDays(-1).Day;
                                    //UpdateKpiFromEbiDb(cscs, year, month, day);
                                    throw new Exception("Not Implemented Yet");
                                }
                            }
                        }
                    }
                    HealthCheck.Update(true, "UpdateKpiFromEbiDb");
                }
                catch (Exception ex)
                {
                    HealthCheck.Update(false, "UpdateKpiFromEbiDb", null, ex);
                }
                finally
                {
                }
            }

            /// <summary>
            /// ~2m to run.
            /// Should run before 'ALCOA.CSMS.BatchJobScripts.EBI'
            /// </summary>
            public static void ExportCsv(string When, int? Year, int? Month, int? Day)
            {
                //Connect to all EBI Servers and dump CSV File :)
                try
                {
                    string saveFolder = "";
                    string fileName = "";
                    Configuration config = new Configuration();
                    if (When == "Yesterday")
                    {
                        saveFolder = config.GetValue(ConfigList.EbiYesterdayDir);
                        DateTime dtYesterday = DateTime.Now.AddDays(-1);
                        fileName = String.Format("ContractorSwipes_{0}-{1}-{2}.csv", dtYesterday.Day, dtYesterday.Month, dtYesterday.Year);
                    }
                    else if (When == "Today")
                    {
                        saveFolder = config.GetValue(ConfigList.EbiTodayDir);
                        DateTime dtYesterday = DateTime.Now.AddDays(-1);
                        fileName = String.Format("ContractorSwipes2_{0}-{1}-{2}.csv", dtYesterday.Day, dtYesterday.Month, dtYesterday.Year);
                    }
                    else if (When == "All")
                    {
                        saveFolder = config.GetValue(ConfigList.EbiAllFilePath);
                        if (Year != null)
                        {
                            fileName = String.Format("ContractorSwipes_{0}.csv", Year);
                            if (Month != null)
                            {
                                fileName = String.Format("ContractorSwipes_{0}-{1}.csv", Month, Year);
                                if (Day != null)
                                {
                                    fileName = String.Format("ContractorSwipes_{0}-{1}-{2}.csv", Day, Month, Year);
                                }
                            }
                        }
                        else
                        {
                            fileName = String.Format("ContractorSwipes_All.csv");
                        }
                    }

                    if (String.IsNullOrEmpty(saveFolder) || String.IsNullOrEmpty(fileName))
                    {
                        throw new Exception("Expected 'Yesterday' or 'Today' or 'All' or 'All [Year]'");
                    }

                    string csvHeaders = "CompanyName,ClockId,FullName,AccessCardNo,Site,SwipeDateTime";

                    TextWriter tw = new StreamWriter(saveFolder + fileName, false);
                    tw.WriteLine(csvHeaders);

                    SitesService sService = new SitesService();
                    TList<Sites> sitesAll = sService.GetAll();
                    foreach (Sites s in sitesAll)
                    {
                        if (!String.IsNullOrEmpty(s.EbiServerName) && !String.IsNullOrEmpty(s.EbiAllViewName) && !String.IsNullOrEmpty(s.EbiTodayViewName))
                        {
                            try
                            {
                                string server = s.EbiServerName;

                                //TODO Temporary. Next Upgrade, add column 'EbiYesterdayViewName'
                                //Yesterday
                                string view = s.EbiAllViewName.Replace("_All_v2", ""); 
                                if (When == "Today")
                                {
                                    view = s.EbiTodayViewName;
                                }
                                else if (When == "All")
                                {
                                    view = s.EbiAllViewName;
                                    if (Year != null)
                                    {
                                        view = s.EbiAllViewName + " WHERE YEAR(SwipeDateTime) = " + Year.ToString();
                                        if (Month != null)
                                        {
                                            view = s.EbiAllViewName + " WHERE YEAR(SwipeDateTime) = " + Year.ToString() + " MONTH(SwipeDateTime) = " + Month.ToString();
                                            if (Day != null)
                                            {
                                                view = s.EbiAllViewName + " WHERE YEAR(SwipeDateTime) = " + Year.ToString() + " AND MONTH(SwipeDateTime) = " +
                                                            Month.ToString() + " AND DAY(SwipeDateTime) = " + Day.ToString();
                                            }
                                        }
                                    }
                                }

                                ConnectionStringSettings conString = ConfigurationManager.ConnectionStrings[String.Format("Alcoa.Ebi.{0}.ConnectionString", server)];
                                string strConnString = conString.ConnectionString;
                                Log4NetService.LogInfo("Connecting to: " + server + "...");
                                using (SqlConnection cn = new SqlConnection(strConnString))
                                {
                                    cn.Open();
                                    Log4NetService.LogInfo("Reading View: " + view + "...");
                                    SqlCommand cmd = new SqlCommand("SELECT * FROM " + view, cn);
                                    cmd.CommandTimeout = 60000;
                                    SqlDataReader r = cmd.ExecuteReader();
                                    Log4NetService.LogInfo("Read...");
                                    
                                    int rowCnt = 0;
                                    while (r.Read())
                                    {
                                        rowCnt++;
                                        try
                                        {
                                            string CompanyName = (string)r["Company"];
                                            string ClockId = (string)r["ClockID"];
                                            string FullName = (string)r["FullName"];
                                            string AccessCardNo = (string)r["CardNumber"];
                                            string Site = (string)r["Site"];
                                            DateTime SwipeDateTime = (DateTime)r["SwipeDateTime"];
                                            string SwipeDateTime_Formatted = "";

                                            String dtFormat = "M/d/yyyy $ h:mm:ss # tt";

                                            SwipeDateTime_Formatted = SwipeDateTime.ToString(dtFormat, CultureInfo.InvariantCulture);
                                            SwipeDateTime_Formatted = SwipeDateTime_Formatted.Replace(" $ ", "   ");
                                            SwipeDateTime_Formatted = SwipeDateTime_Formatted.Replace(" # ", "");

                                            tw.WriteLine("\"" + CompanyName + "\",\"" + ClockId + "\",\"" + FullName + "\",\"" + AccessCardNo + "\",\"" + Site + "\",\"" + SwipeDateTime_Formatted + "\"");
                                        }
                                        catch (Exception ex)
                                        {
                                            Log4NetService.LogInfo("Error In Row " + rowCnt + ": " + ex.Message);
                                        }
                                    }
                                    Log4NetService.LogInfo("Finished.\n\n");
                                    cn.Close();
                                }
                            }
                            catch (Exception ex)
                            {
                                Log4NetService.LogInfo("Error In " + s.EbiServerName + ": " + ex.Message);
                            }
                        }
                    }
                    tw.Close();
                }
                catch (Exception ex)
                {
                    Log4NetService.LogInfo(ex.Message.ToString() + "\nServer Configuration Account Error. Contact Administrator.");
                }
                finally
                {
                }
            }
        }
    }
}
