﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.CsaProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(CsaDataSourceDesigner))]
	public class CsaDataSource : ProviderDataSource<Csa, CsaKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CsaDataSource class.
		/// </summary>
		public CsaDataSource() : base(new CsaService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the CsaDataSourceView used by the CsaDataSource.
		/// </summary>
		protected CsaDataSourceView CsaView
		{
			get { return ( View as CsaDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the CsaDataSource control invokes to retrieve data.
		/// </summary>
		public CsaSelectMethod SelectMethod
		{
			get
			{
				CsaSelectMethod selectMethod = CsaSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (CsaSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the CsaDataSourceView class that is to be
		/// used by the CsaDataSource.
		/// </summary>
		/// <returns>An instance of the CsaDataSourceView class.</returns>
		protected override BaseDataSourceView<Csa, CsaKey> GetNewDataSourceView()
		{
			return new CsaDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the CsaDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class CsaDataSourceView : ProviderDataSourceView<Csa, CsaKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CsaDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the CsaDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public CsaDataSourceView(CsaDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal CsaDataSource CsaOwner
		{
			get { return Owner as CsaDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal CsaSelectMethod SelectMethod
		{
			get { return CsaOwner.SelectMethod; }
			set { CsaOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal CsaService CsaProvider
		{
			get { return Provider as CsaService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<Csa> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<Csa> results = null;
			Csa item;
			count = 0;
			
			System.Int32 _csaId;
			System.Int32 _companyId;
			System.Int32 _siteId;
			System.Int32 _qtrId;
			System.Int64 _year;
			System.Int32 _createdByUserId;
			System.Int32 _modifiedByUserId;

			switch ( SelectMethod )
			{
				case CsaSelectMethod.Get:
					CsaKey entityKey  = new CsaKey();
					entityKey.Load(values);
					item = CsaProvider.Get(entityKey);
					results = new TList<Csa>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case CsaSelectMethod.GetAll:
                    results = CsaProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case CsaSelectMethod.GetPaged:
					results = CsaProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case CsaSelectMethod.Find:
					if ( FilterParameters != null )
						results = CsaProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = CsaProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case CsaSelectMethod.GetByCsaId:
					_csaId = ( values["CsaId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["CsaId"], typeof(System.Int32)) : (int)0;
					item = CsaProvider.GetByCsaId(_csaId);
					results = new TList<Csa>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				case CsaSelectMethod.GetByCompanyIdSiteIdQtrIdYear:
					_companyId = ( values["CompanyId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["CompanyId"], typeof(System.Int32)) : (int)0;
					_siteId = ( values["SiteId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["SiteId"], typeof(System.Int32)) : (int)0;
					_qtrId = ( values["QtrId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["QtrId"], typeof(System.Int32)) : (int)0;
					_year = ( values["Year"] != null ) ? (System.Int64) EntityUtil.ChangeType(values["Year"], typeof(System.Int64)) : (long)0;
					item = CsaProvider.GetByCompanyIdSiteIdQtrIdYear(_companyId, _siteId, _qtrId, _year);
					results = new TList<Csa>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// FK
				case CsaSelectMethod.GetByCreatedByUserId:
					_createdByUserId = ( values["CreatedByUserId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["CreatedByUserId"], typeof(System.Int32)) : (int)0;
					results = CsaProvider.GetByCreatedByUserId(_createdByUserId, this.StartIndex, this.PageSize, out count);
					break;
				case CsaSelectMethod.GetByModifiedByUserId:
					_modifiedByUserId = ( values["ModifiedByUserId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["ModifiedByUserId"], typeof(System.Int32)) : (int)0;
					results = CsaProvider.GetByModifiedByUserId(_modifiedByUserId, this.StartIndex, this.PageSize, out count);
					break;
				case CsaSelectMethod.GetByCompanyId:
					_companyId = ( values["CompanyId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["CompanyId"], typeof(System.Int32)) : (int)0;
					results = CsaProvider.GetByCompanyId(_companyId, this.StartIndex, this.PageSize, out count);
					break;
				case CsaSelectMethod.GetBySiteId:
					_siteId = ( values["SiteId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["SiteId"], typeof(System.Int32)) : (int)0;
					results = CsaProvider.GetBySiteId(_siteId, this.StartIndex, this.PageSize, out count);
					break;
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == CsaSelectMethod.Get || SelectMethod == CsaSelectMethod.GetByCsaId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				Csa entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					CsaProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<Csa> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			CsaProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region CsaDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the CsaDataSource class.
	/// </summary>
	public class CsaDataSourceDesigner : ProviderDataSourceDesigner<Csa, CsaKey>
	{
		/// <summary>
		/// Initializes a new instance of the CsaDataSourceDesigner class.
		/// </summary>
		public CsaDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public CsaSelectMethod SelectMethod
		{
			get { return ((CsaDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new CsaDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region CsaDataSourceActionList

	/// <summary>
	/// Supports the CsaDataSourceDesigner class.
	/// </summary>
	internal class CsaDataSourceActionList : DesignerActionList
	{
		private CsaDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the CsaDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public CsaDataSourceActionList(CsaDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public CsaSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion CsaDataSourceActionList
	
	#endregion CsaDataSourceDesigner
	
	#region CsaSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the CsaDataSource.SelectMethod property.
	/// </summary>
	public enum CsaSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByCsaId method.
		/// </summary>
		GetByCsaId,
		/// <summary>
		/// Represents the GetByCompanyIdSiteIdQtrIdYear method.
		/// </summary>
		GetByCompanyIdSiteIdQtrIdYear,
		/// <summary>
		/// Represents the GetByCreatedByUserId method.
		/// </summary>
		GetByCreatedByUserId,
		/// <summary>
		/// Represents the GetByModifiedByUserId method.
		/// </summary>
		GetByModifiedByUserId,
		/// <summary>
		/// Represents the GetByCompanyId method.
		/// </summary>
		GetByCompanyId,
		/// <summary>
		/// Represents the GetBySiteId method.
		/// </summary>
		GetBySiteId
	}
	
	#endregion CsaSelectMethod

	#region CsaFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Csa"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CsaFilter : SqlFilter<CsaColumn>
	{
	}
	
	#endregion CsaFilter

	#region CsaExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Csa"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CsaExpressionBuilder : SqlExpressionBuilder<CsaColumn>
	{
	}
	
	#endregion CsaExpressionBuilder	

	#region CsaProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;CsaChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="Csa"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CsaProperty : ChildEntityProperty<CsaChildEntityTypes>
	{
	}
	
	#endregion CsaProperty
}

