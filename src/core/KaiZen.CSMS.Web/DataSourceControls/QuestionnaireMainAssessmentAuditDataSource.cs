﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.QuestionnaireMainAssessmentAuditProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(QuestionnaireMainAssessmentAuditDataSourceDesigner))]
	public class QuestionnaireMainAssessmentAuditDataSource : ProviderDataSource<QuestionnaireMainAssessmentAudit, QuestionnaireMainAssessmentAuditKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainAssessmentAuditDataSource class.
		/// </summary>
		public QuestionnaireMainAssessmentAuditDataSource() : base(new QuestionnaireMainAssessmentAuditService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the QuestionnaireMainAssessmentAuditDataSourceView used by the QuestionnaireMainAssessmentAuditDataSource.
		/// </summary>
		protected QuestionnaireMainAssessmentAuditDataSourceView QuestionnaireMainAssessmentAuditView
		{
			get { return ( View as QuestionnaireMainAssessmentAuditDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the QuestionnaireMainAssessmentAuditDataSource control invokes to retrieve data.
		/// </summary>
		public QuestionnaireMainAssessmentAuditSelectMethod SelectMethod
		{
			get
			{
				QuestionnaireMainAssessmentAuditSelectMethod selectMethod = QuestionnaireMainAssessmentAuditSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (QuestionnaireMainAssessmentAuditSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the QuestionnaireMainAssessmentAuditDataSourceView class that is to be
		/// used by the QuestionnaireMainAssessmentAuditDataSource.
		/// </summary>
		/// <returns>An instance of the QuestionnaireMainAssessmentAuditDataSourceView class.</returns>
		protected override BaseDataSourceView<QuestionnaireMainAssessmentAudit, QuestionnaireMainAssessmentAuditKey> GetNewDataSourceView()
		{
			return new QuestionnaireMainAssessmentAuditDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the QuestionnaireMainAssessmentAuditDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class QuestionnaireMainAssessmentAuditDataSourceView : ProviderDataSourceView<QuestionnaireMainAssessmentAudit, QuestionnaireMainAssessmentAuditKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainAssessmentAuditDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the QuestionnaireMainAssessmentAuditDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public QuestionnaireMainAssessmentAuditDataSourceView(QuestionnaireMainAssessmentAuditDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal QuestionnaireMainAssessmentAuditDataSource QuestionnaireMainAssessmentAuditOwner
		{
			get { return Owner as QuestionnaireMainAssessmentAuditDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal QuestionnaireMainAssessmentAuditSelectMethod SelectMethod
		{
			get { return QuestionnaireMainAssessmentAuditOwner.SelectMethod; }
			set { QuestionnaireMainAssessmentAuditOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal QuestionnaireMainAssessmentAuditService QuestionnaireMainAssessmentAuditProvider
		{
			get { return Provider as QuestionnaireMainAssessmentAuditService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<QuestionnaireMainAssessmentAudit> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<QuestionnaireMainAssessmentAudit> results = null;
			QuestionnaireMainAssessmentAudit item;
			count = 0;
			
			System.Int32 _auditId;

			switch ( SelectMethod )
			{
				case QuestionnaireMainAssessmentAuditSelectMethod.Get:
					QuestionnaireMainAssessmentAuditKey entityKey  = new QuestionnaireMainAssessmentAuditKey();
					entityKey.Load(values);
					item = QuestionnaireMainAssessmentAuditProvider.Get(entityKey);
					results = new TList<QuestionnaireMainAssessmentAudit>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case QuestionnaireMainAssessmentAuditSelectMethod.GetAll:
                    results = QuestionnaireMainAssessmentAuditProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case QuestionnaireMainAssessmentAuditSelectMethod.GetPaged:
					results = QuestionnaireMainAssessmentAuditProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case QuestionnaireMainAssessmentAuditSelectMethod.Find:
					if ( FilterParameters != null )
						results = QuestionnaireMainAssessmentAuditProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = QuestionnaireMainAssessmentAuditProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case QuestionnaireMainAssessmentAuditSelectMethod.GetByAuditId:
					_auditId = ( values["AuditId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["AuditId"], typeof(System.Int32)) : (int)0;
					item = QuestionnaireMainAssessmentAuditProvider.GetByAuditId(_auditId);
					results = new TList<QuestionnaireMainAssessmentAudit>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				// FK
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == QuestionnaireMainAssessmentAuditSelectMethod.Get || SelectMethod == QuestionnaireMainAssessmentAuditSelectMethod.GetByAuditId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				QuestionnaireMainAssessmentAudit entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					QuestionnaireMainAssessmentAuditProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<QuestionnaireMainAssessmentAudit> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			QuestionnaireMainAssessmentAuditProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region QuestionnaireMainAssessmentAuditDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the QuestionnaireMainAssessmentAuditDataSource class.
	/// </summary>
	public class QuestionnaireMainAssessmentAuditDataSourceDesigner : ProviderDataSourceDesigner<QuestionnaireMainAssessmentAudit, QuestionnaireMainAssessmentAuditKey>
	{
		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainAssessmentAuditDataSourceDesigner class.
		/// </summary>
		public QuestionnaireMainAssessmentAuditDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public QuestionnaireMainAssessmentAuditSelectMethod SelectMethod
		{
			get { return ((QuestionnaireMainAssessmentAuditDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new QuestionnaireMainAssessmentAuditDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region QuestionnaireMainAssessmentAuditDataSourceActionList

	/// <summary>
	/// Supports the QuestionnaireMainAssessmentAuditDataSourceDesigner class.
	/// </summary>
	internal class QuestionnaireMainAssessmentAuditDataSourceActionList : DesignerActionList
	{
		private QuestionnaireMainAssessmentAuditDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainAssessmentAuditDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public QuestionnaireMainAssessmentAuditDataSourceActionList(QuestionnaireMainAssessmentAuditDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public QuestionnaireMainAssessmentAuditSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion QuestionnaireMainAssessmentAuditDataSourceActionList
	
	#endregion QuestionnaireMainAssessmentAuditDataSourceDesigner
	
	#region QuestionnaireMainAssessmentAuditSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the QuestionnaireMainAssessmentAuditDataSource.SelectMethod property.
	/// </summary>
	public enum QuestionnaireMainAssessmentAuditSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByAuditId method.
		/// </summary>
		GetByAuditId
	}
	
	#endregion QuestionnaireMainAssessmentAuditSelectMethod

	#region QuestionnaireMainAssessmentAuditFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireMainAssessmentAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireMainAssessmentAuditFilter : SqlFilter<QuestionnaireMainAssessmentAuditColumn>
	{
	}
	
	#endregion QuestionnaireMainAssessmentAuditFilter

	#region QuestionnaireMainAssessmentAuditExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireMainAssessmentAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireMainAssessmentAuditExpressionBuilder : SqlExpressionBuilder<QuestionnaireMainAssessmentAuditColumn>
	{
	}
	
	#endregion QuestionnaireMainAssessmentAuditExpressionBuilder	

	#region QuestionnaireMainAssessmentAuditProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;QuestionnaireMainAssessmentAuditChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireMainAssessmentAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireMainAssessmentAuditProperty : ChildEntityProperty<QuestionnaireMainAssessmentAuditChildEntityTypes>
	{
	}
	
	#endregion QuestionnaireMainAssessmentAuditProperty
}

