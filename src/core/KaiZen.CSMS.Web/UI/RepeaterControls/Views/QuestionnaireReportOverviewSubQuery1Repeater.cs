﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.ComponentModel;
using System.Web.UI.Design.WebControls;
using System.Web.UI.Design;
using System.Web.UI.WebControls;

namespace KaiZen.CSMS.Web.UI
{
    /// <summary>
    /// A designer class for a strongly typed repeater <c>QuestionnaireReportOverviewSubQuery1Repeater</c>
    /// </summary>
	public class QuestionnaireReportOverviewSubQuery1RepeaterDesigner : System.Web.UI.Design.ControlDesigner
	{
	    /// <summary>
        /// Initializes a new instance of the <see cref="T:QuestionnaireReportOverviewSubQuery1RepeaterDesigner"/> class.
        /// </summary>
		public QuestionnaireReportOverviewSubQuery1RepeaterDesigner()
		{
		}

        /// <summary>
        /// Initializes the control designer and loads the specified component.
        /// </summary>
        /// <param name="component">The control being designed.</param>
		public override void Initialize(IComponent component)
		{
			if (!(component is QuestionnaireReportOverviewSubQuery1Repeater))
			{ 
				throw new ArgumentException("Component is not a QuestionnaireReportOverviewSubQuery1Repeater."); 
			} 
			base.Initialize(component); 
			base.SetViewFlags(ViewFlags.TemplateEditing, true); 
		}


		/// <summary>
		/// Generate HTML for the designer
		/// </summary>
		/// <returns>a string of design time HTML</returns>
		public override string GetDesignTimeHtml()
		{

			// Get the instance this designer applies to
			//
			QuestionnaireReportOverviewSubQuery1Repeater z = (QuestionnaireReportOverviewSubQuery1Repeater)Component;
			z.DataBind();

			return base.GetDesignTimeHtml();
		}
	}

    /// <summary>
    /// A strongly typed repeater control for the <see cref="QuestionnaireReportOverviewSubQuery1Repeater"/> Type.
    /// </summary>
	[Designer(typeof(QuestionnaireReportOverviewSubQuery1RepeaterDesigner))]
	[ParseChildren(true)]
	[ToolboxData("<{0}:QuestionnaireReportOverviewSubQuery1Repeater runat=\"server\"></{0}:QuestionnaireReportOverviewSubQuery1Repeater>")]
	public class QuestionnaireReportOverviewSubQuery1Repeater : CompositeDataBoundControl, System.Web.UI.INamingContainer
	{
	    /// <summary>
        /// Initializes a new instance of the <see cref="T:QuestionnaireReportOverviewSubQuery1Repeater"/> class.
        /// </summary>
		public QuestionnaireReportOverviewSubQuery1Repeater()
		{
		}

		/// <summary>
        /// Gets a <see cref="T:System.Web.UI.ControlCollection"></see> object that represents the child controls for a specified server control in the UI hierarchy.
        /// </summary>
        /// <value></value>
        /// <returns>The collection of child controls for the specified server control.</returns>
		public override ControlCollection Controls
		{
			get
			{
				this.EnsureChildControls();
				return base.Controls;
			}
		}

		private ITemplate m_headerTemplate;
		/// <summary>
        /// Gets or sets the header template.
        /// </summary>
        /// <value>The header template.</value>
		[Browsable(false)]
		[TemplateContainer(typeof(QuestionnaireReportOverviewSubQuery1Item))]
		[PersistenceMode(PersistenceMode.InnerDefaultProperty)]
		public ITemplate HeaderTemplate
		{
			get { return m_headerTemplate; }
			set { m_headerTemplate = value; }
		}

		private ITemplate m_itemTemplate;
		/// <summary>
        /// Gets or sets the item template.
        /// </summary>
        /// <value>The item template.</value>
		[Browsable(false)]
		[TemplateContainer(typeof(QuestionnaireReportOverviewSubQuery1Item))]
		[PersistenceMode(PersistenceMode.InnerDefaultProperty)]
		public ITemplate ItemTemplate
		{
			get { return m_itemTemplate; }
			set { m_itemTemplate = value; }
		}

		private ITemplate m_seperatorTemplate;
		/// <summary>
        /// Gets or sets the Seperator Template
        /// </summary>
        [Browsable(false)]
        [TemplateContainer(typeof(QuestionnaireReportOverviewSubQuery1Item))]
        [PersistenceMode(PersistenceMode.InnerDefaultProperty)]
        public ITemplate SeperatorTemplate
        {
            get { return m_seperatorTemplate; }
            set { m_seperatorTemplate = value; }
        }

		private ITemplate m_altenateItemTemplate;
        /// <summary>
        /// Gets or sets the alternating item template.
        /// </summary>
        /// <value>The alternating item template.</value>
		[Browsable(false)]
		[TemplateContainer(typeof(QuestionnaireReportOverviewSubQuery1Item))]
		[PersistenceMode(PersistenceMode.InnerDefaultProperty)]
		public ITemplate AlternatingItemTemplate
		{
			get { return m_altenateItemTemplate; }
			set { m_altenateItemTemplate = value; }
		}

		private ITemplate m_footerTemplate;
        /// <summary>
        /// Gets or sets the footer template.
        /// </summary>
        /// <value>The footer template.</value>
		[Browsable(false)]
		[TemplateContainer(typeof(QuestionnaireReportOverviewSubQuery1Item))]
		[PersistenceMode(PersistenceMode.InnerDefaultProperty)]
		public ITemplate FooterTemplate
		{
			get { return m_footerTemplate; }
			set { m_footerTemplate = value; }
		}
		
		
		/// <summary>
        /// Overridden and Empty so that span tags are not written
        /// </summary>
        /// <param name="writer"></param>
        public override void RenderBeginTag(HtmlTextWriter writer)
        {
            
        }

        /// <summary>
        /// Overridden and Empty so that span tags are not written
        /// </summary>
        /// <param name="writer"></param>
        public override void RenderEndTag(HtmlTextWriter writer)
        {
                
        }		

//      /// <summary>
//      /// Called by the ASP.NET page framework to notify server controls that use composition-based implementation to create any child controls they contain in preparation for posting back or rendering.
//      /// </summary>
//		protected override void CreateChildControls()
//		{
//			if (ChildControlsCreated)
//			{
//				return;
//			}
//			Controls.Clear();
//
//			if (m_headerTemplate != null)
//			{
//				Control headerItem = new Control();
//				m_headerTemplate.InstantiateIn(headerItem);
//				Controls.Add(headerItem);
//			}
//
//			
//			if (m_footerTemplate != null)
//			{
//				Control footerItem = new Control();
//				m_footerTemplate.InstantiateIn(footerItem);
//				Controls.Add(footerItem);
//			}
//			ChildControlsCreated = true;
//		}
		
		/// <summary>
      /// Called by the ASP.NET page framework to notify server controls that use composition-based implementation to create any child controls they contain in preparation for posting back or rendering.
      /// </summary>
		protected override int CreateChildControls(System.Collections.IEnumerable dataSource, bool dataBinding)
      {
         int pos = 0;

         if (dataBinding)
         {
            //Instantiate the Header template (if exists)
            if (m_headerTemplate != null)
            {
                Control headerItem = new Control();
                m_headerTemplate.InstantiateIn(headerItem);
                Controls.Add(headerItem);
            }
			if (dataSource != null)
			{
				foreach (object o in dataSource)
				{
						KaiZen.CSMS.Entities.QuestionnaireReportOverviewSubQuery1 entity = o as KaiZen.CSMS.Entities.QuestionnaireReportOverviewSubQuery1;
						QuestionnaireReportOverviewSubQuery1Item container = new QuestionnaireReportOverviewSubQuery1Item(entity);
	
						if (m_itemTemplate != null && (pos % 2) == 0)
						{
							m_itemTemplate.InstantiateIn(container);
							
							if (m_seperatorTemplate != null)
							{
								m_seperatorTemplate.InstantiateIn(container);
							}
						}
						else
						{
							if (m_altenateItemTemplate != null)
							{
								m_altenateItemTemplate.InstantiateIn(container);
								if (m_seperatorTemplate != null)
								{
									m_seperatorTemplate.InstantiateIn(container);
								}

							}
							else if (m_itemTemplate != null)
							{
								m_itemTemplate.InstantiateIn(container);
								if (m_seperatorTemplate != null)
								{
									m_seperatorTemplate.InstantiateIn(container);
								}

							}
							else
							{
								// no template !!!
							}
						}
						Controls.Add(container);
						
						container.DataBind();
						
						pos++;
				}
			}            
			//Instantiate the Footer template (if exists)
            if (m_footerTemplate != null)
            {
                Control footerItem = new Control();
                m_footerTemplate.InstantiateIn(footerItem);
                Controls.Add(footerItem);
            }				
		 }
			
			return pos;
		}
		
      /// <summary>
      /// Raises the <see cref="E:System.Web.UI.Control.PreRender"></see> event.
      /// </summary>
      /// <param name="e">An <see cref="T:System.EventArgs"></see> object that contains the event data.</param>
		protected override void OnPreRender(EventArgs e)
		{
			base.DataBind();
		}

		#region Design time
        /// <summary>
        /// Renders at design time.
        /// </summary>
        /// <returns>a  string of the Designed HTML</returns>
		internal string RenderAtDesignTime()
		{			
			return "Designer currently not implemented"; 
		}

		#endregion
	}

    /// <summary>
    /// A wrapper type for the entity
    /// </summary>
	[System.ComponentModel.ToolboxItem(false)]
	public class QuestionnaireReportOverviewSubQuery1Item : System.Web.UI.Control, System.Web.UI.INamingContainer
	{
		private KaiZen.CSMS.Entities.QuestionnaireReportOverviewSubQuery1 _entity;

        /// <summary>
        /// Initializes a new instance of the <see cref="T:QuestionnaireReportOverviewSubQuery1Item"/> class.
        /// </summary>
		public QuestionnaireReportOverviewSubQuery1Item()
			: base()
		{ }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:QuestionnaireReportOverviewSubQuery1Item"/> class.
        /// </summary>
		public QuestionnaireReportOverviewSubQuery1Item(KaiZen.CSMS.Entities.QuestionnaireReportOverviewSubQuery1 entity)
			: base()
		{
			_entity = entity;
		}
		
        /// <summary>
        /// Gets the CompanyId
        /// </summary>
        /// <value>The CompanyId.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32 CompanyId
		{
			get { return _entity.CompanyId; }
		}
        /// <summary>
        /// Gets the Kwi
        /// </summary>
        /// <value>The Kwi.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Kwi
		{
			get { return _entity.Kwi; }
		}
        /// <summary>
        /// Gets the KwiCompanySiteCategory
        /// </summary>
        /// <value>The KwiCompanySiteCategory.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String KwiCompanySiteCategory
		{
			get { return _entity.KwiCompanySiteCategory; }
		}
        /// <summary>
        /// Gets the Pin
        /// </summary>
        /// <value>The Pin.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Pin
		{
			get { return _entity.Pin; }
		}
        /// <summary>
        /// Gets the PinCompanySiteCategory
        /// </summary>
        /// <value>The PinCompanySiteCategory.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String PinCompanySiteCategory
		{
			get { return _entity.PinCompanySiteCategory; }
		}
        /// <summary>
        /// Gets the Wgp
        /// </summary>
        /// <value>The Wgp.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Wgp
		{
			get { return _entity.Wgp; }
		}
        /// <summary>
        /// Gets the WgpCompanySiteCategory
        /// </summary>
        /// <value>The WgpCompanySiteCategory.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String WgpCompanySiteCategory
		{
			get { return _entity.WgpCompanySiteCategory; }
		}
        /// <summary>
        /// Gets the Hun
        /// </summary>
        /// <value>The Hun.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Hun
		{
			get { return _entity.Hun; }
		}
        /// <summary>
        /// Gets the HunCompanySiteCategory
        /// </summary>
        /// <value>The HunCompanySiteCategory.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String HunCompanySiteCategory
		{
			get { return _entity.HunCompanySiteCategory; }
		}
        /// <summary>
        /// Gets the Wdl
        /// </summary>
        /// <value>The Wdl.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Wdl
		{
			get { return _entity.Wdl; }
		}
        /// <summary>
        /// Gets the WdlCompanySiteCategory
        /// </summary>
        /// <value>The WdlCompanySiteCategory.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String WdlCompanySiteCategory
		{
			get { return _entity.WdlCompanySiteCategory; }
		}
        /// <summary>
        /// Gets the Bun
        /// </summary>
        /// <value>The Bun.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Bun
		{
			get { return _entity.Bun; }
		}
        /// <summary>
        /// Gets the BunCompanySiteCategory
        /// </summary>
        /// <value>The BunCompanySiteCategory.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String BunCompanySiteCategory
		{
			get { return _entity.BunCompanySiteCategory; }
		}
        /// <summary>
        /// Gets the Fml
        /// </summary>
        /// <value>The Fml.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Fml
		{
			get { return _entity.Fml; }
		}
        /// <summary>
        /// Gets the FmlCompanySiteCategory
        /// </summary>
        /// <value>The FmlCompanySiteCategory.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String FmlCompanySiteCategory
		{
			get { return _entity.FmlCompanySiteCategory; }
		}
        /// <summary>
        /// Gets the Bgn
        /// </summary>
        /// <value>The Bgn.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Bgn
		{
			get { return _entity.Bgn; }
		}
        /// <summary>
        /// Gets the BgnCompanySiteCategory
        /// </summary>
        /// <value>The BgnCompanySiteCategory.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String BgnCompanySiteCategory
		{
			get { return _entity.BgnCompanySiteCategory; }
		}
        /// <summary>
        /// Gets the Ce
        /// </summary>
        /// <value>The Ce.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Ce
		{
			get { return _entity.Ce; }
		}
        /// <summary>
        /// Gets the CeCompanySiteCategory
        /// </summary>
        /// <value>The CeCompanySiteCategory.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String CeCompanySiteCategory
		{
			get { return _entity.CeCompanySiteCategory; }
		}
        /// <summary>
        /// Gets the Ang
        /// </summary>
        /// <value>The Ang.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Ang
		{
			get { return _entity.Ang; }
		}
        /// <summary>
        /// Gets the AngCompanySiteCategory
        /// </summary>
        /// <value>The AngCompanySiteCategory.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String AngCompanySiteCategory
		{
			get { return _entity.AngCompanySiteCategory; }
		}
        /// <summary>
        /// Gets the Ptl
        /// </summary>
        /// <value>The Ptl.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Ptl
		{
			get { return _entity.Ptl; }
		}
        /// <summary>
        /// Gets the PtlCompanySiteCategory
        /// </summary>
        /// <value>The PtlCompanySiteCategory.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String PtlCompanySiteCategory
		{
			get { return _entity.PtlCompanySiteCategory; }
		}
        /// <summary>
        /// Gets the Pth
        /// </summary>
        /// <value>The Pth.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Pth
		{
			get { return _entity.Pth; }
		}
        /// <summary>
        /// Gets the PthCompanySiteCategory
        /// </summary>
        /// <value>The PthCompanySiteCategory.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String PthCompanySiteCategory
		{
			get { return _entity.PthCompanySiteCategory; }
		}
        /// <summary>
        /// Gets the Pel
        /// </summary>
        /// <value>The Pel.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Pel
		{
			get { return _entity.Pel; }
		}
        /// <summary>
        /// Gets the PelCompanySiteCategory
        /// </summary>
        /// <value>The PelCompanySiteCategory.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String PelCompanySiteCategory
		{
			get { return _entity.PelCompanySiteCategory; }
		}
        /// <summary>
        /// Gets the Arp
        /// </summary>
        /// <value>The Arp.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Arp
		{
			get { return _entity.Arp; }
		}
        /// <summary>
        /// Gets the ArpCompanySiteCategory
        /// </summary>
        /// <value>The ArpCompanySiteCategory.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String ArpCompanySiteCategory
		{
			get { return _entity.ArpCompanySiteCategory; }
		}
        /// <summary>
        /// Gets the Yen
        /// </summary>
        /// <value>The Yen.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Yen
		{
			get { return _entity.Yen; }
		}
        /// <summary>
        /// Gets the YenCompanySiteCategory
        /// </summary>
        /// <value>The YenCompanySiteCategory.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String YenCompanySiteCategory
		{
			get { return _entity.YenCompanySiteCategory; }
		}

	}
}
