﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AdminTaskMetrics.ascx.cs"
    Inherits="ALCOA.CSMS.Website.UserControls.Admin.AdminTaskMetrics" %>
<%@ Register Assembly="System.Web.Extensions" Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Assembly="DevExpress.Web.ASPxPivotGrid.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPivotGrid.Export" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.XtraCharts.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.XtraCharts" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.XtraCharts.v14.1.Web, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.XtraCharts.Web" TagPrefix="dxchartsui" %>
<%@ Register Assembly="DevExpress.Web.ASPxPivotGrid.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPivotGrid" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dxtc" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dxw" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallbackPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dxpc" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<table>
    <tbody>
        <tr>
            <td style="width: 120px;">
                <%--<dxe:ASPxButton ID="btn1" runat="server" AutoPostBack="False" Text="Report Manager"
                                                    UseSubmitBehavior="False" Width="120px">
                                                </dxe:ASPxButton>--%>
            </td>
            <td style="width: 120px; padding-left: 10px">
                <%--<strong>Current Report:</strong>--%>
            </td>
            <td style="width: 550px">
                <%--<dxe:ASPxLabel ID="lblLayout" runat="server" Width="100%" Font-Bold="False" Font-Italic="False"
                                                    Text="Default">
                                                </dxe:ASPxLabel>--%>
            </td>
            <td align="right" style="text-align: right; padding-left: 50px" width="100px">
                <a href="javascript:gridPivot.ChangeCustomizationFieldsVisibility();">Customize</a>
            </td>
        </tr>
    </tbody>
</table>
<dx:ASPxCallbackPanel ID="ASPxCallbackPanel1" runat="server" Width="200px">
    <PanelCollection>
        <dx:PanelContent runat="server">
            <br />
            <dx:ASPxPivotGrid ID="gridPivot" ClientInstanceName="gridPivot" runat="server" Width="850px"
                DataSourceID="AdminTaskHistoryDataSource1" EnableCallBacks="False">
                <Fields>
                    <dx:PivotGridField ID="fieldAdminTaskId" AreaIndex="0" FieldName="AdminTaskId" 
                        Caption="Task #" Area="RowArea">
                    </dx:PivotGridField>
                    <dx:PivotGridField ID="fieldAdminTaskSourceDesc" AreaIndex="0" FieldName="AdminTaskSourceDesc"
                        Caption="Source">
                    </dx:PivotGridField>
                    <dx:PivotGridField ID="fieldAdminTaskTypeName" AreaIndex="1" 
                        FieldName="AdminTaskTypeName" Caption="Type" Area="RowArea">
                    </dx:PivotGridField>
                    <dx:PivotGridField ID="fieldAdminTaskTypeDesc" AreaIndex="7" FieldName="AdminTaskTypeDesc"
                        Caption="Type 2" Visible="False">
                    </dx:PivotGridField>
                    <dx:PivotGridField ID="fieldStatusDesc" AreaIndex="2" FieldName="StatusDesc"
                        Caption="Status" Area="RowArea">
                    </dx:PivotGridField>
                    <dx:PivotGridField ID="fieldAdminTaskComments" AreaIndex="4" FieldName="AdminTaskComments"
                        Caption="Task Comments" Visible="False">
                    </dx:PivotGridField>
                    <dx:PivotGridField ID="fieldDateOpened" AreaIndex="1" FieldName="DateOpened"
                        Caption="Date Opened">
                    </dx:PivotGridField>
                    <dx:PivotGridField ID="fieldDateClosed" AreaIndex="2" FieldName="DateClosed"
                        Caption="Date Closed">
                    </dx:PivotGridField>
                    <dx:PivotGridField ID="fieldOpenedByUser" AreaIndex="3" FieldName="OpenedByUser"
                        Caption="Opened By">
                    </dx:PivotGridField>
                    <dx:PivotGridField ID="fieldClosedByUser" AreaIndex="4" FieldName="ClosedByUser"
                        Caption="Closed By">
                    </dx:PivotGridField>
                    <dx:PivotGridField ID="fieldBusinessDaysWith" AreaIndex="0" FieldName="BusinessDaysWith"
                        Caption="Business Days With" Area="DataArea">
                    </dx:PivotGridField>
                    <dx:PivotGridField ID="fieldDaysWith" AreaIndex="9" FieldName="DaysWith"
                        Caption="Days With" Visible="False">
                    </dx:PivotGridField>
                    <dx:PivotGridField ID="fieldHoursWith" AreaIndex="9" FieldName="HoursWith"
                        Caption="Hours With" Visible="False">
                    </dx:PivotGridField>
                    <dx:PivotGridField ID="fieldMinsWith" AreaIndex="9" FieldName="MinsWith"
                        Caption="Mins With" Visible="False">
                    </dx:PivotGridField>
                    <dx:PivotGridField ID="fieldAccessDesc" AreaIndex="9" FieldName="AccessDesc"
                        Caption="Access" Visible="False">
                    </dx:PivotGridField>
                    <dx:PivotGridField ID="fieldCompanyName" AreaIndex="9" FieldName="CompanyName"
                        Caption="Company Name" Visible="False">
                    </dx:PivotGridField>
                    <dx:PivotGridField ID="fieldLogin" AreaIndex="9" Caption="Login"
                        FieldName="Login" Visible="False">
                    </dx:PivotGridField>
                    <dx:PivotGridField ID="fieldEmailAddress" AreaIndex="9" Caption="Email Address"
                        FieldName="EmailAddress" Visible="False">
                    </dx:PivotGridField>
                    <dx:PivotGridField ID="fieldFirstName" AreaIndex="9" Caption="First Name"
                        FieldName="FirstName" Visible="False">
                    </dx:PivotGridField>
                    <dx:PivotGridField ID="fieldLastName" AreaIndex="9" FieldName="LastName"
                        Caption="Last Name" Visible="False">
                    </dx:PivotGridField>
                    <dx:PivotGridField ID="fieldJobRole" AreaIndex="9" FieldName="JobRole"
                        Caption="Job Role" Visible="False">
                    </dx:PivotGridField>
                    <dx:PivotGridField ID="fieldJobTitle" AreaIndex="9" Caption="Job Title" 
                        FieldName="JobTitle" Visible="False">
                    </dx:PivotGridField>
                    <dx:PivotGridField ID="fieldMobileNo" AreaIndex="9" Caption="Mobile No" 
                        FieldName="MobileNo" Visible="False">
                    </dx:PivotGridField>
                    <dx:PivotGridField ID="fieldTelephoneNo" AreaIndex="18" Caption="Telephone No" 
                        FieldName="TelephoneNo" Visible="False">
                    </dx:PivotGridField>
                    <dx:PivotGridField ID="fieldFaxNo" AreaIndex="19" Caption="Fax No" 
                        FieldName="FaxNo" Visible="False">
                    </dx:PivotGridField>
                </Fields>
                <OptionsCustomization CustomizationFormStyle="Simple" />
                <OptionsChartDataSource  FieldValuesProvideMode="DisplayText" />
                <OptionsPager Position="Bottom" RowsPerPage="5" NumericButtonCount="5">
                    <AllButton Visible="True">
                    </AllButton>
                </OptionsPager>
                <OptionsLoadingPanel>
                    <Image Url="~/App_Themes/Office2003Blue/PivotGrid/Loading.gif">
                    </Image>
                </OptionsLoadingPanel>
                <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                    <CustomizationFieldsBackground Url="~/App_Themes/Office2003Blue/PivotGrid/pgCustomizationFormBackground.gif">
                    </CustomizationFieldsBackground>
                    <LoadingPanel Url="~/App_Themes/Office2003Blue/PivotGrid/Loading.gif">
                    </LoadingPanel>
                </Images>
                <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue">
                    <CustomizationFieldsHeaderStyle>
                        <Paddings PaddingLeft="12px" PaddingRight="6px" />
                    </CustomizationFieldsHeaderStyle>
                </Styles>
                <OptionsView ShowColumnGrandTotals="False" ShowColumnTotals="False" ShowRowGrandTotals="False"
                    ShowRowTotals="False" DataHeadersPopupMinCount="1" EnableFilterControlPopupMenuScrolling="True" />
            </dx:ASPxPivotGrid>
            <table border="0" cellpadding="0" cellspacing="0" style="width: 850px; height: 100%">
                <tr>
                    <td style="width: 880px; text-align: right">
                        <div align="right">
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <div style="padding-top: 6px">
                                        <table cellspacing="0" cellpadding="0" border="0">
                                            <tbody>
                                                <tr>
                                                    <td align="right" style="vertical-align: middle; padding-right: 3px; text-align: -moz-right;
                                                        text-align: right;" valign="middle">
                                                        <strong>Export:</strong>
                                                    </td>
                                                    <td align="right" style="vertical-align: middle; padding-right: 2px; text-align: -moz-right;
                                                        text-align: right;" valign="middle">
                                                        <asp:ImageButton ID="imgBtnExcel" OnClick="imgBtnExcel_Click" runat="server" ToolTip="Export table to Microsoft Excel (XLS)"
                                                            ImageUrl="~/Images/ExportLogos/excel.gif"></asp:ImageButton>
                                                        <asp:ImageButton ID="imgBtnWord" OnClick="imgBtnWord_Click" runat="server" ToolTip="Export table to Microsoft Word Document (RTF)"
                                                            ImageUrl="~/Images/ExportLogos/word.gif"></asp:ImageButton>
                                                        <asp:ImageButton ID="imgBtnPdf" OnClick="imgBtnPdf_Click" runat="server" ToolTip="Export table to Adobe Acrobat (PDF)"
                                                            ImageUrl="~/Images/ExportLogos/pdf.gif"></asp:ImageButton>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:PostBackTrigger ControlID="imgBtnExcel"></asp:PostBackTrigger>
                                    <asp:PostBackTrigger ControlID="imgBtnWord"></asp:PostBackTrigger>
                                    <asp:PostBackTrigger ControlID="imgBtnPdf"></asp:PostBackTrigger>
                                </Triggers>
                            </asp:UpdatePanel>
                            <dx:ASPxPivotGridExporter ID="gridPivotExporter" runat="server" ASPxPivotGridID="gridPivot">
                            </dx:ASPxPivotGridExporter>
                        </div>
                    </td>
                </tr>
            </table>
            <br />
            <dxchartsui:WebChartControl ID="WebChartControl1" runat="server" DataSourceID="gridPivot"
                Height="600px" Width="850px" SeriesDataMember="Series" EnableCallBacks="False">
                <seriestemplate argumentdatamember="Arguments" valuedatamembersserializable="Values">
                                                    <ViewSerializable>
<cc1:SideBySideBarSeriesView HiddenSerializableString="to be serialized">
                                                    </cc1:SideBySideBarSeriesView>
</ViewSerializable>
                                                    <LabelSerializable>
<cc1:SideBySideBarSeriesLabel HiddenSerializableString="to be serialized" LineVisible="True" Antialiasing="True">
                                                        <FillStyle >
                                                            <OptionsSerializable>
<cc1:SolidFillOptions HiddenSerializableString="to be serialized" />
</OptionsSerializable>
                                                        </FillStyle>
                                                    </cc1:SideBySideBarSeriesLabel>
</LabelSerializable>
                                                    <PointOptionsSerializable>
<cc1:PointOptions HiddenSerializableString="to be serialized">
                                                    </cc1:PointOptions>
</PointOptionsSerializable>
                                                    <LegendPointOptionsSerializable>
<cc1:PointOptions HiddenSerializableString="to be serialized">
                                                    </cc1:PointOptions>
</LegendPointOptionsSerializable>
                                                </seriestemplate>
                <fillstyle>
                                                    <OptionsSerializable>
<cc1:SolidFillOptions HiddenSerializableString="to be serialized" />
</OptionsSerializable>
                                                </fillstyle>
                <diagramserializable>
<cc1:XYDiagram Rotated="True">
                                                    <axisx visibleinpanesserializable="-1" reverse="True">
<Range SideMarginsEnabled="True"></Range>
</axisx>
                                                    <axisy visibleinpanesserializable="-1">
<Range SideMarginsEnabled="True"></Range>
</axisy>
                                                </cc1:XYDiagram>
</diagramserializable>
                <legend visible="False" MaxHorizontalPercentage="30"></legend>
            </dxchartsui:WebChartControl>
            <%--<dxpc:ASPxPopupControl ID="ASPxPopupControl1" runat="server" HeaderText="Report Manager"
                                                Height="213px" Width="623px" CssPostfix="Office2003Blue" EnableHotTrack="False"
                                                Modal="True" PopupElementID="btn1" AllowDragging="True" AllowResize="True" PopupHorizontalAlign="WindowCenter"
                                                PopupVerticalAlign="WindowCenter" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                                <SizeGripImage Height="16px" Width="16px" />
                                                <HeaderStyle>
                                                    <Paddings PaddingRight="6px" />
                                                </HeaderStyle>
                                                <CloseButtonImage Height="12px" Width="13px" />
                                                <ContentCollection>
                                                    <dxpc:PopupControlContentControl ID="PopupControlContentControl1" runat="server"
                                                        SupportsDisabledAttribute="True">
                                                        <dxwgv:ASPxGridView runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                            CssPostfix="Office2003Blue" KeyFieldName="ReportId" AutoGenerateColumns="False"
                                                            DataSourceID="sqldsLayouts" Width="100%" ID="ASPxGridView1" OnHtmlRowCreated="grid2_RowCreated"
                                                            OnRowUpdating="grid2_RowUpdating" OnRowInserting="grid2_RowInserting" OnInitNewRow="grid2_InitNewRow"
                                                            OnRowValidating="grid2_RowValidating" OnStartRowEditing="grid2_StartRowEditing">
                                                            <Columns>
                                                                <dxwgv:GridViewCommandColumn ShowInCustomizationForm="False" Name="commandCol" Width="100px"
                                                                    Caption="Action" VisibleIndex="0">
                                                                    <EditButton Visible="True" Text="Edit">
                                                                        <Image AlternateText="Edit" Url="~/Images/gridEdit.gif">
                                                                        </Image>
                                                                    </EditButton>
                                                                    <NewButton Visible="True" Text="New">
                                                                        <Image AlternateText="New" Url="~/Images/gridNew.gif">
                                                                        </Image>
                                                                    </NewButton>
                                                                    <DeleteButton Visible="True" Text="Delete">
                                                                        <Image AlternateText="Delete" Url="~/Images/gridDelete.gif">
                                                                        </Image>
                                                                    </DeleteButton>
                                                                    <CancelButton Visible="True" Text="Cancel">
                                                                        <Image AlternateText="Cancel" Url="../Images/gridCancel.gif">
                                                                        </Image>
                                                                    </CancelButton>
                                                                    <UpdateButton Visible="True" Text="Save">
                                                                        <Image AlternateText="Save" Url="../Images/gridSave.gif">
                                                                        </Image>
                                                                    </UpdateButton>
                                                                </dxwgv:GridViewCommandColumn>
                                                                <dxwgv:GridViewDataTextColumn FieldName="ReportId" ReadOnly="True" ShowInCustomizationForm="True"
                                                                    Visible="False" VisibleIndex="1">
                                                                    <EditFormSettings Visible="True"></EditFormSettings>
                                                                </dxwgv:GridViewDataTextColumn>
                                                                <dxwgv:GridViewDataTextColumn ShowInCustomizationForm="True" Width="30px" Caption=" "
                                                                    ToolTip="Load Selected Custom Report" VisibleIndex="1">
                                                                    <EditFormSettings Visible="False"></EditFormSettings>
                                                                    <DataItemTemplate>
                                                                        <table cellpadding="0" cellspacing="0">
                                                                            <tr>
                                                                                <td style="width: 30px">
                                                                                    <dxe:ASPxHyperLink ID="hlLoad" runat="server" Text="Load">
                                                                                    </dxe:ASPxHyperLink>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </DataItemTemplate>
                                                                    <CellStyle HorizontalAlign="Left">
                                                                    </CellStyle>
                                                                </dxwgv:GridViewDataTextColumn>
                                                                <dxwgv:GridViewDataTextColumn FieldName="Area" ShowInCustomizationForm="True" Visible="False"
                                                                    VisibleIndex="2">
                                                                    <EditFormSettings Visible="True"></EditFormSettings>
                                                                </dxwgv:GridViewDataTextColumn>
                                                                <dxwgv:GridViewDataTextColumn FieldName="ReportName" ShowInCustomizationForm="True"
                                                                    VisibleIndex="2">
                                                                </dxwgv:GridViewDataTextColumn>
                                                                <dxwgv:GridViewDataComboBoxColumn FieldName="ReportDescription" ReadOnly="True" ShowInCustomizationForm="True"
                                                                    Name="gcbReportDescription" Caption="Company" Visible="False" VisibleIndex="4">
                                                                    <PropertiesComboBox DataSourceID="sqldsCompaniesList" TextField="CompanyName" ValueField="CompanyId"
                                                                        ValueType="System.String" DropDownHeight="150px">
                                                                    </PropertiesComboBox>
                                                                    <EditFormSettings Visible="False"></EditFormSettings>
                                                                </dxwgv:GridViewDataComboBoxColumn>
                                                                <dxwgv:GridViewDataTextColumn FieldName="ReportLayout" ShowInCustomizationForm="True"
                                                                    Visible="False" VisibleIndex="5">
                                                                </dxwgv:GridViewDataTextColumn>
                                                                <dxwgv:GridViewDataTextColumn FieldName="ModifiedByUserId" ReadOnly="True" ShowInCustomizationForm="True"
                                                                    Visible="False" VisibleIndex="6">
                                                                    <EditFormSettings Visible="True"></EditFormSettings>
                                                                </dxwgv:GridViewDataTextColumn>
                                                                <dxwgv:GridViewDataDateColumn FieldName="ModifiedDate" SortIndex="0" SortOrder="Descending"
                                                                    ReadOnly="True" ShowInCustomizationForm="True" VisibleIndex="3">
                                                                    <Settings SortMode="Value"></Settings>
                                                                    <EditFormSettings Visible="True"></EditFormSettings>
                                                                </dxwgv:GridViewDataDateColumn>
                                                            </Columns>
                                                            <SettingsBehavior ConfirmDelete="True"></SettingsBehavior>
                                                            <SettingsEditing Mode="Inline"></SettingsEditing>
                                                            <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                                                <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
                                                                </LoadingPanelOnStatusBar>
                                                                <LoadingPanel Url="~/App_Themes/Office2003Blue/GridView/Loading.gif">
                                                                </LoadingPanel>
                                                            </Images>
                                                            <ImagesFilterControl>
                                                                <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
                                                                </LoadingPanel>
                                                            </ImagesFilterControl>
                                                            <Styles CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css">
                                                                <LoadingPanel ImageSpacing="10px">
                                                                </LoadingPanel>
                                                                <Header ImageSpacing="5px" SortingImageSpacing="5px">
                                                                </Header>
                                                            </Styles>
                                                        </dxwgv:ASPxGridView>
                                                        <asp:SqlDataSource runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
                                                            DeleteCommand="DELETE FROM [CustomReportingLayouts] WHERE [ReportId] = @ReportId"
                                                            InsertCommand="INSERT INTO [CustomReportingLayouts] ([Area], [ReportName], [ReportDescription], [ReportLayout], [ModifiedByUserId], [ModifiedDate]) VALUES (@Area, @ReportName, @ReportDescription, @ReportLayout, @ModifiedByUserId, @ModifiedDate)"
                                                            SelectCommand="SELECT * FROM [CustomReportingLayouts] WHERE ([ModifiedByUserId] = @ModifiedByUserId AND Area = 'WLT_SQQ_DSA')"
                                                            UpdateCommand="UPDATE [CustomReportingLayouts] SET [Area] = @Area, [ReportName] = @ReportName, [ReportDescription] = @ReportDescription, [ReportLayout] = @ReportLayout, [ModifiedByUserId] = @ModifiedByUserId, [ModifiedDate] = @ModifiedDate WHERE [ReportId] = @ReportId"
                                                            ID="sqldsLayouts">
                                                            <DeleteParameters>
                                                                <asp:Parameter Name="ReportId" Type="Int32"></asp:Parameter>
                                                            </DeleteParameters>
                                                            <InsertParameters>
                                                                <asp:Parameter Name="Area" Type="String"></asp:Parameter>
                                                                <asp:Parameter Name="ReportName" Type="String"></asp:Parameter>
                                                                <asp:Parameter Name="ReportDescription" Type="String"></asp:Parameter>
                                                                <asp:Parameter Name="ReportLayout" Type="String"></asp:Parameter>
                                                                <asp:Parameter Name="ModifiedByUserId" Type="Int32"></asp:Parameter>
                                                                <asp:Parameter Name="ModifiedDate" Type="DateTime"></asp:Parameter>
                                                            </InsertParameters>
                                                            <SelectParameters>
                                                                <asp:SessionParameter SessionField="UserId" DefaultValue="0" Name="ModifiedByUserId"
                                                                    Type="Int32"></asp:SessionParameter>
                                                            </SelectParameters>
                                                            <UpdateParameters>
                                                                <asp:Parameter Name="Area" Type="String"></asp:Parameter>
                                                                <asp:Parameter Name="ReportName" Type="String"></asp:Parameter>
                                                                <asp:Parameter Name="ReportDescription" Type="String"></asp:Parameter>
                                                                <asp:Parameter Name="ReportLayout" Type="String"></asp:Parameter>
                                                                <asp:Parameter Name="ModifiedByUserId" Type="Int32"></asp:Parameter>
                                                                <asp:Parameter Name="ModifiedDate" Type="DateTime"></asp:Parameter>
                                                                <asp:Parameter Name="ReportId" Type="Int32"></asp:Parameter>
                                                            </UpdateParameters>
                                                        </asp:SqlDataSource>
                                                    </dxpc:PopupControlContentControl>
                                                </ContentCollection>
                                            </dxpc:ASPxPopupControl>--%>
        </dx:PanelContent>
    </PanelCollection>
</dx:ASPxCallbackPanel>

<data:AdminTaskHistoryDataSource ID="AdminTaskHistoryDataSource1"
    runat="server">
</data:AdminTaskHistoryDataSource>