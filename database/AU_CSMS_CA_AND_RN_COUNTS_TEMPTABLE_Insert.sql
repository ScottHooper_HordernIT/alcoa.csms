USE [AuA_CSMS_Dev]
GO

/****** Object:  StoredProcedure [dbo].[AU_CSMS_CA_AND_RN_COUNTS_TEMPTABLE_Insert]    Script Date: 15/07/2015 6:32:45 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

  
      
CREATE PROCEDURE [dbo].[AU_CSMS_CA_AND_RN_COUNTS_TEMPTABLE_Insert]      
(      
 @COUNT_TYPE varchar(max),       
 @SUBBUSINESS_UNIT_ENTITY varchar(max),      
 @SITE_ENTITY varchar(max),      
 @CONTRACTING_COMPANY varchar(max),      
 @MONTH_BEGINNING datetime,      
 @WEEK_BEGINNING datetime,      
 @TOTAL_COUNT int,      
 @MODIFIED_DATE datetime      
)      
AS      
    IF EXISTS(SELECT * FROM AU_CSMS_CA_AND_RN_COUNTS_TEMPTABLE   
  WHERE [COUNT_TYPE]  = @COUNT_TYPE  AND   
     [SUBBUSINESS_UNIT_ENTITY] =  @SUBBUSINESS_UNIT_ENTITY AND   
     [SITE_ENTITY]  =   @SITE_ENTITY AND  
     [CONTRACTING_COMPANY] = @CONTRACTING_COMPANY AND      
     [MONTH_BEGINNING] = @MONTH_BEGINNING   AND   
     [WEEK_BEGINNING] = @WEEK_BEGINNING)  
    UPDATE  AU_CSMS_CA_AND_RN_COUNTS_TEMPTABLE SET  
  [TOTAL_COUNT]   = @TOTAL_COUNT   
  ,[MODIFIED_DATE] = @MODIFIED_DATE  
 WHERE  [COUNT_TYPE]  = @COUNT_TYPE  AND   
     [SUBBUSINESS_UNIT_ENTITY] =  @SUBBUSINESS_UNIT_ENTITY AND   
     [SITE_ENTITY]  =   @SITE_ENTITY AND  
     [CONTRACTING_COMPANY] = @CONTRACTING_COMPANY AND      
     [MONTH_BEGINNING] = @MONTH_BEGINNING   AND   
     [WEEK_BEGINNING] = @WEEK_BEGINNING  
     ELSE     
    INSERT INTO [dbo].[AU_CSMS_CA_AND_RN_COUNTS_TEMPTABLE]      
     (      
     [COUNT_TYPE]      
     ,[SUBBUSINESS_UNIT_ENTITY]      
     ,[SITE_ENTITY]      
     ,[CONTRACTING_COMPANY]      
     ,[MONTH_BEGINNING]      
     ,[WEEK_BEGINNING]      
     ,[TOTAL_COUNT]      
     ,[MODIFIED_DATE]      
     )      
    VALUES      
     (      
      @COUNT_TYPE      
     ,@SUBBUSINESS_UNIT_ENTITY      
     ,@SITE_ENTITY      
     ,@CONTRACTING_COMPANY      
     ,@MONTH_BEGINNING      
     ,@WEEK_BEGINNING      
     ,@TOTAL_COUNT      
     ,@MODIFIED_DATE      
     )      
          
    -- Get the identity value      
    SET @TOTAL_COUNT = SCOPE_IDENTITY()      
---------------------------------------

GO

