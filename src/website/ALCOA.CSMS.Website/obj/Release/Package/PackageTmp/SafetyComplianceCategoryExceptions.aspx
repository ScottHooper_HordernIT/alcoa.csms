﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SafetyComplianceCategoryExceptions.aspx.cs" Inherits="ALCOA.CSMS.Website.SafetyComplianceCategoryExceptions" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
    
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxRoundPanel" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxPanel" tagprefix="dx" %>
    <%@ Register assembly="DevExpress.Web.v14.1" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dxpc" %>


<%@ Register assembly="DevExpress.Web.v14.1" namespace="DevExpress.Web.ASPxPopupControl" tagprefix="dx" %>
<%@ Register Assembly="AjaxControls" Namespace="AjaxControls" TagPrefix="cc1" %>
    <%@ Register Src="~/UserControls/Other/LoadingScreen.ascx" TagName="LoadingScreen"
    TagPrefix="uc3" %>


<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script type="text/javascript" language="javascript">
        function doClose(e) // note: takes the event as an arg (IE doesn't)  
        {
            if (!e) e = window.event; // fix IE  

            if (e.keyCode) // IE  
            {
                if (e.keyCode == "27") window.close();
            }
            else if (e.charCode) // Netscape/Firefox/Opera  
            {
                if (e.keyCode == "27") window.close();
            }
        }
        document.onkeydown = doClose;  
 </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="scriptmanager1" runat="Server" EnablePageMethods="true" EnablePartialRendering="true" AsyncPostBackTimeout="600" LoadScriptsBeforeUI="true">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">

    <ContentTemplate>
    <div>
    
    <table border="0"  cellspacing="0" 
            style="width: 100%;" >
 
     <tr>
     
     <td style="text-align:center;width: 100%; " align="right"  >
   
       <dx:ASPxButton ID="ASPxButton1" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
            CssPostfix="Office2003Blue" PostBackUrl="javascript:window.close();" 
            Text="Close (ESC)" 
             SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" 
             Paddings-PaddingLeft="4px">
        </dx:ASPxButton>
        
     <%--<asp:LinkButton ID="btnClose" ForeColor="Black" runat="server" OnClientClick="window.close();" Text="[Exit]" />--%>
     </td>
     
        
     <%--<asp:LinkButton ID="btnPrint" ForeColor="Black" runat="server" OnClientClick="window.print();" Text="[Print]" />--%>
     </tr>
    <tr>
        <td></td>
    </tr>
         <tr>
         <td style="text-align:center;width: 100%; font-size:12pt; font-weight:bold" align="right";   >
            <strong>Residential Category Exceptions List</strong>
         </td>

     </tr>
    
     </table>
          
<br />
        <br />
        <dxwgv:ASPxGridView
            ID="GridFromDS"
            runat="server" AutoGenerateColumns="False"
            CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
            CssPostfix="Office2003Blue"
            KeyFieldName="CompanySiteCategoryExceptionId"
           
            Width="100%" EnableRowsCache="False"
            >
            <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
                </LoadingPanelOnStatusBar>
                <CollapsedButton Height="12px" Width="11px" />
                <ExpandedButton Height="12px" Width="11px" />
                <DetailCollapsedButton Height="12px" Width="11px" />
                <DetailExpandedButton Height="12px" Width="11px" />
                <LoadingPanel Url="~/App_Themes/Office2003BlueOld/GridView/Loading.gif"> 
                </LoadingPanel>
            </Images>
            <ImagesFilterControl>
                <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
                </LoadingPanel>
            </ImagesFilterControl>
            <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
                CssPostfix="Office2003Blue">
                <Header ImageSpacing="5px" SortingImageSpacing="5px" />
                <LoadingPanel ImageSpacing="10px" />
            </Styles>
            <SettingsText EmptyDataRow="No Residential Category Exceptions were found." />
           <Columns>
               <dxwgv:GridViewCommandColumn VisibleIndex="0" ShowClearFilterButton="True" ShowDeleteButton="True" ShowEditButton="True" ShowNewButtonInHeader="True">
            <EditButton Visible="True">
            </EditButton>
            <NewButton Visible="True">
            </NewButton>
            <DeleteButton Visible="True">
            </DeleteButton>
            <ClearFilterButton Visible="True">
            </ClearFilterButton>
                </dxwgv:GridViewCommandColumn>
               
               <dxwgv:GridViewDataComboBoxColumn Caption="Company" FieldName="CompanyId" VisibleIndex="3"
                   SortIndex="1" SortOrder="Ascending">
                   <propertiescombobox textfield="CompanyName" valuefield="CompanyId"
                       valuetype="System.Int32" incrementalfilteringmode="StartsWith"></propertiescombobox>
                   <Settings SortMode="DisplayText" AllowHeaderFilter="False" />
               </dxwgv:GridViewDataComboBoxColumn>
               <dxwgv:GridViewDataComboBoxColumn Caption="Site" FieldName="SiteId" VisibleIndex="4"
                   SortIndex="1" SortOrder="Ascending">
                   <PropertiesComboBox TextField="SiteName" ValueField="SiteId"
                       ValueType="System.Int32" IncrementalFilteringMode="StartsWith"></PropertiesComboBox>
                   <Settings SortMode="DisplayText" AllowHeaderFilter="False" />
               </dxwgv:GridViewDataComboBoxColumn>

               <dxwgv:GridViewDataComboBoxColumn Caption="Residential Category" FieldName="CompanySiteCategoryId"
                   VisibleIndex="5" SortOrder="Ascending">
                   <PropertiesComboBox TextField="CategoryDesc"
                       ValueField="CompanySiteCategoryId" ValueType="System.Int32" IncrementalFilteringMode="StartsWith"></PropertiesComboBox>
                   <Settings SortMode="DisplayText" />
               </dxwgv:GridViewDataComboBoxColumn>



               <dxwgv:GridViewDataDateColumn Caption="From Date" FieldName="MonthYearFrom"  
                VisibleIndex="7">
                    <PropertiesDateEdit DisplayFormatString="MMM yyyy" EditFormatString="MMM yyyy"></PropertiesDateEdit>
                    
                 <settings allowautofilter="false" AllowHeaderFilter="false" SortMode="Value" />

                </dxwgv:GridViewDataDateColumn>
                
                 
                <dxwgv:GridViewDataDateColumn Caption="To Date" FieldName="MonthYearTo"  VisibleIndex="8" >
                    <PropertiesDateEdit DisplayFormatString="MMM yyyy" EditFormatString="MMM yyyy"></PropertiesDateEdit>
                    <settings allowautofilter="false" AllowHeaderFilter="false" SortMode="Value" />
                
                 
                </dxwgv:GridViewDataDateColumn>
                
              
                
            </Columns>

            <Settings ShowHeaderFilterButton="true" showfilterrow="True" />
            <SettingsPager Visible="False" Mode="ShowAllRecords" />
            <SettingsEditing Mode="Inline" />
             
            <SettingsBehavior ColumnResizeMode="NextColumn" ConfirmDelete="True"></SettingsBehavior>
        </dxwgv:ASPxGridView>


    </div>
     </ContentTemplate>
    <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="scriptmanager1" EventName="Load" />
                        </Triggers>

    </asp:UpdatePanel>

    <dxpc:ASPxPopupControl ID="ASPxPopupControl1" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
    CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
    EnableHotTrack="False" HeaderText="Warning" Height="111px" Modal="True" PopupHorizontalAlign="WindowCenter"
    PopupVerticalAlign="WindowCenter" Width="439px">
    <HeaderStyle>
        <Paddings PaddingRight="6px"></Paddings>
    </HeaderStyle>
    <ContentCollection>
        <dxpc:PopupControlContentControl ID="PopupControlContentControl1" runat="server"
            SupportsDisabledAttribute="True">
            <dxe:ASPxLabel runat="server" ID="ASPxLabel1" Font-Size="14px" Text=""
                Height="30px">
                <Border BorderWidth="10px" BorderColor="White"></Border>
            </dxe:ASPxLabel>
        </dxpc:PopupControlContentControl>
    </ContentCollection>
</dxpc:ASPxPopupControl>
<cc1:ModalUpdateProgress ID="ModalUpdateProgress1" runat="server" BackgroundCssClass="modalProgressGreyBackground"
            DisplayAfter="0">
            <ProgressTemplate>
                <uc3:LoadingScreen ID="LoadingScreen1" runat="server" />
            </ProgressTemplate>
        </cc1:ModalUpdateProgress>

    </form>
</body>
</html>

