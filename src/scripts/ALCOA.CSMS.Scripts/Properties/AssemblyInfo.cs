﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("ALCOA.CSMS.Scripts")]
[assembly: AssemblyDescription("Kai-Zen CSMS (Customised for Alcoa)")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Yujia Li < www.kai-zen.com.au >")]
[assembly: AssemblyProduct("Kai-Zen CSMS (Customised for Alcoa)")]
[assembly: AssemblyCopyright("Copyright © Yujia Li < www.kai-zen.com.au >")]
[assembly: AssemblyTrademark("Yujia Li < www.kai-zen.com.au >")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("fd7df232-dc19-443a-a928-81077f51849f")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
