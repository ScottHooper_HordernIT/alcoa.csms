﻿
using System;

namespace KaiZen.CSMS.Entities
{
	/// <summary>
    /// An enum representation of the 'CompanyNote' table's Access levels. [No description found in the database]
	/// </summary>
	[Serializable]
    public enum NoteAccessList
	{
		/// <summary> 
        /// None
		/// </summary>
        [EnumTextValue(@"None")]
		None = 0, 

		/// <summary> 
		/// Full Control of Site
		/// </summary>
        [EnumTextValue(@"All")]
		Administrator = 1, 

		/// <summary> 
        /// H&S Assessor
		/// </summary>
        [EnumTextValue(@"H&S")]
		Ehs = 2, 

		/// <summary> 
		/// PreQual Contractors
		/// </summary>
        [EnumTextValue(@"Procurement")]
        Procurement = 3

	}
}
