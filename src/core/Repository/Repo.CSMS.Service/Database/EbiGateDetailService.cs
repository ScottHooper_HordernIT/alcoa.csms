﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface IEbiGateDetailService
    {
        EbiGateDetail Get(Expression<Func<EbiGateDetail, bool>> where, List<Expression<Func<EbiGateDetail, object>>> includes);
        List<EbiGateDetail> GetMany(int? top, Expression<Func<EbiGateDetail, bool>> where, Func<IQueryable<EbiGateDetail>, IOrderedQueryable<EbiGateDetail>> orderBy, List<Expression<Func<EbiGateDetail, object>>> includes);
        List<EbiGateDetail> GetManyPaged(int pageNumber, int pageSize, Expression<Func<EbiGateDetail, bool>> where, Func<IQueryable<EbiGateDetail>, IOrderedQueryable<EbiGateDetail>> orderBy, List<Expression<Func<EbiGateDetail, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<EbiGateDetail, bool>> where, List<Expression<Func<EbiGateDetail, object>>> includes);
        void Insert(EbiGateDetail item, bool saveChanges = true);
        void Update(EbiGateDetail item, bool saveChanges = true);
        void Delete(EbiGateDetail item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class EbiGateDetailService : IEbiGateDetailService
    {
        private readonly IRepository<EbiGateDetail> _repository;

        public EbiGateDetailService(IRepository<EbiGateDetail> repository)
        {
            this._repository = repository;
        }

        public EbiGateDetail Get(Expression<Func<EbiGateDetail, bool>> where, List<Expression<Func<EbiGateDetail, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<EbiGateDetail> GetMany(int? top, Expression<Func<EbiGateDetail, bool>> where, Func<IQueryable<EbiGateDetail>, IOrderedQueryable<EbiGateDetail>> orderBy, List<Expression<Func<EbiGateDetail, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<EbiGateDetail> GetManyPaged(int pageNumber, int pageSize, Expression<Func<EbiGateDetail, bool>> where, Func<IQueryable<EbiGateDetail>, IOrderedQueryable<EbiGateDetail>> orderBy, List<Expression<Func<EbiGateDetail, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<EbiGateDetail, bool>> where, List<Expression<Func<EbiGateDetail, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(EbiGateDetail item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("EbiGateDetail");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(EbiGateDetail item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("EbiGateDetail");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(EbiGateDetail item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("EbiGateDetail");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
