﻿
using System;

namespace KaiZen.CSMS.Entities
{
	/// <summary>
	/// An enum representation of the 'QuestionnaireInitialContactEmailType' table. [No description found in the database]
	/// </summary>
	/// <remark>this enumeration contains the items contained in the table QuestionnaireInitialContactEmailType</remark>
	[Serializable, Flags]
	public enum QuestionnaireInitialContactEmailTypeList
	{
		/// <summary> 
		/// Notify Supplier that their questionnaire is about to expire
		/// </summary>
		[EnumTextValue(@"Notify Supplier that their questionnaire is about to expire")]
		ExpiryReminder = 1, 

		/// <summary> 
		/// Verify specified supplier contact details refers to correct person "completing" the supplier questionnaire.
		/// </summary>
		[EnumTextValue(@"Verify specified supplier contact details refers to correct person 'completing' the supplier questionnaire.")]
		CorrectContactCheck = 2

	}
}
