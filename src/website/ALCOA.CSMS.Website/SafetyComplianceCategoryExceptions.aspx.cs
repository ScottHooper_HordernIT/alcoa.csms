﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Text;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxTabControl;
using DevExpress.Web.Data;
using DevExpress.Web.ASPxGridView;
using System.Collections.Generic;
using System.Reflection;
using DevExpress.Web.ASPxPopupControl;
using Repo.CSMS.Service.Database;
using Repo.CSMS.DAL.EntityModels;

namespace ALCOA.CSMS.Website
{
    public partial class SafetyComplianceCategoryExceptions : System.Web.UI.Page
    {
        Auth auth = new Auth();
        Repo.CSMS.Service.Database.ICompanySiteCategoryExceptionService companySiteCategoryExceptionService = ALCOA.CSMS.Website.Global.GetInstance<Repo.CSMS.Service.Database.ICompanySiteCategoryExceptionService>();
        Repo.CSMS.Service.Database.ISiteService siteService = ALCOA.CSMS.Website.Global.GetInstance<Repo.CSMS.Service.Database.ISiteService>();
        Repo.CSMS.Service.Database.ICompanyService companyService = ALCOA.CSMS.Website.Global.GetInstance<Repo.CSMS.Service.Database.ICompanyService>();
        Repo.CSMS.Service.Database.ICompanySiteCategoryService companySiteCategoryService = ALCOA.CSMS.Website.Global.GetInstance<Repo.CSMS.Service.Database.ICompanySiteCategoryService>();
        protected void Page_Init(object sender, EventArgs e)
        {
            Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack);

            GridViewDataComboBoxColumn column = (GridFromDS.Columns["SiteId"] as GridViewDataComboBoxColumn);
            column.PropertiesComboBox.DataSource = siteService.GetMany(null, null, null, null);
            column.PropertiesComboBox.ValueField = "SiteId";
            column.PropertiesComboBox.ValueType = typeof(int);
            column.PropertiesComboBox.TextField = "SiteName";

            GridViewDataComboBoxColumn companyColumn = (GridFromDS.Columns["CompanyId"] as GridViewDataComboBoxColumn);
            companyColumn.PropertiesComboBox.DataSource = companyService.GetMany(null, null, null, null);
            companyColumn.PropertiesComboBox.ValueField = "CompanyId";
            companyColumn.PropertiesComboBox.ValueType = typeof(int);
            companyColumn.PropertiesComboBox.TextField = "CompanyName";

            GridViewDataComboBoxColumn companySiteCategoryColumn = (GridFromDS.Columns["CompanySiteCategoryId"] as GridViewDataComboBoxColumn);
            companySiteCategoryColumn.PropertiesComboBox.DataSource = companySiteCategoryService.GetMany(null, null, null, null);
            companySiteCategoryColumn.PropertiesComboBox.ValueField = "CompanySiteCategoryId";
            companySiteCategoryColumn.PropertiesComboBox.ValueType = typeof(int);
            companySiteCategoryColumn.PropertiesComboBox.TextField = "CategoryDesc";

            GridFromDS.DataSource = companySiteCategoryExceptionService.GetAll();
            GridFromDS.DataBind();
            GridFromDS.RowUpdating += grd_RowUpdating;
            GridFromDS.RowInserting += grd_RowInserting;
            GridFromDS.RowValidating += grid_RowValidating;
            GridFromDS.RowDeleting += grd_RowDeleting;
            //GridFromDS.StartRowEditing += grd_StartRowEditing;
            
        }

        private void LoadData() //Loads the grid data and all the data for the drop down lists.
        {
            GridFromDS.DataSource = companySiteCategoryExceptionService.GetAll();
            GridFromDS.DataBind();
        }

        private void moduleInit(bool postBack)
        {
        }
        protected void grd_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
        {
            int id = (int)e.Keys["CompanySiteCategoryExceptionId"];
            companySiteCategoryExceptionService.Delete(id);
            e.Cancel = true;
            GridFromDS.CancelEdit();
            //GridFromDS.DataSource = companySiteCategoryExceptionService.GetAll();
            //GridFromDS.DataBind();
            LoadData();

        }
        protected void grid_RowValidating(object sender, DevExpress.Web.Data.ASPxDataValidationEventArgs e)
        {
            foreach (GridViewColumn column in GridFromDS.Columns)
            {
                GridViewDataColumn dataColumn = column as GridViewDataColumn;
                if (dataColumn == null) continue;
                if (e.NewValues[dataColumn.FieldName] == null)
                {
                    e.Errors[dataColumn] = "Value can't be null.";
                }
            }
            if (e.Errors.Count > 0) 
                e.RowError = "Please, fill all fields.";
            
            if (e.NewValues["MonthYearFrom"] != null && e.NewValues["MonthYearTo"] != null)
            {
                //Set the dates to the first day of teh month
                DateTime monthYearFrom = (DateTime)e.NewValues["MonthYearFrom"];
                DateTime monthYearFrom1stDay = new DateTime(monthYearFrom.Year, monthYearFrom.Month, 1);
                DateTime monthYearTo = (DateTime)e.NewValues["MonthYearTo"];
                DateTime monthYearTo1stDay = new DateTime(monthYearTo.Year, monthYearTo.Month, 1);
                //See if the date range selected overlaps any other entry.

                int companyId = (int)e.NewValues["CompanyId"];
                int siteId = (int)e.NewValues["SiteId"];
                int? key = -1;
                if (!e.IsNewRow) 
                   key = (int)e.Keys["CompanySiteCategoryExceptionId"];
                
                List<CompanySiteCategoryException> checkStartDate = companySiteCategoryExceptionService.GetMany(null,i => i.CompanyId == companyId && i.SiteId == siteId && monthYearFrom1stDay >= i.MonthYearFrom && monthYearFrom1stDay <= i.MonthYearTo && i.CompanySiteCategoryExceptionId != key,null,null);
                List<CompanySiteCategoryException> checkEndDate = companySiteCategoryExceptionService.GetMany(null, i => i.CompanyId == companyId && i.SiteId == siteId && monthYearTo1stDay >= i.MonthYearFrom && monthYearTo1stDay <= i.MonthYearTo && i.CompanySiteCategoryExceptionId != key, null, null);
                List<CompanySiteCategoryException> checkOverlap = companySiteCategoryExceptionService.GetMany(null, i => i.CompanyId == companyId && i.SiteId == siteId && monthYearFrom1stDay <= i.MonthYearFrom && monthYearTo1stDay >= i.MonthYearTo && i.CompanySiteCategoryExceptionId != key, null, null);
                if (checkStartDate.Count > 0 || checkEndDate.Count > 0 || checkOverlap.Count > 0)
                {
                    e.RowError = "The dates for this exception overlap with another entry.  Check all exceptions for this company and site.";
                }
                //Check the start date is before teh end date.
                if (monthYearFrom1stDay > monthYearTo1stDay)
                    e.RowError = "From date cannot be after To date";
            }

        }
        
        protected void grd_RowInserting(object sender, ASPxDataInsertingEventArgs e)
        {
            GridFromDS.DoRowValidation();
            CompanySiteCategoryException csce = new CompanySiteCategoryException();
            csce.CompanyId = (int)e.NewValues["CompanyId"];
            csce.CompanySiteCategoryId = (int)e.NewValues["CompanySiteCategoryId"];
            csce.SiteId = (int)e.NewValues["SiteId"];
            //Get the date from the grid and set the day to the first day of the month.
            csce.MonthYearFrom = (DateTime)e.NewValues["MonthYearFrom"];
            csce.MonthYearFrom = new DateTime(csce.MonthYearFrom.Year, csce.MonthYearFrom.Month, 1);
            csce.MonthYearTo = (DateTime)e.NewValues["MonthYearTo"];
            csce.MonthYearTo = new DateTime(csce.MonthYearTo.Year, csce.MonthYearTo.Month, 1);
            companySiteCategoryExceptionService.Insert(csce);
            e.Cancel = true;
            GridFromDS.CancelEdit();
            LoadData();
        }
        protected void grd_RowUpdating(object sender, ASPxDataUpdatingEventArgs e)
        {
            GridFromDS.DoRowValidation();
            int id = (int)e.Keys["CompanySiteCategoryExceptionId"];
            CompanySiteCategoryException csce = companySiteCategoryExceptionService.Get(i => i.CompanySiteCategoryExceptionId == id,null);
            csce.CompanyId = (int)e.NewValues["CompanyId"];
            csce.CompanySiteCategoryId = (int)e.NewValues["CompanySiteCategoryId"];
            csce.SiteId = (int)e.NewValues["SiteId"];
            //Get the date from the grid and set the day to the first day of the month.
            csce.MonthYearFrom = (DateTime)e.NewValues["MonthYearFrom"];
            csce.MonthYearFrom = new DateTime(csce.MonthYearFrom.Year, csce.MonthYearFrom.Month, 1);
            csce.MonthYearTo = (DateTime)e.NewValues["MonthYearTo"];
            csce.MonthYearTo = new DateTime(csce.MonthYearTo.Year, csce.MonthYearTo.Month, 1);
            companySiteCategoryExceptionService.Update(csce);
            e.Cancel = true;
            GridFromDS.CancelEdit();
            LoadData();
        }
        protected void grd_StartRowEditing(object sender,DevExpress.Web.Data.ASPxStartRowEditingEventArgs e)
        {
            // Validates the edited row if it isn't a new row,.
            if (!GridFromDS.IsNewRowEditing)
                GridFromDS.DoRowValidation();
        }
    }
}