
/****** Object:  StoredProcedure [dbo].[Training_Management_Validity_Insert]    Script Date: 9/07/2015 2:50:01 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

      
CREATE PROCEDURE [dbo].[Training_Management_Validity_Insert]      
(      
      
 @PERSON_ID int,      
 @FULL_NAME varchar(MAX),      
 @EMPL_ID varchar(MAX),      
 @COURSE_NAME nvarchar(80),      
 @COURSE_END_DATE datetime,      
 @EXPIRATION_DATE datetime,      
 @VENDOR_NO varchar(MAX),      
 @CompanyId int,      
 @SiteId int,    
 @CompanyName varchar(max),    
 @SiteName varchar(max),  
@TRAINING_TITLE NVARCHAR(80),  
@LAST_UPDATE_DATE DATETIME     
         
)      
AS      
   IF EXISTS(SELECT * FROM Training_Management_Validity WHERE PERSON_ID=@PERSON_ID AND COURSE_NAME=@COURSE_NAME AND COURSE_END_DATE=@COURSE_END_DATE AND TRAINING_TITLE=@TRAINING_TITLE)   
      
UPDATE Training_Management_Validity SET   
 PERSON_ID = @PERSON_ID  
,FULL_NAME = @FULL_NAME  
,EMPL_ID = @EMPL_ID  
,COURSE_NAME = @COURSE_NAME  
,COURSE_END_DATE = @COURSE_END_DATE  
,EXPIRATION_DATE = @EXPIRATION_DATE  
, VENDOR_NO = @VENDOR_NO  
, CompanyId = @CompanyId  
,SiteId = @SiteId  
,Companyname = @CompanyName  
, SiteName =  @SiteName  
, TRAINING_TITLE = @TRAINING_TITLE  
, LAST_UPDATE_DATE = @LAST_UPDATE_DATE WHERE   
 PERSON_ID=@PERSON_ID AND COURSE_NAME=@COURSE_NAME AND COURSE_END_DATE=@COURSE_END_DATE AND TRAINING_TITLE=@TRAINING_TITLE   
  
ELSE  
  
INSERT INTO [dbo].[Training_Management_Validity]       
      
(PERSON_ID,FULL_NAME,EMPL_ID,COURSE_NAME,      
         COURSE_END_DATE,EXPIRATION_DATE,                    
         VENDOR_NO,                    
         CompanyId,SiteId,Companyname, SiteName,LAST_UPDATE_DATE,TRAINING_TITLE)       
 VALUES (@PERSON_ID,@FULL_NAME,@EMPL_ID,@COURSE_NAME,      
         @COURSE_END_DATE,@EXPIRATION_DATE,                    
         @VENDOR_NO,                    
         @CompanyId,@SiteId,@Companyname, @SiteName,@LAST_UPDATE_DATE,@TRAINING_TITLE)      
----------------------------------------

GO


