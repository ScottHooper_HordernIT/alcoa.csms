﻿#region Using directives

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="SqExemptionFriendlyAllProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract class SqExemptionFriendlyAllProviderBaseCore : EntityViewProviderBase<SqExemptionFriendlyAll>
	{
		#region Custom Methods
		
		
		#region _SqExemption_Friendly_All_GetByQuestionnaireId
		
		/// <summary>
		///	This method wrap the '_SqExemption_Friendly_All_GetByQuestionnaireId' stored procedure. 
		/// </summary>
		/// <param name="questionnaireId"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetByQuestionnaireId(System.Int32? questionnaireId)
		{
			return GetByQuestionnaireId(null, 0, int.MaxValue , questionnaireId);
		}
		
		/// <summary>
		///	This method wrap the '_SqExemption_Friendly_All_GetByQuestionnaireId' stored procedure. 
		/// </summary>
		/// <param name="questionnaireId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetByQuestionnaireId(int start, int pageLength, System.Int32? questionnaireId)
		{
			return GetByQuestionnaireId(null, start, pageLength , questionnaireId);
		}
				
		/// <summary>
		///	This method wrap the '_SqExemption_Friendly_All_GetByQuestionnaireId' stored procedure. 
		/// </summary>
		/// <param name="questionnaireId"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetByQuestionnaireId(TransactionManager transactionManager, System.Int32? questionnaireId)
		{
			return GetByQuestionnaireId(transactionManager, 0, int.MaxValue , questionnaireId);
		}
		
		/// <summary>
		///	This method wrap the '_SqExemption_Friendly_All_GetByQuestionnaireId' stored procedure. 
		/// </summary>
		/// <param name="questionnaireId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet GetByQuestionnaireId(TransactionManager transactionManager, int start, int pageLength, System.Int32? questionnaireId);
		
		#endregion

		
		#endregion

		#region Helper Functions
		
		/*
		///<summary>
		/// Fill an VList&lt;SqExemptionFriendlyAll&gt; From a DataSet
		///</summary>
		/// <param name="dataSet">the DataSet</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList&lt;SqExemptionFriendlyAll&gt;"/></returns>
		protected static VList&lt;SqExemptionFriendlyAll&gt; Fill(DataSet dataSet, VList<SqExemptionFriendlyAll> rows, int start, int pagelen)
		{
			if (dataSet.Tables.Count == 1)
			{
				return Fill(dataSet.Tables[0], rows, start, pagelen);
			}
			else
			{
				return new VList<SqExemptionFriendlyAll>();
			}	
		}
		
		
		///<summary>
		/// Fill an VList&lt;SqExemptionFriendlyAll&gt; From a DataTable
		///</summary>
		/// <param name="dataTable">the DataTable that hold the data.</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList<SqExemptionFriendlyAll>"/></returns>
		protected static VList&lt;SqExemptionFriendlyAll&gt; Fill(DataTable dataTable, VList<SqExemptionFriendlyAll> rows, int start, int pagelen)
		{
			int recordnum = 0;
			
			System.Collections.IEnumerator dataRows =  dataTable.Rows.GetEnumerator();
			
			while (dataRows.MoveNext() && (pagelen != 0))
			{
				if(recordnum >= start)
				{
					DataRow row = (DataRow)dataRows.Current;
				
					SqExemptionFriendlyAll c = new SqExemptionFriendlyAll();
					c.CompanyName = (Convert.IsDBNull(row["CompanyName"]))?string.Empty:(System.String)row["CompanyName"];
					c.RequestingCompanyName = (Convert.IsDBNull(row["RequestingCompanyName"]))?string.Empty:(System.String)row["RequestingCompanyName"];
					c.SiteName = (Convert.IsDBNull(row["SiteName"]))?string.Empty:(System.String)row["SiteName"];
					c.DateApplied = (Convert.IsDBNull(row["DateApplied"]))?DateTime.MinValue:(System.DateTime)row["DateApplied"];
					c.ValidFrom = (Convert.IsDBNull(row["ValidFrom"]))?DateTime.MinValue:(System.DateTime)row["ValidFrom"];
					c.ValidTo = (Convert.IsDBNull(row["ValidTo"]))?DateTime.MinValue:(System.DateTime)row["ValidTo"];
					c.CompanyId = (Convert.IsDBNull(row["CompanyId"]))?(int)0:(System.Int32)row["CompanyId"];
					c.CompanyStatusDesc = (Convert.IsDBNull(row["CompanyStatusDesc"]))?string.Empty:(System.String)row["CompanyStatusDesc"];
					c.SqExemptionId = (Convert.IsDBNull(row["SqExemptionId"]))?(int)0:(System.Int32)row["SqExemptionId"];
					c.AcceptChanges();
					rows.Add(c);
					pagelen -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		*/	
						
		///<summary>
		/// Fill an <see cref="VList&lt;SqExemptionFriendlyAll&gt;"/> From a DataReader.
		///</summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pageLength">number of row.</param>
		///<returns>a <see cref="VList&lt;SqExemptionFriendlyAll&gt;"/></returns>
		protected VList<SqExemptionFriendlyAll> Fill(IDataReader reader, VList<SqExemptionFriendlyAll> rows, int start, int pageLength)
		{
			int recordnum = 0;
			while (reader.Read() && (pageLength != 0))
			{
				if(recordnum >= start)
				{
					SqExemptionFriendlyAll entity = null;
					if (DataRepository.Provider.UseEntityFactory)
					{
						entity = EntityManager.CreateViewEntity<SqExemptionFriendlyAll>("SqExemptionFriendlyAll",  DataRepository.Provider.EntityCreationalFactoryType); 
					}
					else
					{
						entity = new SqExemptionFriendlyAll();
					}
					
					entity.SuppressEntityEvents = true;

					entity.CompanyName = (System.String)reader[((int)SqExemptionFriendlyAllColumn.CompanyName)];
					//entity.CompanyName = (Convert.IsDBNull(reader["CompanyName"]))?string.Empty:(System.String)reader["CompanyName"];
					entity.RequestingCompanyName = (System.String)reader[((int)SqExemptionFriendlyAllColumn.RequestingCompanyName)];
					//entity.RequestingCompanyName = (Convert.IsDBNull(reader["RequestingCompanyName"]))?string.Empty:(System.String)reader["RequestingCompanyName"];
					entity.SiteName = (System.String)reader[((int)SqExemptionFriendlyAllColumn.SiteName)];
					//entity.SiteName = (Convert.IsDBNull(reader["SiteName"]))?string.Empty:(System.String)reader["SiteName"];
					entity.DateApplied = (System.DateTime)reader[((int)SqExemptionFriendlyAllColumn.DateApplied)];
					//entity.DateApplied = (Convert.IsDBNull(reader["DateApplied"]))?DateTime.MinValue:(System.DateTime)reader["DateApplied"];
					entity.ValidFrom = (System.DateTime)reader[((int)SqExemptionFriendlyAllColumn.ValidFrom)];
					//entity.ValidFrom = (Convert.IsDBNull(reader["ValidFrom"]))?DateTime.MinValue:(System.DateTime)reader["ValidFrom"];
					entity.ValidTo = (System.DateTime)reader[((int)SqExemptionFriendlyAllColumn.ValidTo)];
					//entity.ValidTo = (Convert.IsDBNull(reader["ValidTo"]))?DateTime.MinValue:(System.DateTime)reader["ValidTo"];
					entity.CompanyId = (System.Int32)reader[((int)SqExemptionFriendlyAllColumn.CompanyId)];
					//entity.CompanyId = (Convert.IsDBNull(reader["CompanyId"]))?(int)0:(System.Int32)reader["CompanyId"];
					entity.CompanyStatusDesc = (reader.IsDBNull(((int)SqExemptionFriendlyAllColumn.CompanyStatusDesc)))?null:(System.String)reader[((int)SqExemptionFriendlyAllColumn.CompanyStatusDesc)];
					//entity.CompanyStatusDesc = (Convert.IsDBNull(reader["CompanyStatusDesc"]))?string.Empty:(System.String)reader["CompanyStatusDesc"];
					entity.SqExemptionId = (System.Int32)reader[((int)SqExemptionFriendlyAllColumn.SqExemptionId)];
					//entity.SqExemptionId = (Convert.IsDBNull(reader["SqExemptionId"]))?(int)0:(System.Int32)reader["SqExemptionId"];
					entity.AcceptChanges();
					entity.SuppressEntityEvents = false;
					
					rows.Add(entity);
					pageLength -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		
		
		/// <summary>
		/// Refreshes the <see cref="SqExemptionFriendlyAll"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="SqExemptionFriendlyAll"/> object to refresh.</param>
		protected void RefreshEntity(IDataReader reader, SqExemptionFriendlyAll entity)
		{
			reader.Read();
			entity.CompanyName = (System.String)reader[((int)SqExemptionFriendlyAllColumn.CompanyName)];
			//entity.CompanyName = (Convert.IsDBNull(reader["CompanyName"]))?string.Empty:(System.String)reader["CompanyName"];
			entity.RequestingCompanyName = (System.String)reader[((int)SqExemptionFriendlyAllColumn.RequestingCompanyName)];
			//entity.RequestingCompanyName = (Convert.IsDBNull(reader["RequestingCompanyName"]))?string.Empty:(System.String)reader["RequestingCompanyName"];
			entity.SiteName = (System.String)reader[((int)SqExemptionFriendlyAllColumn.SiteName)];
			//entity.SiteName = (Convert.IsDBNull(reader["SiteName"]))?string.Empty:(System.String)reader["SiteName"];
			entity.DateApplied = (System.DateTime)reader[((int)SqExemptionFriendlyAllColumn.DateApplied)];
			//entity.DateApplied = (Convert.IsDBNull(reader["DateApplied"]))?DateTime.MinValue:(System.DateTime)reader["DateApplied"];
			entity.ValidFrom = (System.DateTime)reader[((int)SqExemptionFriendlyAllColumn.ValidFrom)];
			//entity.ValidFrom = (Convert.IsDBNull(reader["ValidFrom"]))?DateTime.MinValue:(System.DateTime)reader["ValidFrom"];
			entity.ValidTo = (System.DateTime)reader[((int)SqExemptionFriendlyAllColumn.ValidTo)];
			//entity.ValidTo = (Convert.IsDBNull(reader["ValidTo"]))?DateTime.MinValue:(System.DateTime)reader["ValidTo"];
			entity.CompanyId = (System.Int32)reader[((int)SqExemptionFriendlyAllColumn.CompanyId)];
			//entity.CompanyId = (Convert.IsDBNull(reader["CompanyId"]))?(int)0:(System.Int32)reader["CompanyId"];
			entity.CompanyStatusDesc = (reader.IsDBNull(((int)SqExemptionFriendlyAllColumn.CompanyStatusDesc)))?null:(System.String)reader[((int)SqExemptionFriendlyAllColumn.CompanyStatusDesc)];
			//entity.CompanyStatusDesc = (Convert.IsDBNull(reader["CompanyStatusDesc"]))?string.Empty:(System.String)reader["CompanyStatusDesc"];
			entity.SqExemptionId = (System.Int32)reader[((int)SqExemptionFriendlyAllColumn.SqExemptionId)];
			//entity.SqExemptionId = (Convert.IsDBNull(reader["SqExemptionId"]))?(int)0:(System.Int32)reader["SqExemptionId"];
			reader.Close();
	
			entity.AcceptChanges();
		}
		
		/*
		/// <summary>
		/// Refreshes the <see cref="SqExemptionFriendlyAll"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="SqExemptionFriendlyAll"/> object.</param>
		protected static void RefreshEntity(DataSet dataSet, SqExemptionFriendlyAll entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.CompanyName = (Convert.IsDBNull(dataRow["CompanyName"]))?string.Empty:(System.String)dataRow["CompanyName"];
			entity.RequestingCompanyName = (Convert.IsDBNull(dataRow["RequestingCompanyName"]))?string.Empty:(System.String)dataRow["RequestingCompanyName"];
			entity.SiteName = (Convert.IsDBNull(dataRow["SiteName"]))?string.Empty:(System.String)dataRow["SiteName"];
			entity.DateApplied = (Convert.IsDBNull(dataRow["DateApplied"]))?DateTime.MinValue:(System.DateTime)dataRow["DateApplied"];
			entity.ValidFrom = (Convert.IsDBNull(dataRow["ValidFrom"]))?DateTime.MinValue:(System.DateTime)dataRow["ValidFrom"];
			entity.ValidTo = (Convert.IsDBNull(dataRow["ValidTo"]))?DateTime.MinValue:(System.DateTime)dataRow["ValidTo"];
			entity.CompanyId = (Convert.IsDBNull(dataRow["CompanyId"]))?(int)0:(System.Int32)dataRow["CompanyId"];
			entity.CompanyStatusDesc = (Convert.IsDBNull(dataRow["CompanyStatusDesc"]))?string.Empty:(System.String)dataRow["CompanyStatusDesc"];
			entity.SqExemptionId = (Convert.IsDBNull(dataRow["SqExemptionId"]))?(int)0:(System.Int32)dataRow["SqExemptionId"];
			entity.AcceptChanges();
		}
		*/
			
		#endregion Helper Functions
	}//end class

	#region SqExemptionFriendlyAllFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SqExemptionFriendlyAll"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SqExemptionFriendlyAllFilterBuilder : SqlFilterBuilder<SqExemptionFriendlyAllColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SqExemptionFriendlyAllFilterBuilder class.
		/// </summary>
		public SqExemptionFriendlyAllFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the SqExemptionFriendlyAllFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SqExemptionFriendlyAllFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SqExemptionFriendlyAllFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SqExemptionFriendlyAllFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SqExemptionFriendlyAllFilterBuilder

	#region SqExemptionFriendlyAllParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SqExemptionFriendlyAll"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SqExemptionFriendlyAllParameterBuilder : ParameterizedSqlFilterBuilder<SqExemptionFriendlyAllColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SqExemptionFriendlyAllParameterBuilder class.
		/// </summary>
		public SqExemptionFriendlyAllParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the SqExemptionFriendlyAllParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SqExemptionFriendlyAllParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SqExemptionFriendlyAllParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SqExemptionFriendlyAllParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SqExemptionFriendlyAllParameterBuilder
	
	#region SqExemptionFriendlyAllSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SqExemptionFriendlyAll"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class SqExemptionFriendlyAllSortBuilder : SqlSortBuilder<SqExemptionFriendlyAllColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SqExemptionFriendlyAllSqlSortBuilder class.
		/// </summary>
		public SqExemptionFriendlyAllSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion SqExemptionFriendlyAllSortBuilder

} // end namespace
