﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="CompaniesEhsimsMapProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class CompaniesEhsimsMapProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.CompaniesEhsimsMap, KaiZen.CSMS.Entities.CompaniesEhsimsMapKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.CompaniesEhsimsMapKey key)
		{
			return Delete(transactionManager, key.EhssimsMapId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_ehssimsMapId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _ehssimsMapId)
		{
			return Delete(null, _ehssimsMapId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_ehssimsMapId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _ehssimsMapId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompaniesEhsimsMap_Companies key.
		///		FK_CompaniesEhsimsMap_Companies Description: 
		/// </summary>
		/// <param name="_companyId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompaniesEhsimsMap objects.</returns>
		public TList<CompaniesEhsimsMap> GetByCompanyId(System.Int32 _companyId)
		{
			int count = -1;
			return GetByCompanyId(_companyId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompaniesEhsimsMap_Companies key.
		///		FK_CompaniesEhsimsMap_Companies Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompaniesEhsimsMap objects.</returns>
		/// <remarks></remarks>
		public TList<CompaniesEhsimsMap> GetByCompanyId(TransactionManager transactionManager, System.Int32 _companyId)
		{
			int count = -1;
			return GetByCompanyId(transactionManager, _companyId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompaniesEhsimsMap_Companies key.
		///		FK_CompaniesEhsimsMap_Companies Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompaniesEhsimsMap objects.</returns>
		public TList<CompaniesEhsimsMap> GetByCompanyId(TransactionManager transactionManager, System.Int32 _companyId, int start, int pageLength)
		{
			int count = -1;
			return GetByCompanyId(transactionManager, _companyId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompaniesEhsimsMap_Companies key.
		///		fkCompaniesEhsimsMapCompanies Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_companyId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompaniesEhsimsMap objects.</returns>
		public TList<CompaniesEhsimsMap> GetByCompanyId(System.Int32 _companyId, int start, int pageLength)
		{
			int count =  -1;
			return GetByCompanyId(null, _companyId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompaniesEhsimsMap_Companies key.
		///		fkCompaniesEhsimsMapCompanies Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_companyId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompaniesEhsimsMap objects.</returns>
		public TList<CompaniesEhsimsMap> GetByCompanyId(System.Int32 _companyId, int start, int pageLength,out int count)
		{
			return GetByCompanyId(null, _companyId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompaniesEhsimsMap_Companies key.
		///		FK_CompaniesEhsimsMap_Companies Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompaniesEhsimsMap objects.</returns>
		public abstract TList<CompaniesEhsimsMap> GetByCompanyId(TransactionManager transactionManager, System.Int32 _companyId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompaniesEhsimsMap_Sites key.
		///		FK_CompaniesEhsimsMap_Sites Description: 
		/// </summary>
		/// <param name="_siteId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompaniesEhsimsMap objects.</returns>
		public TList<CompaniesEhsimsMap> GetBySiteId(System.Int32 _siteId)
		{
			int count = -1;
			return GetBySiteId(_siteId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompaniesEhsimsMap_Sites key.
		///		FK_CompaniesEhsimsMap_Sites Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_siteId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompaniesEhsimsMap objects.</returns>
		/// <remarks></remarks>
		public TList<CompaniesEhsimsMap> GetBySiteId(TransactionManager transactionManager, System.Int32 _siteId)
		{
			int count = -1;
			return GetBySiteId(transactionManager, _siteId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompaniesEhsimsMap_Sites key.
		///		FK_CompaniesEhsimsMap_Sites Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_siteId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompaniesEhsimsMap objects.</returns>
		public TList<CompaniesEhsimsMap> GetBySiteId(TransactionManager transactionManager, System.Int32 _siteId, int start, int pageLength)
		{
			int count = -1;
			return GetBySiteId(transactionManager, _siteId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompaniesEhsimsMap_Sites key.
		///		fkCompaniesEhsimsMapSites Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_siteId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompaniesEhsimsMap objects.</returns>
		public TList<CompaniesEhsimsMap> GetBySiteId(System.Int32 _siteId, int start, int pageLength)
		{
			int count =  -1;
			return GetBySiteId(null, _siteId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompaniesEhsimsMap_Sites key.
		///		fkCompaniesEhsimsMapSites Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_siteId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompaniesEhsimsMap objects.</returns>
		public TList<CompaniesEhsimsMap> GetBySiteId(System.Int32 _siteId, int start, int pageLength,out int count)
		{
			return GetBySiteId(null, _siteId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompaniesEhsimsMap_Sites key.
		///		FK_CompaniesEhsimsMap_Sites Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_siteId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompaniesEhsimsMap objects.</returns>
		public abstract TList<CompaniesEhsimsMap> GetBySiteId(TransactionManager transactionManager, System.Int32 _siteId, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.CompaniesEhsimsMap Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.CompaniesEhsimsMapKey key, int start, int pageLength)
		{
			return GetByEhssimsMapId(transactionManager, key.EhssimsMapId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_CompaniesEhsimsMap index.
		/// </summary>
		/// <param name="_ehssimsMapId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompaniesEhsimsMap"/> class.</returns>
		public KaiZen.CSMS.Entities.CompaniesEhsimsMap GetByEhssimsMapId(System.Int32 _ehssimsMapId)
		{
			int count = -1;
			return GetByEhssimsMapId(null,_ehssimsMapId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CompaniesEhsimsMap index.
		/// </summary>
		/// <param name="_ehssimsMapId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompaniesEhsimsMap"/> class.</returns>
		public KaiZen.CSMS.Entities.CompaniesEhsimsMap GetByEhssimsMapId(System.Int32 _ehssimsMapId, int start, int pageLength)
		{
			int count = -1;
			return GetByEhssimsMapId(null, _ehssimsMapId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CompaniesEhsimsMap index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_ehssimsMapId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompaniesEhsimsMap"/> class.</returns>
		public KaiZen.CSMS.Entities.CompaniesEhsimsMap GetByEhssimsMapId(TransactionManager transactionManager, System.Int32 _ehssimsMapId)
		{
			int count = -1;
			return GetByEhssimsMapId(transactionManager, _ehssimsMapId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CompaniesEhsimsMap index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_ehssimsMapId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompaniesEhsimsMap"/> class.</returns>
		public KaiZen.CSMS.Entities.CompaniesEhsimsMap GetByEhssimsMapId(TransactionManager transactionManager, System.Int32 _ehssimsMapId, int start, int pageLength)
		{
			int count = -1;
			return GetByEhssimsMapId(transactionManager, _ehssimsMapId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CompaniesEhsimsMap index.
		/// </summary>
		/// <param name="_ehssimsMapId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompaniesEhsimsMap"/> class.</returns>
		public KaiZen.CSMS.Entities.CompaniesEhsimsMap GetByEhssimsMapId(System.Int32 _ehssimsMapId, int start, int pageLength, out int count)
		{
			return GetByEhssimsMapId(null, _ehssimsMapId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CompaniesEhsimsMap index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_ehssimsMapId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompaniesEhsimsMap"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.CompaniesEhsimsMap GetByEhssimsMapId(TransactionManager transactionManager, System.Int32 _ehssimsMapId, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_CompaniesEhsimsMap_ContCompanyCodeSiteId index.
		/// </summary>
		/// <param name="_contCompanyCode"></param>
		/// <param name="_siteId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompaniesEhsimsMap"/> class.</returns>
		public KaiZen.CSMS.Entities.CompaniesEhsimsMap GetByContCompanyCodeSiteId(System.String _contCompanyCode, System.Int32 _siteId)
		{
			int count = -1;
			return GetByContCompanyCodeSiteId(null,_contCompanyCode, _siteId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_CompaniesEhsimsMap_ContCompanyCodeSiteId index.
		/// </summary>
		/// <param name="_contCompanyCode"></param>
		/// <param name="_siteId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompaniesEhsimsMap"/> class.</returns>
		public KaiZen.CSMS.Entities.CompaniesEhsimsMap GetByContCompanyCodeSiteId(System.String _contCompanyCode, System.Int32 _siteId, int start, int pageLength)
		{
			int count = -1;
			return GetByContCompanyCodeSiteId(null, _contCompanyCode, _siteId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_CompaniesEhsimsMap_ContCompanyCodeSiteId index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_contCompanyCode"></param>
		/// <param name="_siteId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompaniesEhsimsMap"/> class.</returns>
		public KaiZen.CSMS.Entities.CompaniesEhsimsMap GetByContCompanyCodeSiteId(TransactionManager transactionManager, System.String _contCompanyCode, System.Int32 _siteId)
		{
			int count = -1;
			return GetByContCompanyCodeSiteId(transactionManager, _contCompanyCode, _siteId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_CompaniesEhsimsMap_ContCompanyCodeSiteId index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_contCompanyCode"></param>
		/// <param name="_siteId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompaniesEhsimsMap"/> class.</returns>
		public KaiZen.CSMS.Entities.CompaniesEhsimsMap GetByContCompanyCodeSiteId(TransactionManager transactionManager, System.String _contCompanyCode, System.Int32 _siteId, int start, int pageLength)
		{
			int count = -1;
			return GetByContCompanyCodeSiteId(transactionManager, _contCompanyCode, _siteId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_CompaniesEhsimsMap_ContCompanyCodeSiteId index.
		/// </summary>
		/// <param name="_contCompanyCode"></param>
		/// <param name="_siteId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompaniesEhsimsMap"/> class.</returns>
		public KaiZen.CSMS.Entities.CompaniesEhsimsMap GetByContCompanyCodeSiteId(System.String _contCompanyCode, System.Int32 _siteId, int start, int pageLength, out int count)
		{
			return GetByContCompanyCodeSiteId(null, _contCompanyCode, _siteId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_CompaniesEhsimsMap_ContCompanyCodeSiteId index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_contCompanyCode"></param>
		/// <param name="_siteId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompaniesEhsimsMap"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.CompaniesEhsimsMap GetByContCompanyCodeSiteId(TransactionManager transactionManager, System.String _contCompanyCode, System.Int32 _siteId, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;CompaniesEhsimsMap&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;CompaniesEhsimsMap&gt;"/></returns>
		public static TList<CompaniesEhsimsMap> Fill(IDataReader reader, TList<CompaniesEhsimsMap> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.CompaniesEhsimsMap c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("CompaniesEhsimsMap")
					.Append("|").Append((System.Int32)reader[((int)CompaniesEhsimsMapColumn.EhssimsMapId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<CompaniesEhsimsMap>(
					key.ToString(), // EntityTrackingKey
					"CompaniesEhsimsMap",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.CompaniesEhsimsMap();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.EhssimsMapId = (System.Int32)reader[((int)CompaniesEhsimsMapColumn.EhssimsMapId - 1)];
					c.CompanyId = (System.Int32)reader[((int)CompaniesEhsimsMapColumn.CompanyId - 1)];
					c.ContCompanyCode = (System.String)reader[((int)CompaniesEhsimsMapColumn.ContCompanyCode - 1)];
					c.SiteId = (System.Int32)reader[((int)CompaniesEhsimsMapColumn.SiteId - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.CompaniesEhsimsMap"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.CompaniesEhsimsMap"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.CompaniesEhsimsMap entity)
		{
			if (!reader.Read()) return;
			
			entity.EhssimsMapId = (System.Int32)reader[((int)CompaniesEhsimsMapColumn.EhssimsMapId - 1)];
			entity.CompanyId = (System.Int32)reader[((int)CompaniesEhsimsMapColumn.CompanyId - 1)];
			entity.ContCompanyCode = (System.String)reader[((int)CompaniesEhsimsMapColumn.ContCompanyCode - 1)];
			entity.SiteId = (System.Int32)reader[((int)CompaniesEhsimsMapColumn.SiteId - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.CompaniesEhsimsMap"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.CompaniesEhsimsMap"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.CompaniesEhsimsMap entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.EhssimsMapId = (System.Int32)dataRow["EhssimsMapId"];
			entity.CompanyId = (System.Int32)dataRow["CompanyId"];
			entity.ContCompanyCode = (System.String)dataRow["ContCompanyCode"];
			entity.SiteId = (System.Int32)dataRow["SiteId"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.CompaniesEhsimsMap"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.CompaniesEhsimsMap Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.CompaniesEhsimsMap entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region CompanyIdSource	
			if (CanDeepLoad(entity, "Companies|CompanyIdSource", deepLoadType, innerList) 
				&& entity.CompanyIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.CompanyId;
				Companies tmpEntity = EntityManager.LocateEntity<Companies>(EntityLocator.ConstructKeyFromPkItems(typeof(Companies), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.CompanyIdSource = tmpEntity;
				else
					entity.CompanyIdSource = DataRepository.CompaniesProvider.GetByCompanyId(transactionManager, entity.CompanyId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CompanyIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.CompanyIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.CompaniesProvider.DeepLoad(transactionManager, entity.CompanyIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion CompanyIdSource

			#region SiteIdSource	
			if (CanDeepLoad(entity, "Sites|SiteIdSource", deepLoadType, innerList) 
				&& entity.SiteIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.SiteId;
				Sites tmpEntity = EntityManager.LocateEntity<Sites>(EntityLocator.ConstructKeyFromPkItems(typeof(Sites), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.SiteIdSource = tmpEntity;
				else
					entity.SiteIdSource = DataRepository.SitesProvider.GetBySiteId(transactionManager, entity.SiteId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'SiteIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.SiteIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.SitesProvider.DeepLoad(transactionManager, entity.SiteIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion SiteIdSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.CompaniesEhsimsMap object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.CompaniesEhsimsMap instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.CompaniesEhsimsMap Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.CompaniesEhsimsMap entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region CompanyIdSource
			if (CanDeepSave(entity, "Companies|CompanyIdSource", deepSaveType, innerList) 
				&& entity.CompanyIdSource != null)
			{
				DataRepository.CompaniesProvider.Save(transactionManager, entity.CompanyIdSource);
				entity.CompanyId = entity.CompanyIdSource.CompanyId;
			}
			#endregion 
			
			#region SiteIdSource
			if (CanDeepSave(entity, "Sites|SiteIdSource", deepSaveType, innerList) 
				&& entity.SiteIdSource != null)
			{
				DataRepository.SitesProvider.Save(transactionManager, entity.SiteIdSource);
				entity.SiteId = entity.SiteIdSource.SiteId;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region CompaniesEhsimsMapChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.CompaniesEhsimsMap</c>
	///</summary>
	public enum CompaniesEhsimsMapChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>Companies</c> at CompanyIdSource
		///</summary>
		[ChildEntityType(typeof(Companies))]
		Companies,
			
		///<summary>
		/// Composite Property for <c>Sites</c> at SiteIdSource
		///</summary>
		[ChildEntityType(typeof(Sites))]
		Sites,
		}
	
	#endregion CompaniesEhsimsMapChildEntityTypes
	
	#region CompaniesEhsimsMapFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;CompaniesEhsimsMapColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CompaniesEhsimsMap"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompaniesEhsimsMapFilterBuilder : SqlFilterBuilder<CompaniesEhsimsMapColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompaniesEhsimsMapFilterBuilder class.
		/// </summary>
		public CompaniesEhsimsMapFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the CompaniesEhsimsMapFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CompaniesEhsimsMapFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CompaniesEhsimsMapFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CompaniesEhsimsMapFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CompaniesEhsimsMapFilterBuilder
	
	#region CompaniesEhsimsMapParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;CompaniesEhsimsMapColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CompaniesEhsimsMap"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompaniesEhsimsMapParameterBuilder : ParameterizedSqlFilterBuilder<CompaniesEhsimsMapColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompaniesEhsimsMapParameterBuilder class.
		/// </summary>
		public CompaniesEhsimsMapParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the CompaniesEhsimsMapParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CompaniesEhsimsMapParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CompaniesEhsimsMapParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CompaniesEhsimsMapParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CompaniesEhsimsMapParameterBuilder
	
	#region CompaniesEhsimsMapSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;CompaniesEhsimsMapColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CompaniesEhsimsMap"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class CompaniesEhsimsMapSortBuilder : SqlSortBuilder<CompaniesEhsimsMapColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompaniesEhsimsMapSqlSortBuilder class.
		/// </summary>
		public CompaniesEhsimsMapSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion CompaniesEhsimsMapSortBuilder
	
} // end namespace
