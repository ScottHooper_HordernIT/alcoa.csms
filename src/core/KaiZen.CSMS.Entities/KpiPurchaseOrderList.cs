﻿#region Using directives

using System;

#endregion

namespace KaiZen.CSMS.Entities
{	
	///<summary>
	/// An object representation of the 'KpiPurchaseOrderList' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class KpiPurchaseOrderList : KpiPurchaseOrderListBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="KpiPurchaseOrderList"/> instance.
		///</summary>
		public KpiPurchaseOrderList():base(){}	
		
		#endregion
	}
}
