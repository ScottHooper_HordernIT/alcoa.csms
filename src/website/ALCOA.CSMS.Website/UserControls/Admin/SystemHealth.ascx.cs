﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using DevExpress.Web.ASPxGridView;

public partial class UserControls_Admin_SystemHealth : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    protected void grid_RowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {
        if (e.RowType != GridViewRowType.Data) return;

        if (ASPxGridView1.GetRowValues(e.VisibleIndex, "Success") == DBNull.Value) return;
        bool Success = (bool)ASPxGridView1.GetRowValues(e.VisibleIndex, "Success");
        Image img = ASPxGridView1.FindRowCellTemplateControl(e.VisibleIndex, null, "cImage") as Image;
        if (img != null)
        {
            if (Success)
            {
                img.ImageUrl = "~/Images/TrafficLights/tlGreen.gif";
            }
            else
            {
                img.ImageUrl = "~/Images/TrafficLights/tlRed.gif";
            }
        }

    }
}
