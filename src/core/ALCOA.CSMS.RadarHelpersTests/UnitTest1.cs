﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using rh = ALCOA.CSMS.BatchJobScripts.Code;
using System.Collections.Generic;
//using Repo.CSMS.RadarHelpers;

namespace ALCOA.CSMS.RadarHelpersTests
{
    [TestClass]
    public class RadarHelpersTests
    {
        [TestMethod]
        public void GetNumberOfCompliantCompaniesByRegion()
        {
            rh.RadarHelper rhx = new rh.RadarHelper();
            int result = rhx.GetNumberOfCompliantCompaniesByRegion(4,2, "JSAAudits", "JSAScore");
            Assert.AreNotEqual(0, result);
        }

        
        [TestMethod]
        public void UpdateNonCompliantCompaniesAllSites()
        {
            rh.RadarHelper rhx = new rh.RadarHelper();
            rhx.UpdatePercentageOfCompliantCompaniesAllSites("JSAAudits", "JSA Field Audit Verifications", "JSAScore");
            Assert.AreNotEqual(0, 1);
        }



        [TestMethod]
        public void GetPercentageOfCompliantCompanies()
        {
            rh.RadarHelper rhx = new rh.RadarHelper();
            int result = rhx.GetPercentageOfCompaniesCompliant(15,2, "JSAAudits", "JSAScore");
            Assert.AreNotEqual(0, result);
        }

        [TestMethod]
        public void GetNumberOfCompliantCompanies()    
        {
            rh.RadarHelper rhx = new rh.RadarHelper();
            int result = rhx.GetNumberOfCompliantCompanies(15, 2, "JSAAudits", "JSAScore");
            Assert.AreEqual(10, result);
        }
        [TestMethod]
        public void RecordCompanySiteCategoryStandardHistory()
        {
            rh.RadarHelper rhx = new rh.RadarHelper();
            rhx.RecordCompanySiteCategoryStandardHistory();
            //Assert.IsNotNull(result);
        }

        [TestMethod]
        public void GetPercentageSafetyPlansCompliant()
        {
            rh.RadarHelper rhx = new rh.RadarHelper();
            int result = rhx.GetPercentageOfCompaniesSafetyPlansCompliant(1, 1);

            Assert.IsNotNull(result);
        }
        [TestMethod]
        public void GetPercentageSafetyPlansCompliantByRegion()
        {
            rh.RadarHelper rhx = new rh.RadarHelper();
            int result = rhx.GetPercentageOfCompaniesSafetyPlansCompliantByRegion(1, 1);

            Assert.IsNotNull(result);
        }
        
        [TestMethod]
        public void CommaStringFromListTest()
        {
            rh.RadarHelper rhx = new rh.RadarHelper();
            List<string> lst = new List<string>();
            lst.Add("Company1");
            lst.Add("Company2");
            lst.Add("Company3");
            string result = rhx.CommaStringFromList(lst);
            Assert.AreEqual("Company1, Company2, Company3", result);
        }
        
        
        [TestMethod]
        public void GetNonCompliantCompaniesTest()
        {
            rh.RadarHelper rhx = new rh.RadarHelper();
            string result = rhx.GetNonCompliantCompanies(1, 1, "JSAAudits","JSAScore");
            Assert.AreNotEqual("", result);

        }
        [TestMethod]
        public void GetNonCompliantCompaniesTest1()
        {
            rh.RadarHelper rhx = new rh.RadarHelper();
            string result = rhx.GetNonCompliantCompanies(1, 1, "Behavioural Observations (%)", "JSAScore");
            Assert.AreNotEqual("", result);

        }
        [TestMethod]
        public void GetCompaniesNotCSACompliantTest()
        {
            rh.RadarHelper rhx = new rh.RadarHelper();
            string result = rhx.GetCompaniesNotCSACompliant(3,2);
            Assert.AreNotEqual("",result);
        }
        [TestMethod]
        public void UpdateCompaniesNotCSACompliantAllSites()
        {
            rh.RadarHelper rhx = new rh.RadarHelper();
            rhx.UpdateCompaniesNotCSACompliantAllSites();
            //Assert.AreNotEqual("", result);
        }
        [TestMethod]
        public void GetMonthNameFromMonthNumberTest()
        {
            rh.RadarHelper rhx = new rh.RadarHelper();
            string result = rhx.GetMonthNameFromMonthNumber(2);
            Assert.AreEqual("Feb",result);
        }
        [TestMethod]
        public void GetNumberOfActiveCompaniesByRegionTest()
        {
            rh.RadarHelper rhx = new rh.RadarHelper();
            int result = rhx.GetNumberOfCompaniesByRegion(1, 2);
            Assert.IsNotNull(result);
        }
        [TestMethod]
        public void GetNumberOfActiveCompanies()
        {
            rh.RadarHelper rhx = new rh.RadarHelper();
            int result = rhx.GetNumberOfCompanies(4, 2);
            Assert.IsNotNull(result);
        }
        [TestMethod]
        public void GetNumberOfCompaniesOnsite()
        {
            rh.RadarHelper rhx = new rh.RadarHelper();
            int result = rhx.GetNumberOfCompaniesOnSite(4, 2);
            Assert.IsNotNull(result);
        }
        [TestMethod]
        public void GetPercentageCSACompliant()
        {

            rh.RadarHelper rhx = new rh.RadarHelper();
            int result = rhx.GetPercentageOfCompaniesCSACompliant(4, 2);

            Assert.IsNotNull(result);
        }
        [TestMethod]
        public void GetPercentageCSACompliantByRegion()
        {

            rh.RadarHelper rhx = new rh.RadarHelper();
            int result = rhx.GetPercentageOfCompaniesCSACompliantByRegion(1, 2);

            Assert.IsNotNull(result);
        }
        [TestMethod]
        public void GetCountCSACompliant()
        {

            rh.RadarHelper rhx = new rh.RadarHelper();
            int result = rhx.GetNumberOfCompaniesCSACompliant(4, 2);

            Assert.IsNotNull(result);
        }
        [TestMethod]
        public void GetCountCSACompliantByRegion()
        {

            rh.RadarHelper rhx = new rh.RadarHelper();
            int result = rhx.GetNumberOfCompaniesCSACompliantByRegion(1, 2);

            Assert.IsNotNull(result);
        }
        [TestMethod]
        public void isCSACompliant()
        {
            
            rh.RadarHelper rhx = new rh.RadarHelper();
            string result = rhx.IsCompanyCSACompliantForPreviousMonth(144, 4);

            Assert.AreEqual("true", result);
        }
        [TestMethod]
        public void CsaQtr1()
        {
            DateTime dt = new DateTime(2016,1,1);
            rh.CSAQuarter csaQtr = new rh.CSAQuarter(dt, 1);
            Assert.AreEqual(1,csaQtr.CurrentQuarter);
        }
        [TestMethod]
        public void CsaQtr2()
        {
            DateTime dt = new DateTime(2016, 4, 1);
            rh.CSAQuarter csaQtr = new rh.CSAQuarter(dt, 1);
            Assert.AreEqual(2, csaQtr.CurrentQuarter);
        }
        [TestMethod]
        public void CsaQtr3()
        {
            DateTime dt = new DateTime(2016, 7, 1);
            rh.CSAQuarter csaQtr = new rh.CSAQuarter(dt, 1);
            Assert.AreEqual(3, csaQtr.CurrentQuarter);
        }
        [TestMethod]
        public void CsaQtr4()
        {
            DateTime dt = new DateTime(2016, 11, 1);
            rh.CSAQuarter csaQtr = new rh.CSAQuarter(dt, 1);
            Assert.AreEqual(4, csaQtr.CurrentQuarter);
        }

        [TestMethod]
        public void CsaQtr12()
        {
            DateTime dt = new DateTime(2016, 1, 1);
            rh.CSAQuarter csaQtr = new rh.CSAQuarter(dt, 2);
            Assert.AreEqual(1, csaQtr.CurrentQuarter);
        }
        [TestMethod]
        public void CsaQtr22()
        {
            DateTime dt = new DateTime(2016, 4, 1);
            rh.CSAQuarter csaQtr = new rh.CSAQuarter(dt, 2);
            Assert.AreEqual(1, csaQtr.CurrentQuarter);
        }
        [TestMethod]
        public void CsaQtr32()
        {
            DateTime dt = new DateTime(2016, 7, 1);
            rh.CSAQuarter csaQtr = new rh.CSAQuarter(dt, 2);
            Assert.AreEqual(3, csaQtr.CurrentQuarter);
        }
        [TestMethod]
        public void CsaQtr42()
        {
            DateTime dt = new DateTime(2016, 11, 1);
            rh.CSAQuarter csaQtr = new rh.CSAQuarter(dt, 2);
            Assert.AreEqual(3, csaQtr.CurrentQuarter);
        }
        [TestMethod]
        public void CsaQtr12p()
        {
            DateTime dt = new DateTime(2016, 1, 1);
            rh.CSAQuarter csaQtr = new rh.CSAQuarter(dt, 2);
            Assert.AreEqual(3, csaQtr.PreviousQuarter);
        }
        [TestMethod]
        public void CsaQtr22p()
        {
            DateTime dt = new DateTime(2016, 4, 1);
            rh.CSAQuarter csaQtr = new rh.CSAQuarter(dt, 2);
            Assert.AreEqual(3, csaQtr.PreviousQuarter);
        }
        [TestMethod]
        public void CsaQtr32p()
        {
            DateTime dt = new DateTime(2016, 7, 1);
            rh.CSAQuarter csaQtr = new rh.CSAQuarter(dt, 2);
            Assert.AreEqual(1, csaQtr.PreviousQuarter);
        }
        [TestMethod]
        public void CsaQtr42p()
        {
            DateTime dt = new DateTime(2016, 11, 1);
            rh.CSAQuarter csaQtr = new rh.CSAQuarter(dt, 2);
            Assert.AreEqual(1, csaQtr.PreviousQuarter);
        }
        [TestMethod]
        public void CsaQtr12py()
        {
            DateTime dt = new DateTime(2016, 1, 1);
            rh.CSAQuarter csaQtr = new rh.CSAQuarter(dt, 2);
            Assert.AreEqual(2015, csaQtr.PreviousQuarterYear);
        }
        [TestMethod]
        public void GetNonCompliantCompaniesSafetyPlansTest()
        {
            rh.RadarHelper rhx = new rh.RadarHelper();
            string result = rhx.GetNonCompliantCompaniesSafetyPlans(1, 1);
            Assert.AreNotEqual("", result);
        }
    }
}
