using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using KaiZen.CSMS.Services;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS;
using KaiZen.CSMS.Encryption;
using System.Reflection;
using System.Xml;
using DevExpress.Web.ASPxPopupControl;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxCallbackPanel;
using DevExpress.Web.ASPxClasses;

public partial class UserControls_TwentyOnePointAudit : System.Web.UI.UserControl
{

    #region Global Var sections
    string[] section01 = new String[] { "1a", "1b", "1c", "1c1", "1c2", "1c3", "1c4", "1c5", "1c6", "1c7", "1c8", "1c9", "1c10", "1c11", "1c12",
                                            "1d", "1e", "1f", "1g", "1h", "1i", "1j", "1k", 
                                            "1l", "1m", "1n", "1o", "1p", "1q", "1r", "1s", 
                                            "1t", "1u", "1v", "1w", "1x", "1y", "1z" };
    string[] section02 = new String[] { "2a", "2b", "2c", "2d", "2e", "2f", "2g", "2h", "2i" };
    string[] section03 = new String[] { "3a", "3b", "3c", "3d", "3e", "3f", "3g", "3h", "3i" };
    string[] section04 = new String[] { "4a", "4b", "4c", "4d", "4e", "4f", "4g", "4h", "4i", "4j", "4k", "4l" };
    string[] section05 = new String[] { "5a", "5b", "5c", "5d", "5e", "5f", "5g", "5h"};
    string[] section06 = new String[] { "6a", "6b", "6c", "6d", "6e", "6f", "6g", "6h", "6i", "6j", "6k", "6l" };
    string[] section07 = new String[] { "7a", "7b", "7c", "7d", "7e", "7f"};
    string[] section08 = new String[] { "8a", "8b", "8c", "8d", "8e", "8f", "8g", "8h", "8i" };
    string[] section09 = new String[] { "9a", "9b", "9c", "9d", "9e", "9f", "9g", "9h" };
    string[] section10 = new String[] { "10a", "10b", "10c", "10d", "10e", "10f", "10g", "10h" };
    string[] section11 = new String[] { "11a", "11b", "11c", "11d", "11e", "11f"};
    string[] section12 = new String[] { "12a", "12b", "12c", "12d", "12e"};
    string[] section13 = new String[] { "13a", "13b", "13c", "13d", "13e", "13f"};
    string[] section14 = new String[] { "14a", "14b", "14c", "14d", "14e", "14f", "14g", "14h" };
    string[] section15 = new String[] { "15a", "15b", "15c", "15d", "15e", "15f", "15g", "15h", "15i" };
    string[] section16 = new String[] { "16a", "16b", "16c", "16d", "16e", "16f", "16g", "16h", "16i", "16j" };
    string[] section17 = new String[] { "17a", "17b", "17c", "17d", "17e", "17f", "17g", "17h", "17i", "17j", "17k", "17l", "17m" };
    string[] section18 = new String[] { "18a", "18b", "18c", "18d", "18e", "18f", "18g", "18h", "18i", "18j", "18k", "18l", 
                                        "18m", "18n", "18o", "18p" };
    string[] section19 = new String[] { "19a", "19b", "19c", "19d", "19e", "19f" };
    string[] section20 = new String[] { "20a", "20b", "20c", "20d", "20e" };
    string[] section21 = new String[] { "21a", "21b", "21c", "21d", "21e", "21f", "21g", "21h", "21i" };
    #endregion

    TwentyOnePointAuditService topaService2;
    KaiZen.CSMS.Entities.TwentyOnePointAudit topa2;

    Auth auth = new Auth();
    protected void Page_Load(object sender, EventArgs e) { Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); }

    private void moduleInit(bool postBack)
    {

        if (!postBack)
        {
            //StayAlive sa = new StayAlive();
            //Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "reconn key", sa.KeepAlive());
            //Populate Previous 5 Years into DDL
            ddlYear.Items.Clear();
            //ddlYear.Items.Add((Convert.ToInt32(DateTime.Now.Year) - 3).ToString());
            //ddlYear.Items.Add((Convert.ToInt32(DateTime.Now.Year) - 2).ToString());
            //ddlYear.Items.Add((Convert.ToInt32(DateTime.Now.Year) - 1).ToString());
            //ddlYear.Items.Add(DateTime.Now.Year.ToString());
            ddlYear.Items.Add("2007");
            ddlYear.Items.Add("2008");
            ddlYear.SelectedIndex = 1;

            ddlMonth.DataSourceID = "topaQtr";
            ddlMonth.DataTextField = "QtrName";
            ddlMonth.DataValueField = "QtrId";
            ddlMonth.DataBind();

            ddlCompanies.Text = "< Select Company >";
            ddlCompanies.DataSourceID = "sqldsCompaniesList";
            ddlCompanies.TextField = "CompanyName";
            ddlCompanies.ValueField = "CompanyId";
            ddlCompanies.DataBind();

            SessionHandler.spVar_CompanyId = auth.CompanyId.ToString();
            FillSitesCombo(SessionHandler.spVar_CompanyId);

            switch (auth.RoleId)
            {
                case ((int)RoleList.Reader):
                    btnSave.Visible = true;
                    btnSave.Enabled = true;
                    //btnSearchGo.Text = "View";
                    break;
                ////Not Visible to Contractor's anymore.
                //case ((int)RoleList.Contractor):
                //    ddlCompanies.Value = auth.CompanyId;
                //    ddlCompanies.Enabled = false;
                //    //ddlMonth.Items.Remove("Alcoa Annual Review");
                //    ddlMonth.Items.Remove(new ListItem("Alcoa Annual Review","5"));
                //    break;
                case ((int)RoleList.Administrator):
                   
                    break;
                default: // RoleList.Disabled or No Role
                    break;
            }
        }
        else
        { //postback
            //CalcScores_Load();

        }
    }

    protected void ddlSites_Callback(object source, CallbackEventArgsBase e)
    {
        FillSitesCombo(e.Parameter);
        ddlSites.SelectedIndex = 0;
    }
    // internal
    protected void FillSitesCombo(string companyId)
    {
        if (string.IsNullOrEmpty(companyId)) return;
        SessionHandler.spVar_CompanyId = companyId;

        SitesService sService = new SitesService();
        CompanySiteCategoryStandardService cscsService = new CompanySiteCategoryStandardService();
        TList<CompanySiteCategoryStandard> cscsTlist = cscsService.GetByCompanyId(Convert.ToInt32(companyId));

        ddlSites.Items.Clear();
        int _i = 0;

        if (Convert.ToInt32(companyId) > 0)
        {
            if (cscsTlist != null)
            {
                if (cscsTlist.Count > 0)
                {
                    SitesFilters sitesFilters = new SitesFilters();
                    sitesFilters.Append(SitesColumn.IsVisible, "1");
                    TList<Sites> sitesList = DataRepository.SitesProvider.GetPaged(sitesFilters.ToString(), "SiteName ASC", 0, 100, out _i);
                    foreach (Sites s in sitesList)
                    {
                        foreach (CompanySiteCategoryStandard cscs in cscsTlist)
                        {
                            if (s.SiteId == cscs.SiteId)
                            {
                                ddlSites.Items.Add(s.SiteName, cscs.SiteId);
                            }
                        }
                    }
                }
            }
        }

        //if (string.IsNullOrEmpty(companyId)) return;
        //SessionHandler.spVar_CompanyId = companyId;
        //ddlSites.DataSourceID = "sqldsResidential_Sites_ByCompany";
        //ddlSites.TextField = "SiteName";
        //ddlSites.ValueField = "SiteId";
        //sqldsResidential_Sites_ByCompany.SelectParameters[0].DefaultValue = companyId;
        //sqldsResidential_Sites_ByCompany.DataBind();
        //ddlSites.DataBind();
    }

    protected void CalcScores_Load()
    {
        #region Score
        foreach (string str in section01) { CalcScore(str); }
        foreach (string str in section02) { CalcScore(str); }
        foreach (string str in section03) { CalcScore(str); }
        foreach (string str in section04) { CalcScore(str); }
        foreach (string str in section05) { CalcScore(str); }
        foreach (string str in section06) { CalcScore(str); }
        foreach (string str in section07) { CalcScore(str); }
        foreach (string str in section08) { CalcScore(str); }
        foreach (string str in section09) { CalcScore(str); }
        foreach (string str in section10) { CalcScore(str); }
        foreach (string str in section11) { CalcScore(str); }
        foreach (string str in section12) { CalcScore(str); }
        foreach (string str in section13) { CalcScore(str); }
        foreach (string str in section14) { CalcScore(str); }
        foreach (string str in section15) { CalcScore(str); }
        foreach (string str in section16) { CalcScore(str); }
        foreach (string str in section17) { CalcScore(str); }
        foreach (string str in section18) { CalcScore(str); }
        foreach (string str in section19) { CalcScore(str); }
        foreach (string str in section20) { CalcScore(str); }
        foreach (string str in section21) { CalcScore(str); }
        #endregion
        #region ScoreA/B/C
        int totalScoreA = 0;
        int totalScoreB = 0;
        double totalScoreC = 0;
        //1
        totalScoreA = 0;
        totalScoreB = 0;
        totalScoreC = 0;
        foreach (string str in section01) { totalScoreA += CalcTotalScoreA(str); }
        if (totalScoreA != 0)
        {
            totalScoreB = CalcTotalScoreB(01);
            totalScoreC = (Double)(Convert.ToDouble(totalScoreB) / Convert.ToDouble(totalScoreA)) * 100;
        }
        pAAScore1a.Text = totalScoreA.ToString();
        pAAScore1b.Text = totalScoreB.ToString();
        pAAScore1c.Text = Convert.ToInt32(totalScoreC).ToString() + "%";
        pAAScore1a_.Text = pAAScore1a.Text;
        pAAScore1b_.Text = pAAScore1b.Text;
        pAAScore1c_.Text = pAAScore1c.Text;
        //2
        totalScoreA = 0;
        totalScoreB = 0;
        totalScoreC = 0;
        foreach (string str in section02) { totalScoreA += CalcTotalScoreA(str); }
        if (totalScoreA != 0)
        {
            totalScoreB = CalcTotalScoreB(02);
            totalScoreC = (Double)(Convert.ToDouble(totalScoreB) / Convert.ToDouble(totalScoreA)) * 100;
        }
        pAAScore2a.Text = totalScoreA.ToString();
        pAAScore2b.Text = totalScoreB.ToString();
        pAAScore2c.Text = Convert.ToInt32(totalScoreC).ToString() + "%";
        pAAScore2a_.Text = pAAScore2a.Text;
        pAAScore2b_.Text = pAAScore2b.Text;
        pAAScore2c_.Text = pAAScore2c.Text;
        //3
        totalScoreA = 0;
        totalScoreB = 0;
        totalScoreC = 0;
        foreach (string str in section03) { totalScoreA += CalcTotalScoreA(str); }
        if (totalScoreA != 0)
        {
            totalScoreB = CalcTotalScoreB(03);
            totalScoreC = (Double)(Convert.ToDouble(totalScoreB) / Convert.ToDouble(totalScoreA)) * 100;
        }
        pAAScore3a.Text = totalScoreA.ToString();
        pAAScore3b.Text = totalScoreB.ToString();
        pAAScore3c.Text = Convert.ToInt32(totalScoreC).ToString() + "%";
        pAAScore3a_.Text = pAAScore3a.Text;
        pAAScore3b_.Text = pAAScore3b.Text;
        pAAScore3c_.Text = pAAScore3c.Text;
        //4
        totalScoreA = 0;
        totalScoreB = 0;
        totalScoreC = 0;
        foreach (string str in section04) { totalScoreA += CalcTotalScoreA(str); }
        if (totalScoreA != 0)
        {
            totalScoreB = CalcTotalScoreB(04);
            totalScoreC = (Double)(Convert.ToDouble(totalScoreB) / Convert.ToDouble(totalScoreA)) * 100;
        }
        pAAScore4a.Text = totalScoreA.ToString();
        pAAScore4b.Text = totalScoreB.ToString();
        pAAScore4c.Text = Convert.ToInt32(totalScoreC).ToString() + "%";
        pAAScore4a_.Text = pAAScore4a.Text;
        pAAScore4b_.Text = pAAScore4b.Text;
        pAAScore4c_.Text = pAAScore4c.Text;
        //5
        totalScoreA = 0;
        totalScoreB = 0;
        totalScoreC = 0;
        foreach (string str in section05) { totalScoreA += CalcTotalScoreA(str); }
        if (totalScoreA != 0)
        {
            totalScoreB = CalcTotalScoreB(05);
            totalScoreC = (Double)(Convert.ToDouble(totalScoreB) / Convert.ToDouble(totalScoreA)) * 100;
        }
        pAAScore5a.Text = totalScoreA.ToString();
        pAAScore5b.Text = totalScoreB.ToString();
        pAAScore5c.Text = Convert.ToInt32(totalScoreC).ToString() + "%";
        pAAScore5a_.Text = pAAScore5a.Text;
        pAAScore5b_.Text = pAAScore5b.Text;
        pAAScore5c_.Text = pAAScore5c.Text;
        //6
        totalScoreA = 0;
        totalScoreB = 0;
        totalScoreC = 0;
        foreach (string str in section06) { totalScoreA += CalcTotalScoreA(str); }
        if (totalScoreA != 0)
        {
            totalScoreB = CalcTotalScoreB(06);
            totalScoreC = (Double)(Convert.ToDouble(totalScoreB) / Convert.ToDouble(totalScoreA)) * 100;
        }
        pAAScore6a.Text = totalScoreA.ToString();
        pAAScore6b.Text = totalScoreB.ToString();
        pAAScore6c.Text = Convert.ToInt32(totalScoreC).ToString() + "%";
        pAAScore6a_.Text = pAAScore6a.Text;
        pAAScore6b_.Text = pAAScore6b.Text;
        pAAScore6c_.Text = pAAScore6c.Text;
        //7
        totalScoreA = 0;
        totalScoreB = 0;
        totalScoreC = 0;
        foreach (string str in section07) { totalScoreA += CalcTotalScoreA(str); }
        if (totalScoreA != 0)
        {
            totalScoreB = CalcTotalScoreB(07);
            totalScoreC = (Double)(Convert.ToDouble(totalScoreB) / Convert.ToDouble(totalScoreA)) * 100;
        }
        pAAScore7a.Text = totalScoreA.ToString();
        pAAScore7b.Text = totalScoreB.ToString();
        pAAScore7c.Text = Convert.ToInt32(totalScoreC).ToString() + "%";
        pAAScore7a_.Text = pAAScore7a.Text;
        pAAScore7b_.Text = pAAScore7b.Text;
        pAAScore7c_.Text = pAAScore7c.Text;
        //8
        totalScoreA = 0;
        totalScoreB = 0;
        totalScoreC = 0;
        foreach (string str in section08) { totalScoreA += CalcTotalScoreA(str); }
        if (totalScoreA != 0)
        {
            totalScoreB = CalcTotalScoreB(08);
            totalScoreC = (Double)(Convert.ToDouble(totalScoreB) / Convert.ToDouble(totalScoreA)) * 100;
        }
        pAAScore8a.Text = totalScoreA.ToString();
        pAAScore8b.Text = totalScoreB.ToString();
        pAAScore8c.Text = Convert.ToInt32(totalScoreC).ToString() + "%";
        pAAScore8a_.Text = pAAScore8a.Text;
        pAAScore8b_.Text = pAAScore8b.Text;
        pAAScore8c_.Text = pAAScore8c.Text;
        //9
        totalScoreA = 0;
        totalScoreB = 0;
        totalScoreC = 0;
        foreach (string str in section09) { totalScoreA += CalcTotalScoreA(str); }
        if (totalScoreA != 0)
        {
            totalScoreB = CalcTotalScoreB(09);
            totalScoreC = (Double)(Convert.ToDouble(totalScoreB) / Convert.ToDouble(totalScoreA)) * 100;
        }
        pAAScore9a.Text = totalScoreA.ToString();
        pAAScore9b.Text = totalScoreB.ToString();
        pAAScore9c.Text = Convert.ToInt32(totalScoreC).ToString() + "%";
        pAAScore9a_.Text = pAAScore9a.Text;
        pAAScore9b_.Text = pAAScore9b.Text;
        pAAScore9c_.Text = pAAScore9c.Text;
        //10
        totalScoreA = 0;
        totalScoreB = 0;
        totalScoreC = 0;
        foreach (string str in section10) { totalScoreA += CalcTotalScoreA(str); }
        if (totalScoreA != 0)
        {
            totalScoreB = CalcTotalScoreB(10);
            totalScoreC = (Double)(Convert.ToDouble(totalScoreB) / Convert.ToDouble(totalScoreA)) * 100;
        }
        pAAScore10a.Text = totalScoreA.ToString();
        pAAScore10b.Text = totalScoreB.ToString();
        pAAScore10c.Text = Convert.ToInt32(totalScoreC).ToString() + "%";
        pAAScore10a_.Text = pAAScore10a.Text;
        pAAScore10b_.Text = pAAScore10b.Text;
        pAAScore10c_.Text = pAAScore10c.Text;
        //11
        totalScoreA = 0;
        totalScoreB = 0;
        totalScoreC = 0;
        foreach (string str in section11) { totalScoreA += CalcTotalScoreA(str); }
        if (totalScoreA != 0)
        {
            totalScoreB = CalcTotalScoreB(11);
            totalScoreC = (Double)(Convert.ToDouble(totalScoreB) / Convert.ToDouble(totalScoreA)) * 100;
        }
        pAAScore11a.Text = totalScoreA.ToString();
        pAAScore11b.Text = totalScoreB.ToString();
        pAAScore11c.Text = Convert.ToInt32(totalScoreC).ToString() + "%";
        pAAScore11a_.Text = pAAScore11a.Text;
        pAAScore11b_.Text = pAAScore11b.Text;
        pAAScore11c_.Text = pAAScore11c.Text;
        //12
        totalScoreA = 0;
        totalScoreB = 0;
        totalScoreC = 0;
        foreach (string str in section12) { totalScoreA += CalcTotalScoreA(str); }
        if (totalScoreA != 0)
        {
            totalScoreB = CalcTotalScoreB(12);
            totalScoreC = (Double)(Convert.ToDouble(totalScoreB) / Convert.ToDouble(totalScoreA)) * 100;
        }
        pAAScore12a.Text = totalScoreA.ToString();
        pAAScore12b.Text = totalScoreB.ToString();
        pAAScore12c.Text = Convert.ToInt32(totalScoreC).ToString() + "%";
        pAAScore12a_.Text = pAAScore12a.Text;
        pAAScore12b_.Text = pAAScore12b.Text;
        pAAScore12c_.Text = pAAScore12c.Text;
        //13
        totalScoreA = 0;
        totalScoreB = 0;
        totalScoreC = 0;
        foreach (string str in section13) { totalScoreA += CalcTotalScoreA(str); }
        if (totalScoreA != 0)
        {
            totalScoreB = CalcTotalScoreB(13);
            totalScoreC = (Double)(Convert.ToDouble(totalScoreB) / Convert.ToDouble(totalScoreA)) * 100;
        }
        pAAScore13a.Text = totalScoreA.ToString();
        pAAScore13b.Text = totalScoreB.ToString();
        pAAScore13c.Text = Convert.ToInt32(totalScoreC).ToString() + "%";
        pAAScore13a_.Text = pAAScore13a.Text;
        pAAScore13b_.Text = pAAScore13b.Text;
        pAAScore13c_.Text = pAAScore13c.Text;
        //14
        totalScoreA = 0;
        totalScoreB = 0;
        totalScoreC = 0;
        foreach (string str in section14) { totalScoreA += CalcTotalScoreA(str); }
        if (totalScoreA != 0)
        {
            totalScoreB = CalcTotalScoreB(14);
            totalScoreC = (Double)(Convert.ToDouble(totalScoreB) / Convert.ToDouble(totalScoreA)) * 100;
        }
        pAAScore14a.Text = totalScoreA.ToString();
        pAAScore14b.Text = totalScoreB.ToString();
        pAAScore14c.Text = Convert.ToInt32(totalScoreC).ToString() + "%";
        pAAScore14a_.Text = pAAScore14a.Text;
        pAAScore14b_.Text = pAAScore14b.Text;
        pAAScore14c_.Text = pAAScore14c.Text;
        //15
        totalScoreA = 0;
        totalScoreB = 0;
        totalScoreC = 0;
        foreach (string str in section15) { totalScoreA += CalcTotalScoreA(str); }
        if (totalScoreA != 0)
        {
            totalScoreB = CalcTotalScoreB(15);
            totalScoreC = (Double)(Convert.ToDouble(totalScoreB) / Convert.ToDouble(totalScoreA)) * 100;
        }
        pAAScore15a.Text = totalScoreA.ToString();
        pAAScore15b.Text = totalScoreB.ToString();
        pAAScore15c.Text = Convert.ToInt32(totalScoreC).ToString() + "%";
        pAAScore15a_.Text = pAAScore15a.Text;
        pAAScore15b_.Text = pAAScore15b.Text;
        pAAScore15c_.Text = pAAScore15c.Text;
        //16
        totalScoreA = 0;
        totalScoreB = 0;
        totalScoreC = 0;
        foreach (string str in section16) { totalScoreA += CalcTotalScoreA(str); }
        if (totalScoreA != 0)
        {
            totalScoreB = CalcTotalScoreB(16);
            totalScoreC = (Double)(Convert.ToDouble(totalScoreB) / Convert.ToDouble(totalScoreA)) * 100;
        }
        pAAScore16a.Text = totalScoreA.ToString();
        pAAScore16b.Text = totalScoreB.ToString();
        pAAScore16c.Text = Convert.ToInt32(totalScoreC).ToString() + "%";
        pAAScore16a_.Text = pAAScore16a.Text;
        pAAScore16b_.Text = pAAScore16b.Text;
        pAAScore16c_.Text = pAAScore16c.Text;
        //17
        totalScoreA = 0;
        totalScoreB = 0;
        totalScoreC = 0;
        foreach (string str in section17) { totalScoreA += CalcTotalScoreA(str); }
        if (totalScoreA != 0)
        {
            totalScoreB = CalcTotalScoreB(17);
            totalScoreC = (Double)(Convert.ToDouble(totalScoreB) / Convert.ToDouble(totalScoreA)) * 100;
        }
        pAAScore17a.Text = totalScoreA.ToString();
        pAAScore17b.Text = totalScoreB.ToString();
        pAAScore17c.Text = Convert.ToInt32(totalScoreC).ToString() + "%";
        pAAScore17a_.Text = pAAScore17a.Text;
        pAAScore17b_.Text = pAAScore17b.Text;
        pAAScore17c_.Text = pAAScore17c.Text;
        //18
        totalScoreA = 0;
        totalScoreB = 0;
        totalScoreC = 0;
        foreach (string str in section18) { totalScoreA += CalcTotalScoreA(str); }
        if (totalScoreA != 0)
        {
            totalScoreB = CalcTotalScoreB(18);
            totalScoreC = (Double)(Convert.ToDouble(totalScoreB) / Convert.ToDouble(totalScoreA)) * 100;
        }
        pAAScore18a.Text = totalScoreA.ToString();
        pAAScore18b.Text = totalScoreB.ToString();
        pAAScore18c.Text = Convert.ToInt32(totalScoreC).ToString() + "%";
        pAAScore18a_.Text = pAAScore18a.Text;
        pAAScore18b_.Text = pAAScore18b.Text;
        pAAScore18c_.Text = pAAScore18c.Text;
        //19
        totalScoreA = 0;
        totalScoreB = 0;
        totalScoreC = 0;
        foreach (string str in section19) { totalScoreA += CalcTotalScoreA(str); }
        if (totalScoreA != 0)
        {
            totalScoreB = CalcTotalScoreB(19);
            totalScoreC = (Double)(Convert.ToDouble(totalScoreB) / Convert.ToDouble(totalScoreA)) * 100;
        }
        pAAScore19a.Text = totalScoreA.ToString();
        pAAScore19b.Text = totalScoreB.ToString();
        pAAScore19c.Text = Convert.ToInt32(totalScoreC).ToString() + "%";
        pAAScore19a_.Text = pAAScore19a.Text;
        pAAScore19b_.Text = pAAScore19b.Text;
        pAAScore19c_.Text = pAAScore19c.Text;
        //20
        totalScoreA = 0;
        totalScoreB = 0;
        totalScoreC = 0;
        foreach (string str in section20) { totalScoreA += CalcTotalScoreA(str); }
        if (totalScoreA != 0)
        {
            totalScoreB = CalcTotalScoreB(20);
            totalScoreC = (Double)(Convert.ToDouble(totalScoreB) / Convert.ToDouble(totalScoreA)) * 100;
        }
        pAAScore20a.Text = totalScoreA.ToString();
        pAAScore20b.Text = totalScoreB.ToString();
        pAAScore20c.Text = Convert.ToInt32(totalScoreC).ToString() + "%";
        pAAScore20a_.Text = pAAScore20a.Text;
        pAAScore20b_.Text = pAAScore20b.Text;
        pAAScore20c_.Text = pAAScore20c.Text;
        //21
        totalScoreA = 0;
        totalScoreB = 0;
        totalScoreC = 0;
        foreach (string str in section21) { totalScoreA += CalcTotalScoreA(str); }
        if (totalScoreA != 0)
        {
            totalScoreB = CalcTotalScoreB(21);
            totalScoreC = (Double)(Convert.ToDouble(totalScoreB) / Convert.ToDouble(totalScoreA)) * 100;
        }
        pAAScore21a.Text = totalScoreA.ToString();
        pAAScore21b.Text = totalScoreB.ToString();
        pAAScore21c.Text = Convert.ToInt32(totalScoreC).ToString() + "%";
        pAAScore21a_.Text = pAAScore21a.Text;
        pAAScore21b_.Text = pAAScore21b.Text;
        pAAScore21c_.Text = pAAScore21c.Text;
        #endregion
        #region ScoreTotal
        int pAAScoreTotala_ = 0;
        int pAAScoreTotalb_ = 0;
        int pAAScoreTotalc_ = 0;

        pAAScoreTotala_ = Convert.ToInt32(pAAScore1a_.Text) + Convert.ToInt32(pAAScore2a_.Text) + Convert.ToInt32(pAAScore3a_.Text) + Convert.ToInt32(pAAScore4a_.Text)
                             + Convert.ToInt32(pAAScore5a_.Text) + Convert.ToInt32(pAAScore6a_.Text) + Convert.ToInt32(pAAScore7a_.Text) + Convert.ToInt32(pAAScore8a_.Text)
                             + Convert.ToInt32(pAAScore9a_.Text) + Convert.ToInt32(pAAScore10a_.Text) + Convert.ToInt32(pAAScore11a_.Text) + Convert.ToInt32(pAAScore12a_.Text)
                             + Convert.ToInt32(pAAScore13a_.Text) + Convert.ToInt32(pAAScore14a_.Text) + Convert.ToInt32(pAAScore15a_.Text) + Convert.ToInt32(pAAScore16a_.Text)
                             + Convert.ToInt32(pAAScore17a_.Text) + Convert.ToInt32(pAAScore18a_.Text) + Convert.ToInt32(pAAScore19a_.Text) + Convert.ToInt32(pAAScore20a_.Text)
                             + Convert.ToInt32(pAAScore21a_.Text);
        pAAScoreTotala.Text = pAAScoreTotala_.ToString();
        pAAScoreTotalb_ = Convert.ToInt32(pAAScore1b_.Text) + Convert.ToInt32(pAAScore2b_.Text) + Convert.ToInt32(pAAScore3b_.Text) + Convert.ToInt32(pAAScore4b_.Text)
                             + Convert.ToInt32(pAAScore5b_.Text) + Convert.ToInt32(pAAScore6b_.Text) + Convert.ToInt32(pAAScore7b_.Text) + Convert.ToInt32(pAAScore8b_.Text)
                             + Convert.ToInt32(pAAScore9b_.Text) + Convert.ToInt32(pAAScore10b_.Text) + Convert.ToInt32(pAAScore11b_.Text) + Convert.ToInt32(pAAScore12b_.Text)
                             + Convert.ToInt32(pAAScore13b_.Text) + Convert.ToInt32(pAAScore14b_.Text) + Convert.ToInt32(pAAScore15b_.Text) + Convert.ToInt32(pAAScore16b_.Text)
                             + Convert.ToInt32(pAAScore17b_.Text) + Convert.ToInt32(pAAScore18b_.Text) + Convert.ToInt32(pAAScore19b_.Text) + Convert.ToInt32(pAAScore20b_.Text)
                             + Convert.ToInt32(pAAScore21b_.Text);
        pAAScoreTotalb.Text = pAAScoreTotalb_.ToString();

        double a = Convert.ToDouble(pAAScoreTotala_);
        double b = Convert.ToDouble(pAAScoreTotalb_);
        double c = b / a;
        double c2 = c * 100;
        pAAScoreTotalc_ = (int)Math.Round(c2, 0);
        if (a != 0)
        {
            pAAScoreTotalc.Text = pAAScoreTotalc_.ToString() + "%";
        }
        else
        {
            pAAScoreTotalc.Text = "0%";
        }
        

        #endregion
    }
    protected void CalcScore(string id)
    {
        string Weight = ""; //Label
        string Achieved = ""; //Textbox
        string Score = ""; //Label
        string js = "";

        Label oWeightCtl;
        DropDownList oAchievedCtl;
        Label oScoreCtl;

        Control oCtlWeight = this.FindControl("pWeight" + id);
        oWeightCtl = (Label)oCtlWeight;
        Weight = oWeightCtl.Text.ToString();

        Control oCtlAchieved = this.FindControl("pAchieved" + id);
        oAchievedCtl = (DropDownList)oCtlAchieved;
        Achieved = oAchievedCtl.Text.ToString();

        Control oCtlScore = this.FindControl("pScore" + id);
        oScoreCtl = (Label)oCtlScore;
        Score = oScoreCtl.Text.ToString();

        //js = "(document.getElementById('" + Score + "').innerText = ((document.getElementById('" + Achieved + "').value) * (document.getElementById('" + Weight + "').innerText))); return false;";
        //js = "(document.getElementById('<%=" + "pScore" + id + ".ClientID%>').innerText = ((document.getElementById('<%=" + "pAchieved" + id + ".ClientID%>').value) * (document.getElementById('<%=" + "pWeight" + id + ".ClientID%>').innerText))); return false;";
        js = "(document.getElementById('ctl00_contentBody_TwentyOnePointAudit_pScore" + id + "').innerText = ((document.getElementById('ctl00_contentBody_TwentyOnePointAudit_pAchieved" + id + "').value) * (document.getElementById('ctl00_contentBody_TwentyOnePointAudit_pWeight" + id + "').innerText))); return false;";
        oAchievedCtl.Attributes.Add("onBlur", "Javascript:" + js);

        oScoreCtl.Text = (NullableStringToInteger(oWeightCtl.Text) * NullableStringToInteger(oAchievedCtl.Text)).ToString();
    }
    protected int CalcTotalScoreA(string id)
    {
        string Weight = ""; //Label
        string Achieved = ""; //Textbox
        //string Score = ""; //Label
        //string js = "";
        int value = 0; 

        Label oWeightCtl;
        DropDownList oAchievedCtl;
        //Label oScoreCtl;

        Control oCtlWeight = this.FindControl("pWeight" + id);
        oWeightCtl = (Label)oCtlWeight;
        Weight = oWeightCtl.Text.ToString();

        Control oCtlAchieved = this.FindControl("pAchieved" + id);
        oAchievedCtl = (DropDownList)oCtlAchieved;
        Achieved = oAchievedCtl.Text.ToString();

        if (Achieved != "")
        {
            value = Convert.ToInt32(Weight) * 5;
        }
        return value;
    }
    protected int CalcTotalScoreB(int id)
    {
        string Weight = ""; //Label
        string Achieved = ""; //Textbox
        double total = 0;

        switch (id)
        {
            case 01:
                foreach (string str in section01) {
                    Label oWeightCtl;
                    DropDownList oAchievedCtl;

                    Control oCtlWeight = this.FindControl("pWeight" + str);
                    oWeightCtl = (Label)oCtlWeight;
                    Weight = oWeightCtl.Text.ToString();

                    Control oCtlAchieved = this.FindControl("pAchieved" + str);
                    oAchievedCtl = (DropDownList)oCtlAchieved;
                    Achieved = oAchievedCtl.Text.ToString();

                    double temp = 0.0;
                    try
                    {
                        if (!String.IsNullOrEmpty(Achieved))
                        { temp = Convert.ToInt32(Weight) * Convert.ToInt32(Achieved); }
                        else
                        { temp = 0; }
                    }
                    catch { temp = 0; }
                    total += temp;
                }
                    break;
                case 02:
                foreach (string str in section02) {
                    Label oWeightCtl;
                    DropDownList oAchievedCtl;

                    Control oCtlWeight = this.FindControl("pWeight" + str);
                    oWeightCtl = (Label)oCtlWeight;
                    Weight = oWeightCtl.Text.ToString();

                    Control oCtlAchieved = this.FindControl("pAchieved" + str);
                    oAchievedCtl = (DropDownList)oCtlAchieved;
                    Achieved = oAchievedCtl.Text.ToString();

                    double temp = 0.0;
                    try
                    {
                        if (!String.IsNullOrEmpty(Achieved))
                        { temp = Convert.ToInt32(Weight) * Convert.ToInt32(Achieved); }
                        else
                        { temp = 0; }
                    }
                    catch { temp = 0; }
                    total += temp;
                }
                    break;
                case 03:
                    foreach (string str in section03)
                    {
                        Label oWeightCtl;
                        DropDownList oAchievedCtl;

                        Control oCtlWeight = this.FindControl("pWeight" + str);
                        oWeightCtl = (Label)oCtlWeight;
                        Weight = oWeightCtl.Text.ToString();

                        Control oCtlAchieved = this.FindControl("pAchieved" + str);
                        oAchievedCtl = (DropDownList)oCtlAchieved;
                        Achieved = oAchievedCtl.Text.ToString();

                        double temp = 0.0;
                        try
                        {
                            if (!String.IsNullOrEmpty(Achieved))
                            { temp = Convert.ToInt32(Weight) * Convert.ToInt32(Achieved); }
                            else
                            { temp = 0; }
                        }
                        catch { temp = 0; }
                        total += temp;
                    }
                    break;
                case 04:
                    foreach (string str in section04)
                    {
                        Label oWeightCtl;
                        DropDownList oAchievedCtl;

                        Control oCtlWeight = this.FindControl("pWeight" + str);
                        oWeightCtl = (Label)oCtlWeight;
                        Weight = oWeightCtl.Text.ToString();

                        Control oCtlAchieved = this.FindControl("pAchieved" + str);
                        oAchievedCtl = (DropDownList)oCtlAchieved;
                        Achieved = oAchievedCtl.Text.ToString();

                        double temp = 0.0;
                        try
                        {
                            if (!String.IsNullOrEmpty(Achieved))
                            { temp = Convert.ToInt32(Weight) * Convert.ToInt32(Achieved); }
                            else
                            { temp = 0; }
                        }
                        catch { temp = 0; }
                        total += temp;
                    }
                    break;
                case 05:
                    foreach (string str in section05)
                    {
                        Label oWeightCtl;
                        DropDownList oAchievedCtl;

                        Control oCtlWeight = this.FindControl("pWeight" + str);
                        oWeightCtl = (Label)oCtlWeight;
                        Weight = oWeightCtl.Text.ToString();

                        Control oCtlAchieved = this.FindControl("pAchieved" + str);
                        oAchievedCtl = (DropDownList)oCtlAchieved;
                        Achieved = oAchievedCtl.Text.ToString();

                        double temp = 0.0;
                        try
                        {
                            if (!String.IsNullOrEmpty(Achieved))
                            { temp = Convert.ToInt32(Weight) * Convert.ToInt32(Achieved); }
                            else
                            { temp = 0; }
                        }
                        catch { temp = 0; }
                        total += temp;
                    }
                    break;
                case 06:
                    foreach (string str in section06)
                    {
                        Label oWeightCtl;
                        DropDownList oAchievedCtl;

                        Control oCtlWeight = this.FindControl("pWeight" + str);
                        oWeightCtl = (Label)oCtlWeight;
                        Weight = oWeightCtl.Text.ToString();

                        Control oCtlAchieved = this.FindControl("pAchieved" + str);
                        oAchievedCtl = (DropDownList)oCtlAchieved;
                        Achieved = oAchievedCtl.Text.ToString();

                        double temp = 0.0;
                        try
                        {
                            if (!String.IsNullOrEmpty(Achieved))
                            { temp = Convert.ToInt32(Weight) * Convert.ToInt32(Achieved); }
                            else
                            { temp = 0; }
                        }
                        catch { temp = 0; }
                        total += temp;
                    }
                    break;
                case 07:
                    foreach (string str in section07)
                    {
                        Label oWeightCtl;
                        DropDownList oAchievedCtl;

                        Control oCtlWeight = this.FindControl("pWeight" + str);
                        oWeightCtl = (Label)oCtlWeight;
                        Weight = oWeightCtl.Text.ToString();

                        Control oCtlAchieved = this.FindControl("pAchieved" + str);
                        oAchievedCtl = (DropDownList)oCtlAchieved;
                        Achieved = oAchievedCtl.Text.ToString();

                        double temp = 0.0;
                        try
                        {
                            if (!String.IsNullOrEmpty(Achieved))
                            { temp = Convert.ToInt32(Weight) * Convert.ToInt32(Achieved); }
                            else
                            { temp = 0; }
                        }
                        catch { temp = 0; }
                        total += temp;
                    }
                    break;
                case 08:
                    foreach (string str in section08)
                    {
                        Label oWeightCtl;
                        DropDownList oAchievedCtl;

                        Control oCtlWeight = this.FindControl("pWeight" + str);
                        oWeightCtl = (Label)oCtlWeight;
                        Weight = oWeightCtl.Text.ToString();

                        Control oCtlAchieved = this.FindControl("pAchieved" + str);
                        oAchievedCtl = (DropDownList)oCtlAchieved;
                        Achieved = oAchievedCtl.Text.ToString();

                        double temp = 0.0;
                        try
                        {
                            if (!String.IsNullOrEmpty(Achieved))
                            { temp = Convert.ToInt32(Weight) * Convert.ToInt32(Achieved); }
                            else
                            { temp = 0; }
                        }
                        catch { temp = 0; }
                        total += temp;
                    }
                    break;
                case 09:
                    foreach (string str in section09)
                    {
                        Label oWeightCtl;
                        DropDownList oAchievedCtl;

                        Control oCtlWeight = this.FindControl("pWeight" + str);
                        oWeightCtl = (Label)oCtlWeight;
                        Weight = oWeightCtl.Text.ToString();

                        Control oCtlAchieved = this.FindControl("pAchieved" + str);
                        oAchievedCtl = (DropDownList)oCtlAchieved;
                        Achieved = oAchievedCtl.Text.ToString();

                        double temp = 0.0;
                        try
                        {
                            if (!String.IsNullOrEmpty(Achieved))
                            { temp = Convert.ToInt32(Weight) * Convert.ToInt32(Achieved); }
                            else
                            { temp = 0; }
                        }
                        catch { temp = 0; }
                        total += temp;
                    }
                    break;
                case 10:
                    foreach (string str in section10)
                    {
                        Label oWeightCtl;
                        DropDownList oAchievedCtl;

                        Control oCtlWeight = this.FindControl("pWeight" + str);
                        oWeightCtl = (Label)oCtlWeight;
                        Weight = oWeightCtl.Text.ToString();

                        Control oCtlAchieved = this.FindControl("pAchieved" + str);
                        oAchievedCtl = (DropDownList)oCtlAchieved;
                        Achieved = oAchievedCtl.Text.ToString();

                        double temp = 0.0;
                        try
                        {
                            if (!String.IsNullOrEmpty(Achieved))
                            { temp = Convert.ToInt32(Weight) * Convert.ToInt32(Achieved); }
                            else
                            { temp = 0; }
                        }
                        catch { temp = 0; }
                        total += temp;
                    }
                    break;
                case 11:
                    foreach (string str in section11)
                    {
                        Label oWeightCtl;
                        DropDownList oAchievedCtl;

                        Control oCtlWeight = this.FindControl("pWeight" + str);
                        oWeightCtl = (Label)oCtlWeight;
                        Weight = oWeightCtl.Text.ToString();

                        Control oCtlAchieved = this.FindControl("pAchieved" + str);
                        oAchievedCtl = (DropDownList)oCtlAchieved;
                        Achieved = oAchievedCtl.Text.ToString();

                        double temp = 0.0;
                        try
                        {
                            if (!String.IsNullOrEmpty(Achieved))
                            { temp = Convert.ToInt32(Weight) * Convert.ToInt32(Achieved); }
                            else
                            { temp = 0; }
                        }
                        catch { temp = 0; }
                        total += temp;
                    }
                    break;
                case 12:
                    foreach (string str in section12)
                    {
                        Label oWeightCtl;
                        DropDownList oAchievedCtl;

                        Control oCtlWeight = this.FindControl("pWeight" + str);
                        oWeightCtl = (Label)oCtlWeight;
                        Weight = oWeightCtl.Text.ToString();

                        Control oCtlAchieved = this.FindControl("pAchieved" + str);
                        oAchievedCtl = (DropDownList)oCtlAchieved;
                        Achieved = oAchievedCtl.Text.ToString();

                        double temp = 0.0;
                        try
                        {
                            if (!String.IsNullOrEmpty(Achieved))
                            { temp = Convert.ToInt32(Weight) * Convert.ToInt32(Achieved); }
                            else
                            { temp = 0; }
                        }
                        catch { temp = 0; }
                        total += temp;
                    }
                    break;
                case 13:
                    foreach (string str in section13)
                    {
                        Label oWeightCtl;
                        DropDownList oAchievedCtl;

                        Control oCtlWeight = this.FindControl("pWeight" + str);
                        oWeightCtl = (Label)oCtlWeight;
                        Weight = oWeightCtl.Text.ToString();

                        Control oCtlAchieved = this.FindControl("pAchieved" + str);
                        oAchievedCtl = (DropDownList)oCtlAchieved;
                        Achieved = oAchievedCtl.Text.ToString();

                        double temp = 0.0;
                        try
                        {
                            if (!String.IsNullOrEmpty(Achieved))
                            { temp = Convert.ToInt32(Weight) * Convert.ToInt32(Achieved); }
                            else
                            { temp = 0; }
                        }
                        catch { temp = 0; }
                        total += temp;
                    }
                    break;
                case 14:
                    foreach (string str in section14)
                    {
                        Label oWeightCtl;
                        DropDownList oAchievedCtl;

                        Control oCtlWeight = this.FindControl("pWeight" + str);
                        oWeightCtl = (Label)oCtlWeight;
                        Weight = oWeightCtl.Text.ToString();

                        Control oCtlAchieved = this.FindControl("pAchieved" + str);
                        oAchievedCtl = (DropDownList)oCtlAchieved;
                        Achieved = oAchievedCtl.Text.ToString();

                        double temp = 0.0;
                        try
                        {
                            if (!String.IsNullOrEmpty(Achieved))
                            { temp = Convert.ToInt32(Weight) * Convert.ToInt32(Achieved); }
                            else
                            { temp = 0; }
                        }
                        catch { temp = 0; }
                        total += temp;
                    }
                    break;
                case 15:
                    foreach (string str in section15)
                    {
                        Label oWeightCtl;
                        DropDownList oAchievedCtl;

                        Control oCtlWeight = this.FindControl("pWeight" + str);
                        oWeightCtl = (Label)oCtlWeight;
                        Weight = oWeightCtl.Text.ToString();

                        Control oCtlAchieved = this.FindControl("pAchieved" + str);
                        oAchievedCtl = (DropDownList)oCtlAchieved;
                        Achieved = oAchievedCtl.Text.ToString();

                        double temp = 0.0;
                        try
                        {
                            if (!String.IsNullOrEmpty(Achieved))
                            { temp = Convert.ToInt32(Weight) * Convert.ToInt32(Achieved); }
                            else
                            { temp = 0; }
                        }
                        catch { temp = 0; }
                        total += temp;
                    }
                    break;
                case 16:
                    foreach (string str in section16)
                    {
                        Label oWeightCtl;
                        DropDownList oAchievedCtl;

                        Control oCtlWeight = this.FindControl("pWeight" + str);
                        oWeightCtl = (Label)oCtlWeight;
                        Weight = oWeightCtl.Text.ToString();

                        Control oCtlAchieved = this.FindControl("pAchieved" + str);
                        oAchievedCtl = (DropDownList)oCtlAchieved;
                        Achieved = oAchievedCtl.Text.ToString();

                        double temp = 0.0;
                        try
                        {
                            if (!String.IsNullOrEmpty(Achieved))
                            { temp = Convert.ToInt32(Weight) * Convert.ToInt32(Achieved); }
                            else
                            { temp = 0; }
                        }
                        catch { temp = 0; }
                        total += temp;
                    }
                    break;
                case 17:
                    foreach (string str in section17)
                    {
                        Label oWeightCtl;
                        DropDownList oAchievedCtl;

                        Control oCtlWeight = this.FindControl("pWeight" + str);
                        oWeightCtl = (Label)oCtlWeight;
                        Weight = oWeightCtl.Text.ToString();

                        Control oCtlAchieved = this.FindControl("pAchieved" + str);
                        oAchievedCtl = (DropDownList)oCtlAchieved;
                        Achieved = oAchievedCtl.Text.ToString();

                        double temp = 0.0;
                        try
                        {
                            if (!String.IsNullOrEmpty(Achieved))
                            { temp = Convert.ToInt32(Weight) * Convert.ToInt32(Achieved); }
                            else
                            { temp = 0; }
                        }
                        catch { temp = 0; }
                        total += temp;
                    }
                    break;
                case 18:
                    foreach (string str in section18)
                    {
                        Label oWeightCtl;
                        DropDownList oAchievedCtl;

                        Control oCtlWeight = this.FindControl("pWeight" + str);
                        oWeightCtl = (Label)oCtlWeight;
                        Weight = oWeightCtl.Text.ToString();

                        Control oCtlAchieved = this.FindControl("pAchieved" + str);
                        oAchievedCtl = (DropDownList)oCtlAchieved;
                        Achieved = oAchievedCtl.Text.ToString();

                        double temp = 0.0;
                        try
                        {
                            if (!String.IsNullOrEmpty(Achieved))
                            { temp = Convert.ToInt32(Weight) * Convert.ToInt32(Achieved); }
                            else
                            { temp = 0; }
                        }
                        catch { temp = 0; }
                        total += temp;
                    }
                    break;
                case 19:
                    foreach (string str in section19)
                    {
                        Label oWeightCtl;
                        DropDownList oAchievedCtl;

                        Control oCtlWeight = this.FindControl("pWeight" + str);
                        oWeightCtl = (Label)oCtlWeight;
                        Weight = oWeightCtl.Text.ToString();

                        Control oCtlAchieved = this.FindControl("pAchieved" + str);
                        oAchievedCtl = (DropDownList)oCtlAchieved;
                        Achieved = oAchievedCtl.Text.ToString();

                        double temp = 0.0;
                        try
                        {
                            if (!String.IsNullOrEmpty(Achieved))
                            { temp = Convert.ToInt32(Weight) * Convert.ToInt32(Achieved); }
                            else
                            { temp = 0; }
                        }
                        catch { temp = 0; }
                        total += temp;
                    }
                    break;
                case 20:
                    foreach (string str in section20)
                    {
                        Label oWeightCtl;
                        DropDownList oAchievedCtl;

                        Control oCtlWeight = this.FindControl("pWeight" + str);
                        oWeightCtl = (Label)oCtlWeight;
                        Weight = oWeightCtl.Text.ToString();

                        Control oCtlAchieved = this.FindControl("pAchieved" + str);
                        oAchievedCtl = (DropDownList)oCtlAchieved;
                        Achieved = oAchievedCtl.Text.ToString();

                        double temp = 0.0;
                        try
                        {
                            if (!String.IsNullOrEmpty(Achieved))
                            { temp = Convert.ToInt32(Weight) * Convert.ToInt32(Achieved); }
                            else
                            { temp = 0; }
                        }
                        catch { temp = 0; }
                        total += temp;
                    }
                    break;
                case 21:
                    foreach (string str in section21)
                    {
                        Label oWeightCtl;
                        DropDownList oAchievedCtl;

                        Control oCtlWeight = this.FindControl("pWeight" + str);
                        oWeightCtl = (Label)oCtlWeight;
                        Weight = oWeightCtl.Text.ToString();

                        Control oCtlAchieved = this.FindControl("pAchieved" + str);
                        oAchievedCtl = (DropDownList)oCtlAchieved;
                        Achieved = oAchievedCtl.Text.ToString();

                        double temp = 0.0;
                        try
                        {
                            if (!String.IsNullOrEmpty(Achieved))
                            { temp = Convert.ToInt32(Weight) * Convert.ToInt32(Achieved); }
                            else
                            { temp = 0; }
                        }
                        catch { temp = 0; }
                        total += temp;
                    }
                    break;
            default:
                break;
        }
        return Convert.ToInt32(total);
    }
    protected void CleanUp_Load()
    {
        foreach (string str in section01) { CleanUp(str); }
        foreach (string str in section02) { CleanUp(str); }
        foreach (string str in section03) { CleanUp(str); }
        foreach (string str in section04) { CleanUp(str); }
        foreach (string str in section05) { CleanUp(str); }
        foreach (string str in section06) { CleanUp(str); }
        foreach (string str in section07) { CleanUp(str); }
        foreach (string str in section08) { CleanUp(str); }
        foreach (string str in section09) { CleanUp(str); }
        foreach (string str in section10) { CleanUp(str); }
        foreach (string str in section11) { CleanUp(str); }
        foreach (string str in section12) { CleanUp(str); }
        foreach (string str in section13) { CleanUp(str); }
        foreach (string str in section14) { CleanUp(str); }
        foreach (string str in section15) { CleanUp(str); }
        foreach (string str in section16) { CleanUp(str); }
        foreach (string str in section17) { CleanUp(str); }
        foreach (string str in section18) { CleanUp(str); }
        foreach (string str in section19) { CleanUp(str); }
        foreach (string str in section20) { CleanUp(str); }
        foreach (string str in section21) { CleanUp(str); }
        lblSave.Text = "";
        UpdatePanel1.Visible = false;

        pAAScore1a.Text = "0";
        pAAScore2a.Text = "0";
        pAAScore3a.Text = "0";
        pAAScore4a.Text = "0";
        pAAScore5a.Text = "0";
        pAAScore6a.Text = "0";
        pAAScore7a.Text = "0";
        pAAScore8a.Text = "0";
        pAAScore9a.Text = "0";
        pAAScore10a.Text = "0";
        pAAScore11a.Text = "0";
        pAAScore12a.Text = "0";
        pAAScore13a.Text = "0";
        pAAScore14a.Text = "0";
        pAAScore15a.Text = "0";
        pAAScore16a.Text = "0";
        pAAScore17a.Text = "0";
        pAAScore18a.Text = "0";
        pAAScore19a.Text = "0";
        pAAScore20a.Text = "0";
        pAAScore21a.Text = "0";
        pAAScore1b.Text = "0";
        pAAScore2b.Text = "0";
        pAAScore3b.Text = "0";
        pAAScore4b.Text = "0";
        pAAScore5b.Text = "0";
        pAAScore6b.Text = "0";
        pAAScore7b.Text = "0";
        pAAScore8b.Text = "0";
        pAAScore9b.Text = "0";
        pAAScore10b.Text = "0";
        pAAScore11b.Text = "0";
        pAAScore12b.Text = "0";
        pAAScore13b.Text = "0";
        pAAScore14b.Text = "0";
        pAAScore15b.Text = "0";
        pAAScore16b.Text = "0";
        pAAScore17b.Text = "0";
        pAAScore18b.Text = "0";
        pAAScore19b.Text = "0";
        pAAScore20b.Text = "0";
        pAAScore21b.Text = "0";
        pAAScore1c.Text = "0%";
        pAAScore2c.Text = "0%";
        pAAScore3c.Text = "0%";
        pAAScore4c.Text = "0%";
        pAAScore5c.Text = "0%";
        pAAScore6c.Text = "0%";
        pAAScore7c.Text = "0%";
        pAAScore8c.Text = "0%";
        pAAScore9c.Text = "0%";
        pAAScore10c.Text = "0%";
        pAAScore11c.Text = "0%";
        pAAScore12c.Text = "0%";
        pAAScore13c.Text = "0%";
        pAAScore14c.Text = "0%";
        pAAScore15c.Text = "0%";
        pAAScore16c.Text = "0%";
        pAAScore17c.Text = "0%";
        pAAScore18c.Text = "0%";
        pAAScore19c.Text = "0%";
        pAAScore20c.Text = "0%";
        pAAScore21c.Text = "0%";
        pAAScoreTotala.Text = "0";
        pAAScoreTotalb.Text = "0";
        pAAScoreTotalc.Text = "0%";

    }
    protected void CleanUp(string id)
    {
        DropDownList oAchievedCtl;
        Label oScoreCtl;
        TextBox oObservationCtl;

        Control oCtlAchieved = this.FindControl("pAchieved" + id);
        oAchievedCtl = (DropDownList)oCtlAchieved;
        oAchievedCtl.Text = "";

        Control oCtlScore = this.FindControl("pScore" + id);
        oScoreCtl = (Label)oCtlScore;
        oScoreCtl.Text = "";

        Control oCtlObservation = this.FindControl("pObservation" + id);
        oObservationCtl = (TextBox)oCtlObservation;
        oObservationCtl.Text = "";
    }
    public int? NullableStringToInteger(string input)
    {
        if (String.IsNullOrEmpty(input)) { return null; }
        else { return Convert.ToInt32(input); }
    }

    protected void Button2_Click1(object sender, EventArgs e) {
        int errorCount = 0;
        try
        {
            if (String.IsNullOrEmpty(ddlCompanies.SelectedItem.Text.ToString())) { errorCount++; };
            if (String.IsNullOrEmpty(ddlSites.SelectedItem.Text.ToString())) { errorCount++; };
        }
        catch (Exception ex)
        {
            Elmah.ErrorSignal.FromContext(Context).Raise(ex);
            errorCount = 1;
            PopupWindow pcWindow = new PopupWindow("You haven't selected a company and/or site. If no site exists for your company when it should, please contact the AUA Contractor Services Team.");
            pcWindow.FooterText = "";
            pcWindow.ShowOnPageLoad = true;
            pcWindow.Modal = true;
            ASPxPopupControl1.Windows.Add(pcWindow);
        }

        if (errorCount > 0)
        {
            PopupWindow pcWindow = new PopupWindow("You haven't selected a company and/or site. If no site exists for your company when it should, please contact the AUA Contractor Services Team.");
            pcWindow.FooterText = "";
            pcWindow.ShowOnPageLoad = true;
            pcWindow.Modal = true;
            ASPxPopupControl1.Windows.Add(pcWindow);
        }
        else
        {
            TwentyOnePointAudit_Init();
        }
    } // View Button
    protected void btnSave_Click(object sender, EventArgs e){ TwentyOnePointAudit_Save(); } // Save Button

    private void TwentyOnePointAudit_Init()
    {
        switch (auth.RoleId)
        {
            case 1: //Reader - Read Only
                TwentyOnePointAudit_ReadOnly(true);
                CleanUp_Load();
                TwentyOnePointAudit_Load((int)ddlCompanies.SelectedItem.Value, (int)ddlSites.SelectedItem.Value, Convert.ToInt32(ddlMonth.SelectedValue), Convert.ToInt32(ddlYear.SelectedValue));
                if (ddlMonth.SelectedItem.Text != "Alcoa Annual Review")
                {
                    btnSave.Visible = true;
                    btnSave.Enabled = false;
                }
                else
                {
                    btnSave.Visible = true;
                    btnSave.Enabled = true;
                }
                break;
            case 2: //Contractor - Edit Own - Can't edit previous quarters
                //TODO: contractor var.
                //if selected (current qtr > selected qtr) || "alcoa annual review", TwentyOnePointAudit_ReadOnly(true) else TwentyOnePointAudit_ReadOnly(false)
                CleanUp_Load();
                TwentyOnePointAudit_Load((int)ddlCompanies.SelectedItem.Value, (int)ddlSites.SelectedItem.Value, Convert.ToInt32(ddlMonth.SelectedValue), Convert.ToInt32(ddlYear.SelectedValue));
                break;
            case 3: //Administrator - Full Access
                CleanUp_Load();
                TwentyOnePointAudit_Load((int)ddlCompanies.SelectedItem.Value, (int)ddlSites.SelectedItem.Value, Convert.ToInt32(ddlMonth.SelectedValue), Convert.ToInt32(ddlYear.SelectedValue));
                break;
            default: //Access Denied.
                break;
        }
    }
    private void TwentyOnePointAudit_Load(int CompanyId, int SiteId, int QtrId, int Year)
    {
        //System.Threading.Thread.Sleep(500);
        SessionHandler.spVar_CompanyId = CompanyId.ToString();
        SessionHandler.spVar_SiteId = SiteId.ToString();
        SessionHandler.spVar_QtrId = QtrId.ToString();
        SessionHandler.spVar_Year = Year.ToString();
        SessionHandler.spVar_TopaId = "";

        TwentyOnePointAuditService topaService = new TwentyOnePointAuditService();
        TList<KaiZen.CSMS.Entities.TwentyOnePointAudit> topaList = topaService.GetAll();
        if (topaList.Count != 0)
        {
            TList<KaiZen.CSMS.Entities.TwentyOnePointAudit> _topaList = topaList.FindAll( //Find KPI We Need
                delegate(KaiZen.CSMS.Entities.TwentyOnePointAudit topa)
                {
                    return
                        topa.CompanyId == CompanyId &&
                        topa.SiteId == SiteId &&
                        topa.QtrId == QtrId &&
                        topa.Year == Year;
                }
            );

            if (_topaList.Count != 0)
            {
                foreach (KaiZen.CSMS.Entities.TwentyOnePointAudit topa in _topaList)
                {
                    SessionHandler.spVar_TopaId = topa.Id.ToString();
                    UsersService usersService = new UsersService();
                    Users userEntity;
                    userEntity = usersService.GetByUserId(topa.CreatedbyUserId);
                    userEntity = usersService.GetByUserId(topa.ModifiedbyUserId);

                    #region  Section 01
                    if (topa.Point01Id.HasValue) // Section #01
                    {
                       SessionHandler.spVar_TopaId_01 = topa.Point01Id.Value.ToString();
                        TwentyOnePointAudit01Service topaService01 = new TwentyOnePointAudit01Service();
                        KaiZen.CSMS.Entities.TwentyOnePointAudit01 topa01 = topaService01.GetById(topa.Point01Id.Value);

                        foreach (string str in section01)
                        {
                            try
                            {
                                string name = "Achieved" + str;
                                DropDownList ddlAchieved = (DropDownList)this.FindControl("p" + name);

                                Type type = topa01.GetType();
                                PropertyInfo valueInfo = type.GetProperty(name);

                                object propertyValue = valueInfo.GetValue(topa01, null);
                                ddlAchieved.Text = propertyValue.ToString();
                            }
                            catch
                            { }

                            try
                            {
                                string name = "Observation" + str;
                                TextBox tbObservation = (TextBox)this.FindControl("p" + name);

                                Type type = topa01.GetType();
                                PropertyInfo valueInfo = type.GetProperty(name);

                                object propertyValue = valueInfo.GetValue(topa01, null);
                                tbObservation.Text = propertyValue.ToString();
                            }
                            catch
                            { }
                        }
                    }
                    #endregion
                    #region  Section 02
                    if (topa.Point02Id.HasValue) // Section #02
                    {
                        SessionHandler.spVar_TopaId_02 = topa.Point02Id.Value.ToString();
                        TwentyOnePointAudit02Service topaService02 = new TwentyOnePointAudit02Service();
                        KaiZen.CSMS.Entities.TwentyOnePointAudit02 topa02 = topaService02.GetById(topa.Point02Id.Value);

                        foreach (string str in section02)
                        {
                            try
                            {
                                string name = "Achieved" + str;
                                DropDownList ddlAchieved = (DropDownList)this.FindControl("p" + name);

                                Type type = topa02.GetType();
                                PropertyInfo valueInfo = type.GetProperty(name);

                                object propertyValue = valueInfo.GetValue(topa02, null);
                                ddlAchieved.Text = propertyValue.ToString();
                            }
                            catch
                            { }

                            try
                            {
                                string name = "Observation" + str;
                                TextBox tbObservation = (TextBox)this.FindControl("p" + name);

                                Type type = topa02.GetType();
                                PropertyInfo valueInfo = type.GetProperty(name);

                                object propertyValue = valueInfo.GetValue(topa02, null);
                                tbObservation.Text = propertyValue.ToString();
                            }
                            catch
                            { }
                        }
                    }
                    #endregion
                    #region  Section 03
                    if (topa.Point03Id.HasValue) // Section #03
                    {
                        SessionHandler.spVar_TopaId_03 = topa.Point03Id.Value.ToString();
                        TwentyOnePointAudit03Service topaService03 = new TwentyOnePointAudit03Service();
                        KaiZen.CSMS.Entities.TwentyOnePointAudit03 topa03 = topaService03.GetById(topa.Point03Id.Value);
                        foreach (string str in section03)
                        {
                            try
                            {
                                string name = "Achieved" + str;
                                DropDownList ddlAchieved = (DropDownList)this.FindControl("p" + name);

                                Type type = topa03.GetType();
                                PropertyInfo valueInfo = type.GetProperty(name);

                                object propertyValue = valueInfo.GetValue(topa03, null);
                                ddlAchieved.Text = propertyValue.ToString();
                            }
                            catch
                            { }

                            try
                            {
                                string name = "Observation" + str;
                                TextBox tbObservation = (TextBox)this.FindControl("p" + name);

                                Type type = topa03.GetType();
                                PropertyInfo valueInfo = type.GetProperty(name);

                                object propertyValue = valueInfo.GetValue(topa03, null);
                                tbObservation.Text = propertyValue.ToString();
                            }
                            catch
                            { }
                        }
                    }
                    #endregion
                    #region  Section 04
                    if (topa.Point04Id.HasValue) // Section #04
                    {
                        SessionHandler.spVar_TopaId_04 = topa.Point04Id.Value.ToString();
                        TwentyOnePointAudit04Service topaService04 = new TwentyOnePointAudit04Service();
                        KaiZen.CSMS.Entities.TwentyOnePointAudit04 topa04 = topaService04.GetById(topa.Point04Id.Value);
                        foreach (string str in section04)
                        {
                            try
                            {
                                string name = "Achieved" + str;
                                DropDownList ddlAchieved = (DropDownList)this.FindControl("p" + name);

                                Type type = topa04.GetType();
                                PropertyInfo valueInfo = type.GetProperty(name);

                                object propertyValue = valueInfo.GetValue(topa04, null);
                                ddlAchieved.Text = propertyValue.ToString();
                            }
                            catch
                            { }

                            try
                            {
                                string name = "Observation" + str;
                                TextBox tbObservation = (TextBox)this.FindControl("p" + name);

                                Type type = topa04.GetType();
                                PropertyInfo valueInfo = type.GetProperty(name);

                                object propertyValue = valueInfo.GetValue(topa04, null);
                                tbObservation.Text = propertyValue.ToString();
                            }
                            catch
                            { }
                        }
                    }
                    #endregion
                    #region  Section 05
                    if (topa.Point05Id.HasValue) // Section #05
                    {
                        SessionHandler.spVar_TopaId_05 = topa.Point05Id.Value.ToString();
                        TwentyOnePointAudit05Service topaService05 = new TwentyOnePointAudit05Service();
                        KaiZen.CSMS.Entities.TwentyOnePointAudit05 topa05 = topaService05.GetById(topa.Point05Id.Value);
                        foreach (string str in section05)
                        {
                            try
                            {
                                string name = "Achieved" + str;
                                DropDownList ddlAchieved = (DropDownList)this.FindControl("p" + name);

                                Type type = topa05.GetType();
                                PropertyInfo valueInfo = type.GetProperty(name);

                                object propertyValue = valueInfo.GetValue(topa05, null);
                                ddlAchieved.Text = propertyValue.ToString();
                            }
                            catch
                            { }

                            try
                            {
                                string name = "Observation" + str;
                                TextBox tbObservation = (TextBox)this.FindControl("p" + name);

                                Type type = topa05.GetType();
                                PropertyInfo valueInfo = type.GetProperty(name);

                                object propertyValue = valueInfo.GetValue(topa05, null);
                                tbObservation.Text = propertyValue.ToString();
                            }
                            catch
                            { }
                        }
                    }
                    #endregion
                    #region  Section 06
                    if (topa.Point06Id.HasValue) // Section #06
                    {
                        SessionHandler.spVar_TopaId_06 = topa.Point06Id.Value.ToString();
                        TwentyOnePointAudit06Service topaService06 = new TwentyOnePointAudit06Service();
                        KaiZen.CSMS.Entities.TwentyOnePointAudit06 topa06 = topaService06.GetById(topa.Point06Id.Value);
                        foreach (string str in section06)
                        {
                            try
                            {
                                string name = "Achieved" + str;
                                DropDownList ddlAchieved = (DropDownList)this.FindControl("p" + name);

                                Type type = topa06.GetType();
                                PropertyInfo valueInfo = type.GetProperty(name);

                                object propertyValue = valueInfo.GetValue(topa06, null);
                                ddlAchieved.Text = propertyValue.ToString();
                            }
                            catch
                            { }

                            try
                            {
                                string name = "Observation" + str;
                                TextBox tbObservation = (TextBox)this.FindControl("p" + name);

                                Type type = topa06.GetType();
                                PropertyInfo valueInfo = type.GetProperty(name);

                                object propertyValue = valueInfo.GetValue(topa06, null);
                                tbObservation.Text = propertyValue.ToString();
                            }
                            catch
                            { }
                        }
                    }
                    #endregion
                    #region  Section 07
                    if (topa.Point07Id.HasValue) // Section #07
                    {
                        SessionHandler.spVar_TopaId_07 = topa.Point07Id.Value.ToString();
                        TwentyOnePointAudit07Service topaService07 = new TwentyOnePointAudit07Service();
                        KaiZen.CSMS.Entities.TwentyOnePointAudit07 topa07 = topaService07.GetById(topa.Point07Id.Value);
                        foreach (string str in section07)
                        {
                            try
                            {
                                string name = "Achieved" + str;
                                DropDownList ddlAchieved = (DropDownList)this.FindControl("p" + name);

                                Type type = topa07.GetType();
                                PropertyInfo valueInfo = type.GetProperty(name);

                                object propertyValue = valueInfo.GetValue(topa07, null);
                                ddlAchieved.Text = propertyValue.ToString();
                            }
                            catch
                            { }

                            try
                            {
                                string name = "Observation" + str;
                                TextBox tbObservation = (TextBox)this.FindControl("p" + name);

                                Type type = topa07.GetType();
                                PropertyInfo valueInfo = type.GetProperty(name);

                                object propertyValue = valueInfo.GetValue(topa07, null);
                                tbObservation.Text = propertyValue.ToString();
                            }
                            catch
                            { }
                        }
                    }
                    #endregion
                    #region  Section 08
                    if (topa.Point08Id.HasValue) // Section #08
                    {
                        SessionHandler.spVar_TopaId_08 = topa.Point08Id.Value.ToString();
                        TwentyOnePointAudit08Service topaService08 = new TwentyOnePointAudit08Service();
                        KaiZen.CSMS.Entities.TwentyOnePointAudit08 topa08 = topaService08.GetById(topa.Point08Id.Value);
                        foreach (string str in section08)
                        {
                            try
                            {
                                string name = "Achieved" + str;
                                DropDownList ddlAchieved = (DropDownList)this.FindControl("p" + name);

                                Type type = topa08.GetType();
                                PropertyInfo valueInfo = type.GetProperty(name);

                                object propertyValue = valueInfo.GetValue(topa08, null);
                                ddlAchieved.Text = propertyValue.ToString();
                            }
                            catch
                            { }

                            try
                            {
                                string name = "Observation" + str;
                                TextBox tbObservation = (TextBox)this.FindControl("p" + name);

                                Type type = topa08.GetType();
                                PropertyInfo valueInfo = type.GetProperty(name);

                                object propertyValue = valueInfo.GetValue(topa08, null);
                                tbObservation.Text = propertyValue.ToString();
                            }
                            catch
                            { }
                        }
                    }
                    #endregion
                    #region  Section 09
                    if (topa.Point09Id.HasValue) // Section #09
                    {
                        SessionHandler.spVar_TopaId_09 = topa.Point09Id.Value.ToString();
                        TwentyOnePointAudit09Service topaService09 = new TwentyOnePointAudit09Service();
                        KaiZen.CSMS.Entities.TwentyOnePointAudit09 topa09 = topaService09.GetById(topa.Point09Id.Value);
                        foreach (string str in section09)
                        {
                            try
                            {
                                string name = "Achieved" + str;
                                DropDownList ddlAchieved = (DropDownList)this.FindControl("p" + name);

                                Type type = topa09.GetType();
                                PropertyInfo valueInfo = type.GetProperty(name);

                                object propertyValue = valueInfo.GetValue(topa09, null);
                                ddlAchieved.Text = propertyValue.ToString();
                            }
                            catch
                            { }

                            try
                            {
                                string name = "Observation" + str;
                                TextBox tbObservation = (TextBox)this.FindControl("p" + name);

                                Type type = topa09.GetType();
                                PropertyInfo valueInfo = type.GetProperty(name);

                                object propertyValue = valueInfo.GetValue(topa09, null);
                                tbObservation.Text = propertyValue.ToString();
                            }
                            catch
                            { }
                        }
                    }
                    #endregion
                    #region  Section 10
                    if (topa.Point10Id.HasValue) // Section #10
                    {
                        SessionHandler.spVar_TopaId_10 = topa.Point10Id.Value.ToString();
                        TwentyOnePointAudit10Service topaService10 = new TwentyOnePointAudit10Service();
                        KaiZen.CSMS.Entities.TwentyOnePointAudit10 topa10 = topaService10.GetById(topa.Point10Id.Value);
                        foreach (string str in section10)
                        {
                            try
                            {
                                string name = "Achieved" + str;
                                DropDownList ddlAchieved = (DropDownList)this.FindControl("p" + name);

                                Type type = topa10.GetType();
                                PropertyInfo valueInfo = type.GetProperty(name);

                                object propertyValue = valueInfo.GetValue(topa10, null);
                                ddlAchieved.Text = propertyValue.ToString();
                            }
                            catch
                            { }

                            try
                            {
                                string name = "Observation" + str;
                                TextBox tbObservation = (TextBox)this.FindControl("p" + name);

                                Type type = topa10.GetType();
                                PropertyInfo valueInfo = type.GetProperty(name);

                                object propertyValue = valueInfo.GetValue(topa10, null);
                                tbObservation.Text = propertyValue.ToString();
                            }
                            catch
                            { }
                        }
                    }
                    #endregion
                    #region  Section 11
                    if (topa.Point11Id.HasValue) // Section #11
                    {
                        SessionHandler.spVar_TopaId_11 = topa.Point11Id.Value.ToString();
                        TwentyOnePointAudit11Service topaService11 = new TwentyOnePointAudit11Service();
                        KaiZen.CSMS.Entities.TwentyOnePointAudit11 topa11 = topaService11.GetById(topa.Point11Id.Value);
                        foreach (string str in section11)
                        {
                            try
                            {
                                string name = "Achieved" + str;
                                DropDownList ddlAchieved = (DropDownList)this.FindControl("p" + name);

                                Type type = topa11.GetType();
                                PropertyInfo valueInfo = type.GetProperty(name);

                                object propertyValue = valueInfo.GetValue(topa11, null);
                                ddlAchieved.Text = propertyValue.ToString();
                            }
                            catch
                            { }

                            try
                            {
                                string name = "Observation" + str;
                                TextBox tbObservation = (TextBox)this.FindControl("p" + name);

                                Type type = topa11.GetType();
                                PropertyInfo valueInfo = type.GetProperty(name);

                                object propertyValue = valueInfo.GetValue(topa11, null);
                                tbObservation.Text = propertyValue.ToString();
                            }
                            catch
                            { }
                        }
                    }
                    #endregion
                    #region  Section 12
                    if (topa.Point12Id.HasValue) // Section #12
                    {
                        SessionHandler.spVar_TopaId_12 = topa.Point12Id.Value.ToString();
                        TwentyOnePointAudit12Service topaService12 = new TwentyOnePointAudit12Service();
                        KaiZen.CSMS.Entities.TwentyOnePointAudit12 topa12 = topaService12.GetById(topa.Point12Id.Value);
                        foreach (string str in section12)
                        {
                            try
                            {
                                string name = "Achieved" + str;
                                DropDownList ddlAchieved = (DropDownList)this.FindControl("p" + name);

                                Type type = topa12.GetType();
                                PropertyInfo valueInfo = type.GetProperty(name);

                                object propertyValue = valueInfo.GetValue(topa12, null);
                                ddlAchieved.Text = propertyValue.ToString();
                            }
                            catch
                            { }

                            try
                            {
                                string name = "Observation" + str;
                                TextBox tbObservation = (TextBox)this.FindControl("p" + name);

                                Type type = topa12.GetType();
                                PropertyInfo valueInfo = type.GetProperty(name);

                                object propertyValue = valueInfo.GetValue(topa12, null);
                                tbObservation.Text = propertyValue.ToString();
                            }
                            catch
                            { }
                        }
                    }
                    #endregion
                    #region  Section 13
                    if (topa.Point13Id.HasValue) // Section #13
                    {
                        SessionHandler.spVar_TopaId_13 = topa.Point13Id.Value.ToString();
                        TwentyOnePointAudit13Service topaService13 = new TwentyOnePointAudit13Service();
                        KaiZen.CSMS.Entities.TwentyOnePointAudit13 topa13 = topaService13.GetById(topa.Point13Id.Value);
                        foreach (string str in section13)
                        {
                            try
                            {
                                string name = "Achieved" + str;
                                DropDownList ddlAchieved = (DropDownList)this.FindControl("p" + name);

                                Type type = topa13.GetType();
                                PropertyInfo valueInfo = type.GetProperty(name);

                                object propertyValue = valueInfo.GetValue(topa13, null);
                                ddlAchieved.Text = propertyValue.ToString();
                            }
                            catch
                            { }

                            try
                            {
                                string name = "Observation" + str;
                                TextBox tbObservation = (TextBox)this.FindControl("p" + name);

                                Type type = topa13.GetType();
                                PropertyInfo valueInfo = type.GetProperty(name);

                                object propertyValue = valueInfo.GetValue(topa13, null);
                                tbObservation.Text = propertyValue.ToString();
                            }
                            catch
                            { }
                        }
                    }
                    #endregion
                    #region  Section 14
                    if (topa.Point14Id.HasValue) // Section #14
                    {
                        SessionHandler.spVar_TopaId_14 = topa.Point14Id.Value.ToString();
                        TwentyOnePointAudit14Service topaService14 = new TwentyOnePointAudit14Service();
                        KaiZen.CSMS.Entities.TwentyOnePointAudit14 topa14 = topaService14.GetById(topa.Point14Id.Value);
                        foreach (string str in section14)
                        {
                            try
                            {
                                string name = "Achieved" + str;
                                DropDownList ddlAchieved = (DropDownList)this.FindControl("p" + name);

                                Type type = topa14.GetType();
                                PropertyInfo valueInfo = type.GetProperty(name);

                                object propertyValue = valueInfo.GetValue(topa14, null);
                                ddlAchieved.Text = propertyValue.ToString();
                            }
                            catch
                            { }

                            try
                            {
                                string name = "Observation" + str;
                                TextBox tbObservation = (TextBox)this.FindControl("p" + name);

                                Type type = topa14.GetType();
                                PropertyInfo valueInfo = type.GetProperty(name);

                                object propertyValue = valueInfo.GetValue(topa14, null);
                                tbObservation.Text = propertyValue.ToString();
                            }
                            catch
                            { }
                        }
                    }
                    #endregion
                    #region  Section 15
                    if (topa.Point15Id.HasValue) // Section #15
                    {
                        SessionHandler.spVar_TopaId_15 = topa.Point15Id.Value.ToString();
                        TwentyOnePointAudit15Service topaService15 = new TwentyOnePointAudit15Service();
                        KaiZen.CSMS.Entities.TwentyOnePointAudit15 topa15 = topaService15.GetById(topa.Point15Id.Value);
                        foreach (string str in section15)
                        {
                            try
                            {
                                string name = "Achieved" + str;
                                DropDownList ddlAchieved = (DropDownList)this.FindControl("p" + name);

                                Type type = topa15.GetType();
                                PropertyInfo valueInfo = type.GetProperty(name);

                                object propertyValue = valueInfo.GetValue(topa15, null);
                                ddlAchieved.Text = propertyValue.ToString();
                            }
                            catch
                            { }

                            try
                            {
                                string name = "Observation" + str;
                                TextBox tbObservation = (TextBox)this.FindControl("p" + name);

                                Type type = topa15.GetType();
                                PropertyInfo valueInfo = type.GetProperty(name);

                                object propertyValue = valueInfo.GetValue(topa15, null);
                                tbObservation.Text = propertyValue.ToString();
                            }
                            catch
                            { }
                        }
                    }
                    #endregion
                    #region  Section 16
                    if (topa.Point16Id.HasValue) // Section #16
                    {
                        SessionHandler.spVar_TopaId_16 = topa.Point16Id.Value.ToString();
                        TwentyOnePointAudit16Service topaService16 = new TwentyOnePointAudit16Service();
                        KaiZen.CSMS.Entities.TwentyOnePointAudit16 topa16 = topaService16.GetById(topa.Point16Id.Value);
                        foreach (string str in section16)
                        {
                            try
                            {
                                string name = "Achieved" + str;
                                DropDownList ddlAchieved = (DropDownList)this.FindControl("p" + name);

                                Type type = topa16.GetType();
                                PropertyInfo valueInfo = type.GetProperty(name);

                                object propertyValue = valueInfo.GetValue(topa16, null);
                                ddlAchieved.Text = propertyValue.ToString();
                            }
                            catch
                            { }

                            try
                            {
                                string name = "Observation" + str;
                                TextBox tbObservation = (TextBox)this.FindControl("p" + name);

                                Type type = topa16.GetType();
                                PropertyInfo valueInfo = type.GetProperty(name);

                                object propertyValue = valueInfo.GetValue(topa16, null);
                                tbObservation.Text = propertyValue.ToString();
                            }
                            catch
                            { }
                        }
                    }
                    #endregion
                    #region  Section 17
                    if (topa.Point17Id.HasValue) // Section #17
                    {
                        SessionHandler.spVar_TopaId_17 = topa.Point17Id.Value.ToString();
                        TwentyOnePointAudit17Service topaService17 = new TwentyOnePointAudit17Service();
                        KaiZen.CSMS.Entities.TwentyOnePointAudit17 topa17 = topaService17.GetById(topa.Point17Id.Value);
                        foreach (string str in section17)
                        {
                            try
                            {
                                string name = "Achieved" + str;
                                DropDownList ddlAchieved = (DropDownList)this.FindControl("p" + name);

                                Type type = topa17.GetType();
                                PropertyInfo valueInfo = type.GetProperty(name);

                                object propertyValue = valueInfo.GetValue(topa17, null);
                                ddlAchieved.Text = propertyValue.ToString();
                            }
                            catch
                            { }

                            try
                            {
                                string name = "Observation" + str;
                                TextBox tbObservation = (TextBox)this.FindControl("p" + name);

                                Type type = topa17.GetType();
                                PropertyInfo valueInfo = type.GetProperty(name);

                                object propertyValue = valueInfo.GetValue(topa17, null);
                                tbObservation.Text = propertyValue.ToString();
                            }
                            catch
                            { }
                        }
                    }
                                        #endregion
                    #region  Section 18
                    if (topa.Point18Id.HasValue) // Section #18
                    {
                        SessionHandler.spVar_TopaId_18 = topa.Point18Id.Value.ToString();
                        TwentyOnePointAudit18Service topaService18 = new TwentyOnePointAudit18Service();
                        KaiZen.CSMS.Entities.TwentyOnePointAudit18 topa18 = topaService18.GetById(topa.Point18Id.Value);
                        foreach (string str in section18)
                        {
                            try
                            {
                                string name = "Achieved" + str;
                                DropDownList ddlAchieved = (DropDownList)this.FindControl("p" + name);

                                Type type = topa18.GetType();
                                PropertyInfo valueInfo = type.GetProperty(name);

                                object propertyValue = valueInfo.GetValue(topa18, null);
                                ddlAchieved.Text = propertyValue.ToString();
                            }
                            catch
                            { }

                            try
                            {
                                string name = "Observation" + str;
                                TextBox tbObservation = (TextBox)this.FindControl("p" + name);

                                Type type = topa18.GetType();
                                PropertyInfo valueInfo = type.GetProperty(name);

                                object propertyValue = valueInfo.GetValue(topa18, null);
                                tbObservation.Text = propertyValue.ToString();
                            }
                            catch
                            { }
                        }
                    }
                                        #endregion
                    #region  Section 19
                    if (topa.Point19Id.HasValue) // Section #19
                    {
                        SessionHandler.spVar_TopaId_19 = topa.Point19Id.Value.ToString();
                        TwentyOnePointAudit19Service topaService19 = new TwentyOnePointAudit19Service();
                        KaiZen.CSMS.Entities.TwentyOnePointAudit19 topa19 = topaService19.GetById(topa.Point19Id.Value);
                        foreach (string str in section19)
                        {
                            try
                            {
                                string name = "Achieved" + str;
                                DropDownList ddlAchieved = (DropDownList)this.FindControl("p" + name);

                                Type type = topa19.GetType();
                                PropertyInfo valueInfo = type.GetProperty(name);

                                object propertyValue = valueInfo.GetValue(topa19, null);
                                ddlAchieved.Text = propertyValue.ToString();
                            }
                            catch
                            { }

                            try
                            {
                                string name = "Observation" + str;
                                TextBox tbObservation = (TextBox)this.FindControl("p" + name);

                                Type type = topa19.GetType();
                                PropertyInfo valueInfo = type.GetProperty(name);

                                object propertyValue = valueInfo.GetValue(topa19, null);
                                tbObservation.Text = propertyValue.ToString();
                            }
                            catch
                            { }
                        }
                    }
                                        #endregion
                    #region  Section 20
                    if (topa.Point20Id.HasValue) // Section #20
                    {
                        SessionHandler.spVar_TopaId_20 = topa.Point20Id.Value.ToString();
                        TwentyOnePointAudit20Service topaService20 = new TwentyOnePointAudit20Service();
                        KaiZen.CSMS.Entities.TwentyOnePointAudit20 topa20 = topaService20.GetById(topa.Point20Id.Value);
                        foreach (string str in section20)
                        {
                            try
                            {
                                string name = "Achieved" + str;
                                DropDownList ddlAchieved = (DropDownList)this.FindControl("p" + name);

                                Type type = topa20.GetType();
                                PropertyInfo valueInfo = type.GetProperty(name);

                                object propertyValue = valueInfo.GetValue(topa20, null);
                                ddlAchieved.Text = propertyValue.ToString();
                            }
                            catch
                            { }

                            try
                            {
                                string name = "Observation" + str;
                                TextBox tbObservation = (TextBox)this.FindControl("p" + name);

                                Type type = topa20.GetType();
                                PropertyInfo valueInfo = type.GetProperty(name);

                                object propertyValue = valueInfo.GetValue(topa20, null);
                                tbObservation.Text = propertyValue.ToString();
                            }
                            catch
                            { }
                        }
                    }
                                        #endregion
                    #region  Section 21
                    if (topa.Point21Id.HasValue) // Section #21
                    {
                        SessionHandler.spVar_TopaId_21 = topa.Point21Id.Value.ToString();
                        TwentyOnePointAudit21Service topaService21 = new TwentyOnePointAudit21Service();
                        KaiZen.CSMS.Entities.TwentyOnePointAudit21 topa21 = topaService21.GetById(topa.Point21Id.Value);
                        foreach (string str in section21)
                        {
                            try
                            {
                                string name = "Achieved" + str;
                                DropDownList ddlAchieved = (DropDownList)this.FindControl("p" + name);

                                Type type = topa21.GetType();
                                PropertyInfo valueInfo = type.GetProperty(name);

                                object propertyValue = valueInfo.GetValue(topa21, null);
                                ddlAchieved.Text = propertyValue.ToString();
                            }
                            catch
                            { }

                            try
                            {
                                string name = "Observation" + str;
                                TextBox tbObservation = (TextBox)this.FindControl("p" + name);

                                Type type = topa21.GetType();
                                PropertyInfo valueInfo = type.GetProperty(name);

                                object propertyValue = valueInfo.GetValue(topa21, null);
                                tbObservation.Text = propertyValue.ToString();
                            }
                            catch
                            { }
                        }
                    }
                                        #endregion

                }
                
                SessionHandler.spVar_TopaState = "Update"; // Update
                btnSave.Text = "Update Record";
                lblSave.Text = "";
                UpdatePanel1.Visible = true;
            }
            else
            {
                SessionHandler.spVar_TopaState = "New"; //New
                btnSave.Text = "Save New Record";
                lblSave.Text = "No Existing Data Found. Please enter and save.";
                TwentyOnePointAudit_New();
            }
        }
        else
        {
            SessionHandler.spVar_TopaState = "New"; //New
            btnSave.Text = "Save New Record";
            TwentyOnePointAudit_New();
        }
        CalcScores_Load();
                    
   
    } //Attempt to load topa, !exist -> New()
    private void TwentyOnePointAudit_New() {UpdatePanel1.Visible = true;}
    private void TwentyOnePointAudit_ReadOnly(bool enable) { } // Set Everything to read only()
    private void TwentyOnePointAudit_Save()
    {
        //System.Threading.Thread.Sleep(500);

        TwentyOnePointAuditService topaService = new TwentyOnePointAuditService();
        KaiZen.CSMS.Entities.TwentyOnePointAudit topa;
        try
        {
            if (SessionHandler.spVar_TopaId != "") { topa = topaService.GetById(Convert.ToInt32(SessionHandler.spVar_TopaId)); }
            else { topa = new KaiZen.CSMS.Entities.TwentyOnePointAudit(); }
        }
        catch { topa = new KaiZen.CSMS.Entities.TwentyOnePointAudit(); }
        
        topa.ModifiedbyUserId = auth.UserId;
        topa.DateModified = DateTime.Now;


        #region Section 01
	string Section01 = SessionHandler.spVar_TopaId_01;
        if (!String.IsNullOrEmpty(Section01)) //Update 'Section 01'
        {
            TwentyOnePointAudit01Service topaService01 = new TwentyOnePointAudit01Service();
            KaiZen.CSMS.Entities.TwentyOnePointAudit01 topa01 = topaService01.GetById(Convert.ToInt32(Section01));

            foreach (string str in section01)
            {
                try
                {
                    string name = "Achieved" + str;
                    DropDownList ddlAchieved = (DropDownList)this.FindControl("p" + name);

                    Type type = topa01.GetType();
                    PropertyInfo valueInfo = type.GetProperty(name);
                    valueInfo.SetValue(topa01, NullableStringToInteger(ddlAchieved.Text), null);
                }
                catch
                { }

                try
                {
                    string name = "Observation" + str;
                    TextBox tbObservation = (TextBox)this.FindControl("p" + name);

                    Type type = topa01.GetType();
                    PropertyInfo valueInfo = type.GetProperty(name);
                    valueInfo.SetValue(topa01, tbObservation.Text, null);
                }
                catch
                { }
            }
         
            topaService01.Update(topa01);
            topa.Point01Id = Convert.ToInt32(Section01);

        }
        else //New 'Section 01'
        {
            TwentyOnePointAudit01Service topaService01 = new TwentyOnePointAudit01Service();
            KaiZen.CSMS.Entities.TwentyOnePointAudit01 topa01 = new KaiZen.CSMS.Entities.TwentyOnePointAudit01();

            foreach (string str in section01)
            {
                try
                {
                    string name = "Achieved" + str;
                    DropDownList ddlAchieved = (DropDownList)this.FindControl("p" + name);

                    Type type = topa01.GetType();
                    PropertyInfo valueInfo = type.GetProperty(name);
                    valueInfo.SetValue(topa01, NullableStringToInteger(ddlAchieved.Text), null);
                }
                catch
                { }

                try
                {
                    string name = "Observation" + str;
                    TextBox tbObservation = (TextBox)this.FindControl("p" + name);

                    Type type = topa01.GetType();
                    PropertyInfo valueInfo = type.GetProperty(name);
                    valueInfo.SetValue(topa01, tbObservation.Text, null);
                }
                catch
                { }
            }

            topaService01.Save(topa01);
            topa.Point01Id = topa01.Id;

        }
        #endregion
        #region Section 02
        string Section02 = SessionHandler.spVar_TopaId_02;
        if (!String.IsNullOrEmpty(Section02)) //Update 'Section 02'
        {
            TwentyOnePointAudit02Service topaService02 = new TwentyOnePointAudit02Service();
            KaiZen.CSMS.Entities.TwentyOnePointAudit02 topa02 = topaService02.GetById(Convert.ToInt32(Section02));
            foreach (string str in section02)
            {
                try
                {
                    string name = "Achieved" + str;
                    DropDownList ddlAchieved = (DropDownList)this.FindControl("p" + name);

                    Type type = topa02.GetType();
                    PropertyInfo valueInfo = type.GetProperty(name);
                    valueInfo.SetValue(topa02, NullableStringToInteger(ddlAchieved.Text), null);
                }
                catch
                { }

                try
                {
                    string name = "Observation" + str;
                    TextBox tbObservation = (TextBox)this.FindControl("p" + name);

                    Type type = topa02.GetType();
                    PropertyInfo valueInfo = type.GetProperty(name);
                    valueInfo.SetValue(topa02, tbObservation.Text, null);
                }
                catch
                { }
            }
            topaService02.Update(topa02);
            topa.Point02Id = Convert.ToInt32(Section02);

        }
        else //New 'Section 02'
        {
            TwentyOnePointAudit02Service topaService02 = new TwentyOnePointAudit02Service();
            KaiZen.CSMS.Entities.TwentyOnePointAudit02 topa02 = new KaiZen.CSMS.Entities.TwentyOnePointAudit02();
            foreach (string str in section02)
            {
                try
                {
                    string name = "Achieved" + str;
                    DropDownList ddlAchieved = (DropDownList)this.FindControl("p" + name);

                    Type type = topa02.GetType();
                    PropertyInfo valueInfo = type.GetProperty(name);
                    valueInfo.SetValue(topa02, NullableStringToInteger(ddlAchieved.Text), null);
                }
                catch
                { }

                try
                {
                    string name = "Observation" + str;
                    TextBox tbObservation = (TextBox)this.FindControl("p" + name);

                    Type type = topa02.GetType();
                    PropertyInfo valueInfo = type.GetProperty(name);
                    valueInfo.SetValue(topa02, tbObservation.Text, null);
                }
                catch
                { }
            }
            topaService02.Save(topa02);
            topa.Point02Id = topa02.Id;

        }
        #endregion
        #region Section 03
        string Section03 = SessionHandler.spVar_TopaId_03;
        if (!String.IsNullOrEmpty(Section03)) //Update 'Section 03'
        {
            TwentyOnePointAudit03Service topaService03 = new TwentyOnePointAudit03Service();
            KaiZen.CSMS.Entities.TwentyOnePointAudit03 topa03 = topaService03.GetById(Convert.ToInt32(Section03));
            foreach (string str in section03)
            {
                try
                {
                    string name = "Achieved" + str;
                    DropDownList ddlAchieved = (DropDownList)this.FindControl("p" + name);

                    Type type = topa03.GetType();
                    PropertyInfo valueInfo = type.GetProperty(name);
                    valueInfo.SetValue(topa03, NullableStringToInteger(ddlAchieved.Text), null);
                }
                catch
                { }

                try
                {
                    string name = "Observation" + str;
                    TextBox tbObservation = (TextBox)this.FindControl("p" + name);

                    Type type = topa03.GetType();
                    PropertyInfo valueInfo = type.GetProperty(name);
                    valueInfo.SetValue(topa03, tbObservation.Text, null);
                }
                catch
                { }
            }
            topaService03.Update(topa03);
            topa.Point03Id = Convert.ToInt32(Section03);

        }
        else //New 'Section 03'
        {
            TwentyOnePointAudit03Service topaService03 = new TwentyOnePointAudit03Service();
            KaiZen.CSMS.Entities.TwentyOnePointAudit03 topa03 = new KaiZen.CSMS.Entities.TwentyOnePointAudit03();
            foreach (string str in section03)
            {
                try
                {
                    string name = "Achieved" + str;
                    DropDownList ddlAchieved = (DropDownList)this.FindControl("p" + name);

                    Type type = topa03.GetType();
                    PropertyInfo valueInfo = type.GetProperty(name);
                    valueInfo.SetValue(topa03, NullableStringToInteger(ddlAchieved.Text), null);
                }
                catch
                { }

                try
                {
                    string name = "Observation" + str;
                    TextBox tbObservation = (TextBox)this.FindControl("p" + name);

                    Type type = topa03.GetType();
                    PropertyInfo valueInfo = type.GetProperty(name);
                    valueInfo.SetValue(topa03, tbObservation.Text, null);
                }
                catch
                { }
            }
            topaService03.Save(topa03);
            topa.Point03Id = topa03.Id;

        }
        #endregion
        #region Section 04
        string Section04 = SessionHandler.spVar_TopaId_04;
        if (!String.IsNullOrEmpty(Section04)) //Update 'Section 04'
        {
            TwentyOnePointAudit04Service topaService04 = new TwentyOnePointAudit04Service();
            KaiZen.CSMS.Entities.TwentyOnePointAudit04 topa04 = topaService04.GetById(Convert.ToInt32(Section04));
            foreach (string str in section04)
            {
                try
                {
                    string name = "Achieved" + str;
                    DropDownList ddlAchieved = (DropDownList)this.FindControl("p" + name);

                    Type type = topa04.GetType();
                    PropertyInfo valueInfo = type.GetProperty(name);
                    valueInfo.SetValue(topa04, NullableStringToInteger(ddlAchieved.Text), null);
                }
                catch
                { }

                try
                {
                    string name = "Observation" + str;
                    TextBox tbObservation = (TextBox)this.FindControl("p" + name);

                    Type type = topa04.GetType();
                    PropertyInfo valueInfo = type.GetProperty(name);
                    valueInfo.SetValue(topa04, tbObservation.Text, null);
                }
                catch
                { }
            }
            topaService04.Update(topa04);
            topa.Point04Id = Convert.ToInt32(Section04);

        }
        else //New 'Section 04'
        {
            TwentyOnePointAudit04Service topaService04 = new TwentyOnePointAudit04Service();
            KaiZen.CSMS.Entities.TwentyOnePointAudit04 topa04 = new KaiZen.CSMS.Entities.TwentyOnePointAudit04();
            foreach (string str in section04)
            {
                try
                {
                    string name = "Achieved" + str;
                    DropDownList ddlAchieved = (DropDownList)this.FindControl("p" + name);

                    Type type = topa04.GetType();
                    PropertyInfo valueInfo = type.GetProperty(name);
                    valueInfo.SetValue(topa04, NullableStringToInteger(ddlAchieved.Text), null);
                }
                catch
                { }

                try
                {
                    string name = "Observation" + str;
                    TextBox tbObservation = (TextBox)this.FindControl("p" + name);

                    Type type = topa04.GetType();
                    PropertyInfo valueInfo = type.GetProperty(name);
                    valueInfo.SetValue(topa04, tbObservation.Text, null);
                }
                catch
                { }
            }
            topaService04.Save(topa04);
            topa.Point04Id = topa04.Id;

        }
        #endregion
        #region Section 05
        string Section05 = SessionHandler.spVar_TopaId_05;
        if (!String.IsNullOrEmpty(Section05)) //Update 'Section 05'
        {
            TwentyOnePointAudit05Service topaService05 = new TwentyOnePointAudit05Service();
            KaiZen.CSMS.Entities.TwentyOnePointAudit05 topa05 = topaService05.GetById(Convert.ToInt32(Section05));
            foreach (string str in section05)
            {
                try
                {
                    string name = "Achieved" + str;
                    DropDownList ddlAchieved = (DropDownList)this.FindControl("p" + name);

                    Type type = topa05.GetType();
                    PropertyInfo valueInfo = type.GetProperty(name);
                    valueInfo.SetValue(topa05, NullableStringToInteger(ddlAchieved.Text), null);
                }
                catch
                { }

                try
                {
                    string name = "Observation" + str;
                    TextBox tbObservation = (TextBox)this.FindControl("p" + name);

                    Type type = topa05.GetType();
                    PropertyInfo valueInfo = type.GetProperty(name);
                    valueInfo.SetValue(topa05, tbObservation.Text, null);
                }
                catch
                { }
            }
            topaService05.Update(topa05);
            topa.Point05Id = Convert.ToInt32(Section05);

        }
        else //New 'Section 05'
        {
            TwentyOnePointAudit05Service topaService05 = new TwentyOnePointAudit05Service();
            KaiZen.CSMS.Entities.TwentyOnePointAudit05 topa05 = new KaiZen.CSMS.Entities.TwentyOnePointAudit05();
            foreach (string str in section05)
            {
                try
                {
                    string name = "Achieved" + str;
                    DropDownList ddlAchieved = (DropDownList)this.FindControl("p" + name);

                    Type type = topa05.GetType();
                    PropertyInfo valueInfo = type.GetProperty(name);
                    valueInfo.SetValue(topa05, NullableStringToInteger(ddlAchieved.Text), null);
                }
                catch
                { }

                try
                {
                    string name = "Observation" + str;
                    TextBox tbObservation = (TextBox)this.FindControl("p" + name);

                    Type type = topa05.GetType();
                    PropertyInfo valueInfo = type.GetProperty(name);
                    valueInfo.SetValue(topa05, tbObservation.Text, null);
                }
                catch
                { }
            }
            topaService05.Save(topa05);
            topa.Point05Id = topa05.Id;

        }
        #endregion
        #region Section 06
        string Section06 = SessionHandler.spVar_TopaId_06;
        if (!String.IsNullOrEmpty(Section06)) //Update 'Section 06'
        {
            TwentyOnePointAudit06Service topaService06 = new TwentyOnePointAudit06Service();
            KaiZen.CSMS.Entities.TwentyOnePointAudit06 topa06 = topaService06.GetById(Convert.ToInt32(Section06));
            foreach (string str in section06)
            {
                try
                {
                    string name = "Achieved" + str;
                    DropDownList ddlAchieved = (DropDownList)this.FindControl("p" + name);

                    Type type = topa06.GetType();
                    PropertyInfo valueInfo = type.GetProperty(name);
                    valueInfo.SetValue(topa06, NullableStringToInteger(ddlAchieved.Text), null);
                }
                catch
                { }

                try
                {
                    string name = "Observation" + str;
                    TextBox tbObservation = (TextBox)this.FindControl("p" + name);

                    Type type = topa06.GetType();
                    PropertyInfo valueInfo = type.GetProperty(name);
                    valueInfo.SetValue(topa06, tbObservation.Text, null);
                }
                catch
                { }
            }
            topaService06.Update(topa06);
            topa.Point06Id = Convert.ToInt32(Section06);

        }
        else //New 'Section 06'
        {
            TwentyOnePointAudit06Service topaService06 = new TwentyOnePointAudit06Service();
            KaiZen.CSMS.Entities.TwentyOnePointAudit06 topa06 = new KaiZen.CSMS.Entities.TwentyOnePointAudit06();
            foreach (string str in section06)
            {
                try
                {
                    string name = "Achieved" + str;
                    DropDownList ddlAchieved = (DropDownList)this.FindControl("p" + name);

                    Type type = topa06.GetType();
                    PropertyInfo valueInfo = type.GetProperty(name);
                    valueInfo.SetValue(topa06, NullableStringToInteger(ddlAchieved.Text), null);
                }
                catch
                { }

                try
                {
                    string name = "Observation" + str;
                    TextBox tbObservation = (TextBox)this.FindControl("p" + name);

                    Type type = topa06.GetType();
                    PropertyInfo valueInfo = type.GetProperty(name);
                    valueInfo.SetValue(topa06, tbObservation.Text, null);
                }
                catch
                { }
            }
            topaService06.Save(topa06);
            topa.Point06Id = topa06.Id;

        }
        #endregion
        #region Section 07
        string Section07 = SessionHandler.spVar_TopaId_07;
        if (!String.IsNullOrEmpty(Section07)) //Update 'Section 07'
        {
            TwentyOnePointAudit07Service topaService07 = new TwentyOnePointAudit07Service();
            KaiZen.CSMS.Entities.TwentyOnePointAudit07 topa07 = topaService07.GetById(Convert.ToInt32(Section07));
            foreach (string str in section07)
            {
                try
                {
                    string name = "Achieved" + str;
                    DropDownList ddlAchieved = (DropDownList)this.FindControl("p" + name);

                    Type type = topa07.GetType();
                    PropertyInfo valueInfo = type.GetProperty(name);
                    valueInfo.SetValue(topa07, NullableStringToInteger(ddlAchieved.Text), null);
                }
                catch
                { }

                try
                {
                    string name = "Observation" + str;
                    TextBox tbObservation = (TextBox)this.FindControl("p" + name);

                    Type type = topa07.GetType();
                    PropertyInfo valueInfo = type.GetProperty(name);
                    valueInfo.SetValue(topa07, tbObservation.Text, null);
                }
                catch
                { }
            }
            topaService07.Update(topa07);
            topa.Point07Id = Convert.ToInt32(Section07);

        }
        else //New 'Section 07'
        {
            TwentyOnePointAudit07Service topaService07 = new TwentyOnePointAudit07Service();
            KaiZen.CSMS.Entities.TwentyOnePointAudit07 topa07 = new KaiZen.CSMS.Entities.TwentyOnePointAudit07();
            foreach (string str in section07)
            {
                try
                {
                    string name = "Achieved" + str;
                    DropDownList ddlAchieved = (DropDownList)this.FindControl("p" + name);

                    Type type = topa07.GetType();
                    PropertyInfo valueInfo = type.GetProperty(name);
                    valueInfo.SetValue(topa07, NullableStringToInteger(ddlAchieved.Text), null);
                }
                catch
                { }

                try
                {
                    string name = "Observation" + str;
                    TextBox tbObservation = (TextBox)this.FindControl("p" + name);

                    Type type = topa07.GetType();
                    PropertyInfo valueInfo = type.GetProperty(name);
                    valueInfo.SetValue(topa07, tbObservation.Text, null);
                }
                catch
                { }
            }
            topaService07.Save(topa07);
            topa.Point07Id = topa07.Id;

        }
        #endregion
        #region Section 08
        string Section08 = SessionHandler.spVar_TopaId_08;
        if (!String.IsNullOrEmpty(Section08)) //Update 'Section 08'
        {
            TwentyOnePointAudit08Service topaService08 = new TwentyOnePointAudit08Service();
            KaiZen.CSMS.Entities.TwentyOnePointAudit08 topa08 = topaService08.GetById(Convert.ToInt32(Section08));
            foreach (string str in section08)
            {
                try
                {
                    string name = "Achieved" + str;
                    DropDownList ddlAchieved = (DropDownList)this.FindControl("p" + name);

                    Type type = topa08.GetType();
                    PropertyInfo valueInfo = type.GetProperty(name);
                    valueInfo.SetValue(topa08, NullableStringToInteger(ddlAchieved.Text), null);
                }
                catch
                { }

                try
                {
                    string name = "Observation" + str;
                    TextBox tbObservation = (TextBox)this.FindControl("p" + name);

                    Type type = topa08.GetType();
                    PropertyInfo valueInfo = type.GetProperty(name);
                    valueInfo.SetValue(topa08, tbObservation.Text, null);
                }
                catch
                { }
            }
            topaService08.Update(topa08);
            topa.Point08Id = Convert.ToInt32(Section08);

        }
        else //New 'Section 08'
        {
            TwentyOnePointAudit08Service topaService08 = new TwentyOnePointAudit08Service();
            KaiZen.CSMS.Entities.TwentyOnePointAudit08 topa08 = new KaiZen.CSMS.Entities.TwentyOnePointAudit08();
            foreach (string str in section08)
            {
                try
                {
                    string name = "Achieved" + str;
                    DropDownList ddlAchieved = (DropDownList)this.FindControl("p" + name);

                    Type type = topa08.GetType();
                    PropertyInfo valueInfo = type.GetProperty(name);
                    valueInfo.SetValue(topa08, NullableStringToInteger(ddlAchieved.Text), null);
                }
                catch
                { }

                try
                {
                    string name = "Observation" + str;
                    TextBox tbObservation = (TextBox)this.FindControl("p" + name);

                    Type type = topa08.GetType();
                    PropertyInfo valueInfo = type.GetProperty(name);
                    valueInfo.SetValue(topa08, tbObservation.Text, null);
                }
                catch
                { }
            }
            topaService08.Save(topa08);
            topa.Point08Id = topa08.Id;

        }
         #endregion
        #region Section 09
        string Section09 = SessionHandler.spVar_TopaId_09;
        if (!String.IsNullOrEmpty(Section09)) //Update 'Section 09'
        {
            TwentyOnePointAudit09Service topaService09 = new TwentyOnePointAudit09Service();
            KaiZen.CSMS.Entities.TwentyOnePointAudit09 topa09 = topaService09.GetById(Convert.ToInt32(Section09));
            foreach (string str in section09)
            {
                try
                {
                    string name = "Achieved" + str;
                    DropDownList ddlAchieved = (DropDownList)this.FindControl("p" + name);

                    Type type = topa09.GetType();
                    PropertyInfo valueInfo = type.GetProperty(name);
                    valueInfo.SetValue(topa09, NullableStringToInteger(ddlAchieved.Text), null);
                }
                catch
                { }

                try
                {
                    string name = "Observation" + str;
                    TextBox tbObservation = (TextBox)this.FindControl("p" + name);

                    Type type = topa09.GetType();
                    PropertyInfo valueInfo = type.GetProperty(name);
                    valueInfo.SetValue(topa09, tbObservation.Text, null);
                }
                catch
                { }
            }
            topaService09.Update(topa09);
            topa.Point09Id = Convert.ToInt32(Section09);

        }
        else //New 'Section 09'
        {
            TwentyOnePointAudit09Service topaService09 = new TwentyOnePointAudit09Service();
            KaiZen.CSMS.Entities.TwentyOnePointAudit09 topa09 = new KaiZen.CSMS.Entities.TwentyOnePointAudit09();
            foreach (string str in section09)
            {
                try
                {
                    string name = "Achieved" + str;
                    DropDownList ddlAchieved = (DropDownList)this.FindControl("p" + name);

                    Type type = topa09.GetType();
                    PropertyInfo valueInfo = type.GetProperty(name);
                    valueInfo.SetValue(topa09, NullableStringToInteger(ddlAchieved.Text), null);
                }
                catch
                { }

                try
                {
                    string name = "Observation" + str;
                    TextBox tbObservation = (TextBox)this.FindControl("p" + name);

                    Type type = topa09.GetType();
                    PropertyInfo valueInfo = type.GetProperty(name);
                    valueInfo.SetValue(topa09, tbObservation.Text, null);
                }
                catch
                { }
            }
            topaService09.Save(topa09);
            topa.Point09Id = topa09.Id;

        }
        #endregion
        #region Section 10
        string Section10 = SessionHandler.spVar_TopaId_10;
        if (!String.IsNullOrEmpty(Section10)) //Update 'Section 10'
        {
            TwentyOnePointAudit10Service topaService10 = new TwentyOnePointAudit10Service();
            KaiZen.CSMS.Entities.TwentyOnePointAudit10 topa10 = topaService10.GetById(Convert.ToInt32(Section10));
            foreach (string str in section10)
            {
                try
                {
                    string name = "Achieved" + str;
                    DropDownList ddlAchieved = (DropDownList)this.FindControl("p" + name);

                    Type type = topa10.GetType();
                    PropertyInfo valueInfo = type.GetProperty(name);
                    valueInfo.SetValue(topa10, NullableStringToInteger(ddlAchieved.Text), null);
                }
                catch
                { }

                try
                {
                    string name = "Observation" + str;
                    TextBox tbObservation = (TextBox)this.FindControl("p" + name);

                    Type type = topa10.GetType();
                    PropertyInfo valueInfo = type.GetProperty(name);
                    valueInfo.SetValue(topa10, tbObservation.Text, null);
                }
                catch
                { }
            }
            topaService10.Update(topa10);
            topa.Point10Id = Convert.ToInt32(Section10);

        }
        else //New 'Section 10'
        {
            TwentyOnePointAudit10Service topaService10 = new TwentyOnePointAudit10Service();
            KaiZen.CSMS.Entities.TwentyOnePointAudit10 topa10 = new KaiZen.CSMS.Entities.TwentyOnePointAudit10();
            foreach (string str in section10)
            {
                try
                {
                    string name = "Achieved" + str;
                    DropDownList ddlAchieved = (DropDownList)this.FindControl("p" + name);

                    Type type = topa10.GetType();
                    PropertyInfo valueInfo = type.GetProperty(name);
                    valueInfo.SetValue(topa10, NullableStringToInteger(ddlAchieved.Text), null);
                }
                catch
                { }

                try
                {
                    string name = "Observation" + str;
                    TextBox tbObservation = (TextBox)this.FindControl("p" + name);

                    Type type = topa10.GetType();
                    PropertyInfo valueInfo = type.GetProperty(name);
                    valueInfo.SetValue(topa10, tbObservation.Text, null);
                }
                catch
                { }
            }
            topaService10.Save(topa10);
            topa.Point10Id = topa10.Id;

        }
        #endregion
        #region Section 11
        string Section11 = SessionHandler.spVar_TopaId_11;
        if (!String.IsNullOrEmpty(Section11)) //Update 'Section 11'
        {
            TwentyOnePointAudit11Service topaService11 = new TwentyOnePointAudit11Service();
            KaiZen.CSMS.Entities.TwentyOnePointAudit11 topa11 = topaService11.GetById(Convert.ToInt32(Section11));
            foreach (string str in section11)
            {
                try
                {
                    string name = "Achieved" + str;
                    DropDownList ddlAchieved = (DropDownList)this.FindControl("p" + name);

                    Type type = topa11.GetType();
                    PropertyInfo valueInfo = type.GetProperty(name);
                    valueInfo.SetValue(topa11, NullableStringToInteger(ddlAchieved.Text), null);
                }
                catch
                { }

                try
                {
                    string name = "Observation" + str;
                    TextBox tbObservation = (TextBox)this.FindControl("p" + name);

                    Type type = topa11.GetType();
                    PropertyInfo valueInfo = type.GetProperty(name);
                    valueInfo.SetValue(topa11, tbObservation.Text, null);
                }
                catch
                { }
            }
            topaService11.Update(topa11);
            topa.Point11Id = Convert.ToInt32(Section11);

        }
        else //New 'Section 11'
        {
            TwentyOnePointAudit11Service topaService11 = new TwentyOnePointAudit11Service();
            KaiZen.CSMS.Entities.TwentyOnePointAudit11 topa11 = new KaiZen.CSMS.Entities.TwentyOnePointAudit11();
            foreach (string str in section11)
            {
                try
                {
                    string name = "Achieved" + str;
                    DropDownList ddlAchieved = (DropDownList)this.FindControl("p" + name);

                    Type type = topa11.GetType();
                    PropertyInfo valueInfo = type.GetProperty(name);
                    valueInfo.SetValue(topa11, NullableStringToInteger(ddlAchieved.Text), null);
                }
                catch
                { }

                try
                {
                    string name = "Observation" + str;
                    TextBox tbObservation = (TextBox)this.FindControl("p" + name);

                    Type type = topa11.GetType();
                    PropertyInfo valueInfo = type.GetProperty(name);
                    valueInfo.SetValue(topa11, tbObservation.Text, null);
                }
                catch
                { }
            }
            topaService11.Save(topa11);
            topa.Point11Id = topa11.Id;

        }
        #endregion
        #region Section 12
        string Section12 = SessionHandler.spVar_TopaId_12;
        if (!String.IsNullOrEmpty(Section12)) //Update 'Section 12'
        {
            TwentyOnePointAudit12Service topaService12 = new TwentyOnePointAudit12Service();
            KaiZen.CSMS.Entities.TwentyOnePointAudit12 topa12 = topaService12.GetById(Convert.ToInt32(Section12));
            foreach (string str in section12)
            {
                try
                {
                    string name = "Achieved" + str;
                    DropDownList ddlAchieved = (DropDownList)this.FindControl("p" + name);

                    Type type = topa12.GetType();
                    PropertyInfo valueInfo = type.GetProperty(name);
                    valueInfo.SetValue(topa12, NullableStringToInteger(ddlAchieved.Text), null);
                }
                catch
                { }

                try
                {
                    string name = "Observation" + str;
                    TextBox tbObservation = (TextBox)this.FindControl("p" + name);

                    Type type = topa12.GetType();
                    PropertyInfo valueInfo = type.GetProperty(name);
                    valueInfo.SetValue(topa12, tbObservation.Text, null);
                }
                catch
                { }
            }
            topaService12.Update(topa12);
            topa.Point12Id = Convert.ToInt32(Section12);

        }
        else //New 'Section 12'
        {
            TwentyOnePointAudit12Service topaService12 = new TwentyOnePointAudit12Service();
            KaiZen.CSMS.Entities.TwentyOnePointAudit12 topa12 = new KaiZen.CSMS.Entities.TwentyOnePointAudit12();
            foreach (string str in section12)
            {
                try
                {
                    string name = "Achieved" + str;
                    DropDownList ddlAchieved = (DropDownList)this.FindControl("p" + name);

                    Type type = topa12.GetType();
                    PropertyInfo valueInfo = type.GetProperty(name);
                    valueInfo.SetValue(topa12, NullableStringToInteger(ddlAchieved.Text), null);
                }
                catch
                { }

                try
                {
                    string name = "Observation" + str;
                    TextBox tbObservation = (TextBox)this.FindControl("p" + name);

                    Type type = topa12.GetType();
                    PropertyInfo valueInfo = type.GetProperty(name);
                    valueInfo.SetValue(topa12, tbObservation.Text, null);
                }
                catch
                { }
            }
            topaService12.Save(topa12);
            topa.Point12Id = topa12.Id;

        }
        #endregion
        #region Section 13
        string Section13 = SessionHandler.spVar_TopaId_13;
        if (!String.IsNullOrEmpty(Section13)) //Update 'Section 13'
        {
            TwentyOnePointAudit13Service topaService13 = new TwentyOnePointAudit13Service();
            KaiZen.CSMS.Entities.TwentyOnePointAudit13 topa13 = topaService13.GetById(Convert.ToInt32(Section13));
            foreach (string str in section13)
            {
                try
                {
                    string name = "Achieved" + str;
                    DropDownList ddlAchieved = (DropDownList)this.FindControl("p" + name);

                    Type type = topa13.GetType();
                    PropertyInfo valueInfo = type.GetProperty(name);
                    valueInfo.SetValue(topa13, NullableStringToInteger(ddlAchieved.Text), null);
                }
                catch
                { }

                try
                {
                    string name = "Observation" + str;
                    TextBox tbObservation = (TextBox)this.FindControl("p" + name);

                    Type type = topa13.GetType();
                    PropertyInfo valueInfo = type.GetProperty(name);
                    valueInfo.SetValue(topa13, tbObservation.Text, null);
                }
                catch
                { }
            }
            topaService13.Update(topa13);
            topa.Point13Id = Convert.ToInt32(Section13);

        }
        else //New 'Section 13'
        {
            TwentyOnePointAudit13Service topaService13 = new TwentyOnePointAudit13Service();
            KaiZen.CSMS.Entities.TwentyOnePointAudit13 topa13 = new KaiZen.CSMS.Entities.TwentyOnePointAudit13();
            foreach (string str in section13)
            {
                try
                {
                    string name = "Achieved" + str;
                    DropDownList ddlAchieved = (DropDownList)this.FindControl("p" + name);

                    Type type = topa13.GetType();
                    PropertyInfo valueInfo = type.GetProperty(name);
                    valueInfo.SetValue(topa13, NullableStringToInteger(ddlAchieved.Text), null);
                }
                catch
                { }

                try
                {
                    string name = "Observation" + str;
                    TextBox tbObservation = (TextBox)this.FindControl("p" + name);

                    Type type = topa13.GetType();
                    PropertyInfo valueInfo = type.GetProperty(name);
                    valueInfo.SetValue(topa13, tbObservation.Text, null);
                }
                catch
                { }
            }
            topaService13.Save(topa13);
            topa.Point13Id = topa13.Id;

        }
         #endregion
        #region Section 14
        string Section14 = SessionHandler.spVar_TopaId_14;
        if (!String.IsNullOrEmpty(Section14)) //Update 'Section 14'
        {
            TwentyOnePointAudit14Service topaService14 = new TwentyOnePointAudit14Service();
            KaiZen.CSMS.Entities.TwentyOnePointAudit14 topa14 = topaService14.GetById(Convert.ToInt32(Section14));
            foreach (string str in section14)
            {
                try
                {
                    string name = "Achieved" + str;
                    DropDownList ddlAchieved = (DropDownList)this.FindControl("p" + name);

                    Type type = topa14.GetType();
                    PropertyInfo valueInfo = type.GetProperty(name);
                    valueInfo.SetValue(topa14, NullableStringToInteger(ddlAchieved.Text), null);
                }
                catch
                { }

                try
                {
                    string name = "Observation" + str;
                    TextBox tbObservation = (TextBox)this.FindControl("p" + name);

                    Type type = topa14.GetType();
                    PropertyInfo valueInfo = type.GetProperty(name);
                    valueInfo.SetValue(topa14, tbObservation.Text, null);
                }
                catch
                { }
            }
            topaService14.Update(topa14);
            topa.Point14Id = Convert.ToInt32(Section14);

        }
        else //New 'Section 14'
        {
            TwentyOnePointAudit14Service topaService14 = new TwentyOnePointAudit14Service();
            KaiZen.CSMS.Entities.TwentyOnePointAudit14 topa14 = new KaiZen.CSMS.Entities.TwentyOnePointAudit14();
            foreach (string str in section14)
            {
                try
                {
                    string name = "Achieved" + str;
                    DropDownList ddlAchieved = (DropDownList)this.FindControl("p" + name);

                    Type type = topa14.GetType();
                    PropertyInfo valueInfo = type.GetProperty(name);
                    valueInfo.SetValue(topa14, NullableStringToInteger(ddlAchieved.Text), null);
                }
                catch
                { }

                try
                {
                    string name = "Observation" + str;
                    TextBox tbObservation = (TextBox)this.FindControl("p" + name);

                    Type type = topa14.GetType();
                    PropertyInfo valueInfo = type.GetProperty(name);
                    valueInfo.SetValue(topa14, tbObservation.Text, null);
                }
                catch
                { }
            }
            topaService14.Save(topa14);
            topa.Point14Id = topa14.Id;
        }
#endregion
        #region Section 15
        string Section15 = SessionHandler.spVar_TopaId_15;
        if (!String.IsNullOrEmpty(Section15)) //Update 'Section 15'
        {
            TwentyOnePointAudit15Service topaService15 = new TwentyOnePointAudit15Service();
            KaiZen.CSMS.Entities.TwentyOnePointAudit15 topa15 = topaService15.GetById(Convert.ToInt32(Section15));
            foreach (string str in section15)
            {
                try
                {
                    string name = "Achieved" + str;
                    DropDownList ddlAchieved = (DropDownList)this.FindControl("p" + name);

                    Type type = topa15.GetType();
                    PropertyInfo valueInfo = type.GetProperty(name);
                    valueInfo.SetValue(topa15, NullableStringToInteger(ddlAchieved.Text), null);
                }
                catch
                { }

                try
                {
                    string name = "Observation" + str;
                    TextBox tbObservation = (TextBox)this.FindControl("p" + name);

                    Type type = topa15.GetType();
                    PropertyInfo valueInfo = type.GetProperty(name);
                    valueInfo.SetValue(topa15, tbObservation.Text, null);
                }
                catch
                { }
            }
            topaService15.Update(topa15);
            topa.Point15Id = Convert.ToInt32(Section15);

        }
        else //New 'Section 15'
        {
            TwentyOnePointAudit15Service topaService15 = new TwentyOnePointAudit15Service();
            KaiZen.CSMS.Entities.TwentyOnePointAudit15 topa15 = new KaiZen.CSMS.Entities.TwentyOnePointAudit15();
            foreach (string str in section15)
            {
                try
                {
                    string name = "Achieved" + str;
                    DropDownList ddlAchieved = (DropDownList)this.FindControl("p" + name);

                    Type type = topa15.GetType();
                    PropertyInfo valueInfo = type.GetProperty(name);
                    valueInfo.SetValue(topa15, NullableStringToInteger(ddlAchieved.Text), null);
                }
                catch
                { }

                try
                {
                    string name = "Observation" + str;
                    TextBox tbObservation = (TextBox)this.FindControl("p" + name);

                    Type type = topa15.GetType();
                    PropertyInfo valueInfo = type.GetProperty(name);
                    valueInfo.SetValue(topa15, tbObservation.Text, null);
                }
                catch
                { }
            }
            topaService15.Save(topa15);
            topa.Point15Id = topa15.Id;
        }
	#endregion
        #region Section 16
        string Section16 = SessionHandler.spVar_TopaId_16;
        if (!String.IsNullOrEmpty(Section16)) //Update 'Section 16'
        {
            TwentyOnePointAudit16Service topaService16 = new TwentyOnePointAudit16Service();
            KaiZen.CSMS.Entities.TwentyOnePointAudit16 topa16 = topaService16.GetById(Convert.ToInt32(Section16));
            foreach (string str in section16)
            {
                try
                {
                    string name = "Achieved" + str;
                    DropDownList ddlAchieved = (DropDownList)this.FindControl("p" + name);

                    Type type = topa16.GetType();
                    PropertyInfo valueInfo = type.GetProperty(name);
                    valueInfo.SetValue(topa16, NullableStringToInteger(ddlAchieved.Text), null);
                }
                catch
                { }

                try
                {
                    string name = "Observation" + str;
                    TextBox tbObservation = (TextBox)this.FindControl("p" + name);

                    Type type = topa16.GetType();
                    PropertyInfo valueInfo = type.GetProperty(name);
                    valueInfo.SetValue(topa16, tbObservation.Text, null);
                }
                catch
                { }
            }
            topaService16.Update(topa16);
            topa.Point16Id = Convert.ToInt32(Section16);

        }
        else //New 'Section 16'
        {
            TwentyOnePointAudit16Service topaService16 = new TwentyOnePointAudit16Service();
            KaiZen.CSMS.Entities.TwentyOnePointAudit16 topa16 = new KaiZen.CSMS.Entities.TwentyOnePointAudit16();
            foreach (string str in section16)
            {
                try
                {
                    string name = "Achieved" + str;
                    DropDownList ddlAchieved = (DropDownList)this.FindControl("p" + name);

                    Type type = topa16.GetType();
                    PropertyInfo valueInfo = type.GetProperty(name);
                    valueInfo.SetValue(topa16, NullableStringToInteger(ddlAchieved.Text), null);
                }
                catch
                { }

                try
                {
                    string name = "Observation" + str;
                    TextBox tbObservation = (TextBox)this.FindControl("p" + name);

                    Type type = topa16.GetType();
                    PropertyInfo valueInfo = type.GetProperty(name);
                    valueInfo.SetValue(topa16, tbObservation.Text, null);
                }
                catch
                { }
            }
            topaService16.Save(topa16);
            topa.Point16Id = topa16.Id;
        }
                    #endregion
        #region Section 17
        string Section17 = SessionHandler.spVar_TopaId_17;
        if (!String.IsNullOrEmpty(Section17)) //Update 'Section 17'
        {
            TwentyOnePointAudit17Service topaService17 = new TwentyOnePointAudit17Service();
            KaiZen.CSMS.Entities.TwentyOnePointAudit17 topa17 = topaService17.GetById(Convert.ToInt32(Section17));
            foreach (string str in section17)
            {
                try
                {
                    string name = "Achieved" + str;
                    DropDownList ddlAchieved = (DropDownList)this.FindControl("p" + name);

                    Type type = topa17.GetType();
                    PropertyInfo valueInfo = type.GetProperty(name);
                    valueInfo.SetValue(topa17, NullableStringToInteger(ddlAchieved.Text), null);
                }
                catch
                { }

                try
                {
                    string name = "Observation" + str;
                    TextBox tbObservation = (TextBox)this.FindControl("p" + name);

                    Type type = topa17.GetType();
                    PropertyInfo valueInfo = type.GetProperty(name);
                    valueInfo.SetValue(topa17, tbObservation.Text, null);
                }
                catch
                { }
            }
            topaService17.Update(topa17);
            topa.Point17Id = Convert.ToInt32(Section17);

        }
        else //New 'Section 17'
        {
            TwentyOnePointAudit17Service topaService17 = new TwentyOnePointAudit17Service();
            KaiZen.CSMS.Entities.TwentyOnePointAudit17 topa17 = new KaiZen.CSMS.Entities.TwentyOnePointAudit17();
            foreach (string str in section17)
            {
                try
                {
                    string name = "Achieved" + str;
                    DropDownList ddlAchieved = (DropDownList)this.FindControl("p" + name);

                    Type type = topa17.GetType();
                    PropertyInfo valueInfo = type.GetProperty(name);
                    valueInfo.SetValue(topa17, NullableStringToInteger(ddlAchieved.Text), null);
                }
                catch
                { }

                try
                {
                    string name = "Observation" + str;
                    TextBox tbObservation = (TextBox)this.FindControl("p" + name);

                    Type type = topa17.GetType();
                    PropertyInfo valueInfo = type.GetProperty(name);
                    valueInfo.SetValue(topa17, tbObservation.Text, null);
                }
                catch
                { }
            }
            topaService17.Save(topa17);
            topa.Point17Id = topa17.Id;
        }
                #endregion
        #region Section 18
        string Section18 = SessionHandler.spVar_TopaId_18;
        if (!String.IsNullOrEmpty(Section18)) //Update 'Section 18'
        {
            TwentyOnePointAudit18Service topaService18 = new TwentyOnePointAudit18Service();
            KaiZen.CSMS.Entities.TwentyOnePointAudit18 topa18 = topaService18.GetById(Convert.ToInt32(Section18));
            foreach (string str in section18)
            {
                try
                {
                    string name = "Achieved" + str;
                    DropDownList ddlAchieved = (DropDownList)this.FindControl("p" + name);

                    Type type = topa18.GetType();
                    PropertyInfo valueInfo = type.GetProperty(name);
                    valueInfo.SetValue(topa18, NullableStringToInteger(ddlAchieved.Text), null);
                }
                catch
                { }

                try
                {
                    string name = "Observation" + str;
                    TextBox tbObservation = (TextBox)this.FindControl("p" + name);

                    Type type = topa18.GetType();
                    PropertyInfo valueInfo = type.GetProperty(name);
                    valueInfo.SetValue(topa18, tbObservation.Text, null);
                }
                catch
                { }
            }
            topaService18.Update(topa18);
            topa.Point18Id = Convert.ToInt32(Section18);

        }
        else //New 'Section 18'
        {
            TwentyOnePointAudit18Service topaService18 = new TwentyOnePointAudit18Service();
            KaiZen.CSMS.Entities.TwentyOnePointAudit18 topa18 = new KaiZen.CSMS.Entities.TwentyOnePointAudit18();
            foreach (string str in section18)
            {
                try
                {
                    string name = "Achieved" + str;
                    DropDownList ddlAchieved = (DropDownList)this.FindControl("p" + name);

                    Type type = topa18.GetType();
                    PropertyInfo valueInfo = type.GetProperty(name);
                    valueInfo.SetValue(topa18, NullableStringToInteger(ddlAchieved.Text), null);
                }
                catch
                { }

                try
                {
                    string name = "Observation" + str;
                    TextBox tbObservation = (TextBox)this.FindControl("p" + name);

                    Type type = topa18.GetType();
                    PropertyInfo valueInfo = type.GetProperty(name);
                    valueInfo.SetValue(topa18, tbObservation.Text, null);
                }
                catch
                { }
            }
            topaService18.Save(topa18);
            topa.Point18Id = topa18.Id;
        }
                #endregion
        #region Section 19
        string Section19 = SessionHandler.spVar_TopaId_19;
        if (!String.IsNullOrEmpty(Section19)) //Update 'Section 19'
        {
            TwentyOnePointAudit19Service topaService19 = new TwentyOnePointAudit19Service();
            KaiZen.CSMS.Entities.TwentyOnePointAudit19 topa19 = topaService19.GetById(Convert.ToInt32(Section19));
            foreach (string str in section19)
            {
                try
                {
                    string name = "Achieved" + str;
                    DropDownList ddlAchieved = (DropDownList)this.FindControl("p" + name);

                    Type type = topa19.GetType();
                    PropertyInfo valueInfo = type.GetProperty(name);
                    valueInfo.SetValue(topa19, NullableStringToInteger(ddlAchieved.Text), null);
                }
                catch
                { }

                try
                {
                    string name = "Observation" + str;
                    TextBox tbObservation = (TextBox)this.FindControl("p" + name);

                    Type type = topa19.GetType();
                    PropertyInfo valueInfo = type.GetProperty(name);
                    valueInfo.SetValue(topa19, tbObservation.Text, null);
                }
                catch
                { }
            }
            topaService19.Update(topa19);
            topa.Point19Id = Convert.ToInt32(Section19);

        }
        else //New 'Section 19'
        {
            TwentyOnePointAudit19Service topaService19 = new TwentyOnePointAudit19Service();
            KaiZen.CSMS.Entities.TwentyOnePointAudit19 topa19 = new KaiZen.CSMS.Entities.TwentyOnePointAudit19();
            foreach (string str in section19)
            {
                try
                {
                    string name = "Achieved" + str;
                    DropDownList ddlAchieved = (DropDownList)this.FindControl("p" + name);

                    Type type = topa19.GetType();
                    PropertyInfo valueInfo = type.GetProperty(name);
                    valueInfo.SetValue(topa19, NullableStringToInteger(ddlAchieved.Text), null);
                }
                catch
                { }

                try
                {
                    string name = "Observation" + str;
                    TextBox tbObservation = (TextBox)this.FindControl("p" + name);

                    Type type = topa19.GetType();
                    PropertyInfo valueInfo = type.GetProperty(name);
                    valueInfo.SetValue(topa19, tbObservation.Text, null);
                }
                catch
                { }
            }
            topaService19.Save(topa19);
            topa.Point19Id = topa19.Id;
        }
                #endregion
        #region Section 20
        string Section20 = SessionHandler.spVar_TopaId_20;
        if (!String.IsNullOrEmpty(Section20)) //Update 'Section 20'
        {
            TwentyOnePointAudit20Service topaService20 = new TwentyOnePointAudit20Service();
            KaiZen.CSMS.Entities.TwentyOnePointAudit20 topa20 = topaService20.GetById(Convert.ToInt32(Section20));
            foreach (string str in section20)
            {
                try
                {
                    string name = "Achieved" + str;
                    DropDownList ddlAchieved = (DropDownList)this.FindControl("p" + name);

                    Type type = topa20.GetType();
                    PropertyInfo valueInfo = type.GetProperty(name);
                    valueInfo.SetValue(topa20, NullableStringToInteger(ddlAchieved.Text), null);
                }
                catch
                { }

                try
                {
                    string name = "Observation" + str;
                    TextBox tbObservation = (TextBox)this.FindControl("p" + name);

                    Type type = topa20.GetType();
                    PropertyInfo valueInfo = type.GetProperty(name);
                    valueInfo.SetValue(topa20, tbObservation.Text, null);
                }
                catch
                { }
            }
            topaService20.Update(topa20);
            topa.Point20Id = Convert.ToInt32(Section20);

        }
        else //New 'Section 20'
        {
            TwentyOnePointAudit20Service topaService20 = new TwentyOnePointAudit20Service();
            KaiZen.CSMS.Entities.TwentyOnePointAudit20 topa20 = new KaiZen.CSMS.Entities.TwentyOnePointAudit20();
            foreach (string str in section20)
            {
                try
                {
                    string name = "Achieved" + str;
                    DropDownList ddlAchieved = (DropDownList)this.FindControl("p" + name);

                    Type type = topa20.GetType();
                    PropertyInfo valueInfo = type.GetProperty(name);
                    valueInfo.SetValue(topa20, NullableStringToInteger(ddlAchieved.Text), null);
                }
                catch
                { }

                try
                {
                    string name = "Observation" + str;
                    TextBox tbObservation = (TextBox)this.FindControl("p" + name);

                    Type type = topa20.GetType();
                    PropertyInfo valueInfo = type.GetProperty(name);
                    valueInfo.SetValue(topa20, tbObservation.Text, null);
                }
                catch
                { }
            }
            topaService20.Save(topa20);
            topa.Point20Id = topa20.Id;
        }
                #endregion
        #region Section 21
        string Section21 = SessionHandler.spVar_TopaId_21;
        if (!String.IsNullOrEmpty(Section21)) //Update 'Section 21'
        {
            TwentyOnePointAudit21Service topaService21 = new TwentyOnePointAudit21Service();
            KaiZen.CSMS.Entities.TwentyOnePointAudit21 topa21 = topaService21.GetById(Convert.ToInt32(Section21));
            foreach (string str in section21)
            {
                try
                {
                    string name = "Achieved" + str;
                    DropDownList ddlAchieved = (DropDownList)this.FindControl("p" + name);

                    Type type = topa21.GetType();
                    PropertyInfo valueInfo = type.GetProperty(name);
                    valueInfo.SetValue(topa21, NullableStringToInteger(ddlAchieved.Text), null);
                }
                catch
                { }

                try
                {
                    string name = "Observation" + str;
                    TextBox tbObservation = (TextBox)this.FindControl("p" + name);

                    Type type = topa21.GetType();
                    PropertyInfo valueInfo = type.GetProperty(name);
                    valueInfo.SetValue(topa21, tbObservation.Text, null);
                }
                catch
                { }
            }
            topaService21.Update(topa21);
            topa.Point21Id = Convert.ToInt32(Section21);

        }
        else //New 'Section 21'
        {
            TwentyOnePointAudit21Service topaService21 = new TwentyOnePointAudit21Service();
            KaiZen.CSMS.Entities.TwentyOnePointAudit21 topa21 = new KaiZen.CSMS.Entities.TwentyOnePointAudit21();
            foreach (string str in section21)
            {
                try
                {
                    string name = "Achieved" + str;
                    DropDownList ddlAchieved = (DropDownList)this.FindControl("p" + name);

                    Type type = topa21.GetType();
                    PropertyInfo valueInfo = type.GetProperty(name);
                    valueInfo.SetValue(topa21, NullableStringToInteger(ddlAchieved.Text), null);
                }
                catch
                { }

                try
                {
                    string name = "Observation" + str;
                    TextBox tbObservation = (TextBox)this.FindControl("p" + name);

                    Type type = topa21.GetType();
                    PropertyInfo valueInfo = type.GetProperty(name);
                    valueInfo.SetValue(topa21, tbObservation.Text, null);
                }
                catch
                { }
            }
            topaService21.Save(topa21);
            topa.Point21Id = topa21.Id;
        }
                #endregion

        CalcScores_Load();


        topa.TotalPossibleScore = nullableToInt32(pAAScoreTotala.Text);
        topa.TotalActualScore = nullableToInt32(pAAScoreTotalb.Text);
        topa.TotalRating = nullableToInt32((pAAScoreTotalc.Text).Replace("%", ""));

        switch (SessionHandler.spVar_TopaState)
        {
            case "Update":
                //Session["topaId"]
                //topaService.Update(topa);
                try { topaService.Update(topa); lblSave.Text = "Record Updated and Saved successfully."; }
                catch { lblSave.Text = "Not Saved, Please Try Again. Error Occured: " + topaService.ServiceProcessResult.ToString(); }
                break;
            case "New":
                topa.CompanyId = Convert.ToInt32(SessionHandler.spVar_CompanyId);
                topa.SiteId = Convert.ToInt32(SessionHandler.spVar_SiteId);
                topa.QtrId = Convert.ToInt32(SessionHandler.spVar_QtrId);
                topa.Year = Convert.ToInt32(SessionHandler.spVar_Year);
                topa.DateAdded = DateTime.Now;
                topa.CreatedbyUserId = auth.UserId;
                try {
                    topaService.Save(topa);
                    lblSave.Text = "Record Created and Saved successfully.";
                    btnSave.Text = "Update Record";
                   SessionHandler.spVar_TopaState = "Update";
                }
                catch { lblSave.Text = "Not Saved, Please Try Again. Error Occured: " + topaService.ServiceProcessResult.ToString(); }
                break;
            default:
                // Do nothing!
                break;
        }
    }

    private void TwentyOnePointAudit_Loadv2(int CompanyId, int SiteId, int QtrId, int Year)
    {
        //System.Threading.Thread.Sleep(500);
        SessionHandler.spVar_CompanyId = CompanyId.ToString();
        SessionHandler.spVar_SiteId = SiteId.ToString();
        SessionHandler.spVar_QtrId = QtrId.ToString();
        SessionHandler.spVar_Year = Year.ToString();

        topaService2 = new TwentyOnePointAuditService();
        TList<KaiZen.CSMS.Entities.TwentyOnePointAudit> topaList = topaService2.GetAll();
        if (topaList.Count != 0)
        {
            TList<KaiZen.CSMS.Entities.TwentyOnePointAudit> _topaList = topaList.FindAll( //Find KPI We Need
                delegate(KaiZen.CSMS.Entities.TwentyOnePointAudit topa)
                {
                    return
                        topa.CompanyId == CompanyId &&
                        topa.SiteId == SiteId &&
                        topa.QtrId == QtrId &&
                        topa.Year == Year;
                }
            );

            if (_topaList.Count != 0)
            {
                foreach (KaiZen.CSMS.Entities.TwentyOnePointAudit topa in _topaList)
                {
                    UsersService usersService = new UsersService();
                    Users userEntity;
                    userEntity = usersService.GetByUserId(topa.CreatedbyUserId);
                    userEntity = usersService.GetByUserId(topa.ModifiedbyUserId);
                    topa2 = topa;

                    //string[] userParameters = null;
                    //Type thisType = this.GetType();
                    //MethodInfo theMethod = thisType.GetMethod("topa.Point01Id.HasValue");
                    //if (theMethod.Invoke(this, userParameters))
                    //{
                    //    int j;
                    //}
                    //else
                    //{
                    //    int i;
                    //}


                    #region  Section 01
                    if (topa.Point01Id.HasValue) // Section #01
                    {
                        SessionHandler.spVar_TopaId_01 = topa.Point01Id.Value.ToString();
                        TwentyOnePointAudit01Service topaService01 = new TwentyOnePointAudit01Service();
                        KaiZen.CSMS.Entities.TwentyOnePointAudit01 topa01 = topaService01.GetById(topa.Point01Id.Value);
                        pAchieved1a.Text = topa01.Achieved1a.ToString();
                        pAchieved1b.Text = topa01.Achieved1b.ToString();
                        pAchieved1c.Text = topa01.Achieved1c.ToString();
                        pAchieved1d.Text = topa01.Achieved1d.ToString();
                        pAchieved1e.Text = topa01.Achieved1e.ToString();
                        pAchieved1f.Text = topa01.Achieved1f.ToString();
                        pAchieved1g.Text = topa01.Achieved1g.ToString();
                        pAchieved1h.Text = topa01.Achieved1h.ToString();
                        pAchieved1i.Text = topa01.Achieved1i.ToString();
                        pAchieved1j.Text = topa01.Achieved1j.ToString();
                        pAchieved1k.Text = topa01.Achieved1k.ToString();
                        pAchieved1l.Text = topa01.Achieved1l.ToString();
                        pAchieved1m.Text = topa01.Achieved1m.ToString();
                        pAchieved1n.Text = topa01.Achieved1n.ToString();
                        pAchieved1o.Text = topa01.Achieved1o.ToString();
                        pAchieved1p.Text = topa01.Achieved1p.ToString();
                        pAchieved1q.Text = topa01.Achieved1q.ToString();
                        pAchieved1r.Text = topa01.Achieved1r.ToString();
                        pAchieved1s.Text = topa01.Achieved1s.ToString();
                        pObservation1a.Text = topa01.Observation1a.ToString();
                        pObservation1b.Text = topa01.Observation1b.ToString();
                        pObservation1c.Text = topa01.Observation1c.ToString();
                        pObservation1d.Text = topa01.Observation1d.ToString();
                        pObservation1e.Text = topa01.Observation1e.ToString();
                        pObservation1f.Text = topa01.Observation1f.ToString();
                        pObservation1g.Text = topa01.Observation1g.ToString();
                        pObservation1h.Text = topa01.Observation1h.ToString();
                        pObservation1i.Text = topa01.Observation1i.ToString();
                        pObservation1j.Text = topa01.Observation1j.ToString();
                        pObservation1k.Text = topa01.Observation1k.ToString();
                        pObservation1l.Text = topa01.Observation1l.ToString();
                        pObservation1m.Text = topa01.Observation1m.ToString();
                        pObservation1n.Text = topa01.Observation1n.ToString();
                        pObservation1o.Text = topa01.Observation1o.ToString();
                        pObservation1p.Text = topa01.Observation1p.ToString();
                        pObservation1q.Text = topa01.Observation1q.ToString();
                        pObservation1r.Text = topa01.Observation1r.ToString();
                        pObservation1s.Text = topa01.Observation1s.ToString();
                    }
                    #endregion
                }
                btnSave.Text = "Update Record";
                lblSave.Text = "";
                UpdatePanel1.Visible = true;
            }
            else
            {
                topa2 = new KaiZen.CSMS.Entities.TwentyOnePointAudit(); 
                btnSave.Text = "Save New Record";
                lblSave.Text = "No Existing Data Found. Please enter and save.";
                TwentyOnePointAudit_New();
            }
        }
        else
        {
            SessionHandler.spVar_TopaState = "New"; //New
            btnSave.Text = "Save New Record";
            TwentyOnePointAudit_New();
        }
        CalcScores_Load();


    } //Attempt to load topa, !exist -> New()

    private int nullableToInt32(string input)
    {
        try
        {
            if (!String.IsNullOrEmpty(input))
            {
                return Convert.ToInt32(input);
            }
            else
            {
                return 0;
            }
        }
        catch (Exception ex)
        {
            Elmah.ErrorSignal.FromContext(Context).Raise(ex);
            return 0;
        }
    }

    protected void ddlCompanies_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillSitesCombo(ddlCompanies.SelectedItem.Value.ToString());
        //ddlSites.SelectedIndex = 0;
    }

}
