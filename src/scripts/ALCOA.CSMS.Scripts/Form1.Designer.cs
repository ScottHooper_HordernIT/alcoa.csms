namespace ALCOA.CSMS.Scripts
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.button13 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.listView1 = new System.Windows.Forms.ListView();
            this.chCompanyName = new System.Windows.Forms.ColumnHeader();
            this.chCompanyABN = new System.Windows.Forms.ColumnHeader();
            this.chPhoneNo = new System.Windows.Forms.ColumnHeader();
            this.chFaxNo = new System.Windows.Forms.ColumnHeader();
            this.chMobNo = new System.Windows.Forms.ColumnHeader();
            this.chAddressBusiness = new System.Windows.Forms.ColumnHeader();
            this.chAddressPostal = new System.Windows.Forms.ColumnHeader();
            this.chSafetyQualified = new System.Windows.Forms.ColumnHeader();
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.button9 = new System.Windows.Forms.Button();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.listView2 = new System.Windows.Forms.ListView();
            this.chUsersAlcoan_UserLogon = new System.Windows.Forms.ColumnHeader();
            this.chUsersAlcoan_LastName = new System.Windows.Forms.ColumnHeader();
            this.chUsersAlcoan_FirstName = new System.Windows.Forms.ColumnHeader();
            this.chUsersAlcoan_CompanyName = new System.Windows.Forms.ColumnHeader();
            this.chUsersAlcoan_CompanyNameId = new System.Windows.Forms.ColumnHeader();
            this.chUsersAlcoan_Title = new System.Windows.Forms.ColumnHeader();
            this.chUsersAlcoan_PhoneNo = new System.Windows.Forms.ColumnHeader();
            this.chUsersAlcoan_FaxNo = new System.Windows.Forms.ColumnHeader();
            this.chUsersAlcoan_MobNo = new System.Windows.Forms.ColumnHeader();
            this.chUsersAlcoan_Email = new System.Windows.Forms.ColumnHeader();
            this.chUsersAlcoan_RoleID = new System.Windows.Forms.ColumnHeader();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.listView3 = new System.Windows.Forms.ListView();
            this.columnHeader11 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader12 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader13 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader30 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader14 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader15 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader16 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader17 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader18 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader19 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader20 = new System.Windows.Forms.ColumnHeader();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.listView4 = new System.Windows.Forms.ListView();
            this.columnHeader31 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader21 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader22 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader23 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader24 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader25 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader26 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader27 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader28 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader29 = new System.Windows.Forms.ColumnHeader();
            this.button5 = new System.Windows.Forms.Button();
            this.richTextBox2 = new System.Windows.Forms.RichTextBox();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.button8 = new System.Windows.Forms.Button();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.tabPage8 = new System.Windows.Forms.TabPage();
            this.button14 = new System.Windows.Forms.Button();
            this.tabControl3 = new System.Windows.Forms.TabControl();
            this.tabPage9 = new System.Windows.Forms.TabPage();
            this.listView7 = new System.Windows.Forms.ListView();
            this.columnHeader44 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader45 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader46 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader47 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader48 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader49 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader50 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader51 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader54 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader55 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader56 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader78 = new System.Windows.Forms.ColumnHeader();
            this.tabPage11 = new System.Windows.Forms.TabPage();
            this.listView9 = new System.Windows.Forms.ListView();
            this.columnHeader68 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader69 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader70 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader71 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader72 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader74 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader76 = new System.Windows.Forms.ColumnHeader();
            this.button15 = new System.Windows.Forms.Button();
            this.richTextBox4 = new System.Windows.Forms.RichTextBox();
            this.button16 = new System.Windows.Forms.Button();
            this.button17 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.button18 = new System.Windows.Forms.Button();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.tabPage7 = new System.Windows.Forms.TabPage();
            this.button12 = new System.Windows.Forms.Button();
            this.richTextBox3 = new System.Windows.Forms.RichTextBox();
            this.button10 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.listView6 = new System.Windows.Forms.ListView();
            this.columnHeader52 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader53 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader57 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader58 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader59 = new System.Windows.Forms.ColumnHeader();
            this.label3 = new System.Windows.Forms.Label();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.tabPage10 = new System.Windows.Forms.TabPage();
            this.richTextBox5 = new System.Windows.Forms.RichTextBox();
            this.button20 = new System.Windows.Forms.Button();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.button19 = new System.Windows.Forms.Button();
            this.listView8 = new System.Windows.Forms.ListView();
            this.chTimeStamp = new System.Windows.Forms.ColumnHeader();
            this.chFileName = new System.Windows.Forms.ColumnHeader();
            this.chFileTag = new System.Windows.Forms.ColumnHeader();
            this.chWhatToDo = new System.Windows.Forms.ColumnHeader();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.listView5 = new System.Windows.Forms.ListView();
            this.columnHeader32 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader33 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader34 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader35 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader36 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader37 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader38 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader39 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader40 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader41 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader42 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader43 = new System.Windows.Forms.ColumnHeader();
            this.tabPage12 = new System.Windows.Forms.TabPage();
            this.button24 = new System.Windows.Forms.Button();
            this.button21 = new System.Windows.Forms.Button();
            this.rtbsims = new System.Windows.Forms.RichTextBox();
            this.button22 = new System.Windows.Forms.Button();
            this.button23 = new System.Windows.Forms.Button();
            this.lvsims = new System.Windows.Forms.ListView();
            this.columnHeader60 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader61 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader62 = new System.Windows.Forms.ColumnHeader();
            this.label5 = new System.Windows.Forms.Label();
            this.tbSimsCSV = new System.Windows.Forms.TextBox();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.columnHeader1 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader2 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader3 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader4 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader5 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader6 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader7 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader8 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader9 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader10 = new System.Windows.Forms.ColumnHeader();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.button25 = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabControl2.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.tabPage8.SuspendLayout();
            this.tabControl3.SuspendLayout();
            this.tabPage9.SuspendLayout();
            this.tabPage11.SuspendLayout();
            this.tabPage7.SuspendLayout();
            this.tabPage10.SuspendLayout();
            this.tabPage6.SuspendLayout();
            this.tabPage12.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage8);
            this.tabControl1.Controls.Add(this.tabPage7);
            this.tabControl1.Controls.Add(this.tabPage10);
            this.tabControl1.Controls.Add(this.tabPage6);
            this.tabControl1.Controls.Add(this.tabPage12);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(862, 449);
            this.tabControl1.TabIndex = 1;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.button13);
            this.tabPage1.Controls.Add(this.button4);
            this.tabPage1.Controls.Add(this.richTextBox1);
            this.tabPage1.Controls.Add(this.button3);
            this.tabPage1.Controls.Add(this.button2);
            this.tabPage1.Controls.Add(this.listView1);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.button1);
            this.tabPage1.Controls.Add(this.textBox1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(854, 423);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Import Companies";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // button13
            // 
            this.button13.Location = new System.Drawing.Point(407, 0);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(75, 23);
            this.button13.TabIndex = 8;
            this.button13.Text = "button13";
            this.button13.UseVisualStyleBackColor = true;
            this.button13.Click += new System.EventHandler(this.button13_Click);
            // 
            // button4
            // 
            this.button4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.button4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.Location = new System.Drawing.Point(800, 400);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(46, 20);
            this.button4.TabIndex = 7;
            this.button4.Text = "Save";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // richTextBox1
            // 
            this.richTextBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.richTextBox1.Font = new System.Drawing.Font("Lucida Console", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox1.ForeColor = System.Drawing.Color.Blue;
            this.richTextBox1.Location = new System.Drawing.Point(13, 255);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(832, 139);
            this.richTextBox1.TabIndex = 6;
            this.richTextBox1.Text = "";
            // 
            // button3
            // 
            this.button3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.Location = new System.Drawing.Point(13, 226);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(832, 23);
            this.button3.TabIndex = 5;
            this.button3.Text = "2. Write SQL Script (INSERT)";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button2
            // 
            this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(13, 32);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(832, 23);
            this.button2.TabIndex = 4;
            this.button2.Text = "1. Read in CSV File to below view";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // listView1
            // 
            this.listView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.chCompanyName,
            this.chCompanyABN,
            this.chPhoneNo,
            this.chFaxNo,
            this.chMobNo,
            this.chAddressBusiness,
            this.chAddressPostal,
            this.chSafetyQualified});
            this.listView1.FullRowSelect = true;
            this.listView1.GridLines = true;
            this.listView1.Location = new System.Drawing.Point(13, 61);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(832, 159);
            this.listView1.TabIndex = 3;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            // 
            // chCompanyName
            // 
            this.chCompanyName.Text = "CompanyName";
            this.chCompanyName.Width = 134;
            // 
            // chCompanyABN
            // 
            this.chCompanyABN.Text = "CompanyABN";
            this.chCompanyABN.Width = 78;
            // 
            // chPhoneNo
            // 
            this.chPhoneNo.Text = "PhoneNo";
            // 
            // chFaxNo
            // 
            this.chFaxNo.Text = "FaxNo";
            // 
            // chMobNo
            // 
            this.chMobNo.Text = "MobNo";
            // 
            // chAddressBusiness
            // 
            this.chAddressBusiness.Text = "AddressBusiness";
            this.chAddressBusiness.Width = 184;
            // 
            // chAddressPostal
            // 
            this.chAddressPostal.Text = "AddressPostal";
            this.chAddressPostal.Width = 168;
            // 
            // chSafetyQualified
            // 
            this.chSafetyQualified.Text = "SafetyQualified";
            this.chSafetyQualified.Width = 84;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "From CSV File:";
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.Location = new System.Drawing.Point(820, 4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(25, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "...";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // textBox1
            // 
            this.textBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox1.Location = new System.Drawing.Point(92, 6);
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(727, 20);
            this.textBox1.TabIndex = 0;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.button9);
            this.tabPage2.Controls.Add(this.tabControl2);
            this.tabPage2.Controls.Add(this.button5);
            this.tabPage2.Controls.Add(this.richTextBox2);
            this.tabPage2.Controls.Add(this.button6);
            this.tabPage2.Controls.Add(this.button7);
            this.tabPage2.Controls.Add(this.label2);
            this.tabPage2.Controls.Add(this.button8);
            this.tabPage2.Controls.Add(this.textBox2);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(854, 423);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Import Users";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // button9
            // 
            this.button9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button9.Location = new System.Drawing.Point(17, 392);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(61, 28);
            this.button9.TabIndex = 17;
            this.button9.Text = "Auto-Fit";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button9_Click_1);
            // 
            // tabControl2
            // 
            this.tabControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl2.Controls.Add(this.tabPage4);
            this.tabControl2.Controls.Add(this.tabPage3);
            this.tabControl2.Controls.Add(this.tabPage5);
            this.tabControl2.Location = new System.Drawing.Point(13, 68);
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(832, 159);
            this.tabControl2.TabIndex = 16;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.listView2);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(824, 133);
            this.tabPage4.TabIndex = 1;
            this.tabPage4.Text = "Users (Alcoan)";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // listView2
            // 
            this.listView2.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.chUsersAlcoan_UserLogon,
            this.chUsersAlcoan_LastName,
            this.chUsersAlcoan_FirstName,
            this.chUsersAlcoan_CompanyName,
            this.chUsersAlcoan_CompanyNameId,
            this.chUsersAlcoan_Title,
            this.chUsersAlcoan_PhoneNo,
            this.chUsersAlcoan_FaxNo,
            this.chUsersAlcoan_MobNo,
            this.chUsersAlcoan_Email,
            this.chUsersAlcoan_RoleID});
            this.listView2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listView2.FullRowSelect = true;
            this.listView2.GridLines = true;
            this.listView2.Location = new System.Drawing.Point(3, 3);
            this.listView2.Name = "listView2";
            this.listView2.Size = new System.Drawing.Size(818, 127);
            this.listView2.TabIndex = 11;
            this.listView2.UseCompatibleStateImageBehavior = false;
            this.listView2.View = System.Windows.Forms.View.Details;
            // 
            // chUsersAlcoan_UserLogon
            // 
            this.chUsersAlcoan_UserLogon.Text = "UserLogon";
            this.chUsersAlcoan_UserLogon.Width = 140;
            // 
            // chUsersAlcoan_LastName
            // 
            this.chUsersAlcoan_LastName.Text = "LastName";
            this.chUsersAlcoan_LastName.Width = 78;
            // 
            // chUsersAlcoan_FirstName
            // 
            this.chUsersAlcoan_FirstName.Text = "FirstName";
            this.chUsersAlcoan_FirstName.Width = 75;
            // 
            // chUsersAlcoan_CompanyName
            // 
            this.chUsersAlcoan_CompanyName.Text = "Company";
            this.chUsersAlcoan_CompanyName.Width = 112;
            // 
            // chUsersAlcoan_CompanyNameId
            // 
            this.chUsersAlcoan_CompanyNameId.Text = "#";
            this.chUsersAlcoan_CompanyNameId.Width = 20;
            // 
            // chUsersAlcoan_Title
            // 
            this.chUsersAlcoan_Title.Text = "Title";
            // 
            // chUsersAlcoan_PhoneNo
            // 
            this.chUsersAlcoan_PhoneNo.Text = "PhoneNo";
            this.chUsersAlcoan_PhoneNo.Width = 63;
            // 
            // chUsersAlcoan_FaxNo
            // 
            this.chUsersAlcoan_FaxNo.Text = "FaxNo";
            this.chUsersAlcoan_FaxNo.Width = 59;
            // 
            // chUsersAlcoan_MobNo
            // 
            this.chUsersAlcoan_MobNo.Text = "MobNo";
            this.chUsersAlcoan_MobNo.Width = 53;
            // 
            // chUsersAlcoan_Email
            // 
            this.chUsersAlcoan_Email.Text = "Email";
            this.chUsersAlcoan_Email.Width = 109;
            // 
            // chUsersAlcoan_RoleID
            // 
            this.chUsersAlcoan_RoleID.Text = "RoleID";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.listView3);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(824, 133);
            this.tabPage3.TabIndex = 0;
            this.tabPage3.Text = "Users (AlcoaDirect)";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // listView3
            // 
            this.listView3.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader11,
            this.columnHeader12,
            this.columnHeader13,
            this.columnHeader30,
            this.columnHeader14,
            this.columnHeader15,
            this.columnHeader16,
            this.columnHeader17,
            this.columnHeader18,
            this.columnHeader19,
            this.columnHeader20});
            this.listView3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listView3.FullRowSelect = true;
            this.listView3.GridLines = true;
            this.listView3.Location = new System.Drawing.Point(3, 3);
            this.listView3.Name = "listView3";
            this.listView3.Size = new System.Drawing.Size(818, 127);
            this.listView3.TabIndex = 12;
            this.listView3.UseCompatibleStateImageBehavior = false;
            this.listView3.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader11
            // 
            this.columnHeader11.Text = "UserLogon";
            this.columnHeader11.Width = 140;
            // 
            // columnHeader12
            // 
            this.columnHeader12.Text = "LastName";
            this.columnHeader12.Width = 78;
            // 
            // columnHeader13
            // 
            this.columnHeader13.Text = "FirstName";
            this.columnHeader13.Width = 95;
            // 
            // columnHeader30
            // 
            this.columnHeader30.Text = "Company";
            this.columnHeader30.Width = 103;
            // 
            // columnHeader14
            // 
            this.columnHeader14.Text = "#";
            this.columnHeader14.Width = 20;
            // 
            // columnHeader15
            // 
            this.columnHeader15.Text = "Title";
            // 
            // columnHeader16
            // 
            this.columnHeader16.Text = "PhoneNo";
            this.columnHeader16.Width = 63;
            // 
            // columnHeader17
            // 
            this.columnHeader17.Text = "FaxNo";
            this.columnHeader17.Width = 59;
            // 
            // columnHeader18
            // 
            this.columnHeader18.Text = "MobNo";
            this.columnHeader18.Width = 53;
            // 
            // columnHeader19
            // 
            this.columnHeader19.Text = "Email";
            this.columnHeader19.Width = 109;
            // 
            // columnHeader20
            // 
            this.columnHeader20.Text = "RoleID";
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.listView4);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Size = new System.Drawing.Size(824, 133);
            this.tabPage5.TabIndex = 2;
            this.tabPage5.Text = "Contractor Contacts List";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // listView4
            // 
            this.listView4.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader31,
            this.columnHeader21,
            this.columnHeader22,
            this.columnHeader23,
            this.columnHeader24,
            this.columnHeader25,
            this.columnHeader26,
            this.columnHeader27,
            this.columnHeader28,
            this.columnHeader29});
            this.listView4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listView4.FullRowSelect = true;
            this.listView4.GridLines = true;
            this.listView4.Location = new System.Drawing.Point(0, 0);
            this.listView4.Name = "listView4";
            this.listView4.Size = new System.Drawing.Size(824, 133);
            this.listView4.TabIndex = 12;
            this.listView4.UseCompatibleStateImageBehavior = false;
            this.listView4.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader31
            // 
            this.columnHeader31.Text = "Company";
            this.columnHeader31.Width = 175;
            // 
            // columnHeader21
            // 
            this.columnHeader21.Text = "#";
            this.columnHeader21.Width = 26;
            // 
            // columnHeader22
            // 
            this.columnHeader22.Text = "ContactFirstName";
            this.columnHeader22.Width = 97;
            // 
            // columnHeader23
            // 
            this.columnHeader23.Text = "ContactLastName";
            this.columnHeader23.Width = 97;
            // 
            // columnHeader24
            // 
            this.columnHeader24.Text = "ContactRole";
            this.columnHeader24.Width = 72;
            // 
            // columnHeader25
            // 
            this.columnHeader25.Text = "ContactTitle";
            this.columnHeader25.Width = 72;
            // 
            // columnHeader26
            // 
            this.columnHeader26.Text = "ContactEmail";
            this.columnHeader26.Width = 75;
            // 
            // columnHeader27
            // 
            this.columnHeader27.Text = "ContactMobile";
            this.columnHeader27.Width = 80;
            // 
            // columnHeader28
            // 
            this.columnHeader28.Text = "ContactPhone";
            this.columnHeader28.Width = 85;
            // 
            // columnHeader29
            // 
            this.columnHeader29.Text = "SiteId";
            this.columnHeader29.Width = 41;
            // 
            // button5
            // 
            this.button5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.button5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button5.Location = new System.Drawing.Point(799, 392);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(46, 20);
            this.button5.TabIndex = 15;
            this.button5.Text = "Save";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // richTextBox2
            // 
            this.richTextBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.richTextBox2.Font = new System.Drawing.Font("Lucida Console", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox2.ForeColor = System.Drawing.Color.Blue;
            this.richTextBox2.Location = new System.Drawing.Point(13, 262);
            this.richTextBox2.Name = "richTextBox2";
            this.richTextBox2.Size = new System.Drawing.Size(832, 124);
            this.richTextBox2.TabIndex = 14;
            this.richTextBox2.Text = "";
            // 
            // button6
            // 
            this.button6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.button6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button6.Location = new System.Drawing.Point(13, 233);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(832, 23);
            this.button6.TabIndex = 13;
            this.button6.Text = "2. Write SQL Script (INSERT)";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button7
            // 
            this.button7.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.button7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button7.Location = new System.Drawing.Point(13, 39);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(832, 23);
            this.button7.TabIndex = 12;
            this.button7.Text = "1. Read in CSV File to below view";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(10, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(76, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "From CSV File:";
            // 
            // button8
            // 
            this.button8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button8.Location = new System.Drawing.Point(820, 11);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(25, 23);
            this.button8.TabIndex = 9;
            this.button8.Text = "...";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // textBox2
            // 
            this.textBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox2.Location = new System.Drawing.Point(92, 13);
            this.textBox2.Name = "textBox2";
            this.textBox2.ReadOnly = true;
            this.textBox2.Size = new System.Drawing.Size(727, 20);
            this.textBox2.TabIndex = 8;
            // 
            // tabPage8
            // 
            this.tabPage8.Controls.Add(this.button25);
            this.tabPage8.Controls.Add(this.button14);
            this.tabPage8.Controls.Add(this.tabControl3);
            this.tabPage8.Controls.Add(this.button15);
            this.tabPage8.Controls.Add(this.richTextBox4);
            this.tabPage8.Controls.Add(this.button16);
            this.tabPage8.Controls.Add(this.button17);
            this.tabPage8.Controls.Add(this.label4);
            this.tabPage8.Controls.Add(this.button18);
            this.tabPage8.Controls.Add(this.textBox4);
            this.tabPage8.Location = new System.Drawing.Point(4, 22);
            this.tabPage8.Name = "tabPage8";
            this.tabPage8.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage8.Size = new System.Drawing.Size(854, 423);
            this.tabPage8.TabIndex = 4;
            this.tabPage8.Text = "Import Alcoan Users";
            this.tabPage8.UseVisualStyleBackColor = true;
            // 
            // button14
            // 
            this.button14.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button14.Location = new System.Drawing.Point(17, 388);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(61, 28);
            this.button14.TabIndex = 26;
            this.button14.Text = "Auto-Fit";
            this.button14.UseVisualStyleBackColor = true;
            // 
            // tabControl3
            // 
            this.tabControl3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl3.Controls.Add(this.tabPage9);
            this.tabControl3.Controls.Add(this.tabPage11);
            this.tabControl3.Location = new System.Drawing.Point(13, 64);
            this.tabControl3.Name = "tabControl3";
            this.tabControl3.SelectedIndex = 0;
            this.tabControl3.Size = new System.Drawing.Size(832, 159);
            this.tabControl3.TabIndex = 25;
            // 
            // tabPage9
            // 
            this.tabPage9.Controls.Add(this.listView7);
            this.tabPage9.Location = new System.Drawing.Point(4, 22);
            this.tabPage9.Name = "tabPage9";
            this.tabPage9.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage9.Size = new System.Drawing.Size(824, 133);
            this.tabPage9.TabIndex = 1;
            this.tabPage9.Text = "Users (Alcoan)";
            this.tabPage9.UseVisualStyleBackColor = true;
            // 
            // listView7
            // 
            this.listView7.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader44,
            this.columnHeader45,
            this.columnHeader46,
            this.columnHeader47,
            this.columnHeader48,
            this.columnHeader49,
            this.columnHeader50,
            this.columnHeader51,
            this.columnHeader54,
            this.columnHeader55,
            this.columnHeader56,
            this.columnHeader78});
            this.listView7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listView7.FullRowSelect = true;
            this.listView7.GridLines = true;
            this.listView7.Location = new System.Drawing.Point(3, 3);
            this.listView7.Name = "listView7";
            this.listView7.Size = new System.Drawing.Size(818, 127);
            this.listView7.TabIndex = 11;
            this.listView7.UseCompatibleStateImageBehavior = false;
            this.listView7.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader44
            // 
            this.columnHeader44.Text = "UserLogon";
            this.columnHeader44.Width = 140;
            // 
            // columnHeader45
            // 
            this.columnHeader45.Text = "LastName";
            this.columnHeader45.Width = 78;
            // 
            // columnHeader46
            // 
            this.columnHeader46.Text = "FirstName";
            this.columnHeader46.Width = 75;
            // 
            // columnHeader47
            // 
            this.columnHeader47.Text = "Company";
            this.columnHeader47.Width = 112;
            // 
            // columnHeader48
            // 
            this.columnHeader48.Text = "#";
            this.columnHeader48.Width = 20;
            // 
            // columnHeader49
            // 
            this.columnHeader49.Text = "Position";
            // 
            // columnHeader50
            // 
            this.columnHeader50.Text = "PhoneNo";
            this.columnHeader50.Width = 63;
            // 
            // columnHeader51
            // 
            this.columnHeader51.Text = "FaxNo";
            this.columnHeader51.Width = 59;
            // 
            // columnHeader54
            // 
            this.columnHeader54.Text = "MobNo";
            this.columnHeader54.Width = 53;
            // 
            // columnHeader55
            // 
            this.columnHeader55.Text = "Email";
            this.columnHeader55.Width = 109;
            // 
            // columnHeader56
            // 
            this.columnHeader56.Text = "RoleID";
            // 
            // columnHeader78
            // 
            this.columnHeader78.Text = "existsAlready?";
            // 
            // tabPage11
            // 
            this.tabPage11.Controls.Add(this.listView9);
            this.tabPage11.Location = new System.Drawing.Point(4, 22);
            this.tabPage11.Name = "tabPage11";
            this.tabPage11.Size = new System.Drawing.Size(824, 133);
            this.tabPage11.TabIndex = 2;
            this.tabPage11.Text = "Alcoa Contacts List";
            this.tabPage11.UseVisualStyleBackColor = true;
            // 
            // listView9
            // 
            this.listView9.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader68,
            this.columnHeader69,
            this.columnHeader70,
            this.columnHeader71,
            this.columnHeader72,
            this.columnHeader74,
            this.columnHeader76});
            this.listView9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listView9.FullRowSelect = true;
            this.listView9.GridLines = true;
            this.listView9.Location = new System.Drawing.Point(0, 0);
            this.listView9.Name = "listView9";
            this.listView9.Size = new System.Drawing.Size(824, 133);
            this.listView9.TabIndex = 12;
            this.listView9.UseCompatibleStateImageBehavior = false;
            this.listView9.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader68
            // 
            this.columnHeader68.Text = "#";
            this.columnHeader68.Width = 42;
            // 
            // columnHeader69
            // 
            this.columnHeader69.Text = "Site";
            this.columnHeader69.Width = 96;
            // 
            // columnHeader70
            // 
            this.columnHeader70.Text = "ContactFirstName";
            this.columnHeader70.Width = 97;
            // 
            // columnHeader71
            // 
            this.columnHeader71.Text = "ContactLastName";
            this.columnHeader71.Width = 97;
            // 
            // columnHeader72
            // 
            this.columnHeader72.Text = "ContactPositionRole";
            this.columnHeader72.Width = 72;
            // 
            // columnHeader74
            // 
            this.columnHeader74.Text = "ContactEmail";
            this.columnHeader74.Width = 75;
            // 
            // columnHeader76
            // 
            this.columnHeader76.Text = "ContactPhone";
            this.columnHeader76.Width = 85;
            // 
            // button15
            // 
            this.button15.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.button15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button15.Location = new System.Drawing.Point(799, 388);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(46, 20);
            this.button15.TabIndex = 24;
            this.button15.Text = "Save";
            this.button15.UseVisualStyleBackColor = true;
            // 
            // richTextBox4
            // 
            this.richTextBox4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.richTextBox4.Font = new System.Drawing.Font("Lucida Console", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox4.ForeColor = System.Drawing.Color.Blue;
            this.richTextBox4.Location = new System.Drawing.Point(13, 258);
            this.richTextBox4.Name = "richTextBox4";
            this.richTextBox4.Size = new System.Drawing.Size(832, 124);
            this.richTextBox4.TabIndex = 23;
            this.richTextBox4.Text = "";
            // 
            // button16
            // 
            this.button16.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.button16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button16.Location = new System.Drawing.Point(13, 229);
            this.button16.Name = "button16";
            this.button16.Size = new System.Drawing.Size(832, 23);
            this.button16.TabIndex = 22;
            this.button16.Text = "2. Write SQL Script (INSERT)";
            this.button16.UseVisualStyleBackColor = true;
            this.button16.Click += new System.EventHandler(this.button16_Click);
            // 
            // button17
            // 
            this.button17.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.button17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button17.Location = new System.Drawing.Point(13, 35);
            this.button17.Name = "button17";
            this.button17.Size = new System.Drawing.Size(832, 23);
            this.button17.TabIndex = 21;
            this.button17.Text = "1. Read in CSV File to below view";
            this.button17.UseVisualStyleBackColor = true;
            this.button17.Click += new System.EventHandler(this.button17_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(10, 12);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(76, 13);
            this.label4.TabIndex = 20;
            this.label4.Text = "From CSV File:";
            // 
            // button18
            // 
            this.button18.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button18.Location = new System.Drawing.Point(820, 7);
            this.button18.Name = "button18";
            this.button18.Size = new System.Drawing.Size(25, 23);
            this.button18.TabIndex = 19;
            this.button18.Text = "...";
            this.button18.UseVisualStyleBackColor = true;
            this.button18.Click += new System.EventHandler(this.button18_Click);
            // 
            // textBox4
            // 
            this.textBox4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox4.Location = new System.Drawing.Point(92, 9);
            this.textBox4.Name = "textBox4";
            this.textBox4.ReadOnly = true;
            this.textBox4.Size = new System.Drawing.Size(727, 20);
            this.textBox4.TabIndex = 18;
            // 
            // tabPage7
            // 
            this.tabPage7.Controls.Add(this.button12);
            this.tabPage7.Controls.Add(this.richTextBox3);
            this.tabPage7.Controls.Add(this.button10);
            this.tabPage7.Controls.Add(this.button11);
            this.tabPage7.Controls.Add(this.listView6);
            this.tabPage7.Controls.Add(this.label3);
            this.tabPage7.Controls.Add(this.textBox3);
            this.tabPage7.Location = new System.Drawing.Point(4, 22);
            this.tabPage7.Name = "tabPage7";
            this.tabPage7.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage7.Size = new System.Drawing.Size(854, 423);
            this.tabPage7.TabIndex = 3;
            this.tabPage7.Text = "Residential";
            this.tabPage7.UseVisualStyleBackColor = true;
            // 
            // button12
            // 
            this.button12.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button12.Location = new System.Drawing.Point(820, 14);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(25, 23);
            this.button12.TabIndex = 13;
            this.button12.Text = "...";
            this.button12.UseVisualStyleBackColor = true;
            this.button12.Click += new System.EventHandler(this.button12_Click);
            // 
            // richTextBox3
            // 
            this.richTextBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.richTextBox3.Font = new System.Drawing.Font("Lucida Console", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox3.ForeColor = System.Drawing.Color.Blue;
            this.richTextBox3.Location = new System.Drawing.Point(13, 266);
            this.richTextBox3.Name = "richTextBox3";
            this.richTextBox3.Size = new System.Drawing.Size(832, 139);
            this.richTextBox3.TabIndex = 12;
            this.richTextBox3.Text = "";
            // 
            // button10
            // 
            this.button10.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.button10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button10.Location = new System.Drawing.Point(13, 237);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(832, 23);
            this.button10.TabIndex = 11;
            this.button10.Text = "2. Write SQL Script (INSERT)";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // button11
            // 
            this.button11.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.button11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button11.Location = new System.Drawing.Point(13, 43);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(832, 23);
            this.button11.TabIndex = 10;
            this.button11.Text = "1. Read in CSV File to below view";
            this.button11.UseVisualStyleBackColor = true;
            this.button11.Click += new System.EventHandler(this.button11_Click);
            // 
            // listView6
            // 
            this.listView6.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.listView6.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader52,
            this.columnHeader53,
            this.columnHeader57,
            this.columnHeader58,
            this.columnHeader59});
            this.listView6.FullRowSelect = true;
            this.listView6.GridLines = true;
            this.listView6.Location = new System.Drawing.Point(13, 72);
            this.listView6.Name = "listView6";
            this.listView6.Size = new System.Drawing.Size(832, 159);
            this.listView6.TabIndex = 9;
            this.listView6.UseCompatibleStateImageBehavior = false;
            this.listView6.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader52
            // 
            this.columnHeader52.Text = "Company Name";
            this.columnHeader52.Width = 220;
            // 
            // columnHeader53
            // 
            this.columnHeader53.Text = "Site";
            this.columnHeader53.Width = 184;
            // 
            // columnHeader57
            // 
            this.columnHeader57.Text = "Residential Status";
            this.columnHeader57.Width = 143;
            // 
            // columnHeader58
            // 
            this.columnHeader58.Text = "CompanyId";
            this.columnHeader58.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // columnHeader59
            // 
            this.columnHeader59.Text = "SiteId";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(10, 20);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(76, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "From CSV File:";
            // 
            // textBox3
            // 
            this.textBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox3.Location = new System.Drawing.Point(92, 17);
            this.textBox3.Name = "textBox3";
            this.textBox3.ReadOnly = true;
            this.textBox3.Size = new System.Drawing.Size(727, 20);
            this.textBox3.TabIndex = 7;
            // 
            // tabPage10
            // 
            this.tabPage10.Controls.Add(this.richTextBox5);
            this.tabPage10.Controls.Add(this.button20);
            this.tabPage10.Controls.Add(this.textBox5);
            this.tabPage10.Controls.Add(this.button19);
            this.tabPage10.Controls.Add(this.listView8);
            this.tabPage10.Location = new System.Drawing.Point(4, 22);
            this.tabPage10.Name = "tabPage10";
            this.tabPage10.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage10.Size = new System.Drawing.Size(854, 423);
            this.tabPage10.TabIndex = 5;
            this.tabPage10.Text = "Parse RoboCopy Log(s)";
            this.tabPage10.UseVisualStyleBackColor = true;
            // 
            // richTextBox5
            // 
            this.richTextBox5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.richTextBox5.Location = new System.Drawing.Point(543, 35);
            this.richTextBox5.Name = "richTextBox5";
            this.richTextBox5.Size = new System.Drawing.Size(308, 380);
            this.richTextBox5.TabIndex = 22;
            this.richTextBox5.Text = "";
            // 
            // button20
            // 
            this.button20.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button20.Location = new System.Drawing.Point(743, 6);
            this.button20.Name = "button20";
            this.button20.Size = new System.Drawing.Size(25, 23);
            this.button20.TabIndex = 21;
            this.button20.Text = "...";
            this.button20.UseVisualStyleBackColor = true;
            this.button20.Click += new System.EventHandler(this.button20_Click);
            // 
            // textBox5
            // 
            this.textBox5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox5.Location = new System.Drawing.Point(543, 6);
            this.textBox5.Name = "textBox5";
            this.textBox5.ReadOnly = true;
            this.textBox5.Size = new System.Drawing.Size(194, 20);
            this.textBox5.TabIndex = 20;
            this.textBox5.Text = "W:\\APSS\\DistribLogs";
            // 
            // button19
            // 
            this.button19.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button19.Location = new System.Drawing.Point(774, 6);
            this.button19.Name = "button19";
            this.button19.Size = new System.Drawing.Size(75, 23);
            this.button19.TabIndex = 1;
            this.button19.Text = "Parse";
            this.button19.UseVisualStyleBackColor = true;
            this.button19.Click += new System.EventHandler(this.button19_Click);
            // 
            // listView8
            // 
            this.listView8.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.listView8.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.chTimeStamp,
            this.chFileName,
            this.chFileTag,
            this.chWhatToDo});
            this.listView8.FullRowSelect = true;
            this.listView8.GridLines = true;
            this.listView8.Location = new System.Drawing.Point(8, 6);
            this.listView8.Name = "listView8";
            this.listView8.Size = new System.Drawing.Size(529, 411);
            this.listView8.TabIndex = 0;
            this.listView8.UseCompatibleStateImageBehavior = false;
            this.listView8.View = System.Windows.Forms.View.Details;
            // 
            // chTimeStamp
            // 
            this.chTimeStamp.Text = "TimeStamp";
            this.chTimeStamp.Width = 200;
            // 
            // chFileName
            // 
            this.chFileName.Text = "FileName";
            this.chFileName.Width = 70;
            // 
            // chFileTag
            // 
            this.chFileTag.Text = "FileTag";
            this.chFileTag.Width = 70;
            // 
            // chWhatToDo
            // 
            this.chWhatToDo.Text = "What to do?";
            this.chWhatToDo.Width = 150;
            // 
            // tabPage6
            // 
            this.tabPage6.Controls.Add(this.listView5);
            this.tabPage6.Location = new System.Drawing.Point(4, 22);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage6.Size = new System.Drawing.Size(854, 423);
            this.tabPage6.TabIndex = 2;
            this.tabPage6.Text = "Debug/Error Log";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // listView5
            // 
            this.listView5.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader32,
            this.columnHeader33,
            this.columnHeader34,
            this.columnHeader35,
            this.columnHeader36,
            this.columnHeader37,
            this.columnHeader38,
            this.columnHeader39,
            this.columnHeader40,
            this.columnHeader41,
            this.columnHeader42,
            this.columnHeader43});
            this.listView5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listView5.Location = new System.Drawing.Point(3, 3);
            this.listView5.Name = "listView5";
            this.listView5.Size = new System.Drawing.Size(848, 417);
            this.listView5.TabIndex = 0;
            this.listView5.UseCompatibleStateImageBehavior = false;
            this.listView5.View = System.Windows.Forms.View.Details;
            // 
            // tabPage12
            // 
            this.tabPage12.Controls.Add(this.button24);
            this.tabPage12.Controls.Add(this.button21);
            this.tabPage12.Controls.Add(this.rtbsims);
            this.tabPage12.Controls.Add(this.button22);
            this.tabPage12.Controls.Add(this.button23);
            this.tabPage12.Controls.Add(this.lvsims);
            this.tabPage12.Controls.Add(this.label5);
            this.tabPage12.Controls.Add(this.tbSimsCSV);
            this.tabPage12.Location = new System.Drawing.Point(4, 22);
            this.tabPage12.Name = "tabPage12";
            this.tabPage12.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage12.Size = new System.Drawing.Size(854, 423);
            this.tabPage12.TabIndex = 6;
            this.tabPage12.Text = "Import EHSIMS codes";
            this.tabPage12.UseVisualStyleBackColor = true;
            // 
            // button24
            // 
            this.button24.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button24.Location = new System.Drawing.Point(819, 1);
            this.button24.Name = "button24";
            this.button24.Size = new System.Drawing.Size(25, 23);
            this.button24.TabIndex = 15;
            this.button24.Text = "...";
            this.button24.UseVisualStyleBackColor = true;
            this.button24.Click += new System.EventHandler(this.button24_Click);
            // 
            // button21
            // 
            this.button21.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.button21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button21.Location = new System.Drawing.Point(799, 398);
            this.button21.Name = "button21";
            this.button21.Size = new System.Drawing.Size(46, 20);
            this.button21.TabIndex = 14;
            this.button21.Text = "Save";
            this.button21.UseVisualStyleBackColor = true;
            // 
            // rtbsims
            // 
            this.rtbsims.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.rtbsims.Font = new System.Drawing.Font("Lucida Console", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtbsims.ForeColor = System.Drawing.Color.Blue;
            this.rtbsims.Location = new System.Drawing.Point(12, 253);
            this.rtbsims.Name = "rtbsims";
            this.rtbsims.Size = new System.Drawing.Size(832, 139);
            this.rtbsims.TabIndex = 13;
            this.rtbsims.Text = "";
            // 
            // button22
            // 
            this.button22.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.button22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button22.Location = new System.Drawing.Point(12, 224);
            this.button22.Name = "button22";
            this.button22.Size = new System.Drawing.Size(832, 23);
            this.button22.TabIndex = 12;
            this.button22.Text = "2. Write SQL Script (INSERT)";
            this.button22.UseVisualStyleBackColor = true;
            this.button22.Click += new System.EventHandler(this.button22_Click);
            // 
            // button23
            // 
            this.button23.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.button23.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button23.Location = new System.Drawing.Point(12, 30);
            this.button23.Name = "button23";
            this.button23.Size = new System.Drawing.Size(832, 23);
            this.button23.TabIndex = 11;
            this.button23.Text = "1. Read in CSV File to below view";
            this.button23.UseVisualStyleBackColor = true;
            this.button23.Click += new System.EventHandler(this.button23_Click);
            // 
            // lvsims
            // 
            this.lvsims.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lvsims.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader60,
            this.columnHeader61,
            this.columnHeader62});
            this.lvsims.FullRowSelect = true;
            this.lvsims.GridLines = true;
            this.lvsims.Location = new System.Drawing.Point(12, 59);
            this.lvsims.Name = "lvsims";
            this.lvsims.Size = new System.Drawing.Size(832, 159);
            this.lvsims.TabIndex = 10;
            this.lvsims.UseCompatibleStateImageBehavior = false;
            this.lvsims.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader60
            // 
            this.columnHeader60.Text = "Location Code";
            this.columnHeader60.Width = 88;
            // 
            // columnHeader61
            // 
            this.columnHeader61.Text = "Cont Company Code";
            this.columnHeader61.Width = 142;
            // 
            // columnHeader62
            // 
            this.columnHeader62.Text = "WAO CSM Company Name";
            this.columnHeader62.Width = 351;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(9, 7);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(76, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "From CSV File:";
            // 
            // tbSimsCSV
            // 
            this.tbSimsCSV.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tbSimsCSV.Location = new System.Drawing.Point(91, 4);
            this.tbSimsCSV.Name = "tbSimsCSV";
            this.tbSimsCSV.ReadOnly = true;
            this.tbSimsCSV.Size = new System.Drawing.Size(727, 20);
            this.tbSimsCSV.TabIndex = 8;
            this.tbSimsCSV.Text = "C:\\TempRead\\ehs sims.csv";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "UserLogon";
            this.columnHeader1.Width = 140;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "LastName";
            this.columnHeader2.Width = 78;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "FirstName";
            this.columnHeader3.Width = 95;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "CompanyNameID";
            this.columnHeader4.Width = 95;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Title";
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "PhoneNo";
            this.columnHeader6.Width = 63;
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "FaxNo";
            this.columnHeader7.Width = 59;
            // 
            // columnHeader8
            // 
            this.columnHeader8.Text = "MobNo";
            this.columnHeader8.Width = 53;
            // 
            // columnHeader9
            // 
            this.columnHeader9.Text = "Email";
            this.columnHeader9.Width = 109;
            // 
            // columnHeader10
            // 
            this.columnHeader10.Text = "RoleID";
            // 
            // button25
            // 
            this.button25.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.button25.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button25.Location = new System.Drawing.Point(362, 258);
            this.button25.Name = "button25";
            this.button25.Size = new System.Drawing.Size(392, 23);
            this.button25.TabIndex = 23;
            this.button25.Text = "2. Write SQL Script (INSERT) for Pre-Qual";
            this.button25.UseVisualStyleBackColor = true;
            this.button25.Click += new System.EventHandler(this.button25_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(862, 449);
            this.Controls.Add(this.tabControl1);
            this.IsMdiContainer = true;
            this.Name = "Form1";
            this.Text = "ALCOA.WAO.CSMWP.Scripts v0.1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabControl2.ResumeLayout(false);
            this.tabPage4.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.tabPage5.ResumeLayout(false);
            this.tabPage8.ResumeLayout(false);
            this.tabPage8.PerformLayout();
            this.tabControl3.ResumeLayout(false);
            this.tabPage9.ResumeLayout(false);
            this.tabPage11.ResumeLayout(false);
            this.tabPage7.ResumeLayout(false);
            this.tabPage7.PerformLayout();
            this.tabPage10.ResumeLayout(false);
            this.tabPage10.PerformLayout();
            this.tabPage6.ResumeLayout(false);
            this.tabPage12.ResumeLayout(false);
            this.tabPage12.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.ColumnHeader chCompanyName;
        private System.Windows.Forms.ColumnHeader chCompanyABN;
        private System.Windows.Forms.ColumnHeader chPhoneNo;
        private System.Windows.Forms.ColumnHeader chFaxNo;
        private System.Windows.Forms.ColumnHeader chMobNo;
        private System.Windows.Forms.ColumnHeader chAddressBusiness;
        private System.Windows.Forms.ColumnHeader chAddressPostal;
        private System.Windows.Forms.ColumnHeader chSafetyQualified;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.TabControl tabControl2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.RichTextBox richTextBox2;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.ListView listView2;
        private System.Windows.Forms.ColumnHeader chUsersAlcoan_UserLogon;
        private System.Windows.Forms.ColumnHeader chUsersAlcoan_LastName;
        private System.Windows.Forms.ColumnHeader chUsersAlcoan_FirstName;
        private System.Windows.Forms.ColumnHeader chUsersAlcoan_CompanyNameId;
        private System.Windows.Forms.ColumnHeader chUsersAlcoan_Title;
        private System.Windows.Forms.ColumnHeader chUsersAlcoan_PhoneNo;
        private System.Windows.Forms.ColumnHeader chUsersAlcoan_FaxNo;
        private System.Windows.Forms.ColumnHeader chUsersAlcoan_MobNo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.ColumnHeader chUsersAlcoan_Email;
        private System.Windows.Forms.ColumnHeader chUsersAlcoan_RoleID;
        private System.Windows.Forms.ListView listView3;
        private System.Windows.Forms.ColumnHeader columnHeader11;
        private System.Windows.Forms.ColumnHeader columnHeader12;
        private System.Windows.Forms.ColumnHeader columnHeader13;
        private System.Windows.Forms.ColumnHeader columnHeader14;
        private System.Windows.Forms.ColumnHeader columnHeader15;
        private System.Windows.Forms.ColumnHeader columnHeader16;
        private System.Windows.Forms.ColumnHeader columnHeader17;
        private System.Windows.Forms.ColumnHeader columnHeader18;
        private System.Windows.Forms.ColumnHeader columnHeader19;
        private System.Windows.Forms.ColumnHeader columnHeader20;
        private System.Windows.Forms.ListView listView4;
        private System.Windows.Forms.ColumnHeader columnHeader21;
        private System.Windows.Forms.ColumnHeader columnHeader22;
        private System.Windows.Forms.ColumnHeader columnHeader23;
        private System.Windows.Forms.ColumnHeader columnHeader24;
        private System.Windows.Forms.ColumnHeader columnHeader25;
        private System.Windows.Forms.ColumnHeader columnHeader26;
        private System.Windows.Forms.ColumnHeader columnHeader27;
        private System.Windows.Forms.ColumnHeader columnHeader28;
        private System.Windows.Forms.ColumnHeader columnHeader29;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.ColumnHeader columnHeader8;
        private System.Windows.Forms.ColumnHeader columnHeader9;
        private System.Windows.Forms.ColumnHeader columnHeader10;
        private System.Windows.Forms.ColumnHeader chUsersAlcoan_CompanyName;
        private System.Windows.Forms.ColumnHeader columnHeader30;
        private System.Windows.Forms.ColumnHeader columnHeader31;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.ListView listView5;
        private System.Windows.Forms.ColumnHeader columnHeader32;
        private System.Windows.Forms.ColumnHeader columnHeader33;
        private System.Windows.Forms.ColumnHeader columnHeader34;
        private System.Windows.Forms.ColumnHeader columnHeader35;
        private System.Windows.Forms.ColumnHeader columnHeader36;
        private System.Windows.Forms.ColumnHeader columnHeader37;
        private System.Windows.Forms.ColumnHeader columnHeader38;
        private System.Windows.Forms.ColumnHeader columnHeader39;
        private System.Windows.Forms.ColumnHeader columnHeader40;
        private System.Windows.Forms.ColumnHeader columnHeader41;
        private System.Windows.Forms.ColumnHeader columnHeader42;
        private System.Windows.Forms.ColumnHeader columnHeader43;
        private System.Windows.Forms.TabPage tabPage7;
        private System.Windows.Forms.RichTextBox richTextBox3;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.ListView listView6;
        private System.Windows.Forms.ColumnHeader columnHeader52;
        private System.Windows.Forms.ColumnHeader columnHeader53;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.TabPage tabPage8;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.TabControl tabControl3;
        private System.Windows.Forms.TabPage tabPage9;
        private System.Windows.Forms.ListView listView7;
        private System.Windows.Forms.ColumnHeader columnHeader44;
        private System.Windows.Forms.ColumnHeader columnHeader45;
        private System.Windows.Forms.ColumnHeader columnHeader46;
        private System.Windows.Forms.ColumnHeader columnHeader47;
        private System.Windows.Forms.ColumnHeader columnHeader48;
        private System.Windows.Forms.ColumnHeader columnHeader49;
        private System.Windows.Forms.ColumnHeader columnHeader50;
        private System.Windows.Forms.ColumnHeader columnHeader51;
        private System.Windows.Forms.ColumnHeader columnHeader54;
        private System.Windows.Forms.ColumnHeader columnHeader55;
        private System.Windows.Forms.ColumnHeader columnHeader56;
        private System.Windows.Forms.TabPage tabPage11;
        private System.Windows.Forms.ListView listView9;
        private System.Windows.Forms.ColumnHeader columnHeader68;
        private System.Windows.Forms.ColumnHeader columnHeader69;
        private System.Windows.Forms.ColumnHeader columnHeader70;
        private System.Windows.Forms.ColumnHeader columnHeader71;
        private System.Windows.Forms.ColumnHeader columnHeader72;
        private System.Windows.Forms.ColumnHeader columnHeader74;
        private System.Windows.Forms.ColumnHeader columnHeader76;
        private System.Windows.Forms.Button button15;
        private System.Windows.Forms.RichTextBox richTextBox4;
        private System.Windows.Forms.Button button16;
        private System.Windows.Forms.Button button17;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button button18;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.ColumnHeader columnHeader78;
        private System.Windows.Forms.ColumnHeader columnHeader57;
        private System.Windows.Forms.ColumnHeader columnHeader58;
        private System.Windows.Forms.ColumnHeader columnHeader59;
        private System.Windows.Forms.TabPage tabPage10;
        private System.Windows.Forms.RichTextBox richTextBox5;
        private System.Windows.Forms.Button button20;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.Button button19;
        private System.Windows.Forms.ListView listView8;
        private System.Windows.Forms.ColumnHeader chTimeStamp;
        private System.Windows.Forms.ColumnHeader chFileName;
        private System.Windows.Forms.ColumnHeader chFileTag;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.ColumnHeader chWhatToDo;
        private System.Windows.Forms.TabPage tabPage12;
        private System.Windows.Forms.Button button21;
        private System.Windows.Forms.RichTextBox rtbsims;
        private System.Windows.Forms.Button button22;
        private System.Windows.Forms.Button button23;
        private System.Windows.Forms.ListView lvsims;
        private System.Windows.Forms.ColumnHeader columnHeader60;
        private System.Windows.Forms.ColumnHeader columnHeader61;
        private System.Windows.Forms.ColumnHeader columnHeader62;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tbSimsCSV;
        private System.Windows.Forms.Button button24;
        private System.Windows.Forms.Button button25;


    }
}

