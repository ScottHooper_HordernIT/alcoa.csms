//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Repo.CSMS.DAL.EntityModels
{
    using System;
    using System.Collections.Generic;
    
    public partial class CompanyContractorReviewAudit
    {
        public int CompanyContractorReviewAuditId { get; set; }
        public string CompanyAbn { get; set; }
        public decimal CWKNumber { get; set; }
        public System.DateTime ReviewDate { get; set; }
        public int CompanyId { get; set; }
        public int LabourClassTypeId { get; set; }
        public bool AlcoaRecordActive { get; set; }
        public bool ActiveInduction { get; set; }
        public bool StillAtCompany { get; set; }
        public bool ActiveAtAlcoa { get; set; }
        public string Comments { get; set; }
        public bool Changed { get; set; }
        public Nullable<int> ChangedByUserId { get; set; }
        public Nullable<System.DateTime> ChangedDate { get; set; }
        public bool Processed { get; set; }
        public Nullable<int> ProcessedByUserId { get; set; }
        public Nullable<System.DateTime> ProcessedDate { get; set; }
        public Nullable<int> LastUpdateByUserId { get; set; }
        public System.DateTime LastUpdateDate { get; set; }
        public int LastUpdateReasonTypeId { get; set; }
    }
}
