﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="SafetyPlansSeQuestionsProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class SafetyPlansSeQuestionsProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.SafetyPlansSeQuestions, KaiZen.CSMS.Entities.SafetyPlansSeQuestionsKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.SafetyPlansSeQuestionsKey key)
		{
			return Delete(transactionManager, key.QuestionId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_questionId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _questionId)
		{
			return Delete(null, _questionId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _questionId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.SafetyPlansSeQuestions Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.SafetyPlansSeQuestionsKey key, int start, int pageLength)
		{
			return GetByQuestionId(transactionManager, key.QuestionId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_SafetyPlans_SE_Questions index.
		/// </summary>
		/// <param name="_questionId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.SafetyPlansSeQuestions"/> class.</returns>
		public KaiZen.CSMS.Entities.SafetyPlansSeQuestions GetByQuestionId(System.Int32 _questionId)
		{
			int count = -1;
			return GetByQuestionId(null,_questionId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SafetyPlans_SE_Questions index.
		/// </summary>
		/// <param name="_questionId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.SafetyPlansSeQuestions"/> class.</returns>
		public KaiZen.CSMS.Entities.SafetyPlansSeQuestions GetByQuestionId(System.Int32 _questionId, int start, int pageLength)
		{
			int count = -1;
			return GetByQuestionId(null, _questionId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SafetyPlans_SE_Questions index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.SafetyPlansSeQuestions"/> class.</returns>
		public KaiZen.CSMS.Entities.SafetyPlansSeQuestions GetByQuestionId(TransactionManager transactionManager, System.Int32 _questionId)
		{
			int count = -1;
			return GetByQuestionId(transactionManager, _questionId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SafetyPlans_SE_Questions index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.SafetyPlansSeQuestions"/> class.</returns>
		public KaiZen.CSMS.Entities.SafetyPlansSeQuestions GetByQuestionId(TransactionManager transactionManager, System.Int32 _questionId, int start, int pageLength)
		{
			int count = -1;
			return GetByQuestionId(transactionManager, _questionId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SafetyPlans_SE_Questions index.
		/// </summary>
		/// <param name="_questionId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.SafetyPlansSeQuestions"/> class.</returns>
		public KaiZen.CSMS.Entities.SafetyPlansSeQuestions GetByQuestionId(System.Int32 _questionId, int start, int pageLength, out int count)
		{
			return GetByQuestionId(null, _questionId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SafetyPlans_SE_Questions index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.SafetyPlansSeQuestions"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.SafetyPlansSeQuestions GetByQuestionId(TransactionManager transactionManager, System.Int32 _questionId, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_SafetyPlans_SE_Questions index.
		/// </summary>
		/// <param name="_question"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.SafetyPlansSeQuestions"/> class.</returns>
		public KaiZen.CSMS.Entities.SafetyPlansSeQuestions GetByQuestion(System.String _question)
		{
			int count = -1;
			return GetByQuestion(null,_question, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_SafetyPlans_SE_Questions index.
		/// </summary>
		/// <param name="_question"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.SafetyPlansSeQuestions"/> class.</returns>
		public KaiZen.CSMS.Entities.SafetyPlansSeQuestions GetByQuestion(System.String _question, int start, int pageLength)
		{
			int count = -1;
			return GetByQuestion(null, _question, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_SafetyPlans_SE_Questions index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_question"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.SafetyPlansSeQuestions"/> class.</returns>
		public KaiZen.CSMS.Entities.SafetyPlansSeQuestions GetByQuestion(TransactionManager transactionManager, System.String _question)
		{
			int count = -1;
			return GetByQuestion(transactionManager, _question, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_SafetyPlans_SE_Questions index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_question"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.SafetyPlansSeQuestions"/> class.</returns>
		public KaiZen.CSMS.Entities.SafetyPlansSeQuestions GetByQuestion(TransactionManager transactionManager, System.String _question, int start, int pageLength)
		{
			int count = -1;
			return GetByQuestion(transactionManager, _question, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_SafetyPlans_SE_Questions index.
		/// </summary>
		/// <param name="_question"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.SafetyPlansSeQuestions"/> class.</returns>
		public KaiZen.CSMS.Entities.SafetyPlansSeQuestions GetByQuestion(System.String _question, int start, int pageLength, out int count)
		{
			return GetByQuestion(null, _question, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_SafetyPlans_SE_Questions index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_question"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.SafetyPlansSeQuestions"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.SafetyPlansSeQuestions GetByQuestion(TransactionManager transactionManager, System.String _question, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;SafetyPlansSeQuestions&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;SafetyPlansSeQuestions&gt;"/></returns>
		public static TList<SafetyPlansSeQuestions> Fill(IDataReader reader, TList<SafetyPlansSeQuestions> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.SafetyPlansSeQuestions c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("SafetyPlansSeQuestions")
					.Append("|").Append((System.Int32)reader[((int)SafetyPlansSeQuestionsColumn.QuestionId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<SafetyPlansSeQuestions>(
					key.ToString(), // EntityTrackingKey
					"SafetyPlansSeQuestions",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.SafetyPlansSeQuestions();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.QuestionId = (System.Int32)reader[((int)SafetyPlansSeQuestionsColumn.QuestionId - 1)];
					c.OriginalQuestionId = c.QuestionId;
					c.Question = (System.String)reader[((int)SafetyPlansSeQuestionsColumn.Question - 1)];
					c.QuestionText = (reader.IsDBNull(((int)SafetyPlansSeQuestionsColumn.QuestionText - 1)))?null:(System.Byte[])reader[((int)SafetyPlansSeQuestionsColumn.QuestionText - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.SafetyPlansSeQuestions"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.SafetyPlansSeQuestions"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.SafetyPlansSeQuestions entity)
		{
			if (!reader.Read()) return;
			
			entity.QuestionId = (System.Int32)reader[((int)SafetyPlansSeQuestionsColumn.QuestionId - 1)];
			entity.OriginalQuestionId = (System.Int32)reader["QuestionId"];
			entity.Question = (System.String)reader[((int)SafetyPlansSeQuestionsColumn.Question - 1)];
			entity.QuestionText = (reader.IsDBNull(((int)SafetyPlansSeQuestionsColumn.QuestionText - 1)))?null:(System.Byte[])reader[((int)SafetyPlansSeQuestionsColumn.QuestionText - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.SafetyPlansSeQuestions"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.SafetyPlansSeQuestions"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.SafetyPlansSeQuestions entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.QuestionId = (System.Int32)dataRow["QuestionId"];
			entity.OriginalQuestionId = (System.Int32)dataRow["QuestionId"];
			entity.Question = (System.String)dataRow["Question"];
			entity.QuestionText = Convert.IsDBNull(dataRow["QuestionText"]) ? null : (System.Byte[])dataRow["QuestionText"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.SafetyPlansSeQuestions"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.SafetyPlansSeQuestions Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.SafetyPlansSeQuestions entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetByQuestionId methods when available
			
			#region SafetyPlansSeAnswersCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<SafetyPlansSeAnswers>|SafetyPlansSeAnswersCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'SafetyPlansSeAnswersCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.SafetyPlansSeAnswersCollection = DataRepository.SafetyPlansSeAnswersProvider.GetByQuestionId(transactionManager, entity.QuestionId);

				if (deep && entity.SafetyPlansSeAnswersCollection.Count > 0)
				{
					deepHandles.Add("SafetyPlansSeAnswersCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<SafetyPlansSeAnswers>) DataRepository.SafetyPlansSeAnswersProvider.DeepLoad,
						new object[] { transactionManager, entity.SafetyPlansSeAnswersCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.SafetyPlansSeQuestions object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.SafetyPlansSeQuestions instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.SafetyPlansSeQuestions Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.SafetyPlansSeQuestions entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<SafetyPlansSeAnswers>
				if (CanDeepSave(entity.SafetyPlansSeAnswersCollection, "List<SafetyPlansSeAnswers>|SafetyPlansSeAnswersCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(SafetyPlansSeAnswers child in entity.SafetyPlansSeAnswersCollection)
					{
						if(child.QuestionIdSource != null)
						{
							child.QuestionId = child.QuestionIdSource.QuestionId;
						}
						else
						{
							child.QuestionId = entity.QuestionId;
						}

					}

					if (entity.SafetyPlansSeAnswersCollection.Count > 0 || entity.SafetyPlansSeAnswersCollection.DeletedItems.Count > 0)
					{
						//DataRepository.SafetyPlansSeAnswersProvider.Save(transactionManager, entity.SafetyPlansSeAnswersCollection);
						
						deepHandles.Add("SafetyPlansSeAnswersCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< SafetyPlansSeAnswers >) DataRepository.SafetyPlansSeAnswersProvider.DeepSave,
							new object[] { transactionManager, entity.SafetyPlansSeAnswersCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region SafetyPlansSeQuestionsChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.SafetyPlansSeQuestions</c>
	///</summary>
	public enum SafetyPlansSeQuestionsChildEntityTypes
	{

		///<summary>
		/// Collection of <c>SafetyPlansSeQuestions</c> as OneToMany for SafetyPlansSeAnswersCollection
		///</summary>
		[ChildEntityType(typeof(TList<SafetyPlansSeAnswers>))]
		SafetyPlansSeAnswersCollection,
	}
	
	#endregion SafetyPlansSeQuestionsChildEntityTypes
	
	#region SafetyPlansSeQuestionsFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;SafetyPlansSeQuestionsColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SafetyPlansSeQuestions"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SafetyPlansSeQuestionsFilterBuilder : SqlFilterBuilder<SafetyPlansSeQuestionsColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SafetyPlansSeQuestionsFilterBuilder class.
		/// </summary>
		public SafetyPlansSeQuestionsFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the SafetyPlansSeQuestionsFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SafetyPlansSeQuestionsFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SafetyPlansSeQuestionsFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SafetyPlansSeQuestionsFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SafetyPlansSeQuestionsFilterBuilder
	
	#region SafetyPlansSeQuestionsParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;SafetyPlansSeQuestionsColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SafetyPlansSeQuestions"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SafetyPlansSeQuestionsParameterBuilder : ParameterizedSqlFilterBuilder<SafetyPlansSeQuestionsColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SafetyPlansSeQuestionsParameterBuilder class.
		/// </summary>
		public SafetyPlansSeQuestionsParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the SafetyPlansSeQuestionsParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SafetyPlansSeQuestionsParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SafetyPlansSeQuestionsParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SafetyPlansSeQuestionsParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SafetyPlansSeQuestionsParameterBuilder
	
	#region SafetyPlansSeQuestionsSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;SafetyPlansSeQuestionsColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SafetyPlansSeQuestions"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class SafetyPlansSeQuestionsSortBuilder : SqlSortBuilder<SafetyPlansSeQuestionsColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SafetyPlansSeQuestionsSqlSortBuilder class.
		/// </summary>
		public SafetyPlansSeQuestionsSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion SafetyPlansSeQuestionsSortBuilder
	
} // end namespace
