﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database.View
{
    #region Interface

    public interface ICompanySiteCategoryStandardDetailService
    {
        CompanySiteCategoryStandardDetail Get(Expression<Func<CompanySiteCategoryStandardDetail, bool>> where, List<Expression<Func<CompanySiteCategoryStandardDetail, object>>> includes);
        List<CompanySiteCategoryStandardDetail> GetMany(int? top, Expression<Func<CompanySiteCategoryStandardDetail, bool>> where, Func<IQueryable<CompanySiteCategoryStandardDetail>, IOrderedQueryable<CompanySiteCategoryStandardDetail>> orderBy, List<Expression<Func<CompanySiteCategoryStandardDetail, object>>> includes);
        List<CompanySiteCategoryStandardDetail> GetManyPaged(int pageNumber, int pageSize, Expression<Func<CompanySiteCategoryStandardDetail, bool>> where, Func<IQueryable<CompanySiteCategoryStandardDetail>, IOrderedQueryable<CompanySiteCategoryStandardDetail>> orderBy, List<Expression<Func<CompanySiteCategoryStandardDetail, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<CompanySiteCategoryStandardDetail, bool>> where, List<Expression<Func<CompanySiteCategoryStandardDetail, object>>> includes);
    }
    #endregion

    #region Interface Implementation

    public class CompanySiteCategoryStandardDetailService : ICompanySiteCategoryStandardDetailService
    {
        private readonly IRepository<CompanySiteCategoryStandardDetail> _repository;

        public CompanySiteCategoryStandardDetailService(IRepository<CompanySiteCategoryStandardDetail> repository)
        {
            this._repository = repository;
        }

        public CompanySiteCategoryStandardDetail Get(Expression<Func<CompanySiteCategoryStandardDetail, bool>> where, List<Expression<Func<CompanySiteCategoryStandardDetail, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<CompanySiteCategoryStandardDetail> GetMany(int? top, Expression<Func<CompanySiteCategoryStandardDetail, bool>> where, Func<IQueryable<CompanySiteCategoryStandardDetail>, IOrderedQueryable<CompanySiteCategoryStandardDetail>> orderBy, List<Expression<Func<CompanySiteCategoryStandardDetail, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<CompanySiteCategoryStandardDetail> GetManyPaged(int pageNumber, int pageSize, Expression<Func<CompanySiteCategoryStandardDetail, bool>> where, Func<IQueryable<CompanySiteCategoryStandardDetail>, IOrderedQueryable<CompanySiteCategoryStandardDetail>> orderBy, List<Expression<Func<CompanySiteCategoryStandardDetail, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<CompanySiteCategoryStandardDetail, bool>> where, List<Expression<Func<CompanySiteCategoryStandardDetail, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }
    }

    #endregion
}
