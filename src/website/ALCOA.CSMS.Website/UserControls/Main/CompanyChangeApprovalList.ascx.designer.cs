﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace ALCOA.CSMS.Website.UserControls.Main {
    
    
    public partial class CompanyChangeApprovalList {
        
        /// <summary>
        /// ASPxPageControl1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::DevExpress.Web.ASPxTabControl.ASPxPageControl ASPxPageControl1;
        
        /// <summary>
        /// gridCcsrAwaitingApproval control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::DevExpress.Web.ASPxGridView.ASPxGridView gridCcsrAwaitingApproval;
        
        /// <summary>
        /// ExportButtons1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::UserControls_Tiny_ExportButtons ExportButtons1;
        
        /// <summary>
        /// gridCcsrHistory control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::DevExpress.Web.ASPxGridView.ASPxGridView gridCcsrHistory;
        
        /// <summary>
        /// ExportButtons2 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::UserControls_Tiny_ExportButtons ExportButtons2;
        
        /// <summary>
        /// CompanyStatusDataSource1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::KaiZen.CSMS.Web.Data.CompanyStatusDataSource CompanyStatusDataSource1;
        
        /// <summary>
        /// CompaniesDataSource1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::KaiZen.CSMS.Web.Data.CompaniesDataSource CompaniesDataSource1;
        
        /// <summary>
        /// UsersFullNameDataSource1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::KaiZen.CSMS.Web.Data.UsersFullNameDataSource UsersFullNameDataSource1;
        
        /// <summary>
        /// CompanyStatusChangeApprovalDataSource1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.SqlDataSource CompanyStatusChangeApprovalDataSource1;
        
        /// <summary>
        /// CompanyStatusChangeApprovalDataSource2 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.SqlDataSource CompanyStatusChangeApprovalDataSource2;
    }
}
