﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxEditors;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using DevExpress.Web.ASPxGridView;
using DevExpress.XtraPrinting;

namespace ALCOA.CSMS.Website.UserControls.Main
{
    public partial class RiskNotificationsCorrectiveActionsOpenClosed : System.Web.UI.UserControl
    {
        Auth auth = new Auth();
        public void Page_Load(object sender, EventArgs e) { Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); }


        private void moduleInit(bool postBack)
        {

            if (!IsPostBack)
            {
                FillYearCombo();
                FillMonth();

                

                Helper.ExportGrid.Settings(ucExportButtons2, "Risk Notifications & Corrective Actions Open-Closed", "gridRNC");
               

                SqlDSRNCorrectiveActions.SelectParameters["Year"].DefaultValue = cbYear1.Value.ToString();
                SqlDSRNCorrectiveActions.SelectParameters["Month"].DefaultValue = cbMonth.Value.ToString();
                gridRNC.DataBind();


                string lastModifieddate = string.Empty;
                DataTable dvDate = ((DataView)sqldaModifiedDate.Select(DataSourceSelectArguments.Empty)).ToTable();
                if(dvDate.Rows.Count>0)
                {
                    if (dvDate.Rows[0][0] != null)
                    {
                        DateTime lastdate = Convert.ToDateTime(dvDate.Rows[0][0]);
                        lastModifieddate = lastdate.ToString("dd-MM-yyyy hh:mm:tt");
                    }
                    else
                    {
                        lastModifieddate = "n/a";
                    }
                }

                lblLastMod.Text = lastModifieddate;
            }
        }


        protected void FillYearCombo()
        {
            int yearToStart = 2011;
            int currentYear = DateTime.Now.Year;

            while (currentYear >= yearToStart)
            {
                cbYear1.Items.Add(currentYear.ToString(), currentYear);
                currentYear--;
            }
            cbYear1.SelectedIndex = 0;
        }
        protected void FillMonth()
        {

            cbMonth.Items.Insert(0, new ListEditItem("[Ytd]", 13));

            cbMonth.Items.Insert(1, new ListEditItem("January", 1));
            cbMonth.Items.Insert(2, new ListEditItem("February", 2));
            cbMonth.Items.Insert(3, new ListEditItem("March", 3));
            cbMonth.Items.Insert(4, new ListEditItem("April", 4));

            cbMonth.Items.Insert(5, new ListEditItem("May", 5));
            cbMonth.Items.Insert(6, new ListEditItem("June", 6));
            cbMonth.Items.Insert(7, new ListEditItem("July", 7));
            cbMonth.Items.Insert(8, new ListEditItem("August", 8));

            cbMonth.Items.Insert(9, new ListEditItem("September", 9));
            cbMonth.Items.Insert(10, new ListEditItem("October", 10));
            cbMonth.Items.Insert(11, new ListEditItem("November", 11));
            cbMonth.Items.Insert(12, new ListEditItem("December", 12));

            cbMonth.SelectedIndex = 0;
        }

        protected void btnFilter_Click(object sender, EventArgs e)
        {
            string selectedMonth = Convert.ToString(cbMonth.SelectedItem.Text);
            if (selectedMonth != "[Ytd]")
                ShowColumns(selectedMonth);
            else
            {
                GridViewBandColumn db = new GridViewBandColumn();
                db = (GridViewBandColumn)gridRNC.Columns[3];
                for (int i = 0; i <= 52; i++)
                    db.Columns[i].Visible = true;
            }
            SqlDSRNCorrectiveActions.SelectParameters["Year"].DefaultValue = cbYear1.Value.ToString();
            SqlDSRNCorrectiveActions.SelectParameters["Month"].DefaultValue = cbMonth.Value.ToString();
            gridRNC.DataBind();
        }
        private void ShowColumns(string selectedMonth)
        { 
            string strMonth=selectedMonth.Substring(0,3);
            GridViewBandColumn db = new GridViewBandColumn();
            db=(GridViewBandColumn)gridRNC.Columns[3];
            for (int i = 0; i <= 52; i++)
                db.Columns[i].Visible = false;
            for(int j = 0; j <= 52; j++)
            {
                if (db.Columns[j].Caption.Contains(strMonth))
                {
                    db.Columns[j].Visible = true;
                }
            }

        }

    }
}