﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.ComponentModel;
using System.Web.UI.Design.WebControls;
using System.Web.UI.Design;
using System.Web.UI.WebControls;

namespace KaiZen.CSMS.Web.UI
{
    /// <summary>
    /// A designer class for a strongly typed repeater <c>TwentyOnePointAuditRepeater</c>
    /// </summary>
	public class TwentyOnePointAuditRepeaterDesigner : System.Web.UI.Design.ControlDesigner
	{
	    /// <summary>
        /// Initializes a new instance of the <see cref="T:TwentyOnePointAuditRepeaterDesigner"/> class.
        /// </summary>
		public TwentyOnePointAuditRepeaterDesigner()
		{
		}

        /// <summary>
        /// Initializes the control designer and loads the specified component.
        /// </summary>
        /// <param name="component">The control being designed.</param>
		public override void Initialize(IComponent component)
		{
			if (!(component is TwentyOnePointAuditRepeater))
			{ 
				throw new ArgumentException("Component is not a TwentyOnePointAuditRepeater."); 
			} 
			base.Initialize(component); 
			base.SetViewFlags(ViewFlags.TemplateEditing, true); 
		}


		/// <summary>
		/// Generate HTML for the designer
		/// </summary>
		/// <returns>a string of design time HTML</returns>
		public override string GetDesignTimeHtml()
		{

			// Get the instance this designer applies to
			//
			TwentyOnePointAuditRepeater z = (TwentyOnePointAuditRepeater)Component;
			z.DataBind();

			return base.GetDesignTimeHtml();

			//return z.RenderAtDesignTime();

			//	ControlCollection c = z.Controls;
			//Totem z = (Totem) Component;
			//Totem z = (Totem) Component;
			//return ("<div style='border: 1px gray dotted; background-color: lightgray'><b>TagStat :</b> zae |  qsdds</div>");

		}
	}

    /// <summary>
    /// A strongly typed repeater control for the <see cref="TwentyOnePointAuditRepeater"/> Type.
    /// </summary>
	[Designer(typeof(TwentyOnePointAuditRepeaterDesigner))]
	[ParseChildren(true)]
	[ToolboxData("<{0}:TwentyOnePointAuditRepeater runat=\"server\"></{0}:TwentyOnePointAuditRepeater>")]
	public class TwentyOnePointAuditRepeater : CompositeDataBoundControl, System.Web.UI.INamingContainer
	{
	    /// <summary>
        /// Initializes a new instance of the <see cref="T:TwentyOnePointAuditRepeater"/> class.
        /// </summary>
		public TwentyOnePointAuditRepeater()
		{
		}

		/// <summary>
        /// Gets a <see cref="T:System.Web.UI.ControlCollection"></see> object that represents the child controls for a specified server control in the UI hierarchy.
        /// </summary>
        /// <value></value>
        /// <returns>The collection of child controls for the specified server control.</returns>
		public override ControlCollection Controls
		{
			get
			{
				this.EnsureChildControls();
				return base.Controls;
			}
		}

		private ITemplate m_headerTemplate;
		/// <summary>
        /// Gets or sets the header template.
        /// </summary>
        /// <value>The header template.</value>
		[Browsable(false)]
		[TemplateContainer(typeof(TwentyOnePointAuditItem))]
		[PersistenceMode(PersistenceMode.InnerDefaultProperty)]
		public ITemplate HeaderTemplate
		{
			get { return m_headerTemplate; }
			set { m_headerTemplate = value; }
		}

		private ITemplate m_itemTemplate;
		/// <summary>
        /// Gets or sets the item template.
        /// </summary>
        /// <value>The item template.</value>
		[Browsable(false)]
		[TemplateContainer(typeof(TwentyOnePointAuditItem))]
		[PersistenceMode(PersistenceMode.InnerDefaultProperty)]
		public ITemplate ItemTemplate
		{
			get { return m_itemTemplate; }
			set { m_itemTemplate = value; }
		}

		private ITemplate m_seperatorTemplate;
        /// <summary>
        /// Gets or sets the Seperator Template
        /// </summary>
        [Browsable(false)]
        [TemplateContainer(typeof(TwentyOnePointAuditItem))]
        [PersistenceMode(PersistenceMode.InnerDefaultProperty)]
        public ITemplate SeperatorTemplate
        {
            get { return m_seperatorTemplate; }
            set { m_seperatorTemplate = value; }
        }
			
		private ITemplate m_altenateItemTemplate;
        /// <summary>
        /// Gets or sets the alternating item template.
        /// </summary>
        /// <value>The alternating item template.</value>
		[Browsable(false)]
		[TemplateContainer(typeof(TwentyOnePointAuditItem))]
		[PersistenceMode(PersistenceMode.InnerDefaultProperty)]
		public ITemplate AlternatingItemTemplate
		{
			get { return m_altenateItemTemplate; }
			set { m_altenateItemTemplate = value; }
		}

		private ITemplate m_footerTemplate;
        /// <summary>
        /// Gets or sets the footer template.
        /// </summary>
        /// <value>The footer template.</value>
		[Browsable(false)]
		[TemplateContainer(typeof(TwentyOnePointAuditItem))]
		[PersistenceMode(PersistenceMode.InnerDefaultProperty)]
		public ITemplate FooterTemplate
		{
			get { return m_footerTemplate; }
			set { m_footerTemplate = value; }
		}

//      /// <summary>
//      /// Called by the ASP.NET page framework to notify server controls that use composition-based implementation to create any child controls they contain in preparation for posting back or rendering.
//      /// </summary>
//		protected override void CreateChildControls()
//      {
//         if (ChildControlsCreated)
//         {
//            return;
//         }

//         Controls.Clear();

//         //Instantiate the Header template (if exists)
//         if (m_headerTemplate != null)
//         {
//            Control headerItem = new Control();
//            m_headerTemplate.InstantiateIn(headerItem);
//            Controls.Add(headerItem);
//         }

//         //Instantiate the Footer template (if exists)
//         if (m_footerTemplate != null)
//         {
//            Control footerItem = new Control();
//            m_footerTemplate.InstantiateIn(footerItem);
//            Controls.Add(footerItem);
//         }
//
//         ChildControlsCreated = true;
//      }
	
		/// <summary>
        /// Overridden and Empty so that span tags are not written
        /// </summary>
        /// <param name="writer"></param>
        public override void RenderBeginTag(HtmlTextWriter writer)
        {
            
        }

        /// <summary>
        /// Overridden and Empty so that span tags are not written
        /// </summary>
        /// <param name="writer"></param>
        public override void RenderEndTag(HtmlTextWriter writer)
        {
                
        }		
		
		/// <summary>
      	/// Called by the ASP.NET page framework to notify server controls that use composition-based implementation to create any child controls they contain in preparation for posting back or rendering.
      	/// </summary>
		protected override int CreateChildControls(System.Collections.IEnumerable dataSource, bool dataBinding)
      	{
         int pos = 0;

         if (dataBinding)
         {
            //Instantiate the Header template (if exists)
            if (m_headerTemplate != null)
            {
                Control headerItem = new Control();
                m_headerTemplate.InstantiateIn(headerItem);
                Controls.Add(headerItem);
            }
			if (dataSource != null)
			{
				foreach (object o in dataSource)
				{
						KaiZen.CSMS.Entities.TwentyOnePointAudit entity = o as KaiZen.CSMS.Entities.TwentyOnePointAudit;
						TwentyOnePointAuditItem container = new TwentyOnePointAuditItem(entity);
	
						if (m_itemTemplate != null && (pos % 2) == 0)
						{
							m_itemTemplate.InstantiateIn(container);
							
							if (m_seperatorTemplate != null)
							{
								m_seperatorTemplate.InstantiateIn(container);
							}
						}
						else
						{
							if (m_altenateItemTemplate != null)
							{
								m_altenateItemTemplate.InstantiateIn(container);
								
								if (m_seperatorTemplate != null)
								{
									m_seperatorTemplate.InstantiateIn(container);
								}
								
							}
							else if (m_itemTemplate != null)
							{
								m_itemTemplate.InstantiateIn(container);
								
								if (m_seperatorTemplate != null)
								{
									m_seperatorTemplate.InstantiateIn(container);
								}
							}
							else
							{
								// no template !!!
							}
						}
						Controls.Add(container);
						
						container.DataBind();
						
						pos++;
				}
			}
            //Instantiate the Footer template (if exists)
            if (m_footerTemplate != null)
            {
                Control footerItem = new Control();
                m_footerTemplate.InstantiateIn(footerItem);
                Controls.Add(footerItem);
            }

		}
			
			return pos;
		}

        /// <summary>
        /// Raises the <see cref="E:System.Web.UI.Control.PreRender"></see> event.
        /// </summary>
        /// <param name="e">An <see cref="T:System.EventArgs"></see> object that contains the event data.</param>
		protected override void OnPreRender(EventArgs e)
		{
			base.DataBind();
		}

		#region Design time
        /// <summary>
        /// Renders at design time.
        /// </summary>
        /// <returns>a  string of the Designed HTML</returns>
		internal string RenderAtDesignTime()
		{			
			return "Designer currently not implemented"; 
		}

		#endregion
	}

    /// <summary>
    /// A wrapper type for the entity
    /// </summary>
	[System.ComponentModel.ToolboxItem(false)]
	public class TwentyOnePointAuditItem : System.Web.UI.Control, System.Web.UI.INamingContainer
	{
		private KaiZen.CSMS.Entities.TwentyOnePointAudit _entity;

        /// <summary>
        /// Initializes a new instance of the <see cref="T:TwentyOnePointAuditItem"/> class.
        /// </summary>
		public TwentyOnePointAuditItem()
			: base()
		{ }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:TwentyOnePointAuditItem"/> class.
        /// </summary>
		public TwentyOnePointAuditItem(KaiZen.CSMS.Entities.TwentyOnePointAudit entity)
			: base()
		{
			_entity = entity;
		}
		
        /// <summary>
        /// Gets the Id
        /// </summary>
        /// <value>The Id.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32 Id
		{
			get { return _entity.Id; }
		}
        /// <summary>
        /// Gets the CompanyId
        /// </summary>
        /// <value>The CompanyId.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32 CompanyId
		{
			get { return _entity.CompanyId; }
		}
        /// <summary>
        /// Gets the SiteId
        /// </summary>
        /// <value>The SiteId.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32 SiteId
		{
			get { return _entity.SiteId; }
		}
        /// <summary>
        /// Gets the CreatedbyUserId
        /// </summary>
        /// <value>The CreatedbyUserId.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32 CreatedbyUserId
		{
			get { return _entity.CreatedbyUserId; }
		}
        /// <summary>
        /// Gets the ModifiedbyUserId
        /// </summary>
        /// <value>The ModifiedbyUserId.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32 ModifiedbyUserId
		{
			get { return _entity.ModifiedbyUserId; }
		}
        /// <summary>
        /// Gets the DateAdded
        /// </summary>
        /// <value>The DateAdded.</value>
		[System.ComponentModel.Bindable(true)]
		public System.DateTime DateAdded
		{
			get { return _entity.DateAdded; }
		}
        /// <summary>
        /// Gets the DateModified
        /// </summary>
        /// <value>The DateModified.</value>
		[System.ComponentModel.Bindable(true)]
		public System.DateTime DateModified
		{
			get { return _entity.DateModified; }
		}
        /// <summary>
        /// Gets the QtrId
        /// </summary>
        /// <value>The QtrId.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32 QtrId
		{
			get { return _entity.QtrId; }
		}
        /// <summary>
        /// Gets the Year
        /// </summary>
        /// <value>The Year.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int64 Year
		{
			get { return _entity.Year; }
		}
        /// <summary>
        /// Gets the TotalPossibleScore
        /// </summary>
        /// <value>The TotalPossibleScore.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? TotalPossibleScore
		{
			get { return _entity.TotalPossibleScore; }
		}
        /// <summary>
        /// Gets the TotalActualScore
        /// </summary>
        /// <value>The TotalActualScore.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? TotalActualScore
		{
			get { return _entity.TotalActualScore; }
		}
        /// <summary>
        /// Gets the TotalRating
        /// </summary>
        /// <value>The TotalRating.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? TotalRating
		{
			get { return _entity.TotalRating; }
		}
        /// <summary>
        /// Gets the Point01Id
        /// </summary>
        /// <value>The Point01Id.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Point01Id
		{
			get { return _entity.Point01Id; }
		}
        /// <summary>
        /// Gets the Point02Id
        /// </summary>
        /// <value>The Point02Id.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Point02Id
		{
			get { return _entity.Point02Id; }
		}
        /// <summary>
        /// Gets the Point03Id
        /// </summary>
        /// <value>The Point03Id.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Point03Id
		{
			get { return _entity.Point03Id; }
		}
        /// <summary>
        /// Gets the Point04Id
        /// </summary>
        /// <value>The Point04Id.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Point04Id
		{
			get { return _entity.Point04Id; }
		}
        /// <summary>
        /// Gets the Point05Id
        /// </summary>
        /// <value>The Point05Id.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Point05Id
		{
			get { return _entity.Point05Id; }
		}
        /// <summary>
        /// Gets the Point06Id
        /// </summary>
        /// <value>The Point06Id.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Point06Id
		{
			get { return _entity.Point06Id; }
		}
        /// <summary>
        /// Gets the Point07Id
        /// </summary>
        /// <value>The Point07Id.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Point07Id
		{
			get { return _entity.Point07Id; }
		}
        /// <summary>
        /// Gets the Point08Id
        /// </summary>
        /// <value>The Point08Id.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Point08Id
		{
			get { return _entity.Point08Id; }
		}
        /// <summary>
        /// Gets the Point09Id
        /// </summary>
        /// <value>The Point09Id.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Point09Id
		{
			get { return _entity.Point09Id; }
		}
        /// <summary>
        /// Gets the Point10Id
        /// </summary>
        /// <value>The Point10Id.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Point10Id
		{
			get { return _entity.Point10Id; }
		}
        /// <summary>
        /// Gets the Point11Id
        /// </summary>
        /// <value>The Point11Id.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Point11Id
		{
			get { return _entity.Point11Id; }
		}
        /// <summary>
        /// Gets the Point12Id
        /// </summary>
        /// <value>The Point12Id.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Point12Id
		{
			get { return _entity.Point12Id; }
		}
        /// <summary>
        /// Gets the Point13Id
        /// </summary>
        /// <value>The Point13Id.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Point13Id
		{
			get { return _entity.Point13Id; }
		}
        /// <summary>
        /// Gets the Point14Id
        /// </summary>
        /// <value>The Point14Id.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Point14Id
		{
			get { return _entity.Point14Id; }
		}
        /// <summary>
        /// Gets the Point15Id
        /// </summary>
        /// <value>The Point15Id.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Point15Id
		{
			get { return _entity.Point15Id; }
		}
        /// <summary>
        /// Gets the Point16Id
        /// </summary>
        /// <value>The Point16Id.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Point16Id
		{
			get { return _entity.Point16Id; }
		}
        /// <summary>
        /// Gets the Point17Id
        /// </summary>
        /// <value>The Point17Id.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Point17Id
		{
			get { return _entity.Point17Id; }
		}
        /// <summary>
        /// Gets the Point18Id
        /// </summary>
        /// <value>The Point18Id.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Point18Id
		{
			get { return _entity.Point18Id; }
		}
        /// <summary>
        /// Gets the Point19Id
        /// </summary>
        /// <value>The Point19Id.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Point19Id
		{
			get { return _entity.Point19Id; }
		}
        /// <summary>
        /// Gets the Point20Id
        /// </summary>
        /// <value>The Point20Id.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Point20Id
		{
			get { return _entity.Point20Id; }
		}
        /// <summary>
        /// Gets the Point21Id
        /// </summary>
        /// <value>The Point21Id.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Point21Id
		{
			get { return _entity.Point21Id; }
		}

        /// <summary>
        /// Gets a <see cref="T:KaiZen.CSMS.Entities.TwentyOnePointAudit"></see> object
        /// </summary>
        /// <value></value>
        [System.ComponentModel.Bindable(true)]
        public KaiZen.CSMS.Entities.TwentyOnePointAudit Entity
        {
            get { return _entity; }
        }
	}
}
