﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Linq;
using System.Web;
using System.Text;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using KaiZen.CSMS.Web;
using KaiZen.CSMS.Services;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxTabControl;
using DevExpress.Web.Data;
using DevExpress.Web.ASPxGridView;
using System.Drawing;
using DevExpress.Web.ASPxCallbackPanel;
using DevExpress.Web.ASPxClasses;
using DevExpress.XtraPrinting;
using System.Collections.Generic;
using System.IO;


public partial class UserControls_Tiny_ComplianceReports_ComplianceReport_KPISQ : System.Web.UI.UserControl
{
    Auth auth = new Auth();
    protected void Page_Load(object sender, EventArgs e) {  Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); }
    private void moduleInit(bool postBack)
    {
        if (!postBack)
        {


            BindData();

            switch (auth.RoleId)
            {
                case ((int)RoleList.Reader):
                    //full access
                    break;
                case ((int)RoleList.Contractor):
                    //restrict to own company
                    //sqldsNonComplianceReportYearContractor.DataBind();
                    //grid.DataSourceID = "sqldsNonComplianceReportYearContractor";
                    grid.Settings.ShowFilterRow = false;
                    grid.FilterExpression = String.Format("CompanyId = {0}", auth.CompanyId);
                    grid.DataBind();
                    break;
                case ((int)RoleList.Administrator):
                    //full access

                    break;
                default:
                    // do something
                    Response.Redirect("AccessDenied.aspx?error=" + Server.UrlEncode("Unrecognised User."));
                    break;
            }

            if (SessionHandler.spVar_Page == "ComplianceReportBySite")
            {
                if (Convert.ToInt32(SessionHandler.spVar_SiteId) > 0) //Site
                {
                    grid.Columns["Site Name"].Visible = false;
                }
                else
                {
                    grid.Columns["Site Name"].Visible = true;
                }
            }
        }
        else
        {
            grid.DataSource = (List<ReportRow>)Session["dtNonComplianceKPISQ"];
            grid.DataBind();
        }
    }

    public static string Capitalize(string value)
    {
        return System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(value);

    }
    protected void grid_RowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {
    }

    protected void BindData()
    {
        var storedProcedureService = ALCOA.CSMS.Website.Global.GetInstance<Repo.CSMS.Service.Database.IStoredProcedureService>();
        var questionnaireService = ALCOA.CSMS.Website.Global.GetInstance<Repo.CSMS.Service.Database.IQuestionnaireService>();
        var companyService = ALCOA.CSMS.Website.Global.GetInstance<Repo.CSMS.Service.Database.ICompanyService>();
        var companySiteCategoryStandardService = ALCOA.CSMS.Website.Global.GetInstance<Repo.CSMS.Service.Database.ICompanySiteCategoryStandardService>();

        var ds = storedProcedureService.SP_GetReport_Compliance_SQ(1);

        var report = new List<ReportRow>();

        //Get a list of all companyids in query
        var companyIdList = ds.Select(e => e.CompanyId).Distinct().ToList();
        //Get a list of the most recent questionnaire modified by each of these companies (in 1 query)
        var qList2 = questionnaireService.GetMany(null, e => companyIdList.Contains(e.CompanyId), null, null)
            .GroupBy(g => g.CompanyId, (key, q) => q.OrderByDescending(o => o.ModifiedDate).FirstOrDefault()).ToList();
        //Get list of all companies in query
        var companies = companyService.GetMany(null, e => companyIdList.Contains(e.CompanyId), null, new List<System.Linq.Expressions.Expression<Func<Repo.CSMS.DAL.EntityModels.Company, object>>>() { e => e.CompanyStatus });
        //Get all companySiteCategoryStandards
        var companySiteCategoryStandards = companySiteCategoryStandardService.GetMany(null, e => companyIdList.Contains(e.CompanyId), null, null);

        foreach (var dr in ds)
        {
            var newrow = new ReportRow();
            newrow.CompanyId = dr.CompanyId;
            newrow.CompanyName2 = dr.CompanyName2;
            newrow.SiteId = dr.SiteId;
            newrow.SiteName2 = dr.SiteName2;
            newrow.LastEbiSwipe = dr.LastEbiSwipe;
            newrow.LastKpiEntered = dr.LastKpiEntered;
            newrow.Type = dr.Type;
            newrow.SqValidTo = dr.SqValidTo;
            newrow.CompanySqStatus = dr.CompanySqStatus;
            int? CompanySiteCategoryId = null;
            newrow.CategoryDesc = dr.CategoryDesc;

            try
            {
                string Valid = "";
                string ApprovedForSite = "";
                string Validity = "";

                int SiteId = newrow.SiteId;
                int CompanyId = newrow.CompanyId;

                QuestionnaireFilters qFilters = new QuestionnaireFilters();
                qFilters.Append(QuestionnaireColumn.CompanyId, CompanyId.ToString());
                //TList<Questionnaire> qList = DataRepository.QuestionnaireProvider.GetPaged(String.Format("CompanyId = {0}", CompanyId), "ModifiedDate DESC", 0, 9999, out _count);
                var q = qList2.Where(e => e.CompanyId == CompanyId).FirstOrDefault();
                if (q != null)
                {

                    Valid = "?";
                    ApprovedForSite = "?";
                    Validity = "?";
                    //CompanySiteCategoryStandardService cscsService = new CompanySiteCategoryStandardService();
                    //CompanySiteCategoryStandard cscs = cscsService.GetByCompanyIdSiteId(q.CompanyId, SiteId);
                    var cscs = companySiteCategoryStandards.Where(e => e.CompanyId == q.CompanyId && e.SiteId == SiteId).FirstOrDefault();
                    if (cscs != null)
                    {
                        if (cscs.Approved == true)
                        {
                            ApprovedForSite = "Yes";
                        }
                        if (cscs.Approved == false)
                        {
                            ApprovedForSite = "No";
                        }
                        if (cscs.Approved == null)
                        {
                            ApprovedForSite = "Tentative";
                        }
                        CompanySiteCategoryId = cscs.CompanySiteCategoryId;
                    }

                    Repo.CSMS.DAL.EntityModels.CompanyStatus cs = null;
                    var c = companies.Where(e => e.CompanyId == CompanyId).FirstOrDefault();
                    if (c != null)
                    {
                        cs = c.CompanyStatus;
                    }
                    //CompaniesService cService = new CompaniesService();
                    //Companies c = cService.GetByCompanyId(CompanyId);
                    //CompanyStatusService csService = new CompanyStatusService();
                    //CompanyStatus cs = csService.GetByCompanyStatusId(c.CompanyStatusId);

                    if (cs != null &&
                        (cs.CompanyStatusDesc.StartsWith("Active") || cs.CompanyStatusDesc.StartsWith("Acceptable")) &&
                        q.MainAssessmentValidTo > DateTime.Now &&
                        q.Status == (int)QuestionnaireStatusList.AssessmentComplete)
                    {
                        Valid = "Yes";
                    }

                    if (q.MainAssessmentValidTo != null)
                    {
                        DateTime expiry = (DateTime)q.MainAssessmentValidTo;
                        if (DateTime.Now >= expiry)
                        {
                            Validity = "Expired";
                        }
                        if (expiry > DateTime.Now)
                        {
                            TimeSpan span = expiry - DateTime.Now;
                            if (span.TotalDays <= 60)
                            {
                                Validity = "Expiring";
                            }
                            else
                            {
                                Validity = "Current";
                            }
                        }
                    }
                }

                newrow.ValidForSite = Valid;
                newrow.ApprovedForSite = ApprovedForSite;
                newrow.SqValidity = Validity;

                if (CompanySiteCategoryId != null)
                {
                    newrow.CompanySiteCategoryId = CompanySiteCategoryId;
                }
                //else
                //{
                //    newdr["CompanySiteCategoryId"] = typeof(DBNull);
                //}
                bool add = true;
                if (CompanySiteCategoryId != null && SessionHandler.spVar_CategoryId != "-1")
                {
                    if (SessionHandler.spVar_CategoryId != CompanySiteCategoryId.ToString()) add = false;
                }

                if (add)
                {
                    //if (SessionHandler.spVar_CategoryId2 == "%") add = true;

                    if (Convert.ToInt32(SessionHandler.spVar_SiteId) > 0)
                    {
                        add = false;
                        if (SessionHandler.spVar_SiteId == dr.SiteId.ToString()) add = true;
                    }
                    else
                    {
                        add = false;
                        RegionsSitesService rsService = new RegionsSitesService();
                        RegionsSites rs = rsService.GetBySiteIdRegionId(Convert.ToInt32(dr.SiteId.ToString()), (Convert.ToInt32(SessionHandler.spVar_SiteId) * -1));
                        if (rs != null) add = true;
                    }
                }

                if (add)
                {
                    report.Add(newrow);
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromContext(Context).Raise(ex);
            }
        }

        Session["dtNonComplianceKPISQ"] = report;
        grid.DataSource = (List<ReportRow>)Session["dtNonComplianceKPISQ"];
        grid.DataBind();
    }

    protected void grid_AutoFilterCellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
    {
        if (e.Column.FieldName == "CompanyId" || e.Column.FieldName == "SiteName")
        {
            ASPxComboBox comboBox = e.Editor as ASPxComboBox;
            comboBox.ClientSideEvents.Init = "function(s, e) {s.InsertItem(0, '(ALL)', ''); s.RemoveItem(-1);}";
        }
    }

    public PrintableComponentLink ExportToPdf()
    {
        PrintableComponentLink pcl = new PrintableComponentLink(new PrintingSystem());
        pcl.Component = gridExport;
        pcl.CreateReportHeaderArea += new CreateAreaEventHandler(pcl_CreateReportHeaderArea);
        pcl.CreateDocument(false);
        return pcl;
    }

    public void pcl_CreateReportHeaderArea(object sender, CreateAreaEventArgs e)
    {
        TextBrick brick1 = e.Graph.DrawString("Contractors On Site (KPI)", System.Drawing.ColorTranslator.FromHtml("#000099"), new RectangleF(0, 0, 620, 20), DevExpress.XtraPrinting.BorderSide.None);
        brick1.HorzAlignment = DevExpress.Utils.HorzAlignment.Near;
        brick1.Font = new Font(FontFamily.GenericSansSerif, 12, FontStyle.Regular);
    }

    public string ExportToHtml()
    {
        var sb = new StringBuilder();

        sb.Append("<table width='100%' border='1' style='font-size:9px; color:Black;'>");
        sb.Append("<tbody>");
        sb.Append("<tr>");
        sb.Append("<td width='22%' bgcolor='#94b6e8' border='1' align='left' valign='top'>Residential Category</td>");
        sb.Append("<td width='22%' bgcolor='#94b6e8' border='1' align='left' valign='top'>Company Name</td>");
        sb.Append("<td width='7%' bgcolor='#94b6e8' border='1' align='left' valign='top'>Last Time On Site (EBI)</td>");
        sb.Append("<td width='7%' bgcolor='#94b6e8' border='1' align='left' valign='top'>Last KPI Entered</td>");
        sb.Append("<td width='7%' bgcolor='#94b6e8' border='1' align='left' valign='top'>Type</td>");
        sb.Append("<td width='7%' bgcolor='#94b6e8' border='1' align='center' valign='top'>Valid For Site</td>");
        sb.Append("<td width='7%' bgcolor='#94b6e8' border='1' align='center' valign='top'>Approved For Site</td>");
        sb.Append("<td width='7%' bgcolor='#94b6e8' border='1' align='center' valign='top'>SQ Validity</td>");
        sb.Append("<td width='7%' bgcolor='#94b6e8' border='1' align='left' valign='top'>SQ Valid To</td>");
        sb.Append("<td width='7%' bgcolor='#94b6e8' border='1' align='left' valign='top'>Company SQ Status</td>");
        sb.Append("</tr>");

        var dt = (List<ReportRow>)grid.DataSource;
        if (dt != null && dt.Count > 0)
        {
            dt = dt.OrderByDescending(e => e.CategoryDesc).ThenBy(e => e.CompanyName2).ToList();

            foreach (var dr in dt)
            {
                sb.Append("<tr>");
                sb.Append(string.Format("<td width='22%' border='1' align='left' valign='top'>{0}</td>", dr.CategoryDesc.ToString()));
                sb.Append(string.Format("<td width='22%' border='1' align='left' valign='top'>{0}</td>", dr.CompanyName2));
                sb.Append(string.Format("<td width='7%' border='1' align='left' valign='top'>{0}</td>", (dr.LastEbiSwipe != null ? dr.LastEbiSwipe.Value.ToString("yyyy/MM/dd") : "")));
                sb.Append(string.Format("<td width='7%' border='1' align='left' valign='top'>{0}</td>", (dr.LastKpiEntered != null ? dr.LastKpiEntered.Value.ToString("yyyy/MM") : "")));
                sb.Append(string.Format("<td width='7%' border='1' align='left' valign='top'>{0}</td>", dr.Type.ToString()));
                sb.Append(string.Format("<td width='7%' border='1' align='center' valign='top'>{0}</td>", dr.ValidForSite.ToString()));
                sb.Append(string.Format("<td width='7%' border='1' align='center' valign='top'>{0}</td>", dr.ApprovedForSite.ToString()));
                sb.Append(string.Format("<td width='7%' border='1' align='center' valign='top'>{0}</td>", dr.SqValidity.ToString()));
                sb.Append(string.Format("<td width='7%' border='1' align='left' valign='top'>{0}</td>", (dr.SqValidTo != null ? dr.SqValidTo.Value.ToString("dd/MM/yyyy") : "")));
                sb.Append(string.Format("<td width='7%' border='1' align='left' valign='top'>{0}</td>", dr.CompanySqStatus.ToString()));
            }
        }
        else
        {
            sb.Append("<tr>");
            sb.Append("<td colspan='4'>Contractors On Site (KPI) submitted</td>");
            sb.Append("</tr>");
        }

        sb.Append("</tbody>");
        sb.Append("</table>");

        return sb.ToString();
    }
}

public class ReportRow
{
    public int CompanyId { get; set; }
    public string CompanyName2 { get; set; }
    public int SiteId { get; set; }
    public string SiteName2 { get; set; }
    public DateTime? LastEbiSwipe { get; set; }
    public DateTime? LastKpiEntered { get; set; }
    public string Type { get; set; }
    public DateTime? SqValidTo { get; set; }
    public string CompanySqStatus { get; set; }
    public string ValidForSite { get; set; }
    public string ApprovedForSite { get; set; }
    public string SqValidity { get; set; }
    public int? CompanySiteCategoryId { get; set; }
    public string CategoryDesc { get; set; }
}