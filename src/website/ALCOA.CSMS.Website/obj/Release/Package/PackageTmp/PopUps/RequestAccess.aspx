﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/popup.master" AutoEventWireup="true"
    CodeBehind="RequestAccess.aspx.cs" Inherits="ALCOA.CSMS.Website.PopUps.RequestAccess" %>

<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    
<style>
 p.MsoNormal
	{margin-top:0cm;
	margin-right:0cm;
	margin-bottom:10.0pt;
	margin-left:0cm;
	line-height:115%;
	font-size:11.0pt;
	font-family:"Calibri","sans-serif";
	}
        .style2
        {
            border-collapse: collapse;
            font-size: 11.0pt;
            font-family: Calibri, sans-serif;
            border: 1.0pt solid black;
        }
        .style6
        {
            width: 600px;
        }
        .style7
        {
            color: #FF0000;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    To request access for someone in your company, please complete and submit this 
    form (once).<br />
    <br />
    <table border="1" cellpadding="0" cellspacing="0" class="style2" 
        style="mso-border-alt: solid black .5pt; mso-border-themecolor: text1; mso-yfti-tbllook: 1184; mso-padding-alt: 0cm 5.4pt 0cm 5.4pt">
        <tr style="mso-yfti-irow:0;mso-yfti-firstrow:yes;height:27.3pt">
            <td width="149">
                <p align="center" class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal">
                    <b style="mso-bidi-font-weight:normal">
                    
                    Description</b></p>
            </td>
            <td width="262">
                <p align="center" class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal">
                    <b style="mso-bidi-font-weight:normal">
                    
                    Employee Information</b></p>
            </td>
            <td width="205">
                <p align="center" class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal">
                    <b style="mso-bidi-font-weight:normal">
                    
                    Guidance notes</b></p>
            </td>
        </tr>
        <tr style="mso-yfti-irow:2;height:1.0cm">
            <td width="149">
                <p align="center" class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal">
                    <b style="mso-bidi-font-weight:normal">
                    
                    First Name</b></p>
            </td>
            <td width="262" style="vertical-align: middle; text-align: center;">
                <dx:ASPxTextBox ID="tbFirstName" runat="server" Width="220px" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                    CssPostfix="Office2003Blue" 
                        SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                    <ValidationSettings CausesValidation="True" ErrorDisplayMode="ImageWithTooltip">
                        <RequiredField IsRequired="True" />
                    </ValidationSettings>
                </dx:ASPxTextBox>
            </td>
            <td width="205">
                <p align="center" class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal">
                    First Name</p>
            </td>
        </tr>
        <tr style="mso-yfti-irow:3;height:27.35pt">
            <td width="149">
                <p align="center" class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal">
                    <b style="mso-bidi-font-weight:normal">
                    
                    Last Name</b></p>
            </td>
            <td width="262" style="vertical-align: middle; text-align: center;">
                <dx:ASPxTextBox ID="tbLastName" runat="server" Width="220px" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                    CssPostfix="Office2003Blue" 
                        SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                    <ValidationSettings CausesValidation="True" ErrorDisplayMode="ImageWithTooltip">
                        <RequiredField IsRequired="True" />
                    </ValidationSettings>
                </dx:ASPxTextBox>
            </td>
            <td width="205">
                <p align="center" class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal">
                    Surname</p>
            </td>
        </tr>
        <tr style="mso-yfti-irow:4;height:27.35pt">
            <td width="149">
                <p align="center" class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal">
                    <b style="mso-bidi-font-weight:normal">
                    
                    Title</b></p>
            </td>
            <td width="262" style="vertical-align: middle; text-align: center;">
                <dx:ASPxComboBox ID="cbTitle" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                    CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                    ValueType="System.Int32" Width="220px" >
                    <Items>
                        <dx:ListEditItem Text="Mr." Value="0" />
                        <dx:ListEditItem Text="Mrs." Value="1" />
                        <dx:ListEditItem Text="Ms." Value="2" />
                        <dx:ListEditItem Text="Dr" Value="3" />
                        <dx:ListEditItem Text="Other" Value="4" />
                    </Items>
                    <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                    </LoadingPanelImage>
                    <ValidationSettings CausesValidation="True" ErrorDisplayMode="ImageWithTooltip">
                        <RequiredField IsRequired="True" />
                    </ValidationSettings>
                </dx:ASPxComboBox>
            </td>
            <td width="205">
                <p align="center" class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal">
                    Ms. Mrs ,Mr, Dr etc</p>
            </td>
        </tr>
        <tr style="mso-yfti-irow:5;height:27.75pt">
            <td width="149">
                <p align="center" class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal">
                    <b style="mso-bidi-font-weight:normal">
                    
                    Company</b></p>
            </td>
            <td width="262" style="vertical-align: middle; text-align: center;">
                <dx:ASPxComboBox ID="cbCompany" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                    CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                    ValueType="System.Int32" Width="220px" Enabled="False">
                    <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                    </LoadingPanelImage>
                    <ValidationSettings CausesValidation="True" ErrorDisplayMode="ImageWithTooltip">
                        <RequiredField IsRequired="True" />
                    </ValidationSettings>
                </dx:ASPxComboBox>
            </td>
            <td width="205">
                <p align="center" class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal">
                    Company Name</p>
            </td>
        </tr>
        <tr style="mso-yfti-irow:6;height:28.15pt">
            <td width="149">
                <p align="center" class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal">
                    <b style="mso-bidi-font-weight:normal">
                    
                    Position Held</b></p>
            </td>
            <td width="262" style="vertical-align: middle; text-align: center;">
                <dx:ASPxTextBox ID="tbJobTitle" runat="server" Width="220px" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                    CssPostfix="Office2003Blue" 
                    SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                    <ValidationSettings CausesValidation="True" ErrorDisplayMode="ImageWithTooltip">
                        <RequiredField IsRequired="True" />
                    </ValidationSettings>
                </dx:ASPxTextBox>
            </td>
            <td width="205">
                <p align="center" class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal">
                    Director, Manager , Supervisor etc</p>
            </td>
        </tr>
        <tr style="mso-yfti-irow:7;height:27.85pt">
            <td width="149">
                <p align="center" class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal">
                    <b style="mso-bidi-font-weight:normal">
                    
                    Telephone No. 
                    Please include area code</b></p>
            </td>
            <td width="262" style="vertical-align: middle; text-align: center;">
                <dx:ASPxTextBox ID="tbTelephoneNo" runat="server" Width="220px" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                    CssPostfix="Office2003Blue" 
                    SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                    <ValidationSettings CausesValidation="True" ErrorDisplayMode="ImageWithTooltip">
                        <RequiredField IsRequired="True" />
                    </ValidationSettings>
                </dx:ASPxTextBox>
            </td>
            <td width="205">
                <p align="center" class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal">
                    Office telephone number</p>
            </td>
        </tr>
        <tr style="mso-yfti-irow:8;height:28.25pt">
            <td width="149">
                <p align="center" class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal">
                    <b style="mso-bidi-font-weight:normal">
                    
                    Mob. Number</b></p>
            </td>
            <td width="262" style="vertical-align: middle; text-align: center;">
                <dx:ASPxTextBox ID="tbMobileNumber" runat="server" Width="220px" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                    CssPostfix="Office2003Blue" 
                        SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                    <ValidationSettings CausesValidation="True" ErrorDisplayMode="ImageWithTooltip">
                        <RequiredField IsRequired="True" />
                    </ValidationSettings>
                </dx:ASPxTextBox>
            </td>
            <td width="205">
                <p align="center" class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal">
                    Mobile telephone number</p>
            </td>
        </tr>
        <tr style="mso-yfti-irow:9;height:27.2pt">
            <td width="149">
                <p align="center" class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal">
                    <b style="mso-bidi-font-weight:normal">
                    
                    Fax. Number</b></p>
                <p align="center" class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal">
                    <b style="mso-bidi-font-weight:normal">
                    
                    Please include area code</b></p>
            </td>
            <td width="262" style="vertical-align: middle; text-align: center;">
                <dx:ASPxTextBox ID="tbFaxNo" runat="server" Width="220px" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                    CssPostfix="Office2003Blue" 
                    SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                    <ValidationSettings CausesValidation="True" ErrorDisplayMode="ImageWithTooltip">
                        <RequiredField IsRequired="True" />
                    </ValidationSettings>
                </dx:ASPxTextBox>
            </td>
            <td width="205">
                <p align="center" class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal">
                    Fax number</p>
            </td>
        </tr>
        <tr style="mso-yfti-irow:10;mso-yfti-lastrow:yes;height:27.65pt">
            <td width="149">
                <p align="center" class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal">
                    <b style="mso-bidi-font-weight:normal">
                    
                    Email</b></p>
            </td>
            <td width="262" style="vertical-align: middle; text-align: center;">
            <dx:ASPxTextBox 
                        ID="tbEmail" runat="server" Width="220px" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                    CssPostfix="Office2003Blue" 
                        SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                    <ValidationSettings CausesValidation="True" ErrorDisplayMode="ImageWithTooltip">
                        <RegularExpression ErrorText="Not a Valid E-Mail." ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" />
                        <RequiredField IsRequired="True" />
                    </ValidationSettings>
                </dx:ASPxTextBox>
            </td>
            <td width="205">
                <p align="center" class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal">
                    Email address</p>
            </td>
        </tr>
    </table>
    <br />
    <br />
    <table cellpadding="0" cellspacing="0" class="style6">
        <tr>
            <td>
                <b style="mso-bidi-font-weight:normal">
                
                Are they accessing the system externally?</b></td>
            <td>
                <dx:ASPxComboBox ID="cbAccess" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                    CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                    ValueType="System.Int32" Width="70px">
                    <Items>
                        <dx:ListEditItem Text="Yes" Value="1" />
                        <dx:ListEditItem Text="No" Value="0" />
                    </Items>
                    <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                    </LoadingPanelImage>
                    <ValidationSettings CausesValidation="True" ErrorDisplayMode="ImageWithTooltip">
                        <RequiredField IsRequired="True" />
                    </ValidationSettings>
                </dx:ASPxComboBox>
            </td>
        </tr>
        <tr>
            <td>
                If they wish to access CSMS using their Alcoa Login (if they have one) select &#39;No&#39;, 
                otherwise select &#39;Yes&#39; so that they can access CSMS externally via the internet.</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style7">
                <strong>NOTE: If &#39;Yes&#39; selected please enter their user log in details below. Otherwise ignore this field.</strong></td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                <dx:ASPxTextBox ID="tbLogin" runat="server" Style="margin-bottom: 8px" Width="270px"
                    CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue"
                    SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                    <ValidationSettings ErrorDisplayMode="ImageWithTooltip">
                    </ValidationSettings>
                </dx:ASPxTextBox>
            </td>
            <td>
                &nbsp;</td>
        </tr>
    </table>
    <br />
    <strong>Optional Comments:</strong><dx:ASPxMemo ID="mTaskComments" 
        runat="server" Height="100px" Width="600px">
                </dx:ASPxMemo>
            <br />
    <dx:ASPxButton ID="btnRequestAccess" runat="server" 
        CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
        CssPostfix="Office2003Blue" onclick="btnRequestAccess_Click" 
        SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" 
        Text="Request Access" Width="600px">
    </dx:ASPxButton>
    <dx:ASPxLabel ID="lblError" runat="server" Font-Bold="True" Font-Size="Small" 
        ForeColor="Red">
    </dx:ASPxLabel>
    <br />
    <br />
    <data:CsmsAccessDataSource ID="CsmsAccessDataSource1" runat="server">
    </data:CsmsAccessDataSource>
    </asp:Content>
    