﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/user.master" AutoEventWireup="true" Inherits="NonComplianceSQ" Codebehind="NonComplianceSQ.aspx.cs" %>

<%@ Register Src="UserControls/Main/ComplianceReports/NonComplianceSQ.ascx" TagName="NonComplianceSQ"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css" media="screen">
        #container
        {
            margin: 0px 0px;
            padding: 0px; /* Need to set body margin and padding to get consistency between browsers. */
            text-align: center; /* Hack for IE5/Win */
            overflow: hidden;
        }
        #content
        {
            margin: 0px auto; /* Right and left margin widths set to "auto" */ /*text-align:center;  Counteract to IE5/Win Hack */
            padding: 0px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="Server">
    <uc1:NonComplianceSQ ID="NonComplianceSQ1" runat="server" />
</asp:Content>
