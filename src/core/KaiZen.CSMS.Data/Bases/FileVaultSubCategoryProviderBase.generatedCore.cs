﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="FileVaultSubCategoryProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class FileVaultSubCategoryProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.FileVaultSubCategory, KaiZen.CSMS.Entities.FileVaultSubCategoryKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.FileVaultSubCategoryKey key)
		{
			return Delete(transactionManager, key.FileVaultSubCategoryId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_fileVaultSubCategoryId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _fileVaultSubCategoryId)
		{
			return Delete(null, _fileVaultSubCategoryId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_fileVaultSubCategoryId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _fileVaultSubCategoryId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.FileVaultSubCategory Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.FileVaultSubCategoryKey key, int start, int pageLength)
		{
			return GetByFileVaultSubCategoryId(transactionManager, key.FileVaultSubCategoryId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_FileVaultSubCategory index.
		/// </summary>
		/// <param name="_fileVaultSubCategoryId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.FileVaultSubCategory"/> class.</returns>
		public KaiZen.CSMS.Entities.FileVaultSubCategory GetByFileVaultSubCategoryId(System.Int32 _fileVaultSubCategoryId)
		{
			int count = -1;
			return GetByFileVaultSubCategoryId(null,_fileVaultSubCategoryId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_FileVaultSubCategory index.
		/// </summary>
		/// <param name="_fileVaultSubCategoryId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.FileVaultSubCategory"/> class.</returns>
		public KaiZen.CSMS.Entities.FileVaultSubCategory GetByFileVaultSubCategoryId(System.Int32 _fileVaultSubCategoryId, int start, int pageLength)
		{
			int count = -1;
			return GetByFileVaultSubCategoryId(null, _fileVaultSubCategoryId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_FileVaultSubCategory index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_fileVaultSubCategoryId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.FileVaultSubCategory"/> class.</returns>
		public KaiZen.CSMS.Entities.FileVaultSubCategory GetByFileVaultSubCategoryId(TransactionManager transactionManager, System.Int32 _fileVaultSubCategoryId)
		{
			int count = -1;
			return GetByFileVaultSubCategoryId(transactionManager, _fileVaultSubCategoryId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_FileVaultSubCategory index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_fileVaultSubCategoryId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.FileVaultSubCategory"/> class.</returns>
		public KaiZen.CSMS.Entities.FileVaultSubCategory GetByFileVaultSubCategoryId(TransactionManager transactionManager, System.Int32 _fileVaultSubCategoryId, int start, int pageLength)
		{
			int count = -1;
			return GetByFileVaultSubCategoryId(transactionManager, _fileVaultSubCategoryId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_FileVaultSubCategory index.
		/// </summary>
		/// <param name="_fileVaultSubCategoryId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.FileVaultSubCategory"/> class.</returns>
		public KaiZen.CSMS.Entities.FileVaultSubCategory GetByFileVaultSubCategoryId(System.Int32 _fileVaultSubCategoryId, int start, int pageLength, out int count)
		{
			return GetByFileVaultSubCategoryId(null, _fileVaultSubCategoryId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_FileVaultSubCategory index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_fileVaultSubCategoryId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.FileVaultSubCategory"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.FileVaultSubCategory GetByFileVaultSubCategoryId(TransactionManager transactionManager, System.Int32 _fileVaultSubCategoryId, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key UK_FileVaultSubCategoryName index.
		/// </summary>
		/// <param name="_subCategoryName"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.FileVaultSubCategory"/> class.</returns>
		public KaiZen.CSMS.Entities.FileVaultSubCategory GetBySubCategoryName(System.String _subCategoryName)
		{
			int count = -1;
			return GetBySubCategoryName(null,_subCategoryName, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the UK_FileVaultSubCategoryName index.
		/// </summary>
		/// <param name="_subCategoryName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.FileVaultSubCategory"/> class.</returns>
		public KaiZen.CSMS.Entities.FileVaultSubCategory GetBySubCategoryName(System.String _subCategoryName, int start, int pageLength)
		{
			int count = -1;
			return GetBySubCategoryName(null, _subCategoryName, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the UK_FileVaultSubCategoryName index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_subCategoryName"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.FileVaultSubCategory"/> class.</returns>
		public KaiZen.CSMS.Entities.FileVaultSubCategory GetBySubCategoryName(TransactionManager transactionManager, System.String _subCategoryName)
		{
			int count = -1;
			return GetBySubCategoryName(transactionManager, _subCategoryName, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the UK_FileVaultSubCategoryName index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_subCategoryName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.FileVaultSubCategory"/> class.</returns>
		public KaiZen.CSMS.Entities.FileVaultSubCategory GetBySubCategoryName(TransactionManager transactionManager, System.String _subCategoryName, int start, int pageLength)
		{
			int count = -1;
			return GetBySubCategoryName(transactionManager, _subCategoryName, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the UK_FileVaultSubCategoryName index.
		/// </summary>
		/// <param name="_subCategoryName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.FileVaultSubCategory"/> class.</returns>
		public KaiZen.CSMS.Entities.FileVaultSubCategory GetBySubCategoryName(System.String _subCategoryName, int start, int pageLength, out int count)
		{
			return GetBySubCategoryName(null, _subCategoryName, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the UK_FileVaultSubCategoryName index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_subCategoryName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.FileVaultSubCategory"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.FileVaultSubCategory GetBySubCategoryName(TransactionManager transactionManager, System.String _subCategoryName, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;FileVaultSubCategory&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;FileVaultSubCategory&gt;"/></returns>
		public static TList<FileVaultSubCategory> Fill(IDataReader reader, TList<FileVaultSubCategory> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.FileVaultSubCategory c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("FileVaultSubCategory")
					.Append("|").Append((System.Int32)reader[((int)FileVaultSubCategoryColumn.FileVaultSubCategoryId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<FileVaultSubCategory>(
					key.ToString(), // EntityTrackingKey
					"FileVaultSubCategory",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.FileVaultSubCategory();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.FileVaultSubCategoryId = (System.Int32)reader[((int)FileVaultSubCategoryColumn.FileVaultSubCategoryId - 1)];
					c.SubCategoryName = (System.String)reader[((int)FileVaultSubCategoryColumn.SubCategoryName - 1)];
					c.SubCategoryDesc = (System.String)reader[((int)FileVaultSubCategoryColumn.SubCategoryDesc - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.FileVaultSubCategory"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.FileVaultSubCategory"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.FileVaultSubCategory entity)
		{
			if (!reader.Read()) return;
			
			entity.FileVaultSubCategoryId = (System.Int32)reader[((int)FileVaultSubCategoryColumn.FileVaultSubCategoryId - 1)];
			entity.SubCategoryName = (System.String)reader[((int)FileVaultSubCategoryColumn.SubCategoryName - 1)];
			entity.SubCategoryDesc = (System.String)reader[((int)FileVaultSubCategoryColumn.SubCategoryDesc - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.FileVaultSubCategory"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.FileVaultSubCategory"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.FileVaultSubCategory entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.FileVaultSubCategoryId = (System.Int32)dataRow["FileVaultSubCategoryId"];
			entity.SubCategoryName = (System.String)dataRow["SubCategoryName"];
			entity.SubCategoryDesc = (System.String)dataRow["SubCategoryDesc"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.FileVaultSubCategory"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.FileVaultSubCategory Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.FileVaultSubCategory entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetByFileVaultSubCategoryId methods when available
			
			#region FileVaultTableCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<FileVaultTable>|FileVaultTableCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'FileVaultTableCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.FileVaultTableCollection = DataRepository.FileVaultTableProvider.GetByFileVaultSubCategoryId(transactionManager, entity.FileVaultSubCategoryId);

				if (deep && entity.FileVaultTableCollection.Count > 0)
				{
					deepHandles.Add("FileVaultTableCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<FileVaultTable>) DataRepository.FileVaultTableProvider.DeepLoad,
						new object[] { transactionManager, entity.FileVaultTableCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.FileVaultSubCategory object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.FileVaultSubCategory instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.FileVaultSubCategory Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.FileVaultSubCategory entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<FileVaultTable>
				if (CanDeepSave(entity.FileVaultTableCollection, "List<FileVaultTable>|FileVaultTableCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(FileVaultTable child in entity.FileVaultTableCollection)
					{
						if(child.FileVaultSubCategoryIdSource != null)
						{
							child.FileVaultSubCategoryId = child.FileVaultSubCategoryIdSource.FileVaultSubCategoryId;
						}
						else
						{
							child.FileVaultSubCategoryId = entity.FileVaultSubCategoryId;
						}

					}

					if (entity.FileVaultTableCollection.Count > 0 || entity.FileVaultTableCollection.DeletedItems.Count > 0)
					{
						//DataRepository.FileVaultTableProvider.Save(transactionManager, entity.FileVaultTableCollection);
						
						deepHandles.Add("FileVaultTableCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< FileVaultTable >) DataRepository.FileVaultTableProvider.DeepSave,
							new object[] { transactionManager, entity.FileVaultTableCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region FileVaultSubCategoryChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.FileVaultSubCategory</c>
	///</summary>
	public enum FileVaultSubCategoryChildEntityTypes
	{

		///<summary>
		/// Collection of <c>FileVaultSubCategory</c> as OneToMany for FileVaultTableCollection
		///</summary>
		[ChildEntityType(typeof(TList<FileVaultTable>))]
		FileVaultTableCollection,
	}
	
	#endregion FileVaultSubCategoryChildEntityTypes
	
	#region FileVaultSubCategoryFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;FileVaultSubCategoryColumn&gt;"/> class
	/// that is used exclusively with a <see cref="FileVaultSubCategory"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FileVaultSubCategoryFilterBuilder : SqlFilterBuilder<FileVaultSubCategoryColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FileVaultSubCategoryFilterBuilder class.
		/// </summary>
		public FileVaultSubCategoryFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the FileVaultSubCategoryFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public FileVaultSubCategoryFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the FileVaultSubCategoryFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public FileVaultSubCategoryFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion FileVaultSubCategoryFilterBuilder
	
	#region FileVaultSubCategoryParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;FileVaultSubCategoryColumn&gt;"/> class
	/// that is used exclusively with a <see cref="FileVaultSubCategory"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FileVaultSubCategoryParameterBuilder : ParameterizedSqlFilterBuilder<FileVaultSubCategoryColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FileVaultSubCategoryParameterBuilder class.
		/// </summary>
		public FileVaultSubCategoryParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the FileVaultSubCategoryParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public FileVaultSubCategoryParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the FileVaultSubCategoryParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public FileVaultSubCategoryParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion FileVaultSubCategoryParameterBuilder
	
	#region FileVaultSubCategorySortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;FileVaultSubCategoryColumn&gt;"/> class
	/// that is used exclusively with a <see cref="FileVaultSubCategory"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class FileVaultSubCategorySortBuilder : SqlSortBuilder<FileVaultSubCategoryColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FileVaultSubCategorySqlSortBuilder class.
		/// </summary>
		public FileVaultSubCategorySortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion FileVaultSubCategorySortBuilder
	
} // end namespace
