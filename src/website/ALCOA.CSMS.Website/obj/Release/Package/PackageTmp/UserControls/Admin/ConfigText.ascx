<%@ Control Language="C#" AutoEventWireup="true" Inherits="Adminv2_UserControls_ConfigText" Codebehind="ConfigText.ascx.cs" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dxtc" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dxw" %>
<%@ Register Assembly="DevExpress.Web.ASPxHtmlEditor.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxHtmlEditor" TagPrefix="dxhe" %>
<%@ Register Assembly="DevExpress.Web.ASPxSpellChecker.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxSpellChecker" TagPrefix="dxwsc" %>

<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<dxtc:ASPxPageControl ID="ASPxPageControl1" runat="server" ActiveTabIndex="0" 
    CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
    CssPostfix="Office2003Blue">
    <TabPages>
        <dxtc:TabPage Text="Library &gt; Definitions">
            <ContentCollection>
                <dxw:ContentControl runat="server">Library &gt; Definitions &gt; Embedded / Non-Embedded<br /> <br />
                    <dxhe:ASPxHtmlEditor
                    ID="ASPxHtmlEditor2" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                    CssPostfix="Office2003Blue">
<Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue">
<ViewArea>
<Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px" />
</ViewArea>
</Styles>

<StylesRoundPanel>
<ControlStyle BackColor="#DDECFE">
<Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px" />
</ControlStyle>
</StylesRoundPanel>

<StylesStatusBar>
<ActiveTab BackColor="White"></ActiveTab>
</StylesStatusBar>

<SettingsImageUpload>
<ValidationSettings AllowedFileExtensions=".jpg, .jpeg, .gif, .png"></ValidationSettings>
</SettingsImageUpload>

<SettingsImageSelector>
<CommonSettings AllowedFileExtensions=".jpe, .jpeg, .jpg, .gif, .png"></CommonSettings>
</SettingsImageSelector>

<SettingsDocumentSelector>
<CommonSettings AllowedFileExtensions=".rtf, .pdf, .doc, .docx, .odt, .txt, .xls, .xlsx, .ods, .ppt, .pptx, .odp"></CommonSettings>
</SettingsDocumentSelector>

<Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
    <LoadingPanel Url="~/App_Themes/Office2003BlueOld/HtmlEditor/Loading.gif"><%--Enhancement_023:change by debashis for testing platform upgradation--%>
    </LoadingPanel>
                        </Images>

<PartsRoundPanel>
<TopLeftCorner Height="9px"
                            Width="9px" />

<NoHeaderTopLeftCorner Height="9px"
                            Width="9px" />

<TopRightCorner Height="9px"
                            Width="9px" />

<NoHeaderTopRightCorner Height="9px"
                            Width="9px" />

<BottomRightCorner Height="9px"
                            Width="9px" />

<BottomLeftCorner Height="9px"
                            Width="9px" />

<HeaderLeftEdge>
<BackgroundImage HorizontalPosition="left" ImageUrl="~/App_Themes/Office2003Blue/HtmlEditor/RoundPanel/herpHeader.png"
                                Repeat="RepeatX" VerticalPosition="top" />
</HeaderLeftEdge>

<HeaderContent>
<BackgroundImage HorizontalPosition="left" ImageUrl="~/App_Themes/Office2003Blue/HtmlEditor/RoundPanel/herpHeader.png"
                                Repeat="RepeatX" VerticalPosition="top" />
</HeaderContent>

<HeaderRightEdge>
<BackgroundImage HorizontalPosition="left" ImageUrl="~/App_Themes/Office2003Blue/HtmlEditor/RoundPanel/herpHeader.png"
                                Repeat="RepeatX" VerticalPosition="top" />
</HeaderRightEdge>

<NoHeaderTopEdge BackColor="#DDECFE"></NoHeaderTopEdge>

<TopEdge>
<BackgroundImage HorizontalPosition="left" ImageUrl="~/App_Themes/Office2003Blue/HtmlEditor/RoundPanel/herpTopEdge.png"
                                Repeat="RepeatX" VerticalPosition="top" />
</TopEdge>
</PartsRoundPanel>
</dxhe:ASPxHtmlEditor>
 <br /><br />Library &gt; Definitions &gt; Safety Frequency Rates<br /> <br />
                    <dxhe:ASPxHtmlEditor
    ID="ASPxHtmlEditor3" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
    CssPostfix="Office2003Blue">
<Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue">
<ViewArea>
<Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px" />
</ViewArea>
</Styles>

<StylesRoundPanel>
<ControlStyle BackColor="#DDECFE">
<Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px" />
</ControlStyle>
</StylesRoundPanel>

<StylesStatusBar>
<ActiveTab BackColor="White"></ActiveTab>
</StylesStatusBar>

<SettingsImageUpload>
<ValidationSettings AllowedFileExtensions=".jpg, .jpeg, .gif, .png"></ValidationSettings>
</SettingsImageUpload>

<SettingsImageSelector>
<CommonSettings AllowedFileExtensions=".jpe, .jpeg, .jpg, .gif, .png"></CommonSettings>
</SettingsImageSelector>

<SettingsDocumentSelector>
<CommonSettings AllowedFileExtensions=".rtf, .pdf, .doc, .docx, .odt, .txt, .xls, .xlsx, .ods, .ppt, .pptx, .odp"></CommonSettings>
</SettingsDocumentSelector>

<Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
    <LoadingPanel Url="~/App_Themes/Office2003BlueOld/HtmlEditor/Loading.gif"><%--Enhancement_023:change by debashis for testing platform upgradation--%>
    </LoadingPanel>
                        </Images>

<PartsRoundPanel>
<TopLeftCorner Height="9px"
            Width="9px" />

<NoHeaderTopLeftCorner Height="9px"
            Width="9px" />

<TopRightCorner Height="9px"
            Width="9px" />

<NoHeaderTopRightCorner Height="9px"
            Width="9px" />

<BottomRightCorner Height="9px"
            Width="9px" />

<BottomLeftCorner Height="9px"
            Width="9px" />

<HeaderLeftEdge>
<BackgroundImage HorizontalPosition="left" ImageUrl="~/App_Themes/Office2003Blue/HtmlEditor/RoundPanel/herpHeader.png"
                Repeat="RepeatX" VerticalPosition="top" />
</HeaderLeftEdge>

<HeaderContent>
<BackgroundImage HorizontalPosition="left" ImageUrl="~/App_Themes/Office2003Blue/HtmlEditor/RoundPanel/herpHeader.png"
                Repeat="RepeatX" VerticalPosition="top" />
</HeaderContent>

<HeaderRightEdge>
<BackgroundImage HorizontalPosition="left" ImageUrl="~/App_Themes/Office2003Blue/HtmlEditor/RoundPanel/herpHeader.png"
                Repeat="RepeatX" VerticalPosition="top" />
</HeaderRightEdge>

<NoHeaderTopEdge BackColor="#DDECFE"></NoHeaderTopEdge>

<TopEdge>
<BackgroundImage HorizontalPosition="left" ImageUrl="~/App_Themes/Office2003Blue/HtmlEditor/RoundPanel/herpTopEdge.png"
                Repeat="RepeatX" VerticalPosition="top" />
</TopEdge>
</PartsRoundPanel>
</dxhe:ASPxHtmlEditor>
                    <br />
                    <br />
                    Library &gt; Definitions &gt; Supervision Requirements<br />
                    <dxhe:ASPxHtmlEditor ID="ASPxHtmlEditor4" runat="server" 
                        CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
                        CssPostfix="Office2003Blue">
                        <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
                            CssPostfix="Office2003Blue">
                            <ViewArea>
                                <Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px" />
                            </ViewArea>
                        </Styles>
                        <StylesRoundPanel>
                            <ControlStyle BackColor="#DDECFE">
                            <Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px" />
                            </ControlStyle>
                        </StylesRoundPanel>
                        <StylesStatusBar>
                            <ActiveTab BackColor="White">
                            </ActiveTab>
                        </StylesStatusBar>
                        <SettingsImageUpload>
                            <ValidationSettings AllowedFileExtensions=".jpg, .jpeg, .gif, .png">
                            </ValidationSettings>
                        </SettingsImageUpload>
                        <SettingsImageSelector>
                            <CommonSettings AllowedFileExtensions=".jpe, .jpeg, .jpg, .gif, .png" />
                        </SettingsImageSelector>
                        <SettingsDocumentSelector>
                            <CommonSettings AllowedFileExtensions=".rtf, .pdf, .doc, .docx, .odt, .txt, .xls, .xlsx, .ods, .ppt, .pptx, .odp" />
                        </SettingsDocumentSelector>
                        <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                            <LoadingPanel Url="~/App_Themes/Office2003BlueOld/HtmlEditor/Loading.gif"><%--Enhancement_023:change by debashis for testing platform upgradation--%>
                            </LoadingPanel>
                        </Images>
                        <PartsRoundPanel>
                            <TopLeftCorner Height="9px" Width="9px">
                            </TopLeftCorner>
                            <NoHeaderTopLeftCorner Height="9px" Width="9px">
                            </NoHeaderTopLeftCorner>
                            <TopRightCorner Height="9px" Width="9px">
                            </TopRightCorner>
                            <NoHeaderTopRightCorner Height="9px" Width="9px">
                            </NoHeaderTopRightCorner>
                            <BottomRightCorner Height="9px" Width="9px">
                            </BottomRightCorner>
                            <BottomLeftCorner Height="9px" Width="9px">
                            </BottomLeftCorner>
                            <HeaderLeftEdge>
                                <BackgroundImage HorizontalPosition="left" 
                                    ImageUrl="~/App_Themes/Office2003Blue/HtmlEditor/RoundPanel/herpHeader.png" 
                                    Repeat="RepeatX" VerticalPosition="top" />
                            </HeaderLeftEdge>
                            <HeaderContent>
                                <BackgroundImage HorizontalPosition="left" 
                                    ImageUrl="~/App_Themes/Office2003Blue/HtmlEditor/RoundPanel/herpHeader.png" 
                                    Repeat="RepeatX" VerticalPosition="top" />
                            </HeaderContent>
                            <HeaderRightEdge>
                                <BackgroundImage HorizontalPosition="left" 
                                    ImageUrl="~/App_Themes/Office2003Blue/HtmlEditor/RoundPanel/herpHeader.png" 
                                    Repeat="RepeatX" VerticalPosition="top" />
                            </HeaderRightEdge>
                            <NoHeaderTopEdge BackColor="#DDECFE">
                            </NoHeaderTopEdge>
                            <TopEdge>
                                <BackgroundImage HorizontalPosition="left" 
                                    ImageUrl="~/App_Themes/Office2003Blue/HtmlEditor/RoundPanel/herpTopEdge.png" 
                                    Repeat="RepeatX" VerticalPosition="top" />
                            </TopEdge>
                        </PartsRoundPanel>
                    </dxhe:ASPxHtmlEditor>
                    <br />
 <br /><br /><asp:Label ID="lblError" runat="server"></asp:Label>
                    <br />
                    <dxe:ASPxButton ID="ASPxButton1" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
    CssPostfix="Office2003Blue" OnClick="ASPxButton1_Click" Text="Save/Update" 
                        SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"></dxe:ASPxButton>
 </dxw:ContentControl>
            </ContentCollection>
        </dxtc:TabPage>
        <dxtc:TabPage Text="Loading Messages">
            <ContentCollection>
                <dxw:ContentControl runat="server">
<dxwgv:ASPxGridView
            ID="ASPxGridView1"
            runat="server"
            AutoGenerateColumns="False"
            CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
            CssPostfix="Office2003Blue"
            DataSourceID="ConfigTextDataSource1" Oninitnewrow="grid_InitNewRow"
            KeyFieldName="ConfigTextId" Width="900px">
    <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
        <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
        </LoadingPanelOnStatusBar>
        <CollapsedButton Height="12px" Width="11px" />
        <ExpandedButton Height="12px" Width="11px" />
        <DetailCollapsedButton Height="12px" Width="11px" />
        <DetailExpandedButton Height="12px" Width="11px" />
        <LoadingPanel Url="~/App_Themes/Office2003BlueOld/GridView/Loading.gif"><%--Enhancement_023:change by debashis for testing platform upgradation--%>
        </LoadingPanel>
    </Images>
    <ImagesFilterControl>
        <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
        </LoadingPanel>
    </ImagesFilterControl>
    <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
        CssPostfix="Office2003Blue">
        <Header ImageSpacing="5px" SortingImageSpacing="5px" />
        <LoadingPanel ImageSpacing="10px" />
    </Styles>
    <Columns>
        <dxwgv:GridViewCommandColumn VisibleIndex="0" Width="60px" Caption="Action">
            <EditButton Visible="True">
            </EditButton>
            <NewButton Visible="True">
            </NewButton>
            <DeleteButton Visible="True">
            </DeleteButton>
            <HeaderStyle HorizontalAlign="Center" />
        </dxwgv:GridViewCommandColumn>
        <dxwgv:GridViewDataTextColumn FieldName="ConfigTextId" ReadOnly="True" Visible="False"
                    VisibleIndex="0">
            <EditFormSettings Visible="False" />
        </dxwgv:GridViewDataTextColumn>
        <dxwgv:GridViewDataComboBoxColumn Caption="Type" FieldName="ConfigTextTypeId" VisibleIndex="1"
                    Width="110px">
            <PropertiesComboBox DataSourceID="ConfigTextTypeDataSource1" TextField="ConfigTextTypeDesc"
                        ValueField="ConfigTextTypeId" ValueType="System.Int32">
            </PropertiesComboBox>
        </dxwgv:GridViewDataComboBoxColumn>
        <dxwgv:GridViewDataTextColumn FieldName="ConfigText" VisibleIndex="2" Width="710px">
        </dxwgv:GridViewDataTextColumn>
    </Columns>
    <Settings ShowGroupPanel="True" ShowHeaderFilterButton="True" />
    <SettingsBehavior ConfirmDelete="True" />
    <SettingsPager>
        <AllButton Visible="True">
        </AllButton>
    </SettingsPager>
    <SettingsEditing Mode="Inline" />
</dxwgv:ASPxGridView>

                </dxw:ContentControl>
            </ContentCollection>
        </dxtc:TabPage>
    </TabPages>
    <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
    </LoadingPanelImage>
    <LoadingPanelStyle ImageSpacing="6px">
    </LoadingPanelStyle>
    <ContentStyle>
        <Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px" />
    </ContentStyle>
</dxtc:ASPxPageControl>
<br />
<br />
<data:ConfigTextDataSource ID="ConfigTextDataSource1" runat="server" />
<br />
<data:ConfigTextTypeDataSource ID="ConfigTextTypeDataSource1" runat="server" />
