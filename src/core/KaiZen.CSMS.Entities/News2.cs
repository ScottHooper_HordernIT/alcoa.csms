﻿#region Using directives

using System;

#endregion

namespace KaiZen.CSMS.Entities
{	
	///<summary>
	/// An object representation of the 'News2' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class News2 : News2Base
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="News2"/> instance.
		///</summary>
		public News2():base(){}	
		
		#endregion
	}
}
