﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Services;
using KaiZen.CSMS.Web;

using System.Text;

public partial class PopUps_QuestionnaireRationale : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //Type = Supplier | Verification
        //SectionId = 1-7 (Verification only)
        //QuestionId = 1-30, Summary etc.
        //SubType == Why | Help
        try
        {
            if (Request.QueryString["Type"] != null)
            {
                switch (Request.QueryString["Type"])
                {
                    case "Supplier":
                        if (Request.QueryString["QuestionId"] != null)
                        {
                            if (Request.QueryString["SubType"] != null)
                            {
                                QuestionnaireMainRationaleService qmrService = new QuestionnaireMainRationaleService();
                                QuestionnaireMainRationale q = qmrService.GetByQuestionNo(Request.QueryString["QuestionId"]);
                                if (q != null)
                                {
                                    switch (Request.QueryString["SubType"])
                                    {
                                        case "Why":
                                            if (q.Rationale != null)
                                            {
                                                if (q.Rationale.Length != 0)
                                                    PlaceHolder1.Controls.Add(new LiteralControl(Encoding.ASCII.GetString(q.Rationale)));
                                            }
                                            break;
                                        case "Help":
                                            if (q.Assistance != null)
                                            {
                                                if (q.Assistance.Length != 0)
                                                    PlaceHolder1.Controls.Add(new LiteralControl(Encoding.ASCII.GetString(q.Assistance)));
                                            }
                                            break;
                                        default:
                                            throw new Exception("Argument Expected. Correct SubType Not Supplied.");
                                    }
                                }
                            }
                            else
                            {
                                throw new Exception("Argument Expected. SubType Not Supplied.");
                            }
                        }
                        else
                        {
                            throw new Exception("Argument Expected. QuestionId Not Supplied.");
                        }
                        break;
                    case "Verification":
                        if (Request.QueryString["SectionId"] != null || Request.QueryString["QuestionId"] != null || Request.QueryString["SubType"] != null)
                        {
                            int SectionId = -1;
                            int.TryParse(Request.QueryString["SectionId"], out SectionId);
                            if (SectionId == -1) throw new Exception("Argument Expected. Correct SectionId Not Supplied.");

                            QuestionnaireVerificationRationaleService qvrService = new QuestionnaireVerificationRationaleService();
                            QuestionnaireVerificationRationale q = qvrService.GetBySectionIdQuestionId(SectionId, Request.QueryString["QuestionId"]);
                            
                            if (q != null)
                            {
                                switch (Request.QueryString["SubType"])
                                {
                                    case "Why":
                                        if (q.Rationale != null)
                                        {
                                            if (q.Rationale.Length != 0)
                                                PlaceHolder1.Controls.Add(new LiteralControl(Encoding.ASCII.GetString(q.Rationale)));
                                        }
                                        break;
                                    case "Help":
                                        if (q.Assistance != null)
                                        {
                                            if (q.Assistance.Length != 0)
                                                PlaceHolder1.Controls.Add(new LiteralControl(Encoding.ASCII.GetString(q.Assistance)));
                                        }
                                        break;
                                    default:
                                        throw new Exception("Argument Expected. Correct SubType Not Supplied.");
                                }
                            }
                        }
                        else
                        {
                            throw new Exception("Argument Expected. SectionId, QuestionId or SubType Not Supplied.");
                        }
                        break;
                    default:
                        throw new Exception("Argument Expected. Type Not Supplied.");
                }
            }
        }
        catch (Exception ex)
        {
            Elmah.ErrorSignal.FromContext(Context).Raise(ex);
            throw new Exception(ex.Message);
        }
    }
}
