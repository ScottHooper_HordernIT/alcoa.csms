﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="ConfigNavigationProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class ConfigNavigationProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.ConfigNavigation, KaiZen.CSMS.Entities.ConfigNavigationKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.ConfigNavigationKey key)
		{
			return Delete(transactionManager, key.ConfigNavigationId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_configNavigationId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _configNavigationId)
		{
			return Delete(null, _configNavigationId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_configNavigationId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _configNavigationId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.ConfigNavigation Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.ConfigNavigationKey key, int start, int pageLength)
		{
			return GetByConfigNavigationId(transactionManager, key.ConfigNavigationId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_ConfigNavigation index.
		/// </summary>
		/// <param name="_configNavigationId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.ConfigNavigation"/> class.</returns>
		public KaiZen.CSMS.Entities.ConfigNavigation GetByConfigNavigationId(System.Int32 _configNavigationId)
		{
			int count = -1;
			return GetByConfigNavigationId(null,_configNavigationId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ConfigNavigation index.
		/// </summary>
		/// <param name="_configNavigationId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.ConfigNavigation"/> class.</returns>
		public KaiZen.CSMS.Entities.ConfigNavigation GetByConfigNavigationId(System.Int32 _configNavigationId, int start, int pageLength)
		{
			int count = -1;
			return GetByConfigNavigationId(null, _configNavigationId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ConfigNavigation index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_configNavigationId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.ConfigNavigation"/> class.</returns>
		public KaiZen.CSMS.Entities.ConfigNavigation GetByConfigNavigationId(TransactionManager transactionManager, System.Int32 _configNavigationId)
		{
			int count = -1;
			return GetByConfigNavigationId(transactionManager, _configNavigationId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ConfigNavigation index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_configNavigationId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.ConfigNavigation"/> class.</returns>
		public KaiZen.CSMS.Entities.ConfigNavigation GetByConfigNavigationId(TransactionManager transactionManager, System.Int32 _configNavigationId, int start, int pageLength)
		{
			int count = -1;
			return GetByConfigNavigationId(transactionManager, _configNavigationId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ConfigNavigation index.
		/// </summary>
		/// <param name="_configNavigationId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.ConfigNavigation"/> class.</returns>
		public KaiZen.CSMS.Entities.ConfigNavigation GetByConfigNavigationId(System.Int32 _configNavigationId, int start, int pageLength, out int count)
		{
			return GetByConfigNavigationId(null, _configNavigationId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ConfigNavigation index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_configNavigationId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.ConfigNavigation"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.ConfigNavigation GetByConfigNavigationId(TransactionManager transactionManager, System.Int32 _configNavigationId, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ConfigNavigation index.
		/// </summary>
		/// <param name="_navigateUrl"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.ConfigNavigation"/> class.</returns>
		public KaiZen.CSMS.Entities.ConfigNavigation GetByNavigateUrl(System.String _navigateUrl)
		{
			int count = -1;
			return GetByNavigateUrl(null,_navigateUrl, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ConfigNavigation index.
		/// </summary>
		/// <param name="_navigateUrl"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.ConfigNavigation"/> class.</returns>
		public KaiZen.CSMS.Entities.ConfigNavigation GetByNavigateUrl(System.String _navigateUrl, int start, int pageLength)
		{
			int count = -1;
			return GetByNavigateUrl(null, _navigateUrl, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ConfigNavigation index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_navigateUrl"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.ConfigNavigation"/> class.</returns>
		public KaiZen.CSMS.Entities.ConfigNavigation GetByNavigateUrl(TransactionManager transactionManager, System.String _navigateUrl)
		{
			int count = -1;
			return GetByNavigateUrl(transactionManager, _navigateUrl, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ConfigNavigation index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_navigateUrl"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.ConfigNavigation"/> class.</returns>
		public KaiZen.CSMS.Entities.ConfigNavigation GetByNavigateUrl(TransactionManager transactionManager, System.String _navigateUrl, int start, int pageLength)
		{
			int count = -1;
			return GetByNavigateUrl(transactionManager, _navigateUrl, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ConfigNavigation index.
		/// </summary>
		/// <param name="_navigateUrl"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.ConfigNavigation"/> class.</returns>
		public KaiZen.CSMS.Entities.ConfigNavigation GetByNavigateUrl(System.String _navigateUrl, int start, int pageLength, out int count)
		{
			return GetByNavigateUrl(null, _navigateUrl, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ConfigNavigation index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_navigateUrl"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.ConfigNavigation"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.ConfigNavigation GetByNavigateUrl(TransactionManager transactionManager, System.String _navigateUrl, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;ConfigNavigation&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;ConfigNavigation&gt;"/></returns>
		public static TList<ConfigNavigation> Fill(IDataReader reader, TList<ConfigNavigation> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.ConfigNavigation c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("ConfigNavigation")
					.Append("|").Append((System.Int32)reader[((int)ConfigNavigationColumn.ConfigNavigationId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<ConfigNavigation>(
					key.ToString(), // EntityTrackingKey
					"ConfigNavigation",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.ConfigNavigation();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.ConfigNavigationId = (System.Int32)reader[((int)ConfigNavigationColumn.ConfigNavigationId - 1)];
					c.ParentId = (System.Int32)reader[((int)ConfigNavigationColumn.ParentId - 1)];
					c.Text = (reader.IsDBNull(((int)ConfigNavigationColumn.Text - 1)))?null:(System.String)reader[((int)ConfigNavigationColumn.Text - 1)];
					c.TextContractor = (reader.IsDBNull(((int)ConfigNavigationColumn.TextContractor - 1)))?null:(System.String)reader[((int)ConfigNavigationColumn.TextContractor - 1)];
					c.NavigateUrl = (reader.IsDBNull(((int)ConfigNavigationColumn.NavigateUrl - 1)))?null:(System.String)reader[((int)ConfigNavigationColumn.NavigateUrl - 1)];
					c.NavigateUrlContractor = (reader.IsDBNull(((int)ConfigNavigationColumn.NavigateUrlContractor - 1)))?null:(System.String)reader[((int)ConfigNavigationColumn.NavigateUrlContractor - 1)];
					c.VisibleBy = (reader.IsDBNull(((int)ConfigNavigationColumn.VisibleBy - 1)))?null:(System.String)reader[((int)ConfigNavigationColumn.VisibleBy - 1)];
					c.DisplayOrderId = (reader.IsDBNull(((int)ConfigNavigationColumn.DisplayOrderId - 1)))?null:(System.Int32?)reader[((int)ConfigNavigationColumn.DisplayOrderId - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.ConfigNavigation"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.ConfigNavigation"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.ConfigNavigation entity)
		{
			if (!reader.Read()) return;
			
			entity.ConfigNavigationId = (System.Int32)reader[((int)ConfigNavigationColumn.ConfigNavigationId - 1)];
			entity.ParentId = (System.Int32)reader[((int)ConfigNavigationColumn.ParentId - 1)];
			entity.Text = (reader.IsDBNull(((int)ConfigNavigationColumn.Text - 1)))?null:(System.String)reader[((int)ConfigNavigationColumn.Text - 1)];
			entity.TextContractor = (reader.IsDBNull(((int)ConfigNavigationColumn.TextContractor - 1)))?null:(System.String)reader[((int)ConfigNavigationColumn.TextContractor - 1)];
			entity.NavigateUrl = (reader.IsDBNull(((int)ConfigNavigationColumn.NavigateUrl - 1)))?null:(System.String)reader[((int)ConfigNavigationColumn.NavigateUrl - 1)];
			entity.NavigateUrlContractor = (reader.IsDBNull(((int)ConfigNavigationColumn.NavigateUrlContractor - 1)))?null:(System.String)reader[((int)ConfigNavigationColumn.NavigateUrlContractor - 1)];
			entity.VisibleBy = (reader.IsDBNull(((int)ConfigNavigationColumn.VisibleBy - 1)))?null:(System.String)reader[((int)ConfigNavigationColumn.VisibleBy - 1)];
			entity.DisplayOrderId = (reader.IsDBNull(((int)ConfigNavigationColumn.DisplayOrderId - 1)))?null:(System.Int32?)reader[((int)ConfigNavigationColumn.DisplayOrderId - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.ConfigNavigation"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.ConfigNavigation"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.ConfigNavigation entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.ConfigNavigationId = (System.Int32)dataRow["ConfigNavigationId"];
			entity.ParentId = (System.Int32)dataRow["ParentId"];
			entity.Text = Convert.IsDBNull(dataRow["Text"]) ? null : (System.String)dataRow["Text"];
			entity.TextContractor = Convert.IsDBNull(dataRow["Text_Contractor"]) ? null : (System.String)dataRow["Text_Contractor"];
			entity.NavigateUrl = Convert.IsDBNull(dataRow["NavigateURL"]) ? null : (System.String)dataRow["NavigateURL"];
			entity.NavigateUrlContractor = Convert.IsDBNull(dataRow["NavigateURL_Contractor"]) ? null : (System.String)dataRow["NavigateURL_Contractor"];
			entity.VisibleBy = Convert.IsDBNull(dataRow["VisibleBy"]) ? null : (System.String)dataRow["VisibleBy"];
			entity.DisplayOrderId = Convert.IsDBNull(dataRow["DisplayOrderId"]) ? null : (System.Int32?)dataRow["DisplayOrderId"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.ConfigNavigation"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.ConfigNavigation Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.ConfigNavigation entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.ConfigNavigation object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.ConfigNavigation instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.ConfigNavigation Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.ConfigNavigation entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region ConfigNavigationChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.ConfigNavigation</c>
	///</summary>
	public enum ConfigNavigationChildEntityTypes
	{
	}
	
	#endregion ConfigNavigationChildEntityTypes
	
	#region ConfigNavigationFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;ConfigNavigationColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ConfigNavigation"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ConfigNavigationFilterBuilder : SqlFilterBuilder<ConfigNavigationColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ConfigNavigationFilterBuilder class.
		/// </summary>
		public ConfigNavigationFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ConfigNavigationFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ConfigNavigationFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ConfigNavigationFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ConfigNavigationFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ConfigNavigationFilterBuilder
	
	#region ConfigNavigationParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;ConfigNavigationColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ConfigNavigation"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ConfigNavigationParameterBuilder : ParameterizedSqlFilterBuilder<ConfigNavigationColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ConfigNavigationParameterBuilder class.
		/// </summary>
		public ConfigNavigationParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ConfigNavigationParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ConfigNavigationParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ConfigNavigationParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ConfigNavigationParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ConfigNavigationParameterBuilder
	
	#region ConfigNavigationSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;ConfigNavigationColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ConfigNavigation"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class ConfigNavigationSortBuilder : SqlSortBuilder<ConfigNavigationColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ConfigNavigationSqlSortBuilder class.
		/// </summary>
		public ConfigNavigationSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion ConfigNavigationSortBuilder
	
} // end namespace
