﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.QuestionnaireProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(QuestionnaireDataSourceDesigner))]
	public class QuestionnaireDataSource : ProviderDataSource<Questionnaire, QuestionnaireKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireDataSource class.
		/// </summary>
		public QuestionnaireDataSource() : base(new QuestionnaireService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the QuestionnaireDataSourceView used by the QuestionnaireDataSource.
		/// </summary>
		protected QuestionnaireDataSourceView QuestionnaireView
		{
			get { return ( View as QuestionnaireDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the QuestionnaireDataSource control invokes to retrieve data.
		/// </summary>
		public QuestionnaireSelectMethod SelectMethod
		{
			get
			{
				QuestionnaireSelectMethod selectMethod = QuestionnaireSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (QuestionnaireSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the QuestionnaireDataSourceView class that is to be
		/// used by the QuestionnaireDataSource.
		/// </summary>
		/// <returns>An instance of the QuestionnaireDataSourceView class.</returns>
		protected override BaseDataSourceView<Questionnaire, QuestionnaireKey> GetNewDataSourceView()
		{
			return new QuestionnaireDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the QuestionnaireDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class QuestionnaireDataSourceView : ProviderDataSourceView<Questionnaire, QuestionnaireKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the QuestionnaireDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public QuestionnaireDataSourceView(QuestionnaireDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal QuestionnaireDataSource QuestionnaireOwner
		{
			get { return Owner as QuestionnaireDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal QuestionnaireSelectMethod SelectMethod
		{
			get { return QuestionnaireOwner.SelectMethod; }
			set { QuestionnaireOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal QuestionnaireService QuestionnaireProvider
		{
			get { return Provider as QuestionnaireService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<Questionnaire> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<Questionnaire> results = null;
			Questionnaire item;
			count = 0;
			
			System.Int32 _questionnaireId;
			System.Int32? _questionnairePresentlyWithActionId_nullable;
			System.Int32? _mainAssessmentByUserId_nullable;
			System.Int32? _initialModifiedByUserId_nullable;
			System.Int32? _mainModifiedByUserId_nullable;
			System.Int32 _status;
			System.Int32 _initialStatus;
			System.Int32 _mainStatus;
			System.Int32 _verificationStatus;
			System.Int32? _approvedByUserId_nullable;
			System.Int32 _createdByUserId;
			System.Int32 _modifiedByUserId;
			System.Int32? _verificationModifiedByUserId_nullable;

			switch ( SelectMethod )
			{
				case QuestionnaireSelectMethod.Get:
					QuestionnaireKey entityKey  = new QuestionnaireKey();
					entityKey.Load(values);
					item = QuestionnaireProvider.Get(entityKey);
					results = new TList<Questionnaire>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case QuestionnaireSelectMethod.GetAll:
                    results = QuestionnaireProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case QuestionnaireSelectMethod.GetPaged:
					results = QuestionnaireProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case QuestionnaireSelectMethod.Find:
					if ( FilterParameters != null )
						results = QuestionnaireProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = QuestionnaireProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case QuestionnaireSelectMethod.GetByQuestionnaireId:
					_questionnaireId = ( values["QuestionnaireId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["QuestionnaireId"], typeof(System.Int32)) : (int)0;
					item = QuestionnaireProvider.GetByQuestionnaireId(_questionnaireId);
					results = new TList<Questionnaire>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				case QuestionnaireSelectMethod.GetByQuestionnairePresentlyWithActionId:
					_questionnairePresentlyWithActionId_nullable = (System.Int32?) EntityUtil.ChangeType(values["QuestionnairePresentlyWithActionId"], typeof(System.Int32?));
					results = QuestionnaireProvider.GetByQuestionnairePresentlyWithActionId(_questionnairePresentlyWithActionId_nullable, this.StartIndex, this.PageSize, out count);
					break;
				// FK
				case QuestionnaireSelectMethod.GetByMainAssessmentByUserId:
					_mainAssessmentByUserId_nullable = (System.Int32?) EntityUtil.ChangeType(values["MainAssessmentByUserId"], typeof(System.Int32?));
					results = QuestionnaireProvider.GetByMainAssessmentByUserId(_mainAssessmentByUserId_nullable, this.StartIndex, this.PageSize, out count);
					break;
				case QuestionnaireSelectMethod.GetByInitialModifiedByUserId:
					_initialModifiedByUserId_nullable = (System.Int32?) EntityUtil.ChangeType(values["InitialModifiedByUserId"], typeof(System.Int32?));
					results = QuestionnaireProvider.GetByInitialModifiedByUserId(_initialModifiedByUserId_nullable, this.StartIndex, this.PageSize, out count);
					break;
				case QuestionnaireSelectMethod.GetByMainModifiedByUserId:
					_mainModifiedByUserId_nullable = (System.Int32?) EntityUtil.ChangeType(values["MainModifiedByUserId"], typeof(System.Int32?));
					results = QuestionnaireProvider.GetByMainModifiedByUserId(_mainModifiedByUserId_nullable, this.StartIndex, this.PageSize, out count);
					break;
				case QuestionnaireSelectMethod.GetByStatus:
					_status = ( values["Status"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["Status"], typeof(System.Int32)) : (int)0;
					results = QuestionnaireProvider.GetByStatus(_status, this.StartIndex, this.PageSize, out count);
					break;
				case QuestionnaireSelectMethod.GetByInitialStatus:
					_initialStatus = ( values["InitialStatus"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["InitialStatus"], typeof(System.Int32)) : (int)0;
					results = QuestionnaireProvider.GetByInitialStatus(_initialStatus, this.StartIndex, this.PageSize, out count);
					break;
				case QuestionnaireSelectMethod.GetByMainStatus:
					_mainStatus = ( values["MainStatus"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["MainStatus"], typeof(System.Int32)) : (int)0;
					results = QuestionnaireProvider.GetByMainStatus(_mainStatus, this.StartIndex, this.PageSize, out count);
					break;
				case QuestionnaireSelectMethod.GetByVerificationStatus:
					_verificationStatus = ( values["VerificationStatus"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["VerificationStatus"], typeof(System.Int32)) : (int)0;
					results = QuestionnaireProvider.GetByVerificationStatus(_verificationStatus, this.StartIndex, this.PageSize, out count);
					break;
				case QuestionnaireSelectMethod.GetByApprovedByUserId:
					_approvedByUserId_nullable = (System.Int32?) EntityUtil.ChangeType(values["ApprovedByUserId"], typeof(System.Int32?));
					results = QuestionnaireProvider.GetByApprovedByUserId(_approvedByUserId_nullable, this.StartIndex, this.PageSize, out count);
					break;
				case QuestionnaireSelectMethod.GetByCreatedByUserId:
					_createdByUserId = ( values["CreatedByUserId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["CreatedByUserId"], typeof(System.Int32)) : (int)0;
					results = QuestionnaireProvider.GetByCreatedByUserId(_createdByUserId, this.StartIndex, this.PageSize, out count);
					break;
				case QuestionnaireSelectMethod.GetByModifiedByUserId:
					_modifiedByUserId = ( values["ModifiedByUserId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["ModifiedByUserId"], typeof(System.Int32)) : (int)0;
					results = QuestionnaireProvider.GetByModifiedByUserId(_modifiedByUserId, this.StartIndex, this.PageSize, out count);
					break;
				case QuestionnaireSelectMethod.GetByVerificationModifiedByUserId:
					_verificationModifiedByUserId_nullable = (System.Int32?) EntityUtil.ChangeType(values["VerificationModifiedByUserId"], typeof(System.Int32?));
					results = QuestionnaireProvider.GetByVerificationModifiedByUserId(_verificationModifiedByUserId_nullable, this.StartIndex, this.PageSize, out count);
					break;
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == QuestionnaireSelectMethod.Get || SelectMethod == QuestionnaireSelectMethod.GetByQuestionnaireId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				Questionnaire entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					QuestionnaireProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<Questionnaire> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			QuestionnaireProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region QuestionnaireDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the QuestionnaireDataSource class.
	/// </summary>
	public class QuestionnaireDataSourceDesigner : ProviderDataSourceDesigner<Questionnaire, QuestionnaireKey>
	{
		/// <summary>
		/// Initializes a new instance of the QuestionnaireDataSourceDesigner class.
		/// </summary>
		public QuestionnaireDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public QuestionnaireSelectMethod SelectMethod
		{
			get { return ((QuestionnaireDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new QuestionnaireDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region QuestionnaireDataSourceActionList

	/// <summary>
	/// Supports the QuestionnaireDataSourceDesigner class.
	/// </summary>
	internal class QuestionnaireDataSourceActionList : DesignerActionList
	{
		private QuestionnaireDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the QuestionnaireDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public QuestionnaireDataSourceActionList(QuestionnaireDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public QuestionnaireSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion QuestionnaireDataSourceActionList
	
	#endregion QuestionnaireDataSourceDesigner
	
	#region QuestionnaireSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the QuestionnaireDataSource.SelectMethod property.
	/// </summary>
	public enum QuestionnaireSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByQuestionnaireId method.
		/// </summary>
		GetByQuestionnaireId,
		/// <summary>
		/// Represents the GetByQuestionnairePresentlyWithActionId method.
		/// </summary>
		GetByQuestionnairePresentlyWithActionId,
		/// <summary>
		/// Represents the GetByMainAssessmentByUserId method.
		/// </summary>
		GetByMainAssessmentByUserId,
		/// <summary>
		/// Represents the GetByInitialModifiedByUserId method.
		/// </summary>
		GetByInitialModifiedByUserId,
		/// <summary>
		/// Represents the GetByMainModifiedByUserId method.
		/// </summary>
		GetByMainModifiedByUserId,
		/// <summary>
		/// Represents the GetByStatus method.
		/// </summary>
		GetByStatus,
		/// <summary>
		/// Represents the GetByInitialStatus method.
		/// </summary>
		GetByInitialStatus,
		/// <summary>
		/// Represents the GetByMainStatus method.
		/// </summary>
		GetByMainStatus,
		/// <summary>
		/// Represents the GetByVerificationStatus method.
		/// </summary>
		GetByVerificationStatus,
		/// <summary>
		/// Represents the GetByApprovedByUserId method.
		/// </summary>
		GetByApprovedByUserId,
		/// <summary>
		/// Represents the GetByCreatedByUserId method.
		/// </summary>
		GetByCreatedByUserId,
		/// <summary>
		/// Represents the GetByModifiedByUserId method.
		/// </summary>
		GetByModifiedByUserId,
		/// <summary>
		/// Represents the GetByVerificationModifiedByUserId method.
		/// </summary>
		GetByVerificationModifiedByUserId
	}
	
	#endregion QuestionnaireSelectMethod

	#region QuestionnaireFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Questionnaire"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireFilter : SqlFilter<QuestionnaireColumn>
	{
	}
	
	#endregion QuestionnaireFilter

	#region QuestionnaireExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Questionnaire"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireExpressionBuilder : SqlExpressionBuilder<QuestionnaireColumn>
	{
	}
	
	#endregion QuestionnaireExpressionBuilder	

	#region QuestionnaireProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;QuestionnaireChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="Questionnaire"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireProperty : ChildEntityProperty<QuestionnaireChildEntityTypes>
	{
	}
	
	#endregion QuestionnaireProperty
}

