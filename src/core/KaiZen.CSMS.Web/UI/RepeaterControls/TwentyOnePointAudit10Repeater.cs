﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.ComponentModel;
using System.Web.UI.Design.WebControls;
using System.Web.UI.Design;
using System.Web.UI.WebControls;

namespace KaiZen.CSMS.Web.UI
{
    /// <summary>
    /// A designer class for a strongly typed repeater <c>TwentyOnePointAudit10Repeater</c>
    /// </summary>
	public class TwentyOnePointAudit10RepeaterDesigner : System.Web.UI.Design.ControlDesigner
	{
	    /// <summary>
        /// Initializes a new instance of the <see cref="T:TwentyOnePointAudit10RepeaterDesigner"/> class.
        /// </summary>
		public TwentyOnePointAudit10RepeaterDesigner()
		{
		}

        /// <summary>
        /// Initializes the control designer and loads the specified component.
        /// </summary>
        /// <param name="component">The control being designed.</param>
		public override void Initialize(IComponent component)
		{
			if (!(component is TwentyOnePointAudit10Repeater))
			{ 
				throw new ArgumentException("Component is not a TwentyOnePointAudit10Repeater."); 
			} 
			base.Initialize(component); 
			base.SetViewFlags(ViewFlags.TemplateEditing, true); 
		}


		/// <summary>
		/// Generate HTML for the designer
		/// </summary>
		/// <returns>a string of design time HTML</returns>
		public override string GetDesignTimeHtml()
		{

			// Get the instance this designer applies to
			//
			TwentyOnePointAudit10Repeater z = (TwentyOnePointAudit10Repeater)Component;
			z.DataBind();

			return base.GetDesignTimeHtml();

			//return z.RenderAtDesignTime();

			//	ControlCollection c = z.Controls;
			//Totem z = (Totem) Component;
			//Totem z = (Totem) Component;
			//return ("<div style='border: 1px gray dotted; background-color: lightgray'><b>TagStat :</b> zae |  qsdds</div>");

		}
	}

    /// <summary>
    /// A strongly typed repeater control for the <see cref="TwentyOnePointAudit10Repeater"/> Type.
    /// </summary>
	[Designer(typeof(TwentyOnePointAudit10RepeaterDesigner))]
	[ParseChildren(true)]
	[ToolboxData("<{0}:TwentyOnePointAudit10Repeater runat=\"server\"></{0}:TwentyOnePointAudit10Repeater>")]
	public class TwentyOnePointAudit10Repeater : CompositeDataBoundControl, System.Web.UI.INamingContainer
	{
	    /// <summary>
        /// Initializes a new instance of the <see cref="T:TwentyOnePointAudit10Repeater"/> class.
        /// </summary>
		public TwentyOnePointAudit10Repeater()
		{
		}

		/// <summary>
        /// Gets a <see cref="T:System.Web.UI.ControlCollection"></see> object that represents the child controls for a specified server control in the UI hierarchy.
        /// </summary>
        /// <value></value>
        /// <returns>The collection of child controls for the specified server control.</returns>
		public override ControlCollection Controls
		{
			get
			{
				this.EnsureChildControls();
				return base.Controls;
			}
		}

		private ITemplate m_headerTemplate;
		/// <summary>
        /// Gets or sets the header template.
        /// </summary>
        /// <value>The header template.</value>
		[Browsable(false)]
		[TemplateContainer(typeof(TwentyOnePointAudit10Item))]
		[PersistenceMode(PersistenceMode.InnerDefaultProperty)]
		public ITemplate HeaderTemplate
		{
			get { return m_headerTemplate; }
			set { m_headerTemplate = value; }
		}

		private ITemplate m_itemTemplate;
		/// <summary>
        /// Gets or sets the item template.
        /// </summary>
        /// <value>The item template.</value>
		[Browsable(false)]
		[TemplateContainer(typeof(TwentyOnePointAudit10Item))]
		[PersistenceMode(PersistenceMode.InnerDefaultProperty)]
		public ITemplate ItemTemplate
		{
			get { return m_itemTemplate; }
			set { m_itemTemplate = value; }
		}

		private ITemplate m_seperatorTemplate;
        /// <summary>
        /// Gets or sets the Seperator Template
        /// </summary>
        [Browsable(false)]
        [TemplateContainer(typeof(TwentyOnePointAudit10Item))]
        [PersistenceMode(PersistenceMode.InnerDefaultProperty)]
        public ITemplate SeperatorTemplate
        {
            get { return m_seperatorTemplate; }
            set { m_seperatorTemplate = value; }
        }
			
		private ITemplate m_altenateItemTemplate;
        /// <summary>
        /// Gets or sets the alternating item template.
        /// </summary>
        /// <value>The alternating item template.</value>
		[Browsable(false)]
		[TemplateContainer(typeof(TwentyOnePointAudit10Item))]
		[PersistenceMode(PersistenceMode.InnerDefaultProperty)]
		public ITemplate AlternatingItemTemplate
		{
			get { return m_altenateItemTemplate; }
			set { m_altenateItemTemplate = value; }
		}

		private ITemplate m_footerTemplate;
        /// <summary>
        /// Gets or sets the footer template.
        /// </summary>
        /// <value>The footer template.</value>
		[Browsable(false)]
		[TemplateContainer(typeof(TwentyOnePointAudit10Item))]
		[PersistenceMode(PersistenceMode.InnerDefaultProperty)]
		public ITemplate FooterTemplate
		{
			get { return m_footerTemplate; }
			set { m_footerTemplate = value; }
		}

//      /// <summary>
//      /// Called by the ASP.NET page framework to notify server controls that use composition-based implementation to create any child controls they contain in preparation for posting back or rendering.
//      /// </summary>
//		protected override void CreateChildControls()
//      {
//         if (ChildControlsCreated)
//         {
//            return;
//         }

//         Controls.Clear();

//         //Instantiate the Header template (if exists)
//         if (m_headerTemplate != null)
//         {
//            Control headerItem = new Control();
//            m_headerTemplate.InstantiateIn(headerItem);
//            Controls.Add(headerItem);
//         }

//         //Instantiate the Footer template (if exists)
//         if (m_footerTemplate != null)
//         {
//            Control footerItem = new Control();
//            m_footerTemplate.InstantiateIn(footerItem);
//            Controls.Add(footerItem);
//         }
//
//         ChildControlsCreated = true;
//      }
	
		/// <summary>
        /// Overridden and Empty so that span tags are not written
        /// </summary>
        /// <param name="writer"></param>
        public override void RenderBeginTag(HtmlTextWriter writer)
        {
            
        }

        /// <summary>
        /// Overridden and Empty so that span tags are not written
        /// </summary>
        /// <param name="writer"></param>
        public override void RenderEndTag(HtmlTextWriter writer)
        {
                
        }		
		
		/// <summary>
      	/// Called by the ASP.NET page framework to notify server controls that use composition-based implementation to create any child controls they contain in preparation for posting back or rendering.
      	/// </summary>
		protected override int CreateChildControls(System.Collections.IEnumerable dataSource, bool dataBinding)
      	{
         int pos = 0;

         if (dataBinding)
         {
            //Instantiate the Header template (if exists)
            if (m_headerTemplate != null)
            {
                Control headerItem = new Control();
                m_headerTemplate.InstantiateIn(headerItem);
                Controls.Add(headerItem);
            }
			if (dataSource != null)
			{
				foreach (object o in dataSource)
				{
						KaiZen.CSMS.Entities.TwentyOnePointAudit10 entity = o as KaiZen.CSMS.Entities.TwentyOnePointAudit10;
						TwentyOnePointAudit10Item container = new TwentyOnePointAudit10Item(entity);
	
						if (m_itemTemplate != null && (pos % 2) == 0)
						{
							m_itemTemplate.InstantiateIn(container);
							
							if (m_seperatorTemplate != null)
							{
								m_seperatorTemplate.InstantiateIn(container);
							}
						}
						else
						{
							if (m_altenateItemTemplate != null)
							{
								m_altenateItemTemplate.InstantiateIn(container);
								
								if (m_seperatorTemplate != null)
								{
									m_seperatorTemplate.InstantiateIn(container);
								}
								
							}
							else if (m_itemTemplate != null)
							{
								m_itemTemplate.InstantiateIn(container);
								
								if (m_seperatorTemplate != null)
								{
									m_seperatorTemplate.InstantiateIn(container);
								}
							}
							else
							{
								// no template !!!
							}
						}
						Controls.Add(container);
						
						container.DataBind();
						
						pos++;
				}
			}
            //Instantiate the Footer template (if exists)
            if (m_footerTemplate != null)
            {
                Control footerItem = new Control();
                m_footerTemplate.InstantiateIn(footerItem);
                Controls.Add(footerItem);
            }

		}
			
			return pos;
		}

        /// <summary>
        /// Raises the <see cref="E:System.Web.UI.Control.PreRender"></see> event.
        /// </summary>
        /// <param name="e">An <see cref="T:System.EventArgs"></see> object that contains the event data.</param>
		protected override void OnPreRender(EventArgs e)
		{
			base.DataBind();
		}

		#region Design time
        /// <summary>
        /// Renders at design time.
        /// </summary>
        /// <returns>a  string of the Designed HTML</returns>
		internal string RenderAtDesignTime()
		{			
			return "Designer currently not implemented"; 
		}

		#endregion
	}

    /// <summary>
    /// A wrapper type for the entity
    /// </summary>
	[System.ComponentModel.ToolboxItem(false)]
	public class TwentyOnePointAudit10Item : System.Web.UI.Control, System.Web.UI.INamingContainer
	{
		private KaiZen.CSMS.Entities.TwentyOnePointAudit10 _entity;

        /// <summary>
        /// Initializes a new instance of the <see cref="T:TwentyOnePointAudit10Item"/> class.
        /// </summary>
		public TwentyOnePointAudit10Item()
			: base()
		{ }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:TwentyOnePointAudit10Item"/> class.
        /// </summary>
		public TwentyOnePointAudit10Item(KaiZen.CSMS.Entities.TwentyOnePointAudit10 entity)
			: base()
		{
			_entity = entity;
		}
		
        /// <summary>
        /// Gets the Id
        /// </summary>
        /// <value>The Id.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32 Id
		{
			get { return _entity.Id; }
		}
        /// <summary>
        /// Gets the Achieved10a
        /// </summary>
        /// <value>The Achieved10a.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved10a
		{
			get { return _entity.Achieved10a; }
		}
        /// <summary>
        /// Gets the Achieved10b
        /// </summary>
        /// <value>The Achieved10b.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved10b
		{
			get { return _entity.Achieved10b; }
		}
        /// <summary>
        /// Gets the Achieved10c
        /// </summary>
        /// <value>The Achieved10c.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved10c
		{
			get { return _entity.Achieved10c; }
		}
        /// <summary>
        /// Gets the Achieved10d
        /// </summary>
        /// <value>The Achieved10d.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved10d
		{
			get { return _entity.Achieved10d; }
		}
        /// <summary>
        /// Gets the Achieved10e
        /// </summary>
        /// <value>The Achieved10e.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved10e
		{
			get { return _entity.Achieved10e; }
		}
        /// <summary>
        /// Gets the Achieved10f
        /// </summary>
        /// <value>The Achieved10f.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved10f
		{
			get { return _entity.Achieved10f; }
		}
        /// <summary>
        /// Gets the Achieved10g
        /// </summary>
        /// <value>The Achieved10g.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved10g
		{
			get { return _entity.Achieved10g; }
		}
        /// <summary>
        /// Gets the Achieved10h
        /// </summary>
        /// <value>The Achieved10h.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved10h
		{
			get { return _entity.Achieved10h; }
		}
        /// <summary>
        /// Gets the Observation10a
        /// </summary>
        /// <value>The Observation10a.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation10a
		{
			get { return _entity.Observation10a; }
		}
        /// <summary>
        /// Gets the Observation10b
        /// </summary>
        /// <value>The Observation10b.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation10b
		{
			get { return _entity.Observation10b; }
		}
        /// <summary>
        /// Gets the Observation10c
        /// </summary>
        /// <value>The Observation10c.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation10c
		{
			get { return _entity.Observation10c; }
		}
        /// <summary>
        /// Gets the Observation10d
        /// </summary>
        /// <value>The Observation10d.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation10d
		{
			get { return _entity.Observation10d; }
		}
        /// <summary>
        /// Gets the Observation10e
        /// </summary>
        /// <value>The Observation10e.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation10e
		{
			get { return _entity.Observation10e; }
		}
        /// <summary>
        /// Gets the Observation10f
        /// </summary>
        /// <value>The Observation10f.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation10f
		{
			get { return _entity.Observation10f; }
		}
        /// <summary>
        /// Gets the Observation10g
        /// </summary>
        /// <value>The Observation10g.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation10g
		{
			get { return _entity.Observation10g; }
		}
        /// <summary>
        /// Gets the Observation10h
        /// </summary>
        /// <value>The Observation10h.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation10h
		{
			get { return _entity.Observation10h; }
		}
        /// <summary>
        /// Gets the TotalScore
        /// </summary>
        /// <value>The TotalScore.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? TotalScore
		{
			get { return _entity.TotalScore; }
		}

        /// <summary>
        /// Gets a <see cref="T:KaiZen.CSMS.Entities.TwentyOnePointAudit10"></see> object
        /// </summary>
        /// <value></value>
        [System.ComponentModel.Bindable(true)]
        public KaiZen.CSMS.Entities.TwentyOnePointAudit10 Entity
        {
            get { return _entity; }
        }
	}
}
