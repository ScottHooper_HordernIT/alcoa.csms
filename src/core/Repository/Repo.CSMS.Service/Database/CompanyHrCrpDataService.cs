﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface ICompanyHrCrpDataService
    {
        CompaniesHrCrpData Get(Expression<Func<CompaniesHrCrpData, bool>> where, List<Expression<Func<CompaniesHrCrpData, object>>> includes);
        List<CompaniesHrCrpData> GetMany(int? top, Expression<Func<CompaniesHrCrpData, bool>> where, Func<IQueryable<CompaniesHrCrpData>, IOrderedQueryable<CompaniesHrCrpData>> orderBy, List<Expression<Func<CompaniesHrCrpData, object>>> includes);
        List<CompaniesHrCrpData> GetManyPaged(int pageNumber, int pageSize, Expression<Func<CompaniesHrCrpData, bool>> where, Func<IQueryable<CompaniesHrCrpData>, IOrderedQueryable<CompaniesHrCrpData>> orderBy, List<Expression<Func<CompaniesHrCrpData, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<CompaniesHrCrpData, bool>> where, List<Expression<Func<CompaniesHrCrpData, object>>> includes);
        void Insert(CompaniesHrCrpData item, bool saveChanges = true);
        void Update(CompaniesHrCrpData item, bool saveChanges = true);
        void Delete(CompaniesHrCrpData item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class CompanyHrCrpDataService : ICompanyHrCrpDataService
    {
        private readonly IRepository<CompaniesHrCrpData> _repository;

        public CompanyHrCrpDataService(IRepository<CompaniesHrCrpData> repository)
        {
            this._repository = repository;
        }

        public CompaniesHrCrpData Get(Expression<Func<CompaniesHrCrpData, bool>> where, List<Expression<Func<CompaniesHrCrpData, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<CompaniesHrCrpData> GetMany(int? top, Expression<Func<CompaniesHrCrpData, bool>> where, Func<IQueryable<CompaniesHrCrpData>, IOrderedQueryable<CompaniesHrCrpData>> orderBy, List<Expression<Func<CompaniesHrCrpData, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<CompaniesHrCrpData> GetManyPaged(int pageNumber, int pageSize, Expression<Func<CompaniesHrCrpData, bool>> where, Func<IQueryable<CompaniesHrCrpData>, IOrderedQueryable<CompaniesHrCrpData>> orderBy, List<Expression<Func<CompaniesHrCrpData, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<CompaniesHrCrpData, bool>> where, List<Expression<Func<CompaniesHrCrpData, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(CompaniesHrCrpData item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("CompaniesHrCrpData");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(CompaniesHrCrpData item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("CompaniesHrCrpData");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(CompaniesHrCrpData item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("CompaniesHrCrpData");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
