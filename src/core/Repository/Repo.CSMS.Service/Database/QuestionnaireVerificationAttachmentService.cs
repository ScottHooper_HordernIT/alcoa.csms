﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface IQuestionnaireVerificationAttachmentService
    {
        QuestionnaireVerificationAttachment Get(Expression<Func<QuestionnaireVerificationAttachment, bool>> where, List<Expression<Func<QuestionnaireVerificationAttachment, object>>> includes);
        List<QuestionnaireVerificationAttachment> GetMany(int? top, Expression<Func<QuestionnaireVerificationAttachment, bool>> where, Func<IQueryable<QuestionnaireVerificationAttachment>, IOrderedQueryable<QuestionnaireVerificationAttachment>> orderBy, List<Expression<Func<QuestionnaireVerificationAttachment, object>>> includes);
        List<QuestionnaireVerificationAttachment> GetManyPaged(int pageNumber, int pageSize, Expression<Func<QuestionnaireVerificationAttachment, bool>> where, Func<IQueryable<QuestionnaireVerificationAttachment>, IOrderedQueryable<QuestionnaireVerificationAttachment>> orderBy, List<Expression<Func<QuestionnaireVerificationAttachment, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<QuestionnaireVerificationAttachment, bool>> where, List<Expression<Func<QuestionnaireVerificationAttachment, object>>> includes);
        void Insert(QuestionnaireVerificationAttachment item, bool saveChanges = true);
        void Update(QuestionnaireVerificationAttachment item, bool saveChanges = true);
        void Delete(QuestionnaireVerificationAttachment item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class QuestionnaireVerificationAttachmentService : IQuestionnaireVerificationAttachmentService
    {
        private readonly IRepository<QuestionnaireVerificationAttachment> _repository;

        public QuestionnaireVerificationAttachmentService(IRepository<QuestionnaireVerificationAttachment> repository)
        {
            this._repository = repository;
        }

        public QuestionnaireVerificationAttachment Get(Expression<Func<QuestionnaireVerificationAttachment, bool>> where, List<Expression<Func<QuestionnaireVerificationAttachment, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<QuestionnaireVerificationAttachment> GetMany(int? top, Expression<Func<QuestionnaireVerificationAttachment, bool>> where, Func<IQueryable<QuestionnaireVerificationAttachment>, IOrderedQueryable<QuestionnaireVerificationAttachment>> orderBy, List<Expression<Func<QuestionnaireVerificationAttachment, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<QuestionnaireVerificationAttachment> GetManyPaged(int pageNumber, int pageSize, Expression<Func<QuestionnaireVerificationAttachment, bool>> where, Func<IQueryable<QuestionnaireVerificationAttachment>, IOrderedQueryable<QuestionnaireVerificationAttachment>> orderBy, List<Expression<Func<QuestionnaireVerificationAttachment, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<QuestionnaireVerificationAttachment, bool>> where, List<Expression<Func<QuestionnaireVerificationAttachment, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(QuestionnaireVerificationAttachment item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("QuestionnaireVerificationAttachment");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(QuestionnaireVerificationAttachment item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("QuestionnaireVerificationAttachment");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(QuestionnaireVerificationAttachment item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("QuestionnaireVerificationAttachment");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
