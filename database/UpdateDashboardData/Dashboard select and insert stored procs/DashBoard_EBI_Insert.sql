/****** Object:  StoredProcedure [dbo].[DashBoard_EBI_Insert]    Script Date: 3/08/2015 1:24:02 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[DashBoard_EBI_Insert]        
(        
 @Site int        
 ,@Month int        
 ,@Year int        
         
 ,@YesSQCurrent decimal(18,2)        
 ,@NoCurrentSQnotfound decimal(18,2)        
 ,@NoSQExpired decimal(18,2)        
 ,@NoAccesstoSiteNotGranted decimal(18,2)        
 ,@NoSafetyQualificationnotfound decimal(18,2)        
 ,@NoInformationcouldnotbefound decimal(18,2)        
 ,@CompanyhasavalidSQExemption decimal(18,2)        
        
)        
AS        
      if exists(Select * from  DashBoard_EBI where SiteId=@Site and [Month]=@Month  
 and [Year]=@Year)  
 begin   
 Update DashBoard_EBI Set  
   [Yes - SQ Current]  = @YesSQCurrent      
     ,[No - Current SQ not found] = @NoCurrentSQnotfound       
     ,[No - SQ Expired] = @NoSQExpired       
     ,[No - Access to Site Not Granted]  = @NoAccesstoSiteNotGranted      
     ,[No - Safety Qualification not found] = @NoSafetyQualificationnotfound       
     ,[No - Information could not be found] = @NoInformationcouldnotbefound        
     ,[Company has a valid SQ Exemption]= @CompanyhasavalidSQExemption
     ,[LastModifiedDate] = GETDATE()
    where SiteId=@Site AND [Month]=@Month AND [Year]=@Year  
   end    
      else  
      begin      
    INSERT INTO [dbo].[DashBoard_EBI]        
     (        
     [SiteId]        
     ,[Month]        
     ,[Year]             
     ,[Yes - SQ Current]        
     ,[No - Current SQ not found]        
     ,[No - SQ Expired]        
     ,[No - Access to Site Not Granted]        
     ,[No - Safety Qualification not found]        
     ,[No - Information could not be found]        
     ,[Company has a valid SQ Exemption]        
     ,[LastModifiedDate] 
   )        
    VALUES        
     (        
     @Site        
     ,@Month        
     ,@Year        
     ,@YesSQCurrent        
     ,@NoCurrentSQnotfound        
     ,@NoSQExpired        
     ,@NoAccesstoSiteNotGranted        
     ,@NoSafetyQualificationnotfound        
     ,@NoInformationcouldnotbefound        
     ,@CompanyhasavalidSQExemption   
     ,GETDATE()     
     )     
end     
---------------------------------------

GO


