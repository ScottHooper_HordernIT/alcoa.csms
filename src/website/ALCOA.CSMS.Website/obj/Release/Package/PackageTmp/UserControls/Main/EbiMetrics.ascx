﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EbiMetrics.ascx.cs" Inherits="ALCOA.CSMS.Website.UserControls.Main.EbiMetrics" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register Src="../Other/ExportButtons.ascx" TagName="ExportButtons" TagPrefix="uc1" %>

<table border="0" cellpadding="2" cellspacing="0" width="900">
    <tr>
        <td class="pageName">
            <span class="title">Performance Management > Tools</span><br />
            <span class="date">EBI Metrics</span><br />
            <img height="1" src="images/grfc_dottedline.gif" width="24" /><br /></td>
    </tr>
</table>

<p>
    <table cellpadding="0" cellspacing="0">
        <tr>
            <td>
                Select Start Date:
            </td>
            <td style="padding-left: 2px">
    <dx:ASPxDateEdit ID="deFrom" runat="server" 
        CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
        CssPostfix="Office2003Blue"
        SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" 
                    Width="100px">
        <ButtonStyle Width="13px">
        </ButtonStyle>
    </dx:ASPxDateEdit>
            </td>
            
            <td style="padding-left: 2px">
                <dx:ASPxButton ID="btnFilter" runat="server" Text="Filter" 
                    CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
                    CssPostfix="Office2003Blue" onclick="btnFilter_Click" 
                    SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
            </dx:ASPxButton>
            </td>
        </tr>
    </table>
</p>
<dx:ASPxGridView ID="grid" runat="server" 
    CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
    CssPostfix="Office2003Blue" AutoGenerateColumns="False" Width="900px">
    <Columns>
        <dx:GridViewDataComboBoxColumn Caption="Site" FieldName="SiteId" GroupIndex="0" 
            SortIndex="0" SortOrder="Ascending" VisibleIndex="0" FixedStyle="Left">
            <PropertiesComboBox DataSourceID="SitesDataSource1" TextField="SiteName" 
                ValueField="SiteId" ValueType="System.Int32">
            </PropertiesComboBox>
        </dx:GridViewDataComboBoxColumn>
        <dx:GridViewDataComboBoxColumn Caption="Metric" FieldName="MetricId" 
            SortIndex="1" SortOrder="Ascending" VisibleIndex="1" FixedStyle="Left" 
            Width="400px">
            <PropertiesComboBox DataSourceID="EbiIssueDataSource1" TextField="IssueDesc" 
                ValueField="EbiIssueId" ValueType="System.Int32">
            </PropertiesComboBox>
        </dx:GridViewDataComboBoxColumn>
        <dx:GridViewDataTextColumn FieldName="Day1" VisibleIndex="2" Width="45px">
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn FieldName="Day2" VisibleIndex="3" Width="45px">
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn FieldName="Day3" VisibleIndex="4" Width="45px">
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn FieldName="Day4" VisibleIndex="5" Width="45px">
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn FieldName="Day5" VisibleIndex="6" Width="45px">
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn FieldName="Day6" VisibleIndex="7" Width="45px">
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn FieldName="Day7" VisibleIndex="8" Width="45px">
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn FieldName="Day8" VisibleIndex="9" Width="45px">
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn FieldName="Day9" VisibleIndex="10" Width="45px">
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn FieldName="Day10" VisibleIndex="11" Width="45px">
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn FieldName="Day11" VisibleIndex="12" Width="45px">
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn FieldName="Day12" VisibleIndex="13" Width="45px">
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn FieldName="Day13" VisibleIndex="14" Width="45px">
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn FieldName="Day14" VisibleIndex="15" Width="45px">
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn FieldName="Day15" VisibleIndex="16" Width="45px">
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn FieldName="Day16" VisibleIndex="17" Width="45px">
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn FieldName="Day17" VisibleIndex="18" Width="45px">
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn FieldName="Day18" VisibleIndex="19" Width="45px">
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn FieldName="Day19" VisibleIndex="20" Width="45px">
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn FieldName="Day20" VisibleIndex="21" Width="45px">
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn FieldName="Day21" VisibleIndex="22" Width="45px">
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn FieldName="Day22" VisibleIndex="23" Width="45px">
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn FieldName="Day23" VisibleIndex="24" Width="45px">
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn FieldName="Day24" VisibleIndex="25" Width="45px">
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn FieldName="Day25" VisibleIndex="26" Width="45px">
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn FieldName="Day26" VisibleIndex="27" Width="45px">
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn FieldName="Day27" VisibleIndex="28" Width="45px">
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn FieldName="Day28" VisibleIndex="29" Width="45px">
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn FieldName="Day29" VisibleIndex="30" Width="45px">
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn FieldName="Day30" VisibleIndex="31" Width="45px">
        </dx:GridViewDataTextColumn>
    </Columns>
    <SettingsPager Mode="ShowAllRecords" PageSize="200">
    </SettingsPager>
    <Settings ShowGroupPanel="True" ShowHorizontalScrollBar="True" />
    <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
        <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
        </LoadingPanelOnStatusBar>
        <LoadingPanel Url="~/App_Themes/Office2003BlueOld/GridView/Loading.gif"><%--Enhancement_023:change by debashis for testing platform upgradation--%>
        </LoadingPanel>
    </Images>
    <ImagesFilterControl>
        <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
        </LoadingPanel>
    </ImagesFilterControl>
    <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
        CssPostfix="Office2003Blue">
        <Header ImageSpacing="5px" SortingImageSpacing="5px">
        </Header>
        <LoadingPanel ImageSpacing="10px">
        </LoadingPanel>
    </Styles>
    <StylesEditors>
        <ProgressBar Height="25px">
        </ProgressBar>
    </StylesEditors>
</dx:ASPxGridView>
<table width="900px">
    <tr align="right">
        <td colspan="3" style="padding-top:6px; text-align: right; text-align: -moz-right; width: 100%"
            align="right">
            <div align="right">
                <uc1:ExportButtons ID="ucExportButtons" runat="server" />
            </div>
        </td>
    </tr>
</table>
    <br />


<data:EbiIssueDataSource ID="EbiIssueDataSource1" runat="server">
</data:EbiIssueDataSource>
<data:SitesDataSource ID="SitesDataSource1" runat="server">
</data:SitesDataSource>


