﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface IQuestionnaireMainAssessmentAuditService
    {
        QuestionnaireMainAssessmentAudit Get(Expression<Func<QuestionnaireMainAssessmentAudit, bool>> where, List<Expression<Func<QuestionnaireMainAssessmentAudit, object>>> includes);
        List<QuestionnaireMainAssessmentAudit> GetMany(int? top, Expression<Func<QuestionnaireMainAssessmentAudit, bool>> where, Func<IQueryable<QuestionnaireMainAssessmentAudit>, IOrderedQueryable<QuestionnaireMainAssessmentAudit>> orderBy, List<Expression<Func<QuestionnaireMainAssessmentAudit, object>>> includes);
        List<QuestionnaireMainAssessmentAudit> GetManyPaged(int pageNumber, int pageSize, Expression<Func<QuestionnaireMainAssessmentAudit, bool>> where, Func<IQueryable<QuestionnaireMainAssessmentAudit>, IOrderedQueryable<QuestionnaireMainAssessmentAudit>> orderBy, List<Expression<Func<QuestionnaireMainAssessmentAudit, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<QuestionnaireMainAssessmentAudit, bool>> where, List<Expression<Func<QuestionnaireMainAssessmentAudit, object>>> includes);
        void Insert(QuestionnaireMainAssessmentAudit item, bool saveChanges = true);
        void Update(QuestionnaireMainAssessmentAudit item, bool saveChanges = true);
        void Delete(QuestionnaireMainAssessmentAudit item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class QuestionnaireMainAssessmentAuditService : IQuestionnaireMainAssessmentAuditService
    {
        private readonly IRepository<QuestionnaireMainAssessmentAudit> _repository;

        public QuestionnaireMainAssessmentAuditService(IRepository<QuestionnaireMainAssessmentAudit> repository)
        {
            this._repository = repository;
        }

        public QuestionnaireMainAssessmentAudit Get(Expression<Func<QuestionnaireMainAssessmentAudit, bool>> where, List<Expression<Func<QuestionnaireMainAssessmentAudit, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<QuestionnaireMainAssessmentAudit> GetMany(int? top, Expression<Func<QuestionnaireMainAssessmentAudit, bool>> where, Func<IQueryable<QuestionnaireMainAssessmentAudit>, IOrderedQueryable<QuestionnaireMainAssessmentAudit>> orderBy, List<Expression<Func<QuestionnaireMainAssessmentAudit, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<QuestionnaireMainAssessmentAudit> GetManyPaged(int pageNumber, int pageSize, Expression<Func<QuestionnaireMainAssessmentAudit, bool>> where, Func<IQueryable<QuestionnaireMainAssessmentAudit>, IOrderedQueryable<QuestionnaireMainAssessmentAudit>> orderBy, List<Expression<Func<QuestionnaireMainAssessmentAudit, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<QuestionnaireMainAssessmentAudit, bool>> where, List<Expression<Func<QuestionnaireMainAssessmentAudit, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(QuestionnaireMainAssessmentAudit item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("QuestionnaireMainAssessmentAudit");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(QuestionnaireMainAssessmentAudit item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("QuestionnaireMainAssessmentAudit");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(QuestionnaireMainAssessmentAudit item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("QuestionnaireMainAssessmentAudit");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
