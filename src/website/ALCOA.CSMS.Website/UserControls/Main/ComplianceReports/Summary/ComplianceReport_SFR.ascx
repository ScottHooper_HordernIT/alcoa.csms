<%@ Control Language="C#" AutoEventWireup="true" Inherits="UserControls_Tiny_ComplianceReports_ComplianceReport_SFR" Codebehind="ComplianceReport_SFR.ascx.cs" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<dxwgv:ASPxGridView ID="grid"
                ClientInstanceName="grid"
                runat="server"
                AutoGenerateColumns="False"
                CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                CssPostfix="Office2003Blue"
                Width="900px"
                KeyFieldName="SiteName"
             >
    <Columns>
        <dxwgv:GridViewDataTextColumn Caption="Site Name" SortIndex="1" SortOrder="Ascending"
                                                    FieldName="SiteName" VisibleIndex="0">
</dxwgv:GridViewDataTextColumn>
        <dxwgv:GridViewDataComboBoxColumn Name="gcbResidentialCategory" Caption="Residential Category"
            FieldName="CompanySiteCategoryId" SortIndex="0" SortOrder="Ascending" UnboundType="String"
            VisibleIndex="1" FixedStyle="Left" Width="145px">
            <PropertiesComboBox DataSourceID="CompanySiteCategoryDataSource" DropDownHeight="150px"
                TextField="CategoryDesc" ValueField="CompanySiteCategoryId" ValueType="System.Int32"
                IncrementalFilteringMode="StartsWith">
            </PropertiesComboBox>
            <Settings SortMode="DisplayText" />
        </dxwgv:GridViewDataComboBoxColumn>
        <dxwgv:GridViewDataTextColumn Caption="LWDFR" FieldName="LWDFR" VisibleIndex="2">
        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<CellStyle HorizontalAlign="Center"></CellStyle>
</dxwgv:GridViewDataTextColumn>
        <dxwgv:GridViewDataTextColumn Caption="TRIFR" FieldName="TRIFR" VisibleIndex="3">
        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<CellStyle HorizontalAlign="Center"></CellStyle>
</dxwgv:GridViewDataTextColumn>
        <dxwgv:GridViewDataTextColumn Caption="AIFR" FieldName="AIFR" VisibleIndex="4">
        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<CellStyle HorizontalAlign="Center"></CellStyle>
</dxwgv:GridViewDataTextColumn>
        <dxwgv:GridViewDataTextColumn Caption="IFE: Injury Ratio" FieldName="IFEIR" VisibleIndex="5">
        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<CellStyle HorizontalAlign="Center"></CellStyle>
</dxwgv:GridViewDataTextColumn>
    </Columns>
    <ImagesFilterControl>
        <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
        </LoadingPanel>
    </ImagesFilterControl>
<Styles CssPostfix="Office2003Blue" 
        CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css">
<Header SortingImageSpacing="5px" ImageSpacing="5px"></Header>
<AlternatingRow Enabled="True"></AlternatingRow>
<LoadingPanel ImageSpacing="10px"></LoadingPanel>
</Styles>


<Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
    <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
    </LoadingPanelOnStatusBar>
    <LoadingPanel Url="~/App_Themes/Office2003Blue/GridView/Loading.gif">
    </LoadingPanel>
</Images>

<StylesEditors>
<ProgressBar Height="25px"></ProgressBar>
</StylesEditors>
</dxwgv:aspxgridview>


<data:CompanySiteCategoryDataSource ID="CompanySiteCategoryDataSource" runat="server" SelectMethod="GetPaged"
            EnablePaging="True" EnableSorting="True">
        </data:CompanySiteCategoryDataSource>