using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using KaiZen.CSMS.Services;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxTabControl;
using DevExpress.Web.Data;
using DevExpress.Web.ASPxGridView;  

public partial class Adminv2_UserControls_Procurement : System.Web.UI.UserControl
{
    Auth auth = new Auth();

    protected void Page_Load(object sender, EventArgs e) {  Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); }

    private void moduleInit(bool postBack)
    {
        if (!postBack)
        { //first time load
            if (auth.RoleId != (int)RoleList.Administrator)
            {
                Response.Redirect("default.aspx");
            }
        }
    }

    protected void grid_InitNewRow(object sender, ASPxDataInitNewRowEventArgs e) //Default Values 
    {
        bool active = true;
        if (e.NewValues["Enabled"] == null) { e.NewValues["Enabled"] = active; }
        if (e.NewValues["ModifiedByUserId"] == null) { e.NewValues["ModifiedByUserId"] = auth.UserId; }
    }
    protected void grid_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        e.NewValues["ModifiedByUserId"] = auth.UserId;
        e.NewValues["ModifiedDate"] = DateTime.Now;
    }
    protected void grid_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        e.NewValues["ModifiedByUserId"] = auth.UserId;
        e.NewValues["ModifiedDate"] = DateTime.Now;
    }

    protected void grid_RowCreated(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewTableRowEventArgs e)
    {
        if (e.RowType != DevExpress.Web.ASPxGridView.GridViewRowType.Data) return;
        Label lbl1 = ASPxGridView1.FindRowCellTemplateControl(e.VisibleIndex, null, "noSQProcContact") as Label;
        Label lbl2 = ASPxGridView1.FindRowCellTemplateControl(e.VisibleIndex, null, "noSQFuncProcMgr") as Label;
        int UserId = (int)ASPxGridView1.GetRowValues(e.VisibleIndex, "UserId");

        //noSQFuncProcMgr
        string c1 = "-";
        string c2 = "-";

        QuestionnaireInitialResponseService qirService = new QuestionnaireInitialResponseService();
        DataSet ds1 = qirService.GetCountByQuestionIdUserId("2", UserId.ToString());
        DataSet ds2 = qirService.GetCountByQuestionIdUserId("2_1", UserId.ToString());

        if (ds1.Tables[0] != null)
        {
            foreach (DataRow dr in ds1.Tables[0].Rows)
            {
                c1 = dr[0].ToString();
            }
        }

        if (ds2.Tables[0] != null)
        {
            foreach (DataRow dr in ds2.Tables[0].Rows)
            {
                c2 = dr[0].ToString();
            }
        }

        if (c1 == "0") c1 = "-";
        if (c2 == "0") c2 = "-";

        lbl1.Text = c1;
        lbl2.Text = c2;

    }
}
