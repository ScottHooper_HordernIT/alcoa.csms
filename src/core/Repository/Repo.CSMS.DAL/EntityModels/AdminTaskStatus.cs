//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Repo.CSMS.DAL.EntityModels
{
    using System;
    using System.Collections.Generic;
    
    public partial class AdminTaskStatus
    {
        public AdminTaskStatus()
        {
            this.AdminTasks = new HashSet<AdminTask>();
        }
    
        public int AdminTaskStatusId { get; set; }
        public string StatusName { get; set; }
        public string StatusDesc { get; set; }
    
        public virtual ICollection<AdminTask> AdminTasks { get; set; }
    }
}
