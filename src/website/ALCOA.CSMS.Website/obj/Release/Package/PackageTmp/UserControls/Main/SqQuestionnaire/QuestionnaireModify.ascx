<%@ Control Language="C#" AutoEventWireup="true" Inherits="UserControls_SqQuestionnaire_QuestionnaireModify"
    CodeBehind="QuestionnaireModify.ascx.cs" %>
<%@ Register Src="../../Other/ExportButtons.ascx" TagName="ExportButtons" TagPrefix="uc1" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxRoundPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallbackPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx1" %>
<%@ Register Assembly="DevExpress.Web.v14.1" Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1" Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1" Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1" Namespace="DevExpress.Web.ASPxRoundPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1" Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>

<%--//Change by debashis--for popup width issue--%>
<style type="text/css">
    .wrap { white-space:normal; text-align:left }
    .hypercolor
    {
        color:#0d45b7;
    }
    .auto-style2 {
        height: 90px;
        width: 75px;
    }
</style>
<%--//debashis--%>
<asp:UpdatePanel ID="UpdatePanel1" UpdateMode="Always" runat="server" RenderMode="Inline"
    Visible="True">
    <ContentTemplate>
<table border="0" cellpadding="2" cellspacing="0" width="890">
    <tr>
        <td class="pageName" colspan="4" style="height: 17px">
            <span class="bodycopy"><span class="title">Safety Qualification</span><br />
                <span class="date">
                    <dx:ASPxHyperLink ID="hlQuestionnaire" runat="server" Text="Questionnaire" ToolTip="Go back to Safety Qualification Page"
                        NavigateUrl="~/SafetyPQ_Questionnaire.aspx">
                    </dx:ASPxHyperLink>
                    <asp:Label ID="lblMiddle" runat="server" Text="&gt;"></asp:Label>
                    <asp:Label ID="lblMode" runat="server" Text="[Unspecified]"></asp:Label></span><br />
                <img height="1" src="images/grfc_dottedline.gif" width="24" />&nbsp;</span>
        </td>
    </tr>
    <tr>
        <td colspan="1" style="width: 40px; height: 30px; text-align: left">&nbsp;
        <dx:ASPxHyperLink ID="hlAdmin" runat="server"  CssClass="hypercolor" ClientInstanceName="hlAdmin" Text="Admin" Visible="False"
            NavigateUrl="javascript:void(0);" >
        </dx:ASPxHyperLink>
        
        </td>
        <td colspan="2" style="height: 30px; text-align: right">
            <dx:ASPxHyperLink ID="ASPxHyperLink1" runat="server" Height="22px" NavigateUrl="javascript:popUp('PopUps/SafetyPQ_Definitions.aspx');"
                Text="Definitions and Instructions">
            </dx:ASPxHyperLink>
            <img src="Images/help.png" />
        </td>
    </tr>
    <tr>
        <td class="pageName" colspan="4" style="height: 17px; text-align: center;" align="center">
            <asp:Panel runat="server" ID="panelBoxSqExemptionFound" Visible="false" Width="100%" HorizontalAlign="center">
                <div style="border-right: black 1px solid; padding-right: 10px; border-top: black 1px solid; padding-left: 10px; padding-bottom: 10px; border-left: black 1px solid; padding-top: 10px; border-bottom: black 1px solid; background-color: #ffffcc; text-align: center"
                    align="center">
                    <asp:Panel runat="server" ID="panelBoxSqExemptionFoundNotice" Visible="false">
                        <table>
                            <tr>
                                <td style="padding-right: 20px; width: 100px;">
                                    <img alt="" src="Images/exclamationmark.png" height="83" width="100" />
                                </td>
                                <td>
                                    <span style="font-size: 13pt; color: maroon"><strong><span style="font-family: Verdana">Valid Safety Qualification Exemption Found!</span><br />
                                    </strong></span>
                                    <br />
                                    <div align="left">
                                        <span style="color: red">A valid Safety Qualification Exemption has been found and as such the following company is Safety Qualification Exempt on approved listed sites (scroll to bottom of page).
                <br />
                                        </span>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                    <asp:Panel runat="server" ID="panelBoxSqExemptionFoundProcurement" Visible="false">
                        <table>
                            <tr>
                                <td style="padding-right: 20px; width: 100px;">
                                    <br />
                                    <img alt="" src="Images/exclamationmark.png" height="83" width="100" />
                                    <br />
                                    <br />
                                </td>
                                <td>
                                    <span style="font-size: 13pt; color: maroon"><strong><span style="font-family: Verdana">Valid Safety Qualification Exemption Found!</span><br />
                                    </strong></span>
                                    <br />
                                    <div align="left">
                                        <span style="color: red">A valid Safety Qualification Exemption has been found and as such the following company is Safety Qualification Exempt on approved listed sites (scroll to bottom of page).
                <br />
                                            <br />
                                        </span><span style="color: maroon">Note: Procurement still need to either:<br />
                                            1) Continue the Safety Qualification process (so that they may become Safety Qualified
                    even after the exemption expires); or<br />
                                            2) Change Company Status to 'InActive - No Re-Qualification Required (in Status
                    Report, No Access to Site)'.</span>
                                        <br />
                                        <br />
                                        <span style="color: red">Inaction of the two above options will result in the company staying on
                    the procurement worklist (work required to be completed). </span>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </div>
            </asp:Panel>
            <asp:Panel runat="server" ID="panelBox2" Visible="false" Width="100%" HorizontalAlign="center">
                <div style="border-right: black 1px solid; padding-right: 10px; border-top: black 1px solid; padding-left: 10px; padding-bottom: 10px; border-left: black 1px solid; padding-top: 10px; border-bottom: black 1px solid; background-color: #ffffcc; text-align: center"
                    align="center">
                    <span style="font-size: 13pt; color: maroon"><strong><span style="font-family: Verdana">
                        <asp:Label ID="lblpanelBox1" runat="server" Text="Company Status Change Request pending approval"></asp:Label></span><br />
                    </strong></span>
                    <div align="center">
                        <span style="color: red">A Request to Change Company Status is pending approval by the
                            Manager - Procurement Operations (or CSMS Administrator).<br />
                            Until this request has been approved (or not approved), no further company status
                            changes may occur.</span>
                    </div>
                    <br />
                    <table width="690px">
                        <tr>
                            <td class="style1" style="text-align: right;">
                                <b>Current Company Status: </b>
                            </td>
                            <td style="text-align: left">
                                <dx:ASPxLabel ID="lblCscpCompanyStatusCurrent_Top" runat="server" Text="(Not Assigned)">
                                </dx:ASPxLabel>
                            </td>
                        </tr>
                        <tr>
                            <td class="style2" style="text-align: right;">New Company Status:
                            </td>
                            <td style="text-align: left">
                                <dx:ASPxLabel ID="lblCscpCompanyStatusNew_Top" runat="server" Text="(Not Assigned)">
                                </dx:ASPxLabel>
                            </td>
                        </tr>
                        <tr>
                            <td class="style2" style="text-align: right;">Comments:
                            </td>
                            <td style="text-align: left">
                                <dx:ASPxLabel ID="lblCscpComments_Top" runat="server" Text="(Not Assigned)">
                                </dx:ASPxLabel>
                            </td>
                        </tr>
                        <tr>
                            <td class="style2" style="text-align: right;">Requested By:
                            </td>
                            <td style="text-align: left">
                                <dx:ASPxLabel ID="lblCscpRequestedBy_Top" runat="server" Text="(Not Assigned)">
                                </dx:ASPxLabel>
                            </td>
                        </tr>
                    </table>
                    <br />
                    Click 'Change' next to 'Company Status' for more information.
                </div>
            </asp:Panel>
            <asp:Panel runat="server" ID="panelBox" Visible="false" Width="100%" HorizontalAlign="center">
                <div style="border-right: black 1px solid; padding-right: 10px; border-top: black 1px solid; padding-left: 10px; padding-bottom: 10px; border-left: black 1px solid; padding-top: 10px; border-bottom: black 1px solid; background-color: #ffffcc; text-align: center"
                    align="center">
                    <span style="font-size: 13pt; color: maroon"><strong><span style="font-family: Verdana">
                        <asp:Label ID="lblBox1" runat="server" Text="Submitted Successfully"></asp:Label></span><br />
                    </strong><span style="color: maroon"><span style="font-size: 10pt; font-family: Verdana">
                        <asp:Label ID="lblBox2" runat="server" Text="Your questionnaire will now be assessed and procurement will be in contact with you regarding your submission."></asp:Label>
                        <div align="center">
                            <dx:ASPxButton ID="btnSendBackToSupplier_PopUp" ClientInstanceName="btnSendBackToSupplier_PopUp"
                                AutoPostBack="false" UseSubmitBehavior="false" runat="server" Text="Send Back To Supplier"
                                Visible="false" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue"
                                SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                            </dx:ASPxButton>
                            <dx:ASPxHyperLink ID="hlLatestSQ" runat="server" Visible="false" NavigateUrl="javascript:void(0);"
                                Text="Click Here to view the latest questionnaire for this company">
                            </dx:ASPxHyperLink>
                            <span style="font-size: 13pt; color: maroon"><span style="color: maroon"><span style="font-size: 10pt; font-family: Verdana">
                                <dx:ASPxButton ID="btnNotifySupplierContact" runat="server" AutoPostBack="false"
                                    ClientInstanceName="btnNotifySupplierContact" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                    CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                    Text="Notify Supplier Contact" UseSubmitBehavior="false" Visible="false">
                                </dx:ASPxButton>
                            </span></span></span>
                        </div>
                    </span></span></span>
                </div>
                <dx:ASPxPopupControl ID="popupNotify" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                    CssPostfix="Office2003Blue" EnableHotTrack="False" Modal="True" PopupElementID="btnNotifySupplierContact"
                    PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" ShowHeader="False"
                    SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" Width="420px">
                    <HeaderStyle>
                        <Paddings PaddingRight="6px" />
                    </HeaderStyle>
                    <ContentCollection>
                        <dx:PopupControlContentControl runat="server">
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td colspan="2">
                                        <dxe:ASPxLabel ID="lblNotifyNotes" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                            CssPostfix="Office2003Blue" ForeColor="Red">
                                        </dxe:ASPxLabel>
                                    </td>
                                </tr>
                                <tr>
                                    <td>From:
                                      </td>
                                    <td>
                                        <dxe:ASPxTextBox ID="tbNotifyFrom" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                            CssPostfix="Office2003Blue" Enabled="False" ReadOnly="True" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                            Width="500px">
                                        </dxe:ASPxTextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>To:
                                      </td>
                                    <td>
                                        <dxe:ASPxTextBox ID="tbNotifyTo" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                            CssPostfix="Office2003Blue" Enabled="False" ReadOnly="True" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                            Width="500px">
                                        </dxe:ASPxTextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>CC:
                                      </td>
                                    <td>
                                        <dxe:ASPxTextBox ID="tbNotifyCC" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                            CssPostfix="Office2003Blue" Enabled="False" ReadOnly="True" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                            Width="500px">
                                        </dxe:ASPxTextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Subject:
                                      </td>
                                    <td>
                                        <dxe:ASPxTextBox ID="tbNotifySubject" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                            CssPostfix="Office2003Blue" Enabled="False" ReadOnly="True" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                            Width="500px">
                                        </dxe:ASPxTextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Message:
                                      </td>
                                    <td>
                                        <dxe:ASPxMemo ID="mbNotifyMessage" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                            CssPostfix="Office2003Blue" Height="150px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                            Width="500px">
                                        </dxe:ASPxMemo>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <div align="center">
                                            <dx:ASPxButton ID="btnSendNotificationEmail" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                Text="Send Notification E-Mail" Width="224px" Enabled="False" OnClick="btnSendNotificationEmail_Click">
                                                <ClientSideEvents Click="function (s, e) {e.processOnServer = confirm('Are you sure you want to send the contact a notification e-mail?');}" />
                                            </dx:ASPxButton>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>&nbsp;
                                      </td>
                                    <td>Number of Emails Sent:
                                          <dxe:ASPxLabel ID="lblNotifyNotes0" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                            CssPostfix="Office2003Blue" ForeColor="Red">
                                        </dxe:ASPxLabel>
                                        <dxe:ASPxLabel ID="lblNotifyNotes1" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                            CssPostfix="Office2003Blue" ForeColor="Red">
                                        </dxe:ASPxLabel>
                                    </td>
                                </tr>
                            </table>
                        </dx:PopupControlContentControl>
                    </ContentCollection>
                </dx:ASPxPopupControl>
                <br />
            </asp:Panel>

            <div align="center">
                <dx:ASPxPageControl ID="ASPxPageControl1" runat="server" ActiveTabIndex="0" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                    CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                    <TabPages>
                        <dx:TabPage Name="Questionnaire" Text="Questionnaire">
                            <ContentCollection>
                                <dx:ContentControl ID="ContentControl1" runat="server">
                                    <asp:Panel ID="panelModify" runat="server" Width="630px">
                                        <table class="templateTable" cellpadding="2" cellspacing="1" style="border-right: #c2d4da 1px solid; border-top: #c2d4da 1px solid; border-left: #c2d4da 1px solid; border-bottom: #c2d4da 1px solid"
                                            width="630">
                                            <tr>
                                                <td class="templateCaption" style="width: 260px; height: 20px; text-align: right">
                                                    <strong><span style="font-size: 11pt">Company Name:</span></strong>
                                                  </td>
                                                <td colspan="3" style="height: 20px; text-align: left; width: 196px;">
                                                    <asp:Label ID="lblCompanyName" runat="server" ForeColor="Black" Text="-" Font-Size="11pt"></asp:Label>
                                                </td>
                                                <td>
                                                    <dx:ASPxButton ID="btnPrint" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                        CssPostfix="Office2003Blue" Text="Print All" OnClick="btnPrint_Click" ToolTip="Show Printable Version of complete Safety Qualification Questionnaire (May take some time to load due to large size of data)"
                                                        AutoPostBack="False" EnableClientSideAPI="False" UseSubmitBehavior="False" Width="106px"
                                                        SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                                    </dx:ASPxButton>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="templateCaption" style="width: 260px; height: 20px; text-align: right">
                                                    <strong><span style="font-size: 11pt">Company Type:</span></strong>
                                                  </td>
                                                <td colspan="4" style="height: 20px; text-align: left; width: 296px;">
                                                    <asp:Label ID="lblSubcontractor" runat="server" ForeColor="Black" Text="-" Font-Size="11pt"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="templateCaption" style="width: 260px; height: 12px; text-align: right">
                                                    <strong><span style="font-size: 11pt">Company Status:</span></strong>
                                                  </td>
                                                <td style="height: 12px; text-align: left; width: 160px;">
                                                    <dx:ASPxComboBox ID="cbCompanyStatus" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                        CssPostfix="Office2003Blue" DataSourceID="CompanyStatusDataSource" TextField="CompanyStatusDesc"
                                                        ValueField="CompanyStatusId" ValueType="System.Int32" DropDownHeight="210px"
                                                        Width="153px" IncrementalFilteringMode="StartsWith" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                        ReadOnly="true">
                                                        <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                                                        </LoadingPanelImage>
                                                        <ButtonStyle Width="13px">
                                                        </ButtonStyle>
                                                    </dx:ASPxComboBox>
                                                </td>
                                                <td style="height: 12px; text-align: left; width: 149px;">
                                                    
                                                    <dx:ASPxHyperLink ID="hlChangeCompanyStatus" runat="server" Visible="false" ClientInstanceName="hlChangeCompanyStatus"
                                                        NavigateUrl="javascript:void(0);" Text="Change">
                                                    </dx:ASPxHyperLink>
                                                    <dx:ASPxPopupControl ID="popupChangeCompanyStatus" runat="server" AllowDragging="True"
                                                        CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue"
                                                        HeaderText="Change Company Status" Modal="True" PopupElementID="hlChangeCompanyStatus"
                                                        Width="520px" Font-Bold="False" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                        ShowHeader="False">
                                                        <SizeGripImage Height="16px" Width="16px" />
                                                        <ContentCollection>
                                                            <dx:PopupControlContentControl ID="popupContentChangeCompanyStatus" runat="server"
                                                                BackColor="#DDECFE">
                                                                <dx:ASPxRoundPanel ID="rpChangeCompanyStatus" runat="server" BackColor="#DDECFE"
                                                                    CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue"
                                                                    ShowHeader="false" EnableDefaultAppearance="False" HeaderText="Change Company Status"
                                                                    SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" Width="490px">
                                                                    <TopEdge>
                                                                        <BackgroundImage HorizontalPosition="left" ImageUrl="~/App_Themes/Office2003Blue/Web/rpTopEdge.png"
                                                                            Repeat="RepeatX" VerticalPosition="top" />
                                                                    </TopEdge>
                                                                    <ContentPaddings Padding="2px" PaddingBottom="10px" PaddingTop="10px" />
                                                                    <HeaderRightEdge>
                                                                        <BackgroundImage HorizontalPosition="left" ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png"
                                                                            Repeat="RepeatX" VerticalPosition="top" />
                                                                    </HeaderRightEdge>
                                                                    <Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px" />
                                                                    <HeaderLeftEdge>
                                                                        <BackgroundImage HorizontalPosition="left" ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png"
                                                                            Repeat="RepeatX" VerticalPosition="top" />
                                                                    </HeaderLeftEdge>
                                                                    <HeaderStyle BackColor="#7BA4E0">
                                                                        <Paddings Padding="0px" PaddingBottom="7px" PaddingLeft="2px" PaddingRight="2px" />
                                                                        <BorderBottom BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px" />
                                                                    </HeaderStyle>
                                                                    <HeaderContent>
                                                                        <BackgroundImage HorizontalPosition="left" ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png"
                                                                            Repeat="RepeatX" VerticalPosition="top" />
                                                                    </HeaderContent>
                                                                    <DisabledStyle ForeColor="Gray">
                                                                    </DisabledStyle>
                                                                    <NoHeaderTopEdge BackColor="#DDECFE">
                                                                    </NoHeaderTopEdge>
                                                                    <PanelCollection>
                                                                        <dx:PanelContent ID="PanelContent4" runat="server">
                                                                            <dx:ASPxPanel ID="panelChangeCompanyStatus" runat="server" Width="490px">
                                                                                <PanelCollection>
                                                                                    <dx:PanelContent runat="server">
                                                                                        <table width="490px">
                                                                                            <tr>
                                                                                                <td class="style1" style="text-align: right;">
                                                                                                    <b>Current Company Status: </b>
                                                                                                </td>
                                                                                                <td style="text-align: left">
                                                                                                    <dx:ASPxLabel ID="lblCompanyStatus" runat="server" Text="(Not Assigned)">
                                                                                                    </dx:ASPxLabel>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="style2" style="text-align: right;">New Company Status:
                                                                                                  </td>
                                                                                                <td style="text-align: left">
                                                                                                    <dx:ASPxComboBox runat="server" IncrementalFilteringMode="StartsWith" ValueType="System.Int32"
                                                                                                        DataSourceID="CompanyStatusDataSource" TextField="CompanyStatusDesc" ValueField="CompanyStatusId"
                                                                                                        Width="240px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                                                                        CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                                                        ID="cbChangeCompanyStatus" Enabled="true">
                                                                                                        <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                                                                                                        </LoadingPanelImage>
                                                                                                        <ButtonStyle Width="13px">
                                                                                                        </ButtonStyle>
                                                                                                        <ValidationSettings Display="Dynamic" ValidationGroup="ChangeCompanyStatus">
                                                                                                            <RequiredField IsRequired="True" />
                                                                                                        </ValidationSettings>
                                                                                                    </dx:ASPxComboBox>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                        <br />
                                                                                        <div align="center" style="padding-bottom: 1px">
                                                                                            <b>Additional Comments (recorded in History Log)<span style="font-size: 13pt; color: maroon"><span
                                                                                                style="color: maroon"><span style="font-size: 10pt; font-family: Verdana"></span></span></span></b>
                                                                                          </div>
                                                                                        <dx:ASPxMemo ID="mbCompanyStatus" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                                            CssPostfix="Office2003Blue" Height="53px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                                                            Width="100%">
                                                                                            <ValidationSettings Display="Dynamic" ValidationGroup="ChangeCompanyStatus">
                                                                                                <RequiredField IsRequired="True" />
                                                                                            </ValidationSettings>
                                                                                        </dx:ASPxMemo>
                                                                                        <dx:ASPxLabel ID="lblChangeCompanyStatusError" runat="server" Font-Bold="True" ForeColor="Red">
                                                                                        </dx:ASPxLabel>
                                                                                        <br />
                                                                                        <div align="center">
                                                                                            <dx:ASPxButton ID="btnChangeCompanyStatus" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                                                CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                                                                Text="Change Company Status" Width="236px" OnClick="btnChangeCompanyStatus_Click"
                                                                                                ValidationGroup="ChangeCompanyStatus">
                                                                                                <ClientSideEvents Click="function (s, e) {e.processOnServer = confirm('Are you sure you want to change the current company status?');}" />
                                                                                            </dx:ASPxButton>
                                                                                            <br />
                                                                                            <span style="color: red">Note: Changing a Company Status to 'InActive' will forward
                                                                                                a request to the Manager - Procurement Operations for acceptance/confirmation.Note: Changing a Company Status to 'InActive' will forward
                                                                                                a request to the Manager - Procurement Operations for acceptance/confirmation.Note: Changing a Company Status to 'InActive' will forward
                                                                                                a request to the Manager - Procurement Operations for acceptance/confirmation.Note: Changing a Company Status to 'InActive' will forward
                                                                                                a request to the Manager - Procurement Operations for acceptance/confirmation.</span>
                                                                                          </div>
                                                                                    </dx:PanelContent>
                                                                                </PanelCollection>
                                                                            </dx:ASPxPanel>
                                                                            <br />
                                                                            <dx:ASPxPanel ID="panelChangeCompanyStatusPending" runat="server" Width="490px" Visible="false">
                                                                                <PanelCollection>
                                                                                    <dx:PanelContent runat="server">
                                                                                        <div align="center">
                                                                                            <span style="color: red">A Request to Change Company Status is pending approval by the
                            Manager - Procurement Operations (or CSMS Administrator).A Request to Change Company Status is pending approval by the
                            Manager - Procurement Operations (or CSMS Administrator).A Request to Change Company Status is pending approval by the
                            Manager - Procurement Operations (or CSMS Administrator).A Request to Change Company Status is pending approval by the
                            Manager - Procurement Operations (or CSMS Administrator).<br />
                                                                                                Until this request has been approved (or not approved), no further company status
                                                                                                changes may occur. Until this request has been approved (or not approved), no further company status
                                                                                                changes may occur. Until this request has been approved (or not approved), no further company status
                                                                                                changes may occur. Until this request has been approved (or not approved), no further company status
                                                                                                changes may occur.</span>
                                                                                          </div>
                                                                                        <br />
                                                                                        <table width="490px">
                                                                                            <tr>
                                                                                                <td class="style1" style="text-align: right;">
                                                                                                    <b>Current Company Status: </b>
                                                                                                </td>
                                                                                                <td style="text-align: left">
                                                                                                    <dx:ASPxLabel ID="lblCscpCompanyStatusCurrent" runat="server" Text="(Not Assigned)">
                                                                                                    </dx:ASPxLabel>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="style2" style="text-align: right;">New Company Status:
                                                                                                  </td>
                                                                                                <td style="text-align: left">
                                                                                                    <dx:ASPxLabel ID="lblCscpCompanyStatusNew" runat="server" Text="(Not Assigned)">
                                                                                                    </dx:ASPxLabel>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="style2" style="text-align: right;">Comments:
                                                                                                  </td>
                                                                                                <td style="text-align: left">
                                                                                                    <dx:ASPxLabel ID="lblCscpComments" runat="server" Text="(Not Assigned)">
                                                                                                    </dx:ASPxLabel>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="style2" style="text-align: right;">Requested By:
                                                                                                  </td>
                                                                                                <td style="text-align: left">
                                                                                                    <dx:ASPxLabel ID="lblCscpRequestedBy" runat="server" Text="(Not Assigned)">
                                                                                                    </dx:ASPxLabel>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                        <br />
                                                                                        <div align="center" style="padding-bottom: 1px">
                                                                                            <b>Additional Comments (recorded in History Log)<span style="font-size: 13pt; color: maroon"><span
                                                                                                style="color: maroon"><span style="font-size: 10pt; font-family: Verdana"></span></span></span></b>
                                                                                          </div>
                                                                                        <dx:ASPxMemo ID="mbCscpCompanyStatus" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                                            CssPostfix="Office2003Blue" Height="53px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                                                            Width="100%">
                                                                                            <ValidationSettings Display="Dynamic" ValidationGroup="CscpChangeCompanyStatus">
                                                                                                <RequiredField IsRequired="True" />
                                                                                            </ValidationSettings>
                                                                                        </dx:ASPxMemo>
                                                                                        <dx:ASPxLabel ID="lblCscpChangeCompanyStatusError" runat="server" Font-Bold="True"
                                                                                            ForeColor="Red">
                                                                                        </dx:ASPxLabel>
                                                                                        <br />
                                                                                        <div align="center">
                                                                                            <dx:ASPxButton ID="btnCscpApprove" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                                                CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                                                                Text="Approve" Width="236px" OnClick="btnCscpApprove_Click" ValidationGroup="CscpChangeCompanyStatus">
                                                                                                <ClientSideEvents Click="function (s, e) {e.processOnServer = confirm('Are you sure you want to approve this Company Status Change Request?');}" />
                                                                                            </dx:ASPxButton>
                                                                                            <dx:ASPxButton ID="btnCscpNotApprove" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                                                CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                                                                Text="NOT Approve" Width="236px" OnClick="btnCscpNotApprove_Click" ValidationGroup="CscpChangeCompanyStatus">
                                                                                                <ClientSideEvents Click="function (s, e) {e.processOnServer = confirm('Are you sure you want to NOT approve this Company Status Change Request?');}" />
                                                                                            </dx:ASPxButton>
                                                                                        </div>
                                                                                    </dx:PanelContent>
                                                                                </PanelCollection>
                                                                            </dx:ASPxPanel>
                                                                        </dx:PanelContent>
                                                                    </PanelCollection>
                                                                </dx:ASPxRoundPanel>
                                                            </dx:PopupControlContentControl>
                                                        </ContentCollection>
                                                        <CloseButtonImage Height="12px" Width="13px" />
                                                        <HeaderStyle>
                                                            <Paddings PaddingRight="6px" />
                                                        </HeaderStyle>
                                                    </dx:ASPxPopupControl>
                                                </td>
                                                <td colspan="2" style="height: 12px; text-align: left; width: 116px;">
                                                    <dx:ASPxButton ID="btnClose" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                        CssPostfix="Office2003Blue" Text="Close" OnClick="btnClose_Click" AutoPostBack="False"
                                                        EnableClientSideAPI="False" UseSubmitBehavior="False" Width="106px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                                        <%--<ClientSideEvents Click="function(s, e) {
	window.location='SafetyPQ_Questionnaire.aspx';
}" />--%>
                                                    </dx:ASPxButton>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="templateCaption" style="width: 260px; height: 3px; text-align: right">
                                                    <span style="font-size: 11pt"><strong>Safety Assessor:</strong></span>
                                                  </td>
                                                <td style="height: 3px; text-align: left; width: 160px;">
                                                    <dx:ASPxComboBox ID="cbEhsConsultant" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                        CssPostfix="Office2003Blue" DataSourceID="QuestionnaireStatusDataSource1" ReadOnly="true"
                                                        TextField="QuestionnaireStatusDesc" ValueField="QuestionnaireStatusId" ValueType="System.Int32"
                                                        Enabled="false" Width="153px" IncrementalFilteringMode="StartsWith" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                                        <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                                                        </LoadingPanelImage>
                                                        <ButtonStyle Width="13px">
                                                        </ButtonStyle>
                                                    </dx:ASPxComboBox>
                                                </td>
                                                <td style="height: 3px; text-align: left; width: 149px;">
                                                    <dx:ASPxHyperLink ID="hlChangeHSAssessor" runat="server" Visible="false" ClientInstanceName="hlChangeHSAssessor"
                                                        NavigateUrl="javascript:void(0);" Text="Change">
                                                    </dx:ASPxHyperLink>
                                                </td>
                                                <td colspan="2" style="padding-right: 5px; height: 16px; text-align: left; width: 116px;">
                                                    <dx:ASPxButton ID="btnRiskSummary" runat="server" AutoPostBack="False" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                        CssPostfix="Office2003Blue" OnClick="btnRiskSummary_Click" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                        Text="Risk Summary" UseSubmitBehavior="False" Visible="False" Width="106px">
                                                    </dx:ASPxButton>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="templateCaption" style="width: 260px; height: 16px; text-align: right">
                                                    <strong><span style="font-size: 11pt">Questionnaire Status:</span></strong>
                                                  </td>
                                                <td style="width: 160px; height: 16px; text-align: left; padding-right: 5px;">
                                                    <dx:ASPxComboBox ID="cbQuestionnaireStatus" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                        CssPostfix="Office2003Blue" DataSourceID="QuestionnaireStatusDataSource1" TextField="QuestionnaireStatusDesc"
                                                        ValueField="QuestionnaireStatusId" ValueType="System.Int32" Width="153px" IncrementalFilteringMode="StartsWith"
                                                        SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                                        <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                                                        </LoadingPanelImage>
                                                        <ButtonStyle Width="13px">
                                                        </ButtonStyle>
                                                    </dx:ASPxComboBox>
                                                </td>
                                                <td style="height: 16px; text-align: left; padding-right: 5px; width: 149px;">
                                                    <dx:ASPxButton ID="btnSave" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                        CssPostfix="Office2003Blue" OnClick="btnSave_Click" Text="Save" ValidationGroup="Save"
                                                        Width="70px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                                    </dx:ASPxButton>
                                                </td>
                                                <td colspan="2" style="padding-right: 5px; height: 16px; text-align: left; width: 116px;">
                                                    <dx:ASPxButton ID="btnScoreAnalysis" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                        CssPostfix="Office2003Blue" Text="Score Analysis" OnClick="btnScoreAnalysis_Click"
                                                        AutoPostBack="False" EnableClientSideAPI="True" UseSubmitBehavior="True" Visible="false"
                                                        Width="106px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                                    </dx:ASPxButton>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="templateCaption" style="width: 260px; height: 36px; text-align: right">
                                                    <strong><span style="font-size: 11pt">Questionnaire Type:</span></strong>
                                                  </td>
                                                <td style="padding-right: 5px; width: 160px; height: 36px; text-align: left">
                                                    <dx:ASPxComboBox ID="cbSqType" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                        CssPostfix="Office2003Blue" ValueType="System.Int32" Width="153px" IncrementalFilteringMode="StartsWith"
                                                        ReadOnly="True" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                                        <Items>
                                                            <dx:ListEditItem Text="Pre-Qualification" Value="0" />
                                                            <dx:ListEditItem Text="Re-Qualification" Value="1" />
                                                        </Items>
                                                        <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                                                        </LoadingPanelImage>
                                                        <ButtonStyle Width="13px">
                                                        </ButtonStyle>
                                                    </dx:ASPxComboBox>
                                                </td>
                                                <td style="padding-right: 5px; height: 36px; text-align: left; width: 149px;">
                                                    <dx:ASPxButton ID="btnReQualify" runat="server" Text="Re-Qualify" OnClick="btnReQualify_Click"
                                                        ClientInstanceName="btnReQualify2" Enabled="false" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                        CssPostfix="Office2003Blue" Width="106px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                                        <ClientSideEvents Click="function (s, e) {e.processOnServer = confirm('Are you sure you want to re-qualify this questionnaire?');}" />
                                                    </dx:ASPxButton>
                                                </td>
                                                <td style="padding-right: 5px; text-align: left; height: 36px; width: 116px;" colspan="2">
                                                    <dx:ASPxButton ID="btnCompare" runat="server" OnClick="btnCompare_Click" Text="Compare & Assess"
                                                        CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue"
                                                        Width="106px" Enabled="False" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                                    </dx:ASPxButton>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="templateCaption" style="height: 7px; text-align: center" colspan="5">
                                                    <asp:Label ID="lblStatus" runat="server" ForeColor="Black" Text="-" Font-Size="10pt"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="templateCaption" style="width: 260px; height: 4px; text-align: right">
                                                    <span style="color: gray"><strong>Created By:</strong> </span>
                                                </td>
                                                <td style="height: 4px; text-align: left;" colspan="4">
                                                    <asp:Label ID="lblCreatedBy" runat="server" ForeColor="Gray" Text="-"></asp:Label>
                                                    <span style="color: #808080"></span>
                                                </td>
                                            </tr>
                                            <tr style="color: #808080">
                                                <td class="templateCaption" style="width: 260px; text-align: right; height: 17px;">
                                                    <span style="color: gray"><strong>Submitted By:</strong> </span>
                                                </td>
                                                <td style="text-align: left; height: 17px;" colspan="4">
                                                    <asp:Label ID="lblSubmittedBy" runat="server" ForeColor="Gray" Text="-"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr style="color: #808080">
                                                <td class="templateCaption" style="width: 260px; text-align: right; height: 17px;">
                                                    <strong>Assessed By:</strong>
                                                  </td>
                                                <td colspan="4" style="text-align: left; height: 17px;">
                                                    <asp:Label ID="lblAssessedBy" runat="server" ForeColor="Gray" Text="-"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr style="color: #808080">
                                                <td class="templateCaption" style="width: 260px; text-align: right; height: 17px;">
                                                    <asp:Label ID="lblPresentlyWithText" runat="server" Text="Presently With:" Font-Bold="true"></asp:Label>
                                                </td>
                                                <td colspan="4" style="text-align: left; height: 17px;">
                                                    <asp:Label ID="lblPresentlyWith" runat="server" ForeColor="Gray" Text="-"></asp:Label>&nbsp;
                                                  </td>
                                            </tr>
                                            <tr style="color: #808080">
                                                <td class="templateCaption" style="width: 260px; text-align: right; height: 17px;">
                                                    <asp:Label ID="lblExpiresAtText" runat="server" Text="Valid Until:" Font-Bold="true"></asp:Label>
                                                </td>
                                                <td colspan="4" style="text-align: left; height: 17px;">
                                                    <asp:Label ID="lblExpiresAt" runat="server" ForeColor="Gray" Text="-"></asp:Label>&nbsp;
                                                      <dx:ASPxHyperLink ID="hlPreviousQuestionnaire" runat="server" Text="(Previous Questionnaire)"
                                                        Visible="false">
                                                    </dx:ASPxHyperLink>
                                                </td>
                                            </tr>
                                            <tr style="color: #808080">
                                                <td class="templateCaption" colspan="5" style="text-align: center">
                                                    <br />
                                                    <div align="center">
                                                        <table border="0" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td style="width: 270px; height: 41px">
                                                                    <asp:ImageButton ID="img1Initial" runat="server" ImageUrl="~/Images/Questionnaire/1.gif" />
                                                                </td>
                                                                <td style="width: 70px; height: 41px; text-align: center">
                                                                    <asp:ImageButton ID="imgArrowLeft1Initial" runat="server" ImageUrl="~/Images/Questionnaire/arrowLeft.gif"
                                                                        PostBackUrl="javascript:void(0);" Enabled="false" />
                                                                </td>
                                                                <td style="width: 170px; height: 41px; text-align: left;">
                                                                    <asp:Label ID="lblStatus1Initial" runat="server" Text="Click on this button" Font-Size="11pt"
                                                                        ForeColor="Red"></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <asp:Panel ID="Panel1" runat="server" Width="100%">
                                                            <table border="0" cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td style="width: 270px; height: 41px; text-align: center;">
                                                                        <asp:Image ID="imgArrow1" runat="server" ImageUrl="~/Images/Questionnaire/Arrow.gif" />
                                                                        <br />
                                                                        <asp:ImageButton ID="img2Main" runat="server" ImageUrl="~/Images/Questionnaire/2.gif" />
                                                                        <asp:Image ID="imgArrow2" runat="server" ImageUrl="~/Images/Questionnaire/Arrow.gif" />
                                                                    </td>
                                                                    <td style="width: 70px; height: 41px; text-align: center">
                                                                        <asp:ImageButton ID="imgArrowLeft2Main" runat="server" ImageUrl="~/Images/Questionnaire/arrowLeft.gif"
                                                                            PostBackUrl="javascript:void(0);" Enabled="false" />
                                                                    </td>
                                                                    <td style="width: 170px; height: 41px; text-align: left;">
                                                                        <asp:Label ID="lblStatus2Main" runat="server" Text="Click on this button" Font-Size="11pt"
                                                                            ForeColor="Red"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </asp:Panel>
                                                        <asp:Panel ID="Panel2SubmitArrow" runat="server" Width="100%" Visible="False">
                                                            <table border="0" cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td style="width: 270px; height: 41px; text-align: center;">
                                                                        <asp:Image ID="imgArrow2Submit" runat="server" ImageUrl="~/Images/Questionnaire/Arrow.gif"
                                                                            Visible="False" />
                                                                    </td>
                                                                    <td style="width: 70px; height: 41px; text-align: center"></td>
                                                                    <td style="width: 170px; height: 41px; text-align: left;"></td>
                                                                </tr>
                                                            </table>
                                                        </asp:Panel>
                                                        <asp:Panel ID="panel_2" runat="server" Width="100%">
                                                            <table border="0" cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td style="width: 270px; height: 41px; text-align: center;">
                                                                        <asp:LinkButton ID="img3Submit" runat="server" OnClick="Submit_Click" PostBackUrl="javascript:alert('You may not submit until all questionnaires have been completed.');">
                                                                              <img id="img3SubmitImg" src="~/Images/Questionnaire/3s.gif" alt="" runat="server" />
                                                                        </img></img></img></img></img></asp:LinkButton>
                                                                    </td>
                                                                    <td style="width: 70px; height: 41px; text-align: center">
                                                                        <asp:ImageButton ID="imgArrowLeft3Submit" runat="server" ImageUrl="~/Images/Questionnaire/arrowLeft.gif"
                                                                            PostBackUrl="javascript:void(0);" Enabled="false" />
                                                                    </td>
                                                                    <td style="width: 170px; height: 41px; text-align: left;">
                                                                        <asp:Label ID="lblStatus3Submit" runat="server" Text="Click on this button" Font-Size="11pt"
                                                                            ForeColor="Red"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </asp:Panel>
                                                        <asp:Panel ID="panel_3" runat="server" Width="100%" Visible="False">
                                                            <table border="0" cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td rowspan="2" style="width: 270px; text-align: center">
                                                                        <asp:ImageButton ID="img3Verification" runat="server" ImageUrl="~/Images/Questionnaire/3.gif" />
                                                                        <asp:Image ID="imgArrow4" runat="server" ImageUrl="~/Images/Questionnaire/Arrow.gif" />
                                                                    </td>
                                                                    <td style="width: 70px; height: 41px; text-align: center">
                                                                        <asp:ImageButton ID="imgArrowLeft3Verification" runat="server" ImageUrl="~/Images/Questionnaire/arrowLeft.gif"
                                                                            PostBackUrl="javascript:void(0);" Enabled="false" />
                                                                    </td>
                                                                    <td style="width: 170px; height: 41px; text-align: left;">
                                                                        <asp:Label ID="lblStatus3Verification" runat="server" Text="Click on this button"
                                                                            Font-Size="11pt" ForeColor="Red"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 70px; height: 41px; text-align: center"></td>
                                                                    <td style="width: 170px; height: 41px; text-align: left"></td>
                                                                </tr>
                                                            </table>
                                                            <table border="0" cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td style="width: 150px; height: 41px">
                                                                        <asp:LinkButton ID="lnk4Submit" runat="server" OnClick="Submit_Click" PostBackUrl="javascript:alert('You may not submit until all questionnaires have been completed.');">
                                                                              <img id="lnk4SubmitImg" src="~/Images/Questionnaire/4s.gif" alt="" runat="server" />
                                                                        </img></img></img></img></img></asp:LinkButton>
                                                                    </td>
                                                                    <td style="width: 70px; height: 41px; text-align: center">
                                                                        <asp:ImageButton ID="imgArrowLeft4Submit" runat="server" ImageUrl="~/Images/Questionnaire/arrowLeft.gif"
                                                                            PostBackUrl="javascript:void(0);" Enabled="false" />
                                                                    </td>
                                                                    <td style="width: 171px; height: 41px; text-align: left;">
                                                                        <asp:Label ID="lblStatus4Submit" runat="server" Font-Size="11pt" Text="Click on this button"
                                                                            ForeColor="Red"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </asp:Panel>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr style="color: #808080">
                                                <td class="templateCaption" colspan="5" style="text-align: center">
                                                    <div align="center">
                                                        <asp:Panel ID="panelRecommend" runat="server" Visible="False" Width="530px">
                                                            <table border="0" cellpadding="0" cellspacing="0" style="padding-left: 1px; padding-bottom: 1px; padding-top: 1px">
                                                                <tr>
                                                                    <td colspan="5" style="height: 13px; text-align: center">
                                                                        <span style="font-size: 12pt; color: black;"><strong>
                                                                            <br />
                                                                            Recommendation &amp; Comments</strong></span>
                                                                      </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="5" style="height: 13px; text-align: center">
                                                                        <asp:Label ID="lblReadOnly" runat="server" Visible="false" ForeColor="Red" Text="nb. This Panel is read-only until all questionnaires are assessed as Approved."></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr style="color: #000000">
                                                                    <td style="padding-right: 5px; width: 93px; text-align: right">Recommended:
                                                                      </td>
                                                                    <td style="text-align: left" colspan="4">
                                                                        <dx:ASPxRadioButtonList ID="rbRecommended" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                            CssPostfix="Office2003Blue" RepeatDirection="Horizontal" ValueType="System.Int32"
                                                                            Width="100px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                                                            <Items>
                                                                                <dx:ListEditItem Text="Yes" Value="1" />
                                                                                <dx:ListEditItem Text="No" Value="0" />
                                                                            </Items>
                                                                            <ValidationSettings ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True" ValidationGroup="Save">
                                                                                <RequiredField IsRequired="True" />
                                                                            </ValidationSettings>
                                                                        </dx:ASPxRadioButtonList>
                                                                    </td>
                                                                </tr>
                                                                <tr style="color: #000000">
                                                                    <td style="padding-right: 5px; width: 93px; text-align: right; height: 42px;">Level of Supervision:&nbsp;
                                                                      </td>
                                                                    <td style="width: 112px; text-align: left; height: 42px;">
                                                                        <dx:ASPxComboBox ID="cbDirect" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                            CssPostfix="Office2003Blue" ValueType="System.String" Width="112px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                                                            <Items>
                                                                                <dx:ListEditItem Text="Direct" Value="Direct" />
                                                                                <dx:ListEditItem Text="InDirect" Value="InDirect" />
                                                                            </Items>
                                                                            <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                                                                            </LoadingPanelImage>
                                                                            <ButtonStyle Width="13px">
                                                                            </ButtonStyle>
                                                                            <ValidationSettings CausesValidation="True" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                                                                                ValidationGroup="Save">
                                                                                <RequiredField IsRequired="True" />
                                                                            </ValidationSettings>
                                                                        </dx:ASPxComboBox>
                                                                    </td>
                                                                    <td style="width: 79px; text-align: left; height: 42px;">
                                                                        <dx:ASPxButton ID="btnChangeLos" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                            CssPostfix="Office2003Blue" OnClick="btnChangeLos_Click" Text="Change" Visible="False"
                                                                            Width="70px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                                                        </dx:ASPxButton>
                                                                    </td>
                                                                    <td style="padding-right: 5px; width: 99px; text-align: right; height: 42px;">Questionnaire based result:
                                                                      </td>
                                                                    <td style="text-align: left; width: 62px; height: 42px;">
                                                                        <dx:ASPxComboBox ID="cbSupervisionQuestionnaire" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                            CssPostfix="Office2003Blue" ValueType="System.String" SelectedIndex="0" Width="90px"
                                                                            Enabled="False" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                                                            <Columns>
                                                                                <dx:ListBoxColumn Caption="Questionnaire Status" FieldName="QuestionnaireStatusName" />
                                                                            </Columns>
                                                                            <Items>
                                                                                <dx:ListEditItem Text="Direct" Value="Direct" />
                                                                                <dx:ListEditItem Text="InDirect" Value="InDirect" />
                                                                            </Items>
                                                                            <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                                                                            </LoadingPanelImage>
                                                                            <ButtonStyle Width="13px">
                                                                            </ButtonStyle>
                                                                        </dx:ASPxComboBox>
                                                                    </td>
                                                                </tr>
                                                                <tr style="color: #000000">
                                                                    <td style="padding-right: 5px; width: 93px; text-align: right">Comments:
                                                                      </td>
                                                                    <td colspan="4" style="height: 88px; text-align: left">
                                                                        <dx:ASPxMemo ID="mRecommended" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                            CssPostfix="Office2003Blue" Height="71px" Width="410px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                                                            <ValidationSettings ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True" ValidationGroup="Save">
                                                                                <RequiredField IsRequired="True" />
                                                                            </ValidationSettings>
                                                                        </dx:ASPxMemo>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </asp:Panel>
                                                        <%--DT230 Cindi Thornton 25/9/15 move detail grid to new always visible panel. Deleted from panelRecommend above--%>
                                                        <dx:ASPxPanel ID="panelLocation" runat="server" Width="530px" Visible="true" Enabled="true">
                                                            <PanelCollection>
                                                                <dx:PanelContent ID="PanelContent8" runat="server">
                                                                      <table border="0" cellpadding="0" cellspacing="0" style="padding-left: 1px; padding-bottom: 1px; padding-top: 1px">
                                                                          <tr style="color: #000000">
                                                                    <td style="padding-right: 5px; width: 68px; text-align: right; height: 90px;">Location(s) Approval:
                                                                      </td>
                                                                    <td colspan="4" style="height: 90px; text-align: left">
                                                                        <dx:ASPxGridView ID="detailGrid" ClientInstanceName="detailGrid" runat="server" AutoGenerateColumns="False"
                                                                            KeyFieldName="CompanySiteCategoryStandardId" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                            OnCellEditorInitialize="grid_CellEditorInitialize" CssPostfix="Office2003Blue"
                                                                            OnRowUpdating="grid_RowUpdating" OnInitNewRow="grid_InitNewRow" OnRowInserting="grid_RowInserting"
                                                                            Width="410px" OnStartRowEditing="grid_StartRowEditing" OnHtmlDataCellPrepared="grid_HtmlDataCellPrepared">
                                                                            <Columns>
                                                                                <dx:GridViewCommandColumn Caption="Action" Name="commandCol" VisibleIndex="0" Width="100px">
                                                                                    <EditButton Text="Change Approval" Visible="True">
                                                                                    </EditButton>
                                                                                    <CellStyle ForeColor="#993300">
                                                                                    </CellStyle>
                                                                                    <NewButton Visible="True">
                                                                                    </NewButton>
                                                                                </dx:GridViewCommandColumn>
                                                                                <dx:GridViewDataComboBoxColumn Name="Site" Caption="Site" FieldName="SiteId" UnboundType="String"
                                                                                    VisibleIndex="1" SortOrder="Ascending">
                                                                                    <PropertiesComboBox DataSourceID="SitesDataSource2" DropDownHeight="150px" TextField="SiteName"
                                                                                        IncrementalFilteringMode="StartsWith" ValueField="SiteId" ValueType="System.Int32">
                                                                                    </PropertiesComboBox>
                                                                                </dx:GridViewDataComboBoxColumn>
                                                                                <dx:GridViewDataComboBoxColumn Caption="Residential Category" FieldName="CompanySiteCategoryId"
                                                                                    VisibleIndex="2">
                                                                                    <PropertiesComboBox DataSourceID="CompanySiteCategoryDataSource" TextField="CategoryDesc"
                                                                                        ValueField="CompanySiteCategoryId" ValueType="System.Int32" IncrementalFilteringMode="StartsWith">
                                                                                    </PropertiesComboBox>
                                                                                    <Settings SortMode="DisplayText" />
                                                                                </dx:GridViewDataComboBoxColumn>
                                                                                <dx:GridViewDataTextColumn Caption="Residential Category" FieldName="CompanySiteCategoryDesc"
                                                                                    VisibleIndex="2">
                                                                                        <CellStyle HorizontalAlign="Center">
                                                                                        </CellStyle>
                                                                                </dx:GridViewDataTextColumn>
                                                                                <dx:GridViewDataTextColumn FieldName="SiteId" VisibleIndex="2" ReadOnly="True" Visible="False">
                                                                                </dx:GridViewDataTextColumn>
                                                                                <dx:GridViewDataComboBoxColumn Caption="Location Sponsor" FieldName="LocationSponsorUserId"
                                                                                    VisibleIndex="3">
                                                                                    <PropertiesComboBox DataSourceID="UsersAlcoanDataSource2" DropDownHeight="150px"
                                                                                        TextField="UserFullName" IncrementalFilteringMode="StartsWith" ValueField="UserId"
                                                                                        ValueType="System.Int32">
                                                                                    </PropertiesComboBox>
                                                                                </dx:GridViewDataComboBoxColumn>
                                                                                <dx:GridViewDataComboBoxColumn FieldName="Approved" VisibleIndex="4">
                                                                                    <PropertiesComboBox ValueType="System.String" NullDisplayText="Tentative">
                                                                                        <Items>
                                                                                            <dx:ListEditItem Text="Yes" Value="Yes"></dx:ListEditItem>
                                                                                            <dx:ListEditItem Text="No" Value="No"></dx:ListEditItem>
                                                                                            <%--<dx:ListEditItem Text="Tentative" Value="Tentative"></dx:ListEditItem>--%>
                                                                                        </Items>
                                                                                    </PropertiesComboBox>
                                                                                </dx:GridViewDataComboBoxColumn>
                                                                                <dx:GridViewDataTextColumn FieldName="ApprovedByUserId" Visible="false">
                                                                                </dx:GridViewDataTextColumn>
                                                                                <dx:GridViewDataTextColumn FieldName="ApprovedDate" Visible="false">
                                                                                </dx:GridViewDataTextColumn>
                                                                            </Columns>
                                                                            <SettingsEditing Mode="Inline" />
                                                                            <ImagesFilterControl>
                                                                                <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
                                                                                </LoadingPanel>
                                                                            </ImagesFilterControl>
                                                                            <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue">
                                                                                <Header ImageSpacing="5px" SortingImageSpacing="5px" Wrap="True">
                                                                                </Header>
                                                                                <LoadingPanel ImageSpacing="10px">
                                                                                </LoadingPanel>
                                                                            </Styles>
                                                                            <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                                                                <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
                                                                                </LoadingPanelOnStatusBar>
                                                                                <CollapsedButton Height="12px" Width="11px" />
                                                                                <ExpandedButton Height="12px" Width="11px" />
                                                                                <DetailCollapsedButton Height="12px" Width="11px" />
                                                                                <DetailExpandedButton Height="12px" Width="11px" />
                                                                                <FilterRowButton Height="13px" Width="13px" />
                                                                                <LoadingPanel Url="~/App_Themes/Office2003Blue/GridView/Loading.gif">
                                                                                </LoadingPanel>
                                                                            </Images>
                                                                        </dx:ASPxGridView>
                                                                    </td>
                                                                </tr>
                                                                          </table>
                                                                     </dx:PanelContent>
                                                            </PanelCollection>
                                                        </dx:ASPxPanel>
                                                        <%--End DT230--%>
                                                        <dx:ASPxPanel ID="panelProcurement" runat="server" Width="530px" Visible="false" Enabled="false">
                                                            <PanelCollection>
                                                                <dx:PanelContent runat="server">
                                                                    <table style="padding-left: 1px; padding-bottom: 1px; color: #000000; padding-top: 1px"
                                                                        cellspacing="0" cellpadding="0" width="100%" border="0">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td style="padding-right: 5px; height: 32px; text-align: right" width="113">Procurement Contact:
                                                                                  </td>
                                                                                <td style="width: 318px; height: 32px; text-align: left">
                                                                                    <dx:ASPxComboBox runat="server" IncrementalFilteringMode="StartsWith" ValueType="System.Int32"
                                                                                        DataSourceID="UsersProcurementListDataSource2" TextField="UserFullName" ValueField="UserId"
                                                                                        Width="303px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                                                        CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                                        ID="cbProcurementView" Enabled="false" ReadOnly="true">
                                                                                        <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                                                                                        </LoadingPanelImage>
                                                                                        <ButtonStyle Width="13px">
                                                                                        </ButtonStyle>
                                                                                    </dx:ASPxComboBox>
                                                                                </td>
                                                                                <td style="height: 32px; text-align: left" width="92" colspan="1">
                                                                                    <dx:ASPxHyperLink ID="hlChangeProc" runat="server" ClientInstanceName="hlChangeProc"
                                                                                        NavigateUrl="javascript:void(0);" Text="Change" Visible="True">
                                                                                    </dx:ASPxHyperLink>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td style="padding-right: 5px; height: 12px; text-align: right" width="113">Procurement Functional Supervisor:
                                                                                  </td>
                                                                                <td style="width: 318px; height: 12px; text-align: left">
                                                                                    <dx:ASPxComboBox runat="server" IncrementalFilteringMode="StartsWith" ValueType="System.Int32"
                                                                                        DataSourceID="UsersAlcoanDataSource2" TextField="UserFullName" ValueField="UserId"
                                                                                        Width="303px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                                                        CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                                        ID="cbContract" Enabled="False" ReadOnly="True">
                                                                                        <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                                                                                        </LoadingPanelImage>
                                                                                        <ButtonStyle Width="13px">
                                                                                        </ButtonStyle>
                                                                                    </dx:ASPxComboBox>
                                                                                </td>
                                                                                <td style="width: 53px; height: 12px; text-align: left" colspan="1">
                                                                                    <dx:ASPxHyperLink ID="hlChangeFunctionalManager" runat="server" ClientInstanceName="hlChangeFunctionalManager"
                                                                                        NavigateUrl="javascript:void(0);" Text="Change">
                                                                                    </dx:ASPxHyperLink>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td style="padding-right: 5px; height: 5px; text-align: right" colspan="3"></td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </dx:PanelContent>
                                                            </PanelCollection>
                                                        </dx:ASPxPanel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="templateCaption" colspan="5" style="text-align: center">
                                                    <dx:ASPxPanel ID="panelSqExemption" runat="server" Width="530px" Visible="true" Enabled="true">
                                                        <PanelCollection>
                                                            <dx:PanelContent ID="PanelContent5" runat="server">
                                                                <div style="padding-bottom: 2px;">
                                                                    <strong><span style="font-size: 11pt">Authority to Access Site (Safety Qualification
                                                                        Exemption)</span></strong>
                                                                  </div>
                                                                <div align="center">
                                                                    <%--<asp:PlaceHolder ID="phSqExemption" runat="server"></asp:PlaceHolder>--%>
                                                                    <dx:ASPxGridView ID="gvSqExemption" runat="server" AutoGenerateColumns="False" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                        CssPostfix="Office2003Blue" DataSourceID="SqlDataSource1" KeyFieldName="SqExemptionId"
                                                                        Width="550px" OnHtmlRowCreated="gvSqExemption_HtmlRowCreated">
                                                                        <Columns>
                                                                            <dx:GridViewDataTextColumn Caption=" " FieldName="SqExemptionId" ReadOnly="True"
                                                                                VisibleIndex="1">
                                                                                <DataItemTemplate>
                                                                                    <dx:ASPxHyperLink ID="hlEdit" runat="server" Text="Edit">
                                                                                    </dx:ASPxHyperLink>
                                                                                </DataItemTemplate>
                                                                            </dx:GridViewDataTextColumn>
                                                                            <dx:GridViewDataTextColumn FieldName="CompanyId" Visible="False" VisibleIndex="2">
                                                                            </dx:GridViewDataTextColumn>
                                                                            <dx:GridViewDataTextColumn Caption="Site" FieldName="SiteName" VisibleIndex="3">
                                                                            </dx:GridViewDataTextColumn>
                                                                            <dx:GridViewDataDateColumn FieldName="DateApplied" VisibleIndex="4">
                                                                            </dx:GridViewDataDateColumn>
                                                                            <dx:GridViewDataDateColumn FieldName="ValidFrom" VisibleIndex="5">
                                                                            </dx:GridViewDataDateColumn>
                                                                            <dx:GridViewDataDateColumn FieldName="ValidTo" VisibleIndex="6" SortIndex="0" SortOrder="Descending">
                                                                            </dx:GridViewDataDateColumn>
                                                                            <dx:GridViewDataTextColumn Caption="Company Type" FieldName="CompanyStatusDesc" VisibleIndex="7">
                                                                            </dx:GridViewDataTextColumn>
                                                                            <dx:GridViewDataTextColumn Caption="Requested By" FieldName="RequestingCompanyName"
                                                                                VisibleIndex="8">
                                                                            </dx:GridViewDataTextColumn>
                                                                        </Columns>
                                                                        <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                                                            <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
                                                                            </LoadingPanelOnStatusBar>
                                                                            <LoadingPanel Url="~/App_Themes/Office2003Blue/GridView/Loading.gif">
                                                                            </LoadingPanel>
                                                                        </Images>
                                                                        <ImagesFilterControl>
                                                                            <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
                                                                            </LoadingPanel>
                                                                        </ImagesFilterControl>
                                                                        <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue">
                                                                            <Header ImageSpacing="5px" SortingImageSpacing="5px">
                                                                            </Header>
                                                                            <LoadingPanel ImageSpacing="10px">
                                                                            </LoadingPanel>
                                                                        </Styles>
                                                                        <StylesEditors>
                                                                            <ProgressBar Height="25px">
                                                                            </ProgressBar>
                                                                        </StylesEditors>
                                                                    </dx:ASPxGridView>
                                                                    <br />
                                                                    <dx:ASPxHyperLink ID="hlNew" runat="server" Text="Add Authority to access site">
                                                                    </dx:ASPxHyperLink>
                                                                    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
                                                                        SelectCommand="_SqExemption_Friendly_All_GetByQuestionnaireId" SelectCommandType="StoredProcedure">
                                                                        <SelectParameters>
                                                                            <asp:QueryStringParameter DefaultValue="0" Name="QuestionnaireId" QueryStringField="q"
                                                                                Type="Int32" />
                                                                        </SelectParameters>
                                                                    </asp:SqlDataSource>
                                                                </div>
                                                                <br />
                                                            </dx:PanelContent>
                                                        </PanelCollection>
                                                    </dx:ASPxPanel>
                                                </td>
                                            </tr>
                                        </table>
                                        <asp:Label ID="lblSave" runat="server" Font-Italic="True" ForeColor="Red"></asp:Label>
                                    </asp:Panel>

                                </dx:ContentControl>
                            </ContentCollection>
                        </dx:TabPage>
                        <dx:TabPage Name="HistoryLog" Text="History Log">
                            <ContentCollection>
                                <dx:ContentControl ID="ContentControl2" runat="server">
                                    <asp:Panel ID="panelHistoryLog" runat="server" Width="830px">
                                        <table>
                                            <tr>
                                                <td style="width: 830px; text-align: center" class="pageName" align="right">
                                                    <div align="left">
                                                        <dx:ASPxHyperLink ID="hlAddInternalComment" runat="server" ClientInstanceName="hlAddInternalComment"
                                                            NavigateUrl="javascript:void(0);" Text="Add Comment">
                                                        </dx:ASPxHyperLink>
                                                        <dx:ASPxPopupControl ID="popupAddInternalComment" runat="server" AllowDragging="True"
                                                            CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue"
                                                            HeaderText="Add Internal Comment" Modal="True" PopupElementID="hlAddInternalComment"
                                                            Width="520px" Font-Bold="False" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                            ShowHeader="False">
                                                            <SizeGripImage Height="16px" Width="16px" />
                                                            <ContentCollection>
                                                                <dx:PopupControlContentControl ID="popupContentAddInternalComment" runat="server"
                                                                    BackColor="#DDECFE">
                                                                    <dx:ASPxRoundPanel ID="rpAddInternalComment" runat="server" BackColor="#DDECFE"
                                                                        CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue"
                                                                        ShowHeader="false" EnableDefaultAppearance="False" HeaderText="Change Procurement Functional Supervisor"
                                                                        SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" Width="490px">
                                                                        <TopEdge>
                                                                            <BackgroundImage HorizontalPosition="left" ImageUrl="~/App_Themes/Office2003Blue/Web/rpTopEdge.png"
                                                                                Repeat="RepeatX" VerticalPosition="top" />
                                                                        </TopEdge>
                                                                        <ContentPaddings Padding="2px" PaddingBottom="10px" PaddingTop="10px" />
                                                                        <HeaderRightEdge>
                                                                            <BackgroundImage HorizontalPosition="left" ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png"
                                                                                Repeat="RepeatX" VerticalPosition="top" />
                                                                        </HeaderRightEdge>
                                                                        <Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px" />
                                                                        <HeaderLeftEdge>
                                                                            <BackgroundImage HorizontalPosition="left" ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png"
                                                                                Repeat="RepeatX" VerticalPosition="top" />
                                                                        </HeaderLeftEdge>
                                                                        <HeaderStyle BackColor="#7BA4E0">
                                                                            <Paddings Padding="0px" PaddingBottom="7px" PaddingLeft="2px" PaddingRight="2px" />
                                                                            <BorderBottom BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px" />
                                                                        </HeaderStyle>
                                                                        <HeaderContent>
                                                                            <BackgroundImage HorizontalPosition="left" ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png"
                                                                                Repeat="RepeatX" VerticalPosition="top" />
                                                                        </HeaderContent>
                                                                        <DisabledStyle ForeColor="Gray"></DisabledStyle>
                                                                        <NoHeaderTopEdge BackColor="#DDECFE"></NoHeaderTopEdge>
                                                                        <PanelCollection>
                                                                            <dx:PanelContent ID="PanelContent3" runat="server">
                                                                                <div align="center" style="padding-bottom: 1px">
                                                                                    <b>Comments to be added to Action Log:<span style="font-size: 13pt; color: maroon"><span style="color: maroon"><span style="font-size: 10pt; font-family: Verdana"></span></span></span></b>
                                                                                  </div>
                                                                                <dx:ASPxMemo ID="mbAddInternalComment" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                                    CssPostfix="Office2003Blue" Height="53px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                                                    Width="100%">
                                                                                    <ValidationSettings RequiredField-IsRequired="true"
                                                                                        ValidationGroup="AddInternalComment" RequiredField-ErrorText="Required Field!"
                                                                                        ErrorDisplayMode="ImageWithTooltip">
                                                                                        <RequiredField IsRequired="True" ErrorText="Required Field!"></RequiredField>
                                                                                    </ValidationSettings>
                                                                                </dx:ASPxMemo>
                                                                                <dx:ASPxLabel ID="lblAddInternalComment_Error" runat="server" Font-Bold="True" ForeColor="Red">
                                                                                </dx:ASPxLabel>
                                                                                <br />
                                                                                <div align="center">
                                                                                    <dx:ASPxButton ID="btnAddInternalComment" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                                        CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                                                        Text="Add Comment" Width="270px" OnClick="btnCAddInternalComment_Click"
                                                                                        ValidationGroup="AddInternalComment">
                                                                                        <ClientSideEvents Click="function (s, e) {e.processOnServer = confirm('Are you sure you want to add this comment? It cannot be edited or deleted after adding.');}" />
                                                                                    </dx:ASPxButton>
                                                                                </div>
                                                                            </dx:PanelContent>
                                                                        </PanelCollection>
                                                                    </dx:ASPxRoundPanel>
                                                                </dx:PopupControlContentControl>
                                                            </ContentCollection>
                                                            <CloseButtonImage Height="12px" Width="13px" />
                                                            <HeaderStyle>
                                                                <Paddings PaddingRight="6px" />
                                                            </HeaderStyle>
                                                        </dx:ASPxPopupControl>
                                                    </div>
                                                    <div align="right">
                                                        <a href="javascript:window.print();">Print</a>&nbsp;|
                                                          <a style="color: #0000ff; text-align: right; text-decoration: none" href="javascript:ShowHideCustomizationWindow()">
                                                            Customize</a>
                                                      </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div align="left" style="text-align: left">
                                                        <dx:ASPxGridView ID="grid" runat="server" ClientInstanceName="grid"
                                                            CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" DataSourceID="odsActionLog"
                                                            CssPostfix="Office2003Blue" Width="820px" AutoGenerateColumns="False"
                                                            OnHtmlRowCreated="gridHistoryLog_RowCreated">
                                                            <Columns>
                                                                <dx:GridViewDataDateColumn Caption="Date Time" FieldName="CreatedDate" VisibleIndex="0" SortIndex="0" SortOrder="Descending" Width="130px">
                                                                    <CellStyle HorizontalAlign="Left" />
                                                                    <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy HH:mm:ss" />
                                                                </dx:GridViewDataDateColumn>
                                                                <dx:GridViewDataTextColumn Caption="Action (Detailed)" FieldName="HistoryLog" VisibleIndex="1"
                                                                    Width="670px">
                                                                    <DataItemTemplate>
                                                                        <asp:Label ID="lblActionDetailed" runat="server" Text=""></asp:Label>
                                                                    </DataItemTemplate>
                                                                    <CellStyle HorizontalAlign="Left" />
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn Caption="User" FieldName="FullName" Visible="false">
                                                                    <CellStyle HorizontalAlign="Left" />
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn Caption="User Role" FieldName="Role" Visible="false">
                                                                    <CellStyle HorizontalAlign="Left" />
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn Caption="User Privledged Role" FieldName="CreatedByPrivledgedRole" Visible="false">
                                                                    <CellStyle HorizontalAlign="Left" />
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn Caption="User Company" FieldName="CompanyName" Visible="false">
                                                                    <CellStyle HorizontalAlign="Left" />
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn Caption="Action" FieldName="ActionDescription" Visible="false">
                                                                    <CellStyle HorizontalAlign="Left" />
                                                                </dx:GridViewDataTextColumn>
                                                            </Columns>
                                                            <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                                                <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003BlueOld/GridView/gvLoadingOnStatusBar.gif"><%--<Enhancement_023 changes>--%>
                                                                </LoadingPanelOnStatusBar>
                                                                <LoadingPanel Url="~/App_Themes/Office2003BlueOld/GridView/Loading.gif"><%--<Enhancement_023 changes>--%>
                                                                </LoadingPanel>
                                                            </Images>
                                                            <ImagesFilterControl>
                                                                <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
                                                                </LoadingPanel>
                                                            </ImagesFilterControl>
                                                            <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                CssPostfix="Office2003Blue">
                                                                <LoadingPanel ImageSpacing="10px">
                                                                </LoadingPanel>
                                                                <Header ImageSpacing="5px" SortingImageSpacing="5px">
                                                                </Header>
                                                            </Styles>
                                                            <StylesEditors>
                                                                <ProgressBar Height="25px">
                                                                </ProgressBar>
                                                            </StylesEditors>
                                                            <SettingsCustomizationWindow Enabled="True"></SettingsCustomizationWindow>
                                                            <Settings ShowHeaderFilterButton="True" ShowHorizontalScrollBar="true" ShowVerticalScrollBar="true" VerticalScrollableHeight="400"></Settings>
                                                            <SettingsBehavior ColumnResizeMode="NextColumn" />
                                                        </dx:ASPxGridView>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: right">
                                                    <div align="right">
                                                        <uc1:ExportButtons ID="ucExportButtons" runat="server"></uc1:ExportButtons>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </dx:ContentControl>
                            </ContentCollection>
                        </dx:TabPage>
                        <%-- Jolly - Spec- 33 --%>
                        <dx:TabPage Name="CompanyNotes" Text="Company Notes">
                            <ContentCollection>
                                <dx:ContentControl ID="ContentControl3" runat="server">
                                    <asp:Panel ID="panelCompanyNotes" runat="server" Width="830px">
                                        <table>
                                            <tr>
                                                <td style="width: 830px; text-align: center" class="pageName" align="right">
                                                    <div align="left">
                                                        <dx:ASPxHyperLink ID="hlAddInternalNote" runat="server" ClientInstanceName="hlAddInternalNote"
                                                            NavigateUrl="javascript:void(0);" Text="Add Note">
                                                        </dx:ASPxHyperLink>
                                                        <dx:ASPxPopupControl ID="popupAddInternalNote" runat="server" AllowDragging="True"
                                                            CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue"
                                                            HeaderText="Add Internal Note" Modal="True" PopupElementID="hlAddInternalNote"
                                                            Width="520px" Font-Bold="False" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                            ShowHeader="False">
                                                            <SizeGripImage Height="16px" Width="16px" />
                                                            <ContentCollection>
                                                                <dx:PopupControlContentControl ID="popupContentAddInternalNote" runat="server"
                                                                    BackColor="#DDECFE">
                                                                    <dx:ASPxRoundPanel ID="rpAddInternalNote" runat="server" BackColor="#DDECFE"
                                                                        CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue"
                                                                        ShowHeader="false" EnableDefaultAppearance="False" HeaderText="Change Procurement Functional Supervisor"
                                                                        SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" Width="490px">
                                                                        <TopEdge>
                                                                            <BackgroundImage HorizontalPosition="left" ImageUrl="~/App_Themes/Office2003Blue/Web/rpTopEdge.png"
                                                                                Repeat="RepeatX" VerticalPosition="top" />
                                                                        </TopEdge>
                                                                        <ContentPaddings Padding="2px" PaddingBottom="10px" PaddingTop="10px" />
                                                                        <HeaderRightEdge>
                                                                            <BackgroundImage HorizontalPosition="left" ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png"
                                                                                Repeat="RepeatX" VerticalPosition="top" />
                                                                        </HeaderRightEdge>
                                                                        <Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px" />
                                                                        <HeaderLeftEdge>
                                                                            <BackgroundImage HorizontalPosition="left" ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png"
                                                                                Repeat="RepeatX" VerticalPosition="top" />
                                                                        </HeaderLeftEdge>
                                                                        <HeaderStyle BackColor="#7BA4E0">
                                                                            <Paddings Padding="0px" PaddingBottom="7px" PaddingLeft="2px" PaddingRight="2px" />
                                                                            <BorderBottom BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px" />
                                                                        </HeaderStyle>
                                                                        <HeaderContent>
                                                                            <BackgroundImage HorizontalPosition="left" ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png"
                                                                                Repeat="RepeatX" VerticalPosition="top" />
                                                                        </HeaderContent>
                                                                        <DisabledStyle ForeColor="Gray"></DisabledStyle>
                                                                        <NoHeaderTopEdge BackColor="#DDECFE"></NoHeaderTopEdge>
                                                                        <PanelCollection>
                                                                            <dx:PanelContent ID="PanelContent6" runat="server">
                                                                                <div align="center" style="padding-bottom: 1px">
                                                                                    <b>Note to be added to Company Notes:<span style="font-size: 13pt; color: maroon"><span style="color: maroon"><span style="font-size: 10pt; font-family: Verdana"></span></span></span></b>
                                                                                  </div>
                                                                                <dx:ASPxMemo ID="mbAddInternalNote" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                                    CssPostfix="Office2003Blue" Height="53px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                                                    Width="100%">
                                                                                    <ValidationSettings RequiredField-IsRequired="true"
                                                                                        ValidationGroup="AddInternalNote" RequiredField-ErrorText="Required Field!"
                                                                                        ErrorDisplayMode="ImageWithTooltip">
                                                                                        <RequiredField IsRequired="True" ErrorText="Required Field!"></RequiredField>
                                                                                    </ValidationSettings>
                                                                                </dx:ASPxMemo>
                                                                                <dx:ASPxLabel ID="lblAddInternalNote_Error" runat="server" Font-Bold="True" ForeColor="Red">
                                                                                </dx:ASPxLabel>
                                                                                <br />
                                                                                <div align="center">
                                                                                    <dx:ASPxButton ID="btnAddInternalNote" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                                        CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                                                        Text="Add Note" Width="270px" OnClick="btnCAddInternalNote_Click"
                                                                                        ValidationGroup="AddInternalNote">
                                                                                        <ClientSideEvents Click="function (s, e) {e.processOnServer = confirm('Are you sure you want to add this note? It cannot be edited or deleted after adding.');}" />
                                                                                    </dx:ASPxButton>
                                                                                </div>
                                                                            </dx:PanelContent>
                                                                        </PanelCollection>
                                                                    </dx:ASPxRoundPanel>
                                                                </dx:PopupControlContentControl>
                                                            </ContentCollection>
                                                            <CloseButtonImage Height="12px" Width="13px" />
                                                            <HeaderStyle>
                                                                <Paddings PaddingRight="6px" />
                                                            </HeaderStyle>
                                                        </dx:ASPxPopupControl>
                                                    </div>
                                                    <div align="right">
                                                        <a href="javascript:window.print();">Print</a>&nbsp;|<a style="color: #0000ff; text-align: right; text-decoration: none" href="javascript:ShowHideCustomizationWindow()">
                                              Customize</a>
                                                      </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div align="left" style="text-align: left">
                                                        <dx:ASPxGridView ID="gridCompanyNote" runat="server" ClientInstanceName="grid"
                                                            CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" DataSourceID="odsActionNote"
                                                            CssPostfix="Office2003Blue" Width="820px" AutoGenerateColumns="False"
                                                            OnHtmlRowCreated="gridCompanyNote_RowCreated">
                                                            <Columns>
                                                                <dx:GridViewDataDateColumn Caption="Date Time" FieldName="CreatedDate" VisibleIndex="0" SortIndex="0" SortOrder="Descending" Width="130px">
                                                                    <CellStyle HorizontalAlign="Left" />
                                                                    <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy HH:mm:ss" />
                                                                </dx:GridViewDataDateColumn>
                                                                <dx:GridViewDataTextColumn Caption="Notes" FieldName="ActionNote" VisibleIndex="1"
                                                                    Width="470px">
                                                                    <DataItemTemplate>
                                                                        <asp:Label ID="lblActionNote" runat="server" Text=""></asp:Label>
                                                                    </DataItemTemplate>
                                                                    <CellStyle HorizontalAlign="Left" />
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn Caption="Added By" FieldName="FullName" VisibleIndex="2">
                                                                    <CellStyle HorizontalAlign="Left" />
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn Caption="Added By Id" FieldName="CreatedByUserId" Visible="false">
                                                                    <CellStyle HorizontalAlign="Left" />
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn Caption="Visibility" FieldName="Visibility" VisibleIndex="3">
                                                                    <CellStyle HorizontalAlign="Left" />
                                                                </dx:GridViewDataTextColumn>
                                                            </Columns>
                                                            <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                                                <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003BlueOld/GridView/gvLoadingOnStatusBar.gif"><%--<Enhancement_023 changes>--%>
                                                                </LoadingPanelOnStatusBar>
                                                                <LoadingPanel Url="~/App_Themes/Office2003BlueOld/GridView/Loading.gif"><%--<Enhancement_023 changes>--%>
                                                                </LoadingPanel>
                                                            </Images>
                                                            <ImagesFilterControl>
                                                                <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
                                                                </LoadingPanel>
                                                            </ImagesFilterControl>
                                                            <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                CssPostfix="Office2003Blue">
                                                                <LoadingPanel ImageSpacing="10px">
                                                                </LoadingPanel>
                                                                <Header ImageSpacing="5px" SortingImageSpacing="5px">
                                                                </Header>
                                                            </Styles>
                                                            <StylesEditors>
                                                                <ProgressBar Height="25px">
                                                                </ProgressBar>
                                                            </StylesEditors>
                                                            <SettingsCustomizationWindow Enabled="True"></SettingsCustomizationWindow>
                                                            <Settings ShowHeaderFilterButton="True" ShowHorizontalScrollBar="true" ShowVerticalScrollBar="true" VerticalScrollableHeight="400"></Settings>
                                                            <SettingsBehavior ColumnResizeMode="NextColumn" />
                                                        </dx:ASPxGridView>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: right">
                                                    <div align="right">
                                                        <uc1:ExportButtons ID="ExportButtons1" runat="server"></uc1:ExportButtons>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </dx:ContentControl>
                            </ContentCollection>
                        </dx:TabPage>
                        <%-- Jolly - End of Spec- 33 --%>
                    </TabPages>
                    <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif"></LoadingPanelImage>
                    <LoadingPanelStyle ImageSpacing="6px"></LoadingPanelStyle>
                    <ContentStyle>
                        <Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px" />
                    </ContentStyle>
                </dx:ASPxPageControl>
            </div>
            <data:EntityDataSource ID="UsersEhsConsultantsDataSource2" runat="server" EntityKeyTypeName="System.Int32"
                EntityTypeName="KaiZen.CSMS.UsersEhsConsultants, KaiZen.CSMS" Filter="Enabled = True"
                ProviderName="UsersEhsConsultantsProvider" SelectMethod="GetAll" Sort="UserFullNameLogon ASC">
            </data:EntityDataSource>
            <asp:Label ID="Label5" runat="server" Font-Bold="False" ForeColor="Maroon" Text="No Safety Qualification Procurement Questionnaire exists, Please contact Alcoa's relevant Procurement representative."
                Visible="False" Font-Size="12pt" Width="500px"></asp:Label>
            <br />
            <br />
            <dx:ASPxPopupControl ID="popupProcurementContact" runat="server" AllowDragging="True"
                CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue"
                HeaderText="Change Procurement Contact" Modal="True" PopupElementID="hlChangeProc"
                Width="520px" Font-Bold="False" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                ShowHeader="False">
                <SizeGripImage Height="16px" Width="16px" />
                <ContentCollection>
                    <dx:PopupControlContentControl ID="popupContentProcurementContact" runat="server"
                        BackColor="#DDECFE">
                        <dx:ASPxRoundPanel ID="rpChangeProcurementContact" runat="server" BackColor="#DDECFE"
                            CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue"
                            ShowHeader="false" EnableDefaultAppearance="False" HeaderText="Change Procurement Contact"
                            SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" Width="490px">
                            <TopEdge>
                                <BackgroundImage HorizontalPosition="left" ImageUrl="~/App_Themes/Office2003Blue/Web/rpTopEdge.png"
                                    Repeat="RepeatX" VerticalPosition="top" />
                            </TopEdge>
                            <ContentPaddings Padding="2px" PaddingBottom="10px" PaddingTop="10px" />
                            <HeaderRightEdge>
                                <BackgroundImage HorizontalPosition="left" ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png"
                                    Repeat="RepeatX" VerticalPosition="top" />
                            </HeaderRightEdge>
                            <Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px" />
                            <HeaderLeftEdge>
                                <BackgroundImage HorizontalPosition="left" ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png"
                                    Repeat="RepeatX" VerticalPosition="top" />
                            </HeaderLeftEdge>
                            <HeaderStyle BackColor="#7BA4E0">
                                <Paddings Padding="0px" PaddingBottom="7px" PaddingLeft="2px" PaddingRight="2px" />
                                <BorderBottom BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px" />
                            </HeaderStyle>
                            <HeaderContent>
                                <BackgroundImage HorizontalPosition="left" ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png"
                                    Repeat="RepeatX" VerticalPosition="top" />
                            </HeaderContent>
                            <DisabledStyle ForeColor="Gray">
                            </DisabledStyle>
                            <NoHeaderTopEdge BackColor="#DDECFE">
                            </NoHeaderTopEdge>
                            <PanelCollection>
                                <dx:PanelContent runat="server">
                                    <table width="490px">
                                        <tr>
                                            <td class="style1" style="text-align: right;">
                                                <b>Current Procurement Contact: </b>
                                            </td>
                                            <td style="text-align: left">
                                                <dx:ASPxLabel ID="lblProcContact" runat="server" Text="(Not Assigned)">
                                                </dx:ASPxLabel>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="style2" style="text-align: right;">New Procurement Contact:
                                              </td>
                                            <td style="text-align: left">
                                                <dx:ASPxComboBox runat="server" IncrementalFilteringMode="StartsWith" ValueType="System.Int32"
                                                    DataSourceID="UsersProcurementListDataSource2" TextField="UserFullName" ValueField="UserId"
                                                    Width="240px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                    CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                    ID="cbProcurement" Enabled="true">
                                                    <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                                                    </LoadingPanelImage>
                                                    <ButtonStyle Width="13px">
                                                    </ButtonStyle>
                                                    <ValidationSettings Display="Dynamic" ValidationGroup="ChangeProc">
                                                        <RequiredField IsRequired="True" />
                                                    </ValidationSettings>
                                                </dx:ASPxComboBox>
                                            </td>
                                        </tr>
                                    </table>
                                    <br />
                                    <div align="center" style="padding-bottom: 1px">
                                        <b>Additional Comments (for New Procurement Contact E-Mail)<span style="font-size: 13pt; color: maroon"><span style="color: maroon"><span style="font-size: 10pt; font-family: Verdana"></span></span></span></b>
                                      </div>
                                    <dx:ASPxMemo ID="mbProcComments" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                        CssPostfix="Office2003Blue" Height="53px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                        Width="100%">
                                        <ValidationSettings Display="Dynamic" ValidationGroup="ChangeProc">
                                            <RequiredField IsRequired="True" />
                                        </ValidationSettings>
                                    </dx:ASPxMemo>
                                    <dx:ASPxCheckBox ID="cbProcEmail" runat="server" Checked="True" Text="Notify New Procurement Contact via E-Mail (Optional)">
                                    </dx:ASPxCheckBox>
                                    <dx:ASPxLabel ID="lblChangeProcError" runat="server" Font-Bold="True" ForeColor="Red">
                                    </dx:ASPxLabel>
                                    <br />
                                    <div align="center">
                                        <dx:ASPxButton ID="btnChangeProc" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                            CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                            Text="Change Procurement Contact" Width="236px" OnClick="btnChangeProc_Click"
                                            ValidationGroup="ChangeProc">
                                            <ClientSideEvents Click="function (s, e) {e.processOnServer = confirm('Are you sure you want to change the current procurement contact?');}" />
                                        </dx:ASPxButton>
                                    </div>
                                </dx:PanelContent>
                            </PanelCollection>
                        </dx:ASPxRoundPanel>
                    </dx:PopupControlContentControl>
                </ContentCollection>
                <CloseButtonImage Height="12px" Width="13px" />
                <HeaderStyle>
                    <Paddings PaddingRight="6px" />
                </HeaderStyle>
            </dx:ASPxPopupControl>
            <dx:ASPxPopupControl ID="popupFunctionalManager" runat="server" AllowDragging="True"
                CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue"
                HeaderText="Change Procurement Contact" Modal="True" PopupElementID="hlChangeFunctionalManager"
                Width="520px" Font-Bold="False" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                ShowHeader="False">
                <SizeGripImage Height="16px" Width="16px" />
                <ContentCollection>
                    <dx:PopupControlContentControl ID="popupContentFunctionalManager" runat="server"
                        BackColor="#DDECFE">
                        <dx:ASPxRoundPanel ID="rpChangeFunctionalManager" runat="server" BackColor="#DDECFE"
                            CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue"
                            ShowHeader="false" EnableDefaultAppearance="False" HeaderText="Change Procurement Functional Supervisor"
                            SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" Width="490px">
                            <TopEdge>
                                <BackgroundImage HorizontalPosition="left" ImageUrl="~/App_Themes/Office2003Blue/Web/rpTopEdge.png"
                                    Repeat="RepeatX" VerticalPosition="top" />
                            </TopEdge>
                            <ContentPaddings Padding="2px" PaddingBottom="10px" PaddingTop="10px" />
                            <HeaderRightEdge>
                                <BackgroundImage HorizontalPosition="left" ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png"
                                    Repeat="RepeatX" VerticalPosition="top" />
                            </HeaderRightEdge>
                            <Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px" />
                            <HeaderLeftEdge>
                                <BackgroundImage HorizontalPosition="left" ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png"
                                    Repeat="RepeatX" VerticalPosition="top" />
                            </HeaderLeftEdge>
                            <HeaderStyle BackColor="#7BA4E0">
                                <Paddings Padding="0px" PaddingBottom="7px" PaddingLeft="2px" PaddingRight="2px" />
                                <BorderBottom BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px" />
                            </HeaderStyle>
                            <HeaderContent>
                                <BackgroundImage HorizontalPosition="left" ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png"
                                    Repeat="RepeatX" VerticalPosition="top" />
                            </HeaderContent>
                            <DisabledStyle ForeColor="Gray">
                            </DisabledStyle>
                            <NoHeaderTopEdge BackColor="#DDECFE">
                            </NoHeaderTopEdge>
                            <PanelCollection>
                                <dx:PanelContent ID="PanelContent1" runat="server">
                                    <table width="490px">
                                        <tr>
                                            <td class="style1" style="text-align: right;">
                                                <b>Current Procurement Functional Supervisor: </b>
                                            </td>
                                            <td style="text-align: left">
                                                <dx:ASPxLabel ID="lblFunctionalManager_Current" runat="server" Text="(Not Assigned)">
                                                </dx:ASPxLabel>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="style2" style="text-align: right;">New Procurement Functional Supervisor:
                                              </td>
                                            <td style="text-align: left">
                                                <dx:ASPxComboBox runat="server" IncrementalFilteringMode="StartsWith" ValueType="System.Int32"
                                                    DataSourceID="UsersProcurementListDataSource2" TextField="UserFullName" ValueField="UserId"
                                                    Width="240px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                    CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                    ID="cbFunctionalManager_New" Enabled="true">
                                                    <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                                                    </LoadingPanelImage>
                                                    <ButtonStyle Width="13px">
                                                    </ButtonStyle>
                                                    <ValidationSettings Display="Dynamic" ValidationGroup="ChangeFunctionalManager">
                                                        <RequiredField IsRequired="True" />
                                                    </ValidationSettings>
                                                </dx:ASPxComboBox>
                                            </td>
                                        </tr>
                                    </table>
                                    <br />
                                    <div align="center" style="padding-bottom: 1px">
                                        <b>Additional Comments (for New Procurement Functional Supervisor E-Mail)<span style="font-size: 13pt; color: maroon"><span style="color: maroon"><span style="font-size: 10pt; font-family: Verdana"></span></span></span></b>
                                      </div>
                                    <dx:ASPxMemo ID="mbFunctionalManager_AdditionalComments" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                        CssPostfix="Office2003Blue" Height="53px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                        Width="100%">
                                        <ValidationSettings Display="Dynamic" ValidationGroup="ChangeFunctionalManager">
                                            <RequiredField IsRequired="True" />
                                        </ValidationSettings>
                                    </dx:ASPxMemo>
                                    <dx:ASPxCheckBox ID="cbFunctionalManager_Notify" runat="server" Checked="True" Text="Notify New Procurement Functional Supervisor via E-Mail (Optional)">
                                    </dx:ASPxCheckBox>
                                    <dx:ASPxLabel ID="lblFunctionalManager_Error" runat="server" Font-Bold="True" ForeColor="Red">
                                    </dx:ASPxLabel>
                                    <br />
                                    <div align="center">
                                        <dx:ASPxButton ID="btnChangeFunctionalManager" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                            CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                            Text="Change Procurement Functional Supervisor" Width="270px" OnClick="btnChangeFunctionalManager_Click"
                                            ValidationGroup="ChangeFunctionalManager">
                                            <ClientSideEvents Click="function (s, e) {e.processOnServer = confirm('Are you sure you want to change the current Procurement Runctional Supervisor?');}" />
                                        </dx:ASPxButton>
                                    </div>
                                </dx:PanelContent>
                            </PanelCollection>
                        </dx:ASPxRoundPanel>
                    </dx:PopupControlContentControl>
                </ContentCollection>
                <CloseButtonImage Height="12px" Width="13px" />
                <HeaderStyle>
                    <Paddings PaddingRight="6px" />
                </HeaderStyle>
            </dx:ASPxPopupControl>
            <dx:ASPxPopupControl ID="popupHSAssessor" runat="server" AllowDragging="True" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                CssPostfix="Office2003Blue" HeaderText="Change H&S Assessor" Modal="True" PopupElementID="hlChangeHSAssessor"
                Width="520px" Font-Bold="False" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                ShowHeader="False">
                <SizeGripImage Height="16px" Width="16px" />
                <ContentCollection>
                    <dx:PopupControlContentControl ID="popupContentChangeHSAssessor" runat="server" BackColor="#DDECFE">
                        <dx:ASPxRoundPanel ID="rpChangeHSAssessor" runat="server" BackColor="#DDECFE" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                            CssPostfix="Office2003Blue" ShowHeader="false" EnableDefaultAppearance="False"
                            HeaderText="Change Procurement Contact" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                            Width="490px">
                            <TopEdge>
                                <BackgroundImage HorizontalPosition="left" ImageUrl="~/App_Themes/Office2003Blue/Web/rpTopEdge.png"
                                    Repeat="RepeatX" VerticalPosition="top" />
                            </TopEdge>
                            <ContentPaddings Padding="2px" PaddingBottom="10px" PaddingTop="10px" />
                            <HeaderRightEdge>
                                <BackgroundImage HorizontalPosition="left" ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png"
                                    Repeat="RepeatX" VerticalPosition="top" />
                            </HeaderRightEdge>
                            <Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px" />
                            <HeaderLeftEdge>
                                <BackgroundImage HorizontalPosition="left" ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png"
                                    Repeat="RepeatX" VerticalPosition="top" />
                            </HeaderLeftEdge>
                            <HeaderStyle BackColor="#7BA4E0">
                                <Paddings Padding="0px" PaddingBottom="7px" PaddingLeft="2px" PaddingRight="2px" />
                                <BorderBottom BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px" />
                            </HeaderStyle>
                            <HeaderContent>
                                <BackgroundImage HorizontalPosition="left" ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png"
                                    Repeat="RepeatX" VerticalPosition="top" />
                            </HeaderContent>
                            <DisabledStyle ForeColor="Gray">
                            </DisabledStyle>
                            <NoHeaderTopEdge BackColor="#DDECFE">
                            </NoHeaderTopEdge>
                            <PanelCollection>
                                <dx:PanelContent ID="PanelContent2" runat="server">
                                    <table width="490px">
                                        <tr>
                                            <td class="style1" style="text-align: right;">
                                                <b>Current H&S Assessor: </b>
                                            </td>
                                            <td style="text-align: left">
                                                <dx:ASPxLabel ID="lblHSAssessor_Current" runat="server" Text="(Not Assigned)">
                                                </dx:ASPxLabel>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="style2" style="text-align: right;">New H&S Assessor:
                                              </td>
                                            <td style="text-align: left">
                                                <dx:ASPxComboBox runat="server" IncrementalFilteringMode="StartsWith" ValueType="System.Int32"
                                                    DataSourceID="UsersEhsConsultantsDataSource2" TextField="UserFullName" ValueField="EhsConsultantId"
                                                    Width="240px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                    CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                    ID="cbHSAssessor_New" Enabled="true">
                                                    <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                                                    </LoadingPanelImage>
                                                    <ButtonStyle Width="13px">
                                                    </ButtonStyle>
                                                    <ValidationSettings Display="Dynamic" ValidationGroup="ChangeHSAssessor">
                                                        <RequiredField IsRequired="True" />
                                                    </ValidationSettings>
                                                </dx:ASPxComboBox>
                                            </td>
                                        </tr>
                                    </table>
                                    <br />
                                    <div align="center" style="padding-bottom: 1px">
                                        <b>Additional Comments (for New H&amp;S Assessor E-Mail)<span style="font-size: 13pt; color: maroon"><span style="color: maroon"><span style="font-size: 10pt; font-family: Verdana"></span></span></span></b>
                                      </div>
                                    <dx:ASPxMemo ID="mbHSAssessor_AdditionalComments" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                        CssPostfix="Office2003Blue" Height="53px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                        Width="100%">
                                        <ValidationSettings Display="Dynamic" ValidationGroup="ChangeHSAssessor">
                                            <RequiredField IsRequired="True" />
                                        </ValidationSettings>
                                    </dx:ASPxMemo>
                                    <dx:ASPxCheckBox ID="cbHSAssessor_Notify" runat="server" Checked="True" Text="Notify New H&S Assessor via E-Mail (Optional)">
                                    </dx:ASPxCheckBox>
                                    <dx:ASPxLabel ID="lblHSAssessor_Error" runat="server" Font-Bold="True" ForeColor="Red">
                                    </dx:ASPxLabel>
                                    <br />
                                    <div align="center">
                                        <dx:ASPxButton ID="btnChangeHSAssessor" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                            CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                            Text="Change H&S Assessor Contact" Width="236px" OnClick="btnChangeHSAssessor_Click"
                                            ValidationGroup="ChangeHSAssessor">
                                            <ClientSideEvents Click="function (s, e) {e.processOnServer = confirm('Are you sure you want to change the current H&S Assessor?');}" />
                                        </dx:ASPxButton>
                                    </div>
                                </dx:PanelContent>
                            </PanelCollection>
                        </dx:ASPxRoundPanel>
                    </dx:PopupControlContentControl>
                </ContentCollection>
                <CloseButtonImage Height="12px" Width="13px" />
                <HeaderStyle>
                    <Paddings PaddingRight="6px" />
                </HeaderStyle>
            </dx:ASPxPopupControl>
            <dx:ASPxPopupControl ID="ASPxPopupControl1" runat="server" AllowDragging="True" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                CssPostfix="Office2003Blue" EnableHotTrack="False" HeaderText="Administrative Controls"
                Modal="True" PopupElementID="hlAdmin" Width="240px" Font-Bold="False" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                <SizeGripImage Height="16px" Width="16px" />
                <ContentCollection>
                    <dx:PopupControlContentControl runat="server">
                        <table border="0" cellpadding="0" cellspacing="0" width="200">
                            <tr>
                                <!--Wrap the Text of button ->Added by Debashis for deployment -->
                                <td style="text-align: center">
                                    <dx:ASPxButton ID="btnDelete" runat="server" Enabled="False" Text="Delete this questionnaire"
                                        ClientInstanceName="btnDelete" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
                                        CssPostfix="Office2003Blue" OnClick="btnDelete_Click" Width="220px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                        <ClientSideEvents Click="function (s, e) {e.processOnServer = confirm('Are you sure you want to delete this questionnaire?');}" />
                                    </dx:ASPxButton>
                                    <br />
                                    <br />
                                   <%--<dx:ASPxButton ID="btnForceReQualify" runat="server" Enabled="False" Text="Forcibly Re-Qualify questionnaire"
                                        ClientInstanceName="btnRequalify" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
                                        CssPostfix="Office2003Blue" OnClick="btnReQualify_Click" Width="220px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                        <ClientSideEvents Click="function (s, e) {e.processOnServer = confirm('Are you sure you want to forcibly re-qualify this questionnaire?');}" />
                                    </dx:ASPxButton>--%>

                                    <dx:ASPxButton ID="btnForceReQualify" runat="server" Enabled="True" Text="Forcibly Re-Qualify questionnaire"
                                        CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssClass="wrap" Wrap="True" HorizontalAlign="Left"
                                        CssPostfix="Office2003Blue" Width="220px"
                                        Visible="True" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" ClientInstanceName="btnForceReQualify" AutoPostBack="false">
                                    </dx:ASPxButton>


                                    <%--DT374 --%>

                                     <dx:ASPxPopupControl ID="popupForciblyRequireReQualification" runat="server" AllowDragging="True"
                                                        CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue"
                                                        HeaderText="Forcibly Require Requalification" Modal="True" PopupElementID="btnForceRequalify"
                                                        Width="200px" Font-Bold="False" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                        ShowHeader="False">
                                                        <ClientSideEvents PopUp="function(s){s.UpdatePosition()}" />
                                                        <SizeGripImage Height="16px" Width="16px" />
                                        <ContentCollection>
                                            <dx:PopupControlContentControl ID="popupContentForciblyRequireRequalify" runat="server" BackColor="#DDECFE">

                                                <dx:ASPxRoundPanel ID="ASPxRoundPanel2" runat="server" BackColor="#DDECFE"
                                                                    CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue"
                                                                    ShowHeader="false" EnableDefaultAppearance="False" HeaderText="Forcibly Require Requalify"
                                                                    SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" Width="490px">
                                                    <TopEdge>
                                                        <BackgroundImage HorizontalPosition="left" ImageUrl="~/App_Themes/Office2003Blue/Web/rpTopEdge.png"
                                                            Repeat="RepeatX" VerticalPosition="top" />
                                                    </TopEdge>
                                                    <ContentPaddings Padding="2px" PaddingBottom="10px" PaddingTop="10px" />
                                                    <HeaderRightEdge>
                                                        <BackgroundImage HorizontalPosition="left" ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png"
                                                            Repeat="RepeatX" VerticalPosition="top" />
                                                    </HeaderRightEdge>
                                                    <Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px" />
                                                    <HeaderLeftEdge>
                                                        <BackgroundImage HorizontalPosition="left" ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png"
                                                            Repeat="RepeatX" VerticalPosition="top" />
                                                    </HeaderLeftEdge>
                                                    <HeaderStyle BackColor="#7BA4E0">
                                                        <Paddings Padding="0px" PaddingBottom="7px" PaddingLeft="2px" PaddingRight="2px" />
                                                        <BorderBottom BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px" />
                                                    </HeaderStyle>
                                                    <HeaderContent>
                                                        <BackgroundImage HorizontalPosition="left" ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png"
                                                            Repeat="RepeatX" VerticalPosition="top" />
                                                    </HeaderContent>
                                                    <DisabledStyle ForeColor="Gray">
                                                    </DisabledStyle>
                                                    <NoHeaderTopEdge BackColor="#DDECFE">
                                                    </NoHeaderTopEdge>
                                                    <PanelCollection>
                                                        <dx:PanelContent ID="PanelContent9" runat="server">
                                                            <span style="color: black; font-weight:bold;">Reason for forcibly requiring Re-Qualify Questionnaire (Recorded in History Log)</span>
                                                            <dx:ASPxMemo ID="mbForciblyRequireRequalify" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                                CssPostfix="Office2003Blue" Height="53px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                                                Width="100%">
                                                                <ValidationSettings Display="Dynamic" ValidationGroup="ForciblyRequireRequalify">
                                                                    <RequiredField IsRequired="True" />
                                                                </ValidationSettings>
                                                            </dx:ASPxMemo>
                                                            <dx:ASPxLabel ID="ASPxLabel1" runat="server" Font-Bold="True" ForeColor="Red">
                                                            </dx:ASPxLabel>
                                                            <br />
                                                            <div align="center">
                                                                <dx:ASPxButton ID="btnForciblyRequireRequalify" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                                                                CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                                                                                Text="Force Requalify" Width="236px" 
                                                                                                                ValidationGroup="ForciblyRequireRequalify" OnClick="btnForciblyRequireRequalify_Click">
                                                                </dx:ASPxButton>
                                                            </div>
                                                        </dx:PanelContent>
                                                    </PanelCollection>
                                                </dx:ASPxRoundPanel>
				                            </dx:PopupControlContentControl>
                                        </ContentCollection>
                                        <CloseButtonImage Height="12px" Width="13px" />
                                        <HeaderStyle>
                                            <Paddings PaddingRight="6px" />
                                        </HeaderStyle>
                                    </dx:ASPxPopupControl>
                                    <%--End DT374 --%>
                                    <br />
                                    <br />
                                    <dx:ASPxButton ID="btnForceReQualify2" runat="server" Enabled="False" Text="Forcibly Re-Qualify questionnaire as Direct Contractor"
                                        ClientInstanceName="btnRequalify2" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssClass="wrap" Wrap="True" HorizontalAlign="Left"
                                        CssPostfix="Office2003Blue" OnClick="btnReQualify2_Click" Width="220px" Visible="False"
                                        SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                        <ClientSideEvents Click="function (s, e) {e.processOnServer = confirm('Are you sure you want to forcibly re-qualify this questionnaire as a Direct Contractor?');}" />
                                    </dx:ASPxButton>
                                    <br />
                                    <br />
                                    <dx:ASPxButton ID="btnForceReQualify3" runat="server" Enabled="False" Text="Forcibly Re-Qualify questionnaire as Sub Contractor"
                                        ClientInstanceName="btnRequalify3" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssClass="wrap" Wrap="True" HorizontalAlign="Left"
                                        CssPostfix="Office2003Blue" OnClick="btnReQualify3_Click" Width="220px" Visible="False"
                                        SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                        <ClientSideEvents Click="function (s, e) {e.processOnServer = confirm('Are you sure you want to forcibly re-qualify this questionnaire as a Sub Contractor?');}" />
                                    </dx:ASPxButton>
                                    <br />
                                    <br />
                                    <%--<dx:ASPxButton ID="btnForceRequireVerification" runat="server" Enabled="False" Text="Forcibly REQUIRE Verification Questionnaire & Set Questionnaire Status to [Incomplete]"
                                        ClientInstanceName="btnForceRequireVerification" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssClass="wrap" Wrap="True" HorizontalAlign="Left"
                                        CssPostfix="Office2003Blue" OnClick="btnForceRequireVerification_Click" Width="220px"
                                        Visible="True" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                        <ClientSideEvents Click="function (s, e) {e.processOnServer = confirm('Are you sure you want to forcibly require verification questionnaire and set Questionnaire Status to [Incomplete]?');}" />
                                    </dx:ASPxButton>
                                    <br />--%>
                                   
                                    <dx:ASPxButton ID="btnForceRequireVerification" runat="server" Enabled="True" Text="Forcibly REQUIRE Verification Questionnaire & Set Questionnaire Status to [Incomplete]"
                                        CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssClass="wrap" Wrap="True" HorizontalAlign="Left"
                                        CssPostfix="Office2003Blue" Width="220px"
                                        Visible="True" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" ClientInstanceName="btnForceRequireVerification" AutoPostBack="false">
                                    </dx:ASPxButton>
                                    <dx:ASPxPopupControl ID="popupForciblyRequireVerification" runat="server" AllowDragging="True"
                                                        CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue"
                                                        HeaderText="Forcibly Require Verification Questionnaire" Modal="True" PopupElementID="btnForceRequireVerification"
                                                        Width="200px" Font-Bold="False" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                        ShowHeader="False">
                                                        <ClientSideEvents PopUp="function(s){s.UpdatePosition()}" />
                                                        <SizeGripImage Height="16px" Width="16px" />
                                        <ContentCollection>
                                            <dx:PopupControlContentControl ID="popupContentForciblyRequireVerification" runat="server" BackColor="#DDECFE">

                                                <dx:ASPxRoundPanel ID="ASPxRoundPanel1" runat="server" BackColor="#DDECFE"
                                                                    CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue"
                                                                    ShowHeader="false" EnableDefaultAppearance="False" HeaderText="Forcibly Require Verification"
                                                                    SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" Width="490px">
                                                                    <TopEdge>
                                                                        <BackgroundImage HorizontalPosition="left" ImageUrl="~/App_Themes/Office2003Blue/Web/rpTopEdge.png"
                                                                            Repeat="RepeatX" VerticalPosition="top" />
                                                                    </TopEdge>
                                                                    <ContentPaddings Padding="2px" PaddingBottom="10px" PaddingTop="10px" />
                                                                    <HeaderRightEdge>
                                                                        <BackgroundImage HorizontalPosition="left" ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png"
                                                                            Repeat="RepeatX" VerticalPosition="top" />
                                                                    </HeaderRightEdge>
                                                                    <Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px" />
                                                                    <HeaderLeftEdge>
                                                                        <BackgroundImage HorizontalPosition="left" ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png"
                                                                            Repeat="RepeatX" VerticalPosition="top" />
                                                                    </HeaderLeftEdge>
                                                                    <HeaderStyle BackColor="#7BA4E0">
                                                                        <Paddings Padding="0px" PaddingBottom="7px" PaddingLeft="2px" PaddingRight="2px" />
                                                                        <BorderBottom BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px" />
                                                                    </HeaderStyle>
                                                                    <HeaderContent>
                                                                        <BackgroundImage HorizontalPosition="left" ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png"
                                                                            Repeat="RepeatX" VerticalPosition="top" />
                                                                    </HeaderContent>
                                                                    <DisabledStyle ForeColor="Gray">
                                                                    </DisabledStyle>
                                                                    <NoHeaderTopEdge BackColor="#DDECFE">
                                                                    </NoHeaderTopEdge>
                                                                    <PanelCollection>
                                                                        <dx:PanelContent ID="PanelContent7" runat="server">
                                                                            



                                                <span style="color: black; font-weight:bold;">Reason for forcibly requiring Verification Questionnaire (Recorded in History Log and Sent to Supplier)</span>

                                                  <dx:ASPxMemo ID="mbForciblyRequireVerification" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                                            CssPostfix="Office2003Blue" Height="53px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                                                            Width="100%">
                                                    <ValidationSettings Display="Dynamic" ValidationGroup="ForciblyRequireVerification">
                                                                                                <RequiredField IsRequired="True" />
                                                    </ValidationSettings>

                                                </dx:ASPxMemo>
                                                <dx:ASPxLabel ID="lblForciblyRequireVerificationError" runat="server" Font-Bold="True" ForeColor="Red">
                                                </dx:ASPxLabel>
                                                <br />
                                                <div align="center">

                                                <dx:ASPxButton ID="btnForciblyRequireVerification" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                                                CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                                                                Text="Send Back to Supplier" Width="236px" 
                                                                                                ValidationGroup="ForciblyRequireVerification" OnClick="btnForciblyRequireVerification_Click">
                                                </dx:ASPxButton>
                                                    </div>
                                                                    
                                                                            </dx:PanelContent>
                                                                        </PanelCollection>
                                                                        </dx:ASPxRoundPanel>


				                            </dx:PopupControlContentControl>
                                        </ContentCollection>
                                        <CloseButtonImage Height="12px" Width="13px" />
                                        <HeaderStyle>
                                            <Paddings PaddingRight="6px" />
                                        </HeaderStyle>
                                    </dx:ASPxPopupControl>
                                    <br />
                                    <br />
                                    <dx:ASPxButton ID="btnForceDeleteVerification" runat="server" Enabled="False" Text="Forcibly Delete (Remove) Verification Questionnaire"
                                        ClientInstanceName="btnForceDeleteVerification" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssClass="wrap" Wrap="True" HorizontalAlign="Left"
                                        CssPostfix="Office2003Blue" OnClick="btnForceDeleteVerification_Click" Width="220px"
                                        Visible="True" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                        <ClientSideEvents Click="function (s, e) {e.processOnServer = confirm('Are you sure you want to forcibly Delete (Remove) the Verification questionnaire?');}" />
                                    </dx:ASPxButton>
                                    <br />
                                    <br />
                                    <dx:ASPxButton ID="btnSendBackToProcurementStage" runat="server" Enabled="True" Text="Send Back to Procurement Stage"
                                        ClientInstanceName="btnSendBackToProcurementStage" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssClass="wrap" Wrap="True" HorizontalAlign="Left"
                                        CssPostfix="Office2003Blue" OnClick="btnSendBackToProcurementStage_Click" Width="220px"
                                        Visible="True" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                        <ClientSideEvents Click="function (s, e) {e.processOnServer = confirm('Are you sure you want to send this questionnaire back to the procurement stage?');}" />
                                    </dx:ASPxButton>
                                    <br />
                                    <br />
                                    </td>
                                <!--Wrap the Text of button ->Ended by Debashis for deployment -->
                            </tr>
                            <tr>
                                <td style="width: 100%; height: 13px; text-align: center">
                                    <strong>Restrictions</strong>
                                  </td>
                            </tr>
                            <tr>
                                <td style="width: 100%; height: 13px">Delete option only available if Questionnaire Status is not 'Assessment Complete'.
                                  </td>
                            </tr>
                        </table>
                    </dx:PopupControlContentControl>
                </ContentCollection>
                <CloseButtonImage Height="12px" Width="13px" />
                <HeaderStyle>
                    <Paddings PaddingRight="6px" />
                </HeaderStyle>
            </dx:ASPxPopupControl>
            
            <dx:ASPxPopupControl ID="ASPxPopupControl2" runat="server" ClientInstanceName="ASPxPopupControl2"
                BackColor="Info" CloseAction="CloseButton" PopupElementID="btnSendBackToSupplier_PopUp"
                CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue"
                EnableHotTrack="False" ForeColor="Black" Modal="True" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                HeaderText="Reminder" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter"
                Width="800px">
                <ContentCollection>
                    <dx:PopupControlContentControl runat="server">
                        <br />
                        <br />
                        <div style="border-right: black 1px solid; padding-right: 10px; border-top: black 1px solid; padding-left: 10px; padding-bottom: 10px; border-left: black 1px solid; padding-top: 10px; border-bottom: black 1px solid; background-color: #ffffcc; text-align: center">
                            <span style="font-size: 13pt; color: maroon"><strong><span style="font-family: Verdana">
                                <asp:Label ID="lblBox1_PopUp" runat="server" Text="Submitted Successfully"></asp:Label></span><br />
                            </strong><span style="color: maroon"><span style="font-size: 10pt; font-family: Verdana">
                                <asp:Label ID="lblBox2_PopUp" runat="server" Text="Your questionnaire will now be assessed and procurement will be in contact with you regarding your submission."></asp:Label>
                                <br />
                                <div align="center">
                                    <dx:ASPxButton ID="btnOk" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                        CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                        Text="OK" UseSubmitBehavior="False" Visible="false" AutoPostBack="false">
                                        <ClientSideEvents Click="function(s, e) { ASPxPopupControl2.Hide();
    }"></ClientSideEvents>
                                    </dx:ASPxButton>
                                </div>
                                <dx:ASPxPanel ID="pnlSendBackToSupplier" runat="server" Visible="False" Width="100%">
                                    <PanelCollection>
                                        <dx:PanelContent runat="server">
                                            To send this questionnaire back to the supplier for completion:
                                              <p align="left">
                                                1) Please verify that the supplier's e-mail address is correct below (if not, please
                                                change accordingly).
                                                1) Please verify that the supplier's e-mail address is correct below (if not, please
                                                change accordingly).1) Please verify that the supplier's e-mail address is correct below (if not, please
                                                change accordingly).
                                                1) Please verify that the supplier's e-mail address is correct below (if not, please
                                                change accordingly). <br />
                                                2) Click the 'Send back to Supplier' button.
                                              </p>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <dx:ASPxTextBox ID="tbSupplierEmail" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                            CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                            Width="300px">
                                                        </dx:ASPxTextBox>
                                                    </td>
                                                    <td>
                                                        <dx:ASPxButton ID="btnSendBackToSupplier" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                            CssPostfix="Office2003Blue" OnClick="btnSendBackToSupplier_Click" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                            Text="Send back to Supplier">
                                                        </dx:ASPxButton>
                                                    </td>
                                                </tr>
                                            </table>
                                        </dx:PanelContent>
                                    </PanelCollection>
                                </dx:ASPxPanel>
                            </span></span></span>
                        </div>
                        <br />
                    </dx:PopupControlContentControl>
                </ContentCollection>
                <HeaderStyle>
                    <Paddings PaddingRight="6px" />
                </HeaderStyle>
            </dx:ASPxPopupControl>
        </td>
    </tr>
</table>
</ContentTemplate>
</asp:UpdatePanel>
<data:EntityDataSource ID="UsersAlcoanDataSource2" runat="server" ProviderName="UsersAlcoanProvider"
    EntityTypeName="KaiZen.CSMS.UsersAlcoan, KaiZen.CSMS" EntityKeyTypeName="System.Int32"
    SelectMethod="GetAll" Sort="UserFullNameLogon ASC" />
<data:EntityDataSource ID="SitesDataSource2" runat="server" ProviderName="SitesProvider"
    EntityTypeName="KaiZen.CSMS.Sites, KaiZen.CSMS" EntityKeyTypeName="System.Int32"
    SelectMethod="GetAll" Sort="SiteName ASC" Filter="IsVisible = True" />
<data:QuestionnaireStatusDataSource ID="QuestionnaireStatusDataSource1" runat="server"
    Filter="">
</data:QuestionnaireStatusDataSource>
<data:CompanyStatusDataSource ID="CompanyStatusDataSource" runat="server" Sort="Ordinal ASC">
</data:CompanyStatusDataSource>
<data:EntityDataSource ID="UsersProcurementListDataSource2" runat="server" EntityKeyTypeName="System.Int32"
    EntityTypeName="KaiZen.CSMS.UsersProcurementList, KaiZen.CSMS" Filter="Enabled = True"
    ProviderName="UsersProcurementListProvider" SelectMethod="GetAll" Sort="UserFullNameLogon ASC">
</data:EntityDataSource>
<data:CompanySiteCategoryDataSource ID="CompanySiteCategoryDataSource" runat="server"
    SelectMethod="GetPaged" EnablePaging="True" EnableSorting="True">
</data:CompanySiteCategoryDataSource>
<asp:ObjectDataSource ID="odsActionLog" runat="server" OldValuesParameterFormatString="original_{0}"
    SelectMethod="GetByQuestionnaireId" TypeName="KaiZen.CSMS.Services.QuestionnaireActionLogFriendlyService"
    DataObjectTypeName="KaiZen.CSMS.Entities.QuestionnaireActionLogFriendly" DeleteMethod="Delete"
    InsertMethod="Insert" UpdateMethod="Update">
    <SelectParameters>
        <asp:QueryStringParameter DefaultValue="0" Name="QuestionnaireId" QueryStringField="q"
            Type="Int32" />
    </SelectParameters>
</asp:ObjectDataSource>
<asp:ObjectDataSource ID="odsActionNote" runat="server" OldValuesParameterFormatString="original_{0}"
    SelectMethod="GetByCreatedByCompanyIdVisibility" TypeName="KaiZen.CSMS.Services.CompanyNoteService"
    DataObjectTypeName="KaiZen.CSMS.Entities.CompanyNote" DeleteMethod="Delete"
    InsertMethod="Insert" UpdateMethod="Update">
    <SelectParameters>
        <asp:QueryStringParameter DefaultValue="0" Name="_CreatedByCompanyId" Type="Int32" />
    </SelectParameters>
    <SelectParameters>
        <asp:QueryStringParameter DefaultValue="All" Name="_Visibility" Type="String" />
    </SelectParameters>
</asp:ObjectDataSource>
