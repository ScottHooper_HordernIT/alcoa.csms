﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using KaiZen.CSMS.Entities;

public partial class Admin : System.Web.UI.Page
{
    Auth auth = new Auth();

    protected void Page_Load(object sender, EventArgs e) { Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); }

    private void moduleInit(bool postBack)
    {
        try
        {
            if (Request.QueryString["s"] == null)
            {
                Control uc = LoadControl("~/UserControls/Admin/default.ascx");
                PlaceHolder1.Controls.Add(uc);
            }
            else
            {
                String s = Request.QueryString["s"].ToString();

                //if (s == "UsersCompaniesMap1")
                //{
                //    lblArr.Visible = true;
                //    usersMapLink.Visible = true;
                //}
                //else
                //{
                //    lblArr.Visible = false;
                //    usersMapLink.Visible = false;
                //}

                if (auth.RoleId != (int)RoleList.Administrator) s = "default"; //throw new Exception("You are not an Administrator.");
                Control uc = LoadControl("~/UserControls/Admin/" + s + ".ascx");
                PlaceHolder1.Controls.Add(uc);
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Could not load Administrative Section. Error: " + ex.Message.ToString());
        }
    }
}
