﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="UserControls_Admin_EventLog" Codebehind="EventLog.ascx.cs" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="cc1" %>
    
<dxwgv:aspxgridview id="ASPxGridView1" runat="server" autogeneratecolumns="False"
    cssfilepath="~/App_Themes/Office2003Blue/{0}/styles.css" csspostfix="Office2003Blue"
    width="900px">
<Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
<ExpandedButton Height="12px" Width="11px"></ExpandedButton>

    <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
    </LoadingPanelOnStatusBar>

<CollapsedButton Height="12px" Width="11px"></CollapsedButton>

<DetailCollapsedButton Height="12px" Width="11px"></DetailCollapsedButton>

<DetailExpandedButton Height="12px" Width="11px"></DetailExpandedButton>
    <LoadingPanel Url="~/App_Themes/Office2003BlueOld/GridView/Loading.gif"><%--Enhancement_023:change by debashis for testing platform upgradation--%>
    </LoadingPanel>
</Images>

    <ImagesFilterControl>
        <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
        </LoadingPanel>
    </ImagesFilterControl>

<Styles CssPostfix="Office2003Blue" 
        CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css">
<LoadingPanel ImageSpacing="10px"></LoadingPanel>

<Header SortingImageSpacing="5px" ImageSpacing="5px"></Header>
</Styles>
<Columns>
<dxwgv:GridViewDataDateColumn VisibleIndex="0" FieldName="TimeGenerated" Width="100px" SortIndex="0" SortOrder="Descending" Caption="Time" GroupIndex="0" UnboundType="DateTime">
<PropertiesDateEdit DisplayFormatString="yyyy-MM-dd HH:mm:ss">
                    </PropertiesDateEdit>
</dxwgv:GridViewDataDateColumn>
<dxwgv:GridViewDataMemoColumn VisibleIndex="1" FieldName="Message">
<PropertiesMemoEdit Rows="3"></PropertiesMemoEdit>
</dxwgv:GridViewDataMemoColumn>
</Columns>
    <Settings ShowFilterRow="True" ShowGroupPanel="True" />
    <SettingsBehavior ColumnResizeMode="Control" />
    <SettingsPager>
        <AllButton Visible="True">
        </AllButton>
    </SettingsPager>
</dxwgv:aspxgridview>
&nbsp;
<table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
    <tr>
        <td style="width: 900px; text-align: right">
            Number of Rows to show per page:<asp:DropDownList ID="DropDownList1" runat="server"
                AutoPostBack="True" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged">
                <asp:ListItem>10</asp:ListItem>
                <asp:ListItem>20</asp:ListItem>
                <asp:ListItem>50</asp:ListItem>
                <asp:ListItem>All</asp:ListItem>
            </asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td style="width: 900px; text-align: right">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="conditional">
                <ContentTemplate>
                    <strong>&nbsp;Export:</strong>
                    <asp:ImageButton ID="ImageButton3" runat="server" ImageUrl="~/Images/exportLogos/excel.gif"
                        OnClick="ImageButton3_Click" ToolTip="Export table to Microsoft Excel (XLS)" />&nbsp;
                    <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/exportLogos/word.gif"
                        OnClick="ImageButton1_Click" ToolTip="Export Table to Microsoft Word Document (RTF)" />&nbsp;
                    <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/Images/exportLogos/pdf.gif"
                        OnClick="ImageButton2_Click" ToolTip="Export table to Adobe Acrobat (PDF)" />
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="ImageButton1" />
                    <asp:PostBackTrigger ControlID="ImageButton2" />
                    <asp:PostBackTrigger ControlID="ImageButton3" />
                </Triggers>
            </asp:UpdatePanel>
        </td>
    </tr>
</table>
<cc1:aspxgridviewexporter id="ASPxGridViewExporter1" runat="server" filename="Event Logs"
    gridviewid="ASPxGridView1">
</cc1:aspxgridviewexporter>
