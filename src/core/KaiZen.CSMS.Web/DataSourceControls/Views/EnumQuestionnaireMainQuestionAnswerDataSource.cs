﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.EnumQuestionnaireMainQuestionAnswerProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[CLSCompliant(true)]
	[Designer(typeof(EnumQuestionnaireMainQuestionAnswerDataSourceDesigner))]
	public class EnumQuestionnaireMainQuestionAnswerDataSource : ReadOnlyDataSource<EnumQuestionnaireMainQuestionAnswer>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EnumQuestionnaireMainQuestionAnswerDataSource class.
		/// </summary>
		public EnumQuestionnaireMainQuestionAnswerDataSource() : base(new EnumQuestionnaireMainQuestionAnswerService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the EnumQuestionnaireMainQuestionAnswerDataSourceView used by the EnumQuestionnaireMainQuestionAnswerDataSource.
		/// </summary>
		protected EnumQuestionnaireMainQuestionAnswerDataSourceView EnumQuestionnaireMainQuestionAnswerView
		{
			get { return ( View as EnumQuestionnaireMainQuestionAnswerDataSourceView ); }
		}
		
		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the EnumQuestionnaireMainQuestionAnswerDataSourceView class that is to be
		/// used by the EnumQuestionnaireMainQuestionAnswerDataSource.
		/// </summary>
		/// <returns>An instance of the EnumQuestionnaireMainQuestionAnswerDataSourceView class.</returns>
		protected override BaseDataSourceView<EnumQuestionnaireMainQuestionAnswer, Object> GetNewDataSourceView()
		{
			return new EnumQuestionnaireMainQuestionAnswerDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the EnumQuestionnaireMainQuestionAnswerDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class EnumQuestionnaireMainQuestionAnswerDataSourceView : ReadOnlyDataSourceView<EnumQuestionnaireMainQuestionAnswer>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EnumQuestionnaireMainQuestionAnswerDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the EnumQuestionnaireMainQuestionAnswerDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public EnumQuestionnaireMainQuestionAnswerDataSourceView(EnumQuestionnaireMainQuestionAnswerDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal EnumQuestionnaireMainQuestionAnswerDataSource EnumQuestionnaireMainQuestionAnswerOwner
		{
			get { return Owner as EnumQuestionnaireMainQuestionAnswerDataSource; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal EnumQuestionnaireMainQuestionAnswerService EnumQuestionnaireMainQuestionAnswerProvider
		{
			get { return Provider as EnumQuestionnaireMainQuestionAnswerService; }
		}

		#endregion Properties
		
		#region Methods
		
		#endregion Methods
	}

	#region EnumQuestionnaireMainQuestionAnswerDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the EnumQuestionnaireMainQuestionAnswerDataSource class.
	/// </summary>
	public class EnumQuestionnaireMainQuestionAnswerDataSourceDesigner : ReadOnlyDataSourceDesigner<EnumQuestionnaireMainQuestionAnswer>
	{
	}

	#endregion EnumQuestionnaireMainQuestionAnswerDataSourceDesigner

	#region EnumQuestionnaireMainQuestionAnswerFilter

	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EnumQuestionnaireMainQuestionAnswer"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EnumQuestionnaireMainQuestionAnswerFilter : SqlFilter<EnumQuestionnaireMainQuestionAnswerColumn>
	{
	}

	#endregion EnumQuestionnaireMainQuestionAnswerFilter

	#region EnumQuestionnaireMainQuestionAnswerExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EnumQuestionnaireMainQuestionAnswer"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EnumQuestionnaireMainQuestionAnswerExpressionBuilder : SqlExpressionBuilder<EnumQuestionnaireMainQuestionAnswerColumn>
	{
	}
	
	#endregion EnumQuestionnaireMainQuestionAnswerExpressionBuilder		
}

