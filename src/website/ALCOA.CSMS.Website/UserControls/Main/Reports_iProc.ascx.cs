using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using DevExpress.XtraCharts;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxEditors;
using DevExpress.XtraPrinting;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using LumenWorks.Framework.IO.Csv;
using System.IO;
using System.Security;
using System.Security.Principal;

public partial class UserControls_Reports_iProc : System.Web.UI.UserControl
{
    Auth auth = new Auth();
    protected void Page_Load(object sender, EventArgs e) {  Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); }

    private void moduleInit(bool postBack)
    {
        if (!postBack)
        {
            if (auth.RoleId == (int)RoleList.Reader || auth.RoleId == (int)RoleList.Administrator)
            {
                SessionHandler.spVar_CompanyId = "-1";
            }
            else
            {
                SessionHandler.spVar_CompanyId = auth.CompanyId.ToString();
                //ASPxGridView1.Columns["CompanyName"].Visible = false;
                grid.GroupBy((GridViewDataColumn)grid.Columns["SiteName"], 1);
                grid.ExpandAll();
            }
            Calc();

            // Export
            String exportFileName = @"ALCOA CSMS - iProc Extract Report"; //Chrome & IE is fine.
            String userAgent = Request.Headers.Get("User-Agent");
            if (
                (userAgent.Contains("Mozilla") && !userAgent.Contains("AppleWebKit") && !userAgent.Contains("MSIE")) || //Mozilla
                (userAgent.Contains("AppleWebKit") && !userAgent.Contains("Chrome"))      //Safari
                )
            {
                exportFileName = exportFileName.Replace(" ", "_");
            }
            Helper.ExportGrid.Settings(ucExportButtons, exportFileName);
        }
        else
        {
            grid.DataSource = (DataTable)Session["dtGrid"];
            grid.DataBind();
        }
    }

    public void Calc()
    {
        //1. Read EHSIMS data from csv flat file into datatable
        //string csvFilePath = "C:\\TempRead\\iProc Contractor Services Report.csv";
        Configuration configuration = new Configuration();
        string csvFilePath = @configuration.GetValue(ConfigList.ContractorIprocExtractFilePath);
        const string startHeader = "Supplier#";

        //2.1 Read from the actual Header of the CSV file. (Ignore junk at start of CSV file)
        string myFile = "";
        bool start = false;

        WindowsImpersonationContext impContext = null;
        try
        {
            impContext = Helper.ImpersonateWAOCSMUser();
        }
        catch (ApplicationException ex)
        {
            Response.Write(ex.Message);
        }

        try
        {
            if (null != impContext)
            {
                using (impContext)
                {
                    try
                    {
                        using (FileStream logFileStream = new FileStream(csvFilePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                        {
                            StreamReader logFileReader = new StreamReader(logFileStream);
                            while (!logFileReader.EndOfStream)
                            {
                                string line = logFileReader.ReadLine();
                                if (line.Contains(startHeader)) start = true;
                                if (start == true)
                                    myFile += line + "\n";
                            }
                            logFileReader.Close();
                            logFileStream.Close();
                        }
                    }
                    catch (Exception ex)
                    {
                        Elmah.ErrorSignal.FromContext(Context).Raise(ex);
                        Response.Write(ex.Message);
                    }

                    if (!String.IsNullOrEmpty(myFile))
                    {
                        //2.1 Create the DataTable which will store the variation report
                        DataTable dtGrid = new DataTable();
                        dtGrid.Columns.Add("CompanyName", typeof(string));
                        dtGrid.Columns.Add("LocCode", typeof(string));
                        dtGrid.Columns.Add("Month", typeof(int));
                        dtGrid.Columns.Add("Year", typeof(int));
                        dtGrid.Columns.Add("No of Claims", typeof(int));
                        dtGrid.Columns.Add("KPI Lodged", typeof(string));

                        //2.3 Read Filtered CSV File and insert into DataTable

                        using (CsvReader csv = new CsvReader(new StringReader(myFile), true))
                        {
                            int fieldCount = csv.FieldCount;
                            //string[] headers = csv.GetFieldHeaders();

                            if (fieldCount != 6) throw new Exception(
                                                    String.Format("Error: iProc file. CSMS expected 6 Columns. Input file has {0}", fieldCount));

                            while (csv.ReadNextRecord())
                            {
                                DataRow dr = dtGrid.NewRow();
                                string CompanyName = "";

                                //TList<Companies> c = DataRepository.CompaniesProvider.GetByIprocSupplierNo(csv["Supplier#"]);
                                GetABNbyVendor.SelectParameters["Vendor_Number"].DefaultValue = csv["Supplier#"].ToString();
                                DataView dVDistinct = (DataView)GetABNbyVendor.Select(DataSourceSelectArguments.Empty);
                                DataTable dtDistinct = dVDistinct.ToTable();
                                if (dtDistinct.Rows.Count > 0)
                                {
                                    string ABNVendor = dtDistinct.Rows[0][0].ToString();
                                    Companies c = DataRepository.CompaniesProvider.GetByCompanyAbn(ABNVendor);
                                    if (c != null)
                                    {
                                        CompanyName = c.CompanyName;
                                    }
                                    else
                                    {
                                        CompanyName = "";
                                    }
                                }
                                else
                                    CompanyName = "";

                                

                                if (!string.IsNullOrEmpty(CompanyName))
                                {
                                    dr["CompanyName"] = CompanyName;
                                    //Convert LBCCode to SiteId
                                    string LocCode = "";
                                    string Lbc = csv["Lbc"].ToString();
                                    int iLbc = 0;
                                    int.TryParse(Lbc, out iLbc);

                                    if (String.IsNullOrEmpty(Lbc)) throw new Exception("Lbc Empty");
                                    int SiteId = 0;
                                    using (DataSet ds = DataRepository.SitesProvider.GetSiteIdByIprocCode(iLbc))
                                    {
                                        if (ds != null)
                                        {
                                            if (ds.Tables[0].Rows.Count > 0)
                                            {
                                                if (ds.Tables[0].Rows[0].ItemArray[0] != null)
                                                {
                                                    int.TryParse(ds.Tables[0].Rows[0].ItemArray[0].ToString(), out SiteId);
                                                }
                                            }
                                        }
                                    }
                                    using (DataSet ds = DataRepository.SitesProvider.GetSiteNameByIprocCode(iLbc))
                                    {
                                        if (ds != null)
                                        {
                                            if (ds.Tables[0].Rows.Count > 0)
                                            {
                                                if (ds.Tables[0].Rows[0].ItemArray[0] != null)
                                                {
                                                    LocCode = ds.Tables[0].Rows[0].ItemArray[0].ToString();
                                                }
                                            }
                                        }
                                    }
                                    if (SiteId == 0 || String.IsNullOrEmpty(LocCode))
                                    {
                                        // ???
                                    }
                                    else
                                    {
                                        //Check if SiteId = Site Group
                                        //hardcoded for now
                                        if (SiteId == 9) SiteId = -2;   //WAO,
                                        if (SiteId == 11) SiteId = -3;  //All Mines
                                        if (SiteId == 16) SiteId = -4;  //Vic Ops
                                        if (SiteId == 10) SiteId = -5;  //Australia (All Sites)

                                        dr["LocCode"] = LocCode;
                                        DateTime dtPreviousMonth;
                                        if (DateTime.Now.Day < 15)
                                        {
                                            dtPreviousMonth = DateTime.Now.AddMonths(-2);
                                        }
                                        else
                                        {
                                            dtPreviousMonth = DateTime.Now.AddMonths(01);
                                        }

                                        dr["Month"] = dtPreviousMonth.Month;
                                        dr["Year"] = dtPreviousMonth.Year;
                                        dr["No of Claims"] = csv["No of Claims"];

                                        int countKPIs = 0;
                                        using (DataSet ds = DataRepository.KpiProvider.GetCountIproc(CompanyName, SiteId,
                                                                                    dtPreviousMonth.Month, dtPreviousMonth.Year))
                                        {
                                            if (ds != null)
                                            {
                                                if (ds.Tables[0].Rows.Count > 0)
                                                {
                                                    if (ds.Tables[0].Rows[0].ItemArray[0] != null)
                                                    {
                                                        int.TryParse(ds.Tables[0].Rows[0].ItemArray[0].ToString(), out countKPIs);
                                                    }
                                                }
                                            }
                                        }

                                        if (countKPIs > 0)
                                        {
                                            dr["KPI Lodged"] = "Yes";
                                        }
                                        else
                                        {
                                            dr["KPI Lodged"] = "No";
                                            if (auth.RoleId == (int)RoleList.Contractor)
                                            {
                                                if (auth.CompanyName == dr["CompanyName"].ToString())
                                                {
                                                    dtGrid.Rows.Add(dr);
                                                }
                                            }
                                            else
                                            {
                                                dtGrid.Rows.Add(dr);
                                            }
                                        }
                                    }
                                }
                            }

                            Session["dtGrid"] = dtGrid;
                            grid.DataSource = (DataTable)Session["dtGrid"];
                            grid.SortBy((GridViewDataColumn)grid.Columns["CompanyName"], 0);
                            grid.SortBy((GridViewDataColumn)grid.Columns["LocCode"], 1);
                            grid.DataBind();
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            Elmah.ErrorSignal.FromContext(Context).Raise(ex);
            Response.Write(ex.Message);
            //Response.Write("Server Configuration Account Error. Contact Administrator");
        }
        finally
        {
            //if (impContext != null) 
            //    //impContext.Undo();
        }
    }
}
