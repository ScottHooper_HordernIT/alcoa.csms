﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.UsersCompaniesProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[CLSCompliant(true)]
	[Designer(typeof(UsersCompaniesDataSourceDesigner))]
	public class UsersCompaniesDataSource : ReadOnlyDataSource<UsersCompanies>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UsersCompaniesDataSource class.
		/// </summary>
		public UsersCompaniesDataSource() : base(new UsersCompaniesService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the UsersCompaniesDataSourceView used by the UsersCompaniesDataSource.
		/// </summary>
		protected UsersCompaniesDataSourceView UsersCompaniesView
		{
			get { return ( View as UsersCompaniesDataSourceView ); }
		}
		
		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the UsersCompaniesDataSourceView class that is to be
		/// used by the UsersCompaniesDataSource.
		/// </summary>
		/// <returns>An instance of the UsersCompaniesDataSourceView class.</returns>
		protected override BaseDataSourceView<UsersCompanies, Object> GetNewDataSourceView()
		{
			return new UsersCompaniesDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the UsersCompaniesDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class UsersCompaniesDataSourceView : ReadOnlyDataSourceView<UsersCompanies>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UsersCompaniesDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the UsersCompaniesDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public UsersCompaniesDataSourceView(UsersCompaniesDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal UsersCompaniesDataSource UsersCompaniesOwner
		{
			get { return Owner as UsersCompaniesDataSource; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal UsersCompaniesService UsersCompaniesProvider
		{
			get { return Provider as UsersCompaniesService; }
		}

		#endregion Properties
		
		#region Methods
		
		#endregion Methods
	}

	#region UsersCompaniesDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the UsersCompaniesDataSource class.
	/// </summary>
	public class UsersCompaniesDataSourceDesigner : ReadOnlyDataSourceDesigner<UsersCompanies>
	{
	}

	#endregion UsersCompaniesDataSourceDesigner

	#region UsersCompaniesFilter

	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="UsersCompanies"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UsersCompaniesFilter : SqlFilter<UsersCompaniesColumn>
	{
	}

	#endregion UsersCompaniesFilter

	#region UsersCompaniesExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="UsersCompanies"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UsersCompaniesExpressionBuilder : SqlExpressionBuilder<UsersCompaniesColumn>
	{
	}
	
	#endregion UsersCompaniesExpressionBuilder		
}

