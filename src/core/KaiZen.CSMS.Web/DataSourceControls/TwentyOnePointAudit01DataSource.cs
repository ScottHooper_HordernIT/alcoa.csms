﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.TwentyOnePointAudit01Provider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(TwentyOnePointAudit01DataSourceDesigner))]
	public class TwentyOnePointAudit01DataSource : ProviderDataSource<TwentyOnePointAudit01, TwentyOnePointAudit01Key>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit01DataSource class.
		/// </summary>
		public TwentyOnePointAudit01DataSource() : base(new TwentyOnePointAudit01Service())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the TwentyOnePointAudit01DataSourceView used by the TwentyOnePointAudit01DataSource.
		/// </summary>
		protected TwentyOnePointAudit01DataSourceView TwentyOnePointAudit01View
		{
			get { return ( View as TwentyOnePointAudit01DataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the TwentyOnePointAudit01DataSource control invokes to retrieve data.
		/// </summary>
		public TwentyOnePointAudit01SelectMethod SelectMethod
		{
			get
			{
				TwentyOnePointAudit01SelectMethod selectMethod = TwentyOnePointAudit01SelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (TwentyOnePointAudit01SelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the TwentyOnePointAudit01DataSourceView class that is to be
		/// used by the TwentyOnePointAudit01DataSource.
		/// </summary>
		/// <returns>An instance of the TwentyOnePointAudit01DataSourceView class.</returns>
		protected override BaseDataSourceView<TwentyOnePointAudit01, TwentyOnePointAudit01Key> GetNewDataSourceView()
		{
			return new TwentyOnePointAudit01DataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the TwentyOnePointAudit01DataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class TwentyOnePointAudit01DataSourceView : ProviderDataSourceView<TwentyOnePointAudit01, TwentyOnePointAudit01Key>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit01DataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the TwentyOnePointAudit01DataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public TwentyOnePointAudit01DataSourceView(TwentyOnePointAudit01DataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal TwentyOnePointAudit01DataSource TwentyOnePointAudit01Owner
		{
			get { return Owner as TwentyOnePointAudit01DataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal TwentyOnePointAudit01SelectMethod SelectMethod
		{
			get { return TwentyOnePointAudit01Owner.SelectMethod; }
			set { TwentyOnePointAudit01Owner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal TwentyOnePointAudit01Service TwentyOnePointAudit01Provider
		{
			get { return Provider as TwentyOnePointAudit01Service; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<TwentyOnePointAudit01> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<TwentyOnePointAudit01> results = null;
			TwentyOnePointAudit01 item;
			count = 0;
			
			System.Int32 _id;

			switch ( SelectMethod )
			{
				case TwentyOnePointAudit01SelectMethod.Get:
					TwentyOnePointAudit01Key entityKey  = new TwentyOnePointAudit01Key();
					entityKey.Load(values);
					item = TwentyOnePointAudit01Provider.Get(entityKey);
					results = new TList<TwentyOnePointAudit01>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case TwentyOnePointAudit01SelectMethod.GetAll:
                    results = TwentyOnePointAudit01Provider.GetAll(StartIndex, PageSize, out count);
                    break;
				case TwentyOnePointAudit01SelectMethod.GetPaged:
					results = TwentyOnePointAudit01Provider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case TwentyOnePointAudit01SelectMethod.Find:
					if ( FilterParameters != null )
						results = TwentyOnePointAudit01Provider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = TwentyOnePointAudit01Provider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case TwentyOnePointAudit01SelectMethod.GetById:
					_id = ( values["Id"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["Id"], typeof(System.Int32)) : (int)0;
					item = TwentyOnePointAudit01Provider.GetById(_id);
					results = new TList<TwentyOnePointAudit01>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				// FK
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == TwentyOnePointAudit01SelectMethod.Get || SelectMethod == TwentyOnePointAudit01SelectMethod.GetById )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				TwentyOnePointAudit01 entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					TwentyOnePointAudit01Provider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<TwentyOnePointAudit01> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			TwentyOnePointAudit01Provider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region TwentyOnePointAudit01DataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the TwentyOnePointAudit01DataSource class.
	/// </summary>
	public class TwentyOnePointAudit01DataSourceDesigner : ProviderDataSourceDesigner<TwentyOnePointAudit01, TwentyOnePointAudit01Key>
	{
		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit01DataSourceDesigner class.
		/// </summary>
		public TwentyOnePointAudit01DataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public TwentyOnePointAudit01SelectMethod SelectMethod
		{
			get { return ((TwentyOnePointAudit01DataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new TwentyOnePointAudit01DataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region TwentyOnePointAudit01DataSourceActionList

	/// <summary>
	/// Supports the TwentyOnePointAudit01DataSourceDesigner class.
	/// </summary>
	internal class TwentyOnePointAudit01DataSourceActionList : DesignerActionList
	{
		private TwentyOnePointAudit01DataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit01DataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public TwentyOnePointAudit01DataSourceActionList(TwentyOnePointAudit01DataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public TwentyOnePointAudit01SelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion TwentyOnePointAudit01DataSourceActionList
	
	#endregion TwentyOnePointAudit01DataSourceDesigner
	
	#region TwentyOnePointAudit01SelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the TwentyOnePointAudit01DataSource.SelectMethod property.
	/// </summary>
	public enum TwentyOnePointAudit01SelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetById method.
		/// </summary>
		GetById
	}
	
	#endregion TwentyOnePointAudit01SelectMethod

	#region TwentyOnePointAudit01Filter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit01"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAudit01Filter : SqlFilter<TwentyOnePointAudit01Column>
	{
	}
	
	#endregion TwentyOnePointAudit01Filter

	#region TwentyOnePointAudit01ExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit01"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAudit01ExpressionBuilder : SqlExpressionBuilder<TwentyOnePointAudit01Column>
	{
	}
	
	#endregion TwentyOnePointAudit01ExpressionBuilder	

	#region TwentyOnePointAudit01Property
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;TwentyOnePointAudit01ChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit01"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAudit01Property : ChildEntityProperty<TwentyOnePointAudit01ChildEntityTypes>
	{
	}
	
	#endregion TwentyOnePointAudit01Property
}

