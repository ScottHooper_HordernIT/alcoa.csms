﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repo.CSMS.DAL.Infrastructure
{
    public interface IObjectContext : IDisposable
    {
        void SaveChanges();
        ObjectContextOptions ContextOptions { get; }
    }
}
