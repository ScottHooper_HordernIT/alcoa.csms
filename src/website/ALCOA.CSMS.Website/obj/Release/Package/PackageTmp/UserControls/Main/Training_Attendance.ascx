<%@ Control Language="C#" AutoEventWireup="true" Inherits="UserControls_Training_Attendance" Codebehind="Training_Attendance.ascx.cs" %>
<table border="0" cellpadding="2" cellspacing="0" width="100%">
    <tr>
        <td class="pageName" colspan="3" style="height: 16px">
            <span class="bodycopy"><span class="title">Training</span><br />
                <span class="date">Attendance</span><br />
                <img height="1" src="images/grfc_dottedline.gif" width="24" /><br />
            </span>
        </td>
    </tr>
    <tr>
        <td colspan="3" style="text-align: center; height: 83px;">
            <br />
            <span style="font-size: 10pt; color: blue; font-family: Verdana; mso-fareast-font-family: 'Times New Roman';
                mso-bidi-font-family: 'Times New Roman'; mso-ansi-language: EN-AU; mso-fareast-language: EN-AU;
                mso-bidi-language: AR-SA">Please ensure that completed Training Attendance sheets are forwarded to the Centralized Contractor Management Officer/Admin Assistant, Address (Alcoa Pinjarra Refinery, Pinjarra, Western Australia, 6208).<br />
            </span>
            <br />
            <asp:Button ID="btnDownload" runat="server" Text="Download Training Attendance Record" /><br />
            <br />
            <asp:Label ID="lblMsg" runat="server"></asp:Label></td>
    </tr>
</table>
