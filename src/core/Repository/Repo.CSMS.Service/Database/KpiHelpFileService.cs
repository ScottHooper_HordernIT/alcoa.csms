﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface IKpiHelpFileService
    {
        KpiHelpFile Get(Expression<Func<KpiHelpFile, bool>> where, List<Expression<Func<KpiHelpFile, object>>> includes);
        List<KpiHelpFile> GetMany(int? top, Expression<Func<KpiHelpFile, bool>> where, Func<IQueryable<KpiHelpFile>, IOrderedQueryable<KpiHelpFile>> orderBy, List<Expression<Func<KpiHelpFile, object>>> includes);
        List<KpiHelpFile> GetManyPaged(int pageNumber, int pageSize, Expression<Func<KpiHelpFile, bool>> where, Func<IQueryable<KpiHelpFile>, IOrderedQueryable<KpiHelpFile>> orderBy, List<Expression<Func<KpiHelpFile, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<KpiHelpFile, bool>> where, List<Expression<Func<KpiHelpFile, object>>> includes);
        void Insert(KpiHelpFile item, bool saveChanges = true);
        void Update(KpiHelpFile item, bool saveChanges = true);
        void Delete(KpiHelpFile item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class KpiHelpFileService : IKpiHelpFileService
    {
        private readonly IRepository<KpiHelpFile> _repository;

        public KpiHelpFileService(IRepository<KpiHelpFile> repository)
        {
            this._repository = repository;
        }

        public KpiHelpFile Get(Expression<Func<KpiHelpFile, bool>> where, List<Expression<Func<KpiHelpFile, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<KpiHelpFile> GetMany(int? top, Expression<Func<KpiHelpFile, bool>> where, Func<IQueryable<KpiHelpFile>, IOrderedQueryable<KpiHelpFile>> orderBy, List<Expression<Func<KpiHelpFile, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<KpiHelpFile> GetManyPaged(int pageNumber, int pageSize, Expression<Func<KpiHelpFile, bool>> where, Func<IQueryable<KpiHelpFile>, IOrderedQueryable<KpiHelpFile>> orderBy, List<Expression<Func<KpiHelpFile, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<KpiHelpFile, bool>> where, List<Expression<Func<KpiHelpFile, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(KpiHelpFile item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("KpiHelpFile");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(KpiHelpFile item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("KpiHelpFile");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(KpiHelpFile item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("KpiHelpFile");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
