﻿
/*
	File generated by NetTiers templates [www.NetTiers.com]
	Important: Do not modify this file. Edit the file QuestionnaireReportOverviewSubQuery3.cs instead.
*/

#region Using Directives
using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Security.Permissions;
using System.Xml.Serialization;
using System.Runtime.Serialization;
using System.Security;
using System.Data;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Entities.Validation;
using Entities = KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;


using Microsoft.Practices.EnterpriseLibrary.Logging;

#endregion 

namespace KaiZen.CSMS.Services
{		
	
	///<summary>
	/// An object representation of the 'QuestionnaireReportOverview_SubQuery3' View.
	///</summary>
	/// <remarks>
	/// IMPORTANT!!! You should not modify this partial  class, modify the QuestionnaireReportOverviewSubQuery3.cs file instead.
	/// All custom implementations should be done in the <see cref="QuestionnaireReportOverviewSubQuery3"/> class.
	/// </remarks>
	[DataObject]
	public partial class QuestionnaireReportOverviewSubQuery3ServiceBase : ServiceViewBase<QuestionnaireReportOverviewSubQuery3>
	{

		#region Constructors
		///<summary>
		/// Creates a new <see cref="QuestionnaireReportOverviewSubQuery3"/> instance .
		///</summary>
		public QuestionnaireReportOverviewSubQuery3ServiceBase() : base()
		{
		}
		
		///<summary>
		/// A simple factory method to create a new <see cref="QuestionnaireReportOverviewSubQuery3"/> instance.
		///</summary>
		///<param name="_companyId"></param>
		///<param name="_kwiSponsorName"></param>
		///<param name="_kwiSponsor"></param>
		///<param name="_kwiSpaName"></param>
		///<param name="_kwiSpa"></param>
		///<param name="_pinSponsorName"></param>
		///<param name="_pinSponsor"></param>
		///<param name="_pinSpaName"></param>
		///<param name="_pinSpa"></param>
		///<param name="_wgpSponsorName"></param>
		///<param name="_wgpSponsor"></param>
		///<param name="_wgpSpaName"></param>
		///<param name="_wgpSpa"></param>
		///<param name="_hunSponsorName"></param>
		///<param name="_hunSponsor"></param>
		///<param name="_hunSpaName"></param>
		///<param name="_hunSpa"></param>
		///<param name="_wdlSponsorName"></param>
		///<param name="_wdlSponsor"></param>
		///<param name="_wdlSpaName"></param>
		///<param name="_wdlSpa"></param>
		///<param name="_bunSponsorName"></param>
		///<param name="_bunSponsor"></param>
		///<param name="_bunSpaName"></param>
		///<param name="_bunSpa"></param>
		///<param name="_fmlSponsorName"></param>
		///<param name="_fmlSponsor"></param>
		///<param name="_fmlSpaName"></param>
		///<param name="_fmlSpa"></param>
		///<param name="_bgnSponsorName"></param>
		///<param name="_bgnSponsor"></param>
		///<param name="_bgnSpaName"></param>
		///<param name="_bgnSpa"></param>
		///<param name="_ceSponsorName"></param>
		///<param name="_ceSponsor"></param>
		///<param name="_ceSpaName"></param>
		///<param name="_ceSpa"></param>
		///<param name="_angSponsorName"></param>
		///<param name="_angSponsor"></param>
		///<param name="_angSpaName"></param>
		///<param name="_angSpa"></param>
		///<param name="_ptlSponsorName"></param>
		///<param name="_ptlSponsor"></param>
		///<param name="_ptlSpaName"></param>
		///<param name="_ptlSpa"></param>
		///<param name="_pthSponsorName"></param>
		///<param name="_pthSponsor"></param>
		///<param name="_pthSpaName"></param>
		///<param name="_pthSpa"></param>
		///<param name="_pelSponsorName"></param>
		///<param name="_pelSponsor"></param>
		///<param name="_pelSpaName"></param>
		///<param name="_pelSpa"></param>
		///<param name="_arpSponsorName"></param>
		///<param name="_arpSponsor"></param>
		///<param name="_arpSpaName"></param>
		///<param name="_arpSpa"></param>
		///<param name="_yenSponsorName"></param>
		///<param name="_yenSponsor"></param>
		///<param name="_yenSpaName"></param>
		///<param name="_yenSpa"></param>
		public static QuestionnaireReportOverviewSubQuery3 CreateQuestionnaireReportOverviewSubQuery3(System.Int32 _companyId, System.String _kwiSponsorName, System.Int32? _kwiSponsor, System.String _kwiSpaName, System.Int32? _kwiSpa, System.String _pinSponsorName, System.Int32? _pinSponsor, System.String _pinSpaName, System.Int32? _pinSpa, System.String _wgpSponsorName, System.Int32? _wgpSponsor, System.String _wgpSpaName, System.Int32? _wgpSpa, System.String _hunSponsorName, System.Int32? _hunSponsor, System.String _hunSpaName, System.Int32? _hunSpa, System.String _wdlSponsorName, System.Int32? _wdlSponsor, System.String _wdlSpaName, System.Int32? _wdlSpa, System.String _bunSponsorName, System.Int32? _bunSponsor, System.String _bunSpaName, System.Int32? _bunSpa, System.String _fmlSponsorName, System.Int32? _fmlSponsor, System.String _fmlSpaName, System.Int32? _fmlSpa, System.String _bgnSponsorName, System.Int32? _bgnSponsor, System.String _bgnSpaName, System.Int32? _bgnSpa, System.String _ceSponsorName, System.Int32? _ceSponsor, System.String _ceSpaName, System.Int32? _ceSpa, System.String _angSponsorName, System.Int32? _angSponsor, System.String _angSpaName, System.Int32? _angSpa, System.String _ptlSponsorName, System.Int32? _ptlSponsor, System.String _ptlSpaName, System.Int32? _ptlSpa, System.String _pthSponsorName, System.Int32? _pthSponsor, System.String _pthSpaName, System.Int32? _pthSpa, System.String _pelSponsorName, System.Int32? _pelSponsor, System.String _pelSpaName, System.Int32? _pelSpa, System.String _arpSponsorName, System.Int32? _arpSponsor, System.String _arpSpaName, System.Int32? _arpSpa, System.String _yenSponsorName, System.Int32? _yenSponsor, System.String _yenSpaName, System.Int32? _yenSpa)
		{
			QuestionnaireReportOverviewSubQuery3 newEntityQuestionnaireReportOverviewSubQuery3 = new QuestionnaireReportOverviewSubQuery3();
			newEntityQuestionnaireReportOverviewSubQuery3.CompanyId  = _companyId;
			newEntityQuestionnaireReportOverviewSubQuery3.KwiSponsorName  = _kwiSponsorName;
			newEntityQuestionnaireReportOverviewSubQuery3.KwiSponsor  = _kwiSponsor;
			newEntityQuestionnaireReportOverviewSubQuery3.KwiSpaName  = _kwiSpaName;
			newEntityQuestionnaireReportOverviewSubQuery3.KwiSpa  = _kwiSpa;
			newEntityQuestionnaireReportOverviewSubQuery3.PinSponsorName  = _pinSponsorName;
			newEntityQuestionnaireReportOverviewSubQuery3.PinSponsor  = _pinSponsor;
			newEntityQuestionnaireReportOverviewSubQuery3.PinSpaName  = _pinSpaName;
			newEntityQuestionnaireReportOverviewSubQuery3.PinSpa  = _pinSpa;
			newEntityQuestionnaireReportOverviewSubQuery3.WgpSponsorName  = _wgpSponsorName;
			newEntityQuestionnaireReportOverviewSubQuery3.WgpSponsor  = _wgpSponsor;
			newEntityQuestionnaireReportOverviewSubQuery3.WgpSpaName  = _wgpSpaName;
			newEntityQuestionnaireReportOverviewSubQuery3.WgpSpa  = _wgpSpa;
			newEntityQuestionnaireReportOverviewSubQuery3.HunSponsorName  = _hunSponsorName;
			newEntityQuestionnaireReportOverviewSubQuery3.HunSponsor  = _hunSponsor;
			newEntityQuestionnaireReportOverviewSubQuery3.HunSpaName  = _hunSpaName;
			newEntityQuestionnaireReportOverviewSubQuery3.HunSpa  = _hunSpa;
			newEntityQuestionnaireReportOverviewSubQuery3.WdlSponsorName  = _wdlSponsorName;
			newEntityQuestionnaireReportOverviewSubQuery3.WdlSponsor  = _wdlSponsor;
			newEntityQuestionnaireReportOverviewSubQuery3.WdlSpaName  = _wdlSpaName;
			newEntityQuestionnaireReportOverviewSubQuery3.WdlSpa  = _wdlSpa;
			newEntityQuestionnaireReportOverviewSubQuery3.BunSponsorName  = _bunSponsorName;
			newEntityQuestionnaireReportOverviewSubQuery3.BunSponsor  = _bunSponsor;
			newEntityQuestionnaireReportOverviewSubQuery3.BunSpaName  = _bunSpaName;
			newEntityQuestionnaireReportOverviewSubQuery3.BunSpa  = _bunSpa;
			newEntityQuestionnaireReportOverviewSubQuery3.FmlSponsorName  = _fmlSponsorName;
			newEntityQuestionnaireReportOverviewSubQuery3.FmlSponsor  = _fmlSponsor;
			newEntityQuestionnaireReportOverviewSubQuery3.FmlSpaName  = _fmlSpaName;
			newEntityQuestionnaireReportOverviewSubQuery3.FmlSpa  = _fmlSpa;
			newEntityQuestionnaireReportOverviewSubQuery3.BgnSponsorName  = _bgnSponsorName;
			newEntityQuestionnaireReportOverviewSubQuery3.BgnSponsor  = _bgnSponsor;
			newEntityQuestionnaireReportOverviewSubQuery3.BgnSpaName  = _bgnSpaName;
			newEntityQuestionnaireReportOverviewSubQuery3.BgnSpa  = _bgnSpa;
			newEntityQuestionnaireReportOverviewSubQuery3.CeSponsorName  = _ceSponsorName;
			newEntityQuestionnaireReportOverviewSubQuery3.CeSponsor  = _ceSponsor;
			newEntityQuestionnaireReportOverviewSubQuery3.CeSpaName  = _ceSpaName;
			newEntityQuestionnaireReportOverviewSubQuery3.CeSpa  = _ceSpa;
			newEntityQuestionnaireReportOverviewSubQuery3.AngSponsorName  = _angSponsorName;
			newEntityQuestionnaireReportOverviewSubQuery3.AngSponsor  = _angSponsor;
			newEntityQuestionnaireReportOverviewSubQuery3.AngSpaName  = _angSpaName;
			newEntityQuestionnaireReportOverviewSubQuery3.AngSpa  = _angSpa;
			newEntityQuestionnaireReportOverviewSubQuery3.PtlSponsorName  = _ptlSponsorName;
			newEntityQuestionnaireReportOverviewSubQuery3.PtlSponsor  = _ptlSponsor;
			newEntityQuestionnaireReportOverviewSubQuery3.PtlSpaName  = _ptlSpaName;
			newEntityQuestionnaireReportOverviewSubQuery3.PtlSpa  = _ptlSpa;
			newEntityQuestionnaireReportOverviewSubQuery3.PthSponsorName  = _pthSponsorName;
			newEntityQuestionnaireReportOverviewSubQuery3.PthSponsor  = _pthSponsor;
			newEntityQuestionnaireReportOverviewSubQuery3.PthSpaName  = _pthSpaName;
			newEntityQuestionnaireReportOverviewSubQuery3.PthSpa  = _pthSpa;
			newEntityQuestionnaireReportOverviewSubQuery3.PelSponsorName  = _pelSponsorName;
			newEntityQuestionnaireReportOverviewSubQuery3.PelSponsor  = _pelSponsor;
			newEntityQuestionnaireReportOverviewSubQuery3.PelSpaName  = _pelSpaName;
			newEntityQuestionnaireReportOverviewSubQuery3.PelSpa  = _pelSpa;
			newEntityQuestionnaireReportOverviewSubQuery3.ArpSponsorName  = _arpSponsorName;
			newEntityQuestionnaireReportOverviewSubQuery3.ArpSponsor  = _arpSponsor;
			newEntityQuestionnaireReportOverviewSubQuery3.ArpSpaName  = _arpSpaName;
			newEntityQuestionnaireReportOverviewSubQuery3.ArpSpa  = _arpSpa;
			newEntityQuestionnaireReportOverviewSubQuery3.YenSponsorName  = _yenSponsorName;
			newEntityQuestionnaireReportOverviewSubQuery3.YenSponsor  = _yenSponsor;
			newEntityQuestionnaireReportOverviewSubQuery3.YenSpaName  = _yenSpaName;
			newEntityQuestionnaireReportOverviewSubQuery3.YenSpa  = _yenSpa;
			return newEntityQuestionnaireReportOverviewSubQuery3;
		}
		#endregion Constructors

		#region Fields
		//private static SecurityContext<QuestionnaireReportOverviewSubQuery3> securityContext = new SecurityContext<QuestionnaireReportOverviewSubQuery3>();
		private static readonly string layerExceptionPolicy = "ServiceLayerExceptionPolicy";
		private static readonly bool noTranByDefault = false;
		private static readonly int defaultMaxRecords = 10000;
		#endregion 
		
		#region Data Access Methods
			
		#region Get 
		/// <summary>
		/// Attempts to do a parameterized version of a simple whereclause. 
		/// Returns rows meeting the whereClause condition from the DataSource.
		/// </summary>
		/// <param name="whereClause">Specifies the condition for the rows returned by a query (Name='John Doe', Name='John Doe' AND Id='1', Name='John Doe' OR Id='1').</param>
        /// <param name="orderBy">Specifies the sort criteria for the rows in the DataSource (Name ASC; BirthDay DESC, Name ASC);</param>
		/// <remarks>Does NOT Support Advanced Operations such as SubSelects.  See GetPaged for that functionality.</remarks>
		/// <returns>Returns a typed collection of Entity objects.</returns>
		public override VList<QuestionnaireReportOverviewSubQuery3> Get(string whereClause, string orderBy)
		{
			int totalCount = -1;
			return Get(whereClause, orderBy, 0, defaultMaxRecords, out totalCount);
		}

		/// <summary>
		/// Returns rows meeting the whereClause condition from the DataSource.
		/// </summary>
		/// <param name="whereClause">Specifies the condition for the rows returned by a query (Name='John Doe', Name='John Doe' AND Id='1', Name='John Doe' OR Id='1').</param>
        /// <param name="orderBy">Specifies the sort criteria for the rows in the DataSource (Name ASC; BirthDay DESC, Name ASC);</param>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="totalCount">out parameter to get total records for query</param>
		/// <remarks>Does NOT Support Advanced Operations such as SubSelects.  See GetPaged for that functionality.</remarks>
		/// <returns>Returns a typed collection TList{QuestionnaireReportOverviewSubQuery3} of <c>QuestionnaireReportOverviewSubQuery3</c> objects.</returns>
		public override VList<QuestionnaireReportOverviewSubQuery3> Get(string whereClause, string orderBy, int start, int pageLength, out int totalCount)
		{
			// throws security exception if not authorized
			//SecurityContext.IsAuthorized("Get");
								
			// get this data
			VList<QuestionnaireReportOverviewSubQuery3> list = null;
			totalCount = -1;
			TransactionManager transactionManager = null; 

			try
            {	
				//since this is a read operation, don't create a tran by default, only use tran if provided to us for custom isolation level
				transactionManager = ConnectionScope.ValidateOrCreateTransaction(noTranByDefault);
				NetTiersProvider dataProvider = ConnectionScope.Current.DataProvider;
					
				//Access repository
				list = dataProvider.QuestionnaireReportOverviewSubQuery3Provider.Get(transactionManager, whereClause, orderBy, start, pageLength, out totalCount);
				
				//if borrowed tran, leave open for next call
			}
            catch (Exception exc)
            {
				//if open, rollback, it's possible this is part of a larger commit
                if (transactionManager != null && transactionManager.IsOpen) 
					transactionManager.Rollback();
				
				//Handle exception based on policy
                if (DomainUtil.HandleException(exc, layerExceptionPolicy)) 
					throw;
			}
			return list;
		}
		
		#endregion Get Methods
		
		#region GetAll
		/// <summary>
		/// Get a complete collection of <see cref="QuestionnaireReportOverviewSubQuery3" /> entities.
		/// </summary>
		/// <returns></returns>
		public virtual VList<QuestionnaireReportOverviewSubQuery3> GetAll() 
		{
			int totalCount = -1;
			return GetAll(0, defaultMaxRecords, out totalCount);
		}

       
		/// <summary>
		/// Get a set portion of a complete list of <see cref="QuestionnaireReportOverviewSubQuery3" /> entities
		/// </summary>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="totalCount">out parameter, number of total rows in given query.</param>
		/// <returns>a <see cref="TList{QuestionnaireReportOverviewSubQuery3}"/> </returns>
		public override VList<QuestionnaireReportOverviewSubQuery3> GetAll(int start, int pageLength, out int totalCount) 
		{
			// throws security exception if not authorized
			//SecurityContext.IsAuthorized("GetAll");
			
			// get this data
			VList<QuestionnaireReportOverviewSubQuery3> list = null;
			totalCount = -1;
			TransactionManager transactionManager = null; 

			try
            {	
				//since this is a read operation, don't create a tran by default, only use tran if provided to us for custom isolation level
				transactionManager = ConnectionScope.ValidateOrCreateTransaction(noTranByDefault);
				NetTiersProvider dataProvider = ConnectionScope.Current.DataProvider;					

				//Access repository
				list = dataProvider.QuestionnaireReportOverviewSubQuery3Provider.GetAll(transactionManager, start, pageLength, out totalCount);	
			}
            catch (Exception exc)
            {
				//if open, rollback, it's possible this is part of a larger commit
                if (transactionManager != null && transactionManager.IsOpen) 
					transactionManager.Rollback();
				
				//Handle exception based on policy
                if (DomainUtil.HandleException(exc, layerExceptionPolicy)) 
					throw;
			}
			return list;
		}
		#endregion GetAll

		#region GetPaged
		/// <summary>
		/// Gets a page of <see cref="TList{QuestionnaireReportOverviewSubQuery3}" /> rows from the DataSource.
		/// </summary>
		/// <param name="totalCount">Out Parameter, Number of rows in the DataSource.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of <c>QuestionnaireReportOverviewSubQuery3</c> objects.</returns>
		public virtual VList<QuestionnaireReportOverviewSubQuery3> GetPaged(out int totalCount)
		{
			return GetPaged(null, null, 0, defaultMaxRecords, out totalCount);
		}
		
		/// <summary>
		/// Gets a page of <see cref="TList{QuestionnaireReportOverviewSubQuery3}" /> rows from the DataSource.
		/// </summary>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="totalCount">Number of rows in the DataSource.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of <c>QuestionnaireReportOverviewSubQuery3</c> objects.</returns>
		public virtual VList<QuestionnaireReportOverviewSubQuery3> GetPaged(int start, int pageLength, out int totalCount)
		{
			return GetPaged(null, null, start, pageLength, out totalCount);
		}

		/// <summary>
		/// Gets a page of entity rows with a <see cref="TList{QuestionnaireReportOverviewSubQuery3}" /> from the DataSource with a where clause and order by clause.
		/// </summary>
		/// <param name="whereClause">Specifies the condition for the rows returned by a query (Name='John Doe', Name='John Doe' AND Id='1', Name='John Doe' OR Id='1').</param>
		/// <param name="orderBy">Specifies the sort criteria for the rows in the DataSource (Name ASC; BirthDay DESC, Name ASC).</param>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="totalCount">Out Parameter, Number of rows in the DataSource.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of <c>QuestionnaireReportOverviewSubQuery3</c> objects.</returns>
		public override VList<QuestionnaireReportOverviewSubQuery3> GetPaged(string whereClause,string orderBy, int start, int pageLength, out int totalCount)
		{
			// throws security exception if not authorized
			//SecurityContext.IsAuthorized("GetPaged");
			
			// get this data
			VList<QuestionnaireReportOverviewSubQuery3> list = null;
			totalCount = -1;
			TransactionManager transactionManager = null; 

			try
            {	
				//since this is a read operation, don't create a tran by default, only use tran if provided to us for custom isolation level
				transactionManager = ConnectionScope.ValidateOrCreateTransaction(noTranByDefault);
				NetTiersProvider dataProvider = ConnectionScope.Current.DataProvider;
					
				//Access repository
				list = dataProvider.QuestionnaireReportOverviewSubQuery3Provider.GetPaged(transactionManager, whereClause, orderBy, start, pageLength, out totalCount);
				
				//if borrowed tran, leave open for next call
			}
            catch (Exception exc)
            {
				//if open, rollback, it's possible this is part of a larger commit
                if (transactionManager != null && transactionManager.IsOpen) 
					transactionManager.Rollback();
				
				//Handle exception based on policy
                if (DomainUtil.HandleException(exc, layerExceptionPolicy)) 
					throw;
			}
			return list;			
		}
		
		/// <summary>
		/// Gets the number of rows in the DataSource that match the specified whereClause.
		/// This method is only provided as a workaround for the ObjectDataSource's need to 
		/// execute another method to discover the total count instead of using another param, like our out param.  
		/// This method should be avoided if using the ObjectDataSource or another method.
		/// </summary>
		/// <param name="whereClause">Specifies the condition for the rows returned by a query (Name='John Doe', Name='John Doe' AND Id='1', Name='John Doe' OR Id='1').</param>
		/// <param name="totalCount">Number of rows in the DataSource.</param>
		/// <returns>Returns the number of rows.</returns>
		public int GetTotalItems(string whereClause, out int totalCount)
		{
			GetPaged(whereClause, null, 0, defaultMaxRecords, out totalCount);
			return totalCount;
		}
		#endregion GetPaged	

		#region Find Methods

		/// <summary>
		/// 	Returns rows from the DataSource that meet the parameter conditions.
		/// </summary>
		/// <param name="parameters">A collection of <see cref="SqlFilterParameter"/> objects.</param>
		/// <returns>Returns a typed collection of <c>QuestionnaireReportOverviewSubQuery3</c> objects.</returns>
		public virtual VList<QuestionnaireReportOverviewSubQuery3> Find(IFilterParameterCollection parameters)
		{
			return Find(parameters, null);
		}
		
		/// <summary>
		/// 	Returns rows from the DataSource that meet the parameter conditions.
		/// </summary>
		/// <param name="parameters">A collection of <see cref="SqlFilterParameter"/> objects.</param>
		/// <param name="orderBy">Specifies the sort criteria for the rows in the DataSource (Name ASC; BirthDay DESC, Name ASC);</param>
		/// <returns>Returns a typed collection of <c>QuestionnaireReportOverviewSubQuery3</c> objects.</returns>
		public virtual VList<QuestionnaireReportOverviewSubQuery3> Find(IFilterParameterCollection parameters, string orderBy)
		{
			int count = 0;
			return Find(parameters, orderBy, 0, defaultMaxRecords, out count);
		}
		
		/// <summary>
		/// 	Returns rows from the DataSource that meet the parameter conditions.
		/// </summary>
		/// <param name="parameters">A collection of <see cref="SqlFilterParameter"/> objects.</param>
		/// <param name="orderBy">Specifies the sort criteria for the rows in the DataSource (Name ASC; BirthDay DESC, Name ASC);</param>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out. The number of rows that match this query.</param>
		/// <returns>Returns a typed collection of <c>QuestionnaireReportOverviewSubQuery3</c> objects.</returns>
		public override VList<QuestionnaireReportOverviewSubQuery3> Find(IFilterParameterCollection parameters, string orderBy, int start, int pageLength, out int count)
		{
			// throws security exception if not authorized
			//SecurityContext.IsAuthorized("Find");
								
			// get this data
			TransactionManager transactionManager = null; 
			VList<QuestionnaireReportOverviewSubQuery3> list = null;
			count = -1;
			
			try
            {	
				//since this is a read operation, don't create a tran by default, only use tran if provided to us for custom isolation level
				transactionManager = ConnectionScope.ValidateOrCreateTransaction(noTranByDefault);
				NetTiersProvider dataProvider = ConnectionScope.Current.DataProvider;
					
				//Access repository
				list = dataProvider.QuestionnaireReportOverviewSubQuery3Provider.Find(transactionManager, parameters, orderBy, start, pageLength, out count);
			}
            catch (Exception exc)
            {
				//if open, rollback, it's possible this is part of a larger commit
                if (transactionManager != null && transactionManager.IsOpen) 
					transactionManager.Rollback();
				
				//Handle exception based on policy
                if (DomainUtil.HandleException(exc, layerExceptionPolicy)) 
					throw;
			}
			
			return list;
		}
		
		#endregion Find Methods
		
		#region Custom Methods
		#endregion
		
		#endregion Data Access Methods
		
	
	}//End Class
} // end namespace



