﻿<%@ Control Language="C#" AutoEventWireup="true"
    Inherits="UserControls_FinancialReport" Codebehind="FinancialReport.ascx.cs" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Src="../Other/ExportButtons.ascx" TagName="ExportButtons" TagPrefix="uc1" %>
<%@ Register assembly="DevExpress.Web.v14.1" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<table border="0" cellpadding="2" cellspacing="0" width="100%">
    <tr>
        <td class="pageName" colspan="3" style="height: 17px">
            <span class="bodycopy"><span class="title">Financial Reports</span><br />
                <span class="date">Contractor Details - Hours</span><br />
                <img src="images/grfc_dottedline.gif" width="24" height="1">&nbsp;</span>
        </td>
    </tr>
    <tr>
        <td colspan="3" style="height: 17px">
            <table border="0">
                <tr>
                    <td style="width: 82px; height: 10px; text-align: right">
                        <strong>Site/Region:</strong>
                    </td>
                    <td style="width: 131px; height: 10px; text-align: right">
                        <dxe:ASPxComboBox ID="cbRegionSite" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                            CssPostfix="Office2003Blue" IncrementalFilteringMode="StartsWith" SelectedIndex="0"
                            ValueType="System.Int32" Width="140px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                            <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                            </LoadingPanelImage>
                            <ButtonStyle Width="13px">
                            </ButtonStyle>
                        </dxe:ASPxComboBox>
                    </td>
                    <td style="width: 46px; height: 10px; text-align: right">
                        <strong>Month:</strong>
                    </td>
                    <td style="width: 83px; height: 10px; text-align: left">
                        <dxe:ASPxComboBox ID="ddlMonth" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                            CssPostfix="Office2003Blue" IncrementalFilteringMode="StartsWith" SelectedIndex="0"
                            ValueType="System.Int32" Width="90px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                            <Items>
                                <dxe:ListEditItem Text="January" Value="1" />
                                <dxe:ListEditItem Text="February" Value="2" />
                                <dxe:ListEditItem Text="March" Value="3" />
                                <dxe:ListEditItem Text="April" Value="4" />
                                <dxe:ListEditItem Text="May" Value="5" />
                                <dxe:ListEditItem Text="June" Value="6" />
                                <dxe:ListEditItem Text="July" Value="7" />
                                <dxe:ListEditItem Text="August" Value="8" />
                                <dxe:ListEditItem Text="September" Value="9" />
                                <dxe:ListEditItem Text="October" Value="10" />
                                <dxe:ListEditItem Text="November" Value="11" />
                                <dxe:ListEditItem Text="December" Value="12" />
                            </Items>
                            <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                            </LoadingPanelImage>
                            <ButtonStyle Width="13px">
                            </ButtonStyle>
                        </dxe:ASPxComboBox>
                    </td>
                    <td style="width: 59px; height: 10px; text-align: left">
                        <dxe:ASPxComboBox ID="ddlYear" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                            CssPostfix="Office2003Blue" IncrementalFilteringMode="StartsWith" ValueType="System.Int32"
                            Width="60px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                            <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                            </LoadingPanelImage>
                            <ButtonStyle Cursor="pointer" Width="13px">
                            </ButtonStyle>
                        </dxe:ASPxComboBox>
                    </td>
                    <td style="width: 95px; height: 10px; text-align: right">
                        <dxe:ASPxButton ID="ASPxButton1" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                            CssPostfix="Office2003Blue" OnClick="ASPxButton1_Click" Text="Search / Go" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                        </dxe:ASPxButton>
                    </td>
                </tr>
            </table>
            <dxwgv:ASPxGridView ID="grid" ClientInstanceName="grid" runat="server" AutoGenerateColumns="False"
                CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue"
                DataSourceID="sqldsFR" Width="900px">
                <TotalSummary>
                    <dxwgv:ASPxSummaryItem FieldName="Projects" ShowInColumn="Projects" SummaryType="Count" />
                    <dxwgv:ASPxSummaryItem FieldName="ManHours" DisplayFormat="N" ShowInColumn="Man Hours"
                        SummaryType="Sum" />
                </TotalSummary>
                <SettingsPager AlwaysShowPager="True" PageSize="20">
                    <AllButton Visible="True">
                    </AllButton>
                </SettingsPager>
                <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                    <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
                    </LoadingPanelOnStatusBar>
                    <LoadingPanel Url="~/App_Themes/Office2003BlueOld/GridView/Loading.gif"><%--Enhancement_023:change by debashis for testing platform upgradation--%>
                    </LoadingPanel>
                </Images>
                <Settings ShowFooter="True" ShowGroupedColumns="True" ShowGroupPanel="True" />
                <ImagesFilterControl>
                    <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
                    </LoadingPanel>
                </ImagesFilterControl>
                <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue">
                    <Header ImageSpacing="5px" SortingImageSpacing="5px">
                    </Header>
                    <LoadingPanel ImageSpacing="10px">
                    </LoadingPanel>
                </Styles>
                <Columns>
                    <dxwgv:GridViewDataTextColumn Caption="Site" FieldName="SiteName" GroupIndex="0"
                        SortIndex="0" SortOrder="Ascending" VisibleIndex="0">
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn FieldName="Year" ReadOnly="True" VisibleIndex="1">
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn FieldName="Month" ReadOnly="True" VisibleIndex="2">
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn FieldName="CompanyName" VisibleIndex="3">
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn FieldName="ManHours" VisibleIndex="4">
                        <PropertiesTextEdit DisplayFormatString="N">
                        </PropertiesTextEdit>
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn FieldName="Projects" VisibleIndex="5">
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn FieldName="Type" VisibleIndex="6">
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn FieldName="FTE" VisibleIndex="7">
                    </dxwgv:GridViewDataTextColumn>
                </Columns>
                <StylesEditors>
                    <ProgressBar Height="25px">
                    </ProgressBar>
                </StylesEditors>
            </dxwgv:ASPxGridView>
        </td>
    </tr>
    <tr>
        <td colspan="3" style="text-align: right">
            Number of Rows to show per page:<asp:DropDownList ID="DropDownList1" runat="server"
                AutoPostBack="True" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged">
                <asp:ListItem>10</asp:ListItem>
                <asp:ListItem>20</asp:ListItem>
                <asp:ListItem>50</asp:ListItem>
                <asp:ListItem>All</asp:ListItem>
            </asp:DropDownList>
        </td>
    </tr>
    <tr align="right">
        <td colspan="3" style="padding-top:6px; text-align: right; text-align: -moz-right; width: 100%"
            align="right">
            <div align="right">
                <uc1:ExportButtons ID="ucExportButtons" runat="server" />
            </div>
        </td>
    </tr>
</table>
<asp:SqlDataSource ID="sqldsYear" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="select distinct YEAR(dbo.KPI.kpiDateTime) as 'Year' from kpi order by YEAR(dbo.KPI.kpiDateTime) desc" SelectCommandType="Text">
</asp:SqlDataSource>
<asp:SqlDataSource ID="sqldsFR" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="_AdHoc_FinancialReport1" SelectCommandType="StoredProcedure">
    <SelectParameters>
        <asp:SessionParameter DefaultValue="12" Name="Month" SessionField="spVar_Month" Type="Int32" />
        <asp:SessionParameter DefaultValue="2007" Name="Year" SessionField="spVar_Year" Type="Int32" />
    </SelectParameters>
</asp:SqlDataSource>
