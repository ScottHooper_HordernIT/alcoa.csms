﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.KpiProjectListProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(KpiProjectListDataSourceDesigner))]
	public class KpiProjectListDataSource : ProviderDataSource<KpiProjectList, KpiProjectListKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the KpiProjectListDataSource class.
		/// </summary>
		public KpiProjectListDataSource() : base(new KpiProjectListService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the KpiProjectListDataSourceView used by the KpiProjectListDataSource.
		/// </summary>
		protected KpiProjectListDataSourceView KpiProjectListView
		{
			get { return ( View as KpiProjectListDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the KpiProjectListDataSource control invokes to retrieve data.
		/// </summary>
		public KpiProjectListSelectMethod SelectMethod
		{
			get
			{
				KpiProjectListSelectMethod selectMethod = KpiProjectListSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (KpiProjectListSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the KpiProjectListDataSourceView class that is to be
		/// used by the KpiProjectListDataSource.
		/// </summary>
		/// <returns>An instance of the KpiProjectListDataSourceView class.</returns>
		protected override BaseDataSourceView<KpiProjectList, KpiProjectListKey> GetNewDataSourceView()
		{
			return new KpiProjectListDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the KpiProjectListDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class KpiProjectListDataSourceView : ProviderDataSourceView<KpiProjectList, KpiProjectListKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the KpiProjectListDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the KpiProjectListDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public KpiProjectListDataSourceView(KpiProjectListDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal KpiProjectListDataSource KpiProjectListOwner
		{
			get { return Owner as KpiProjectListDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal KpiProjectListSelectMethod SelectMethod
		{
			get { return KpiProjectListOwner.SelectMethod; }
			set { KpiProjectListOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal KpiProjectListService KpiProjectListProvider
		{
			get { return Provider as KpiProjectListService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<KpiProjectList> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<KpiProjectList> results = null;
			KpiProjectList item;
			count = 0;
			
			System.Int32 _kpiProjectListId;
			System.String _projectNumber;
			System.String _projectDescription;

			switch ( SelectMethod )
			{
				case KpiProjectListSelectMethod.Get:
					KpiProjectListKey entityKey  = new KpiProjectListKey();
					entityKey.Load(values);
					item = KpiProjectListProvider.Get(entityKey);
					results = new TList<KpiProjectList>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case KpiProjectListSelectMethod.GetAll:
                    results = KpiProjectListProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case KpiProjectListSelectMethod.GetPaged:
					results = KpiProjectListProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case KpiProjectListSelectMethod.Find:
					if ( FilterParameters != null )
						results = KpiProjectListProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = KpiProjectListProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case KpiProjectListSelectMethod.GetByKpiProjectListId:
					_kpiProjectListId = ( values["KpiProjectListId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["KpiProjectListId"], typeof(System.Int32)) : (int)0;
					item = KpiProjectListProvider.GetByKpiProjectListId(_kpiProjectListId);
					results = new TList<KpiProjectList>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				case KpiProjectListSelectMethod.GetByProjectNumber:
					_projectNumber = ( values["ProjectNumber"] != null ) ? (System.String) EntityUtil.ChangeType(values["ProjectNumber"], typeof(System.String)) : string.Empty;
					item = KpiProjectListProvider.GetByProjectNumber(_projectNumber);
					results = new TList<KpiProjectList>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case KpiProjectListSelectMethod.GetByProjectDescription:
					_projectDescription = ( values["ProjectDescription"] != null ) ? (System.String) EntityUtil.ChangeType(values["ProjectDescription"], typeof(System.String)) : string.Empty;
					item = KpiProjectListProvider.GetByProjectDescription(_projectDescription);
					results = new TList<KpiProjectList>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// FK
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == KpiProjectListSelectMethod.Get || SelectMethod == KpiProjectListSelectMethod.GetByKpiProjectListId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				KpiProjectList entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					KpiProjectListProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<KpiProjectList> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			KpiProjectListProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region KpiProjectListDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the KpiProjectListDataSource class.
	/// </summary>
	public class KpiProjectListDataSourceDesigner : ProviderDataSourceDesigner<KpiProjectList, KpiProjectListKey>
	{
		/// <summary>
		/// Initializes a new instance of the KpiProjectListDataSourceDesigner class.
		/// </summary>
		public KpiProjectListDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public KpiProjectListSelectMethod SelectMethod
		{
			get { return ((KpiProjectListDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new KpiProjectListDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region KpiProjectListDataSourceActionList

	/// <summary>
	/// Supports the KpiProjectListDataSourceDesigner class.
	/// </summary>
	internal class KpiProjectListDataSourceActionList : DesignerActionList
	{
		private KpiProjectListDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the KpiProjectListDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public KpiProjectListDataSourceActionList(KpiProjectListDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public KpiProjectListSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion KpiProjectListDataSourceActionList
	
	#endregion KpiProjectListDataSourceDesigner
	
	#region KpiProjectListSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the KpiProjectListDataSource.SelectMethod property.
	/// </summary>
	public enum KpiProjectListSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByKpiProjectListId method.
		/// </summary>
		GetByKpiProjectListId,
		/// <summary>
		/// Represents the GetByProjectNumber method.
		/// </summary>
		GetByProjectNumber,
		/// <summary>
		/// Represents the GetByProjectDescription method.
		/// </summary>
		GetByProjectDescription
	}
	
	#endregion KpiProjectListSelectMethod

	#region KpiProjectListFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="KpiProjectList"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class KpiProjectListFilter : SqlFilter<KpiProjectListColumn>
	{
	}
	
	#endregion KpiProjectListFilter

	#region KpiProjectListExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="KpiProjectList"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class KpiProjectListExpressionBuilder : SqlExpressionBuilder<KpiProjectListColumn>
	{
	}
	
	#endregion KpiProjectListExpressionBuilder	

	#region KpiProjectListProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;KpiProjectListChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="KpiProjectList"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class KpiProjectListProperty : ChildEntityProperty<KpiProjectListChildEntityTypes>
	{
	}
	
	#endregion KpiProjectListProperty
}

