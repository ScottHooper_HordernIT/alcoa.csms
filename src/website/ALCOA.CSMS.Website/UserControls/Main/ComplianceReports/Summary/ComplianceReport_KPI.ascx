﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="UserControls_Tiny_ComplianceReports_ComplianceReport_KPI" Codebehind="ComplianceReport_KPI.ascx.cs" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.UI" TagPrefix="asp" %>

<dxwgv:ASPxGridView ID="grid" ClientInstanceName="grid" runat="server" AutoGenerateColumns="False"
    CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue"
    Width="900px" OnHtmlRowCreated="grid_RowCreated" KeyFieldName="CompanyID" OnAutoFilterCellEditorInitialize="grid_AutoFilterCellEditorInitialize">
    <SettingsCustomizationWindow Enabled="True"></SettingsCustomizationWindow>
    <SettingsBehavior ConfirmDelete="True"></SettingsBehavior>
    <ImagesFilterControl>
        <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
        </LoadingPanel>
    </ImagesFilterControl>
    <Styles CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css">
        <Header SortingImageSpacing="5px" ImageSpacing="5px">
        </Header>
        <AlternatingRow Enabled="True">
        </AlternatingRow>
        <LoadingPanel ImageSpacing="10px">
        </LoadingPanel>
    </Styles>
    <SettingsPager Mode="ShowAllRecords" PageSize="100" Visible="False">
        <AllButton Text="All">
        </AllButton>
        <NextPageButton Text="Next &gt;">
        </NextPageButton>
        <PrevPageButton Text="&lt; Prev">
        </PrevPageButton>
    </SettingsPager>
    <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
        <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
        </LoadingPanelOnStatusBar>
        <LoadingPanel Url="~/App_Themes/Office2003BlueOld/GridView/Loading.gif"><%--Enhancement_023:change by debashis for testing platform upgradation--%>
        </LoadingPanel>
    </Images>
    <SettingsCookies CookiesID="NonComplianceKPI" Version="0.1"></SettingsCookies>
    <SettingsText EmptyDataRow="No KPI(s) submitted" CommandClearFilter="Clear"></SettingsText>
    <Columns>
        <dxwgv:GridViewDataComboBoxColumn FieldName="CompanyId" Name="CompanyBox" Caption="Company Name"
            VisibleIndex="0">
            <PropertiesComboBox IncrementalFilteringMode="StartsWith" DataSourceID="sqldsCompaniesList"
                TextField="CompanyName" ValueField="CompanyId" ValueType="System.Int32" DropDownHeight="150px">
            </PropertiesComboBox>
            <EditFormSettings Visible="True"></EditFormSettings>
        </dxwgv:GridViewDataComboBoxColumn>
        <dxwgv:GridViewDataComboBoxColumn FieldName="SiteId" Visible="false" SortOrder="Ascending">
        </dxwgv:GridViewDataComboBoxColumn>
        <dxwgv:GridViewDataComboBoxColumn Caption="Site Name" FieldName="SiteName" Name="SiteBox"
            VisibleIndex="1" SortIndex="1" SortOrder="Ascending">
            <PropertiesComboBox DataSourceID="sqldsResidentialSites" DropDownHeight="150px" TextField="SiteName"
                ValueField="SiteName" ValueType="System.String" IncrementalFilteringMode="StartsWith">
            </PropertiesComboBox>
            <EditFormSettings Visible="True" />
            <Settings SortMode="DisplayText" />
            <DataItemTemplate>
                <asp:Label ID="lblSiteName" runat="server" Text=""></asp:Label>
            </DataItemTemplate>
        </dxwgv:GridViewDataComboBoxColumn>
        <dxwgv:GridViewDataComboBoxColumn Name="gcbResidentialCategory" Caption="Residential Category"
            FieldName="CompanySiteCategoryId" SortIndex="0" SortOrder="Ascending" UnboundType="String"
            VisibleIndex="2" FixedStyle="Left" Width="145px">
            <PropertiesComboBox DataSourceID="CompanySiteCategoryDataSource" DropDownHeight="150px"
                TextField="CategoryDesc" ValueField="CompanySiteCategoryId" ValueType="System.Int32"
                IncrementalFilteringMode="StartsWith">
            </PropertiesComboBox>
            <Settings SortMode="DisplayText" />
        </dxwgv:GridViewDataComboBoxColumn>
        <dxwgv:GridViewDataTextColumn FieldName="Jan" ReadOnly="True" Name="Jan" VisibleIndex="3">
            <DataItemTemplate>
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <asp:Image ID="changeImage1" runat="server" ImageUrl="~/Images/spacer.gif" Visible="true"
                                GenerateEmptyAlternateText="True" />
                        </td>
                        <td style="width: 30px">
                            <asp:Label ID="noKPISubmitted1" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                </table>
            </DataItemTemplate>
            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
            <CellStyle HorizontalAlign="Center">
            </CellStyle>
        </dxwgv:GridViewDataTextColumn>
        <dxwgv:GridViewDataTextColumn FieldName="Feb" ReadOnly="True" Name="Feb" VisibleIndex="4">
            <DataItemTemplate>
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <asp:Image ID="changeImage2" runat="server" ImageUrl="~/Images/spacer.gif" Visible="true"
                                GenerateEmptyAlternateText="True" />
                        </td>
                        <td style="width: 20px">
                            <asp:Label ID="noKPISubmitted2" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                </table>
            </DataItemTemplate>
            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
            <CellStyle HorizontalAlign="Center">
            </CellStyle>
        </dxwgv:GridViewDataTextColumn>
        <dxwgv:GridViewDataTextColumn FieldName="Mar" ReadOnly="True" Name="Mar" VisibleIndex="5">
            <DataItemTemplate>
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <asp:Image ID="changeImage3" runat="server" ImageUrl="~/Images/spacer.gif" Visible="true"
                                GenerateEmptyAlternateText="True" />
                        </td>
                        <td style="width: 20px">
                            <asp:Label ID="noKPISubmitted3" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                </table>
            </DataItemTemplate>
            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
            <CellStyle HorizontalAlign="Center">
            </CellStyle>
        </dxwgv:GridViewDataTextColumn>
        <dxwgv:GridViewDataTextColumn FieldName="Apr" ReadOnly="True" Name="Apr" VisibleIndex="6">
            <DataItemTemplate>
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <asp:Image ID="changeImage4" runat="server" ImageUrl="~/Images/spacer.gif" Visible="true"
                                GenerateEmptyAlternateText="True" />
                        </td>
                        <td style="width: 20px">
                            <asp:Label ID="noKPISubmitted4" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                </table>
            </DataItemTemplate>
            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
            <CellStyle HorizontalAlign="Center">
            </CellStyle>
        </dxwgv:GridViewDataTextColumn>
        <dxwgv:GridViewDataTextColumn FieldName="May" ReadOnly="True" Name="May" VisibleIndex="7">
            <DataItemTemplate>
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <asp:Image ID="changeImage5" runat="server" ImageUrl="~/Images/spacer.gif" Visible="true"
                                GenerateEmptyAlternateText="True" />
                        </td>
                        <td style="width: 20px">
                            <asp:Label ID="noKPISubmitted5" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                </table>
            </DataItemTemplate>
            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
            <CellStyle HorizontalAlign="Center">
            </CellStyle>
        </dxwgv:GridViewDataTextColumn>
        <dxwgv:GridViewDataTextColumn FieldName="Jun" ReadOnly="True" Name="Jun" VisibleIndex="8">
            <DataItemTemplate>
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <asp:Image ID="changeImage6" runat="server" ImageUrl="~/Images/spacer.gif" Visible="true"
                                GenerateEmptyAlternateText="True" />
                        </td>
                        <td style="width: 20px">
                            <asp:Label ID="noKPISubmitted6" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                </table>
            </DataItemTemplate>
            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
            <CellStyle HorizontalAlign="Center">
            </CellStyle>
        </dxwgv:GridViewDataTextColumn>
        <dxwgv:GridViewDataTextColumn FieldName="Jul" ReadOnly="True" Name="Jul" VisibleIndex="9">
            <DataItemTemplate>
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <asp:Image ID="changeImage7" runat="server" ImageUrl="~/Images/spacer.gif" Visible="true"
                                GenerateEmptyAlternateText="True" />
                        </td>
                        <td style="width: 20px">
                            <asp:Label ID="noKPISubmitted7" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                </table>
            </DataItemTemplate>
            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
            <CellStyle HorizontalAlign="Center">
            </CellStyle>
        </dxwgv:GridViewDataTextColumn>
        <dxwgv:GridViewDataTextColumn FieldName="Aug" ReadOnly="True" Name="Aug" VisibleIndex="10">
            <DataItemTemplate>
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <asp:Image ID="changeImage8" runat="server" ImageUrl="~/Images/spacer.gif" Visible="true"
                                GenerateEmptyAlternateText="True" />
                        </td>
                        <td style="width: 20px">
                            <asp:Label ID="noKPISubmitted8" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                </table>
            </DataItemTemplate>
            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
            <CellStyle HorizontalAlign="Center">
            </CellStyle>
        </dxwgv:GridViewDataTextColumn>
        <dxwgv:GridViewDataTextColumn FieldName="Sep" ReadOnly="True" Name="Sep" VisibleIndex="11">
            <DataItemTemplate>
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <asp:Image ID="changeImage9" runat="server" ImageUrl="~/Images/spacer.gif" Visible="true"
                                GenerateEmptyAlternateText="True" />
                        </td>
                        <td style="width: 20px">
                            <asp:Label ID="noKPISubmitted9" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                </table>
            </DataItemTemplate>
            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
            <CellStyle HorizontalAlign="Center">
            </CellStyle>
        </dxwgv:GridViewDataTextColumn>
        <dxwgv:GridViewDataTextColumn FieldName="Oct" ReadOnly="True" Name="Oct" VisibleIndex="12">
            <DataItemTemplate>
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <asp:Image ID="changeImage10" runat="server" ImageUrl="~/Images/spacer.gif" Visible="true"
                                GenerateEmptyAlternateText="True" />
                        </td>
                        <td style="width: 20px">
                            <asp:Label ID="noKPISubmitted10" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                </table>
            </DataItemTemplate>
            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
            <CellStyle HorizontalAlign="Center">
            </CellStyle>
        </dxwgv:GridViewDataTextColumn>
        <dxwgv:GridViewDataTextColumn FieldName="Nov" ReadOnly="True" Name="Nov" VisibleIndex="13">
            <DataItemTemplate>
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <asp:Image ID="changeImage11" runat="server" ImageUrl="~/Images/spacer.gif" Visible="true"
                                GenerateEmptyAlternateText="True" />
                        </td>
                        <td style="width: 20px">
                            <asp:Label ID="noKPISubmitted11" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                </table>
            </DataItemTemplate>
            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
            <CellStyle HorizontalAlign="Center">
            </CellStyle>
        </dxwgv:GridViewDataTextColumn>
        <dxwgv:GridViewDataTextColumn FieldName="Dec" ReadOnly="True" Name="Dec" VisibleIndex="14">
            <DataItemTemplate>
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <asp:Image ID="changeImage12" runat="server" ImageUrl="~/Images/spacer.gif" Visible="true"
                                GenerateEmptyAlternateText="True" />
                        </td>
                        <td style="width: 20px">
                            <asp:Label ID="noKPISubmitted12" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                </table>
            </DataItemTemplate>
            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
            <CellStyle HorizontalAlign="Center">
            </CellStyle>
        </dxwgv:GridViewDataTextColumn>
        <dxwgv:GridViewDataTextColumn FieldName="Year" ReadOnly="True" Name="Year" VisibleIndex="15">
            <DataItemTemplate>
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="width: 20px">
                            <asp:Label ID="noKPISubmitted13" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                </table>
            </DataItemTemplate>
            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
            <CellStyle HorizontalAlign="Center">
            </CellStyle>
        </dxwgv:GridViewDataTextColumn>
    </Columns>
    <Settings ShowFilterRow="True" ShowGroupPanel="True"></Settings>
    <StylesEditors>
        <ProgressBar Height="25px">
        </ProgressBar>
    </StylesEditors>
</dxwgv:ASPxGridView>

<dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="grid"></dxwgv:ASPxGridViewExporter>

<asp:SqlDataSource ID="sqldsKPIYear" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    ProviderName="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString.ProviderName %>"
    SelectCommand="SELECT DISTINCT(YEAR(kpiDateTime)) As [Year] FROM dbo.KPI ORDER BY [Year] ASC">
</asp:SqlDataSource>
<asp:SqlDataSource ID="sqldsCompaniesList" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="_Companies_GetCompanyNameList" SelectCommandType="StoredProcedure">
</asp:SqlDataSource>

<asp:SqlDataSource ID="sqldsResidentialSites" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    ProviderName="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString.ProviderName %>"
    SelectCommand="SELECT DISTINCT(dbo.CompanySiteCategoryStandard.SiteId) As [SiteId], dbo.Sites.SiteName As [SiteName] FROM dbo.CompanySiteCategoryStandard INNER JOIN dbo.Sites on dbo.CompanySiteCategoryStandard.SiteId = dbo.Sites.SiteId ORDER BY [SiteName] ASC">
</asp:SqlDataSource>
<data:CompanySiteCategoryDataSource ID="CompanySiteCategoryDataSource" runat="server"
    SelectMethod="GetPaged" EnablePaging="True" EnableSorting="True">
</data:CompanySiteCategoryDataSource>
