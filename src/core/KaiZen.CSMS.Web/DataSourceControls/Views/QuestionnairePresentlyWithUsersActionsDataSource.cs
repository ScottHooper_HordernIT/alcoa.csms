﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.QuestionnairePresentlyWithUsersActionsProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[CLSCompliant(true)]
	[Designer(typeof(QuestionnairePresentlyWithUsersActionsDataSourceDesigner))]
	public class QuestionnairePresentlyWithUsersActionsDataSource : ReadOnlyDataSource<QuestionnairePresentlyWithUsersActions>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnairePresentlyWithUsersActionsDataSource class.
		/// </summary>
		public QuestionnairePresentlyWithUsersActionsDataSource() : base(new QuestionnairePresentlyWithUsersActionsService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the QuestionnairePresentlyWithUsersActionsDataSourceView used by the QuestionnairePresentlyWithUsersActionsDataSource.
		/// </summary>
		protected QuestionnairePresentlyWithUsersActionsDataSourceView QuestionnairePresentlyWithUsersActionsView
		{
			get { return ( View as QuestionnairePresentlyWithUsersActionsDataSourceView ); }
		}
		
		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the QuestionnairePresentlyWithUsersActionsDataSourceView class that is to be
		/// used by the QuestionnairePresentlyWithUsersActionsDataSource.
		/// </summary>
		/// <returns>An instance of the QuestionnairePresentlyWithUsersActionsDataSourceView class.</returns>
		protected override BaseDataSourceView<QuestionnairePresentlyWithUsersActions, Object> GetNewDataSourceView()
		{
			return new QuestionnairePresentlyWithUsersActionsDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the QuestionnairePresentlyWithUsersActionsDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class QuestionnairePresentlyWithUsersActionsDataSourceView : ReadOnlyDataSourceView<QuestionnairePresentlyWithUsersActions>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnairePresentlyWithUsersActionsDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the QuestionnairePresentlyWithUsersActionsDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public QuestionnairePresentlyWithUsersActionsDataSourceView(QuestionnairePresentlyWithUsersActionsDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal QuestionnairePresentlyWithUsersActionsDataSource QuestionnairePresentlyWithUsersActionsOwner
		{
			get { return Owner as QuestionnairePresentlyWithUsersActionsDataSource; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal QuestionnairePresentlyWithUsersActionsService QuestionnairePresentlyWithUsersActionsProvider
		{
			get { return Provider as QuestionnairePresentlyWithUsersActionsService; }
		}

		#endregion Properties
		
		#region Methods
		
		#endregion Methods
	}

	#region QuestionnairePresentlyWithUsersActionsDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the QuestionnairePresentlyWithUsersActionsDataSource class.
	/// </summary>
	public class QuestionnairePresentlyWithUsersActionsDataSourceDesigner : ReadOnlyDataSourceDesigner<QuestionnairePresentlyWithUsersActions>
	{
	}

	#endregion QuestionnairePresentlyWithUsersActionsDataSourceDesigner

	#region QuestionnairePresentlyWithUsersActionsFilter

	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnairePresentlyWithUsersActions"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnairePresentlyWithUsersActionsFilter : SqlFilter<QuestionnairePresentlyWithUsersActionsColumn>
	{
	}

	#endregion QuestionnairePresentlyWithUsersActionsFilter

	#region QuestionnairePresentlyWithUsersActionsExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnairePresentlyWithUsersActions"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnairePresentlyWithUsersActionsExpressionBuilder : SqlExpressionBuilder<QuestionnairePresentlyWithUsersActionsColumn>
	{
	}
	
	#endregion QuestionnairePresentlyWithUsersActionsExpressionBuilder		
}

