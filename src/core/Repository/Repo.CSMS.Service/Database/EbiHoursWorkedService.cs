﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface IEbiHoursWorkedService
    {
        EbiHoursWorked Get(Expression<Func<EbiHoursWorked, bool>> where, List<Expression<Func<EbiHoursWorked, object>>> includes);
        List<EbiHoursWorked> GetMany(int? top, Expression<Func<EbiHoursWorked, bool>> where, Func<IQueryable<EbiHoursWorked>, IOrderedQueryable<EbiHoursWorked>> orderBy, List<Expression<Func<EbiHoursWorked, object>>> includes);
        List<EbiHoursWorked> GetManyPaged(int pageNumber, int pageSize, Expression<Func<EbiHoursWorked, bool>> where, Func<IQueryable<EbiHoursWorked>, IOrderedQueryable<EbiHoursWorked>> orderBy, List<Expression<Func<EbiHoursWorked, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<EbiHoursWorked, bool>> where, List<Expression<Func<EbiHoursWorked, object>>> includes);
        void Insert(EbiHoursWorked item, bool saveChanges = true);
        void Update(EbiHoursWorked item, bool saveChanges = true);
        void Delete(EbiHoursWorked item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class EbiHoursWorkedService : IEbiHoursWorkedService
    {
        private readonly IRepository<EbiHoursWorked> _repository;

        public EbiHoursWorkedService(IRepository<EbiHoursWorked> repository)
        {
            this._repository = repository;
        }

        public EbiHoursWorked Get(Expression<Func<EbiHoursWorked, bool>> where, List<Expression<Func<EbiHoursWorked, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<EbiHoursWorked> GetMany(int? top, Expression<Func<EbiHoursWorked, bool>> where, Func<IQueryable<EbiHoursWorked>, IOrderedQueryable<EbiHoursWorked>> orderBy, List<Expression<Func<EbiHoursWorked, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<EbiHoursWorked> GetManyPaged(int pageNumber, int pageSize, Expression<Func<EbiHoursWorked, bool>> where, Func<IQueryable<EbiHoursWorked>, IOrderedQueryable<EbiHoursWorked>> orderBy, List<Expression<Func<EbiHoursWorked, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<EbiHoursWorked, bool>> where, List<Expression<Func<EbiHoursWorked, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(EbiHoursWorked item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("EbiHoursWorked");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(EbiHoursWorked item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("EbiHoursWorked");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(EbiHoursWorked item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("EbiHoursWorked");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
