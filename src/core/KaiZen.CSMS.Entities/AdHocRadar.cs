﻿#region Using directives

using System;

#endregion

namespace KaiZen.CSMS.Entities
{	
	///<summary>
	/// An object representation of the 'AdHoc_Radar' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class AdHocRadar : AdHocRadarBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="AdHocRadar"/> instance.
		///</summary>
		public AdHocRadar():base(){}	
		
		#endregion
	}
}
