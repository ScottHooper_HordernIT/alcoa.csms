﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;
using System.Web;
using log4net.Core;

namespace ALCOA.CSMS.Batch.Common.Log
{
    public static class Log4NetService
    {
        private static ILog logger;
        private static bool isConfigured = false;

        static Log4NetService()
        {
            if (!isConfigured)
            {
                log4net.Config.XmlConfigurator.Configure();
                logger = LogManager.GetLogger(typeof(Log4NetService));
                isConfigured = true;
            }
        }

        public static Level TriggerLevel { get; set; }

        #region Log Error

        public static void Shutdown()
        {
            logger.Logger.Repository.Shutdown();
        }

        public static void LogError(string message)
        {
            MDC.Set("user", Environment.UserName);
            MDC.Set("machine", Environment.MachineName);
            logger.Error(message);
        }

        public static void LogError(string message, Exception ex)
        {
            MDC.Set("user", Environment.UserName);
            MDC.Set("machine", Environment.MachineName);
            logger.Error(message, ex);
        }

        public static void LogError(Exception ex)
        {
            MDC.Set("user", Environment.UserName);
            MDC.Set("machine", Environment.MachineName);
            logger.Error("", ex);
        }

        #endregion

        #region Log Warning

        public static void LogWarning(string message)
        {
            MDC.Set("user", Environment.UserName);
            MDC.Set("machine", Environment.MachineName);
            logger.Warn(message);
        }

        public static void LogWarning(Exception ex)
        {
            MDC.Set("user", Environment.UserName);
            MDC.Set("machine", Environment.MachineName);
            logger.Warn("", ex);
        }

        public static void LogWarning(string message, Exception ex)
        {
            MDC.Set("user", Environment.UserName);
            MDC.Set("machine", Environment.MachineName);
            logger.Warn(message, ex);
        }

        #endregion

        #region Log Info

        public static void LogInfo(string message)
        {
            MDC.Set("user", Environment.UserName);
            MDC.Set("machine", Environment.MachineName);
            logger.Info(message);
        }

        public static void LogInfo(Exception ex)
        {
            MDC.Set("user", Environment.UserName);
            MDC.Set("machine", Environment.MachineName);
            logger.Info("", ex);
        }

        public static void LogInfo(string message, Exception ex)
        {
            MDC.Set("user", Environment.UserName);
            MDC.Set("machine", Environment.MachineName);
            logger.Info(message, ex);
        }

        #endregion

        #region Log Debug

        public static void LogDebug(string message)
        {
            MDC.Set("user", Environment.UserName);
            MDC.Set("machine", Environment.MachineName);
            logger.Debug(message);
        }

        public static void LogDebug(Exception ex)
        {
            MDC.Set("user", Environment.UserName);
            MDC.Set("machine", Environment.MachineName);
            logger.Debug("", ex);
        }

        public static void LogDebug(string message, Exception ex)
        {
            MDC.Set("user", Environment.UserName);
            MDC.Set("machine", Environment.MachineName);
            logger.Debug(message, ex);
        }

        #endregion
    }
}
