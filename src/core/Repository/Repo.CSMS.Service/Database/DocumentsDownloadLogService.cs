﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface IDocumentsDownloadLogService
    {
        DocumentsDownloadLog Get(Expression<Func<DocumentsDownloadLog, bool>> where, List<Expression<Func<DocumentsDownloadLog, object>>> includes);
        List<DocumentsDownloadLog> GetMany(int? top, Expression<Func<DocumentsDownloadLog, bool>> where, Func<IQueryable<DocumentsDownloadLog>, IOrderedQueryable<DocumentsDownloadLog>> orderBy, List<Expression<Func<DocumentsDownloadLog, object>>> includes);
        List<DocumentsDownloadLog> GetManyPaged(int pageNumber, int pageSize, Expression<Func<DocumentsDownloadLog, bool>> where, Func<IQueryable<DocumentsDownloadLog>, IOrderedQueryable<DocumentsDownloadLog>> orderBy, List<Expression<Func<DocumentsDownloadLog, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<DocumentsDownloadLog, bool>> where, List<Expression<Func<DocumentsDownloadLog, object>>> includes);
        void Insert(DocumentsDownloadLog item, bool saveChanges = true);
        void Update(DocumentsDownloadLog item, bool saveChanges = true);
        void Delete(DocumentsDownloadLog item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class DocumentsDownloadLogService : IDocumentsDownloadLogService
    {
        private readonly IRepository<DocumentsDownloadLog> _repository;

        public DocumentsDownloadLogService(IRepository<DocumentsDownloadLog> repository)
        {
            this._repository = repository;
        }

        public DocumentsDownloadLog Get(Expression<Func<DocumentsDownloadLog, bool>> where, List<Expression<Func<DocumentsDownloadLog, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<DocumentsDownloadLog> GetMany(int? top, Expression<Func<DocumentsDownloadLog, bool>> where, Func<IQueryable<DocumentsDownloadLog>, IOrderedQueryable<DocumentsDownloadLog>> orderBy, List<Expression<Func<DocumentsDownloadLog, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<DocumentsDownloadLog> GetManyPaged(int pageNumber, int pageSize, Expression<Func<DocumentsDownloadLog, bool>> where, Func<IQueryable<DocumentsDownloadLog>, IOrderedQueryable<DocumentsDownloadLog>> orderBy, List<Expression<Func<DocumentsDownloadLog, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<DocumentsDownloadLog, bool>> where, List<Expression<Func<DocumentsDownloadLog, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(DocumentsDownloadLog item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("DocumentsDownloadLog");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(DocumentsDownloadLog item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("DocumentsDownloadLog");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(DocumentsDownloadLog item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("DocumentsDownloadLog");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
