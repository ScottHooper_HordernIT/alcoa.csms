﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.OrphanCompaniesEhsConsultantIdProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[CLSCompliant(true)]
	[Designer(typeof(OrphanCompaniesEhsConsultantIdDataSourceDesigner))]
	public class OrphanCompaniesEhsConsultantIdDataSource : ReadOnlyDataSource<OrphanCompaniesEhsConsultantId>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the OrphanCompaniesEhsConsultantIdDataSource class.
		/// </summary>
		public OrphanCompaniesEhsConsultantIdDataSource() : base(new OrphanCompaniesEhsConsultantIdService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the OrphanCompaniesEhsConsultantIdDataSourceView used by the OrphanCompaniesEhsConsultantIdDataSource.
		/// </summary>
		protected OrphanCompaniesEhsConsultantIdDataSourceView OrphanCompaniesEhsConsultantIdView
		{
			get { return ( View as OrphanCompaniesEhsConsultantIdDataSourceView ); }
		}
		
		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the OrphanCompaniesEhsConsultantIdDataSourceView class that is to be
		/// used by the OrphanCompaniesEhsConsultantIdDataSource.
		/// </summary>
		/// <returns>An instance of the OrphanCompaniesEhsConsultantIdDataSourceView class.</returns>
		protected override BaseDataSourceView<OrphanCompaniesEhsConsultantId, Object> GetNewDataSourceView()
		{
			return new OrphanCompaniesEhsConsultantIdDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the OrphanCompaniesEhsConsultantIdDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class OrphanCompaniesEhsConsultantIdDataSourceView : ReadOnlyDataSourceView<OrphanCompaniesEhsConsultantId>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the OrphanCompaniesEhsConsultantIdDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the OrphanCompaniesEhsConsultantIdDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public OrphanCompaniesEhsConsultantIdDataSourceView(OrphanCompaniesEhsConsultantIdDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal OrphanCompaniesEhsConsultantIdDataSource OrphanCompaniesEhsConsultantIdOwner
		{
			get { return Owner as OrphanCompaniesEhsConsultantIdDataSource; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal OrphanCompaniesEhsConsultantIdService OrphanCompaniesEhsConsultantIdProvider
		{
			get { return Provider as OrphanCompaniesEhsConsultantIdService; }
		}

		#endregion Properties
		
		#region Methods
		
		#endregion Methods
	}

	#region OrphanCompaniesEhsConsultantIdDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the OrphanCompaniesEhsConsultantIdDataSource class.
	/// </summary>
	public class OrphanCompaniesEhsConsultantIdDataSourceDesigner : ReadOnlyDataSourceDesigner<OrphanCompaniesEhsConsultantId>
	{
	}

	#endregion OrphanCompaniesEhsConsultantIdDataSourceDesigner

	#region OrphanCompaniesEhsConsultantIdFilter

	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="OrphanCompaniesEhsConsultantId"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class OrphanCompaniesEhsConsultantIdFilter : SqlFilter<OrphanCompaniesEhsConsultantIdColumn>
	{
	}

	#endregion OrphanCompaniesEhsConsultantIdFilter

	#region OrphanCompaniesEhsConsultantIdExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="OrphanCompaniesEhsConsultantId"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class OrphanCompaniesEhsConsultantIdExpressionBuilder : SqlExpressionBuilder<OrphanCompaniesEhsConsultantIdColumn>
	{
	}
	
	#endregion OrphanCompaniesEhsConsultantIdExpressionBuilder		
}

