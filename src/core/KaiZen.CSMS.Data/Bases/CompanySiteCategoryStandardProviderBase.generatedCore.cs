﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="CompanySiteCategoryStandardProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class CompanySiteCategoryStandardProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.CompanySiteCategoryStandard, KaiZen.CSMS.Entities.CompanySiteCategoryStandardKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.CompanySiteCategoryStandardKey key)
		{
			return Delete(transactionManager, key.CompanySiteCategoryStandardId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_companySiteCategoryStandardId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _companySiteCategoryStandardId)
		{
			return Delete(null, _companySiteCategoryStandardId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companySiteCategoryStandardId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _companySiteCategoryStandardId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompanySiteCategoryStandard_Users_ModifiedBy key.
		///		FK_CompanySiteCategoryStandard_Users_ModifiedBy Description: 
		/// </summary>
		/// <param name="_modifiedByUserId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanySiteCategoryStandard objects.</returns>
		public TList<CompanySiteCategoryStandard> GetByModifiedByUserId(System.Int32 _modifiedByUserId)
		{
			int count = -1;
			return GetByModifiedByUserId(_modifiedByUserId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompanySiteCategoryStandard_Users_ModifiedBy key.
		///		FK_CompanySiteCategoryStandard_Users_ModifiedBy Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_modifiedByUserId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanySiteCategoryStandard objects.</returns>
		/// <remarks></remarks>
		public TList<CompanySiteCategoryStandard> GetByModifiedByUserId(TransactionManager transactionManager, System.Int32 _modifiedByUserId)
		{
			int count = -1;
			return GetByModifiedByUserId(transactionManager, _modifiedByUserId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompanySiteCategoryStandard_Users_ModifiedBy key.
		///		FK_CompanySiteCategoryStandard_Users_ModifiedBy Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_modifiedByUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanySiteCategoryStandard objects.</returns>
		public TList<CompanySiteCategoryStandard> GetByModifiedByUserId(TransactionManager transactionManager, System.Int32 _modifiedByUserId, int start, int pageLength)
		{
			int count = -1;
			return GetByModifiedByUserId(transactionManager, _modifiedByUserId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompanySiteCategoryStandard_Users_ModifiedBy key.
		///		fkCompanySiteCategoryStandardUsersModifiedBy Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_modifiedByUserId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanySiteCategoryStandard objects.</returns>
		public TList<CompanySiteCategoryStandard> GetByModifiedByUserId(System.Int32 _modifiedByUserId, int start, int pageLength)
		{
			int count =  -1;
			return GetByModifiedByUserId(null, _modifiedByUserId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompanySiteCategoryStandard_Users_ModifiedBy key.
		///		fkCompanySiteCategoryStandardUsersModifiedBy Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_modifiedByUserId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanySiteCategoryStandard objects.</returns>
		public TList<CompanySiteCategoryStandard> GetByModifiedByUserId(System.Int32 _modifiedByUserId, int start, int pageLength,out int count)
		{
			return GetByModifiedByUserId(null, _modifiedByUserId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompanySiteCategoryStandard_Users_ModifiedBy key.
		///		FK_CompanySiteCategoryStandard_Users_ModifiedBy Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_modifiedByUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanySiteCategoryStandard objects.</returns>
		public abstract TList<CompanySiteCategoryStandard> GetByModifiedByUserId(TransactionManager transactionManager, System.Int32 _modifiedByUserId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompanySiteCategoryStandard_Users_ApprovedBy key.
		///		FK_CompanySiteCategoryStandard_Users_ApprovedBy Description: 
		/// </summary>
		/// <param name="_approvedByUserId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanySiteCategoryStandard objects.</returns>
		public TList<CompanySiteCategoryStandard> GetByApprovedByUserId(System.Int32? _approvedByUserId)
		{
			int count = -1;
			return GetByApprovedByUserId(_approvedByUserId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompanySiteCategoryStandard_Users_ApprovedBy key.
		///		FK_CompanySiteCategoryStandard_Users_ApprovedBy Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_approvedByUserId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanySiteCategoryStandard objects.</returns>
		/// <remarks></remarks>
		public TList<CompanySiteCategoryStandard> GetByApprovedByUserId(TransactionManager transactionManager, System.Int32? _approvedByUserId)
		{
			int count = -1;
			return GetByApprovedByUserId(transactionManager, _approvedByUserId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompanySiteCategoryStandard_Users_ApprovedBy key.
		///		FK_CompanySiteCategoryStandard_Users_ApprovedBy Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_approvedByUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanySiteCategoryStandard objects.</returns>
		public TList<CompanySiteCategoryStandard> GetByApprovedByUserId(TransactionManager transactionManager, System.Int32? _approvedByUserId, int start, int pageLength)
		{
			int count = -1;
			return GetByApprovedByUserId(transactionManager, _approvedByUserId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompanySiteCategoryStandard_Users_ApprovedBy key.
		///		fkCompanySiteCategoryStandardUsersApprovedBy Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_approvedByUserId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanySiteCategoryStandard objects.</returns>
		public TList<CompanySiteCategoryStandard> GetByApprovedByUserId(System.Int32? _approvedByUserId, int start, int pageLength)
		{
			int count =  -1;
			return GetByApprovedByUserId(null, _approvedByUserId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompanySiteCategoryStandard_Users_ApprovedBy key.
		///		fkCompanySiteCategoryStandardUsersApprovedBy Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_approvedByUserId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanySiteCategoryStandard objects.</returns>
		public TList<CompanySiteCategoryStandard> GetByApprovedByUserId(System.Int32? _approvedByUserId, int start, int pageLength,out int count)
		{
			return GetByApprovedByUserId(null, _approvedByUserId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompanySiteCategoryStandard_Users_ApprovedBy key.
		///		FK_CompanySiteCategoryStandard_Users_ApprovedBy Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_approvedByUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanySiteCategoryStandard objects.</returns>
		public abstract TList<CompanySiteCategoryStandard> GetByApprovedByUserId(TransactionManager transactionManager, System.Int32? _approvedByUserId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompanySiteCategoryStandard_Users_LocationSponsor key.
		///		FK_CompanySiteCategoryStandard_Users_LocationSponsor Description: 
		/// </summary>
		/// <param name="_locationSponsorUserId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanySiteCategoryStandard objects.</returns>
		public TList<CompanySiteCategoryStandard> GetByLocationSponsorUserId(System.Int32? _locationSponsorUserId)
		{
			int count = -1;
			return GetByLocationSponsorUserId(_locationSponsorUserId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompanySiteCategoryStandard_Users_LocationSponsor key.
		///		FK_CompanySiteCategoryStandard_Users_LocationSponsor Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_locationSponsorUserId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanySiteCategoryStandard objects.</returns>
		/// <remarks></remarks>
		public TList<CompanySiteCategoryStandard> GetByLocationSponsorUserId(TransactionManager transactionManager, System.Int32? _locationSponsorUserId)
		{
			int count = -1;
			return GetByLocationSponsorUserId(transactionManager, _locationSponsorUserId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompanySiteCategoryStandard_Users_LocationSponsor key.
		///		FK_CompanySiteCategoryStandard_Users_LocationSponsor Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_locationSponsorUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanySiteCategoryStandard objects.</returns>
		public TList<CompanySiteCategoryStandard> GetByLocationSponsorUserId(TransactionManager transactionManager, System.Int32? _locationSponsorUserId, int start, int pageLength)
		{
			int count = -1;
			return GetByLocationSponsorUserId(transactionManager, _locationSponsorUserId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompanySiteCategoryStandard_Users_LocationSponsor key.
		///		fkCompanySiteCategoryStandardUsersLocationSponsor Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_locationSponsorUserId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanySiteCategoryStandard objects.</returns>
		public TList<CompanySiteCategoryStandard> GetByLocationSponsorUserId(System.Int32? _locationSponsorUserId, int start, int pageLength)
		{
			int count =  -1;
			return GetByLocationSponsorUserId(null, _locationSponsorUserId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompanySiteCategoryStandard_Users_LocationSponsor key.
		///		fkCompanySiteCategoryStandardUsersLocationSponsor Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_locationSponsorUserId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanySiteCategoryStandard objects.</returns>
		public TList<CompanySiteCategoryStandard> GetByLocationSponsorUserId(System.Int32? _locationSponsorUserId, int start, int pageLength,out int count)
		{
			return GetByLocationSponsorUserId(null, _locationSponsorUserId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompanySiteCategoryStandard_Users_LocationSponsor key.
		///		FK_CompanySiteCategoryStandard_Users_LocationSponsor Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_locationSponsorUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanySiteCategoryStandard objects.</returns>
		public abstract TList<CompanySiteCategoryStandard> GetByLocationSponsorUserId(TransactionManager transactionManager, System.Int32? _locationSponsorUserId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompanySiteCategoryStandard_Users_Arp key.
		///		FK_CompanySiteCategoryStandard_Users_Arp Description: 
		/// </summary>
		/// <param name="_arpUserId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanySiteCategoryStandard objects.</returns>
		public TList<CompanySiteCategoryStandard> GetByArpUserId(System.Int32? _arpUserId)
		{
			int count = -1;
			return GetByArpUserId(_arpUserId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompanySiteCategoryStandard_Users_Arp key.
		///		FK_CompanySiteCategoryStandard_Users_Arp Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_arpUserId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanySiteCategoryStandard objects.</returns>
		/// <remarks></remarks>
		public TList<CompanySiteCategoryStandard> GetByArpUserId(TransactionManager transactionManager, System.Int32? _arpUserId)
		{
			int count = -1;
			return GetByArpUserId(transactionManager, _arpUserId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompanySiteCategoryStandard_Users_Arp key.
		///		FK_CompanySiteCategoryStandard_Users_Arp Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_arpUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanySiteCategoryStandard objects.</returns>
		public TList<CompanySiteCategoryStandard> GetByArpUserId(TransactionManager transactionManager, System.Int32? _arpUserId, int start, int pageLength)
		{
			int count = -1;
			return GetByArpUserId(transactionManager, _arpUserId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompanySiteCategoryStandard_Users_Arp key.
		///		fkCompanySiteCategoryStandardUsersArp Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_arpUserId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanySiteCategoryStandard objects.</returns>
		public TList<CompanySiteCategoryStandard> GetByArpUserId(System.Int32? _arpUserId, int start, int pageLength)
		{
			int count =  -1;
			return GetByArpUserId(null, _arpUserId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompanySiteCategoryStandard_Users_Arp key.
		///		fkCompanySiteCategoryStandardUsersArp Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_arpUserId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanySiteCategoryStandard objects.</returns>
		public TList<CompanySiteCategoryStandard> GetByArpUserId(System.Int32? _arpUserId, int start, int pageLength,out int count)
		{
			return GetByArpUserId(null, _arpUserId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompanySiteCategoryStandard_Users_Arp key.
		///		FK_CompanySiteCategoryStandard_Users_Arp Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_arpUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanySiteCategoryStandard objects.</returns>
		public abstract TList<CompanySiteCategoryStandard> GetByArpUserId(TransactionManager transactionManager, System.Int32? _arpUserId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompanySiteCategoryStandard_Companies key.
		///		FK_CompanySiteCategoryStandard_Companies Description: 
		/// </summary>
		/// <param name="_companyId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanySiteCategoryStandard objects.</returns>
		public TList<CompanySiteCategoryStandard> GetByCompanyId(System.Int32 _companyId)
		{
			int count = -1;
			return GetByCompanyId(_companyId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompanySiteCategoryStandard_Companies key.
		///		FK_CompanySiteCategoryStandard_Companies Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanySiteCategoryStandard objects.</returns>
		/// <remarks></remarks>
		public TList<CompanySiteCategoryStandard> GetByCompanyId(TransactionManager transactionManager, System.Int32 _companyId)
		{
			int count = -1;
			return GetByCompanyId(transactionManager, _companyId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompanySiteCategoryStandard_Companies key.
		///		FK_CompanySiteCategoryStandard_Companies Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanySiteCategoryStandard objects.</returns>
		public TList<CompanySiteCategoryStandard> GetByCompanyId(TransactionManager transactionManager, System.Int32 _companyId, int start, int pageLength)
		{
			int count = -1;
			return GetByCompanyId(transactionManager, _companyId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompanySiteCategoryStandard_Companies key.
		///		fkCompanySiteCategoryStandardCompanies Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_companyId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanySiteCategoryStandard objects.</returns>
		public TList<CompanySiteCategoryStandard> GetByCompanyId(System.Int32 _companyId, int start, int pageLength)
		{
			int count =  -1;
			return GetByCompanyId(null, _companyId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompanySiteCategoryStandard_Companies key.
		///		fkCompanySiteCategoryStandardCompanies Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_companyId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanySiteCategoryStandard objects.</returns>
		public TList<CompanySiteCategoryStandard> GetByCompanyId(System.Int32 _companyId, int start, int pageLength,out int count)
		{
			return GetByCompanyId(null, _companyId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompanySiteCategoryStandard_Companies key.
		///		FK_CompanySiteCategoryStandard_Companies Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanySiteCategoryStandard objects.</returns>
		public abstract TList<CompanySiteCategoryStandard> GetByCompanyId(TransactionManager transactionManager, System.Int32 _companyId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompanySiteCategoryStandard_CompanySiteCategory key.
		///		FK_CompanySiteCategoryStandard_CompanySiteCategory Description: 
		/// </summary>
		/// <param name="_companySiteCategoryId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanySiteCategoryStandard objects.</returns>
		public TList<CompanySiteCategoryStandard> GetByCompanySiteCategoryId(System.Int32? _companySiteCategoryId)
		{
			int count = -1;
			return GetByCompanySiteCategoryId(_companySiteCategoryId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompanySiteCategoryStandard_CompanySiteCategory key.
		///		FK_CompanySiteCategoryStandard_CompanySiteCategory Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companySiteCategoryId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanySiteCategoryStandard objects.</returns>
		/// <remarks></remarks>
		public TList<CompanySiteCategoryStandard> GetByCompanySiteCategoryId(TransactionManager transactionManager, System.Int32? _companySiteCategoryId)
		{
			int count = -1;
			return GetByCompanySiteCategoryId(transactionManager, _companySiteCategoryId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompanySiteCategoryStandard_CompanySiteCategory key.
		///		FK_CompanySiteCategoryStandard_CompanySiteCategory Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companySiteCategoryId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanySiteCategoryStandard objects.</returns>
		public TList<CompanySiteCategoryStandard> GetByCompanySiteCategoryId(TransactionManager transactionManager, System.Int32? _companySiteCategoryId, int start, int pageLength)
		{
			int count = -1;
			return GetByCompanySiteCategoryId(transactionManager, _companySiteCategoryId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompanySiteCategoryStandard_CompanySiteCategory key.
		///		fkCompanySiteCategoryStandardCompanySiteCategory Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_companySiteCategoryId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanySiteCategoryStandard objects.</returns>
		public TList<CompanySiteCategoryStandard> GetByCompanySiteCategoryId(System.Int32? _companySiteCategoryId, int start, int pageLength)
		{
			int count =  -1;
			return GetByCompanySiteCategoryId(null, _companySiteCategoryId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompanySiteCategoryStandard_CompanySiteCategory key.
		///		fkCompanySiteCategoryStandardCompanySiteCategory Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_companySiteCategoryId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanySiteCategoryStandard objects.</returns>
		public TList<CompanySiteCategoryStandard> GetByCompanySiteCategoryId(System.Int32? _companySiteCategoryId, int start, int pageLength,out int count)
		{
			return GetByCompanySiteCategoryId(null, _companySiteCategoryId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompanySiteCategoryStandard_CompanySiteCategory key.
		///		FK_CompanySiteCategoryStandard_CompanySiteCategory Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companySiteCategoryId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanySiteCategoryStandard objects.</returns>
		public abstract TList<CompanySiteCategoryStandard> GetByCompanySiteCategoryId(TransactionManager transactionManager, System.Int32? _companySiteCategoryId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompanySiteCategoryStandard_Sites key.
		///		FK_CompanySiteCategoryStandard_Sites Description: 
		/// </summary>
		/// <param name="_siteId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanySiteCategoryStandard objects.</returns>
		public TList<CompanySiteCategoryStandard> GetBySiteId(System.Int32 _siteId)
		{
			int count = -1;
			return GetBySiteId(_siteId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompanySiteCategoryStandard_Sites key.
		///		FK_CompanySiteCategoryStandard_Sites Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_siteId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanySiteCategoryStandard objects.</returns>
		/// <remarks></remarks>
		public TList<CompanySiteCategoryStandard> GetBySiteId(TransactionManager transactionManager, System.Int32 _siteId)
		{
			int count = -1;
			return GetBySiteId(transactionManager, _siteId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompanySiteCategoryStandard_Sites key.
		///		FK_CompanySiteCategoryStandard_Sites Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_siteId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanySiteCategoryStandard objects.</returns>
		public TList<CompanySiteCategoryStandard> GetBySiteId(TransactionManager transactionManager, System.Int32 _siteId, int start, int pageLength)
		{
			int count = -1;
			return GetBySiteId(transactionManager, _siteId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompanySiteCategoryStandard_Sites key.
		///		fkCompanySiteCategoryStandardSites Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_siteId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanySiteCategoryStandard objects.</returns>
		public TList<CompanySiteCategoryStandard> GetBySiteId(System.Int32 _siteId, int start, int pageLength)
		{
			int count =  -1;
			return GetBySiteId(null, _siteId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompanySiteCategoryStandard_Sites key.
		///		fkCompanySiteCategoryStandardSites Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_siteId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanySiteCategoryStandard objects.</returns>
		public TList<CompanySiteCategoryStandard> GetBySiteId(System.Int32 _siteId, int start, int pageLength,out int count)
		{
			return GetBySiteId(null, _siteId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompanySiteCategoryStandard_Sites key.
		///		FK_CompanySiteCategoryStandard_Sites Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_siteId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanySiteCategoryStandard objects.</returns>
		public abstract TList<CompanySiteCategoryStandard> GetBySiteId(TransactionManager transactionManager, System.Int32 _siteId, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.CompanySiteCategoryStandard Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.CompanySiteCategoryStandardKey key, int start, int pageLength)
		{
			return GetByCompanySiteCategoryStandardId(transactionManager, key.CompanySiteCategoryStandardId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_CompanySiteCategoryStandard index.
		/// </summary>
		/// <param name="_companySiteCategoryStandardId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompanySiteCategoryStandard"/> class.</returns>
		public KaiZen.CSMS.Entities.CompanySiteCategoryStandard GetByCompanySiteCategoryStandardId(System.Int32 _companySiteCategoryStandardId)
		{
			int count = -1;
			return GetByCompanySiteCategoryStandardId(null,_companySiteCategoryStandardId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CompanySiteCategoryStandard index.
		/// </summary>
		/// <param name="_companySiteCategoryStandardId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompanySiteCategoryStandard"/> class.</returns>
		public KaiZen.CSMS.Entities.CompanySiteCategoryStandard GetByCompanySiteCategoryStandardId(System.Int32 _companySiteCategoryStandardId, int start, int pageLength)
		{
			int count = -1;
			return GetByCompanySiteCategoryStandardId(null, _companySiteCategoryStandardId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CompanySiteCategoryStandard index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companySiteCategoryStandardId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompanySiteCategoryStandard"/> class.</returns>
		public KaiZen.CSMS.Entities.CompanySiteCategoryStandard GetByCompanySiteCategoryStandardId(TransactionManager transactionManager, System.Int32 _companySiteCategoryStandardId)
		{
			int count = -1;
			return GetByCompanySiteCategoryStandardId(transactionManager, _companySiteCategoryStandardId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CompanySiteCategoryStandard index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companySiteCategoryStandardId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompanySiteCategoryStandard"/> class.</returns>
		public KaiZen.CSMS.Entities.CompanySiteCategoryStandard GetByCompanySiteCategoryStandardId(TransactionManager transactionManager, System.Int32 _companySiteCategoryStandardId, int start, int pageLength)
		{
			int count = -1;
			return GetByCompanySiteCategoryStandardId(transactionManager, _companySiteCategoryStandardId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CompanySiteCategoryStandard index.
		/// </summary>
		/// <param name="_companySiteCategoryStandardId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompanySiteCategoryStandard"/> class.</returns>
		public KaiZen.CSMS.Entities.CompanySiteCategoryStandard GetByCompanySiteCategoryStandardId(System.Int32 _companySiteCategoryStandardId, int start, int pageLength, out int count)
		{
			return GetByCompanySiteCategoryStandardId(null, _companySiteCategoryStandardId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CompanySiteCategoryStandard index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companySiteCategoryStandardId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompanySiteCategoryStandard"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.CompanySiteCategoryStandard GetByCompanySiteCategoryStandardId(TransactionManager transactionManager, System.Int32 _companySiteCategoryStandardId, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_CompanySiteCategoryStandard index.
		/// </summary>
		/// <param name="_companyId"></param>
		/// <param name="_siteId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompanySiteCategoryStandard"/> class.</returns>
		public KaiZen.CSMS.Entities.CompanySiteCategoryStandard GetByCompanyIdSiteId(System.Int32 _companyId, System.Int32 _siteId)
		{
			int count = -1;
			return GetByCompanyIdSiteId(null,_companyId, _siteId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_CompanySiteCategoryStandard index.
		/// </summary>
		/// <param name="_companyId"></param>
		/// <param name="_siteId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompanySiteCategoryStandard"/> class.</returns>
		public KaiZen.CSMS.Entities.CompanySiteCategoryStandard GetByCompanyIdSiteId(System.Int32 _companyId, System.Int32 _siteId, int start, int pageLength)
		{
			int count = -1;
			return GetByCompanyIdSiteId(null, _companyId, _siteId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_CompanySiteCategoryStandard index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyId"></param>
		/// <param name="_siteId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompanySiteCategoryStandard"/> class.</returns>
		public KaiZen.CSMS.Entities.CompanySiteCategoryStandard GetByCompanyIdSiteId(TransactionManager transactionManager, System.Int32 _companyId, System.Int32 _siteId)
		{
			int count = -1;
			return GetByCompanyIdSiteId(transactionManager, _companyId, _siteId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_CompanySiteCategoryStandard index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyId"></param>
		/// <param name="_siteId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompanySiteCategoryStandard"/> class.</returns>
		public KaiZen.CSMS.Entities.CompanySiteCategoryStandard GetByCompanyIdSiteId(TransactionManager transactionManager, System.Int32 _companyId, System.Int32 _siteId, int start, int pageLength)
		{
			int count = -1;
			return GetByCompanyIdSiteId(transactionManager, _companyId, _siteId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_CompanySiteCategoryStandard index.
		/// </summary>
		/// <param name="_companyId"></param>
		/// <param name="_siteId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompanySiteCategoryStandard"/> class.</returns>
		public KaiZen.CSMS.Entities.CompanySiteCategoryStandard GetByCompanyIdSiteId(System.Int32 _companyId, System.Int32 _siteId, int start, int pageLength, out int count)
		{
			return GetByCompanyIdSiteId(null, _companyId, _siteId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_CompanySiteCategoryStandard index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyId"></param>
		/// <param name="_siteId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompanySiteCategoryStandard"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.CompanySiteCategoryStandard GetByCompanyIdSiteId(TransactionManager transactionManager, System.Int32 _companyId, System.Int32 _siteId, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_CompanySiteCategoryStandard_1 index.
		/// </summary>
		/// <param name="_siteId"></param>
		/// <param name="_companySiteCategoryId"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;CompanySiteCategoryStandard&gt;"/> class.</returns>
		public TList<CompanySiteCategoryStandard> GetBySiteIdCompanySiteCategoryId(System.Int32 _siteId, System.Int32? _companySiteCategoryId)
		{
			int count = -1;
			return GetBySiteIdCompanySiteCategoryId(null,_siteId, _companySiteCategoryId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_CompanySiteCategoryStandard_1 index.
		/// </summary>
		/// <param name="_siteId"></param>
		/// <param name="_companySiteCategoryId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;CompanySiteCategoryStandard&gt;"/> class.</returns>
		public TList<CompanySiteCategoryStandard> GetBySiteIdCompanySiteCategoryId(System.Int32 _siteId, System.Int32? _companySiteCategoryId, int start, int pageLength)
		{
			int count = -1;
			return GetBySiteIdCompanySiteCategoryId(null, _siteId, _companySiteCategoryId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_CompanySiteCategoryStandard_1 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_siteId"></param>
		/// <param name="_companySiteCategoryId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;CompanySiteCategoryStandard&gt;"/> class.</returns>
		public TList<CompanySiteCategoryStandard> GetBySiteIdCompanySiteCategoryId(TransactionManager transactionManager, System.Int32 _siteId, System.Int32? _companySiteCategoryId)
		{
			int count = -1;
			return GetBySiteIdCompanySiteCategoryId(transactionManager, _siteId, _companySiteCategoryId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_CompanySiteCategoryStandard_1 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_siteId"></param>
		/// <param name="_companySiteCategoryId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;CompanySiteCategoryStandard&gt;"/> class.</returns>
		public TList<CompanySiteCategoryStandard> GetBySiteIdCompanySiteCategoryId(TransactionManager transactionManager, System.Int32 _siteId, System.Int32? _companySiteCategoryId, int start, int pageLength)
		{
			int count = -1;
			return GetBySiteIdCompanySiteCategoryId(transactionManager, _siteId, _companySiteCategoryId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_CompanySiteCategoryStandard_1 index.
		/// </summary>
		/// <param name="_siteId"></param>
		/// <param name="_companySiteCategoryId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;CompanySiteCategoryStandard&gt;"/> class.</returns>
		public TList<CompanySiteCategoryStandard> GetBySiteIdCompanySiteCategoryId(System.Int32 _siteId, System.Int32? _companySiteCategoryId, int start, int pageLength, out int count)
		{
			return GetBySiteIdCompanySiteCategoryId(null, _siteId, _companySiteCategoryId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_CompanySiteCategoryStandard_1 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_siteId"></param>
		/// <param name="_companySiteCategoryId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;CompanySiteCategoryStandard&gt;"/> class.</returns>
		public abstract TList<CompanySiteCategoryStandard> GetBySiteIdCompanySiteCategoryId(TransactionManager transactionManager, System.Int32 _siteId, System.Int32? _companySiteCategoryId, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#region _CompanySiteCategoryStandard_Sites_ByCompany_ResidentialOnly 
		
		/// <summary>
		///	This method wrap the '_CompanySiteCategoryStandard_Sites_ByCompany_ResidentialOnly' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Sites_ByCompany_ResidentialOnly(System.Int32? companyId)
		{
			return Sites_ByCompany_ResidentialOnly(null, 0, int.MaxValue , companyId);
		}
		
		/// <summary>
		///	This method wrap the '_CompanySiteCategoryStandard_Sites_ByCompany_ResidentialOnly' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Sites_ByCompany_ResidentialOnly(int start, int pageLength, System.Int32? companyId)
		{
			return Sites_ByCompany_ResidentialOnly(null, start, pageLength , companyId);
		}
				
		/// <summary>
		///	This method wrap the '_CompanySiteCategoryStandard_Sites_ByCompany_ResidentialOnly' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Sites_ByCompany_ResidentialOnly(TransactionManager transactionManager, System.Int32? companyId)
		{
			return Sites_ByCompany_ResidentialOnly(transactionManager, 0, int.MaxValue , companyId);
		}
		
		/// <summary>
		///	This method wrap the '_CompanySiteCategoryStandard_Sites_ByCompany_ResidentialOnly' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet Sites_ByCompany_ResidentialOnly(TransactionManager transactionManager, int start, int pageLength , System.Int32? companyId);
		
		#endregion
		
		#region _CompanySiteCategoryStandard_GetByRegionIdCompanySiteCategoryId 
		
		/// <summary>
		///	This method wrap the '_CompanySiteCategoryStandard_GetByRegionIdCompanySiteCategoryId' stored procedure. 
		/// </summary>
		/// <param name="regionId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companySiteCategoryId"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetByRegionIdCompanySiteCategoryId(System.Int32? regionId, System.Int32? companySiteCategoryId)
		{
			return GetByRegionIdCompanySiteCategoryId(null, 0, int.MaxValue , regionId, companySiteCategoryId);
		}
		
		/// <summary>
		///	This method wrap the '_CompanySiteCategoryStandard_GetByRegionIdCompanySiteCategoryId' stored procedure. 
		/// </summary>
		/// <param name="regionId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companySiteCategoryId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetByRegionIdCompanySiteCategoryId(int start, int pageLength, System.Int32? regionId, System.Int32? companySiteCategoryId)
		{
			return GetByRegionIdCompanySiteCategoryId(null, start, pageLength , regionId, companySiteCategoryId);
		}
				
		/// <summary>
		///	This method wrap the '_CompanySiteCategoryStandard_GetByRegionIdCompanySiteCategoryId' stored procedure. 
		/// </summary>
		/// <param name="regionId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companySiteCategoryId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetByRegionIdCompanySiteCategoryId(TransactionManager transactionManager, System.Int32? regionId, System.Int32? companySiteCategoryId)
		{
			return GetByRegionIdCompanySiteCategoryId(transactionManager, 0, int.MaxValue , regionId, companySiteCategoryId);
		}
		
		/// <summary>
		///	This method wrap the '_CompanySiteCategoryStandard_GetByRegionIdCompanySiteCategoryId' stored procedure. 
		/// </summary>
		/// <param name="regionId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companySiteCategoryId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet GetByRegionIdCompanySiteCategoryId(TransactionManager transactionManager, int start, int pageLength , System.Int32? regionId, System.Int32? companySiteCategoryId);
		
		#endregion
		
		#region _CompanySiteCategoryStandard_Sites_ByCompany 
		
		/// <summary>
		///	This method wrap the '_CompanySiteCategoryStandard_Sites_ByCompany' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Sites_ByCompany(System.Int32? companyId)
		{
			return Sites_ByCompany(null, 0, int.MaxValue , companyId);
		}
		
		/// <summary>
		///	This method wrap the '_CompanySiteCategoryStandard_Sites_ByCompany' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Sites_ByCompany(int start, int pageLength, System.Int32? companyId)
		{
			return Sites_ByCompany(null, start, pageLength , companyId);
		}
				
		/// <summary>
		///	This method wrap the '_CompanySiteCategoryStandard_Sites_ByCompany' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Sites_ByCompany(TransactionManager transactionManager, System.Int32? companyId)
		{
			return Sites_ByCompany(transactionManager, 0, int.MaxValue , companyId);
		}
		
		/// <summary>
		///	This method wrap the '_CompanySiteCategoryStandard_Sites_ByCompany' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet Sites_ByCompany(TransactionManager transactionManager, int start, int pageLength , System.Int32? companyId);
		
		#endregion
		
		#region _CompanySiteCategoryStandard_GetDistinctSites 
		
		/// <summary>
		///	This method wrap the '_CompanySiteCategoryStandard_GetDistinctSites' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetDistinctSites(System.Int32? companyId)
		{
			return GetDistinctSites(null, 0, int.MaxValue , companyId);
		}
		
		/// <summary>
		///	This method wrap the '_CompanySiteCategoryStandard_GetDistinctSites' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetDistinctSites(int start, int pageLength, System.Int32? companyId)
		{
			return GetDistinctSites(null, start, pageLength , companyId);
		}
				
		/// <summary>
		///	This method wrap the '_CompanySiteCategoryStandard_GetDistinctSites' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetDistinctSites(TransactionManager transactionManager, System.Int32? companyId)
		{
			return GetDistinctSites(transactionManager, 0, int.MaxValue , companyId);
		}
		
		/// <summary>
		///	This method wrap the '_CompanySiteCategoryStandard_GetDistinctSites' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet GetDistinctSites(TransactionManager transactionManager, int start, int pageLength , System.Int32? companyId);
		
		#endregion
		
		#region _CompanySiteCategoryStandard_Report 
		
		/// <summary>
		///	This method wrap the '_CompanySiteCategoryStandard_Report' stored procedure. 
		/// </summary>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Report()
		{
			return Report(null, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_CompanySiteCategoryStandard_Report' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Report(int start, int pageLength)
		{
			return Report(null, start, pageLength );
		}
				
		/// <summary>
		///	This method wrap the '_CompanySiteCategoryStandard_Report' stored procedure. 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Report(TransactionManager transactionManager)
		{
			return Report(transactionManager, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_CompanySiteCategoryStandard_Report' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet Report(TransactionManager transactionManager, int start, int pageLength );
		
		#endregion
		
		#region _CompanySiteCategoryStandard_GetByQuestionnaireId 
		
		/// <summary>
		///	This method wrap the '_CompanySiteCategoryStandard_GetByQuestionnaireId' stored procedure. 
		/// </summary>
		/// <param name="questionnaireId"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetByQuestionnaireId(System.Int32? questionnaireId)
		{
			return GetByQuestionnaireId(null, 0, int.MaxValue , questionnaireId);
		}
		
		/// <summary>
		///	This method wrap the '_CompanySiteCategoryStandard_GetByQuestionnaireId' stored procedure. 
		/// </summary>
		/// <param name="questionnaireId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetByQuestionnaireId(int start, int pageLength, System.Int32? questionnaireId)
		{
			return GetByQuestionnaireId(null, start, pageLength , questionnaireId);
		}
				
		/// <summary>
		///	This method wrap the '_CompanySiteCategoryStandard_GetByQuestionnaireId' stored procedure. 
		/// </summary>
		/// <param name="questionnaireId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetByQuestionnaireId(TransactionManager transactionManager, System.Int32? questionnaireId)
		{
			return GetByQuestionnaireId(transactionManager, 0, int.MaxValue , questionnaireId);
		}
		
		/// <summary>
		///	This method wrap the '_CompanySiteCategoryStandard_GetByQuestionnaireId' stored procedure. 
		/// </summary>
		/// <param name="questionnaireId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet GetByQuestionnaireId(TransactionManager transactionManager, int start, int pageLength , System.Int32? questionnaireId);
		
		#endregion

        #region Item# 19

        public int Set_Arp(String ArpName, System.Int32? ArpSiteId, string personIdARP)
        {
            return Set_Arp(null, 0, int.MaxValue, ArpName, ArpSiteId, personIdARP);
        }
        public abstract int Set_Arp(TransactionManager transactionManager, int start, int pageLength, String ArpName, System.Int32? ArpSiteId, string personIdARP);

        public int Update_Arp(String ArpName, System.Int32? ArpUserId, System.Int32? ArpSiteId, string personIdARP)
        {
            return Update_Arp(null, 0, int.MaxValue, ArpName, ArpUserId, ArpSiteId, personIdARP);
        }
        public abstract int Update_Arp(TransactionManager transactionManager, int start, int pageLength, String ArpName
            , System.Int32? ArpUserId, System.Int32? ArpSiteId, string personIdARP);


        public DataSet GetTotalArp()
        {
            return GetTotalArp(null, 0, int.MaxValue);
        }
        public abstract DataSet GetTotalArp(TransactionManager transactionManager, int start, int pageLength);


        public DataSet GetArp_PersonId(int Val)
        {
            return GetArp_PersonId(null, 0, int.MaxValue, Val);
        }
        public abstract DataSet GetArp_PersonId(TransactionManager transactionManager, int start, int pageLength, int Val);


        public DataSet getCompanySiteCategoryStandard()
        {
            return getCompanySiteCategoryStandard(null, 0, int.MaxValue);
        }
        public abstract DataSet getCompanySiteCategoryStandard(TransactionManager transactionManager, int start, int pageLength);

        #endregion

        #endregion

        #region Helper Functions

        /// <summary>
		/// Fill a TList&lt;CompanySiteCategoryStandard&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;CompanySiteCategoryStandard&gt;"/></returns>
		public static TList<CompanySiteCategoryStandard> Fill(IDataReader reader, TList<CompanySiteCategoryStandard> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.CompanySiteCategoryStandard c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("CompanySiteCategoryStandard")
					.Append("|").Append((System.Int32)reader[((int)CompanySiteCategoryStandardColumn.CompanySiteCategoryStandardId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<CompanySiteCategoryStandard>(
					key.ToString(), // EntityTrackingKey
					"CompanySiteCategoryStandard",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.CompanySiteCategoryStandard();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.CompanySiteCategoryStandardId = (System.Int32)reader[((int)CompanySiteCategoryStandardColumn.CompanySiteCategoryStandardId - 1)];
					c.CompanyId = (System.Int32)reader[((int)CompanySiteCategoryStandardColumn.CompanyId - 1)];
					c.SiteId = (System.Int32)reader[((int)CompanySiteCategoryStandardColumn.SiteId - 1)];
					c.CompanySiteCategoryId = (reader.IsDBNull(((int)CompanySiteCategoryStandardColumn.CompanySiteCategoryId - 1)))?null:(System.Int32?)reader[((int)CompanySiteCategoryStandardColumn.CompanySiteCategoryId - 1)];
					c.LocationSpaUserId = (reader.IsDBNull(((int)CompanySiteCategoryStandardColumn.LocationSpaUserId - 1)))?null:(System.Int32?)reader[((int)CompanySiteCategoryStandardColumn.LocationSpaUserId - 1)];
					c.LocationSponsorUserId = (reader.IsDBNull(((int)CompanySiteCategoryStandardColumn.LocationSponsorUserId - 1)))?null:(System.Int32?)reader[((int)CompanySiteCategoryStandardColumn.LocationSponsorUserId - 1)];
					c.ArpUserId = (reader.IsDBNull(((int)CompanySiteCategoryStandardColumn.ArpUserId - 1)))?null:(System.Int32?)reader[((int)CompanySiteCategoryStandardColumn.ArpUserId - 1)];
					c.Area = (reader.IsDBNull(((int)CompanySiteCategoryStandardColumn.Area - 1)))?null:(System.String)reader[((int)CompanySiteCategoryStandardColumn.Area - 1)];
					c.Approved = (reader.IsDBNull(((int)CompanySiteCategoryStandardColumn.Approved - 1)))?null:(System.Boolean?)reader[((int)CompanySiteCategoryStandardColumn.Approved - 1)];
					c.ApprovedByUserId = (reader.IsDBNull(((int)CompanySiteCategoryStandardColumn.ApprovedByUserId - 1)))?null:(System.Int32?)reader[((int)CompanySiteCategoryStandardColumn.ApprovedByUserId - 1)];
					c.ApprovedDate = (reader.IsDBNull(((int)CompanySiteCategoryStandardColumn.ApprovedDate - 1)))?null:(System.DateTime?)reader[((int)CompanySiteCategoryStandardColumn.ApprovedDate - 1)];
					c.ModifiedByUserId = (System.Int32)reader[((int)CompanySiteCategoryStandardColumn.ModifiedByUserId - 1)];
					c.ModifiedDate = (System.DateTime)reader[((int)CompanySiteCategoryStandardColumn.ModifiedDate - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.CompanySiteCategoryStandard"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.CompanySiteCategoryStandard"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.CompanySiteCategoryStandard entity)
		{
			if (!reader.Read()) return;
			
			entity.CompanySiteCategoryStandardId = (System.Int32)reader[((int)CompanySiteCategoryStandardColumn.CompanySiteCategoryStandardId - 1)];
			entity.CompanyId = (System.Int32)reader[((int)CompanySiteCategoryStandardColumn.CompanyId - 1)];
			entity.SiteId = (System.Int32)reader[((int)CompanySiteCategoryStandardColumn.SiteId - 1)];
			entity.CompanySiteCategoryId = (reader.IsDBNull(((int)CompanySiteCategoryStandardColumn.CompanySiteCategoryId - 1)))?null:(System.Int32?)reader[((int)CompanySiteCategoryStandardColumn.CompanySiteCategoryId - 1)];
			entity.LocationSpaUserId = (reader.IsDBNull(((int)CompanySiteCategoryStandardColumn.LocationSpaUserId - 1)))?null:(System.Int32?)reader[((int)CompanySiteCategoryStandardColumn.LocationSpaUserId - 1)];
			entity.LocationSponsorUserId = (reader.IsDBNull(((int)CompanySiteCategoryStandardColumn.LocationSponsorUserId - 1)))?null:(System.Int32?)reader[((int)CompanySiteCategoryStandardColumn.LocationSponsorUserId - 1)];
			entity.ArpUserId = (reader.IsDBNull(((int)CompanySiteCategoryStandardColumn.ArpUserId - 1)))?null:(System.Int32?)reader[((int)CompanySiteCategoryStandardColumn.ArpUserId - 1)];
			entity.Area = (reader.IsDBNull(((int)CompanySiteCategoryStandardColumn.Area - 1)))?null:(System.String)reader[((int)CompanySiteCategoryStandardColumn.Area - 1)];
			entity.Approved = (reader.IsDBNull(((int)CompanySiteCategoryStandardColumn.Approved - 1)))?null:(System.Boolean?)reader[((int)CompanySiteCategoryStandardColumn.Approved - 1)];
			entity.ApprovedByUserId = (reader.IsDBNull(((int)CompanySiteCategoryStandardColumn.ApprovedByUserId - 1)))?null:(System.Int32?)reader[((int)CompanySiteCategoryStandardColumn.ApprovedByUserId - 1)];
			entity.ApprovedDate = (reader.IsDBNull(((int)CompanySiteCategoryStandardColumn.ApprovedDate - 1)))?null:(System.DateTime?)reader[((int)CompanySiteCategoryStandardColumn.ApprovedDate - 1)];
			entity.ModifiedByUserId = (System.Int32)reader[((int)CompanySiteCategoryStandardColumn.ModifiedByUserId - 1)];
			entity.ModifiedDate = (System.DateTime)reader[((int)CompanySiteCategoryStandardColumn.ModifiedDate - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.CompanySiteCategoryStandard"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.CompanySiteCategoryStandard"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.CompanySiteCategoryStandard entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.CompanySiteCategoryStandardId = (System.Int32)dataRow["CompanySiteCategoryStandardId"];
			entity.CompanyId = (System.Int32)dataRow["CompanyId"];
			entity.SiteId = (System.Int32)dataRow["SiteId"];
			entity.CompanySiteCategoryId = Convert.IsDBNull(dataRow["CompanySiteCategoryId"]) ? null : (System.Int32?)dataRow["CompanySiteCategoryId"];
			entity.LocationSpaUserId = Convert.IsDBNull(dataRow["LocationSpaUserId"]) ? null : (System.Int32?)dataRow["LocationSpaUserId"];
			entity.LocationSponsorUserId = Convert.IsDBNull(dataRow["LocationSponsorUserId"]) ? null : (System.Int32?)dataRow["LocationSponsorUserId"];
			entity.ArpUserId = Convert.IsDBNull(dataRow["ArpUserId"]) ? null : (System.Int32?)dataRow["ArpUserId"];
			entity.Area = Convert.IsDBNull(dataRow["Area"]) ? null : (System.String)dataRow["Area"];
			entity.Approved = Convert.IsDBNull(dataRow["Approved"]) ? null : (System.Boolean?)dataRow["Approved"];
			entity.ApprovedByUserId = Convert.IsDBNull(dataRow["ApprovedByUserId"]) ? null : (System.Int32?)dataRow["ApprovedByUserId"];
			entity.ApprovedDate = Convert.IsDBNull(dataRow["ApprovedDate"]) ? null : (System.DateTime?)dataRow["ApprovedDate"];
			entity.ModifiedByUserId = (System.Int32)dataRow["ModifiedByUserId"];
			entity.ModifiedDate = (System.DateTime)dataRow["ModifiedDate"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.CompanySiteCategoryStandard"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.CompanySiteCategoryStandard Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.CompanySiteCategoryStandard entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region ModifiedByUserIdSource	
			if (CanDeepLoad(entity, "Users|ModifiedByUserIdSource", deepLoadType, innerList) 
				&& entity.ModifiedByUserIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.ModifiedByUserId;
				Users tmpEntity = EntityManager.LocateEntity<Users>(EntityLocator.ConstructKeyFromPkItems(typeof(Users), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.ModifiedByUserIdSource = tmpEntity;
				else
					entity.ModifiedByUserIdSource = DataRepository.UsersProvider.GetByUserId(transactionManager, entity.ModifiedByUserId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ModifiedByUserIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.ModifiedByUserIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.UsersProvider.DeepLoad(transactionManager, entity.ModifiedByUserIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion ModifiedByUserIdSource

			#region ApprovedByUserIdSource	
			if (CanDeepLoad(entity, "Users|ApprovedByUserIdSource", deepLoadType, innerList) 
				&& entity.ApprovedByUserIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.ApprovedByUserId ?? (int)0);
				Users tmpEntity = EntityManager.LocateEntity<Users>(EntityLocator.ConstructKeyFromPkItems(typeof(Users), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.ApprovedByUserIdSource = tmpEntity;
				else
					entity.ApprovedByUserIdSource = DataRepository.UsersProvider.GetByUserId(transactionManager, (entity.ApprovedByUserId ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ApprovedByUserIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.ApprovedByUserIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.UsersProvider.DeepLoad(transactionManager, entity.ApprovedByUserIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion ApprovedByUserIdSource

			#region LocationSponsorUserIdSource	
			if (CanDeepLoad(entity, "Users|LocationSponsorUserIdSource", deepLoadType, innerList) 
				&& entity.LocationSponsorUserIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.LocationSponsorUserId ?? (int)0);
				Users tmpEntity = EntityManager.LocateEntity<Users>(EntityLocator.ConstructKeyFromPkItems(typeof(Users), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.LocationSponsorUserIdSource = tmpEntity;
				else
					entity.LocationSponsorUserIdSource = DataRepository.UsersProvider.GetByUserId(transactionManager, (entity.LocationSponsorUserId ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'LocationSponsorUserIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.LocationSponsorUserIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.UsersProvider.DeepLoad(transactionManager, entity.LocationSponsorUserIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion LocationSponsorUserIdSource

			#region ArpUserIdSource	
			if (CanDeepLoad(entity, "Users|ArpUserIdSource", deepLoadType, innerList) 
				&& entity.ArpUserIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.ArpUserId ?? (int)0);
				Users tmpEntity = EntityManager.LocateEntity<Users>(EntityLocator.ConstructKeyFromPkItems(typeof(Users), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.ArpUserIdSource = tmpEntity;
				else
					entity.ArpUserIdSource = DataRepository.UsersProvider.GetByUserId(transactionManager, (entity.ArpUserId ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ArpUserIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.ArpUserIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.UsersProvider.DeepLoad(transactionManager, entity.ArpUserIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion ArpUserIdSource

			#region CompanyIdSource	
			if (CanDeepLoad(entity, "Companies|CompanyIdSource", deepLoadType, innerList) 
				&& entity.CompanyIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.CompanyId;
				Companies tmpEntity = EntityManager.LocateEntity<Companies>(EntityLocator.ConstructKeyFromPkItems(typeof(Companies), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.CompanyIdSource = tmpEntity;
				else
					entity.CompanyIdSource = DataRepository.CompaniesProvider.GetByCompanyId(transactionManager, entity.CompanyId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CompanyIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.CompanyIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.CompaniesProvider.DeepLoad(transactionManager, entity.CompanyIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion CompanyIdSource

			#region CompanySiteCategoryIdSource	
			if (CanDeepLoad(entity, "CompanySiteCategory|CompanySiteCategoryIdSource", deepLoadType, innerList) 
				&& entity.CompanySiteCategoryIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.CompanySiteCategoryId ?? (int)0);
				CompanySiteCategory tmpEntity = EntityManager.LocateEntity<CompanySiteCategory>(EntityLocator.ConstructKeyFromPkItems(typeof(CompanySiteCategory), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.CompanySiteCategoryIdSource = tmpEntity;
				else
					entity.CompanySiteCategoryIdSource = DataRepository.CompanySiteCategoryProvider.GetByCompanySiteCategoryId(transactionManager, (entity.CompanySiteCategoryId ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CompanySiteCategoryIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.CompanySiteCategoryIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.CompanySiteCategoryProvider.DeepLoad(transactionManager, entity.CompanySiteCategoryIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion CompanySiteCategoryIdSource

			#region SiteIdSource	
			if (CanDeepLoad(entity, "Sites|SiteIdSource", deepLoadType, innerList) 
				&& entity.SiteIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.SiteId;
				Sites tmpEntity = EntityManager.LocateEntity<Sites>(EntityLocator.ConstructKeyFromPkItems(typeof(Sites), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.SiteIdSource = tmpEntity;
				else
					entity.SiteIdSource = DataRepository.SitesProvider.GetBySiteId(transactionManager, entity.SiteId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'SiteIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.SiteIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.SitesProvider.DeepLoad(transactionManager, entity.SiteIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion SiteIdSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.CompanySiteCategoryStandard object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.CompanySiteCategoryStandard instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.CompanySiteCategoryStandard Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.CompanySiteCategoryStandard entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region ModifiedByUserIdSource
			if (CanDeepSave(entity, "Users|ModifiedByUserIdSource", deepSaveType, innerList) 
				&& entity.ModifiedByUserIdSource != null)
			{
				DataRepository.UsersProvider.Save(transactionManager, entity.ModifiedByUserIdSource);
				entity.ModifiedByUserId = entity.ModifiedByUserIdSource.UserId;
			}
			#endregion 
			
			#region ApprovedByUserIdSource
			if (CanDeepSave(entity, "Users|ApprovedByUserIdSource", deepSaveType, innerList) 
				&& entity.ApprovedByUserIdSource != null)
			{
				DataRepository.UsersProvider.Save(transactionManager, entity.ApprovedByUserIdSource);
				entity.ApprovedByUserId = entity.ApprovedByUserIdSource.UserId;
			}
			#endregion 
			
			#region LocationSponsorUserIdSource
			if (CanDeepSave(entity, "Users|LocationSponsorUserIdSource", deepSaveType, innerList) 
				&& entity.LocationSponsorUserIdSource != null)
			{
				DataRepository.UsersProvider.Save(transactionManager, entity.LocationSponsorUserIdSource);
				entity.LocationSponsorUserId = entity.LocationSponsorUserIdSource.UserId;
			}
			#endregion 
			
			#region ArpUserIdSource
			if (CanDeepSave(entity, "Users|ArpUserIdSource", deepSaveType, innerList) 
				&& entity.ArpUserIdSource != null)
			{
				DataRepository.UsersProvider.Save(transactionManager, entity.ArpUserIdSource);
				entity.ArpUserId = entity.ArpUserIdSource.UserId;
			}
			#endregion 
			
			#region CompanyIdSource
			if (CanDeepSave(entity, "Companies|CompanyIdSource", deepSaveType, innerList) 
				&& entity.CompanyIdSource != null)
			{
				DataRepository.CompaniesProvider.Save(transactionManager, entity.CompanyIdSource);
				entity.CompanyId = entity.CompanyIdSource.CompanyId;
			}
			#endregion 
			
			#region CompanySiteCategoryIdSource
			if (CanDeepSave(entity, "CompanySiteCategory|CompanySiteCategoryIdSource", deepSaveType, innerList) 
				&& entity.CompanySiteCategoryIdSource != null)
			{
				DataRepository.CompanySiteCategoryProvider.Save(transactionManager, entity.CompanySiteCategoryIdSource);
				entity.CompanySiteCategoryId = entity.CompanySiteCategoryIdSource.CompanySiteCategoryId;
			}
			#endregion 
			
			#region SiteIdSource
			if (CanDeepSave(entity, "Sites|SiteIdSource", deepSaveType, innerList) 
				&& entity.SiteIdSource != null)
			{
				DataRepository.SitesProvider.Save(transactionManager, entity.SiteIdSource);
				entity.SiteId = entity.SiteIdSource.SiteId;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region CompanySiteCategoryStandardChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.CompanySiteCategoryStandard</c>
	///</summary>
	public enum CompanySiteCategoryStandardChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>Users</c> at ModifiedByUserIdSource
		///</summary>
		[ChildEntityType(typeof(Users))]
		Users,
			
		///<summary>
		/// Composite Property for <c>Companies</c> at CompanyIdSource
		///</summary>
		[ChildEntityType(typeof(Companies))]
		Companies,
			
		///<summary>
		/// Composite Property for <c>CompanySiteCategory</c> at CompanySiteCategoryIdSource
		///</summary>
		[ChildEntityType(typeof(CompanySiteCategory))]
		CompanySiteCategory,
			
		///<summary>
		/// Composite Property for <c>Sites</c> at SiteIdSource
		///</summary>
		[ChildEntityType(typeof(Sites))]
		Sites,
		}
	
	#endregion CompanySiteCategoryStandardChildEntityTypes
	
	#region CompanySiteCategoryStandardFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;CompanySiteCategoryStandardColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CompanySiteCategoryStandard"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompanySiteCategoryStandardFilterBuilder : SqlFilterBuilder<CompanySiteCategoryStandardColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompanySiteCategoryStandardFilterBuilder class.
		/// </summary>
		public CompanySiteCategoryStandardFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the CompanySiteCategoryStandardFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CompanySiteCategoryStandardFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CompanySiteCategoryStandardFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CompanySiteCategoryStandardFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CompanySiteCategoryStandardFilterBuilder
	
	#region CompanySiteCategoryStandardParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;CompanySiteCategoryStandardColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CompanySiteCategoryStandard"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompanySiteCategoryStandardParameterBuilder : ParameterizedSqlFilterBuilder<CompanySiteCategoryStandardColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompanySiteCategoryStandardParameterBuilder class.
		/// </summary>
		public CompanySiteCategoryStandardParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the CompanySiteCategoryStandardParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CompanySiteCategoryStandardParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CompanySiteCategoryStandardParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CompanySiteCategoryStandardParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CompanySiteCategoryStandardParameterBuilder
	
	#region CompanySiteCategoryStandardSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;CompanySiteCategoryStandardColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CompanySiteCategoryStandard"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class CompanySiteCategoryStandardSortBuilder : SqlSortBuilder<CompanySiteCategoryStandardColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompanySiteCategoryStandardSqlSortBuilder class.
		/// </summary>
		public CompanySiteCategoryStandardSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion CompanySiteCategoryStandardSortBuilder
	
} // end namespace
