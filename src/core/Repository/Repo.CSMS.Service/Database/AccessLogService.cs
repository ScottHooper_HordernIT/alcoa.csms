﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface IAccessLogService
    {
        AccessLog Get(Expression<Func<AccessLog, bool>> where, List<Expression<Func<AccessLog, object>>> includes);
        List<AccessLog> GetMany(int? top, Expression<Func<AccessLog, bool>> where, Func<IQueryable<AccessLog>, IOrderedQueryable<AccessLog>> orderBy, List<Expression<Func<AccessLog, object>>> includes);
        List<AccessLog> GetManyPaged(int pageNumber, int pageSize, Expression<Func<AccessLog, bool>> where, Func<IQueryable<AccessLog>, IOrderedQueryable<AccessLog>> orderBy, List<Expression<Func<AccessLog, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<AccessLog, bool>> where, List<Expression<Func<AccessLog, object>>> includes);
        void Insert(AccessLog item, bool saveChanges = true);
        void Update(AccessLog item, bool saveChanges = true);
        void Delete(AccessLog item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class AccessLogService : IAccessLogService
    {
        private readonly IRepository<AccessLog> _repository;

        public AccessLogService(IRepository<AccessLog> repository)
        {
            this._repository = repository;
        }

        public AccessLog Get(Expression<Func<AccessLog, bool>> where, List<Expression<Func<AccessLog, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<AccessLog> GetMany(int? top, Expression<Func<AccessLog, bool>> where, Func<IQueryable<AccessLog>, IOrderedQueryable<AccessLog>> orderBy, List<Expression<Func<AccessLog, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<AccessLog> GetManyPaged(int pageNumber, int pageSize, Expression<Func<AccessLog, bool>> where, Func<IQueryable<AccessLog>, IOrderedQueryable<AccessLog>> orderBy, List<Expression<Func<AccessLog, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<AccessLog, bool>> where, List<Expression<Func<AccessLog, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(AccessLog item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("AccessLog");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(AccessLog item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("AccessLog");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(AccessLog item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("AccessLog");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
