﻿
using System;

namespace KaiZen.CSMS.Entities
{
	/// <summary>
	/// An enum representation of the 'FileVaultSubCategory' table. [No description found in the database]
	/// </summary>
	/// <remark>this enumeration contains the items contained in the table FileVaultSubCategory</remark>
	[Serializable]
	public enum FileVaultSubCategoryList
	{
		/// <summary> 
		/// Everyone
		/// </summary>
		[EnumTextValue(@"Everyone")]
		HelpFiles_Everyone = 1, 

		/// <summary> 
		/// Administrator
		/// </summary>
		[EnumTextValue(@"Administrator")]
		HelpFiles_Administrator = 2, 

		/// <summary> 
		/// Contractor (Pre-Qual)
		/// </summary>
		[EnumTextValue(@"Contractor (Pre-Qual)")]
		HelpFiles_Contractor_Prequal = 3, 

		/// <summary> 
		/// Contractor
		/// </summary>
		[EnumTextValue(@"Contractor")]
		HelpFiles_Contractor_Requal = 4, 

		/// <summary> 
		/// Contractor - View Engineering Project Hours
		/// </summary>
		[EnumTextValue(@"Contractor - View Engineering Project Hours")]
		HelpFiles_Contractor_ViewEngineeringHours = 5, 

		/// <summary> 
		/// H&S Assessor
		/// </summary>
		[EnumTextValue(@"H&S Assessor")]
		HelpFiles_HsAssessor = 6, 

		/// <summary> 
		/// Procurement
		/// </summary>
		[EnumTextValue(@"Procurement")]
		HelpFiles_Procurement = 7, 

		/// <summary> 
		/// Reader
		/// </summary>
		[EnumTextValue(@"Reader")]
		HelpFiles_Reader = 8, 

		/// <summary> 
		/// Reader - EBI Data Access
		/// </summary>
		[EnumTextValue(@"Reader - EBI Data Access")]
		HelpFiles_Reader_Ebi = 9, 

		/// <summary> 
		/// Reader - Safety Qualification Read Access
		/// </summary>
		[EnumTextValue(@"Reader - Safety Qualification Read Access")]
		HelpFiles_Reader_SafetyQualificationReadAccess = 10, 

		/// <summary> 
		/// Reader - Financial Reporting Access
		/// </summary>
		[EnumTextValue(@"Reader - Financial Reporting Access")]
		HelpFiles_Reader_FinancialReportingAccess = 11, 

		/// <summary> 
		/// Reader - Training Package Trainers
		/// </summary>
		[EnumTextValue(@"Reader - Training Package Trainers")]
		HelpFiles_Reader_TrainingPackageTrainers = 12, 

		/// <summary> 
		/// H&S Assessor - Lead
		/// </summary>
		[EnumTextValue(@"H&S Assessor - Lead")]
		HelpFiles_HsAssessor_Lead = 14

	}
}
