﻿using System;
using System.ComponentModel;

namespace KaiZen.CSMS.Entities
{
	/// <summary>
	///		The data structure representation of the 'Ebi' table via interface.
	/// </summary>
	/// <remarks>
	/// 	This struct is generated by a tool and should never be modified.
	/// </remarks>
	public interface IEbi 
	{
		/// <summary>			
		/// EbiId : 
		/// </summary>
		/// <remarks>Member of the primary key of the underlying table "Ebi"</remarks>
		System.Int32 EbiId { get; set; }
				
		
		
		/// <summary>
		/// CompanyName : 
		/// </summary>
		System.String  CompanyName  { get; set; }
		
		/// <summary>
		/// ClockId : 
		/// </summary>
		System.Int32?  ClockId  { get; set; }
		
		/// <summary>
		/// FullName : 
		/// </summary>
		System.String  FullName  { get; set; }
		
		/// <summary>
		/// AccessCardNo : 
		/// </summary>
		System.Int32?  AccessCardNo  { get; set; }
		
		/// <summary>
		/// SwipeSite : 
		/// </summary>
		System.String  SwipeSite  { get; set; }
		
		/// <summary>
		/// DataChecked : 
		/// </summary>
		System.Boolean?  DataChecked  { get; set; }
		
		/// <summary>
		/// SwipeDateTime : 
		/// </summary>
		System.DateTime  SwipeDateTime  { get; set; }
		
		/// <summary>
		/// SwipeYear : 
		/// </summary>
		System.Int32  SwipeYear  { get; set; }
		
		/// <summary>
		/// SwipeMonth : 
		/// </summary>
		System.Int32  SwipeMonth  { get; set; }
		
		/// <summary>
		/// SwipeDay : 
		/// </summary>
		System.Int32  SwipeDay  { get; set; }
			
		/// <summary>
		/// Creates a new object that is a copy of the current instance.
		/// </summary>
		/// <returns>A new object that is a copy of this instance.</returns>
		System.Object Clone();
		
		#region Data Properties

		#endregion Data Properties

	}
}


