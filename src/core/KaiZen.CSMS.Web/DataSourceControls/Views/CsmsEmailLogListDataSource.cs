﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.CsmsEmailLogListProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[CLSCompliant(true)]
	[Designer(typeof(CsmsEmailLogListDataSourceDesigner))]
	public class CsmsEmailLogListDataSource : ReadOnlyDataSource<CsmsEmailLogList>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CsmsEmailLogListDataSource class.
		/// </summary>
		public CsmsEmailLogListDataSource() : base(new CsmsEmailLogListService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the CsmsEmailLogListDataSourceView used by the CsmsEmailLogListDataSource.
		/// </summary>
		protected CsmsEmailLogListDataSourceView CsmsEmailLogListView
		{
			get { return ( View as CsmsEmailLogListDataSourceView ); }
		}
		
		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the CsmsEmailLogListDataSourceView class that is to be
		/// used by the CsmsEmailLogListDataSource.
		/// </summary>
		/// <returns>An instance of the CsmsEmailLogListDataSourceView class.</returns>
		protected override BaseDataSourceView<CsmsEmailLogList, Object> GetNewDataSourceView()
		{
			return new CsmsEmailLogListDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the CsmsEmailLogListDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class CsmsEmailLogListDataSourceView : ReadOnlyDataSourceView<CsmsEmailLogList>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CsmsEmailLogListDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the CsmsEmailLogListDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public CsmsEmailLogListDataSourceView(CsmsEmailLogListDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal CsmsEmailLogListDataSource CsmsEmailLogListOwner
		{
			get { return Owner as CsmsEmailLogListDataSource; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal CsmsEmailLogListService CsmsEmailLogListProvider
		{
			get { return Provider as CsmsEmailLogListService; }
		}

		#endregion Properties
		
		#region Methods
		
		#endregion Methods
	}

	#region CsmsEmailLogListDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the CsmsEmailLogListDataSource class.
	/// </summary>
	public class CsmsEmailLogListDataSourceDesigner : ReadOnlyDataSourceDesigner<CsmsEmailLogList>
	{
	}

	#endregion CsmsEmailLogListDataSourceDesigner

	#region CsmsEmailLogListFilter

	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CsmsEmailLogList"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CsmsEmailLogListFilter : SqlFilter<CsmsEmailLogListColumn>
	{
	}

	#endregion CsmsEmailLogListFilter

	#region CsmsEmailLogListExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CsmsEmailLogList"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CsmsEmailLogListExpressionBuilder : SqlExpressionBuilder<CsmsEmailLogListColumn>
	{
	}
	
	#endregion CsmsEmailLogListExpressionBuilder		
}

