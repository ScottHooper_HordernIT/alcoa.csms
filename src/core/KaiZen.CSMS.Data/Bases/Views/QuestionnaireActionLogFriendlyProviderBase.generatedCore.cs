﻿#region Using directives

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="QuestionnaireActionLogFriendlyProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract class QuestionnaireActionLogFriendlyProviderBaseCore : EntityViewProviderBase<QuestionnaireActionLogFriendly>
	{
		#region Custom Methods
		
		
		#region _QuestionnaireActionLogFriendly_GetByQuestionnaireId
		
		/// <summary>
		///	This method wrap the '_QuestionnaireActionLogFriendly_GetByQuestionnaireId' stored procedure. 
		/// </summary>
		/// <param name="questionnaireId"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetByQuestionnaireId(System.Int32? questionnaireId)
		{
			return GetByQuestionnaireId(null, 0, int.MaxValue , questionnaireId);
		}
		
		/// <summary>
		///	This method wrap the '_QuestionnaireActionLogFriendly_GetByQuestionnaireId' stored procedure. 
		/// </summary>
		/// <param name="questionnaireId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetByQuestionnaireId(int start, int pageLength, System.Int32? questionnaireId)
		{
			return GetByQuestionnaireId(null, start, pageLength , questionnaireId);
		}
				
		/// <summary>
		///	This method wrap the '_QuestionnaireActionLogFriendly_GetByQuestionnaireId' stored procedure. 
		/// </summary>
		/// <param name="questionnaireId"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetByQuestionnaireId(TransactionManager transactionManager, System.Int32? questionnaireId)
		{
			return GetByQuestionnaireId(transactionManager, 0, int.MaxValue , questionnaireId);
		}
		
		/// <summary>
		///	This method wrap the '_QuestionnaireActionLogFriendly_GetByQuestionnaireId' stored procedure. 
		/// </summary>
		/// <param name="questionnaireId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet GetByQuestionnaireId(TransactionManager transactionManager, int start, int pageLength, System.Int32? questionnaireId);
		
		#endregion

		
		#endregion

		#region Helper Functions
		
		/*
		///<summary>
		/// Fill an VList&lt;QuestionnaireActionLogFriendly&gt; From a DataSet
		///</summary>
		/// <param name="dataSet">the DataSet</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList&lt;QuestionnaireActionLogFriendly&gt;"/></returns>
		protected static VList&lt;QuestionnaireActionLogFriendly&gt; Fill(DataSet dataSet, VList<QuestionnaireActionLogFriendly> rows, int start, int pagelen)
		{
			if (dataSet.Tables.Count == 1)
			{
				return Fill(dataSet.Tables[0], rows, start, pagelen);
			}
			else
			{
				return new VList<QuestionnaireActionLogFriendly>();
			}	
		}
		
		
		///<summary>
		/// Fill an VList&lt;QuestionnaireActionLogFriendly&gt; From a DataTable
		///</summary>
		/// <param name="dataTable">the DataTable that hold the data.</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList<QuestionnaireActionLogFriendly>"/></returns>
		protected static VList&lt;QuestionnaireActionLogFriendly&gt; Fill(DataTable dataTable, VList<QuestionnaireActionLogFriendly> rows, int start, int pagelen)
		{
			int recordnum = 0;
			
			System.Collections.IEnumerator dataRows =  dataTable.Rows.GetEnumerator();
			
			while (dataRows.MoveNext() && (pagelen != 0))
			{
				if(recordnum >= start)
				{
					DataRow row = (DataRow)dataRows.Current;
				
					QuestionnaireActionLogFriendly c = new QuestionnaireActionLogFriendly();
					c.QuestionnaireActionLogId = (Convert.IsDBNull(row["QuestionnaireActionLogId"]))?(int)0:(System.Int32)row["QuestionnaireActionLogId"];
					c.QuestionnaireId = (Convert.IsDBNull(row["QuestionnaireId"]))?(int)0:(System.Int32)row["QuestionnaireId"];
					c.CreatedDate = (Convert.IsDBNull(row["CreatedDate"]))?DateTime.MinValue:(System.DateTime)row["CreatedDate"];
					c.CreatedByPrivledgedRole = (Convert.IsDBNull(row["CreatedByPrivledgedRole"]))?string.Empty:(System.String)row["CreatedByPrivledgedRole"];
					c.HistoryLog = (Convert.IsDBNull(row["HistoryLog"]))?string.Empty:(System.String)row["HistoryLog"];
					c.ActionDescription = (Convert.IsDBNull(row["ActionDescription"]))?string.Empty:(System.String)row["ActionDescription"];
					c.FullName = (Convert.IsDBNull(row["FullName"]))?string.Empty:(System.String)row["FullName"];
					c.CompanyName = (Convert.IsDBNull(row["CompanyName"]))?string.Empty:(System.String)row["CompanyName"];
					c.Role = (Convert.IsDBNull(row["Role"]))?string.Empty:(System.String)row["Role"];
					c.AcceptChanges();
					rows.Add(c);
					pagelen -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		*/	
						
		///<summary>
		/// Fill an <see cref="VList&lt;QuestionnaireActionLogFriendly&gt;"/> From a DataReader.
		///</summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pageLength">number of row.</param>
		///<returns>a <see cref="VList&lt;QuestionnaireActionLogFriendly&gt;"/></returns>
		protected VList<QuestionnaireActionLogFriendly> Fill(IDataReader reader, VList<QuestionnaireActionLogFriendly> rows, int start, int pageLength)
		{
			int recordnum = 0;
			while (reader.Read() && (pageLength != 0))
			{
				if(recordnum >= start)
				{
					QuestionnaireActionLogFriendly entity = null;
					if (DataRepository.Provider.UseEntityFactory)
					{
						entity = EntityManager.CreateViewEntity<QuestionnaireActionLogFriendly>("QuestionnaireActionLogFriendly",  DataRepository.Provider.EntityCreationalFactoryType); 
					}
					else
					{
						entity = new QuestionnaireActionLogFriendly();
					}
					
					entity.SuppressEntityEvents = true;

					entity.QuestionnaireActionLogId = (System.Int32)reader[((int)QuestionnaireActionLogFriendlyColumn.QuestionnaireActionLogId)];
					//entity.QuestionnaireActionLogId = (Convert.IsDBNull(reader["QuestionnaireActionLogId"]))?(int)0:(System.Int32)reader["QuestionnaireActionLogId"];
					entity.QuestionnaireId = (System.Int32)reader[((int)QuestionnaireActionLogFriendlyColumn.QuestionnaireId)];
					//entity.QuestionnaireId = (Convert.IsDBNull(reader["QuestionnaireId"]))?(int)0:(System.Int32)reader["QuestionnaireId"];
					entity.CreatedDate = (System.DateTime)reader[((int)QuestionnaireActionLogFriendlyColumn.CreatedDate)];
					//entity.CreatedDate = (Convert.IsDBNull(reader["CreatedDate"]))?DateTime.MinValue:(System.DateTime)reader["CreatedDate"];
					entity.CreatedByPrivledgedRole = (reader.IsDBNull(((int)QuestionnaireActionLogFriendlyColumn.CreatedByPrivledgedRole)))?null:(System.String)reader[((int)QuestionnaireActionLogFriendlyColumn.CreatedByPrivledgedRole)];
					//entity.CreatedByPrivledgedRole = (Convert.IsDBNull(reader["CreatedByPrivledgedRole"]))?string.Empty:(System.String)reader["CreatedByPrivledgedRole"];
					entity.HistoryLog = (System.String)reader[((int)QuestionnaireActionLogFriendlyColumn.HistoryLog)];
					//entity.HistoryLog = (Convert.IsDBNull(reader["HistoryLog"]))?string.Empty:(System.String)reader["HistoryLog"];
					entity.ActionDescription = (System.String)reader[((int)QuestionnaireActionLogFriendlyColumn.ActionDescription)];
					//entity.ActionDescription = (Convert.IsDBNull(reader["ActionDescription"]))?string.Empty:(System.String)reader["ActionDescription"];
					entity.FullName = (System.String)reader[((int)QuestionnaireActionLogFriendlyColumn.FullName)];
					//entity.FullName = (Convert.IsDBNull(reader["FullName"]))?string.Empty:(System.String)reader["FullName"];
					entity.CompanyName = (System.String)reader[((int)QuestionnaireActionLogFriendlyColumn.CompanyName)];
					//entity.CompanyName = (Convert.IsDBNull(reader["CompanyName"]))?string.Empty:(System.String)reader["CompanyName"];
					entity.Role = (System.String)reader[((int)QuestionnaireActionLogFriendlyColumn.Role)];
					//entity.Role = (Convert.IsDBNull(reader["Role"]))?string.Empty:(System.String)reader["Role"];
					entity.AcceptChanges();
					entity.SuppressEntityEvents = false;
					
					rows.Add(entity);
					pageLength -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		
		
		/// <summary>
		/// Refreshes the <see cref="QuestionnaireActionLogFriendly"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="QuestionnaireActionLogFriendly"/> object to refresh.</param>
		protected void RefreshEntity(IDataReader reader, QuestionnaireActionLogFriendly entity)
		{
			reader.Read();
			entity.QuestionnaireActionLogId = (System.Int32)reader[((int)QuestionnaireActionLogFriendlyColumn.QuestionnaireActionLogId)];
			//entity.QuestionnaireActionLogId = (Convert.IsDBNull(reader["QuestionnaireActionLogId"]))?(int)0:(System.Int32)reader["QuestionnaireActionLogId"];
			entity.QuestionnaireId = (System.Int32)reader[((int)QuestionnaireActionLogFriendlyColumn.QuestionnaireId)];
			//entity.QuestionnaireId = (Convert.IsDBNull(reader["QuestionnaireId"]))?(int)0:(System.Int32)reader["QuestionnaireId"];
			entity.CreatedDate = (System.DateTime)reader[((int)QuestionnaireActionLogFriendlyColumn.CreatedDate)];
			//entity.CreatedDate = (Convert.IsDBNull(reader["CreatedDate"]))?DateTime.MinValue:(System.DateTime)reader["CreatedDate"];
			entity.CreatedByPrivledgedRole = (reader.IsDBNull(((int)QuestionnaireActionLogFriendlyColumn.CreatedByPrivledgedRole)))?null:(System.String)reader[((int)QuestionnaireActionLogFriendlyColumn.CreatedByPrivledgedRole)];
			//entity.CreatedByPrivledgedRole = (Convert.IsDBNull(reader["CreatedByPrivledgedRole"]))?string.Empty:(System.String)reader["CreatedByPrivledgedRole"];
			entity.HistoryLog = (System.String)reader[((int)QuestionnaireActionLogFriendlyColumn.HistoryLog)];
			//entity.HistoryLog = (Convert.IsDBNull(reader["HistoryLog"]))?string.Empty:(System.String)reader["HistoryLog"];
			entity.ActionDescription = (System.String)reader[((int)QuestionnaireActionLogFriendlyColumn.ActionDescription)];
			//entity.ActionDescription = (Convert.IsDBNull(reader["ActionDescription"]))?string.Empty:(System.String)reader["ActionDescription"];
			entity.FullName = (System.String)reader[((int)QuestionnaireActionLogFriendlyColumn.FullName)];
			//entity.FullName = (Convert.IsDBNull(reader["FullName"]))?string.Empty:(System.String)reader["FullName"];
			entity.CompanyName = (System.String)reader[((int)QuestionnaireActionLogFriendlyColumn.CompanyName)];
			//entity.CompanyName = (Convert.IsDBNull(reader["CompanyName"]))?string.Empty:(System.String)reader["CompanyName"];
			entity.Role = (System.String)reader[((int)QuestionnaireActionLogFriendlyColumn.Role)];
			//entity.Role = (Convert.IsDBNull(reader["Role"]))?string.Empty:(System.String)reader["Role"];
			reader.Close();
	
			entity.AcceptChanges();
		}
		
		/*
		/// <summary>
		/// Refreshes the <see cref="QuestionnaireActionLogFriendly"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="QuestionnaireActionLogFriendly"/> object.</param>
		protected static void RefreshEntity(DataSet dataSet, QuestionnaireActionLogFriendly entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.QuestionnaireActionLogId = (Convert.IsDBNull(dataRow["QuestionnaireActionLogId"]))?(int)0:(System.Int32)dataRow["QuestionnaireActionLogId"];
			entity.QuestionnaireId = (Convert.IsDBNull(dataRow["QuestionnaireId"]))?(int)0:(System.Int32)dataRow["QuestionnaireId"];
			entity.CreatedDate = (Convert.IsDBNull(dataRow["CreatedDate"]))?DateTime.MinValue:(System.DateTime)dataRow["CreatedDate"];
			entity.CreatedByPrivledgedRole = (Convert.IsDBNull(dataRow["CreatedByPrivledgedRole"]))?string.Empty:(System.String)dataRow["CreatedByPrivledgedRole"];
			entity.HistoryLog = (Convert.IsDBNull(dataRow["HistoryLog"]))?string.Empty:(System.String)dataRow["HistoryLog"];
			entity.ActionDescription = (Convert.IsDBNull(dataRow["ActionDescription"]))?string.Empty:(System.String)dataRow["ActionDescription"];
			entity.FullName = (Convert.IsDBNull(dataRow["FullName"]))?string.Empty:(System.String)dataRow["FullName"];
			entity.CompanyName = (Convert.IsDBNull(dataRow["CompanyName"]))?string.Empty:(System.String)dataRow["CompanyName"];
			entity.Role = (Convert.IsDBNull(dataRow["Role"]))?string.Empty:(System.String)dataRow["Role"];
			entity.AcceptChanges();
		}
		*/
			
		#endregion Helper Functions
	}//end class

	#region QuestionnaireActionLogFriendlyFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireActionLogFriendly"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireActionLogFriendlyFilterBuilder : SqlFilterBuilder<QuestionnaireActionLogFriendlyColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireActionLogFriendlyFilterBuilder class.
		/// </summary>
		public QuestionnaireActionLogFriendlyFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireActionLogFriendlyFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireActionLogFriendlyFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireActionLogFriendlyFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireActionLogFriendlyFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireActionLogFriendlyFilterBuilder

	#region QuestionnaireActionLogFriendlyParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireActionLogFriendly"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireActionLogFriendlyParameterBuilder : ParameterizedSqlFilterBuilder<QuestionnaireActionLogFriendlyColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireActionLogFriendlyParameterBuilder class.
		/// </summary>
		public QuestionnaireActionLogFriendlyParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireActionLogFriendlyParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireActionLogFriendlyParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireActionLogFriendlyParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireActionLogFriendlyParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireActionLogFriendlyParameterBuilder
	
	#region QuestionnaireActionLogFriendlySortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireActionLogFriendly"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class QuestionnaireActionLogFriendlySortBuilder : SqlSortBuilder<QuestionnaireActionLogFriendlyColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireActionLogFriendlySqlSortBuilder class.
		/// </summary>
		public QuestionnaireActionLogFriendlySortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion QuestionnaireActionLogFriendlySortBuilder

} // end namespace
