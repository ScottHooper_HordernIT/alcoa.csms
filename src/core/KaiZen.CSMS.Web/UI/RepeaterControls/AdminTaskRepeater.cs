﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.ComponentModel;
using System.Web.UI.Design.WebControls;
using System.Web.UI.Design;
using System.Web.UI.WebControls;

namespace KaiZen.CSMS.Web.UI
{
    /// <summary>
    /// A designer class for a strongly typed repeater <c>AdminTaskRepeater</c>
    /// </summary>
	public class AdminTaskRepeaterDesigner : System.Web.UI.Design.ControlDesigner
	{
	    /// <summary>
        /// Initializes a new instance of the <see cref="T:AdminTaskRepeaterDesigner"/> class.
        /// </summary>
		public AdminTaskRepeaterDesigner()
		{
		}

        /// <summary>
        /// Initializes the control designer and loads the specified component.
        /// </summary>
        /// <param name="component">The control being designed.</param>
		public override void Initialize(IComponent component)
		{
			if (!(component is AdminTaskRepeater))
			{ 
				throw new ArgumentException("Component is not a AdminTaskRepeater."); 
			} 
			base.Initialize(component); 
			base.SetViewFlags(ViewFlags.TemplateEditing, true); 
		}


		/// <summary>
		/// Generate HTML for the designer
		/// </summary>
		/// <returns>a string of design time HTML</returns>
		public override string GetDesignTimeHtml()
		{

			// Get the instance this designer applies to
			//
			AdminTaskRepeater z = (AdminTaskRepeater)Component;
			z.DataBind();

			return base.GetDesignTimeHtml();

			//return z.RenderAtDesignTime();

			//	ControlCollection c = z.Controls;
			//Totem z = (Totem) Component;
			//Totem z = (Totem) Component;
			//return ("<div style='border: 1px gray dotted; background-color: lightgray'><b>TagStat :</b> zae |  qsdds</div>");

		}
	}

    /// <summary>
    /// A strongly typed repeater control for the <see cref="AdminTaskRepeater"/> Type.
    /// </summary>
	[Designer(typeof(AdminTaskRepeaterDesigner))]
	[ParseChildren(true)]
	[ToolboxData("<{0}:AdminTaskRepeater runat=\"server\"></{0}:AdminTaskRepeater>")]
	public class AdminTaskRepeater : CompositeDataBoundControl, System.Web.UI.INamingContainer
	{
	    /// <summary>
        /// Initializes a new instance of the <see cref="T:AdminTaskRepeater"/> class.
        /// </summary>
		public AdminTaskRepeater()
		{
		}

		/// <summary>
        /// Gets a <see cref="T:System.Web.UI.ControlCollection"></see> object that represents the child controls for a specified server control in the UI hierarchy.
        /// </summary>
        /// <value></value>
        /// <returns>The collection of child controls for the specified server control.</returns>
		public override ControlCollection Controls
		{
			get
			{
				this.EnsureChildControls();
				return base.Controls;
			}
		}

		private ITemplate m_headerTemplate;
		/// <summary>
        /// Gets or sets the header template.
        /// </summary>
        /// <value>The header template.</value>
		[Browsable(false)]
		[TemplateContainer(typeof(AdminTaskItem))]
		[PersistenceMode(PersistenceMode.InnerDefaultProperty)]
		public ITemplate HeaderTemplate
		{
			get { return m_headerTemplate; }
			set { m_headerTemplate = value; }
		}

		private ITemplate m_itemTemplate;
		/// <summary>
        /// Gets or sets the item template.
        /// </summary>
        /// <value>The item template.</value>
		[Browsable(false)]
		[TemplateContainer(typeof(AdminTaskItem))]
		[PersistenceMode(PersistenceMode.InnerDefaultProperty)]
		public ITemplate ItemTemplate
		{
			get { return m_itemTemplate; }
			set { m_itemTemplate = value; }
		}

		private ITemplate m_seperatorTemplate;
        /// <summary>
        /// Gets or sets the Seperator Template
        /// </summary>
        [Browsable(false)]
        [TemplateContainer(typeof(AdminTaskItem))]
        [PersistenceMode(PersistenceMode.InnerDefaultProperty)]
        public ITemplate SeperatorTemplate
        {
            get { return m_seperatorTemplate; }
            set { m_seperatorTemplate = value; }
        }
			
		private ITemplate m_altenateItemTemplate;
        /// <summary>
        /// Gets or sets the alternating item template.
        /// </summary>
        /// <value>The alternating item template.</value>
		[Browsable(false)]
		[TemplateContainer(typeof(AdminTaskItem))]
		[PersistenceMode(PersistenceMode.InnerDefaultProperty)]
		public ITemplate AlternatingItemTemplate
		{
			get { return m_altenateItemTemplate; }
			set { m_altenateItemTemplate = value; }
		}

		private ITemplate m_footerTemplate;
        /// <summary>
        /// Gets or sets the footer template.
        /// </summary>
        /// <value>The footer template.</value>
		[Browsable(false)]
		[TemplateContainer(typeof(AdminTaskItem))]
		[PersistenceMode(PersistenceMode.InnerDefaultProperty)]
		public ITemplate FooterTemplate
		{
			get { return m_footerTemplate; }
			set { m_footerTemplate = value; }
		}

//      /// <summary>
//      /// Called by the ASP.NET page framework to notify server controls that use composition-based implementation to create any child controls they contain in preparation for posting back or rendering.
//      /// </summary>
//		protected override void CreateChildControls()
//      {
//         if (ChildControlsCreated)
//         {
//            return;
//         }

//         Controls.Clear();

//         //Instantiate the Header template (if exists)
//         if (m_headerTemplate != null)
//         {
//            Control headerItem = new Control();
//            m_headerTemplate.InstantiateIn(headerItem);
//            Controls.Add(headerItem);
//         }

//         //Instantiate the Footer template (if exists)
//         if (m_footerTemplate != null)
//         {
//            Control footerItem = new Control();
//            m_footerTemplate.InstantiateIn(footerItem);
//            Controls.Add(footerItem);
//         }
//
//         ChildControlsCreated = true;
//      }
	
		/// <summary>
        /// Overridden and Empty so that span tags are not written
        /// </summary>
        /// <param name="writer"></param>
        public override void RenderBeginTag(HtmlTextWriter writer)
        {
            
        }

        /// <summary>
        /// Overridden and Empty so that span tags are not written
        /// </summary>
        /// <param name="writer"></param>
        public override void RenderEndTag(HtmlTextWriter writer)
        {
                
        }		
		
		/// <summary>
      	/// Called by the ASP.NET page framework to notify server controls that use composition-based implementation to create any child controls they contain in preparation for posting back or rendering.
      	/// </summary>
		protected override int CreateChildControls(System.Collections.IEnumerable dataSource, bool dataBinding)
      	{
         int pos = 0;

         if (dataBinding)
         {
            //Instantiate the Header template (if exists)
            if (m_headerTemplate != null)
            {
                Control headerItem = new Control();
                m_headerTemplate.InstantiateIn(headerItem);
                Controls.Add(headerItem);
            }
			if (dataSource != null)
			{
				foreach (object o in dataSource)
				{
						KaiZen.CSMS.Entities.AdminTask entity = o as KaiZen.CSMS.Entities.AdminTask;
						AdminTaskItem container = new AdminTaskItem(entity);
	
						if (m_itemTemplate != null && (pos % 2) == 0)
						{
							m_itemTemplate.InstantiateIn(container);
							
							if (m_seperatorTemplate != null)
							{
								m_seperatorTemplate.InstantiateIn(container);
							}
						}
						else
						{
							if (m_altenateItemTemplate != null)
							{
								m_altenateItemTemplate.InstantiateIn(container);
								
								if (m_seperatorTemplate != null)
								{
									m_seperatorTemplate.InstantiateIn(container);
								}
								
							}
							else if (m_itemTemplate != null)
							{
								m_itemTemplate.InstantiateIn(container);
								
								if (m_seperatorTemplate != null)
								{
									m_seperatorTemplate.InstantiateIn(container);
								}
							}
							else
							{
								// no template !!!
							}
						}
						Controls.Add(container);
						
						container.DataBind();
						
						pos++;
				}
			}
            //Instantiate the Footer template (if exists)
            if (m_footerTemplate != null)
            {
                Control footerItem = new Control();
                m_footerTemplate.InstantiateIn(footerItem);
                Controls.Add(footerItem);
            }

		}
			
			return pos;
		}

        /// <summary>
        /// Raises the <see cref="E:System.Web.UI.Control.PreRender"></see> event.
        /// </summary>
        /// <param name="e">An <see cref="T:System.EventArgs"></see> object that contains the event data.</param>
		protected override void OnPreRender(EventArgs e)
		{
			base.DataBind();
		}

		#region Design time
        /// <summary>
        /// Renders at design time.
        /// </summary>
        /// <returns>a  string of the Designed HTML</returns>
		internal string RenderAtDesignTime()
		{			
			return "Designer currently not implemented"; 
		}

		#endregion
	}

    /// <summary>
    /// A wrapper type for the entity
    /// </summary>
	[System.ComponentModel.ToolboxItem(false)]
	public class AdminTaskItem : System.Web.UI.Control, System.Web.UI.INamingContainer
	{
		private KaiZen.CSMS.Entities.AdminTask _entity;

        /// <summary>
        /// Initializes a new instance of the <see cref="T:AdminTaskItem"/> class.
        /// </summary>
		public AdminTaskItem()
			: base()
		{ }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:AdminTaskItem"/> class.
        /// </summary>
		public AdminTaskItem(KaiZen.CSMS.Entities.AdminTask entity)
			: base()
		{
			_entity = entity;
		}
		
        /// <summary>
        /// Gets the AdminTaskId
        /// </summary>
        /// <value>The AdminTaskId.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32 AdminTaskId
		{
			get { return _entity.AdminTaskId; }
		}
        /// <summary>
        /// Gets the AdminTaskSourceId
        /// </summary>
        /// <value>The AdminTaskSourceId.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32 AdminTaskSourceId
		{
			get { return _entity.AdminTaskSourceId; }
		}
        /// <summary>
        /// Gets the AdminTaskTypeId
        /// </summary>
        /// <value>The AdminTaskTypeId.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32 AdminTaskTypeId
		{
			get { return _entity.AdminTaskTypeId; }
		}
        /// <summary>
        /// Gets the AdminTaskStatusId
        /// </summary>
        /// <value>The AdminTaskStatusId.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32 AdminTaskStatusId
		{
			get { return _entity.AdminTaskStatusId; }
		}
        /// <summary>
        /// Gets the AdminTaskComments
        /// </summary>
        /// <value>The AdminTaskComments.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String AdminTaskComments
		{
			get { return _entity.AdminTaskComments; }
		}
        /// <summary>
        /// Gets the DateOpened
        /// </summary>
        /// <value>The DateOpened.</value>
		[System.ComponentModel.Bindable(true)]
		public System.DateTime DateOpened
		{
			get { return _entity.DateOpened; }
		}
        /// <summary>
        /// Gets the OpenedByUserId
        /// </summary>
        /// <value>The OpenedByUserId.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32 OpenedByUserId
		{
			get { return _entity.OpenedByUserId; }
		}
        /// <summary>
        /// Gets the DateClosed
        /// </summary>
        /// <value>The DateClosed.</value>
		[System.ComponentModel.Bindable(true)]
		public System.DateTime? DateClosed
		{
			get { return _entity.DateClosed; }
		}
        /// <summary>
        /// Gets the ClosedByUserId
        /// </summary>
        /// <value>The ClosedByUserId.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? ClosedByUserId
		{
			get { return _entity.ClosedByUserId; }
		}
        /// <summary>
        /// Gets the CsmsAccessId
        /// </summary>
        /// <value>The CsmsAccessId.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32 CsmsAccessId
		{
			get { return _entity.CsmsAccessId; }
		}
        /// <summary>
        /// Gets the CompanyId
        /// </summary>
        /// <value>The CompanyId.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32 CompanyId
		{
			get { return _entity.CompanyId; }
		}
        /// <summary>
        /// Gets the Login
        /// </summary>
        /// <value>The Login.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Login
		{
			get { return _entity.Login; }
		}
        /// <summary>
        /// Gets the EmailAddress
        /// </summary>
        /// <value>The EmailAddress.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String EmailAddress
		{
			get { return _entity.EmailAddress; }
		}
        /// <summary>
        /// Gets the FirstName
        /// </summary>
        /// <value>The FirstName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String FirstName
		{
			get { return _entity.FirstName; }
		}
        /// <summary>
        /// Gets the LastName
        /// </summary>
        /// <value>The LastName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String LastName
		{
			get { return _entity.LastName; }
		}
        /// <summary>
        /// Gets the JobRole
        /// </summary>
        /// <value>The JobRole.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String JobRole
		{
			get { return _entity.JobRole; }
		}
        /// <summary>
        /// Gets the JobTitle
        /// </summary>
        /// <value>The JobTitle.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String JobTitle
		{
			get { return _entity.JobTitle; }
		}
        /// <summary>
        /// Gets the MobileNo
        /// </summary>
        /// <value>The MobileNo.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String MobileNo
		{
			get { return _entity.MobileNo; }
		}
        /// <summary>
        /// Gets the TelephoneNo
        /// </summary>
        /// <value>The TelephoneNo.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String TelephoneNo
		{
			get { return _entity.TelephoneNo; }
		}
        /// <summary>
        /// Gets the FaxNo
        /// </summary>
        /// <value>The FaxNo.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String FaxNo
		{
			get { return _entity.FaxNo; }
		}

        /// <summary>
        /// Gets a <see cref="T:KaiZen.CSMS.Entities.AdminTask"></see> object
        /// </summary>
        /// <value></value>
        [System.ComponentModel.Bindable(true)]
        public KaiZen.CSMS.Entities.AdminTask Entity
        {
            get { return _entity; }
        }
	}
}
