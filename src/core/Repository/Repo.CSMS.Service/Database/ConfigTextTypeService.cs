﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface IConfigTextTypeService
    {
        ConfigTextType Get(Expression<Func<ConfigTextType, bool>> where, List<Expression<Func<ConfigTextType, object>>> includes);
        List<ConfigTextType> GetMany(int? top, Expression<Func<ConfigTextType, bool>> where, Func<IQueryable<ConfigTextType>, IOrderedQueryable<ConfigTextType>> orderBy, List<Expression<Func<ConfigTextType, object>>> includes);
        List<ConfigTextType> GetManyPaged(int pageNumber, int pageSize, Expression<Func<ConfigTextType, bool>> where, Func<IQueryable<ConfigTextType>, IOrderedQueryable<ConfigTextType>> orderBy, List<Expression<Func<ConfigTextType, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<ConfigTextType, bool>> where, List<Expression<Func<ConfigTextType, object>>> includes);
        void Insert(ConfigTextType item, bool saveChanges = true);
        void Update(ConfigTextType item, bool saveChanges = true);
        void Delete(ConfigTextType item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class ConfigTextTypeService : IConfigTextTypeService
    {
        private readonly IRepository<ConfigTextType> _repository;

        public ConfigTextTypeService(IRepository<ConfigTextType> repository)
        {
            this._repository = repository;
        }

        public ConfigTextType Get(Expression<Func<ConfigTextType, bool>> where, List<Expression<Func<ConfigTextType, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<ConfigTextType> GetMany(int? top, Expression<Func<ConfigTextType, bool>> where, Func<IQueryable<ConfigTextType>, IOrderedQueryable<ConfigTextType>> orderBy, List<Expression<Func<ConfigTextType, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<ConfigTextType> GetManyPaged(int pageNumber, int pageSize, Expression<Func<ConfigTextType, bool>> where, Func<IQueryable<ConfigTextType>, IOrderedQueryable<ConfigTextType>> orderBy, List<Expression<Func<ConfigTextType, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<ConfigTextType, bool>> where, List<Expression<Func<ConfigTextType, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(ConfigTextType item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("ConfigTextType");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(ConfigTextType item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("ConfigTextType");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(ConfigTextType item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("ConfigTextType");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
