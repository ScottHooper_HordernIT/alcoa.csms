﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.DocumentsProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(DocumentsDataSourceDesigner))]
	public class DocumentsDataSource : ProviderDataSource<Documents, DocumentsKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the DocumentsDataSource class.
		/// </summary>
		public DocumentsDataSource() : base(new DocumentsService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the DocumentsDataSourceView used by the DocumentsDataSource.
		/// </summary>
		protected DocumentsDataSourceView DocumentsView
		{
			get { return ( View as DocumentsDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DocumentsDataSource control invokes to retrieve data.
		/// </summary>
		public DocumentsSelectMethod SelectMethod
		{
			get
			{
				DocumentsSelectMethod selectMethod = DocumentsSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (DocumentsSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the DocumentsDataSourceView class that is to be
		/// used by the DocumentsDataSource.
		/// </summary>
		/// <returns>An instance of the DocumentsDataSourceView class.</returns>
		protected override BaseDataSourceView<Documents, DocumentsKey> GetNewDataSourceView()
		{
			return new DocumentsDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the DocumentsDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class DocumentsDataSourceView : ProviderDataSourceView<Documents, DocumentsKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the DocumentsDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the DocumentsDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public DocumentsDataSourceView(DocumentsDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal DocumentsDataSource DocumentsOwner
		{
			get { return Owner as DocumentsDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal DocumentsSelectMethod SelectMethod
		{
			get { return DocumentsOwner.SelectMethod; }
			set { DocumentsOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal DocumentsService DocumentsProvider
		{
			get { return Provider as DocumentsService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<Documents> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<Documents> results = null;
			Documents item;
			count = 0;
			
			System.Int32 _documentId;
			System.String _documentFileName;
			System.String _documentType;
			System.Int32? _regionId_nullable;

			switch ( SelectMethod )
			{
				case DocumentsSelectMethod.Get:
					DocumentsKey entityKey  = new DocumentsKey();
					entityKey.Load(values);
					item = DocumentsProvider.Get(entityKey);
					results = new TList<Documents>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case DocumentsSelectMethod.GetAll:
                    results = DocumentsProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case DocumentsSelectMethod.GetPaged:
					results = DocumentsProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case DocumentsSelectMethod.Find:
					if ( FilterParameters != null )
						results = DocumentsProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = DocumentsProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case DocumentsSelectMethod.GetByDocumentId:
					_documentId = ( values["DocumentId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["DocumentId"], typeof(System.Int32)) : (int)0;
					item = DocumentsProvider.GetByDocumentId(_documentId);
					results = new TList<Documents>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				case DocumentsSelectMethod.GetByDocumentFileName:
					_documentFileName = ( values["DocumentFileName"] != null ) ? (System.String) EntityUtil.ChangeType(values["DocumentFileName"], typeof(System.String)) : string.Empty;
					results = DocumentsProvider.GetByDocumentFileName(_documentFileName, this.StartIndex, this.PageSize, out count);
					break;
				case DocumentsSelectMethod.GetByDocumentType:
					_documentType = ( values["DocumentType"] != null ) ? (System.String) EntityUtil.ChangeType(values["DocumentType"], typeof(System.String)) : string.Empty;
					results = DocumentsProvider.GetByDocumentType(_documentType, this.StartIndex, this.PageSize, out count);
					break;
				// FK
				case DocumentsSelectMethod.GetByRegionId:
					_regionId_nullable = (System.Int32?) EntityUtil.ChangeType(values["RegionId"], typeof(System.Int32?));
					results = DocumentsProvider.GetByRegionId(_regionId_nullable, this.StartIndex, this.PageSize, out count);
					break;
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == DocumentsSelectMethod.Get || SelectMethod == DocumentsSelectMethod.GetByDocumentId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				Documents entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					DocumentsProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<Documents> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			DocumentsProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region DocumentsDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the DocumentsDataSource class.
	/// </summary>
	public class DocumentsDataSourceDesigner : ProviderDataSourceDesigner<Documents, DocumentsKey>
	{
		/// <summary>
		/// Initializes a new instance of the DocumentsDataSourceDesigner class.
		/// </summary>
		public DocumentsDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public DocumentsSelectMethod SelectMethod
		{
			get { return ((DocumentsDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new DocumentsDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region DocumentsDataSourceActionList

	/// <summary>
	/// Supports the DocumentsDataSourceDesigner class.
	/// </summary>
	internal class DocumentsDataSourceActionList : DesignerActionList
	{
		private DocumentsDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the DocumentsDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public DocumentsDataSourceActionList(DocumentsDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public DocumentsSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion DocumentsDataSourceActionList
	
	#endregion DocumentsDataSourceDesigner
	
	#region DocumentsSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the DocumentsDataSource.SelectMethod property.
	/// </summary>
	public enum DocumentsSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByDocumentId method.
		/// </summary>
		GetByDocumentId,
		/// <summary>
		/// Represents the GetByDocumentFileName method.
		/// </summary>
		GetByDocumentFileName,
		/// <summary>
		/// Represents the GetByDocumentType method.
		/// </summary>
		GetByDocumentType,
		/// <summary>
		/// Represents the GetByRegionId method.
		/// </summary>
		GetByRegionId
	}
	
	#endregion DocumentsSelectMethod

	#region DocumentsFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Documents"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class DocumentsFilter : SqlFilter<DocumentsColumn>
	{
	}
	
	#endregion DocumentsFilter

	#region DocumentsExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Documents"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class DocumentsExpressionBuilder : SqlExpressionBuilder<DocumentsColumn>
	{
	}
	
	#endregion DocumentsExpressionBuilder	

	#region DocumentsProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;DocumentsChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="Documents"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class DocumentsProperty : ChildEntityProperty<DocumentsChildEntityTypes>
	{
	}
	
	#endregion DocumentsProperty
}

