﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.QuestionnaireEscalationProcurementListProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[CLSCompliant(true)]
	[Designer(typeof(QuestionnaireEscalationProcurementListDataSourceDesigner))]
	public class QuestionnaireEscalationProcurementListDataSource : ReadOnlyDataSource<QuestionnaireEscalationProcurementList>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireEscalationProcurementListDataSource class.
		/// </summary>
		public QuestionnaireEscalationProcurementListDataSource() : base(new QuestionnaireEscalationProcurementListService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the QuestionnaireEscalationProcurementListDataSourceView used by the QuestionnaireEscalationProcurementListDataSource.
		/// </summary>
		protected QuestionnaireEscalationProcurementListDataSourceView QuestionnaireEscalationProcurementListView
		{
			get { return ( View as QuestionnaireEscalationProcurementListDataSourceView ); }
		}
		
		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the QuestionnaireEscalationProcurementListDataSourceView class that is to be
		/// used by the QuestionnaireEscalationProcurementListDataSource.
		/// </summary>
		/// <returns>An instance of the QuestionnaireEscalationProcurementListDataSourceView class.</returns>
		protected override BaseDataSourceView<QuestionnaireEscalationProcurementList, Object> GetNewDataSourceView()
		{
			return new QuestionnaireEscalationProcurementListDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the QuestionnaireEscalationProcurementListDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class QuestionnaireEscalationProcurementListDataSourceView : ReadOnlyDataSourceView<QuestionnaireEscalationProcurementList>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireEscalationProcurementListDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the QuestionnaireEscalationProcurementListDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public QuestionnaireEscalationProcurementListDataSourceView(QuestionnaireEscalationProcurementListDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal QuestionnaireEscalationProcurementListDataSource QuestionnaireEscalationProcurementListOwner
		{
			get { return Owner as QuestionnaireEscalationProcurementListDataSource; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal QuestionnaireEscalationProcurementListService QuestionnaireEscalationProcurementListProvider
		{
			get { return Provider as QuestionnaireEscalationProcurementListService; }
		}

		#endregion Properties
		
		#region Methods
		
		#endregion Methods
	}

	#region QuestionnaireEscalationProcurementListDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the QuestionnaireEscalationProcurementListDataSource class.
	/// </summary>
	public class QuestionnaireEscalationProcurementListDataSourceDesigner : ReadOnlyDataSourceDesigner<QuestionnaireEscalationProcurementList>
	{
	}

	#endregion QuestionnaireEscalationProcurementListDataSourceDesigner

	#region QuestionnaireEscalationProcurementListFilter

	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireEscalationProcurementList"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireEscalationProcurementListFilter : SqlFilter<QuestionnaireEscalationProcurementListColumn>
	{
	}

	#endregion QuestionnaireEscalationProcurementListFilter

	#region QuestionnaireEscalationProcurementListExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireEscalationProcurementList"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireEscalationProcurementListExpressionBuilder : SqlExpressionBuilder<QuestionnaireEscalationProcurementListColumn>
	{
	}
	
	#endregion QuestionnaireEscalationProcurementListExpressionBuilder		
}

