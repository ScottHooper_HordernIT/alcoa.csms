using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using LumenWorks.Framework.IO.Csv;
using System.IO;
using System.Data.SqlClient;
using System.Collections;
using System.Text.RegularExpressions;

namespace ALCOA.CSMS.Scripts
{
    public partial class Form1 : Form
    {

       string connectString = @"Data Source=(LOCAL)\SQLEXPRESS;Initial Catalog=ALCOA_WAO_CSMWP_dev;Persist Security Info=True;User ID=WAO_CSMWP;Password=secure";

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            textBox1.Text = System.IO.Directory.GetCurrentDirectory() + "\\companies.csv";
            textBox2.Text = System.IO.Directory.GetCurrentDirectory() + "\\users.csv";
            textBox3.Text = System.IO.Directory.GetCurrentDirectory() + "\\residential.csv";
        }
        public string openFileName()
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "csv files (*.csv)|*.csv";
            dialog.InitialDirectory = System.IO.Directory.GetCurrentDirectory();
            dialog.Title = "Select a csv file";
            return (dialog.ShowDialog() == DialogResult.OK)
               ? dialog.FileName : null;
        } //returns user selected *.csv file

        public string openFileNameRoboCopyLogs()
        {
            FolderBrowserDialog dialog = new FolderBrowserDialog();
            dialog.RootFolder = Environment.SpecialFolder.MyComputer;
            dialog.ShowNewFolderButton = false;
            return (dialog.ShowDialog() == DialogResult.OK)
                ? dialog.SelectedPath : null;
            //OpenFileDialog dialog = new OpenFileDialog();
            //dialog.Filter = "log files (*.*)|*.*";
            //dialog.InitialDirectory = System.IO.Directory.GetCurrentDirectory();
            //dialog.Title = "Select a RoboCopy log file";
            //return (dialog.ShowDialog() == DialogResult.OK)
            //   ? dialog.FileName : null;
        }
        public string GetcompanyId(string companyName)
        {
            SqlConnection con = new SqlConnection(connectString);
            con.Open();
            string sql = "SELECT dbo.Companies.CompanyID FROM dbo.Companies WHERE dbo.Companies.CompanyName LIKE '" + companyName + "'";
            SqlCommand cmd = new SqlCommand(sql, con);
            SqlDataReader reader = cmd.ExecuteReader();
            string companyId = "";
            while (reader.Read())
            {
                companyId = reader[0].ToString();
            }
            reader.Close();
            con.Close();
            return companyId;
        }

        public string GetsiteId(string siteName)
        {
            SqlConnection con = new SqlConnection(connectString);
            con.Open();
            string sql = "SELECT dbo.Sites.SiteID FROM dbo.Sites WHERE dbo.Sites.SiteName LIKE '" + siteName + "'";
            SqlCommand cmd = new SqlCommand(sql, con);
            SqlDataReader reader = cmd.ExecuteReader();
            string siteId = "";
            while (reader.Read())
            {
                siteId = reader[0].ToString();
            }
            reader.Close();
            con.Close();
            return siteId;
        }

        public string GetuserId(string userId)
        {
            SqlConnection con = new SqlConnection(connectString);
            con.Open();
            string sql = "SELECT dbo.Users.UserID FROM dbo.Users WHERE dbo.Users.UserLogon LIKE '" + userId + "'";
            SqlCommand cmd = new SqlCommand(sql, con);
            SqlDataReader reader = cmd.ExecuteReader();
            string siteId = "";
            while (reader.Read())
            {
                siteId = reader[0].ToString();
            }
            reader.Close();
            con.Close();
            return siteId;
        }

        public string userExistAlready(string userLogon)
        {
            SqlConnection con = new SqlConnection(connectString);
            con.Open();
            string sql = "SELECT COUNT(dbo.Users.UserLogon) FROM dbo.Users WHERE dbo.Users.UserLogon LIKE '" + userLogon + "'";
            SqlCommand cmd = new SqlCommand(sql, con);
            SqlDataReader reader = cmd.ExecuteReader();
            string companyId = "";
            while (reader.Read())
            {
                companyId = reader[0].ToString();
            }
            reader.Close();
            con.Close();
            return companyId;
        }
        #region Companies
        private void button1_Click(object sender, EventArgs e) { textBox1.Text = openFileName(); } //Select CSV File
        private void button2_Click(object sender, EventArgs e) { ReadCSVFile(); }
        private void button3_Click(object sender, EventArgs e) { WriteSQLScript(); }
        private void button4_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();

            saveFileDialog1.Filter = "sql script files (*.sql)|*.sql|All files (*.*)|*.*";
            //saveFileDialog1.FilterIndex = 2;
            saveFileDialog1.RestoreDirectory = true;

            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                System.IO.TextWriter tw = new StreamWriter(saveFileDialog1.OpenFile());
                tw.Write(richTextBox1.Text);
                tw.Close();
            }
        } //Save Generated SQL Script to File

        public void ReadCSVFile()
        {
            string csvFilePath = textBox1.Text;

            using (CsvReader csv =
                   new CsvReader(new StreamReader(csvFilePath), true))
            {
                int fieldCount = csv.FieldCount;
                string[] headers = csv.GetFieldHeaders();

                while (csv.ReadNextRecord())
                {
                    ListViewItem itemCompany = new ListViewItem(csv["CompanyName"], 0);
                    itemCompany.SubItems.Add(csv["CompanyABN"]);
                    itemCompany.SubItems.Add(csv["PhoneNo"]);
                    itemCompany.SubItems.Add(csv["FaxNo"]);
                    itemCompany.SubItems.Add(csv["MobNo"]);
                    itemCompany.SubItems.Add(csv["AddressBusiness"]);
                    itemCompany.SubItems.Add(csv["AddressPostal"]);
                    itemCompany.SubItems.Add(csv["SafetyQualified"]);
                    listView1.Items.Add(itemCompany);
                }

                //while (csv.ReadNextRecord())
                //{
                //    for (int i = 0; i < fieldCount; i++)
                //        Console.Write(string.Format("{0} = {1};",
                //                      headers[i], csv[i]));

                //    Console.WriteLine();
                //    //richTextBox1.Text = richTextBox1.Text + string.Format("{0} = {1};", headers[i], csv[i]) + "\n";
                //}
            }
        }
        public void WriteSQLScript()
        {
            string csvFilePath = textBox1.Text;

            using (CsvReader csv =
                   new CsvReader(new StreamReader(csvFilePath), true))
            {
                int fieldCount = csv.FieldCount;
                string[] headers = csv.GetFieldHeaders();

                while (csv.ReadNextRecord())
                {
                    string sqlcmd = "";
                    sqlcmd = "INSERT INTO [ALCOA_WAO_CSMWP_dev].[dbo].[Companies] ";
                    sqlcmd = sqlcmd + "([CompanyName],[CompanyABN],[PhoneNo],[FaxNo],[MobNo],[AddressBusiness],";
                    sqlcmd = sqlcmd + "[AddressPostal],[SafetyQualified], [ModifiedByUserId]) ";
                    sqlcmd = sqlcmd + "VALUES (";
                    sqlcmd = sqlcmd + "'" + csv["CompanyName"] + "',";
                    if (!String.IsNullOrEmpty(csv["CompanyABN"]))
                    {
                        sqlcmd = sqlcmd + "'" + csv["CompanyABN"] + "',";
                    }
                    else
                    {
                        sqlcmd = sqlcmd + "'-',"; //CompanyABN can not be NULL...
                    }
                    if (!String.IsNullOrEmpty(csv["PhoneNo"]))
                    {
                        sqlcmd = sqlcmd + "'" + csv["PhoneNo"] + "',";
                    }
                    else
                    {
                        sqlcmd = sqlcmd + "Null,";
                    }
                    if (!String.IsNullOrEmpty(csv["FaxNo"]))
                    {
                        sqlcmd = sqlcmd + "'" + csv["FaxNo"] + "',";
                    }
                    else
                    {
                        sqlcmd = sqlcmd + "Null,";
                    }
                    if (!String.IsNullOrEmpty(csv["MobNo"]))
                    {
                        sqlcmd = sqlcmd + "'" + csv["MobNo"] + "',";
                    }
                    else
                    {
                        sqlcmd = sqlcmd + "Null,";
                    }
                    if (!String.IsNullOrEmpty(csv["AddressBusiness"]))
                    {
                        sqlcmd = sqlcmd + "'" + csv["AddressBusiness"] + "',";
                    }
                    else
                    {
                        sqlcmd = sqlcmd + "Null,";
                    }
                    if (!String.IsNullOrEmpty(csv["AddressPostal"]))
                    {
                        sqlcmd = sqlcmd + "'" + csv["AddressPostal"] + "',";
                    }
                    else
                    {
                        sqlcmd = sqlcmd + "Null,";
                    }

                    if (csv["SafetyQualified"] == "Yes")
                    {
                        sqlcmd = sqlcmd + "1";
                    }
                    else
                    {
                        sqlcmd = sqlcmd + "0";
                    }

                    sqlcmd = sqlcmd + ",0)";
                    //sqlcmd = sqlcmd + "<'" + csv["CompanyName"] + "';, varchar(200),>";
                    //sqlcmd = sqlcmd + ",<'" + csv["CompanyABN"] + "', varchar(50),>";
                    //sqlcmd = sqlcmd + ",<'" + csv["PhoneNo"] + "', varchar(20),>";
                    //sqlcmd = sqlcmd + ",<'" + csv["MobNo"] + "', varchar(20),>";
                    //sqlcmd = sqlcmd + ",<'" + csv["AddressBusiness"] + "', varchar(200),>";
                    //sqlcmd = sqlcmd + ",<'" + csv["AddressPostal"] + "', varchar(200),>";

                    //if (csv["SafetyQualified"] == "Yes")
                    //{
                    //    sqlcmd = sqlcmd + ",<'True', bit,>)";
                    //}
                    //else
                    //{
                    //    sqlcmd = sqlcmd + ",<'False', bit,>)";
                    //}




                    richTextBox1.Text = richTextBox1.Text + "\n " + sqlcmd;

                }
            }
        }

        #endregion

        #region users
        private void button8_Click(object sender, EventArgs e) { textBox2.Text = openFileName(); } //Select CSV File

        private void button7_Click(object sender, EventArgs e)
        {
            string csvFilePath = textBox2.Text;

            using (CsvReader csv =
                   new CsvReader(new StreamReader(csvFilePath), true))
            {
                int fieldCount = csv.FieldCount;
                string[] headers = csv.GetFieldHeaders();

                while (csv.ReadNextRecord())
                {
                    //Company,Alcoa Logon ?,Date Sent to AD ?,Position,First Name,Last Name,E-Mail Address,Title,Telephone Number,Mobile Number,Alcoa Location(s) 
                    if (!String.IsNullOrEmpty(csv["E-Mail Address"]))
                    {
                        if (csv["Alcoa Logon ?"] == "Yes")
                        {
                            ListViewItem itemUser = new ListViewItem("AUSTRALIA_ASIA\\" + csv["UserLogon"], 0); //UserLogon
                            itemUser.SubItems.Add(csv["Last Name"]); //LastName
                            itemUser.SubItems.Add(csv["First Name"]); //FirstName
                            itemUser.SubItems.Add(csv["Company"]);
                            itemUser.SubItems.Add(GetcompanyId(csv["Company"])); //CompanyNameID
                            itemUser.SubItems.Add(csv["Title"]); //Title
                            itemUser.SubItems.Add(csv["Telephone Number"]); //PhoneNo
                            itemUser.SubItems.Add("[TBD]"); //FaxNo
                            itemUser.SubItems.Add(csv["Mobile Number"]); //MobNo
                            itemUser.SubItems.Add(csv["E-Mail Address"]);//Email
                            itemUser.SubItems.Add("2"); //RoleID - Assume Contractor by Default

                            if (IsInCollection2(itemUser) == false)
                            {
                                listView2.Items.Add(itemUser);
                            }
                        }
                        else
                        {
                            ListViewItem itemUser = new ListViewItem(csv["E-Mail Address"], 0); //UserLogon
                            itemUser.SubItems.Add(csv["Last Name"]); //LastName
                            itemUser.SubItems.Add(csv["First Name"]); //FirstName
                            itemUser.SubItems.Add(csv["Company"]); //CompanyNameID
                            itemUser.SubItems.Add(GetcompanyId(csv["Company"]));
                            itemUser.SubItems.Add(csv["Title"]); //Title
                            itemUser.SubItems.Add(csv["Telephone Number"]); //PhoneNo
                            itemUser.SubItems.Add("[TBD]"); //FaxNo
                            itemUser.SubItems.Add(csv["Mobile Number"]); //MobNo
                            itemUser.SubItems.Add(csv["E-Mail Address"]);//Email
                            itemUser.SubItems.Add("2"); //RoleID - Assume Contractor by Default

                            if (IsInCollection3(itemUser) == false)
                            {
                                listView3.Items.Add(itemUser);
                            }
                        }

                        //Company Contacts List
                        ListViewItem itemContact = new ListViewItem(csv["Company"], 0); //CompanyId
                        itemContact.SubItems.Add(GetcompanyId(csv["Company"]));
                        itemContact.SubItems.Add(csv["First Name"]); //ContactFirstName
                        itemContact.SubItems.Add(csv["Last Name"]); //ContactLastName
                        itemContact.SubItems.Add(csv["Position"]); //ContactRole
                        itemContact.SubItems.Add(csv["Title"]); //ContactTitle
                        itemContact.SubItems.Add(csv["E-Mail Address"]); //ContactEmail
                        itemContact.SubItems.Add(csv["Mobile Number"]); //ContactMobile
                        itemContact.SubItems.Add(csv["Telephone Number"]); //ContactPhone
                        itemContact.SubItems.Add("[TBD]");//SiteId
                        listView4.Items.Add(itemContact);
                    }
                }
            }
        } //Read CSV File
        private void button6_Click(object sender, EventArgs e)
        {
            //alcoadirect
            #region AlcoaDirect
            string csvFilePath = textBox2.Text;

            using (CsvReader csv =
                   new CsvReader(new StreamReader(csvFilePath), true))
            {
                int fieldCount = csv.FieldCount;
                string[] headers = csv.GetFieldHeaders();

                while (csv.ReadNextRecord())
                {
                    if (!String.IsNullOrEmpty(csv["E-Mail Address"]))
                    {
                        #region AlcoaDirect
                        if (csv["Alcoa Logon ?"] != "Yes")
                        {
                            string sqlcmd = "";
                            sqlcmd = "INSERT INTO [ALCOA_WAO_CSMWP_dev].[dbo].[Users] ";
                            sqlcmd = sqlcmd + "([UserLogon],[LastName],[FirstName],[CompanyNameID],[Title],";
                            sqlcmd = sqlcmd + "[PhoneNo],[FaxNo], [MobNo], [Email], [RoleID], [ModifiedByUserId]) ";
                            sqlcmd = sqlcmd + "VALUES (";
                            sqlcmd = sqlcmd + "'" + csv["E-Mail Address"] + "',";
                            if (!String.IsNullOrEmpty(csv["Last Name"]))
                            {
                                sqlcmd = sqlcmd + "'" + csv["Last Name"] + "',";
                            }
                            else
                            {
                                sqlcmd = sqlcmd + "Null,";
                            }
                            if (!String.IsNullOrEmpty(csv["First Name"]))
                            {
                                sqlcmd = sqlcmd + "'" + csv["First Name"] + "',";
                            }
                            else
                            {
                                sqlcmd = sqlcmd + "Null,";
                            }

                            string companyId = GetcompanyId(csv["Company"]);
                            if (string.IsNullOrEmpty(companyId))
                            {
                                sqlcmd = sqlcmd + "Null,";
                            }
                            else
                            {
                                sqlcmd = sqlcmd + companyId + ",";
                            }
                            if (!String.IsNullOrEmpty(csv["Title"]))
                            {
                                sqlcmd = sqlcmd + "'" + csv["Title"] + "',";
                            }
                            else
                            {
                                sqlcmd = sqlcmd + "Null,";
                            }
                            if (!String.IsNullOrEmpty(csv["Telephone Number"]))
                            {
                                sqlcmd = sqlcmd + "'" + csv["Telephone Number"] + "',";
                            }
                            else
                            {
                                sqlcmd = sqlcmd + "Null,";
                            }
                            sqlcmd = sqlcmd + "Null,"; //faxno
                            if (!String.IsNullOrEmpty(csv["Mobile Number"]))
                            {
                                sqlcmd = sqlcmd + "'" + csv["Mobile Number"] + "',";
                            }
                            else
                            {
                                sqlcmd = sqlcmd + "Null,";
                            }
                            if (!String.IsNullOrEmpty(csv["E-Mail Address"]))
                            {
                                sqlcmd = sqlcmd + "'" + csv["E-Mail Address"] + "',";
                            }
                            else
                            {
                                sqlcmd = sqlcmd + "Null,";
                            }

                            //RoleId
                            sqlcmd = sqlcmd + "2";
                            sqlcmd = sqlcmd + ",0)";
                            richTextBox2.Text = richTextBox2.Text + "\n " + sqlcmd;
                        }
                        #endregion
                        #region Alcoan
                        else
                        {
                            if (!String.IsNullOrEmpty(csv["UserLogon"]))
                            {
                                string sqlcmd = "";
                                sqlcmd = "INSERT INTO [ALCOA_WAO_CSMWP_dev].[dbo].[Users] ";
                                sqlcmd = sqlcmd + "([UserLogon],[LastName],[FirstName],[CompanyNameID],[Title],";
                                sqlcmd = sqlcmd + "[PhoneNo],[FaxNo], [MobNo], [Email], [RoleID], [ModifiedByUserId]) ";
                                sqlcmd = sqlcmd + "VALUES (";
                                sqlcmd = sqlcmd + "'AUSTRALIA_ASIA\\" + csv["UserLogon"] + "',";
                                if (!String.IsNullOrEmpty(csv["Last Name"]))
                                {
                                    sqlcmd = sqlcmd + "'" + csv["Last Name"] + "',";
                                }
                                else
                                {
                                    sqlcmd = sqlcmd + "Null,";
                                }
                                if (!String.IsNullOrEmpty(csv["First Name"]))
                                {
                                    sqlcmd = sqlcmd + "'" + csv["First Name"] + "',";
                                }
                                else
                                {
                                    sqlcmd = sqlcmd + "Null,";
                                }

                                string companyId = GetcompanyId(csv["Company"]);
                                if (string.IsNullOrEmpty(companyId))
                                {
                                    sqlcmd = sqlcmd + "Null,";
                                }
                                else
                                {
                                    sqlcmd = sqlcmd + companyId + ",";
                                }
                                if (!String.IsNullOrEmpty(csv["Title"]))
                                {
                                    sqlcmd = sqlcmd + "'" + csv["Title"] + "',";
                                }
                                else
                                {
                                    sqlcmd = sqlcmd + "Null,";
                                }
                                if (!String.IsNullOrEmpty(csv["Telephone Number"]))
                                {
                                    sqlcmd = sqlcmd + "'" + csv["Telephone Number"] + "',";
                                }
                                else
                                {
                                    sqlcmd = sqlcmd + "Null,";
                                }
                                sqlcmd = sqlcmd + "Null,"; //faxno
                                if (!String.IsNullOrEmpty(csv["Mobile Number"]))
                                {
                                    sqlcmd = sqlcmd + "'" + csv["Mobile Number"] + "',";
                                }
                                else
                                {
                                    sqlcmd = sqlcmd + "Null,";
                                }
                                if (!String.IsNullOrEmpty(csv["E-Mail Address"]))
                                {
                                    sqlcmd = sqlcmd + "'" + csv["E-Mail Address"] + "',";
                                }
                                else
                                {
                                    sqlcmd = sqlcmd + "Null,";
                                }

                                //RoleId
                                sqlcmd = sqlcmd + "2";
                                sqlcmd = sqlcmd + ",0)";
                                richTextBox2.Text = richTextBox2.Text + "\n " + sqlcmd;
                            }
                        }
                        #endregion
                    }

                    if (!String.IsNullOrEmpty(csv["First Name"]))
                    {
                        if (!String.IsNullOrEmpty(csv["Last Name"]))
                        {
                            if (!String.IsNullOrEmpty(csv["Alcoa Location(s) this person is Responsible for"]))
                            {
                                string AllSites = csv["Alcoa Location(s) this person is Responsible for"];
                                string[] Sites = AllSites.Split(',');

                                foreach (string SiteName in Sites)
                                {
                                    string SiteId = GetsiteId(SiteName);
                                    if (!String.IsNullOrEmpty(SiteId))
                                    {
                                        string sqlcmd = "";
                                        sqlcmd = "INSERT INTO [ALCOA_WAO_CSMWP_dev].[dbo].[ContactsContractors] ";
                                        sqlcmd = sqlcmd + "([CompanyID],[ContactFirstName],[ContactLastName],[ContactRole],[ContactTitle],";
                                        sqlcmd = sqlcmd + "[ContactEmail],[ContactMobile], [ContactPhone], [SiteId], [ModifiedByUserId]) ";
                                        sqlcmd = sqlcmd + "VALUES (";

                                        string companyId = GetcompanyId(csv["Company"]);
                                        if (string.IsNullOrEmpty(companyId))
                                        {
                                            sqlcmd = sqlcmd + "Null,";
                                        }
                                        else
                                        {
                                            sqlcmd = sqlcmd + companyId + ",";
                                        }

                                        if (!String.IsNullOrEmpty(csv["First Name"]))
                                        {
                                            sqlcmd = sqlcmd + "'" + csv["First Name"] + "',";
                                        }
                                        else
                                        {
                                            sqlcmd = sqlcmd + "Null,";
                                        }
                                        if (!String.IsNullOrEmpty(csv["Last Name"]))
                                        {
                                            sqlcmd = sqlcmd + "'" + csv["Last Name"] + "',";
                                        }
                                        else
                                        {
                                            sqlcmd = sqlcmd + "Null,";
                                        }

                                        if (string.IsNullOrEmpty(csv["Position"]))
                                        {
                                            sqlcmd = sqlcmd + "Null,";
                                        }
                                        else
                                        {
                                            sqlcmd = sqlcmd + "'" + csv["Position"] + "',";
                                        }
                                        if (!String.IsNullOrEmpty(csv["Title"]))
                                        {
                                            sqlcmd = sqlcmd + "'" + csv["Title"] + "',";
                                        }
                                        else
                                        {
                                            sqlcmd = sqlcmd + "Null,";
                                        }

                                        if (!String.IsNullOrEmpty(csv["E-Mail Address"]))
                                        {
                                            sqlcmd = sqlcmd + "'" + csv["E-Mail Address"] + "',";
                                        }
                                        else
                                        {
                                            sqlcmd = sqlcmd + "Null,";
                                        }
                                        if (!String.IsNullOrEmpty(csv["Mobile Number"]))
                                        {
                                            sqlcmd = sqlcmd + "'" + csv["Mobile Number"] + "',";
                                        }
                                        else
                                        {
                                            sqlcmd = sqlcmd + "Null,";
                                        }
                                        if (!String.IsNullOrEmpty(csv["Telephone Number"]))
                                        {
                                            sqlcmd = sqlcmd + "'" + csv["Telephone Number"] + "',";
                                        }
                                        else
                                        {
                                            sqlcmd = sqlcmd + "Null,";
                                        }

                                        sqlcmd = sqlcmd + SiteId + ",0)";
                                        richTextBox2.Text = richTextBox2.Text + "\n " + sqlcmd;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            #endregion

        } //Generate SQL Script
        private void button5_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();

            saveFileDialog1.Filter = "sql script files (*.sql)|*.sql|All files (*.*)|*.*";
            //saveFileDialog1.FilterIndex = 2;
            saveFileDialog1.RestoreDirectory = true;

            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                System.IO.TextWriter tw = new StreamWriter(saveFileDialog1.OpenFile());
                tw.Write(richTextBox2.Text);
                tw.Close();
            }
        } //Save Generated SQL Script to File

        private bool IsInCollection2(ListViewItem lvi)
        {
            foreach (ListViewItem item in listView2.Items)
            {
                bool subItemEqualFlag = true;
                for (int i = 0; i < item.SubItems.Count; i++)
                {
                    string sub1 = item.SubItems[i].Text;
                    string sub2 = lvi.SubItems[i].Text;
                    if (sub1 != sub2)
                    {
                        subItemEqualFlag = false;
                    }
                }
                if (subItemEqualFlag)
                    return true;
            }
            return false;

        }
        private bool IsInCollection3(ListViewItem lvi)
        {
            foreach (ListViewItem item in listView3.Items)
            {
                bool subItemEqualFlag = true;
                for (int i = 0; i < item.SubItems.Count; i++)
                {
                    string sub1 = item.SubItems[i].Text;
                    string sub2 = lvi.SubItems[i].Text;
                    if (sub1 != sub2)
                    {
                        subItemEqualFlag = false;
                    }
                }
                if (subItemEqualFlag)
                    return true;
            }
            return false;

        }


        #endregion
        #region residential

        private void button9_Click_1(object sender, EventArgs e)
        {
            listView2.AutoResizeColumns(ColumnHeaderAutoResizeStyle.ColumnContent);
            listView3.AutoResizeColumns(ColumnHeaderAutoResizeStyle.ColumnContent);
            listView4.AutoResizeColumns(ColumnHeaderAutoResizeStyle.ColumnContent);
        }

        private void button12_Click(object sender, EventArgs e)
        {
            textBox3.Text = openFileName();
        }

        private void button11_Click(object sender, EventArgs e)
        {
            string csvFilePath = textBox3.Text;

            using (CsvReader csv =
                   new CsvReader(new StreamReader(csvFilePath), true))
            {
                int fieldCount = csv.FieldCount;
                string[] headers = csv.GetFieldHeaders();

                while (csv.ReadNextRecord())
                {
                    ListViewItem itemResidential = new ListViewItem(csv["Company Name"], 0);
                    itemResidential.SubItems.Add(csv["Site"]);
                    itemResidential.SubItems.Add(csv["Status"]);
                    itemResidential.SubItems.Add(GetcompanyId(csv["Company Name"]));
                    itemResidential.SubItems.Add(GetsiteId(csv["Site"]));

                    if (IsInCollection2(itemResidential) == false)
                    {
                        listView6.Items.Add(itemResidential);
                    }
                }
            } //Auto Resize Columns


        }

        private void button10_Click(object sender, EventArgs e)
        {
             string csvFilePath = textBox3.Text;

             using (CsvReader csv =
                    new CsvReader(new StreamReader(csvFilePath), true))
             {
                 int fieldCount = csv.FieldCount;
                 string[] headers = csv.GetFieldHeaders();

                 while (csv.ReadNextRecord())
                 {
                     if (!String.IsNullOrEmpty(csv["Site"]))
                     {
                         string AllSites = csv["Site"];
                         string[] Sites = AllSites.Split(',');

                         foreach (string SiteName in Sites)
                         {
                             string SiteId = GetsiteId(SiteName);
                             if (!String.IsNullOrEmpty(SiteId))
                             {
                                 string sqlcmd = "";
                                 sqlcmd = "INSERT INTO [ALCOA_WAO_CSMWP_dev].[dbo].[Residential] ";
                                 sqlcmd = sqlcmd + "([CompanyId],[SiteId],[ResidentialStatus]) ";
                                 sqlcmd = sqlcmd + "VALUES (";

                                 string companyId = GetcompanyId(csv["Company Name"]);
                                 if (string.IsNullOrEmpty(companyId))
                                 {
                                     sqlcmd = sqlcmd + "Null,";
                                 }
                                 else
                                 {
                                     sqlcmd = sqlcmd + companyId + ",";
                                 }

                                 string siteId = GetsiteId(SiteName);
                                 if (!String.IsNullOrEmpty(siteId))
                                 {
                                     sqlcmd = sqlcmd + siteId + ",";
                                 }
                                 else
                                 {
                                     sqlcmd = sqlcmd + "Null)";
                                 }
                                 if (!String.IsNullOrEmpty(csv["Status"]))
                                 {
                                     string rStatus = "0";
                                     if (csv["Status"] == "RESIDENTIAL") { rStatus = "1"; };
                                     sqlcmd = sqlcmd + rStatus + ")";
                                 }
                                 else
                                 {
                                     sqlcmd = sqlcmd + "Null)";
                                 }

                                 richTextBox3.Text = richTextBox3.Text + "\n " + sqlcmd;
                             }
                         }
                     }
                 }
             }
        }

        private void button13_Click(object sender, EventArgs e)
        {
            StreamReader re = File.OpenText("d:\\temp\\bp\\emailList.txt");
            string key = null;
            Hashtable ht = new Hashtable();

            String mailTo = "";
            while ((key = re.ReadLine()) != null)
            {
                string add;
                if ((key == null) || (key == "")) { add = ""; }
                else { add = key; }

                if (!ht.ContainsKey(add)) //Anti-Duplication
                {
                    ht[add] = 0;
                    mailTo += ";" + add;
                    //lblEmailList.Text += "<br />" + add + ",";
                }
            }
            //re.close;
            richTextBox1.Text = mailTo;
        }
        #endregion

        #region alcoan
        private bool IsInCollection7(ListViewItem lvi)
        {
            foreach (ListViewItem item in listView7.Items)
            {
                bool subItemEqualFlag = true;
                for (int i = 0; i < item.SubItems.Count; i++)
                {
                    string sub1 = item.SubItems[i].Text;
                    string sub2 = lvi.SubItems[i].Text;
                    if (sub1 != sub2)
                    {
                        subItemEqualFlag = false;
                    }
                }
                if (subItemEqualFlag)
                    return true;
            }
            return false;

        }
        
        private void button18_Click(object sender, EventArgs e)
        {
            textBox4.Text = openFileName();
        }
        #endregion

        private void button17_Click(object sender, EventArgs e)
        {
            string csvFilePath = textBox4.Text;

            using (CsvReader csv =
                   new CsvReader(new StreamReader(csvFilePath), true))
            {
                int fieldCount = csv.FieldCount;
                string[] headers = csv.GetFieldHeaders();

                while (csv.ReadNextRecord())
                {
                    //Company,Alcoa Logon ?,Date Sent to AD ?,Position,First Name,Last Name,E-Mail Address,Title,Telephone Number,Mobile Number,Alcoa Location(s) 
                    if (!String.IsNullOrEmpty(csv["E-Mail Address"]))
                    {
                        if (csv["Alcoa Logon ?"] == "Yes")
                        {
                            ListViewItem itemUser = new ListViewItem("AUSTRALIA_ASIA\\" + csv["UserLogon"], 0); //UserLogon
                            itemUser.SubItems.Add(csv["Last Name"]); //LastName
                            itemUser.SubItems.Add(csv["First Name"]); //FirstName
                            itemUser.SubItems.Add(csv["Company"]);
                            itemUser.SubItems.Add(GetcompanyId(csv["Company"])); //CompanyNameID
                            itemUser.SubItems.Add(csv["Position"]); //Title
                            itemUser.SubItems.Add(csv["Telephone Number"]); //PhoneNo
                            itemUser.SubItems.Add("[TBD]"); //FaxNo
                            itemUser.SubItems.Add(csv["Mobile Number"]); //MobNo
                            itemUser.SubItems.Add(csv["E-Mail Address"]);//Email
                            itemUser.SubItems.Add("1"); //RoleID - Assume Contractor by Default
                            itemUser.SubItems.Add(userExistAlready("AUSTRALIA_ASIA\\" + csv["UserLogon"]));

                            if (IsInCollection7(itemUser) == false)
                            {
                                listView7.Items.Add(itemUser);
                            }
                        }
                        else
                        {
                          
                        }

                        //add to alcoaContacts list
                        ListViewItem itemContact = new ListViewItem(csv["Alcoa Location(s) this person is Responsible for"], 0); //CompanyId
                        itemContact.SubItems.Add(GetsiteId(csv["Alcoa Location(s) this person is Responsible for"]));//SiteId
                        itemContact.SubItems.Add(csv["First Name"]); //ContactFirstName
                        itemContact.SubItems.Add(csv["Last Name"]); //ContactLastName
                        itemContact.SubItems.Add(csv["Position"]); //ContactTitle
                        itemContact.SubItems.Add(csv["E-Mail Address"]); //ContactEmail
                        itemContact.SubItems.Add(csv["Telephone Number"]); //no
                        
                        listView9.Items.Add(itemContact);

                    }
                }
            }
        }

        private void button16_Click(object sender, EventArgs e)
        {
            string csvFilePath = textBox4.Text;

            using (CsvReader csv =
                   new CsvReader(new StreamReader(csvFilePath), true))
            {
                int fieldCount = csv.FieldCount;
                string[] headers = csv.GetFieldHeaders();

                while (csv.ReadNextRecord())
                {
                    if (!String.IsNullOrEmpty(csv["E-Mail Address"]))
                    {
                        if (!String.IsNullOrEmpty(csv["UserLogon"]))
                        {
                            string sqlcmd = "";
                            sqlcmd = "INSERT INTO [ALCOA_WAO_CSMWP_dev].[dbo].[Users] ";
                            sqlcmd = sqlcmd + "([UserLogon],[LastName],[FirstName],[CompanyNameID],[Title],";
                            sqlcmd = sqlcmd + "[PhoneNo],[FaxNo], [MobNo], [Email], [RoleID], [ModifiedByUserId]) ";
                            sqlcmd = sqlcmd + "VALUES (";
                            sqlcmd = sqlcmd + "'AUSTRALIA_ASIA\\" + csv["UserLogon"] + "',";
                            if (!String.IsNullOrEmpty(csv["Last Name"]))
                            {
                                sqlcmd = sqlcmd + "'" + csv["Last Name"] + "',";
                            }
                            else
                            {
                                sqlcmd = sqlcmd + "Null,";
                            }
                            if (!String.IsNullOrEmpty(csv["First Name"]))
                            {
                                sqlcmd = sqlcmd + "'" + csv["First Name"] + "',";
                            }
                            else
                            {
                                sqlcmd = sqlcmd + "Null,";
                            }

                            string companyId = GetcompanyId(csv["Company"]);
                            if (string.IsNullOrEmpty(companyId))
                            {
                                sqlcmd = sqlcmd + "Null,";
                            }
                            else
                            {
                                sqlcmd = sqlcmd + companyId + ",";
                            }
                            if (!String.IsNullOrEmpty(csv["Position"]))
                            {
                                sqlcmd = sqlcmd + "'" + csv["Position"] + "',";
                            }
                            else
                            {
                                sqlcmd = sqlcmd + "Null,";
                            }
                            if (!String.IsNullOrEmpty(csv["Telephone Number"]))
                            {
                                sqlcmd = sqlcmd + "'" + csv["Telephone Number"] + "',";
                            }
                            else
                            {
                                sqlcmd = sqlcmd + "Null,";
                            }
                            sqlcmd = sqlcmd + "Null,"; //faxno
                            if (!String.IsNullOrEmpty(csv["Mobile Number"]))
                            {
                                sqlcmd = sqlcmd + "'" + csv["Mobile Number"] + "',";
                            }
                            else
                            {
                                sqlcmd = sqlcmd + "Null,";
                            }
                            if (!String.IsNullOrEmpty(csv["E-Mail Address"]))
                            {
                                sqlcmd = sqlcmd + "'" + csv["E-Mail Address"] + "',";
                            }
                            else
                            {
                                sqlcmd = sqlcmd + "Null,";
                            }

                            //RoleId
                            sqlcmd = sqlcmd + "1";
                            sqlcmd = sqlcmd + ",0)";
                            richTextBox4.Text = richTextBox4.Text + "\n " + sqlcmd;
                        }
                    }

                    string AllSites = csv["Alcoa Location(s) this person is Responsible for"];
                            string[] Sites = AllSites.Split(',');

                            foreach (string SiteName in Sites)
                            {
                                string SiteId = GetsiteId(SiteName);
                                if (!String.IsNullOrEmpty(SiteId))
                                {
                                    string sqlcmd = "";
                                    sqlcmd = "INSERT INTO [ALCOA_WAO_CSMWP_dev].[dbo].[ContactsAlcoa] ";
                                    sqlcmd = sqlcmd + "([SiteId],[ContactFirstName],[ContactLastName],[ContactTitlePosition],";
                                    sqlcmd = sqlcmd + "[ContactEmail],[ContactMainNo],[ModifiedByUserId]) ";
                                    sqlcmd = sqlcmd + "VALUES (";

                                    if (string.IsNullOrEmpty(SiteId))
                                    {
                                        sqlcmd = sqlcmd + "Null,";
                                    }
                                    else
                                    {
                                        sqlcmd = sqlcmd + SiteId + ",";
                                    }

                                    if (!String.IsNullOrEmpty(csv["First Name"]))
                                    {
                                        sqlcmd = sqlcmd + "'" + csv["First Name"] + "',";
                                    }
                                    else
                                    {
                                        sqlcmd = sqlcmd + "Null,";
                                    }
                                    if (!String.IsNullOrEmpty(csv["Last Name"]))
                                    {
                                        sqlcmd = sqlcmd + "'" + csv["Last Name"] + "',";
                                    }
                                    else
                                    {
                                        sqlcmd = sqlcmd + "Null,";
                                    }

                                    if (string.IsNullOrEmpty(csv["Position"]))
                                    {
                                        sqlcmd = sqlcmd + "Null,";
                                    }
                                    else
                                    {
                                        sqlcmd = sqlcmd + "'" + csv["Position"] + "',";
                                    }

                                    if (!String.IsNullOrEmpty(csv["E-Mail Address"]))
                                    {
                                        sqlcmd = sqlcmd + "'" + csv["E-Mail Address"] + "',";
                                    }
                                    else
                                    {
                                        sqlcmd = sqlcmd + "Null,";
                                    }
                                    if (!String.IsNullOrEmpty(csv["Telephone Number"]))
                                    {
                                        sqlcmd = sqlcmd + "'" + csv["Telephone Number"] + "'";
                                    }
                                    else
                                    {
                                        sqlcmd = sqlcmd + "Null";
                                    }

                                    sqlcmd = sqlcmd + ",0)";
                                    richTextBox4.Text = richTextBox4.Text + "\n " + sqlcmd;
                                }
                            }

                }
            }
        }

        private void button20_Click(object sender, EventArgs e)
        {
            textBox5.Text = openFileNameRoboCopyLogs();
        } //Get Absolute File Path of RoboCopy log File

        private void button19_Click(object sender, EventArgs e)
        {
            string[] fileEntries = Directory.GetFiles(textBox5.Text);
            foreach (string fileName in fileEntries)
            {
                if (fileName.Contains("WAOCSM_Sync"))
                {
                    TextReader textr = new StreamReader(fileName);
                    string line;
                    int lineNumber = 0;

                    string TimeStampField = "  Started : ";
                    string TimeStamp = "";

                    string divider = "------------------------------------------------------------------------------";
                    int dividerSection = 0;

                    int filesLine = 0;
                    int entries = 0;

                    while ((line = textr.ReadLine()) != null)
                    {
                        if (line.Contains(TimeStampField))
                        {
                            TimeStamp = line.Replace(TimeStampField, "");
                        }
                        if (line.Contains(divider) == true)
                        {
                            dividerSection++;
                        }
                        else
                        {
                            switch (dividerSection)
                            {
                                case 3:
                                    //begin filechanges log
                                    filesLine++;
                                    if ((filesLine >= 3) && line.Length > 0)
                                    {
                                        string[] ChangedFiles = line.Split('\t');
                                        ListViewItem lv = new ListViewItem(TimeStamp);
                                        lv.SubItems.Add(ChangedFiles[4].ToString());  //filename
                                        lv.SubItems.Add(TrimString(ChangedFiles[1].ToString())); //filetag

                                        if (TrimString(ChangedFiles[1].ToString()) == "Older")
                                        {
                                            lv.SubItems.Add("Do Nothing!");
                                        }
                                        else
                                        {
                                            lv.SubItems.Add("Reset Read/Unread Status!");
                                        }

                                        listView8.Items.Add(lv);
                                        entries++;
                                        //richTextBox5.Text += colum + "\n";
                                    }

                                    break;
                                case 4:
                                    //end filechanges log
                                    break;
                                default:
                                    break;
                            }
                        }

                        lineNumber++;
                    }
                    textr.Close();

                    //richTextBox5.Text += TimeStamp + "\n";
                    richTextBox5.Text += "Processed File: " + fileName + "with (" + entries + ") entries.\n";

                } //Parse RoboCopy Log file to lstview and results to richtextbox...
            }
        }

        /// <summary>
        /// Method for removing trailing and leadingt whitespace from a string
        /// </summary>
        /// <param name="str">The string to trim</param>
        /// <returns></returns>
        public string TrimString(string str)
        {
            try
            {
                string pattern = @"^[ \t]+|[ \t]+$";
                Regex reg = new Regex(pattern, RegexOptions.IgnoreCase);
                str = reg.Replace(str, "");
                return str;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void button23_Click(object sender, EventArgs e)
        {
            lvsims.Items.Clear();
            String csvFilePath = tbSimsCSV.Text;

            using (CsvReader csv =
                   new CsvReader(new StreamReader(csvFilePath), true))
            {
                int fieldCount = csv.FieldCount;
                string[] headers = csv.GetFieldHeaders();

                while (csv.ReadNextRecord())
                {
                    if (!String.IsNullOrEmpty(csv["WAO CSM Company Name"]))
                    {

                            ListViewItem itemUser = new ListViewItem(csv["Location Code"], 0);
                            itemUser.SubItems.Add(csv["Cont Company Code"]);
                            itemUser.SubItems.Add(csv["WAO CSM Company Name"]);

                            if (IsInCollection7(itemUser) == false)
                            {
                                lvsims.Items.Add(itemUser);
                            }
                    }
                }
            }
        }

        private void button24_Click(object sender, EventArgs e)
        {
            tbSimsCSV.Text = openFileName();
        }

        private void button22_Click(object sender, EventArgs e)
        {
            //CompaniesEhsimsMap
            string csvFilePath = tbSimsCSV.Text;

            using (CsvReader csv =
                   new CsvReader(new StreamReader(csvFilePath), true))
            {
                int fieldCount = csv.FieldCount;
                string[] headers = csv.GetFieldHeaders();

                while (csv.ReadNextRecord())
                {
                    string sqlcmd = "";
                    sqlcmd = "INSERT INTO [ALCOA_WAO_CSMWP].[dbo].[CompaniesEhsimsMap] ";
                    sqlcmd = sqlcmd + "([LocCode],[ContCompanyCode],[CompanyId]) ";
                    sqlcmd = sqlcmd + "VALUES (";


                    if (!String.IsNullOrEmpty(csv["Location Code"]))
                    {
                        sqlcmd = sqlcmd + "'" + csv["Location Code"] + "',";
                    }
                    else
                    {
                        sqlcmd = sqlcmd + "Null)";
                    }

                    if (!String.IsNullOrEmpty(csv["Cont Company Code"]))
                    {
                        sqlcmd = sqlcmd + "'" + csv["Cont Company Code"] + "',";
                    }
                    else
                    {
                        sqlcmd = sqlcmd + "Null)";
                    }

                    string companyId = GetcompanyId(csv["WAO CSM Company Name"]);
                    if (string.IsNullOrEmpty(companyId))
                    {
                        sqlcmd = sqlcmd + "Null,";
                    }
                    else
                    {
                        sqlcmd = sqlcmd + companyId + ")";
                    }

                    rtbsims.Text = rtbsims.Text + "\n " + sqlcmd;
                }
            }
        }

        private void button25_Click(object sender, EventArgs e)
        {
            string csvFilePath = textBox4.Text;

            using (CsvReader csv =
                   new CsvReader(new StreamReader(csvFilePath), true))
            {
                int fieldCount = csv.FieldCount;
                string[] headers = csv.GetFieldHeaders();

                while (csv.ReadNextRecord())
                {
                    if (!String.IsNullOrEmpty(csv["E-Mail Address"]))
                    {
                        if (!String.IsNullOrEmpty(csv["UserLogon"]))
                        {
                            string sqlcmd = "";
                            sqlcmd = "INSERT INTO [ALCOA_WAO_CSMWP_dev].[dbo].[Users] ";
                            sqlcmd = sqlcmd + "([UserLogon],[LastName],[FirstName],[CompanyNameID],[Title],";
                            sqlcmd = sqlcmd + "[PhoneNo],[FaxNo], [MobNo], [Email], [RoleID], [ModifiedByUserId]) ";
                            sqlcmd = sqlcmd + "VALUES (";
                            sqlcmd = sqlcmd + "'AUSTRALIA_ASIA\\" + csv["UserLogon"] + "',";
                            if (!String.IsNullOrEmpty(csv["Last Name"]))
                            {
                                sqlcmd = sqlcmd + "'" + csv["Last Name"] + "',";
                            }
                            else
                            {
                                sqlcmd = sqlcmd + "Null,";
                            }
                            if (!String.IsNullOrEmpty(csv["First Name"]))
                            {
                                sqlcmd = sqlcmd + "'" + csv["First Name"] + "',";
                            }
                            else
                            {
                                sqlcmd = sqlcmd + "Null,";
                            }

                            string companyId = GetcompanyId(csv["Company"]);
                            if (string.IsNullOrEmpty(companyId))
                            {
                                sqlcmd = sqlcmd + "Null,";
                            }
                            else
                            {
                                sqlcmd = sqlcmd + companyId + ",";
                            }
                            if (!String.IsNullOrEmpty(csv["Position"]))
                            {
                                sqlcmd = sqlcmd + "'" + csv["Position"] + "',";
                            }
                            else
                            {
                                sqlcmd = sqlcmd + "Null,";
                            }
                            if (!String.IsNullOrEmpty(csv["Telephone Number"]))
                            {
                                sqlcmd = sqlcmd + "'" + csv["Telephone Number"] + "',";
                            }
                            else
                            {
                                sqlcmd = sqlcmd + "Null,";
                            }
                            sqlcmd = sqlcmd + "Null,"; //faxno
                            if (!String.IsNullOrEmpty(csv["Mobile Number"]))
                            {
                                sqlcmd = sqlcmd + "'" + csv["Mobile Number"] + "',";
                            }
                            else
                            {
                                sqlcmd = sqlcmd + "Null,";
                            }
                            if (!String.IsNullOrEmpty(csv["E-Mail Address"]))
                            {
                                sqlcmd = sqlcmd + "'" + csv["E-Mail Address"] + "',";
                            }
                            else
                            {
                                sqlcmd = sqlcmd + "Null,";
                            }

                            //RoleId
                            sqlcmd = sqlcmd + "1";
                            sqlcmd = sqlcmd + ",0)";
                            richTextBox4.Text = richTextBox4.Text + "\n " + sqlcmd;
                        }
                    }
                }
            }

            using (CsvReader csv =
                   new CsvReader(new StreamReader(csvFilePath), true))
            {
                int fieldCount = csv.FieldCount;
                string[] headers = csv.GetFieldHeaders();

                while (csv.ReadNextRecord())
                {
                    if (!String.IsNullOrEmpty(csv["E-Mail Address"]))
                    {
                        if (!String.IsNullOrEmpty(csv["UserLogon"]))
                        {
                            string sqlcmd = "";
                            sqlcmd = "INSERT INTO [ALCOA_WAO_CSMWP_dev].[dbo].[UsersProcurement] ";
                            sqlcmd = sqlcmd + "([UserId],[Enabled]) ";
                            sqlcmd = sqlcmd + "VALUES (";
                            sqlcmd = sqlcmd + GetuserId(csv["UserLogon"]) + " ";
                            //Enabled
                            sqlcmd = sqlcmd + ",1)";
                            richTextBox4.Text = richTextBox4.Text + "\n " + sqlcmd;
                        }
                    }
                }
            }


        }
    }
}