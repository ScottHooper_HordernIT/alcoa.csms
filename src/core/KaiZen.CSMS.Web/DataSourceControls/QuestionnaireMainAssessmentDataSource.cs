﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.QuestionnaireMainAssessmentProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(QuestionnaireMainAssessmentDataSourceDesigner))]
	public class QuestionnaireMainAssessmentDataSource : ProviderDataSource<QuestionnaireMainAssessment, QuestionnaireMainAssessmentKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainAssessmentDataSource class.
		/// </summary>
		public QuestionnaireMainAssessmentDataSource() : base(new QuestionnaireMainAssessmentService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the QuestionnaireMainAssessmentDataSourceView used by the QuestionnaireMainAssessmentDataSource.
		/// </summary>
		protected QuestionnaireMainAssessmentDataSourceView QuestionnaireMainAssessmentView
		{
			get { return ( View as QuestionnaireMainAssessmentDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the QuestionnaireMainAssessmentDataSource control invokes to retrieve data.
		/// </summary>
		public QuestionnaireMainAssessmentSelectMethod SelectMethod
		{
			get
			{
				QuestionnaireMainAssessmentSelectMethod selectMethod = QuestionnaireMainAssessmentSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (QuestionnaireMainAssessmentSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the QuestionnaireMainAssessmentDataSourceView class that is to be
		/// used by the QuestionnaireMainAssessmentDataSource.
		/// </summary>
		/// <returns>An instance of the QuestionnaireMainAssessmentDataSourceView class.</returns>
		protected override BaseDataSourceView<QuestionnaireMainAssessment, QuestionnaireMainAssessmentKey> GetNewDataSourceView()
		{
			return new QuestionnaireMainAssessmentDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the QuestionnaireMainAssessmentDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class QuestionnaireMainAssessmentDataSourceView : ProviderDataSourceView<QuestionnaireMainAssessment, QuestionnaireMainAssessmentKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainAssessmentDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the QuestionnaireMainAssessmentDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public QuestionnaireMainAssessmentDataSourceView(QuestionnaireMainAssessmentDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal QuestionnaireMainAssessmentDataSource QuestionnaireMainAssessmentOwner
		{
			get { return Owner as QuestionnaireMainAssessmentDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal QuestionnaireMainAssessmentSelectMethod SelectMethod
		{
			get { return QuestionnaireMainAssessmentOwner.SelectMethod; }
			set { QuestionnaireMainAssessmentOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal QuestionnaireMainAssessmentService QuestionnaireMainAssessmentProvider
		{
			get { return Provider as QuestionnaireMainAssessmentService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<QuestionnaireMainAssessment> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<QuestionnaireMainAssessment> results = null;
			QuestionnaireMainAssessment item;
			count = 0;
			
			System.Int32 _responseId;
			System.Int32 _questionnaireId;
			System.String _questionNo;
			System.Boolean? _assessorApproval_nullable;
			System.Int32 _modifiedByUserId;

			switch ( SelectMethod )
			{
				case QuestionnaireMainAssessmentSelectMethod.Get:
					QuestionnaireMainAssessmentKey entityKey  = new QuestionnaireMainAssessmentKey();
					entityKey.Load(values);
					item = QuestionnaireMainAssessmentProvider.Get(entityKey);
					results = new TList<QuestionnaireMainAssessment>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case QuestionnaireMainAssessmentSelectMethod.GetAll:
                    results = QuestionnaireMainAssessmentProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case QuestionnaireMainAssessmentSelectMethod.GetPaged:
					results = QuestionnaireMainAssessmentProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case QuestionnaireMainAssessmentSelectMethod.Find:
					if ( FilterParameters != null )
						results = QuestionnaireMainAssessmentProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = QuestionnaireMainAssessmentProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case QuestionnaireMainAssessmentSelectMethod.GetByResponseId:
					_responseId = ( values["ResponseId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["ResponseId"], typeof(System.Int32)) : (int)0;
					item = QuestionnaireMainAssessmentProvider.GetByResponseId(_responseId);
					results = new TList<QuestionnaireMainAssessment>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				case QuestionnaireMainAssessmentSelectMethod.GetByQuestionnaireIdQuestionNo:
					_questionnaireId = ( values["QuestionnaireId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["QuestionnaireId"], typeof(System.Int32)) : (int)0;
					_questionNo = ( values["QuestionNo"] != null ) ? (System.String) EntityUtil.ChangeType(values["QuestionNo"], typeof(System.String)) : string.Empty;
					item = QuestionnaireMainAssessmentProvider.GetByQuestionnaireIdQuestionNo(_questionnaireId, _questionNo);
					results = new TList<QuestionnaireMainAssessment>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case QuestionnaireMainAssessmentSelectMethod.GetByQuestionnaireIdAssessorApproval:
					_questionnaireId = ( values["QuestionnaireId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["QuestionnaireId"], typeof(System.Int32)) : (int)0;
					_assessorApproval_nullable = (System.Boolean?) EntityUtil.ChangeType(values["AssessorApproval"], typeof(System.Boolean?));
					results = QuestionnaireMainAssessmentProvider.GetByQuestionnaireIdAssessorApproval(_questionnaireId, _assessorApproval_nullable, this.StartIndex, this.PageSize, out count);
					break;
				// FK
				case QuestionnaireMainAssessmentSelectMethod.GetByQuestionnaireId:
					_questionnaireId = ( values["QuestionnaireId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["QuestionnaireId"], typeof(System.Int32)) : (int)0;
					results = QuestionnaireMainAssessmentProvider.GetByQuestionnaireId(_questionnaireId, this.StartIndex, this.PageSize, out count);
					break;
				case QuestionnaireMainAssessmentSelectMethod.GetByModifiedByUserId:
					_modifiedByUserId = ( values["ModifiedByUserId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["ModifiedByUserId"], typeof(System.Int32)) : (int)0;
					results = QuestionnaireMainAssessmentProvider.GetByModifiedByUserId(_modifiedByUserId, this.StartIndex, this.PageSize, out count);
					break;
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == QuestionnaireMainAssessmentSelectMethod.Get || SelectMethod == QuestionnaireMainAssessmentSelectMethod.GetByResponseId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				QuestionnaireMainAssessment entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					QuestionnaireMainAssessmentProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<QuestionnaireMainAssessment> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			QuestionnaireMainAssessmentProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region QuestionnaireMainAssessmentDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the QuestionnaireMainAssessmentDataSource class.
	/// </summary>
	public class QuestionnaireMainAssessmentDataSourceDesigner : ProviderDataSourceDesigner<QuestionnaireMainAssessment, QuestionnaireMainAssessmentKey>
	{
		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainAssessmentDataSourceDesigner class.
		/// </summary>
		public QuestionnaireMainAssessmentDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public QuestionnaireMainAssessmentSelectMethod SelectMethod
		{
			get { return ((QuestionnaireMainAssessmentDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new QuestionnaireMainAssessmentDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region QuestionnaireMainAssessmentDataSourceActionList

	/// <summary>
	/// Supports the QuestionnaireMainAssessmentDataSourceDesigner class.
	/// </summary>
	internal class QuestionnaireMainAssessmentDataSourceActionList : DesignerActionList
	{
		private QuestionnaireMainAssessmentDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainAssessmentDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public QuestionnaireMainAssessmentDataSourceActionList(QuestionnaireMainAssessmentDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public QuestionnaireMainAssessmentSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion QuestionnaireMainAssessmentDataSourceActionList
	
	#endregion QuestionnaireMainAssessmentDataSourceDesigner
	
	#region QuestionnaireMainAssessmentSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the QuestionnaireMainAssessmentDataSource.SelectMethod property.
	/// </summary>
	public enum QuestionnaireMainAssessmentSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByResponseId method.
		/// </summary>
		GetByResponseId,
		/// <summary>
		/// Represents the GetByQuestionnaireIdQuestionNo method.
		/// </summary>
		GetByQuestionnaireIdQuestionNo,
		/// <summary>
		/// Represents the GetByQuestionnaireIdAssessorApproval method.
		/// </summary>
		GetByQuestionnaireIdAssessorApproval,
		/// <summary>
		/// Represents the GetByQuestionnaireId method.
		/// </summary>
		GetByQuestionnaireId,
		/// <summary>
		/// Represents the GetByModifiedByUserId method.
		/// </summary>
		GetByModifiedByUserId
	}
	
	#endregion QuestionnaireMainAssessmentSelectMethod

	#region QuestionnaireMainAssessmentFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireMainAssessment"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireMainAssessmentFilter : SqlFilter<QuestionnaireMainAssessmentColumn>
	{
	}
	
	#endregion QuestionnaireMainAssessmentFilter

	#region QuestionnaireMainAssessmentExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireMainAssessment"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireMainAssessmentExpressionBuilder : SqlExpressionBuilder<QuestionnaireMainAssessmentColumn>
	{
	}
	
	#endregion QuestionnaireMainAssessmentExpressionBuilder	

	#region QuestionnaireMainAssessmentProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;QuestionnaireMainAssessmentChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireMainAssessment"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireMainAssessmentProperty : ChildEntityProperty<QuestionnaireMainAssessmentChildEntityTypes>
	{
	}
	
	#endregion QuestionnaireMainAssessmentProperty
}

