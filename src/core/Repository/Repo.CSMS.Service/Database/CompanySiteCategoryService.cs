﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface ICompanySiteCategoryService
    {
        CompanySiteCategory Get(Expression<Func<CompanySiteCategory, bool>> where, List<Expression<Func<CompanySiteCategory, object>>> includes);
        List<CompanySiteCategory> GetMany(int? top, Expression<Func<CompanySiteCategory, bool>> where, Func<IQueryable<CompanySiteCategory>, IOrderedQueryable<CompanySiteCategory>> orderBy, List<Expression<Func<CompanySiteCategory, object>>> includes);
        List<CompanySiteCategory> GetManyPaged(int pageNumber, int pageSize, Expression<Func<CompanySiteCategory, bool>> where, Func<IQueryable<CompanySiteCategory>, IOrderedQueryable<CompanySiteCategory>> orderBy, List<Expression<Func<CompanySiteCategory, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<CompanySiteCategory, bool>> where, List<Expression<Func<CompanySiteCategory, object>>> includes);
        void Insert(CompanySiteCategory item, bool saveChanges = true);
        void Update(CompanySiteCategory item, bool saveChanges = true);
        void Delete(CompanySiteCategory item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class CompanySiteCategoryService : ICompanySiteCategoryService
    {
        private readonly IRepository<CompanySiteCategory> _repository;

        public CompanySiteCategoryService(IRepository<CompanySiteCategory> repository)
        {
            this._repository = repository;
        }

        public CompanySiteCategory Get(Expression<Func<CompanySiteCategory, bool>> where, List<Expression<Func<CompanySiteCategory, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<CompanySiteCategory> GetMany(int? top, Expression<Func<CompanySiteCategory, bool>> where, Func<IQueryable<CompanySiteCategory>, IOrderedQueryable<CompanySiteCategory>> orderBy, List<Expression<Func<CompanySiteCategory, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<CompanySiteCategory> GetManyPaged(int pageNumber, int pageSize, Expression<Func<CompanySiteCategory, bool>> where, Func<IQueryable<CompanySiteCategory>, IOrderedQueryable<CompanySiteCategory>> orderBy, List<Expression<Func<CompanySiteCategory, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<CompanySiteCategory, bool>> where, List<Expression<Func<CompanySiteCategory, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(CompanySiteCategory item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("CompanySiteCategory");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(CompanySiteCategory item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("CompanySiteCategory");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(CompanySiteCategory item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("CompanySiteCategory");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
