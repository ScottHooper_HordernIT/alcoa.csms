﻿
#region Using Directives
using System;
using System.ComponentModel;
using System.Collections;
using System.Xml.Serialization;
using System.Data;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Entities.Validation;

using KaiZen.CSMS.Data;
using Microsoft.Practices.EnterpriseLibrary.Logging;

#endregion

namespace KaiZen.CSMS.Services
{		
	
	///<summary>
	/// An component type implementation of the 'ConfigSa812Sites' table.
	///</summary>
	/// <remarks>
	/// All custom implementations should be done here.
	/// </remarks>
	[CLSCompliant(true)]
	public partial class ConfigSa812SitesService : KaiZen.CSMS.Services.ConfigSa812SitesServiceBase
	{
		/// <summary>
		/// Initializes a new instance of the ConfigSa812SitesService class.
		/// </summary>
		public ConfigSa812SitesService() : base()
		{
		}
		
	}//End Class


} // end namespace
