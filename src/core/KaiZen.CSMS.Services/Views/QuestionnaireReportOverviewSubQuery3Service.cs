﻿
#region Using Directives
using System;
using System.ComponentModel;
using System.Collections;
using System.Xml.Serialization;
using System.Data;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Entities.Validation;

using KaiZen.CSMS.Data;
using Microsoft.Practices.EnterpriseLibrary.Logging;

#endregion

namespace KaiZen.CSMS.Services
{		
	
	///<summary>
	/// An component type implementation of the 'QuestionnaireReportOverview_SubQuery3' table.
	///</summary>
	/// <remarks>
	/// All custom implementations should be done here.
	/// </remarks>
	[CLSCompliant(true)]
	public partial class QuestionnaireReportOverviewSubQuery3Service : KaiZen.CSMS.Services.QuestionnaireReportOverviewSubQuery3ServiceBase
	{
		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportOverviewSubQuery3Service class.
		/// </summary>
		public QuestionnaireReportOverviewSubQuery3Service() : base()
		{
		}
		
	}//End Class


} // end namespace
