﻿#region Using directives

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="CompaniesEhsConsultantsProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract class CompaniesEhsConsultantsProviderBaseCore : EntityViewProviderBase<CompaniesEhsConsultants>
	{
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions
		
		/*
		///<summary>
		/// Fill an VList&lt;CompaniesEhsConsultants&gt; From a DataSet
		///</summary>
		/// <param name="dataSet">the DataSet</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList&lt;CompaniesEhsConsultants&gt;"/></returns>
		protected static VList&lt;CompaniesEhsConsultants&gt; Fill(DataSet dataSet, VList<CompaniesEhsConsultants> rows, int start, int pagelen)
		{
			if (dataSet.Tables.Count == 1)
			{
				return Fill(dataSet.Tables[0], rows, start, pagelen);
			}
			else
			{
				return new VList<CompaniesEhsConsultants>();
			}	
		}
		
		
		///<summary>
		/// Fill an VList&lt;CompaniesEhsConsultants&gt; From a DataTable
		///</summary>
		/// <param name="dataTable">the DataTable that hold the data.</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList<CompaniesEhsConsultants>"/></returns>
		protected static VList&lt;CompaniesEhsConsultants&gt; Fill(DataTable dataTable, VList<CompaniesEhsConsultants> rows, int start, int pagelen)
		{
			int recordnum = 0;
			
			System.Collections.IEnumerator dataRows =  dataTable.Rows.GetEnumerator();
			
			while (dataRows.MoveNext() && (pagelen != 0))
			{
				if(recordnum >= start)
				{
					DataRow row = (DataRow)dataRows.Current;
				
					CompaniesEhsConsultants c = new CompaniesEhsConsultants();
					c.CompanyId = (Convert.IsDBNull(row["CompanyId"]))?(int)0:(System.Int32)row["CompanyId"];
					c.CompanyName = (Convert.IsDBNull(row["CompanyName"]))?string.Empty:(System.String)row["CompanyName"];
					c.EhsConsultantId = (Convert.IsDBNull(row["EHSConsultantId"]))?(int)0:(System.Int32?)row["EHSConsultantId"];
					c.UserId = (Convert.IsDBNull(row["UserId"]))?(int)0:(System.Int32?)row["UserId"];
					c.Enabled = (Convert.IsDBNull(row["Enabled"]))?false:(System.Boolean?)row["Enabled"];
					c.UserFullName = (Convert.IsDBNull(row["UserFullName"]))?string.Empty:(System.String)row["UserFullName"];
					c.UserFullNameLogon = (Convert.IsDBNull(row["UserFullNameLogon"]))?string.Empty:(System.String)row["UserFullNameLogon"];
					c.Email = (Convert.IsDBNull(row["Email"]))?string.Empty:(System.String)row["Email"];
					c.PhoneNo = (Convert.IsDBNull(row["PhoneNo"]))?string.Empty:(System.String)row["PhoneNo"];
					c.AcceptChanges();
					rows.Add(c);
					pagelen -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		*/	
						
		///<summary>
		/// Fill an <see cref="VList&lt;CompaniesEhsConsultants&gt;"/> From a DataReader.
		///</summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pageLength">number of row.</param>
		///<returns>a <see cref="VList&lt;CompaniesEhsConsultants&gt;"/></returns>
		protected VList<CompaniesEhsConsultants> Fill(IDataReader reader, VList<CompaniesEhsConsultants> rows, int start, int pageLength)
		{
			int recordnum = 0;
			while (reader.Read() && (pageLength != 0))
			{
				if(recordnum >= start)
				{
					CompaniesEhsConsultants entity = null;
					if (DataRepository.Provider.UseEntityFactory)
					{
						entity = EntityManager.CreateViewEntity<CompaniesEhsConsultants>("CompaniesEhsConsultants",  DataRepository.Provider.EntityCreationalFactoryType); 
					}
					else
					{
						entity = new CompaniesEhsConsultants();
					}
					
					entity.SuppressEntityEvents = true;

					entity.CompanyId = (System.Int32)reader[((int)CompaniesEhsConsultantsColumn.CompanyId)];
					//entity.CompanyId = (Convert.IsDBNull(reader["CompanyId"]))?(int)0:(System.Int32)reader["CompanyId"];
					entity.CompanyName = (System.String)reader[((int)CompaniesEhsConsultantsColumn.CompanyName)];
					//entity.CompanyName = (Convert.IsDBNull(reader["CompanyName"]))?string.Empty:(System.String)reader["CompanyName"];
					entity.EhsConsultantId = (reader.IsDBNull(((int)CompaniesEhsConsultantsColumn.EhsConsultantId)))?null:(System.Int32?)reader[((int)CompaniesEhsConsultantsColumn.EhsConsultantId)];
					//entity.EhsConsultantId = (Convert.IsDBNull(reader["EHSConsultantId"]))?(int)0:(System.Int32?)reader["EHSConsultantId"];
					entity.UserId = (reader.IsDBNull(((int)CompaniesEhsConsultantsColumn.UserId)))?null:(System.Int32?)reader[((int)CompaniesEhsConsultantsColumn.UserId)];
					//entity.UserId = (Convert.IsDBNull(reader["UserId"]))?(int)0:(System.Int32?)reader["UserId"];
					entity.Enabled = (reader.IsDBNull(((int)CompaniesEhsConsultantsColumn.Enabled)))?null:(System.Boolean?)reader[((int)CompaniesEhsConsultantsColumn.Enabled)];
					//entity.Enabled = (Convert.IsDBNull(reader["Enabled"]))?false:(System.Boolean?)reader["Enabled"];
					entity.UserFullName = (reader.IsDBNull(((int)CompaniesEhsConsultantsColumn.UserFullName)))?null:(System.String)reader[((int)CompaniesEhsConsultantsColumn.UserFullName)];
					//entity.UserFullName = (Convert.IsDBNull(reader["UserFullName"]))?string.Empty:(System.String)reader["UserFullName"];
					entity.UserFullNameLogon = (reader.IsDBNull(((int)CompaniesEhsConsultantsColumn.UserFullNameLogon)))?null:(System.String)reader[((int)CompaniesEhsConsultantsColumn.UserFullNameLogon)];
					//entity.UserFullNameLogon = (Convert.IsDBNull(reader["UserFullNameLogon"]))?string.Empty:(System.String)reader["UserFullNameLogon"];
					entity.Email = (reader.IsDBNull(((int)CompaniesEhsConsultantsColumn.Email)))?null:(System.String)reader[((int)CompaniesEhsConsultantsColumn.Email)];
					//entity.Email = (Convert.IsDBNull(reader["Email"]))?string.Empty:(System.String)reader["Email"];
					entity.PhoneNo = (reader.IsDBNull(((int)CompaniesEhsConsultantsColumn.PhoneNo)))?null:(System.String)reader[((int)CompaniesEhsConsultantsColumn.PhoneNo)];
					//entity.PhoneNo = (Convert.IsDBNull(reader["PhoneNo"]))?string.Empty:(System.String)reader["PhoneNo"];
					entity.AcceptChanges();
					entity.SuppressEntityEvents = false;
					
					rows.Add(entity);
					pageLength -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		
		
		/// <summary>
		/// Refreshes the <see cref="CompaniesEhsConsultants"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="CompaniesEhsConsultants"/> object to refresh.</param>
		protected void RefreshEntity(IDataReader reader, CompaniesEhsConsultants entity)
		{
			reader.Read();
			entity.CompanyId = (System.Int32)reader[((int)CompaniesEhsConsultantsColumn.CompanyId)];
			//entity.CompanyId = (Convert.IsDBNull(reader["CompanyId"]))?(int)0:(System.Int32)reader["CompanyId"];
			entity.CompanyName = (System.String)reader[((int)CompaniesEhsConsultantsColumn.CompanyName)];
			//entity.CompanyName = (Convert.IsDBNull(reader["CompanyName"]))?string.Empty:(System.String)reader["CompanyName"];
			entity.EhsConsultantId = (reader.IsDBNull(((int)CompaniesEhsConsultantsColumn.EhsConsultantId)))?null:(System.Int32?)reader[((int)CompaniesEhsConsultantsColumn.EhsConsultantId)];
			//entity.EhsConsultantId = (Convert.IsDBNull(reader["EHSConsultantId"]))?(int)0:(System.Int32?)reader["EHSConsultantId"];
			entity.UserId = (reader.IsDBNull(((int)CompaniesEhsConsultantsColumn.UserId)))?null:(System.Int32?)reader[((int)CompaniesEhsConsultantsColumn.UserId)];
			//entity.UserId = (Convert.IsDBNull(reader["UserId"]))?(int)0:(System.Int32?)reader["UserId"];
			entity.Enabled = (reader.IsDBNull(((int)CompaniesEhsConsultantsColumn.Enabled)))?null:(System.Boolean?)reader[((int)CompaniesEhsConsultantsColumn.Enabled)];
			//entity.Enabled = (Convert.IsDBNull(reader["Enabled"]))?false:(System.Boolean?)reader["Enabled"];
			entity.UserFullName = (reader.IsDBNull(((int)CompaniesEhsConsultantsColumn.UserFullName)))?null:(System.String)reader[((int)CompaniesEhsConsultantsColumn.UserFullName)];
			//entity.UserFullName = (Convert.IsDBNull(reader["UserFullName"]))?string.Empty:(System.String)reader["UserFullName"];
			entity.UserFullNameLogon = (reader.IsDBNull(((int)CompaniesEhsConsultantsColumn.UserFullNameLogon)))?null:(System.String)reader[((int)CompaniesEhsConsultantsColumn.UserFullNameLogon)];
			//entity.UserFullNameLogon = (Convert.IsDBNull(reader["UserFullNameLogon"]))?string.Empty:(System.String)reader["UserFullNameLogon"];
			entity.Email = (reader.IsDBNull(((int)CompaniesEhsConsultantsColumn.Email)))?null:(System.String)reader[((int)CompaniesEhsConsultantsColumn.Email)];
			//entity.Email = (Convert.IsDBNull(reader["Email"]))?string.Empty:(System.String)reader["Email"];
			entity.PhoneNo = (reader.IsDBNull(((int)CompaniesEhsConsultantsColumn.PhoneNo)))?null:(System.String)reader[((int)CompaniesEhsConsultantsColumn.PhoneNo)];
			//entity.PhoneNo = (Convert.IsDBNull(reader["PhoneNo"]))?string.Empty:(System.String)reader["PhoneNo"];
			reader.Close();
	
			entity.AcceptChanges();
		}
		
		/*
		/// <summary>
		/// Refreshes the <see cref="CompaniesEhsConsultants"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="CompaniesEhsConsultants"/> object.</param>
		protected static void RefreshEntity(DataSet dataSet, CompaniesEhsConsultants entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.CompanyId = (Convert.IsDBNull(dataRow["CompanyId"]))?(int)0:(System.Int32)dataRow["CompanyId"];
			entity.CompanyName = (Convert.IsDBNull(dataRow["CompanyName"]))?string.Empty:(System.String)dataRow["CompanyName"];
			entity.EhsConsultantId = (Convert.IsDBNull(dataRow["EHSConsultantId"]))?(int)0:(System.Int32?)dataRow["EHSConsultantId"];
			entity.UserId = (Convert.IsDBNull(dataRow["UserId"]))?(int)0:(System.Int32?)dataRow["UserId"];
			entity.Enabled = (Convert.IsDBNull(dataRow["Enabled"]))?false:(System.Boolean?)dataRow["Enabled"];
			entity.UserFullName = (Convert.IsDBNull(dataRow["UserFullName"]))?string.Empty:(System.String)dataRow["UserFullName"];
			entity.UserFullNameLogon = (Convert.IsDBNull(dataRow["UserFullNameLogon"]))?string.Empty:(System.String)dataRow["UserFullNameLogon"];
			entity.Email = (Convert.IsDBNull(dataRow["Email"]))?string.Empty:(System.String)dataRow["Email"];
			entity.PhoneNo = (Convert.IsDBNull(dataRow["PhoneNo"]))?string.Empty:(System.String)dataRow["PhoneNo"];
			entity.AcceptChanges();
		}
		*/
			
		#endregion Helper Functions
	}//end class

	#region CompaniesEhsConsultantsFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CompaniesEhsConsultants"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompaniesEhsConsultantsFilterBuilder : SqlFilterBuilder<CompaniesEhsConsultantsColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompaniesEhsConsultantsFilterBuilder class.
		/// </summary>
		public CompaniesEhsConsultantsFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the CompaniesEhsConsultantsFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CompaniesEhsConsultantsFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CompaniesEhsConsultantsFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CompaniesEhsConsultantsFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CompaniesEhsConsultantsFilterBuilder

	#region CompaniesEhsConsultantsParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CompaniesEhsConsultants"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompaniesEhsConsultantsParameterBuilder : ParameterizedSqlFilterBuilder<CompaniesEhsConsultantsColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompaniesEhsConsultantsParameterBuilder class.
		/// </summary>
		public CompaniesEhsConsultantsParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the CompaniesEhsConsultantsParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CompaniesEhsConsultantsParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CompaniesEhsConsultantsParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CompaniesEhsConsultantsParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CompaniesEhsConsultantsParameterBuilder
	
	#region CompaniesEhsConsultantsSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CompaniesEhsConsultants"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class CompaniesEhsConsultantsSortBuilder : SqlSortBuilder<CompaniesEhsConsultantsColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompaniesEhsConsultantsSqlSortBuilder class.
		/// </summary>
		public CompaniesEhsConsultantsSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion CompaniesEhsConsultantsSortBuilder

} // end namespace
