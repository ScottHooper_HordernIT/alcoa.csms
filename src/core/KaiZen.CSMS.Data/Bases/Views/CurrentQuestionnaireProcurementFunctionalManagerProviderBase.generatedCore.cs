﻿#region Using directives

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="CurrentQuestionnaireProcurementFunctionalManagerProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract class CurrentQuestionnaireProcurementFunctionalManagerProviderBaseCore : EntityViewProviderBase<CurrentQuestionnaireProcurementFunctionalManager>
	{
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions
		
		/*
		///<summary>
		/// Fill an VList&lt;CurrentQuestionnaireProcurementFunctionalManager&gt; From a DataSet
		///</summary>
		/// <param name="dataSet">the DataSet</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList&lt;CurrentQuestionnaireProcurementFunctionalManager&gt;"/></returns>
		protected static VList&lt;CurrentQuestionnaireProcurementFunctionalManager&gt; Fill(DataSet dataSet, VList<CurrentQuestionnaireProcurementFunctionalManager> rows, int start, int pagelen)
		{
			if (dataSet.Tables.Count == 1)
			{
				return Fill(dataSet.Tables[0], rows, start, pagelen);
			}
			else
			{
				return new VList<CurrentQuestionnaireProcurementFunctionalManager>();
			}	
		}
		
		
		///<summary>
		/// Fill an VList&lt;CurrentQuestionnaireProcurementFunctionalManager&gt; From a DataTable
		///</summary>
		/// <param name="dataTable">the DataTable that hold the data.</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList<CurrentQuestionnaireProcurementFunctionalManager>"/></returns>
		protected static VList&lt;CurrentQuestionnaireProcurementFunctionalManager&gt; Fill(DataTable dataTable, VList<CurrentQuestionnaireProcurementFunctionalManager> rows, int start, int pagelen)
		{
			int recordnum = 0;
			
			System.Collections.IEnumerator dataRows =  dataTable.Rows.GetEnumerator();
			
			while (dataRows.MoveNext() && (pagelen != 0))
			{
				if(recordnum >= start)
				{
					DataRow row = (DataRow)dataRows.Current;
				
					CurrentQuestionnaireProcurementFunctionalManager c = new CurrentQuestionnaireProcurementFunctionalManager();
					c.UserId = (Convert.IsDBNull(row["UserId"]))?(int)0:(System.Int32?)row["UserId"];
					c.UserFullName = (Convert.IsDBNull(row["UserFullName"]))?string.Empty:(System.String)row["UserFullName"];
					c.AcceptChanges();
					rows.Add(c);
					pagelen -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		*/	
						
		///<summary>
		/// Fill an <see cref="VList&lt;CurrentQuestionnaireProcurementFunctionalManager&gt;"/> From a DataReader.
		///</summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pageLength">number of row.</param>
		///<returns>a <see cref="VList&lt;CurrentQuestionnaireProcurementFunctionalManager&gt;"/></returns>
		protected VList<CurrentQuestionnaireProcurementFunctionalManager> Fill(IDataReader reader, VList<CurrentQuestionnaireProcurementFunctionalManager> rows, int start, int pageLength)
		{
			int recordnum = 0;
			while (reader.Read() && (pageLength != 0))
			{
				if(recordnum >= start)
				{
					CurrentQuestionnaireProcurementFunctionalManager entity = null;
					if (DataRepository.Provider.UseEntityFactory)
					{
						entity = EntityManager.CreateViewEntity<CurrentQuestionnaireProcurementFunctionalManager>("CurrentQuestionnaireProcurementFunctionalManager",  DataRepository.Provider.EntityCreationalFactoryType); 
					}
					else
					{
						entity = new CurrentQuestionnaireProcurementFunctionalManager();
					}
					
					entity.SuppressEntityEvents = true;

					entity.UserId = (reader.IsDBNull(((int)CurrentQuestionnaireProcurementFunctionalManagerColumn.UserId)))?null:(System.Int32?)reader[((int)CurrentQuestionnaireProcurementFunctionalManagerColumn.UserId)];
					//entity.UserId = (Convert.IsDBNull(reader["UserId"]))?(int)0:(System.Int32?)reader["UserId"];
					entity.UserFullName = (System.String)reader[((int)CurrentQuestionnaireProcurementFunctionalManagerColumn.UserFullName)];
					//entity.UserFullName = (Convert.IsDBNull(reader["UserFullName"]))?string.Empty:(System.String)reader["UserFullName"];
					entity.AcceptChanges();
					entity.SuppressEntityEvents = false;
					
					rows.Add(entity);
					pageLength -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		
		
		/// <summary>
		/// Refreshes the <see cref="CurrentQuestionnaireProcurementFunctionalManager"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="CurrentQuestionnaireProcurementFunctionalManager"/> object to refresh.</param>
		protected void RefreshEntity(IDataReader reader, CurrentQuestionnaireProcurementFunctionalManager entity)
		{
			reader.Read();
			entity.UserId = (reader.IsDBNull(((int)CurrentQuestionnaireProcurementFunctionalManagerColumn.UserId)))?null:(System.Int32?)reader[((int)CurrentQuestionnaireProcurementFunctionalManagerColumn.UserId)];
			//entity.UserId = (Convert.IsDBNull(reader["UserId"]))?(int)0:(System.Int32?)reader["UserId"];
			entity.UserFullName = (System.String)reader[((int)CurrentQuestionnaireProcurementFunctionalManagerColumn.UserFullName)];
			//entity.UserFullName = (Convert.IsDBNull(reader["UserFullName"]))?string.Empty:(System.String)reader["UserFullName"];
			reader.Close();
	
			entity.AcceptChanges();
		}
		
		/*
		/// <summary>
		/// Refreshes the <see cref="CurrentQuestionnaireProcurementFunctionalManager"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="CurrentQuestionnaireProcurementFunctionalManager"/> object.</param>
		protected static void RefreshEntity(DataSet dataSet, CurrentQuestionnaireProcurementFunctionalManager entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.UserId = (Convert.IsDBNull(dataRow["UserId"]))?(int)0:(System.Int32?)dataRow["UserId"];
			entity.UserFullName = (Convert.IsDBNull(dataRow["UserFullName"]))?string.Empty:(System.String)dataRow["UserFullName"];
			entity.AcceptChanges();
		}
		*/
			
		#endregion Helper Functions
	}//end class

	#region CurrentQuestionnaireProcurementFunctionalManagerFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CurrentQuestionnaireProcurementFunctionalManager"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CurrentQuestionnaireProcurementFunctionalManagerFilterBuilder : SqlFilterBuilder<CurrentQuestionnaireProcurementFunctionalManagerColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CurrentQuestionnaireProcurementFunctionalManagerFilterBuilder class.
		/// </summary>
		public CurrentQuestionnaireProcurementFunctionalManagerFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the CurrentQuestionnaireProcurementFunctionalManagerFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CurrentQuestionnaireProcurementFunctionalManagerFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CurrentQuestionnaireProcurementFunctionalManagerFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CurrentQuestionnaireProcurementFunctionalManagerFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CurrentQuestionnaireProcurementFunctionalManagerFilterBuilder

	#region CurrentQuestionnaireProcurementFunctionalManagerParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CurrentQuestionnaireProcurementFunctionalManager"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CurrentQuestionnaireProcurementFunctionalManagerParameterBuilder : ParameterizedSqlFilterBuilder<CurrentQuestionnaireProcurementFunctionalManagerColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CurrentQuestionnaireProcurementFunctionalManagerParameterBuilder class.
		/// </summary>
		public CurrentQuestionnaireProcurementFunctionalManagerParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the CurrentQuestionnaireProcurementFunctionalManagerParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CurrentQuestionnaireProcurementFunctionalManagerParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CurrentQuestionnaireProcurementFunctionalManagerParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CurrentQuestionnaireProcurementFunctionalManagerParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CurrentQuestionnaireProcurementFunctionalManagerParameterBuilder
	
	#region CurrentQuestionnaireProcurementFunctionalManagerSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CurrentQuestionnaireProcurementFunctionalManager"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class CurrentQuestionnaireProcurementFunctionalManagerSortBuilder : SqlSortBuilder<CurrentQuestionnaireProcurementFunctionalManagerColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CurrentQuestionnaireProcurementFunctionalManagerSqlSortBuilder class.
		/// </summary>
		public CurrentQuestionnaireProcurementFunctionalManagerSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion CurrentQuestionnaireProcurementFunctionalManagerSortBuilder

} // end namespace
