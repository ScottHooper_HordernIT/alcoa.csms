﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.UsersProcurementAuditProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(UsersProcurementAuditDataSourceDesigner))]
	public class UsersProcurementAuditDataSource : ProviderDataSource<UsersProcurementAudit, UsersProcurementAuditKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UsersProcurementAuditDataSource class.
		/// </summary>
		public UsersProcurementAuditDataSource() : base(new UsersProcurementAuditService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the UsersProcurementAuditDataSourceView used by the UsersProcurementAuditDataSource.
		/// </summary>
		protected UsersProcurementAuditDataSourceView UsersProcurementAuditView
		{
			get { return ( View as UsersProcurementAuditDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the UsersProcurementAuditDataSource control invokes to retrieve data.
		/// </summary>
		public UsersProcurementAuditSelectMethod SelectMethod
		{
			get
			{
				UsersProcurementAuditSelectMethod selectMethod = UsersProcurementAuditSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (UsersProcurementAuditSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the UsersProcurementAuditDataSourceView class that is to be
		/// used by the UsersProcurementAuditDataSource.
		/// </summary>
		/// <returns>An instance of the UsersProcurementAuditDataSourceView class.</returns>
		protected override BaseDataSourceView<UsersProcurementAudit, UsersProcurementAuditKey> GetNewDataSourceView()
		{
			return new UsersProcurementAuditDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the UsersProcurementAuditDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class UsersProcurementAuditDataSourceView : ProviderDataSourceView<UsersProcurementAudit, UsersProcurementAuditKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UsersProcurementAuditDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the UsersProcurementAuditDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public UsersProcurementAuditDataSourceView(UsersProcurementAuditDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal UsersProcurementAuditDataSource UsersProcurementAuditOwner
		{
			get { return Owner as UsersProcurementAuditDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal UsersProcurementAuditSelectMethod SelectMethod
		{
			get { return UsersProcurementAuditOwner.SelectMethod; }
			set { UsersProcurementAuditOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal UsersProcurementAuditService UsersProcurementAuditProvider
		{
			get { return Provider as UsersProcurementAuditService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<UsersProcurementAudit> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<UsersProcurementAudit> results = null;
			UsersProcurementAudit item;
			count = 0;
			
			System.Int32 _auditId;

			switch ( SelectMethod )
			{
				case UsersProcurementAuditSelectMethod.Get:
					UsersProcurementAuditKey entityKey  = new UsersProcurementAuditKey();
					entityKey.Load(values);
					item = UsersProcurementAuditProvider.Get(entityKey);
					results = new TList<UsersProcurementAudit>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case UsersProcurementAuditSelectMethod.GetAll:
                    results = UsersProcurementAuditProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case UsersProcurementAuditSelectMethod.GetPaged:
					results = UsersProcurementAuditProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case UsersProcurementAuditSelectMethod.Find:
					if ( FilterParameters != null )
						results = UsersProcurementAuditProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = UsersProcurementAuditProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case UsersProcurementAuditSelectMethod.GetByAuditId:
					_auditId = ( values["AuditId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["AuditId"], typeof(System.Int32)) : (int)0;
					item = UsersProcurementAuditProvider.GetByAuditId(_auditId);
					results = new TList<UsersProcurementAudit>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				// FK
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == UsersProcurementAuditSelectMethod.Get || SelectMethod == UsersProcurementAuditSelectMethod.GetByAuditId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				UsersProcurementAudit entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					UsersProcurementAuditProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<UsersProcurementAudit> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			UsersProcurementAuditProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region UsersProcurementAuditDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the UsersProcurementAuditDataSource class.
	/// </summary>
	public class UsersProcurementAuditDataSourceDesigner : ProviderDataSourceDesigner<UsersProcurementAudit, UsersProcurementAuditKey>
	{
		/// <summary>
		/// Initializes a new instance of the UsersProcurementAuditDataSourceDesigner class.
		/// </summary>
		public UsersProcurementAuditDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public UsersProcurementAuditSelectMethod SelectMethod
		{
			get { return ((UsersProcurementAuditDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new UsersProcurementAuditDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region UsersProcurementAuditDataSourceActionList

	/// <summary>
	/// Supports the UsersProcurementAuditDataSourceDesigner class.
	/// </summary>
	internal class UsersProcurementAuditDataSourceActionList : DesignerActionList
	{
		private UsersProcurementAuditDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the UsersProcurementAuditDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public UsersProcurementAuditDataSourceActionList(UsersProcurementAuditDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public UsersProcurementAuditSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion UsersProcurementAuditDataSourceActionList
	
	#endregion UsersProcurementAuditDataSourceDesigner
	
	#region UsersProcurementAuditSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the UsersProcurementAuditDataSource.SelectMethod property.
	/// </summary>
	public enum UsersProcurementAuditSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByAuditId method.
		/// </summary>
		GetByAuditId
	}
	
	#endregion UsersProcurementAuditSelectMethod

	#region UsersProcurementAuditFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="UsersProcurementAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UsersProcurementAuditFilter : SqlFilter<UsersProcurementAuditColumn>
	{
	}
	
	#endregion UsersProcurementAuditFilter

	#region UsersProcurementAuditExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="UsersProcurementAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UsersProcurementAuditExpressionBuilder : SqlExpressionBuilder<UsersProcurementAuditColumn>
	{
	}
	
	#endregion UsersProcurementAuditExpressionBuilder	

	#region UsersProcurementAuditProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;UsersProcurementAuditChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="UsersProcurementAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UsersProcurementAuditProperty : ChildEntityProperty<UsersProcurementAuditChildEntityTypes>
	{
	}
	
	#endregion UsersProcurementAuditProperty
}

