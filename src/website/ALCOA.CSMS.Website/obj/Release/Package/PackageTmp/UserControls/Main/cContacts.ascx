﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="UserControls_cContacts" Codebehind="cContacts.ascx.cs" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxRoundPanel" TagPrefix="dxrp" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dxp" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Src="../Other/ExportButtons.ascx" TagName="ExportButtons" TagPrefix="uc1" %>
<table border="0" cellpadding="2" cellspacing="0" width="100%">
    <tr>
        <td class="pageName" colspan="3" style="height: 43px">
            <span class="bodycopy"><span class="title">Contractor Contacts
                <asp:Button ID="btnEdit" runat="server" OnClick="Button2_Click" Text="(Edit/New/Delete)"
                    BackColor="White" BorderColor="White" BorderStyle="None" Font-Names="Verdana"
                    Font-Size="XX-Small" Font-Underline="True" ForeColor="Red" ToolTip="Click me to switch between modification and viewing modes of the contacts list." /></span><br>
                <span class="date">
                    <asp:Label ID="lblRestriction" runat="server" Text="Alcoa requires that you have at minimum added your '<b>KPI Administration Contact</b>', '<b>Contract Manager</b>', '<b>Principal</b>' and '<b>Operation Manager</b>' contacts to the list below."></asp:Label>
                    &nbsp;</span>
                <br />
                <img src="images/grfc_dottedline.gif" width="24" height="1">&nbsp;</span>
        </td>
    </tr>
    <tr>
        <td colspan="3" style="text-align: center">
        <div align="center">
            <dxrp:ASPxRoundPanel ID="ASPxRoundPanel1" runat="server" BackColor="#DDECFE" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                CssPostfix="Office2003Blue" EnableDefaultAppearance="False" HeaderText="Confirm validity of current company details"
                Visible="False" Width="650px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                <TopEdge>
                    <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpTopEdge.png" Repeat="RepeatX"
                        VerticalPosition="top" HorizontalPosition="left" />
                </TopEdge>
                <BottomRightCorner Height="9px" Width="9px" />
                <ContentPaddings Padding="2px" PaddingBottom="10px" PaddingTop="10px" />
                <NoHeaderTopRightCorner Height="9px" Width="9px" />
                <HeaderRightEdge>
                    <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" Repeat="RepeatX"
                        VerticalPosition="top" HorizontalPosition="left" />
                </HeaderRightEdge>
                <Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px" />
                <HeaderLeftEdge>
                    <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" Repeat="RepeatX"
                        VerticalPosition="top" HorizontalPosition="left" />
                </HeaderLeftEdge>
                <HeaderStyle BackColor="#7BA4E0" HorizontalAlign="Center">
                    <BorderBottom BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px" />
                    <Paddings Padding="0px" PaddingBottom="7px" PaddingLeft="2px" PaddingRight="2px" />
                </HeaderStyle>
                <TopRightCorner Height="9px" Width="9px" />
                <HeaderContent>
                    <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" Repeat="RepeatX"
                        VerticalPosition="top" HorizontalPosition="left" />
                </HeaderContent>
                <NoHeaderTopEdge BackColor="#DDECFE">
                </NoHeaderTopEdge>
                <NoHeaderTopLeftCorner Height="9px" Width="9px" />
                <PanelCollection>
                    <dxp:PanelContent runat="server">
                    <div align="center">
                        <span style="color: red">Alcoa requires you to ensure that your companies details are
                            up-to-date every 3 months.<br />
                            Please verify and if required update your companies details below followed by clicking
                            the below button.<br />
                        </span>
                        <br />
                        
                        <dxe:ASPxButton ID="ASPxButton1" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                            CssPostfix="Office2003Blue" Text="I verify that my companies details are up-to-date"
                            Font-Bold="False" OnClick="ASPxButton1_Click">
                        </dxe:ASPxButton>
                        </div>
                    </dxp:PanelContent>
                </PanelCollection>
                <TopLeftCorner Height="9px" Width="9px" />
                <BottomLeftCorner Height="9px" Width="9px" />
                <DisabledStyle ForeColor="Gray">
                </DisabledStyle>
            </dxrp:ASPxRoundPanel>
            </div>
        </td>
    </tr>
    <tr>
        <td style="width: 346px">
            <dxe:ASPxComboBox ID="ASPxcbCompanies" runat="server" AutoPostBack="True" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                CssPostfix="Office2003Blue" OnSelectedIndexChanged="ASPxcbCompanies_SelectedIndexChanged"
                ValueType="System.Int32" Width="346px" IncrementalFilteringMode="StartsWith" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                </LoadingPanelImage>
                <ButtonStyle Width="13px">
                </ButtonStyle>
            </dxe:ASPxComboBox>
        </td>
        <td style="width: 700px">
            <asp:Button ID="btnShowAll" runat="server" Font-Bold="True" OnClick="Button1_Click"
                Text="Show All" Visible="False" />
            <asp:Label ID="lblReader" runat="server" Visible="False"></asp:Label>
        </td>
        <td style="width: 276px; text-align: right;">
            <a href="javascript:ShowHideCustomizationWindow()"><span style="color: #0000ff; text-decoration: underline">Customize</span></a>
        </td>
    </tr>
    <tr>
        <td colspan="3">
            <asp:Label ID="Label1" runat="server" Visible="False"></asp:Label>
            <dxwgv:ASPxGridView ID="grid" ClientInstanceName="grid" runat="server" AutoGenerateColumns="False"
                CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue"
                DataSourceID="ContactsContractorsDataSource" KeyFieldName="ContactId" Width="900px"
                OnInitNewRow="grid_InitNewRow" OnRowValidating="grid_RowValidating" OnStartRowEditing="grid_StartRowEditing"
                OnRowUpdating="grid_RowUpdating" OnRowDeleting="grid_RowDeleting">
                <Columns>
                    <dxwgv:GridViewCommandColumn Caption="Action" Visible="False" VisibleIndex="0" Width="103px">
                        <EditButton Visible="True">
                        </EditButton>
                        <NewButton Visible="True">
                        </NewButton>
                        <DeleteButton Visible="True">
                        </DeleteButton>
                        <ClearFilterButton Visible="True">
                        </ClearFilterButton>
                    </dxwgv:GridViewCommandColumn>
                    <dxwgv:GridViewDataComboBoxColumn Caption="Company" FieldName="CompanyId" Name="CompanyBox"
                        VisibleIndex="0" GroupIndex="1" SortIndex="0" SortOrder="Ascending">
                        <PropertiesComboBox DataSourceID="CompaniesDataSource" DropDownHeight="150px" TextField="CompanyName"
                            ValueField="CompanyId" ValueType="System.String" IncrementalFilteringMode="StartsWith">
                        </PropertiesComboBox>
                        <EditFormSettings Visible="True" />
                        <Settings SortMode="DisplayText" />
                    </dxwgv:GridViewDataComboBoxColumn>
                    <dxwgv:GridViewDataComboBoxColumn Caption="Site" FieldName="SiteId" UnboundType="String"
                        VisibleIndex="1">
                        <PropertiesComboBox DataSourceID="SitesDataSource" DropDownHeight="150px" TextField="SiteName"
                            ValueField="SiteId" ValueType="System.String" IncrementalFilteringMode="StartsWith">
                        </PropertiesComboBox>
                        <Settings SortMode="DisplayText" />
                    </dxwgv:GridViewDataComboBoxColumn>
                    <dxwgv:GridViewDataComboBoxColumn Caption="Role" FieldName="ContactRole" VisibleIndex="2"
                        SortIndex="1" SortOrder="Ascending">
                        <PropertiesComboBox ValueType="System.String" IncrementalFilteringMode="StartsWith">
                            <Items>
                                <dxe:ListEditItem Text="Principal" Value="Principal"></dxe:ListEditItem>
                                <dxe:ListEditItem Text="C.E.O" Value="C.E.O"></dxe:ListEditItem>
                                <dxe:ListEditItem Text="Contract Manager" Value="Contract Manager"></dxe:ListEditItem>
                                <dxe:ListEditItem Text="KPI Administration Contact" Value="KPI Administration Contact">
                                </dxe:ListEditItem>
                                <dxe:ListEditItem Text="Operation Manager" Value="Operation Manager"></dxe:ListEditItem>
                                <dxe:ListEditItem Text="General Manager" Value="General Manager"></dxe:ListEditItem>
                                <dxe:ListEditItem Text="HR Manager" Value="HR Manager"></dxe:ListEditItem>
                                <dxe:ListEditItem Text="IR Manager" Value="IR Manager"></dxe:ListEditItem>
                                <dxe:ListEditItem Text="Safety Manager" Value="Safety Manager"></dxe:ListEditItem>
                                <dxe:ListEditItem Text="Training Manager" Value="Training Manager"></dxe:ListEditItem>
                                <dxe:ListEditItem Text="Purchasing Manager" Value="Purchasing Manager"></dxe:ListEditItem>
                                <dxe:ListEditItem Text="Senior Site Supervisor" Value="Senior Site Supervisor"></dxe:ListEditItem>
                                <dxe:ListEditItem Text="Site Manager Supervisor" Value="Site Manager Supervisor">
                                </dxe:ListEditItem>
                                <dxe:ListEditItem Text="(Other)" Value="(Other)"></dxe:ListEditItem>
                            </Items>
                        </PropertiesComboBox>
                    </dxwgv:GridViewDataComboBoxColumn>
                    <dxwgv:GridViewDataTextColumn Caption="First Name" FieldName="ContactFirstName" VisibleIndex="3">
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="Last Name" FieldName="ContactLastName" VisibleIndex="4"
                        SortIndex="2" SortOrder="Ascending">
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="Title" FieldName="ContactTitle" VisibleIndex="5">
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataHyperLinkColumn Caption="Email" FieldName="ContactEmail" VisibleIndex="6">
                        <PropertiesHyperLinkEdit NavigateUrlFormatString="Mailto:{0}" TextField="ContactEmail">
                        </PropertiesHyperLinkEdit>
                    </dxwgv:GridViewDataHyperLinkColumn>
                    <dxwgv:GridViewDataTextColumn Caption="Phone #" FieldName="ContactPhone" VisibleIndex="7">
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="Mobile #" FieldName="ContactMobile" VisibleIndex="8">
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="ContactId" FieldName="ContactId" ReadOnly="True"
                        ShowInCustomizationForm="False" Visible="False">
                        <EditFormSettings Visible="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="ModifiedByUserId" FieldName="ModifiedByUserId"
                        ShowInCustomizationForm="False" Visible="False">
                        <EditFormSettings Visible="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="9">
                        <ClearFilterButton Visible="True">
                        </ClearFilterButton>
                        <HeaderTemplate>
                            <input type="checkbox" onclick="grid.SelectAllRowsOnPage(this.checked);" style="vertical-align: middle;"
                                title="Select/Unselect all rows on the page"></input>
                        </HeaderTemplate>
                        <HeaderStyle HorizontalAlign="Center" />
                    </dxwgv:GridViewCommandColumn>
                </Columns>
                <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                    <ExpandedButton Height="12px" Width="11px" />
                    <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
                    </LoadingPanelOnStatusBar>
                    <CollapsedButton Height="12px" Width="11px" />
                    <DetailCollapsedButton Height="12px" Width="11px" />
                    <DetailExpandedButton Height="12px" Width="11px" />
                    <LoadingPanel Url="~/App_Themes/Office2003BlueOld/GridView/Loading.gif"><%--Enhancement_023:change by debashis for testing platform upgradation--%>
                    </LoadingPanel>
                </Images>
                <ImagesFilterControl>
                    <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
                    </LoadingPanel>
                </ImagesFilterControl>
                <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue">
                    <Header ImageSpacing="5px" SortingImageSpacing="5px">
                    </Header>
                    <LoadingPanel ImageSpacing="10px">
                    </LoadingPanel>
                    <AlternatingRow Enabled="True">
                    </AlternatingRow>
                </Styles>
                <SettingsPager>
                    <AllButton Visible="True">
                    </AllButton>
                </SettingsPager>
                <SettingsBehavior ConfirmDelete="True" ColumnResizeMode="NextColumn" />
                <SettingsCookies CookiesID="cContacts" Version="0.1" />
                <SettingsCustomizationWindow Enabled="True" />
                <Settings ShowGroupPanel="True" ShowVerticalScrollBar="True" VerticalScrollableHeight="215" />
            </dxwgv:ASPxGridView>
        </td>
    </tr>
    <tr>
        <td colspan="3" style="height: 35px; text-align: right">
            <asp:Button ID="btnEmailSelected" runat="server" OnClick="btnEmailSelected_Click"
                Text="E-Mail Selected" />
        </td>
    </tr>
    <tr align="right">
        <td colspan="3" style="padding-top: 6px; text-align: right; text-align: -moz-right;
            width: 100%" align="right">
            <div align="right">
                <uc1:ExportButtons ID="ucExportButtons" runat="server" />
            </div>
        </td>
    </tr>
</table>
<td colspan="3" style="height: 35px; text-align: right">
    <asp:ObjectDataSource ID="odsContractorContactsDS_Contractors" runat="server" OldValuesParameterFormatString="original_{0}"
        SelectMethod="GetByCompanyId" TypeName="KaiZen.CSMS.Services.ContactsContractorsService"
        DataObjectTypeName="KaiZen.CSMS.Entities.ContactsContractors" DeleteMethod="Delete"
        InsertMethod="Insert" UpdateMethod="Update">
        <SelectParameters>
            <asp:SessionParameter DefaultValue="0" Name="CompanyId" SessionField="spvar_CompanyId"
                Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <data:ContactsContractorsDataSource ID="odsContractorContactsDS" runat="server" SelectMethod="GetAll"
        EnableSorting="true" EnableDeepLoad="false">
    </data:ContactsContractorsDataSource>
    <asp:ObjectDataSource ID="odsCompaniesDS_Contractor" runat="server" SelectMethod="GetByCompanyId"
        TypeName="KaiZen.CSMS.Services.CompaniesService" OldValuesParameterFormatString="original_{0}">
        <SelectParameters>
            <asp:SessionParameter DefaultValue="0" Name="companyId" SessionField="spvar_CompanyId"
                Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <data:SitesDataSource ID="SitesDataSource" runat="server" EnableDeepLoad="False"
        EnableSorting="true" SelectMethod="GetAll">
    </data:SitesDataSource>
    <data:CompaniesDataSource ID="CompaniesDataSource" runat="server" SelectMethod="GetAll"
        EnableSorting="true" EnableDeepLoad="False">
    </data:CompaniesDataSource>
    <data:ContactsContractorsDataSource ID="ContactsContractorsDataSource" runat="server"
        SelectMethod="GetPaged" EnableSorting="true" EnableDeepLoad="false" Filter="CompanyName NOT LIKE 'Alcoa'">
        <DeepLoadProperties Method="IncludeChildren" Recursive="False">
        </DeepLoadProperties>
    </data:ContactsContractorsDataSource>
    <asp:SqlDataSource ID="sqldsCompaniesList" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
        SelectCommand="_Companies_GetCompanyNameListWithoutInactive" SelectCommandType="StoredProcedure">
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="sqldsResidential_Sites_ByCompany" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
        SelectCommand="_Residential_Sites_ByCompany" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter DefaultValue="0" Name="CompanyId" Type="String" SessionField="spVar_CompanyId" />
        </SelectParameters>
    </asp:SqlDataSource>
