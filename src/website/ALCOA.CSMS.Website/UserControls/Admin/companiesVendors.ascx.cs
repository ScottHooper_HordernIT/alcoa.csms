using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using KaiZen.CSMS.Services;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.Library;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxTabControl;
using DevExpress.Web.Data;
using DevExpress.Web.ASPxGridView;
using model = Repo.CSMS.DAL.EntityModels;
using repo = Repo.CSMS.Service.Database;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;

public partial class Adminv2_UserControls_CompaniesVendors : System.Web.UI.UserControl
{
    Auth auth = new Auth();
    repo.IEmailTemplateService emailTemplateService = ALCOA.CSMS.Website.Global.GetInstance<repo.IEmailTemplateService>();
    repo.IEmailLogService emailLogService = ALCOA.CSMS.Website.Global.GetInstance<repo.IEmailLogService>();
    repo.ISiteService siteService = ALCOA.CSMS.Website.Global.GetInstance<repo.ISiteService>();
    repo.IEHSConsultantService ehsConsultantService = ALCOA.CSMS.Website.Global.GetInstance<repo.IEHSConsultantService>();
    //repo.ICompanyService companyService = ALCOA.CSMS.Website.Global.GetInstance<repo.ICompanyService>();
    repo.IQuestionnaireService questionnaireService = ALCOA.CSMS.Website.Global.GetInstance<repo.IQuestionnaireService>();
    repo.IQuestionnaireInitialResponseService questionnaireInitialResponseService = ALCOA.CSMS.Website.Global.GetInstance<repo.IQuestionnaireInitialResponseService>();
    repo.IUserService userService = ALCOA.CSMS.Website.Global.GetInstance<repo.IUserService>();
    protected void Page_Load(object sender, EventArgs e) {  Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); }

    private void moduleInit(bool postBack)
    {
        if (!postBack)
        {
            if (auth.RoleId != (int)RoleList.Administrator) Response.Redirect("default.aspx");

            // Export
            String exportFileName = @"ALCOA CSMS - Admin - Companies - Vendors"; //Chrome & IE is fine.
            String userAgent = Request.Headers.Get("User-Agent");
            if (
                (userAgent.Contains("Mozilla") && !userAgent.Contains("AppleWebKit") && !userAgent.Contains("MSIE")) || //Mozilla
                (userAgent.Contains("AppleWebKit") && !userAgent.Contains("Chrome"))      //Safari
                )
            {
                exportFileName = exportFileName.Replace(" ", "_");
            }
            Helper.ExportGrid.Settings(companyVendorExportButtons, exportFileName, "grid_Companies_Vendors");
        }
    }

}
