﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.ComponentModel;
using System.Web.UI.Design.WebControls;
using System.Web.UI.Design;
using System.Web.UI.WebControls;

namespace KaiZen.CSMS.Web.UI
{
    /// <summary>
    /// A designer class for a strongly typed repeater <c>TwentyOnePointAudit06Repeater</c>
    /// </summary>
	public class TwentyOnePointAudit06RepeaterDesigner : System.Web.UI.Design.ControlDesigner
	{
	    /// <summary>
        /// Initializes a new instance of the <see cref="T:TwentyOnePointAudit06RepeaterDesigner"/> class.
        /// </summary>
		public TwentyOnePointAudit06RepeaterDesigner()
		{
		}

        /// <summary>
        /// Initializes the control designer and loads the specified component.
        /// </summary>
        /// <param name="component">The control being designed.</param>
		public override void Initialize(IComponent component)
		{
			if (!(component is TwentyOnePointAudit06Repeater))
			{ 
				throw new ArgumentException("Component is not a TwentyOnePointAudit06Repeater."); 
			} 
			base.Initialize(component); 
			base.SetViewFlags(ViewFlags.TemplateEditing, true); 
		}


		/// <summary>
		/// Generate HTML for the designer
		/// </summary>
		/// <returns>a string of design time HTML</returns>
		public override string GetDesignTimeHtml()
		{

			// Get the instance this designer applies to
			//
			TwentyOnePointAudit06Repeater z = (TwentyOnePointAudit06Repeater)Component;
			z.DataBind();

			return base.GetDesignTimeHtml();

			//return z.RenderAtDesignTime();

			//	ControlCollection c = z.Controls;
			//Totem z = (Totem) Component;
			//Totem z = (Totem) Component;
			//return ("<div style='border: 1px gray dotted; background-color: lightgray'><b>TagStat :</b> zae |  qsdds</div>");

		}
	}

    /// <summary>
    /// A strongly typed repeater control for the <see cref="TwentyOnePointAudit06Repeater"/> Type.
    /// </summary>
	[Designer(typeof(TwentyOnePointAudit06RepeaterDesigner))]
	[ParseChildren(true)]
	[ToolboxData("<{0}:TwentyOnePointAudit06Repeater runat=\"server\"></{0}:TwentyOnePointAudit06Repeater>")]
	public class TwentyOnePointAudit06Repeater : CompositeDataBoundControl, System.Web.UI.INamingContainer
	{
	    /// <summary>
        /// Initializes a new instance of the <see cref="T:TwentyOnePointAudit06Repeater"/> class.
        /// </summary>
		public TwentyOnePointAudit06Repeater()
		{
		}

		/// <summary>
        /// Gets a <see cref="T:System.Web.UI.ControlCollection"></see> object that represents the child controls for a specified server control in the UI hierarchy.
        /// </summary>
        /// <value></value>
        /// <returns>The collection of child controls for the specified server control.</returns>
		public override ControlCollection Controls
		{
			get
			{
				this.EnsureChildControls();
				return base.Controls;
			}
		}

		private ITemplate m_headerTemplate;
		/// <summary>
        /// Gets or sets the header template.
        /// </summary>
        /// <value>The header template.</value>
		[Browsable(false)]
		[TemplateContainer(typeof(TwentyOnePointAudit06Item))]
		[PersistenceMode(PersistenceMode.InnerDefaultProperty)]
		public ITemplate HeaderTemplate
		{
			get { return m_headerTemplate; }
			set { m_headerTemplate = value; }
		}

		private ITemplate m_itemTemplate;
		/// <summary>
        /// Gets or sets the item template.
        /// </summary>
        /// <value>The item template.</value>
		[Browsable(false)]
		[TemplateContainer(typeof(TwentyOnePointAudit06Item))]
		[PersistenceMode(PersistenceMode.InnerDefaultProperty)]
		public ITemplate ItemTemplate
		{
			get { return m_itemTemplate; }
			set { m_itemTemplate = value; }
		}

		private ITemplate m_seperatorTemplate;
        /// <summary>
        /// Gets or sets the Seperator Template
        /// </summary>
        [Browsable(false)]
        [TemplateContainer(typeof(TwentyOnePointAudit06Item))]
        [PersistenceMode(PersistenceMode.InnerDefaultProperty)]
        public ITemplate SeperatorTemplate
        {
            get { return m_seperatorTemplate; }
            set { m_seperatorTemplate = value; }
        }
			
		private ITemplate m_altenateItemTemplate;
        /// <summary>
        /// Gets or sets the alternating item template.
        /// </summary>
        /// <value>The alternating item template.</value>
		[Browsable(false)]
		[TemplateContainer(typeof(TwentyOnePointAudit06Item))]
		[PersistenceMode(PersistenceMode.InnerDefaultProperty)]
		public ITemplate AlternatingItemTemplate
		{
			get { return m_altenateItemTemplate; }
			set { m_altenateItemTemplate = value; }
		}

		private ITemplate m_footerTemplate;
        /// <summary>
        /// Gets or sets the footer template.
        /// </summary>
        /// <value>The footer template.</value>
		[Browsable(false)]
		[TemplateContainer(typeof(TwentyOnePointAudit06Item))]
		[PersistenceMode(PersistenceMode.InnerDefaultProperty)]
		public ITemplate FooterTemplate
		{
			get { return m_footerTemplate; }
			set { m_footerTemplate = value; }
		}

//      /// <summary>
//      /// Called by the ASP.NET page framework to notify server controls that use composition-based implementation to create any child controls they contain in preparation for posting back or rendering.
//      /// </summary>
//		protected override void CreateChildControls()
//      {
//         if (ChildControlsCreated)
//         {
//            return;
//         }

//         Controls.Clear();

//         //Instantiate the Header template (if exists)
//         if (m_headerTemplate != null)
//         {
//            Control headerItem = new Control();
//            m_headerTemplate.InstantiateIn(headerItem);
//            Controls.Add(headerItem);
//         }

//         //Instantiate the Footer template (if exists)
//         if (m_footerTemplate != null)
//         {
//            Control footerItem = new Control();
//            m_footerTemplate.InstantiateIn(footerItem);
//            Controls.Add(footerItem);
//         }
//
//         ChildControlsCreated = true;
//      }
	
		/// <summary>
        /// Overridden and Empty so that span tags are not written
        /// </summary>
        /// <param name="writer"></param>
        public override void RenderBeginTag(HtmlTextWriter writer)
        {
            
        }

        /// <summary>
        /// Overridden and Empty so that span tags are not written
        /// </summary>
        /// <param name="writer"></param>
        public override void RenderEndTag(HtmlTextWriter writer)
        {
                
        }		
		
		/// <summary>
      	/// Called by the ASP.NET page framework to notify server controls that use composition-based implementation to create any child controls they contain in preparation for posting back or rendering.
      	/// </summary>
		protected override int CreateChildControls(System.Collections.IEnumerable dataSource, bool dataBinding)
      	{
         int pos = 0;

         if (dataBinding)
         {
            //Instantiate the Header template (if exists)
            if (m_headerTemplate != null)
            {
                Control headerItem = new Control();
                m_headerTemplate.InstantiateIn(headerItem);
                Controls.Add(headerItem);
            }
			if (dataSource != null)
			{
				foreach (object o in dataSource)
				{
						KaiZen.CSMS.Entities.TwentyOnePointAudit06 entity = o as KaiZen.CSMS.Entities.TwentyOnePointAudit06;
						TwentyOnePointAudit06Item container = new TwentyOnePointAudit06Item(entity);
	
						if (m_itemTemplate != null && (pos % 2) == 0)
						{
							m_itemTemplate.InstantiateIn(container);
							
							if (m_seperatorTemplate != null)
							{
								m_seperatorTemplate.InstantiateIn(container);
							}
						}
						else
						{
							if (m_altenateItemTemplate != null)
							{
								m_altenateItemTemplate.InstantiateIn(container);
								
								if (m_seperatorTemplate != null)
								{
									m_seperatorTemplate.InstantiateIn(container);
								}
								
							}
							else if (m_itemTemplate != null)
							{
								m_itemTemplate.InstantiateIn(container);
								
								if (m_seperatorTemplate != null)
								{
									m_seperatorTemplate.InstantiateIn(container);
								}
							}
							else
							{
								// no template !!!
							}
						}
						Controls.Add(container);
						
						container.DataBind();
						
						pos++;
				}
			}
            //Instantiate the Footer template (if exists)
            if (m_footerTemplate != null)
            {
                Control footerItem = new Control();
                m_footerTemplate.InstantiateIn(footerItem);
                Controls.Add(footerItem);
            }

		}
			
			return pos;
		}

        /// <summary>
        /// Raises the <see cref="E:System.Web.UI.Control.PreRender"></see> event.
        /// </summary>
        /// <param name="e">An <see cref="T:System.EventArgs"></see> object that contains the event data.</param>
		protected override void OnPreRender(EventArgs e)
		{
			base.DataBind();
		}

		#region Design time
        /// <summary>
        /// Renders at design time.
        /// </summary>
        /// <returns>a  string of the Designed HTML</returns>
		internal string RenderAtDesignTime()
		{			
			return "Designer currently not implemented"; 
		}

		#endregion
	}

    /// <summary>
    /// A wrapper type for the entity
    /// </summary>
	[System.ComponentModel.ToolboxItem(false)]
	public class TwentyOnePointAudit06Item : System.Web.UI.Control, System.Web.UI.INamingContainer
	{
		private KaiZen.CSMS.Entities.TwentyOnePointAudit06 _entity;

        /// <summary>
        /// Initializes a new instance of the <see cref="T:TwentyOnePointAudit06Item"/> class.
        /// </summary>
		public TwentyOnePointAudit06Item()
			: base()
		{ }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:TwentyOnePointAudit06Item"/> class.
        /// </summary>
		public TwentyOnePointAudit06Item(KaiZen.CSMS.Entities.TwentyOnePointAudit06 entity)
			: base()
		{
			_entity = entity;
		}
		
        /// <summary>
        /// Gets the Id
        /// </summary>
        /// <value>The Id.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32 Id
		{
			get { return _entity.Id; }
		}
        /// <summary>
        /// Gets the Achieved6a
        /// </summary>
        /// <value>The Achieved6a.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved6a
		{
			get { return _entity.Achieved6a; }
		}
        /// <summary>
        /// Gets the Achieved6b
        /// </summary>
        /// <value>The Achieved6b.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved6b
		{
			get { return _entity.Achieved6b; }
		}
        /// <summary>
        /// Gets the Achieved6c
        /// </summary>
        /// <value>The Achieved6c.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved6c
		{
			get { return _entity.Achieved6c; }
		}
        /// <summary>
        /// Gets the Achieved6d
        /// </summary>
        /// <value>The Achieved6d.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved6d
		{
			get { return _entity.Achieved6d; }
		}
        /// <summary>
        /// Gets the Achieved6e
        /// </summary>
        /// <value>The Achieved6e.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved6e
		{
			get { return _entity.Achieved6e; }
		}
        /// <summary>
        /// Gets the Achieved6f
        /// </summary>
        /// <value>The Achieved6f.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved6f
		{
			get { return _entity.Achieved6f; }
		}
        /// <summary>
        /// Gets the Achieved6g
        /// </summary>
        /// <value>The Achieved6g.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved6g
		{
			get { return _entity.Achieved6g; }
		}
        /// <summary>
        /// Gets the Achieved6h
        /// </summary>
        /// <value>The Achieved6h.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved6h
		{
			get { return _entity.Achieved6h; }
		}
        /// <summary>
        /// Gets the Achieved6i
        /// </summary>
        /// <value>The Achieved6i.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved6i
		{
			get { return _entity.Achieved6i; }
		}
        /// <summary>
        /// Gets the Achieved6j
        /// </summary>
        /// <value>The Achieved6j.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved6j
		{
			get { return _entity.Achieved6j; }
		}
        /// <summary>
        /// Gets the Achieved6k
        /// </summary>
        /// <value>The Achieved6k.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved6k
		{
			get { return _entity.Achieved6k; }
		}
        /// <summary>
        /// Gets the Achieved6l
        /// </summary>
        /// <value>The Achieved6l.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved6l
		{
			get { return _entity.Achieved6l; }
		}
        /// <summary>
        /// Gets the Observation6a
        /// </summary>
        /// <value>The Observation6a.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation6a
		{
			get { return _entity.Observation6a; }
		}
        /// <summary>
        /// Gets the Observation6b
        /// </summary>
        /// <value>The Observation6b.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation6b
		{
			get { return _entity.Observation6b; }
		}
        /// <summary>
        /// Gets the Observation6c
        /// </summary>
        /// <value>The Observation6c.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation6c
		{
			get { return _entity.Observation6c; }
		}
        /// <summary>
        /// Gets the Observation6d
        /// </summary>
        /// <value>The Observation6d.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation6d
		{
			get { return _entity.Observation6d; }
		}
        /// <summary>
        /// Gets the Observation6e
        /// </summary>
        /// <value>The Observation6e.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation6e
		{
			get { return _entity.Observation6e; }
		}
        /// <summary>
        /// Gets the Observation6f
        /// </summary>
        /// <value>The Observation6f.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation6f
		{
			get { return _entity.Observation6f; }
		}
        /// <summary>
        /// Gets the Observation6g
        /// </summary>
        /// <value>The Observation6g.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation6g
		{
			get { return _entity.Observation6g; }
		}
        /// <summary>
        /// Gets the Observation6h
        /// </summary>
        /// <value>The Observation6h.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation6h
		{
			get { return _entity.Observation6h; }
		}
        /// <summary>
        /// Gets the Observation6i
        /// </summary>
        /// <value>The Observation6i.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation6i
		{
			get { return _entity.Observation6i; }
		}
        /// <summary>
        /// Gets the Observation6j
        /// </summary>
        /// <value>The Observation6j.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation6j
		{
			get { return _entity.Observation6j; }
		}
        /// <summary>
        /// Gets the Observation6k
        /// </summary>
        /// <value>The Observation6k.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation6k
		{
			get { return _entity.Observation6k; }
		}
        /// <summary>
        /// Gets the Observation6l
        /// </summary>
        /// <value>The Observation6l.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation6l
		{
			get { return _entity.Observation6l; }
		}
        /// <summary>
        /// Gets the TotalScore
        /// </summary>
        /// <value>The TotalScore.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? TotalScore
		{
			get { return _entity.TotalScore; }
		}

        /// <summary>
        /// Gets a <see cref="T:KaiZen.CSMS.Entities.TwentyOnePointAudit06"></see> object
        /// </summary>
        /// <value></value>
        [System.ComponentModel.Bindable(true)]
        public KaiZen.CSMS.Entities.TwentyOnePointAudit06 Entity
        {
            get { return _entity; }
        }
	}
}
