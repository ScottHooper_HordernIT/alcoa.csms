using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using KaiZen.CSMS.Services;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

public partial class Adminv2_UserControls_Config : System.Web.UI.UserControl
{
    Auth auth = new Auth();

    protected void Page_Load(object sender, EventArgs e) {  Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); }

    private void moduleInit(bool postBack)
    {
        if (!postBack)
        { //first time load
            if (auth.RoleId != (int)RoleList.Administrator)
            {
                Response.Redirect("default.aspx");
            }
            else
            {
                Configuration config = new Configuration();
                if (config.GetValue(ConfigList.EscalationChainDaysWithProcurement).ToString() != "-1")
                    ASPxCheckBox1.Checked = true;
                if (config.GetValue(ConfigList.EscalationChainDaysWithSupplier).ToString() != "-1")
                    ASPxCheckBox2.Checked = true;
                if (config.GetValue(ConfigList.EscalationChainDaysWithHsAssessor).ToString() != "-1")
                    ASPxCheckBox3.Checked = true;
            }
        }
    }
    protected void grid_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        e.NewValues["Key"] = e.OldValues["Key"];
    }

    protected void ASPxButton1_Click(object sender, EventArgs e)
    {
        Configuration config = new Configuration();
        ConfigService cService = new ConfigService();
        if (ASPxCheckBox1.Checked)
        {
            if (config.GetValue(ConfigList.EscalationChainDaysWithProcurement).ToString() == "-1")
            {
                Config c = cService.GetByKey("EscalationChainDaysWithProcurement");
                c.Value = "7";
                cService.Save(c);
            }
        }
        else
        {
            if (config.GetValue(ConfigList.EscalationChainDaysWithProcurement).ToString() != "-1")
            {
                Config c = cService.GetByKey("EscalationChainDaysWithProcurement");
                c.Value = "-1";
                cService.Save(c);
            }
        }

        if (ASPxCheckBox2.Checked)
        {
            if (config.GetValue(ConfigList.EscalationChainDaysWithProcurement).ToString() == "-1")
            {
                Config c = cService.GetByKey("EscalationChainDaysWithSupplier");
                c.Value = "15";
                cService.Save(c);
            }
        }
        else
        {
            if (config.GetValue(ConfigList.EscalationChainDaysWithProcurement).ToString() != "-1")
            {
                Config c = cService.GetByKey("EscalationChainDaysWithSupplier");
                c.Value = "-1";
                cService.Save(c);
            }
        }

        if (ASPxCheckBox3.Checked)
        {
            if (config.GetValue(ConfigList.EscalationChainDaysWithProcurement).ToString() == "-1")
            {
                Config c = cService.GetByKey("EscalationChainDaysWithHsAssessor");
                c.Value = "7";
                cService.Save(c);
            }
        }
        else
        {
            if (config.GetValue(ConfigList.EscalationChainDaysWithProcurement).ToString() != "-1")
            {
                Config c = cService.GetByKey("EscalationChainDaysWithHsAssessor");
                c.Value = "-1";
                cService.Save(c);
            }
        }
    }

}
