﻿#region Using directives

using System;

#endregion

namespace KaiZen.CSMS.Entities
{	
	///<summary>
	/// An object representation of the 'FileVaultTable_List' view. [No description found in the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class FileVaultTableList : FileVaultTableListBase
	{
		#region Constructors

		///<summary>
		/// Creates a new <see cref="FileVaultTableList"/> instance.
		///</summary>
		public FileVaultTableList():base(){}	
		
		#endregion
	}
}
