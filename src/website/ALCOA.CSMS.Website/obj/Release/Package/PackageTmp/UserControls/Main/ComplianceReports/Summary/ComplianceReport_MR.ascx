﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="UserControls_Tiny_ComplianceReports_ComplianceReport_MR" Codebehind="ComplianceReport_MR.ascx.cs" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
    <dxwgv:ASPxGridView ID="grid" runat="server" AutoGenerateColumns="False" ClientInstanceName="grid"
                CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
    CssPostfix="Office2003Blue" OnRowInserting="grid_OnRowInserting" OnHtmlRowCreated="grid_RowCreated"
                OnInitNewRow="grid_InitNewRow" 
    OnRowUpdating="grid_RowUpdating" DataSourceID="DocumentsDataSource" 
    KeyFieldName="DocumentId">
                <Columns>
                <dxwgv:GridViewCommandColumn Caption="Action" Visible="False" VisibleIndex="0" Width="50px">
                            <EditButton Visible="True">
                            </EditButton>
                            <NewButton Visible="True">
                            </NewButton>
                            <DeleteButton Visible="True">
                            </DeleteButton>
                            <ClearFilterButton Visible="True">
                            </ClearFilterButton>
                        </dxwgv:GridViewCommandColumn>
                        <dxwgv:GridViewDataTextColumn FieldName="DocumentId" ReadOnly="True" Visible="False"
                            VisibleIndex="0">
                            <EditFormSettings Visible="False" />
                            <CellStyle>
                                <BackgroundImage ImageUrl="~/Images/ArticleIcon.gif" Repeat="NoRepeat" />
                            </CellStyle>
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn ReadOnly="True" VisibleIndex="0" Width="8px">
                            <EditFormSettings Visible="False" />
                            <CellStyle>
                                <Paddings Padding="5px" />
                                <BackgroundImage ImageUrl="~/Images/ArticleIcon.gif" Repeat="NoRepeat" />
                            </CellStyle>
                        </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn FieldName="DocumentName" Caption="Document Name" VisibleIndex="2" Width="700px" SortIndex="0" SortOrder="Ascending">
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn FieldName="DocumentFileName" Caption="File Name" VisibleIndex="1" Width="100px">
                        </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn FieldName="DocumentFileDescription" Visible="False" VisibleIndex="3">
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn FieldName="DocumentType" Visible="False" VisibleIndex="3">
                        <EditFormSettings Visible="False" />
                    </dxwgv:GridViewDataTextColumn>
                     <dxwgv:GridViewDataTextColumn Name="Read" Caption="Read" FieldName="DocumentFileName" ReadOnly="True"
                 VisibleIndex="3" Width="120px">
                 <DataItemTemplate>
                     <table cellpadding="0" cellspacing="0"><tr>
                     <td style="width:120px"><asp:Label ID="lblRead" runat="server" Text="" ></asp:Label></td>
                     </tr></table>
                 </DataItemTemplate>
                 <HeaderStyle HorizontalAlign="Center" />
                        <CellStyle HorizontalAlign="Center">
                        </CellStyle>
             </dxwgv:GridViewDataTextColumn>
             <dxwgv:GridViewDataComboBoxColumn Caption="Relevant To" FieldName="RegionId" VisibleIndex="4" Width="220px" SortIndex="0" SortOrder="Ascending">
                        <PropertiesComboBox DataSourceID="RegionsDataSource" DropDownHeight="150px" TextField="RegionName"
                            ValueField="RegionId" ValueType="System.Int32">
                        </PropertiesComboBox>
                 <EditFormSettings Visible="False" />
            </dxwgv:GridViewDataComboBoxColumn>
                </Columns>
                <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                    <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
                    </LoadingPanelOnStatusBar>
                    <LoadingPanel Url="~/App_Themes/Office2003Blue/GridView/Loading.gif">
                    </LoadingPanel>
                </Images>
                <ImagesFilterControl>
                    <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
                    </LoadingPanel>
                </ImagesFilterControl>
                <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
                    CssPostfix="Office2003Blue">
                    <Header ImageSpacing="5px" SortingImageSpacing="5px">
                    </Header>
                    <LoadingPanel ImageSpacing="10px">
                    </LoadingPanel>
                </Styles>
                <SettingsEditing Mode="Inline" />
                <SettingsBehavior ConfirmDelete="True" />
        <SettingsPager Mode="ShowAllRecords">
        </SettingsPager>
                <StylesEditors>
                    <ProgressBar Height="25px">
                    </ProgressBar>
                </StylesEditors>
            </dxwgv:ASPxGridView>
            
            
            <data:RegionsDataSource
                    ID="RegionsDataSource"
                    runat="server"
                    EnableDeepLoad="False"
                    EnableSorting="True"
                    SelectMethod="GetPaged">
                </data:RegionsDataSource>
                
            <data:DocumentsDataSource
                    ID="DocumentsDataSource"
                    runat="server"
                    EnableDeepLoad="False"
                    EnableSorting="true"
                    SelectMethod="GetPaged">
                    <Parameters>
                      <data:SqlParameter Name="WhereClause" UseParameterizedFilters="false">
                         <Filters>
                            <data:DocumentsFilter Column="DocumentType" DefaultValue="MR" SessionField="MR" />
                            <data:DocumentsFilter Column="RegionId" DefaultValue="" SessionField="Documents_Region" />
                         </Filters>
                      </data:SqlParameter>
                   </Parameters>
                </data:DocumentsDataSource>
                
                <asp:SqlDataSource ID="sqldsReadCD" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
                   SelectCommand="_DocumentsDownloadLog_ComplianceReport_ByCompany" SelectCommandType="StoredProcedure">
    <SelectParameters>
        <asp:SessionParameter DefaultValue="0" Name="CompanyId" SessionField="spvar_CompanyId" Type="Int32" />
    </SelectParameters>
</asp:SqlDataSource>
