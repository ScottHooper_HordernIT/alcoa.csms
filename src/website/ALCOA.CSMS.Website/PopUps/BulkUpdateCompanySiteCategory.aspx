﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/popup.master" AutoEventWireup="true" CodeBehind="BulkUpdateCompanySiteCategory.aspx.cs" Inherits="ALCOA.CSMS.Website.PopUps.BulkUpdateCompanySiteCategory" %>

<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxUploadControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<dx:ASPxUploadControl ID="uc" runat="server" ShowProgressPanel="True" 
        CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
        CssPostfix="Office2003Blue">
            <ValidationSettings  
                AllowedFileExtensions=".csv"
                GeneralErrorText="File Upload Failed."
                MaxFileSize="26252928" 
                MaxFileSizeErrorText="File size exceeds the maximum allowed size of 25Mb "
                NotAllowedFileExtensionErrorText="File NOT uploaded. Please upload only files of the following file type. All Other file types will not be uploaded: CSV">
            </ValidationSettings>
        </dx:ASPxUploadControl>

        <p>
            <dx:ASPxButton ID="btnPreview" runat="server" OnClick="btnPreview_Click" Text="Preview">
            </dx:ASPxButton>
        </p>
        <p>
            <dx:ASPxButton ID="btnUpload" runat="server" Text="Upload" OnClick="btnUpload_Click">
            </dx:ASPxButton>
            <br />
        </p>
        <dx:ASPxGridView ID="ASPxGridView1" runat="server">
        </dx:ASPxGridView>

</asp:Content>
