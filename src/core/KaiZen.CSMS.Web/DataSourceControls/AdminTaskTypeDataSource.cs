﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.AdminTaskTypeProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(AdminTaskTypeDataSourceDesigner))]
	public class AdminTaskTypeDataSource : ProviderDataSource<AdminTaskType, AdminTaskTypeKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AdminTaskTypeDataSource class.
		/// </summary>
		public AdminTaskTypeDataSource() : base(new AdminTaskTypeService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the AdminTaskTypeDataSourceView used by the AdminTaskTypeDataSource.
		/// </summary>
		protected AdminTaskTypeDataSourceView AdminTaskTypeView
		{
			get { return ( View as AdminTaskTypeDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the AdminTaskTypeDataSource control invokes to retrieve data.
		/// </summary>
		public AdminTaskTypeSelectMethod SelectMethod
		{
			get
			{
				AdminTaskTypeSelectMethod selectMethod = AdminTaskTypeSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (AdminTaskTypeSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the AdminTaskTypeDataSourceView class that is to be
		/// used by the AdminTaskTypeDataSource.
		/// </summary>
		/// <returns>An instance of the AdminTaskTypeDataSourceView class.</returns>
		protected override BaseDataSourceView<AdminTaskType, AdminTaskTypeKey> GetNewDataSourceView()
		{
			return new AdminTaskTypeDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the AdminTaskTypeDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class AdminTaskTypeDataSourceView : ProviderDataSourceView<AdminTaskType, AdminTaskTypeKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AdminTaskTypeDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the AdminTaskTypeDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public AdminTaskTypeDataSourceView(AdminTaskTypeDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal AdminTaskTypeDataSource AdminTaskTypeOwner
		{
			get { return Owner as AdminTaskTypeDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal AdminTaskTypeSelectMethod SelectMethod
		{
			get { return AdminTaskTypeOwner.SelectMethod; }
			set { AdminTaskTypeOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal AdminTaskTypeService AdminTaskTypeProvider
		{
			get { return Provider as AdminTaskTypeService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<AdminTaskType> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<AdminTaskType> results = null;
			AdminTaskType item;
			count = 0;
			
			System.Int32 _adminTaskTypeId;
			System.String _adminTaskTypeName;

			switch ( SelectMethod )
			{
				case AdminTaskTypeSelectMethod.Get:
					AdminTaskTypeKey entityKey  = new AdminTaskTypeKey();
					entityKey.Load(values);
					item = AdminTaskTypeProvider.Get(entityKey);
					results = new TList<AdminTaskType>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case AdminTaskTypeSelectMethod.GetAll:
                    results = AdminTaskTypeProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case AdminTaskTypeSelectMethod.GetPaged:
					results = AdminTaskTypeProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case AdminTaskTypeSelectMethod.Find:
					if ( FilterParameters != null )
						results = AdminTaskTypeProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = AdminTaskTypeProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case AdminTaskTypeSelectMethod.GetByAdminTaskTypeId:
					_adminTaskTypeId = ( values["AdminTaskTypeId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["AdminTaskTypeId"], typeof(System.Int32)) : (int)0;
					item = AdminTaskTypeProvider.GetByAdminTaskTypeId(_adminTaskTypeId);
					results = new TList<AdminTaskType>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				case AdminTaskTypeSelectMethod.GetByAdminTaskTypeName:
					_adminTaskTypeName = ( values["AdminTaskTypeName"] != null ) ? (System.String) EntityUtil.ChangeType(values["AdminTaskTypeName"], typeof(System.String)) : string.Empty;
					item = AdminTaskTypeProvider.GetByAdminTaskTypeName(_adminTaskTypeName);
					results = new TList<AdminTaskType>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// FK
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == AdminTaskTypeSelectMethod.Get || SelectMethod == AdminTaskTypeSelectMethod.GetByAdminTaskTypeId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				AdminTaskType entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					AdminTaskTypeProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<AdminTaskType> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			AdminTaskTypeProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region AdminTaskTypeDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the AdminTaskTypeDataSource class.
	/// </summary>
	public class AdminTaskTypeDataSourceDesigner : ProviderDataSourceDesigner<AdminTaskType, AdminTaskTypeKey>
	{
		/// <summary>
		/// Initializes a new instance of the AdminTaskTypeDataSourceDesigner class.
		/// </summary>
		public AdminTaskTypeDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public AdminTaskTypeSelectMethod SelectMethod
		{
			get { return ((AdminTaskTypeDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new AdminTaskTypeDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region AdminTaskTypeDataSourceActionList

	/// <summary>
	/// Supports the AdminTaskTypeDataSourceDesigner class.
	/// </summary>
	internal class AdminTaskTypeDataSourceActionList : DesignerActionList
	{
		private AdminTaskTypeDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the AdminTaskTypeDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public AdminTaskTypeDataSourceActionList(AdminTaskTypeDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public AdminTaskTypeSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion AdminTaskTypeDataSourceActionList
	
	#endregion AdminTaskTypeDataSourceDesigner
	
	#region AdminTaskTypeSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the AdminTaskTypeDataSource.SelectMethod property.
	/// </summary>
	public enum AdminTaskTypeSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByAdminTaskTypeId method.
		/// </summary>
		GetByAdminTaskTypeId,
		/// <summary>
		/// Represents the GetByAdminTaskTypeName method.
		/// </summary>
		GetByAdminTaskTypeName
	}
	
	#endregion AdminTaskTypeSelectMethod

	#region AdminTaskTypeFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AdminTaskType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AdminTaskTypeFilter : SqlFilter<AdminTaskTypeColumn>
	{
	}
	
	#endregion AdminTaskTypeFilter

	#region AdminTaskTypeExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AdminTaskType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AdminTaskTypeExpressionBuilder : SqlExpressionBuilder<AdminTaskTypeColumn>
	{
	}
	
	#endregion AdminTaskTypeExpressionBuilder	

	#region AdminTaskTypeProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;AdminTaskTypeChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="AdminTaskType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AdminTaskTypeProperty : ChildEntityProperty<AdminTaskTypeChildEntityTypes>
	{
	}
	
	#endregion AdminTaskTypeProperty
}

