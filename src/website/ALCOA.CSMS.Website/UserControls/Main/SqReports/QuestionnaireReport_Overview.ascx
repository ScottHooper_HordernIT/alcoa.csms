<%@ Control Language="C#" AutoEventWireup="true" Inherits="UserControls_Tiny_QuestionnaireReports_QuestionnaireReport_Overview"
    CodeBehind="QuestionnaireReport_Overview.ascx.cs" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Src="../../Other/ExportButtonsText.ascx" TagName="ExportButtons" TagPrefix="uc1" %>
<%@ Register assembly="DevExpress.Web.v14.1" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <table border="0" cellpadding="2" cellspacing="0" width="100%">
            <tr>
                <td class="pageName" colspan="3" style="height: 17px">
                    <a name="save"></a><span class="bodycopy"><span class="title">Reports</span><br />
                        <span class="date">Safety Qualification &gt; Contractors</span><br />
                        <img height="1" src="images/grfc_dottedline.gif" width="24" />&nbsp;</span>
                </td>
            </tr>
        </table>
        <table>
            <tr>
                <td width="900" style="height: 55px">
                    <table border="0" cellpadding="0" cellspacing="0" style="width:100%;">
                        <tr>
                            <td>
                                <table border="0" cellpadding="0" cellspacing="0" style="width:100%; vertical-align: middle">
                                    <tr>
                                        <td style="width: 80px; height: 34px; text-align: right">
                                            Site/Region:
                                        </td>
                                        <td style="width: 100px; height: 34px; padding-left: 2px;">
                                            <dxe:ASPxComboBox ID="cbFilterSite" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                CssPostfix="Office2003Blue" ValueType="System.String" Width="128px" IncrementalFilteringMode="StartsWith"
                                                SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                                <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                                                </LoadingPanelImage>
                                                <ButtonStyle Width="13px">
                                                </ButtonStyle>
                                            </dxe:ASPxComboBox>
                                        </td>
                                        <td style="width: 80px; height: 34px; text-align: right">
                                            Company:
                                        </td>
                                        <td style="height: 34px; padding-left: 2px;">
                                            <dxe:ASPxComboBox ID="cbFilterCompany" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                CssPostfix="Office2003Blue" ValueType="System.String" Width="295px" IncrementalFilteringMode="StartsWith"
                                                SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                                <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                                                </LoadingPanelImage>
                                                <ButtonStyle Width="13px">
                                                </ButtonStyle>
                                            </dxe:ASPxComboBox>
                                        </td>
                                        <td style="width: 80px; height: 34px; text-align: right">
                                            ABN:
                                        </td>
                                        <td style="width: 100px; height: 34px; padding-left: 2px;">
                                            <dxe:ASPxComboBox ID="cbFilterABN" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                CssPostfix="Office2003Blue" ValueType="System.String" Width="130px" IncrementalFilteringMode="StartsWith"
                                                SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                                <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                                                </LoadingPanelImage>
                                                <ButtonStyle Width="13px">
                                                </ButtonStyle>
                                            </dxe:ASPxComboBox>
                                        </td>
                                    </tr>
                                </table>
                                <table border="0" cellpadding="0" cellspacing="0" style="width:100%; vertical-align: middle">
                                    <tr>
                                        <td style="width: 80px; height: 34px; text-align: right">
                                            Service:
                                        </td>
                                        <td style="width:295px; height: 34px; padding-left: 2px;">
                                            <dxe:ASPxComboBox ID="cbFilterService" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                CssPostfix="Office2003Blue" ValueType="System.String" Width="295px" IncrementalFilteringMode="StartsWith"
                                                SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                                <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                                                </LoadingPanelImage>
                                                <ButtonStyle Width="13px">
                                                </ButtonStyle>
                                            </dxe:ASPxComboBox>
                                        </td>
                                        <td style="width: 80px; height: 34px; text-align: right">
                                            Type:
                                        </td>
                                        <td style="vertical-align: middle; height: 34px; padding-left: 2px;">
                                            <dxe:ASPxComboBox ID="cbFilterType" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                CssPostfix="Office2003Blue" ValueType="System.String" Width="130px" IncrementalFilteringMode="StartsWith"
                                                SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                                <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                                                </LoadingPanelImage>
                                                <ButtonStyle Width="13px">
                                                </ButtonStyle>
                                            </dxe:ASPxComboBox>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td style="width: 93px;height: 34px; text-align: right">
                                <dxe:ASPxButton ID="btnFilter" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                    CssPostfix="Office2003Blue" Text="Filter" Width="81px" OnClick="btnFilter_Click"
                                    ClientInstanceName="btnFilter" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                </dxe:ASPxButton>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <%--    <tr>
        <td style="height: 1px; text-align: right" width="900">
            Customize</td>
    </tr>--%>
            <tr>
                <td width="900" style="height: 208px">
                    <dxwgv:ASPxGridView ID="grid" ClientInstanceName="grid" runat="server" AutoGenerateColumns="False"
                        OnHtmlRowCreated="grid_RowCreated" OnProcessColumnAutoFilter="grid_ProcessColumnAutoFilter"
                        OnAutoFilterCellEditorCreate="grid_AutoFilterCellEditorCreate" OnAutoFilterCellEditorInitialize="grid_AutoFilterCellEditorInitialize"
                        CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue"
                        Width="100%">
                        <Columns>
                            <dxwgv:GridViewDataTextColumn Caption=" " FieldName="QuestionnaireId" FixedStyle="Left"
                                ReadOnly="True" VisibleIndex="0" Width="52px">
                                <Settings AllowAutoFilter="False" AllowHeaderFilter="False" />
                                <EditFormSettings Visible="False" />
                                <DataItemTemplate>
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <asp:HyperLink ID="hlAssess" runat="server" Text="Review" NavigateUrl=""></asp:HyperLink>
                                            </td>
                                        </tr>
                                    </table>
                                </DataItemTemplate>
                            </dxwgv:GridViewDataTextColumn>
                            <dxwgv:GridViewDataTextColumn Caption="TL" FieldName="TrafficLight" FixedStyle="Left" SortIndex="0"
                                SortOrder="Ascending" VisibleIndex="1" Width="55px">
                                <DataItemTemplate>
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <dataitemtemplate>
                                                    <asp:Image ID="imgColumnCompanyTrafficLight" runat="server" ImageUrl="~/Images/spacer.gif" Visible="true" GenerateEmptyAlternateText="True" />
                                                </dataitemtemplate>
                                            </td>
                                        </tr>
                                    </table>
                                </DataItemTemplate>
                            </dxwgv:GridViewDataTextColumn>
                            <dxwgv:GridViewDataComboBoxColumn FieldName="CompanyName" FixedStyle="Left" SortIndex="2"
                                SortOrder="Ascending" VisibleIndex="3" Width="215px">
                                <PropertiesComboBox IncrementalFilteringMode="StartsWith" ValueType="System.Int32">
                                </PropertiesComboBox>
                            </dxwgv:GridViewDataComboBoxColumn>

                            
                          <%--  Added By Sayani--%>
                            <dxwgv:GridViewDataColumn Caption="Contractor Responsible Persons (CRP)" FieldName="KWI_CRP_Name" VisibleIndex="4" Width="255px">
                                <HeaderStyle HorizontalAlign="Center" />
                                <CellStyle HorizontalAlign="Left">
                                </CellStyle>
                              
                            </dxwgv:GridViewDataColumn>
                            
                            
                            
                           <%-- END--%>

                            <dxwgv:GridViewDataTextColumn Caption="Type" FieldName="Type" VisibleIndex="5" Width="110px">
                                <HeaderStyle HorizontalAlign="Center" />
                                <CellStyle HorizontalAlign="Left">
                                </CellStyle>
                            </dxwgv:GridViewDataTextColumn>


                            <dxwgv:GridViewDataTextColumn Caption="RiskRating" FieldName="MainAssessmentRiskRating"
                                Name="RiskRating" Visible="False" VisibleIndex="6">
                                <EditFormSettings Visible="False" />
                            </dxwgv:GridViewDataTextColumn>
                            <dxwgv:GridViewDataTextColumn Caption="Procurement Risk Rating" FieldName="InitialRiskAssessment"
                                Visible="False" VisibleIndex="7" Width="115px">
                                <HeaderStyle HorizontalAlign="Center" Wrap="True" />
                            </dxwgv:GridViewDataTextColumn>
                            <dxwgv:GridViewDataTextColumn Caption="33.055.1 Risk Rating" FieldName="MainAssessmentRiskRating"
                                Visible="False" VisibleIndex="8" Width="100px">
                                <DataItemTemplate>
                                    <asp:HyperLink ID="RiskLevel" runat="server" Text="" NavigateUrl=""></asp:HyperLink>
                                </DataItemTemplate>
                                <HeaderStyle HorizontalAlign="Center" Wrap="True" />
                            </dxwgv:GridViewDataTextColumn>
                            <dxwgv:GridViewDataTextColumn Caption="Final Risk Rating" Name="FinalRiskRating" FieldName="FinalRiskRating"
                                VisibleIndex="9" Width="110px">
                                <HeaderStyle HorizontalAlign="Center" Wrap="True" />
                            </dxwgv:GridViewDataTextColumn>
                            <dxwgv:GridViewDataComboBoxColumn FieldName="Status" Name="Status" UnboundType="String"
                                Visible="False" VisibleIndex="10" Width="130px">
                                <PropertiesComboBox DataSourceID="QuestionnaireStatusDataSource" DropDownHeight="150px"
                                    TextField="QuestionnaireStatusDesc" ValueField="QuestionnaireStatusId" ValueType="System.String">
                                </PropertiesComboBox>
                            </dxwgv:GridViewDataComboBoxColumn>

                            <dxwgv:GridViewDataTextColumn Caption="KWI" FieldName="KWI" ToolTip="Approved to work at Kwinana?"
                                VisibleIndex="11" Width="40px">
                                <Settings AllowAutoFilter="False" AllowHeaderFilter="False" AllowSort="False" />
                                <DataItemTemplate>
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblKWI" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </DataItemTemplate>
                                <HeaderStyle HorizontalAlign="Center" />
                                <CellStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                </CellStyle>
                            </dxwgv:GridViewDataTextColumn>
                            <dxwgv:GridViewDataComboBoxColumn Caption="KWI SPA" FieldName="KWI_Spa_Name" ToolTip="Kwinana Designated SPA"
                                VisibleIndex="12" Width="115px">
                            </dxwgv:GridViewDataComboBoxColumn>
                            <dxwgv:GridViewDataComboBoxColumn Caption="KWI Sponsor" FieldName="KWI_Sponsor_Name" ToolTip="Kwinana Designated Sponsor"
                                VisibleIndex="13" Width="115px">
                            </dxwgv:GridViewDataComboBoxColumn>
                            <dxwgv:GridViewDataComboBoxColumn Caption="KWI Residential Category" FieldName="KWI_CompanySiteCategory" ToolTip="Kwinana Residential Category"
                                VisibleIndex="14" Width="125px">
                            </dxwgv:GridViewDataComboBoxColumn>
                            <dxwgv:GridViewDataComboBoxColumn Caption="KWI ARP" FieldName="KWI_ARP_Name" ToolTip="Kwinana Alcoa Responsible Person"
                                VisibleIndex="15" Width="115px">
                            </dxwgv:GridViewDataComboBoxColumn>
                            <%--<dxwgv:GridViewDataComboBoxColumn Caption="KWI CRP" FieldName="KwiCrpName" ToolTip="Kwinana Contract Responsible Person"
                                VisibleIndex="8" Width="115px">
                            </dxwgv:GridViewDataComboBoxColumn>--%>

                            <dxwgv:GridViewDataTextColumn Caption="PIN" FieldName="PIN" ToolTip="Approved to work at Pinjarra?"
                                VisibleIndex="17" Width="40px">
                                <Settings AllowAutoFilter="False" AllowHeaderFilter="False" AllowSort="False" />
                                <DataItemTemplate>
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblPIN" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </DataItemTemplate>
                                <HeaderStyle HorizontalAlign="Center" />
                                <CellStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                </CellStyle>
                            </dxwgv:GridViewDataTextColumn>
                            <dxwgv:GridViewDataComboBoxColumn Caption="PIN SPA" FieldName="PIN_Spa_Name" ToolTip="Pinjarra Designated SPA"
                                VisibleIndex="18" Width="115px">
                            </dxwgv:GridViewDataComboBoxColumn>
                            <dxwgv:GridViewDataComboBoxColumn Caption="PIN Sponsor" FieldName="PIN_Sponsor_Name" ToolTip="Pinjarra Designated Sponsor"
                                VisibleIndex="19" Width="115px">
                            </dxwgv:GridViewDataComboBoxColumn>
                            <dxwgv:GridViewDataComboBoxColumn Caption="PIN Residential Category" FieldName="PIN_CompanySiteCategory" ToolTip="Pinjarra Residential Category"
                                VisibleIndex="20" Width="125px">
                            </dxwgv:GridViewDataComboBoxColumn>
                            <dxwgv:GridViewDataComboBoxColumn Caption="PIN ARP" FieldName="PIN_ARP_Name" ToolTip="Pinjarra Alcoa Responsible Person"
                                VisibleIndex="21" Width="115px">
                            </dxwgv:GridViewDataComboBoxColumn>
                          <%--  <dxwgv:GridViewDataComboBoxColumn Caption="PIN CRP" FieldName="PinCrpName" ToolTip="Pinjarra Contract Responsible Person"
                                VisibleIndex="13" Width="115px">
                            </dxwgv:GridViewDataComboBoxColumn>--%>

                            <dxwgv:GridViewDataTextColumn Caption="WGP" FieldName="WGP" ToolTip="Approved to work at Wagerup?"
                                VisibleIndex="22" Width="40px">
                                <Settings AllowAutoFilter="False" AllowHeaderFilter="False" AllowSort="False" />
                                <DataItemTemplate>
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblWGP" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </DataItemTemplate>
                                <HeaderStyle HorizontalAlign="Center" />
                                <CellStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                </CellStyle>
                            </dxwgv:GridViewDataTextColumn>
                            <dxwgv:GridViewDataComboBoxColumn Caption="WGP SPA" FieldName="WGP_Spa_Name" ToolTip="Wagerup Designated SPA"
                                VisibleIndex="23" Width="115px">
                            </dxwgv:GridViewDataComboBoxColumn>
                            <dxwgv:GridViewDataComboBoxColumn Caption="WGP Sponsor" FieldName="WGP_Sponsor_Name" ToolTip="Wagerup Designated Sponsor"
                                VisibleIndex="24" Width="115px">
                            </dxwgv:GridViewDataComboBoxColumn>
                            <dxwgv:GridViewDataComboBoxColumn Caption="WGP Residential Category" FieldName="WGP_CompanySiteCategory" ToolTip="Wagerup Residential Category"
                                VisibleIndex="25" Width="125px">
                            </dxwgv:GridViewDataComboBoxColumn>
                            <dxwgv:GridViewDataComboBoxColumn Caption="WGP ARP" FieldName="WGP_ARP_Name" ToolTip="Wagerup Alcoa Responsible Person"
                                VisibleIndex="26" Width="115px">
                            </dxwgv:GridViewDataComboBoxColumn>
                          <%--  <dxwgv:GridViewDataComboBoxColumn Caption="WGP CRP" FieldName="WgpCrpName" ToolTip="Wagerup Contract Responsible Person"
                                VisibleIndex="18" Width="115px">
                            </dxwgv:GridViewDataComboBoxColumn>--%>

                            <dxwgv:GridViewDataTextColumn Caption="HUN" FieldName="HUN" ToolTip="Approved to work at Huntly (Mine)?"
                                VisibleIndex="27" Width="40px">
                                <Settings AllowAutoFilter="False" AllowHeaderFilter="False" AllowSort="False" />
                                <DataItemTemplate>
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblHUN" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </DataItemTemplate>
                                <HeaderStyle HorizontalAlign="Center" />
                                <CellStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                </CellStyle>
                            </dxwgv:GridViewDataTextColumn>
                            <dxwgv:GridViewDataComboBoxColumn Caption="HUN SPA" FieldName="HUN_Spa_Name" ToolTip="Huntly (Mine) Designated SPA"
                                VisibleIndex="28" Width="115px">
                            </dxwgv:GridViewDataComboBoxColumn>
                            <dxwgv:GridViewDataComboBoxColumn Caption="HUN Sponsor" FieldName="HUN_Sponsor_Name" ToolTip="Huntly (Mine) Designated Sponsor"
                                VisibleIndex="29" Width="115px">
                            </dxwgv:GridViewDataComboBoxColumn>
                            <dxwgv:GridViewDataComboBoxColumn Caption="HUN Residential Category" FieldName="HUN_CompanySiteCategory" ToolTip="Huntly (Mine) Residential Category"
                                VisibleIndex="30" Width="125px">
                            </dxwgv:GridViewDataComboBoxColumn>
                            <dxwgv:GridViewDataComboBoxColumn Caption="HUN ARP" FieldName="HUN_ARP_Name" ToolTip="Huntly (Mine) Alcoa Responsible Person"
                                VisibleIndex="31" Width="115px">
                            </dxwgv:GridViewDataComboBoxColumn>
                   <%--         <dxwgv:GridViewDataComboBoxColumn Caption="HUN CRP" FieldName="HunCrpName" ToolTip="Huntly (Mine) Contract Responsible Person"
                                VisibleIndex="24" Width="115px">
                            </dxwgv:GridViewDataComboBoxColumn>--%>

                            <dxwgv:GridViewDataTextColumn Caption="WDL" FieldName="WDL" ToolTip="Approved to work at Willowdale (Mine)?"
                                VisibleIndex="32" Width="40px">
                                <Settings AllowAutoFilter="False" AllowHeaderFilter="False" AllowSort="False" />
                                <DataItemTemplate>
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblWDL" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </DataItemTemplate>
                                <HeaderStyle HorizontalAlign="Center" />
                                <CellStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                </CellStyle>
                            </dxwgv:GridViewDataTextColumn>
                            <dxwgv:GridViewDataComboBoxColumn Caption="WDL SPA" FieldName="WDL_Spa_Name" ToolTip="Willowdale (Mine) Designated SPA"
                                VisibleIndex="33" Width="115px">
                            </dxwgv:GridViewDataComboBoxColumn>
                            <dxwgv:GridViewDataComboBoxColumn Caption="WDL Sponsor" FieldName="WDL_Sponsor_Name" ToolTip="Willowdale (Mine) Designated Sponsor"
                                VisibleIndex="34" Width="115px">
                            </dxwgv:GridViewDataComboBoxColumn>
                            <dxwgv:GridViewDataComboBoxColumn Caption="WDL Residential Category" FieldName="WDL_CompanySiteCategory" ToolTip="Willowdale (Mine) Residential Category"
                                VisibleIndex="35" Width="125px">
                            </dxwgv:GridViewDataComboBoxColumn>
                            <dxwgv:GridViewDataComboBoxColumn Caption="WDL ARP" FieldName="WDL_ARP_Name" ToolTip="Willowdale (Mine) Alcoa Responsible Person"
                                VisibleIndex="36" Width="115px">
                            </dxwgv:GridViewDataComboBoxColumn>
                          <%--  <dxwgv:GridViewDataComboBoxColumn Caption="WDL CRP" FieldName="WdlCrpName" ToolTip="Willowdale (Mine) Contract Responsible Person"
                                VisibleIndex="29" Width="115px">
                            </dxwgv:GridViewDataComboBoxColumn>--%>

                            <dxwgv:GridViewDataTextColumn Caption="BUN" FieldName="BUN" ToolTip="Approved to work at Bunbury Port?"
                                VisibleIndex="37" Width="40px">
                                <Settings AllowAutoFilter="False" AllowHeaderFilter="False" AllowSort="False" />
                                <DataItemTemplate>
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblBUN" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </DataItemTemplate>
                                <HeaderStyle HorizontalAlign="Center" />
                                <CellStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                </CellStyle>
                            </dxwgv:GridViewDataTextColumn>
                            <dxwgv:GridViewDataComboBoxColumn Caption="BUN SPA" FieldName="BUN_Spa_Name" ToolTip="Bunbury Port Designated SPA"
                                VisibleIndex="38" Width="115px">
                            </dxwgv:GridViewDataComboBoxColumn>
                            <dxwgv:GridViewDataComboBoxColumn Caption="BUN Sponsor" FieldName="BUN_Sponsor_Name" ToolTip="Bunbury Port Designated Sponsor"
                                VisibleIndex="39" Width="115px">
                            </dxwgv:GridViewDataComboBoxColumn>
                            <dxwgv:GridViewDataComboBoxColumn Caption="BUN Residential Category" FieldName="BUN_CompanySiteCategory" ToolTip="Bunbury Port Residential Category"
                                VisibleIndex="40" Width="125px">
                            </dxwgv:GridViewDataComboBoxColumn>
                            <dxwgv:GridViewDataComboBoxColumn Caption="BUN ARP" FieldName="BUN_ARP_Name" ToolTip="Bunbury Port Alcoa Responsible Person"
                                VisibleIndex="41" Width="115px">
                            </dxwgv:GridViewDataComboBoxColumn>
                         <%--   <dxwgv:GridViewDataComboBoxColumn Caption="BUN CRP" FieldName="BunCrpName" ToolTip="Bunbury Port Contract Responsible Person"
                                VisibleIndex="34" Width="115px">
                            </dxwgv:GridViewDataComboBoxColumn>--%>

                            <dxwgv:GridViewDataTextColumn Caption="FML" FieldName="FML" ToolTip="Approved to work at Farmlands?"
                                VisibleIndex="42" Width="40px">
                                <Settings AllowAutoFilter="False" AllowHeaderFilter="False" AllowSort="False" />
                                <DataItemTemplate>
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblFML" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </DataItemTemplate>
                                <HeaderStyle HorizontalAlign="Center" />
                            </dxwgv:GridViewDataTextColumn>
                            <dxwgv:GridViewDataComboBoxColumn Caption="FML SPA" FieldName="FML_Spa_Name" ToolTip="Farmlands Designated SPA"
                                VisibleIndex="43" Width="115px">
                            </dxwgv:GridViewDataComboBoxColumn>
                            <dxwgv:GridViewDataComboBoxColumn Caption="FML Sponsor" FieldName="FML_Sponsor_Name" ToolTip="Farmlands Designated Sponsor"
                                VisibleIndex="44" Width="115px">
                            </dxwgv:GridViewDataComboBoxColumn>
                            <dxwgv:GridViewDataComboBoxColumn Caption="FML Residential Category" FieldName="FML_CompanySiteCategory" ToolTip="Farmlands Residential Category"
                                VisibleIndex="45" Width="125px">
                            </dxwgv:GridViewDataComboBoxColumn>
                            <dxwgv:GridViewDataComboBoxColumn Caption="FML ARP" FieldName="FML_ARP_Name" ToolTip="Farmlands Alcoa Responsible Person"
                                VisibleIndex="46" Width="115px">
                            </dxwgv:GridViewDataComboBoxColumn>
                           <%-- <dxwgv:GridViewDataComboBoxColumn Caption="FML CRP" FieldName="FmlCrpName" ToolTip="Farmlands Contract Responsible Person"
                                VisibleIndex="39" Width="115px">
                            </dxwgv:GridViewDataComboBoxColumn>--%>

                            <dxwgv:GridViewDataTextColumn Caption="BGN" FieldName="BGN" ToolTip="Approved to work at Booragoon Office?"
                                VisibleIndex="47" Width="40px">
                                <Settings AllowAutoFilter="False" AllowHeaderFilter="False" AllowSort="False" />
                                <DataItemTemplate>
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblBGN" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </DataItemTemplate>
                                <HeaderStyle HorizontalAlign="Center" />
                                <CellStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                </CellStyle>
                            </dxwgv:GridViewDataTextColumn>
                            <dxwgv:GridViewDataComboBoxColumn Caption="BGN SPA" FieldName="BGN_Spa_Name" ToolTip="Booragoon Office Designated SPA"
                                VisibleIndex="48" Width="115px">
                            </dxwgv:GridViewDataComboBoxColumn>
                            <dxwgv:GridViewDataComboBoxColumn Caption="BGN Sponsor" FieldName="BGN_Sponsor_Name" ToolTip="Booragoon Office Designated Sponsor"
                                VisibleIndex="49" Width="115px">
                            </dxwgv:GridViewDataComboBoxColumn>
                            <dxwgv:GridViewDataComboBoxColumn Caption="BGN Residential Category" FieldName="BGN_CompanySiteCategory" ToolTip="Booragoon Residential Category"
                                VisibleIndex="50" Width="125px">
                            </dxwgv:GridViewDataComboBoxColumn>
                            <dxwgv:GridViewDataComboBoxColumn Caption="BGN ARP" FieldName="BGN_ARP_Name" ToolTip="Booragoon Alcoa Responsible Person"
                                VisibleIndex="51" Width="115px">
                            </dxwgv:GridViewDataComboBoxColumn>
                           <%-- <dxwgv:GridViewDataComboBoxColumn Caption="BGN CRP" FieldName="BgnCrpName" ToolTip="Booragoon Contract Responsible Person"
                                VisibleIndex="44" Width="115px">
                            </dxwgv:GridViewDataComboBoxColumn>--%>

                            <dxwgv:GridViewDataTextColumn Caption="CE" FieldName="CE" ToolTip="Approved to work at Central Engineering (WAO)?"
                                VisibleIndex="52" Width="40px">
                                <Settings AllowAutoFilter="False" AllowHeaderFilter="False" AllowSort="False" />
                                <DataItemTemplate>
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblCE" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </DataItemTemplate>
                                <HeaderStyle HorizontalAlign="Center" />
                                <CellStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                </CellStyle>
                            </dxwgv:GridViewDataTextColumn>
                            <dxwgv:GridViewDataComboBoxColumn Caption="CE SPA" FieldName="CE_Spa_Name" ToolTip="Central Engineering (WAO) Designated SPA"
                                VisibleIndex="53" Width="115px">
                            </dxwgv:GridViewDataComboBoxColumn>
                            <dxwgv:GridViewDataComboBoxColumn Caption="CE Sponsor" FieldName="CE_Sponsor_Name" ToolTip="Central Engineering (WAO) Designated Sponsor"
                                VisibleIndex="54" Width="115px">
                            </dxwgv:GridViewDataComboBoxColumn>
                            <dxwgv:GridViewDataComboBoxColumn Caption="CE Residential Category" FieldName="CE_CompanySiteCategory" ToolTip="Central Engineering (WAO) Residential Category"
                                VisibleIndex="55" Width="125px">
                            </dxwgv:GridViewDataComboBoxColumn>
                            <dxwgv:GridViewDataComboBoxColumn Caption="CE ARP" FieldName="CE_ARP_Name" ToolTip="Central Engineering (WAO) Alcoa Responsible Person"
                                VisibleIndex="56" Width="115px">
                            </dxwgv:GridViewDataComboBoxColumn>
                            <%--<dxwgv:GridViewDataComboBoxColumn Caption="CE CRP" FieldName="CeCrpName" ToolTip="Central Engineering (WAO) Contract Responsible Person"
                                VisibleIndex="49" Width="115px">
                            </dxwgv:GridViewDataComboBoxColumn>--%>

                            <dxwgv:GridViewDataTextColumn Caption="ANG" FieldName="ANG" ToolTip="Approved to work at Anglesea?"
                                VisibleIndex="57" Width="40px">
                                <Settings AllowAutoFilter="False" AllowHeaderFilter="False" AllowSort="False" />
                                <DataItemTemplate>
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblANG" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </DataItemTemplate>
                                <HeaderStyle HorizontalAlign="Center" />
                                <CellStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                </CellStyle>
                            </dxwgv:GridViewDataTextColumn>
                            <dxwgv:GridViewDataComboBoxColumn Caption="ANG SPA" FieldName="ANG_Spa_Name" ToolTip="Anglesea Designated SPA"
                                VisibleIndex="58" Width="115px">
                            </dxwgv:GridViewDataComboBoxColumn>
                            <dxwgv:GridViewDataComboBoxColumn Caption="ANG Sponsor" FieldName="ANG_Sponsor_Name" ToolTip="Anglesea Designated Sponsor"
                                VisibleIndex="59" Width="115px">
                            </dxwgv:GridViewDataComboBoxColumn>
                            <dxwgv:GridViewDataComboBoxColumn Caption="ANG Residential Category" FieldName="ANG_CompanySiteCategory" ToolTip="Anglesea Residential Category"
                                VisibleIndex="60" Width="125px">
                            </dxwgv:GridViewDataComboBoxColumn>
                            <dxwgv:GridViewDataComboBoxColumn Caption="ANG ARP" FieldName="ANG_ARP_Name" ToolTip="Anglesea Alcoa Responsible Person"
                                VisibleIndex="61" Width="115px">
                            </dxwgv:GridViewDataComboBoxColumn>
                            <%--<dxwgv:GridViewDataComboBoxColumn Caption="ANG CRP" FieldName="AngCrpName" ToolTip="Anglesea Contract Responsible Person"
                                VisibleIndex="54" Width="115px">
                            </dxwgv:GridViewDataComboBoxColumn>--%>

                            <dxwgv:GridViewDataTextColumn Caption="PTL" FieldName="PTD" ToolTip="Approved to work at Portland?"
                                VisibleIndex="62" Width="40px">
                                <Settings AllowAutoFilter="False" AllowHeaderFilter="False" AllowSort="False" />
                                <DataItemTemplate>
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblPTL" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </DataItemTemplate>
                                <HeaderStyle HorizontalAlign="Center" />
                                <CellStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                </CellStyle>
                            </dxwgv:GridViewDataTextColumn>
                            <dxwgv:GridViewDataComboBoxColumn Caption="PTL SPA" FieldName="PTD_Spa_Name" ToolTip="Portland Designated SPA"
                                VisibleIndex="63" Width="115px">
                            </dxwgv:GridViewDataComboBoxColumn>
                            <dxwgv:GridViewDataComboBoxColumn Caption="PTL Sponsor" FieldName="PTD_Sponsor_Name" ToolTip="Portland Designated Sponsor"
                                VisibleIndex="64" Width="115px">
                            </dxwgv:GridViewDataComboBoxColumn>
                            <dxwgv:GridViewDataComboBoxColumn Caption="PTL Residential Category" FieldName="PTD_CompanySiteCategory" ToolTip="Portland Residential Category"
                                VisibleIndex="65" Width="125px">
                            </dxwgv:GridViewDataComboBoxColumn>
                            <dxwgv:GridViewDataComboBoxColumn Caption="PTL ARP" FieldName="PTD_ARP_Name" ToolTip="Portland Alcoa Responsible Person"
                                VisibleIndex="66" Width="115px">
                            </dxwgv:GridViewDataComboBoxColumn>
                          <%--  <dxwgv:GridViewDataComboBoxColumn Caption="PTL CRP" FieldName="PtlCrpName" ToolTip="Portland Contract Responsible Person"
                                VisibleIndex="59" Width="115px">
                            </dxwgv:GridViewDataComboBoxColumn>--%>

                            <dxwgv:GridViewDataTextColumn Caption="PTH" FieldName="PTH" ToolTip="Approved to work at Point Henry?"
                                VisibleIndex="67" Width="40px">
                                <Settings AllowAutoFilter="False" AllowHeaderFilter="False" AllowSort="False" />
                                <DataItemTemplate>
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblPTH" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </DataItemTemplate>
                                <HeaderStyle HorizontalAlign="Center" />
                                <CellStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                </CellStyle>
                            </dxwgv:GridViewDataTextColumn>
                            <dxwgv:GridViewDataComboBoxColumn Caption="PTH SPA" FieldName="PTH_Spa_Name" ToolTip="Point Henry Designated SPA"
                                VisibleIndex="68" Width="115px">
                            </dxwgv:GridViewDataComboBoxColumn>
                            <dxwgv:GridViewDataComboBoxColumn Caption="PTH Sponsor" FieldName="PTH_Sponsor_Name" ToolTip="Point Henry Designated Sponsor"
                                VisibleIndex="69" Width="115px">
                            </dxwgv:GridViewDataComboBoxColumn>
                            <dxwgv:GridViewDataComboBoxColumn Caption="PTH Residential Category" FieldName="PTH_CompanySiteCategory" ToolTip="Point Henry Residential Category"
                                VisibleIndex="70" Width="125px">
                            </dxwgv:GridViewDataComboBoxColumn>
                            <dxwgv:GridViewDataComboBoxColumn Caption="PTH ARP" FieldName="PTH_ARP_Name" ToolTip="Point Henry Alcoa Responsible Person"
                                VisibleIndex="71" Width="115px">
                            </dxwgv:GridViewDataComboBoxColumn>
                            <%--<dxwgv:GridViewDataComboBoxColumn Caption="PTH CRP" FieldName="PthCrpName" ToolTip="Point Henry Contract Responsible Person"
                                VisibleIndex="64" Width="115px">
                            </dxwgv:GridViewDataComboBoxColumn>--%>

                            <dxwgv:GridViewDataTextColumn Caption="PEEL" FieldName="PNJ" ToolTip="Approved to work at Peel Office?"
                                VisibleIndex="72" Width="40px">
                                <Settings AllowAutoFilter="False" AllowHeaderFilter="False" AllowSort="False" />
                                <DataItemTemplate>
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblPEEL" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </DataItemTemplate>
                                <HeaderStyle HorizontalAlign="Center" />
                                <CellStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                </CellStyle>
                            </dxwgv:GridViewDataTextColumn>
                            <dxwgv:GridViewDataComboBoxColumn Caption="PEEL SPA" FieldName="PNJ_Spa_Name" ToolTip="Peel Office Designated DPA"
                                VisibleIndex="73" Width="115px">
                            </dxwgv:GridViewDataComboBoxColumn>
                            <dxwgv:GridViewDataComboBoxColumn Caption="PEEL Sponsor" FieldName="PNJ_Sponsor_Name" ToolTip="Peel Office Designated Sponsor"
                                VisibleIndex="74" Width="115px">
                            </dxwgv:GridViewDataComboBoxColumn>
                            <dxwgv:GridViewDataComboBoxColumn Caption="PEEL Residential Category" FieldName="PNJ_CompanySiteCategory" ToolTip="Peel Office Residential Category"
                                VisibleIndex="75" Width="125px">
                            </dxwgv:GridViewDataComboBoxColumn>
                            <dxwgv:GridViewDataComboBoxColumn Caption="PEEL ARP" FieldName="PNJ_ARP_Name" ToolTip="Peel Office Alcoa Responsible Person"
                                VisibleIndex="76" Width="115px">
                            </dxwgv:GridViewDataComboBoxColumn>
                           <%-- <dxwgv:GridViewDataComboBoxColumn Caption="PEEL CRP" FieldName="PelCrpName" ToolTip="Peel Office Contract Responsible Person"
                                VisibleIndex="69" Width="115px">
                            </dxwgv:GridViewDataComboBoxColumn>--%>

                            <dxwgv:GridViewDataTextColumn Caption="PTH (ARP)" FieldName="KPH" ToolTip="Approved to work at ARP Point Henry?"
                                VisibleIndex="77" Width="40px">
                                <Settings AllowAutoFilter="False" AllowHeaderFilter="False" AllowSort="False" />
                                <DataItemTemplate>
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblARP" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </DataItemTemplate>
                                <HeaderStyle HorizontalAlign="Center" />
                                <CellStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                </CellStyle>
                            </dxwgv:GridViewDataTextColumn>
                             <dxwgv:GridViewDataComboBoxColumn Caption="PTH (ARP) SPA" FieldName="KPH_Spa_Name"
                                ToolTip="ARP Point Henry Designated SPA" VisibleIndex="78" Width="130px">
                            </dxwgv:GridViewDataComboBoxColumn>
                            <dxwgv:GridViewDataComboBoxColumn Caption="PTH (ARP) Sponsor" FieldName="KPH_Sponsor_Name"
                                ToolTip="ARP Point Henry Designated Sponsor" VisibleIndex="79" Width="130px">
                            </dxwgv:GridViewDataComboBoxColumn>
                            <dxwgv:GridViewDataComboBoxColumn Caption="PTH (ARP) Residential Category" FieldName="KPH_CompanySiteCategory" ToolTip="ARP Point Henry Residential Category"
                                VisibleIndex="80" Width="125px">
                            </dxwgv:GridViewDataComboBoxColumn>
                            <dxwgv:GridViewDataComboBoxColumn Caption="PTH (ARP) ARP" FieldName="KPH_ARP_Name" ToolTip="ARP Point Henry Alcoa Responsible Person"
                                VisibleIndex="81" Width="115px">
                            </dxwgv:GridViewDataComboBoxColumn>
                           <%-- <dxwgv:GridViewDataComboBoxColumn Caption="PTH (ARP) CRP" FieldName="ArpCrpName" ToolTip="ARP Point Henry Contract Responsible Person"
                                VisibleIndex="74" Width="115px">
                            </dxwgv:GridViewDataComboBoxColumn>--%>


                            <dxwgv:GridViewDataTextColumn Caption="YEN (ARP)" FieldName="YEN" ToolTip="Approved to work at Yennora?"
                                VisibleIndex="82" Width="40px">
                                <Settings AllowAutoFilter="False" AllowHeaderFilter="False" AllowSort="False" />
                                <DataItemTemplate>
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblYEN" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </DataItemTemplate>
                                <HeaderStyle HorizontalAlign="Center" />
                                <CellStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                </CellStyle>
                            </dxwgv:GridViewDataTextColumn>
                            <dxwgv:GridViewDataComboBoxColumn Caption="YEN (ARP) SPA" FieldName="YEN_Spa_Name"
                                ToolTip="Yennora Designated SPA" VisibleIndex="83" Width="130px">
                            </dxwgv:GridViewDataComboBoxColumn>
                            <dxwgv:GridViewDataComboBoxColumn Caption="YEN (ARP) Sponsor" FieldName="YEN_Sponsor_Name"
                                ToolTip="Yennora Designated Sponsor" VisibleIndex="84" Width="130px">
                            </dxwgv:GridViewDataComboBoxColumn>
                            <dxwgv:GridViewDataComboBoxColumn Caption="YEN (ARP) Residential Category" FieldName="YEN_CompanySiteCategory" ToolTip="Yennora Residential Category"
                                VisibleIndex="85" Width="125px">
                            </dxwgv:GridViewDataComboBoxColumn>
                            <dxwgv:GridViewDataComboBoxColumn Caption="YEN (ARP) ARP" FieldName="YEN_ARP_Name" ToolTip="Yennora Alcoa Responsible Person"
                                VisibleIndex="86" Width="115px">
                            </dxwgv:GridViewDataComboBoxColumn>
                           <%-- <dxwgv:GridViewDataComboBoxColumn Caption="YEN (ARP) CRP" FieldName="YenCrpName" ToolTip="Yennora Contract Responsible Person"
                                VisibleIndex="79" Width="115px">
                            </dxwgv:GridViewDataComboBoxColumn>--%>

                            <dxwgv:GridViewDataTextColumn FieldName="DescriptionOfWork" VisibleIndex="87" Width="190px">
                                <Settings AllowAutoFilter="False" />
                            </dxwgv:GridViewDataTextColumn>
                            <dxwgv:GridViewDataTextColumn FieldName="LevelOfSupervision" VisibleIndex="88" Width="125px">
                            </dxwgv:GridViewDataTextColumn>
                            <dxwgv:GridViewDataComboBoxColumn Caption="Procurement Contact" FieldName="ProcurementContactUser"
                                VisibleIndex="89" Width="144px">
                                <PropertiesComboBox ValueType="System.String">
                                </PropertiesComboBox>
                            </dxwgv:GridViewDataComboBoxColumn>
                            <dxwgv:GridViewDataComboBoxColumn Caption="Safety Assessor" FieldName="SafetyAssessor"
                                Name="EHS Consultant" VisibleIndex="16" Width="115px">
                                <PropertiesComboBox ValueType="System.String">
                                </PropertiesComboBox>
                                <Settings SortMode="DisplayText" />
                            </dxwgv:GridViewDataComboBoxColumn>
                            <dxwgv:GridViewDataComboBoxColumn Caption="Contract Manager" FieldName="ContractManagerUser"
                                VisibleIndex="90" Width="126px">
                                <PropertiesComboBox ValueType="System.String">
                                </PropertiesComboBox>
                                <Settings AllowAutoFilter="True" />
                            </dxwgv:GridViewDataComboBoxColumn>
                            <dxwgv:GridViewDataComboBoxColumn Caption="Primary Service" FieldName="TypeOfService"
                                VisibleIndex="91" Width="205px">
                                <PropertiesComboBox DataSourceID="QuestionnaireServicesCategory" TextField="CategoryText"
                                    ValueField="CategoryId" ValueType="System.Int32">
                                </PropertiesComboBox>
                            </dxwgv:GridViewDataComboBoxColumn>
                            <dxwgv:GridViewDataComboBoxColumn Caption="Assessor" FieldName="ApprovedByUser" ShowInCustomizationForm="False"
                                Visible="False" VisibleIndex="92" Width="115px">
                            </dxwgv:GridViewDataComboBoxColumn>
                            <dxwgv:GridViewDataDateColumn Caption="Next SQ Due Date" FieldName="MainAssessmentValidTo"
                                ToolTip="Next Safety Qualification Due Date" VisibleIndex="93" Width="100px">
                                <Settings AllowAutoFilter="True" />
                            </dxwgv:GridViewDataDateColumn>
                            <dxwgv:GridViewDataCheckColumn FieldName="IsVerificationRequired" ShowInCustomizationForm="False"
                                Visible="False" VisibleIndex="94" Width="100px">
                            </dxwgv:GridViewDataCheckColumn>
                            <dxwgv:GridViewDataDateColumn Caption="Initial SQ Date" FieldName="InitialSpqDate"
                                Visible="False" VisibleIndex="95" Width="100px">
                            </dxwgv:GridViewDataDateColumn>
                            <dxwgv:GridViewDataCheckColumn FieldName="Active" Visible="False" VisibleIndex="96"
                                Width="100px">
                            </dxwgv:GridViewDataCheckColumn>
                            <dxwgv:GridViewDataTextColumn FieldName="QuestionnaireId" Visible="False" VisibleIndex="97"
                                Width="100px">
                                <EditFormSettings Visible="False" />
                            </dxwgv:GridViewDataTextColumn>
                            <dxwgv:GridViewDataTextColumn Caption="Company ABN" FieldName="CompanyAbn" VisibleIndex="98"
                                Width="106px">
                                <Settings AllowAutoFilter="True" />
                            </dxwgv:GridViewDataTextColumn>
                            <dxwgv:GridViewDataTextColumn Caption="Company Status" FieldName="CompanyStatusDesc"
                                VisibleIndex="99" Width="300px">
                                <HeaderStyle Wrap="False" />
                            </dxwgv:GridViewDataTextColumn>
                            <dxwgv:GridViewDataComboBoxColumn Caption="Is SQ Required?" FieldName="IsMainRequired"
                                VisibleIndex="100" Width="116px">
                                <PropertiesComboBox ValueType="System.Int32">
                                    <Items>
                                        <dxe:ListEditItem Text="Yes" Value="1"></dxe:ListEditItem>
                                        <dxe:ListEditItem Text="No" Value="0"></dxe:ListEditItem>
                                    </Items>
                                    <ItemStyle Font-Bold="True" />
                                </PropertiesComboBox>
                                <Settings SortMode="DisplayText" />
                                <HeaderStyle HorizontalAlign="Center" />
                                <CellStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                </CellStyle>
                            </dxwgv:GridViewDataComboBoxColumn>
                            <dxwgv:GridViewDataTextColumn Caption="Validity" FieldName="Validity" VisibleIndex="101"
                                Width="100px">
                                <HeaderStyle Wrap="False" />
                            </dxwgv:GridViewDataTextColumn>
                            <dxwgv:GridViewDataTextColumn Caption="First Company Requesting" FieldName="RequestingCompanyName" Width="130px" VisibleIndex="1001">
    <HeaderStyle Wrap="True" HorizontalAlign="Center"></HeaderStyle>
    </dxwgv:GridViewDataTextColumn>
                            <dxwgv:GridViewDataTextColumn Caption="Reason Supplier Required" FieldName="ReasonContractor" Width="130px" VisibleIndex="1002" Visible="false">
    <HeaderStyle Wrap="True" HorizontalAlign="Center"></HeaderStyle>
    </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Company Name" FieldName="CompanyName" SortIndex="0" SortOrder="Ascending" VisibleIndex="2"></dxwgv:GridViewDataTextColumn></Columns>
                        <ImagesFilterControl>
                            <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif"> 
                            </LoadingPanel>
                        </ImagesFilterControl>
                        <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue">
                            <Header ImageSpacing="5px" SortingImageSpacing="5px">
                            </Header>
                            <LoadingPanel ImageSpacing="10px">
                            </LoadingPanel>
                            <AlternatingRow Enabled="True">
                            </AlternatingRow>
                        </Styles>
                        <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                            <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
                            </LoadingPanelOnStatusBar>
                            <CollapsedButton Width="11px" />
                            <ExpandedButton Width="11px" />
                            <DetailCollapsedButton Width="11px" />
                            <DetailExpandedButton Width="11px" />
                            <FilterRowButton Width="13px" />
                            <LoadingPanel Url="~/App_Themes/Office2003BlueOld/GridView/Loading.gif"> <%--Enhancement_023:change by debashis for testing platform upgradation--%>
                            </LoadingPanel>
                        </Images>
                        <Settings ShowGroupPanel="True" ShowHeaderFilterButton="True" ShowHorizontalScrollBar="True"
                            ShowVerticalScrollBar="True" VerticalScrollableHeight="190" />
                        <SettingsBehavior ColumnResizeMode="Control" />
                        <SettingsPager PageSize="4">
                            <AllButton Visible="True">
                            </AllButton>
                        </SettingsPager>
                        <StylesEditors>
                            <ProgressBar Height="25px">
                            </ProgressBar>
                        </StylesEditors>
                    </dxwgv:ASPxGridView>
                </td>
            </tr>
            <tr >
                <td  class="pageName" colspan="7" style="height: 20px; text-align: right;
                    text-align: -moz-right; width: 900px;">
                    <div align="right">
                        <uc1:ExportButtons ID="ucExportButtons" runat="server" />
                    </div>

                </td>
            </tr>
            
           <tr>
              <td>
                    <dx:ASPxHyperLink ID="hlArpCrp" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                        CssPostfix="Office2003Blue" runat="server" Text="View Alcoa Responsible Persons (ARPs � Alcoa Employees) listing"
                        NavigateUrl="javascript:popUp('ViewArpListing.aspx?Help=Default');">
                    </dx:ASPxHyperLink>
              </td>
           </tr>
           <tr>
            <td></td>
            </tr>
            <tr>
                <td>
                    <table border="0" cellpadding="0" cellspacing="0" class="dxgvControl_Office2003_Blue"
                        style="width: 100%; border-collapse: collapse; empty-cells: show">
                        <tbody>
                            <tr>
                                <td class="dxgvHeader_Office2003_Blue" colspan="2" style="border-top-width: 0px;
                                    font-weight: bold; border-left-width: 0px; white-space: normal; height: 13px;
                                    text-align: left;">
                                    <span style="color: maroon">What does all these icons mean? (LEGEND)</span>
                                </td>
                            </tr>
                            <tr class="dxgvDataRow_Office2003_Blue">
                                <td class="dxgv" style="width: 30px; height: 20px; text-align: center">
                                    <img src="Images/TrafficLights/tlGreen.gif" />
                                </td>
                                <td class="dxgv" style="width: 972px; height: 20px; text-align: left">
                                    (Green) This company's safety qualification is current and active.
                                </td>
                            </tr>
                            <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                <td class="dxgv" style="width: 30px; height: 20px; text-align: center">
                                    <img src="Images/TrafficLights/tlYellow.gif" />
                                </td>
                                <td class="dxgv" style="width: 972px; height: 20px; text-align: left">
                                    (Yellow) This company's safety qualification is current and active, but is due to expire within 60 days.
                                </td>
                            </tr>
                            <tr class="dxgvDataRow_Office2003_Blue">
                                <td class="dxgv" style="width: 30px; height: 20px; text-align: center">
                                    <img src="Images/TrafficLights/tlRed.gif" />
                                </td>
                                <td class="dxgv" style="width: 972px; height: 20px; text-align: left">
                                    (Red) This company's safety qualification is not current and/or has expired.
                                </td>
                            </tr>
                            <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                <td class="dxgv" style="width: 30px; height: 20px; text-align: center">
                                    <img src="Images/TrafficLights/tlBlack.gif" />
                                </td>
                                <td class="dxgv" style="width: 972px; height: 20px; text-align: left">
                                    (Black) This company's safety qualification deems them as not recommended.
                                </td>
                            </tr>
                            <tr class="dxgvDataRow_Office2003_Blue">
                                <td class="dxgv" style="width: 30px; height: 20px; text-align: center">
                                    <img src="Images/TrafficLights/tlBlue.gif" />
                                </td>
                                <td class="dxgv" style="width: 972px; height: 20px; text-align: left">
                                    (Blue) This company is not required to complete an safety qualification as they are technical professionals or a low level delivery company.
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </table>
    </ContentTemplate>
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="btnFilter" EventName="Click"></asp:AsyncPostBackTrigger>
    </Triggers>
</asp:UpdatePanel>
<data:QuestionnaireReportOverviewDataSource ID="QuestionnaireReportOverviewDataSource1"
    runat="server" Filter="" SelectMethod="GetPaged">
</data:QuestionnaireReportOverviewDataSource>
<data:CompaniesDataSource ID="CompaniesDataSource" runat="server">
    <DeepLoadProperties Method="IncludeChildren" Recursive="False">
    </DeepLoadProperties>
</data:CompaniesDataSource>
<data:QuestionnaireStatusDataSource ID="QuestionnaireStatusDataSource" runat="server">
    <DeepLoadProperties Method="IncludeChildren" Recursive="False">
    </DeepLoadProperties>
</data:QuestionnaireStatusDataSource>
<data:QuestionnaireServicesCategoryDataSource ID="QuestionnaireServicesCategory"
    runat="server">
    <DeepLoadProperties Method="IncludeChildren" Recursive="False">
    </DeepLoadProperties>
</data:QuestionnaireServicesCategoryDataSource>
<%--<asp:SqlDataSource ID="QuestionnaireReportOverviewDataSource_FilterType" runat="server"
    ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="_QuestionnaireReportOverview_FilterType" SelectCommandType="StoredProcedure">
</asp:SqlDataSource>--%>
<asp:SqlDataSource ID="QuestionnaireReportOverviewDataSource_FilterCompanies" runat="server"
    ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="_QuestionnaireReportOverview_FilterCompanies" SelectCommandType="StoredProcedure">
</asp:SqlDataSource>
<asp:SqlDataSource ID="QuestionnaireReportOverviewDataSource_FilterABN" runat="server"
    ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="_QuestionnaireReportOverview_FilterABN" SelectCommandType="StoredProcedure">
</asp:SqlDataSource>
<asp:SqlDataSource ID="QuestionnaireReportOverviewDataSource_FilterServices" runat="server"
    ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="_QuestionnaireReportOverview_FilterServices" SelectCommandType="StoredProcedure">
</asp:SqlDataSource>
<asp:SqlDataSource ID="FilterTypeDS" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="SELECT 'Contractor' AS [Type] UNION SELECT 'Sub Contractor' AS [Type]">
</asp:SqlDataSource>
