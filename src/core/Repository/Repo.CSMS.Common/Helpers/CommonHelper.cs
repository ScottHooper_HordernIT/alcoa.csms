﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Reflection;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq.Expressions;
using System.Configuration;

namespace Repo.CSMS.Common.Helpers
{
    public class CommonHelper
    {
        #region String Functions

        /// <summary>
        /// Ensure that a string is not null
        /// </summary>
        /// <param name="str">Input string</param>
        /// <returns>Result</returns>
        public static string EnsureNotNull(string str)
        {
            if (str == null)
                return string.Empty;

            return str;
        }

        /// <summary>
        /// Verifies that a string is in valid e-mail format
        /// </summary>
        /// <param name="email">Email to verify</param>
        /// <returns>true if the string is a valid e-mail address and false if it's not</returns>
        public static bool IsValidEmail(string email)
        {
            bool result = false;
            if (String.IsNullOrEmpty(email))
                return result;
            email = email.Trim();
            result = Regex.IsMatch(email, @"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$");
            return result;
        }

        /// <summary>
        /// Convert input string to camel case string (eg. thisIsCamelCase)
        /// </summary>
        /// <param name="input">Input string</param>
        /// <returns>String in camel case format</returns>
        public static string ToCamelCase(string input)
        {
            string result = "";

            try
            {
                //Return empty string if input is empty or null
                if (string.IsNullOrEmpty(input)) return "";

                //Remove any non-alphanumeric characters
                Regex regex = new Regex("[^a-zA-Z0-9 -]");
                result = regex.Replace(input, String.Empty);

                //Convert first letter of each work to uppercase and remove all spaces
                TextInfo textInfo = new CultureInfo("en-AU", false).TextInfo;
                result = textInfo.ToTitleCase(result.Trim()).Replace(" ", "");

                //Convert first letter of first word to lowercase
                if (result.Length > 0)
                    result = result[0].ToString().ToLower() + result.Substring(1);
            }
            catch { }

            return result;
        }

        public static string ConvertStringArrayToString(string[] array)
        {
            //
            // Concatenate all the elements into a StringBuilder.
            //
            if (array == null) return "";

            int count = 0;
            foreach (string c in array)
            {
                count++;
            }


            StringBuilder builder = new StringBuilder();
            int readNumber = 0;
            foreach (string value in array)
            {
                readNumber++;
                builder.Append(value);
                if (readNumber < count)
                {
                    builder.Append(", ");
                }
            }
            
            return builder.ToString();
        }

        /// <summary>
        /// Trims the string.
        /// </summary>
        /// <param name="str">The STR.</param>
        /// <returns></returns>
        public static string TrimString(string str)
        {
            try
            {
                string pattern = @"^[ \t]+|[ \t]+$";
                Regex reg = new Regex(pattern, RegexOptions.IgnoreCase);
                str = reg.Replace(str, "");
                return str;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Converts a list of email addresses to a comma separated string making sure we don't have a comma at the end.
        /// The mail messages can take a comma separated string to Add all values in one hit
        /// </summary>
        /// <param name="listAsString"></param>
        /// <returns></returns>
        public static string ConvertToCSV(string listAsString)
        {
            if (!string.IsNullOrEmpty(listAsString))
            {
                string list = listAsString.Replace(";", ",");

                if (list.EndsWith(","))
                    list = list.Remove(list.Length - 1, 1);

                return list;
            }
            else
                return null;
        }

        #endregion

        #region Property Functions

        public static string GetDisplayName(Type dataType, string fieldName)
        {
            try
            {
                DisplayNameAttribute attribute = (DisplayNameAttribute)dataType.GetProperty(fieldName).GetCustomAttributes(typeof(DisplayNameAttribute), true).SingleOrDefault();

                if (attribute == null)
                {
                    MetadataTypeAttribute metadataType = (MetadataTypeAttribute)dataType.GetCustomAttributes(typeof(MetadataTypeAttribute), true).FirstOrDefault();
                    if (metadataType != null)
                    {
                        var metaProperty = metadataType.MetadataClassType.GetProperty(fieldName);
                        if (metaProperty != null)
                        {
                            attribute = (DisplayNameAttribute)metaProperty.GetCustomAttributes(typeof(DisplayNameAttribute), true).SingleOrDefault();
                        }
                    }
                }
                return (attribute != null) ? attribute.DisplayName : fieldName;
            }
            catch { return fieldName; }
        }

        public static string GetEditName(Type dataType, string fieldName)
        {
            try
            {
                EditNameAttribute attribute = (EditNameAttribute)dataType.GetProperty(fieldName).GetCustomAttributes(typeof(EditNameAttribute), true).SingleOrDefault();

                if (attribute == null)
                {
                    MetadataTypeAttribute metadataType = (MetadataTypeAttribute)dataType.GetCustomAttributes(typeof(MetadataTypeAttribute), true).FirstOrDefault();
                    if (metadataType != null)
                    {
                        var metaProperty = metadataType.MetadataClassType.GetProperty(fieldName);
                        if (metaProperty != null)
                        {
                            attribute = (EditNameAttribute)metaProperty.GetCustomAttributes(typeof(EditNameAttribute), true).SingleOrDefault();
                        }
                    }
                }
                return (attribute != null) ? attribute.EditName : GetDisplayName(dataType, fieldName);
            }
            catch { return GetDisplayName(dataType, fieldName); }
        }

        public static Attribute GetCustomAttribute(Type dataType, MemberInfo property, Type attributeType)
        {
            try
            {
                var attribute = (Attribute)property.GetCustomAttributes(attributeType, true).SingleOrDefault();

                if (attribute == null)
                {
                    MetadataTypeAttribute metadataType = (MetadataTypeAttribute)dataType.GetCustomAttributes(typeof(MetadataTypeAttribute), true).FirstOrDefault();
                    if (metadataType != null)
                    {
                        var metaProperty = metadataType.MetadataClassType.GetProperty(property.Name);
                        if (metaProperty != null)
                        {
                            attribute = (Attribute)metaProperty.GetCustomAttributes(attributeType, true).SingleOrDefault();
                        }
                    }
                }
                return attribute;
            }
            catch { return null; }
        }

        public static string GetPropertyName<T>(Expression<Func<T>> expression)
        {
            MemberExpression propertyExpression = (MemberExpression)expression.Body;
            MemberInfo propertyMember = propertyExpression.Member;

            Object[] displayAttributes = propertyMember.GetCustomAttributes(typeof(DisplayAttribute), true);
            if (displayAttributes != null && displayAttributes.Length == 1)
                return ((DisplayAttribute)displayAttributes[0]).Name;

            return propertyMember.Name;
        }

        public static bool IsPropertyAttributeDefined(Type dataType, PropertyInfo property, Type attributeType)
        {
            bool isPropertyAttributeDefined = false;

            try
            {
                isPropertyAttributeDefined = property.IsDefined(attributeType, true);

                if (!isPropertyAttributeDefined)
                {
                    //check for meta data class.
                    MetadataTypeAttribute metadataType = (MetadataTypeAttribute)dataType.GetCustomAttributes(typeof(MetadataTypeAttribute), true).FirstOrDefault();
                    if (metadataType != null)
                    {
                        var metaProperty = metadataType.MetadataClassType.GetProperty(property.Name);
                        if (property != null)
                        {
                            isPropertyAttributeDefined = metaProperty.IsDefined(attributeType, true);
                        }
                    }
                }
            }
            catch { }
            
            return isPropertyAttributeDefined;
        }

        #endregion

        #region Mail Settings

        public static string MailerTemplatePath
        {
            get
            {
                string mailerTemplatePath = "";

                if (ConfigurationManager.AppSettings["Mailer.TemplatePath"] != null)
                    mailerTemplatePath = ConfigurationManager.AppSettings["Mailer.TemplatePath"].ToString();

                return mailerTemplatePath;
            }
        }

        public static string URL
        {
            get
            {
                string url = "";

                if (ConfigurationManager.AppSettings["url"] != null)
                    url = ConfigurationManager.AppSettings["url"].ToString();

                return url;
            }
        }


        #endregion
    }
}
