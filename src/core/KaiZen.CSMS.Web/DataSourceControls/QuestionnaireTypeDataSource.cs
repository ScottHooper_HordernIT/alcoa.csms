﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.QuestionnaireTypeProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(QuestionnaireTypeDataSourceDesigner))]
	public class QuestionnaireTypeDataSource : ProviderDataSource<QuestionnaireType, QuestionnaireTypeKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireTypeDataSource class.
		/// </summary>
		public QuestionnaireTypeDataSource() : base(new QuestionnaireTypeService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the QuestionnaireTypeDataSourceView used by the QuestionnaireTypeDataSource.
		/// </summary>
		protected QuestionnaireTypeDataSourceView QuestionnaireTypeView
		{
			get { return ( View as QuestionnaireTypeDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the QuestionnaireTypeDataSource control invokes to retrieve data.
		/// </summary>
		public QuestionnaireTypeSelectMethod SelectMethod
		{
			get
			{
				QuestionnaireTypeSelectMethod selectMethod = QuestionnaireTypeSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (QuestionnaireTypeSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the QuestionnaireTypeDataSourceView class that is to be
		/// used by the QuestionnaireTypeDataSource.
		/// </summary>
		/// <returns>An instance of the QuestionnaireTypeDataSourceView class.</returns>
		protected override BaseDataSourceView<QuestionnaireType, QuestionnaireTypeKey> GetNewDataSourceView()
		{
			return new QuestionnaireTypeDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the QuestionnaireTypeDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class QuestionnaireTypeDataSourceView : ProviderDataSourceView<QuestionnaireType, QuestionnaireTypeKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireTypeDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the QuestionnaireTypeDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public QuestionnaireTypeDataSourceView(QuestionnaireTypeDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal QuestionnaireTypeDataSource QuestionnaireTypeOwner
		{
			get { return Owner as QuestionnaireTypeDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal QuestionnaireTypeSelectMethod SelectMethod
		{
			get { return QuestionnaireTypeOwner.SelectMethod; }
			set { QuestionnaireTypeOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal QuestionnaireTypeService QuestionnaireTypeProvider
		{
			get { return Provider as QuestionnaireTypeService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<QuestionnaireType> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<QuestionnaireType> results = null;
			QuestionnaireType item;
			count = 0;
			
			System.Int32 _questionnaireTypeId;
			System.String _questionnaireTypeName;

			switch ( SelectMethod )
			{
				case QuestionnaireTypeSelectMethod.Get:
					QuestionnaireTypeKey entityKey  = new QuestionnaireTypeKey();
					entityKey.Load(values);
					item = QuestionnaireTypeProvider.Get(entityKey);
					results = new TList<QuestionnaireType>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case QuestionnaireTypeSelectMethod.GetAll:
                    results = QuestionnaireTypeProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case QuestionnaireTypeSelectMethod.GetPaged:
					results = QuestionnaireTypeProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case QuestionnaireTypeSelectMethod.Find:
					if ( FilterParameters != null )
						results = QuestionnaireTypeProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = QuestionnaireTypeProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case QuestionnaireTypeSelectMethod.GetByQuestionnaireTypeId:
					_questionnaireTypeId = ( values["QuestionnaireTypeId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["QuestionnaireTypeId"], typeof(System.Int32)) : (int)0;
					item = QuestionnaireTypeProvider.GetByQuestionnaireTypeId(_questionnaireTypeId);
					results = new TList<QuestionnaireType>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				case QuestionnaireTypeSelectMethod.GetByQuestionnaireTypeName:
					_questionnaireTypeName = ( values["QuestionnaireTypeName"] != null ) ? (System.String) EntityUtil.ChangeType(values["QuestionnaireTypeName"], typeof(System.String)) : string.Empty;
					item = QuestionnaireTypeProvider.GetByQuestionnaireTypeName(_questionnaireTypeName);
					results = new TList<QuestionnaireType>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// FK
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == QuestionnaireTypeSelectMethod.Get || SelectMethod == QuestionnaireTypeSelectMethod.GetByQuestionnaireTypeId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				QuestionnaireType entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					QuestionnaireTypeProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<QuestionnaireType> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			QuestionnaireTypeProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region QuestionnaireTypeDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the QuestionnaireTypeDataSource class.
	/// </summary>
	public class QuestionnaireTypeDataSourceDesigner : ProviderDataSourceDesigner<QuestionnaireType, QuestionnaireTypeKey>
	{
		/// <summary>
		/// Initializes a new instance of the QuestionnaireTypeDataSourceDesigner class.
		/// </summary>
		public QuestionnaireTypeDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public QuestionnaireTypeSelectMethod SelectMethod
		{
			get { return ((QuestionnaireTypeDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new QuestionnaireTypeDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region QuestionnaireTypeDataSourceActionList

	/// <summary>
	/// Supports the QuestionnaireTypeDataSourceDesigner class.
	/// </summary>
	internal class QuestionnaireTypeDataSourceActionList : DesignerActionList
	{
		private QuestionnaireTypeDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the QuestionnaireTypeDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public QuestionnaireTypeDataSourceActionList(QuestionnaireTypeDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public QuestionnaireTypeSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion QuestionnaireTypeDataSourceActionList
	
	#endregion QuestionnaireTypeDataSourceDesigner
	
	#region QuestionnaireTypeSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the QuestionnaireTypeDataSource.SelectMethod property.
	/// </summary>
	public enum QuestionnaireTypeSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByQuestionnaireTypeId method.
		/// </summary>
		GetByQuestionnaireTypeId,
		/// <summary>
		/// Represents the GetByQuestionnaireTypeName method.
		/// </summary>
		GetByQuestionnaireTypeName
	}
	
	#endregion QuestionnaireTypeSelectMethod

	#region QuestionnaireTypeFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireTypeFilter : SqlFilter<QuestionnaireTypeColumn>
	{
	}
	
	#endregion QuestionnaireTypeFilter

	#region QuestionnaireTypeExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireTypeExpressionBuilder : SqlExpressionBuilder<QuestionnaireTypeColumn>
	{
	}
	
	#endregion QuestionnaireTypeExpressionBuilder	

	#region QuestionnaireTypeProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;QuestionnaireTypeChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireTypeProperty : ChildEntityProperty<QuestionnaireTypeChildEntityTypes>
	{
	}
	
	#endregion QuestionnaireTypeProperty
}

