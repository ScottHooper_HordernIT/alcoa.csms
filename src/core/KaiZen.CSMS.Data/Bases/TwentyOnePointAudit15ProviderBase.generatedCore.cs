﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="TwentyOnePointAudit15ProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class TwentyOnePointAudit15ProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.TwentyOnePointAudit15, KaiZen.CSMS.Entities.TwentyOnePointAudit15Key>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.TwentyOnePointAudit15Key key)
		{
			return Delete(transactionManager, key.Id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _id)
		{
			return Delete(null, _id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _id);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.TwentyOnePointAudit15 Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.TwentyOnePointAudit15Key key, int start, int pageLength)
		{
			return GetById(transactionManager, key.Id, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_TwentyOnePointAudit_15 index.
		/// </summary>
		/// <param name="_id"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.TwentyOnePointAudit15"/> class.</returns>
		public KaiZen.CSMS.Entities.TwentyOnePointAudit15 GetById(System.Int32 _id)
		{
			int count = -1;
			return GetById(null,_id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_TwentyOnePointAudit_15 index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.TwentyOnePointAudit15"/> class.</returns>
		public KaiZen.CSMS.Entities.TwentyOnePointAudit15 GetById(System.Int32 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(null, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_TwentyOnePointAudit_15 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.TwentyOnePointAudit15"/> class.</returns>
		public KaiZen.CSMS.Entities.TwentyOnePointAudit15 GetById(TransactionManager transactionManager, System.Int32 _id)
		{
			int count = -1;
			return GetById(transactionManager, _id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_TwentyOnePointAudit_15 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.TwentyOnePointAudit15"/> class.</returns>
		public KaiZen.CSMS.Entities.TwentyOnePointAudit15 GetById(TransactionManager transactionManager, System.Int32 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(transactionManager, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_TwentyOnePointAudit_15 index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.TwentyOnePointAudit15"/> class.</returns>
		public KaiZen.CSMS.Entities.TwentyOnePointAudit15 GetById(System.Int32 _id, int start, int pageLength, out int count)
		{
			return GetById(null, _id, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_TwentyOnePointAudit_15 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.TwentyOnePointAudit15"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.TwentyOnePointAudit15 GetById(TransactionManager transactionManager, System.Int32 _id, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;TwentyOnePointAudit15&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;TwentyOnePointAudit15&gt;"/></returns>
		public static TList<TwentyOnePointAudit15> Fill(IDataReader reader, TList<TwentyOnePointAudit15> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.TwentyOnePointAudit15 c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("TwentyOnePointAudit15")
					.Append("|").Append((System.Int32)reader[((int)TwentyOnePointAudit15Column.Id - 1)]).ToString();
					c = EntityManager.LocateOrCreate<TwentyOnePointAudit15>(
					key.ToString(), // EntityTrackingKey
					"TwentyOnePointAudit15",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.TwentyOnePointAudit15();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.Id = (System.Int32)reader[((int)TwentyOnePointAudit15Column.Id - 1)];
					c.Achieved15a = (reader.IsDBNull(((int)TwentyOnePointAudit15Column.Achieved15a - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit15Column.Achieved15a - 1)];
					c.Achieved15b = (reader.IsDBNull(((int)TwentyOnePointAudit15Column.Achieved15b - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit15Column.Achieved15b - 1)];
					c.Achieved15c = (reader.IsDBNull(((int)TwentyOnePointAudit15Column.Achieved15c - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit15Column.Achieved15c - 1)];
					c.Achieved15d = (reader.IsDBNull(((int)TwentyOnePointAudit15Column.Achieved15d - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit15Column.Achieved15d - 1)];
					c.Achieved15e = (reader.IsDBNull(((int)TwentyOnePointAudit15Column.Achieved15e - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit15Column.Achieved15e - 1)];
					c.Achieved15f = (reader.IsDBNull(((int)TwentyOnePointAudit15Column.Achieved15f - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit15Column.Achieved15f - 1)];
					c.Achieved15g = (reader.IsDBNull(((int)TwentyOnePointAudit15Column.Achieved15g - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit15Column.Achieved15g - 1)];
					c.Achieved15h = (reader.IsDBNull(((int)TwentyOnePointAudit15Column.Achieved15h - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit15Column.Achieved15h - 1)];
					c.Achieved15i = (reader.IsDBNull(((int)TwentyOnePointAudit15Column.Achieved15i - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit15Column.Achieved15i - 1)];
					c.Observation15a = (reader.IsDBNull(((int)TwentyOnePointAudit15Column.Observation15a - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit15Column.Observation15a - 1)];
					c.Observation15b = (reader.IsDBNull(((int)TwentyOnePointAudit15Column.Observation15b - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit15Column.Observation15b - 1)];
					c.Observation15c = (reader.IsDBNull(((int)TwentyOnePointAudit15Column.Observation15c - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit15Column.Observation15c - 1)];
					c.Observation15d = (reader.IsDBNull(((int)TwentyOnePointAudit15Column.Observation15d - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit15Column.Observation15d - 1)];
					c.Observation15e = (reader.IsDBNull(((int)TwentyOnePointAudit15Column.Observation15e - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit15Column.Observation15e - 1)];
					c.Observation15f = (reader.IsDBNull(((int)TwentyOnePointAudit15Column.Observation15f - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit15Column.Observation15f - 1)];
					c.Observation15g = (reader.IsDBNull(((int)TwentyOnePointAudit15Column.Observation15g - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit15Column.Observation15g - 1)];
					c.Observation15h = (reader.IsDBNull(((int)TwentyOnePointAudit15Column.Observation15h - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit15Column.Observation15h - 1)];
					c.Observation15i = (reader.IsDBNull(((int)TwentyOnePointAudit15Column.Observation15i - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit15Column.Observation15i - 1)];
					c.TotalScore = (reader.IsDBNull(((int)TwentyOnePointAudit15Column.TotalScore - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit15Column.TotalScore - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.TwentyOnePointAudit15"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.TwentyOnePointAudit15"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.TwentyOnePointAudit15 entity)
		{
			if (!reader.Read()) return;
			
			entity.Id = (System.Int32)reader[((int)TwentyOnePointAudit15Column.Id - 1)];
			entity.Achieved15a = (reader.IsDBNull(((int)TwentyOnePointAudit15Column.Achieved15a - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit15Column.Achieved15a - 1)];
			entity.Achieved15b = (reader.IsDBNull(((int)TwentyOnePointAudit15Column.Achieved15b - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit15Column.Achieved15b - 1)];
			entity.Achieved15c = (reader.IsDBNull(((int)TwentyOnePointAudit15Column.Achieved15c - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit15Column.Achieved15c - 1)];
			entity.Achieved15d = (reader.IsDBNull(((int)TwentyOnePointAudit15Column.Achieved15d - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit15Column.Achieved15d - 1)];
			entity.Achieved15e = (reader.IsDBNull(((int)TwentyOnePointAudit15Column.Achieved15e - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit15Column.Achieved15e - 1)];
			entity.Achieved15f = (reader.IsDBNull(((int)TwentyOnePointAudit15Column.Achieved15f - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit15Column.Achieved15f - 1)];
			entity.Achieved15g = (reader.IsDBNull(((int)TwentyOnePointAudit15Column.Achieved15g - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit15Column.Achieved15g - 1)];
			entity.Achieved15h = (reader.IsDBNull(((int)TwentyOnePointAudit15Column.Achieved15h - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit15Column.Achieved15h - 1)];
			entity.Achieved15i = (reader.IsDBNull(((int)TwentyOnePointAudit15Column.Achieved15i - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit15Column.Achieved15i - 1)];
			entity.Observation15a = (reader.IsDBNull(((int)TwentyOnePointAudit15Column.Observation15a - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit15Column.Observation15a - 1)];
			entity.Observation15b = (reader.IsDBNull(((int)TwentyOnePointAudit15Column.Observation15b - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit15Column.Observation15b - 1)];
			entity.Observation15c = (reader.IsDBNull(((int)TwentyOnePointAudit15Column.Observation15c - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit15Column.Observation15c - 1)];
			entity.Observation15d = (reader.IsDBNull(((int)TwentyOnePointAudit15Column.Observation15d - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit15Column.Observation15d - 1)];
			entity.Observation15e = (reader.IsDBNull(((int)TwentyOnePointAudit15Column.Observation15e - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit15Column.Observation15e - 1)];
			entity.Observation15f = (reader.IsDBNull(((int)TwentyOnePointAudit15Column.Observation15f - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit15Column.Observation15f - 1)];
			entity.Observation15g = (reader.IsDBNull(((int)TwentyOnePointAudit15Column.Observation15g - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit15Column.Observation15g - 1)];
			entity.Observation15h = (reader.IsDBNull(((int)TwentyOnePointAudit15Column.Observation15h - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit15Column.Observation15h - 1)];
			entity.Observation15i = (reader.IsDBNull(((int)TwentyOnePointAudit15Column.Observation15i - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit15Column.Observation15i - 1)];
			entity.TotalScore = (reader.IsDBNull(((int)TwentyOnePointAudit15Column.TotalScore - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit15Column.TotalScore - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.TwentyOnePointAudit15"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.TwentyOnePointAudit15"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.TwentyOnePointAudit15 entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.Id = (System.Int32)dataRow["ID"];
			entity.Achieved15a = Convert.IsDBNull(dataRow["Achieved15a"]) ? null : (System.Int32?)dataRow["Achieved15a"];
			entity.Achieved15b = Convert.IsDBNull(dataRow["Achieved15b"]) ? null : (System.Int32?)dataRow["Achieved15b"];
			entity.Achieved15c = Convert.IsDBNull(dataRow["Achieved15c"]) ? null : (System.Int32?)dataRow["Achieved15c"];
			entity.Achieved15d = Convert.IsDBNull(dataRow["Achieved15d"]) ? null : (System.Int32?)dataRow["Achieved15d"];
			entity.Achieved15e = Convert.IsDBNull(dataRow["Achieved15e"]) ? null : (System.Int32?)dataRow["Achieved15e"];
			entity.Achieved15f = Convert.IsDBNull(dataRow["Achieved15f"]) ? null : (System.Int32?)dataRow["Achieved15f"];
			entity.Achieved15g = Convert.IsDBNull(dataRow["Achieved15g"]) ? null : (System.Int32?)dataRow["Achieved15g"];
			entity.Achieved15h = Convert.IsDBNull(dataRow["Achieved15h"]) ? null : (System.Int32?)dataRow["Achieved15h"];
			entity.Achieved15i = Convert.IsDBNull(dataRow["Achieved15i"]) ? null : (System.Int32?)dataRow["Achieved15i"];
			entity.Observation15a = Convert.IsDBNull(dataRow["Observation15a"]) ? null : (System.String)dataRow["Observation15a"];
			entity.Observation15b = Convert.IsDBNull(dataRow["Observation15b"]) ? null : (System.String)dataRow["Observation15b"];
			entity.Observation15c = Convert.IsDBNull(dataRow["Observation15c"]) ? null : (System.String)dataRow["Observation15c"];
			entity.Observation15d = Convert.IsDBNull(dataRow["Observation15d"]) ? null : (System.String)dataRow["Observation15d"];
			entity.Observation15e = Convert.IsDBNull(dataRow["Observation15e"]) ? null : (System.String)dataRow["Observation15e"];
			entity.Observation15f = Convert.IsDBNull(dataRow["Observation15f"]) ? null : (System.String)dataRow["Observation15f"];
			entity.Observation15g = Convert.IsDBNull(dataRow["Observation15g"]) ? null : (System.String)dataRow["Observation15g"];
			entity.Observation15h = Convert.IsDBNull(dataRow["Observation15h"]) ? null : (System.String)dataRow["Observation15h"];
			entity.Observation15i = Convert.IsDBNull(dataRow["Observation15i"]) ? null : (System.String)dataRow["Observation15i"];
			entity.TotalScore = Convert.IsDBNull(dataRow["TotalScore"]) ? null : (System.Int32?)dataRow["TotalScore"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.TwentyOnePointAudit15"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.TwentyOnePointAudit15 Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.TwentyOnePointAudit15 entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetById methods when available
			
			#region TwentyOnePointAuditCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<TwentyOnePointAudit>|TwentyOnePointAuditCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'TwentyOnePointAuditCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.TwentyOnePointAuditCollection = DataRepository.TwentyOnePointAuditProvider.GetByPoint15Id(transactionManager, entity.Id);

				if (deep && entity.TwentyOnePointAuditCollection.Count > 0)
				{
					deepHandles.Add("TwentyOnePointAuditCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<TwentyOnePointAudit>) DataRepository.TwentyOnePointAuditProvider.DeepLoad,
						new object[] { transactionManager, entity.TwentyOnePointAuditCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.TwentyOnePointAudit15 object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.TwentyOnePointAudit15 instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.TwentyOnePointAudit15 Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.TwentyOnePointAudit15 entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<TwentyOnePointAudit>
				if (CanDeepSave(entity.TwentyOnePointAuditCollection, "List<TwentyOnePointAudit>|TwentyOnePointAuditCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(TwentyOnePointAudit child in entity.TwentyOnePointAuditCollection)
					{
						if(child.Point15IdSource != null)
						{
							child.Point15Id = child.Point15IdSource.Id;
						}
						else
						{
							child.Point15Id = entity.Id;
						}

					}

					if (entity.TwentyOnePointAuditCollection.Count > 0 || entity.TwentyOnePointAuditCollection.DeletedItems.Count > 0)
					{
						//DataRepository.TwentyOnePointAuditProvider.Save(transactionManager, entity.TwentyOnePointAuditCollection);
						
						deepHandles.Add("TwentyOnePointAuditCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< TwentyOnePointAudit >) DataRepository.TwentyOnePointAuditProvider.DeepSave,
							new object[] { transactionManager, entity.TwentyOnePointAuditCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region TwentyOnePointAudit15ChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.TwentyOnePointAudit15</c>
	///</summary>
	public enum TwentyOnePointAudit15ChildEntityTypes
	{

		///<summary>
		/// Collection of <c>TwentyOnePointAudit15</c> as OneToMany for TwentyOnePointAuditCollection
		///</summary>
		[ChildEntityType(typeof(TList<TwentyOnePointAudit>))]
		TwentyOnePointAuditCollection,
	}
	
	#endregion TwentyOnePointAudit15ChildEntityTypes
	
	#region TwentyOnePointAudit15FilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;TwentyOnePointAudit15Column&gt;"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit15"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAudit15FilterBuilder : SqlFilterBuilder<TwentyOnePointAudit15Column>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit15FilterBuilder class.
		/// </summary>
		public TwentyOnePointAudit15FilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit15FilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public TwentyOnePointAudit15FilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit15FilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public TwentyOnePointAudit15FilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion TwentyOnePointAudit15FilterBuilder
	
	#region TwentyOnePointAudit15ParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;TwentyOnePointAudit15Column&gt;"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit15"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAudit15ParameterBuilder : ParameterizedSqlFilterBuilder<TwentyOnePointAudit15Column>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit15ParameterBuilder class.
		/// </summary>
		public TwentyOnePointAudit15ParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit15ParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public TwentyOnePointAudit15ParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit15ParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public TwentyOnePointAudit15ParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion TwentyOnePointAudit15ParameterBuilder
	
	#region TwentyOnePointAudit15SortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;TwentyOnePointAudit15Column&gt;"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit15"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class TwentyOnePointAudit15SortBuilder : SqlSortBuilder<TwentyOnePointAudit15Column>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit15SqlSortBuilder class.
		/// </summary>
		public TwentyOnePointAudit15SortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion TwentyOnePointAudit15SortBuilder
	
} // end namespace
