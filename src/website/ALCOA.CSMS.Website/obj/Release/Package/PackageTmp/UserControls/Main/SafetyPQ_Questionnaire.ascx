<%@ Control Language="C#" AutoEventWireup="true" Inherits="UserControls_SafetyPQ_Questionnaire" CodeBehind="SafetyPQ_Questionnaire.ascx.cs" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dxtc" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dxw" %>
<%@ Register Src="SqQuestionnaire/QuestionnaireList.ascx" TagName="QuestionnaireList_WorkList"
    TagPrefix="uc2" %>
<%@ Register Src="SqQuestionnaire/QuestionnaireList_Completed.ascx" TagName="QuestionnaireList_Completed"
    TagPrefix="uc3" %>
<%@ Register Src="../Other/ExportButtons.ascx" TagName="ExportButtons" TagPrefix="uc1" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="System.Web.Extensions" Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallbackPanel" TagPrefix="dxcp" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dxp" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>

<style type="text/css">
    .style1 {
        width: 204px;
        height: 28px;
    }

    .style2 {
        width: 100px;
        height: 28px;
    }

    .style3 {
        width: 88px;
        height: 28px;
    }

    .style4 {
        width: 200px;
        height: 28px;
    }

    .styleSearchButton {
        float: right;
    }
</style>

<asp:Panel runat="server" ID="pnlMain">
    <table border="0" cellpadding="2" cellspacing="0" width="890px">
        <tr>
            <td class="pageName" colspan="3" style="height: 17px; width: 890px;">
                <span class="bodycopy"><span class="title">Safety Qualification</span><br />
                    <span class="date">Questionnaire</span><br />
                    <img src="images/grfc_dottedline.gif" width="24" height="1">&nbsp;</span>
            </td>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 890px">
        <tr>
            <td colspan="2" style="text-align: right; width: 890px;">
                <div align="right">
                    <table border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td style="padding-right: 10px; text-align: right" class="style1">
                                <span style="font-size: 13px"><strong>Initiate Questionnaire for:</strong></span></td>
                            <td style="padding-right: 5px; vertical-align: middle; text-align: center;"
                                class="style2">
                                <dxe:ASPxButton ID="btn1" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                    CssPostfix="Office2003Blue" Text="Contractor" Width="130px"
                                    AutoPostBack="False" PostBackUrl="javascript:void(0);"
                                    UseSubmitBehavior="False"
                                    SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"></dxe:ASPxButton>
                            </td>
                            <td style="padding-left: 5px; padding-right: 60px;" class="style3">
                                <dxe:ASPxButton ID="btn2" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                    CssPostfix="Office2003Blue" Text="Sub Contractor" Width="112px" AutoPostBack="False"
                                    PostBackUrl="javascript:void(0);" UseSubmitBehavior="False" Enabled="False"
                                    SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"></dxe:ASPxButton>
                            </td>
                            <td style="padding-left: 10px;" class="style4">
                                <dxe:ASPxHyperLink ID="ASPxHyperLink1" runat="server" Height="22px" NavigateUrl="javascript:popUp('PopUps/SafetyPQ_Definitions.aspx');"
                                    Text="Definitions and Instructions"></dxe:ASPxHyperLink>
                                <img src="Images/help.png" /></td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="width: 890px; height: 1px; text-align: center">
                <asp:Label ID="Label1" runat="server" Text="nb. You may only initiate a new questionnaire if no Incomplete Questionnaires exist. " Visible="False"></asp:Label></td>
        </tr>
        <tr>
            <td align="right" class="pageName" style="width: 890px; padding-top: 2px; height: 30px; text-align: left">

                <dxtc:ASPxPageControl ID="ASPxPageControl1" runat="server" ActiveTabIndex="0"
                    CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue"
                    Width="500px"><TabPages><dxtc:TabPage Name="NotAssessmentComplete" Text="Work List"><ContentCollection><dxw:ContentControl runat="server"><uc2:QuestionnaireList_WorkList ID="QuestionnaireList_NotAssessmentComplete" runat="server" /></dxw:ContentControl></ContentCollection></dxtc:TabPage><dxtc:TabPage Name="AssessmentComplete" Text="Assign Company Status"><ContentCollection><dxw:ContentControl runat="server"><uc3:QuestionnaireList_Completed ID="QuestionnaireList_AssessmentComplete" runat="server" /></dxw:ContentControl></ContentCollection></dxtc:TabPage></TabPages><LoadingPanelImage Url="~/App_Themes/Office/Web/Loading.gif"></LoadingPanelImage><LoadingPanelStyle ImageSpacing="6px"></LoadingPanelStyle><ContentStyle><Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px" /></ContentStyle></dxtc:ASPxPageControl>
            </td>
        </tr>
    </table>
    <asp:SqlDataSource ID="sqldsCompaniesList" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
        SelectCommand="_Companies_GetCompanyNameList" SelectCommandType="StoredProcedure"></asp:SqlDataSource>

    <dxpc:ASPxPopupControl ID="ASPxPopupControl1" runat="server" AllowDragging="True"
        AllowResize="True" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
        CssPostfix="Office2003Blue" EnableHotTrack="False"
        HeaderText="Primary Contractor - Initiate Questionnaire" Modal="True" PopupElementID="btn1"
        PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter"
        Width="700px" Style="text-align: center"
        SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"><ContentCollection><dxpc:PopupControlContentControl runat="server"><asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional"><contenttemplate><table cellpadding="2" cellspacing="1" class="templateTable" style="border-right: #c2d4da 1px solid;
        border-top: #c2d4da 1px solid; border-left: #c2d4da 1px solid; border-bottom: #c2d4da 1px solid" width="700"><tr><td class="templateCaption" colspan="2" 
                style="text-align: left; height: 1px; width: 560px;"><asp:Label ID="lblSave" runat="server" Font-Italic="True" ForeColor="Red"></asp:Label></td></tr><tr><td colspan="3" rowspan="1" style="height: 20px; text-align: left">Please search by ABN and then select the Company Name from the Procurement Vendor List:</td></tr><tr><td class="templateCaption" style="width: 260px; text-align: right;"><strong>ABN:</strong> </td><td style="width: 300px; text-align: left;"><div style="float:left; padding-right:10px"><dxe:ASPxTextBox ID="tbAbn" runat="server" Width="200px" 
                    OnValidation="tbAbn_OnValidation" TabIndex="3" 
                    CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
                    CssPostfix="Office2003Blue" 
                    SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" ><ValidationSettings Display="Dynamic" ValidationGroup="k" CausesValidation="True" SetFocusOnError="True" EnableCustomValidation="True"><RequiredField IsRequired="True" /></ValidationSettings><MaskSettings ErrorText="Invalid ABN" Mask="00 000 000 000" /></dxe:ASPxTextBox></div><div style="float:left"><dxe:ASPxButton ID="btnSearch" CssClass="styleSearchButton" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                    CssPostfix="Office2003Blue" Height="10px" OnClick="btnSearch_Click" Text="Search"
                    Width="50px" Font-Bold="True" TabIndex="4" ValidationGroup="k"
                    SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" Enabled="true"  CausesValidation="true" ></dxe:ASPxButton></div></td><td rowspan="2" style="width: 140px; text-align: left"><dxe:ASPxButton ID="btnCreate" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                    CssPostfix="Office2003Blue" Height="37px" OnClick="btnCreate_Click" Text="Create/Initiate"
                    Width="125px" Font-Bold="True" ValidationGroup="c" TabIndex="4" 
                    SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" Enabled="false"></dxe:ASPxButton></td></tr><tr><td class="templateCaption" style="width: 260px; text-align: right;"><strong>Company Name:</strong> </td><td style="width: 300px; text-align: left;"><dxe:ASPxComboBox ID="tbCompanyName" runat="server" ClientInstanceName="tbCompanyName"
                    CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue"
                     IncrementalFilteringMode="StartsWith"
                     Width="250px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" Enabled="false"><loadingpanelimage url="~/App_Themes/Blue/Web/Loading.gif"></loadingpanelimage><buttonstyle width="13px"></buttonstyle><ValidationSettings CausesValidation="True" ErrorDisplayMode="ImageWithTooltip" 
                        SetFocusOnError="True" ValidationGroup="c"><RequiredField ErrorText="Please Select a Company" IsRequired="True" /></ValidationSettings></dxe:ASPxComboBox></td></tr><tr><td class="templateCaption" style="text-align: left;" colspan="2"><asp:Label ID="lblSave2" runat="server" Font-Italic="True" ForeColor="Red"></asp:Label></td><td rowspan="1" style="width: 140px; text-align: right"></td></tr></table></contenttemplate><triggers><asp:AsyncPostBackTrigger ControlID="btnCreate" EventName="Click" /><asp:AsyncPostBackTrigger ControlID="btnSearch" EventName="Click" /></triggers></asp:UpdatePanel></dxpc:PopupControlContentControl></ContentCollection><HeaderStyle><Paddings PaddingRight="6px"></Paddings></HeaderStyle></dxpc:ASPxPopupControl>
    <dxpc:ASPxPopupControl ID="ASPxPopupControl2" runat="server" AllowDragging="True"
        AllowResize="True" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
        CssPostfix="Office2003Blue" EnableHotTrack="False"
        HeaderText="Sub Contractor - Initiate Questionnaire" Modal="True" PopupElementID="btn2"
        PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" Style="text-align: center"
        Width="700px"
        SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"><SizeGripImage Width="16px" /><ContentCollection><dxpc:PopupControlContentControl runat="server"><asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional"><contenttemplate><table cellpadding="2" cellspacing="1" class="templateTable" style="border-right: #c2d4da 1px solid;
                            border-top: #c2d4da 1px solid; border-left: #c2d4da 1px solid; border-bottom: #c2d4da 1px solid"
                            width="700"><tr><td colspan="3" rowspan="1" style="height: 20px; text-align: left">Please enter the Company Name and ABN below to initiate Questionnaire:</td></tr><tr><td class="templateCaption" style="width: 260px; text-align: right"><strong>Company Name:</strong> </td><td style="width: 300px; text-align: left"><dxe:ASPxTextBox ID="tbCompanyName2" runat="server" TabIndex="2" Width="250px" 
                                        CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
                                        CssPostfix="Office2003Blue" 
                                        SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"><ValidationSettings CausesValidation="True" SetFocusOnError="True" ValidationGroup="c2"><RequiredField IsRequired="True" /></ValidationSettings></dxe:ASPxTextBox></td><td rowspan="2" style="width: 140px; text-align: left"><dxe:ASPxButton ID="btnCreate2" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                        CssPostfix="Office2003Blue" Font-Bold="True" Height="37px"
                                        OnClick="btnCreate2_Click" TabIndex="4" Text="Create/Initiate" ValidationGroup="c2"
                                        Width="125px" 
                                        SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"></dxe:ASPxButton></td></tr><tr><td class="templateCaption" style="width: 260px; text-align: right"><strong>ABN:</strong> </td><td style="width: 300px; text-align: left"><dxe:ASPxTextBox ID="tbAbn2" runat="server" OnValidation="tbAbn2_OnValidation" TabIndex="3"
                                        Width="250px" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
                                        CssPostfix="Office2003Blue" 
                                        SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"><ValidationSettings CausesValidation="True" EnableCustomValidation="True" SetFocusOnError="True"
                                            ValidationGroup="c2"><RequiredField IsRequired="True" /></ValidationSettings><MaskSettings ErrorText="Invalid ABN" Mask="00 000 000 000" /></dxe:ASPxTextBox></td></tr><tr><td class="templateCaption" colspan="2" style="text-align: left"><asp:Label ID="lblSave2_2" runat="server" Font-Italic="True" ForeColor="Red"></asp:Label></td><td rowspan="1" style="width: 140px; text-align: right"></td></tr></table><asp:SqlDataSource ID="dsDirectCompanyQualifiedDatabase" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
                            SelectCommand="_QuestionnaireWithLocationApprovalView_Companies" SelectCommandType="StoredProcedure"></asp:SqlDataSource><asp:SqlDataSource ID="companyNamesDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
                            SelectCommand="_QuestionnaireWithLocationApprovalView_Companies" SelectCommandType="StoredProcedure"></asp:SqlDataSource></contenttemplate><triggers><asp:AsyncPostBackTrigger ControlID="btnCreate2" EventName="Click" /></triggers></asp:UpdatePanel></dxpc:PopupControlContentControl></ContentCollection><CloseButtonImage Width="13px" /><HeaderStyle><Paddings PaddingRight="6px" /></HeaderStyle></dxpc:ASPxPopupControl>
</asp:Panel>
<br />
