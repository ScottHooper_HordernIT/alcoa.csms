﻿#region Using directives

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="UsersProcurementEscalationListProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract class UsersProcurementEscalationListProviderBaseCore : EntityViewProviderBase<UsersProcurementEscalationList>
	{
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions
		
		/*
		///<summary>
		/// Fill an VList&lt;UsersProcurementEscalationList&gt; From a DataSet
		///</summary>
		/// <param name="dataSet">the DataSet</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList&lt;UsersProcurementEscalationList&gt;"/></returns>
		protected static VList&lt;UsersProcurementEscalationList&gt; Fill(DataSet dataSet, VList<UsersProcurementEscalationList> rows, int start, int pagelen)
		{
			if (dataSet.Tables.Count == 1)
			{
				return Fill(dataSet.Tables[0], rows, start, pagelen);
			}
			else
			{
				return new VList<UsersProcurementEscalationList>();
			}	
		}
		
		
		///<summary>
		/// Fill an VList&lt;UsersProcurementEscalationList&gt; From a DataTable
		///</summary>
		/// <param name="dataTable">the DataTable that hold the data.</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList<UsersProcurementEscalationList>"/></returns>
		protected static VList&lt;UsersProcurementEscalationList&gt; Fill(DataTable dataTable, VList<UsersProcurementEscalationList> rows, int start, int pagelen)
		{
			int recordnum = 0;
			
			System.Collections.IEnumerator dataRows =  dataTable.Rows.GetEnumerator();
			
			while (dataRows.MoveNext() && (pagelen != 0))
			{
				if(recordnum >= start)
				{
					DataRow row = (DataRow)dataRows.Current;
				
					UsersProcurementEscalationList c = new UsersProcurementEscalationList();
					c.UsersProcurementId = (Convert.IsDBNull(row["UsersProcurementId"]))?(int)0:(System.Int32)row["UsersProcurementId"];
					c.UserId = (Convert.IsDBNull(row["UserId"]))?(int)0:(System.Int32)row["UserId"];
					c.Enabled = (Convert.IsDBNull(row["Enabled"]))?false:(System.Boolean)row["Enabled"];
					c.SiteId = (Convert.IsDBNull(row["SiteId"]))?(int)0:(System.Int32?)row["SiteId"];
					c.RegionId = (Convert.IsDBNull(row["RegionId"]))?(int)0:(System.Int32?)row["RegionId"];
					c.SupervisorUserId = (Convert.IsDBNull(row["SupervisorUserId"]))?(int)0:(System.Int32?)row["SupervisorUserId"];
					c.Level2UserId = (Convert.IsDBNull(row["Level2UserId"]))?(int)0:(System.Int32?)row["Level2UserId"];
					c.Level3UserId = (Convert.IsDBNull(row["Level3UserId"]))?(int)0:(System.Int32?)row["Level3UserId"];
					c.Level4UserId = (Convert.IsDBNull(row["Level4UserId"]))?(int)0:(System.Int32?)row["Level4UserId"];
					c.Level5UserId = (Convert.IsDBNull(row["Level5UserId"]))?(int)0:(System.Int32?)row["Level5UserId"];
					c.Level6UserId = (Convert.IsDBNull(row["Level6UserId"]))?(int)0:(System.Int32?)row["Level6UserId"];
					c.Level7UserId = (Convert.IsDBNull(row["Level7UserId"]))?(int)0:(System.Int32?)row["Level7UserId"];
					c.AcceptChanges();
					rows.Add(c);
					pagelen -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		*/	
						
		///<summary>
		/// Fill an <see cref="VList&lt;UsersProcurementEscalationList&gt;"/> From a DataReader.
		///</summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pageLength">number of row.</param>
		///<returns>a <see cref="VList&lt;UsersProcurementEscalationList&gt;"/></returns>
		protected VList<UsersProcurementEscalationList> Fill(IDataReader reader, VList<UsersProcurementEscalationList> rows, int start, int pageLength)
		{
			int recordnum = 0;
			while (reader.Read() && (pageLength != 0))
			{
				if(recordnum >= start)
				{
					UsersProcurementEscalationList entity = null;
					if (DataRepository.Provider.UseEntityFactory)
					{
						entity = EntityManager.CreateViewEntity<UsersProcurementEscalationList>("UsersProcurementEscalationList",  DataRepository.Provider.EntityCreationalFactoryType); 
					}
					else
					{
						entity = new UsersProcurementEscalationList();
					}
					
					entity.SuppressEntityEvents = true;

					entity.UsersProcurementId = (System.Int32)reader[((int)UsersProcurementEscalationListColumn.UsersProcurementId)];
					//entity.UsersProcurementId = (Convert.IsDBNull(reader["UsersProcurementId"]))?(int)0:(System.Int32)reader["UsersProcurementId"];
					entity.UserId = (System.Int32)reader[((int)UsersProcurementEscalationListColumn.UserId)];
					//entity.UserId = (Convert.IsDBNull(reader["UserId"]))?(int)0:(System.Int32)reader["UserId"];
					entity.Enabled = (System.Boolean)reader[((int)UsersProcurementEscalationListColumn.Enabled)];
					//entity.Enabled = (Convert.IsDBNull(reader["Enabled"]))?false:(System.Boolean)reader["Enabled"];
					entity.SiteId = (reader.IsDBNull(((int)UsersProcurementEscalationListColumn.SiteId)))?null:(System.Int32?)reader[((int)UsersProcurementEscalationListColumn.SiteId)];
					//entity.SiteId = (Convert.IsDBNull(reader["SiteId"]))?(int)0:(System.Int32?)reader["SiteId"];
					entity.RegionId = (reader.IsDBNull(((int)UsersProcurementEscalationListColumn.RegionId)))?null:(System.Int32?)reader[((int)UsersProcurementEscalationListColumn.RegionId)];
					//entity.RegionId = (Convert.IsDBNull(reader["RegionId"]))?(int)0:(System.Int32?)reader["RegionId"];
					entity.SupervisorUserId = (reader.IsDBNull(((int)UsersProcurementEscalationListColumn.SupervisorUserId)))?null:(System.Int32?)reader[((int)UsersProcurementEscalationListColumn.SupervisorUserId)];
					//entity.SupervisorUserId = (Convert.IsDBNull(reader["SupervisorUserId"]))?(int)0:(System.Int32?)reader["SupervisorUserId"];
					entity.Level2UserId = (reader.IsDBNull(((int)UsersProcurementEscalationListColumn.Level2UserId)))?null:(System.Int32?)reader[((int)UsersProcurementEscalationListColumn.Level2UserId)];
					//entity.Level2UserId = (Convert.IsDBNull(reader["Level2UserId"]))?(int)0:(System.Int32?)reader["Level2UserId"];
					entity.Level3UserId = (reader.IsDBNull(((int)UsersProcurementEscalationListColumn.Level3UserId)))?null:(System.Int32?)reader[((int)UsersProcurementEscalationListColumn.Level3UserId)];
					//entity.Level3UserId = (Convert.IsDBNull(reader["Level3UserId"]))?(int)0:(System.Int32?)reader["Level3UserId"];
					entity.Level4UserId = (reader.IsDBNull(((int)UsersProcurementEscalationListColumn.Level4UserId)))?null:(System.Int32?)reader[((int)UsersProcurementEscalationListColumn.Level4UserId)];
					//entity.Level4UserId = (Convert.IsDBNull(reader["Level4UserId"]))?(int)0:(System.Int32?)reader["Level4UserId"];
					entity.Level5UserId = (reader.IsDBNull(((int)UsersProcurementEscalationListColumn.Level5UserId)))?null:(System.Int32?)reader[((int)UsersProcurementEscalationListColumn.Level5UserId)];
					//entity.Level5UserId = (Convert.IsDBNull(reader["Level5UserId"]))?(int)0:(System.Int32?)reader["Level5UserId"];
					entity.Level6UserId = (reader.IsDBNull(((int)UsersProcurementEscalationListColumn.Level6UserId)))?null:(System.Int32?)reader[((int)UsersProcurementEscalationListColumn.Level6UserId)];
					//entity.Level6UserId = (Convert.IsDBNull(reader["Level6UserId"]))?(int)0:(System.Int32?)reader["Level6UserId"];
					entity.Level7UserId = (reader.IsDBNull(((int)UsersProcurementEscalationListColumn.Level7UserId)))?null:(System.Int32?)reader[((int)UsersProcurementEscalationListColumn.Level7UserId)];
					//entity.Level7UserId = (Convert.IsDBNull(reader["Level7UserId"]))?(int)0:(System.Int32?)reader["Level7UserId"];
					entity.AcceptChanges();
					entity.SuppressEntityEvents = false;
					
					rows.Add(entity);
					pageLength -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		
		
		/// <summary>
		/// Refreshes the <see cref="UsersProcurementEscalationList"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="UsersProcurementEscalationList"/> object to refresh.</param>
		protected void RefreshEntity(IDataReader reader, UsersProcurementEscalationList entity)
		{
			reader.Read();
			entity.UsersProcurementId = (System.Int32)reader[((int)UsersProcurementEscalationListColumn.UsersProcurementId)];
			//entity.UsersProcurementId = (Convert.IsDBNull(reader["UsersProcurementId"]))?(int)0:(System.Int32)reader["UsersProcurementId"];
			entity.UserId = (System.Int32)reader[((int)UsersProcurementEscalationListColumn.UserId)];
			//entity.UserId = (Convert.IsDBNull(reader["UserId"]))?(int)0:(System.Int32)reader["UserId"];
			entity.Enabled = (System.Boolean)reader[((int)UsersProcurementEscalationListColumn.Enabled)];
			//entity.Enabled = (Convert.IsDBNull(reader["Enabled"]))?false:(System.Boolean)reader["Enabled"];
			entity.SiteId = (reader.IsDBNull(((int)UsersProcurementEscalationListColumn.SiteId)))?null:(System.Int32?)reader[((int)UsersProcurementEscalationListColumn.SiteId)];
			//entity.SiteId = (Convert.IsDBNull(reader["SiteId"]))?(int)0:(System.Int32?)reader["SiteId"];
			entity.RegionId = (reader.IsDBNull(((int)UsersProcurementEscalationListColumn.RegionId)))?null:(System.Int32?)reader[((int)UsersProcurementEscalationListColumn.RegionId)];
			//entity.RegionId = (Convert.IsDBNull(reader["RegionId"]))?(int)0:(System.Int32?)reader["RegionId"];
			entity.SupervisorUserId = (reader.IsDBNull(((int)UsersProcurementEscalationListColumn.SupervisorUserId)))?null:(System.Int32?)reader[((int)UsersProcurementEscalationListColumn.SupervisorUserId)];
			//entity.SupervisorUserId = (Convert.IsDBNull(reader["SupervisorUserId"]))?(int)0:(System.Int32?)reader["SupervisorUserId"];
			entity.Level2UserId = (reader.IsDBNull(((int)UsersProcurementEscalationListColumn.Level2UserId)))?null:(System.Int32?)reader[((int)UsersProcurementEscalationListColumn.Level2UserId)];
			//entity.Level2UserId = (Convert.IsDBNull(reader["Level2UserId"]))?(int)0:(System.Int32?)reader["Level2UserId"];
			entity.Level3UserId = (reader.IsDBNull(((int)UsersProcurementEscalationListColumn.Level3UserId)))?null:(System.Int32?)reader[((int)UsersProcurementEscalationListColumn.Level3UserId)];
			//entity.Level3UserId = (Convert.IsDBNull(reader["Level3UserId"]))?(int)0:(System.Int32?)reader["Level3UserId"];
			entity.Level4UserId = (reader.IsDBNull(((int)UsersProcurementEscalationListColumn.Level4UserId)))?null:(System.Int32?)reader[((int)UsersProcurementEscalationListColumn.Level4UserId)];
			//entity.Level4UserId = (Convert.IsDBNull(reader["Level4UserId"]))?(int)0:(System.Int32?)reader["Level4UserId"];
			entity.Level5UserId = (reader.IsDBNull(((int)UsersProcurementEscalationListColumn.Level5UserId)))?null:(System.Int32?)reader[((int)UsersProcurementEscalationListColumn.Level5UserId)];
			//entity.Level5UserId = (Convert.IsDBNull(reader["Level5UserId"]))?(int)0:(System.Int32?)reader["Level5UserId"];
			entity.Level6UserId = (reader.IsDBNull(((int)UsersProcurementEscalationListColumn.Level6UserId)))?null:(System.Int32?)reader[((int)UsersProcurementEscalationListColumn.Level6UserId)];
			//entity.Level6UserId = (Convert.IsDBNull(reader["Level6UserId"]))?(int)0:(System.Int32?)reader["Level6UserId"];
			entity.Level7UserId = (reader.IsDBNull(((int)UsersProcurementEscalationListColumn.Level7UserId)))?null:(System.Int32?)reader[((int)UsersProcurementEscalationListColumn.Level7UserId)];
			//entity.Level7UserId = (Convert.IsDBNull(reader["Level7UserId"]))?(int)0:(System.Int32?)reader["Level7UserId"];
			reader.Close();
	
			entity.AcceptChanges();
		}
		
		/*
		/// <summary>
		/// Refreshes the <see cref="UsersProcurementEscalationList"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="UsersProcurementEscalationList"/> object.</param>
		protected static void RefreshEntity(DataSet dataSet, UsersProcurementEscalationList entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.UsersProcurementId = (Convert.IsDBNull(dataRow["UsersProcurementId"]))?(int)0:(System.Int32)dataRow["UsersProcurementId"];
			entity.UserId = (Convert.IsDBNull(dataRow["UserId"]))?(int)0:(System.Int32)dataRow["UserId"];
			entity.Enabled = (Convert.IsDBNull(dataRow["Enabled"]))?false:(System.Boolean)dataRow["Enabled"];
			entity.SiteId = (Convert.IsDBNull(dataRow["SiteId"]))?(int)0:(System.Int32?)dataRow["SiteId"];
			entity.RegionId = (Convert.IsDBNull(dataRow["RegionId"]))?(int)0:(System.Int32?)dataRow["RegionId"];
			entity.SupervisorUserId = (Convert.IsDBNull(dataRow["SupervisorUserId"]))?(int)0:(System.Int32?)dataRow["SupervisorUserId"];
			entity.Level2UserId = (Convert.IsDBNull(dataRow["Level2UserId"]))?(int)0:(System.Int32?)dataRow["Level2UserId"];
			entity.Level3UserId = (Convert.IsDBNull(dataRow["Level3UserId"]))?(int)0:(System.Int32?)dataRow["Level3UserId"];
			entity.Level4UserId = (Convert.IsDBNull(dataRow["Level4UserId"]))?(int)0:(System.Int32?)dataRow["Level4UserId"];
			entity.Level5UserId = (Convert.IsDBNull(dataRow["Level5UserId"]))?(int)0:(System.Int32?)dataRow["Level5UserId"];
			entity.Level6UserId = (Convert.IsDBNull(dataRow["Level6UserId"]))?(int)0:(System.Int32?)dataRow["Level6UserId"];
			entity.Level7UserId = (Convert.IsDBNull(dataRow["Level7UserId"]))?(int)0:(System.Int32?)dataRow["Level7UserId"];
			entity.AcceptChanges();
		}
		*/
			
		#endregion Helper Functions
	}//end class

	#region UsersProcurementEscalationListFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="UsersProcurementEscalationList"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UsersProcurementEscalationListFilterBuilder : SqlFilterBuilder<UsersProcurementEscalationListColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UsersProcurementEscalationListFilterBuilder class.
		/// </summary>
		public UsersProcurementEscalationListFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the UsersProcurementEscalationListFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public UsersProcurementEscalationListFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the UsersProcurementEscalationListFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public UsersProcurementEscalationListFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion UsersProcurementEscalationListFilterBuilder

	#region UsersProcurementEscalationListParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="UsersProcurementEscalationList"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UsersProcurementEscalationListParameterBuilder : ParameterizedSqlFilterBuilder<UsersProcurementEscalationListColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UsersProcurementEscalationListParameterBuilder class.
		/// </summary>
		public UsersProcurementEscalationListParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the UsersProcurementEscalationListParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public UsersProcurementEscalationListParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the UsersProcurementEscalationListParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public UsersProcurementEscalationListParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion UsersProcurementEscalationListParameterBuilder
	
	#region UsersProcurementEscalationListSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="UsersProcurementEscalationList"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class UsersProcurementEscalationListSortBuilder : SqlSortBuilder<UsersProcurementEscalationListColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UsersProcurementEscalationListSqlSortBuilder class.
		/// </summary>
		public UsersProcurementEscalationListSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion UsersProcurementEscalationListSortBuilder

} // end namespace
