﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.AnnualTargetsProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(AnnualTargetsDataSourceDesigner))]
	public class AnnualTargetsDataSource : ProviderDataSource<AnnualTargets, AnnualTargetsKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AnnualTargetsDataSource class.
		/// </summary>
		public AnnualTargetsDataSource() : base(new AnnualTargetsService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the AnnualTargetsDataSourceView used by the AnnualTargetsDataSource.
		/// </summary>
		protected AnnualTargetsDataSourceView AnnualTargetsView
		{
			get { return ( View as AnnualTargetsDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the AnnualTargetsDataSource control invokes to retrieve data.
		/// </summary>
		public AnnualTargetsSelectMethod SelectMethod
		{
			get
			{
				AnnualTargetsSelectMethod selectMethod = AnnualTargetsSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (AnnualTargetsSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the AnnualTargetsDataSourceView class that is to be
		/// used by the AnnualTargetsDataSource.
		/// </summary>
		/// <returns>An instance of the AnnualTargetsDataSourceView class.</returns>
		protected override BaseDataSourceView<AnnualTargets, AnnualTargetsKey> GetNewDataSourceView()
		{
			return new AnnualTargetsDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the AnnualTargetsDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class AnnualTargetsDataSourceView : ProviderDataSourceView<AnnualTargets, AnnualTargetsKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AnnualTargetsDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the AnnualTargetsDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public AnnualTargetsDataSourceView(AnnualTargetsDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal AnnualTargetsDataSource AnnualTargetsOwner
		{
			get { return Owner as AnnualTargetsDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal AnnualTargetsSelectMethod SelectMethod
		{
			get { return AnnualTargetsOwner.SelectMethod; }
			set { AnnualTargetsOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal AnnualTargetsService AnnualTargetsProvider
		{
			get { return Provider as AnnualTargetsService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<AnnualTargets> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<AnnualTargets> results = null;
			AnnualTargets item;
			count = 0;
			
			System.Int32 _annualTargetsId;
			System.Int32 _year;
			System.Int32 _companySiteCategoryId;

			switch ( SelectMethod )
			{
				case AnnualTargetsSelectMethod.Get:
					AnnualTargetsKey entityKey  = new AnnualTargetsKey();
					entityKey.Load(values);
					item = AnnualTargetsProvider.Get(entityKey);
					results = new TList<AnnualTargets>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case AnnualTargetsSelectMethod.GetAll:
                    results = AnnualTargetsProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case AnnualTargetsSelectMethod.GetPaged:
					results = AnnualTargetsProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case AnnualTargetsSelectMethod.Find:
					if ( FilterParameters != null )
						results = AnnualTargetsProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = AnnualTargetsProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case AnnualTargetsSelectMethod.GetByAnnualTargetsId:
					_annualTargetsId = ( values["AnnualTargetsId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["AnnualTargetsId"], typeof(System.Int32)) : (int)0;
					item = AnnualTargetsProvider.GetByAnnualTargetsId(_annualTargetsId);
					results = new TList<AnnualTargets>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				case AnnualTargetsSelectMethod.GetByYear:
					_year = ( values["Year"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["Year"], typeof(System.Int32)) : (int)0;
					results = AnnualTargetsProvider.GetByYear(_year, this.StartIndex, this.PageSize, out count);
					break;
				case AnnualTargetsSelectMethod.GetByYearCompanySiteCategoryId:
					_year = ( values["Year"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["Year"], typeof(System.Int32)) : (int)0;
					_companySiteCategoryId = ( values["CompanySiteCategoryId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["CompanySiteCategoryId"], typeof(System.Int32)) : (int)0;
					item = AnnualTargetsProvider.GetByYearCompanySiteCategoryId(_year, _companySiteCategoryId);
					results = new TList<AnnualTargets>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// FK
				case AnnualTargetsSelectMethod.GetByCompanySiteCategoryId:
					_companySiteCategoryId = ( values["CompanySiteCategoryId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["CompanySiteCategoryId"], typeof(System.Int32)) : (int)0;
					results = AnnualTargetsProvider.GetByCompanySiteCategoryId(_companySiteCategoryId, this.StartIndex, this.PageSize, out count);
					break;
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == AnnualTargetsSelectMethod.Get || SelectMethod == AnnualTargetsSelectMethod.GetByAnnualTargetsId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				AnnualTargets entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					AnnualTargetsProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<AnnualTargets> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			AnnualTargetsProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region AnnualTargetsDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the AnnualTargetsDataSource class.
	/// </summary>
	public class AnnualTargetsDataSourceDesigner : ProviderDataSourceDesigner<AnnualTargets, AnnualTargetsKey>
	{
		/// <summary>
		/// Initializes a new instance of the AnnualTargetsDataSourceDesigner class.
		/// </summary>
		public AnnualTargetsDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public AnnualTargetsSelectMethod SelectMethod
		{
			get { return ((AnnualTargetsDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new AnnualTargetsDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region AnnualTargetsDataSourceActionList

	/// <summary>
	/// Supports the AnnualTargetsDataSourceDesigner class.
	/// </summary>
	internal class AnnualTargetsDataSourceActionList : DesignerActionList
	{
		private AnnualTargetsDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the AnnualTargetsDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public AnnualTargetsDataSourceActionList(AnnualTargetsDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public AnnualTargetsSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion AnnualTargetsDataSourceActionList
	
	#endregion AnnualTargetsDataSourceDesigner
	
	#region AnnualTargetsSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the AnnualTargetsDataSource.SelectMethod property.
	/// </summary>
	public enum AnnualTargetsSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByAnnualTargetsId method.
		/// </summary>
		GetByAnnualTargetsId,
		/// <summary>
		/// Represents the GetByYear method.
		/// </summary>
		GetByYear,
		/// <summary>
		/// Represents the GetByYearCompanySiteCategoryId method.
		/// </summary>
		GetByYearCompanySiteCategoryId,
		/// <summary>
		/// Represents the GetByCompanySiteCategoryId method.
		/// </summary>
		GetByCompanySiteCategoryId
	}
	
	#endregion AnnualTargetsSelectMethod

	#region AnnualTargetsFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AnnualTargets"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AnnualTargetsFilter : SqlFilter<AnnualTargetsColumn>
	{
	}
	
	#endregion AnnualTargetsFilter

	#region AnnualTargetsExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AnnualTargets"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AnnualTargetsExpressionBuilder : SqlExpressionBuilder<AnnualTargetsColumn>
	{
	}
	
	#endregion AnnualTargetsExpressionBuilder	

	#region AnnualTargetsProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;AnnualTargetsChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="AnnualTargets"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AnnualTargetsProperty : ChildEntityProperty<AnnualTargetsChildEntityTypes>
	{
	}
	
	#endregion AnnualTargetsProperty
}

