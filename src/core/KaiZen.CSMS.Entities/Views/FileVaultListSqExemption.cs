﻿#region Using directives

using System;

#endregion

namespace KaiZen.CSMS.Entities
{	
	///<summary>
	/// An object representation of the 'FileVault_List_SqExemption' view. [No description found in the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class FileVaultListSqExemption : FileVaultListSqExemptionBase
	{
		#region Constructors

		///<summary>
		/// Creates a new <see cref="FileVaultListSqExemption"/> instance.
		///</summary>
		public FileVaultListSqExemption():base(){}	
		
		#endregion
	}
}
