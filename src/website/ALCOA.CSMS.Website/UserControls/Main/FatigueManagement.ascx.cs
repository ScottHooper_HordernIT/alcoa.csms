﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Entities;
using DevExpress.Web.ASPxEditors;
using KaiZen.CSMS.Services;
using System.Data;
using DevExpress.Web.ASPxClasses;
using DevExpress.Web.ASPxGridView;
using DevExpress.Data.Filtering;

namespace ALCOA.CSMS.Website.UserControls.Main
{
    public partial class FatigueManagement : System.Web.UI.UserControl
    {
        #region Services

        public Repo.CSMS.Service.Database.IFatigueManagementDetailService FatigueManagementDetailService { get; set; }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                var fatigueManagementDetail = this.FatigueManagementDetailService.GetMaxDateTime(null, i => i.EntryDateTime, null);
                LastUpdated24hLabel.Text = LastUpdated7dLabel.Text = (fatigueManagementDetail != null ? fatigueManagementDetail.Value.ToString("dd/MM/yyyy hh:mm") : "");
            }
        }

        protected void grid_RowCreated(object sender, ASPxGridViewTableRowEventArgs e)
        {
            if (e.RowType == GridViewRowType.Data)
            {
                //ASPxHyperLink hlCompany = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "hlCompany") as ASPxHyperLink;

                int clockId = Convert.ToInt32(grid.GetRowValues(e.VisibleIndex, "ClockId"));
                string fullname = Convert.ToString(grid.GetRowValues(e.VisibleIndex, "FullName"));



                if (e.RowType != GridViewRowType.Data) return;
                ASPxHyperLink hlFullName = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "hlFullName") as ASPxHyperLink;
                if (hlFullName != null)
                    hlFullName.NavigateUrl = String.Format("javascript:popUp('popups/FatigueMgtPopup.aspx?C={0}')", clockId);


                if (hlFullName != null)
                {
                    hlFullName.Text = fullname;
                }

                //e.Row.Cells[0].Attributes.Add("onClick", String.Format("javascript:popUp('PopUps/TrainingMetricsPopup.aspx?kpi={0}&site={1}')", 1, 2 ));
            }
        }

        protected void grid24_RowCreated(object sender, ASPxGridViewTableRowEventArgs e)
        {
            if (e.RowType == GridViewRowType.Data)
            {
                //ASPxHyperLink hlCompany = grid24.FindRowCellTemplateControl(e.VisibleIndex, null, "hlCompany") as ASPxHyperLink;

                int clockId = Convert.ToInt32(grid24.GetRowValues(e.VisibleIndex, "ClockId"));
                string fullname = Convert.ToString(grid24.GetRowValues(e.VisibleIndex, "FullName"));



                if (e.RowType != GridViewRowType.Data) return;
                ASPxHyperLink hlFullName = grid24.FindRowCellTemplateControl(e.VisibleIndex, null, "hl24FullName") as ASPxHyperLink;
                if (hlFullName != null)
                    hlFullName.NavigateUrl = String.Format("javascript:popUp('popups/FatigueMgtPopup.aspx?C={0}')", clockId);


                if (hlFullName != null)
                {
                    hlFullName.Text = fullname;
                }

                //e.Row.Cells[0].Attributes.Add("onClick", String.Format("javascript:popUp('PopUps/TrainingMetricsPopup.aspx?kpi={0}&site={1}')", 1, 2 ));
            }
        }

        protected void grid7_RowCreated(object sender, ASPxGridViewTableRowEventArgs e)
        {
            if (e.RowType == GridViewRowType.Data)
            {
                //ASPxHyperLink hlCompany = grid7.FindRowCellTemplateControl(e.VisibleIndex, null, "hlCompany") as ASPxHyperLink;

                int clockId = Convert.ToInt32(grid7.GetRowValues(e.VisibleIndex, "ClockId"));
                string fullname = Convert.ToString(grid7.GetRowValues(e.VisibleIndex, "FullName"));



                if (e.RowType != GridViewRowType.Data) return;
                ASPxHyperLink hlFullName = grid7.FindRowCellTemplateControl(e.VisibleIndex, null, "hl7FullName") as ASPxHyperLink;
                if (hlFullName != null)
                    hlFullName.NavigateUrl = String.Format("javascript:popUp('popups/FatigueMgtPopup.aspx?C={0}')", clockId);


                if (hlFullName != null)
                {
                    hlFullName.Text = fullname;
                }

                //e.Row.Cells[0].Attributes.Add("onClick", String.Format("javascript:popUp('PopUps/TrainingMetricsPopup.aspx?kpi={0}&site={1}')", 1, 2 ));
            }
        }

        protected void grid24_AutoFilterCellEditorCreate(object sender, ASPxGridViewEditorCreateEventArgs e)
        {
            if (e.Column.FieldName == "TotalTime")
            {
                ComboBoxProperties combo = new ComboBoxProperties();
                e.EditorProperties = combo;
            }
        }

        protected void grid24_AutoFilterCellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
        {
            if (e.Column.FieldName == "TotalTime")
            {
                ASPxComboBox combo = e.Editor as ASPxComboBox;
                combo.ValueType = typeof(string);

                combo.Items.Add("< 12 hours");
                combo.Items.Add("12-14 hours");
                combo.Items.Add("14-16 hours");
                combo.Items.Add("16+ hours");
            }
        }

        protected void grid24_ProcessColumnAutoFilter(object sender, ASPxGridViewAutoFilterEventArgs e)
        {
            if (e.Column.FieldName != "TotalTime") return;
            if (e.Kind == GridViewAutoFilterEventKind.ExtractDisplayText)
            {
                e.Value = Session["value24"].ToString();
                return;
            }
            else
            {
                int start = 0, end = 24;
                string value = e.Value;

                if (value == "< 12 hours")
                    end = 12;
                else if (value == "12-14 hours")
                {
                    start = 12;
                    end = 14;
                }
                else if (value == "14-16 hours")
                {
                    start = 14;
                    end = 16;
                }
                else if (value == "16+ hours")
                {
                    start = 16;
                }
               
                Session["value24"] = value;
                e.Criteria = new GroupOperator(GroupOperatorType.And,
                    new BinaryOperator(e.Column.FieldName, start, BinaryOperatorType.GreaterOrEqual),
                    new BinaryOperator(e.Column.FieldName, end, BinaryOperatorType.Less));

            }
        }

        protected void grid7_AutoFilterCellEditorCreate(object sender, ASPxGridViewEditorCreateEventArgs e)
        {
            if (e.Column.FieldName == "TotalTime")
            {
                ComboBoxProperties combo = new ComboBoxProperties();
                e.EditorProperties = combo;
            }
        }

        protected void grid7_AutoFilterCellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
        {
            if (e.Column.FieldName == "TotalTime")
            {
                ASPxComboBox combo = e.Editor as ASPxComboBox;
                combo.ValueType = typeof(string);

                combo.Items.Add("< 50 hours");
                combo.Items.Add("50-60 hours");
                combo.Items.Add("60-64 hours");
                combo.Items.Add("64+ hours");
            }
        }

        protected void grid7_ProcessColumnAutoFilter(object sender, ASPxGridViewAutoFilterEventArgs e)
        {
            if (e.Column.FieldName != "TotalTime") return;
            if (e.Kind == GridViewAutoFilterEventKind.ExtractDisplayText)
            {
                e.Value = Session["value7"].ToString();
                return;
            }
            else
            {
                int start = 0, end = 168;
                string value = e.Value;

                if (value == "< 50 hours")
                    end = 50;
                else if (value == "50-60 hours")
                {
                    start = 50;
                    end = 60;
                }
                else if (value == "60-64 hours")
                {
                    start = 60;
                    end = 64;
                }
                else if (value == "64+ hours")
                {
                    start = 64;
                }

                Session["value7"] = value;
                e.Criteria = new GroupOperator(GroupOperatorType.And,
                    new BinaryOperator(e.Column.FieldName, start, BinaryOperatorType.GreaterOrEqual),
                    new BinaryOperator(e.Column.FieldName, end, BinaryOperatorType.Less));

            }
        }
    }
}