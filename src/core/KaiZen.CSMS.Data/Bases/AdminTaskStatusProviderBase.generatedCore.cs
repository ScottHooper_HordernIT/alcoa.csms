﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="AdminTaskStatusProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class AdminTaskStatusProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.AdminTaskStatus, KaiZen.CSMS.Entities.AdminTaskStatusKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.AdminTaskStatusKey key)
		{
			return Delete(transactionManager, key.AdminTaskStatusId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_adminTaskStatusId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _adminTaskStatusId)
		{
			return Delete(null, _adminTaskStatusId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_adminTaskStatusId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _adminTaskStatusId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.AdminTaskStatus Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.AdminTaskStatusKey key, int start, int pageLength)
		{
			return GetByAdminTaskStatusId(transactionManager, key.AdminTaskStatusId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_AdminTaskStatus index.
		/// </summary>
		/// <param name="_adminTaskStatusId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdminTaskStatus"/> class.</returns>
		public KaiZen.CSMS.Entities.AdminTaskStatus GetByAdminTaskStatusId(System.Int32 _adminTaskStatusId)
		{
			int count = -1;
			return GetByAdminTaskStatusId(null,_adminTaskStatusId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_AdminTaskStatus index.
		/// </summary>
		/// <param name="_adminTaskStatusId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdminTaskStatus"/> class.</returns>
		public KaiZen.CSMS.Entities.AdminTaskStatus GetByAdminTaskStatusId(System.Int32 _adminTaskStatusId, int start, int pageLength)
		{
			int count = -1;
			return GetByAdminTaskStatusId(null, _adminTaskStatusId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_AdminTaskStatus index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_adminTaskStatusId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdminTaskStatus"/> class.</returns>
		public KaiZen.CSMS.Entities.AdminTaskStatus GetByAdminTaskStatusId(TransactionManager transactionManager, System.Int32 _adminTaskStatusId)
		{
			int count = -1;
			return GetByAdminTaskStatusId(transactionManager, _adminTaskStatusId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_AdminTaskStatus index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_adminTaskStatusId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdminTaskStatus"/> class.</returns>
		public KaiZen.CSMS.Entities.AdminTaskStatus GetByAdminTaskStatusId(TransactionManager transactionManager, System.Int32 _adminTaskStatusId, int start, int pageLength)
		{
			int count = -1;
			return GetByAdminTaskStatusId(transactionManager, _adminTaskStatusId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_AdminTaskStatus index.
		/// </summary>
		/// <param name="_adminTaskStatusId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdminTaskStatus"/> class.</returns>
		public KaiZen.CSMS.Entities.AdminTaskStatus GetByAdminTaskStatusId(System.Int32 _adminTaskStatusId, int start, int pageLength, out int count)
		{
			return GetByAdminTaskStatusId(null, _adminTaskStatusId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_AdminTaskStatus index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_adminTaskStatusId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdminTaskStatus"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.AdminTaskStatus GetByAdminTaskStatusId(TransactionManager transactionManager, System.Int32 _adminTaskStatusId, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_AdminTaskStatusName index.
		/// </summary>
		/// <param name="_statusName"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdminTaskStatus"/> class.</returns>
		public KaiZen.CSMS.Entities.AdminTaskStatus GetByStatusName(System.String _statusName)
		{
			int count = -1;
			return GetByStatusName(null,_statusName, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_AdminTaskStatusName index.
		/// </summary>
		/// <param name="_statusName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdminTaskStatus"/> class.</returns>
		public KaiZen.CSMS.Entities.AdminTaskStatus GetByStatusName(System.String _statusName, int start, int pageLength)
		{
			int count = -1;
			return GetByStatusName(null, _statusName, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_AdminTaskStatusName index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_statusName"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdminTaskStatus"/> class.</returns>
		public KaiZen.CSMS.Entities.AdminTaskStatus GetByStatusName(TransactionManager transactionManager, System.String _statusName)
		{
			int count = -1;
			return GetByStatusName(transactionManager, _statusName, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_AdminTaskStatusName index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_statusName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdminTaskStatus"/> class.</returns>
		public KaiZen.CSMS.Entities.AdminTaskStatus GetByStatusName(TransactionManager transactionManager, System.String _statusName, int start, int pageLength)
		{
			int count = -1;
			return GetByStatusName(transactionManager, _statusName, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_AdminTaskStatusName index.
		/// </summary>
		/// <param name="_statusName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdminTaskStatus"/> class.</returns>
		public KaiZen.CSMS.Entities.AdminTaskStatus GetByStatusName(System.String _statusName, int start, int pageLength, out int count)
		{
			return GetByStatusName(null, _statusName, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_AdminTaskStatusName index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_statusName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdminTaskStatus"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.AdminTaskStatus GetByStatusName(TransactionManager transactionManager, System.String _statusName, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;AdminTaskStatus&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;AdminTaskStatus&gt;"/></returns>
		public static TList<AdminTaskStatus> Fill(IDataReader reader, TList<AdminTaskStatus> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.AdminTaskStatus c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("AdminTaskStatus")
					.Append("|").Append((System.Int32)reader[((int)AdminTaskStatusColumn.AdminTaskStatusId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<AdminTaskStatus>(
					key.ToString(), // EntityTrackingKey
					"AdminTaskStatus",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.AdminTaskStatus();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.AdminTaskStatusId = (System.Int32)reader[((int)AdminTaskStatusColumn.AdminTaskStatusId - 1)];
					c.StatusName = (System.String)reader[((int)AdminTaskStatusColumn.StatusName - 1)];
					c.StatusDesc = (System.String)reader[((int)AdminTaskStatusColumn.StatusDesc - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.AdminTaskStatus"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.AdminTaskStatus"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.AdminTaskStatus entity)
		{
			if (!reader.Read()) return;
			
			entity.AdminTaskStatusId = (System.Int32)reader[((int)AdminTaskStatusColumn.AdminTaskStatusId - 1)];
			entity.StatusName = (System.String)reader[((int)AdminTaskStatusColumn.StatusName - 1)];
			entity.StatusDesc = (System.String)reader[((int)AdminTaskStatusColumn.StatusDesc - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.AdminTaskStatus"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.AdminTaskStatus"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.AdminTaskStatus entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.AdminTaskStatusId = (System.Int32)dataRow["AdminTaskStatusId"];
			entity.StatusName = (System.String)dataRow["StatusName"];
			entity.StatusDesc = (System.String)dataRow["StatusDesc"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.AdminTaskStatus"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.AdminTaskStatus Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.AdminTaskStatus entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetByAdminTaskStatusId methods when available
			
			#region AdminTaskCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<AdminTask>|AdminTaskCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'AdminTaskCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.AdminTaskCollection = DataRepository.AdminTaskProvider.GetByAdminTaskStatusId(transactionManager, entity.AdminTaskStatusId);

				if (deep && entity.AdminTaskCollection.Count > 0)
				{
					deepHandles.Add("AdminTaskCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<AdminTask>) DataRepository.AdminTaskProvider.DeepLoad,
						new object[] { transactionManager, entity.AdminTaskCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.AdminTaskStatus object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.AdminTaskStatus instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.AdminTaskStatus Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.AdminTaskStatus entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<AdminTask>
				if (CanDeepSave(entity.AdminTaskCollection, "List<AdminTask>|AdminTaskCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(AdminTask child in entity.AdminTaskCollection)
					{
						if(child.AdminTaskStatusIdSource != null)
						{
							child.AdminTaskStatusId = child.AdminTaskStatusIdSource.AdminTaskStatusId;
						}
						else
						{
							child.AdminTaskStatusId = entity.AdminTaskStatusId;
						}

					}

					if (entity.AdminTaskCollection.Count > 0 || entity.AdminTaskCollection.DeletedItems.Count > 0)
					{
						//DataRepository.AdminTaskProvider.Save(transactionManager, entity.AdminTaskCollection);
						
						deepHandles.Add("AdminTaskCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< AdminTask >) DataRepository.AdminTaskProvider.DeepSave,
							new object[] { transactionManager, entity.AdminTaskCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region AdminTaskStatusChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.AdminTaskStatus</c>
	///</summary>
	public enum AdminTaskStatusChildEntityTypes
	{

		///<summary>
		/// Collection of <c>AdminTaskStatus</c> as OneToMany for AdminTaskCollection
		///</summary>
		[ChildEntityType(typeof(TList<AdminTask>))]
		AdminTaskCollection,
	}
	
	#endregion AdminTaskStatusChildEntityTypes
	
	#region AdminTaskStatusFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;AdminTaskStatusColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AdminTaskStatus"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AdminTaskStatusFilterBuilder : SqlFilterBuilder<AdminTaskStatusColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AdminTaskStatusFilterBuilder class.
		/// </summary>
		public AdminTaskStatusFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the AdminTaskStatusFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AdminTaskStatusFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AdminTaskStatusFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AdminTaskStatusFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AdminTaskStatusFilterBuilder
	
	#region AdminTaskStatusParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;AdminTaskStatusColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AdminTaskStatus"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AdminTaskStatusParameterBuilder : ParameterizedSqlFilterBuilder<AdminTaskStatusColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AdminTaskStatusParameterBuilder class.
		/// </summary>
		public AdminTaskStatusParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the AdminTaskStatusParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AdminTaskStatusParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AdminTaskStatusParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AdminTaskStatusParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AdminTaskStatusParameterBuilder
	
	#region AdminTaskStatusSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;AdminTaskStatusColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AdminTaskStatus"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class AdminTaskStatusSortBuilder : SqlSortBuilder<AdminTaskStatusColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AdminTaskStatusSqlSortBuilder class.
		/// </summary>
		public AdminTaskStatusSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion AdminTaskStatusSortBuilder
	
} // end namespace
