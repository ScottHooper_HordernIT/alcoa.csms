﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.AdminTaskProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(AdminTaskDataSourceDesigner))]
	public class AdminTaskDataSource : ProviderDataSource<AdminTask, AdminTaskKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AdminTaskDataSource class.
		/// </summary>
		public AdminTaskDataSource() : base(new AdminTaskService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the AdminTaskDataSourceView used by the AdminTaskDataSource.
		/// </summary>
		protected AdminTaskDataSourceView AdminTaskView
		{
			get { return ( View as AdminTaskDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the AdminTaskDataSource control invokes to retrieve data.
		/// </summary>
		public AdminTaskSelectMethod SelectMethod
		{
			get
			{
				AdminTaskSelectMethod selectMethod = AdminTaskSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (AdminTaskSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the AdminTaskDataSourceView class that is to be
		/// used by the AdminTaskDataSource.
		/// </summary>
		/// <returns>An instance of the AdminTaskDataSourceView class.</returns>
		protected override BaseDataSourceView<AdminTask, AdminTaskKey> GetNewDataSourceView()
		{
			return new AdminTaskDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the AdminTaskDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class AdminTaskDataSourceView : ProviderDataSourceView<AdminTask, AdminTaskKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AdminTaskDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the AdminTaskDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public AdminTaskDataSourceView(AdminTaskDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal AdminTaskDataSource AdminTaskOwner
		{
			get { return Owner as AdminTaskDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal AdminTaskSelectMethod SelectMethod
		{
			get { return AdminTaskOwner.SelectMethod; }
			set { AdminTaskOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal AdminTaskService AdminTaskProvider
		{
			get { return Provider as AdminTaskService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<AdminTask> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<AdminTask> results = null;
			AdminTask item;
			count = 0;
			
			System.Int32 _adminTaskId;
			System.Int32 _adminTaskTypeId;
			System.String _login;
			System.Int32 _adminTaskStatusId;
			System.Int32? _closedByUserId_nullable;
			System.Int32 _adminTaskSourceId;
			System.Int32 _csmsAccessId;

			switch ( SelectMethod )
			{
				case AdminTaskSelectMethod.Get:
					AdminTaskKey entityKey  = new AdminTaskKey();
					entityKey.Load(values);
					item = AdminTaskProvider.Get(entityKey);
					results = new TList<AdminTask>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case AdminTaskSelectMethod.GetAll:
                    results = AdminTaskProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case AdminTaskSelectMethod.GetPaged:
					results = AdminTaskProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case AdminTaskSelectMethod.Find:
					if ( FilterParameters != null )
						results = AdminTaskProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = AdminTaskProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case AdminTaskSelectMethod.GetByAdminTaskId:
					_adminTaskId = ( values["AdminTaskId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["AdminTaskId"], typeof(System.Int32)) : (int)0;
					item = AdminTaskProvider.GetByAdminTaskId(_adminTaskId);
					results = new TList<AdminTask>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				case AdminTaskSelectMethod.GetByAdminTaskTypeIdLogin:
					_adminTaskTypeId = ( values["AdminTaskTypeId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["AdminTaskTypeId"], typeof(System.Int32)) : (int)0;
					_login = ( values["Login"] != null ) ? (System.String) EntityUtil.ChangeType(values["Login"], typeof(System.String)) : string.Empty;
					results = AdminTaskProvider.GetByAdminTaskTypeIdLogin(_adminTaskTypeId, _login, this.StartIndex, this.PageSize, out count);
					break;
				case AdminTaskSelectMethod.GetByAdminTaskTypeIdLoginAdminTaskStatusId:
					_adminTaskTypeId = ( values["AdminTaskTypeId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["AdminTaskTypeId"], typeof(System.Int32)) : (int)0;
					_login = ( values["Login"] != null ) ? (System.String) EntityUtil.ChangeType(values["Login"], typeof(System.String)) : string.Empty;
					_adminTaskStatusId = ( values["AdminTaskStatusId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["AdminTaskStatusId"], typeof(System.Int32)) : (int)0;
					results = AdminTaskProvider.GetByAdminTaskTypeIdLoginAdminTaskStatusId(_adminTaskTypeId, _login, _adminTaskStatusId, this.StartIndex, this.PageSize, out count);
					break;
				// FK
				case AdminTaskSelectMethod.GetByAdminTaskTypeId:
					_adminTaskTypeId = ( values["AdminTaskTypeId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["AdminTaskTypeId"], typeof(System.Int32)) : (int)0;
					results = AdminTaskProvider.GetByAdminTaskTypeId(_adminTaskTypeId, this.StartIndex, this.PageSize, out count);
					break;
				case AdminTaskSelectMethod.GetByClosedByUserId:
					_closedByUserId_nullable = (System.Int32?) EntityUtil.ChangeType(values["ClosedByUserId"], typeof(System.Int32?));
					results = AdminTaskProvider.GetByClosedByUserId(_closedByUserId_nullable, this.StartIndex, this.PageSize, out count);
					break;
				case AdminTaskSelectMethod.GetByAdminTaskSourceId:
					_adminTaskSourceId = ( values["AdminTaskSourceId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["AdminTaskSourceId"], typeof(System.Int32)) : (int)0;
					results = AdminTaskProvider.GetByAdminTaskSourceId(_adminTaskSourceId, this.StartIndex, this.PageSize, out count);
					break;
				case AdminTaskSelectMethod.GetByAdminTaskStatusId:
					_adminTaskStatusId = ( values["AdminTaskStatusId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["AdminTaskStatusId"], typeof(System.Int32)) : (int)0;
					results = AdminTaskProvider.GetByAdminTaskStatusId(_adminTaskStatusId, this.StartIndex, this.PageSize, out count);
					break;
				case AdminTaskSelectMethod.GetByCsmsAccessId:
					_csmsAccessId = ( values["CsmsAccessId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["CsmsAccessId"], typeof(System.Int32)) : (int)0;
					results = AdminTaskProvider.GetByCsmsAccessId(_csmsAccessId, this.StartIndex, this.PageSize, out count);
					break;
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == AdminTaskSelectMethod.Get || SelectMethod == AdminTaskSelectMethod.GetByAdminTaskId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				AdminTask entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					AdminTaskProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<AdminTask> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			AdminTaskProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region AdminTaskDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the AdminTaskDataSource class.
	/// </summary>
	public class AdminTaskDataSourceDesigner : ProviderDataSourceDesigner<AdminTask, AdminTaskKey>
	{
		/// <summary>
		/// Initializes a new instance of the AdminTaskDataSourceDesigner class.
		/// </summary>
		public AdminTaskDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public AdminTaskSelectMethod SelectMethod
		{
			get { return ((AdminTaskDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new AdminTaskDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region AdminTaskDataSourceActionList

	/// <summary>
	/// Supports the AdminTaskDataSourceDesigner class.
	/// </summary>
	internal class AdminTaskDataSourceActionList : DesignerActionList
	{
		private AdminTaskDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the AdminTaskDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public AdminTaskDataSourceActionList(AdminTaskDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public AdminTaskSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion AdminTaskDataSourceActionList
	
	#endregion AdminTaskDataSourceDesigner
	
	#region AdminTaskSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the AdminTaskDataSource.SelectMethod property.
	/// </summary>
	public enum AdminTaskSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByAdminTaskId method.
		/// </summary>
		GetByAdminTaskId,
		/// <summary>
		/// Represents the GetByAdminTaskTypeIdLogin method.
		/// </summary>
		GetByAdminTaskTypeIdLogin,
		/// <summary>
		/// Represents the GetByAdminTaskTypeIdLoginAdminTaskStatusId method.
		/// </summary>
		GetByAdminTaskTypeIdLoginAdminTaskStatusId,
		/// <summary>
		/// Represents the GetByAdminTaskTypeId method.
		/// </summary>
		GetByAdminTaskTypeId,
		/// <summary>
		/// Represents the GetByClosedByUserId method.
		/// </summary>
		GetByClosedByUserId,
		/// <summary>
		/// Represents the GetByAdminTaskSourceId method.
		/// </summary>
		GetByAdminTaskSourceId,
		/// <summary>
		/// Represents the GetByAdminTaskStatusId method.
		/// </summary>
		GetByAdminTaskStatusId,
		/// <summary>
		/// Represents the GetByCsmsAccessId method.
		/// </summary>
		GetByCsmsAccessId
	}
	
	#endregion AdminTaskSelectMethod

	#region AdminTaskFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AdminTask"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AdminTaskFilter : SqlFilter<AdminTaskColumn>
	{
	}
	
	#endregion AdminTaskFilter

	#region AdminTaskExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AdminTask"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AdminTaskExpressionBuilder : SqlExpressionBuilder<AdminTaskColumn>
	{
	}
	
	#endregion AdminTaskExpressionBuilder	

	#region AdminTaskProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;AdminTaskChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="AdminTask"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AdminTaskProperty : ChildEntityProperty<AdminTaskChildEntityTypes>
	{
	}
	
	#endregion AdminTaskProperty
}

