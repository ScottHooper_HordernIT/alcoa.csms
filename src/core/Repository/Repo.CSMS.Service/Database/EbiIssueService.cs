﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface IEbiIssueService
    {
        EbiIssue Get(Expression<Func<EbiIssue, bool>> where, List<Expression<Func<EbiIssue, object>>> includes);
        List<EbiIssue> GetMany(int? top, Expression<Func<EbiIssue, bool>> where, Func<IQueryable<EbiIssue>, IOrderedQueryable<EbiIssue>> orderBy, List<Expression<Func<EbiIssue, object>>> includes);
        List<EbiIssue> GetManyPaged(int pageNumber, int pageSize, Expression<Func<EbiIssue, bool>> where, Func<IQueryable<EbiIssue>, IOrderedQueryable<EbiIssue>> orderBy, List<Expression<Func<EbiIssue, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<EbiIssue, bool>> where, List<Expression<Func<EbiIssue, object>>> includes);
        void Insert(EbiIssue item, bool saveChanges = true);
        void Update(EbiIssue item, bool saveChanges = true);
        void Delete(EbiIssue item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class EbiIssueService : IEbiIssueService
    {
        private readonly IRepository<EbiIssue> _repository;

        public EbiIssueService(IRepository<EbiIssue> repository)
        {
            this._repository = repository;
        }

        public EbiIssue Get(Expression<Func<EbiIssue, bool>> where, List<Expression<Func<EbiIssue, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<EbiIssue> GetMany(int? top, Expression<Func<EbiIssue, bool>> where, Func<IQueryable<EbiIssue>, IOrderedQueryable<EbiIssue>> orderBy, List<Expression<Func<EbiIssue, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<EbiIssue> GetManyPaged(int pageNumber, int pageSize, Expression<Func<EbiIssue, bool>> where, Func<IQueryable<EbiIssue>, IOrderedQueryable<EbiIssue>> orderBy, List<Expression<Func<EbiIssue, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<EbiIssue, bool>> where, List<Expression<Func<EbiIssue, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(EbiIssue item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("EbiIssue");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(EbiIssue item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("EbiIssue");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(EbiIssue item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("EbiIssue");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
