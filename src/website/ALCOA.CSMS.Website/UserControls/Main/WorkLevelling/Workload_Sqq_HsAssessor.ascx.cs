﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Text;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using KaiZen.CSMS.Web;
using KaiZen.CSMS.Services;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxTabControl;
using DevExpress.Web.Data;
using DevExpress.Web.ASPxGridView;
using System.Drawing;
using DevExpress.XtraPrinting;
using System.Data.SqlClient;

using DevExpress.Web.ASPxCallbackPanel;
using DevExpress.Web.ASPxClasses;
using DevExpress.XtraCharts.Native;
using DevExpress.XtraPrintingLinks;
using DevExpress.Web.ASPxPivotGrid;
using System.IO;

namespace ALCOA.CSMS.Website.UserControls.Main.WorkLevelling
{
    public partial class Workload_Sqq_HsAssessor : System.Web.UI.UserControl
    {
        Auth auth = new Auth();
        const string fileName = "ALCOA CSMS - Work Levelling Tool - Safety Qualification Questionnaires - Days Since Activity";

        protected void Page_Load(object sender, EventArgs e) { Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack, Page.IsCallback); }

        private void moduleInit(bool postBack, bool callBack)
        {
            if (!postBack)
            {
                cmbYear.DataSourceID = "dsQuestionnaire_GetDistinctYears";
                cmbYear.TextField = "Year";
                cmbYear.ValueField = "Year";
                cmbYear.Value = "-1";
                cmbYear.DataBind();
                cmbYear.Items.Add("All", -1);

                SessionHandler.spVar_Year = "%";
                LoadData();

                // Export
                String exportFileName = @"ALCOA CSMS - Work Levelling Tool - Safety Qualification Questionnaires - EHS Consultants"; //Chrome & IE is fine.
                String userAgent = Request.Headers.Get("User-Agent");
                if (
                    (userAgent.Contains("Mozilla") && !userAgent.Contains("AppleWebKit") && !userAgent.Contains("MSIE")) || //Mozilla
                    (userAgent.Contains("AppleWebKit") && !userAgent.Contains("Chrome"))      //Safari
                    )
                {
                    exportFileName = exportFileName.Replace(" ", "_");
                }
                Helper.ExportGrid.Settings(ucExportButtons, exportFileName);

                //grid.FilterExpression = "[CompanyStatusName] not like 'InActive%'";
            }
        }

        protected void cmbYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbYear.SelectedItem.Value.ToString() == "-1")
            {
                SessionHandler.spVar_Year = "%";
            }
            else
            {
                SessionHandler.spVar_Year = cmbYear.SelectedItem.Value.ToString();
            }
            LoadData();
        }

        protected void LoadData()
        {
            //grid.DataSource = dsQuestionnaire_GetEhsConsultantsWorkLoad;
            dsQuestionnaire_GetEhsConsultantsWorkLoad.DataBind();
            grid.DataBind();
        }

        protected void grid_AutoFilterCellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
        {
            //if (e.Column.FieldName == "UserFullNameLogon")
            //{
            //    ASPxComboBox comboBox = e.Editor as ASPxComboBox;
            //    //comboBox.Items.Clear();
            //    comboBox.ClientSideEvents.Init = "function(s, e) {s.InsertItem(0, '(Un-Allocated)', '(Un-Allocated)'); s.RemoveItem(-1);}";
            //    comboBox.ClientSideEvents.Init = "function(s, e) {s.InsertItem(-1, '(All)', '');}";
            //}
        }
    }
}