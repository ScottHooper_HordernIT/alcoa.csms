﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface IUsersEbiAuditService
    {
        UsersEbiAudit Get(Expression<Func<UsersEbiAudit, bool>> where, List<Expression<Func<UsersEbiAudit, object>>> includes);
        List<UsersEbiAudit> GetMany(int? top, Expression<Func<UsersEbiAudit, bool>> where, Func<IQueryable<UsersEbiAudit>, IOrderedQueryable<UsersEbiAudit>> orderBy, List<Expression<Func<UsersEbiAudit, object>>> includes);
        List<UsersEbiAudit> GetManyPaged(int pageNumber, int pageSize, Expression<Func<UsersEbiAudit, bool>> where, Func<IQueryable<UsersEbiAudit>, IOrderedQueryable<UsersEbiAudit>> orderBy, List<Expression<Func<UsersEbiAudit, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<UsersEbiAudit, bool>> where, List<Expression<Func<UsersEbiAudit, object>>> includes);
        void Insert(UsersEbiAudit item, bool saveChanges = true);
        void Update(UsersEbiAudit item, bool saveChanges = true);
        void Delete(UsersEbiAudit item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class UsersEbiAuditService : IUsersEbiAuditService
    {
        private readonly IRepository<UsersEbiAudit> _repository;

        public UsersEbiAuditService(IRepository<UsersEbiAudit> repository)
        {
            this._repository = repository;
        }

        public UsersEbiAudit Get(Expression<Func<UsersEbiAudit, bool>> where, List<Expression<Func<UsersEbiAudit, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<UsersEbiAudit> GetMany(int? top, Expression<Func<UsersEbiAudit, bool>> where, Func<IQueryable<UsersEbiAudit>, IOrderedQueryable<UsersEbiAudit>> orderBy, List<Expression<Func<UsersEbiAudit, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<UsersEbiAudit> GetManyPaged(int pageNumber, int pageSize, Expression<Func<UsersEbiAudit, bool>> where, Func<IQueryable<UsersEbiAudit>, IOrderedQueryable<UsersEbiAudit>> orderBy, List<Expression<Func<UsersEbiAudit, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<UsersEbiAudit, bool>> where, List<Expression<Func<UsersEbiAudit, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(UsersEbiAudit item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("UsersEbiAudit");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(UsersEbiAudit item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("UsersEbiAudit");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(UsersEbiAudit item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("UsersEbiAudit");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
