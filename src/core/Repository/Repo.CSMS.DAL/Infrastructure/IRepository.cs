﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using System.Data.SqlClient;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.DAL.Infrastructure
{
    public interface IRepository<T> : IQueryable where T : class
    {
        //Insert entity
        void Insert(T entity);

        //Update entity
        void Update(T entity);

        //Delete entity
        void Delete(T entity);
        void Delete(Expression<Func<T, bool>> where);

        //Get single entity
        T Get(Expression<Func<T, bool>> where);
        T Get(Expression<Func<T, bool>> where, List<Expression<Func<T, object>>> includes, bool isEntityTracking = true);

        //Get all entities
        IEnumerable<T> GetAll();
        IEnumerable<T> GetAll(int? top);
        IEnumerable<T> GetAll(int? top, List<Expression<Func<T, object>>> includes, bool isEntityTracking = true);
        IEnumerable<T> GetAll(int? top, Func<IQueryable<T>, IOrderedQueryable<T>> orderBy, List<Expression<Func<T, object>>> includes, bool isEntityTracking = true);

        //Get entities where condition(s) met
        IEnumerable<T> GetMany(Expression<Func<T, bool>> where);
        IEnumerable<T> GetMany(Expression<Func<T, bool>> where, int? top);
        IEnumerable<T> GetMany(Expression<Func<T, bool>> where, int? top, Func<IQueryable<T>, IOrderedQueryable<T>> orderBy);
        IEnumerable<T> GetMany(Expression<Func<T, bool>> where, int? top, Func<IQueryable<T>, IOrderedQueryable<T>> orderBy, List<Expression<Func<T, object>>> includes, bool isEntityTracking = true);

        //Get sum of entities based on selector
        decimal? GetSum(Expression<Func<T, bool>> where, Expression<Func<T, decimal>> selector, List<Expression<Func<T, object>>> includes);
        float? GetSum(Expression<Func<T, bool>> where, Expression<Func<T, float>> selector, List<Expression<Func<T, object>>> includes);
        double? GetSum(Expression<Func<T, bool>> where, Expression<Func<T, double>> selector, List<Expression<Func<T, object>>> includes);
        int? GetSum(Expression<Func<T, bool>> where, Expression<Func<T, int>> selector, List<Expression<Func<T, object>>> includes);

        //Get maximum of entities based on selector
        int? GetMax(Expression<Func<T, bool>> where, Expression<Func<T, int>> selector, List<Expression<Func<T, object>>> includes);
        DateTime? GetMax(Expression<Func<T, bool>> where, Expression<Func<T, DateTime?>> selector, List<Expression<Func<T, object>>> includes);

        //Get count of entities
        int? GetCount(Expression<Func<T, bool>> where, List<Expression<Func<T, object>>> includes);

        //Get entities where condition(s) met and paged
        IEnumerable<T> GetManyPaged(int pageNumber, int pageSize, Expression<Func<T, bool>> where, Func<IQueryable<T>, IOrderedQueryable<T>> orderBy, out int pageCount, out int totalRecords);
        IEnumerable<T> GetManyPaged(int pageNumber, int pageSize, Expression<Func<T, bool>> where, Func<IQueryable<T>, IOrderedQueryable<T>> orderBy, List<Expression<Func<T, object>>> includes, out int pageCount, out int totalRecords);

        //Execute stored procedure/function
        int ExecuteCustomFunction(string commandName, params ObjectParameter[] parameters);
        IEnumerable<T> ExecuteCustomFunction<T>(string commandName, params ObjectParameter[] parameters);

        //Save changes (transation)
        void SaveChanges();
    }
}
