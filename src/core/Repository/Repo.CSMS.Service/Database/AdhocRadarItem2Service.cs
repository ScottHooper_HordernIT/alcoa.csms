﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface IAdhocRadarItems2Service
    {
        AdHoc_Radar_Items2 Get(Expression<Func<AdHoc_Radar_Items2, bool>> where, List<Expression<Func<AdHoc_Radar_Items2, object>>> includes);
        List<AdHoc_Radar_Items2> GetMany(int? top, Expression<Func<AdHoc_Radar_Items2, bool>> where, Func<IQueryable<AdHoc_Radar_Items2>, IOrderedQueryable<AdHoc_Radar_Items2>> orderBy, List<Expression<Func<AdHoc_Radar_Items2, object>>> includes);
        List<AdHoc_Radar_Items2> GetManyPaged(int pageNumber, int pageSize, Expression<Func<AdHoc_Radar_Items2, bool>> where, Func<IQueryable<AdHoc_Radar_Items2>, IOrderedQueryable<AdHoc_Radar_Items2>> orderBy, List<Expression<Func<AdHoc_Radar_Items2, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<AdHoc_Radar_Items2, bool>> where, List<Expression<Func<AdHoc_Radar_Items2, object>>> includes);
        void Insert(AdHoc_Radar_Items2 item, bool saveChanges = true);
        void Update(AdHoc_Radar_Items2 item, bool saveChanges = true);
        void Delete(AdHoc_Radar_Items2 item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class AdhocRadarItems2Service : IAdhocRadarItems2Service
    {
        private readonly IRepository<AdHoc_Radar_Items2> _repository;

        public AdhocRadarItems2Service(IRepository<AdHoc_Radar_Items2> repository)
        {
            this._repository = repository;
        }

        public AdHoc_Radar_Items2 Get(Expression<Func<AdHoc_Radar_Items2, bool>> where, List<Expression<Func<AdHoc_Radar_Items2, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<AdHoc_Radar_Items2> GetMany(int? top, Expression<Func<AdHoc_Radar_Items2, bool>> where, Func<IQueryable<AdHoc_Radar_Items2>, IOrderedQueryable<AdHoc_Radar_Items2>> orderBy, List<Expression<Func<AdHoc_Radar_Items2, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<AdHoc_Radar_Items2> GetManyPaged(int pageNumber, int pageSize, Expression<Func<AdHoc_Radar_Items2, bool>> where, Func<IQueryable<AdHoc_Radar_Items2>, IOrderedQueryable<AdHoc_Radar_Items2>> orderBy, List<Expression<Func<AdHoc_Radar_Items2, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<AdHoc_Radar_Items2, bool>> where, List<Expression<Func<AdHoc_Radar_Items2, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(AdHoc_Radar_Items2 item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("AdHoc_Radar_Items2");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(AdHoc_Radar_Items2 item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("AdHoc_Radar_Items2");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(AdHoc_Radar_Items2 item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("AdHoc_Radar_Items2");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
