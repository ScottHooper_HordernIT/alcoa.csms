﻿#region Using directives

using System;

#endregion

namespace KaiZen.CSMS.Entities
{	
	///<summary>
	/// An object representation of the 'CsmsEmailLog' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class CsmsEmailLog : CsmsEmailLogBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="CsmsEmailLog"/> instance.
		///</summary>
		public CsmsEmailLog():base(){}	
		
		#endregion
	}
}
