﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using System.Web;
using System.Data.Entity.Core.Objects;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface IAnnualTargetService
    {
        AnnualTarget Get(Expression<Func<AnnualTarget, bool>> where, List<Expression<Func<AnnualTarget, object>>> includes);
        List<AnnualTarget> GetMany(int? top, Expression<Func<AnnualTarget, bool>> where, Func<IQueryable<AnnualTarget>, IOrderedQueryable<AnnualTarget>> orderBy, List<Expression<Func<AnnualTarget, object>>> includes);
        List<AnnualTarget> GetManyPaged(int pageNumber, int pageSize, Expression<Func<AnnualTarget, bool>> where, Func<IQueryable<AnnualTarget>, IOrderedQueryable<AnnualTarget>> orderBy, List<Expression<Func<AnnualTarget, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<AnnualTarget, bool>> where, List<Expression<Func<AnnualTarget, object>>> includes);
        void Insert(AnnualTarget item, bool saveChanges = true);
        void Update(AnnualTarget item, bool saveChanges = true);
        void Delete(AnnualTarget item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class AnnualTargetService : IAnnualTargetService
    {
        private readonly IRepository<AnnualTarget> _repository;

        public AnnualTargetService(IRepository<AnnualTarget> repository)
        {
            this._repository = repository;
        }

        public AnnualTarget Get(Expression<Func<AnnualTarget, bool>> where, List<Expression<Func<AnnualTarget, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<AnnualTarget> GetMany(int? top, Expression<Func<AnnualTarget, bool>> where, Func<IQueryable<AnnualTarget>, IOrderedQueryable<AnnualTarget>> orderBy, List<Expression<Func<AnnualTarget, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<AnnualTarget> GetManyPaged(int pageNumber, int pageSize, Expression<Func<AnnualTarget, bool>> where, Func<IQueryable<AnnualTarget>, IOrderedQueryable<AnnualTarget>> orderBy, List<Expression<Func<AnnualTarget, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<AnnualTarget, bool>> where, List<Expression<Func<AnnualTarget, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(AnnualTarget item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("AnnualTarget");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(AnnualTarget item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("AnnualTarget");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(AnnualTarget item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("AnnualTarget");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
