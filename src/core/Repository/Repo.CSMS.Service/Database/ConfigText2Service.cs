﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface IConfigText2Service
    {
        ConfigText2 Get(Expression<Func<ConfigText2, bool>> where, List<Expression<Func<ConfigText2, object>>> includes);
        List<ConfigText2> GetMany(int? top, Expression<Func<ConfigText2, bool>> where, Func<IQueryable<ConfigText2>, IOrderedQueryable<ConfigText2>> orderBy, List<Expression<Func<ConfigText2, object>>> includes);
        List<ConfigText2> GetManyPaged(int pageNumber, int pageSize, Expression<Func<ConfigText2, bool>> where, Func<IQueryable<ConfigText2>, IOrderedQueryable<ConfigText2>> orderBy, List<Expression<Func<ConfigText2, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<ConfigText2, bool>> where, List<Expression<Func<ConfigText2, object>>> includes);
        void Insert(ConfigText2 item, bool saveChanges = true);
        void Update(ConfigText2 item, bool saveChanges = true);
        void Delete(ConfigText2 item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class ConfigText2Service : IConfigText2Service
    {
        private readonly IRepository<ConfigText2> _repository;

        public ConfigText2Service(IRepository<ConfigText2> repository)
        {
            this._repository = repository;
        }

        public ConfigText2 Get(Expression<Func<ConfigText2, bool>> where, List<Expression<Func<ConfigText2, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<ConfigText2> GetMany(int? top, Expression<Func<ConfigText2, bool>> where, Func<IQueryable<ConfigText2>, IOrderedQueryable<ConfigText2>> orderBy, List<Expression<Func<ConfigText2, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<ConfigText2> GetManyPaged(int pageNumber, int pageSize, Expression<Func<ConfigText2, bool>> where, Func<IQueryable<ConfigText2>, IOrderedQueryable<ConfigText2>> orderBy, List<Expression<Func<ConfigText2, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<ConfigText2, bool>> where, List<Expression<Func<ConfigText2, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(ConfigText2 item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("ConfigText2");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(ConfigText2 item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("ConfigText2");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(ConfigText2 item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("ConfigText2");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
