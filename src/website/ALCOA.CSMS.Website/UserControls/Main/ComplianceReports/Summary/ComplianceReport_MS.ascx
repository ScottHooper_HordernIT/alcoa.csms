﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="UserControls_Tiny_ComplianceReports_ComplianceReport_MS" Codebehind="ComplianceReport_MS.ascx.cs" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"  Namespace="System.Web.UI" TagPrefix="asp" %>

    <dxwgv:ASPxGridView ID="grid"
                ClientInstanceName="grid"
                runat="server"
                AutoGenerateColumns="False"
                CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                CssPostfix="Office2003Blue"
                Width="900px"
                OnHtmlRowCreated="grid_RowCreated"
                OnAutoFilterCellEditorInitialize="grid_AutoFilterCellEditorInitialize"
             >
                <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                    <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
                    </LoadingPanelOnStatusBar>
                    <LoadingPanel Url="~/App_Themes/Office2003BlueOld/GridView/Loading.gif"><%--Enhancement_023:change by debashis for testing platform upgradation--%>
                    </LoadingPanel>
                </Images>
                <Settings ShowGroupedColumns="True" ShowFilterRow="True" />
                <SettingsPager Visible="False" PageSize="100" Mode="ShowAllRecords">
                    <AllButton Text="All">
                    </AllButton>
                    <NextPageButton Text="Next &gt;">
                    </NextPageButton>
                    <PrevPageButton Text="&lt; Prev">
                    </PrevPageButton>
                </SettingsPager>
                <SettingsCustomizationWindow Enabled="True" />
                <ImagesFilterControl>
                    <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
                    </LoadingPanel>
                </ImagesFilterControl>
                <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
                    CssPostfix="Office2003Blue">
                    <Header ImageSpacing="5px" SortingImageSpacing="5px">
                    </Header>
                    <AlternatingRow Enabled="True">
                    </AlternatingRow>
                    <LoadingPanel ImageSpacing="10px">
                    </LoadingPanel>
                </Styles>
                <SettingsBehavior ConfirmDelete="True"/>
                <SettingsCookies CookiesID="NonComplianceTraining" Version="0.1" />
                <Columns>
                    <dxwgv:GridViewDataComboBoxColumn Caption="Company Name" FieldName="CompanyId" Name="CompanyBox"
                        VisibleIndex="0">
                        <PropertiesComboBox DataSourceID="sqldsCompaniesList" DropDownHeight="150px" TextField="CompanyName"
                            ValueField="CompanyId" ValueType="System.Int32" IncrementalFilteringMode="StartsWith">
                        </PropertiesComboBox>
                        <EditFormSettings Visible="True" />
                        <Settings SortMode="DisplayText" />
                    </dxwgv:GridViewDataComboBoxColumn>
                    
                    <dxwgv:GridViewDataTextColumn FieldName="SiteName" ReadOnly="True" 
                        Caption="Site" Name="SiteName" VisibleIndex="1" SortIndex="1" SortOrder="Ascending" Width="350px">
                    <Settings AutoFilterCondition="BeginsWith" />
                    </dxwgv:GridViewDataTextColumn>
                              
                              <dxwgv:GridViewDataComboBoxColumn Name="gcbResidentialCategory" Caption="Residential Category"
            FieldName="CompanySiteCategoryId" SortIndex="0" SortOrder="Ascending" UnboundType="String"
            VisibleIndex="2" FixedStyle="Left" Width="145px">
            <PropertiesComboBox DataSourceID="CompanySiteCategoryDataSource" DropDownHeight="150px"
                TextField="CategoryDesc" ValueField="CompanySiteCategoryId" ValueType="System.Int32"
                IncrementalFilteringMode="StartsWith">
            </PropertiesComboBox>
            <Settings SortMode="DisplayText" />
        </dxwgv:GridViewDataComboBoxColumn>
                       
<dxwgv:GridViewDataTextColumn FieldName="Qtr1" ReadOnly="True" Name="Qtr1" Caption="Qtr 1" VisibleIndex="3"><DataItemTemplate>
                     <table cellpadding="0" cellspacing="0"><tr>
                     <td><asp:Image ID="changeImage1" runat="server" ImageUrl="~/Images/spacer.gif" Visible="true" GenerateEmptyAlternateText="True" /></td>
                     <td style="width:5px"><asp:Label ID="noKPISubmitted1" runat="server" Text="" ></asp:Label></td>
                     </tr></table>
                        
</DataItemTemplate>

<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<CellStyle HorizontalAlign="Center"></CellStyle>
</dxwgv:GridViewDataTextColumn>
<dxwgv:GridViewDataTextColumn FieldName="Qtr2" ReadOnly="True" Name="Qtr2" Caption="Qtr 2" VisibleIndex="4"><DataItemTemplate>
                     <table cellpadding="0" cellspacing="0"><tr>
                     <td><asp:Image ID="changeImage2" runat="server" ImageUrl="~/Images/spacer.gif" Visible="true" GenerateEmptyAlternateText="True" /></td>
                     <td style="width:5px"><asp:Label ID="noKPISubmitted2" runat="server" Text="" ></asp:Label></td>
                     </tr></table>
                        
</DataItemTemplate>

<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<CellStyle HorizontalAlign="Center"></CellStyle>
</dxwgv:GridViewDataTextColumn>
<dxwgv:GridViewDataTextColumn FieldName="Qtr3" ReadOnly="True" Name="Qtr3" Caption="Qtr 3" VisibleIndex="5"><DataItemTemplate>
                     <table cellpadding="0" cellspacing="0"><tr>
                     <td><asp:Image ID="changeImage3" runat="server" ImageUrl="~/Images/spacer.gif" Visible="true" GenerateEmptyAlternateText="True" /></td>
                     <td style="width:5px"><asp:Label ID="noKPISubmitted3" runat="server" Text="" ></asp:Label></td>
                     </tr></table>
                        
</DataItemTemplate>

<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<CellStyle HorizontalAlign="Center"></CellStyle>
</dxwgv:GridViewDataTextColumn>
<dxwgv:GridViewDataTextColumn FieldName="Qtr4" ReadOnly="True" Name="Qtr4" Caption="Qtr 4" VisibleIndex="6"><DataItemTemplate>
                     <table cellpadding="0" cellspacing="0"><tr>
                     <td><asp:Image ID="changeImage4" runat="server" ImageUrl="~/Images/spacer.gif" Visible="true" GenerateEmptyAlternateText="True" /></td>
                     <td style="width:5px"><asp:Label ID="noKPISubmitted4" runat="server" Text="" ></asp:Label></td>
                     </tr></table>
                        
</DataItemTemplate>

<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<CellStyle HorizontalAlign="Center"></CellStyle>
</dxwgv:GridViewDataTextColumn>

<dxwgv:GridViewDataTextColumn FieldName="Year" ReadOnly="True" Name="Year" VisibleIndex="15"><DataItemTemplate>
                     <table cellpadding="0" cellspacing="0"><tr>
                     <td style="width:20px"><asp:Label ID="noKPISubmitted13" runat="server" Text="" ></asp:Label></td>
                     </tr></table>
                        
</DataItemTemplate>

<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<CellStyle HorizontalAlign="Center"></CellStyle>
</dxwgv:GridViewDataTextColumn>
                </Columns>
        <SettingsText EmptyDataRow="No Medical Report(s) submitted" />
                <StylesEditors>
                    <ProgressBar Height="25px">
                    </ProgressBar>
                </StylesEditors>
            </dxwgv:ASPxGridView>

    <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="grid"></dxwgv:ASPxGridViewExporter>

            <asp:SqlDataSource ID="sqldsKPIYear" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
                    ProviderName="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString.ProviderName %>"
                    SelectCommand="SELECT DISTINCT(YEAR(kpiDateTime)) As [Year] FROM dbo.KPI ORDER BY [Year] ASC">
                </asp:SqlDataSource>
                
                <asp:SqlDataSource ID="sqldsCompaniesList" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="_Companies_GetCompanyNameList" SelectCommandType="StoredProcedure">
</asp:SqlDataSource>

<asp:SqlDataSource ID="sqldsResidentialSites" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    ProviderName="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString.ProviderName %>"
    SelectCommand="SELECT DISTINCT(dbo.CompanySiteCategoryStandard.SiteId) As [SiteId], dbo.Sites.SiteName As [SiteName] FROM dbo.CompanySiteCategoryStandard INNER JOIN dbo.Sites on dbo.CompanySiteCategoryStandard.SiteId = dbo.Sites.SiteId ORDER BY [SiteName] ASC">
</asp:SqlDataSource>
                
                <data:CompanySiteCategoryDataSource ID="CompanySiteCategoryDataSource" runat="server" SelectMethod="GetPaged"
            EnablePaging="True" EnableSorting="True">
        </data:CompanySiteCategoryDataSource>