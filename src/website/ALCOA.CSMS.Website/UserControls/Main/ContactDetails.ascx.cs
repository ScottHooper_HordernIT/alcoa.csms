﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Text;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using KaiZen.CSMS.Web;
using KaiZen.CSMS.Services;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxTabControl;
using DevExpress.Web.Data;
using DevExpress.Web.ASPxGridView;
using System.Drawing;
using DevExpress.Web.ASPxCallbackPanel;
using DevExpress.Web.ASPxClasses;
using DevExpress.XtraPrinting;


namespace ALCOA.CSMS.Website.UserControls.Main
{
    public partial class ContactDetails : System.Web.UI.UserControl
    {
        Auth auth = new Auth();
        protected void Page_Load(object sender, EventArgs e)
        {
            Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack);
        }
        private void moduleInit(bool postBack)
        {
            if (!postBack)
            {
                switch (auth.RoleId)
                {
                    case ((int)RoleList.Reader):
                        //full access
                        break;
                    case ((int)RoleList.Contractor):
                        //restrict to own company
                        SessionHandler.spVar_CompanyId = auth.CompanyId.ToString();
                        ASPxGridView1.Settings.ShowFilterRow = false;
                        ASPxGridView1.Settings.ShowFilterBar = GridViewStatusBarMode.Hidden;
                        ASPxGridView1.FilterExpression = String.Format("CompanyId = {0}", auth.CompanyId);
                        ASPxGridView1.DataBind();
                        break;
                    case ((int)RoleList.Administrator):
                        //full access
                        break;
                    default:
                        // do something
                        Response.Redirect("AccessDenied.aspx?error=" + Server.UrlEncode("Unrecognised User."));
                        break;
                }

                // Export
                String exportFileName = @"ALCOA CSMS - Contact Details"; //Chrome & IE is fine.
                String userAgent = Request.Headers.Get("User-Agent");
                if (
                    (userAgent.Contains("Mozilla") && !userAgent.Contains("AppleWebKit") && !userAgent.Contains("MSIE")) || //Mozilla
                    (userAgent.Contains("AppleWebKit") && !userAgent.Contains("Chrome"))      //Safari
                    )
                {
                    exportFileName = exportFileName.Replace(" ", "_");
                }
                Helper.ExportGrid.Settings(ucExportButtons, exportFileName, "ASPxGridView1");
            }
        }

        public static string Capitalize(string value)
        {
            return System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(value);
        }

        protected void grid_RowCreated(object sender, ASPxGridViewTableRowEventArgs e)
        {
            if (e.RowType != GridViewRowType.Data) return;

            if (ASPxGridView1.GetRowValues(e.VisibleIndex, "IsOver90Days") != DBNull.Value)
            {
                String Over90Days = Convert.ToString(ASPxGridView1.GetRowValues(e.VisibleIndex, "IsOver90Days"));
                System.Web.UI.WebControls.Image img = ASPxGridView1.FindRowCellTemplateControl(e.VisibleIndex, null, "cImage") as System.Web.UI.WebControls.Image;
                if (img != null)
                {
                    if (Over90Days == "0")
                    {
                        img.ImageUrl = "~/Images/TrafficLights/tlGreen.gif";
                    }
                    else if (Over90Days == "1")
                    {
                        img.ImageUrl = "~/Images/TrafficLights/tlRed.gif";
                    }

                }
            }
        }

        protected void grid_CustomColumnDisplayText(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewColumnDisplayTextEventArgs e)
        {
            if (e.Column.FieldName == "KpiAdministratorDetails")
            {
                if (e.Value != null)
                {
                    e.DisplayText = e.Value.ToString().Replace("|", "<br/>");
                }
            }
        }
    }
}