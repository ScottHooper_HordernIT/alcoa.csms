﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using System.Data.SqlClient;
using System.Data.Entity.Core.Objects;
using Repo.CSMS.Common;

namespace Repo.CSMS.DAL.Infrastructure
{
    public interface IEntity
    {
        int ID { get; set; }
    }
}
