﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.FileVaultListProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[CLSCompliant(true)]
	[Designer(typeof(FileVaultListDataSourceDesigner))]
	public class FileVaultListDataSource : ReadOnlyDataSource<FileVaultList>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FileVaultListDataSource class.
		/// </summary>
		public FileVaultListDataSource() : base(new FileVaultListService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the FileVaultListDataSourceView used by the FileVaultListDataSource.
		/// </summary>
		protected FileVaultListDataSourceView FileVaultListView
		{
			get { return ( View as FileVaultListDataSourceView ); }
		}
		
		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the FileVaultListDataSourceView class that is to be
		/// used by the FileVaultListDataSource.
		/// </summary>
		/// <returns>An instance of the FileVaultListDataSourceView class.</returns>
		protected override BaseDataSourceView<FileVaultList, Object> GetNewDataSourceView()
		{
			return new FileVaultListDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the FileVaultListDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class FileVaultListDataSourceView : ReadOnlyDataSourceView<FileVaultList>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FileVaultListDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the FileVaultListDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public FileVaultListDataSourceView(FileVaultListDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal FileVaultListDataSource FileVaultListOwner
		{
			get { return Owner as FileVaultListDataSource; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal FileVaultListService FileVaultListProvider
		{
			get { return Provider as FileVaultListService; }
		}

		#endregion Properties
		
		#region Methods
		
		#endregion Methods
	}

	#region FileVaultListDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the FileVaultListDataSource class.
	/// </summary>
	public class FileVaultListDataSourceDesigner : ReadOnlyDataSourceDesigner<FileVaultList>
	{
	}

	#endregion FileVaultListDataSourceDesigner

	#region FileVaultListFilter

	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="FileVaultList"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FileVaultListFilter : SqlFilter<FileVaultListColumn>
	{
	}

	#endregion FileVaultListFilter

	#region FileVaultListExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="FileVaultList"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FileVaultListExpressionBuilder : SqlExpressionBuilder<FileVaultListColumn>
	{
	}
	
	#endregion FileVaultListExpressionBuilder		
}

