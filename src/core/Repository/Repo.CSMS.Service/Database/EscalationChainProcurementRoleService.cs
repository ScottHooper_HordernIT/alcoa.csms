﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface IEscalationChainProcurementRoleService
    {
        EscalationChainProcurementRole Get(Expression<Func<EscalationChainProcurementRole, bool>> where, List<Expression<Func<EscalationChainProcurementRole, object>>> includes);
        List<EscalationChainProcurementRole> GetMany(int? top, Expression<Func<EscalationChainProcurementRole, bool>> where, Func<IQueryable<EscalationChainProcurementRole>, IOrderedQueryable<EscalationChainProcurementRole>> orderBy, List<Expression<Func<EscalationChainProcurementRole, object>>> includes);
        List<EscalationChainProcurementRole> GetManyPaged(int pageNumber, int pageSize, Expression<Func<EscalationChainProcurementRole, bool>> where, Func<IQueryable<EscalationChainProcurementRole>, IOrderedQueryable<EscalationChainProcurementRole>> orderBy, List<Expression<Func<EscalationChainProcurementRole, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<EscalationChainProcurementRole, bool>> where, List<Expression<Func<EscalationChainProcurementRole, object>>> includes);
        void Insert(EscalationChainProcurementRole item, bool saveChanges = true);
        void Update(EscalationChainProcurementRole item, bool saveChanges = true);
        void Delete(EscalationChainProcurementRole item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class EscalationChainProcurementRoleService : IEscalationChainProcurementRoleService
    {
        private readonly IRepository<EscalationChainProcurementRole> _repository;

        public EscalationChainProcurementRoleService(IRepository<EscalationChainProcurementRole> repository)
        {
            this._repository = repository;
        }

        public EscalationChainProcurementRole Get(Expression<Func<EscalationChainProcurementRole, bool>> where, List<Expression<Func<EscalationChainProcurementRole, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<EscalationChainProcurementRole> GetMany(int? top, Expression<Func<EscalationChainProcurementRole, bool>> where, Func<IQueryable<EscalationChainProcurementRole>, IOrderedQueryable<EscalationChainProcurementRole>> orderBy, List<Expression<Func<EscalationChainProcurementRole, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<EscalationChainProcurementRole> GetManyPaged(int pageNumber, int pageSize, Expression<Func<EscalationChainProcurementRole, bool>> where, Func<IQueryable<EscalationChainProcurementRole>, IOrderedQueryable<EscalationChainProcurementRole>> orderBy, List<Expression<Func<EscalationChainProcurementRole, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<EscalationChainProcurementRole, bool>> where, List<Expression<Func<EscalationChainProcurementRole, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(EscalationChainProcurementRole item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("EscalationChainProcurementRole");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(EscalationChainProcurementRole item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("EscalationChainProcurementRole");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(EscalationChainProcurementRole item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("EscalationChainProcurementRole");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
