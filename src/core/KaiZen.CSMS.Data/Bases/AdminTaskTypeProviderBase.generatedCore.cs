﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="AdminTaskTypeProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class AdminTaskTypeProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.AdminTaskType, KaiZen.CSMS.Entities.AdminTaskTypeKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.AdminTaskTypeKey key)
		{
			return Delete(transactionManager, key.AdminTaskTypeId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_adminTaskTypeId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _adminTaskTypeId)
		{
			return Delete(null, _adminTaskTypeId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_adminTaskTypeId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _adminTaskTypeId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.AdminTaskType Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.AdminTaskTypeKey key, int start, int pageLength)
		{
			return GetByAdminTaskTypeId(transactionManager, key.AdminTaskTypeId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_AdminTaskType index.
		/// </summary>
		/// <param name="_adminTaskTypeId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdminTaskType"/> class.</returns>
		public KaiZen.CSMS.Entities.AdminTaskType GetByAdminTaskTypeId(System.Int32 _adminTaskTypeId)
		{
			int count = -1;
			return GetByAdminTaskTypeId(null,_adminTaskTypeId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_AdminTaskType index.
		/// </summary>
		/// <param name="_adminTaskTypeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdminTaskType"/> class.</returns>
		public KaiZen.CSMS.Entities.AdminTaskType GetByAdminTaskTypeId(System.Int32 _adminTaskTypeId, int start, int pageLength)
		{
			int count = -1;
			return GetByAdminTaskTypeId(null, _adminTaskTypeId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_AdminTaskType index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_adminTaskTypeId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdminTaskType"/> class.</returns>
		public KaiZen.CSMS.Entities.AdminTaskType GetByAdminTaskTypeId(TransactionManager transactionManager, System.Int32 _adminTaskTypeId)
		{
			int count = -1;
			return GetByAdminTaskTypeId(transactionManager, _adminTaskTypeId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_AdminTaskType index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_adminTaskTypeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdminTaskType"/> class.</returns>
		public KaiZen.CSMS.Entities.AdminTaskType GetByAdminTaskTypeId(TransactionManager transactionManager, System.Int32 _adminTaskTypeId, int start, int pageLength)
		{
			int count = -1;
			return GetByAdminTaskTypeId(transactionManager, _adminTaskTypeId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_AdminTaskType index.
		/// </summary>
		/// <param name="_adminTaskTypeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdminTaskType"/> class.</returns>
		public KaiZen.CSMS.Entities.AdminTaskType GetByAdminTaskTypeId(System.Int32 _adminTaskTypeId, int start, int pageLength, out int count)
		{
			return GetByAdminTaskTypeId(null, _adminTaskTypeId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_AdminTaskType index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_adminTaskTypeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdminTaskType"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.AdminTaskType GetByAdminTaskTypeId(TransactionManager transactionManager, System.Int32 _adminTaskTypeId, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key UK_AdminTaskTypeName index.
		/// </summary>
		/// <param name="_adminTaskTypeName"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdminTaskType"/> class.</returns>
		public KaiZen.CSMS.Entities.AdminTaskType GetByAdminTaskTypeName(System.String _adminTaskTypeName)
		{
			int count = -1;
			return GetByAdminTaskTypeName(null,_adminTaskTypeName, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the UK_AdminTaskTypeName index.
		/// </summary>
		/// <param name="_adminTaskTypeName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdminTaskType"/> class.</returns>
		public KaiZen.CSMS.Entities.AdminTaskType GetByAdminTaskTypeName(System.String _adminTaskTypeName, int start, int pageLength)
		{
			int count = -1;
			return GetByAdminTaskTypeName(null, _adminTaskTypeName, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the UK_AdminTaskTypeName index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_adminTaskTypeName"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdminTaskType"/> class.</returns>
		public KaiZen.CSMS.Entities.AdminTaskType GetByAdminTaskTypeName(TransactionManager transactionManager, System.String _adminTaskTypeName)
		{
			int count = -1;
			return GetByAdminTaskTypeName(transactionManager, _adminTaskTypeName, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the UK_AdminTaskTypeName index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_adminTaskTypeName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdminTaskType"/> class.</returns>
		public KaiZen.CSMS.Entities.AdminTaskType GetByAdminTaskTypeName(TransactionManager transactionManager, System.String _adminTaskTypeName, int start, int pageLength)
		{
			int count = -1;
			return GetByAdminTaskTypeName(transactionManager, _adminTaskTypeName, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the UK_AdminTaskTypeName index.
		/// </summary>
		/// <param name="_adminTaskTypeName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdminTaskType"/> class.</returns>
		public KaiZen.CSMS.Entities.AdminTaskType GetByAdminTaskTypeName(System.String _adminTaskTypeName, int start, int pageLength, out int count)
		{
			return GetByAdminTaskTypeName(null, _adminTaskTypeName, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the UK_AdminTaskTypeName index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_adminTaskTypeName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdminTaskType"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.AdminTaskType GetByAdminTaskTypeName(TransactionManager transactionManager, System.String _adminTaskTypeName, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;AdminTaskType&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;AdminTaskType&gt;"/></returns>
		public static TList<AdminTaskType> Fill(IDataReader reader, TList<AdminTaskType> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.AdminTaskType c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("AdminTaskType")
					.Append("|").Append((System.Int32)reader[((int)AdminTaskTypeColumn.AdminTaskTypeId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<AdminTaskType>(
					key.ToString(), // EntityTrackingKey
					"AdminTaskType",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.AdminTaskType();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.AdminTaskTypeId = (System.Int32)reader[((int)AdminTaskTypeColumn.AdminTaskTypeId - 1)];
					c.AdminTaskTypeName = (System.String)reader[((int)AdminTaskTypeColumn.AdminTaskTypeName - 1)];
					c.AdminTaskTypeDesc = (reader.IsDBNull(((int)AdminTaskTypeColumn.AdminTaskTypeDesc - 1)))?null:(System.String)reader[((int)AdminTaskTypeColumn.AdminTaskTypeDesc - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.AdminTaskType"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.AdminTaskType"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.AdminTaskType entity)
		{
			if (!reader.Read()) return;
			
			entity.AdminTaskTypeId = (System.Int32)reader[((int)AdminTaskTypeColumn.AdminTaskTypeId - 1)];
			entity.AdminTaskTypeName = (System.String)reader[((int)AdminTaskTypeColumn.AdminTaskTypeName - 1)];
			entity.AdminTaskTypeDesc = (reader.IsDBNull(((int)AdminTaskTypeColumn.AdminTaskTypeDesc - 1)))?null:(System.String)reader[((int)AdminTaskTypeColumn.AdminTaskTypeDesc - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.AdminTaskType"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.AdminTaskType"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.AdminTaskType entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.AdminTaskTypeId = (System.Int32)dataRow["AdminTaskTypeId"];
			entity.AdminTaskTypeName = (System.String)dataRow["AdminTaskTypeName"];
			entity.AdminTaskTypeDesc = Convert.IsDBNull(dataRow["AdminTaskTypeDesc"]) ? null : (System.String)dataRow["AdminTaskTypeDesc"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.AdminTaskType"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.AdminTaskType Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.AdminTaskType entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetByAdminTaskTypeId methods when available
			
			#region AdminTaskEmailTemplate
			// RelationshipType.OneToOne
			if (CanDeepLoad(entity, "AdminTaskEmailTemplate|AdminTaskEmailTemplate", deepLoadType, innerList))
			{
				entity.AdminTaskEmailTemplate = DataRepository.AdminTaskEmailTemplateProvider.GetByAdminTaskTypeId(transactionManager, entity.AdminTaskTypeId);
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'AdminTaskEmailTemplate' loaded. key " + entity.EntityTrackingKey);
				#endif 

				if (deep && entity.AdminTaskEmailTemplate != null)
				{
					deepHandles.Add("AdminTaskEmailTemplate",
						new KeyValuePair<Delegate, object>((DeepLoadSingleHandle< AdminTaskEmailTemplate >) DataRepository.AdminTaskEmailTemplateProvider.DeepLoad,
						new object[] { transactionManager, entity.AdminTaskEmailTemplate, deep, deepLoadType, childTypes, innerList }
					));
				}
			}
			#endregion 
			
			
			
			#region AdminTaskCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<AdminTask>|AdminTaskCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'AdminTaskCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.AdminTaskCollection = DataRepository.AdminTaskProvider.GetByAdminTaskTypeId(transactionManager, entity.AdminTaskTypeId);

				if (deep && entity.AdminTaskCollection.Count > 0)
				{
					deepHandles.Add("AdminTaskCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<AdminTask>) DataRepository.AdminTaskProvider.DeepLoad,
						new object[] { transactionManager, entity.AdminTaskCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.AdminTaskType object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.AdminTaskType instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.AdminTaskType Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.AdminTaskType entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();

			#region AdminTaskEmailTemplate
			if (CanDeepSave(entity.AdminTaskEmailTemplate, "AdminTaskEmailTemplate|AdminTaskEmailTemplate", deepSaveType, innerList))
			{

				if (entity.AdminTaskEmailTemplate != null)
				{
					// update each child parent id with the real parent id (mostly used on insert)

					entity.AdminTaskEmailTemplate.AdminTaskTypeId = entity.AdminTaskTypeId;
					//DataRepository.AdminTaskEmailTemplateProvider.Save(transactionManager, entity.AdminTaskEmailTemplate);
					deepHandles.Add("AdminTaskEmailTemplate",
						new KeyValuePair<Delegate, object>((DeepSaveSingleHandle< AdminTaskEmailTemplate >) DataRepository.AdminTaskEmailTemplateProvider.DeepSave,
						new object[] { transactionManager, entity.AdminTaskEmailTemplate, deepSaveType, childTypes, innerList }
					));
				}
			} 
			#endregion 
	
			#region List<AdminTask>
				if (CanDeepSave(entity.AdminTaskCollection, "List<AdminTask>|AdminTaskCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(AdminTask child in entity.AdminTaskCollection)
					{
						if(child.AdminTaskTypeIdSource != null)
						{
							child.AdminTaskTypeId = child.AdminTaskTypeIdSource.AdminTaskTypeId;
						}
						else
						{
							child.AdminTaskTypeId = entity.AdminTaskTypeId;
						}

					}

					if (entity.AdminTaskCollection.Count > 0 || entity.AdminTaskCollection.DeletedItems.Count > 0)
					{
						//DataRepository.AdminTaskProvider.Save(transactionManager, entity.AdminTaskCollection);
						
						deepHandles.Add("AdminTaskCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< AdminTask >) DataRepository.AdminTaskProvider.DeepSave,
							new object[] { transactionManager, entity.AdminTaskCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region AdminTaskTypeChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.AdminTaskType</c>
	///</summary>
	public enum AdminTaskTypeChildEntityTypes
	{
		///<summary>
		/// Entity <c>AdminTaskEmailTemplate</c> as OneToOne for AdminTaskEmailTemplate
		///</summary>
		[ChildEntityType(typeof(AdminTaskEmailTemplate))]
		AdminTaskEmailTemplate,

		///<summary>
		/// Collection of <c>AdminTaskType</c> as OneToMany for AdminTaskCollection
		///</summary>
		[ChildEntityType(typeof(TList<AdminTask>))]
		AdminTaskCollection,
	}
	
	#endregion AdminTaskTypeChildEntityTypes
	
	#region AdminTaskTypeFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;AdminTaskTypeColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AdminTaskType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AdminTaskTypeFilterBuilder : SqlFilterBuilder<AdminTaskTypeColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AdminTaskTypeFilterBuilder class.
		/// </summary>
		public AdminTaskTypeFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the AdminTaskTypeFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AdminTaskTypeFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AdminTaskTypeFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AdminTaskTypeFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AdminTaskTypeFilterBuilder
	
	#region AdminTaskTypeParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;AdminTaskTypeColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AdminTaskType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AdminTaskTypeParameterBuilder : ParameterizedSqlFilterBuilder<AdminTaskTypeColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AdminTaskTypeParameterBuilder class.
		/// </summary>
		public AdminTaskTypeParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the AdminTaskTypeParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AdminTaskTypeParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AdminTaskTypeParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AdminTaskTypeParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AdminTaskTypeParameterBuilder
	
	#region AdminTaskTypeSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;AdminTaskTypeColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AdminTaskType"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class AdminTaskTypeSortBuilder : SqlSortBuilder<AdminTaskTypeColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AdminTaskTypeSqlSortBuilder class.
		/// </summary>
		public AdminTaskTypeSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion AdminTaskTypeSortBuilder
	
} // end namespace
