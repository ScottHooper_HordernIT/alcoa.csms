﻿#region Using directives

using System;

#endregion

namespace KaiZen.CSMS.Entities
{	
	///<summary>
	/// An object representation of the 'ConfigText2' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class ConfigText2 : ConfigText2Base
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="ConfigText2"/> instance.
		///</summary>
		public ConfigText2():base(){}	
		
		#endregion
	}
}
