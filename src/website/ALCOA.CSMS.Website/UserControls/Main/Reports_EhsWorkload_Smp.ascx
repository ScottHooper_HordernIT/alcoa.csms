﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="UserControls_Reports_EhsWorkload_Smp" Codebehind="Reports_EhsWorkload_Smp.ascx.cs" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
    <%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"  Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Src="~/UserControls/Other/ExportButtons.ascx" TagName="ExportButtons" TagPrefix="uc1" %>

<table border="0" cellpadding="2" cellspacing="0" width="100%">
    <tr>
        <td class="pageName" colspan="3" style="height: 16px">
        <span class="bodycopy"><span class="title">Performance Management Reports</span><br />
                <span class="date">Work Levelling Tool > Safety Management Plans</span><br />
            <img src="images/grfc_dottedline.gif" width="24" height="1"></span></td>
    </tr>
        <tr>
            <td colspan="3" style="height: 24px; text-align: left">
                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                    <tr>
                        <td style="width: 44px; height: 28px; text-align: right">
                            &nbsp;<strong>Year:</strong>&nbsp;</td>
                        <td style="width: 100px; height: 28px">
                            <dxe:ASPxComboBox ID="cmbYear" runat="server" AutoPostBack="True" ClientInstanceName="cmbYear"
                                CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue"
                                IncrementalFilteringMode="StartsWith"
                                OnSelectedIndexChanged="cmbYear_SelectedIndexChanged" ValueType="System.String"
                                Width="55px" 
                                SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                                </LoadingPanelImage>
                                <ButtonStyle Width="13px">
                                </ButtonStyle>
                            </dxe:ASPxComboBox>
                        </td>
                        <td colspan="4" style="height: 28px" align="right">
                            &nbsp;</td>
                    </tr>
                </table>
                <dxwgv:ASPxGridView ID="grid" ClientInstanceName="grid" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                    CssPostfix="Office2003Blue" 
                    DataSourceID="dsFileDb_GetEhsConsultantsWorkLoad" AutoGenerateColumns="False">
                    <ImagesFilterControl>
                        <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
                        </LoadingPanel>
                    </ImagesFilterControl>
                    <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
                        CssPostfix="Office2003Blue">
                        <Header ImageSpacing="5px" SortingImageSpacing="5px">
                        </Header>
                        <LoadingPanel ImageSpacing="10px">
                        </LoadingPanel>
                    </Styles>
                    <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                        <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
                        </LoadingPanelOnStatusBar>
                        <LoadingPanel Url="~/App_Themes/Office2003BlueOld/GridView/Loading.gif"><%--Enhancement_023:change by debashis for testing platform upgradation--%>
                        </LoadingPanel>
                    </Images>
                    <Settings ShowFilterRow="True" ShowGroupPanel="True" ShowFooter="True" />
                    <Columns>
                        <dxwgv:GridViewDataTextColumn FieldName="FullName" ReadOnly="True" SortIndex="0"
                            SortOrder="Ascending" VisibleIndex="0" Width="200px">
                            <FooterCellStyle HorizontalAlign="Right">
                            </FooterCellStyle>
                            <FooterTemplate>
                                Total:
                            </FooterTemplate>
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataCheckColumn FieldName="Enabled" VisibleIndex="1">
                        </dxwgv:GridViewDataCheckColumn>
                        <dxwgv:GridViewDataTextColumn FieldName="NoAllocated" ReadOnly="True" VisibleIndex="2" Caption="# Allocated">
                            <CellStyle HorizontalAlign="Center">
                            </CellStyle>
                            <FooterCellStyle HorizontalAlign="Center">
                            </FooterCellStyle>
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn FieldName="NoSubmitted" ReadOnly="True" VisibleIndex="3" Caption="# Submitted">
                            <CellStyle HorizontalAlign="Center">
                            </CellStyle>
                            <FooterCellStyle HorizontalAlign="Center">
                            </FooterCellStyle>
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn FieldName="NoBeingAssessed" ReadOnly="True" VisibleIndex="4" Caption="# Being Assessed">
                            <CellStyle HorizontalAlign="Center">
                            </CellStyle>
                            <FooterCellStyle HorizontalAlign="Center">
                            </FooterCellStyle>
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn FieldName="NoApproved" ReadOnly="True" VisibleIndex="5" Caption="# Approved">
                            <CellStyle HorizontalAlign="Center">
                            </CellStyle>
                            <FooterCellStyle HorizontalAlign="Center">
                            </FooterCellStyle>
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn FieldName="NoRejected" ReadOnly="True" VisibleIndex="6" Caption="# Rejected">
                            <CellStyle HorizontalAlign="Center">
                            </CellStyle>
                            <FooterCellStyle HorizontalAlign="Center">
                            </FooterCellStyle>
                        </dxwgv:GridViewDataTextColumn>
                    </Columns>
                    <TotalSummary>
                        <dxwgv:ASPxSummaryItem DisplayFormat="{0}" FieldName="NoAllocated" ShowInColumn="# Allocated"
                            ShowInGroupFooterColumn="# Allocated" SummaryType="Sum" />
                        <dxwgv:ASPxSummaryItem DisplayFormat="{0}" FieldName="NoSubmitted" ShowInColumn="# Submitted"
                            ShowInGroupFooterColumn="# Submitted" SummaryType="Sum" />
                            <dxwgv:ASPxSummaryItem DisplayFormat="{0}" FieldName="NoBeingAssessed" ShowInColumn="# Being Assessed"
                            ShowInGroupFooterColumn="# Being Assessed" SummaryType="Sum" />
                            <dxwgv:ASPxSummaryItem DisplayFormat="{0}" FieldName="NoApproved" ShowInColumn="# Approved"
                            ShowInGroupFooterColumn="# Approved" SummaryType="Sum" />
                            <dxwgv:ASPxSummaryItem DisplayFormat="{0}" FieldName="NoRejected" ShowInColumn="# Rejected"
                            ShowInGroupFooterColumn="# Rejected" SummaryType="Sum" />
                    </TotalSummary>
                    <SettingsPager>
                        <AllButton Visible="True">
                        </AllButton>
                    </SettingsPager>
                    <StylesEditors>
                        <ProgressBar Height="25px">
                        </ProgressBar>
                    </StylesEditors>
                </dxwgv:ASPxGridView>
                <br />
                <dxe:ASPxLabel ID="lblUnallocated" runat="server">
                </dxe:ASPxLabel>
            </td>
        </tr>
        <tr align="right">
        <td colspan="3" style="padding-top:6px; text-align: right; text-align: -moz-right; width: 100%"
            align="right">
            <div align="right">
                <uc1:ExportButtons ID="ucExportButtons" runat="server" />
            </div>
        </td>
    </tr>
</table>

<asp:ObjectDataSource   ID="dsFileDb_GetDistinctYears"
                        runat="server"
                        TypeName="KaiZen.CSMS.Services.FileDbService"
                        SelectMethod="GetDistinctYears">
</asp:ObjectDataSource>

<asp:SqlDataSource ID="sqldsUnallocated" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
                    ProviderName="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString.ProviderName %>"
                    SelectCommand="SELECT COUNT(ISNULL(EHSConsultantId,0)) As [Unallocated] FROM dbo.FileDB WHERE Description LIKE '%' + @CurrentYear + '%'">
    <SelectParameters>
        <asp:QueryStringParameter DefaultValue="2008" Name="CurrentYear" QueryStringField="spVar_CurrentYear" />
    </SelectParameters>
</asp:SqlDataSource>

                

<asp:ObjectDataSource ID="dsFileDb_GetEhsConsultantsWorkLoad" runat="server" TypeName="KaiZen.CSMS.Services.FileDbService" SelectMethod="GetEhsConsultantsWorkload">
<SelectParameters>
<asp:SessionParameter DefaultValue="%" Name="CurrentYear" SessionField="spVar_Year" Type="String" />
</SelectParameters>
</asp:ObjectDataSource>