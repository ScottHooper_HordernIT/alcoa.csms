﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="AdminTaskEmailLogProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class AdminTaskEmailLogProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.AdminTaskEmailLog, KaiZen.CSMS.Entities.AdminTaskEmailLogKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.AdminTaskEmailLogKey key)
		{
			return Delete(transactionManager, key.AdminTaskEmailId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_adminTaskEmailId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _adminTaskEmailId)
		{
			return Delete(null, _adminTaskEmailId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_adminTaskEmailId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _adminTaskEmailId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_AdminTaskEmail_AdminTaskId key.
		///		FK_AdminTaskEmail_AdminTaskId Description: 
		/// </summary>
		/// <param name="_adminTaskId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.AdminTaskEmailLog objects.</returns>
		public TList<AdminTaskEmailLog> GetByAdminTaskId(System.Int32 _adminTaskId)
		{
			int count = -1;
			return GetByAdminTaskId(_adminTaskId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_AdminTaskEmail_AdminTaskId key.
		///		FK_AdminTaskEmail_AdminTaskId Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_adminTaskId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.AdminTaskEmailLog objects.</returns>
		/// <remarks></remarks>
		public TList<AdminTaskEmailLog> GetByAdminTaskId(TransactionManager transactionManager, System.Int32 _adminTaskId)
		{
			int count = -1;
			return GetByAdminTaskId(transactionManager, _adminTaskId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_AdminTaskEmail_AdminTaskId key.
		///		FK_AdminTaskEmail_AdminTaskId Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_adminTaskId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.AdminTaskEmailLog objects.</returns>
		public TList<AdminTaskEmailLog> GetByAdminTaskId(TransactionManager transactionManager, System.Int32 _adminTaskId, int start, int pageLength)
		{
			int count = -1;
			return GetByAdminTaskId(transactionManager, _adminTaskId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_AdminTaskEmail_AdminTaskId key.
		///		fkAdminTaskEmailAdminTaskId Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_adminTaskId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.AdminTaskEmailLog objects.</returns>
		public TList<AdminTaskEmailLog> GetByAdminTaskId(System.Int32 _adminTaskId, int start, int pageLength)
		{
			int count =  -1;
			return GetByAdminTaskId(null, _adminTaskId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_AdminTaskEmail_AdminTaskId key.
		///		fkAdminTaskEmailAdminTaskId Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_adminTaskId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.AdminTaskEmailLog objects.</returns>
		public TList<AdminTaskEmailLog> GetByAdminTaskId(System.Int32 _adminTaskId, int start, int pageLength,out int count)
		{
			return GetByAdminTaskId(null, _adminTaskId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_AdminTaskEmail_AdminTaskId key.
		///		FK_AdminTaskEmail_AdminTaskId Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_adminTaskId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.AdminTaskEmailLog objects.</returns>
		public abstract TList<AdminTaskEmailLog> GetByAdminTaskId(TransactionManager transactionManager, System.Int32 _adminTaskId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_AdminTaskEmailLog_CsmsEmailLogId key.
		///		FK_AdminTaskEmailLog_CsmsEmailLogId Description: 
		/// </summary>
		/// <param name="_csmsEmailLogId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.AdminTaskEmailLog objects.</returns>
		public TList<AdminTaskEmailLog> GetByCsmsEmailLogId(System.Int32 _csmsEmailLogId)
		{
			int count = -1;
			return GetByCsmsEmailLogId(_csmsEmailLogId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_AdminTaskEmailLog_CsmsEmailLogId key.
		///		FK_AdminTaskEmailLog_CsmsEmailLogId Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_csmsEmailLogId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.AdminTaskEmailLog objects.</returns>
		/// <remarks></remarks>
		public TList<AdminTaskEmailLog> GetByCsmsEmailLogId(TransactionManager transactionManager, System.Int32 _csmsEmailLogId)
		{
			int count = -1;
			return GetByCsmsEmailLogId(transactionManager, _csmsEmailLogId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_AdminTaskEmailLog_CsmsEmailLogId key.
		///		FK_AdminTaskEmailLog_CsmsEmailLogId Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_csmsEmailLogId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.AdminTaskEmailLog objects.</returns>
		public TList<AdminTaskEmailLog> GetByCsmsEmailLogId(TransactionManager transactionManager, System.Int32 _csmsEmailLogId, int start, int pageLength)
		{
			int count = -1;
			return GetByCsmsEmailLogId(transactionManager, _csmsEmailLogId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_AdminTaskEmailLog_CsmsEmailLogId key.
		///		fkAdminTaskEmailLogCsmsEmailLogId Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_csmsEmailLogId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.AdminTaskEmailLog objects.</returns>
		public TList<AdminTaskEmailLog> GetByCsmsEmailLogId(System.Int32 _csmsEmailLogId, int start, int pageLength)
		{
			int count =  -1;
			return GetByCsmsEmailLogId(null, _csmsEmailLogId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_AdminTaskEmailLog_CsmsEmailLogId key.
		///		fkAdminTaskEmailLogCsmsEmailLogId Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_csmsEmailLogId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.AdminTaskEmailLog objects.</returns>
		public TList<AdminTaskEmailLog> GetByCsmsEmailLogId(System.Int32 _csmsEmailLogId, int start, int pageLength,out int count)
		{
			return GetByCsmsEmailLogId(null, _csmsEmailLogId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_AdminTaskEmailLog_CsmsEmailLogId key.
		///		FK_AdminTaskEmailLog_CsmsEmailLogId Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_csmsEmailLogId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.AdminTaskEmailLog objects.</returns>
		public abstract TList<AdminTaskEmailLog> GetByCsmsEmailLogId(TransactionManager transactionManager, System.Int32 _csmsEmailLogId, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.AdminTaskEmailLog Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.AdminTaskEmailLogKey key, int start, int pageLength)
		{
			return GetByAdminTaskEmailId(transactionManager, key.AdminTaskEmailId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_AdminTaskEmailLog index.
		/// </summary>
		/// <param name="_adminTaskEmailId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdminTaskEmailLog"/> class.</returns>
		public KaiZen.CSMS.Entities.AdminTaskEmailLog GetByAdminTaskEmailId(System.Int32 _adminTaskEmailId)
		{
			int count = -1;
			return GetByAdminTaskEmailId(null,_adminTaskEmailId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_AdminTaskEmailLog index.
		/// </summary>
		/// <param name="_adminTaskEmailId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdminTaskEmailLog"/> class.</returns>
		public KaiZen.CSMS.Entities.AdminTaskEmailLog GetByAdminTaskEmailId(System.Int32 _adminTaskEmailId, int start, int pageLength)
		{
			int count = -1;
			return GetByAdminTaskEmailId(null, _adminTaskEmailId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_AdminTaskEmailLog index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_adminTaskEmailId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdminTaskEmailLog"/> class.</returns>
		public KaiZen.CSMS.Entities.AdminTaskEmailLog GetByAdminTaskEmailId(TransactionManager transactionManager, System.Int32 _adminTaskEmailId)
		{
			int count = -1;
			return GetByAdminTaskEmailId(transactionManager, _adminTaskEmailId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_AdminTaskEmailLog index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_adminTaskEmailId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdminTaskEmailLog"/> class.</returns>
		public KaiZen.CSMS.Entities.AdminTaskEmailLog GetByAdminTaskEmailId(TransactionManager transactionManager, System.Int32 _adminTaskEmailId, int start, int pageLength)
		{
			int count = -1;
			return GetByAdminTaskEmailId(transactionManager, _adminTaskEmailId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_AdminTaskEmailLog index.
		/// </summary>
		/// <param name="_adminTaskEmailId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdminTaskEmailLog"/> class.</returns>
		public KaiZen.CSMS.Entities.AdminTaskEmailLog GetByAdminTaskEmailId(System.Int32 _adminTaskEmailId, int start, int pageLength, out int count)
		{
			return GetByAdminTaskEmailId(null, _adminTaskEmailId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_AdminTaskEmailLog index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_adminTaskEmailId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdminTaskEmailLog"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.AdminTaskEmailLog GetByAdminTaskEmailId(TransactionManager transactionManager, System.Int32 _adminTaskEmailId, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;AdminTaskEmailLog&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;AdminTaskEmailLog&gt;"/></returns>
		public static TList<AdminTaskEmailLog> Fill(IDataReader reader, TList<AdminTaskEmailLog> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.AdminTaskEmailLog c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("AdminTaskEmailLog")
					.Append("|").Append((System.Int32)reader[((int)AdminTaskEmailLogColumn.AdminTaskEmailId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<AdminTaskEmailLog>(
					key.ToString(), // EntityTrackingKey
					"AdminTaskEmailLog",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.AdminTaskEmailLog();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.AdminTaskEmailId = (System.Int32)reader[((int)AdminTaskEmailLogColumn.AdminTaskEmailId - 1)];
					c.AdminTaskId = (System.Int32)reader[((int)AdminTaskEmailLogColumn.AdminTaskId - 1)];
					c.CsmsEmailLogId = (System.Int32)reader[((int)AdminTaskEmailLogColumn.CsmsEmailLogId - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.AdminTaskEmailLog"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.AdminTaskEmailLog"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.AdminTaskEmailLog entity)
		{
			if (!reader.Read()) return;
			
			entity.AdminTaskEmailId = (System.Int32)reader[((int)AdminTaskEmailLogColumn.AdminTaskEmailId - 1)];
			entity.AdminTaskId = (System.Int32)reader[((int)AdminTaskEmailLogColumn.AdminTaskId - 1)];
			entity.CsmsEmailLogId = (System.Int32)reader[((int)AdminTaskEmailLogColumn.CsmsEmailLogId - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.AdminTaskEmailLog"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.AdminTaskEmailLog"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.AdminTaskEmailLog entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.AdminTaskEmailId = (System.Int32)dataRow["AdminTaskEmailId"];
			entity.AdminTaskId = (System.Int32)dataRow["AdminTaskId"];
			entity.CsmsEmailLogId = (System.Int32)dataRow["CsmsEmailLogId"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.AdminTaskEmailLog"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.AdminTaskEmailLog Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.AdminTaskEmailLog entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region AdminTaskIdSource	
			if (CanDeepLoad(entity, "AdminTask|AdminTaskIdSource", deepLoadType, innerList) 
				&& entity.AdminTaskIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.AdminTaskId;
				AdminTask tmpEntity = EntityManager.LocateEntity<AdminTask>(EntityLocator.ConstructKeyFromPkItems(typeof(AdminTask), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.AdminTaskIdSource = tmpEntity;
				else
					entity.AdminTaskIdSource = DataRepository.AdminTaskProvider.GetByAdminTaskId(transactionManager, entity.AdminTaskId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'AdminTaskIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.AdminTaskIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.AdminTaskProvider.DeepLoad(transactionManager, entity.AdminTaskIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion AdminTaskIdSource

			#region CsmsEmailLogIdSource	
			if (CanDeepLoad(entity, "CsmsEmailLog|CsmsEmailLogIdSource", deepLoadType, innerList) 
				&& entity.CsmsEmailLogIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.CsmsEmailLogId;
				CsmsEmailLog tmpEntity = EntityManager.LocateEntity<CsmsEmailLog>(EntityLocator.ConstructKeyFromPkItems(typeof(CsmsEmailLog), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.CsmsEmailLogIdSource = tmpEntity;
				else
					entity.CsmsEmailLogIdSource = DataRepository.CsmsEmailLogProvider.GetByCsmsEmailLogId(transactionManager, entity.CsmsEmailLogId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CsmsEmailLogIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.CsmsEmailLogIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.CsmsEmailLogProvider.DeepLoad(transactionManager, entity.CsmsEmailLogIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion CsmsEmailLogIdSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.AdminTaskEmailLog object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.AdminTaskEmailLog instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.AdminTaskEmailLog Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.AdminTaskEmailLog entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region AdminTaskIdSource
			if (CanDeepSave(entity, "AdminTask|AdminTaskIdSource", deepSaveType, innerList) 
				&& entity.AdminTaskIdSource != null)
			{
				DataRepository.AdminTaskProvider.Save(transactionManager, entity.AdminTaskIdSource);
				entity.AdminTaskId = entity.AdminTaskIdSource.AdminTaskId;
			}
			#endregion 
			
			#region CsmsEmailLogIdSource
			if (CanDeepSave(entity, "CsmsEmailLog|CsmsEmailLogIdSource", deepSaveType, innerList) 
				&& entity.CsmsEmailLogIdSource != null)
			{
				DataRepository.CsmsEmailLogProvider.Save(transactionManager, entity.CsmsEmailLogIdSource);
				entity.CsmsEmailLogId = entity.CsmsEmailLogIdSource.CsmsEmailLogId;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region AdminTaskEmailLogChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.AdminTaskEmailLog</c>
	///</summary>
	public enum AdminTaskEmailLogChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>AdminTask</c> at AdminTaskIdSource
		///</summary>
		[ChildEntityType(typeof(AdminTask))]
		AdminTask,
			
		///<summary>
		/// Composite Property for <c>CsmsEmailLog</c> at CsmsEmailLogIdSource
		///</summary>
		[ChildEntityType(typeof(CsmsEmailLog))]
		CsmsEmailLog,
		}
	
	#endregion AdminTaskEmailLogChildEntityTypes
	
	#region AdminTaskEmailLogFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;AdminTaskEmailLogColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AdminTaskEmailLog"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AdminTaskEmailLogFilterBuilder : SqlFilterBuilder<AdminTaskEmailLogColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailLogFilterBuilder class.
		/// </summary>
		public AdminTaskEmailLogFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailLogFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AdminTaskEmailLogFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailLogFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AdminTaskEmailLogFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AdminTaskEmailLogFilterBuilder
	
	#region AdminTaskEmailLogParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;AdminTaskEmailLogColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AdminTaskEmailLog"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AdminTaskEmailLogParameterBuilder : ParameterizedSqlFilterBuilder<AdminTaskEmailLogColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailLogParameterBuilder class.
		/// </summary>
		public AdminTaskEmailLogParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailLogParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AdminTaskEmailLogParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailLogParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AdminTaskEmailLogParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AdminTaskEmailLogParameterBuilder
	
	#region AdminTaskEmailLogSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;AdminTaskEmailLogColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AdminTaskEmailLog"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class AdminTaskEmailLogSortBuilder : SqlSortBuilder<AdminTaskEmailLogColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailLogSqlSortBuilder class.
		/// </summary>
		public AdminTaskEmailLogSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion AdminTaskEmailLogSortBuilder
	
} // end namespace
