<%@ Control Language="C#" AutoEventWireup="true" Inherits="UserControls_Tiny_QuestionnaireReports_QuestionnaireReport_Progress" Codebehind="QuestionnaireReport_Progress.ascx.cs" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dxpc" %>
<%@ Register Src="../../Other/ExportButtons.ascx" TagName="ExportButtons" TagPrefix="uc1" %>
    
<asp:UpdatePanel id="UpdatePanel2" runat="server" UpdateMode="Conditional">
<ContentTemplate>
    <style type="text/css">
        .style1
        {
            color: #FF0000;
            font-size: small;
        }
    </style>

    <table border="0" cellpadding="2" cellspacing="0" width="100%">
    <tr>
        <td class="pageName" style="height: 17px">
            <a name="save"></a>
            <span class="bodycopy"><span class="title">Reports</span><br />
            <span class="date">Safety Qualification &gt; Status</span><br />
            <img height="1" src="images/grfc_dottedline.gif" width="24" />&nbsp;</span>
        </td>
    </tr>
    <tr>
        <td class="style1" style="height: 17px; text-align: center; color: #FF0000; font-size: small;">
            <strong>This is not the Qualified Contractors Database, this is to be used to check process status only.</strong><br />
            <strong>To determine if a company is authorised to be on any location, please refer to the <a href="Reports_SafetyPQOverview.aspx">Qualified Contractors Database</a></strong>
        </td>
    </tr>
    </table>
    <table>
    <tr>
        <td>
            <table border="0" cellpadding="0" cellspacing="0" style="vertical-align: middle">
            <tr>
                <td style="width: 120px; height: 46px; text-align: right">
                    <strong>Company:</strong>
                </td>
                <td style="width: 100px; height: 46px; padding-left: 2px;">
                    <dxe:ASPxComboBox ID="cbFilterCompany" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                        CssPostfix="Office2003Blue" ValueType="System.String"
                        Width="130px" IncrementalFilteringMode="StartsWith" 
                        SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                        <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif" />
                        <ButtonStyle Width="13px" />
                    </dxe:ASPxComboBox>
                </td>
                <%--<td style="width: 56px; height: 34px; text-align: right">
                    Type:</td>
                <td style="vertical-align: middle; width: 100px; height: 34px; padding-left: 2px;">
                    <dxe:ASPxComboBox ID="cbFilterType" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                        CssPostfix="Office2003Blue" ValueType="System.String"
                        Width="130px" IncrementalFilteringMode="StartsWith" 
                        SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                        <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                        </LoadingPanelImage>
                        <ButtonStyle Width="13px">
                        </ButtonStyle>
                    </dxe:ASPxComboBox>
                </td>--%>
                <td style="width: 93px; height: 46px; text-align: right">
                    <dxe:ASPxButton ID="btnFilter" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                        CssPostfix="Office2003Blue" Text="Filter" Width="81px" 
                        OnClick="btnFilter_Click" ClientInstanceName="btnFilter" 
                        SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                    </dxe:ASPxButton>
                </td>
                <td style="width: 129px; height: 46px; text-align: right">
                    <dxe:ASPxButton ID="btn1" runat="server" AutoPostBack="False" Text="Report Manager" UseSubmitBehavior="False" Width="120px">
                    </dxe:ASPxButton>
                </td>
                <td style="width: 200px; height: 46px; text-align: right; padding-right: 2px;">
                    <strong>Current Report:</strong></td>
                <td style="width: 144px; height: 46px; text-align: left">
                    <dxe:ASPxLabel ID="lblLayout" runat="server" Font-Bold="False" Font-Italic="False" Text="Default" Width="100%">
                    </dxe:ASPxLabel>
                </td>
                <td style="width:621px; height: 46px;">
                    <div align="right">
                        <a style="color: #0000ff; text-align: right; text-decoration: none" href="javascript:ShowHideCustomizationWindow()">Customize</a>
                    </div>
                </td>
            </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <dxwgv:ASPxGridView ID="grid" runat="server" AutoGenerateColumns="False" ClientInstanceName="grid"
                CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"  Width="100%"
                CssPostfix="Office2003Blue"
                DataSourceID="QuestionnaireReportProgressDataSource1" 
                OnHtmlRowCreated="grid_RowCreated"
                OnHtmlDataCellPrepared="grid_HtmlDataCellPrepared" >
                <SettingsCustomizationWindow Enabled="True" />
                <SettingsBehavior ColumnResizeMode="Control" />
                <ImagesFilterControl>
                    <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif" />
                </ImagesFilterControl>
                <Styles CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css">
                    <Header SortingImageSpacing="5px" ImageSpacing="5px" />
                    <LoadingPanel ImageSpacing="10px" />
                    <AlternatingRow Enabled="True" />
                </Styles>
                <Columns>
                    <dxwgv:GridViewDataTextColumn Caption=" " FieldName="QuestionnaireId" FixedStyle="Left" ReadOnly="True" VisibleIndex="4" Width="52px">
                        <DataItemTemplate>
                            <table cellpadding="0" cellspacing="0"><tr>
                                <td><asp:HyperLink ID="hlAssess" runat="server" Text="Review" NavigateUrl=""></asp:HyperLink></td>
                            </tr></table>
                        </DataItemTemplate>
                        <Settings AllowHeaderFilter="False" />
                        <EditFormSettings Visible="False" />
                    </dxwgv:GridViewDataTextColumn>
        
                    <dxwgv:GridViewDataTextColumn FieldName="CompanyName" Width="205px" Caption="Company Name" SortIndex="0" SortOrder="Ascending" VisibleIndex="5">
                    </dxwgv:GridViewDataTextColumn>
        
                    <dxwgv:GridViewDataTextColumn Caption="Type" FieldName="Type" VisibleIndex="7" Width="120px">
                        <HeaderStyle HorizontalAlign="Center" />
                        <CellStyle HorizontalAlign="Left" />
                    </dxwgv:GridViewDataTextColumn>
        
                    <dxwgv:GridViewDataComboBoxColumn Caption="Questionnaire Status" FieldName="Status" VisibleIndex="6" Width="150px">
                        <PropertiesComboBox DataSourceID="QuestionnaireStatusDataSource" TextField="QuestionnaireStatusDesc" ValueField="QuestionnaireStatusId" ValueType="System.Int32" />
                        <HeaderStyle HorizontalAlign="Center" Wrap="True" />
                    </dxwgv:GridViewDataComboBoxColumn>
        
                    <dxwgv:GridViewDataTextColumn FieldName="SupplierContact" Visible="False" Width="110px">
                        <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                    </dxwgv:GridViewDataTextColumn>
        
                    <dxwgv:GridViewDataTextColumn FieldName="SupplierContactPhone" Visible="False" Width="110px" VisibleIndex="0">
                        <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                    </dxwgv:GridViewDataTextColumn>
        
                    <dxwgv:GridViewDataTextColumn FieldName="SupplierContactEmail" Visible="False" Width="110px" VisibleIndex="1">
                        <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                    </dxwgv:GridViewDataTextColumn>
        
                    <dxwgv:GridViewDataTextColumn FieldName="UserDescription" Caption="Presently With" VisibleIndex="8" Width="115px">
                        <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                    </dxwgv:GridViewDataTextColumn>
        
                    <dxwgv:GridViewDataTextColumn FieldName="ActionDescription" Caption="Presently With Details" Visible="false" Width="110px" VisibleIndex="2">
                        <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                    </dxwgv:GridViewDataTextColumn>
        
                    <dxwgv:GridViewDataTextColumn FieldName="QuestionnairePresentlyWithSinceDaysCount" Caption="Days With" Visible="false" Width="110px" VisibleIndex="3">
                        <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                    </dxwgv:GridViewDataTextColumn>
        
                    <dxwgv:GridViewDataTextColumn Caption="Procurement Risk Rating" FieldName="InitialRiskAssessment" VisibleIndex="9" Width="110px">
                        <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                    </dxwgv:GridViewDataTextColumn>
        
                    <dxwgv:GridViewDataTextColumn FieldName="MainAssessmentRiskRating" Width="100px" Caption="33.055.1 Risk Rating" VisibleIndex="10">
                        <DataItemTemplate>
                            <table cellpadding="0" cellspacing="0"><tr>
                                <td><asp:HyperLink ID="RiskLevel" runat="server" Text="" NavigateUrl=""></asp:HyperLink></td>
                            </tr></table>                      
                        </DataItemTemplate>
                        <HeaderStyle HorizontalAlign="Center" Wrap="True" />
                    </dxwgv:GridViewDataTextColumn>

                    <dxwgv:GridViewDataTextColumn Caption="Final Risk Rating" FieldName="FinalRiskRating" VisibleIndex="11">
                        <HeaderStyle HorizontalAlign="Center" Wrap="True" />
                    </dxwgv:GridViewDataTextColumn>
        
                    <dxwgv:GridViewDataComboBoxColumn FieldName="IsMainRequired" Caption="Is Safety Qualification Required?" VisibleIndex="13" Width="110px">
                        <PropertiesComboBox ValueType="System.Int32">
                            <Items><dxe:ListEditItem Text="Yes" Value="1"></dxe:ListEditItem><dxe:ListEditItem Text="No" Value="0"></dxe:ListEditItem></Items>
                            <ItemStyle Font-Bold="True" />
                        </PropertiesComboBox>
                        <Settings SortMode="DisplayText" />
                        <HeaderStyle HorizontalAlign="Center" Wrap="True" />
                        <CellStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                    </dxwgv:GridViewDataComboBoxColumn>
        
                    <dxwgv:GridViewDataTextColumn Caption="Process Status" FieldName="CompanyStatusName" Width="130px" VisibleIndex="14">
                        <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                    </dxwgv:GridViewDataTextColumn>
        
                    <dxwgv:GridViewDataComboBoxColumn FieldName="Recommended" Width="115px" VisibleIndex="15">
                        <PropertiesComboBox ValueType="System.Int32">
                            <Items><dxe:ListEditItem Text="Yes" Value="1"></dxe:ListEditItem><dxe:ListEditItem Text="No" Value="0"></dxe:ListEditItem></Items>
                        </PropertiesComboBox>
                        <Settings SortMode="DisplayText" />
                        <HeaderStyle HorizontalAlign="Center" Wrap="True" />
                        <CellStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                    </dxwgv:GridViewDataComboBoxColumn>
        
                    <dxwgv:GridViewDataDateColumn FieldName="InitialModifiedDate" Width="115px" Caption="Procurement Questionnaire Last Modified" VisibleIndex="16">
                        <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                    </dxwgv:GridViewDataDateColumn>
        
                    <dxwgv:GridViewDataComboBoxColumn FieldName="CreatedByUser" Width="120px" Caption="Initiator" VisibleIndex="17">
                        <PropertiesComboBox ValueType="System.String" />
                        <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                    </dxwgv:GridViewDataComboBoxColumn>

                    <dxwgv:GridViewDataComboBoxColumn Caption="Procurement Contact" FieldName="ProcurementContactUser" VisibleIndex="18" Width="150px">
                        <PropertiesComboBox ValueType="System.String" />
                        <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                    </dxwgv:GridViewDataComboBoxColumn>

                    <dxwgv:GridViewDataComboBoxColumn FieldName="ContractManagerUser" Width="140px" Caption="Procurement Functional Supervisor" VisibleIndex="19">
                        <PropertiesComboBox ValueType="System.String" />
                        <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                    </dxwgv:GridViewDataComboBoxColumn>

                    <dxwgv:GridViewDataComboBoxColumn FieldName="MainModifiedByUser" Width="120px" Caption="Supplier Questionnaire Last Modified By" VisibleIndex="21">
                        <PropertiesComboBox ValueType="System.String" />
                        <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                    </dxwgv:GridViewDataComboBoxColumn>

                    <dxwgv:GridViewDataDateColumn FieldName="MainModifiedDate" Width="120px" Caption="Supplier Questionnaire Last Modified On" VisibleIndex="20">
                        <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                    </dxwgv:GridViewDataDateColumn>

                    <dxwgv:GridViewDataComboBoxColumn FieldName="IsVerificationRequired" Caption="Is Verification Required?" VisibleIndex="22" Width="110px">
                        <PropertiesComboBox ValueType="System.Int32">
                            <Items><dxe:ListEditItem Text="Yes" Value="1"></dxe:ListEditItem><dxe:ListEditItem Text="No" Value="0"></dxe:ListEditItem></Items>
                            <ItemStyle Font-Bold="True" />
                        </PropertiesComboBox>
                        <Settings SortMode="DisplayText" />
                        <HeaderStyle HorizontalAlign="Center" Wrap="True" />
                        <CellStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                    </dxwgv:GridViewDataComboBoxColumn>

                    <dxwgv:GridViewDataDateColumn FieldName="VerificationModifiedDate" Width="110px" Caption="Verification Questionnaire Last Modified" VisibleIndex="23">
                        <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                    </dxwgv:GridViewDataDateColumn>

                    <dxwgv:GridViewDataComboBoxColumn Caption="Safety Assessor" FieldName="SafetyAssessor" Name="EHS Consultant" UnboundType="String" VisibleIndex="24" Width="140px">
                        <PropertiesComboBox ValueType="System.String" />
                        <HeaderStyle HorizontalAlign="Center" Wrap="True" />
                    </dxwgv:GridViewDataComboBoxColumn>
        
                    <dxwgv:GridViewDataCheckColumn Caption="De-Activated" FieldName="Deactivated2" VisibleIndex="25" Width="110px">
                        <HeaderStyle HorizontalAlign="Center" Wrap="True" />
                    </dxwgv:GridViewDataCheckColumn>

                    <dxwgv:GridViewDataTextColumn Caption="First Company Requesting" FieldName="RequestingCompanyName" Width="130px" VisibleIndex="26">
                        <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                    </dxwgv:GridViewDataTextColumn>

                    <dxwgv:GridViewDataTextColumn Caption="Reason Supplier Required" FieldName="ReasonContractor" Width="130px" VisibleIndex="27" Visible="false">
                        <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                    </dxwgv:GridViewDataTextColumn>

                    <dxwgv:GridViewDataTextColumn Caption="Service(s) - Primary" FieldName="ServicesMain" Width="200px" VisibleIndex="30" Visible="false">
                        <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                    </dxwgv:GridViewDataTextColumn>

                    <dxwgv:GridViewDataTextColumn Caption="Service(s) - Other" FieldName="ServicesOther" Width="200px" VisibleIndex="31" Visible="false">
                        <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                    </dxwgv:GridViewDataTextColumn>

                    <dxwgv:GridViewDataTextColumn Caption="Procurement Region" FieldName="ProcurementRegion" Width="200px" VisibleIndex="32" Visible="false">
                        <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                    </dxwgv:GridViewDataTextColumn>

                    <dxwgv:GridViewDataTextColumn Caption="Safety Assessor Region" FieldName="SafetyAssessorRegion" Width="200px" VisibleIndex="33" Visible="false">
                        <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                    </dxwgv:GridViewDataTextColumn>

                    <dxwgv:GridViewDataDateColumn Caption="Date Created" FieldName="CreatedDate" Visible="false" VisibleIndex="34">
                        <Settings SortMode="Value"></Settings>
                    </dxwgv:GridViewDataDateColumn>

                    <dxwgv:GridViewDataCheckColumn Caption="Is Re-Qualification?" FieldName="IsReQualification" VisibleIndex="28">
                        <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                    </dxwgv:GridViewDataCheckColumn>
        
                    <dxwgv:GridViewDataDateColumn Caption="Expires At" FieldName="MainAssessmentValidTo" VisibleIndex="29">
                    </dxwgv:GridViewDataDateColumn>
    
                    <dxwgv:GridViewDataTextColumn Caption="MainScoreOverall" FieldName="MainScorePOverall" VisibleIndex="35">
                    </dxwgv:GridViewDataTextColumn>
                </Columns>

                <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                    <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
                    </LoadingPanelOnStatusBar>
                    <LoadingPanel Url="~/App_Themes/Office2003BlueOld/GridView/Loading.gif">
                    </LoadingPanel>
                </Images>

                <Settings ShowHeaderFilterButton="True" ShowGroupPanel="True" ShowHorizontalScrollBar="True" ShowVerticalScrollBar="True" VerticalScrollableHeight="275" />
                <SettingsPager>
                    <AllButton Visible="True" />
                </SettingsPager>
                <StylesEditors>
                    <ProgressBar Height="25px" />
                </StylesEditors>
            </dxwgv:ASPxGridView>
        </td>
    </tr>
    <tr align="right">
        <td align="right" class="pageName" colspan="7" style="height: 20px; text-align: right; text-align:-moz-right; width: 900px;">
            <div align="right">
                <uc1:ExportButtons ID="ucExportButtons" runat="server" />
            </div>
        </td>
    </tr>
    </table>
</ContentTemplate>
<triggers>
    <asp:AsyncPostBackTrigger ControlID="btnFilter" EventName="Click"></asp:AsyncPostBackTrigger>
</triggers>
</asp:UpdatePanel>

<asp:SqlDataSource ID="sqldsCompaniesList" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="_Companies_GetCompanyNameList" SelectCommandType="StoredProcedure">
</asp:SqlDataSource>

<data:QuestionnaireReportProgressDataSource ID="QuestionnaireReportProgressDataSource" runat="server">
</data:QuestionnaireReportProgressDataSource>

<asp:SqlDataSource ID="QuestionnaireReportProgressDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="QuestionnaireReportProgress_Get_List" SelectCommandType="StoredProcedure">
</asp:SqlDataSource>

<data:UsersFullNameDataSource ID="UsersFullNameDataSource1" runat="server">
</data:UsersFullNameDataSource>

<data:QuestionnaireStatusDataSource ID="QuestionnaireStatusDataSource" runat="server">
    <DeepLoadProperties Method="IncludeChildren" Recursive="False">
    </DeepLoadProperties>
</data:QuestionnaireStatusDataSource>

<data:CompaniesEhsConsultantsDataSource ID="CompaniesEhsConsultantsDataSource1" runat="server">
</data:CompaniesEhsConsultantsDataSource>
<br />

<asp:SqlDataSource ID="QuestionnaireReportProgressDataSource_FilterCompanies" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="_QuestionnaireReportProgress_FilterCompanies" SelectCommandType="StoredProcedure">
</asp:SqlDataSource>

<asp:SqlDataSource ID="QuestionnaireReportProgressDataSource_FilterType" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="_QuestionnaireReportProgress_FilterType" SelectCommandType="StoredProcedure">
</asp:SqlDataSource>

<dxpc:ASPxPopupControl ID="ASPxPopupControl1" runat="server" 
    HeaderText="Report Manager" Height="213px" Width="623px" 
    CssPostfix="Office2003Blue" EnableHotTrack="False" Modal="True" PopupElementID="btn1"
    AllowDragging="True" AllowResize="True" 
    PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" 
    CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
    SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
    <SizeGripImage Height="16px" Width="16px" />
    <HeaderStyle>
        <Paddings PaddingRight="6px" />
    </HeaderStyle>
    <CloseButtonImage Height="12px" Width="13px" />
    
    <ContentCollection>
    <dxpc:PopupControlContentControl ID="PopupControlContentControl1" runat="server" SupportsDisabledAttribute="True">
        <dxwgv:ASPxGridView runat="server" 
            CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
            CssPostfix="Office2003Blue" KeyFieldName="ReportId" AutoGenerateColumns="False" 
            DataSourceID="sqldsLayouts" Width="100%" ID="ASPxGridView1" 
            OnHtmlRowCreated="grid2_RowCreated" OnRowUpdating="grid2_RowUpdating" 
            OnRowInserting="grid2_RowInserting" OnInitNewRow="grid2_InitNewRow" 
            OnRowValidating="grid2_RowValidating" OnStartRowEditing="grid2_StartRowEditing">
            <Columns>
                <dxwgv:GridViewCommandColumn ShowInCustomizationForm="False" Name="commandCol" Width="100px" Caption="Action" VisibleIndex="0">
                    <EditButton Visible="True" Text="Edit">
                        <Image AlternateText="Edit" Url="~/Images/gridEdit.gif" />
                    </EditButton>
                    <NewButton Visible="True" Text="New">
                        <Image AlternateText="New" Url="~/Images/gridNew.gif" />
                    </NewButton>
                    <DeleteButton Visible="True" Text="Delete">
                        <Image AlternateText="Delete" Url="~/Images/gridDelete.gif" />
                    </DeleteButton>
                    <CancelButton Visible="True" Text="Cancel">
                        <Image AlternateText="Cancel" Url="../Images/gridCancel.gif" />
                    </CancelButton>
                    <UpdateButton Visible="True" Text="Save">
                        <Image AlternateText="Save" Url="../Images/gridSave.gif" />
                    </UpdateButton>
                </dxwgv:GridViewCommandColumn>
    
                <dxwgv:GridViewDataTextColumn FieldName="ReportId" ReadOnly="True" ShowInCustomizationForm="True" Visible="False" VisibleIndex="1">
                    <EditFormSettings Visible="False" />
                </dxwgv:GridViewDataTextColumn>
    
                <dxwgv:GridViewDataTextColumn ShowInCustomizationForm="True" Width="30px" Caption=" " ToolTip="Load Selected Custom Report" VisibleIndex="1">
                    <EditFormSettings Visible="False" />
                    <DataItemTemplate>
                        <table cellpadding="0" cellspacing="0">
                            <tr><td style="width:30px"><dxe:ASPxHyperLink ID="hlLoad" runat="server" Text="Load"></dxe:ASPxHyperLink></td></tr>
                        </table>
                    </DataItemTemplate>
                    <CellStyle HorizontalAlign="Left" />
                </dxwgv:GridViewDataTextColumn>
    
                <dxwgv:GridViewDataTextColumn FieldName="Area" ShowInCustomizationForm="True" Visible="False" VisibleIndex="2">
                    <EditFormSettings Visible="True" />
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn FieldName="ReportName" ShowInCustomizationForm="True" VisibleIndex="2">
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataComboBoxColumn FieldName="ReportDescription" ReadOnly="True" ShowInCustomizationForm="True"
                    Name ="gcbReportDescription" Caption="Company" Visible="False" VisibleIndex="4">
                    <PropertiesComboBox DataSourceID="sqldsCompaniesList" TextField="CompanyName" ValueField="CompanyId" ValueType="System.String" DropDownHeight="150px" />
                    <EditFormSettings Visible="False" />
                </dxwgv:GridViewDataComboBoxColumn>

                <dxwgv:GridViewDataTextColumn FieldName="ReportLayout" ShowInCustomizationForm="True" Visible="False" VisibleIndex="5">
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn FieldName="ModifiedByUserId" ReadOnly="True" ShowInCustomizationForm="True" Visible="False" VisibleIndex="6">
                    <EditFormSettings Visible="True" />
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataDateColumn FieldName="ModifiedDate" SortIndex="0" SortOrder="Descending" ReadOnly="True" ShowInCustomizationForm="True" VisibleIndex="3">
                    <Settings SortMode="Value"></Settings>
                    <EditFormSettings Visible="True" />
                </dxwgv:GridViewDataDateColumn>
            </Columns>

            <SettingsBehavior ConfirmDelete="True" />
            <SettingsEditing Mode="Inline" />
            <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif" />
                <LoadingPanel Url="~/App_Themes/Office2003BlueOld/GridView/Loading.gif" />
            </Images>
            <ImagesFilterControl>
                <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif" />
            </ImagesFilterControl>

            <Styles CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css">
                <LoadingPanel ImageSpacing="10px" />
                <Header ImageSpacing="5px" SortingImageSpacing="5px" />
            </Styles>
        </dxwgv:ASPxGridView>

        <asp:SqlDataSource runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
            DeleteCommand="DELETE FROM [CustomReportingLayouts] WHERE [ReportId] = @ReportId"
            InsertCommand="INSERT INTO [CustomReportingLayouts] ([Area], [ReportName], [ReportDescription], [ReportLayout], [ModifiedByUserId], [ModifiedDate]) VALUES (@Area, @ReportName, @ReportDescription, @ReportLayout, @ModifiedByUserId, @ModifiedDate)" SelectCommand="SELECT * FROM [CustomReportingLayouts] WHERE ([ModifiedByUserId] = @ModifiedByUserId) AND Area = 'SQStatus'" UpdateCommand="UPDATE [CustomReportingLayouts] SET [Area] = @Area, [ReportName] = @ReportName, [ReportDescription] = @ReportDescription, [ReportLayout] = @ReportLayout, [ModifiedByUserId] = @ModifiedByUserId, [ModifiedDate] = @ModifiedDate WHERE [ReportId] = @ReportId"
            ID="sqldsLayouts">
            <DeleteParameters>
                <asp:Parameter Name="ReportId" Type="Int32"></asp:Parameter>
            </DeleteParameters>
            <InsertParameters>
                <asp:Parameter Name="Area" Type="String"></asp:Parameter>
                <asp:Parameter Name="ReportName" Type="String"></asp:Parameter>
                <asp:Parameter Name="ReportDescription" Type="String"></asp:Parameter>
                <asp:Parameter Name="ReportLayout" Type="String"></asp:Parameter>
                <asp:Parameter Name="ModifiedByUserId" Type="Int32"></asp:Parameter>
                <asp:Parameter Name="ModifiedDate" Type="DateTime"></asp:Parameter>
            </InsertParameters>
            <SelectParameters>
            <asp:SessionParameter SessionField="UserId" DefaultValue="0" Name="ModifiedByUserId" Type="Int32"></asp:SessionParameter>
            </SelectParameters>
            <UpdateParameters>
                <asp:Parameter Name="Area" Type="String"></asp:Parameter>
                <asp:Parameter Name="ReportName" Type="String"></asp:Parameter>
                <asp:Parameter Name="ReportDescription" Type="String"></asp:Parameter>
                <asp:Parameter Name="ReportLayout" Type="String"></asp:Parameter>
                <asp:Parameter Name="ModifiedByUserId" Type="Int32"></asp:Parameter>
                <asp:Parameter Name="ModifiedDate" Type="DateTime"></asp:Parameter>
                <asp:Parameter Name="ReportId" Type="Int32"></asp:Parameter>
            </UpdateParameters>
        </asp:SqlDataSource>
    </dxpc:PopupControlContentControl>
    </ContentCollection>
</dxpc:ASPxPopupControl>
