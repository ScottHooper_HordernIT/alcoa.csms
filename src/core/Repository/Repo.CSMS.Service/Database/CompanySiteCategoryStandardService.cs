﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;
using Repo.CSMS.Service.Model;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface ICompanySiteCategoryStandardService
    {
        CompanySiteCategoryStandard Get(Expression<Func<CompanySiteCategoryStandard, bool>> where, List<Expression<Func<CompanySiteCategoryStandard, object>>> includes);
        List<CompanySiteCategoryStandard> GetMany(int? top, Expression<Func<CompanySiteCategoryStandard, bool>> where, Func<IQueryable<CompanySiteCategoryStandard>, IOrderedQueryable<CompanySiteCategoryStandard>> orderBy, List<Expression<Func<CompanySiteCategoryStandard, object>>> includes);
        List<CompanySiteCategoryStandard> GetManyPaged(int pageNumber, int pageSize, Expression<Func<CompanySiteCategoryStandard, bool>> where, Func<IQueryable<CompanySiteCategoryStandard>, IOrderedQueryable<CompanySiteCategoryStandard>> orderBy, List<Expression<Func<CompanySiteCategoryStandard, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<CompanySiteCategoryStandard, bool>> where, List<Expression<Func<CompanySiteCategoryStandard, object>>> includes);
        List<GroupCount> GetGroupCount(Expression<Func<CompanySiteCategoryStandard, bool>> where, Func<CompanySiteCategoryStandard, object> selector);
        void Insert(CompanySiteCategoryStandard item, bool saveChanges = true);
        void Update(CompanySiteCategoryStandard item, bool saveChanges = true);
        void Delete(CompanySiteCategoryStandard item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class CompanySiteCategoryStandardService : ICompanySiteCategoryStandardService
    {
        private readonly IRepository<CompanySiteCategoryStandard> _repository;

        public CompanySiteCategoryStandardService(IRepository<CompanySiteCategoryStandard> repository)
        {
            this._repository = repository;
        }

        public CompanySiteCategoryStandard Get(Expression<Func<CompanySiteCategoryStandard, bool>> where, List<Expression<Func<CompanySiteCategoryStandard, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<CompanySiteCategoryStandard> GetMany(int? top, Expression<Func<CompanySiteCategoryStandard, bool>> where, Func<IQueryable<CompanySiteCategoryStandard>, IOrderedQueryable<CompanySiteCategoryStandard>> orderBy, List<Expression<Func<CompanySiteCategoryStandard, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<CompanySiteCategoryStandard> GetManyPaged(int pageNumber, int pageSize, Expression<Func<CompanySiteCategoryStandard, bool>> where, Func<IQueryable<CompanySiteCategoryStandard>, IOrderedQueryable<CompanySiteCategoryStandard>> orderBy, List<Expression<Func<CompanySiteCategoryStandard, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<CompanySiteCategoryStandard, bool>> where, List<Expression<Func<CompanySiteCategoryStandard, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public List<GroupCount> GetGroupCount(Expression<Func<CompanySiteCategoryStandard, bool>> where, Func<CompanySiteCategoryStandard, object> selector)
        {
            return _repository.GetMany(where).GroupBy(selector).Select(g => new GroupCount { Name = g.Key, Count = g.Count() }).ToList();
        }

        public void Insert(CompanySiteCategoryStandard item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("CompanySiteCategoryStandard");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(CompanySiteCategoryStandard item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("CompanySiteCategoryStandard");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(CompanySiteCategoryStandard item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("CompanySiteCategoryStandard");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
