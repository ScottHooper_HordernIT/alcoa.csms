﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class SafetyPQ_QuestionnaireVerificationSection : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
       

        try
        {
            String sNo = Request.QueryString["s"].ToString();
            String qNo = Request.QueryString["q"].ToString();
            Control uc = LoadControl("~/UserControls/Main/SqQuestionnaire/Verification/QuestionnaireVerification_" + sNo + ".ascx");
            PlaceHolder1.Controls.Add(uc);
        }
        catch (Exception ex)
        {
            throw new Exception("Could not load Questionnaire. Error: " + ex.Message.ToString());
        }
    }
}
