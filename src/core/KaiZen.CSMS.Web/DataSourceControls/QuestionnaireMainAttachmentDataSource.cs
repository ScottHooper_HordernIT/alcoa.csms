﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.QuestionnaireMainAttachmentProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(QuestionnaireMainAttachmentDataSourceDesigner))]
	public class QuestionnaireMainAttachmentDataSource : ProviderDataSource<QuestionnaireMainAttachment, QuestionnaireMainAttachmentKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainAttachmentDataSource class.
		/// </summary>
		public QuestionnaireMainAttachmentDataSource() : base(new QuestionnaireMainAttachmentService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the QuestionnaireMainAttachmentDataSourceView used by the QuestionnaireMainAttachmentDataSource.
		/// </summary>
		protected QuestionnaireMainAttachmentDataSourceView QuestionnaireMainAttachmentView
		{
			get { return ( View as QuestionnaireMainAttachmentDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the QuestionnaireMainAttachmentDataSource control invokes to retrieve data.
		/// </summary>
		public QuestionnaireMainAttachmentSelectMethod SelectMethod
		{
			get
			{
				QuestionnaireMainAttachmentSelectMethod selectMethod = QuestionnaireMainAttachmentSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (QuestionnaireMainAttachmentSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the QuestionnaireMainAttachmentDataSourceView class that is to be
		/// used by the QuestionnaireMainAttachmentDataSource.
		/// </summary>
		/// <returns>An instance of the QuestionnaireMainAttachmentDataSourceView class.</returns>
		protected override BaseDataSourceView<QuestionnaireMainAttachment, QuestionnaireMainAttachmentKey> GetNewDataSourceView()
		{
			return new QuestionnaireMainAttachmentDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the QuestionnaireMainAttachmentDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class QuestionnaireMainAttachmentDataSourceView : ProviderDataSourceView<QuestionnaireMainAttachment, QuestionnaireMainAttachmentKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainAttachmentDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the QuestionnaireMainAttachmentDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public QuestionnaireMainAttachmentDataSourceView(QuestionnaireMainAttachmentDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal QuestionnaireMainAttachmentDataSource QuestionnaireMainAttachmentOwner
		{
			get { return Owner as QuestionnaireMainAttachmentDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal QuestionnaireMainAttachmentSelectMethod SelectMethod
		{
			get { return QuestionnaireMainAttachmentOwner.SelectMethod; }
			set { QuestionnaireMainAttachmentOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal QuestionnaireMainAttachmentService QuestionnaireMainAttachmentProvider
		{
			get { return Provider as QuestionnaireMainAttachmentService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<QuestionnaireMainAttachment> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<QuestionnaireMainAttachment> results = null;
			QuestionnaireMainAttachment item;
			count = 0;
			
			System.Int32 _attachmentId;
			System.Int32 _questionnaireId;
			System.Int32 _answerId;
			System.Int32 _modifiedByUserId;

			switch ( SelectMethod )
			{
				case QuestionnaireMainAttachmentSelectMethod.Get:
					QuestionnaireMainAttachmentKey entityKey  = new QuestionnaireMainAttachmentKey();
					entityKey.Load(values);
					item = QuestionnaireMainAttachmentProvider.Get(entityKey);
					results = new TList<QuestionnaireMainAttachment>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case QuestionnaireMainAttachmentSelectMethod.GetAll:
                    results = QuestionnaireMainAttachmentProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case QuestionnaireMainAttachmentSelectMethod.GetPaged:
					results = QuestionnaireMainAttachmentProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case QuestionnaireMainAttachmentSelectMethod.Find:
					if ( FilterParameters != null )
						results = QuestionnaireMainAttachmentProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = QuestionnaireMainAttachmentProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case QuestionnaireMainAttachmentSelectMethod.GetByAttachmentId:
					_attachmentId = ( values["AttachmentId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["AttachmentId"], typeof(System.Int32)) : (int)0;
					item = QuestionnaireMainAttachmentProvider.GetByAttachmentId(_attachmentId);
					results = new TList<QuestionnaireMainAttachment>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				case QuestionnaireMainAttachmentSelectMethod.GetByQuestionnaireIdAnswerId:
					_questionnaireId = ( values["QuestionnaireId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["QuestionnaireId"], typeof(System.Int32)) : (int)0;
					_answerId = ( values["AnswerId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["AnswerId"], typeof(System.Int32)) : (int)0;
					item = QuestionnaireMainAttachmentProvider.GetByQuestionnaireIdAnswerId(_questionnaireId, _answerId);
					results = new TList<QuestionnaireMainAttachment>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case QuestionnaireMainAttachmentSelectMethod.GetByQuestionnaireId:
					_questionnaireId = ( values["QuestionnaireId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["QuestionnaireId"], typeof(System.Int32)) : (int)0;
					results = QuestionnaireMainAttachmentProvider.GetByQuestionnaireId(_questionnaireId, this.StartIndex, this.PageSize, out count);
					break;
				// FK
				case QuestionnaireMainAttachmentSelectMethod.GetByModifiedByUserId:
					_modifiedByUserId = ( values["ModifiedByUserId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["ModifiedByUserId"], typeof(System.Int32)) : (int)0;
					results = QuestionnaireMainAttachmentProvider.GetByModifiedByUserId(_modifiedByUserId, this.StartIndex, this.PageSize, out count);
					break;
				case QuestionnaireMainAttachmentSelectMethod.GetByAnswerId:
					_answerId = ( values["AnswerId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["AnswerId"], typeof(System.Int32)) : (int)0;
					results = QuestionnaireMainAttachmentProvider.GetByAnswerId(_answerId, this.StartIndex, this.PageSize, out count);
					break;
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == QuestionnaireMainAttachmentSelectMethod.Get || SelectMethod == QuestionnaireMainAttachmentSelectMethod.GetByAttachmentId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				QuestionnaireMainAttachment entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					QuestionnaireMainAttachmentProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<QuestionnaireMainAttachment> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			QuestionnaireMainAttachmentProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region QuestionnaireMainAttachmentDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the QuestionnaireMainAttachmentDataSource class.
	/// </summary>
	public class QuestionnaireMainAttachmentDataSourceDesigner : ProviderDataSourceDesigner<QuestionnaireMainAttachment, QuestionnaireMainAttachmentKey>
	{
		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainAttachmentDataSourceDesigner class.
		/// </summary>
		public QuestionnaireMainAttachmentDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public QuestionnaireMainAttachmentSelectMethod SelectMethod
		{
			get { return ((QuestionnaireMainAttachmentDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new QuestionnaireMainAttachmentDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region QuestionnaireMainAttachmentDataSourceActionList

	/// <summary>
	/// Supports the QuestionnaireMainAttachmentDataSourceDesigner class.
	/// </summary>
	internal class QuestionnaireMainAttachmentDataSourceActionList : DesignerActionList
	{
		private QuestionnaireMainAttachmentDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainAttachmentDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public QuestionnaireMainAttachmentDataSourceActionList(QuestionnaireMainAttachmentDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public QuestionnaireMainAttachmentSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion QuestionnaireMainAttachmentDataSourceActionList
	
	#endregion QuestionnaireMainAttachmentDataSourceDesigner
	
	#region QuestionnaireMainAttachmentSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the QuestionnaireMainAttachmentDataSource.SelectMethod property.
	/// </summary>
	public enum QuestionnaireMainAttachmentSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByAttachmentId method.
		/// </summary>
		GetByAttachmentId,
		/// <summary>
		/// Represents the GetByQuestionnaireIdAnswerId method.
		/// </summary>
		GetByQuestionnaireIdAnswerId,
		/// <summary>
		/// Represents the GetByQuestionnaireId method.
		/// </summary>
		GetByQuestionnaireId,
		/// <summary>
		/// Represents the GetByModifiedByUserId method.
		/// </summary>
		GetByModifiedByUserId,
		/// <summary>
		/// Represents the GetByAnswerId method.
		/// </summary>
		GetByAnswerId
	}
	
	#endregion QuestionnaireMainAttachmentSelectMethod

	#region QuestionnaireMainAttachmentFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireMainAttachment"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireMainAttachmentFilter : SqlFilter<QuestionnaireMainAttachmentColumn>
	{
	}
	
	#endregion QuestionnaireMainAttachmentFilter

	#region QuestionnaireMainAttachmentExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireMainAttachment"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireMainAttachmentExpressionBuilder : SqlExpressionBuilder<QuestionnaireMainAttachmentColumn>
	{
	}
	
	#endregion QuestionnaireMainAttachmentExpressionBuilder	

	#region QuestionnaireMainAttachmentProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;QuestionnaireMainAttachmentChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireMainAttachment"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireMainAttachmentProperty : ChildEntityProperty<QuestionnaireMainAttachmentChildEntityTypes>
	{
	}
	
	#endregion QuestionnaireMainAttachmentProperty
}

