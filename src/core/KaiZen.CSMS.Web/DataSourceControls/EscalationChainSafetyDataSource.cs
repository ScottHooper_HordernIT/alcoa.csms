﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.EscalationChainSafetyProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(EscalationChainSafetyDataSourceDesigner))]
	public class EscalationChainSafetyDataSource : ProviderDataSource<EscalationChainSafety, EscalationChainSafetyKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EscalationChainSafetyDataSource class.
		/// </summary>
		public EscalationChainSafetyDataSource() : base(new EscalationChainSafetyService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the EscalationChainSafetyDataSourceView used by the EscalationChainSafetyDataSource.
		/// </summary>
		protected EscalationChainSafetyDataSourceView EscalationChainSafetyView
		{
			get { return ( View as EscalationChainSafetyDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the EscalationChainSafetyDataSource control invokes to retrieve data.
		/// </summary>
		public EscalationChainSafetySelectMethod SelectMethod
		{
			get
			{
				EscalationChainSafetySelectMethod selectMethod = EscalationChainSafetySelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (EscalationChainSafetySelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the EscalationChainSafetyDataSourceView class that is to be
		/// used by the EscalationChainSafetyDataSource.
		/// </summary>
		/// <returns>An instance of the EscalationChainSafetyDataSourceView class.</returns>
		protected override BaseDataSourceView<EscalationChainSafety, EscalationChainSafetyKey> GetNewDataSourceView()
		{
			return new EscalationChainSafetyDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the EscalationChainSafetyDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class EscalationChainSafetyDataSourceView : ProviderDataSourceView<EscalationChainSafety, EscalationChainSafetyKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EscalationChainSafetyDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the EscalationChainSafetyDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public EscalationChainSafetyDataSourceView(EscalationChainSafetyDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal EscalationChainSafetyDataSource EscalationChainSafetyOwner
		{
			get { return Owner as EscalationChainSafetyDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal EscalationChainSafetySelectMethod SelectMethod
		{
			get { return EscalationChainSafetyOwner.SelectMethod; }
			set { EscalationChainSafetyOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal EscalationChainSafetyService EscalationChainSafetyProvider
		{
			get { return Provider as EscalationChainSafetyService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<EscalationChainSafety> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<EscalationChainSafety> results = null;
			EscalationChainSafety item;
			count = 0;
			
			System.Int32 _escalationChainSafetyId;
			System.Int32 _siteId;
			System.Int32 _level;
			System.Int32 _userId;

			switch ( SelectMethod )
			{
				case EscalationChainSafetySelectMethod.Get:
					EscalationChainSafetyKey entityKey  = new EscalationChainSafetyKey();
					entityKey.Load(values);
					item = EscalationChainSafetyProvider.Get(entityKey);
					results = new TList<EscalationChainSafety>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case EscalationChainSafetySelectMethod.GetAll:
                    results = EscalationChainSafetyProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case EscalationChainSafetySelectMethod.GetPaged:
					results = EscalationChainSafetyProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case EscalationChainSafetySelectMethod.Find:
					if ( FilterParameters != null )
						results = EscalationChainSafetyProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = EscalationChainSafetyProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case EscalationChainSafetySelectMethod.GetByEscalationChainSafetyId:
					_escalationChainSafetyId = ( values["EscalationChainSafetyId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["EscalationChainSafetyId"], typeof(System.Int32)) : (int)0;
					item = EscalationChainSafetyProvider.GetByEscalationChainSafetyId(_escalationChainSafetyId);
					results = new TList<EscalationChainSafety>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				case EscalationChainSafetySelectMethod.GetBySiteId:
					_siteId = ( values["SiteId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["SiteId"], typeof(System.Int32)) : (int)0;
					results = EscalationChainSafetyProvider.GetBySiteId(_siteId, this.StartIndex, this.PageSize, out count);
					break;
				case EscalationChainSafetySelectMethod.GetBySiteIdLevel:
					_siteId = ( values["SiteId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["SiteId"], typeof(System.Int32)) : (int)0;
					_level = ( values["Level"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["Level"], typeof(System.Int32)) : (int)0;
					item = EscalationChainSafetyProvider.GetBySiteIdLevel(_siteId, _level);
					results = new TList<EscalationChainSafety>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// FK
				case EscalationChainSafetySelectMethod.GetByLevel:
					_level = ( values["Level"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["Level"], typeof(System.Int32)) : (int)0;
					results = EscalationChainSafetyProvider.GetByLevel(_level, this.StartIndex, this.PageSize, out count);
					break;
				case EscalationChainSafetySelectMethod.GetByUserId:
					_userId = ( values["UserId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["UserId"], typeof(System.Int32)) : (int)0;
					results = EscalationChainSafetyProvider.GetByUserId(_userId, this.StartIndex, this.PageSize, out count);
					break;
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == EscalationChainSafetySelectMethod.Get || SelectMethod == EscalationChainSafetySelectMethod.GetByEscalationChainSafetyId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				EscalationChainSafety entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					EscalationChainSafetyProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<EscalationChainSafety> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			EscalationChainSafetyProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region EscalationChainSafetyDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the EscalationChainSafetyDataSource class.
	/// </summary>
	public class EscalationChainSafetyDataSourceDesigner : ProviderDataSourceDesigner<EscalationChainSafety, EscalationChainSafetyKey>
	{
		/// <summary>
		/// Initializes a new instance of the EscalationChainSafetyDataSourceDesigner class.
		/// </summary>
		public EscalationChainSafetyDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public EscalationChainSafetySelectMethod SelectMethod
		{
			get { return ((EscalationChainSafetyDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new EscalationChainSafetyDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region EscalationChainSafetyDataSourceActionList

	/// <summary>
	/// Supports the EscalationChainSafetyDataSourceDesigner class.
	/// </summary>
	internal class EscalationChainSafetyDataSourceActionList : DesignerActionList
	{
		private EscalationChainSafetyDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the EscalationChainSafetyDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public EscalationChainSafetyDataSourceActionList(EscalationChainSafetyDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public EscalationChainSafetySelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion EscalationChainSafetyDataSourceActionList
	
	#endregion EscalationChainSafetyDataSourceDesigner
	
	#region EscalationChainSafetySelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the EscalationChainSafetyDataSource.SelectMethod property.
	/// </summary>
	public enum EscalationChainSafetySelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByEscalationChainSafetyId method.
		/// </summary>
		GetByEscalationChainSafetyId,
		/// <summary>
		/// Represents the GetBySiteId method.
		/// </summary>
		GetBySiteId,
		/// <summary>
		/// Represents the GetBySiteIdLevel method.
		/// </summary>
		GetBySiteIdLevel,
		/// <summary>
		/// Represents the GetByLevel method.
		/// </summary>
		GetByLevel,
		/// <summary>
		/// Represents the GetByUserId method.
		/// </summary>
		GetByUserId
	}
	
	#endregion EscalationChainSafetySelectMethod

	#region EscalationChainSafetyFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EscalationChainSafety"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EscalationChainSafetyFilter : SqlFilter<EscalationChainSafetyColumn>
	{
	}
	
	#endregion EscalationChainSafetyFilter

	#region EscalationChainSafetyExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EscalationChainSafety"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EscalationChainSafetyExpressionBuilder : SqlExpressionBuilder<EscalationChainSafetyColumn>
	{
	}
	
	#endregion EscalationChainSafetyExpressionBuilder	

	#region EscalationChainSafetyProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;EscalationChainSafetyChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="EscalationChainSafety"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EscalationChainSafetyProperty : ChildEntityProperty<EscalationChainSafetyChildEntityTypes>
	{
	}
	
	#endregion EscalationChainSafetyProperty
}

