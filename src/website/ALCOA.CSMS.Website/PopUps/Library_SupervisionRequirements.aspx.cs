﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using KaiZen.CSMS.Data;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Services;

using System.Text;

public partial class PopUps_SupervisionRequirements : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        ConfigText2Service ct2Service = new ConfigText2Service();
        TList<ConfigText2> ct2 = ct2Service.GetByConfigTextTypeId(4); //harcoded for now...
        byte[] s = ct2[0].ConfigText;

        string stringS = "";
        if (s != null) stringS = Encoding.ASCII.GetString(s);
        PlaceHolder1.Controls.Add(new LiteralControl(stringS));
    }
}
