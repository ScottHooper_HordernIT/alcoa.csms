﻿#region Using directives

using System;

#endregion

namespace KaiZen.CSMS.Entities
{	
	///<summary>
	/// An object representation of the 'UsersPrivileged2' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class UsersPrivileged2 : UsersPrivileged2Base
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="UsersPrivileged2"/> instance.
		///</summary>
		public UsersPrivileged2():base(){}	
		
		#endregion
	}
}
