﻿
using System;

namespace KaiZen.CSMS.Entities
{
	/// <summary>
	/// An enum representation of the 'AdminTaskSource' table. [No description found in the database]
	/// </summary>
	/// <remark>this enumeration contains the items contained in the table AdminTaskSource</remark>
	[Serializable, Flags]
	public enum AdminTaskSourceList
	{
		/// <summary> 
		/// System
		/// </summary>
		[EnumTextValue(@"System")]
		System = 1, 

		/// <summary> 
		/// Help/Access Request
		/// </summary>
		[EnumTextValue(@"Help/Access Request")]
		Manual = 2

	}
}
