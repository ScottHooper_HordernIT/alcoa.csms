﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="UserControls_Other_userLine" Codebehind="userLine.ascx.cs" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<span style="color: silver"><em>Logged In As:</em> </span>
<dx:ASPxHyperLink ID="hlUser" runat="server" Text="[hlUser]" CssClass="whitefooter" CssPostfix="whitefooter" ForeColor="Silver"></dx:ASPxHyperLink>
