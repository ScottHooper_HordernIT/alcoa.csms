﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="UserControls_Other_news" Codebehind="news.ascx.cs" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxNewsControl" TagPrefix="dx" %>

<dx:ASPxNewsControl ID="ASPxNewsControl1" runat="server" 
    HeaderTextField="Header" DataSourceID="NewsDataSource" DateField="PostedOn" 
    NameField="NewsId" TextField="Contents" EmptyDataText=" " Width="100%" 
    CssFilePath="~/App_Themes/Blue/{0}/styles.css" 
    CssPostfix="Blue" ItemSpacing="2px" 
    SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
    <ItemDateStyle Spacing="0px">
    </ItemDateStyle>
    <ItemSettings>
        <TailImage>
            <SpriteProperties CssClass="dxWeb_hlTail_Office2003Blue" />
        </TailImage>
    </ItemSettings>
    <ItemLeftPanelStyle Spacing="21px">
        <Paddings PaddingTop="2px" />
    </ItemLeftPanelStyle>
    <PagerSettings>
        <AllButton Text="">
        </AllButton>
        <FirstPageButton Text="">
        </FirstPageButton>
        <LastPageButton Text="">
        </LastPageButton>
        <NextPageButton Text="">
        </NextPageButton>
        <PrevPageButton Text="">
        </PrevPageButton>
    </PagerSettings>
    <ItemContentStyle LineHeight="18px">
    </ItemContentStyle>
</dx:ASPxNewsControl>
        
    <data:EntityDataSource ID="NewsDataSource" runat="server"
    ProviderName="NewsProvider"
    EntityTypeName="KaiZen.CSMS.News, KaiZen.CSMS"
    SelectMethod="GetAll"
    Sort="PostedOn DESC"
    />  
