﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface IYearlyTargetService
    {
        YearlyTarget Get(Expression<Func<YearlyTarget, bool>> where, List<Expression<Func<YearlyTarget, object>>> includes);
        List<YearlyTarget> GetMany(int? top, Expression<Func<YearlyTarget, bool>> where, Func<IQueryable<YearlyTarget>, IOrderedQueryable<YearlyTarget>> orderBy, List<Expression<Func<YearlyTarget, object>>> includes);
        List<YearlyTarget> GetManyPaged(int pageNumber, int pageSize, Expression<Func<YearlyTarget, bool>> where, Func<IQueryable<YearlyTarget>, IOrderedQueryable<YearlyTarget>> orderBy, List<Expression<Func<YearlyTarget, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<YearlyTarget, bool>> where, List<Expression<Func<YearlyTarget, object>>> includes);
        void Insert(YearlyTarget item, bool saveChanges = true);
        void Update(YearlyTarget item, bool saveChanges = true);
        void Delete(YearlyTarget item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class YearlyTargetService : IYearlyTargetService
    {
        private readonly IRepository<YearlyTarget> _repository;

        public YearlyTargetService(IRepository<YearlyTarget> repository)
        {
            this._repository = repository;
        }

        public YearlyTarget Get(Expression<Func<YearlyTarget, bool>> where, List<Expression<Func<YearlyTarget, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<YearlyTarget> GetMany(int? top, Expression<Func<YearlyTarget, bool>> where, Func<IQueryable<YearlyTarget>, IOrderedQueryable<YearlyTarget>> orderBy, List<Expression<Func<YearlyTarget, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<YearlyTarget> GetManyPaged(int pageNumber, int pageSize, Expression<Func<YearlyTarget, bool>> where, Func<IQueryable<YearlyTarget>, IOrderedQueryable<YearlyTarget>> orderBy, List<Expression<Func<YearlyTarget, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<YearlyTarget, bool>> where, List<Expression<Func<YearlyTarget, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(YearlyTarget item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("YearlyTarget");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(YearlyTarget item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("YearlyTarget");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(YearlyTarget item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("YearlyTarget");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
