﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Data;
using KaiZen.CSMS.Services;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

using DevExpress.Data;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.Data;
using DevExpress.Web.ASPxPopupControl;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxCallbackPanel;
using DevExpress.Web.ASPxClasses;
using DevExpress.XtraCharts;

using System.ComponentModel;
using System.Reflection;
//Add By Bishwajit for Item#31
using DevExpress.XtraCharts.Web;
using System.Drawing.Imaging;
using System.IO;
using DevExpress.XtraCharts.Native;
using System.Text;
using System.Web.Configuration;
using iTextSharp.text;
using iTextSharp.text.pdf;
//Add By Bishwajit for Item#31

public partial class UserControls_Main_ComplianceCharts2_ComplianceChart2_S_S : System.Web.UI.UserControl
{
    //User Customisable Colors
    const string CellColourBad = "Red"; //"#ffb8b8";
    const string CellColourGood = "Lime"; //"#def3ca";
    const string CellColourUnsure = "";

    int CompanyId, SiteId, Year, MonthId, CategoryId;
    int NoKpiGreen = 0;
    int NoKpi = 0;

    bool uptodate = false;
    string[] rawScoresArray = new string[15];
    private enum TrafficLightColor2
    {
        [Description("0")]
        Grey = 0,
        [Description("1")]
        Red = 1,
        [Description("2")]
        Yellow = 2,
        [Description("3")]
        Green = 3,
    }


    Auth auth = new Auth();
    protected void Page_Load(object sender, EventArgs e) { Helper._Auth.PageLoad(auth); if (!Page.IsPostBack) { moduleInit(Page.IsPostBack); } }
    private void moduleInit(bool postBack)
    {
        //Add By Bishwajit for Item#31
        Session["IsReturnedFromSS"] = true;
        Session["IsValueTrue"] = true;
        //End Add By Bishwajit for Item#31
        //Commented By Bishwajit for Item#31
        //if (!postBack)
        //{
        //End Commented By Bishwajit for Item#31   
        try
        {
            //0. Reset
            ResetData();

            //0. Set Summary Calculation (Grid YTD Averages)
            this.grid2.CustomSummaryCalculate += new DevExpress.Data.CustomSummaryEventHandler(grid2_CustomSummaryCalculate);

            //1. Check Session Variables
            Helper.Radar.CheckInputVariables(false);

            //2. Assign Session Variables
            CompanyId = Convert.ToInt32(SessionHandler.spVar_CompanyId);
            SiteId = Convert.ToInt32(SessionHandler.spVar_SiteId);
            Year = Convert.ToInt32(SessionHandler.spVar_Year);
            //Add By Bishwajit for Item#31
            if (SessionHandler.spVar_MonthId == "")
            {
                SessionHandler.spVar_MonthId = ddlMonth.Value.ToString();
            }
            //End Add By Bishwajit for Item#31
            MonthId = Convert.ToInt32(SessionHandler.spVar_MonthId);

            //3. Validate Company Setup Correctly & Set CategoryId
            Helper.Radar.ValidateCompanyCategorySetup(CompanyId, SiteId);
            //Add By Bishwajit for Item#31
            if (SiteId > 0)
            {
                //End Add By Bishwajit for Item#31
                CategoryId = Helper.Radar.GetCompanyCategoryId_ByCompanyIdSiteId(CompanyId, SiteId);
                // }

                //4. Generate Table Data
                GenerateData();

                //5. Bind Table Data
                BindData();

                //6. Assign ddlMonth
                ddlMonth.Items.Clear();
                ddlMonth.Items.Add("[YTD]", 0);
                MonthsService mService = new MonthsService();
                TList<Months> mTlist = mService.GetAll();
                int upto = 12;
                if (Year == DateTime.Now.Year) upto = DateTime.Now.Month - 1;
                foreach (Months m in mTlist)
                {
                    if (m.MonthId <= upto)
                    {
                        ddlMonth.Items.Add(m.MonthName, m.MonthId);
                    }
                }

                ddlMonth.Value = MonthId;
                if (MonthId == 0)
                {
                    lblGrid2.Text = "YTD Averages:";
                }
                else
                {
                    Months m = mService.GetByMonthId(MonthId);
                    lblGrid2.Text = m.MonthName;
                }

                //Session Value added By Bishwajit For Item#31
                Session["DropDownVal"] = ddlMonth.Text;
                if (divRawScores.Visible)
                {
                    Session["RawVisible"] = true;
                }
                else
                {
                    Session["RawVisible"] = false;
                }
                if (wcSpider.Visible)
                {
                    Session["SpiderIsVisible"] = true;
                }
                else
                {
                    Session["SpiderIsVisible"] = false;
                }
                Session["IsTableVisible"] = true;
                Session["IsReturnedFromSS"] = true;
                //End Add By Bishwajit for Item#31
                //7. Show Contacts Check
                //CheckContractorContactsUpToDate();
                ResetTrafficLights();
                TrafficLights(CompanyId, SiteId, Year);
                //Add By Bishwajit for Item#31
            }
            //End Add By Bishwajit for Item#31
        }

        catch (Exception ex)
        {
            //Add By Bishwajit for Item#31
            if (SiteId > 0)
            {
                throw new Exception(ex.Message);
            }
            //End Add By Bishwajit for Item#31
        }
        //throw new Exception(ex.Message);

    }
    protected void TrafficLights(int CompanyId, int SiteId, int Year)
    {
        int count = 0;
        int fCount = 0;
        int iCount = 0;

        CompaniesService _cService = new CompaniesService();
        Companies c = _cService.GetByCompanyId(CompanyId);

        #region Safety Qualification & Subcontractors
        QuestionnaireWithLocationApprovalViewService qService = new QuestionnaireWithLocationApprovalViewService();
        QuestionnaireWithLocationApprovalViewFilters qFilters = new QuestionnaireWithLocationApprovalViewFilters();
        qFilters.Append(QuestionnaireWithLocationApprovalViewColumn.CompanyId, CompanyId.ToString());
        VList<QuestionnaireWithLocationApprovalView> qVlist = qService.GetPaged(qFilters.ToString(), "QuestionnaireId DESC", 0, 100, out count);

        if (count > 0)
        {

            bool requaling = false;
            if (qVlist[0].Status == (int)QuestionnaireStatusList.Incomplete)
            {
                lblTL_SQ.Text = "Safety Qualification - Incomplete";
                if (count > 1) requaling = true;
            }
            if (qVlist[0].Status == (int)QuestionnaireStatusList.BeingAssessed)
            {
                lblTL_SQ.Text = "Safety Qualification - Being Assessed";
                if (count > 1) requaling = true;
            }

            bool done = false;
            int no = 0;
            int i = 0;
            while (i <= qVlist.Count - 1)
            {
                if (!done)
                {
                    if (qVlist[i] != null)
                    {
                        if (qVlist[i].Status == (int)QuestionnaireStatusList.AssessmentComplete)
                        {
                            lblTL_SQ.Text = "Safety Qualification - Assessment Complete";
                            if (qVlist[i].IsVerificationRequired == true)
                            {
                                if (qVlist[i].LevelOfSupervision != "Direct" && qVlist[i].MainAssessmentValidTo >= DateTime.Now) // this line added by ashley Goldstraw DT504 18/5/2016
                                { gcTL_SC.Value = GetImageURL(TrafficLightColor2.Green); }
                            }

                            if (qVlist[i].MainAssessmentValidTo != null)
                            {
                                if (qVlist[i].MainAssessmentValidTo > DateTime.Now)
                                {
                                    DateTime dt = (DateTime)qVlist[i].MainAssessmentValidTo;
                                    if (qVlist[i].Status == (int)QuestionnaireStatusList.AssessmentComplete)
                                    {
                                        lblTL_SQ.Text = "Safety Qualification valid until " + dt.ToString("dd/MM/yyyy");
                                        if (requaling) lblTL_SQ.Text += " (Currently Re-Qualifying)";
                                        gcTL_SQ.Value = GetImageURL(TrafficLightColor2.Green);


                                        DateTime dtExpiry = Convert.ToDateTime(qVlist[i].MainAssessmentValidTo);
                                        TimeSpan ts = Convert.ToDateTime(dtExpiry) - DateTime.Now;
                                        if (ts.Days <= 60)
                                        {
                                            gcTL_SQ.Value = GetImageURL(TrafficLightColor2.Yellow);
                                            if (ts.Days == 1)
                                            {
                                                lblTL_SQ.Text = "Safety Qualification valid until " + dt.ToString("dd/MM/yyyy") + " (Expires in " + ts.Days + " Day)";
                                            }
                                            else
                                            {
                                                lblTL_SQ.Text = "Safety Qualification valid until " + dt.ToString("dd/MM/yyyy") + " (Expires in " + ts.Days + " Days)";
                                            }
                                            no = i;
                                        }
                                        done = true;
                                    }
                                    else
                                    {
                                        gcTL_SQ.Value = GetImageURL(TrafficLightColor2.Yellow);

                                        if (qVlist[i + 1] != null)
                                        {
                                            if (qVlist[i + 1].Status == (int)QuestionnaireStatusList.AssessmentComplete &&
                                                c.CompanyStatusId == (int)CompanyStatusList.Requal_Incomplete)
                                            {
                                                DateTime dtExpiry = Convert.ToDateTime(qVlist[i].MainAssessmentValidTo);
                                                TimeSpan ts = Convert.ToDateTime(dtExpiry) - DateTime.Now;
                                                if (ts.Days <= 60)
                                                {
                                                    lblTL_SQ.Text = "Safety Qualification Valid until " + dt.ToString("dd/MM/yyyy") + " - Expires in " + ts.Days + " Days.";
                                                    no = i;
                                                    done = true;
                                                }
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    DateTime dt = (DateTime)qVlist[i].MainAssessmentValidTo;
                                    lblTL_SQ.Text = "Safety Qualification Expired at " + dt.ToString("dd/MM/yyyy");
                                    gcTL_SQ.Value = GetImageURL(TrafficLightColor2.Red);
                                    no = i;
                                    done = true;
                                }
                            }
                        }
                    }
                }
                i++;
            }
        }
        else
        {
            gcTL_SQ.Value = GetImageURL(TrafficLightColor2.Red);
            gcTL_SQ.Visible = true;
            gcTL_SQ.ToolTip = "No Safety Qualification exists for your company. Please Contact your procurement contact.";
        }

        #endregion

        #region Contractor Contacts
        ContactsContractorsFilters querycContacts = new ContactsContractorsFilters();
        querycContacts.Append(ContactsContractorsColumn.CompanyId, CompanyId.ToString());
        querycContacts.Append(ContactsContractorsColumn.ContactRole, "Principal");
        TList<ContactsContractors> cContactsList = DataRepository.ContactsContractorsProvider.GetPaged(querycContacts.ToString(), null, 0, 100, out count);
        if (count > 0) { } else { iCount += 1; };
        count = 0;

        querycContacts.Clear();
        querycContacts.Append(ContactsContractorsColumn.CompanyId, CompanyId.ToString());
        querycContacts.Append(ContactsContractorsColumn.ContactRole, "KPI%Administration%Contact");
        cContactsList = DataRepository.ContactsContractorsProvider.GetPaged(querycContacts.ToString(), null, 0, 100, out count);
        if (count > 0) { } else { iCount += 1; };
        count = 0;

        querycContacts.Clear();
        querycContacts.Append(ContactsContractorsColumn.CompanyId, CompanyId.ToString());
        querycContacts.Append(ContactsContractorsColumn.ContactRole, "Contract%Manager");
        cContactsList = DataRepository.ContactsContractorsProvider.GetPaged(querycContacts.ToString(), null, 0, 100, out count);
        if (count > 0) { } else { iCount += 1; };
        count = 0;

        querycContacts.Clear();
        querycContacts.Append(ContactsContractorsColumn.CompanyId, CompanyId.ToString());
        querycContacts.Append(ContactsContractorsColumn.ContactRole, "Operation%Manager");
        cContactsList = DataRepository.ContactsContractorsProvider.GetPaged(querycContacts.ToString(), null, 0, 100, out count);
        if (count > 0) { } else { iCount += 1; };
        count = 0;

        bool contactsOutofDate = false;
        DateTime nowMinus3 = DateTime.Now.AddMonths(-3);


        if (c.ContactsLastUpdated != null)
        {
            if (c.ContactsLastUpdated <= nowMinus3)
            {
                contactsOutofDate = true;
            }
        }
        else
        {
            contactsOutofDate = true;
        }

        if (c.ContactsLastUpdated != null)
        {
            DateTime dt = (DateTime)c.ContactsLastUpdated;
            lblTL_CC.Text += " last updated on " + dt.ToShortDateString();
        }

        if (iCount != 0)
        {
            gcTL_CC.Value = GetImageURL(TrafficLightColor2.Red);
            if (contactsOutofDate)
            {
                gcTL_CC.ToolTip = "Your company's contact list has not been verified as up-to-date within the last 3 months and also does not include the minimum entries of: Principal, KPI Administrator, Contract Manager and Operation Manager.";
            }
            else
            {
                gcTL_CC.ToolTip = "Your company's contact list does not include the minimum entries of: Principal, KPI Administrator, Contract Manager and Operation Manager.";
            }
            fCount += 1;
        }
        else
        {
            if (contactsOutofDate)
            {
                gcTL_CC.Value = GetImageURL(TrafficLightColor2.Red);
                gcTL_CC.ToolTip = "Your company's contact list has not been verified as up-to-date within the last 3 months.";
            }
            else
            {
                gcTL_CC.Value = GetImageURL(TrafficLightColor2.Green);
                gcTL_CC.ToolTip = "Good Work. Your company's contact list has the minimum entries of: Principal, KPI Administrator, Contractor Manager and Operation Manager.";
            }
        }

        #endregion

        //Add By Bishwajit for Item#31
        Session["SQText"] = lblTL_SQ.Text;
        Session["SQColor"] = gcTL_SQ.Value;

        Session["SCText"] = lblTL_SC.Text;
        Session["SCColor"] = gcTL_SC.Value;

        Session["CCText"] = lblTL_CC.Text;
        Session["CCColor"] = gcTL_CC.Value;
        //End Add By Bishwajit for Item#31

    }
    protected void ResetTrafficLights()
    {
        gcTL_SQ.Value = GetImageURL(TrafficLightColor2.Red);
        lblTL_SQ.Text = "Safety Qualification";
        gcTL_SC.Value = GetImageURL(TrafficLightColor2.Red);
        lblTL_SC.Text = "Approval to engage Sub Contractors";

        gcTL_CC.Value = GetImageURL(TrafficLightColor2.Red);
        lblTL_CC.Text = "Contractor Contacts";
    }

    protected void ResetData()
    {
        NoKpi = 0;
        NoKpiGreen = 0;

        Session["Radar_Table1-Summary"] = null;
        Session["Radar_Table2-Ytd"] = null;
        Session["Radar_Table3-Spider"] = null;

        grid.DataSource = null;
        grid.Columns.Clear();
        grid2.DataSource = null;
        grid2.Columns.Clear();
        gridMonthlyE.DataSource = null;
        gridMonthlyE.Columns.Clear();
        gridRawScores.DataSource = null;
        gridRawScores.Columns.Clear();
        gridQtrly.DataSource = null;
        gridQtrly.Columns.Clear();

        wcSpider.Series.Clear();
        wcSpider.DataSource = null;
        wcSpider.DataSourceID = String.Empty;
    }
    protected void GenerateData()
    {
        var siteService = ALCOA.CSMS.Website.Global.GetInstance<Repo.CSMS.Service.Database.ISiteService>();

        //Add By Bishwajit for Item#31
        Session["IsValueTrue"] = false;
        //End Add By Bishwajit for Item#31
        Helper.Radar.CreateGridColumns(SiteId, MonthId, grid, grid2, gridMonthlyE, gridRawScores, gridQtrly, wcSpider);
        DataSet DataSet1_Summary = new DataSet();
        DataSet DataSet2 = new DataSet();
        DataSet DataSet2_Ytd = new DataSet();
        DataSet DataSet2_Ytd_Month = new DataSet();
        DataSet DataSet2_Ytd_Qtr = new DataSet();

        DataSet1_Summary.Tables.Add(Helper.Radar.CreateDataTable(CompanyId, SiteId, 1, null));
        DataSet2.Tables.Add(Helper.Radar.CreateDataTable(CompanyId, SiteId, 2, null));
        DataSet2_Ytd.Tables.Add(Helper.Radar.CreateDataTable(CompanyId, SiteId, 2, null));
        DataSet2_Ytd_Month.Tables.Add(Helper.Radar.CreateDataTable(CompanyId, SiteId, 2, null));
        DataSet2_Ytd_Month.Tables.Add(Helper.Radar.CreateDataTable(CompanyId, SiteId, 2, null)); //rawscores
        DataSet2_Ytd_Qtr.Tables.Add(Helper.Radar.CreateDataTable(CompanyId, SiteId, 2, null));

        DataSet DataSet3_Spider = new DataSet();
        DataTable dtSpider = new DataTable();
        dtSpider = DataSet3_Spider.Tables.Add("Spider");
        dtSpider.Columns.Add("Key", typeof(string));
        dtSpider.Columns.Add("Value", typeof(decimal));

        var site = siteService.Get(e => e.SiteId == SiteId, null);

        bool isInternal = false;
        if (SessionHandler.spVar_RadarInternal == "1") isInternal = true;

        foreach (string KpiMeasure in Helper.Radar.GetKpiMeasurements(-1, isInternal, false)) //Summary
        {
            DataSet1_Summary.Tables[0].Rows.Add(Helper.Radar.CreateDataRow_DataSet1_Summary(DataSet1_Summary.Tables[0], KpiMeasure,
                                                                                    CompanyId, SiteId, MonthId, Year, CategoryId));
        }
        string[] KpiMeasures = Helper.Radar.GetKpiMeasurements(CategoryId, false, false);
        CompanySiteCategoryService cscService = new CompanySiteCategoryService();
        CompanySiteCategory csc = cscService.GetByCompanySiteCategoryId(CategoryId);
        foreach (string KpiMeasure in KpiMeasures) //Ytd
        {

            DataRow dr = Helper.Radar.CreateDataRow_DataSet2_Ytd(DataSet2.Tables[0], KpiMeasure, CompanyId, site, MonthId, Year, csc);
            DataSet2.Tables[0].Rows.Add(dr);
            if (KpiMeasure == "IFE : Injury Ratio")
            {
                if (MonthId == 0) lblIFEInjuryRatio.Text = dr["KpiScoreYtd"].ToString();
                else lblIFEInjuryRatio.Text = dr[String.Format("KpiScoreMonth{0}", MonthId)].ToString();
            }
        }
        bool foundCSA = false;
        int bestCSAScore = 0;
        int spiderCSAScore;
        foreach (DataRow dr in DataSet2.Tables[0].Rows)
        {
            bool _skip = false;
            if (dr["KpiMeasure"].ToString() == "Behavioural Observations - Plan") _skip = true;
            if (dr["KpiMeasure"].ToString() == "Behavioural Observations - Actual") _skip = true;
            if (!_skip)
            {

                if (dr["KpiMeasure"].ToString() != "On site")
                {
                    bool skip = false;

                    //if (Year != DateTime.Now.Year)  //ToDo change this back to DateTime.Now.Year  //AG Removed this as seemd to have a bug and report wrong data
                    if (Year == 20161111)
                    {
                        if (dr["KpiMeasure"].ToString() != "CSA - Self Audit (% Score) - Current Qtr")
                        {
                            if (dr["KpiMeasure"].ToString() == "CSA - Self Audit (% Score) - Previous Qtr")
                            {
                                if (MonthId == 0)
                                {
                                    dr["KpiMeasure"] = "CSA - Self Audit (% Score) - Qtr 4";
                                }
                                else
                                {
                                    dr["KpiMeasure"] = "CSA - Self Audit (% Score) - Qtr";
                                }
                            }

                            DataSet2_Ytd.Tables[0].ImportRow(dr);

                        }
                    }
                    else
                    {
                        DataSet2_Ytd.Tables[0].ImportRow(dr);

                    }


                    //Changes for DT#3112
                    try
                    {
                        if (CategoryId == 2 && DataSet2_Ytd.Tables[0].Rows.Count == 3)
                        {
                            DataSet2_Ytd.Tables[0].Rows[1]["KpiMeasure"] = "CSA - Self Audit (% Score) - Current Half"; 
                            DataSet2_Ytd.Tables[0].Rows[2]["KpiMeasure"] = "CSA - Self Audit (% Score) - Previous Half";
                            /*
                            if (MonthId >= 7 || (MonthId == 0 && DateTime.Now.Month >= 7)) //Half2
                            {
                                //Current Half
                                
                                if (DataSet2_Ytd.Tables[0].Rows[1]["KpiScoreMonth9"].ToString() != "-" && DataSet2_Ytd.Tables[0].Rows[1]["KpiScoreMonth12"].ToString() != "-")
                                {
                                    if (Convert.ToInt32(DataSet2_Ytd.Tables[0].Rows[1]["KpiScoreMonth9"]) >= Convert.ToInt32(DataSet2_Ytd.Tables[0].Rows[1]["KpiScoreMonth12"]))
                                    {
                                        DataSet2_Ytd.Tables[0].Rows[1]["KpiScoreYtd"] = DataSet2_Ytd.Tables[0].Rows[1]["KpiScoreMonth9"];
                                        DataSet2_Ytd.Tables[0].Rows[1]["KpiScoreYtdPercentage"] = DataSet2_Ytd.Tables[0].Rows[1]["KpiScoreMonth9Percentage"];
                                    }
                                    else
                                    {
                                        DataSet2_Ytd.Tables[0].Rows[1]["KpiScoreYtd"] = DataSet2_Ytd.Tables[0].Rows[1]["KpiScoreMonth12"];
                                        DataSet2_Ytd.Tables[0].Rows[1]["KpiScoreYtdPercentage"] = DataSet2_Ytd.Tables[0].Rows[1]["KpiScoreMonth12Percentage"];
                                    }
                                }
                                else if (DataSet2_Ytd.Tables[0].Rows[1]["KpiScoreMonth9"].ToString() != "-" && DataSet2_Ytd.Tables[0].Rows[1]["KpiScoreMonth12"].ToString() == "-")
                                {
                                    DataSet2_Ytd.Tables[0].Rows[1]["KpiScoreYtd"] = DataSet2_Ytd.Tables[0].Rows[1]["KpiScoreMonth9"];
                                    DataSet2_Ytd.Tables[0].Rows[1]["KpiScoreYtdPercentage"] = DataSet2_Ytd.Tables[0].Rows[1]["KpiScoreMonth9Percentage"];
                                }
                                else if (DataSet2_Ytd.Tables[0].Rows[1]["KpiScoreMonth9"].ToString() == "-" && DataSet2_Ytd.Tables[0].Rows[1]["KpiScoreMonth12"].ToString() != "-")
                                {
                                    DataSet2_Ytd.Tables[0].Rows[1]["KpiScoreYtd"] = DataSet2_Ytd.Tables[0].Rows[1]["KpiScoreMonth12"];
                                    DataSet2_Ytd.Tables[0].Rows[1]["KpiScoreYtdPercentage"] = DataSet2_Ytd.Tables[0].Rows[1]["KpiScoreMonth12Percentage"];
                                }
                                else
                                {
                                    DataSet2_Ytd.Tables[0].Rows[1]["KpiScoreYtd"] = "-";
                                    DataSet2_Ytd.Tables[0].Rows[1]["KpiScoreYtdPercentage"] = "-";
                                }

                                //Previous Half
                                if (DataSet2_Ytd.Tables[0].Rows[1]["KpiScoreMonth3"].ToString() != "-" && DataSet2_Ytd.Tables[0].Rows[1]["KpiScoreMonth6"].ToString() != "-")
                                {
                                    int rw3 = Convert.ToInt32(DataSet2_Ytd.Tables[0].Rows[1]["KpiScoreMonth3"]);
                                    int rw6 = Convert.ToInt32(DataSet2_Ytd.Tables[0].Rows[1]["KpiScoreMonth6"]);
                                    if (Convert.ToInt32(DataSet2_Ytd.Tables[0].Rows[1]["KpiScoreMonth3"]) >= Convert.ToInt32(DataSet2_Ytd.Tables[0].Rows[1]["KpiScoreMonth6"]))
                                    {
                                        DataSet2_Ytd.Tables[0].Rows[2]["KpiScoreYtd"] = DataSet2_Ytd.Tables[0].Rows[1]["KpiScoreMonth3"];
                                        DataSet2_Ytd.Tables[0].Rows[2]["KpiScoreYtdPercentage"] = DataSet2_Ytd.Tables[0].Rows[1]["KpiScoreMonth3Percentage"];
                                    }
                                    else
                                    {
                                        DataSet2_Ytd.Tables[0].Rows[2]["KpiScoreYtd"] = DataSet2_Ytd.Tables[0].Rows[1]["KpiScoreMonth6"];
                                        DataSet2_Ytd.Tables[0].Rows[2]["KpiScoreYtdPercentage"] = DataSet2_Ytd.Tables[0].Rows[1]["KpiScoreMonth6Percentage"];
                                    }
                                }
                                else if (DataSet2_Ytd.Tables[0].Rows[1]["KpiScoreMonth3"].ToString() != "-" && DataSet2_Ytd.Tables[0].Rows[1]["KpiScoreMonth6"].ToString() == "-")
                                {
                                    DataSet2_Ytd.Tables[0].Rows[2]["KpiScoreYtd"] = DataSet2_Ytd.Tables[0].Rows[1]["KpiScoreMonth3"];
                                    DataSet2_Ytd.Tables[0].Rows[2]["KpiScoreYtdPercentage"] = DataSet2_Ytd.Tables[0].Rows[1]["KpiScoreMonth3Percentage"];
                                }
                                else if (DataSet2_Ytd.Tables[0].Rows[1]["KpiScoreMonth3"].ToString() == "-" && DataSet2_Ytd.Tables[0].Rows[1]["KpiScoreMonth6"].ToString() != "-")
                                {
                                    DataSet2_Ytd.Tables[0].Rows[2]["KpiScoreYtd"] = DataSet2_Ytd.Tables[0].Rows[1]["KpiScoreMonth6"];
                                    DataSet2_Ytd.Tables[0].Rows[2]["KpiScoreYtdPercentage"] = DataSet2_Ytd.Tables[0].Rows[1]["KpiScoreMonth6Percentage"];
                                }
                                else
                                {
                                    DataSet2_Ytd.Tables[0].Rows[2]["KpiScoreYtd"] = "-";
                                    DataSet2_Ytd.Tables[0].Rows[2]["KpiScoreYtdPercentage"] = "-";
                                }
                            }
                            else  //Half1
                            {
                                //Current Half
                                if (DataSet2_Ytd.Tables[0].Rows[1]["KpiScoreMonth3"].ToString() != "-" && DataSet2_Ytd.Tables[0].Rows[1]["KpiScoreMonth6"].ToString() != "-")
                                {
                                    if (Convert.ToInt32(DataSet2_Ytd.Tables[0].Rows[1]["KpiScoreMonth3"]) >= Convert.ToInt32(DataSet2_Ytd.Tables[0].Rows[1]["KpiScoreMonth6"]))
                                    {
                                        DataSet2_Ytd.Tables[0].Rows[1]["KpiScoreYtd"] = DataSet2_Ytd.Tables[0].Rows[1]["KpiScoreMonth3"];
                                        DataSet2_Ytd.Tables[0].Rows[1]["KpiScoreYtdPercentage"] = DataSet2_Ytd.Tables[0].Rows[1]["KpiScoreMonth3Percentage"];
                                    }
                                    else
                                    {
                                        DataSet2_Ytd.Tables[0].Rows[1]["KpiScoreYtd"] = DataSet2_Ytd.Tables[0].Rows[1]["KpiScoreMonth6"];
                                        DataSet2_Ytd.Tables[0].Rows[1]["KpiScoreYtdPercentage"] = DataSet2_Ytd.Tables[0].Rows[1]["KpiScoreMonth6Percentage"];
                                    }
                                }
                                else if (DataSet2_Ytd.Tables[0].Rows[1]["KpiScoreMonth3"].ToString() != "-" && DataSet2_Ytd.Tables[0].Rows[1]["KpiScoreMonth6"].ToString() == "-")
                                {
                                    DataSet2_Ytd.Tables[0].Rows[1]["KpiScoreYtd"] = DataSet2_Ytd.Tables[0].Rows[1]["KpiScoreMonth3"];
                                    DataSet2_Ytd.Tables[0].Rows[1]["KpiScoreYtdPercentage"] = DataSet2_Ytd.Tables[0].Rows[1]["KpiScoreMonth3Percentage"];
                                }
                                else if (DataSet2_Ytd.Tables[0].Rows[1]["KpiScoreMonth3"].ToString() == "-" && DataSet2_Ytd.Tables[0].Rows[1]["KpiScoreMonth6"].ToString() != "-")
                                {
                                    DataSet2_Ytd.Tables[0].Rows[1]["KpiScoreYtd"] = DataSet2_Ytd.Tables[0].Rows[1]["KpiScoreMonth6"];
                                    DataSet2_Ytd.Tables[0].Rows[1]["KpiScoreYtdPercentage"] = DataSet2_Ytd.Tables[0].Rows[1]["KpiScoreMonth6Percentage"];
                                }
                                else
                                {
                                    DataSet2_Ytd.Tables[0].Rows[1]["KpiScoreYtd"] = "-";
                                    DataSet2_Ytd.Tables[0].Rows[1]["KpiScoreYtdPercentage"] = "-";
                                }

                                //Previous Half
                                //Changes For DT 3215
                                string column;
                                Int32 KpiScorevalue; Int32 finalKpiScoreVal; Int32 KpiScorePercentagevalue; Int32 finalKpiScorePercentageVal;

                                //DataTable dt = new DataTable();
                                DataRow dr1 = Helper.Radar.CreateDataRow_DataSet2_Ytd(DataSet2.Tables[0], "CSA - Self Audit (% Score) - Current Qtr",
                                                                                    CompanyId, site, 12, Year - 1, csc);
                                //DataSet3.Tables[0].Rows.Add(dr1);
                                finalKpiScoreVal = 0;
                                finalKpiScorePercentageVal = 0;
                                for (int i = 7; i <= 12; i++)
                                {
                                    column = "KpiScoreMonth" + i;
                                    if (dr1[column].ToString() != "-")
                                        KpiScorevalue = Convert.ToInt32(dr1[column]);
                                    else
                                        KpiScorevalue = 0;

                                    if (KpiScorevalue > finalKpiScoreVal)
                                    {
                                        finalKpiScoreVal = KpiScorevalue;
                                    }

                                    if (dr1[column + "Percentage"].ToString() != "-")
                                        KpiScorePercentagevalue = Convert.ToInt32(dr1[column + "Percentage"]);
                                    else
                                        KpiScorePercentagevalue = 0;

                                    if (KpiScorePercentagevalue > finalKpiScorePercentageVal)
                                    {
                                        finalKpiScorePercentageVal = KpiScorePercentagevalue;
                                    }
                                }
                                DataSet2_Ytd.Tables[0].Rows[2]["KpiScoreYtd"] = finalKpiScoreVal;
                                DataSet2_Ytd.Tables[0].Rows[2]["KpiScoreYtdPercentage"] = finalKpiScorePercentageVal;
                                //DataSet2_Ytd.Tables[0].Rows[2]["KpiScoreYtd"] = "-";
                                //DataSet2_Ytd.Tables[0].Rows[2]["KpiScoreYtdPercentage"] = "-";

                            }
                            */
                        }
                    }
                    catch (Exception ex)
                    {

                    }
                    //End

                    #region Spider Table
                    try
                    {

                        if (dr["KpiMeasure"].ToString() == "Alcoa Annual Audit Score (%)" ||
                                    dr["KpiMeasure"].ToString() == "IFE : Injury Ratio" ||
                                    dr["KpiMeasure"].ToString() == "CSA - Self Audit (% Score) - Current Qtrxxx")  //Modified Back DT445
                            //|| dr["KpiMeasure"].ToString() == "RN : Injury Ratio")
                        {
                            skip = true;
                        }

                        if (!skip)
                        {
                            string KpiMeasure = dr["KpiMeasure"].ToString();
                            switch (KpiMeasure)
                            {
                                case "CSA - Self Audit (% Score) - Qtr 4":
                                    KpiMeasure = "CSA - Self Audit";
                                    break;
                                case "CSA - Self Audit (% Score) - Qtr":
                                    KpiMeasure = "CSA - Self Audit";
                                    break;
                                case "CSA - Self Audit (% Score) - Previous Qtr":
                                    KpiMeasure = "CSA - Self Audit";
                                    break;
                                case "CSA - Self Audit (% Score) - Current Qtr":
                                    KpiMeasure = "CSA - Self Audit";
                                    break;
                                case "CSA - Self Audit (% Score) - Previous Half":
                                    KpiMeasure = "CSA - Self Audit";
                                    break;
                                case "Workplace Safety & Compliance":
                                    KpiMeasure = "Workplace Safety\n& Compliance";
                                    break;
                                case "Management Health Safety Work Contacts":
                                    KpiMeasure = "Management H&S\nWork Contacts";
                                    break;
                                case "Behavioural Observations (%)":
                                    KpiMeasure = "Behavioural\nSafety Program";
                                    break;
                                case "Fatality Prevention Program":
                                    KpiMeasure = "Fatality\nPrevention";
                                    break;
                                case "JSA Field Audit Verifications":
                                    KpiMeasure = "JSA Field\nAudit";
                                    break;
                                case "Toolbox Meetings":
                                    KpiMeasure = "Toolbox\nMeetings\nper month";
                                    break;
                                case "Alcoa Weekly Contractors Meeting":
                                    KpiMeasure = "Alcoa\nWeekly\nContractors\nMeeting";
                                    break;
                                case "Alcoa Monthly Contractors Meeting":
                                    KpiMeasure = "Alcoa\nMonthly\nContractors\nMeeting";
                                    break;
                                case "Safety Plan(s) Submitted":
                                    KpiMeasure = "Safety Plan(s)\n(% Submitted)";
                                    break;
                                case "% Mandated Training":
                                    KpiMeasure = "% Mandated\nTraining";
                                    break;
                                default:
                                    skip = true;
                                    break;
                            }
                            if (dr["KpiScoreYtdPercentage"].ToString() == "-")
                            {
                                dr["KpiScoreYtdPercentage"] = "0";
                            }
                            string d = dr["KpiScoreYtdPercentage"].ToString();

                            if (KpiMeasure == "CSA - Self Audit")//Added By ashley Goldstraw DT445 12/1/2016
                            {
                                int.TryParse(dr["KpiScoreYtdPercentage"].ToString(), out spiderCSAScore);
                                if (spiderCSAScore > bestCSAScore)
                                {
                                    bestCSAScore = spiderCSAScore;
                                }
                                if (foundCSA == true)
                                { 
                                    dtSpider.Rows.Add(new object[] { KpiMeasure, bestCSAScore.ToString() }); 
                                }
                                foundCSA = true;
                                
                            }
                            else
                            {
                                dtSpider.Rows.Add(new object[] { KpiMeasure, dr["KpiScoreYtdPercentage"].ToString() });
                            }
                        }
                    }
                    catch (Exception ex)
                    { }
                    #endregion

                    if (dr["KpiMeasure"].ToString() != "On site" &&
                        dr["KpiMeasure"].ToString() != "Alcoa Annual Audit Score (%)" &&
                        dr["KpiMeasure"].ToString() != "CSA - Self Audit (% Score) - Current Qtr" && //DT318 Changed from Current Qtr to Previous Qtr by Ashley Goldstraw 16/12/2015 Changed back DT445
                        dr["KpiMeasure"].ToString() != "RN : Injury Ratio")
                    {
                        if (dr["KpiScoreYtd"].ToString() != "-")
                        {
                            NoKpi++;
                        }

                        if (dr["KpiScoreYtdPercentage"].ToString() == "100")
                        {
                            NoKpiGreen++;
                        }
                    }
                }
            }

            //if != embedded3
            if (CategoryId != (int)CompanySiteCategoryList.NE3)
            {
                //show Raw Scores Grid
                divRawScores.Visible = true;
            }
            else
            {
                wcSpider.Visible = false;
            }


            #region Month Table
            try
            {
                if (dr["KpiMeasure"].ToString() != "CSA - Self Audit (% Score) - Current Qtr" && dr["KpiMeasure"].ToString() != "Safety Plan(s) Submitted"
                    && dr["KpiMeasure"].ToString() != "Alcoa Annual Audit Score (%)" && !(dr["KpiMeasure"].ToString().Contains("CSA - Self Audit (% Score)")))
                {
                    if (dr["KpiMeasure"].ToString() == "Behavioural Observations - Plan" || dr["KpiMeasure"].ToString() == "Behavioural Observations - Actual")
                    {
                        DataSet2_Ytd_Month.Tables[1].ImportRow(dr);
                    }
                    else
                    {
                        DataSet2_Ytd_Month.Tables[0].ImportRow(dr);
                    }
                }
            }
            catch (Exception ex)
            { }


            #endregion

            if (dr["KpiMeasure"].ToString() == "CSA - Self Audit (% Score) - Current Qtr")
            {
                dr["KpiMeasure"] = "CSA - Self Audit (% Score) - Qtr";
                DataSet2_Ytd_Qtr.Tables[0].ImportRow(dr);
                if (CategoryId == 2)
                {
                    try
                    {
                        string m3 = DataSet2_Ytd_Qtr.Tables[0].Rows[0]["KpiScoreMonth3"].ToString();
                        string m6 = DataSet2_Ytd_Qtr.Tables[0].Rows[0]["KpiScoreMonth6"].ToString();
                        string m9 = DataSet2_Ytd_Qtr.Tables[0].Rows[0]["KpiScoreMonth9"].ToString();
                        string m12 = DataSet2_Ytd_Qtr.Tables[0].Rows[0]["KpiScoreMonth12"].ToString();
                        if (DataSet2_Ytd_Qtr.Tables[0].Rows[0]["KpiScoreMonth3"].ToString() == "0") DataSet2_Ytd_Qtr.Tables[0].Rows[0]["KpiScoreMonth3"] = "0";
                        if (DataSet2_Ytd_Qtr.Tables[0].Rows[0]["KpiScoreMonth6"].ToString() == "0") DataSet2_Ytd_Qtr.Tables[0].Rows[0]["KpiScoreMonth6"] = "0";
                        if (DataSet2_Ytd_Qtr.Tables[0].Rows[0]["KpiScoreMonth9"].ToString() == "0") DataSet2_Ytd_Qtr.Tables[0].Rows[0]["KpiScoreMonth9"] = "0";
                        if (DataSet2_Ytd_Qtr.Tables[0].Rows[0]["KpiScoreMonth12"].ToString() == "0") DataSet2_Ytd_Qtr.Tables[0].Rows[0]["KpiScoreMonth12"] = "0";
                        
                        if (DataSet2_Ytd_Qtr.Tables[0].Rows[0]["KpiScoreMonth3"].ToString() != "-" && DataSet2_Ytd_Qtr.Tables[0].Rows[0]["KpiScoreMonth6"].ToString() != "-")
                        {
                            if (Convert.ToInt32(DataSet2_Ytd_Qtr.Tables[0].Rows[0]["KpiScoreMonth3"]) < Convert.ToInt32(DataSet2_Ytd_Qtr.Tables[0].Rows[0]["KpiScoreMonth6"]))
                            {
                                DataSet2_Ytd_Qtr.Tables[0].Rows[0]["KpiScoreMonth3"] = DataSet2_Ytd_Qtr.Tables[0].Rows[0]["KpiScoreMonth6"];
                               // DataSet2_Ytd_Qtr.Tables[0].Rows[0]["KpiScoreMonth3"] = "N/A";
                            }
                            else
                            {
                                DataSet2_Ytd_Qtr.Tables[0].Rows[0]["KpiScoreMonth6"] = DataSet2_Ytd_Qtr.Tables[0].Rows[0]["KpiScoreMonth3"];
                                //DataSet2_Ytd_Qtr.Tables[0].Rows[0]["KpiScoreMonth6"] = "N/A";
                            }
                        }
                        else if (DataSet2_Ytd_Qtr.Tables[0].Rows[0]["KpiScoreMonth3"].ToString() != "-" && DataSet2_Ytd_Qtr.Tables[0].Rows[0]["KpiScoreMonth6"].ToString() == "-")
                        {
                            DataSet2_Ytd_Qtr.Tables[0].Rows[0]["KpiScoreMonth6"] = DataSet2_Ytd_Qtr.Tables[0].Rows[0]["KpiScoreMonth3"];
                            //DataSet2_Ytd_Qtr.Tables[0].Rows[0]["KpiScoreMonth6"] = "N/A";
                        }
                        else if (DataSet2_Ytd_Qtr.Tables[0].Rows[0]["KpiScoreMonth3"].ToString() == "-" && DataSet2_Ytd_Qtr.Tables[0].Rows[0]["KpiScoreMonth6"].ToString() != "-")
                        {
                            DataSet2_Ytd_Qtr.Tables[0].Rows[0]["KpiScoreMonth3"] = DataSet2_Ytd_Qtr.Tables[0].Rows[0]["KpiScoreMonth6"];
                            //DataSet2_Ytd_Qtr.Tables[0].Rows[0]["KpiScoreMonth3"] = "N/A";
                        }
                        

                        if (DataSet2_Ytd_Qtr.Tables[0].Rows[0]["KpiScoreMonth9"].ToString() != "-" && DataSet2_Ytd_Qtr.Tables[0].Rows[0]["KpiScoreMonth12"].ToString() != "-")
                        {
                            if (Convert.ToInt32(DataSet2_Ytd_Qtr.Tables[0].Rows[0]["KpiScoreMonth9"]) < Convert.ToInt32(DataSet2_Ytd_Qtr.Tables[0].Rows[0]["KpiScoreMonth12"]))
                                DataSet2_Ytd_Qtr.Tables[0].Rows[0]["KpiScoreMonth9"] = DataSet2_Ytd_Qtr.Tables[0].Rows[0]["KpiScoreMonth12"];
                            else
                                DataSet2_Ytd_Qtr.Tables[0].Rows[0]["KpiScoreMonth12"] = DataSet2_Ytd_Qtr.Tables[0].Rows[0]["KpiScoreMonth9"];
                        }
                        else if (DataSet2_Ytd_Qtr.Tables[0].Rows[0]["KpiScoreMonth9"].ToString() != "-" && DataSet2_Ytd_Qtr.Tables[0].Rows[0]["KpiScoreMonth12"].ToString() == "-")
                        {
                            DataSet2_Ytd_Qtr.Tables[0].Rows[0]["KpiScoreMonth12"] = DataSet2_Ytd_Qtr.Tables[0].Rows[0]["KpiScoreMonth9"];
                        }
                        else if (DataSet2_Ytd_Qtr.Tables[0].Rows[0]["KpiScoreMonth9"].ToString() == "-" && DataSet2_Ytd_Qtr.Tables[0].Rows[0]["KpiScoreMonth12"].ToString() != "-")
                        {
                            DataSet2_Ytd_Qtr.Tables[0].Rows[0]["KpiScoreMonth9"] = DataSet2_Ytd_Qtr.Tables[0].Rows[0]["KpiScoreMonth12"];
                        }
                    }
                    catch (Exception exc)
                    { }
                }
            }
        }
        //int storedValue = 0;
        //int value = 0;
        //foreach (DataRow dr in dtSpider.Rows)
        //{
        //    if (dr["key"].ToString() == "CSA - Self Audit")
        //    {
        //        int.TryParse(dr["value"].ToString(), out value);
        //        if (value > storedValue)
        //            storedValue = value;
        //    }
        //}
        try
        {
            //Medical Schedules & Training Schedules
            DataSet2_Ytd_Qtr.Tables[0].Rows.Add(Helper.Radar.CreateDataRow_DataSet2_Ytd(DataSet2_Ytd_Qtr.Tables[0], "Medical Schedules",
                                                                                            CompanyId, site, MonthId, Year, csc));
            DataSet2_Ytd_Qtr.Tables[0].Rows.Add(Helper.Radar.CreateDataRow_DataSet2_Ytd(DataSet2_Ytd_Qtr.Tables[0], "Training Schedules",
                                                                                            CompanyId, site, MonthId, Year, csc));


            lblLWDFR.Text = Helper.Radar.GetRateScore("LWDFR", CompanyId, SiteId, CategoryId, MonthId, Year);
            lblTRIFR.Text = Helper.Radar.GetRateScore("TRIFR", CompanyId, SiteId, CategoryId, MonthId, Year);
            lblAIFR.Text = Helper.Radar.GetRateScore("AIFR", CompanyId, SiteId, CategoryId, MonthId, Year);
            lblDART.Text = Helper.Radar.GetRateScore("DART", CompanyId, SiteId, CategoryId, MonthId, Year);
            lblIFECount.Text = Helper.Radar.GetRateScore("IFECount", CompanyId, SiteId, CategoryId, MonthId, Year);
            lblRNCount.Text = Helper.Radar.GetRateScore("RNCount", CompanyId, SiteId, CategoryId, MonthId, Year);
            lblIFERNCount.Text = Helper.Radar.GetRateScore("IFE+RN Count", CompanyId, SiteId, CategoryId, MonthId, Year);
            lblIFEInjuryRatio.Text = Helper.Radar.GetRateScore("IFE Injury Ratio", CompanyId, SiteId, CategoryId, MonthId, Year);
            lblRNInjuryRatio.Text = Helper.Radar.GetRateScore("RN Injury Ratio", CompanyId, SiteId, CategoryId, MonthId, Year);
            lblIFERNInjuryRatio.Text = Helper.Radar.GetRateScore("IFERN Injury Ratio", CompanyId, SiteId, CategoryId, MonthId, Year);

            //Add By Bishwajit for Item#31
            Session["WDFR"] = lblLWDFR.Text;
            Session["TRIFR"] = lblTRIFR.Text;
            Session["AIFR"] = lblAIFR.Text;
            Session["DART"] = lblDART.Text;
            Session["IFECount"] = lblIFECount.Text;
            Session["RNCount"] = lblRNCount.Text;
            Session["IFERNCount"] = lblIFERNCount.Text;
            Session["IFEInjuryRatio"] = lblIFEInjuryRatio.Text;
            Session["RNInjuryRatio"] = lblRNInjuryRatio.Text;
            Session["IFERNInjuryRatio"] = lblIFERNInjuryRatio.Text;

            //End Add By Bishwajit for Item#31


            Session["Radar_Table1-Summary"] = DataSet1_Summary;
            Session["Radar_Table2-Ytd"] = DataSet2_Ytd;
            Session["Radar_Table2-YtdMonth"] = DataSet2_Ytd_Month;
            Session["Radar_Table2-YtdQtr"] = DataSet2_Ytd_Qtr;
            Session["Radar_Table3-Spider"] = DataSet3_Spider;
            //Add By Bishwajit for Item#31
            Session["IsValueTrue"] = true;
            //Add By Bishwajit for Item#31
        }
        catch (Exception ex)
        { }
    }
    protected void BindData()
    {
        rawScoresArray = new string[15];

        grid.DataSource = (DataTable)(((DataSet)Session["Radar_Table1-Summary"]).Tables[0]);
        grid2.DataSource = (DataTable)(((DataSet)Session["Radar_Table2-Ytd"]).Tables[0]);
        gridMonthlyE.DataSource = (DataTable)(((DataSet)Session["Radar_Table2-YtdMonth"]).Tables[0]);
        gridRawScores.DataSource = (DataTable)(((DataSet)Session["Radar_Table2-YtdMonth"]).Tables[1]);
        gridQtrly.DataSource = (DataTable)(((DataSet)Session["Radar_Table2-YtdQtr"]).Tables[0]);

        grid.DataBind();
        grid2.DataBind();
        gridMonthlyE.DataBind();
        gridRawScores.DataBind();
        gridQtrly.DataBind();

        wcSpider.Series.Clear();
        Series series = new Series("Spider", ViewType.RadarArea);
        wcSpider.Series.Add(series);
        wcSpider.DataSourceID = String.Empty;
        wcSpider.DataSource = (DataTable)(((DataSet)(Session["Radar_Table3-Spider"])).Tables[0]);
        wcSpider.Series[0].ArgumentScaleType = ScaleType.Qualitative;
        wcSpider.Series[0].ArgumentDataMember = "Key";
        wcSpider.Series[0].ValueScaleType = ScaleType.Numerical;
        wcSpider.Series[0].ValueDataMembers.AddRange(new string[] { "Value" });
        wcSpider.Series[0].Label.Visible = false; //Hide Point Labels until version feature able to have point labels pointing 'inward'
        wcSpider.DataBind();

        //Add By Bishwajit for Item#31

        if (!Directory.Exists(Server.MapPath(@"~/ChartImages/" + auth.UserId.ToString())))
        {
            System.IO.Directory.CreateDirectory(Server.MapPath(@"~/Images/ChartImages/" + auth.UserId.ToString()));
        }

        System.IO.DirectoryInfo directory = new DirectoryInfo(Server.MapPath(@"~/Images/ChartImages/" + auth.UserId.ToString()));
        foreach (FileInfo file in directory.GetFiles())
        {
            file.Delete();
        }

        Session["ImagePath"] = @"~/Images/ChartImages/" + auth.UserId.ToString() + "/" + GetCurrentDateTimeStamp() + "_2.png";
        //Session["ImagePath"] = @"~/Images/ChartImages/" + GetCurrentDateTimeStamp() + "_2.png";

        ((IChartContainer)wcSpider).Chart.ExportToImage(Server.MapPath(Convert.ToString(Session["ImagePath"])), System.Drawing.Imaging.ImageFormat.Png);

        //Add By Bishwajit for Item#31

        lblLastViewedAt.Text = String.Format("Last Viewed at: {0}", DateTime.Now);


        grid2.Columns["Plan"].Caption = "YTD Plan";
        gridMonthlyE.Columns["Plan"].Caption = "Mth Plan";
        gridQtrly.Columns["Plan"].Caption = "Qtr Plan";
    }
    //Add By Bishwajit for Item#31
    private string GetCurrentDateTimeStamp()
    {
        string RetVal = string.Empty;
        RetVal = Convert.ToString(DateTime.Now.Day + DateTime.Now.Month + DateTime.Now.Year);
        RetVal = RetVal + DateTime.Now.TimeOfDay.ToString().Replace(":", "").Substring(0, 6);
        return RetVal;
    }
    //End Add By Bishwajit for Item#31

    protected void ddlMonth_SelectedIndexChanged(object sender, EventArgs e)
    {
        //Add By Bishwajit for Item#31
        try
        {
            //End Add By Bishwajit for Item#31
            int MonthId = 0;
            if (ddlMonth.SelectedItem.Value != null)
            {
                int.TryParse(ddlMonth.SelectedItem.Value.ToString(), out MonthId);
                if (MonthId > 0)
                {
                    SessionHandler.spVar_MonthId = MonthId.ToString();
                }
                else
                {
                    SessionHandler.spVar_MonthId = "0";
                }
            }
            moduleInit(false);
        }
        //Add By Bishwajit for Item#31
        catch (Exception)
        {

            if (SiteId > 0)
            {
                throw;
            }
        }
        //End Add By Bishwajit for Item#31
    }

    protected void grid2_CustomSummaryCalculate(object sender, DevExpress.Data.CustomSummaryEventArgs e)
    {
        //Add By Bishwajit for Item#31
        try
        {
            //End Add By Bishwajit for Item#31
            if (e.SummaryProcess == CustomSummaryProcess.Finalize)
            {
                decimal dev = Convert.ToDecimal(NoKpiGreen);
                if (dev > 0)
                {

                    if (NoKpi != 0) //Added by Ashley Goldstraw to prevent div/0.  DT 319
                    {
                        decimal perc = dev / NoKpi;
                        perc = perc * 100;
                        e.TotalValue = String.Format(Convert.ToInt32(perc).ToString(), "N") + "%";
                    }

                    else
                    {
                        e.TotalValue = "n/a";
                        //e.TotalValue = "0%";
                    }
                }
            }
        }
        //Add By Bishwajit for Item#31
        catch (Exception)
        {

            if (SiteId > 0)
            {
                throw;
            }
        }
        //End Add By Bishwajit for Item#31
    }

    protected void grid2_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {
        //Add By Bishwajit for Item#31
        try
        {
            //End Add By Bishwajit for Item#31
            if (e.RowType != GridViewRowType.Data) return;
            string Kpi = (string)grid2.GetRowValues(e.VisibleIndex, "KpiMeasure");

            if (!String.IsNullOrEmpty(Kpi))
            {
                if (Kpi != "Alcoa Annual Audit Score (%)" && Kpi != "CSA - Self Audit (% Score) - Current Qtr" && Kpi != "CSA - Self Audit (% Score) - Current Half") //DT318 AG Changed from Current to Previous Changed Back DT445
                {
                    for (int i = 3; i < e.Row.Cells.Count; i++)
                    {
                        string colour = "";
                        string _KpiValue = (string)grid2.GetRowValues(e.VisibleIndex, grid2.Columns[i].Name);

                        if (!String.IsNullOrEmpty(_KpiValue))
                        {
                            if (_KpiValue != "-")
                            {
                                if (_KpiValue == "?")
                                {
                                    colour = "Yellow";
                                }
                                else
                                {
                                    int KpiValue = Convert.ToInt32(_KpiValue);
                                    if (KpiValue == 100) colour = CellColourGood;
                                    else colour = CellColourBad;
                                    if (Kpi == "CSA - Self Audit (% Score) - Qtr 4" ||
                                        Kpi == "CSA - Self Audit (% Score) - Previous Qtr" || //DT318 Changed from Previous to Current Changed Back DT445
                                        Kpi == "CSA - Self Audit (% Score) - Previous Half" || //DT318 Changed from Previous to Current Change Back DT445
                                        Kpi == "CSA - Self Audit (% Score) - Qtr")
                                    {
                                        if (KpiValue > 0)
                                        {
                                            string KpiTarget = (string)grid2.GetRowValues(e.VisibleIndex, grid2.Columns[1].Name);
                                            string KpiScore = (string)grid2.GetRowValues(e.VisibleIndex, grid2.Columns[2].Name);
                                            //DT 3215 Changes
                                            double _KpiTarget = Convert.ToDouble(KpiTarget);
                                            int _KpiScore = Convert.ToInt32(KpiScore);
                                            if (_KpiScore < _KpiTarget)
                                            {
                                                e.Row.Cells[2].Style.Add("background-color", CellColourBad); //colour cell red but let them be 'compliant'
                                            }
                                            if ((string)grid2.GetRowValues(e.VisibleIndex, grid2.Columns[3].Name) == "100")
                                                colour = CellColourGood; //If they've done a CSA. Their compliant..
                                        }
                                    }
                                }
                            }
                        }

                        if (!String.IsNullOrEmpty(colour))
                        {
                            e.Row.Cells[i].Style.Add("background-color", colour);
                        }
                    }
                }
                else
                {
                    string colour = "";
                    string _KpiValue = (string)grid2.GetRowValues(e.VisibleIndex, grid2.Columns[2].Name);
                    string _KpiTarget = (string)grid2.GetRowValues(e.VisibleIndex, grid2.Columns[1].Name);
                    if (!String.IsNullOrEmpty(_KpiValue) && !String.IsNullOrEmpty(_KpiTarget))
                    {
                        if (_KpiValue != "-")
                        {
                            if (_KpiValue == "?")
                            {
                                colour = CellColourUnsure;
                            }
                            else
                            {
                                int KpiValue = Convert.ToInt32(_KpiValue);
                                int KpiTarget = Convert.ToInt32(_KpiTarget);
                                if (KpiValue >= KpiTarget) colour = CellColourGood;
                                else colour = CellColourBad;
                            }
                        }
                    }

                    if (!String.IsNullOrEmpty(colour))
                    {
                        e.Row.Cells[2].Style.Add("background-color", colour);
                    }
                }
            }
        }
        //Add By Bishwajit for Item#31
        catch (Exception ex)
        {

            if (SiteId > 0)
            {
                throw;
            }
        }
        //End Add By Bishwajit for Item#31
    }
    protected void gridMonthlyE_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {
        //Add By Bishwajit for Item#31
        try
        {
            //End Add By Bishwajit for Item#31
            if (e.RowType != GridViewRowType.Data) return;
            string Kpi = (string)gridMonthlyE.GetRowValues(e.VisibleIndex, "KpiMeasure");
            if (gridMonthlyE.GetRowValues(e.VisibleIndex, gridMonthlyE.Columns[1].Name) == null) return;
            string _KpiTarget = (string)gridMonthlyE.GetRowValues(e.VisibleIndex, gridMonthlyE.Columns[1].Name);
            if (!String.IsNullOrEmpty(Kpi))
            {

                for (int i = 2; i < e.Row.Cells.Count; i++)
                {
                    string colour = "";

                    string _KpiValue = (string)gridMonthlyE.GetRowValues(e.VisibleIndex, gridMonthlyE.Columns[i].Name);

                    if (Kpi != "On site")
                    {
                        if (!String.IsNullOrEmpty(_KpiTarget))
                        {
                            if (_KpiValue != "-" && _KpiTarget != "-")
                            {
                                if (_KpiValue == "?")
                                {
                                    colour = CellColourUnsure;
                                }
                                else
                                {
                                    if (_KpiTarget == "0")
                                    {
                                        colour = CellColourGood;
                                    }
                                    else
                                    {
                                        if (String.IsNullOrEmpty(_KpiValue))
                                        {
                                            colour = CellColourBad;
                                        }
                                        else
                                        {
                                            Decimal KpiTarget = Convert.ToDecimal(_KpiTarget);
                                            Decimal KpiValue = Convert.ToDecimal(_KpiValue);

                                            if (KpiValue >= KpiTarget) colour = CellColourGood;
                                            else colour = CellColourBad;
                                        }

                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        if (_KpiValue == "?")
                        {
                            colour = CellColourUnsure;
                        }
                    }

                    if (!String.IsNullOrEmpty(colour))
                    {
                        e.Row.Cells[i].Style.Add("background-color", colour);
                    }
                }
            }
        }
        //Add By Bishwajit for Item#31
        catch (Exception ex)
        {

            if (SiteId > 0)
            {
                throw;
            }
        }
        //End Add By Bishwajit for Item#31
    }
    protected void gridRawScores_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {
        //Add By Bishwajit for Item#31
        try
        {
            //End Add By Bishwajit for Item#31
            if (e.RowType != GridViewRowType.Data) return;

            if (rawScoresArray[0] == null)
            {
                for (int i = 0; i < e.Row.Cells.Count; i++)
                {
                    rawScoresArray[i] = (string)gridRawScores.GetRowValues(e.VisibleIndex, gridRawScores.Columns[i].Name);
                }
            }
            else
            {
                for (int i = 0; i < e.Row.Cells.Count; i++)
                {
                    string colour = "";

                    int cellPlan = 0;
                    bool cellPlan_isInt = Int32.TryParse(rawScoresArray[i], out cellPlan);

                    string _cellActual = (string)gridRawScores.GetRowValues(e.VisibleIndex, gridRawScores.Columns[i].Name);
                    int cellActual = 0;
                    bool cellActual_isInt = Int32.TryParse(_cellActual, out cellActual);

                    if (String.IsNullOrEmpty(_cellActual))
                    {
                        colour = CellColourBad;
                    }

                    if (cellPlan_isInt)
                    {
                        if (cellActual_isInt)
                        {
                            if (cellActual >= cellPlan)
                            {
                                colour = CellColourGood;
                            }
                            else
                            {
                                colour = CellColourBad;
                            }
                        }
                    }

                    if (!String.IsNullOrEmpty(colour))
                    {
                        e.Row.Cells[i].Style.Add("background-color", colour);
                    }
                }
            }
        }
        //Add By Bishwajit for Item#31
        catch (Exception ex)
        {

            if (SiteId > 0)
            {
                throw;
            }
        }
        //End Add By Bishwajit for Item#31
    }
    protected void gridQtrly_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {
        //Add By Bishwajit for Item#31
        try
        {
            //End Add By Bishwajit for Item#31
            if (e.RowType != GridViewRowType.Data) return;
            string Kpi = (string)gridQtrly.GetRowValues(e.VisibleIndex, "KpiMeasure");
            string _KpiTarget = (string)gridQtrly.GetRowValues(e.VisibleIndex, gridQtrly.Columns[1].Name);
            if (!String.IsNullOrEmpty(Kpi))
            {

                for (int i = 2; i < e.Row.Cells.Count; i++)
                {
                    string colour = "";

                    string _KpiValue = (string)gridQtrly.GetRowValues(e.VisibleIndex, gridQtrly.Columns[i].Name);

                    if (!String.IsNullOrEmpty(_KpiTarget))
                    {
                        if (_KpiValue != "-" && _KpiTarget != "-")
                        {
                            if (_KpiValue == "?")
                            {
                                colour = CellColourUnsure;
                            }
                            else
                            {
                                if (_KpiTarget == "0")
                                {
                                    colour = CellColourGood;
                                }
                                else
                                {
                                    Decimal KpiTarget = Convert.ToDecimal(_KpiTarget);
                                    Decimal KpiValue = Convert.ToDecimal(_KpiValue);

                                    if (KpiValue >= KpiTarget)
                                    {
                                        colour = CellColourGood; ;
                                    }
                                    else
                                    {
                                        colour = CellColourBad;
                                    }
                                }
                            }
                        }
                    }

                    if (!String.IsNullOrEmpty(colour))
                    {
                        e.Row.Cells[i].Style.Add("background-color", colour);
                    }
                }

                //Added for DT#3112
                if (CategoryId == 2)
                {
                    if (Kpi == "CSA - Self Audit (% Score) - Qtr")
                    {
                        //modified by AG, 8/12/2015 DT412
                        e.Row.Cells[4].ColumnSpan = 2;
                        e.Row.Cells.Remove(e.Row.Cells[5]);
                        
                        e.Row.Cells[2].ColumnSpan = 2;
                        e.Row.Cells.Remove(e.Row.Cells[3]);
                        //End modified by AG, 8/12/2015 DT412                      

                    }
                }
                //End
            }
        }
        //Add By Bishwajit for Item#31
        catch (Exception ex)
        {
            if (SiteId > 0)
            {
                throw;
            }
        }
        //End Add By Bishwajit for Item#31
    }

    public static string GetImageURL(Enum value)
    {

        //try
        //{
        FieldInfo fi = value.GetType().GetField(value.ToString());
        DescriptionAttribute[] attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);
        return (attributes.Length > 0) ? attributes[0].Description : value.ToString();
        //}
        //catch (Exception ex)
        //{

        //    //if (SiteId > 0)
        //    //{
        //    //    throw;
        //    //}
        //}
    }


}