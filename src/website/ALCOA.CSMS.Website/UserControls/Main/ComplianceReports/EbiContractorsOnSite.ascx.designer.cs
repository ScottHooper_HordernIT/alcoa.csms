﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------



public partial class UserControls_EbiContractorsOnSite {
    
    /// <summary>
    /// lblLive control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::DevExpress.Web.ASPxEditors.ASPxLabel lblLive;
    
    /// <summary>
    /// hlLive control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::DevExpress.Web.ASPxEditors.ASPxHyperLink hlLive;
    
    /// <summary>
    /// btnReset control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::DevExpress.Web.ASPxEditors.ASPxButton btnReset;
    
    /// <summary>
    /// grid control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::DevExpress.Web.ASPxGridView.ASPxGridView grid;
    
    /// <summary>
    /// ucExportButtons control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::UserControls_Tiny_ExportButtons ucExportButtons;
    
    /// <summary>
    /// SqExemptionList1 control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::ALCOA.CSMS.Website.UserControls.Main.SqExemptionList SqExemptionList1;
}
