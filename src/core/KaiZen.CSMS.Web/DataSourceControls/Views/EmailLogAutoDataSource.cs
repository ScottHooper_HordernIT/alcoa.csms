﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.EmailLogAutoProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[CLSCompliant(true)]
	[Designer(typeof(EmailLogAutoDataSourceDesigner))]
	public class EmailLogAutoDataSource : ReadOnlyDataSource<EmailLogAuto>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EmailLogAutoDataSource class.
		/// </summary>
		public EmailLogAutoDataSource() : base(new EmailLogAutoService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the EmailLogAutoDataSourceView used by the EmailLogAutoDataSource.
		/// </summary>
		protected EmailLogAutoDataSourceView EmailLogAutoView
		{
			get { return ( View as EmailLogAutoDataSourceView ); }
		}
		
		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the EmailLogAutoDataSourceView class that is to be
		/// used by the EmailLogAutoDataSource.
		/// </summary>
		/// <returns>An instance of the EmailLogAutoDataSourceView class.</returns>
		protected override BaseDataSourceView<EmailLogAuto, Object> GetNewDataSourceView()
		{
			return new EmailLogAutoDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the EmailLogAutoDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class EmailLogAutoDataSourceView : ReadOnlyDataSourceView<EmailLogAuto>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EmailLogAutoDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the EmailLogAutoDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public EmailLogAutoDataSourceView(EmailLogAutoDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal EmailLogAutoDataSource EmailLogAutoOwner
		{
			get { return Owner as EmailLogAutoDataSource; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal EmailLogAutoService EmailLogAutoProvider
		{
			get { return Provider as EmailLogAutoService; }
		}

		#endregion Properties
		
		#region Methods
		
		#endregion Methods
	}

	#region EmailLogAutoDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the EmailLogAutoDataSource class.
	/// </summary>
	public class EmailLogAutoDataSourceDesigner : ReadOnlyDataSourceDesigner<EmailLogAuto>
	{
	}

	#endregion EmailLogAutoDataSourceDesigner

	#region EmailLogAutoFilter

	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EmailLogAuto"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EmailLogAutoFilter : SqlFilter<EmailLogAutoColumn>
	{
	}

	#endregion EmailLogAutoFilter

	#region EmailLogAutoExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EmailLogAuto"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EmailLogAutoExpressionBuilder : SqlExpressionBuilder<EmailLogAutoColumn>
	{
	}
	
	#endregion EmailLogAutoExpressionBuilder		
}

