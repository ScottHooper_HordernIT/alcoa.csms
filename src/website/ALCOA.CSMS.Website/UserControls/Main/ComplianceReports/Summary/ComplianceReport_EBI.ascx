<%@ Control Language="C#" AutoEventWireup="true" Inherits="UserControls_ComplianceReport_EBI" Codebehind="ComplianceReport_EBI.ascx.cs" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>

<table border="0" cellpadding="2" cellspacing="0" width="100%">
    <tr>
    <td class="pageName" colspan="3" style="height: 17px">
nb. Data Refreshed Everyday at 9am (Western Australian Standard Time - GMT+8).<br />
            </td>
    </tr>
    <tr>
        <td>
            <dx:ASPxGridView ID="grid" ClientInstanceName="grid" runat="server" AutoGenerateColumns="False" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue" OnHtmlRowCreated="grid_RowCreated" OnCellEditorInitialize="grid_CellEditorInitialize" width="900px">
                <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue">
                    <Header ImageSpacing="5px" SortingImageSpacing="5px">
                    </Header>
                    <LoadingPanel ImageSpacing="10px">
                    </LoadingPanel>
                    <AlternatingRow Enabled="True">
                    </AlternatingRow>
                </Styles>
                <ImagesFilterControl>
                    <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
                    </LoadingPanel>
                </ImagesFilterControl>
                <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                    <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
                    </LoadingPanelOnStatusBar>
                    <LoadingPanel Url="~/App_Themes/Office2003Blue/GridView/Loading.gif">
                    </LoadingPanel>
                </Images>
                <Columns>
                    <dx:GridViewDataComboBoxColumn Name="gcbResidentialCategory" Caption="Residential Category"
                        FieldName="CompanySiteCategoryId" SortIndex="0" SortOrder="Descending" UnboundType="String"
                        VisibleIndex="0" FixedStyle="Left" Width="145px">
                        <PropertiesComboBox DataSourceID="CompanySiteCategoryDataSource" DropDownHeight="150px"
                            TextField="CategoryDesc" ValueField="CompanySiteCategoryId" ValueType="System.Int32"
                            IncrementalFilteringMode="StartsWith">
                        </PropertiesComboBox>
                        <Settings SortMode="DisplayText" />
                    </dx:GridViewDataComboBoxColumn>
                    <%--<dx:GridViewDataTextColumn Caption="Residential Category" FieldName="CategoryDesc" VisibleIndex="0"
                        Width="145px" SortIndex="0" SortOrder="Descending">
                        <HeaderStyle HorizontalAlign="Center" />
                    </dx:GridViewDataTextColumn>--%>
                    <dx:GridViewDataTextColumn Caption="Site Name" FieldName="Site" VisibleIndex="0"
                        Width="78px" SortIndex="1" SortOrder="Ascending">
                        <HeaderStyle HorizontalAlign="Center" />
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Company Name (EBI)" FieldName="CompanyName" VisibleIndex="2"
                        SortIndex="2" SortOrder="Ascending" Width="113px">
                        <HeaderStyle HorizontalAlign="Center" Wrap="True" />
                        <CellStyle Wrap="true" />
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Company Name (CSMS)" FieldName="CompanyName-CSMS"
                        VisibleIndex="3" Width="108px">
                        <HeaderStyle HorizontalAlign="Center" Wrap="True" />
                        <CellStyle Wrap="true" />
                        <DataItemTemplate>
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <asp:HyperLink ID="hlq" runat="server" Text="CompanyName" NavigateUrl="" Visible="false"></asp:HyperLink><asp:Label
                                            ID="lbq" runat="server" Text="CompanyName" Visible="false"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </DataItemTemplate>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Type" FieldName="Type" VisibleIndex="4" Width="105px">
                        <HeaderStyle HorizontalAlign="Center" />
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="Valid" VisibleIndex="5" Width="70px" SortIndex="1"
                        SortOrder="Descending">
                        <DataItemTemplate>
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <asp:Image ID="changeImage" runat="server" ImageUrl="~/Images/spacer.gif" Visible="true"
                                            GenerateEmptyAlternateText="True" />
                                    </td>
                                </tr>
                            </table>
                        </DataItemTemplate>
                        <HeaderStyle HorizontalAlign="Center" />
                        <CellStyle HorizontalAlign="Center">
                        </CellStyle>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Approved For Site" FieldName="ApprovedOnSite"
                        VisibleIndex="6" Width="71px">
                        <HeaderStyle HorizontalAlign="Center" Wrap="True" />
                        <CellStyle VerticalAlign="Middle" HorizontalAlign="Center" Wrap="true">
                        </CellStyle>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="Validity" VisibleIndex="7" Width="83px">
                        <HeaderStyle HorizontalAlign="Center" Wrap="True" />
                        <CellStyle VerticalAlign="Middle" HorizontalAlign="Center">
                        </CellStyle>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Company SQ Status" FieldName="CompanySQStatus"
                        VisibleIndex="8" Width="150px">
                        <HeaderStyle HorizontalAlign="Center" />
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="SQ Valid Till" FieldName="SqValidTill" VisibleIndex="9"
                        Width="70px">
                        <HeaderStyle HorizontalAlign="Center" Wrap="True" />
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="QuestionnaireId" Visible="False">
                        <HeaderStyle HorizontalAlign="Center" />
                    </dx:GridViewDataTextColumn>
                </Columns>
                <Settings ShowGroupPanel="True" ShowHeaderFilterButton="True" />
                <StylesEditors>
                    <ProgressBar Height="25px">
                    </ProgressBar>
                </StylesEditors>
                <SettingsPager AlwaysShowPager="True" Mode="ShowAllRecords">
                    <AllButton Visible="True">
                    </AllButton>
                </SettingsPager>
                <SettingsBehavior ColumnResizeMode="NextColumn" />
            </dx:ASPxGridView>
        
        </td>
    </tr>
</table>

<data:CompanySiteCategoryDataSource ID="CompanySiteCategoryDataSource" runat="server"
    SelectMethod="GetPaged" EnablePaging="True" EnableSorting="True">
</data:CompanySiteCategoryDataSource>