using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Services;
using KaiZen.CSMS.Data;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxCallbackPanel;
using DevExpress.Web.ASPxClasses;
using DevExpress.Web.Data;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxPopupControl;
using System.Drawing;

public partial class UserControls_SqQuestionnaire_QuestionnaireList_Completed : System.Web.UI.UserControl
{
    Auth auth = new Auth();
    protected void Page_Load(object sender, EventArgs e) {  Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); }

    private void moduleInit(bool postBack)
    {
        if (!postBack)
        {

            //assessment complete only
            //dsQuestionnaire.Filter = "Status = " + ((int)QuestionnaireStatusList.AssessmentComplete).ToString();
            //dsQuestionnaire_Latest.Filter = "Status = " + ((int)QuestionnaireStatusList.AssessmentComplete).ToString();

            switch (auth.RoleId)
            {
                case ((int)RoleList.Reader):
                    if (!Helper.General.isEhsConsultant(auth.UserId))
                    {
                        grid.Columns[" "].Visible = false;
                    }
                    break;
                case ((int)RoleList.Contractor):
                    //Contractor();
                    //Redirect lol.

                    break;
                case ((int)RoleList.PreQual):
                    //Contractor();
                    break;
                case ((int)RoleList.Administrator):

                    break;
                default:
                    // RoleList.Disabled or No Role
                    Response.Redirect(String.Format("AccessDenied.aspx?error={0}", Server.UrlEncode("Unrecognised User.")));
                    break;
            }

            UsersProcurementService upService = new UsersProcurementService();
            UsersProcurement up = upService.GetByUserId(auth.UserId);
            if (auth.RoleId != (int)RoleList.Administrator && up != null)
            //if (up != null)
            {
                if (up.Enabled == true)
                {
                    //QuestionnaireWithLocationApprovalViewFilters qwlavFilters = new QuestionnaireWithLocationApprovalViewFilters(false, false);
                    //qwlavFilters.AppendEquals(QuestionnaireWithLocationApprovalViewColumn.Status, ((int)QuestionnaireStatusList.AssessmentComplete).ToString());
                    //qwlavFilters.AppendEquals(QuestionnaireWithLocationApprovalViewColumn.ProcurementModified, "0");
                    //qwlavFilters.BeginGroup();
                    //qwlavFilters.AppendEquals(QuestionnaireWithLocationApprovalViewColumn.CreatedByUserId, auth.UserId.ToString());
                    //qwlavFilters.Junction = "OR";
                    //qwlavFilters.AppendEquals(QuestionnaireWithLocationApprovalViewColumn.ProcurementContactUserId, auth.UserId.ToString());
                    //qwlavFilters.Junction = "OR";
                    //qwlavFilters.AppendEquals(QuestionnaireWithLocationApprovalViewColumn.ContractManagerUserId, auth.UserId.ToString());
                    //qwlavFilters.EndGroup();

                    //dsQuestionnaire.Filter = "CreatedByUserId = 1360 OR ProcurementContactUserId = 1360 OR ContractManagerUserId = 1360";
                    //dsQuestionnaire_Latest.Filter = "CreatedByUserId = 1360 OR ProcurementContactUserId = 1360 OR ContractManagerUserId = 1360";
                    //dsQuestionnaire.Filter = dsQuestionnaire.Filter + " AND " + "(CreatedByUserId = " + auth.UserId.ToString() + " OR ProcurementContactUserId = " + auth.UserId.ToString() + " OR ContractManagerUserId = " + auth.UserId.ToString() + ")";
                    //dsQuestionnaire_Latest.Filter = dsQuestionnaire.Filter + " AND " + "(CreatedByUserId = " + auth.UserId.ToString() + " OR ProcurementContactUserId = " + auth.UserId.ToString() + " OR ContractManagerUserId = " + auth.UserId.ToString() + ")";
                        
                    grid.Columns["InitialStatus"].Visible = false;
                    grid.Columns["33.055.1 Risk Rating"].Visible = false;
                    grid.Columns["Status"].Visible = true;
                    grid.Columns["Recommended"].Visible = false;
                    grid.Columns["KWI"].Visible = false;
                    grid.Columns["PIN"].Visible = false;
                    grid.Columns["WGP"].Visible = false;
                    grid.Columns["HUN"].Visible = false;
                    grid.Columns["WDL"].Visible = false;
                    grid.Columns["BUN"].Visible = false;
                    grid.Columns["FML"].Visible = false;
                    grid.Columns["BGN"].Visible = false;
                    grid.Columns["ANG"].Visible = false;
                    grid.Columns["PTL"].Visible = false;
                    grid.Columns["PTH"].Visible = false;
                    grid.Columns["PEEL"].Visible = false;
                    grid.Columns["ARP"].Visible = false;
                    grid.Columns["YEN"].Visible = false;
                    grid.Columns["Procurement Risk Rating"].Visible = false;
                    grid.Width = 880;

                    grid.Columns["Company"].Width = 205;
                    grid.Columns["Last Modified At"].Width = 150;
                    
                    grid.Columns[" "].Visible = true;
                    //grid.FilterExpression = "ProcurementModified = 1"; // AND (InitialStatus = 1 OR Status = 3)

                    grid.Columns["FinalRiskRating"].Visible = false;
                    grid.Columns["Procurement Contact"].VisibleIndex = 2;
                    grid.Columns["Company"].Width = Unit.Pixel(190);

                }
                else
                {
                    grid.Columns["Procurement Contact"].Visible = true;
                    grid.Columns["Procurement Functional Supervisor"].Visible = true;
                }
            }
            else
            {
                grid.Columns["Procurement Contact"].Visible = true;
                grid.Columns["Procurement Functional Supervisor"].Visible = true;
            }

            // Export
            String exportFileName = @"ALCOA CSMS - Australian Safety Qualification Process - Completed"; //Chrome & IE is fine.
            String userAgent = Request.Headers.Get("User-Agent");
            if (
                (userAgent.Contains("Mozilla") && !userAgent.Contains("AppleWebKit") && !userAgent.Contains("MSIE")) || //Mozilla
                (userAgent.Contains("AppleWebKit") && !userAgent.Contains("Chrome"))      //Safari
                )
            {
                exportFileName = exportFileName.Replace(" ", "_");
            }
            Helper.ExportGrid.Settings(ucExportButtons, exportFileName, true);
            
            cbShowAll.Checked = false;
            setDb();
        }
        else
        {
            grid.DataSource = (DataTable)Session["dtQuestionnaireAssessmentComplete"];
            grid.DataBind();
        }
    }

    protected void cbShowAll_CheckedChanged(object sender, EventArgs e)
    {
        setDb();
    }

    public void setDb()
    {
        bool proc = false;
        UsersProcurementService upService = new UsersProcurementService();
        UsersProcurement up = upService.GetByUserId(auth.UserId);
        if (auth.RoleId != (int)RoleList.Administrator && up != null)
        //if (up != null)
        {
            if (up.Enabled == true)
            {
                proc = true;
            }
        }

        if (cbShowAll.Checked == false)
        {
            QuestionnaireWithLocationApprovalViewLatestService qs = new QuestionnaireWithLocationApprovalViewLatestService();
            DataSet ds = qs.AssessmentComplete_ByUserId((int)QuestionnaireStatusList.AssessmentComplete, auth.UserId, proc);
            Session["dtQuestionnaireAssessmentComplete"] = ds.Tables[0];
        }
        else
        {
            QuestionnaireWithLocationApprovalViewService qs = new QuestionnaireWithLocationApprovalViewService();
            DataSet ds = qs.AssessmentComplete_ByUserId((int)QuestionnaireStatusList.AssessmentComplete, auth.UserId, proc);
            Session["dtQuestionnaireAssessmentComplete"] = ds.Tables[0];
        }
        grid.DataSource = (DataTable)Session["dtQuestionnaireAssessmentComplete"];
        grid.DataBind();
    }

    protected void approvalText(Label l, string i)
    {
        if (l != null)
        {
            l.Font.Bold = true;
            switch (i)
            {
                case "-":
                    l.Text = "";
                    break;
                case "":
                    l.Text = "?";
                    l.ForeColor = Color.Black;
                    break;
                case "0":
                    l.Text = "No";
                    l.ForeColor = Color.Red;
                    break;
                case "1":
                    l.Text = "Yes";
                    l.ForeColor = Color.Green;
                    break;
                default:
                    l.Text = "";
                    l.ForeColor = Color.Black;
                    break;
            }
        }
    }

    protected void grid_RowCreated(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewTableRowEventArgs e)
    {
        if (e.RowType != DevExpress.Web.ASPxGridView.GridViewRowType.Data) return;

        Label lblKWI = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "lblKWI") as Label;
        Label lblPIN = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "lblPIN") as Label;
        Label lblWGP = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "lblWGP") as Label;
        Label lblHUN = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "lblHUN") as Label;
        Label lblWDL = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "lblWDL") as Label;
        Label lblBUN = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "lblBUN") as Label;
        Label lblFML = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "lblFML") as Label;
        Label lblBGN = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "lblBGN") as Label;
        Label lblANG = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "lblANG") as Label;
        Label lblPTL = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "lblPTL") as Label;
        Label lblPTH = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "lblPTH") as Label;
        Label lblPEEL = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "lblPEEL") as Label;
        Label lblARP = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "lblARP") as Label;
        Label lblYEN = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "lblYEN") as Label;

        string approval = "-";

        if (grid.GetRowValues(e.VisibleIndex, "KWI") != DBNull.Value)
        {
            approval = (string)grid.GetRowValues(e.VisibleIndex, "KWI");
        }
        else
        {
            approval = "";
        }
        if (approval != null) approvalText(lblKWI, approval);

        if (grid.GetRowValues(e.VisibleIndex, "PIN") != DBNull.Value)
        {
            approval = (string)grid.GetRowValues(e.VisibleIndex, "PIN");
        }
        else
        {
            approval = "";
        }
        if (approval != null) approvalText(lblPIN, approval);

        if (grid.GetRowValues(e.VisibleIndex, "WGP") != DBNull.Value)
        {
            approval = (string)grid.GetRowValues(e.VisibleIndex, "WGP");
        }
        else
        {
            approval = "";
        }
        if (approval != null) approvalText(lblWGP, approval);

        if (grid.GetRowValues(e.VisibleIndex, "HUN") != DBNull.Value)
        {
            approval = (string)grid.GetRowValues(e.VisibleIndex, "HUN");
        }
        else
        {
            approval = "";
        }
        if (approval != null) approvalText(lblHUN, approval);

        if (grid.GetRowValues(e.VisibleIndex, "WDL") != DBNull.Value)
        {
            approval = (string)grid.GetRowValues(e.VisibleIndex, "WDL");
        }
        else
        {
            approval = "";
        }
        if (approval != null) approvalText(lblWDL, approval);

        if (grid.GetRowValues(e.VisibleIndex, "BUN") != DBNull.Value)
        {
            approval = (string)grid.GetRowValues(e.VisibleIndex, "BUN");
        }
        else
        {
            approval = "";
        }
        if (approval != null) approvalText(lblBUN, approval);

        if (grid.GetRowValues(e.VisibleIndex, "FML") != DBNull.Value)
        {
            approval = (string)grid.GetRowValues(e.VisibleIndex, "FML");
        }
        else
        {
            approval = "";
        }
        if (approval != null) approvalText(lblFML, approval);

        if (grid.GetRowValues(e.VisibleIndex, "BGN") != DBNull.Value)
        {
            approval = (string)grid.GetRowValues(e.VisibleIndex, "BGN");
        }
        else
        {
            approval = "";
        }
        if (approval != null) approvalText(lblBGN, approval);

        if (grid.GetRowValues(e.VisibleIndex, "ANG") != DBNull.Value)
        {
            approval = (string)grid.GetRowValues(e.VisibleIndex, "ANG");
        }
        else
        {
            approval = "";
        }
        if (approval != null) approvalText(lblANG, approval);

        if (grid.GetRowValues(e.VisibleIndex, "PTL") != DBNull.Value)
        {
            approval = (string)grid.GetRowValues(e.VisibleIndex, "PTL");
        }
        else
        {
            approval = "";
        }
        if (approval != null) approvalText(lblPTL, approval);

        if (grid.GetRowValues(e.VisibleIndex, "PTH") != DBNull.Value)
        {
            approval = (string)grid.GetRowValues(e.VisibleIndex, "PTH");
        }
        else
        {
            approval = "";
        }
        if (approval != null) approvalText(lblPTH, approval);

        if (grid.GetRowValues(e.VisibleIndex, "PEL") != DBNull.Value)
        {
            approval = (string)grid.GetRowValues(e.VisibleIndex, "PEL");
        }
        else
        {
            approval = "";
        }
        if (approval != null) approvalText(lblPEEL, approval);

        if (grid.GetRowValues(e.VisibleIndex, "ARP") != DBNull.Value)
        {
            approval = (string)grid.GetRowValues(e.VisibleIndex, "ARP");
        }
        else
        {
            approval = "";
        }
        if (approval != null) approvalText(lblARP, approval);

        if (grid.GetRowValues(e.VisibleIndex, "YEN") != DBNull.Value)
        {
            approval = (string)grid.GetRowValues(e.VisibleIndex, "YEN");
        }
        else
        {
            approval = "";
        }
        if (approval != null) approvalText(lblYEN, approval);


        HyperLink hlAssess = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "hlAssess") as HyperLink;
        if (hlAssess != null)
        {
            int QuestionnaireId = (int)grid.GetRowValues(e.VisibleIndex, "QuestionnaireId");
            hlAssess.NavigateUrl = String.Format("../../../SafetyPQ_QuestionnaireModify.aspx{0}",
                                    QueryStringModule.Encrypt("q=" + QuestionnaireId.ToString()));

        }

        string mainAssessmentStatus = "";

        HyperLink hl = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "RiskLevel") as HyperLink;
        int? score = null;
        if (grid.GetRowValues(e.VisibleIndex, "MainScorePOverall") != DBNull.Value) score = (int)grid.GetRowValues(e.VisibleIndex, "MainScorePOverall");

        int id = (int)grid.GetRowValues(e.VisibleIndex, "QuestionnaireId");

        if (hl != null)
        {
            if (score != null)
            {
                hl.Text = (string)grid.GetRowValues(e.VisibleIndex, "MainAssessmentRiskRating");
                mainAssessmentStatus = hl.Text;
                //hl.NavigateUrl = String.Format("javascript:popUp('./PopUps/SafetyPQ_QuestionnaireReport.aspx?id={0}');", id);
                hl.NavigateUrl = String.Format("javascript:popUp('./PopUps/SafetyPQ_QuestionnaireReport.aspx{0}');",
                                                QueryStringModule.Encrypt("id=" + id));
            }
            else
            {
                hl.Text = "";
            }
        }

        bool isMainRequired = false;
        if (grid.GetRowValues(e.VisibleIndex, "IsMainRequired") != DBNull.Value)
        {
            if ((bool)grid.GetRowValues(e.VisibleIndex, "IsMainRequired") == true)
            {
                isMainRequired = true;
            }
        }

        if (isMainRequired)
        {
            if (String.IsNullOrEmpty(mainAssessmentStatus))
            {
                if (grid.GetRowValues(e.VisibleIndex, "MainAssessmentRiskRating") != DBNull.Value)
                {
                    mainAssessmentStatus = (string)grid.GetRowValues(e.VisibleIndex, "MainAssessmentRiskRating");
                }
            }
        }

        if (!String.IsNullOrEmpty(mainAssessmentStatus))
        {
            Label lblFinalRiskRating = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "FinalRiskLevel") as Label;
            if (lblFinalRiskRating != null) //procurement users have htis column hidden..
            {
                lblFinalRiskRating.Text = "Incomplete"; //default value.

                int status = (int)grid.GetRowValues(e.VisibleIndex, "Status");
                if (status == (int)QuestionnaireStatusList.AssessmentComplete)
                {
                    string ProcurementRiskRating = (string)grid.GetRowValues(e.VisibleIndex, "InitialRiskAssessment");
                    string SupplierRiskRating = mainAssessmentStatus;
                    if (!String.IsNullOrEmpty(ProcurementRiskRating))
                    {
                        lblFinalRiskRating.Text = ProcurementRiskRating;
                    }
                    if (!String.IsNullOrEmpty(SupplierRiskRating))
                    {
                        int _ProcurementRiskRating = Helper.Questionnaire.getRiskLevel(ProcurementRiskRating);
                        int _SupplierRiskRating = Helper.Questionnaire.getRiskLevel(SupplierRiskRating);

                        if (_SupplierRiskRating > _ProcurementRiskRating)
                        {
                            lblFinalRiskRating.Text = SupplierRiskRating;
                        }
                    }
                }
            }
        }
    }
}
