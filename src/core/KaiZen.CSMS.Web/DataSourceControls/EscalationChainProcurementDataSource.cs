﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.EscalationChainProcurementProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(EscalationChainProcurementDataSourceDesigner))]
	public class EscalationChainProcurementDataSource : ProviderDataSource<EscalationChainProcurement, EscalationChainProcurementKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EscalationChainProcurementDataSource class.
		/// </summary>
		public EscalationChainProcurementDataSource() : base(new EscalationChainProcurementService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the EscalationChainProcurementDataSourceView used by the EscalationChainProcurementDataSource.
		/// </summary>
		protected EscalationChainProcurementDataSourceView EscalationChainProcurementView
		{
			get { return ( View as EscalationChainProcurementDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the EscalationChainProcurementDataSource control invokes to retrieve data.
		/// </summary>
		public EscalationChainProcurementSelectMethod SelectMethod
		{
			get
			{
				EscalationChainProcurementSelectMethod selectMethod = EscalationChainProcurementSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (EscalationChainProcurementSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the EscalationChainProcurementDataSourceView class that is to be
		/// used by the EscalationChainProcurementDataSource.
		/// </summary>
		/// <returns>An instance of the EscalationChainProcurementDataSourceView class.</returns>
		protected override BaseDataSourceView<EscalationChainProcurement, EscalationChainProcurementKey> GetNewDataSourceView()
		{
			return new EscalationChainProcurementDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the EscalationChainProcurementDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class EscalationChainProcurementDataSourceView : ProviderDataSourceView<EscalationChainProcurement, EscalationChainProcurementKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EscalationChainProcurementDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the EscalationChainProcurementDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public EscalationChainProcurementDataSourceView(EscalationChainProcurementDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal EscalationChainProcurementDataSource EscalationChainProcurementOwner
		{
			get { return Owner as EscalationChainProcurementDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal EscalationChainProcurementSelectMethod SelectMethod
		{
			get { return EscalationChainProcurementOwner.SelectMethod; }
			set { EscalationChainProcurementOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal EscalationChainProcurementService EscalationChainProcurementProvider
		{
			get { return Provider as EscalationChainProcurementService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<EscalationChainProcurement> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<EscalationChainProcurement> results = null;
			EscalationChainProcurement item;
			count = 0;
			
			System.Int32 _escalationChainProcurementId;
			System.Int32 _siteId;
			System.Int32 _level;
			System.Int32 _userId;

			switch ( SelectMethod )
			{
				case EscalationChainProcurementSelectMethod.Get:
					EscalationChainProcurementKey entityKey  = new EscalationChainProcurementKey();
					entityKey.Load(values);
					item = EscalationChainProcurementProvider.Get(entityKey);
					results = new TList<EscalationChainProcurement>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case EscalationChainProcurementSelectMethod.GetAll:
                    results = EscalationChainProcurementProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case EscalationChainProcurementSelectMethod.GetPaged:
					results = EscalationChainProcurementProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case EscalationChainProcurementSelectMethod.Find:
					if ( FilterParameters != null )
						results = EscalationChainProcurementProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = EscalationChainProcurementProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case EscalationChainProcurementSelectMethod.GetByEscalationChainProcurementId:
					_escalationChainProcurementId = ( values["EscalationChainProcurementId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["EscalationChainProcurementId"], typeof(System.Int32)) : (int)0;
					item = EscalationChainProcurementProvider.GetByEscalationChainProcurementId(_escalationChainProcurementId);
					results = new TList<EscalationChainProcurement>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				case EscalationChainProcurementSelectMethod.GetBySiteId:
					_siteId = ( values["SiteId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["SiteId"], typeof(System.Int32)) : (int)0;
					results = EscalationChainProcurementProvider.GetBySiteId(_siteId, this.StartIndex, this.PageSize, out count);
					break;
				case EscalationChainProcurementSelectMethod.GetBySiteIdLevel:
					_siteId = ( values["SiteId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["SiteId"], typeof(System.Int32)) : (int)0;
					_level = ( values["Level"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["Level"], typeof(System.Int32)) : (int)0;
					item = EscalationChainProcurementProvider.GetBySiteIdLevel(_siteId, _level);
					results = new TList<EscalationChainProcurement>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// FK
				case EscalationChainProcurementSelectMethod.GetByLevel:
					_level = ( values["Level"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["Level"], typeof(System.Int32)) : (int)0;
					results = EscalationChainProcurementProvider.GetByLevel(_level, this.StartIndex, this.PageSize, out count);
					break;
				case EscalationChainProcurementSelectMethod.GetByUserId:
					_userId = ( values["UserId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["UserId"], typeof(System.Int32)) : (int)0;
					results = EscalationChainProcurementProvider.GetByUserId(_userId, this.StartIndex, this.PageSize, out count);
					break;
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == EscalationChainProcurementSelectMethod.Get || SelectMethod == EscalationChainProcurementSelectMethod.GetByEscalationChainProcurementId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				EscalationChainProcurement entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					EscalationChainProcurementProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<EscalationChainProcurement> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			EscalationChainProcurementProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region EscalationChainProcurementDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the EscalationChainProcurementDataSource class.
	/// </summary>
	public class EscalationChainProcurementDataSourceDesigner : ProviderDataSourceDesigner<EscalationChainProcurement, EscalationChainProcurementKey>
	{
		/// <summary>
		/// Initializes a new instance of the EscalationChainProcurementDataSourceDesigner class.
		/// </summary>
		public EscalationChainProcurementDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public EscalationChainProcurementSelectMethod SelectMethod
		{
			get { return ((EscalationChainProcurementDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new EscalationChainProcurementDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region EscalationChainProcurementDataSourceActionList

	/// <summary>
	/// Supports the EscalationChainProcurementDataSourceDesigner class.
	/// </summary>
	internal class EscalationChainProcurementDataSourceActionList : DesignerActionList
	{
		private EscalationChainProcurementDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the EscalationChainProcurementDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public EscalationChainProcurementDataSourceActionList(EscalationChainProcurementDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public EscalationChainProcurementSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion EscalationChainProcurementDataSourceActionList
	
	#endregion EscalationChainProcurementDataSourceDesigner
	
	#region EscalationChainProcurementSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the EscalationChainProcurementDataSource.SelectMethod property.
	/// </summary>
	public enum EscalationChainProcurementSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByEscalationChainProcurementId method.
		/// </summary>
		GetByEscalationChainProcurementId,
		/// <summary>
		/// Represents the GetBySiteId method.
		/// </summary>
		GetBySiteId,
		/// <summary>
		/// Represents the GetBySiteIdLevel method.
		/// </summary>
		GetBySiteIdLevel,
		/// <summary>
		/// Represents the GetByLevel method.
		/// </summary>
		GetByLevel,
		/// <summary>
		/// Represents the GetByUserId method.
		/// </summary>
		GetByUserId
	}
	
	#endregion EscalationChainProcurementSelectMethod

	#region EscalationChainProcurementFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EscalationChainProcurement"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EscalationChainProcurementFilter : SqlFilter<EscalationChainProcurementColumn>
	{
	}
	
	#endregion EscalationChainProcurementFilter

	#region EscalationChainProcurementExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EscalationChainProcurement"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EscalationChainProcurementExpressionBuilder : SqlExpressionBuilder<EscalationChainProcurementColumn>
	{
	}
	
	#endregion EscalationChainProcurementExpressionBuilder	

	#region EscalationChainProcurementProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;EscalationChainProcurementChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="EscalationChainProcurement"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EscalationChainProcurementProperty : ChildEntityProperty<EscalationChainProcurementChildEntityTypes>
	{
	}
	
	#endregion EscalationChainProcurementProperty
}

