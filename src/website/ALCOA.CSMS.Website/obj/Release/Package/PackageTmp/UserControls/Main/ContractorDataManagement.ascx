﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="ALCOA.CSMS.Website.UserControls.Main.ContractorDataManagement" Codebehind="ContractorDataManagement.ascx.cs" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dxpc" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Src="../Other/ExportButtons.ascx" TagName="ExportButtons" TagPrefix="uc1" %>

<table border="0" cellpadding="2" cellspacing="0" style="width: 100%">
    <thead>
        <tr>
            <th width="33%"></th>
            <th width="33%"></th>
            <th width="33%"></th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td colspan="2">
                <span class="bodycopy"><span class="title">Contractor Person Maintenance</span></span><br>
                <span><a>Contact Details</a></span><span> &gt; </span>
                <span><a href="/ContractorDataManagement.aspx" style="font-weight:bold;">Contractor Persons</a></span><br>
                <img height="1" src="images/grfc_dottedline.gif" width="24">&nbsp;<br>
            </td>
            <td align="right">
                <asp:Button ID="AddNewButton" runat="server" Text="Add New" PostBackUrl="~/ContractorDataManagementDetail.aspx?CWK_NBR=new" />
            </td>
        </tr>
        <tr>
            <td colspan="3" style="text-align: left;" v-align="top">
                <dxwgv:ASPxGridView ID="grid" 
                    runat="server" 
                    AutoGenerateColumns="False" 
                    ClientInstanceName="grid"
                    CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
                    CssPostfix="Office2003Blue" 
                    DataSourceID="ContractorDataSource"
                    KeyFieldName="UserId" 
                    OnInitNewRow="grid_InitNewRow" 
                    Width="900px">
                    <Columns>
                        <dxwgv:GridViewDataTextColumn 
                            Caption="Contractor Number" 
                            FieldName="CWK_NBR" 
                            ReadOnly="True"  
                            VisibleIndex="1"
                            Width="150px">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn 
                            Caption="Full Name (Last, First)" 
                            FieldName="FULL_NAME" 
                            SortIndex="0" 
                            SortOrder="Ascending" 
                            VisibleIndex="2">
                            <Settings AutoFilterCondition="Contains" />
                            <CellStyle HorizontalAlign="Left"></CellStyle>
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn 
                            Caption="Company" 
                            FieldName="CWK_VENDOR_NAME" 
                            VisibleIndex="3">
                            <Settings AutoFilterCondition="Contains" />
                            <CellStyle HorizontalAlign="Left"></CellStyle>
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataDateColumn 
                            Caption="Start Date" 
                            FieldName="EFFECTIVE_START_DATE" 
                            VisibleIndex="4" 
                            Width="100px">
                            <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy"></PropertiesDateEdit> 
                            <Settings AutoFilterCondition="Contains" />
                            <CellStyle HorizontalAlign="Left"></CellStyle>
                        </dxwgv:GridViewDataDateColumn>
                        <dxwgv:GridViewDataTextColumn
                            Caption="" 
                            VisibleIndex="5"
                            Width="50px" 
                            CellStyle-HorizontalAlign="Center">
                            <DataItemTemplate>
                                <a href="ContractorDataManagementDetail.aspx?CWK_NBR=<%# Eval("CWK_NBR") %>&EFFECTIVE_START_DATE=<%# Convert.ToDateTime(Eval("EFFECTIVE_START_DATE")).ToString("yyyy-MM-dd") %>">Edit</a>
                            </DataItemTemplate>
                        </dxwgv:GridViewDataTextColumn>
                    </Columns>
                    <SettingsBehavior ColumnResizeMode="Control" ConfirmDelete="True" EnableRowHotTrack="true" />
                    <SettingsPager>
                        <AllButton Visible="True">
                        </AllButton>
                    </SettingsPager>
                    <Settings ShowFilterRow="True" ShowGroupPanel="True" ShowHeaderFilterButton="True" />
                    <SettingsCustomizationWindow Enabled="True" />
                    <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                        <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
                        </LoadingPanelOnStatusBar>
                        <CollapsedButton Height="12px" Width="11px">
                        </CollapsedButton>
                        <ExpandedButton Height="12px" Width="11px">
                        </ExpandedButton>
                        <DetailCollapsedButton Height="12px" Width="11px">
                        </DetailCollapsedButton>
                        <DetailExpandedButton Height="12px" Width="11px">
                        </DetailExpandedButton>
                        <LoadingPanel Url="~/App_Themes/Office2003BlueOld/GridView/Loading.gif"><%--Enhancement_023:change by debashis for testing platform upgradation--%>
                        </LoadingPanel>
                    </Images>
                    <ImagesFilterControl>
                        <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
                        </LoadingPanel>
                    </ImagesFilterControl>
                    <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue">
                        <Header ImageSpacing="5px" SortingImageSpacing="5px"></Header>
                        <LoadingPanel ImageSpacing="10px"></LoadingPanel>
                        <RowHotTrack BackColor="Gold"></RowHotTrack>
                    </Styles>
                </dxwgv:ASPxGridView>
            </td>
        </tr>
        <tr>
            <td colspan="2"style="height: 10px;" valign="middle">
                <uc1:ExportButtons ID="ucExportButtons" runat="server" />
            </td>
            <td style="width: 250px; height: 40px" align="right">
                Number of Rows to show per page:<asp:DropDownList ID="PageSizeDropDownList" runat="server"
                    AutoPostBack="True" OnSelectedIndexChanged="PageSizeDropDownList_SelectedIndexChanged">
                    <asp:ListItem>10</asp:ListItem>
                    <asp:ListItem>20</asp:ListItem>
                    <asp:ListItem>50</asp:ListItem>
                    <asp:ListItem>All</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr runat="server" id="trExport">
            <td colspan="3">
                <hr />
                <table cellpadding="0 cellspacing="0">
                    <tbody>
                        <tr>
                            <td valign="middle"><strong>Export (year):</strong></td>
                            <td valign="middle">
                                <asp:DropDownList ID="ExportYearDropDownList" runat="server"></asp:DropDownList>
                            </td>
                            <td valign="middle" style="padding-right:5px;"><asp:ImageButton id="imgBtnExcelAll" onclick="imgBtnExcelAll_Click" runat="server" ToolTip="Export database to Microsoft Excel (XLS)" ImageUrl="~/Images/ExportLogos/excel.gif"></asp:ImageButton></td>
                            <td valign="middle"><small>(Export all contractor and training data for selected year.  Nb. could take a while.)</small></td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr runat="server" id="trImport">
            <td colspan="3">
                <table cellpadding="0 cellspacing="0">
                    <tbody>
                        <tr>
                            <td valign="middle"><strong>Import training data:</strong></td>
                            <td valign="middle">
                                <asp:FileUpload ID="ContractorMaintenanceFileUpload" runat="server"  />
                            </td>
                            <td valign="middle" style="padding-right:5px;">
                                <asp:Button ID="imgBtnImport" onclick="imgBtnImport_Click" runat="server" Text="Upload" ToolTip="Import Microsoft Excel (XLS) training data to database"></asp:Button>
                            </td>
                            <td valign="middle"><small>(Here is the Excel spreadsheet <a href="~/Templates/csms_training_data_template.xlsx" runat="server">template</a>.)</small></td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table>

<dxpc:ASPxPopupControl ID="ASPxPopupControl1" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
    CssPostfix="Office2003Blue" EnableHotTrack="False" HeaderText="Warning" Height="111px"
    Modal="True" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter"
    Width="439px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" EncodeHtml="false">
    <SizeGripImage Height="16px" Width="16px">
    </SizeGripImage>
    <HeaderStyle>
        <Paddings PaddingRight="6px"></Paddings>
    </HeaderStyle>
    <CloseButtonImage Height="12px" Width="13px">
    </CloseButtonImage>
    <ContentCollection>
        <dxpc:PopupControlContentControl runat="server" SupportsDisabledAttribute="True">
            <dxe:ASPxLabel runat="server" Height="30px" Font-Size="14px" ID="ASPxLabel9">
                <Border BorderColor="White" BorderWidth="10px"></Border>
            </dxe:ASPxLabel>
        </dxpc:PopupControlContentControl>
    </ContentCollection>
</dxpc:ASPxPopupControl>

<asp:SqlDataSource  
    ID="ContractorDataSource" 
    runat="server" 
    ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="[HR].[XXHR_CWK_History_Select]" 
    SelectCommandType="StoredProcedure">
</asp:SqlDataSource>
