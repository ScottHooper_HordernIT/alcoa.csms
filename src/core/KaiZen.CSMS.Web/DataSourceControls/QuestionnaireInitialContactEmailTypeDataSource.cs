﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.QuestionnaireInitialContactEmailTypeProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(QuestionnaireInitialContactEmailTypeDataSourceDesigner))]
	public class QuestionnaireInitialContactEmailTypeDataSource : ProviderDataSource<QuestionnaireInitialContactEmailType, QuestionnaireInitialContactEmailTypeKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialContactEmailTypeDataSource class.
		/// </summary>
		public QuestionnaireInitialContactEmailTypeDataSource() : base(new QuestionnaireInitialContactEmailTypeService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the QuestionnaireInitialContactEmailTypeDataSourceView used by the QuestionnaireInitialContactEmailTypeDataSource.
		/// </summary>
		protected QuestionnaireInitialContactEmailTypeDataSourceView QuestionnaireInitialContactEmailTypeView
		{
			get { return ( View as QuestionnaireInitialContactEmailTypeDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the QuestionnaireInitialContactEmailTypeDataSource control invokes to retrieve data.
		/// </summary>
		public QuestionnaireInitialContactEmailTypeSelectMethod SelectMethod
		{
			get
			{
				QuestionnaireInitialContactEmailTypeSelectMethod selectMethod = QuestionnaireInitialContactEmailTypeSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (QuestionnaireInitialContactEmailTypeSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the QuestionnaireInitialContactEmailTypeDataSourceView class that is to be
		/// used by the QuestionnaireInitialContactEmailTypeDataSource.
		/// </summary>
		/// <returns>An instance of the QuestionnaireInitialContactEmailTypeDataSourceView class.</returns>
		protected override BaseDataSourceView<QuestionnaireInitialContactEmailType, QuestionnaireInitialContactEmailTypeKey> GetNewDataSourceView()
		{
			return new QuestionnaireInitialContactEmailTypeDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the QuestionnaireInitialContactEmailTypeDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class QuestionnaireInitialContactEmailTypeDataSourceView : ProviderDataSourceView<QuestionnaireInitialContactEmailType, QuestionnaireInitialContactEmailTypeKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialContactEmailTypeDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the QuestionnaireInitialContactEmailTypeDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public QuestionnaireInitialContactEmailTypeDataSourceView(QuestionnaireInitialContactEmailTypeDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal QuestionnaireInitialContactEmailTypeDataSource QuestionnaireInitialContactEmailTypeOwner
		{
			get { return Owner as QuestionnaireInitialContactEmailTypeDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal QuestionnaireInitialContactEmailTypeSelectMethod SelectMethod
		{
			get { return QuestionnaireInitialContactEmailTypeOwner.SelectMethod; }
			set { QuestionnaireInitialContactEmailTypeOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal QuestionnaireInitialContactEmailTypeService QuestionnaireInitialContactEmailTypeProvider
		{
			get { return Provider as QuestionnaireInitialContactEmailTypeService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<QuestionnaireInitialContactEmailType> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<QuestionnaireInitialContactEmailType> results = null;
			QuestionnaireInitialContactEmailType item;
			count = 0;
			
			System.Int32 _questionnaireInitialContactEmailTypeId;
			System.String _typeName;

			switch ( SelectMethod )
			{
				case QuestionnaireInitialContactEmailTypeSelectMethod.Get:
					QuestionnaireInitialContactEmailTypeKey entityKey  = new QuestionnaireInitialContactEmailTypeKey();
					entityKey.Load(values);
					item = QuestionnaireInitialContactEmailTypeProvider.Get(entityKey);
					results = new TList<QuestionnaireInitialContactEmailType>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case QuestionnaireInitialContactEmailTypeSelectMethod.GetAll:
                    results = QuestionnaireInitialContactEmailTypeProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case QuestionnaireInitialContactEmailTypeSelectMethod.GetPaged:
					results = QuestionnaireInitialContactEmailTypeProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case QuestionnaireInitialContactEmailTypeSelectMethod.Find:
					if ( FilterParameters != null )
						results = QuestionnaireInitialContactEmailTypeProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = QuestionnaireInitialContactEmailTypeProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case QuestionnaireInitialContactEmailTypeSelectMethod.GetByQuestionnaireInitialContactEmailTypeId:
					_questionnaireInitialContactEmailTypeId = ( values["QuestionnaireInitialContactEmailTypeId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["QuestionnaireInitialContactEmailTypeId"], typeof(System.Int32)) : (int)0;
					item = QuestionnaireInitialContactEmailTypeProvider.GetByQuestionnaireInitialContactEmailTypeId(_questionnaireInitialContactEmailTypeId);
					results = new TList<QuestionnaireInitialContactEmailType>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				case QuestionnaireInitialContactEmailTypeSelectMethod.GetByTypeName:
					_typeName = ( values["TypeName"] != null ) ? (System.String) EntityUtil.ChangeType(values["TypeName"], typeof(System.String)) : string.Empty;
					item = QuestionnaireInitialContactEmailTypeProvider.GetByTypeName(_typeName);
					results = new TList<QuestionnaireInitialContactEmailType>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// FK
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == QuestionnaireInitialContactEmailTypeSelectMethod.Get || SelectMethod == QuestionnaireInitialContactEmailTypeSelectMethod.GetByQuestionnaireInitialContactEmailTypeId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				QuestionnaireInitialContactEmailType entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					QuestionnaireInitialContactEmailTypeProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<QuestionnaireInitialContactEmailType> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			QuestionnaireInitialContactEmailTypeProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region QuestionnaireInitialContactEmailTypeDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the QuestionnaireInitialContactEmailTypeDataSource class.
	/// </summary>
	public class QuestionnaireInitialContactEmailTypeDataSourceDesigner : ProviderDataSourceDesigner<QuestionnaireInitialContactEmailType, QuestionnaireInitialContactEmailTypeKey>
	{
		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialContactEmailTypeDataSourceDesigner class.
		/// </summary>
		public QuestionnaireInitialContactEmailTypeDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public QuestionnaireInitialContactEmailTypeSelectMethod SelectMethod
		{
			get { return ((QuestionnaireInitialContactEmailTypeDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new QuestionnaireInitialContactEmailTypeDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region QuestionnaireInitialContactEmailTypeDataSourceActionList

	/// <summary>
	/// Supports the QuestionnaireInitialContactEmailTypeDataSourceDesigner class.
	/// </summary>
	internal class QuestionnaireInitialContactEmailTypeDataSourceActionList : DesignerActionList
	{
		private QuestionnaireInitialContactEmailTypeDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialContactEmailTypeDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public QuestionnaireInitialContactEmailTypeDataSourceActionList(QuestionnaireInitialContactEmailTypeDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public QuestionnaireInitialContactEmailTypeSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion QuestionnaireInitialContactEmailTypeDataSourceActionList
	
	#endregion QuestionnaireInitialContactEmailTypeDataSourceDesigner
	
	#region QuestionnaireInitialContactEmailTypeSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the QuestionnaireInitialContactEmailTypeDataSource.SelectMethod property.
	/// </summary>
	public enum QuestionnaireInitialContactEmailTypeSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByQuestionnaireInitialContactEmailTypeId method.
		/// </summary>
		GetByQuestionnaireInitialContactEmailTypeId,
		/// <summary>
		/// Represents the GetByTypeName method.
		/// </summary>
		GetByTypeName
	}
	
	#endregion QuestionnaireInitialContactEmailTypeSelectMethod

	#region QuestionnaireInitialContactEmailTypeFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireInitialContactEmailType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireInitialContactEmailTypeFilter : SqlFilter<QuestionnaireInitialContactEmailTypeColumn>
	{
	}
	
	#endregion QuestionnaireInitialContactEmailTypeFilter

	#region QuestionnaireInitialContactEmailTypeExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireInitialContactEmailType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireInitialContactEmailTypeExpressionBuilder : SqlExpressionBuilder<QuestionnaireInitialContactEmailTypeColumn>
	{
	}
	
	#endregion QuestionnaireInitialContactEmailTypeExpressionBuilder	

	#region QuestionnaireInitialContactEmailTypeProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;QuestionnaireInitialContactEmailTypeChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireInitialContactEmailType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireInitialContactEmailTypeProperty : ChildEntityProperty<QuestionnaireInitialContactEmailTypeChildEntityTypes>
	{
	}
	
	#endregion QuestionnaireInitialContactEmailTypeProperty
}

