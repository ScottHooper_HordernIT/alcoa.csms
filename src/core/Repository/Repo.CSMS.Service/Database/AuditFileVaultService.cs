﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface IAuditFileVaultService
    {
        AuditFileVault Get(Expression<Func<AuditFileVault, bool>> where, List<Expression<Func<AuditFileVault, object>>> includes);
        List<AuditFileVault> GetMany(int? top, Expression<Func<AuditFileVault, bool>> where, Func<IQueryable<AuditFileVault>, IOrderedQueryable<AuditFileVault>> orderBy, List<Expression<Func<AuditFileVault, object>>> includes);
        List<AuditFileVault> GetManyPaged(int pageNumber, int pageSize, Expression<Func<AuditFileVault, bool>> where, Func<IQueryable<AuditFileVault>, IOrderedQueryable<AuditFileVault>> orderBy, List<Expression<Func<AuditFileVault, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<AuditFileVault, bool>> where, List<Expression<Func<AuditFileVault, object>>> includes);
        void Insert(AuditFileVault item, bool saveChanges = true);
        void Update(AuditFileVault item, bool saveChanges = true);
        void Delete(AuditFileVault item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class AuditFileVaultService : IAuditFileVaultService
    {
        private readonly IRepository<AuditFileVault> _repository;

        public AuditFileVaultService(IRepository<AuditFileVault> repository)
        {
            this._repository = repository;
        }

        public AuditFileVault Get(Expression<Func<AuditFileVault, bool>> where, List<Expression<Func<AuditFileVault, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<AuditFileVault> GetMany(int? top, Expression<Func<AuditFileVault, bool>> where, Func<IQueryable<AuditFileVault>, IOrderedQueryable<AuditFileVault>> orderBy, List<Expression<Func<AuditFileVault, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<AuditFileVault> GetManyPaged(int pageNumber, int pageSize, Expression<Func<AuditFileVault, bool>> where, Func<IQueryable<AuditFileVault>, IOrderedQueryable<AuditFileVault>> orderBy, List<Expression<Func<AuditFileVault, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<AuditFileVault, bool>> where, List<Expression<Func<AuditFileVault, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(AuditFileVault item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("AuditFileVault");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(AuditFileVault item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("AuditFileVault");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(AuditFileVault item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("AuditFileVault");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
