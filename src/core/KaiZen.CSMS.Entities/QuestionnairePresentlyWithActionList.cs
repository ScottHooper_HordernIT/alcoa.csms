﻿
using System;

namespace KaiZen.CSMS.Entities
{
	/// <summary>
	/// An enum representation of the 'QuestionnairePresentlyWithAction' table. [No description found in the database]
	/// </summary>
	/// <remark>this enumeration contains the items contained in the table QuestionnairePresentlyWithAction</remark>
	[Serializable]
	public enum QuestionnairePresentlyWithActionList
	{
		/// <summary> 
		/// (De-Activated)
		/// </summary>
		[EnumTextValue(@"(De-Activated)")]
		Deactivated = 1, 

		/// <summary> 
		/// Procurement - Expiring
		/// </summary>
		[EnumTextValue(@"Procurement - Expiring")]
		ProcurementExpiring = 2, 

		/// <summary> 
		/// Procurement - Expired
		/// </summary>
		[EnumTextValue(@"Procurement - Expired")]
		ProcurementExpired = 3, 

		/// <summary> 
		/// Procurement - Procurement Questionnaire
		/// </summary>
		[EnumTextValue(@"Procurement - Procurement Questionnaire")]
		ProcurementQuestionnaire = 4, 

		/// <summary> 
		/// Procurement - Assign Company Status
		/// </summary>
		[EnumTextValue(@"Procurement - Assign Company Status")]
		ProcurementAssignCompanyStatus = 5, 

		/// <summary> 
		/// H&S Assessor - Assessing Procurement Questionnaire
		/// </summary>
		[EnumTextValue(@"H&S Assessor - Assessing Procurement Questionnaire")]
		HSAssessorQuestionnaireProcurement = 6, 

		/// <summary> 
		/// H&S Assessor - Assessing Questionnaire
		/// </summary>
		[EnumTextValue(@"H&S Assessor - Assessing Questionnaire")]
		HSAssessorQuestionnaire = 7, 

		/// <summary> 
		/// Supplier - Supplier Questionnaire
		/// </summary>
		[EnumTextValue(@"Supplier - Supplier Questionnaire")]
		SupplierQuestionnaireSupplier = 8, 

		/// <summary> 
		/// Supplier - Supplier Questionnaire - Resubmitting
		/// </summary>
		[EnumTextValue(@"Supplier - Supplier Questionnaire - Resubmitting")]
		SupplierQuestionnaireIncomplete = 9, 

		/// <summary> 
		/// CSMS - Create Company Logins
		/// </summary>
		[EnumTextValue(@"CSMS - Create Company Logins")]
		CsmsCreateCompanyLogins = 10, 

		/// <summary> 
		/// -
		/// </summary>
		[EnumTextValue(@"-")]
		NoActionRequired = 11

	}
}
