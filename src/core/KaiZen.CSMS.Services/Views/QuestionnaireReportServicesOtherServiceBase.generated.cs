﻿
/*
	File generated by NetTiers templates [www.NetTiers.com]
	Important: Do not modify this file. Edit the file QuestionnaireReportServicesOther.cs instead.
*/

#region Using Directives
using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Security.Permissions;
using System.Xml.Serialization;
using System.Runtime.Serialization;
using System.Security;
using System.Data;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Entities.Validation;
using Entities = KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;


using Microsoft.Practices.EnterpriseLibrary.Logging;

#endregion 

namespace KaiZen.CSMS.Services
{		
	
	///<summary>
	/// An object representation of the 'QuestionnaireReportServicesOther' View.
	///</summary>
	/// <remarks>
	/// IMPORTANT!!! You should not modify this partial  class, modify the QuestionnaireReportServicesOther.cs file instead.
	/// All custom implementations should be done in the <see cref="QuestionnaireReportServicesOther"/> class.
	/// </remarks>
	[DataObject]
	public partial class QuestionnaireReportServicesOtherServiceBase : ServiceViewBase<QuestionnaireReportServicesOther>
	{

		#region Constructors
		///<summary>
		/// Creates a new <see cref="QuestionnaireReportServicesOther"/> instance .
		///</summary>
		public QuestionnaireReportServicesOtherServiceBase() : base()
		{
		}
		
		///<summary>
		/// A simple factory method to create a new <see cref="QuestionnaireReportServicesOther"/> instance.
		///</summary>
		///<param name="_companyName"></param>
		///<param name="_questionnaireId"></param>
		///<param name="_companyId"></param>
		///<param name="_status"></param>
		///<param name="_recommended"></param>
		///<param name="_createdByUserId"></param>
		///<param name="_createdDate"></param>
		///<param name="_modifiedByUserId"></param>
		///<param name="_modifiedDate"></param>
		///<param name="_approvedByUserId"></param>
		///<param name="_approvedDate"></param>
		///<param name="_levelOfSupervision"></param>
		///<param name="_companyStatusDesc"></param>
		///<param name="_answerId"></param>
		///<param name="_answerText"></param>
		///<param name="_type"></param>
		public static QuestionnaireReportServicesOther CreateQuestionnaireReportServicesOther(System.String _companyName, System.Int32 _questionnaireId, System.Int32 _companyId, System.Int32 _status, System.Boolean? _recommended, System.Int32 _createdByUserId, System.DateTime _createdDate, System.Int32 _modifiedByUserId, System.DateTime _modifiedDate, System.Int32? _approvedByUserId, System.DateTime? _approvedDate, System.String _levelOfSupervision, System.String _companyStatusDesc, System.Int32 _answerId, System.String _answerText, System.String _type)
		{
			QuestionnaireReportServicesOther newEntityQuestionnaireReportServicesOther = new QuestionnaireReportServicesOther();
			newEntityQuestionnaireReportServicesOther.CompanyName  = _companyName;
			newEntityQuestionnaireReportServicesOther.QuestionnaireId  = _questionnaireId;
			newEntityQuestionnaireReportServicesOther.CompanyId  = _companyId;
			newEntityQuestionnaireReportServicesOther.Status  = _status;
			newEntityQuestionnaireReportServicesOther.Recommended  = _recommended;
			newEntityQuestionnaireReportServicesOther.CreatedByUserId  = _createdByUserId;
			newEntityQuestionnaireReportServicesOther.CreatedDate  = _createdDate;
			newEntityQuestionnaireReportServicesOther.ModifiedByUserId  = _modifiedByUserId;
			newEntityQuestionnaireReportServicesOther.ModifiedDate  = _modifiedDate;
			newEntityQuestionnaireReportServicesOther.ApprovedByUserId  = _approvedByUserId;
			newEntityQuestionnaireReportServicesOther.ApprovedDate  = _approvedDate;
			newEntityQuestionnaireReportServicesOther.LevelOfSupervision  = _levelOfSupervision;
			newEntityQuestionnaireReportServicesOther.CompanyStatusDesc  = _companyStatusDesc;
			newEntityQuestionnaireReportServicesOther.AnswerId  = _answerId;
			newEntityQuestionnaireReportServicesOther.AnswerText  = _answerText;
			newEntityQuestionnaireReportServicesOther.Type  = _type;
			return newEntityQuestionnaireReportServicesOther;
		}
		#endregion Constructors

		#region Fields
		//private static SecurityContext<QuestionnaireReportServicesOther> securityContext = new SecurityContext<QuestionnaireReportServicesOther>();
		private static readonly string layerExceptionPolicy = "ServiceLayerExceptionPolicy";
		private static readonly bool noTranByDefault = false;
		private static readonly int defaultMaxRecords = 10000;
		#endregion 
		
		#region Data Access Methods
			
		#region Get 
		/// <summary>
		/// Attempts to do a parameterized version of a simple whereclause. 
		/// Returns rows meeting the whereClause condition from the DataSource.
		/// </summary>
		/// <param name="whereClause">Specifies the condition for the rows returned by a query (Name='John Doe', Name='John Doe' AND Id='1', Name='John Doe' OR Id='1').</param>
        /// <param name="orderBy">Specifies the sort criteria for the rows in the DataSource (Name ASC; BirthDay DESC, Name ASC);</param>
		/// <remarks>Does NOT Support Advanced Operations such as SubSelects.  See GetPaged for that functionality.</remarks>
		/// <returns>Returns a typed collection of Entity objects.</returns>
		public override VList<QuestionnaireReportServicesOther> Get(string whereClause, string orderBy)
		{
			int totalCount = -1;
			return Get(whereClause, orderBy, 0, defaultMaxRecords, out totalCount);
		}

		/// <summary>
		/// Returns rows meeting the whereClause condition from the DataSource.
		/// </summary>
		/// <param name="whereClause">Specifies the condition for the rows returned by a query (Name='John Doe', Name='John Doe' AND Id='1', Name='John Doe' OR Id='1').</param>
        /// <param name="orderBy">Specifies the sort criteria for the rows in the DataSource (Name ASC; BirthDay DESC, Name ASC);</param>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="totalCount">out parameter to get total records for query</param>
		/// <remarks>Does NOT Support Advanced Operations such as SubSelects.  See GetPaged for that functionality.</remarks>
		/// <returns>Returns a typed collection TList{QuestionnaireReportServicesOther} of <c>QuestionnaireReportServicesOther</c> objects.</returns>
		public override VList<QuestionnaireReportServicesOther> Get(string whereClause, string orderBy, int start, int pageLength, out int totalCount)
		{
			// throws security exception if not authorized
			//SecurityContext.IsAuthorized("Get");
								
			// get this data
			VList<QuestionnaireReportServicesOther> list = null;
			totalCount = -1;
			TransactionManager transactionManager = null; 

			try
            {	
				//since this is a read operation, don't create a tran by default, only use tran if provided to us for custom isolation level
				transactionManager = ConnectionScope.ValidateOrCreateTransaction(noTranByDefault);
				NetTiersProvider dataProvider = ConnectionScope.Current.DataProvider;
					
				//Access repository
				list = dataProvider.QuestionnaireReportServicesOtherProvider.Get(transactionManager, whereClause, orderBy, start, pageLength, out totalCount);
				
				//if borrowed tran, leave open for next call
			}
            catch (Exception exc)
            {
				//if open, rollback, it's possible this is part of a larger commit
                if (transactionManager != null && transactionManager.IsOpen) 
					transactionManager.Rollback();
				
				//Handle exception based on policy
                if (DomainUtil.HandleException(exc, layerExceptionPolicy)) 
					throw;
			}
			return list;
		}
		
		#endregion Get Methods
		
		#region GetAll
		/// <summary>
		/// Get a complete collection of <see cref="QuestionnaireReportServicesOther" /> entities.
		/// </summary>
		/// <returns></returns>
		public virtual VList<QuestionnaireReportServicesOther> GetAll() 
		{
			int totalCount = -1;
			return GetAll(0, defaultMaxRecords, out totalCount);
		}

       
		/// <summary>
		/// Get a set portion of a complete list of <see cref="QuestionnaireReportServicesOther" /> entities
		/// </summary>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="totalCount">out parameter, number of total rows in given query.</param>
		/// <returns>a <see cref="TList{QuestionnaireReportServicesOther}"/> </returns>
		public override VList<QuestionnaireReportServicesOther> GetAll(int start, int pageLength, out int totalCount) 
		{
			// throws security exception if not authorized
			//SecurityContext.IsAuthorized("GetAll");
			
			// get this data
			VList<QuestionnaireReportServicesOther> list = null;
			totalCount = -1;
			TransactionManager transactionManager = null; 

			try
            {	
				//since this is a read operation, don't create a tran by default, only use tran if provided to us for custom isolation level
				transactionManager = ConnectionScope.ValidateOrCreateTransaction(noTranByDefault);
				NetTiersProvider dataProvider = ConnectionScope.Current.DataProvider;					

				//Access repository
				list = dataProvider.QuestionnaireReportServicesOtherProvider.GetAll(transactionManager, start, pageLength, out totalCount);	
			}
            catch (Exception exc)
            {
				//if open, rollback, it's possible this is part of a larger commit
                if (transactionManager != null && transactionManager.IsOpen) 
					transactionManager.Rollback();
				
				//Handle exception based on policy
                if (DomainUtil.HandleException(exc, layerExceptionPolicy)) 
					throw;
			}
			return list;
		}
		#endregion GetAll

		#region GetPaged
		/// <summary>
		/// Gets a page of <see cref="TList{QuestionnaireReportServicesOther}" /> rows from the DataSource.
		/// </summary>
		/// <param name="totalCount">Out Parameter, Number of rows in the DataSource.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of <c>QuestionnaireReportServicesOther</c> objects.</returns>
		public virtual VList<QuestionnaireReportServicesOther> GetPaged(out int totalCount)
		{
			return GetPaged(null, null, 0, defaultMaxRecords, out totalCount);
		}
		
		/// <summary>
		/// Gets a page of <see cref="TList{QuestionnaireReportServicesOther}" /> rows from the DataSource.
		/// </summary>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="totalCount">Number of rows in the DataSource.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of <c>QuestionnaireReportServicesOther</c> objects.</returns>
		public virtual VList<QuestionnaireReportServicesOther> GetPaged(int start, int pageLength, out int totalCount)
		{
			return GetPaged(null, null, start, pageLength, out totalCount);
		}

		/// <summary>
		/// Gets a page of entity rows with a <see cref="TList{QuestionnaireReportServicesOther}" /> from the DataSource with a where clause and order by clause.
		/// </summary>
		/// <param name="whereClause">Specifies the condition for the rows returned by a query (Name='John Doe', Name='John Doe' AND Id='1', Name='John Doe' OR Id='1').</param>
		/// <param name="orderBy">Specifies the sort criteria for the rows in the DataSource (Name ASC; BirthDay DESC, Name ASC).</param>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="totalCount">Out Parameter, Number of rows in the DataSource.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of <c>QuestionnaireReportServicesOther</c> objects.</returns>
		public override VList<QuestionnaireReportServicesOther> GetPaged(string whereClause,string orderBy, int start, int pageLength, out int totalCount)
		{
			// throws security exception if not authorized
			//SecurityContext.IsAuthorized("GetPaged");
			
			// get this data
			VList<QuestionnaireReportServicesOther> list = null;
			totalCount = -1;
			TransactionManager transactionManager = null; 

			try
            {	
				//since this is a read operation, don't create a tran by default, only use tran if provided to us for custom isolation level
				transactionManager = ConnectionScope.ValidateOrCreateTransaction(noTranByDefault);
				NetTiersProvider dataProvider = ConnectionScope.Current.DataProvider;
					
				//Access repository
				list = dataProvider.QuestionnaireReportServicesOtherProvider.GetPaged(transactionManager, whereClause, orderBy, start, pageLength, out totalCount);
				
				//if borrowed tran, leave open for next call
			}
            catch (Exception exc)
            {
				//if open, rollback, it's possible this is part of a larger commit
                if (transactionManager != null && transactionManager.IsOpen) 
					transactionManager.Rollback();
				
				//Handle exception based on policy
                if (DomainUtil.HandleException(exc, layerExceptionPolicy)) 
					throw;
			}
			return list;			
		}
		
		/// <summary>
		/// Gets the number of rows in the DataSource that match the specified whereClause.
		/// This method is only provided as a workaround for the ObjectDataSource's need to 
		/// execute another method to discover the total count instead of using another param, like our out param.  
		/// This method should be avoided if using the ObjectDataSource or another method.
		/// </summary>
		/// <param name="whereClause">Specifies the condition for the rows returned by a query (Name='John Doe', Name='John Doe' AND Id='1', Name='John Doe' OR Id='1').</param>
		/// <param name="totalCount">Number of rows in the DataSource.</param>
		/// <returns>Returns the number of rows.</returns>
		public int GetTotalItems(string whereClause, out int totalCount)
		{
			GetPaged(whereClause, null, 0, defaultMaxRecords, out totalCount);
			return totalCount;
		}
		#endregion GetPaged	

		#region Find Methods

		/// <summary>
		/// 	Returns rows from the DataSource that meet the parameter conditions.
		/// </summary>
		/// <param name="parameters">A collection of <see cref="SqlFilterParameter"/> objects.</param>
		/// <returns>Returns a typed collection of <c>QuestionnaireReportServicesOther</c> objects.</returns>
		public virtual VList<QuestionnaireReportServicesOther> Find(IFilterParameterCollection parameters)
		{
			return Find(parameters, null);
		}
		
		/// <summary>
		/// 	Returns rows from the DataSource that meet the parameter conditions.
		/// </summary>
		/// <param name="parameters">A collection of <see cref="SqlFilterParameter"/> objects.</param>
		/// <param name="orderBy">Specifies the sort criteria for the rows in the DataSource (Name ASC; BirthDay DESC, Name ASC);</param>
		/// <returns>Returns a typed collection of <c>QuestionnaireReportServicesOther</c> objects.</returns>
		public virtual VList<QuestionnaireReportServicesOther> Find(IFilterParameterCollection parameters, string orderBy)
		{
			int count = 0;
			return Find(parameters, orderBy, 0, defaultMaxRecords, out count);
		}
		
		/// <summary>
		/// 	Returns rows from the DataSource that meet the parameter conditions.
		/// </summary>
		/// <param name="parameters">A collection of <see cref="SqlFilterParameter"/> objects.</param>
		/// <param name="orderBy">Specifies the sort criteria for the rows in the DataSource (Name ASC; BirthDay DESC, Name ASC);</param>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out. The number of rows that match this query.</param>
		/// <returns>Returns a typed collection of <c>QuestionnaireReportServicesOther</c> objects.</returns>
		public override VList<QuestionnaireReportServicesOther> Find(IFilterParameterCollection parameters, string orderBy, int start, int pageLength, out int count)
		{
			// throws security exception if not authorized
			//SecurityContext.IsAuthorized("Find");
								
			// get this data
			TransactionManager transactionManager = null; 
			VList<QuestionnaireReportServicesOther> list = null;
			count = -1;
			
			try
            {	
				//since this is a read operation, don't create a tran by default, only use tran if provided to us for custom isolation level
				transactionManager = ConnectionScope.ValidateOrCreateTransaction(noTranByDefault);
				NetTiersProvider dataProvider = ConnectionScope.Current.DataProvider;
					
				//Access repository
				list = dataProvider.QuestionnaireReportServicesOtherProvider.Find(transactionManager, parameters, orderBy, start, pageLength, out count);
			}
            catch (Exception exc)
            {
				//if open, rollback, it's possible this is part of a larger commit
                if (transactionManager != null && transactionManager.IsOpen) 
					transactionManager.Rollback();
				
				//Handle exception based on policy
                if (DomainUtil.HandleException(exc, layerExceptionPolicy)) 
					throw;
			}
			
			return list;
		}
		
		#endregion Find Methods
		
		#region Custom Methods
		
		#region _QuestionnaireReportServicesOther_GetAllWithEbiActiveSitesThisYear
		/// <summary>
		///	This method wrap the '_QuestionnaireReportServicesOther_GetAllWithEbiActiveSitesThisYear' stored procedure. 
		/// </summary>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public virtual DataSet GetAllWithEbiActiveSitesThisYear()
		{
			return GetAllWithEbiActiveSitesThisYear( 0, defaultMaxRecords );
		}
	
		/// <summary>
		///	This method wrap the '_QuestionnaireReportServicesOther_GetAllWithEbiActiveSitesThisYear' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public virtual DataSet GetAllWithEbiActiveSitesThisYear(int start, int pageLength)
		{
			// throws security exception if not authorized
			//SecurityContext.IsAuthorized("GetAllWithEbiActiveSitesThisYear");
			
		
			DataSet result = null; 
			TransactionManager transactionManager = null; 
			
			try
            {
				bool isBorrowedTransaction = ConnectionScope.Current.HasTransaction;				
				
				//since this is a read operation, don't create a tran by default, only use tran if provided to us for custom isolation level
				transactionManager = ConnectionScope.ValidateOrCreateTransaction(noTranByDefault);
				NetTiersProvider dataProvider = ConnectionScope.Current.DataProvider;
                
				//Call Custom Procedure from Repository
				result = dataProvider.QuestionnaireReportServicesOtherProvider.GetAllWithEbiActiveSitesThisYear(transactionManager, start, pageLength );
	        
            	
			}
            catch (Exception exc)
            {
				//if open, rollback
                if (transactionManager != null && transactionManager.IsOpen)
                        transactionManager.Rollback();
                    
				//Handle exception based on policy
                if (DomainUtil.HandleException(exc, layerExceptionPolicy)) 
					throw;
            }
			
			return result;
		}
		#endregion 
		
		#region _QuestionnaireReportServicesOther_GetAllWithEbiActiveSitesThisYear_ByCompanyId
		/// <summary>
		///	This method wrap the '_QuestionnaireReportServicesOther_GetAllWithEbiActiveSitesThisYear_ByCompanyId' stored procedure. 
		/// </summary>
		/// <param name="sCompanyId"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public virtual DataSet GetAllWithEbiActiveSitesThisYear_ByCompanyId(System.Int32? sCompanyId)
		{
			return GetAllWithEbiActiveSitesThisYear_ByCompanyId( sCompanyId, 0, defaultMaxRecords );
		}
	
		/// <summary>
		///	This method wrap the '_QuestionnaireReportServicesOther_GetAllWithEbiActiveSitesThisYear_ByCompanyId' stored procedure. 
		/// </summary>
		/// <param name="sCompanyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public virtual DataSet GetAllWithEbiActiveSitesThisYear_ByCompanyId(System.Int32? sCompanyId, int start, int pageLength)
		{
			// throws security exception if not authorized
			//SecurityContext.IsAuthorized("GetAllWithEbiActiveSitesThisYear_ByCompanyId");
			
		
			DataSet result = null; 
			TransactionManager transactionManager = null; 
			
			try
            {
				bool isBorrowedTransaction = ConnectionScope.Current.HasTransaction;				
				
				//since this is a read operation, don't create a tran by default, only use tran if provided to us for custom isolation level
				transactionManager = ConnectionScope.ValidateOrCreateTransaction(noTranByDefault);
				NetTiersProvider dataProvider = ConnectionScope.Current.DataProvider;
                
				//Call Custom Procedure from Repository
				result = dataProvider.QuestionnaireReportServicesOtherProvider.GetAllWithEbiActiveSitesThisYear_ByCompanyId(transactionManager, start, pageLength , sCompanyId);
	        
            	
			}
            catch (Exception exc)
            {
				//if open, rollback
                if (transactionManager != null && transactionManager.IsOpen)
                        transactionManager.Rollback();
                    
				//Handle exception based on policy
                if (DomainUtil.HandleException(exc, layerExceptionPolicy)) 
					throw;
            }
			
			return result;
		}
		#endregion 
		#endregion
		
		#endregion Data Access Methods
		
	
	}//End Class
} // end namespace



