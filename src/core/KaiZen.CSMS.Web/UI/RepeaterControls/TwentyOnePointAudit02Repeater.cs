﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.ComponentModel;
using System.Web.UI.Design.WebControls;
using System.Web.UI.Design;
using System.Web.UI.WebControls;

namespace KaiZen.CSMS.Web.UI
{
    /// <summary>
    /// A designer class for a strongly typed repeater <c>TwentyOnePointAudit02Repeater</c>
    /// </summary>
	public class TwentyOnePointAudit02RepeaterDesigner : System.Web.UI.Design.ControlDesigner
	{
	    /// <summary>
        /// Initializes a new instance of the <see cref="T:TwentyOnePointAudit02RepeaterDesigner"/> class.
        /// </summary>
		public TwentyOnePointAudit02RepeaterDesigner()
		{
		}

        /// <summary>
        /// Initializes the control designer and loads the specified component.
        /// </summary>
        /// <param name="component">The control being designed.</param>
		public override void Initialize(IComponent component)
		{
			if (!(component is TwentyOnePointAudit02Repeater))
			{ 
				throw new ArgumentException("Component is not a TwentyOnePointAudit02Repeater."); 
			} 
			base.Initialize(component); 
			base.SetViewFlags(ViewFlags.TemplateEditing, true); 
		}


		/// <summary>
		/// Generate HTML for the designer
		/// </summary>
		/// <returns>a string of design time HTML</returns>
		public override string GetDesignTimeHtml()
		{

			// Get the instance this designer applies to
			//
			TwentyOnePointAudit02Repeater z = (TwentyOnePointAudit02Repeater)Component;
			z.DataBind();

			return base.GetDesignTimeHtml();

			//return z.RenderAtDesignTime();

			//	ControlCollection c = z.Controls;
			//Totem z = (Totem) Component;
			//Totem z = (Totem) Component;
			//return ("<div style='border: 1px gray dotted; background-color: lightgray'><b>TagStat :</b> zae |  qsdds</div>");

		}
	}

    /// <summary>
    /// A strongly typed repeater control for the <see cref="TwentyOnePointAudit02Repeater"/> Type.
    /// </summary>
	[Designer(typeof(TwentyOnePointAudit02RepeaterDesigner))]
	[ParseChildren(true)]
	[ToolboxData("<{0}:TwentyOnePointAudit02Repeater runat=\"server\"></{0}:TwentyOnePointAudit02Repeater>")]
	public class TwentyOnePointAudit02Repeater : CompositeDataBoundControl, System.Web.UI.INamingContainer
	{
	    /// <summary>
        /// Initializes a new instance of the <see cref="T:TwentyOnePointAudit02Repeater"/> class.
        /// </summary>
		public TwentyOnePointAudit02Repeater()
		{
		}

		/// <summary>
        /// Gets a <see cref="T:System.Web.UI.ControlCollection"></see> object that represents the child controls for a specified server control in the UI hierarchy.
        /// </summary>
        /// <value></value>
        /// <returns>The collection of child controls for the specified server control.</returns>
		public override ControlCollection Controls
		{
			get
			{
				this.EnsureChildControls();
				return base.Controls;
			}
		}

		private ITemplate m_headerTemplate;
		/// <summary>
        /// Gets or sets the header template.
        /// </summary>
        /// <value>The header template.</value>
		[Browsable(false)]
		[TemplateContainer(typeof(TwentyOnePointAudit02Item))]
		[PersistenceMode(PersistenceMode.InnerDefaultProperty)]
		public ITemplate HeaderTemplate
		{
			get { return m_headerTemplate; }
			set { m_headerTemplate = value; }
		}

		private ITemplate m_itemTemplate;
		/// <summary>
        /// Gets or sets the item template.
        /// </summary>
        /// <value>The item template.</value>
		[Browsable(false)]
		[TemplateContainer(typeof(TwentyOnePointAudit02Item))]
		[PersistenceMode(PersistenceMode.InnerDefaultProperty)]
		public ITemplate ItemTemplate
		{
			get { return m_itemTemplate; }
			set { m_itemTemplate = value; }
		}

		private ITemplate m_seperatorTemplate;
        /// <summary>
        /// Gets or sets the Seperator Template
        /// </summary>
        [Browsable(false)]
        [TemplateContainer(typeof(TwentyOnePointAudit02Item))]
        [PersistenceMode(PersistenceMode.InnerDefaultProperty)]
        public ITemplate SeperatorTemplate
        {
            get { return m_seperatorTemplate; }
            set { m_seperatorTemplate = value; }
        }
			
		private ITemplate m_altenateItemTemplate;
        /// <summary>
        /// Gets or sets the alternating item template.
        /// </summary>
        /// <value>The alternating item template.</value>
		[Browsable(false)]
		[TemplateContainer(typeof(TwentyOnePointAudit02Item))]
		[PersistenceMode(PersistenceMode.InnerDefaultProperty)]
		public ITemplate AlternatingItemTemplate
		{
			get { return m_altenateItemTemplate; }
			set { m_altenateItemTemplate = value; }
		}

		private ITemplate m_footerTemplate;
        /// <summary>
        /// Gets or sets the footer template.
        /// </summary>
        /// <value>The footer template.</value>
		[Browsable(false)]
		[TemplateContainer(typeof(TwentyOnePointAudit02Item))]
		[PersistenceMode(PersistenceMode.InnerDefaultProperty)]
		public ITemplate FooterTemplate
		{
			get { return m_footerTemplate; }
			set { m_footerTemplate = value; }
		}

//      /// <summary>
//      /// Called by the ASP.NET page framework to notify server controls that use composition-based implementation to create any child controls they contain in preparation for posting back or rendering.
//      /// </summary>
//		protected override void CreateChildControls()
//      {
//         if (ChildControlsCreated)
//         {
//            return;
//         }

//         Controls.Clear();

//         //Instantiate the Header template (if exists)
//         if (m_headerTemplate != null)
//         {
//            Control headerItem = new Control();
//            m_headerTemplate.InstantiateIn(headerItem);
//            Controls.Add(headerItem);
//         }

//         //Instantiate the Footer template (if exists)
//         if (m_footerTemplate != null)
//         {
//            Control footerItem = new Control();
//            m_footerTemplate.InstantiateIn(footerItem);
//            Controls.Add(footerItem);
//         }
//
//         ChildControlsCreated = true;
//      }
	
		/// <summary>
        /// Overridden and Empty so that span tags are not written
        /// </summary>
        /// <param name="writer"></param>
        public override void RenderBeginTag(HtmlTextWriter writer)
        {
            
        }

        /// <summary>
        /// Overridden and Empty so that span tags are not written
        /// </summary>
        /// <param name="writer"></param>
        public override void RenderEndTag(HtmlTextWriter writer)
        {
                
        }		
		
		/// <summary>
      	/// Called by the ASP.NET page framework to notify server controls that use composition-based implementation to create any child controls they contain in preparation for posting back or rendering.
      	/// </summary>
		protected override int CreateChildControls(System.Collections.IEnumerable dataSource, bool dataBinding)
      	{
         int pos = 0;

         if (dataBinding)
         {
            //Instantiate the Header template (if exists)
            if (m_headerTemplate != null)
            {
                Control headerItem = new Control();
                m_headerTemplate.InstantiateIn(headerItem);
                Controls.Add(headerItem);
            }
			if (dataSource != null)
			{
				foreach (object o in dataSource)
				{
						KaiZen.CSMS.Entities.TwentyOnePointAudit02 entity = o as KaiZen.CSMS.Entities.TwentyOnePointAudit02;
						TwentyOnePointAudit02Item container = new TwentyOnePointAudit02Item(entity);
	
						if (m_itemTemplate != null && (pos % 2) == 0)
						{
							m_itemTemplate.InstantiateIn(container);
							
							if (m_seperatorTemplate != null)
							{
								m_seperatorTemplate.InstantiateIn(container);
							}
						}
						else
						{
							if (m_altenateItemTemplate != null)
							{
								m_altenateItemTemplate.InstantiateIn(container);
								
								if (m_seperatorTemplate != null)
								{
									m_seperatorTemplate.InstantiateIn(container);
								}
								
							}
							else if (m_itemTemplate != null)
							{
								m_itemTemplate.InstantiateIn(container);
								
								if (m_seperatorTemplate != null)
								{
									m_seperatorTemplate.InstantiateIn(container);
								}
							}
							else
							{
								// no template !!!
							}
						}
						Controls.Add(container);
						
						container.DataBind();
						
						pos++;
				}
			}
            //Instantiate the Footer template (if exists)
            if (m_footerTemplate != null)
            {
                Control footerItem = new Control();
                m_footerTemplate.InstantiateIn(footerItem);
                Controls.Add(footerItem);
            }

		}
			
			return pos;
		}

        /// <summary>
        /// Raises the <see cref="E:System.Web.UI.Control.PreRender"></see> event.
        /// </summary>
        /// <param name="e">An <see cref="T:System.EventArgs"></see> object that contains the event data.</param>
		protected override void OnPreRender(EventArgs e)
		{
			base.DataBind();
		}

		#region Design time
        /// <summary>
        /// Renders at design time.
        /// </summary>
        /// <returns>a  string of the Designed HTML</returns>
		internal string RenderAtDesignTime()
		{			
			return "Designer currently not implemented"; 
		}

		#endregion
	}

    /// <summary>
    /// A wrapper type for the entity
    /// </summary>
	[System.ComponentModel.ToolboxItem(false)]
	public class TwentyOnePointAudit02Item : System.Web.UI.Control, System.Web.UI.INamingContainer
	{
		private KaiZen.CSMS.Entities.TwentyOnePointAudit02 _entity;

        /// <summary>
        /// Initializes a new instance of the <see cref="T:TwentyOnePointAudit02Item"/> class.
        /// </summary>
		public TwentyOnePointAudit02Item()
			: base()
		{ }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:TwentyOnePointAudit02Item"/> class.
        /// </summary>
		public TwentyOnePointAudit02Item(KaiZen.CSMS.Entities.TwentyOnePointAudit02 entity)
			: base()
		{
			_entity = entity;
		}
		
        /// <summary>
        /// Gets the Id
        /// </summary>
        /// <value>The Id.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32 Id
		{
			get { return _entity.Id; }
		}
        /// <summary>
        /// Gets the Achieved2a
        /// </summary>
        /// <value>The Achieved2a.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved2a
		{
			get { return _entity.Achieved2a; }
		}
        /// <summary>
        /// Gets the Achieved2b
        /// </summary>
        /// <value>The Achieved2b.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved2b
		{
			get { return _entity.Achieved2b; }
		}
        /// <summary>
        /// Gets the Achieved2c
        /// </summary>
        /// <value>The Achieved2c.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved2c
		{
			get { return _entity.Achieved2c; }
		}
        /// <summary>
        /// Gets the Achieved2d
        /// </summary>
        /// <value>The Achieved2d.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved2d
		{
			get { return _entity.Achieved2d; }
		}
        /// <summary>
        /// Gets the Achieved2e
        /// </summary>
        /// <value>The Achieved2e.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved2e
		{
			get { return _entity.Achieved2e; }
		}
        /// <summary>
        /// Gets the Achieved2f
        /// </summary>
        /// <value>The Achieved2f.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved2f
		{
			get { return _entity.Achieved2f; }
		}
        /// <summary>
        /// Gets the Achieved2g
        /// </summary>
        /// <value>The Achieved2g.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved2g
		{
			get { return _entity.Achieved2g; }
		}
        /// <summary>
        /// Gets the Achieved2h
        /// </summary>
        /// <value>The Achieved2h.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved2h
		{
			get { return _entity.Achieved2h; }
		}
        /// <summary>
        /// Gets the Achieved2i
        /// </summary>
        /// <value>The Achieved2i.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved2i
		{
			get { return _entity.Achieved2i; }
		}
        /// <summary>
        /// Gets the Observation2a
        /// </summary>
        /// <value>The Observation2a.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation2a
		{
			get { return _entity.Observation2a; }
		}
        /// <summary>
        /// Gets the Observation2b
        /// </summary>
        /// <value>The Observation2b.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation2b
		{
			get { return _entity.Observation2b; }
		}
        /// <summary>
        /// Gets the Observation2c
        /// </summary>
        /// <value>The Observation2c.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation2c
		{
			get { return _entity.Observation2c; }
		}
        /// <summary>
        /// Gets the Observation2d
        /// </summary>
        /// <value>The Observation2d.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation2d
		{
			get { return _entity.Observation2d; }
		}
        /// <summary>
        /// Gets the Observation2e
        /// </summary>
        /// <value>The Observation2e.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation2e
		{
			get { return _entity.Observation2e; }
		}
        /// <summary>
        /// Gets the Observation2f
        /// </summary>
        /// <value>The Observation2f.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation2f
		{
			get { return _entity.Observation2f; }
		}
        /// <summary>
        /// Gets the Observation2g
        /// </summary>
        /// <value>The Observation2g.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation2g
		{
			get { return _entity.Observation2g; }
		}
        /// <summary>
        /// Gets the Observation2h
        /// </summary>
        /// <value>The Observation2h.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation2h
		{
			get { return _entity.Observation2h; }
		}
        /// <summary>
        /// Gets the Observation2i
        /// </summary>
        /// <value>The Observation2i.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation2i
		{
			get { return _entity.Observation2i; }
		}
        /// <summary>
        /// Gets the TotalScore
        /// </summary>
        /// <value>The TotalScore.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? TotalScore
		{
			get { return _entity.TotalScore; }
		}

        /// <summary>
        /// Gets a <see cref="T:KaiZen.CSMS.Entities.TwentyOnePointAudit02"></see> object
        /// </summary>
        /// <value></value>
        [System.ComponentModel.Bindable(true)]
        public KaiZen.CSMS.Entities.TwentyOnePointAudit02 Entity
        {
            get { return _entity; }
        }
	}
}
