﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="CompaniesHrCrpDataProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class CompaniesHrCrpDataProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.CompaniesHrCrpData, KaiZen.CSMS.Entities.CompaniesHrCrpDataKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.CompaniesHrCrpDataKey key)
		{
			return Delete(transactionManager, key.CompaniesHrCrpDataId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_companiesHrCrpDataId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _companiesHrCrpDataId)
		{
			return Delete(null, _companiesHrCrpDataId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companiesHrCrpDataId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _companiesHrCrpDataId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompaniesHrCrpData_CompaniesHrMap key.
		///		FK_CompaniesHrCrpData_CompaniesHrMap Description: 
		/// </summary>
		/// <param name="_companyId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompaniesHrCrpData objects.</returns>
		public TList<CompaniesHrCrpData> GetByCompanyId(System.Int32 _companyId)
		{
			int count = -1;
			return GetByCompanyId(_companyId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompaniesHrCrpData_CompaniesHrMap key.
		///		FK_CompaniesHrCrpData_CompaniesHrMap Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompaniesHrCrpData objects.</returns>
		/// <remarks></remarks>
		public TList<CompaniesHrCrpData> GetByCompanyId(TransactionManager transactionManager, System.Int32 _companyId)
		{
			int count = -1;
			return GetByCompanyId(transactionManager, _companyId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompaniesHrCrpData_CompaniesHrMap key.
		///		FK_CompaniesHrCrpData_CompaniesHrMap Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompaniesHrCrpData objects.</returns>
		public TList<CompaniesHrCrpData> GetByCompanyId(TransactionManager transactionManager, System.Int32 _companyId, int start, int pageLength)
		{
			int count = -1;
			return GetByCompanyId(transactionManager, _companyId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompaniesHrCrpData_CompaniesHrMap key.
		///		fkCompaniesHrCrpDataCompaniesHrMap Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_companyId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompaniesHrCrpData objects.</returns>
		public TList<CompaniesHrCrpData> GetByCompanyId(System.Int32 _companyId, int start, int pageLength)
		{
			int count =  -1;
			return GetByCompanyId(null, _companyId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompaniesHrCrpData_CompaniesHrMap key.
		///		fkCompaniesHrCrpDataCompaniesHrMap Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_companyId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompaniesHrCrpData objects.</returns>
		public TList<CompaniesHrCrpData> GetByCompanyId(System.Int32 _companyId, int start, int pageLength,out int count)
		{
			return GetByCompanyId(null, _companyId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompaniesHrCrpData_CompaniesHrMap key.
		///		FK_CompaniesHrCrpData_CompaniesHrMap Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompaniesHrCrpData objects.</returns>
		public abstract TList<CompaniesHrCrpData> GetByCompanyId(TransactionManager transactionManager, System.Int32 _companyId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompaniesHrCrpData_Sites key.
		///		FK_CompaniesHrCrpData_Sites Description: 
		/// </summary>
		/// <param name="_siteId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompaniesHrCrpData objects.</returns>
		public TList<CompaniesHrCrpData> GetBySiteId(System.Int32 _siteId)
		{
			int count = -1;
			return GetBySiteId(_siteId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompaniesHrCrpData_Sites key.
		///		FK_CompaniesHrCrpData_Sites Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_siteId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompaniesHrCrpData objects.</returns>
		/// <remarks></remarks>
		public TList<CompaniesHrCrpData> GetBySiteId(TransactionManager transactionManager, System.Int32 _siteId)
		{
			int count = -1;
			return GetBySiteId(transactionManager, _siteId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompaniesHrCrpData_Sites key.
		///		FK_CompaniesHrCrpData_Sites Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_siteId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompaniesHrCrpData objects.</returns>
		public TList<CompaniesHrCrpData> GetBySiteId(TransactionManager transactionManager, System.Int32 _siteId, int start, int pageLength)
		{
			int count = -1;
			return GetBySiteId(transactionManager, _siteId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompaniesHrCrpData_Sites key.
		///		fkCompaniesHrCrpDataSites Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_siteId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompaniesHrCrpData objects.</returns>
		public TList<CompaniesHrCrpData> GetBySiteId(System.Int32 _siteId, int start, int pageLength)
		{
			int count =  -1;
			return GetBySiteId(null, _siteId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompaniesHrCrpData_Sites key.
		///		fkCompaniesHrCrpDataSites Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_siteId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompaniesHrCrpData objects.</returns>
		public TList<CompaniesHrCrpData> GetBySiteId(System.Int32 _siteId, int start, int pageLength,out int count)
		{
			return GetBySiteId(null, _siteId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompaniesHrCrpData_Sites key.
		///		FK_CompaniesHrCrpData_Sites Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_siteId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompaniesHrCrpData objects.</returns>
		public abstract TList<CompaniesHrCrpData> GetBySiteId(TransactionManager transactionManager, System.Int32 _siteId, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.CompaniesHrCrpData Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.CompaniesHrCrpDataKey key, int start, int pageLength)
		{
			return GetByCompaniesHrCrpDataId(transactionManager, key.CompaniesHrCrpDataId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_CompaniesHrCrpData index.
		/// </summary>
		/// <param name="_companiesHrCrpDataId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompaniesHrCrpData"/> class.</returns>
		public KaiZen.CSMS.Entities.CompaniesHrCrpData GetByCompaniesHrCrpDataId(System.Int32 _companiesHrCrpDataId)
		{
			int count = -1;
			return GetByCompaniesHrCrpDataId(null,_companiesHrCrpDataId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CompaniesHrCrpData index.
		/// </summary>
		/// <param name="_companiesHrCrpDataId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompaniesHrCrpData"/> class.</returns>
		public KaiZen.CSMS.Entities.CompaniesHrCrpData GetByCompaniesHrCrpDataId(System.Int32 _companiesHrCrpDataId, int start, int pageLength)
		{
			int count = -1;
			return GetByCompaniesHrCrpDataId(null, _companiesHrCrpDataId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CompaniesHrCrpData index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companiesHrCrpDataId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompaniesHrCrpData"/> class.</returns>
		public KaiZen.CSMS.Entities.CompaniesHrCrpData GetByCompaniesHrCrpDataId(TransactionManager transactionManager, System.Int32 _companiesHrCrpDataId)
		{
			int count = -1;
			return GetByCompaniesHrCrpDataId(transactionManager, _companiesHrCrpDataId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CompaniesHrCrpData index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companiesHrCrpDataId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompaniesHrCrpData"/> class.</returns>
		public KaiZen.CSMS.Entities.CompaniesHrCrpData GetByCompaniesHrCrpDataId(TransactionManager transactionManager, System.Int32 _companiesHrCrpDataId, int start, int pageLength)
		{
			int count = -1;
			return GetByCompaniesHrCrpDataId(transactionManager, _companiesHrCrpDataId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CompaniesHrCrpData index.
		/// </summary>
		/// <param name="_companiesHrCrpDataId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompaniesHrCrpData"/> class.</returns>
		public KaiZen.CSMS.Entities.CompaniesHrCrpData GetByCompaniesHrCrpDataId(System.Int32 _companiesHrCrpDataId, int start, int pageLength, out int count)
		{
			return GetByCompaniesHrCrpDataId(null, _companiesHrCrpDataId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CompaniesHrCrpData index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companiesHrCrpDataId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompaniesHrCrpData"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.CompaniesHrCrpData GetByCompaniesHrCrpDataId(TransactionManager transactionManager, System.Int32 _companiesHrCrpDataId, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;CompaniesHrCrpData&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;CompaniesHrCrpData&gt;"/></returns>
		public static TList<CompaniesHrCrpData> Fill(IDataReader reader, TList<CompaniesHrCrpData> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.CompaniesHrCrpData c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("CompaniesHrCrpData")
					.Append("|").Append((System.Int32)reader[((int)CompaniesHrCrpDataColumn.CompaniesHrCrpDataId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<CompaniesHrCrpData>(
					key.ToString(), // EntityTrackingKey
					"CompaniesHrCrpData",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.CompaniesHrCrpData();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.CompaniesHrCrpDataId = (System.Int32)reader[((int)CompaniesHrCrpDataColumn.CompaniesHrCrpDataId - 1)];
					c.CompanyId = (System.Int32)reader[((int)CompaniesHrCrpDataColumn.CompanyId - 1)];
					c.SiteId = (System.Int32)reader[((int)CompaniesHrCrpDataColumn.SiteId - 1)];
					c.CrpName = (System.String)reader[((int)CompaniesHrCrpDataColumn.CrpName - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.CompaniesHrCrpData"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.CompaniesHrCrpData"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.CompaniesHrCrpData entity)
		{
			if (!reader.Read()) return;
			
			entity.CompaniesHrCrpDataId = (System.Int32)reader[((int)CompaniesHrCrpDataColumn.CompaniesHrCrpDataId - 1)];
			entity.CompanyId = (System.Int32)reader[((int)CompaniesHrCrpDataColumn.CompanyId - 1)];
			entity.SiteId = (System.Int32)reader[((int)CompaniesHrCrpDataColumn.SiteId - 1)];
			entity.CrpName = (System.String)reader[((int)CompaniesHrCrpDataColumn.CrpName - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.CompaniesHrCrpData"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.CompaniesHrCrpData"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.CompaniesHrCrpData entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.CompaniesHrCrpDataId = (System.Int32)dataRow["CompaniesHrCrpDataId"];
			entity.CompanyId = (System.Int32)dataRow["CompanyId"];
			entity.SiteId = (System.Int32)dataRow["SiteId"];
			entity.CrpName = (System.String)dataRow["CrpName"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.CompaniesHrCrpData"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.CompaniesHrCrpData Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.CompaniesHrCrpData entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region CompanyIdSource	
			if (CanDeepLoad(entity, "CompaniesHrMap|CompanyIdSource", deepLoadType, innerList) 
				&& entity.CompanyIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.CompanyId;
				CompaniesHrMap tmpEntity = EntityManager.LocateEntity<CompaniesHrMap>(EntityLocator.ConstructKeyFromPkItems(typeof(CompaniesHrMap), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.CompanyIdSource = tmpEntity;
				else
					entity.CompanyIdSource = DataRepository.CompaniesHrMapProvider.GetByCompanyId(transactionManager, entity.CompanyId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CompanyIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.CompanyIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.CompaniesHrMapProvider.DeepLoad(transactionManager, entity.CompanyIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion CompanyIdSource

			#region SiteIdSource	
			if (CanDeepLoad(entity, "Sites|SiteIdSource", deepLoadType, innerList) 
				&& entity.SiteIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.SiteId;
				Sites tmpEntity = EntityManager.LocateEntity<Sites>(EntityLocator.ConstructKeyFromPkItems(typeof(Sites), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.SiteIdSource = tmpEntity;
				else
					entity.SiteIdSource = DataRepository.SitesProvider.GetBySiteId(transactionManager, entity.SiteId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'SiteIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.SiteIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.SitesProvider.DeepLoad(transactionManager, entity.SiteIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion SiteIdSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.CompaniesHrCrpData object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.CompaniesHrCrpData instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.CompaniesHrCrpData Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.CompaniesHrCrpData entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region CompanyIdSource
			if (CanDeepSave(entity, "CompaniesHrMap|CompanyIdSource", deepSaveType, innerList) 
				&& entity.CompanyIdSource != null)
			{
				DataRepository.CompaniesHrMapProvider.Save(transactionManager, entity.CompanyIdSource);
				entity.CompanyId = entity.CompanyIdSource.CompanyId;
			}
			#endregion 
			
			#region SiteIdSource
			if (CanDeepSave(entity, "Sites|SiteIdSource", deepSaveType, innerList) 
				&& entity.SiteIdSource != null)
			{
				DataRepository.SitesProvider.Save(transactionManager, entity.SiteIdSource);
				entity.SiteId = entity.SiteIdSource.SiteId;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region CompaniesHrCrpDataChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.CompaniesHrCrpData</c>
	///</summary>
	public enum CompaniesHrCrpDataChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>CompaniesHrMap</c> at CompanyIdSource
		///</summary>
		[ChildEntityType(typeof(CompaniesHrMap))]
		CompaniesHrMap,
			
		///<summary>
		/// Composite Property for <c>Sites</c> at SiteIdSource
		///</summary>
		[ChildEntityType(typeof(Sites))]
		Sites,
		}
	
	#endregion CompaniesHrCrpDataChildEntityTypes
	
	#region CompaniesHrCrpDataFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;CompaniesHrCrpDataColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CompaniesHrCrpData"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompaniesHrCrpDataFilterBuilder : SqlFilterBuilder<CompaniesHrCrpDataColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompaniesHrCrpDataFilterBuilder class.
		/// </summary>
		public CompaniesHrCrpDataFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the CompaniesHrCrpDataFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CompaniesHrCrpDataFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CompaniesHrCrpDataFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CompaniesHrCrpDataFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CompaniesHrCrpDataFilterBuilder
	
	#region CompaniesHrCrpDataParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;CompaniesHrCrpDataColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CompaniesHrCrpData"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompaniesHrCrpDataParameterBuilder : ParameterizedSqlFilterBuilder<CompaniesHrCrpDataColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompaniesHrCrpDataParameterBuilder class.
		/// </summary>
		public CompaniesHrCrpDataParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the CompaniesHrCrpDataParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CompaniesHrCrpDataParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CompaniesHrCrpDataParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CompaniesHrCrpDataParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CompaniesHrCrpDataParameterBuilder
	
	#region CompaniesHrCrpDataSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;CompaniesHrCrpDataColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CompaniesHrCrpData"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class CompaniesHrCrpDataSortBuilder : SqlSortBuilder<CompaniesHrCrpDataColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompaniesHrCrpDataSqlSortBuilder class.
		/// </summary>
		public CompaniesHrCrpDataSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion CompaniesHrCrpDataSortBuilder
	
} // end namespace
