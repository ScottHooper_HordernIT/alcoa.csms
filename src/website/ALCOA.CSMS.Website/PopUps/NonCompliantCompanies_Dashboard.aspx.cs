﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Text;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using KaiZen.CSMS.Web;
using KaiZen.CSMS.Services;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxTabControl;
using DevExpress.Web.Data;
using DevExpress.Web.ASPxGridView;
using DevExpress.XtraPrinting;
using System.Collections.Generic;
using model = Repo.CSMS.DAL.EntityModels;


namespace ALCOA.CSMS.Website.PopUps
{
    public partial class NonCompliantCompanies_Dashboard : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string kpi = (Request.QueryString["kpi"] == null ? "0" : Request.QueryString["kpi"]);
            kpi = Utility.GetKpiByIndex(Convert.ToInt32(kpi));

            int site = Convert.ToInt32(Request.QueryString["site"].ToString());
            //lblKpi.Text = kpi + " " + site;
            int year = Convert.ToInt32(Request.QueryString["Dash_Year"].ToString());
            int month = Convert.ToInt32(Request.QueryString["Dash_Month"].ToString()); 
            DataSet dsKpiNonCompliance = null;
            //DataSet PlanScore = (DataSet)Session["dsPlan_S"];
            //DataSet PlanScore = DataRepository.KpiProvider.Plan(year, month);
            //dsKpiNonCompliance = DataRepository.KpiProvider.KpiNonCompliancePopups(site, year, month);
            
           

            if (kpi.Equals("Lost Work Days Frequency Rate   LWDFR"))
            {
                string ItemName = "LWDFR";
                string CmpName = null;
                dsKpiNonCompliance = DataRepository.KpiProvider.Get_NonCompliant_Companies(site, year, month, ItemName);
                if (dsKpiNonCompliance.Tables[0].Rows.Count > 0)
                {

                    //grid.DataSource = dsKpiNonCompliance.Tables[0];
                    //grid.DataBind();
                    CmpName = dsKpiNonCompliance.Tables[0].Rows[0]["NonCompliantCompanies"].ToString();

                    dsKpiNonCompliance = null;
                }
                addNonConpliantCompanies(CmpName);
                //if (dsKpiNonCompliance.Tables[0].Rows.Count > 0)
                //{
                //    DataView dvData = new DataView(dsKpiNonCompliance.Tables[0]);
                //    double  Val =Convert.ToDouble(PlanScore.Tables[0].Rows[0]["PlanValue"].ToString());//Change
                //    dvData.RowFilter = "LWDFR <> '" + Val + "'";//Change


                //    grid.DataSource = dvData.ToTable();
                //    grid.DataBind();
                //}
            }
            else if (kpi.Equals("Total Recordable Injuries Frequency Rate    TRIFR"))
            {
                string ItemName = "TRIFR";
                string CmpName = null;
                dsKpiNonCompliance = DataRepository.KpiProvider.Get_NonCompliant_Companies(site, year, month, ItemName);
                if (dsKpiNonCompliance.Tables[0].Rows.Count > 0)
                {
                    CmpName = dsKpiNonCompliance.Tables[0].Rows[0]["NonCompliantCompanies"].ToString();
                    dsKpiNonCompliance = null;
                }
                addNonConpliantCompanies(CmpName);
                
            }
            else if (kpi.Equals("All Injury Frequency Rate   AIFR"))
            {
                string ItemName = "AIFR";
                string CmpName = null;
                dsKpiNonCompliance = DataRepository.KpiProvider.Get_NonCompliant_Companies(site, year, month, ItemName);
                if (dsKpiNonCompliance.Tables[0].Rows.Count > 0)
                {
                    CmpName = dsKpiNonCompliance.Tables[0].Rows[0]["NonCompliantCompanies"].ToString();
                    dsKpiNonCompliance = null;
                }
                addNonConpliantCompanies(CmpName);
            }
            else if (kpi.Equals("DART"))
            {
                string ItemName = "DART";
                string CmpName = null;
                dsKpiNonCompliance = DataRepository.KpiProvider.Get_NonCompliant_Companies(site, year, month, ItemName);
                if (dsKpiNonCompliance.Tables[0].Rows.Count > 0)
                {
                    CmpName = dsKpiNonCompliance.Tables[0].Rows[0]["NonCompliantCompanies"].ToString();
                    dsKpiNonCompliance = null;
                }
                addNonConpliantCompanies(CmpName);
            }
            else if (kpi.Equals("Lost Work Day   LWD"))
            {
                string ItemName = "LWD";
                string CmpName = null;
                dsKpiNonCompliance = DataRepository.KpiProvider.Get_NonCompliant_Companies(site, year, month, ItemName);
                if (dsKpiNonCompliance.Tables[0].Rows.Count > 0)
                {
                    CmpName = dsKpiNonCompliance.Tables[0].Rows[0]["NonCompliantCompanies"].ToString();
                    if (CmpName != "")
                    CmpName = CmpName.Substring(1);
                    dsKpiNonCompliance = null;
                }
                addNonConpliantCompanies(CmpName);
            }
            else if (kpi.Equals("Restricted Duty   RST"))
            {
                string ItemName = "RST";
                string CmpName = null;
                dsKpiNonCompliance = DataRepository.KpiProvider.Get_NonCompliant_Companies(site, year, month, ItemName);
                if (dsKpiNonCompliance.Tables[0].Rows.Count > 0)
                {
                    CmpName = dsKpiNonCompliance.Tables[0].Rows[0]["NonCompliantCompanies"].ToString();
                    if (CmpName != "")
                    CmpName = CmpName.Substring(1);
                    dsKpiNonCompliance = null;
                }
                addNonConpliantCompanies(CmpName);
            }
            else if (kpi.Equals("Medical Treatments   MT"))
            {
                string ItemName = "MT";
                string CmpName = null;
                dsKpiNonCompliance = DataRepository.KpiProvider.Get_NonCompliant_Companies(site, year, month, ItemName);
                if (dsKpiNonCompliance.Tables[0].Rows.Count > 0)
                {
                    CmpName = dsKpiNonCompliance.Tables[0].Rows[0]["NonCompliantCompanies"].ToString();
                    if (CmpName != "")
                    CmpName = CmpName.Substring(1);
                    dsKpiNonCompliance = null;
                }
                addNonConpliantCompanies(CmpName);
            }
            else if (kpi.Equals("First Aids   FA"))
            {
                string ItemName = "FA";
                string CmpName = null;
                dsKpiNonCompliance = DataRepository.KpiProvider.Get_NonCompliant_Companies(site, year, month, ItemName);
                if (dsKpiNonCompliance.Tables[0].Rows.Count > 0)
                {
                    CmpName = dsKpiNonCompliance.Tables[0].Rows[0]["NonCompliantCompanies"].ToString();
                    if (CmpName != "")
                    CmpName = CmpName.Substring(1);
                    dsKpiNonCompliance = null;
                }
                addNonConpliantCompanies(CmpName);
            }
            else if (kpi.Equals("Injury Free Events   IFE"))
            {
                string ItemName = "IFE";
                string CmpName = null;
                dsKpiNonCompliance = DataRepository.KpiProvider.Get_NonCompliant_Companies(site, year, month, ItemName);
                if (dsKpiNonCompliance.Tables[0].Rows.Count > 0)
                {
                    CmpName = dsKpiNonCompliance.Tables[0].Rows[0]["NonCompliantCompanies"].ToString();
                    if (CmpName != "")
                    CmpName = CmpName.Substring(1);
                    dsKpiNonCompliance = null;
                }
                addNonConpliantCompanies(CmpName);
            }
            else if (kpi.Equals("Risk Notifications"))
            {
                string ItemName = "RN";
                string CmpName = null;
                dsKpiNonCompliance = DataRepository.KpiProvider.Get_NonCompliant_Companies(site, year, month, ItemName);
                if (dsKpiNonCompliance.Tables[0].Rows.Count > 0)
                {
                    CmpName = dsKpiNonCompliance.Tables[0].Rows[0]["NonCompliantCompanies"].ToString();
                    if (CmpName != "")
                    CmpName = CmpName.Substring(1);
                    dsKpiNonCompliance = null;
                }
                addNonConpliantCompanies(CmpName);
            }
            else if (kpi.Equals("IFE / Injury ratio"))
            {
                string ItemName = "IFE Injury Ratio";
                string CmpName = null;
                dsKpiNonCompliance = DataRepository.KpiProvider.Get_NonCompliant_Companies(site, year, month, ItemName);
                if (dsKpiNonCompliance.Tables[0].Rows.Count > 0)
                {
                    CmpName = dsKpiNonCompliance.Tables[0].Rows[0]["NonCompliantCompanies"].ToString();
                    if (CmpName != "")
                    CmpName = CmpName.Substring(1);
                    dsKpiNonCompliance = null;
                }
                addNonConpliantCompanies(CmpName);
            }

            else if (kpi.Equals("RN / Injury ratio"))
            {
                string ItemName = "RN Injury Ratio";
                string CmpName = null;
                dsKpiNonCompliance = DataRepository.KpiProvider.Get_NonCompliant_Companies(site, year, month, ItemName);
                if (dsKpiNonCompliance.Tables[0].Rows.Count > 0)
                {
                    CmpName = dsKpiNonCompliance.Tables[0].Rows[0]["NonCompliantCompanies"].ToString();
                    if (CmpName != "")
                    CmpName = CmpName.Substring(1);
                    dsKpiNonCompliance = null;
                }
                addNonConpliantCompanies(CmpName);
            }

            else if (kpi.Equals("Expired - Currently With Procurement"))
            {
                string ItemName = "Expired - Currently With Procurement";
                string CmpName = null;
                dsKpiNonCompliance = DataRepository.KpiProvider.Get_NonCompliant_Companies(site, year, month, ItemName);
                if (dsKpiNonCompliance.Tables[0].Rows.Count > 0)
                {
                    CmpName = dsKpiNonCompliance.Tables[0].Rows[0]["NonCompliantCompanies"].ToString();
                    dsKpiNonCompliance = null;
                }
                addNonConpliantCompanies(CmpName);
            }
            else if (kpi.Equals("Expired - Currently With Supplier"))
            {
                string ItemName = "Expired - Currently With Supplier";
                string CmpName = null;
                dsKpiNonCompliance = DataRepository.KpiProvider.Get_NonCompliant_Companies(site, year, month, ItemName);
                if (dsKpiNonCompliance.Tables[0].Rows.Count > 0)
                {
                    CmpName = dsKpiNonCompliance.Tables[0].Rows[0]["NonCompliantCompanies"].ToString();
                    dsKpiNonCompliance = null;
                }
                addNonConpliantCompanies(CmpName);
            }
            else if (kpi.Equals("Expiring  > 7 days"))
            {
                string ItemName = "Expiring > 7 days";
                string CmpName = null;
                dsKpiNonCompliance = DataRepository.KpiProvider.Get_NonCompliant_Companies(site, year, month, ItemName);
                if (dsKpiNonCompliance.Tables[0].Rows.Count > 0)
                {
                    CmpName = dsKpiNonCompliance.Tables[0].Rows[0]["NonCompliantCompanies"].ToString();
                    dsKpiNonCompliance = null;
                }
                addNonConpliantCompanies(CmpName);
            }
            else if (kpi.Equals("Questionnaire  > 7 days"))
            {
                string ItemName = "Questionnaire  > 7 days";
                string CmpName = null;
                dsKpiNonCompliance = DataRepository.KpiProvider.Get_NonCompliant_Companies(site, year, month, ItemName);
                if (dsKpiNonCompliance.Tables[0].Rows.Count > 0)
                {
                    CmpName = dsKpiNonCompliance.Tables[0].Rows[0]["NonCompliantCompanies"].ToString();
                    dsKpiNonCompliance = null;
                }
                addNonConpliantCompanies(CmpName);
            }
            else if (kpi.Equals("Assign Company Status   > 7 days"))
            {
                string ItemName = "Assign Company Status > 7 days";
                string CmpName = null;
                dsKpiNonCompliance = DataRepository.KpiProvider.Get_NonCompliant_Companies(site, year, month, ItemName);
                if (dsKpiNonCompliance.Tables[0].Rows.Count > 0)
                {
                    CmpName = dsKpiNonCompliance.Tables[0].Rows[0]["NonCompliantCompanies"].ToString();
                    dsKpiNonCompliance = null;
                }
                addNonConpliantCompanies(CmpName);
            }
            else if (kpi.Equals("Questionnaire  > 28 Days"))
            {
                string ItemName = "Questionnaire > 28 Days";
                string CmpName = null;
                dsKpiNonCompliance = DataRepository.KpiProvider.Get_NonCompliant_Companies(site, year, month, ItemName);
                if (dsKpiNonCompliance.Tables[0].Rows.Count > 0)
                {
                    CmpName = dsKpiNonCompliance.Tables[0].Rows[0]["NonCompliantCompanies"].ToString();
                    dsKpiNonCompliance = null;
                }
                addNonConpliantCompanies(CmpName);
            }

            else if (kpi.Equals("Questionnaire - Resubmitting"))
            {
                string ItemName = "Questionnaire - Resubmitting";
                string CmpName = null;
                dsKpiNonCompliance = DataRepository.KpiProvider.Get_NonCompliant_Companies(site, year, month, ItemName);
                if (dsKpiNonCompliance.Tables[0].Rows.Count > 0)
                {
                    CmpName = dsKpiNonCompliance.Tables[0].Rows[0]["NonCompliantCompanies"].ToString();
                    dsKpiNonCompliance = null;
                }
                addNonConpliantCompanies(CmpName);
            }
            else if (kpi.Equals("Access > 7 Days"))
            {
                string ItemName = "Access > 7 Days";
                string CmpName = null;
                dsKpiNonCompliance = DataRepository.KpiProvider.Get_NonCompliant_Companies(site, year, month, ItemName);
                if (dsKpiNonCompliance.Tables[0].Rows.Count > 0)
                {
                    CmpName = dsKpiNonCompliance.Tables[0].Rows[0]["NonCompliantCompanies"].ToString();
                    dsKpiNonCompliance = null;
                }
                addNonConpliantCompanies(CmpName);
            }
            else if (kpi.Equals("Expired - Currently With H&S Assessor"))
            {
                string ItemName = "Expired - Currently With H&S Assessor";
                string CmpName = null;
                dsKpiNonCompliance = DataRepository.KpiProvider.Get_NonCompliant_Companies(site, year, month, ItemName);
                if (dsKpiNonCompliance.Tables[0].Rows.Count > 0)
                {
                    CmpName = dsKpiNonCompliance.Tables[0].Rows[0]["NonCompliantCompanies"].ToString();
                    dsKpiNonCompliance = null;
                }
                addNonConpliantCompanies(CmpName);
            }
            else if (kpi.Equals("Assessing Procurement Questionnaire > 7 days"))
            {
                string ItemName = "Assessing Procurement Questionnaire > 7 days";
                string CmpName = null;
                dsKpiNonCompliance = DataRepository.KpiProvider.Get_NonCompliant_Companies(site, year, month, ItemName);
                if (dsKpiNonCompliance.Tables[0].Rows.Count > 0)
                {
                    CmpName = dsKpiNonCompliance.Tables[0].Rows[0]["NonCompliantCompanies"].ToString();
                    dsKpiNonCompliance = null;
                }
                addNonConpliantCompanies(CmpName);
            }

            else if (kpi.Equals("Assessing Questionnaire > 7 days"))
            {
                string ItemName = "Assessing Questionnaire > 7 days";
                string CmpName = null;
                dsKpiNonCompliance = DataRepository.KpiProvider.Get_NonCompliant_Companies(site, year, month, ItemName);
                if (dsKpiNonCompliance.Tables[0].Rows.Count > 0)
                {
                    CmpName = dsKpiNonCompliance.Tables[0].Rows[0]["NonCompliantCompanies"].ToString();
                    dsKpiNonCompliance = null;
                }
                addNonConpliantCompanies(CmpName);
            }
            else if (kpi.Equals("SQ process total"))
            {
                string ItemName = "SQ process total";
                string CmpName = null;
                dsKpiNonCompliance = DataRepository.KpiProvider.Get_NonCompliant_Companies(site, year, month, ItemName);
                if (dsKpiNonCompliance.Tables[0].Rows.Count > 0)
                {
                    CmpName = dsKpiNonCompliance.Tables[0].Rows[0]["NonCompliantCompanies"].ToString();
                    dsKpiNonCompliance = null;
                }
                addNonConpliantCompanies(CmpName);
            }
            ///


            else if (kpi.Equals("2) Yes - SQ Current (expiring. in 60 days) / Site access granted"))
            {
                string ItemName = "Yes - SQ Current";
                string CmpName = null;
                dsKpiNonCompliance = DataRepository.KpiProvider.Get_NonCompliant_Companies(site, year, month, ItemName);
                if (dsKpiNonCompliance.Tables[0].Rows.Count > 0)
                {
                    CmpName = dsKpiNonCompliance.Tables[0].Rows[0]["NonCompliantCompanies"].ToString();
                    dsKpiNonCompliance = null;
                }
                addNonConpliantCompanies(CmpName);
            }

            else if (kpi.Equals("3) No - Current SQ not found / Access to Site Not Granted"))
            {
                string ItemName = "No - Current SQ not found";
                string CmpName = null;
                dsKpiNonCompliance = DataRepository.KpiProvider.Get_NonCompliant_Companies(site, year, month, ItemName);
                if (dsKpiNonCompliance.Tables[0].Rows.Count > 0)
                {
                    CmpName = dsKpiNonCompliance.Tables[0].Rows[0]["NonCompliantCompanies"].ToString();
                    dsKpiNonCompliance = null;
                }
                addNonConpliantCompanies(CmpName);
            }

            else if (kpi.Equals("4) No - SQ Expired / Access to Site Not Granted"))
            {
                string ItemName = "No - SQ Expired";
                string CmpName = null;
                dsKpiNonCompliance = DataRepository.KpiProvider.Get_NonCompliant_Companies(site, year, month, ItemName);
                if (dsKpiNonCompliance.Tables[0].Rows.Count > 0)
                {
                    CmpName = dsKpiNonCompliance.Tables[0].Rows[0]["NonCompliantCompanies"].ToString();
                    dsKpiNonCompliance = null;
                }
                addNonConpliantCompanies(CmpName);
            }

            else if (kpi.Equals("5) No - Access to Site Not Granted"))
            {
                string ItemName = "No - Access to Site Not Granted";
                string CmpName = null;
                dsKpiNonCompliance = DataRepository.KpiProvider.Get_NonCompliant_Companies(site, year, month, ItemName);
                if (dsKpiNonCompliance.Tables[0].Rows.Count > 0)
                {
                    CmpName = dsKpiNonCompliance.Tables[0].Rows[0]["NonCompliantCompanies"].ToString();
                    dsKpiNonCompliance = null;
                }
                addNonConpliantCompanies(CmpName);
            }
            else if (kpi.Equals("6) No - Safety Qualification not found"))
            {
                string ItemName = "No - Safety Qualification not found";
                string CmpName = null;
                dsKpiNonCompliance = DataRepository.KpiProvider.Get_NonCompliant_Companies(site, year, month, ItemName);
                if (dsKpiNonCompliance.Tables[0].Rows.Count > 0)
                {
                    CmpName = dsKpiNonCompliance.Tables[0].Rows[0]["NonCompliantCompanies"].ToString();
                    dsKpiNonCompliance = null;
                }
                addNonConpliantCompanies(CmpName);
            }
            else if (kpi.Equals("7) No - Information could not be found"))
            {
                string ItemName = "No - Information could not be found";
                string CmpName = null;
                dsKpiNonCompliance = DataRepository.KpiProvider.Get_NonCompliant_Companies(site, year, month, ItemName);
                if (dsKpiNonCompliance.Tables[0].Rows.Count > 0)
                {
                    CmpName = dsKpiNonCompliance.Tables[0].Rows[0]["NonCompliantCompanies"].ToString();
                    dsKpiNonCompliance = null;
                }
                addNonConpliantCompanies(CmpName);
            }

            else if (kpi.Equals("8) Company has a valid SQ exemption"))
            {
                string ItemName = "Company has a valid SQ Exemption";
                string CmpName = null;
                dsKpiNonCompliance = DataRepository.KpiProvider.Get_NonCompliant_Companies(site, year, month, ItemName);
                if (dsKpiNonCompliance.Tables[0].Rows.Count > 0)
                {
                    CmpName = dsKpiNonCompliance.Tables[0].Rows[0]["NonCompliantCompanies"].ToString();
                    dsKpiNonCompliance = null;
                }
                addNonConpliantCompanies(CmpName);
            }
                ///
            else if (kpi.Equals("Required (# Companies)"))
            {
                string ItemName = "HS Assessors – Required (# Companies)";
                string CmpName = null;
                dsKpiNonCompliance = DataRepository.KpiProvider.Get_NonCompliant_Companies(site, year, month, ItemName);
                if (dsKpiNonCompliance.Tables[0].Rows.Count > 0)
                {
                    CmpName = dsKpiNonCompliance.Tables[0].Rows[0]["NonCompliantCompanies"].ToString();
                    dsKpiNonCompliance = null;
                }
                addNonConpliantCompanies(CmpName);
            }
            else if (kpi.Equals("Being Assessed (# Companies)"))
            {
                string ItemName = "HS Assessors – Being Assessed (# Companies)";
                string CmpName = null;
                dsKpiNonCompliance = DataRepository.KpiProvider.Get_NonCompliant_Companies(site, year, month, ItemName);
                if (dsKpiNonCompliance.Tables[0].Rows.Count > 0)
                {
                    CmpName = dsKpiNonCompliance.Tables[0].Rows[0]["NonCompliantCompanies"].ToString();
                    dsKpiNonCompliance = null;
                }
                addNonConpliantCompanies(CmpName);
            }
            else if (kpi.Equals("Awaiting Assignment (# Companies)"))
            {
                string ItemName = "HS Assessors – Awaiting Assignment";
                string CmpName = null;
                dsKpiNonCompliance = DataRepository.KpiProvider.Get_NonCompliant_Companies(site, year, month, ItemName);
                if (dsKpiNonCompliance.Tables[0].Rows.Count > 0)
                {
                    CmpName = dsKpiNonCompliance.Tables[0].Rows[0]["NonCompliantCompanies"].ToString();
                    dsKpiNonCompliance = null;
                }
                addNonConpliantCompanies(CmpName);
            }
            else if (kpi.Equals("Approved"))
            {
                string ItemName = "HS Assessors – Approved";
                string CmpName = null;
                dsKpiNonCompliance = DataRepository.KpiProvider.Get_NonCompliant_Companies(site, year, month, ItemName);
                if (dsKpiNonCompliance.Tables[0].Rows.Count > 0)
                {
                    CmpName = dsKpiNonCompliance.Tables[0].Rows[0]["NonCompliantCompanies"].ToString();
                    dsKpiNonCompliance = null;
                }
                addNonConpliantCompanies(CmpName);
            }

            else if (kpi.Equals("Approved %"))
            {
                string ItemName = "HS Assessors – Approved %";
                string CmpName = null;
                dsKpiNonCompliance = DataRepository.KpiProvider.Get_NonCompliant_Companies(site, year, month, ItemName);
                if (dsKpiNonCompliance.Tables[0].Rows.Count > 0)
                {
                    CmpName = dsKpiNonCompliance.Tables[0].Rows[0]["NonCompliantCompanies"].ToString();
                    dsKpiNonCompliance = null;
                }
                addNonConpliantCompanies(CmpName);
            }
            else if (kpi.Equals("Contractor Services Audit - Self (Conducted)"))
            {
                string ItemName = "Contractor Services Audit";
                string CmpName = null;
                dsKpiNonCompliance = DataRepository.KpiProvider.Get_NonCompliant_Companies(site, year, month, ItemName);
                if (dsKpiNonCompliance.Tables[0].Rows.Count > 0)
                {
                    CmpName = dsKpiNonCompliance.Tables[0].Rows[0]["NonCompliantCompanies"].ToString();
                    dsKpiNonCompliance = null;
                }
                addNonConpliantCompanies(CmpName);
                //AdHocRadarItemsService ahriService = new AdHocRadarItemsService();
                //AdHocRadarItems ahri = ahriService.GetByYearAdHocRadarIdSiteId(year,8,site);
                //string CompaniesNotComplaint=string.Empty;
                //if(ahri !=null)
                //CompaniesNotComplaint = ahri.CompaniesNotCompliant;
                //string[] cList=CompaniesNotComplaint.Split(',');
                //DataTable dtCompanies=new DataTable();
                //dtCompanies.Columns.Add("CompanyName");
                //foreach (string str in cList)
                //{
                //    dtCompanies.Rows.Add(str);
                //}
                //grid.DataSource = dtCompanies;
                //grid.DataBind();

            }
            else if (kpi.Equals("Workplace Safety & Compliance"))
            {
                string ItemName = "Workplace Safety Compliance";
                string CmpName = null;
                dsKpiNonCompliance = DataRepository.KpiProvider.Get_NonCompliant_Companies(site, year, month, ItemName);
                if (dsKpiNonCompliance.Tables[0].Rows.Count > 0)
                {
                    CmpName = dsKpiNonCompliance.Tables[0].Rows[0]["NonCompliantCompanies"].ToString();
                    dsKpiNonCompliance = null;
                }
                addNonConpliantCompanies(CmpName);
            }
            else if (kpi.Equals("Management Health Safety Work Contacts"))
            {
                string ItemName = "Management Health Safety Work Contacts";
                string CmpName = null;
                dsKpiNonCompliance = DataRepository.KpiProvider.Get_NonCompliant_Companies(site, year, month, ItemName);
                if (dsKpiNonCompliance.Tables[0].Rows.Count > 0)
                {
                    CmpName = dsKpiNonCompliance.Tables[0].Rows[0]["NonCompliantCompanies"].ToString();
                    dsKpiNonCompliance = null;
                }
                addNonConpliantCompanies(CmpName);
            }
            else if (kpi.Equals("Behavioural Observations"))
            {
                  string ItemName = "Behavioural Observations";
                  string CmpName = null;
                  dsKpiNonCompliance = DataRepository.KpiProvider.Get_NonCompliant_Companies(site, year, month, ItemName);
                  if (dsKpiNonCompliance.Tables[0].Rows.Count > 0)
                  {
                      CmpName = dsKpiNonCompliance.Tables[0].Rows[0]["NonCompliantCompanies"].ToString();
                      dsKpiNonCompliance = null;
                  }
                  addNonConpliantCompanies(CmpName);
            }
             
            else if (kpi.Equals("Fatality Prevention Program"))
            {
                string ItemName = "Fatality Prevention Program";
                string CmpName = null;
                dsKpiNonCompliance = DataRepository.KpiProvider.Get_NonCompliant_Companies(site, year, month, ItemName);
                if (dsKpiNonCompliance.Tables[0].Rows.Count > 0)
                {
                    CmpName = dsKpiNonCompliance.Tables[0].Rows[0]["NonCompliantCompanies"].ToString();
                    dsKpiNonCompliance = null;
                }
                addNonConpliantCompanies(CmpName);
            }
            else if (kpi.Equals("JSA Field Audit Verifications"))
            {
                string ItemName = "JSA Field Audit Verifications";
                string CmpName = null;
                dsKpiNonCompliance = DataRepository.KpiProvider.Get_NonCompliant_Companies(site, year, month, ItemName);
                if (dsKpiNonCompliance.Tables[0].Rows.Count > 0)
                {
                    CmpName = dsKpiNonCompliance.Tables[0].Rows[0]["NonCompliantCompanies"].ToString();
                    dsKpiNonCompliance = null;
                }
                addNonConpliantCompanies(CmpName);
            }
            else if (kpi.Equals("Toolbox Meetings"))
            {
                string ItemName = "Toolbox Meetings";
                string CmpName = null;
                dsKpiNonCompliance = DataRepository.KpiProvider.Get_NonCompliant_Companies(site, year, month, ItemName);
                if (dsKpiNonCompliance.Tables[0].Rows.Count > 0)
                {
                    CmpName = dsKpiNonCompliance.Tables[0].Rows[0]["NonCompliantCompanies"].ToString();
                    dsKpiNonCompliance = null;
                }
                addNonConpliantCompanies(CmpName);
            }
            else if (kpi.Equals("Alcoa Weekly Contractors Meeting"))
            {
                string ItemName = "Alcoa Weekly Contractors Meeting";
                string CmpName = null;
                dsKpiNonCompliance = DataRepository.KpiProvider.Get_NonCompliant_Companies(site, year, month, ItemName);
                if (dsKpiNonCompliance.Tables[0].Rows.Count > 0)
                {
                    CmpName = dsKpiNonCompliance.Tables[0].Rows[0]["NonCompliantCompanies"].ToString();
                    dsKpiNonCompliance = null;
                }
                addNonConpliantCompanies(CmpName);
            }
            else if (kpi.Equals("Alcoa Monthly Contractors Meeting"))
            {
                string ItemName = "Alcoa Monthly Contractors Meeting";
                string CmpName = null;
                dsKpiNonCompliance = DataRepository.KpiProvider.Get_NonCompliant_Companies(site, year, month, ItemName);
                if (dsKpiNonCompliance.Tables[0].Rows.Count > 0)
                {
                    CmpName = dsKpiNonCompliance.Tables[0].Rows[0]["NonCompliantCompanies"].ToString();
                    dsKpiNonCompliance = null;
                }
                addNonConpliantCompanies(CmpName);
            }
            else if (kpi.Equals("Safety Plan(s) Submitted"))
            {
                string ItemName = "Safety Plan(s) Submitted";
                string CmpName = null;
                dsKpiNonCompliance = DataRepository.KpiProvider.Get_NonCompliant_Companies(site, year, month, ItemName);
                if (dsKpiNonCompliance.Tables[0].Rows.Count > 0)
                {
                    CmpName = dsKpiNonCompliance.Tables[0].Rows[0]["NonCompliantCompanies"].ToString();
                    dsKpiNonCompliance = null;
                }
                addNonConpliantCompanies(CmpName);
            }
            else if (kpi.Equals("% Mandated Training"))
            {
                string ItemName = "Mandated Training";
                string CmpName = null;
                dsKpiNonCompliance = DataRepository.KpiProvider.Get_NonCompliant_Companies(site, year, month, ItemName);
                if (dsKpiNonCompliance.Tables[0].Rows.Count > 0)
                {
                    CmpName = dsKpiNonCompliance.Tables[0].Rows[0]["NonCompliantCompanies"].ToString();
                    dsKpiNonCompliance = null;
                }
                addNonConpliantCompanies(CmpName);
            }
            //Nonembeded
            else if (kpi.Equals("Contractor Services Audit - Self (Conducted) - NE1"))
            {
                string ItemName = "NE_Contractor Services Audit";
                string CmpName = null;
                dsKpiNonCompliance = DataRepository.KpiProvider.Get_NonCompliant_Companies(site, year, month, ItemName);
                if (dsKpiNonCompliance.Tables[0].Rows.Count > 0)
                {
                    CmpName = dsKpiNonCompliance.Tables[0].Rows[0]["NonCompliantCompanies"].ToString();
                    dsKpiNonCompliance = null;
                }
                addNonConpliantCompanies(CmpName);

            }
            else if (kpi.Equals("Workplace Safety & Compliance - NE1"))
            {
                string ItemName = "NE_Workplace Safety Compliance";
                string CmpName = null;
                dsKpiNonCompliance = DataRepository.KpiProvider.Get_NonCompliant_Companies(site, year, month, ItemName);
                if (dsKpiNonCompliance.Tables[0].Rows.Count > 0)
                {
                    CmpName = dsKpiNonCompliance.Tables[0].Rows[0]["NonCompliantCompanies"].ToString();
                    dsKpiNonCompliance = null;
                }
                addNonConpliantCompanies(CmpName);
            }
            else if (kpi.Equals("Management Health Safety Work Contacts - NE1"))
            {
                string ItemName = "NE_Management Health Safety Work Contacts";
                string CmpName = null;
                dsKpiNonCompliance = DataRepository.KpiProvider.Get_NonCompliant_Companies(site, year, month, ItemName);
                if (dsKpiNonCompliance.Tables[0].Rows.Count > 0)
                {
                    CmpName = dsKpiNonCompliance.Tables[0].Rows[0]["NonCompliantCompanies"].ToString();
                    dsKpiNonCompliance = null;
                }
                addNonConpliantCompanies(CmpName);
            }
            else if (kpi.Equals("Behavioural Observations - NE1"))
            {
                string ItemName = "NE_Behavioural Observations";
                string CmpName = null;
                dsKpiNonCompliance = DataRepository.KpiProvider.Get_NonCompliant_Companies(site, year, month, ItemName);
                if (dsKpiNonCompliance.Tables[0].Rows.Count > 0)
                {
                    CmpName = dsKpiNonCompliance.Tables[0].Rows[0]["NonCompliantCompanies"].ToString();
                    dsKpiNonCompliance = null;
                }
                addNonConpliantCompanies(CmpName);
            }

            else if (kpi.Equals("JSA Field Audit Verifications - NE1"))
            {
                string ItemName = "NE_JSA Field Audit Verifications";
                string CmpName = null;
                dsKpiNonCompliance = DataRepository.KpiProvider.Get_NonCompliant_Companies(site, year, month, ItemName);
                if (dsKpiNonCompliance.Tables[0].Rows.Count > 0)
                {
                    CmpName = dsKpiNonCompliance.Tables[0].Rows[0]["NonCompliantCompanies"].ToString();
                    dsKpiNonCompliance = null;
                }
                addNonConpliantCompanies(CmpName);
            }
            else if (kpi.Equals("Toolbox Meetings - NE1"))
            {
                string ItemName = "NE_Toolbox Meetings";
                string CmpName = null;
                dsKpiNonCompliance = DataRepository.KpiProvider.Get_NonCompliant_Companies(site, year, month, ItemName);
                if (dsKpiNonCompliance.Tables[0].Rows.Count > 0)
                {
                    CmpName = dsKpiNonCompliance.Tables[0].Rows[0]["NonCompliantCompanies"].ToString();
                    dsKpiNonCompliance = null;
                }
                addNonConpliantCompanies(CmpName);
            }
            else if (kpi.Equals("Alcoa Weekly Contractors Meeting - NE1"))
            {
                string ItemName = "NE_Alcoa Weekly Contractors Meeting";
                string CmpName = null;
                dsKpiNonCompliance = DataRepository.KpiProvider.Get_NonCompliant_Companies(site, year, month, ItemName);
                if (dsKpiNonCompliance.Tables[0].Rows.Count > 0)
                {
                    CmpName = dsKpiNonCompliance.Tables[0].Rows[0]["NonCompliantCompanies"].ToString();
                    dsKpiNonCompliance = null;
                }
                addNonConpliantCompanies(CmpName);
            }
            else if (kpi.Equals("Alcoa Monthly Contractors Meeting - NE1"))
            {
                string ItemName = "NE_Alcoa Monthly Contractors Meeting";
                string CmpName = null;
                dsKpiNonCompliance = DataRepository.KpiProvider.Get_NonCompliant_Companies(site, year, month, ItemName);
                if (dsKpiNonCompliance.Tables[0].Rows.Count > 0)
                {
                    CmpName = dsKpiNonCompliance.Tables[0].Rows[0]["NonCompliantCompanies"].ToString();
                    dsKpiNonCompliance = null;
                }
                addNonConpliantCompanies(CmpName);
            }
            else if (kpi.Equals("% Mandated Training - NE1"))
            {
                string ItemName = "NE_Mandated Training";
                string CmpName = null;
                dsKpiNonCompliance = DataRepository.KpiProvider.Get_NonCompliant_Companies(site, year, month, ItemName);
                if (dsKpiNonCompliance.Tables[0].Rows.Count > 0)
                {
                    CmpName = dsKpiNonCompliance.Tables[0].Rows[0]["NonCompliantCompanies"].ToString();
                    dsKpiNonCompliance = null;
                }
                addNonConpliantCompanies(CmpName);
            }
            else if (kpi.Equals("Required to date (month)"))
            {
                string ItemName = "CSA Embedded – Required to date";
                string CmpName = null;
                dsKpiNonCompliance = DataRepository.KpiProvider.Get_NonCompliant_Companies(site, year, month, ItemName);
                if (dsKpiNonCompliance.Tables[0].Rows.Count > 0)
                {
                    CmpName = dsKpiNonCompliance.Tables[0].Rows[0]["NonCompliantCompanies"].ToString();
                    dsKpiNonCompliance = null;
                }
                addNonConpliantCompanies(CmpName);
            }
            else if (kpi.Equals("Actual completed (Cumulative total)"))
            {
                string ItemName = "Embedded – Actual completed";
                string CmpName = null;
                dsKpiNonCompliance = DataRepository.KpiProvider.Get_NonCompliant_Companies(site, year, month, ItemName);
                if (dsKpiNonCompliance.Tables[0].Rows.Count > 0)
                {
                    CmpName = dsKpiNonCompliance.Tables[0].Rows[0]["NonCompliantCompanies"].ToString();
                    dsKpiNonCompliance = null;
                }
                addNonConpliantCompanies(CmpName);
            }
            else if (kpi.Equals("Required to date"))
            {
                string ItemName = "CSA – Non-Embedded 1 – Required to date";
                string CmpName = null;
                dsKpiNonCompliance = DataRepository.KpiProvider.Get_NonCompliant_Companies(site, year, month, ItemName);
                if (dsKpiNonCompliance.Tables[0].Rows.Count > 0)
                {
                    CmpName = dsKpiNonCompliance.Tables[0].Rows[0]["NonCompliantCompanies"].ToString();
                    dsKpiNonCompliance = null;
                }
                addNonConpliantCompanies(CmpName);
            }
            else if (kpi.Equals("Actual completed (Cumulative total) - NE1"))
            {
                string ItemName = "Non-Embedded 1 – Actual completed";
                string CmpName = null;
                dsKpiNonCompliance = DataRepository.KpiProvider.Get_NonCompliant_Companies(site, year, month, ItemName);
                if (dsKpiNonCompliance.Tables[0].Rows.Count > 0)
                {
                    CmpName = dsKpiNonCompliance.Tables[0].Rows[0]["NonCompliantCompanies"].ToString();
                    dsKpiNonCompliance = null;
                }
                addNonConpliantCompanies(CmpName);
            }
            
        }

        public void addNonConpliantCompanies(string CmpName)
        {
            //Modified by Ashley Goldstraw 23/12/2015 DT327 to hide inactive companies from popup
            Repo.CSMS.Service.Database.ICompanyService companyService = ALCOA.CSMS.Website.Global.GetInstance<Repo.CSMS.Service.Database.ICompanyService>(); //Added by AG DT327
            List<model.Company> companyList = companyService.GetMany(null, i => i.CompanyStatus.CompanyStatusName.Contains("InActive") == false, null, null);//Added by AG DT327.  Get a list of the Active Companies
            string[] cList = null;
            DataTable dtCompanies = new DataTable();
            dtCompanies.Columns.Add("CompanyName");

            if (CmpName != null && CmpName != "")
            {
                cList = CmpName.Split(',');


                foreach (string str in cList)
                {
                    if (companyList.Find(c => c.CompanyName == str) != null) //Added by AG DT327.  Check the Company is not inactive.
                    { 
                        dtCompanies.Rows.Add(str); 
                    }
                }
            }
            //else
            //{
            //    dtCompanies.Rows.Add(str);
            //}
            grid.DataSource = dtCompanies;
            grid.DataBind();
        }
    }
}