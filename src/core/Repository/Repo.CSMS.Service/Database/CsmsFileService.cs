﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface ICsmsFileService
    {
        CsmsFile Get(Expression<Func<CsmsFile, bool>> where, List<Expression<Func<CsmsFile, object>>> includes);
        List<CsmsFile> GetMany(int? top, Expression<Func<CsmsFile, bool>> where, Func<IQueryable<CsmsFile>, IOrderedQueryable<CsmsFile>> orderBy, List<Expression<Func<CsmsFile, object>>> includes);
        List<CsmsFile> GetManyPaged(int pageNumber, int pageSize, Expression<Func<CsmsFile, bool>> where, Func<IQueryable<CsmsFile>, IOrderedQueryable<CsmsFile>> orderBy, List<Expression<Func<CsmsFile, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<CsmsFile, bool>> where, List<Expression<Func<CsmsFile, object>>> includes);
        void Insert(CsmsFile item, bool saveChanges = true);
        void Update(CsmsFile item, bool saveChanges = true);
        void Delete(CsmsFile item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class CsmsFileService : ICsmsFileService
    {
        private readonly IRepository<CsmsFile> _repository;

        public CsmsFileService(IRepository<CsmsFile> repository)
        {
            this._repository = repository;
        }

        public CsmsFile Get(Expression<Func<CsmsFile, bool>> where, List<Expression<Func<CsmsFile, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<CsmsFile> GetMany(int? top, Expression<Func<CsmsFile, bool>> where, Func<IQueryable<CsmsFile>, IOrderedQueryable<CsmsFile>> orderBy, List<Expression<Func<CsmsFile, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<CsmsFile> GetManyPaged(int pageNumber, int pageSize, Expression<Func<CsmsFile, bool>> where, Func<IQueryable<CsmsFile>, IOrderedQueryable<CsmsFile>> orderBy, List<Expression<Func<CsmsFile, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<CsmsFile, bool>> where, List<Expression<Func<CsmsFile, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(CsmsFile item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("CsmsFile");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(CsmsFile item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("CsmsFile");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(CsmsFile item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("CsmsFile");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
