﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface IFileVaultTableService
    {
        FileVaultTable Get(Expression<Func<FileVaultTable, bool>> where, List<Expression<Func<FileVaultTable, object>>> includes);
        List<FileVaultTable> GetMany(int? top, Expression<Func<FileVaultTable, bool>> where, Func<IQueryable<FileVaultTable>, IOrderedQueryable<FileVaultTable>> orderBy, List<Expression<Func<FileVaultTable, object>>> includes);
        List<FileVaultTable> GetManyPaged(int pageNumber, int pageSize, Expression<Func<FileVaultTable, bool>> where, Func<IQueryable<FileVaultTable>, IOrderedQueryable<FileVaultTable>> orderBy, List<Expression<Func<FileVaultTable, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<FileVaultTable, bool>> where, List<Expression<Func<FileVaultTable, object>>> includes);
        void Insert(FileVaultTable item, bool saveChanges = true);
        void Update(FileVaultTable item, bool saveChanges = true);
        void Delete(FileVaultTable item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class FileVaultTableService : IFileVaultTableService
    {
        private readonly IRepository<FileVaultTable> _repository;

        public FileVaultTableService(IRepository<FileVaultTable> repository)
        {
            this._repository = repository;
        }

        public FileVaultTable Get(Expression<Func<FileVaultTable, bool>> where, List<Expression<Func<FileVaultTable, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<FileVaultTable> GetMany(int? top, Expression<Func<FileVaultTable, bool>> where, Func<IQueryable<FileVaultTable>, IOrderedQueryable<FileVaultTable>> orderBy, List<Expression<Func<FileVaultTable, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<FileVaultTable> GetManyPaged(int pageNumber, int pageSize, Expression<Func<FileVaultTable, bool>> where, Func<IQueryable<FileVaultTable>, IOrderedQueryable<FileVaultTable>> orderBy, List<Expression<Func<FileVaultTable, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<FileVaultTable, bool>> where, List<Expression<Func<FileVaultTable, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(FileVaultTable item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("FileVaultTable");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(FileVaultTable item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("FileVaultTable");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(FileVaultTable item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("FileVaultTable");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
