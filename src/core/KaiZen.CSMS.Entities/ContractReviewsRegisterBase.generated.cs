﻿
#region using directives
using System;
using System.ComponentModel;
using System.Collections;
using System.Xml.Serialization;
using System.Runtime.Serialization;

using KaiZen.CSMS.Entities.Validation;
#endregion

namespace KaiZen.CSMS.Entities
{
	///<summary>
    /// An object representation of the 'ContractReviewsRegister' table. [No description found the database]	
	///</summary>
	[Serializable]
	[DataObject, CLSCompliant(true)]
    public abstract partial class ContractReviewsRegisterBase : EntityBase, IContractReviewsRegister, IEntityId<ContractReviewsRegisterKey>, System.IComparable, System.ICloneable, ICloneableEx, IEditableObject, IComponent, INotifyPropertyChanged
	{		
		#region Variable Declarations
		
		/// <summary>
		///  Hold the inner data of the entity.
		/// </summary>
		private ContractReviewsRegisterEntityData entityData;
		
		/// <summary>
		/// 	Hold the original data of the entity, as loaded from the repository.
		/// </summary>
		private ContractReviewsRegisterEntityData _originalData;
		
		/// <summary>
		/// 	Hold a backup of the inner data of the entity.
		/// </summary>
		private ContractReviewsRegisterEntityData backupData; 
		
		/// <summary>
		/// 	Key used if Tracking is Enabled for the <see cref="EntityLocator" />.
		/// </summary>
		private string entityTrackingKey;
		
		/// <summary>
		/// 	Hold the parent TList&lt;entity&gt; in which this entity maybe contained.
		/// </summary>
		/// <remark>Mostly used for databinding</remark>
		[NonSerialized]
		private TList<ContractReviewsRegister> parentCollection;
		
		private bool inTxn = false;
		
		/// <summary>
		/// Occurs when a value is being changed for the specified column.
		/// </summary>
		[field:NonSerialized]
		public event ContractReviewsRegisterEventHandler ColumnChanging;		
		
		/// <summary>
		/// Occurs after a value has been changed for the specified column.
		/// </summary>
		[field:NonSerialized]
		public event ContractReviewsRegisterEventHandler ColumnChanged;
		
		#endregion Variable Declarations
		
		#region Constructors
		///<summary>
		/// Creates a new <see cref="ContractReviewsRegisterBase"/> instance.
		///</summary>
		public ContractReviewsRegisterBase()
		{
            this.entityData = new ContractReviewsRegisterEntityData();
			this.backupData = null;
		}		
		
		///<summary>
        /// Creates a new <see cref="ContractReviewsRegisterBase"/> instance.
		///</summary>
		///<param name="_questionnaireId"></param>
		///<param name="_answerId"></param>
		///<param name="_fileName"></param>
		///<param name="_fileHash"></param>
		///<param name="_contentLength"></param>
		///<param name="_content"></param>
		///<param name="_modifiedByUserId"></param>
		///<param name="_modifiedDate"></param>
        public ContractReviewsRegisterBase(System.Int32 _contractId, System.Int32 _companyId,
			System.Int32 _siteId, System.Int32 _year, System.Boolean _january, System.Boolean _february,
            System.Boolean _march, System.Boolean _april, System.Boolean _may, System.Boolean _june,
            System.Boolean _july, System.Boolean _august, System.Boolean _september, System.Boolean _october,
            System.Boolean _november, System.Boolean _december, System.Int32 _yearCount, System.Int32? _locationSponsorUserId,
            System.Int32? _locationSpaUserId,
            System.Int32 _modifiedBy, System.DateTime _modifiedDate)
		{
            this.entityData = new ContractReviewsRegisterEntityData();
			this.backupData = null;
            this.ContractId = _contractId;
            this.CompanyId = _companyId;
			this.SiteId = _siteId;
			this.Year = _year;

            this.January = _january;
            this.February = _february;
            this.March = _march;
            this.April = _april;
            this.May = _may;
			this.June = _june;
			this.July = _july;
            this.August = _august;
            this.September = _september;
            this.October = _october;
            this.November = _november;
            this.December = _december;
            this.YearCount=_yearCount;
            this.LocationSponsorUserId = _locationSponsorUserId;
            this.LocationSpaUserId=_locationSpaUserId;
			this.ModifiedBy = _modifiedBy;
			this.ModifiedDate = _modifiedDate;
		}
		
		///<summary>
        /// A simple factory method to create a new <see cref="ContractReviewsRegister"/> instance.
		///</summary>
		///<param name="_questionnaireId"></param>
		///<param name="_answerId"></param>
		///<param name="_fileName"></param>
		///<param name="_fileHash"></param>
		///<param name="_contentLength"></param>
		///<param name="_content"></param>
		///<param name="_modifiedByUserId"></param>
		///<param name="_modifiedDate"></param>
        public static ContractReviewsRegister CreateContractReviewsRegister(System.Int32 _contractId, System.Int32 _companyId,
            System.Int32 _siteId, System.Int32 _year, System.Boolean _january, System.Boolean _february,
            System.Boolean _march, System.Boolean _april, System.Boolean _may, System.Boolean _june,
            System.Boolean _july, System.Boolean _august, System.Boolean _september, System.Boolean _october,
            System.Boolean _november, System.Boolean _december,
            System.Int32 _yearCount, System.Int32? _locationSponsorUserId, System.Int32? _locationSpaUserId,
            System.Int32 _modifiedBy, System.DateTime _modifiedDate)
		{
            ContractReviewsRegister newContractReviewsRegister = new ContractReviewsRegister();
            newContractReviewsRegister.ContractId = _contractId;
            newContractReviewsRegister.CompanyId = _companyId;
            newContractReviewsRegister.SiteId = _siteId;
            newContractReviewsRegister.Year = _year;

            newContractReviewsRegister.January = _january;
            newContractReviewsRegister.February = _february;
            newContractReviewsRegister.March = _march;
            newContractReviewsRegister.April = _april;
            newContractReviewsRegister.May = _may;
            newContractReviewsRegister.June = _june;
            newContractReviewsRegister.July = _july;
            newContractReviewsRegister.August = _august;
            newContractReviewsRegister.September = _september;
            newContractReviewsRegister.October = _october;
            newContractReviewsRegister.November = _november;
            newContractReviewsRegister.December = _december;

            newContractReviewsRegister.YearCount = _yearCount;
            newContractReviewsRegister.LocationSponsorUserId = _locationSponsorUserId;
            newContractReviewsRegister.LocationSpaUserId = _locationSpaUserId;

            newContractReviewsRegister.ModifiedBy = _modifiedBy;
            newContractReviewsRegister.ModifiedDate = _modifiedDate;
            return newContractReviewsRegister;
		}
				
		#endregion Constructors
			
		#region Properties	
		
		#region Data Properties		
		
		
		/// <summary>
        /// 	Gets or sets the ContractId property. 
		///		
		/// </summary>
		/// <value>This type is int.</value>
		/// <remarks>
		/// This property can not be set to null. 
		/// </remarks>


		[DescriptionAttribute(@""), System.ComponentModel.Bindable( System.ComponentModel.BindableSupport.Yes)]
		[DataObjectField(false, false, false)]
        public virtual System.Int32 ContractId
		{
			get
			{
				return this.entityData.ContractId; 
			}
			
			set
			{
                if (this.entityData.ContractId == value)
					return;

                OnPropertyChanging("ContractId");
                OnColumnChanging(ContractReviewsRegisterColumn.ContractId, this.entityData.ContractId);
                this.entityData.ContractId = value;
				if (this.EntityState == EntityState.Unchanged)
					this.EntityState = EntityState.Changed;
                OnColumnChanged(ContractReviewsRegisterColumn.ContractId, this.entityData.ContractId);
                OnPropertyChanged("ContractId");
			}
		}



        /// <summary>
        /// 	Gets or sets the ContractId property. 
        ///		
        /// </summary>
        /// <value>This type is int.</value>
        /// <remarks>
        /// This property can not be set to null. 
        /// </remarks>


        [DescriptionAttribute(@""), System.ComponentModel.Bindable(System.ComponentModel.BindableSupport.Yes)]
        [DataObjectField(false, false, false)]
        public virtual System.Int32 SiteId
        {
            get
            {
                return this.entityData.SiteId;
            }

            set
            {
                if (this.entityData.SiteId == value)
                    return;

                OnPropertyChanging("SiteId");
                OnColumnChanging(ContractReviewsRegisterColumn.SiteId, this.entityData.SiteId);
                this.entityData.SiteId = value;
                if (this.EntityState == EntityState.Unchanged)
                    this.EntityState = EntityState.Changed;
                OnColumnChanged(ContractReviewsRegisterColumn.SiteId, this.entityData.SiteId);
                OnPropertyChanged("SiteId");
            }
        }



        /// <summary>
        /// 	Gets or sets the CompanyId property. 
        ///		
        /// </summary>
        /// <value>This type is int.</value>
        /// <remarks>
        /// This property can not be set to null. 
        /// </remarks>


        [DescriptionAttribute(@""), System.ComponentModel.Bindable(System.ComponentModel.BindableSupport.Yes)]
        [DataObjectField(false, false, false)]
        public virtual System.Int32 CompanyId
        {
            get
            {
                return this.entityData.CompanyId;
            }

            set
            {
                if (this.entityData.CompanyId == value)
                    return;

                OnPropertyChanging("CompanyId");
                OnColumnChanging(ContractReviewsRegisterColumn.CompanyId, this.entityData.CompanyId);
                this.entityData.CompanyId = value;
                if (this.EntityState == EntityState.Unchanged)
                    this.EntityState = EntityState.Changed;
                OnColumnChanged(ContractReviewsRegisterColumn.CompanyId, this.entityData.CompanyId);
                OnPropertyChanged("CompanyId");
            }
        }

        /// <summary>
        /// 	Gets or sets the SiteId property. 
        ///		
        /// </summary>
        /// <value>This type is int.</value>
        /// <remarks>
        /// This property can not be set to null. 
        /// </remarks>


        [DescriptionAttribute(@""), System.ComponentModel.Bindable(System.ComponentModel.BindableSupport.Yes)]
        [DataObjectField(false, false, false)]
        public virtual System.Int32 Year
        {
            get
            {
                return this.entityData.Year;
            }

            set
            {
                if (this.entityData.Year == value)
                    return;

                OnPropertyChanging("Year");
                OnColumnChanging(ContractReviewsRegisterColumn.Year, this.entityData.Year);
                this.entityData.Year = value;
                if (this.EntityState == EntityState.Unchanged)
                    this.EntityState = EntityState.Changed;
                OnColumnChanged(ContractReviewsRegisterColumn.Year, this.entityData.Year);
                OnPropertyChanged("Year");
            }
        }


        /// <summary>
        /// 	Gets or sets the January property. 
        ///		
        /// </summary>
        /// <value>This type is int.</value>
        /// <remarks>
        /// This property can not be set to null. 
        /// </remarks>


        [DescriptionAttribute(@""), System.ComponentModel.Bindable(System.ComponentModel.BindableSupport.Yes)]
        [DataObjectField(false, false, false)]
        public virtual System.Boolean January
        {
            get
            {
                return this.entityData.January;
            }

            set
            {
                if (this.entityData.January == value)
                    return;

                OnPropertyChanging("January");
                OnColumnChanging(ContractReviewsRegisterColumn.January, this.entityData.January);
                this.entityData.January = value;
                if (this.EntityState == EntityState.Unchanged)
                    this.EntityState = EntityState.Changed;
                OnColumnChanged(ContractReviewsRegisterColumn.January, this.entityData.January);
                OnPropertyChanged("January");
            }
        }



        /// <summary>
        /// 	Gets or sets the February property. 
        ///		
        /// </summary>
        /// <value>This type is int.</value>
        /// <remarks>
        /// This property can not be set to null. 
        /// </remarks>


        [DescriptionAttribute(@""), System.ComponentModel.Bindable(System.ComponentModel.BindableSupport.Yes)]
        [DataObjectField(false, false, false)]
        public virtual System.Boolean February
        {
            get
            {
                return this.entityData.February;
            }

            set
            {
                if (this.entityData.February == value)
                    return;

                OnPropertyChanging("February");
                OnColumnChanging(ContractReviewsRegisterColumn.February, this.entityData.February);
                this.entityData.February = value;
                if (this.EntityState == EntityState.Unchanged)
                    this.EntityState = EntityState.Changed;
                OnColumnChanged(ContractReviewsRegisterColumn.February, this.entityData.February);
                OnPropertyChanged("February");
            }
        }



        /// <summary>
        /// 	Gets or sets the March property. 
        ///		
        /// </summary>
        /// <value>This type is int.</value>
        /// <remarks>
        /// This property can not be set to null. 
        /// </remarks>


        [DescriptionAttribute(@""), System.ComponentModel.Bindable(System.ComponentModel.BindableSupport.Yes)]
        [DataObjectField(false, false, false)]
        public virtual System.Boolean March
        {
            get
            {
                return this.entityData.March;
            }

            set
            {
                if (this.entityData.March == value)
                    return;

                OnPropertyChanging("March");
                OnColumnChanging(ContractReviewsRegisterColumn.March, this.entityData.March);
                this.entityData.March = value;
                if (this.EntityState == EntityState.Unchanged)
                    this.EntityState = EntityState.Changed;
                OnColumnChanged(ContractReviewsRegisterColumn.March, this.entityData.March);
                OnPropertyChanged("March");
            }
        }



        /// <summary>
        /// 	Gets or sets the April property. 
        ///		
        /// </summary>
        /// <value>This type is int.</value>
        /// <remarks>
        /// This property can not be set to null. 
        /// </remarks>


        [DescriptionAttribute(@""), System.ComponentModel.Bindable(System.ComponentModel.BindableSupport.Yes)]
        [DataObjectField(false, false, false)]
        public virtual System.Boolean April
        {
            get
            {
                return this.entityData.April;
            }

            set
            {
                if (this.entityData.April == value)
                    return;

                OnPropertyChanging("April");
                OnColumnChanging(ContractReviewsRegisterColumn.April, this.entityData.April);
                this.entityData.April = value;
                if (this.EntityState == EntityState.Unchanged)
                    this.EntityState = EntityState.Changed;
                OnColumnChanged(ContractReviewsRegisterColumn.April, this.entityData.April);
                OnPropertyChanged("April");
            }
        }



        /// <summary>
        /// 	Gets or sets the May property. 
        ///		
        /// </summary>
        /// <value>This type is int.</value>
        /// <remarks>
        /// This property can not be set to null. 
        /// </remarks>


        [DescriptionAttribute(@""), System.ComponentModel.Bindable(System.ComponentModel.BindableSupport.Yes)]
        [DataObjectField(false, false, false)]
        public virtual System.Boolean May
        {
            get
            {
                return this.entityData.May;
            }

            set
            {
                if (this.entityData.May == value)
                    return;

                OnPropertyChanging("May");
                OnColumnChanging(ContractReviewsRegisterColumn.January, this.entityData.May);
                this.entityData.May = value;
                if (this.EntityState == EntityState.Unchanged)
                    this.EntityState = EntityState.Changed;
                OnColumnChanged(ContractReviewsRegisterColumn.May, this.entityData.May);
                OnPropertyChanged("May");
            }
        }



        /// <summary>
        /// 	Gets or sets the June property. 
        ///		
        /// </summary>
        /// <value>This type is int.</value>
        /// <remarks>
        /// This property can not be set to null. 
        /// </remarks>


        [DescriptionAttribute(@""), System.ComponentModel.Bindable(System.ComponentModel.BindableSupport.Yes)]
        [DataObjectField(false, false, false)]
        public virtual System.Boolean June
        {
            get
            {
                return this.entityData.June;
            }

            set
            {
                if (this.entityData.June == value)
                    return;

                OnPropertyChanging("June");
                OnColumnChanging(ContractReviewsRegisterColumn.June, this.entityData.June);
                this.entityData.June = value;
                if (this.EntityState == EntityState.Unchanged)
                    this.EntityState = EntityState.Changed;
                OnColumnChanged(ContractReviewsRegisterColumn.June, this.entityData.June);
                OnPropertyChanged("June");
            }
        }


        /// <summary>
        /// 	Gets or sets the July property. 
        ///		
        /// </summary>
        /// <value>This type is int.</value>
        /// <remarks>
        /// This property can not be set to null. 
        /// </remarks>


        [DescriptionAttribute(@""), System.ComponentModel.Bindable(System.ComponentModel.BindableSupport.Yes)]
        [DataObjectField(false, false, false)]
        public virtual System.Boolean July
        {
            get
            {
                return this.entityData.July;
            }

            set
            {
                if (this.entityData.July == value)
                    return;

                OnPropertyChanging("July");
                OnColumnChanging(ContractReviewsRegisterColumn.July, this.entityData.July);
                this.entityData.July = value;
                if (this.EntityState == EntityState.Unchanged)
                    this.EntityState = EntityState.Changed;
                OnColumnChanged(ContractReviewsRegisterColumn.July, this.entityData.July);
                OnPropertyChanged("July");
            }
        }



        /// <summary>
        /// 	Gets or sets the August property. 
        ///		
        /// </summary>
        /// <value>This type is int.</value>
        /// <remarks>
        /// This property can not be set to null. 
        /// </remarks>


        [DescriptionAttribute(@""), System.ComponentModel.Bindable(System.ComponentModel.BindableSupport.Yes)]
        [DataObjectField(false, false, false)]
        public virtual System.Boolean August
        {
            get
            {
                return this.entityData.August;
            }

            set
            {
                if (this.entityData.August == value)
                    return;

                OnPropertyChanging("August");
                OnColumnChanging(ContractReviewsRegisterColumn.August, this.entityData.August);
                this.entityData.August = value;
                if (this.EntityState == EntityState.Unchanged)
                    this.EntityState = EntityState.Changed;
                OnColumnChanged(ContractReviewsRegisterColumn.August, this.entityData.August);
                OnPropertyChanged("August");
            }
        }


        /// <summary>
        /// 	Gets or sets the September property. 
        ///		
        /// </summary>
        /// <value>This type is int.</value>
        /// <remarks>
        /// This property can not be set to null. 
        /// </remarks>


        [DescriptionAttribute(@""), System.ComponentModel.Bindable(System.ComponentModel.BindableSupport.Yes)]
        [DataObjectField(false, false, false)]
        public virtual System.Boolean September
        {
            get
            {
                return this.entityData.September;
            }

            set
            {
                if (this.entityData.September == value)
                    return;

                OnPropertyChanging("September");
                OnColumnChanging(ContractReviewsRegisterColumn.September, this.entityData.September);
                this.entityData.September = value;
                if (this.EntityState == EntityState.Unchanged)
                    this.EntityState = EntityState.Changed;
                OnColumnChanged(ContractReviewsRegisterColumn.September, this.entityData.September);
                OnPropertyChanged("September");
            }
        }



        /// <summary>
        /// 	Gets or sets the October property. 
        ///		
        /// </summary>
        /// <value>This type is int.</value>
        /// <remarks>
        /// This property can not be set to null. 
        /// </remarks>


        [DescriptionAttribute(@""), System.ComponentModel.Bindable(System.ComponentModel.BindableSupport.Yes)]
        [DataObjectField(false, false, false)]
        public virtual System.Boolean October
        {
            get
            {
                return this.entityData.October;
            }

            set
            {
                if (this.entityData.October == value)
                    return;

                OnPropertyChanging("October");
                OnColumnChanging(ContractReviewsRegisterColumn.October, this.entityData.October);
                this.entityData.October = value;
                if (this.EntityState == EntityState.Unchanged)
                    this.EntityState = EntityState.Changed;
                OnColumnChanged(ContractReviewsRegisterColumn.October, this.entityData.October);
                OnPropertyChanged("October");
            }
        }


        /// <summary>
        /// 	Gets or sets the November property. 
        ///		
        /// </summary>
        /// <value>This type is int.</value>
        /// <remarks>
        /// This property can not be set to null. 
        /// </remarks>


        [DescriptionAttribute(@""), System.ComponentModel.Bindable(System.ComponentModel.BindableSupport.Yes)]
        [DataObjectField(false, false, false)]
        public virtual System.Boolean November
        {
            get
            {
                return this.entityData.November;
            }

            set
            {
                if (this.entityData.November == value)
                    return;

                OnPropertyChanging("November");
                OnColumnChanging(ContractReviewsRegisterColumn.November, this.entityData.November);
                this.entityData.November = value;
                if (this.EntityState == EntityState.Unchanged)
                    this.EntityState = EntityState.Changed;
                OnColumnChanged(ContractReviewsRegisterColumn.November, this.entityData.November);
                OnPropertyChanged("November");
            }
        }



        /// <summary>
        /// 	Gets or sets the December property. 
        ///		
        /// </summary>
        /// <value>This type is int.</value>
        /// <remarks>
        /// This property can not be set to null. 
        /// </remarks>


        [DescriptionAttribute(@""), System.ComponentModel.Bindable(System.ComponentModel.BindableSupport.Yes)]
        [DataObjectField(false, false, false)]
        public virtual System.Boolean December
        {
            get
            {
                return this.entityData.December;
            }

            set
            {
                if (this.entityData.December == value)
                    return;

                OnPropertyChanging("December");
                OnColumnChanging(ContractReviewsRegisterColumn.December, this.entityData.December);
                this.entityData.December = value;
                if (this.EntityState == EntityState.Unchanged)
                    this.EntityState = EntityState.Changed;
                OnColumnChanged(ContractReviewsRegisterColumn.December, this.entityData.December);
                OnPropertyChanged("December");
            }
        }


        /// <summary>
        /// 	Gets or sets the YearCount property. 
        ///		
        /// </summary>
        /// <value>This type is int.</value>
        /// <remarks>
        /// This property can not be set to null. 
        /// </remarks>


        [DescriptionAttribute(@""), System.ComponentModel.Bindable(System.ComponentModel.BindableSupport.Yes)]
        [DataObjectField(false, false, false)]
        public virtual System.Int32 YearCount
        {
            get
            {
                return this.entityData.YearCount;
            }

            set
            {
                if (this.entityData.YearCount == value)
                    return;

                OnPropertyChanging("YearCount");
                OnColumnChanging(ContractReviewsRegisterColumn.YearCount, this.entityData.YearCount);
                this.entityData.YearCount = value;
                if (this.EntityState == EntityState.Unchanged)
                    this.EntityState = EntityState.Changed;
                OnColumnChanged(ContractReviewsRegisterColumn.YearCount, this.entityData.YearCount);
                OnPropertyChanged("YearCount");
            }
        }



        /// <summary>
        /// 	Gets or sets the LocationSponsorUserId property. 
        ///		
        /// </summary>
        /// <value>This type is int.</value>
        /// <remarks>
        /// This property can not be set to null. 
        /// </remarks>

        [XmlElement(IsNullable = true)]
        [DescriptionAttribute(@""), System.ComponentModel.Bindable(System.ComponentModel.BindableSupport.Yes)]
        [DataObjectField(false, false, false)]
        public virtual System.Int32? LocationSponsorUserId
        {
            get
            {
                return this.entityData.LocationSponsorUserId;
            }

            set
            {
                if (this.entityData.LocationSponsorUserId == value)
                    return;

                OnPropertyChanging("LocationSponsorUserId");
                OnColumnChanging(ContractReviewsRegisterColumn.LocationSponsorUserId, this.entityData.LocationSponsorUserId);
                this.entityData.LocationSponsorUserId = value;
                if (this.EntityState == EntityState.Unchanged)
                    this.EntityState = EntityState.Changed;
                OnColumnChanged(ContractReviewsRegisterColumn.LocationSponsorUserId, this.entityData.LocationSponsorUserId);
                OnPropertyChanged("LocationSponsorUserId");
            }
        }


        /// <summary>
        /// 	Gets or sets the LocationSponsorUserId property. 
        ///		
        /// </summary>
        /// <value>This type is int.</value>
        /// <remarks>
        /// This property can not be set to null. 
        /// </remarks>

        [XmlElement(IsNullable = true)]
        [DescriptionAttribute(@""), System.ComponentModel.Bindable(System.ComponentModel.BindableSupport.Yes)]
        [DataObjectField(false, false, false)]
        public virtual System.Int32? LocationSpaUserId
        {
            get
            {
                return this.entityData.LocationSpaUserId;
            }

            set
            {
                if (this.entityData.LocationSpaUserId == value)
                    return;

                OnPropertyChanging("LocationSpaUserId");
                OnColumnChanging(ContractReviewsRegisterColumn.LocationSpaUserId, this.entityData.LocationSpaUserId);
                this.entityData.LocationSpaUserId = value;
                if (this.EntityState == EntityState.Unchanged)
                    this.EntityState = EntityState.Changed;
                OnColumnChanged(ContractReviewsRegisterColumn.LocationSpaUserId, this.entityData.LocationSpaUserId);
                OnPropertyChanged("LocationSpaUserId");
            }
        }

		
		/// <summary>
		/// 	Gets or sets the ModifiedBy property. 
		///		
		/// </summary>
		/// <value>This type is int.</value>
		/// <remarks>
		/// This property can not be set to null. 
		/// </remarks>
		
		




		[DescriptionAttribute(@""), System.ComponentModel.Bindable( System.ComponentModel.BindableSupport.Yes)]
		[DataObjectField(false, false, false)]
        public virtual System.Int32 ModifiedBy
		{
			get
			{
                return this.entityData.ModifiedBy; 
			}
			
			set
			{
                if (this.entityData.ModifiedBy == value)
					return;
				
                OnPropertyChanging("ModifiedByUserId");
                OnColumnChanging(ContractReviewsRegisterColumn.ModifiedBy, this.entityData.ModifiedBy);
                this.entityData.ModifiedBy = value;
				if (this.EntityState == EntityState.Unchanged)
					this.EntityState = EntityState.Changed;
                OnColumnChanged(ContractReviewsRegisterColumn.ModifiedBy, this.entityData.ModifiedBy);
                OnPropertyChanged("ModifiedBy");
			}
		}
		
		/// <summary>
		/// 	Gets or sets the ModifiedDate property. 
		///		
		/// </summary>
		/// <value>This type is datetime.</value>
		/// <remarks>
		/// This property can not be set to null. 
		/// </remarks>
		
		




		[DescriptionAttribute(@""), System.ComponentModel.Bindable( System.ComponentModel.BindableSupport.Yes)]
		[DataObjectField(false, false, false)]
		public virtual System.DateTime ModifiedDate
		{
			get
			{
				return this.entityData.ModifiedDate; 
			}
			
			set
			{
				if (this.entityData.ModifiedDate == value)
					return;
				
                OnPropertyChanging("ModifiedDate");
                OnColumnChanging(ContractReviewsRegisterColumn.ModifiedDate, this.entityData.ModifiedDate);
				this.entityData.ModifiedDate = value;
				if (this.EntityState == EntityState.Unchanged)
					this.EntityState = EntityState.Changed;
                OnColumnChanged(ContractReviewsRegisterColumn.ModifiedDate, this.entityData.ModifiedDate);
				OnPropertyChanged("ModifiedDate");
			}
		}
		
		#endregion Data Properties		

		#region Source Foreign Key Property
				
		/// <summary>
		/// Gets or sets the source <see cref="Users"/>.
		/// </summary>
		/// <value>The source Users for ModifiedByUserId.</value>
        [XmlIgnore()]
		[Browsable(false), System.ComponentModel.Bindable(System.ComponentModel.BindableSupport.Yes)]
		public virtual Users ModifiedByUserIdSource
      	{
            get { return entityData.ModifiedByUserIdSource; }
            set { entityData.ModifiedByUserIdSource = value; }
      	}
		/// <summary>
		/// Gets or sets the source <see cref="QuestionnaireMainAnswer"/>.
		/// </summary>
		/// <value>The source QuestionnaireMainAnswer for AnswerId.</value>
        [XmlIgnore()]
		[Browsable(false), System.ComponentModel.Bindable(System.ComponentModel.BindableSupport.Yes)]
		public virtual QuestionnaireMainAnswer AnswerIdSource
      	{
            get { return entityData.AnswerIdSource; }
            set { entityData.AnswerIdSource = value; }
      	}
		#endregion
		
		#region Children Collections
		#endregion Children Collections
		
		#endregion
		#region Validation
		
		/// <summary>
		/// Assigns validation rules to this object based on model definition.
		/// </summary>
		/// <remarks>This method overrides the base class to add schema related validation.</remarks>
		protected override void AddValidationRules()
		{
			//Validation rules based on database schema.
			ValidationRules.AddRule( CommonRules.NotNull,
				new ValidationRuleArgs("FileName", "File Name"));
			ValidationRules.AddRule( CommonRules.StringMaxLength, 
				new CommonRules.MaxLengthRuleArgs("FileName", "File Name", 255));
			ValidationRules.AddRule( CommonRules.NotNull,
				new ValidationRuleArgs("FileHash", "File Hash"));
			ValidationRules.AddRule( CommonRules.NotNull,
				new ValidationRuleArgs("Content", "Content"));
		}
   		#endregion
		
		#region Table Meta Data
		/// <summary>
		///		The name of the underlying database table.
		/// </summary>
		[BrowsableAttribute(false), XmlIgnoreAttribute()]
		public override string TableName
		{
            get { return "ContractReviewsRegister"; }
		}
		
		/// <summary>
		///		The name of the underlying database table's columns.
		/// </summary>
		[BrowsableAttribute(false), XmlIgnoreAttribute()]
		public override string[] TableColumns
		{
			get
			{
                return new string[] { "ContractId", "CompanyId", "SiteId", "Year", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December", "YearCount", "LocationSponsorUserId", "LocationSpaUserId", "ModifiedDate", "ModifiedBy" };
			}
		}
		#endregion 
		
		#region IEditableObject
		
		#region  CancelAddNew Event
		/// <summary>
        /// The delegate for the CancelAddNew event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
		public delegate void CancelAddNewEventHandler(object sender, EventArgs e);
    
    	/// <summary>
		/// The CancelAddNew event.
		/// </summary>
		[field:NonSerialized]
		public event CancelAddNewEventHandler CancelAddNew ;

		/// <summary>
        /// Called when [cancel add new].
        /// </summary>
        public void OnCancelAddNew()
        {    
			if (!SuppressEntityEvents)
			{
	            CancelAddNewEventHandler handler = CancelAddNew ;
            	if (handler != null)
	            {    
    	            handler(this, EventArgs.Empty) ;
        	    }
	        }
        }
		#endregion 
		
		/// <summary>
		/// Begins an edit on an object.
		/// </summary>
		void IEditableObject.BeginEdit() 
	    {
	        //Console.WriteLine("Start BeginEdit");
	        if (!inTxn) 
	        {
                this.backupData = this.entityData.Clone() as ContractReviewsRegisterEntityData;
	            inTxn = true;
	            //Console.WriteLine("BeginEdit");
	        }
	        //Console.WriteLine("End BeginEdit");
	    }
	
		/// <summary>
		/// Discards changes since the last <c>BeginEdit</c> call.
		/// </summary>
	    void IEditableObject.CancelEdit() 
	    {
	        //Console.WriteLine("Start CancelEdit");
	        if (this.inTxn) 
	        {
	            this.entityData = this.backupData;
	            this.backupData = null;
				this.inTxn = false;

				if (this.bindingIsNew)
	        	//if (this.EntityState == EntityState.Added)
	        	{
					if (this.parentCollection != null)
                        this.parentCollection.Remove((ContractReviewsRegister)this);
				}	            
	        }
	        //Console.WriteLine("End CancelEdit");
	    }
	
		/// <summary>
		/// Pushes changes since the last <c>BeginEdit</c> or <c>IBindingList.AddNew</c> call into the underlying object.
		/// </summary>
	    void IEditableObject.EndEdit() 
	    {
	        //Console.WriteLine("Start EndEdit" + this.custData.id + this.custData.lastName);
	        if (this.inTxn) 
	        {
	            this.backupData = null;
				if (this.IsDirty) 
				{
					if (this.bindingIsNew) {
						this.EntityState = EntityState.Added;
						this.bindingIsNew = false ;
					}
					else
						if (this.EntityState == EntityState.Unchanged) 
							this.EntityState = EntityState.Changed ;
				}

				this.bindingIsNew = false ;
	            this.inTxn = false;	            
	        }
	        //Console.WriteLine("End EndEdit");
	    }
	    
	    /// <summary>
        /// Gets or sets the parent collection of this current entity, if available.
        /// </summary>
        /// <value>The parent collection.</value>
	    [XmlIgnore]
		[Browsable(false)]
	    public override object ParentCollection
	    {
	        get 
	        {
	            return this.parentCollection;
	        }
	        set 
	        {
                this.parentCollection = value as TList<ContractReviewsRegister>;
	        }
	    }
	    
	    /// <summary>
        /// Called when the entity is changed.
        /// </summary>
	    private void OnEntityChanged() 
	    {
	        if (!SuppressEntityEvents && !inTxn && this.parentCollection != null) 
	        {
                this.parentCollection.EntityChanged(this as ContractReviewsRegister);
	        }
	    }


		#endregion
		
		#region ICloneable Members
		///<summary>
		///  Returns a Typed QuestionnaireMainAttachment Entity 
		///</summary>
        protected virtual ContractReviewsRegister Copy(IDictionary existingCopies)
		{
			if (existingCopies == null)
			{
				// This is the root of the tree to be copied!
				existingCopies = new Hashtable();
			}

			//shallow copy entity
            ContractReviewsRegister copy = new ContractReviewsRegister();
			existingCopies.Add(this, copy);
			copy.SuppressEntityEvents = true;
            copy.ContractId = this.ContractId;
            copy.CompanyId = this.CompanyId;
            copy.SiteId = this.SiteId;				
				copy.Year = this.Year;
                copy.January = this.January;
                copy.February = this.February;
				copy.March = this.March;

                copy.April = this.April;
                copy.May = this.May;
                copy.June = this.June;
                copy.July = this.July;
                copy.August = this.August;
                copy.September = this.September;
                copy.October = this.October;
                copy.November = this.November;
                copy.December = this.December;

                copy.Year = this.YearCount;
                copy.LocationSponsorUserId = this.LocationSponsorUserId;
                copy.LocationSpaUserId = this.LocationSpaUserId;
                
				copy.ModifiedBy = this.ModifiedBy;
				copy.ModifiedDate = this.ModifiedDate;
			
			if (this.ModifiedByUserIdSource != null && existingCopies.Contains(this.ModifiedByUserIdSource))
				copy.ModifiedByUserIdSource = existingCopies[this.ModifiedByUserIdSource] as Users;
			else
				copy.ModifiedByUserIdSource = MakeCopyOf(this.ModifiedByUserIdSource, existingCopies) as Users;
			if (this.AnswerIdSource != null && existingCopies.Contains(this.AnswerIdSource))
				copy.AnswerIdSource = existingCopies[this.AnswerIdSource] as QuestionnaireMainAnswer;
			else
				copy.AnswerIdSource = MakeCopyOf(this.AnswerIdSource, existingCopies) as QuestionnaireMainAnswer;
		
			copy.EntityState = this.EntityState;
			copy.SuppressEntityEvents = false;
			return copy;
		}		
		
		
		
		///<summary>
		///  Returns a Typed QuestionnaireMainAttachment Entity 
		///</summary>
        public virtual ContractReviewsRegister Copy()
		{
			return this.Copy(null);	
		}
		
		///<summary>
		/// ICloneable.Clone() Member, returns the Shallow Copy of this entity.
		///</summary>
		public object Clone()
		{
			return this.Copy(null);
		}
		
		///<summary>
		/// ICloneableEx.Clone() Member, returns the Shallow Copy of this entity.
		///</summary>
		public object Clone(IDictionary existingCopies)
		{
			return this.Copy(existingCopies);
		}
		
		///<summary>
		/// Returns a deep copy of the child collection object passed in.
		///</summary>
		public static object MakeCopyOf(object x)
		{
			if (x == null)
				return null;
				
			if (x is ICloneable)
			{
				// Return a deep copy of the object
				return ((ICloneable)x).Clone();
			}
			else
				throw new System.NotSupportedException("Object Does Not Implement the ICloneable Interface.");
		}
		
		///<summary>
		/// Returns a deep copy of the child collection object passed in.
		///</summary>
		public static object MakeCopyOf(object x, IDictionary existingCopies)
		{
			if (x == null)
				return null;
			
			if (x is ICloneableEx)
			{
				// Return a deep copy of the object
				return ((ICloneableEx)x).Clone(existingCopies);
			}
			else if (x is ICloneable)
			{
				// Return a deep copy of the object
				return ((ICloneable)x).Clone();
			}
			else
				throw new System.NotSupportedException("Object Does Not Implement the ICloneable or IClonableEx Interface.");
		}
		
		
		///<summary>
		///  Returns a Typed QuestionnaireMainAttachment Entity which is a deep copy of the current entity.
		///</summary>
        public virtual ContractReviewsRegister DeepCopy()
		{
            return EntityHelper.Clone<ContractReviewsRegister>(this as ContractReviewsRegister);	
		}
		#endregion
		
		#region Methods	
			
		///<summary>
		/// Revert all changes and restore original values.
		///</summary>
		public override void CancelChanges()
		{
			IEditableObject obj = (IEditableObject) this;
			obj.CancelEdit();

			this.entityData = null;
			if (this._originalData != null)
			{
                this.entityData = this._originalData.Clone() as ContractReviewsRegisterEntityData;
			}
			else
			{
				//Since this had no _originalData, then just reset the entityData with a new one.  entityData cannot be null.
                this.entityData = new ContractReviewsRegisterEntityData();
			}
		}	
		
		/// <summary>
		/// Accepts the changes made to this object.
		/// </summary>
		/// <remarks>
		/// After calling this method, properties: IsDirty, IsNew are false. IsDeleted flag remains unchanged as it is handled by the parent List.
		/// </remarks>
		public override void AcceptChanges()
		{
			base.AcceptChanges();

			// we keep of the original version of the data
			this._originalData = null;
            this._originalData = this.entityData.Clone() as ContractReviewsRegisterEntityData;
		}
		
		#region Comparision with original data
		
		/// <summary>
		/// Determines whether the property value has changed from the original data.
		/// </summary>
		/// <param name="column">The column.</param>
		/// <returns>
		/// 	<c>true</c> if the property value has changed; otherwise, <c>false</c>.
		/// </returns>
        public bool IsPropertyChanged(ContractReviewsRegisterColumn column)
		{
			switch(column)
			{
                case ContractReviewsRegisterColumn.ContractId:
                    return entityData.ContractId != _originalData.ContractId;
                case ContractReviewsRegisterColumn.CompanyId:
                    return entityData.CompanyId != _originalData.CompanyId;
                case ContractReviewsRegisterColumn.SiteId:
                    return entityData.SiteId != _originalData.SiteId;

                case ContractReviewsRegisterColumn.Year:
                    return entityData.Year != _originalData.Year;
                case ContractReviewsRegisterColumn.January:
                    return entityData.January != _originalData.January;
                case ContractReviewsRegisterColumn.February:
                    return entityData.February != _originalData.February;
                case ContractReviewsRegisterColumn.March:
                    return entityData.March != _originalData.March;

                case ContractReviewsRegisterColumn.April:
                    return entityData.April != _originalData.April;
                case ContractReviewsRegisterColumn.May:
                    return entityData.May != _originalData.May;
                case ContractReviewsRegisterColumn.June:
                    return entityData.June != _originalData.June;

                case ContractReviewsRegisterColumn.July:
                    return entityData.July != _originalData.July;
                case ContractReviewsRegisterColumn.August:
                    return entityData.August != _originalData.August;
                case ContractReviewsRegisterColumn.September:
                    return entityData.September != _originalData.September;
                case ContractReviewsRegisterColumn.October:
                    return entityData.October != _originalData.October;
                case ContractReviewsRegisterColumn.November:
                    return entityData.November != _originalData.November;
                case ContractReviewsRegisterColumn.December:
                    return entityData.December != _originalData.December;


                case ContractReviewsRegisterColumn.YearCount:
                    return entityData.YearCount != _originalData.YearCount;

                case ContractReviewsRegisterColumn.LocationSponsorUserId:
                    return entityData.LocationSponsorUserId != _originalData.LocationSponsorUserId;

                case ContractReviewsRegisterColumn.LocationSpaUserId:
                    return entityData.LocationSpaUserId != _originalData.LocationSpaUserId;


                case ContractReviewsRegisterColumn.ModifiedBy:
					return entityData.ModifiedBy != _originalData.ModifiedBy;
                case ContractReviewsRegisterColumn.ModifiedDate:
					return entityData.ModifiedDate != _originalData.ModifiedDate;
			
				default:
					return false;
			}
		}
		
		/// <summary>
		/// Determines whether the property value has changed from the original data.
		/// </summary>
		/// <param name="columnName">The column name.</param>
		/// <returns>
		/// 	<c>true</c> if the property value has changed; otherwise, <c>false</c>.
		/// </returns>
		public override bool IsPropertyChanged(string columnName)
		{
            return IsPropertyChanged(EntityHelper.GetEnumValue<ContractReviewsRegisterColumn>(columnName));
		}
		
		/// <summary>
		/// Determines whether the data has changed from original.
		/// </summary>
		/// <returns>
		/// 	<c>true</c> if data has changed; otherwise, <c>false</c>.
		/// </returns>
		public bool HasDataChanged()
		{
			bool result = false;
            result = result || entityData.ContractId != _originalData.ContractId;
            result = result || entityData.CompanyId != _originalData.CompanyId;
            result = result || entityData.SiteId != _originalData.SiteId;
			
			result = result || entityData.Year != _originalData.Year;
			result = result || entityData.January != _originalData.January;
			result = result || entityData.February != _originalData.February;
			result = result || entityData.March != _originalData.March;

            result = result || entityData.April != _originalData.April;
            result = result || entityData.May != _originalData.May;
            result = result || entityData.June != _originalData.June;
            result = result || entityData.July != _originalData.July;


            result = result || entityData.August != _originalData.August;
            result = result || entityData.September != _originalData.September;
            result = result || entityData.October != _originalData.October;
            result = result || entityData.November != _originalData.November;
            result = result || entityData.December != _originalData.December;

            result = result || entityData.YearCount != _originalData.YearCount;
            result = result || entityData.LocationSponsorUserId != _originalData.LocationSponsorUserId;
            result = result || entityData.LocationSpaUserId != _originalData.LocationSpaUserId;

			result = result || entityData.ModifiedBy != _originalData.ModifiedBy;
			result = result || entityData.ModifiedDate != _originalData.ModifiedDate;
			return result;
		}	
		
		///<summary>
        ///  Returns a ContractReviewsRegister Entity with the original data.
		///</summary>
        public ContractReviewsRegister GetOriginalEntity()
		{
			if (_originalData != null)
                return CreateContractReviewsRegister(	
			    _originalData.ContractId,
				_originalData.CompanyId,				
				_originalData.SiteId,
				_originalData.Year,
				_originalData.January,
				_originalData.February,
                _originalData.March,
                _originalData.April,
                _originalData.May,
                _originalData.June,
                _originalData.July,
                _originalData.August,
                _originalData.September,
                _originalData.October,
                _originalData.November,
                _originalData.December,
                _originalData.YearCount,
                _originalData.LocationSponsorUserId,
                _originalData.LocationSpaUserId,
				_originalData.ModifiedBy,
				_originalData.ModifiedDate
				);

            return (ContractReviewsRegister)this.Clone();
		}
		#endregion
	
	#region Value Semantics Instance Equality
        ///<summary>
        /// Returns a value indicating whether this instance is equal to a specified object using value semantics.
        ///</summary>
        ///<param name="Object1">An object to compare to this instance.</param>
        ///<returns>true if Object1 is a <see cref="ContractReviewsRegisterBase"/> and has the same value as this instance; otherwise, false.</returns>
        public override bool Equals(object Object1)
        {
			// Cast exception if Object1 is null or DbNull
            if (Object1 != null && Object1 != DBNull.Value && Object1 is ContractReviewsRegisterBase)
                return ValueEquals(this, (ContractReviewsRegisterBase)Object1);
			else
				return false;
        }

        /// <summary>
		/// Serves as a hash function for a particular type, suitable for use in hashing algorithms and data structures like a hash table.
        /// Provides a hash function that is appropriate for <see cref="QuestionnaireMainAttachmentBase"/> class 
        /// and that ensures a better distribution in the hash table
        /// </summary>
        /// <returns>number (hash code) that corresponds to the value of an object</returns>
        public override int GetHashCode()
        {
			return 
					this.ContractId.GetHashCode() ^
				this.CompanyId.GetHashCode() ^			
				this.SiteId.GetHashCode() ^
				this.Year.GetHashCode() ^
				this.January.GetHashCode() ^
				this.February.GetHashCode() ^
                this.March.GetHashCode() ^
                this.April.GetHashCode() ^
                this.May.GetHashCode() ^
                this.June.GetHashCode() ^
                this.July.GetHashCode() ^
                this.August.GetHashCode() ^
                this.September.GetHashCode() ^
                this.October.GetHashCode() ^
                this.November.GetHashCode() ^
                this.December.GetHashCode() ^
                 this.YearCount.GetHashCode() ^
                 this.LocationSponsorUserId.GetHashCode() ^
                 this.LocationSpaUserId.GetHashCode() ^
				this.ModifiedBy.GetHashCode() ^				
				this.ModifiedDate.GetHashCode();
        }
		
		///<summary>
		/// Returns a value indicating whether this instance is equal to a specified object using value semantics.
		///</summary>
		///<param name="toObject">An object to compare to this instance.</param>
		///<returns>true if toObject is a <see cref="QuestionnaireMainAttachmentBase"/> and has the same value as this instance; otherwise, false.</returns>
        public virtual bool Equals(ContractReviewsRegisterBase toObject)
		{
			if (toObject == null)
				return false;
			return ValueEquals(this, toObject);
		}
		#endregion
		
		///<summary>
		/// Determines whether the specified <see cref="QuestionnaireMainAttachmentBase"/> instances are considered equal using value semantics.
		///</summary>
		///<param name="Object1">The first <see cref="QuestionnaireMainAttachmentBase"/> to compare.</param>
		///<param name="Object2">The second <see cref="QuestionnaireMainAttachmentBase"/> to compare. </param>
		///<returns>true if Object1 is the same instance as Object2 or if both are null references or if objA.Equals(objB) returns true; otherwise, false.</returns>
        public static bool ValueEquals(ContractReviewsRegisterBase Object1, ContractReviewsRegisterBase Object2)
		{
			// both are null
			if (Object1 == null && Object2 == null)
				return true;

			// one or the other is null, but not both
			if (Object1 == null ^ Object2 == null)
				return false;
				
			bool equal = true;
            if (Object1.ContractId != Object2.ContractId)
				equal = false;
            if (Object1.CompanyId != Object2.CompanyId)
				equal = false;
            if (Object1.SiteId != Object2.SiteId)
				equal = false;
			if (Object1.Year != Object2.Year)
				equal = false;
            if (Object1.January != Object2.January)
				equal = false;
            if (Object1.February != Object2.February)
				equal = false;
            if (Object1.March != Object2.March)
                equal = false;
            if (Object1.April != Object2.April)
                equal = false;
            if (Object1.May != Object2.May)
                equal = false;
            if (Object1.June != Object2.June)
                equal = false;
            if (Object1.July != Object2.July)
                equal = false;
            if (Object1.August != Object2.August)
                equal = false;
            if (Object1.September != Object2.September)
                equal = false;
            if (Object1.October != Object2.October)
                equal = false;
            if (Object1.November != Object2.November)
                equal = false;
            if (Object1.December != Object2.December)
                equal = false;

            if (Object1.YearCount != Object2.YearCount)
                equal = false;
            if (Object1.LocationSponsorUserId != Object2.LocationSponsorUserId)
                equal = false;
            if (Object1.LocationSpaUserId != Object2.LocationSpaUserId)
                equal = false;

            

			if (Object1.ModifiedBy != Object2.ModifiedBy)
				equal = false;
			if (Object1.ModifiedDate != Object2.ModifiedDate)
				equal = false;
					
			return equal;
		}
		
		#endregion
		
		#region IComparable Members
		///<summary>
		/// Compares this instance to a specified object and returns an indication of their relative values.
		///<param name="obj">An object to compare to this instance, or a null reference (Nothing in Visual Basic).</param>
		///</summary>
		///<returns>A signed integer that indicates the relative order of this instance and obj.</returns>
		public virtual int CompareTo(object obj)
		{
			throw new NotImplementedException();
			//return this. GetPropertyName(SourceTable.PrimaryKey.MemberColumns[0]) .CompareTo(((QuestionnaireMainAttachmentBase)obj).GetPropertyName(SourceTable.PrimaryKey.MemberColumns[0]));
		}
		
		/*
		// static method to get a Comparer object
        public static QuestionnaireMainAttachmentComparer GetComparer()
        {
            return new QuestionnaireMainAttachmentComparer();
        }
        */

        // Comparer delegates back to QuestionnaireMainAttachment
        // Employee uses the integer's default
        // CompareTo method
        /*
        public int CompareTo(Item rhs)
        {
            return this.Id.CompareTo(rhs.Id);
        }
        */

/*
        // Special implementation to be called by custom comparer
        public int CompareTo(QuestionnaireMainAttachment rhs, QuestionnaireMainAttachmentColumn which)
        {
            switch (which)
            {
            	
            	
            	case QuestionnaireMainAttachmentColumn.AttachmentId:
            		return this.AttachmentId.CompareTo(rhs.AttachmentId);
            		
            		                 
            	
            	
            	case QuestionnaireMainAttachmentColumn.QuestionnaireId:
            		return this.QuestionnaireId.CompareTo(rhs.QuestionnaireId);
            		
            		                 
            	
            	
            	case QuestionnaireMainAttachmentColumn.AnswerId:
            		return this.AnswerId.CompareTo(rhs.AnswerId);
            		
            		                 
            	
            	
            	case QuestionnaireMainAttachmentColumn.FileName:
            		return this.FileName.CompareTo(rhs.FileName);
            		
            		                 
            	
            		                 
            	
            	
            	case QuestionnaireMainAttachmentColumn.ContentLength:
            		return this.ContentLength.CompareTo(rhs.ContentLength);
            		
            		                 
            	
            		                 
            	
            	
            	case QuestionnaireMainAttachmentColumn.ModifiedByUserId:
            		return this.ModifiedByUserId.CompareTo(rhs.ModifiedByUserId);
            		
            		                 
            	
            	
            	case QuestionnaireMainAttachmentColumn.ModifiedDate:
            		return this.ModifiedDate.CompareTo(rhs.ModifiedDate);
            		
            		                 
            }
            return 0;
        }
        */
	
		#endregion
		
		#region IComponent Members
		
		private ISite _site = null;

		/// <summary>
		/// Gets or Sets the site where this data is located.
		/// </summary>
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public ISite Site
		{
			get{ return this._site; }
			set{ this._site = value; }
		}

		#endregion

		#region IDisposable Members
		
		/// <summary>
		/// Notify those that care when we dispose.
		/// </summary>
		[field:NonSerialized]
		public event System.EventHandler Disposed;

		/// <summary>
		/// Clean up. Nothing here though.
		/// </summary>
		public virtual void Dispose()
		{
			this.parentCollection = null;
			this.Dispose(true);
			GC.SuppressFinalize(this);
		}
		
		/// <summary>
		/// Clean up.
		/// </summary>
		protected virtual void Dispose(bool disposing)
		{
			if (disposing)
			{
				EventHandler handler = Disposed;
				if (handler != null)
					handler(this, EventArgs.Empty);
			}
		}
		
		#endregion
				
		#region IEntityKey<ContractReviewsRegisterKey> Members
		
		// member variable for the EntityId property
        private ContractReviewsRegisterKey _entityId;

		/// <summary>
		/// Gets or sets the EntityId property.
		/// </summary>
		[XmlIgnore]
        public virtual ContractReviewsRegisterKey EntityId
		{
			get
			{
				if ( _entityId == null )
				{
                    _entityId = new ContractReviewsRegisterKey(this);
				}

				return _entityId;
			}
			set
			{
				if ( value != null )
				{
					value.Entity = this;
				}
				
				_entityId = value;
			}
		}
		
		#endregion
		
		#region EntityState
		/// <summary>
		///		Indicates state of object
		/// </summary>
		/// <remarks>0=Unchanged, 1=Added, 2=Changed</remarks>
		[BrowsableAttribute(false) , XmlIgnoreAttribute()]
		public override EntityState EntityState 
		{ 
			get{ return entityData.EntityState;	 } 
			set{ entityData.EntityState = value; } 
		}
		#endregion 
		
		#region EntityTrackingKey
		///<summary>
		/// Provides the tracking key for the <see cref="EntityLocator"/>
		///</summary>
		[XmlIgnore]
		public override string EntityTrackingKey
		{
			get
			{
				if(entityTrackingKey == null)
                    entityTrackingKey = new System.Text.StringBuilder("ContractReviewsRegister")
					.Append("|").Append( this.ContractId.ToString()).ToString();
				return entityTrackingKey;
			}
			set
		    {
		        if (value != null)
                    entityTrackingKey = value;
		    }
		}
		#endregion 
		
		#region ToString Method
		
		///<summary>
		/// Returns a String that represents the current object.
		///</summary>
		public override string ToString()
		{
			return string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "{22}{21}- ContractId: {0}{21}- CompanyId: {1}{21}- SiteId: {2}{21}- Year: {3}{21}- January: {4}{21}- February: {5}{21}- March: {6}{21}- April: {7}{21}- May: {8}{21}- June: {9}{21}- July: {10}{21}- August: {11}{21}- September: {12}{21}- October: {13}{21}- November: {14}{21}- December:  {15}{21}- YearCount: {16}{21}- LocationSponsorUserId: {17}{21}- LocationSpaUserId:  {18}{21}- ModifiedBy: {19}{21}- ModifiedDate: {20}{21}{22}", 
				this.ContractId,
				this.CompanyId,				
				this.SiteId,
				this.Year,
				this.January,
				this.February,
                this.March,
                this.April,
                this.May,
                this.June,
                this.July,
                this.August,
                this.September,
                this.October,
                this.November,
                this.December, 
                this.YearCount,
                this.LocationSponsorUserId,
                this.LocationSpaUserId,
				this.ModifiedBy,
				this.ModifiedDate,
				System.Environment.NewLine, 
				this.GetType(),
				this.Error.Length == 0 ? string.Empty : string.Format("- Error: {0}\n",this.Error));
		}
		
		#endregion ToString Method
		
		#region Inner data class
		
	/// <summary>
        ///		The data structure representation of the 'ContractReviewsRegister' table.
	/// </summary>
	/// <remarks>
	/// 	This struct is generated by a tool and should never be modified.
	/// </remarks>
	[EditorBrowsable(EditorBrowsableState.Never)]
	[Serializable]
        internal protected class ContractReviewsRegisterEntityData : ICloneable, ICloneableEx
	{
		#region Variable Declarations
		private EntityState currentEntityState = EntityState.Added;
		
		#region Primary key(s)
		/// <summary>			
		/// COntractId : 
		/// </summary>
        /// <remarks>Member of the primary key of the underlying table "ContractReviewsRegister"</remarks>
		public System.Int32 ContractId;
			
		#endregion
		
		#region Non Primary key(s)

		/// <summary>
        /// CompanyId : 
		/// </summary>
        public System.Int32 CompanyId = (int)0;



        /// <summary>
        /// SiteId : 
        /// </summary>
        public System.Int32 SiteId = (int)0;


        /// <summary>
        /// Year : 
        /// </summary>
        public System.Int32 Year = (int)0;


        /// <summary>
        /// January : 
        /// </summary>
        public System.Boolean January = false;


        /// <summary>
        /// February : 
        /// </summary>
        public System.Boolean February = false;


        /// <summary>
        /// March : 
        /// </summary>
        public System.Boolean March = false;


        /// <summary>
        /// April : 
        /// </summary>
        public System.Boolean April = false;

        /// <summary>
        /// May : 
        /// </summary>
        public System.Boolean May = false;

        /// <summary>
        /// June : 
        /// </summary>
        public System.Boolean June = false;


        /// <summary>
        /// July : 
        /// </summary>
        public System.Boolean July = false;


        /// <summary>
        /// August : 
        /// </summary>
        public System.Boolean August = false;


        /// <summary>
        /// September : 
        /// </summary>
        public System.Boolean September = false;

        /// <summary>
        /// October : 
        /// </summary>
        public System.Boolean October = false;


        /// <summary>
        /// November : 
        /// </summary>
        public System.Boolean November = false;


        /// <summary>
        /// December : 
        /// </summary>
        public System.Boolean December = false;

        /// <summary>
        /// YearCount : 
        /// </summary>
        public System.Int32 YearCount = (int)0;

        /// <summary>
        /// LocationSponsorUserId : 
        /// </summary>
        public System.Int32? LocationSponsorUserId = null;

        /// <summary>
        /// LocationSpaUserId : 
        /// </summary>
        public System.Int32? LocationSpaUserId = null;


		/// <summary>
		/// ModifiedByUserId : 
		/// </summary>
		public System.Int32 ModifiedBy = (int)0;
		
		/// <summary>
		/// ModifiedDate : 
		/// </summary>
		public System.DateTime ModifiedDate = DateTime.MinValue;
		#endregion
			
		#region Source Foreign Key Property
				
		private Users _modifiedByUserIdSource = null;
		
		/// <summary>
		/// Gets or sets the source <see cref="Users"/>.
		/// </summary>
		/// <value>The source Users for ModifiedByUserId.</value>
		[XmlIgnore()]
		[Browsable(false)]
		public virtual Users ModifiedByUserIdSource
      	{
            get { return this._modifiedByUserIdSource; }
            set { this._modifiedByUserIdSource = value; }
      	}
		private QuestionnaireMainAnswer _answerIdSource = null;
		
		/// <summary>
		/// Gets or sets the source <see cref="QuestionnaireMainAnswer"/>.
		/// </summary>
		/// <value>The source QuestionnaireMainAnswer for AnswerId.</value>
		[XmlIgnore()]
		[Browsable(false)]
		public virtual QuestionnaireMainAnswer AnswerIdSource
      	{
            get { return this._answerIdSource; }
            set { this._answerIdSource = value; }
      	}
		#endregion
        
		#endregion Variable Declarations

		#region Data Properties

		#endregion Data Properties
		#region Clone Method

		/// <summary>
		/// Creates a new object that is a copy of the current instance.
		/// </summary>
		/// <returns>A new object that is a copy of this instance.</returns>
		public Object Clone()
		{
            ContractReviewsRegisterEntityData _tmp = new ContractReviewsRegisterEntityData();
						
			_tmp.ContractId = this.ContractId;
            _tmp.CompanyId = this.CompanyId;
			_tmp.SiteId = this.SiteId;
			_tmp.Year = this.Year;
			_tmp.January = this.January;
			_tmp.February = this.February;
            _tmp.March = this.March;
            _tmp.April = this.April;
            _tmp.May = this.May;
            _tmp.June = this.June;
            _tmp.July = this.July;
            _tmp.August = this.August;
            _tmp.September = this.September;
            _tmp.October = this.October;
            _tmp.November = this.November;
            _tmp.December = this.December;
            _tmp.YearCount=this.YearCount;
            _tmp.LocationSponsorUserId=this.LocationSponsorUserId;
            _tmp.LocationSpaUserId = this.LocationSpaUserId;
			_tmp.ModifiedBy = this.ModifiedBy;
			_tmp.ModifiedDate = this.ModifiedDate;
			
			#region Source Parent Composite Entities
			if (this.ModifiedByUserIdSource != null)
				_tmp.ModifiedByUserIdSource = MakeCopyOf(this.ModifiedByUserIdSource) as Users;
			if (this.AnswerIdSource != null)
				_tmp.AnswerIdSource = MakeCopyOf(this.AnswerIdSource) as QuestionnaireMainAnswer;
			#endregion
		
			#region Child Collections
			#endregion Child Collections
			
			//EntityState
			_tmp.EntityState = this.EntityState;
			
			return _tmp;
		}
		
		/// <summary>
		/// Creates a new object that is a copy of the current instance.
		/// </summary>
		/// <returns>A new object that is a copy of this instance.</returns>
		public object Clone(IDictionary existingCopies)
		{
			if (existingCopies == null)
				existingCopies = new Hashtable();

            ContractReviewsRegisterEntityData _tmp = new ContractReviewsRegisterEntityData();

            _tmp.ContractId = this.ContractId;
            _tmp.CompanyId = this.CompanyId;
            _tmp.SiteId = this.SiteId;
            _tmp.Year = this.Year;
            _tmp.January = this.January;
            _tmp.February = this.February;
            _tmp.March = this.March;
            _tmp.April = this.April;
            _tmp.May = this.May;
            _tmp.June = this.June;
            _tmp.July = this.July;
            _tmp.August = this.August;
            _tmp.September = this.September;
            _tmp.October = this.October;
            _tmp.November = this.November;
            _tmp.December = this.December;
            _tmp.YearCount = this.YearCount;
            _tmp.LocationSponsorUserId = this.LocationSponsorUserId;
            _tmp.LocationSpaUserId = this.LocationSpaUserId;
            _tmp.ModifiedBy = this.ModifiedBy;
            _tmp.ModifiedDate = this.ModifiedDate;
			
			#region Source Parent Composite Entities
			if (this.ModifiedByUserIdSource != null && existingCopies.Contains(this.ModifiedByUserIdSource))
				_tmp.ModifiedByUserIdSource = existingCopies[this.ModifiedByUserIdSource] as Users;
			else
				_tmp.ModifiedByUserIdSource = MakeCopyOf(this.ModifiedByUserIdSource, existingCopies) as Users;
			if (this.AnswerIdSource != null && existingCopies.Contains(this.AnswerIdSource))
				_tmp.AnswerIdSource = existingCopies[this.AnswerIdSource] as QuestionnaireMainAnswer;
			else
				_tmp.AnswerIdSource = MakeCopyOf(this.AnswerIdSource, existingCopies) as QuestionnaireMainAnswer;
			#endregion
		
			#region Child Collections
			#endregion Child Collections
			
			//EntityState
			_tmp.EntityState = this.EntityState;
			
			return _tmp;
		}
		
		#endregion Clone Method
		
		/// <summary>
		///		Indicates state of object
		/// </summary>
		/// <remarks>0=Unchanged, 1=Added, 2=Changed</remarks>
		[BrowsableAttribute(false), XmlIgnoreAttribute()]
		public EntityState	EntityState
		{
			get { return currentEntityState;  }
			set { currentEntityState = value; }
		}
	
	}//End struct

		#endregion
		
				
		
		#region Events trigger
		/// <summary>
		/// Raises the <see cref="ColumnChanging" /> event.
		/// </summary>
    /// <param name="column">The <see cref="ContractReviewsRegisterColumn"/> which has raised the event.</param>
    public virtual void OnColumnChanging(ContractReviewsRegisterColumn column)
		{
			OnColumnChanging(column, null);
			return;
		}
		
		/// <summary>
		/// Raises the <see cref="ColumnChanged" /> event.
		/// </summary>
    /// <param name="column">The <see cref="ContractReviewsRegisterColumn"/> which has raised the event.</param>
    public virtual void OnColumnChanged(ContractReviewsRegisterColumn column)
		{
			OnColumnChanged(column, null);
			return;
		} 
		
		
		/// <summary>
		/// Raises the <see cref="ColumnChanging" /> event.
		/// </summary>
    /// <param name="column">The <see cref="ContractReviewsRegisterColumn"/> which has raised the event.</param>
		/// <param name="value">The changed value.</param>
    public virtual void OnColumnChanging(ContractReviewsRegisterColumn column, object value)
		{
			if(IsEntityTracked && EntityState != EntityState.Added && !EntityManager.TrackChangedEntities)
                EntityManager.StopTracking(entityTrackingKey);
                
			if (!SuppressEntityEvents)
			{
                ContractReviewsRegisterEventHandler handler = ColumnChanging;
				if(handler != null)
				{
                    handler(this, new ContractReviewsRegisterEventArgs(column, value));
				}
			}
		}
		
		/// <summary>
		/// Raises the <see cref="ColumnChanged" /> event.
		/// </summary>
		/// <param name="column">The <see cref="QuestionnaireMainAttachmentColumn"/> which has raised the event.</param>
		/// <param name="value">The changed value.</param>
    public virtual void OnColumnChanged(ContractReviewsRegisterColumn column, object value)
		{
			if (!SuppressEntityEvents)
			{
                ContractReviewsRegisterEventHandler handler = ColumnChanged;
				if(handler != null)
				{
                    handler(this, new ContractReviewsRegisterEventArgs(column, value));
				}
			
				// warn the parent list that i have changed
				OnEntityChanged();
			}
		} 
		#endregion
			
	} // End Class


    #region ContractReviewsRegisterEventArgs class
    /// <summary>
	/// Provides data for the ColumnChanging and ColumnChanged events.
	/// </summary>
	/// <remarks>
	/// The ColumnChanging and ColumnChanged events occur when a change is made to the value 
    /// of a property of a <see cref="ContractReviewsRegister"/> object.
	/// </remarks>
	public class ContractReviewsRegisterEventArgs : System.EventArgs
	{
		private ContractReviewsRegisterColumn column;
		private object value;
		
		///<summary>
		/// Initalizes a new Instance of the QuestionnaireMainAttachmentEventArgs class.
		///</summary>
		public ContractReviewsRegisterEventArgs(ContractReviewsRegisterColumn column)
		{
			this.column = column;
		}
		
		///<summary>
		/// Initalizes a new Instance of the QuestionnaireMainAttachmentEventArgs class.
		///</summary>
        public ContractReviewsRegisterEventArgs(ContractReviewsRegisterColumn column, object value)
		{
			this.column = column;
			this.value = value;
		}
		
		///<summary>
        /// The ContractReviewsRegisterColumn that was modified, which has raised the event.
		///</summary>
		///<value cref="QuestionnaireMainAttachmentColumn" />
        public ContractReviewsRegisterColumn Column { get { return this.column; } }
		
		/// <summary>
        /// Gets the current value of the column.
        /// </summary>
        /// <value>The current value of the column.</value>
		public object Value{ get { return this.value; } }

	}
	#endregion
	
	///<summary>
	/// Define a delegate for all QuestionnaireMainAttachment related events.
	///</summary>
    public delegate void ContractReviewsRegisterEventHandler(object sender, ContractReviewsRegisterEventArgs e);

    #region ContractReviewsRegisterComparer

    /// <summary>
	///	Strongly Typed IComparer
	/// </summary>
    public class ContractReviewsRegisterComparer : System.Collections.Generic.IComparer<ContractReviewsRegister>
	{
        ContractReviewsRegisterColumn whichComparison;
		
		/// <summary>
        /// Initializes a new instance of the <see cref="T:ContractReviewsRegisterComparer"/> class.
        /// </summary>
		public ContractReviewsRegisterComparer()
        {            
        }               
        
        /// <summary>
        /// Initializes a new instance of the <see cref="T:ContractReviewsRegisterComparer"/> class.
        /// </summary>
        /// <param name="column">The column to sort on.</param>
        public ContractReviewsRegisterComparer(ContractReviewsRegisterColumn column)
        {
            this.whichComparison = column;
        }

		/// <summary>
        /// Determines whether the specified <see cref="ContractReviewsRegister"/> instances are considered equal.
        /// </summary>
        /// <param name="a">The first <see cref="ContractReviewsRegister"/> to compare.</param>
        /// <param name="b">The second <c>QuestionnaireMainAttachment</c> to compare.</param>
        /// <returns>true if objA is the same instance as objB or if both are null references or if objA.Equals(objB) returns true; otherwise, false.</returns>
        public bool Equals(ContractReviewsRegister a, ContractReviewsRegister b)
        {
            return this.Compare(a, b) == 0;
        }

		/// <summary>
        /// Gets the hash code of the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns></returns>
        public int GetHashCode(ContractReviewsRegister entity)
        {
            return entity.GetHashCode();
        }

        /// <summary>
        /// Performs a case-insensitive comparison of two objects of the same type and returns a value indicating whether one is less than, equal to, or greater than the other.
        /// </summary>
        /// <param name="a">The first object to compare.</param>
        /// <param name="b">The second object to compare.</param>
        /// <returns></returns>
        public int Compare(ContractReviewsRegister a, ContractReviewsRegister b)
        {
        	EntityPropertyComparer entityPropertyComparer = new EntityPropertyComparer(this.whichComparison.ToString());
        	return entityPropertyComparer.Compare(a, b);
        }

		/// <summary>
        /// Gets or sets the column that will be used for comparison.
        /// </summary>
        /// <value>The comparison column.</value>
        public ContractReviewsRegisterColumn WhichComparison
        {
            get { return this.whichComparison; }
            set { this.whichComparison = value; }
        }
	}
	
	#endregion
	
	#region SafetyPlansSEResponsesAttachmentKey Class

	/// <summary>
    /// Wraps the unique identifier values for the <see cref="ContractReviewsRegister"/> object.
	/// </summary>
	[Serializable]
	[CLSCompliant(true)]
	public class ContractReviewsRegisterKey : EntityKeyBase
	{
		#region Constructors
		
		/// <summary>
        /// Initializes a new instance of the ContractReviewsRegisterKey class.
		/// </summary>
		public ContractReviewsRegisterKey()
		{
		}
		
		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainAttachmentKey class.
		/// </summary>
		public ContractReviewsRegisterKey(ContractReviewsRegisterBase entity)
		{
			this.Entity = entity;

			#region Init Properties

			if ( entity != null )
			{
                this.ContractId = entity.ContractId;
			}

			#endregion
		}
		
		/// <summary>
        /// Initializes a new instance of the ContractReviewsRegisterKey class.
		/// </summary>
        public ContractReviewsRegisterKey(System.Int32 _contractId)
		{
			#region Init Properties

            this.ContractId = _contractId;

			#endregion
		}
		
		#endregion Constructors

		#region Properties
		
		// member variable for the Entity property
		private ContractReviewsRegisterBase _entity;
		
		/// <summary>
		/// Gets or sets the Entity property.
		/// </summary>
		public ContractReviewsRegisterBase Entity
		{
			get { return _entity; }
			set { _entity = value; }
		}
		
		// member variable for the AttachmentId property
        private System.Int32 _contractId;
		
		/// <summary>
		/// Gets or sets the AttachmentId property.
		/// </summary>
		public System.Int32 ContractId
		{
            get { return _contractId; }
			set
			{
				if ( this.Entity != null )
                    this.Entity.ContractId = value;

                _contractId = value;
			}
		}
		
		#endregion

		#region Methods
		
		/// <summary>
		/// Reads values from the supplied <see cref="IDictionary"/> object into
		/// properties of the current object.
		/// </summary>
		/// <param name="values">An <see cref="IDictionary"/> instance that contains
		/// the key/value pairs to be used as property values.</param>
		public override void Load(IDictionary values)
		{
			#region Init Properties

			if ( values != null )
			{
                ContractId = (values["ContractId"] != null) ? (System.Int32)EntityUtil.ChangeType(values["ContractId"], typeof(System.Int32)) : (int)0;
			}

			#endregion
		}

		/// <summary>
		/// Creates a new <see cref="IDictionary"/> object and populates it
		/// with the property values of the current object.
		/// </summary>
		/// <returns>A collection of name/value pairs.</returns>
		public override IDictionary ToDictionary()
		{
			IDictionary values = new Hashtable();

			#region Init Dictionary

            values.Add("ContractId", ContractId);

			#endregion Init Dictionary

			return values;
		}
		
		///<summary>
		/// Returns a String that represents the current object.
		///</summary>
		public override string ToString()
		{
            return String.Format("ContractId: {0}{1}",
                                ContractId,
								System.Environment.NewLine);
		}

		#endregion Methods
	}
	
	#endregion	

    #region ContractReviewsRegisterColumn Enum

    /// <summary>
    /// Enumerate the ContractReviewsRegister columns.
	/// </summary>
	[Serializable]
    public enum ContractReviewsRegisterColumn : int
	{
		/// <summary>
		/// ContractId : 
		/// </summary>
        [EnumTextValue("ContractId")]
        [ColumnEnum("ContractId", typeof(System.Int32), System.Data.DbType.Int32, true, true, false)]
        ContractId = 1,

        /// <summary>
        /// QuestionId : 
        /// </summary>
        [EnumTextValue("CompanyId")]
        [ColumnEnum("CompanyId", typeof(System.Int32), System.Data.DbType.Int32, false, false, false)]
        CompanyId = 2,

		/// <summary>
		/// SiteId : 
		/// </summary>
        [EnumTextValue("SiteId")]
        [ColumnEnum("SiteId", typeof(System.Int32), System.Data.DbType.Int32, false, false, false)]
        SiteId = 3,
		
		/// <summary>
		/// Year : 
		/// </summary>
        [EnumTextValue("Year")]
        [ColumnEnum("Year", typeof(System.Int32), System.Data.DbType.Int32, false, false, false)]
        Year = 4,

        /// <summary>
        /// January : 
        /// </summary>
        [EnumTextValue("January")]
        [ColumnEnum("January", typeof(System.Boolean), System.Data.DbType.Boolean, false, false, false)]
        January = 5,

        /// <summary>
        /// February : 
        /// </summary>
        [EnumTextValue("February")]
        [ColumnEnum("February", typeof(System.Boolean), System.Data.DbType.Boolean, false, false, false)]
        February = 6,


        /// <summary>
        /// March : 
        /// </summary>
        [EnumTextValue("March")]
        [ColumnEnum("March", typeof(System.Boolean), System.Data.DbType.Boolean, false, false, false)]
        March = 7,


        /// <summary>
        /// April : 
        /// </summary>
        [EnumTextValue("April")]
        [ColumnEnum("April", typeof(System.Boolean), System.Data.DbType.Boolean, false, false, false)]
        April = 8,

        /// <summary>
        /// May : 
        /// </summary>
        [EnumTextValue("May")]
        [ColumnEnum("May", typeof(System.Boolean), System.Data.DbType.Boolean, false, false, false)]
        May = 9,

        /// <summary>
        /// June : 
        /// </summary>
        [EnumTextValue("June")]
        [ColumnEnum("June", typeof(System.Boolean), System.Data.DbType.Boolean, false, false, false)]
        June = 10,


        /// <summary>
        /// July : 
        /// </summary>
        [EnumTextValue("July")]
        [ColumnEnum("July", typeof(System.Boolean), System.Data.DbType.Boolean, false, false, false)]
        July = 11,        

        /// <summary>
        /// August : 
        /// </summary>
        [EnumTextValue("August")]
        [ColumnEnum("August", typeof(System.Boolean), System.Data.DbType.Boolean, false, false, false)]
        August = 12,


        /// <summary>
        /// September : 
        /// </summary>
        [EnumTextValue("September")]
        [ColumnEnum("September", typeof(System.Boolean), System.Data.DbType.Boolean, false, false, false)]
        September = 13,


        /// <summary>
        /// October : 
        /// </summary>
        [EnumTextValue("October")]
        [ColumnEnum("October", typeof(System.Boolean), System.Data.DbType.Boolean, false, false, false)]
        October = 14,

        /// <summary>
        /// November : 
        /// </summary>
        [EnumTextValue("November")]
        [ColumnEnum("November", typeof(System.Boolean), System.Data.DbType.Boolean, false, false, false)]
        November = 15,

        /// <summary>
        /// December : 
        /// </summary>
        [EnumTextValue("December")]
        [ColumnEnum("December", typeof(System.Boolean), System.Data.DbType.Boolean, false, false, false)]
        December = 16,

        /// <summary>
        /// YearCount : 
		/// </summary>
        [EnumTextValue("YearCount")]
        [ColumnEnum("YearCount", typeof(System.Int32), System.Data.DbType.Int32, false, false, false)]
        YearCount = 17,

        /// <summary>
        /// LocationSponsorUserId : 
        /// </summary>
        [EnumTextValue("LocationSponsorUserId")]
        [ColumnEnum("LocationSponsorUserId", typeof(System.Int32), System.Data.DbType.Int32, false, false, false)]
        LocationSponsorUserId = 18,

        /// <summary>
        /// LocationSpaUserId : 
        /// </summary>
        [EnumTextValue("LocationSpaUserId")]
        [ColumnEnum("LocationSpaUserId", typeof(System.Int32), System.Data.DbType.Int32, false, false, false)]
        LocationSpaUserId = 19,

        /// <summary>
		/// ModifiedDate : 
		/// </summary>
		[EnumTextValue("ModifiedDate")]
		[ColumnEnum("ModifiedDate", typeof(System.DateTime), System.Data.DbType.DateTime, false, false, false)]
		ModifiedDate = 20,

		/// <summary>
		/// ModifiedBy : 
		/// </summary>
		[EnumTextValue("ModifiedBy")]
		[ColumnEnum("ModifiedBy", typeof(System.Int32), System.Data.DbType.Int32, false, false, false)]
		ModifiedBy = 21
		
	}//End enum

	#endregion SafetyPlansSEResponsesAttachmentColumn Enum

} // end namespace
