﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.CompanySiteCategoryStandardNamesProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[CLSCompliant(true)]
	[Designer(typeof(CompanySiteCategoryStandardNamesDataSourceDesigner))]
	public class CompanySiteCategoryStandardNamesDataSource : ReadOnlyDataSource<CompanySiteCategoryStandardNames>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompanySiteCategoryStandardNamesDataSource class.
		/// </summary>
		public CompanySiteCategoryStandardNamesDataSource() : base(new CompanySiteCategoryStandardNamesService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the CompanySiteCategoryStandardNamesDataSourceView used by the CompanySiteCategoryStandardNamesDataSource.
		/// </summary>
		protected CompanySiteCategoryStandardNamesDataSourceView CompanySiteCategoryStandardNamesView
		{
			get { return ( View as CompanySiteCategoryStandardNamesDataSourceView ); }
		}
		
		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the CompanySiteCategoryStandardNamesDataSourceView class that is to be
		/// used by the CompanySiteCategoryStandardNamesDataSource.
		/// </summary>
		/// <returns>An instance of the CompanySiteCategoryStandardNamesDataSourceView class.</returns>
		protected override BaseDataSourceView<CompanySiteCategoryStandardNames, Object> GetNewDataSourceView()
		{
			return new CompanySiteCategoryStandardNamesDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the CompanySiteCategoryStandardNamesDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class CompanySiteCategoryStandardNamesDataSourceView : ReadOnlyDataSourceView<CompanySiteCategoryStandardNames>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompanySiteCategoryStandardNamesDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the CompanySiteCategoryStandardNamesDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public CompanySiteCategoryStandardNamesDataSourceView(CompanySiteCategoryStandardNamesDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal CompanySiteCategoryStandardNamesDataSource CompanySiteCategoryStandardNamesOwner
		{
			get { return Owner as CompanySiteCategoryStandardNamesDataSource; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal CompanySiteCategoryStandardNamesService CompanySiteCategoryStandardNamesProvider
		{
			get { return Provider as CompanySiteCategoryStandardNamesService; }
		}

		#endregion Properties
		
		#region Methods
		
		#endregion Methods
	}

	#region CompanySiteCategoryStandardNamesDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the CompanySiteCategoryStandardNamesDataSource class.
	/// </summary>
	public class CompanySiteCategoryStandardNamesDataSourceDesigner : ReadOnlyDataSourceDesigner<CompanySiteCategoryStandardNames>
	{
	}

	#endregion CompanySiteCategoryStandardNamesDataSourceDesigner

	#region CompanySiteCategoryStandardNamesFilter

	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CompanySiteCategoryStandardNames"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompanySiteCategoryStandardNamesFilter : SqlFilter<CompanySiteCategoryStandardNamesColumn>
	{
	}

	#endregion CompanySiteCategoryStandardNamesFilter

	#region CompanySiteCategoryStandardNamesExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CompanySiteCategoryStandardNames"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompanySiteCategoryStandardNamesExpressionBuilder : SqlExpressionBuilder<CompanySiteCategoryStandardNamesColumn>
	{
	}
	
	#endregion CompanySiteCategoryStandardNamesExpressionBuilder		
}

