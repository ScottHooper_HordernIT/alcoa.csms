
/****** Object:  StoredProcedure [dbo].[_SafetyQualificationProcess_DashBoard]    Script Date: 3/08/2015 1:43:03 PM ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[_SafetyQualificationProcess_DashBoard]                          
(                          
 @SiteId int,                          
 @Month int,                          
 @Year int                          
)           
                         
AS           
                 
DECLARE @ExpiredCurrWithProcurement INT, @ExpiredCurrWithSupplier INT, @ExpiringGtr7Days INT,          
@QuestionnaireGtr7Days INT,@AssignCompanyStatusGtr7Days INT, @QuestionnaireGtr28Days INT,           
@QuestionnaireResubmitting INT,@AccessGtr7Days INT, @ExpiredCurrWithHSAssessor INT,           
@AssignProcuQuestionnaireGtr7Days INT,@AssignQuestionnaireGtr7Days INT          
          
--DECLARE @cDay varchar(2),@cMonth varchar(2), @cYear varchar(4)          
          
          
--SELECT @cDay = [dbo].[GetDayByMonthYearDB](@Month, @Year)          
--SET  @cMonth = CAST(@Month AS VARCHAR(2))          
--SET  @cYear = CAST(@Year AS VARCHAR(4))          
-------------------          
IF (@SiteId > 0)                         
 BEGIN              
  -------------               
  SELECT @ExpiredCurrWithProcurement = COUNT(DISTINCT A.CompanyId)FROM dbo.Questionnaire AS A                 
  LEFT OUTER JOIN QuestionnaireReportExpiry B          
  ON A.QuestionnaireId = B.QuestionnaireId           
  LEFT OUTER JOIN dbo.QuestionnairePresentlyWithAction AS C          
  ON B.QuestionnairePresentlyWithActionId = C.QuestionnairePresentlyWithActionId             
  LEFT OUTER JOIN dbo.CompanySiteCategoryStandard D           
  ON D.CompanyId = A.CompanyId           
  WHERE C.ActionName = 'ProcurementExpired'          
  AND D.SiteId = @SiteId      
  AND YEAR(GETDATE()) = @Year AND MONTH(GETDATE()) = @Month              
  --AND YEAR(B.MainAssessmentValidTo) = @Year              
  --AND MONTH(B.MainAssessmentValidTo) = @Month                
  --------------          
  SELECT @ExpiredCurrWithSupplier = COUNT(DISTINCT A.CompanyId)FROM dbo.Questionnaire AS A                 
  LEFT OUTER JOIN QuestionnaireReportExpiry B          
  ON A.QuestionnaireId = B.QuestionnaireId           
  LEFT OUTER JOIN dbo.CompanySiteCategoryStandard D           
  ON D.CompanyId = A.CompanyId          
  WHERE B.QuestionnairePresentlyWIthUserDesc = 'Supplier'             
  AND D.SiteId = @SiteId              
  --AND YEAR(B.MainAssessmentValidTo) = @Year              
  --AND MONTH(B.MainAssessmentValidTo) = @Month          
--  AND DATEDIFF(DAY, B.MainAssessmentValidTo, CONVERT(DATETIME, GETDATE(), 103))>0      
  AND B.MainAssessmentValidTo < GETDATE()  
  AND YEAR(GETDATE()) = @Year AND MONTH(GETDATE()) = @Month               
  ----------------          
  SELECT @ExpiringGtr7Days = COUNT(DISTINCT A.CompanyId)FROM dbo.Questionnaire AS A                 
  LEFT OUTER JOIN QuestionnaireReportExpiry B          
  ON A.QuestionnaireId = B.QuestionnaireId           
  LEFT OUTER JOIN dbo.QuestionnairePresentlyWithAction AS C          
  ON B.QuestionnairePresentlyWithActionId = C.QuestionnairePresentlyWithActionId             
  LEFT OUTER JOIN dbo.CompanySiteCategoryStandard D           
  ON D.CompanyId = A.CompanyId           
  WHERE C.ActionName = 'ProcurementExpiring'                
  AND D.SiteId = @SiteId            
  --AND DATEDIFF(DAY, A.QuestionnairePresentlyWithSince, CONVERT(DATETIME, (@cDay+'/'+@cMonth+'/'+@cYear), 103))>7          
  AND DATEDIFF(DAY, A.QuestionnairePresentlyWithSince, CONVERT(DATETIME, GETDATE(), 103))>7      
  AND YEAR(GETDATE()) = @Year AND MONTH(GETDATE()) = @Month          
  -----------------          
  SELECT @QuestionnaireGtr7Days = COUNT(DISTINCT A.CompanyId)FROM dbo.Questionnaire AS A                 
  LEFT OUTER JOIN dbo.QuestionnairePresentlyWithAction AS C          
  ON A.QuestionnairePresentlyWithActionId = C.QuestionnairePresentlyWithActionId             
  LEFT OUTER JOIN dbo.CompanySiteCategoryStandard D           
  ON D.CompanyId = A.CompanyId           
  WHERE C.ActionName = 'ProcurementQuestionnaire'           
  AND A.QuestionnaireId = (SELECT MAX(QuestionnaireId) FROM dbo.Questionnaire WHERE CompanyId = A.CompanyId           
  AND QuestionnairePresentlyWithSince IS NOT NULL)               
  AND D.SiteId = @SiteId            
  --AND DATEDIFF(DAY, A.QuestionnairePresentlyWithSince, CONVERT(DATETIME, (@cDay+'/'+@cMonth+'/'+@cYear), 103))>7          
  AND DATEDIFF(DAY, A.QuestionnairePresentlyWithSince, CONVERT(DATETIME, GETDATE(), 103))>7       
  AND YEAR(GETDATE()) = @Year AND MONTH(GETDATE()) = @Month         
  --------------------          
  SELECT @AssignCompanyStatusGtr7Days = COUNT(DISTINCT A.CompanyId)FROM dbo.Questionnaire AS A                 
  LEFT OUTER JOIN dbo.QuestionnairePresentlyWithAction AS C          
  ON A.QuestionnairePresentlyWithActionId = C.QuestionnairePresentlyWithActionId             
  LEFT OUTER JOIN dbo.CompanySiteCategoryStandard D           
  ON D.CompanyId = A.CompanyId           
  WHERE C.ActionName = 'ProcurementAssignCompanyStatus'          
  AND A.QuestionnaireId = (SELECT MAX(QuestionnaireId) FROM dbo.Questionnaire WHERE CompanyId = A.CompanyId           
  AND QuestionnairePresentlyWithSince IS NOT NULL)               
  AND D.SiteId = @SiteId            
  --AND DATEDIFF(DAY, A.QuestionnairePresentlyWithSince, CONVERT(DATETIME, (@cDay+'/'+@cMonth+'/'+@cYear), 103))>7          
  AND DATEDIFF(DAY, A.QuestionnairePresentlyWithSince, CONVERT(DATETIME, GETDATE(), 103))>7       
  AND YEAR(GETDATE()) = @Year AND MONTH(GETDATE()) = @Month         
  --------------------              
  SELECT @QuestionnaireGtr28Days = COUNT (DISTINCT A.CompanyId)FROM dbo.Questionnaire AS A                 
  LEFT OUTER JOIN dbo.QuestionnairePresentlyWithAction AS C          
  ON A.QuestionnairePresentlyWithActionId = C.QuestionnairePresentlyWithActionId             
  LEFT OUTER JOIN dbo.CompanySiteCategoryStandard D           
  ON D.CompanyId = A.CompanyId           
  WHERE C.ActionName = 'SupplierQuestionnaireSupplier'          
  AND A.QuestionnaireId = (SELECT MAX(QuestionnaireId) FROM dbo.Questionnaire WHERE CompanyId = A.CompanyId           
  AND QuestionnairePresentlyWithSince IS NOT NULL)               
  AND D.SiteId = @SiteId            
  --AND DATEDIFF(DAY, A.QuestionnairePresentlyWithSince, CONVERT(DATETIME, (@cDay+'/'+@cMonth+'/'+@cYear), 103))>28          
  AND DATEDIFF(DAY, A.QuestionnairePresentlyWithSince, CONVERT(DATETIME, GETDATE(), 103))>28       
  AND YEAR(GETDATE()) = @Year AND MONTH(GETDATE()) = @Month         
  --------------------           
  SELECT @QuestionnaireResubmitting = COUNT(DISTINCT A.CompanyId)FROM dbo.Questionnaire AS A                 
  LEFT OUTER JOIN dbo.QuestionnairePresentlyWithAction AS C          
  ON A.QuestionnairePresentlyWithActionId = C.QuestionnairePresentlyWithActionId             
  LEFT OUTER JOIN dbo.CompanySiteCategoryStandard D           
  ON D.CompanyId = A.CompanyId           
  WHERE C.ActionName = 'SupplierQuestionnaireIncomplete'          
  AND A.QuestionnaireId = (SELECT MAX(QuestionnaireId) FROM dbo.Questionnaire WHERE CompanyId = A.CompanyId           
  AND QuestionnairePresentlyWithSince IS NOT NULL)               
  AND D.SiteId = @SiteId            
  --AND DATEDIFF(DAY, A.QuestionnairePresentlyWithSince, CONVERT(DATETIME, (@cDay+'/'+@cMonth+'/'+@cYear), 103))>28           
  AND DATEDIFF(DAY, A.QuestionnairePresentlyWithSince, CONVERT(DATETIME, GETDATE(), 103))>28       
  AND YEAR(GETDATE()) = @Year AND MONTH(GETDATE()) = @Month           
  --------------------              
  SELECT @AccessGtr7Days = COUNT (DISTINCT A.CompanyId)FROM dbo.Questionnaire AS A                 
  LEFT OUTER JOIN dbo.QuestionnairePresentlyWithAction AS C          
  ON A.QuestionnairePresentlyWithActionId = C.QuestionnairePresentlyWithActionId             
  LEFT OUTER JOIN dbo.CompanySiteCategoryStandard D           
  ON D.CompanyId = A.CompanyId           
  WHERE C.ActionName = 'CsmsCreateCompanyLogins'          
  AND A.QuestionnaireId = (SELECT MAX(QuestionnaireId) FROM dbo.Questionnaire WHERE CompanyId = A.CompanyId           
  AND QuestionnairePresentlyWithSince IS NOT NULL)               
  AND D.SiteId = @SiteId            
  --AND DATEDIFF(DAY, A.QuestionnairePresentlyWithSince, CONVERT(DATETIME, (@cDay+'/'+@cMonth+'/'+@cYear), 103))>7          
  AND DATEDIFF(DAY, A.QuestionnairePresentlyWithSince, CONVERT(DATETIME, GETDATE(), 103))>7       
  AND YEAR(GETDATE()) = @Year AND MONTH(GETDATE()) = @Month         
  --------------------            
  SELECT @ExpiredCurrWithHSAssessor = COUNT(DISTINCT A.CompanyId)FROM dbo.Questionnaire AS A                 
  LEFT OUTER JOIN QuestionnaireReportExpiry B          
  ON A.QuestionnaireId = B.QuestionnaireId           
  LEFT OUTER JOIN dbo.CompanySiteCategoryStandard D          
  ON D.CompanyId = A.CompanyId           
  WHERE B.QuestionnairePresentlyWIthUserDesc = 'H&S Assessor'             
  AND D.SiteId = @SiteId              
  --AND YEAR(B.MainAssessmentValidTo) = @Year              
  --AND MONTH(B.MainAssessmentValidTo) = @Month          
--  AND DATEDIFF(DAY, B.MainAssessmentValidTo, CONVERT(DATETIME, GETDATE(), 103))>0      
  AND B.MainAssessmentValidTo < GETDATE()    
  AND YEAR(GETDATE()) = @Year AND MONTH(GETDATE()) = @Month        
  --------------------          
  SELECT @AssignProcuQuestionnaireGtr7Days = COUNT (DISTINCT A.CompanyId)FROM dbo.Questionnaire AS A                 
  LEFT OUTER JOIN dbo.QuestionnairePresentlyWithAction AS C          
  ON A.QuestionnairePresentlyWithActionId = C.QuestionnairePresentlyWithActionId             
LEFT OUTER JOIN dbo.CompanySiteCategoryStandard D           
  ON D.CompanyId = A.CompanyId           
  WHERE C.ActionName = 'HSAssessorQuestionnaireProcurement'          
  AND A.QuestionnaireId = (SELECT MAX(QuestionnaireId) FROM dbo.Questionnaire WHERE CompanyId = A.CompanyId           
  AND QuestionnairePresentlyWithSince IS NOT NULL)               
  AND D.SiteId = @SiteId            
  --AND DATEDIFF(DAY, A.QuestionnairePresentlyWithSince, CONVERT(DATETIME, (@cDay+'/'+@cMonth+'/'+@cYear), 103))>7          
  AND DATEDIFF(DAY, A.QuestionnairePresentlyWithSince, CONVERT(DATETIME, GETDATE(), 103))>7       
  AND YEAR(GETDATE()) = @Year AND MONTH(GETDATE()) = @Month         
  --------------------          
  SELECT @AssignQuestionnaireGtr7Days = COUNT(DISTINCT A.CompanyId)FROM dbo.Questionnaire AS A                 
  LEFT OUTER JOIN dbo.QuestionnairePresentlyWithAction AS C          
  ON A.QuestionnairePresentlyWithActionId = C.QuestionnairePresentlyWithActionId             
  LEFT OUTER JOIN dbo.CompanySiteCategoryStandard D           
  ON D.CompanyId = A.CompanyId           
  WHERE C.ActionName = 'HSAssessorQuestionnaire'          
  AND A.QuestionnaireId = (SELECT MAX(QuestionnaireId) FROM dbo.Questionnaire WHERE CompanyId = A.CompanyId           
  AND QuestionnairePresentlyWithSince IS NOT NULL)               
  AND D.SiteId = @SiteId            
  --AND DATEDIFF(DAY, A.QuestionnairePresentlyWithSince, CONVERT(DATETIME, (@cDay+'/'+@cMonth+'/'+@cYear), 103))>7          
  AND DATEDIFF(DAY, A.QuestionnairePresentlyWithSince, CONVERT(DATETIME, GETDATE(), 103))>7       
  AND YEAR(GETDATE()) = @Year AND MONTH(GETDATE()) = @Month         
  --------------------          
            
  SELECT @ExpiredCurrWithProcurement AS [Expired - Currently With Procurement],           
  @ExpiredCurrWithSupplier AS [Expired - Currently With Supplier],           
  @ExpiringGtr7Days AS [Expiring > 7 days],          
  @QuestionnaireGtr7Days AS [Questionnaire  > 7 days],          
  @AssignCompanyStatusGtr7Days AS [Assign Company Status > 7 days],           
  @QuestionnaireGtr28Days AS [Questionnaire > 28 Days],           
  @QuestionnaireResubmitting AS [Questionnaire - Resubmitting],          
  @AccessGtr7Days AS [Access > 7 Days],           
  @ExpiredCurrWithHSAssessor AS [Expired - Currently With H&S Assessor],           
  @AssignProcuQuestionnaireGtr7Days AS [Assessing Procurement Questionnaire > 7 days],          
  @AssignQuestionnaireGtr7Days AS [Assessing Questionnaire > 7 days],          
  (@ExpiredCurrWithProcurement + @ExpiringGtr7Days + @QuestionnaireGtr7Days + @AssignCompanyStatusGtr7Days + @QuestionnaireGtr28Days + @QuestionnaireResubmitting + @AssignProcuQuestionnaireGtr7Days + @AssignQuestionnaireGtr7Days) AS [SQ process total]    
  
    
      
 END                  
ELSE                  
 BEGIN           
  -------------               
  SELECT @ExpiredCurrWithProcurement = COUNT(DISTINCT A.CompanyId)FROM dbo.Questionnaire AS A                 
  LEFT OUTER JOIN QuestionnaireReportExpiry B          
  ON A.QuestionnaireId = B.QuestionnaireId           
  LEFT OUTER JOIN dbo.QuestionnairePresentlyWithAction AS C          
  ON B.QuestionnairePresentlyWithActionId = C.QuestionnairePresentlyWithActionId             
  LEFT OUTER JOIN dbo.CompanySiteCategoryStandard D           
  ON D.CompanyId = A.CompanyId           
  WHERE C.ActionName = 'ProcurementExpired'          
  AND D.SiteId IN (SELECT SiteId FROM RegionsSites WHERE (-1 * dbo.RegionsSites.RegionId) = @SiteId)               
  --AND YEAR(B.MainAssessmentValidTo) = @Year              
  --AND MONTH(B.MainAssessmentValidTo) = @Month      
  AND YEAR(GETDATE()) = @Year AND MONTH(GETDATE()) = @Month                
  --------------          
  SELECT @ExpiredCurrWithSupplier = COUNT(DISTINCT A.CompanyId)FROM dbo.Questionnaire AS A                 
  LEFT OUTER JOIN QuestionnaireReportExpiry B          
  ON A.QuestionnaireId = B.QuestionnaireId           
  LEFT OUTER JOIN dbo.CompanySiteCategoryStandard D           
  ON D.CompanyId = A.CompanyId          
  WHERE B.QuestionnairePresentlyWIthUserDesc = 'Supplier'             
  AND D.SiteId IN (SELECT SiteId FROM RegionsSites WHERE (-1 * dbo.RegionsSites.RegionId) = @SiteId)               
  --AND YEAR(B.MainAssessmentValidTo) = @Year              
  --AND MONTH(B.MainAssessmentValidTo) = @Month          
 -- AND DATEDIFF(DAY, B.MainAssessmentValidTo, CONVERT(DATETIME, GETDATE(), 103))>0    
  AND B.MainAssessmentValidTo < GETDATE()      
  AND YEAR(GETDATE()) = @Year AND MONTH(GETDATE()) = @Month             
  ----------------          
  SELECT @ExpiringGtr7Days = COUNT(DISTINCT A.CompanyId)FROM dbo.Questionnaire AS A                 
LEFT OUTER JOIN QuestionnaireReportExpiry B          
  ON A.QuestionnaireId = B.QuestionnaireId           
  LEFT OUTER JOIN dbo.QuestionnairePresentlyWithAction AS C          
  ON B.QuestionnairePresentlyWithActionId = C.QuestionnairePresentlyWithActionId             
  LEFT OUTER JOIN dbo.CompanySiteCategoryStandard D           
  ON D.CompanyId = A.CompanyId           
  WHERE C.ActionName = 'ProcurementExpiring'                
  AND D.SiteId IN (SELECT SiteId FROM RegionsSites WHERE (-1 * dbo.RegionsSites.RegionId) = @SiteId)             
  --AND DATEDIFF(DAY, A.QuestionnairePresentlyWithSince, CONVERT(DATETIME, (@cDay+'/'+@cMonth+'/'+@cYear), 103))>7          
  AND DATEDIFF(DAY, A.QuestionnairePresentlyWithSince, CONVERT(DATETIME, GETDATE(), 103))>7        
  AND YEAR(GETDATE()) = @Year AND MONTH(GETDATE()) = @Month        
  -----------------          
  SELECT @QuestionnaireGtr7Days = COUNT(DISTINCT A.CompanyId)FROM dbo.Questionnaire AS A                 
  LEFT OUTER JOIN dbo.QuestionnairePresentlyWithAction AS C          
  ON A.QuestionnairePresentlyWithActionId = C.QuestionnairePresentlyWithActionId             
  LEFT OUTER JOIN dbo.CompanySiteCategoryStandard D           
  ON D.CompanyId = A.CompanyId           
  WHERE C.ActionName = 'ProcurementQuestionnaire'           
  AND A.QuestionnaireId = (SELECT MAX(QuestionnaireId) FROM dbo.Questionnaire WHERE CompanyId = A.CompanyId           
  AND QuestionnairePresentlyWithSince IS NOT NULL)               
  AND D.SiteId IN (SELECT SiteId FROM RegionsSites WHERE (-1 * dbo.RegionsSites.RegionId) = @SiteId)            
  --AND DATEDIFF(DAY, A.QuestionnairePresentlyWithSince, CONVERT(DATETIME, (@cDay+'/'+@cMonth+'/'+@cYear), 103))>7          
  AND DATEDIFF(DAY, A.QuestionnairePresentlyWithSince, CONVERT(DATETIME, GETDATE(), 103))>7      
  AND YEAR(GETDATE()) = @Year AND MONTH(GETDATE()) = @Month          
  --------------------          
  SELECT @AssignCompanyStatusGtr7Days = COUNT(DISTINCT A.CompanyId)FROM dbo.Questionnaire AS A                 
  LEFT OUTER JOIN dbo.QuestionnairePresentlyWithAction AS C          
  ON A.QuestionnairePresentlyWithActionId = C.QuestionnairePresentlyWithActionId             
  LEFT OUTER JOIN dbo.CompanySiteCategoryStandard D           
  ON D.CompanyId = A.CompanyId           
  WHERE C.ActionName = 'ProcurementAssignCompanyStatus'          
  AND A.QuestionnaireId = (SELECT MAX(QuestionnaireId) FROM dbo.Questionnaire WHERE CompanyId = A.CompanyId           AND QuestionnairePresentlyWithSince IS NOT NULL)               
  AND D.SiteId IN (SELECT SiteId FROM RegionsSites WHERE (-1 * dbo.RegionsSites.RegionId) = @SiteId)             
  --AND DATEDIFF(DAY, A.QuestionnairePresentlyWithSince, CONVERT(DATETIME, (@cDay+'/'+@cMonth+'/'+@cYear), 103))>7          
  AND DATEDIFF(DAY, A.QuestionnairePresentlyWithSince, CONVERT(DATETIME, GETDATE(), 103))>7        
  AND YEAR(GETDATE()) = @Year AND MONTH(GETDATE()) = @Month        
  --------------------              
 SELECT @QuestionnaireGtr28Days = COUNT (DISTINCT A.CompanyId)FROM dbo.Questionnaire AS A                 
  LEFT OUTER JOIN dbo.QuestionnairePresentlyWithAction AS C          
  ON A.QuestionnairePresentlyWithActionId = C.QuestionnairePresentlyWithActionId             
  LEFT OUTER JOIN dbo.CompanySiteCategoryStandard D           
  ON D.CompanyId = A.CompanyId           
  WHERE C.ActionName = 'SupplierQuestionnaireSupplier'          
  AND A.QuestionnaireId = (SELECT MAX(QuestionnaireId) FROM dbo.Questionnaire WHERE CompanyId = A.CompanyId           
  AND QuestionnairePresentlyWithSince IS NOT NULL)               
  AND D.SiteId IN (SELECT SiteId FROM RegionsSites WHERE (-1 * dbo.RegionsSites.RegionId) = @SiteId)             
  --AND DATEDIFF(DAY, A.QuestionnairePresentlyWithSince, CONVERT(DATETIME, (@cDay+'/'+@cMonth+'/'+@cYear), 103))>28          
  AND DATEDIFF(DAY, A.QuestionnairePresentlyWithSince, CONVERT(DATETIME, GETDATE(), 103))>28       
  AND YEAR(GETDATE()) = @Year AND MONTH(GETDATE()) = @Month         
  --------------------           
  SELECT @QuestionnaireResubmitting = COUNT(DISTINCT A.CompanyId)FROM dbo.Questionnaire AS A                 
  LEFT OUTER JOIN dbo.QuestionnairePresentlyWithAction AS C          
  ON A.QuestionnairePresentlyWithActionId = C.QuestionnairePresentlyWithActionId             
  LEFT OUTER JOIN dbo.CompanySiteCategoryStandard D           
  ON D.CompanyId = A.CompanyId           
  WHERE C.ActionName = 'SupplierQuestionnaireIncomplete'          
  AND A.QuestionnaireId = (SELECT MAX(QuestionnaireId) FROM dbo.Questionnaire WHERE CompanyId = A.CompanyId           
  AND QuestionnairePresentlyWithSince IS NOT NULL)               
  AND D.SiteId IN (SELECT SiteId FROM RegionsSites WHERE (-1 * dbo.RegionsSites.RegionId) = @SiteId)             
  --AND DATEDIFF(DAY, A.QuestionnairePresentlyWithSince, CONVERT(DATETIME, (@cDay+'/'+@cMonth+'/'+@cYear), 103))>28           
  AND DATEDIFF(DAY, A.QuestionnairePresentlyWithSince, CONVERT(DATETIME, GETDATE(), 103))>28         
  AND YEAR(GETDATE()) = @Year AND MONTH(GETDATE()) = @Month          
  --------------------              
  SELECT @AccessGtr7Days = COUNT (DISTINCT A.CompanyId)FROM dbo.Questionnaire AS A           LEFT OUTER JOIN dbo.QuestionnairePresentlyWithAction AS C          
  ON A.QuestionnairePresentlyWithActionId = C.QuestionnairePresentlyWithActionId             
  LEFT OUTER JOIN dbo.CompanySiteCategoryStandard D           
  ON D.CompanyId = A.CompanyId           
  WHERE C.ActionName = 'CsmsCreateCompanyLogins'          
  AND A.QuestionnaireId = (SELECT MAX(QuestionnaireId) FROM dbo.Questionnaire WHERE CompanyId = A.CompanyId           
  AND QuestionnairePresentlyWithSince IS NOT NULL)               
  AND D.SiteId IN (SELECT SiteId FROM RegionsSites WHERE (-1 * dbo.RegionsSites.RegionId) = @SiteId)             
  --AND DATEDIFF(DAY, A.QuestionnairePresentlyWithSince, CONVERT(DATETIME, (@cDay+'/'+@cMonth+'/'+@cYear), 103))>7          
  AND DATEDIFF(DAY, A.QuestionnairePresentlyWithSince, CONVERT(DATETIME, GETDATE(), 103))>7        
  AND YEAR(GETDATE()) = @Year AND MONTH(GETDATE()) = @Month        
  --------------------            
  SELECT @ExpiredCurrWithHSAssessor = COUNT(DISTINCT A.CompanyId)FROM dbo.Questionnaire AS A                 
  LEFT OUTER JOIN QuestionnaireReportExpiry B          
  ON A.QuestionnaireId = B.QuestionnaireId           
  LEFT OUTER JOIN dbo.CompanySiteCategoryStandard D          
  ON D.CompanyId = A.CompanyId           
  WHERE B.QuestionnairePresentlyWIthUserDesc = 'H&S Assessor'             
  AND D.SiteId IN (SELECT SiteId FROM RegionsSites WHERE (-1 * dbo.RegionsSites.RegionId) = @SiteId)               
  --AND YEAR(B.MainAssessmentValidTo) = @Year              
  --AND MONTH(B.MainAssessmentValidTo) = @Month          
  -- AND DATEDIFF(DAY, B.MainAssessmentValidTo, CONVERT(DATETIME, GETDATE(), 103))>0      
  AND B.MainAssessmentValidTo < GETDATE()    
  AND YEAR(GETDATE()) = @Year AND MONTH(GETDATE()) = @Month        
  --------------------          
  SELECT @AssignProcuQuestionnaireGtr7Days = COUNT (DISTINCT A.CompanyId)FROM dbo.Questionnaire AS A                 
  LEFT OUTER JOIN dbo.QuestionnairePresentlyWithAction AS C          
  ON A.QuestionnairePresentlyWithActionId = C.QuestionnairePresentlyWithActionId             
  LEFT OUTER JOIN dbo.CompanySiteCategoryStandard D           
  ON D.CompanyId = A.CompanyId           
  WHERE C.ActionName = 'HSAssessorQuestionnaireProcurement'          
  AND A.QuestionnaireId = (SELECT MAX(QuestionnaireId) FROM dbo.Questionnaire WHERE CompanyId = A.CompanyId           
  AND QuestionnairePresentlyWithSince IS NOT NULL)               
  AND D.SiteId IN (SELECT SiteId FROM RegionsSites WHERE (-1 * dbo.RegionsSites.RegionId) = @SiteId)             
  --AND DATEDIFF(DAY, A.QuestionnairePresentlyWithSince, CONVERT(DATETIME, (@cDay+'/'+@cMonth+'/'+@cYear), 103))>7          
  AND DATEDIFF(DAY, A.QuestionnairePresentlyWithSince, CONVERT(DATETIME, GETDATE(), 103))>7       
  AND YEAR(GETDATE()) = @Year AND MONTH(GETDATE()) = @Month         
  --------------------          
  SELECT @AssignQuestionnaireGtr7Days = COUNT(DISTINCT A.CompanyId)FROM dbo.Questionnaire AS A                 
  LEFT OUTER JOIN dbo.QuestionnairePresentlyWithAction AS C          
  ON A.QuestionnairePresentlyWithActionId = C.QuestionnairePresentlyWithActionId             
  LEFT OUTER JOIN dbo.CompanySiteCategoryStandard D           
  ON D.CompanyId = A.CompanyId           
  WHERE C.ActionName = 'HSAssessorQuestionnaire'          
  AND A.QuestionnaireId = (SELECT MAX(QuestionnaireId) FROM dbo.Questionnaire WHERE CompanyId = A.CompanyId           
  AND QuestionnairePresentlyWithSince IS NOT NULL)               
  AND D.SiteId IN (SELECT SiteId FROM RegionsSites WHERE (-1 * dbo.RegionsSites.RegionId) = @SiteId)           
  --AND DATEDIFF(DAY, A.QuestionnairePresentlyWithSince, CONVERT(DATETIME, (@cDay+'/'+@cMonth+'/'+@cYear), 103))>7          
  AND DATEDIFF(DAY, A.QuestionnairePresentlyWithSince, CONVERT(DATETIME, GETDATE(), 103))>7       
  AND YEAR(GETDATE()) = @Year AND MONTH(GETDATE()) = @Month      
  --------------------          
           
  SELECT @ExpiredCurrWithProcurement AS [Expired - Currently With Procurement],           
  @ExpiredCurrWithSupplier AS [Expired - Currently With Supplier],           
  @ExpiringGtr7Days AS [Expiring > 7 days],          
  @QuestionnaireGtr7Days AS [Questionnaire  > 7 days],          
  @AssignCompanyStatusGtr7Days AS [Assign Company Status > 7 days],           
  @QuestionnaireGtr28Days AS [Questionnaire > 28 Days],           
  @QuestionnaireResubmitting AS [Questionnaire - Resubmitting],          
  @AccessGtr7Days AS [Access > 7 Days],           
  @ExpiredCurrWithHSAssessor AS [Expired - Currently With H&S Assessor],           
  @AssignProcuQuestionnaireGtr7Days AS [Assessing Procurement Questionnaire > 7 days],          
  @AssignQuestionnaireGtr7Days AS [Assessing Questionnaire > 7 days],          
  (@ExpiredCurrWithProcurement + @ExpiringGtr7Days + @QuestionnaireGtr7Days + @AssignCompanyStatusGtr7Days + @QuestionnaireGtr28Days + @QuestionnaireResubmitting + @AssignProcuQuestionnaireGtr7Days + @AssignQuestionnaireGtr7Days) AS [SQ process total]    
  
    
     
       
 END          
-----------------------------------------------------------------------------------------------

GO


