

/****** Object:  StoredProcedure [dbo].[Ebi_UpdateVendorNoSource]    Script Date: 3/08/2015 4:46:15 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Ebi_UpdateVendorNoSource]              
(              
 @ClockId int,              
 @EbiId  int,            
 @SwipeDateTime DateTime,             
 @Source varchar(max)              
)              
AS              
 DECLARE @VendorNo VARCHAR(MAX)         
 Set @VendorNo=null            
 SELECT TOP 1 @VendorNo=CWK_VENDOR_NO              
 FROM HR..XXHR_CWK_HISTORY_V             
 WHERE CWK_NBR=@ClockId AND EFFECTIVE_END_DATE>=@SwipeDateTime              
 ORDER BY EFFECTIVE_END_DATE ASC              
                  
 -- Modify the Ebi table              
 UPDATE              
 [dbo].[Ebi]              
 SET              
 [Vendor_Number] = @VendorNo,              
 [Source]=@Source              
 WHERE              
 [EbiId] = @EbiId; 

------------------------------------------------------------------------------



GO


