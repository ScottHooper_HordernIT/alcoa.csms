﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="CsmsEmailRecipientTypeProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class CsmsEmailRecipientTypeProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.CsmsEmailRecipientType, KaiZen.CSMS.Entities.CsmsEmailRecipientTypeKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.CsmsEmailRecipientTypeKey key)
		{
			return Delete(transactionManager, key.CsmsEmailRecipientEmailTypeId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_csmsEmailRecipientEmailTypeId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _csmsEmailRecipientEmailTypeId)
		{
			return Delete(null, _csmsEmailRecipientEmailTypeId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_csmsEmailRecipientEmailTypeId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _csmsEmailRecipientEmailTypeId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.CsmsEmailRecipientType Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.CsmsEmailRecipientTypeKey key, int start, int pageLength)
		{
			return GetByCsmsEmailRecipientEmailTypeId(transactionManager, key.CsmsEmailRecipientEmailTypeId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_CsmsEmailRecipientType index.
		/// </summary>
		/// <param name="_csmsEmailRecipientEmailTypeId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CsmsEmailRecipientType"/> class.</returns>
		public KaiZen.CSMS.Entities.CsmsEmailRecipientType GetByCsmsEmailRecipientEmailTypeId(System.Int32 _csmsEmailRecipientEmailTypeId)
		{
			int count = -1;
			return GetByCsmsEmailRecipientEmailTypeId(null,_csmsEmailRecipientEmailTypeId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CsmsEmailRecipientType index.
		/// </summary>
		/// <param name="_csmsEmailRecipientEmailTypeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CsmsEmailRecipientType"/> class.</returns>
		public KaiZen.CSMS.Entities.CsmsEmailRecipientType GetByCsmsEmailRecipientEmailTypeId(System.Int32 _csmsEmailRecipientEmailTypeId, int start, int pageLength)
		{
			int count = -1;
			return GetByCsmsEmailRecipientEmailTypeId(null, _csmsEmailRecipientEmailTypeId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CsmsEmailRecipientType index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_csmsEmailRecipientEmailTypeId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CsmsEmailRecipientType"/> class.</returns>
		public KaiZen.CSMS.Entities.CsmsEmailRecipientType GetByCsmsEmailRecipientEmailTypeId(TransactionManager transactionManager, System.Int32 _csmsEmailRecipientEmailTypeId)
		{
			int count = -1;
			return GetByCsmsEmailRecipientEmailTypeId(transactionManager, _csmsEmailRecipientEmailTypeId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CsmsEmailRecipientType index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_csmsEmailRecipientEmailTypeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CsmsEmailRecipientType"/> class.</returns>
		public KaiZen.CSMS.Entities.CsmsEmailRecipientType GetByCsmsEmailRecipientEmailTypeId(TransactionManager transactionManager, System.Int32 _csmsEmailRecipientEmailTypeId, int start, int pageLength)
		{
			int count = -1;
			return GetByCsmsEmailRecipientEmailTypeId(transactionManager, _csmsEmailRecipientEmailTypeId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CsmsEmailRecipientType index.
		/// </summary>
		/// <param name="_csmsEmailRecipientEmailTypeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CsmsEmailRecipientType"/> class.</returns>
		public KaiZen.CSMS.Entities.CsmsEmailRecipientType GetByCsmsEmailRecipientEmailTypeId(System.Int32 _csmsEmailRecipientEmailTypeId, int start, int pageLength, out int count)
		{
			return GetByCsmsEmailRecipientEmailTypeId(null, _csmsEmailRecipientEmailTypeId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CsmsEmailRecipientType index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_csmsEmailRecipientEmailTypeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CsmsEmailRecipientType"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.CsmsEmailRecipientType GetByCsmsEmailRecipientEmailTypeId(TransactionManager transactionManager, System.Int32 _csmsEmailRecipientEmailTypeId, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key UK_CsmsEmailRecipientType_TypeName index.
		/// </summary>
		/// <param name="_typeName"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CsmsEmailRecipientType"/> class.</returns>
		public KaiZen.CSMS.Entities.CsmsEmailRecipientType GetByTypeName(System.String _typeName)
		{
			int count = -1;
			return GetByTypeName(null,_typeName, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the UK_CsmsEmailRecipientType_TypeName index.
		/// </summary>
		/// <param name="_typeName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CsmsEmailRecipientType"/> class.</returns>
		public KaiZen.CSMS.Entities.CsmsEmailRecipientType GetByTypeName(System.String _typeName, int start, int pageLength)
		{
			int count = -1;
			return GetByTypeName(null, _typeName, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the UK_CsmsEmailRecipientType_TypeName index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_typeName"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CsmsEmailRecipientType"/> class.</returns>
		public KaiZen.CSMS.Entities.CsmsEmailRecipientType GetByTypeName(TransactionManager transactionManager, System.String _typeName)
		{
			int count = -1;
			return GetByTypeName(transactionManager, _typeName, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the UK_CsmsEmailRecipientType_TypeName index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_typeName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CsmsEmailRecipientType"/> class.</returns>
		public KaiZen.CSMS.Entities.CsmsEmailRecipientType GetByTypeName(TransactionManager transactionManager, System.String _typeName, int start, int pageLength)
		{
			int count = -1;
			return GetByTypeName(transactionManager, _typeName, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the UK_CsmsEmailRecipientType_TypeName index.
		/// </summary>
		/// <param name="_typeName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CsmsEmailRecipientType"/> class.</returns>
		public KaiZen.CSMS.Entities.CsmsEmailRecipientType GetByTypeName(System.String _typeName, int start, int pageLength, out int count)
		{
			return GetByTypeName(null, _typeName, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the UK_CsmsEmailRecipientType_TypeName index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_typeName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CsmsEmailRecipientType"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.CsmsEmailRecipientType GetByTypeName(TransactionManager transactionManager, System.String _typeName, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;CsmsEmailRecipientType&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;CsmsEmailRecipientType&gt;"/></returns>
		public static TList<CsmsEmailRecipientType> Fill(IDataReader reader, TList<CsmsEmailRecipientType> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.CsmsEmailRecipientType c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("CsmsEmailRecipientType")
					.Append("|").Append((System.Int32)reader[((int)CsmsEmailRecipientTypeColumn.CsmsEmailRecipientEmailTypeId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<CsmsEmailRecipientType>(
					key.ToString(), // EntityTrackingKey
					"CsmsEmailRecipientType",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.CsmsEmailRecipientType();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.CsmsEmailRecipientEmailTypeId = (System.Int32)reader[((int)CsmsEmailRecipientTypeColumn.CsmsEmailRecipientEmailTypeId - 1)];
					c.TypeName = (System.String)reader[((int)CsmsEmailRecipientTypeColumn.TypeName - 1)];
					c.TypeDesc = (System.String)reader[((int)CsmsEmailRecipientTypeColumn.TypeDesc - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.CsmsEmailRecipientType"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.CsmsEmailRecipientType"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.CsmsEmailRecipientType entity)
		{
			if (!reader.Read()) return;
			
			entity.CsmsEmailRecipientEmailTypeId = (System.Int32)reader[((int)CsmsEmailRecipientTypeColumn.CsmsEmailRecipientEmailTypeId - 1)];
			entity.TypeName = (System.String)reader[((int)CsmsEmailRecipientTypeColumn.TypeName - 1)];
			entity.TypeDesc = (System.String)reader[((int)CsmsEmailRecipientTypeColumn.TypeDesc - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.CsmsEmailRecipientType"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.CsmsEmailRecipientType"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.CsmsEmailRecipientType entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.CsmsEmailRecipientEmailTypeId = (System.Int32)dataRow["CsmsEmailRecipientEmailTypeId"];
			entity.TypeName = (System.String)dataRow["TypeName"];
			entity.TypeDesc = (System.String)dataRow["TypeDesc"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.CsmsEmailRecipientType"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.CsmsEmailRecipientType Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.CsmsEmailRecipientType entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetByCsmsEmailRecipientEmailTypeId methods when available
			
			#region CsmsEmailLogRecipientCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<CsmsEmailLogRecipient>|CsmsEmailLogRecipientCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CsmsEmailLogRecipientCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.CsmsEmailLogRecipientCollection = DataRepository.CsmsEmailLogRecipientProvider.GetByCsmsEmailRecipientTypeId(transactionManager, entity.CsmsEmailRecipientEmailTypeId);

				if (deep && entity.CsmsEmailLogRecipientCollection.Count > 0)
				{
					deepHandles.Add("CsmsEmailLogRecipientCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<CsmsEmailLogRecipient>) DataRepository.CsmsEmailLogRecipientProvider.DeepLoad,
						new object[] { transactionManager, entity.CsmsEmailLogRecipientCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region AdminTaskEmailTemplateRecipientCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<AdminTaskEmailTemplateRecipient>|AdminTaskEmailTemplateRecipientCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'AdminTaskEmailTemplateRecipientCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.AdminTaskEmailTemplateRecipientCollection = DataRepository.AdminTaskEmailTemplateRecipientProvider.GetByCsmsEmailRecipientTypeId(transactionManager, entity.CsmsEmailRecipientEmailTypeId);

				if (deep && entity.AdminTaskEmailTemplateRecipientCollection.Count > 0)
				{
					deepHandles.Add("AdminTaskEmailTemplateRecipientCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<AdminTaskEmailTemplateRecipient>) DataRepository.AdminTaskEmailTemplateRecipientProvider.DeepLoad,
						new object[] { transactionManager, entity.AdminTaskEmailTemplateRecipientCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.CsmsEmailRecipientType object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.CsmsEmailRecipientType instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.CsmsEmailRecipientType Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.CsmsEmailRecipientType entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<CsmsEmailLogRecipient>
				if (CanDeepSave(entity.CsmsEmailLogRecipientCollection, "List<CsmsEmailLogRecipient>|CsmsEmailLogRecipientCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(CsmsEmailLogRecipient child in entity.CsmsEmailLogRecipientCollection)
					{
						if(child.CsmsEmailRecipientTypeIdSource != null)
						{
							child.CsmsEmailRecipientTypeId = child.CsmsEmailRecipientTypeIdSource.CsmsEmailRecipientEmailTypeId;
						}
						else
						{
							child.CsmsEmailRecipientTypeId = entity.CsmsEmailRecipientEmailTypeId;
						}

					}

					if (entity.CsmsEmailLogRecipientCollection.Count > 0 || entity.CsmsEmailLogRecipientCollection.DeletedItems.Count > 0)
					{
						//DataRepository.CsmsEmailLogRecipientProvider.Save(transactionManager, entity.CsmsEmailLogRecipientCollection);
						
						deepHandles.Add("CsmsEmailLogRecipientCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< CsmsEmailLogRecipient >) DataRepository.CsmsEmailLogRecipientProvider.DeepSave,
							new object[] { transactionManager, entity.CsmsEmailLogRecipientCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<AdminTaskEmailTemplateRecipient>
				if (CanDeepSave(entity.AdminTaskEmailTemplateRecipientCollection, "List<AdminTaskEmailTemplateRecipient>|AdminTaskEmailTemplateRecipientCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(AdminTaskEmailTemplateRecipient child in entity.AdminTaskEmailTemplateRecipientCollection)
					{
						if(child.CsmsEmailRecipientTypeIdSource != null)
						{
							child.CsmsEmailRecipientTypeId = child.CsmsEmailRecipientTypeIdSource.CsmsEmailRecipientEmailTypeId;
						}
						else
						{
							child.CsmsEmailRecipientTypeId = entity.CsmsEmailRecipientEmailTypeId;
						}

					}

					if (entity.AdminTaskEmailTemplateRecipientCollection.Count > 0 || entity.AdminTaskEmailTemplateRecipientCollection.DeletedItems.Count > 0)
					{
						//DataRepository.AdminTaskEmailTemplateRecipientProvider.Save(transactionManager, entity.AdminTaskEmailTemplateRecipientCollection);
						
						deepHandles.Add("AdminTaskEmailTemplateRecipientCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< AdminTaskEmailTemplateRecipient >) DataRepository.AdminTaskEmailTemplateRecipientProvider.DeepSave,
							new object[] { transactionManager, entity.AdminTaskEmailTemplateRecipientCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region CsmsEmailRecipientTypeChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.CsmsEmailRecipientType</c>
	///</summary>
	public enum CsmsEmailRecipientTypeChildEntityTypes
	{

		///<summary>
		/// Collection of <c>CsmsEmailRecipientType</c> as OneToMany for CsmsEmailLogRecipientCollection
		///</summary>
		[ChildEntityType(typeof(TList<CsmsEmailLogRecipient>))]
		CsmsEmailLogRecipientCollection,

		///<summary>
		/// Collection of <c>CsmsEmailRecipientType</c> as OneToMany for AdminTaskEmailTemplateRecipientCollection
		///</summary>
		[ChildEntityType(typeof(TList<AdminTaskEmailTemplateRecipient>))]
		AdminTaskEmailTemplateRecipientCollection,
	}
	
	#endregion CsmsEmailRecipientTypeChildEntityTypes
	
	#region CsmsEmailRecipientTypeFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;CsmsEmailRecipientTypeColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CsmsEmailRecipientType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CsmsEmailRecipientTypeFilterBuilder : SqlFilterBuilder<CsmsEmailRecipientTypeColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CsmsEmailRecipientTypeFilterBuilder class.
		/// </summary>
		public CsmsEmailRecipientTypeFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the CsmsEmailRecipientTypeFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CsmsEmailRecipientTypeFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CsmsEmailRecipientTypeFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CsmsEmailRecipientTypeFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CsmsEmailRecipientTypeFilterBuilder
	
	#region CsmsEmailRecipientTypeParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;CsmsEmailRecipientTypeColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CsmsEmailRecipientType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CsmsEmailRecipientTypeParameterBuilder : ParameterizedSqlFilterBuilder<CsmsEmailRecipientTypeColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CsmsEmailRecipientTypeParameterBuilder class.
		/// </summary>
		public CsmsEmailRecipientTypeParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the CsmsEmailRecipientTypeParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CsmsEmailRecipientTypeParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CsmsEmailRecipientTypeParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CsmsEmailRecipientTypeParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CsmsEmailRecipientTypeParameterBuilder
	
	#region CsmsEmailRecipientTypeSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;CsmsEmailRecipientTypeColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CsmsEmailRecipientType"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class CsmsEmailRecipientTypeSortBuilder : SqlSortBuilder<CsmsEmailRecipientTypeColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CsmsEmailRecipientTypeSqlSortBuilder class.
		/// </summary>
		public CsmsEmailRecipientTypeSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion CsmsEmailRecipientTypeSortBuilder
	
} // end namespace
