﻿#region Using directives

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="KpiEngineeringProjectHoursProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract class KpiEngineeringProjectHoursProviderBaseCore : EntityViewProviderBase<KpiEngineeringProjectHours>
	{
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions
		
		/*
		///<summary>
		/// Fill an VList&lt;KpiEngineeringProjectHours&gt; From a DataSet
		///</summary>
		/// <param name="dataSet">the DataSet</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList&lt;KpiEngineeringProjectHours&gt;"/></returns>
		protected static VList&lt;KpiEngineeringProjectHours&gt; Fill(DataSet dataSet, VList<KpiEngineeringProjectHours> rows, int start, int pagelen)
		{
			if (dataSet.Tables.Count == 1)
			{
				return Fill(dataSet.Tables[0], rows, start, pagelen);
			}
			else
			{
				return new VList<KpiEngineeringProjectHours>();
			}	
		}
		
		
		///<summary>
		/// Fill an VList&lt;KpiEngineeringProjectHours&gt; From a DataTable
		///</summary>
		/// <param name="dataTable">the DataTable that hold the data.</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList<KpiEngineeringProjectHours>"/></returns>
		protected static VList&lt;KpiEngineeringProjectHours&gt; Fill(DataTable dataTable, VList<KpiEngineeringProjectHours> rows, int start, int pagelen)
		{
			int recordnum = 0;
			
			System.Collections.IEnumerator dataRows =  dataTable.Rows.GetEnumerator();
			
			while (dataRows.MoveNext() && (pagelen != 0))
			{
				if(recordnum >= start)
				{
					DataRow row = (DataRow)dataRows.Current;
				
					KpiEngineeringProjectHours c = new KpiEngineeringProjectHours();
					c.CompanyName = (Convert.IsDBNull(row["CompanyName"]))?string.Empty:(System.String)row["CompanyName"];
					c.SiteName = (Convert.IsDBNull(row["SiteName"]))?string.Empty:(System.String)row["SiteName"];
					c.CompanySiteCategoryId = (Convert.IsDBNull(row["CompanySiteCategoryId"]))?(int)0:(System.Int32?)row["CompanySiteCategoryId"];
					c.CompanySiteCategoryDesc = (Convert.IsDBNull(row["CompanySiteCategoryDesc"]))?string.Empty:(System.String)row["CompanySiteCategoryDesc"];
					c.KpiDateTimeMonth = (Convert.IsDBNull(row["kpiDateTimeMonth"]))?(int)0:(System.Int32?)row["kpiDateTimeMonth"];
					c.KpiDateTimeYear = (Convert.IsDBNull(row["kpiDateTimeYear"]))?(int)0:(System.Int32?)row["kpiDateTimeYear"];
					c.KpiDateTime = (Convert.IsDBNull(row["kpiDateTime"]))?DateTime.MinValue:(System.DateTime)row["kpiDateTime"];
					c.ProjectCapital1Title = (Convert.IsDBNull(row["projectCapital1Title"]))?string.Empty:(System.String)row["projectCapital1Title"];
					c.ProjectCapital1Hours = (Convert.IsDBNull(row["projectCapital1Hours"]))?0.0m:(System.Decimal?)row["projectCapital1Hours"];
					c.ProjectCapital2Title = (Convert.IsDBNull(row["projectCapital2Title"]))?string.Empty:(System.String)row["projectCapital2Title"];
					c.ProjectCapital2Hours = (Convert.IsDBNull(row["projectCapital2Hours"]))?0.0m:(System.Decimal?)row["projectCapital2Hours"];
					c.ProjectCapital3Title = (Convert.IsDBNull(row["projectCapital3Title"]))?string.Empty:(System.String)row["projectCapital3Title"];
					c.ProjectCapital3Hours = (Convert.IsDBNull(row["projectCapital3Hours"]))?0.0m:(System.Decimal?)row["projectCapital3Hours"];
					c.ProjectCapital4Title = (Convert.IsDBNull(row["projectCapital4Title"]))?string.Empty:(System.String)row["projectCapital4Title"];
					c.ProjectCapital4Hours = (Convert.IsDBNull(row["projectCapital4Hours"]))?0.0m:(System.Decimal?)row["projectCapital4Hours"];
					c.ProjectCapital5Title = (Convert.IsDBNull(row["projectCapital5Title"]))?string.Empty:(System.String)row["projectCapital5Title"];
					c.ProjectCapital5Hours = (Convert.IsDBNull(row["projectCapital5Hours"]))?0.0m:(System.Decimal?)row["projectCapital5Hours"];
					c.ProjectCapital6Title = (Convert.IsDBNull(row["projectCapital6Title"]))?string.Empty:(System.String)row["projectCapital6Title"];
					c.ProjectCapital6Hours = (Convert.IsDBNull(row["projectCapital6Hours"]))?0.0m:(System.Decimal?)row["projectCapital6Hours"];
					c.ProjectCapital7Title = (Convert.IsDBNull(row["projectCapital7Title"]))?string.Empty:(System.String)row["projectCapital7Title"];
					c.ProjectCapital7Hours = (Convert.IsDBNull(row["projectCapital7Hours"]))?0.0m:(System.Decimal?)row["projectCapital7Hours"];
					c.ProjectCapital8Title = (Convert.IsDBNull(row["projectCapital8Title"]))?string.Empty:(System.String)row["projectCapital8Title"];
					c.ProjectCapital8Hours = (Convert.IsDBNull(row["projectCapital8Hours"]))?0.0m:(System.Decimal?)row["projectCapital8Hours"];
					c.ProjectCapital9Title = (Convert.IsDBNull(row["projectCapital9Title"]))?string.Empty:(System.String)row["projectCapital9Title"];
					c.ProjectCapital9Hours = (Convert.IsDBNull(row["projectCapital9Hours"]))?0.0m:(System.Decimal?)row["projectCapital9Hours"];
					c.ProjectCapital10Title = (Convert.IsDBNull(row["projectCapital10Title"]))?string.Empty:(System.String)row["projectCapital10Title"];
					c.ProjectCapital10Hours = (Convert.IsDBNull(row["projectCapital10Hours"]))?0.0m:(System.Decimal?)row["projectCapital10Hours"];
					c.ProjectCapital11Title = (Convert.IsDBNull(row["projectCapital11Title"]))?string.Empty:(System.String)row["projectCapital11Title"];
					c.ProjectCapital11Hours = (Convert.IsDBNull(row["projectCapital11Hours"]))?0.0m:(System.Decimal?)row["projectCapital11Hours"];
					c.ProjectCapital12Title = (Convert.IsDBNull(row["projectCapital12Title"]))?string.Empty:(System.String)row["projectCapital12Title"];
					c.ProjectCapital12Hours = (Convert.IsDBNull(row["projectCapital12Hours"]))?0.0m:(System.Decimal?)row["projectCapital12Hours"];
					c.ProjectCapital13Title = (Convert.IsDBNull(row["projectCapital13Title"]))?string.Empty:(System.String)row["projectCapital13Title"];
					c.ProjectCapital13Hours = (Convert.IsDBNull(row["projectCapital13Hours"]))?0.0m:(System.Decimal?)row["projectCapital13Hours"];
					c.ProjectCapital14Title = (Convert.IsDBNull(row["projectCapital14Title"]))?string.Empty:(System.String)row["projectCapital14Title"];
					c.ProjectCapital14Hours = (Convert.IsDBNull(row["projectCapital14Hours"]))?0.0m:(System.Decimal?)row["projectCapital14Hours"];
					c.ProjectCapital15Title = (Convert.IsDBNull(row["projectCapital15Title"]))?string.Empty:(System.String)row["projectCapital15Title"];
					c.ProjectCapital15Hours = (Convert.IsDBNull(row["projectCapital15Hours"]))?0.0m:(System.Decimal?)row["projectCapital15Hours"];
					c.AcceptChanges();
					rows.Add(c);
					pagelen -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		*/	
						
		///<summary>
		/// Fill an <see cref="VList&lt;KpiEngineeringProjectHours&gt;"/> From a DataReader.
		///</summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pageLength">number of row.</param>
		///<returns>a <see cref="VList&lt;KpiEngineeringProjectHours&gt;"/></returns>
		protected VList<KpiEngineeringProjectHours> Fill(IDataReader reader, VList<KpiEngineeringProjectHours> rows, int start, int pageLength)
		{
			int recordnum = 0;
			while (reader.Read() && (pageLength != 0))
			{
				if(recordnum >= start)
				{
					KpiEngineeringProjectHours entity = null;
					if (DataRepository.Provider.UseEntityFactory)
					{
						entity = EntityManager.CreateViewEntity<KpiEngineeringProjectHours>("KpiEngineeringProjectHours",  DataRepository.Provider.EntityCreationalFactoryType); 
					}
					else
					{
						entity = new KpiEngineeringProjectHours();
					}
					
					entity.SuppressEntityEvents = true;

					entity.CompanyName = (System.String)reader[((int)KpiEngineeringProjectHoursColumn.CompanyName)];
					//entity.CompanyName = (Convert.IsDBNull(reader["CompanyName"]))?string.Empty:(System.String)reader["CompanyName"];
					entity.SiteName = (System.String)reader[((int)KpiEngineeringProjectHoursColumn.SiteName)];
					//entity.SiteName = (Convert.IsDBNull(reader["SiteName"]))?string.Empty:(System.String)reader["SiteName"];
					entity.CompanySiteCategoryId = (reader.IsDBNull(((int)KpiEngineeringProjectHoursColumn.CompanySiteCategoryId)))?null:(System.Int32?)reader[((int)KpiEngineeringProjectHoursColumn.CompanySiteCategoryId)];
					//entity.CompanySiteCategoryId = (Convert.IsDBNull(reader["CompanySiteCategoryId"]))?(int)0:(System.Int32?)reader["CompanySiteCategoryId"];
					entity.CompanySiteCategoryDesc = (System.String)reader[((int)KpiEngineeringProjectHoursColumn.CompanySiteCategoryDesc)];
					//entity.CompanySiteCategoryDesc = (Convert.IsDBNull(reader["CompanySiteCategoryDesc"]))?string.Empty:(System.String)reader["CompanySiteCategoryDesc"];
					entity.KpiDateTimeMonth = (reader.IsDBNull(((int)KpiEngineeringProjectHoursColumn.KpiDateTimeMonth)))?null:(System.Int32?)reader[((int)KpiEngineeringProjectHoursColumn.KpiDateTimeMonth)];
					//entity.KpiDateTimeMonth = (Convert.IsDBNull(reader["kpiDateTimeMonth"]))?(int)0:(System.Int32?)reader["kpiDateTimeMonth"];
					entity.KpiDateTimeYear = (reader.IsDBNull(((int)KpiEngineeringProjectHoursColumn.KpiDateTimeYear)))?null:(System.Int32?)reader[((int)KpiEngineeringProjectHoursColumn.KpiDateTimeYear)];
					//entity.KpiDateTimeYear = (Convert.IsDBNull(reader["kpiDateTimeYear"]))?(int)0:(System.Int32?)reader["kpiDateTimeYear"];
					entity.KpiDateTime = (System.DateTime)reader[((int)KpiEngineeringProjectHoursColumn.KpiDateTime)];
					//entity.KpiDateTime = (Convert.IsDBNull(reader["kpiDateTime"]))?DateTime.MinValue:(System.DateTime)reader["kpiDateTime"];
					entity.ProjectCapital1Title = (reader.IsDBNull(((int)KpiEngineeringProjectHoursColumn.ProjectCapital1Title)))?null:(System.String)reader[((int)KpiEngineeringProjectHoursColumn.ProjectCapital1Title)];
					//entity.ProjectCapital1Title = (Convert.IsDBNull(reader["projectCapital1Title"]))?string.Empty:(System.String)reader["projectCapital1Title"];
					entity.ProjectCapital1Hours = (reader.IsDBNull(((int)KpiEngineeringProjectHoursColumn.ProjectCapital1Hours)))?null:(System.Decimal?)reader[((int)KpiEngineeringProjectHoursColumn.ProjectCapital1Hours)];
					//entity.ProjectCapital1Hours = (Convert.IsDBNull(reader["projectCapital1Hours"]))?0.0m:(System.Decimal?)reader["projectCapital1Hours"];
					entity.ProjectCapital2Title = (reader.IsDBNull(((int)KpiEngineeringProjectHoursColumn.ProjectCapital2Title)))?null:(System.String)reader[((int)KpiEngineeringProjectHoursColumn.ProjectCapital2Title)];
					//entity.ProjectCapital2Title = (Convert.IsDBNull(reader["projectCapital2Title"]))?string.Empty:(System.String)reader["projectCapital2Title"];
					entity.ProjectCapital2Hours = (reader.IsDBNull(((int)KpiEngineeringProjectHoursColumn.ProjectCapital2Hours)))?null:(System.Decimal?)reader[((int)KpiEngineeringProjectHoursColumn.ProjectCapital2Hours)];
					//entity.ProjectCapital2Hours = (Convert.IsDBNull(reader["projectCapital2Hours"]))?0.0m:(System.Decimal?)reader["projectCapital2Hours"];
					entity.ProjectCapital3Title = (reader.IsDBNull(((int)KpiEngineeringProjectHoursColumn.ProjectCapital3Title)))?null:(System.String)reader[((int)KpiEngineeringProjectHoursColumn.ProjectCapital3Title)];
					//entity.ProjectCapital3Title = (Convert.IsDBNull(reader["projectCapital3Title"]))?string.Empty:(System.String)reader["projectCapital3Title"];
					entity.ProjectCapital3Hours = (reader.IsDBNull(((int)KpiEngineeringProjectHoursColumn.ProjectCapital3Hours)))?null:(System.Decimal?)reader[((int)KpiEngineeringProjectHoursColumn.ProjectCapital3Hours)];
					//entity.ProjectCapital3Hours = (Convert.IsDBNull(reader["projectCapital3Hours"]))?0.0m:(System.Decimal?)reader["projectCapital3Hours"];
					entity.ProjectCapital4Title = (reader.IsDBNull(((int)KpiEngineeringProjectHoursColumn.ProjectCapital4Title)))?null:(System.String)reader[((int)KpiEngineeringProjectHoursColumn.ProjectCapital4Title)];
					//entity.ProjectCapital4Title = (Convert.IsDBNull(reader["projectCapital4Title"]))?string.Empty:(System.String)reader["projectCapital4Title"];
					entity.ProjectCapital4Hours = (reader.IsDBNull(((int)KpiEngineeringProjectHoursColumn.ProjectCapital4Hours)))?null:(System.Decimal?)reader[((int)KpiEngineeringProjectHoursColumn.ProjectCapital4Hours)];
					//entity.ProjectCapital4Hours = (Convert.IsDBNull(reader["projectCapital4Hours"]))?0.0m:(System.Decimal?)reader["projectCapital4Hours"];
					entity.ProjectCapital5Title = (reader.IsDBNull(((int)KpiEngineeringProjectHoursColumn.ProjectCapital5Title)))?null:(System.String)reader[((int)KpiEngineeringProjectHoursColumn.ProjectCapital5Title)];
					//entity.ProjectCapital5Title = (Convert.IsDBNull(reader["projectCapital5Title"]))?string.Empty:(System.String)reader["projectCapital5Title"];
					entity.ProjectCapital5Hours = (reader.IsDBNull(((int)KpiEngineeringProjectHoursColumn.ProjectCapital5Hours)))?null:(System.Decimal?)reader[((int)KpiEngineeringProjectHoursColumn.ProjectCapital5Hours)];
					//entity.ProjectCapital5Hours = (Convert.IsDBNull(reader["projectCapital5Hours"]))?0.0m:(System.Decimal?)reader["projectCapital5Hours"];
					entity.ProjectCapital6Title = (reader.IsDBNull(((int)KpiEngineeringProjectHoursColumn.ProjectCapital6Title)))?null:(System.String)reader[((int)KpiEngineeringProjectHoursColumn.ProjectCapital6Title)];
					//entity.ProjectCapital6Title = (Convert.IsDBNull(reader["projectCapital6Title"]))?string.Empty:(System.String)reader["projectCapital6Title"];
					entity.ProjectCapital6Hours = (reader.IsDBNull(((int)KpiEngineeringProjectHoursColumn.ProjectCapital6Hours)))?null:(System.Decimal?)reader[((int)KpiEngineeringProjectHoursColumn.ProjectCapital6Hours)];
					//entity.ProjectCapital6Hours = (Convert.IsDBNull(reader["projectCapital6Hours"]))?0.0m:(System.Decimal?)reader["projectCapital6Hours"];
					entity.ProjectCapital7Title = (reader.IsDBNull(((int)KpiEngineeringProjectHoursColumn.ProjectCapital7Title)))?null:(System.String)reader[((int)KpiEngineeringProjectHoursColumn.ProjectCapital7Title)];
					//entity.ProjectCapital7Title = (Convert.IsDBNull(reader["projectCapital7Title"]))?string.Empty:(System.String)reader["projectCapital7Title"];
					entity.ProjectCapital7Hours = (reader.IsDBNull(((int)KpiEngineeringProjectHoursColumn.ProjectCapital7Hours)))?null:(System.Decimal?)reader[((int)KpiEngineeringProjectHoursColumn.ProjectCapital7Hours)];
					//entity.ProjectCapital7Hours = (Convert.IsDBNull(reader["projectCapital7Hours"]))?0.0m:(System.Decimal?)reader["projectCapital7Hours"];
					entity.ProjectCapital8Title = (reader.IsDBNull(((int)KpiEngineeringProjectHoursColumn.ProjectCapital8Title)))?null:(System.String)reader[((int)KpiEngineeringProjectHoursColumn.ProjectCapital8Title)];
					//entity.ProjectCapital8Title = (Convert.IsDBNull(reader["projectCapital8Title"]))?string.Empty:(System.String)reader["projectCapital8Title"];
					entity.ProjectCapital8Hours = (reader.IsDBNull(((int)KpiEngineeringProjectHoursColumn.ProjectCapital8Hours)))?null:(System.Decimal?)reader[((int)KpiEngineeringProjectHoursColumn.ProjectCapital8Hours)];
					//entity.ProjectCapital8Hours = (Convert.IsDBNull(reader["projectCapital8Hours"]))?0.0m:(System.Decimal?)reader["projectCapital8Hours"];
					entity.ProjectCapital9Title = (reader.IsDBNull(((int)KpiEngineeringProjectHoursColumn.ProjectCapital9Title)))?null:(System.String)reader[((int)KpiEngineeringProjectHoursColumn.ProjectCapital9Title)];
					//entity.ProjectCapital9Title = (Convert.IsDBNull(reader["projectCapital9Title"]))?string.Empty:(System.String)reader["projectCapital9Title"];
					entity.ProjectCapital9Hours = (reader.IsDBNull(((int)KpiEngineeringProjectHoursColumn.ProjectCapital9Hours)))?null:(System.Decimal?)reader[((int)KpiEngineeringProjectHoursColumn.ProjectCapital9Hours)];
					//entity.ProjectCapital9Hours = (Convert.IsDBNull(reader["projectCapital9Hours"]))?0.0m:(System.Decimal?)reader["projectCapital9Hours"];
					entity.ProjectCapital10Title = (reader.IsDBNull(((int)KpiEngineeringProjectHoursColumn.ProjectCapital10Title)))?null:(System.String)reader[((int)KpiEngineeringProjectHoursColumn.ProjectCapital10Title)];
					//entity.ProjectCapital10Title = (Convert.IsDBNull(reader["projectCapital10Title"]))?string.Empty:(System.String)reader["projectCapital10Title"];
					entity.ProjectCapital10Hours = (reader.IsDBNull(((int)KpiEngineeringProjectHoursColumn.ProjectCapital10Hours)))?null:(System.Decimal?)reader[((int)KpiEngineeringProjectHoursColumn.ProjectCapital10Hours)];
					//entity.ProjectCapital10Hours = (Convert.IsDBNull(reader["projectCapital10Hours"]))?0.0m:(System.Decimal?)reader["projectCapital10Hours"];
					entity.ProjectCapital11Title = (reader.IsDBNull(((int)KpiEngineeringProjectHoursColumn.ProjectCapital11Title)))?null:(System.String)reader[((int)KpiEngineeringProjectHoursColumn.ProjectCapital11Title)];
					//entity.ProjectCapital11Title = (Convert.IsDBNull(reader["projectCapital11Title"]))?string.Empty:(System.String)reader["projectCapital11Title"];
					entity.ProjectCapital11Hours = (reader.IsDBNull(((int)KpiEngineeringProjectHoursColumn.ProjectCapital11Hours)))?null:(System.Decimal?)reader[((int)KpiEngineeringProjectHoursColumn.ProjectCapital11Hours)];
					//entity.ProjectCapital11Hours = (Convert.IsDBNull(reader["projectCapital11Hours"]))?0.0m:(System.Decimal?)reader["projectCapital11Hours"];
					entity.ProjectCapital12Title = (reader.IsDBNull(((int)KpiEngineeringProjectHoursColumn.ProjectCapital12Title)))?null:(System.String)reader[((int)KpiEngineeringProjectHoursColumn.ProjectCapital12Title)];
					//entity.ProjectCapital12Title = (Convert.IsDBNull(reader["projectCapital12Title"]))?string.Empty:(System.String)reader["projectCapital12Title"];
					entity.ProjectCapital12Hours = (reader.IsDBNull(((int)KpiEngineeringProjectHoursColumn.ProjectCapital12Hours)))?null:(System.Decimal?)reader[((int)KpiEngineeringProjectHoursColumn.ProjectCapital12Hours)];
					//entity.ProjectCapital12Hours = (Convert.IsDBNull(reader["projectCapital12Hours"]))?0.0m:(System.Decimal?)reader["projectCapital12Hours"];
					entity.ProjectCapital13Title = (reader.IsDBNull(((int)KpiEngineeringProjectHoursColumn.ProjectCapital13Title)))?null:(System.String)reader[((int)KpiEngineeringProjectHoursColumn.ProjectCapital13Title)];
					//entity.ProjectCapital13Title = (Convert.IsDBNull(reader["projectCapital13Title"]))?string.Empty:(System.String)reader["projectCapital13Title"];
					entity.ProjectCapital13Hours = (reader.IsDBNull(((int)KpiEngineeringProjectHoursColumn.ProjectCapital13Hours)))?null:(System.Decimal?)reader[((int)KpiEngineeringProjectHoursColumn.ProjectCapital13Hours)];
					//entity.ProjectCapital13Hours = (Convert.IsDBNull(reader["projectCapital13Hours"]))?0.0m:(System.Decimal?)reader["projectCapital13Hours"];
					entity.ProjectCapital14Title = (reader.IsDBNull(((int)KpiEngineeringProjectHoursColumn.ProjectCapital14Title)))?null:(System.String)reader[((int)KpiEngineeringProjectHoursColumn.ProjectCapital14Title)];
					//entity.ProjectCapital14Title = (Convert.IsDBNull(reader["projectCapital14Title"]))?string.Empty:(System.String)reader["projectCapital14Title"];
					entity.ProjectCapital14Hours = (reader.IsDBNull(((int)KpiEngineeringProjectHoursColumn.ProjectCapital14Hours)))?null:(System.Decimal?)reader[((int)KpiEngineeringProjectHoursColumn.ProjectCapital14Hours)];
					//entity.ProjectCapital14Hours = (Convert.IsDBNull(reader["projectCapital14Hours"]))?0.0m:(System.Decimal?)reader["projectCapital14Hours"];
					entity.ProjectCapital15Title = (reader.IsDBNull(((int)KpiEngineeringProjectHoursColumn.ProjectCapital15Title)))?null:(System.String)reader[((int)KpiEngineeringProjectHoursColumn.ProjectCapital15Title)];
					//entity.ProjectCapital15Title = (Convert.IsDBNull(reader["projectCapital15Title"]))?string.Empty:(System.String)reader["projectCapital15Title"];
					entity.ProjectCapital15Hours = (reader.IsDBNull(((int)KpiEngineeringProjectHoursColumn.ProjectCapital15Hours)))?null:(System.Decimal?)reader[((int)KpiEngineeringProjectHoursColumn.ProjectCapital15Hours)];
					//entity.ProjectCapital15Hours = (Convert.IsDBNull(reader["projectCapital15Hours"]))?0.0m:(System.Decimal?)reader["projectCapital15Hours"];
					entity.AcceptChanges();
					entity.SuppressEntityEvents = false;
					
					rows.Add(entity);
					pageLength -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		
		
		/// <summary>
		/// Refreshes the <see cref="KpiEngineeringProjectHours"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KpiEngineeringProjectHours"/> object to refresh.</param>
		protected void RefreshEntity(IDataReader reader, KpiEngineeringProjectHours entity)
		{
			reader.Read();
			entity.CompanyName = (System.String)reader[((int)KpiEngineeringProjectHoursColumn.CompanyName)];
			//entity.CompanyName = (Convert.IsDBNull(reader["CompanyName"]))?string.Empty:(System.String)reader["CompanyName"];
			entity.SiteName = (System.String)reader[((int)KpiEngineeringProjectHoursColumn.SiteName)];
			//entity.SiteName = (Convert.IsDBNull(reader["SiteName"]))?string.Empty:(System.String)reader["SiteName"];
			entity.CompanySiteCategoryId = (reader.IsDBNull(((int)KpiEngineeringProjectHoursColumn.CompanySiteCategoryId)))?null:(System.Int32?)reader[((int)KpiEngineeringProjectHoursColumn.CompanySiteCategoryId)];
			//entity.CompanySiteCategoryId = (Convert.IsDBNull(reader["CompanySiteCategoryId"]))?(int)0:(System.Int32?)reader["CompanySiteCategoryId"];
			entity.CompanySiteCategoryDesc = (System.String)reader[((int)KpiEngineeringProjectHoursColumn.CompanySiteCategoryDesc)];
			//entity.CompanySiteCategoryDesc = (Convert.IsDBNull(reader["CompanySiteCategoryDesc"]))?string.Empty:(System.String)reader["CompanySiteCategoryDesc"];
			entity.KpiDateTimeMonth = (reader.IsDBNull(((int)KpiEngineeringProjectHoursColumn.KpiDateTimeMonth)))?null:(System.Int32?)reader[((int)KpiEngineeringProjectHoursColumn.KpiDateTimeMonth)];
			//entity.KpiDateTimeMonth = (Convert.IsDBNull(reader["kpiDateTimeMonth"]))?(int)0:(System.Int32?)reader["kpiDateTimeMonth"];
			entity.KpiDateTimeYear = (reader.IsDBNull(((int)KpiEngineeringProjectHoursColumn.KpiDateTimeYear)))?null:(System.Int32?)reader[((int)KpiEngineeringProjectHoursColumn.KpiDateTimeYear)];
			//entity.KpiDateTimeYear = (Convert.IsDBNull(reader["kpiDateTimeYear"]))?(int)0:(System.Int32?)reader["kpiDateTimeYear"];
			entity.KpiDateTime = (System.DateTime)reader[((int)KpiEngineeringProjectHoursColumn.KpiDateTime)];
			//entity.KpiDateTime = (Convert.IsDBNull(reader["kpiDateTime"]))?DateTime.MinValue:(System.DateTime)reader["kpiDateTime"];
			entity.ProjectCapital1Title = (reader.IsDBNull(((int)KpiEngineeringProjectHoursColumn.ProjectCapital1Title)))?null:(System.String)reader[((int)KpiEngineeringProjectHoursColumn.ProjectCapital1Title)];
			//entity.ProjectCapital1Title = (Convert.IsDBNull(reader["projectCapital1Title"]))?string.Empty:(System.String)reader["projectCapital1Title"];
			entity.ProjectCapital1Hours = (reader.IsDBNull(((int)KpiEngineeringProjectHoursColumn.ProjectCapital1Hours)))?null:(System.Decimal?)reader[((int)KpiEngineeringProjectHoursColumn.ProjectCapital1Hours)];
			//entity.ProjectCapital1Hours = (Convert.IsDBNull(reader["projectCapital1Hours"]))?0.0m:(System.Decimal?)reader["projectCapital1Hours"];
			entity.ProjectCapital2Title = (reader.IsDBNull(((int)KpiEngineeringProjectHoursColumn.ProjectCapital2Title)))?null:(System.String)reader[((int)KpiEngineeringProjectHoursColumn.ProjectCapital2Title)];
			//entity.ProjectCapital2Title = (Convert.IsDBNull(reader["projectCapital2Title"]))?string.Empty:(System.String)reader["projectCapital2Title"];
			entity.ProjectCapital2Hours = (reader.IsDBNull(((int)KpiEngineeringProjectHoursColumn.ProjectCapital2Hours)))?null:(System.Decimal?)reader[((int)KpiEngineeringProjectHoursColumn.ProjectCapital2Hours)];
			//entity.ProjectCapital2Hours = (Convert.IsDBNull(reader["projectCapital2Hours"]))?0.0m:(System.Decimal?)reader["projectCapital2Hours"];
			entity.ProjectCapital3Title = (reader.IsDBNull(((int)KpiEngineeringProjectHoursColumn.ProjectCapital3Title)))?null:(System.String)reader[((int)KpiEngineeringProjectHoursColumn.ProjectCapital3Title)];
			//entity.ProjectCapital3Title = (Convert.IsDBNull(reader["projectCapital3Title"]))?string.Empty:(System.String)reader["projectCapital3Title"];
			entity.ProjectCapital3Hours = (reader.IsDBNull(((int)KpiEngineeringProjectHoursColumn.ProjectCapital3Hours)))?null:(System.Decimal?)reader[((int)KpiEngineeringProjectHoursColumn.ProjectCapital3Hours)];
			//entity.ProjectCapital3Hours = (Convert.IsDBNull(reader["projectCapital3Hours"]))?0.0m:(System.Decimal?)reader["projectCapital3Hours"];
			entity.ProjectCapital4Title = (reader.IsDBNull(((int)KpiEngineeringProjectHoursColumn.ProjectCapital4Title)))?null:(System.String)reader[((int)KpiEngineeringProjectHoursColumn.ProjectCapital4Title)];
			//entity.ProjectCapital4Title = (Convert.IsDBNull(reader["projectCapital4Title"]))?string.Empty:(System.String)reader["projectCapital4Title"];
			entity.ProjectCapital4Hours = (reader.IsDBNull(((int)KpiEngineeringProjectHoursColumn.ProjectCapital4Hours)))?null:(System.Decimal?)reader[((int)KpiEngineeringProjectHoursColumn.ProjectCapital4Hours)];
			//entity.ProjectCapital4Hours = (Convert.IsDBNull(reader["projectCapital4Hours"]))?0.0m:(System.Decimal?)reader["projectCapital4Hours"];
			entity.ProjectCapital5Title = (reader.IsDBNull(((int)KpiEngineeringProjectHoursColumn.ProjectCapital5Title)))?null:(System.String)reader[((int)KpiEngineeringProjectHoursColumn.ProjectCapital5Title)];
			//entity.ProjectCapital5Title = (Convert.IsDBNull(reader["projectCapital5Title"]))?string.Empty:(System.String)reader["projectCapital5Title"];
			entity.ProjectCapital5Hours = (reader.IsDBNull(((int)KpiEngineeringProjectHoursColumn.ProjectCapital5Hours)))?null:(System.Decimal?)reader[((int)KpiEngineeringProjectHoursColumn.ProjectCapital5Hours)];
			//entity.ProjectCapital5Hours = (Convert.IsDBNull(reader["projectCapital5Hours"]))?0.0m:(System.Decimal?)reader["projectCapital5Hours"];
			entity.ProjectCapital6Title = (reader.IsDBNull(((int)KpiEngineeringProjectHoursColumn.ProjectCapital6Title)))?null:(System.String)reader[((int)KpiEngineeringProjectHoursColumn.ProjectCapital6Title)];
			//entity.ProjectCapital6Title = (Convert.IsDBNull(reader["projectCapital6Title"]))?string.Empty:(System.String)reader["projectCapital6Title"];
			entity.ProjectCapital6Hours = (reader.IsDBNull(((int)KpiEngineeringProjectHoursColumn.ProjectCapital6Hours)))?null:(System.Decimal?)reader[((int)KpiEngineeringProjectHoursColumn.ProjectCapital6Hours)];
			//entity.ProjectCapital6Hours = (Convert.IsDBNull(reader["projectCapital6Hours"]))?0.0m:(System.Decimal?)reader["projectCapital6Hours"];
			entity.ProjectCapital7Title = (reader.IsDBNull(((int)KpiEngineeringProjectHoursColumn.ProjectCapital7Title)))?null:(System.String)reader[((int)KpiEngineeringProjectHoursColumn.ProjectCapital7Title)];
			//entity.ProjectCapital7Title = (Convert.IsDBNull(reader["projectCapital7Title"]))?string.Empty:(System.String)reader["projectCapital7Title"];
			entity.ProjectCapital7Hours = (reader.IsDBNull(((int)KpiEngineeringProjectHoursColumn.ProjectCapital7Hours)))?null:(System.Decimal?)reader[((int)KpiEngineeringProjectHoursColumn.ProjectCapital7Hours)];
			//entity.ProjectCapital7Hours = (Convert.IsDBNull(reader["projectCapital7Hours"]))?0.0m:(System.Decimal?)reader["projectCapital7Hours"];
			entity.ProjectCapital8Title = (reader.IsDBNull(((int)KpiEngineeringProjectHoursColumn.ProjectCapital8Title)))?null:(System.String)reader[((int)KpiEngineeringProjectHoursColumn.ProjectCapital8Title)];
			//entity.ProjectCapital8Title = (Convert.IsDBNull(reader["projectCapital8Title"]))?string.Empty:(System.String)reader["projectCapital8Title"];
			entity.ProjectCapital8Hours = (reader.IsDBNull(((int)KpiEngineeringProjectHoursColumn.ProjectCapital8Hours)))?null:(System.Decimal?)reader[((int)KpiEngineeringProjectHoursColumn.ProjectCapital8Hours)];
			//entity.ProjectCapital8Hours = (Convert.IsDBNull(reader["projectCapital8Hours"]))?0.0m:(System.Decimal?)reader["projectCapital8Hours"];
			entity.ProjectCapital9Title = (reader.IsDBNull(((int)KpiEngineeringProjectHoursColumn.ProjectCapital9Title)))?null:(System.String)reader[((int)KpiEngineeringProjectHoursColumn.ProjectCapital9Title)];
			//entity.ProjectCapital9Title = (Convert.IsDBNull(reader["projectCapital9Title"]))?string.Empty:(System.String)reader["projectCapital9Title"];
			entity.ProjectCapital9Hours = (reader.IsDBNull(((int)KpiEngineeringProjectHoursColumn.ProjectCapital9Hours)))?null:(System.Decimal?)reader[((int)KpiEngineeringProjectHoursColumn.ProjectCapital9Hours)];
			//entity.ProjectCapital9Hours = (Convert.IsDBNull(reader["projectCapital9Hours"]))?0.0m:(System.Decimal?)reader["projectCapital9Hours"];
			entity.ProjectCapital10Title = (reader.IsDBNull(((int)KpiEngineeringProjectHoursColumn.ProjectCapital10Title)))?null:(System.String)reader[((int)KpiEngineeringProjectHoursColumn.ProjectCapital10Title)];
			//entity.ProjectCapital10Title = (Convert.IsDBNull(reader["projectCapital10Title"]))?string.Empty:(System.String)reader["projectCapital10Title"];
			entity.ProjectCapital10Hours = (reader.IsDBNull(((int)KpiEngineeringProjectHoursColumn.ProjectCapital10Hours)))?null:(System.Decimal?)reader[((int)KpiEngineeringProjectHoursColumn.ProjectCapital10Hours)];
			//entity.ProjectCapital10Hours = (Convert.IsDBNull(reader["projectCapital10Hours"]))?0.0m:(System.Decimal?)reader["projectCapital10Hours"];
			entity.ProjectCapital11Title = (reader.IsDBNull(((int)KpiEngineeringProjectHoursColumn.ProjectCapital11Title)))?null:(System.String)reader[((int)KpiEngineeringProjectHoursColumn.ProjectCapital11Title)];
			//entity.ProjectCapital11Title = (Convert.IsDBNull(reader["projectCapital11Title"]))?string.Empty:(System.String)reader["projectCapital11Title"];
			entity.ProjectCapital11Hours = (reader.IsDBNull(((int)KpiEngineeringProjectHoursColumn.ProjectCapital11Hours)))?null:(System.Decimal?)reader[((int)KpiEngineeringProjectHoursColumn.ProjectCapital11Hours)];
			//entity.ProjectCapital11Hours = (Convert.IsDBNull(reader["projectCapital11Hours"]))?0.0m:(System.Decimal?)reader["projectCapital11Hours"];
			entity.ProjectCapital12Title = (reader.IsDBNull(((int)KpiEngineeringProjectHoursColumn.ProjectCapital12Title)))?null:(System.String)reader[((int)KpiEngineeringProjectHoursColumn.ProjectCapital12Title)];
			//entity.ProjectCapital12Title = (Convert.IsDBNull(reader["projectCapital12Title"]))?string.Empty:(System.String)reader["projectCapital12Title"];
			entity.ProjectCapital12Hours = (reader.IsDBNull(((int)KpiEngineeringProjectHoursColumn.ProjectCapital12Hours)))?null:(System.Decimal?)reader[((int)KpiEngineeringProjectHoursColumn.ProjectCapital12Hours)];
			//entity.ProjectCapital12Hours = (Convert.IsDBNull(reader["projectCapital12Hours"]))?0.0m:(System.Decimal?)reader["projectCapital12Hours"];
			entity.ProjectCapital13Title = (reader.IsDBNull(((int)KpiEngineeringProjectHoursColumn.ProjectCapital13Title)))?null:(System.String)reader[((int)KpiEngineeringProjectHoursColumn.ProjectCapital13Title)];
			//entity.ProjectCapital13Title = (Convert.IsDBNull(reader["projectCapital13Title"]))?string.Empty:(System.String)reader["projectCapital13Title"];
			entity.ProjectCapital13Hours = (reader.IsDBNull(((int)KpiEngineeringProjectHoursColumn.ProjectCapital13Hours)))?null:(System.Decimal?)reader[((int)KpiEngineeringProjectHoursColumn.ProjectCapital13Hours)];
			//entity.ProjectCapital13Hours = (Convert.IsDBNull(reader["projectCapital13Hours"]))?0.0m:(System.Decimal?)reader["projectCapital13Hours"];
			entity.ProjectCapital14Title = (reader.IsDBNull(((int)KpiEngineeringProjectHoursColumn.ProjectCapital14Title)))?null:(System.String)reader[((int)KpiEngineeringProjectHoursColumn.ProjectCapital14Title)];
			//entity.ProjectCapital14Title = (Convert.IsDBNull(reader["projectCapital14Title"]))?string.Empty:(System.String)reader["projectCapital14Title"];
			entity.ProjectCapital14Hours = (reader.IsDBNull(((int)KpiEngineeringProjectHoursColumn.ProjectCapital14Hours)))?null:(System.Decimal?)reader[((int)KpiEngineeringProjectHoursColumn.ProjectCapital14Hours)];
			//entity.ProjectCapital14Hours = (Convert.IsDBNull(reader["projectCapital14Hours"]))?0.0m:(System.Decimal?)reader["projectCapital14Hours"];
			entity.ProjectCapital15Title = (reader.IsDBNull(((int)KpiEngineeringProjectHoursColumn.ProjectCapital15Title)))?null:(System.String)reader[((int)KpiEngineeringProjectHoursColumn.ProjectCapital15Title)];
			//entity.ProjectCapital15Title = (Convert.IsDBNull(reader["projectCapital15Title"]))?string.Empty:(System.String)reader["projectCapital15Title"];
			entity.ProjectCapital15Hours = (reader.IsDBNull(((int)KpiEngineeringProjectHoursColumn.ProjectCapital15Hours)))?null:(System.Decimal?)reader[((int)KpiEngineeringProjectHoursColumn.ProjectCapital15Hours)];
			//entity.ProjectCapital15Hours = (Convert.IsDBNull(reader["projectCapital15Hours"]))?0.0m:(System.Decimal?)reader["projectCapital15Hours"];
			reader.Close();
	
			entity.AcceptChanges();
		}
		
		/*
		/// <summary>
		/// Refreshes the <see cref="KpiEngineeringProjectHours"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KpiEngineeringProjectHours"/> object.</param>
		protected static void RefreshEntity(DataSet dataSet, KpiEngineeringProjectHours entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.CompanyName = (Convert.IsDBNull(dataRow["CompanyName"]))?string.Empty:(System.String)dataRow["CompanyName"];
			entity.SiteName = (Convert.IsDBNull(dataRow["SiteName"]))?string.Empty:(System.String)dataRow["SiteName"];
			entity.CompanySiteCategoryId = (Convert.IsDBNull(dataRow["CompanySiteCategoryId"]))?(int)0:(System.Int32?)dataRow["CompanySiteCategoryId"];
			entity.CompanySiteCategoryDesc = (Convert.IsDBNull(dataRow["CompanySiteCategoryDesc"]))?string.Empty:(System.String)dataRow["CompanySiteCategoryDesc"];
			entity.KpiDateTimeMonth = (Convert.IsDBNull(dataRow["kpiDateTimeMonth"]))?(int)0:(System.Int32?)dataRow["kpiDateTimeMonth"];
			entity.KpiDateTimeYear = (Convert.IsDBNull(dataRow["kpiDateTimeYear"]))?(int)0:(System.Int32?)dataRow["kpiDateTimeYear"];
			entity.KpiDateTime = (Convert.IsDBNull(dataRow["kpiDateTime"]))?DateTime.MinValue:(System.DateTime)dataRow["kpiDateTime"];
			entity.ProjectCapital1Title = (Convert.IsDBNull(dataRow["projectCapital1Title"]))?string.Empty:(System.String)dataRow["projectCapital1Title"];
			entity.ProjectCapital1Hours = (Convert.IsDBNull(dataRow["projectCapital1Hours"]))?0.0m:(System.Decimal?)dataRow["projectCapital1Hours"];
			entity.ProjectCapital2Title = (Convert.IsDBNull(dataRow["projectCapital2Title"]))?string.Empty:(System.String)dataRow["projectCapital2Title"];
			entity.ProjectCapital2Hours = (Convert.IsDBNull(dataRow["projectCapital2Hours"]))?0.0m:(System.Decimal?)dataRow["projectCapital2Hours"];
			entity.ProjectCapital3Title = (Convert.IsDBNull(dataRow["projectCapital3Title"]))?string.Empty:(System.String)dataRow["projectCapital3Title"];
			entity.ProjectCapital3Hours = (Convert.IsDBNull(dataRow["projectCapital3Hours"]))?0.0m:(System.Decimal?)dataRow["projectCapital3Hours"];
			entity.ProjectCapital4Title = (Convert.IsDBNull(dataRow["projectCapital4Title"]))?string.Empty:(System.String)dataRow["projectCapital4Title"];
			entity.ProjectCapital4Hours = (Convert.IsDBNull(dataRow["projectCapital4Hours"]))?0.0m:(System.Decimal?)dataRow["projectCapital4Hours"];
			entity.ProjectCapital5Title = (Convert.IsDBNull(dataRow["projectCapital5Title"]))?string.Empty:(System.String)dataRow["projectCapital5Title"];
			entity.ProjectCapital5Hours = (Convert.IsDBNull(dataRow["projectCapital5Hours"]))?0.0m:(System.Decimal?)dataRow["projectCapital5Hours"];
			entity.ProjectCapital6Title = (Convert.IsDBNull(dataRow["projectCapital6Title"]))?string.Empty:(System.String)dataRow["projectCapital6Title"];
			entity.ProjectCapital6Hours = (Convert.IsDBNull(dataRow["projectCapital6Hours"]))?0.0m:(System.Decimal?)dataRow["projectCapital6Hours"];
			entity.ProjectCapital7Title = (Convert.IsDBNull(dataRow["projectCapital7Title"]))?string.Empty:(System.String)dataRow["projectCapital7Title"];
			entity.ProjectCapital7Hours = (Convert.IsDBNull(dataRow["projectCapital7Hours"]))?0.0m:(System.Decimal?)dataRow["projectCapital7Hours"];
			entity.ProjectCapital8Title = (Convert.IsDBNull(dataRow["projectCapital8Title"]))?string.Empty:(System.String)dataRow["projectCapital8Title"];
			entity.ProjectCapital8Hours = (Convert.IsDBNull(dataRow["projectCapital8Hours"]))?0.0m:(System.Decimal?)dataRow["projectCapital8Hours"];
			entity.ProjectCapital9Title = (Convert.IsDBNull(dataRow["projectCapital9Title"]))?string.Empty:(System.String)dataRow["projectCapital9Title"];
			entity.ProjectCapital9Hours = (Convert.IsDBNull(dataRow["projectCapital9Hours"]))?0.0m:(System.Decimal?)dataRow["projectCapital9Hours"];
			entity.ProjectCapital10Title = (Convert.IsDBNull(dataRow["projectCapital10Title"]))?string.Empty:(System.String)dataRow["projectCapital10Title"];
			entity.ProjectCapital10Hours = (Convert.IsDBNull(dataRow["projectCapital10Hours"]))?0.0m:(System.Decimal?)dataRow["projectCapital10Hours"];
			entity.ProjectCapital11Title = (Convert.IsDBNull(dataRow["projectCapital11Title"]))?string.Empty:(System.String)dataRow["projectCapital11Title"];
			entity.ProjectCapital11Hours = (Convert.IsDBNull(dataRow["projectCapital11Hours"]))?0.0m:(System.Decimal?)dataRow["projectCapital11Hours"];
			entity.ProjectCapital12Title = (Convert.IsDBNull(dataRow["projectCapital12Title"]))?string.Empty:(System.String)dataRow["projectCapital12Title"];
			entity.ProjectCapital12Hours = (Convert.IsDBNull(dataRow["projectCapital12Hours"]))?0.0m:(System.Decimal?)dataRow["projectCapital12Hours"];
			entity.ProjectCapital13Title = (Convert.IsDBNull(dataRow["projectCapital13Title"]))?string.Empty:(System.String)dataRow["projectCapital13Title"];
			entity.ProjectCapital13Hours = (Convert.IsDBNull(dataRow["projectCapital13Hours"]))?0.0m:(System.Decimal?)dataRow["projectCapital13Hours"];
			entity.ProjectCapital14Title = (Convert.IsDBNull(dataRow["projectCapital14Title"]))?string.Empty:(System.String)dataRow["projectCapital14Title"];
			entity.ProjectCapital14Hours = (Convert.IsDBNull(dataRow["projectCapital14Hours"]))?0.0m:(System.Decimal?)dataRow["projectCapital14Hours"];
			entity.ProjectCapital15Title = (Convert.IsDBNull(dataRow["projectCapital15Title"]))?string.Empty:(System.String)dataRow["projectCapital15Title"];
			entity.ProjectCapital15Hours = (Convert.IsDBNull(dataRow["projectCapital15Hours"]))?0.0m:(System.Decimal?)dataRow["projectCapital15Hours"];
			entity.AcceptChanges();
		}
		*/
			
		#endregion Helper Functions
	}//end class

	#region KpiEngineeringProjectHoursFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="KpiEngineeringProjectHours"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class KpiEngineeringProjectHoursFilterBuilder : SqlFilterBuilder<KpiEngineeringProjectHoursColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the KpiEngineeringProjectHoursFilterBuilder class.
		/// </summary>
		public KpiEngineeringProjectHoursFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the KpiEngineeringProjectHoursFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public KpiEngineeringProjectHoursFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the KpiEngineeringProjectHoursFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public KpiEngineeringProjectHoursFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion KpiEngineeringProjectHoursFilterBuilder

	#region KpiEngineeringProjectHoursParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="KpiEngineeringProjectHours"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class KpiEngineeringProjectHoursParameterBuilder : ParameterizedSqlFilterBuilder<KpiEngineeringProjectHoursColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the KpiEngineeringProjectHoursParameterBuilder class.
		/// </summary>
		public KpiEngineeringProjectHoursParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the KpiEngineeringProjectHoursParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public KpiEngineeringProjectHoursParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the KpiEngineeringProjectHoursParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public KpiEngineeringProjectHoursParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion KpiEngineeringProjectHoursParameterBuilder
	
	#region KpiEngineeringProjectHoursSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="KpiEngineeringProjectHours"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class KpiEngineeringProjectHoursSortBuilder : SqlSortBuilder<KpiEngineeringProjectHoursColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the KpiEngineeringProjectHoursSqlSortBuilder class.
		/// </summary>
		public KpiEngineeringProjectHoursSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion KpiEngineeringProjectHoursSortBuilder

} // end namespace
