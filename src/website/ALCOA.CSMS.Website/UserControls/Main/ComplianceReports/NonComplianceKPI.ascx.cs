﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Text;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using KaiZen.CSMS.Web;
using KaiZen.CSMS.Services;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxTabControl;
using DevExpress.Web.Data;
using DevExpress.Web.ASPxGridView;
using System.Drawing;
using DevExpress.Web.ASPxCallbackPanel;
using DevExpress.Web.ASPxClasses;
using DevExpress.XtraPrinting;
using DevExpress.Web.ASPxGridView.Export;


public partial class UserControls_NonComplianceKPI : System.Web.UI.UserControl
{
    Auth auth = new Auth();
    protected void Page_Load(object sender, EventArgs e) {  Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); }
    TList<Sites> sList;
    private void moduleInit(bool postBack)
    {
        if (!postBack)
        {
            string group = "AU";
            if (Request.QueryString["Group"] != null) { group = Request.QueryString["Group"].ToString(); }
            RegionsService rService = new RegionsService();
            Regions rList = rService.GetByRegionInternalName(group);
            cbRegion.Value = rList.RegionId;
            SessionHandler.spVar_RegionId = cbRegion.Value.ToString();

            /*new code*/
            cmbYear.DataSourceID = "sqldsKPIYear";
            cmbYear.TextField = "Year";
            cmbYear.ValueField = "Year";
            cmbYear.Value = DateTime.Now.Year.ToString();
            cmbYear.DataBind();
            SessionHandler.spVar_Year = DateTime.Now.Year.ToString();



            if (SessionHandler.spVar_Page != "SummaryCompliance")
            {
                // Export
                String exportFileName = @"ALCOA CSMS - Non Compliance Report - Key Performance Indicators (KPI)"; //Chrome & IE is fine.
                String userAgent = Request.Headers.Get("User-Agent");
                if (
                    (userAgent.Contains("Mozilla") && !userAgent.Contains("AppleWebKit") && !userAgent.Contains("MSIE")) || //Mozilla
                    (userAgent.Contains("AppleWebKit") && !userAgent.Contains("Chrome"))      //Safari
                    )
                {
                    exportFileName = exportFileName.Replace(" ", "_");
                }
                Helper.ExportGrid.Settings(ucExportKPIReport, exportFileName);
                //Added by Debashis
                ASPxGridViewExporter exCompanyNote = (ASPxGridViewExporter)ucExportKPIReport.FindControl("gridExporter");
                exCompanyNote.GridViewID = "gridKPIReport";
                exCompanyNote.FileName = exportFileName; 
            }
            else
            {
                ucExportKPIReport.Visible = false;
            }


            //DataSet ds = DataRepository.KpiProvider.Get

            switch (auth.RoleId)
            {
                case ((int)RoleList.Reader):
                    //full access
                    break;
                case ((int)RoleList.Contractor):
                    //restrict to own company
                    //sqldsNonComplianceReportYearContractor.DataBind();
                    //gridKPIReport.DataSourceID = "sqldsNonComplianceReportYearContractor";
                    gridKPIReport.Settings.ShowFilterRow = false;
                    gridKPIReport.FilterExpression = String.Format("CompanyId = {0}", auth.CompanyId);
                    gridKPIReport.DataBind();
                    break;
                case ((int)RoleList.Administrator):
                    //full access

                    break;
                default:
                    // do something
                    Response.Redirect("AccessDenied.aspx?error=" + Server.UrlEncode("Unrecognised User."));
                    break;
            }

            //if (!IsPostBack && !IsCallback)
            //{
            //    gridKPIReport.StartEdit(1);
            //} 

            if (SessionHandler.spVar_Page == "SummaryCompliance")
            {
                string companyid, siteid, residentialstatus, year;
                if (Convert.ToInt32(SessionHandler.spVar_CompanyId) < 0) { companyid = "%"; }
                else { companyid = SessionHandler.spVar_CompanyId; };
                if (Convert.ToInt32(SessionHandler.spVar_SiteId) < 0) { siteid = "%"; }
                else { siteid = SessionHandler.spVar_SiteId; };
                residentialstatus = SessionHandler.spVar_CategoryId2;
                year = SessionHandler.spVar_Year;

                gridKPIReport.FilterExpression = String.Format("CompanyId = {0}AND SiteId = {1}AND Embedded = {2}AND Year = {3}", companyid, siteid, residentialstatus, year);
                gridKPIReport.DataBind();
                cmbYear.Value = year;
            }

        }
    }

    public static string Capitalize(string value)
    {
        return System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(value);

    }

    //Start:Change by Debashis for Export issue
    protected void lblSiteName_Init(object sender, EventArgs e)
    {
        Label lblSiteName = (Label)sender;
        GridViewDataItemTemplateContainer c = (GridViewDataItemTemplateContainer)lblSiteName.NamingContainer;
        lblSiteName.Text = DataBinder.Eval(c.DataItem, "SiteName").ToString();
        int i = 0;
        int i2 = 0;
        SitesFilters sFilters = new SitesFilters();
        sFilters.AppendIsNotNull(SitesColumn.SiteNameEbi);
        sFilters.Junction = "AND";
        sFilters.AppendNotEquals(SitesColumn.SiteNameEbi, "");
        sList = DataRepository.SitesProvider.GetPaged(sFilters.ToString(), null, 0, 999999, out i2);

        foreach (Sites s in sList)
        {
            if (lblSiteName.Text == s.SiteName)
            {
                i++;
            }
        }
        if (i == 0) lblSiteName.Text += "*";
    }

    protected void noKPISubmitted13_Init(object sender, EventArgs e)
    {
        int month;
        string selectedYear = "";
        if (cmbYear.SelectedItem != null)
        {
            if (cmbYear.SelectedItem.Value != null)
                selectedYear = cmbYear.SelectedItem.Value.ToString();
        }
        if (String.IsNullOrEmpty(selectedYear)) { selectedYear = DateTime.Now.Year.ToString(); };
        if (selectedYear == DateTime.Now.Year.ToString())
        {
            month = DateTime.Now.Month;
        }
        else
        {
            month = 12;
        }
        bool expected = false;
        int monthCheck = 1;
        while (monthCheck <= month)
        {
            MonthsService mService = new MonthsService();
            Months m = mService.GetByMonthId(monthCheck);
            string monthAbbrev = Capitalize(m.MonthAbbrev.ToLower());
            //
            Label lblnoKPISubmitted11 = (Label)sender;
            GridViewDataItemTemplateContainer c = (GridViewDataItemTemplateContainer)lblnoKPISubmitted11.NamingContainer;
            lblnoKPISubmitted11.Text = " "; //NON-ASCII
            //
            //Label label = gridKPIReport.FindRowCellTemplateControl(e.VisibleIndex, null, String.Format("noKPISubmitted{0}", monthCheck)) as Label;
            int change = (int)gridKPIReport.GetRowValues(c.VisibleIndex, monthAbbrev);
                      
            System.Web.UI.WebControls.Image img = gridKPIReport.FindRowCellTemplateControl(c.VisibleIndex, null, String.Format("changeImage{0}", monthCheck)) as System.Web.UI.WebControls.Image;
            //if (img != null)
            //{
                if (change != 0)
                {
                    if (change != -1)
                    {
                        img.Visible = true;
                        lblnoKPISubmitted11.ForeColor = Color.Green;
                        if (change == 2)
                        {
                            lblnoKPISubmitted11.ForeColor = Color.Black;
                            lblnoKPISubmitted11.Text = "*";
                        }
                        if (change == 3)
                            img.ImageUrl = "~/Images/TrafficLights/tlYellow.gif";
                        else
                            img.ImageUrl = "~/Images/greentick.gif";
                    }
                    else
                    {
                        img.Visible = false;
                    }
                }
                else
                { //0
                    img.ImageUrl = "~/Images/redcross.gif";
                    lblnoKPISubmitted11.ForeColor = Color.Red;
                    img.Visible = true;
                    expected = true;
                }
            //}


            monthCheck++;
            // }
        }
//        ViewState["Expected"] = expected;
        Label lblnoKPISubmitted13 = (Label)sender;
        GridViewDataItemTemplateContainer c1 = (GridViewDataItemTemplateContainer)lblnoKPISubmitted13.NamingContainer;
        int yearCount = (int)gridKPIReport.GetRowValues(c1.VisibleIndex, "Year");

        lblnoKPISubmitted13.Text = yearCount.ToString();

        
            if (expected)
            {
                lblnoKPISubmitted13.ForeColor = Color.Red;
            }
            else
            {
                lblnoKPISubmitted13.ForeColor = Color.Green;
            }
        


    }
    //protected void noKPISubmitted13_Init(object sender, EventArgs e)
    //{
        //Label lblnoKPISubmitted13 = (Label)sender;
        //GridViewDataItemTemplateContainer c = (GridViewDataItemTemplateContainer)lblnoKPISubmitted13.NamingContainer;
        //int yearCount = (int)gridKPIReport.GetRowValues(c.VisibleIndex, "Year");
                
        //lblnoKPISubmitted13.Text = yearCount.ToString();

        //if (ViewState["Expected"] != null)
        //{
        //    bool expected = (bool)ViewState["Expected"];
        //    if (expected)
        //    {
        //        lblnoKPISubmitted13.ForeColor = Color.Red;
        //    }
        //    else
        //    {
        //        lblnoKPISubmitted13.ForeColor = Color.Green;
        //    }
        //}
    //}
    
    //End:Change by Debashis

    protected void gridKPIReport_RowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {
        //Start: Commented By Debashis for deployment issue(Export)
        //int month;
        //string selectedYear = "";
        //if (cmbYear.SelectedItem != null)
        //{
        //    if (cmbYear.SelectedItem.Value != null)
        //        selectedYear = cmbYear.SelectedItem.Value.ToString();
        //}

        //if (String.IsNullOrEmpty(selectedYear)) { selectedYear = DateTime.Now.Year.ToString(); };
        //if (selectedYear == DateTime.Now.Year.ToString())
        //{
        //    month = DateTime.Now.Month;
        //}
        //else
        //{
        //    month = 12;
        //}

        if (e.RowType != GridViewRowType.Data) return;
        //Start: Commented By Debashis for deployment issue(Export)
        /*
        Label lblSiteName = gridKPIReport.FindRowCellTemplateControl(e.VisibleIndex, null, "lblSiteName") as Label;
        //if (lblSiteName != null)
        //{
            lblSiteName.Text = (string)gridKPIReport.GetRowValues(e.VisibleIndex, "SiteName");
            int i = 0;

            int i2 = 0;
            SitesFilters sFilters = new SitesFilters();
            sFilters.AppendIsNotNull(SitesColumn.SiteNameEbi);
            sFilters.Junction = "AND";
            sFilters.AppendNotEquals(SitesColumn.SiteNameEbi, "");
            sList = DataRepository.SitesProvider.GetPaged(sFilters.ToString(), null, 0, 999999, out i2);

            //foreach (Sites s in sList)
            //{
            //    if (lblSiteName.Text == s.SiteName)
            //    {
            //        i++;
            //    }
            //}
            //if (i == 0) lblSiteName.Text += "*";
        //}
        
        
        
        bool expected = false;
        int monthCheck = 1;
        while (monthCheck <= month)
        {
            MonthsService mService = new MonthsService();
            Months m = mService.GetByMonthId(monthCheck);
            string monthAbbrev = Capitalize(m.MonthAbbrev.ToLower());

            Label label = gridKPIReport.FindRowCellTemplateControl(e.VisibleIndex, null, String.Format("noKPISubmitted{0}", monthCheck)) as Label;
            int change = (int)gridKPIReport.GetRowValues(e.VisibleIndex, monthAbbrev);
            //Start: Change By Denashis for deployment issue(Export)
            //if (label != null)
            //{
                label.Text = " "; //NON-ASCII
                System.Web.UI.WebControls.Image img = gridKPIReport.FindRowCellTemplateControl(e.VisibleIndex, null, String.Format("changeImage{0}", monthCheck)) as System.Web.UI.WebControls.Image;
                 if (change != 0)
                    {
                        if (change != -1)
                        {
                            img.Visible = true;
                            label.ForeColor = Color.Green;
                            if (change == 2)
                            {
                                label.ForeColor = Color.Black;
                                label.Text = "*";
                            }
                            if (change == 3)
                                img.ImageUrl = "~/Images/TrafficLights/tlYellow.gif";
                            else
                                img.ImageUrl = "~/Images/greentick.gif";
                        }
                        else
                        {
                            img.Visible = false;
                        }
                    }
                    else
                    { //0
                        img.ImageUrl = "~/Images/redcross.gif";
                        label.ForeColor = Color.Red;
                        img.Visible = true;
                        expected = true;
                    }
                

                monthCheck++;
           // }
        }
        
        Control(e.VisibleIndex, null, "noKPISubmitted13") as Label;
        int yearCount = (int)gridKPIReport.GetRowValues(e.VisibleIndex, "Year");

        // DT # 3061 change
        //if (yearCount > 0) 
        //
        //if (lblYear != null)
        //{
            lblYear.Text = yearCount.ToString();

            if (expected)
            {
                lblYear.ForeColor = Color.Red;
            }
            else
            {
                lblYear.ForeColor = Color.Green;
            }
       // }
         * **/
        //End:: Change By Denashis for deployment issue(Export)
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        SessionHandler.spVar_Year = cmbYear.SelectedItem.Value.ToString();
        sqldsComplianceKPI.DataBind();
        gridKPIReport.DataBind();
    }
    protected void cmbYear_SelectedIndexChanged(object sender, EventArgs e)
    {
        SessionHandler.spVar_Year = cmbYear.SelectedItem.Value.ToString();
        sqldsComplianceKPI.DataBind();
        gridKPIReport.DataBind();
    }

    protected void gridKPIReport_AutoFilterCellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
    {
        if (e.Column.FieldName == "CompanyId" || e.Column.FieldName == "SiteName" || e.Column.FieldName == "CompanySiteCategoryId")
        {
            ASPxComboBox comboBox = e.Editor as ASPxComboBox;
            comboBox.ClientSideEvents.Init = "function(s, e) {s.InsertItem(0, '(ALL)', ''); s.RemoveItem(-1);}";
        }
    }
    protected void cbRegion_SelectedIndexChanged(object sender, EventArgs e)
    {
        SessionHandler.spVar_RegionId = cbRegion.Value.ToString();
    }
}
