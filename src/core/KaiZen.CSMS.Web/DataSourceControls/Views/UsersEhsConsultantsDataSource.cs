﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.UsersEhsConsultantsProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[CLSCompliant(true)]
	[Designer(typeof(UsersEhsConsultantsDataSourceDesigner))]
	public class UsersEhsConsultantsDataSource : ReadOnlyDataSource<UsersEhsConsultants>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UsersEhsConsultantsDataSource class.
		/// </summary>
		public UsersEhsConsultantsDataSource() : base(new UsersEhsConsultantsService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the UsersEhsConsultantsDataSourceView used by the UsersEhsConsultantsDataSource.
		/// </summary>
		protected UsersEhsConsultantsDataSourceView UsersEhsConsultantsView
		{
			get { return ( View as UsersEhsConsultantsDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the UsersEhsConsultantsDataSource control invokes to retrieve data.
		/// </summary>
		public new UsersEhsConsultantsSelectMethod SelectMethod
		{
			get
			{
				UsersEhsConsultantsSelectMethod selectMethod = UsersEhsConsultantsSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (UsersEhsConsultantsSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}
		
		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the UsersEhsConsultantsDataSourceView class that is to be
		/// used by the UsersEhsConsultantsDataSource.
		/// </summary>
		/// <returns>An instance of the UsersEhsConsultantsDataSourceView class.</returns>
		protected override BaseDataSourceView<UsersEhsConsultants, Object> GetNewDataSourceView()
		{
			return new UsersEhsConsultantsDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the UsersEhsConsultantsDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class UsersEhsConsultantsDataSourceView : ReadOnlyDataSourceView<UsersEhsConsultants>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UsersEhsConsultantsDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the UsersEhsConsultantsDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public UsersEhsConsultantsDataSourceView(UsersEhsConsultantsDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal UsersEhsConsultantsDataSource UsersEhsConsultantsOwner
		{
			get { return Owner as UsersEhsConsultantsDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal new UsersEhsConsultantsSelectMethod SelectMethod
		{
			get { return UsersEhsConsultantsOwner.SelectMethod; }
			set { UsersEhsConsultantsOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal UsersEhsConsultantsService UsersEhsConsultantsProvider
		{
			get { return Provider as UsersEhsConsultantsService; }
		}

		#endregion Properties
		
		#region Methods
		
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
	    /// <param name="values"></param>
		/// <param name="count">The total number of rows in the DataSource.</param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<UsersEhsConsultants> GetSelectData(IDictionary values, out int count)
		{	
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			
			IList<UsersEhsConsultants> results = null;
			// UsersEhsConsultants item;
			count = 0;
			

			switch ( SelectMethod )
			{
				case UsersEhsConsultantsSelectMethod.Get:
					results = UsersEhsConsultantsProvider.Get(WhereClause, OrderBy, StartIndex, PageSize, out count);
                    break;
				case UsersEhsConsultantsSelectMethod.GetPaged:
					results = UsersEhsConsultantsProvider.GetPaged(WhereClause, OrderBy, StartIndex, PageSize, out count);
					break;
				case UsersEhsConsultantsSelectMethod.GetAll:
					results = UsersEhsConsultantsProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case UsersEhsConsultantsSelectMethod.Find:
					results = UsersEhsConsultantsProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
                    break;
				// Custom
				case UsersEhsConsultantsSelectMethod.GetAll_Active:
					results = UsersEhsConsultantsProvider.GetAll_Active(StartIndex, PageSize);
					break;
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;
				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}				
			}
			
			return results;
		}
		
		#endregion Methods
	}

	#region UsersEhsConsultantsSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the UsersEhsConsultantsDataSource.SelectMethod property.
	/// </summary>
	public enum UsersEhsConsultantsSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetAll_Active method.
		/// </summary>
		GetAll_Active
	}
	
	#endregion UsersEhsConsultantsSelectMethod
	
	#region UsersEhsConsultantsDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the UsersEhsConsultantsDataSource class.
	/// </summary>
	public class UsersEhsConsultantsDataSourceDesigner : ReadOnlyDataSourceDesigner<UsersEhsConsultants>
	{
		/// <summary>
		/// Initializes a new instance of the UsersEhsConsultantsDataSourceDesigner class.
		/// </summary>
		public UsersEhsConsultantsDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public new UsersEhsConsultantsSelectMethod SelectMethod
		{
			get { return ((UsersEhsConsultantsDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new UsersEhsConsultantsDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region UsersEhsConsultantsDataSourceActionList

	/// <summary>
	/// Supports the UsersEhsConsultantsDataSourceDesigner class.
	/// </summary>
	internal class UsersEhsConsultantsDataSourceActionList : DesignerActionList
	{
		private UsersEhsConsultantsDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the UsersEhsConsultantsDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public UsersEhsConsultantsDataSourceActionList(UsersEhsConsultantsDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public UsersEhsConsultantsSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion UsersEhsConsultantsDataSourceActionList

	#endregion UsersEhsConsultantsDataSourceDesigner

	#region UsersEhsConsultantsFilter

	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="UsersEhsConsultants"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UsersEhsConsultantsFilter : SqlFilter<UsersEhsConsultantsColumn>
	{
	}

	#endregion UsersEhsConsultantsFilter

	#region UsersEhsConsultantsExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="UsersEhsConsultants"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UsersEhsConsultantsExpressionBuilder : SqlExpressionBuilder<UsersEhsConsultantsColumn>
	{
	}
	
	#endregion UsersEhsConsultantsExpressionBuilder		
}

