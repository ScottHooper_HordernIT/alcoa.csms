﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.EbiDailyReportProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(EbiDailyReportDataSourceDesigner))]
	public class EbiDailyReportDataSource : ProviderDataSource<EbiDailyReport, EbiDailyReportKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EbiDailyReportDataSource class.
		/// </summary>
		public EbiDailyReportDataSource() : base(new EbiDailyReportService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the EbiDailyReportDataSourceView used by the EbiDailyReportDataSource.
		/// </summary>
		protected EbiDailyReportDataSourceView EbiDailyReportView
		{
			get { return ( View as EbiDailyReportDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the EbiDailyReportDataSource control invokes to retrieve data.
		/// </summary>
		public EbiDailyReportSelectMethod SelectMethod
		{
			get
			{
				EbiDailyReportSelectMethod selectMethod = EbiDailyReportSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (EbiDailyReportSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the EbiDailyReportDataSourceView class that is to be
		/// used by the EbiDailyReportDataSource.
		/// </summary>
		/// <returns>An instance of the EbiDailyReportDataSourceView class.</returns>
		protected override BaseDataSourceView<EbiDailyReport, EbiDailyReportKey> GetNewDataSourceView()
		{
			return new EbiDailyReportDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the EbiDailyReportDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class EbiDailyReportDataSourceView : ProviderDataSourceView<EbiDailyReport, EbiDailyReportKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EbiDailyReportDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the EbiDailyReportDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public EbiDailyReportDataSourceView(EbiDailyReportDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal EbiDailyReportDataSource EbiDailyReportOwner
		{
			get { return Owner as EbiDailyReportDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal EbiDailyReportSelectMethod SelectMethod
		{
			get { return EbiDailyReportOwner.SelectMethod; }
			set { EbiDailyReportOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal EbiDailyReportService EbiDailyReportProvider
		{
			get { return Provider as EbiDailyReportService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<EbiDailyReport> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<EbiDailyReport> results = null;
			EbiDailyReport item;
			count = 0;
			
			System.Int32 _ebiDailyReportId;
			System.DateTime _dateTime;
			System.Int32 _ebiIssueId;

			switch ( SelectMethod )
			{
				case EbiDailyReportSelectMethod.Get:
					EbiDailyReportKey entityKey  = new EbiDailyReportKey();
					entityKey.Load(values);
					item = EbiDailyReportProvider.Get(entityKey);
					results = new TList<EbiDailyReport>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case EbiDailyReportSelectMethod.GetAll:
                    results = EbiDailyReportProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case EbiDailyReportSelectMethod.GetPaged:
					results = EbiDailyReportProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case EbiDailyReportSelectMethod.Find:
					if ( FilterParameters != null )
						results = EbiDailyReportProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = EbiDailyReportProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case EbiDailyReportSelectMethod.GetByEbiDailyReportId:
					_ebiDailyReportId = ( values["EbiDailyReportId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["EbiDailyReportId"], typeof(System.Int32)) : (int)0;
					item = EbiDailyReportProvider.GetByEbiDailyReportId(_ebiDailyReportId);
					results = new TList<EbiDailyReport>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				case EbiDailyReportSelectMethod.GetByDateTime:
					_dateTime = ( values["DateTime"] != null ) ? (System.DateTime) EntityUtil.ChangeType(values["DateTime"], typeof(System.DateTime)) : DateTime.MinValue;
					results = EbiDailyReportProvider.GetByDateTime(_dateTime, this.StartIndex, this.PageSize, out count);
					break;
				// FK
				case EbiDailyReportSelectMethod.GetByEbiIssueId:
					_ebiIssueId = ( values["EbiIssueId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["EbiIssueId"], typeof(System.Int32)) : (int)0;
					results = EbiDailyReportProvider.GetByEbiIssueId(_ebiIssueId, this.StartIndex, this.PageSize, out count);
					break;
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == EbiDailyReportSelectMethod.Get || SelectMethod == EbiDailyReportSelectMethod.GetByEbiDailyReportId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				EbiDailyReport entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					EbiDailyReportProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<EbiDailyReport> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			EbiDailyReportProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region EbiDailyReportDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the EbiDailyReportDataSource class.
	/// </summary>
	public class EbiDailyReportDataSourceDesigner : ProviderDataSourceDesigner<EbiDailyReport, EbiDailyReportKey>
	{
		/// <summary>
		/// Initializes a new instance of the EbiDailyReportDataSourceDesigner class.
		/// </summary>
		public EbiDailyReportDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public EbiDailyReportSelectMethod SelectMethod
		{
			get { return ((EbiDailyReportDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new EbiDailyReportDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region EbiDailyReportDataSourceActionList

	/// <summary>
	/// Supports the EbiDailyReportDataSourceDesigner class.
	/// </summary>
	internal class EbiDailyReportDataSourceActionList : DesignerActionList
	{
		private EbiDailyReportDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the EbiDailyReportDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public EbiDailyReportDataSourceActionList(EbiDailyReportDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public EbiDailyReportSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion EbiDailyReportDataSourceActionList
	
	#endregion EbiDailyReportDataSourceDesigner
	
	#region EbiDailyReportSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the EbiDailyReportDataSource.SelectMethod property.
	/// </summary>
	public enum EbiDailyReportSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByEbiDailyReportId method.
		/// </summary>
		GetByEbiDailyReportId,
		/// <summary>
		/// Represents the GetByDateTime method.
		/// </summary>
		GetByDateTime,
		/// <summary>
		/// Represents the GetByEbiIssueId method.
		/// </summary>
		GetByEbiIssueId
	}
	
	#endregion EbiDailyReportSelectMethod

	#region EbiDailyReportFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EbiDailyReport"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EbiDailyReportFilter : SqlFilter<EbiDailyReportColumn>
	{
	}
	
	#endregion EbiDailyReportFilter

	#region EbiDailyReportExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EbiDailyReport"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EbiDailyReportExpressionBuilder : SqlExpressionBuilder<EbiDailyReportColumn>
	{
	}
	
	#endregion EbiDailyReportExpressionBuilder	

	#region EbiDailyReportProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;EbiDailyReportChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="EbiDailyReport"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EbiDailyReportProperty : ChildEntityProperty<EbiDailyReportChildEntityTypes>
	{
	}
	
	#endregion EbiDailyReportProperty
}

