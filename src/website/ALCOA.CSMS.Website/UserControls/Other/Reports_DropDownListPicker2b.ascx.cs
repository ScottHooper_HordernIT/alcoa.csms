using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using DevExpress.Web.ASPxClasses;
using DevExpress.Web.ASPxCallbackPanel;

using KaiZen.CSMS.Data;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Services;

public partial class UserControls_Tiny_Reports_DropDownListPicker2b : System.Web.UI.UserControl
{
    Auth auth = new Auth();

    // Delegate declaration
    public delegate void OnButtonClick(string strValue);

    // Event declaration
    public event OnButtonClick btnHandler;

    int intTryParse = 0;

    protected void Page_Load(object sender, EventArgs e) {  Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); }

    private void moduleInit(bool postBack)
    {
        if (!postBack)
        {
            ddlSites.DataSourceID = "dsSitesFilter";
            ddlSites.TextField = "SiteName";
            ddlSites.ValueField = "SiteId";
            ddlSites.DataBind();
            ddlSites.Items.Add("----------------", 0);

            //Add Regions
            RegionsService rService = new RegionsService();
            TList<Regions>  regionList= rService.GetAll();
            if (regionList != null)
            {
                foreach (Regions region in regionList)
                {
                    if(region.IsVisible == true)
                    ddlSites.Items.Add(region.RegionName, (-1 * region.RegionId));
                }
            }

            ddlCategory.DataSourceID = "CompanySiteCategoryDataSource";
            ddlCategory.TextField = "CategoryDesc";
            ddlCategory.ValueField = "CompanySiteCategoryId";
            ddlCategory.DataBind();
            ddlCategory.Items.Add("-----------------------------", 0);
            ddlCategory.Items.Add("All", -1);
            ddlCategory.Value = -1;

            ddlYear.DataSourceID = "sqldsKpi_GetAllYearsSubmitted";
            ddlYear.TextField = "Year";
            ddlYear.ValueField = "Year";
            ddlYear.DataBind();

            DataTable dt = ((DataView)sqldsKpi_GetAllYearsSubmitted.Select(DataSourceSelectArguments.Empty)).ToTable();
            int r = dt.Rows.Count - 1;
            int latestYear = 0;
            Int32.TryParse(dt.Rows[r].ItemArray[0].ToString(), out latestYear);

            if (latestYear != 0)
            {
                ddlYear.Value = latestYear;
            }
            else
            {
                ddlYear.Value = DateTime.Now.Year;
            }

            LoadPage();
        }
    }

    protected void LoadPage()
    {
        if (String.IsNullOrEmpty(SessionHandler.spVar_MonthId)) SessionHandler.spVar_MonthId = "0";
        if (String.IsNullOrEmpty(SessionHandler.spVar_CategoryId)) SessionHandler.spVar_CategoryId = "-1";

        if (String.IsNullOrEmpty(SessionHandler.spVar_SiteId))
        {
            ddlSites.Text = "< Select Site >";
        }
        else
        {
            if (Convert.ToInt32(SessionHandler.spVar_SiteId) != 0)
            {
                ddlSites.Value = Convert.ToInt32(SessionHandler.spVar_SiteId);
                ddlCategory.Value = Convert.ToInt32(SessionHandler.spVar_CategoryId);
                FillCompaniesCombo(ddlSites.Value.ToString(), ddlCategory.Value.ToString());
            }
            else
            {
                ddlSites.Text = "< Select Site >";
            }
        }

        /*
        if (!String.IsNullOrEmpty(SessionHandler.spVar_CompanyId))
        {
            if (Convert.ToInt32(SessionHandler.spVar_CompanyId) > 0) ddlCompanies.Value = Convert.ToInt32(SessionHandler.spVar_CompanyId);
        } */

        if (!String.IsNullOrEmpty(SessionHandler.spVar_Year))
        {
            if (Int32.TryParse(SessionHandler.spVar_Year, out intTryParse))
            {
                ddlYear.Value = Convert.ToInt32(SessionHandler.spVar_Year);
            }
        }


        if (!String.IsNullOrEmpty(SessionHandler.spVar_CategoryId))
        {
            if (Int32.TryParse(SessionHandler.spVar_CategoryId, out intTryParse))
            {
                ddlCategory.Value = Convert.ToInt32(SessionHandler.spVar_CategoryId);
            }
        }
    }

    protected void cmbCompanies_Callback(object source, CallbackEventArgsBase e)
    {
        //ddlCompanies.SelectedIndex = 0;
        //FillCompaniesCombo(e.Parameter);
    }
    protected void ddlSites_SelectedIndexChanged(object sender, EventArgs e)
    {
        //reset categoryid
        FillCompaniesCombo(ddlSites.SelectedItem.Value.ToString(), ddlCategory.SelectedItem.Value.ToString());

    }
    protected void ddlCategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlSites.SelectedItem != null && ddlCategory.SelectedItem != null)
        {
            FillCompaniesCombo(ddlSites.SelectedItem.Value.ToString(), ddlCategory.SelectedItem.Value.ToString());
        }
    }

    protected void FillCompaniesCombo(string SiteId, string CategoryId)
    {
        if (string.IsNullOrEmpty(SiteId)) return;
        if (string.IsNullOrEmpty(CategoryId)) return;

        int _SiteId;
        bool SiteId_isInt = Int32.TryParse(SiteId, out _SiteId);
        if (!SiteId_isInt) return;

        SessionHandler.spVar_CategoryId = CategoryId;

        if (_SiteId > 0) //Specific Site
        {
            SessionHandler.spVar_SiteId = SiteId;
            
        }
    }
    protected void ddlYear_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void btnSearchGo_Click(object sender, EventArgs e)
    {
        if (btnHandler != null)
            btnHandler(string.Empty);
    }

    protected void ASPxButton1_Click(object sender, EventArgs e)
    {
            btnSearchGo_Click(sender, e);

    }
    protected void ASPxButton2_Click(object sender, EventArgs e)
    {
            btnSearchGo_Click(sender, e);
    }
}
