using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using KaiZen.CSMS.Services;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxTabControl;
using DevExpress.Web.Data;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxHtmlEditor;
using System.Text;

public partial class Adminv2_UserControls_ConfigText : System.Web.UI.UserControl
{
    Auth auth = new Auth();

    protected void Page_Load(object sender, EventArgs e) {  Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); }

    private void moduleInit(bool postBack)
    {
        if (!postBack)
        { //first time load
            if (auth.RoleId != (int)RoleList.Administrator)
            {
                Response.Redirect("default.aspx");
            }
            else
            {
                LoadStuff();
            }
        }
    }

    protected void grid_InitNewRow(object sender, ASPxDataInitNewRowEventArgs e)
    {
        e.NewValues["ConfigTextTypeId"] = 1; //TODO: Enum
    }

    protected void  ASPxButton1_Click(object sender, EventArgs e)
    {
        try
        {
            Save();
        }
        catch (Exception ex)
        {
            Elmah.ErrorSignal.FromContext(Context).Raise(ex);
            lblError.Text = "Error: " + ex.Message;
        }
    }

    protected void LoadStuff()
    {
        ConfigText2Service ct2Service = new ConfigText2Service();
        TList<ConfigText2> ct2 = ct2Service.GetByConfigTextTypeId(2); //hardcoeded for now...
        byte[] s = ct2[0].ConfigText;
        if (s != null) ASPxHtmlEditor2.Html = Encoding.ASCII.GetString(s);

        TList<ConfigText2> ct2_ = ct2Service.GetByConfigTextTypeId(3);
        byte[] s_ = ct2_[0].ConfigText;
        if(s_ != null) ASPxHtmlEditor3.Html = Encoding.ASCII.GetString(s_);

        TList<ConfigText2> ct2_2 = ct2Service.GetByConfigTextTypeId(4);
        byte[] s_2 = ct2_2[0].ConfigText;
        if (s_2 != null) ASPxHtmlEditor4.Html = Encoding.ASCII.GetString(s_2);
    }
    protected void Save()
    {
        ASCIIEncoding encoding = new ASCIIEncoding();
        ConfigText2Service ct2Service = new ConfigText2Service();

        TList<ConfigText2> ct2 = ct2Service.GetByConfigTextTypeId(2); //hardcoeded for now...
        ct2[0].ConfigText = (byte[])encoding.GetBytes(ASPxHtmlEditor2.Html);
        ct2Service.Save(ct2[0]);

        TList<ConfigText2> ct2_ = ct2Service.GetByConfigTextTypeId(3);
        ct2_[0].ConfigText = (byte[])encoding.GetBytes(ASPxHtmlEditor3.Html);
        ct2Service.Save(ct2_[0]);

        TList<ConfigText2> ct2_2 = ct2Service.GetByConfigTextTypeId(4);
        ct2_2[0].ConfigText = (byte[])encoding.GetBytes(ASPxHtmlEditor4.Html);
        ct2Service.Save(ct2_2[0]);
    }
}

