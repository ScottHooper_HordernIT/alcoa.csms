﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.TwentyOnePointAudit19Provider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(TwentyOnePointAudit19DataSourceDesigner))]
	public class TwentyOnePointAudit19DataSource : ProviderDataSource<TwentyOnePointAudit19, TwentyOnePointAudit19Key>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit19DataSource class.
		/// </summary>
		public TwentyOnePointAudit19DataSource() : base(new TwentyOnePointAudit19Service())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the TwentyOnePointAudit19DataSourceView used by the TwentyOnePointAudit19DataSource.
		/// </summary>
		protected TwentyOnePointAudit19DataSourceView TwentyOnePointAudit19View
		{
			get { return ( View as TwentyOnePointAudit19DataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the TwentyOnePointAudit19DataSource control invokes to retrieve data.
		/// </summary>
		public TwentyOnePointAudit19SelectMethod SelectMethod
		{
			get
			{
				TwentyOnePointAudit19SelectMethod selectMethod = TwentyOnePointAudit19SelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (TwentyOnePointAudit19SelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the TwentyOnePointAudit19DataSourceView class that is to be
		/// used by the TwentyOnePointAudit19DataSource.
		/// </summary>
		/// <returns>An instance of the TwentyOnePointAudit19DataSourceView class.</returns>
		protected override BaseDataSourceView<TwentyOnePointAudit19, TwentyOnePointAudit19Key> GetNewDataSourceView()
		{
			return new TwentyOnePointAudit19DataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the TwentyOnePointAudit19DataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class TwentyOnePointAudit19DataSourceView : ProviderDataSourceView<TwentyOnePointAudit19, TwentyOnePointAudit19Key>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit19DataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the TwentyOnePointAudit19DataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public TwentyOnePointAudit19DataSourceView(TwentyOnePointAudit19DataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal TwentyOnePointAudit19DataSource TwentyOnePointAudit19Owner
		{
			get { return Owner as TwentyOnePointAudit19DataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal TwentyOnePointAudit19SelectMethod SelectMethod
		{
			get { return TwentyOnePointAudit19Owner.SelectMethod; }
			set { TwentyOnePointAudit19Owner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal TwentyOnePointAudit19Service TwentyOnePointAudit19Provider
		{
			get { return Provider as TwentyOnePointAudit19Service; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<TwentyOnePointAudit19> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<TwentyOnePointAudit19> results = null;
			TwentyOnePointAudit19 item;
			count = 0;
			
			System.Int32 _id;

			switch ( SelectMethod )
			{
				case TwentyOnePointAudit19SelectMethod.Get:
					TwentyOnePointAudit19Key entityKey  = new TwentyOnePointAudit19Key();
					entityKey.Load(values);
					item = TwentyOnePointAudit19Provider.Get(entityKey);
					results = new TList<TwentyOnePointAudit19>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case TwentyOnePointAudit19SelectMethod.GetAll:
                    results = TwentyOnePointAudit19Provider.GetAll(StartIndex, PageSize, out count);
                    break;
				case TwentyOnePointAudit19SelectMethod.GetPaged:
					results = TwentyOnePointAudit19Provider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case TwentyOnePointAudit19SelectMethod.Find:
					if ( FilterParameters != null )
						results = TwentyOnePointAudit19Provider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = TwentyOnePointAudit19Provider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case TwentyOnePointAudit19SelectMethod.GetById:
					_id = ( values["Id"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["Id"], typeof(System.Int32)) : (int)0;
					item = TwentyOnePointAudit19Provider.GetById(_id);
					results = new TList<TwentyOnePointAudit19>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				// FK
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == TwentyOnePointAudit19SelectMethod.Get || SelectMethod == TwentyOnePointAudit19SelectMethod.GetById )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				TwentyOnePointAudit19 entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					TwentyOnePointAudit19Provider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<TwentyOnePointAudit19> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			TwentyOnePointAudit19Provider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region TwentyOnePointAudit19DataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the TwentyOnePointAudit19DataSource class.
	/// </summary>
	public class TwentyOnePointAudit19DataSourceDesigner : ProviderDataSourceDesigner<TwentyOnePointAudit19, TwentyOnePointAudit19Key>
	{
		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit19DataSourceDesigner class.
		/// </summary>
		public TwentyOnePointAudit19DataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public TwentyOnePointAudit19SelectMethod SelectMethod
		{
			get { return ((TwentyOnePointAudit19DataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new TwentyOnePointAudit19DataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region TwentyOnePointAudit19DataSourceActionList

	/// <summary>
	/// Supports the TwentyOnePointAudit19DataSourceDesigner class.
	/// </summary>
	internal class TwentyOnePointAudit19DataSourceActionList : DesignerActionList
	{
		private TwentyOnePointAudit19DataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit19DataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public TwentyOnePointAudit19DataSourceActionList(TwentyOnePointAudit19DataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public TwentyOnePointAudit19SelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion TwentyOnePointAudit19DataSourceActionList
	
	#endregion TwentyOnePointAudit19DataSourceDesigner
	
	#region TwentyOnePointAudit19SelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the TwentyOnePointAudit19DataSource.SelectMethod property.
	/// </summary>
	public enum TwentyOnePointAudit19SelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetById method.
		/// </summary>
		GetById
	}
	
	#endregion TwentyOnePointAudit19SelectMethod

	#region TwentyOnePointAudit19Filter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit19"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAudit19Filter : SqlFilter<TwentyOnePointAudit19Column>
	{
	}
	
	#endregion TwentyOnePointAudit19Filter

	#region TwentyOnePointAudit19ExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit19"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAudit19ExpressionBuilder : SqlExpressionBuilder<TwentyOnePointAudit19Column>
	{
	}
	
	#endregion TwentyOnePointAudit19ExpressionBuilder	

	#region TwentyOnePointAudit19Property
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;TwentyOnePointAudit19ChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit19"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAudit19Property : ChildEntityProperty<TwentyOnePointAudit19ChildEntityTypes>
	{
	}
	
	#endregion TwentyOnePointAudit19Property
}

