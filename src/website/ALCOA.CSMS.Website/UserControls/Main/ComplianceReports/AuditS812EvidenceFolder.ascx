<%@ Control Language="C#" AutoEventWireup="true"
    Inherits="UserControls_AuditS812EvidenceFolder" CodeBehind="AuditS812EvidenceFolder.ascx.cs" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>


<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxUploadControl" TagPrefix="dx" %>


<span class="title">S8.12 Evidence Files</span>
<table border="0" cellpadding="2" cellspacing="0" width="100%">
    <tr>
        <td>
            <dx:ASPxLabel ID="lblNewFile" runat="server" Text="Add New File"></dx:ASPxLabel>
                <dx:ASPxUploadControl ID="uplNewFile" runat="server" UploadMode="Auto" Width="280px" ShowProgressPanel="True" ShowUploadButton="True" OnFileUploadComplete="uplNewFile_FileUploadComplete" FileUploadMode="OnPageLoad">
                    <ClientSideEvents FileUploadComplete="function(s, e) {
	ctl00_body_AuditS812EvidenceFolder1_gridFiles.PerformCallback('');
}" />
                </dx:ASPxUploadControl>
            
        </td>
    </tr>

    <tr>
        <td>
            <dx:ASPxGridView ID="gridFiles" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue" AutoGenerateColumns="False" DataSourceID="dsS812EvidenceFile" KeyFieldName="S812EvidenceFileId"
                OnCustomButtonCallback="grid_CustomButtonCallback" OnCustomCallback="gridFiles_CustomCallback" SettingsBehavior-AllowSelectSingleRowOnly="true">
                <ClientSideEvents CustomButtonClick="function(s, e) {
	                if(e.buttonID == 'btnDownload'){
                        //GetRowValues sends the result to the OnGetRowValues function
                        s.GetRowValues(e.visibleIndex, 'S812EvidenceFileId', OnGetRowValues);
                        
                    }
                }
                
                    " />
                <Columns>
                    <dx:GridViewCommandColumn ShowDeleteButton="True" VisibleIndex="0" Name="CommandCol">
                    </dx:GridViewCommandColumn>
                    <dx:GridViewDataTextColumn Caption="File Name" FieldName="FileName" VisibleIndex="2">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Upload Date" FieldName="UploadDate" VisibleIndex="3">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Id" FieldName="S812EvidenceFileId" VisibleIndex="1" Visible="False">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="File Size" FieldName="FileSize" VisibleIndex="6">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataComboBoxColumn Caption="Uploaded By" FieldName="UploadedById" VisibleIndex="5">
                        <PropertiesComboBox DataSourceID="dsUsers" TextField="FullName" ValueField="UserId">
                        </PropertiesComboBox>
                    </dx:GridViewDataComboBoxColumn>
                    <dx:GridViewCommandColumn Caption="Download" VisibleIndex="8">
                        <CustomButtons>
                            <dx:GridViewCommandColumnCustomButton ID="btnDownload" Text="Download">
                            </dx:GridViewCommandColumnCustomButton>
                        </CustomButtons>
                    </dx:GridViewCommandColumn>
                </Columns>
                <SettingsDataSecurity AllowEdit="False" AllowInsert="False" />
                <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue">
                </Styles>
                <SettingsBehavior ConfirmDelete="True"></SettingsBehavior>
            </dx:ASPxGridView>
        </td>
    </tr>
</table>

<asp:ObjectDataSource ID="dsS812EvidenceFile" runat="server" DataObjectTypeName="Repo.CSMS.DAL.EntityModels.S812EvidenceFile" TypeName="Repo.CSMS.Service.Database.IS812EvidenceFileService"
    DeleteMethod="Delete" InsertMethod="Insert" UpdateMethod="Update" SelectMethod="GetAll"
    OldValuesParameterFormatString="original_{0}" OnObjectCreating="ods_ObjectCreating">
    <DeleteParameters>
        <asp:Parameter Name="S812EvidenceFileId" Type="Int32" />
    </DeleteParameters>
</asp:ObjectDataSource>

<asp:ObjectDataSource ID="dsUsers" runat="server" DataObjectTypeName="Repo.CSMS.DAL.EntityModels.User" TypeName="Repo.CSMS.Service.Database.IUserService"
    SelectMethod="GetAll"
    OldValuesParameterFormatString="original_{0}" OnObjectCreating="ods_UsersCreating"></asp:ObjectDataSource>

<script type="text/javascript">
    var download = function (url, data) {
        if (!(data)) { data = {}; }

        // remove any existing iframes
        $("iframe.download-file").remove();
        
        // add in
        $("<iframe class='download-file'></iframe>")
            .css("display", "none")
            .attr("src", url)
            .appendTo("body");
        // + "?" + $.param(data)
    };
    function OnGetRowValues(values) {
        download('Common/GetFile.aspx?Type=S812CustomFile&&File=Custom&Id='+ values);
    };
</script>