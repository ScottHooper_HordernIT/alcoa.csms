﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="PrivilegeProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class PrivilegeProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.Privilege, KaiZen.CSMS.Entities.PrivilegeKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.PrivilegeKey key)
		{
			return Delete(transactionManager, key.PrivilegeId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_privilegeId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _privilegeId)
		{
			return Delete(null, _privilegeId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_privilegeId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _privilegeId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.Privilege Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.PrivilegeKey key, int start, int pageLength)
		{
			return GetByPrivilegeId(transactionManager, key.PrivilegeId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_Privilege index.
		/// </summary>
		/// <param name="_privilegeId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Privilege"/> class.</returns>
		public KaiZen.CSMS.Entities.Privilege GetByPrivilegeId(System.Int32 _privilegeId)
		{
			int count = -1;
			return GetByPrivilegeId(null,_privilegeId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Privilege index.
		/// </summary>
		/// <param name="_privilegeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Privilege"/> class.</returns>
		public KaiZen.CSMS.Entities.Privilege GetByPrivilegeId(System.Int32 _privilegeId, int start, int pageLength)
		{
			int count = -1;
			return GetByPrivilegeId(null, _privilegeId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Privilege index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_privilegeId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Privilege"/> class.</returns>
		public KaiZen.CSMS.Entities.Privilege GetByPrivilegeId(TransactionManager transactionManager, System.Int32 _privilegeId)
		{
			int count = -1;
			return GetByPrivilegeId(transactionManager, _privilegeId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Privilege index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_privilegeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Privilege"/> class.</returns>
		public KaiZen.CSMS.Entities.Privilege GetByPrivilegeId(TransactionManager transactionManager, System.Int32 _privilegeId, int start, int pageLength)
		{
			int count = -1;
			return GetByPrivilegeId(transactionManager, _privilegeId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Privilege index.
		/// </summary>
		/// <param name="_privilegeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Privilege"/> class.</returns>
		public KaiZen.CSMS.Entities.Privilege GetByPrivilegeId(System.Int32 _privilegeId, int start, int pageLength, out int count)
		{
			return GetByPrivilegeId(null, _privilegeId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Privilege index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_privilegeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Privilege"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.Privilege GetByPrivilegeId(TransactionManager transactionManager, System.Int32 _privilegeId, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_Privilege index.
		/// </summary>
		/// <param name="_privilegeName"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Privilege"/> class.</returns>
		public KaiZen.CSMS.Entities.Privilege GetByPrivilegeName(System.String _privilegeName)
		{
			int count = -1;
			return GetByPrivilegeName(null,_privilegeName, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Privilege index.
		/// </summary>
		/// <param name="_privilegeName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Privilege"/> class.</returns>
		public KaiZen.CSMS.Entities.Privilege GetByPrivilegeName(System.String _privilegeName, int start, int pageLength)
		{
			int count = -1;
			return GetByPrivilegeName(null, _privilegeName, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Privilege index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_privilegeName"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Privilege"/> class.</returns>
		public KaiZen.CSMS.Entities.Privilege GetByPrivilegeName(TransactionManager transactionManager, System.String _privilegeName)
		{
			int count = -1;
			return GetByPrivilegeName(transactionManager, _privilegeName, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Privilege index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_privilegeName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Privilege"/> class.</returns>
		public KaiZen.CSMS.Entities.Privilege GetByPrivilegeName(TransactionManager transactionManager, System.String _privilegeName, int start, int pageLength)
		{
			int count = -1;
			return GetByPrivilegeName(transactionManager, _privilegeName, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Privilege index.
		/// </summary>
		/// <param name="_privilegeName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Privilege"/> class.</returns>
		public KaiZen.CSMS.Entities.Privilege GetByPrivilegeName(System.String _privilegeName, int start, int pageLength, out int count)
		{
			return GetByPrivilegeName(null, _privilegeName, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Privilege index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_privilegeName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Privilege"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.Privilege GetByPrivilegeName(TransactionManager transactionManager, System.String _privilegeName, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;Privilege&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;Privilege&gt;"/></returns>
		public static TList<Privilege> Fill(IDataReader reader, TList<Privilege> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.Privilege c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("Privilege")
					.Append("|").Append((System.Int32)reader[((int)PrivilegeColumn.PrivilegeId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<Privilege>(
					key.ToString(), // EntityTrackingKey
					"Privilege",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.Privilege();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.PrivilegeId = (System.Int32)reader[((int)PrivilegeColumn.PrivilegeId - 1)];
					c.PrivilegeName = (System.String)reader[((int)PrivilegeColumn.PrivilegeName - 1)];
					c.PrivilegeDescription = (System.String)reader[((int)PrivilegeColumn.PrivilegeDescription - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.Privilege"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.Privilege"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.Privilege entity)
		{
			if (!reader.Read()) return;
			
			entity.PrivilegeId = (System.Int32)reader[((int)PrivilegeColumn.PrivilegeId - 1)];
			entity.PrivilegeName = (System.String)reader[((int)PrivilegeColumn.PrivilegeName - 1)];
			entity.PrivilegeDescription = (System.String)reader[((int)PrivilegeColumn.PrivilegeDescription - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.Privilege"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.Privilege"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.Privilege entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.PrivilegeId = (System.Int32)dataRow["PrivilegeId"];
			entity.PrivilegeName = (System.String)dataRow["PrivilegeName"];
			entity.PrivilegeDescription = (System.String)dataRow["PrivilegeDescription"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.Privilege"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.Privilege Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.Privilege entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetByPrivilegeId methods when available
			
			#region UserPrivilegeCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<UserPrivilege>|UserPrivilegeCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'UserPrivilegeCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.UserPrivilegeCollection = DataRepository.UserPrivilegeProvider.GetByPrivilegeId(transactionManager, entity.PrivilegeId);

				if (deep && entity.UserPrivilegeCollection.Count > 0)
				{
					deepHandles.Add("UserPrivilegeCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<UserPrivilege>) DataRepository.UserPrivilegeProvider.DeepLoad,
						new object[] { transactionManager, entity.UserPrivilegeCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.Privilege object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.Privilege instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.Privilege Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.Privilege entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<UserPrivilege>
				if (CanDeepSave(entity.UserPrivilegeCollection, "List<UserPrivilege>|UserPrivilegeCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(UserPrivilege child in entity.UserPrivilegeCollection)
					{
						if(child.PrivilegeIdSource != null)
						{
							child.PrivilegeId = child.PrivilegeIdSource.PrivilegeId;
						}
						else
						{
							child.PrivilegeId = entity.PrivilegeId;
						}

					}

					if (entity.UserPrivilegeCollection.Count > 0 || entity.UserPrivilegeCollection.DeletedItems.Count > 0)
					{
						//DataRepository.UserPrivilegeProvider.Save(transactionManager, entity.UserPrivilegeCollection);
						
						deepHandles.Add("UserPrivilegeCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< UserPrivilege >) DataRepository.UserPrivilegeProvider.DeepSave,
							new object[] { transactionManager, entity.UserPrivilegeCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region PrivilegeChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.Privilege</c>
	///</summary>
	public enum PrivilegeChildEntityTypes
	{

		///<summary>
		/// Collection of <c>Privilege</c> as OneToMany for UserPrivilegeCollection
		///</summary>
		[ChildEntityType(typeof(TList<UserPrivilege>))]
		UserPrivilegeCollection,
	}
	
	#endregion PrivilegeChildEntityTypes
	
	#region PrivilegeFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;PrivilegeColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Privilege"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class PrivilegeFilterBuilder : SqlFilterBuilder<PrivilegeColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the PrivilegeFilterBuilder class.
		/// </summary>
		public PrivilegeFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the PrivilegeFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public PrivilegeFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the PrivilegeFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public PrivilegeFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion PrivilegeFilterBuilder
	
	#region PrivilegeParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;PrivilegeColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Privilege"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class PrivilegeParameterBuilder : ParameterizedSqlFilterBuilder<PrivilegeColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the PrivilegeParameterBuilder class.
		/// </summary>
		public PrivilegeParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the PrivilegeParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public PrivilegeParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the PrivilegeParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public PrivilegeParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion PrivilegeParameterBuilder
	
	#region PrivilegeSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;PrivilegeColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Privilege"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class PrivilegeSortBuilder : SqlSortBuilder<PrivilegeColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the PrivilegeSqlSortBuilder class.
		/// </summary>
		public PrivilegeSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion PrivilegeSortBuilder
	
} // end namespace
