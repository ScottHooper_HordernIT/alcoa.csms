﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.ComponentModel;
using System.Web.UI.Design.WebControls;
using System.Web.UI.Design;
using System.Web.UI.WebControls;

namespace KaiZen.CSMS.Web.UI
{
    /// <summary>
    /// A designer class for a strongly typed repeater <c>ConfigNavigationRepeater</c>
    /// </summary>
	public class ConfigNavigationRepeaterDesigner : System.Web.UI.Design.ControlDesigner
	{
	    /// <summary>
        /// Initializes a new instance of the <see cref="T:ConfigNavigationRepeaterDesigner"/> class.
        /// </summary>
		public ConfigNavigationRepeaterDesigner()
		{
		}

        /// <summary>
        /// Initializes the control designer and loads the specified component.
        /// </summary>
        /// <param name="component">The control being designed.</param>
		public override void Initialize(IComponent component)
		{
			if (!(component is ConfigNavigationRepeater))
			{ 
				throw new ArgumentException("Component is not a ConfigNavigationRepeater."); 
			} 
			base.Initialize(component); 
			base.SetViewFlags(ViewFlags.TemplateEditing, true); 
		}


		/// <summary>
		/// Generate HTML for the designer
		/// </summary>
		/// <returns>a string of design time HTML</returns>
		public override string GetDesignTimeHtml()
		{

			// Get the instance this designer applies to
			//
			ConfigNavigationRepeater z = (ConfigNavigationRepeater)Component;
			z.DataBind();

			return base.GetDesignTimeHtml();

			//return z.RenderAtDesignTime();

			//	ControlCollection c = z.Controls;
			//Totem z = (Totem) Component;
			//Totem z = (Totem) Component;
			//return ("<div style='border: 1px gray dotted; background-color: lightgray'><b>TagStat :</b> zae |  qsdds</div>");

		}
	}

    /// <summary>
    /// A strongly typed repeater control for the <see cref="ConfigNavigationRepeater"/> Type.
    /// </summary>
	[Designer(typeof(ConfigNavigationRepeaterDesigner))]
	[ParseChildren(true)]
	[ToolboxData("<{0}:ConfigNavigationRepeater runat=\"server\"></{0}:ConfigNavigationRepeater>")]
	public class ConfigNavigationRepeater : CompositeDataBoundControl, System.Web.UI.INamingContainer
	{
	    /// <summary>
        /// Initializes a new instance of the <see cref="T:ConfigNavigationRepeater"/> class.
        /// </summary>
		public ConfigNavigationRepeater()
		{
		}

		/// <summary>
        /// Gets a <see cref="T:System.Web.UI.ControlCollection"></see> object that represents the child controls for a specified server control in the UI hierarchy.
        /// </summary>
        /// <value></value>
        /// <returns>The collection of child controls for the specified server control.</returns>
		public override ControlCollection Controls
		{
			get
			{
				this.EnsureChildControls();
				return base.Controls;
			}
		}

		private ITemplate m_headerTemplate;
		/// <summary>
        /// Gets or sets the header template.
        /// </summary>
        /// <value>The header template.</value>
		[Browsable(false)]
		[TemplateContainer(typeof(ConfigNavigationItem))]
		[PersistenceMode(PersistenceMode.InnerDefaultProperty)]
		public ITemplate HeaderTemplate
		{
			get { return m_headerTemplate; }
			set { m_headerTemplate = value; }
		}

		private ITemplate m_itemTemplate;
		/// <summary>
        /// Gets or sets the item template.
        /// </summary>
        /// <value>The item template.</value>
		[Browsable(false)]
		[TemplateContainer(typeof(ConfigNavigationItem))]
		[PersistenceMode(PersistenceMode.InnerDefaultProperty)]
		public ITemplate ItemTemplate
		{
			get { return m_itemTemplate; }
			set { m_itemTemplate = value; }
		}

		private ITemplate m_seperatorTemplate;
        /// <summary>
        /// Gets or sets the Seperator Template
        /// </summary>
        [Browsable(false)]
        [TemplateContainer(typeof(ConfigNavigationItem))]
        [PersistenceMode(PersistenceMode.InnerDefaultProperty)]
        public ITemplate SeperatorTemplate
        {
            get { return m_seperatorTemplate; }
            set { m_seperatorTemplate = value; }
        }
			
		private ITemplate m_altenateItemTemplate;
        /// <summary>
        /// Gets or sets the alternating item template.
        /// </summary>
        /// <value>The alternating item template.</value>
		[Browsable(false)]
		[TemplateContainer(typeof(ConfigNavigationItem))]
		[PersistenceMode(PersistenceMode.InnerDefaultProperty)]
		public ITemplate AlternatingItemTemplate
		{
			get { return m_altenateItemTemplate; }
			set { m_altenateItemTemplate = value; }
		}

		private ITemplate m_footerTemplate;
        /// <summary>
        /// Gets or sets the footer template.
        /// </summary>
        /// <value>The footer template.</value>
		[Browsable(false)]
		[TemplateContainer(typeof(ConfigNavigationItem))]
		[PersistenceMode(PersistenceMode.InnerDefaultProperty)]
		public ITemplate FooterTemplate
		{
			get { return m_footerTemplate; }
			set { m_footerTemplate = value; }
		}

//      /// <summary>
//      /// Called by the ASP.NET page framework to notify server controls that use composition-based implementation to create any child controls they contain in preparation for posting back or rendering.
//      /// </summary>
//		protected override void CreateChildControls()
//      {
//         if (ChildControlsCreated)
//         {
//            return;
//         }

//         Controls.Clear();

//         //Instantiate the Header template (if exists)
//         if (m_headerTemplate != null)
//         {
//            Control headerItem = new Control();
//            m_headerTemplate.InstantiateIn(headerItem);
//            Controls.Add(headerItem);
//         }

//         //Instantiate the Footer template (if exists)
//         if (m_footerTemplate != null)
//         {
//            Control footerItem = new Control();
//            m_footerTemplate.InstantiateIn(footerItem);
//            Controls.Add(footerItem);
//         }
//
//         ChildControlsCreated = true;
//      }
	
		/// <summary>
        /// Overridden and Empty so that span tags are not written
        /// </summary>
        /// <param name="writer"></param>
        public override void RenderBeginTag(HtmlTextWriter writer)
        {
            
        }

        /// <summary>
        /// Overridden and Empty so that span tags are not written
        /// </summary>
        /// <param name="writer"></param>
        public override void RenderEndTag(HtmlTextWriter writer)
        {
                
        }		
		
		/// <summary>
      	/// Called by the ASP.NET page framework to notify server controls that use composition-based implementation to create any child controls they contain in preparation for posting back or rendering.
      	/// </summary>
		protected override int CreateChildControls(System.Collections.IEnumerable dataSource, bool dataBinding)
      	{
         int pos = 0;

         if (dataBinding)
         {
            //Instantiate the Header template (if exists)
            if (m_headerTemplate != null)
            {
                Control headerItem = new Control();
                m_headerTemplate.InstantiateIn(headerItem);
                Controls.Add(headerItem);
            }
			if (dataSource != null)
			{
				foreach (object o in dataSource)
				{
						KaiZen.CSMS.Entities.ConfigNavigation entity = o as KaiZen.CSMS.Entities.ConfigNavigation;
						ConfigNavigationItem container = new ConfigNavigationItem(entity);
	
						if (m_itemTemplate != null && (pos % 2) == 0)
						{
							m_itemTemplate.InstantiateIn(container);
							
							if (m_seperatorTemplate != null)
							{
								m_seperatorTemplate.InstantiateIn(container);
							}
						}
						else
						{
							if (m_altenateItemTemplate != null)
							{
								m_altenateItemTemplate.InstantiateIn(container);
								
								if (m_seperatorTemplate != null)
								{
									m_seperatorTemplate.InstantiateIn(container);
								}
								
							}
							else if (m_itemTemplate != null)
							{
								m_itemTemplate.InstantiateIn(container);
								
								if (m_seperatorTemplate != null)
								{
									m_seperatorTemplate.InstantiateIn(container);
								}
							}
							else
							{
								// no template !!!
							}
						}
						Controls.Add(container);
						
						container.DataBind();
						
						pos++;
				}
			}
            //Instantiate the Footer template (if exists)
            if (m_footerTemplate != null)
            {
                Control footerItem = new Control();
                m_footerTemplate.InstantiateIn(footerItem);
                Controls.Add(footerItem);
            }

		}
			
			return pos;
		}

        /// <summary>
        /// Raises the <see cref="E:System.Web.UI.Control.PreRender"></see> event.
        /// </summary>
        /// <param name="e">An <see cref="T:System.EventArgs"></see> object that contains the event data.</param>
		protected override void OnPreRender(EventArgs e)
		{
			base.DataBind();
		}

		#region Design time
        /// <summary>
        /// Renders at design time.
        /// </summary>
        /// <returns>a  string of the Designed HTML</returns>
		internal string RenderAtDesignTime()
		{			
			return "Designer currently not implemented"; 
		}

		#endregion
	}

    /// <summary>
    /// A wrapper type for the entity
    /// </summary>
	[System.ComponentModel.ToolboxItem(false)]
	public class ConfigNavigationItem : System.Web.UI.Control, System.Web.UI.INamingContainer
	{
		private KaiZen.CSMS.Entities.ConfigNavigation _entity;

        /// <summary>
        /// Initializes a new instance of the <see cref="T:ConfigNavigationItem"/> class.
        /// </summary>
		public ConfigNavigationItem()
			: base()
		{ }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:ConfigNavigationItem"/> class.
        /// </summary>
		public ConfigNavigationItem(KaiZen.CSMS.Entities.ConfigNavigation entity)
			: base()
		{
			_entity = entity;
		}
		
        /// <summary>
        /// Gets the ConfigNavigationId
        /// </summary>
        /// <value>The ConfigNavigationId.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32 ConfigNavigationId
		{
			get { return _entity.ConfigNavigationId; }
		}
        /// <summary>
        /// Gets the ParentId
        /// </summary>
        /// <value>The ParentId.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32 ParentId
		{
			get { return _entity.ParentId; }
		}
        /// <summary>
        /// Gets the Text
        /// </summary>
        /// <value>The Text.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Text
		{
			get { return _entity.Text; }
		}
        /// <summary>
        /// Gets the TextContractor
        /// </summary>
        /// <value>The TextContractor.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String TextContractor
		{
			get { return _entity.TextContractor; }
		}
        /// <summary>
        /// Gets the NavigateUrl
        /// </summary>
        /// <value>The NavigateUrl.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String NavigateUrl
		{
			get { return _entity.NavigateUrl; }
		}
        /// <summary>
        /// Gets the NavigateUrlContractor
        /// </summary>
        /// <value>The NavigateUrlContractor.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String NavigateUrlContractor
		{
			get { return _entity.NavigateUrlContractor; }
		}
        /// <summary>
        /// Gets the VisibleBy
        /// </summary>
        /// <value>The VisibleBy.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String VisibleBy
		{
			get { return _entity.VisibleBy; }
		}
        /// <summary>
        /// Gets the DisplayOrderId
        /// </summary>
        /// <value>The DisplayOrderId.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? DisplayOrderId
		{
			get { return _entity.DisplayOrderId; }
		}

        /// <summary>
        /// Gets a <see cref="T:KaiZen.CSMS.Entities.ConfigNavigation"></see> object
        /// </summary>
        /// <value></value>
        [System.ComponentModel.Bindable(true)]
        public KaiZen.CSMS.Entities.ConfigNavigation Entity
        {
            get { return _entity; }
        }
	}
}
