﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="QuestionnaireInitialContactAuditProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class QuestionnaireInitialContactAuditProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.QuestionnaireInitialContactAudit, KaiZen.CSMS.Entities.QuestionnaireInitialContactAuditKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.QuestionnaireInitialContactAuditKey key)
		{
			return Delete(transactionManager, key.AuditId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_auditId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _auditId)
		{
			return Delete(null, _auditId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_auditId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _auditId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.QuestionnaireInitialContactAudit Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.QuestionnaireInitialContactAuditKey key, int start, int pageLength)
		{
			return GetByAuditId(transactionManager, key.AuditId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_QuestionnaireInitialContactAudit index.
		/// </summary>
		/// <param name="_auditId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireInitialContactAudit"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireInitialContactAudit GetByAuditId(System.Int32 _auditId)
		{
			int count = -1;
			return GetByAuditId(null,_auditId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireInitialContactAudit index.
		/// </summary>
		/// <param name="_auditId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireInitialContactAudit"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireInitialContactAudit GetByAuditId(System.Int32 _auditId, int start, int pageLength)
		{
			int count = -1;
			return GetByAuditId(null, _auditId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireInitialContactAudit index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_auditId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireInitialContactAudit"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireInitialContactAudit GetByAuditId(TransactionManager transactionManager, System.Int32 _auditId)
		{
			int count = -1;
			return GetByAuditId(transactionManager, _auditId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireInitialContactAudit index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_auditId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireInitialContactAudit"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireInitialContactAudit GetByAuditId(TransactionManager transactionManager, System.Int32 _auditId, int start, int pageLength)
		{
			int count = -1;
			return GetByAuditId(transactionManager, _auditId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireInitialContactAudit index.
		/// </summary>
		/// <param name="_auditId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireInitialContactAudit"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireInitialContactAudit GetByAuditId(System.Int32 _auditId, int start, int pageLength, out int count)
		{
			return GetByAuditId(null, _auditId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireInitialContactAudit index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_auditId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireInitialContactAudit"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.QuestionnaireInitialContactAudit GetByAuditId(TransactionManager transactionManager, System.Int32 _auditId, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;QuestionnaireInitialContactAudit&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;QuestionnaireInitialContactAudit&gt;"/></returns>
		public static TList<QuestionnaireInitialContactAudit> Fill(IDataReader reader, TList<QuestionnaireInitialContactAudit> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.QuestionnaireInitialContactAudit c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("QuestionnaireInitialContactAudit")
					.Append("|").Append((System.Int32)reader[((int)QuestionnaireInitialContactAuditColumn.AuditId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<QuestionnaireInitialContactAudit>(
					key.ToString(), // EntityTrackingKey
					"QuestionnaireInitialContactAudit",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.QuestionnaireInitialContactAudit();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.AuditId = (System.Int32)reader[((int)QuestionnaireInitialContactAuditColumn.AuditId - 1)];
					c.AuditedOn = (System.DateTime)reader[((int)QuestionnaireInitialContactAuditColumn.AuditedOn - 1)];
					c.AuditEventId = (System.String)reader[((int)QuestionnaireInitialContactAuditColumn.AuditEventId - 1)];
					c.QuestionnaireInitialContactId = (reader.IsDBNull(((int)QuestionnaireInitialContactAuditColumn.QuestionnaireInitialContactId - 1)))?null:(System.Int32?)reader[((int)QuestionnaireInitialContactAuditColumn.QuestionnaireInitialContactId - 1)];
					c.QuestionnaireId = (reader.IsDBNull(((int)QuestionnaireInitialContactAuditColumn.QuestionnaireId - 1)))?null:(System.Int32?)reader[((int)QuestionnaireInitialContactAuditColumn.QuestionnaireId - 1)];
					c.ContactFirstName = (reader.IsDBNull(((int)QuestionnaireInitialContactAuditColumn.ContactFirstName - 1)))?null:(System.String)reader[((int)QuestionnaireInitialContactAuditColumn.ContactFirstName - 1)];
					c.ContactLastName = (reader.IsDBNull(((int)QuestionnaireInitialContactAuditColumn.ContactLastName - 1)))?null:(System.String)reader[((int)QuestionnaireInitialContactAuditColumn.ContactLastName - 1)];
					c.ContactEmail = (reader.IsDBNull(((int)QuestionnaireInitialContactAuditColumn.ContactEmail - 1)))?null:(System.String)reader[((int)QuestionnaireInitialContactAuditColumn.ContactEmail - 1)];
					c.ContactTitle = (reader.IsDBNull(((int)QuestionnaireInitialContactAuditColumn.ContactTitle - 1)))?null:(System.String)reader[((int)QuestionnaireInitialContactAuditColumn.ContactTitle - 1)];
					c.ContactPhone = (reader.IsDBNull(((int)QuestionnaireInitialContactAuditColumn.ContactPhone - 1)))?null:(System.String)reader[((int)QuestionnaireInitialContactAuditColumn.ContactPhone - 1)];
					c.ContactStatusId = (reader.IsDBNull(((int)QuestionnaireInitialContactAuditColumn.ContactStatusId - 1)))?null:(System.Int32?)reader[((int)QuestionnaireInitialContactAuditColumn.ContactStatusId - 1)];
					c.ModifiedDate = (reader.IsDBNull(((int)QuestionnaireInitialContactAuditColumn.ModifiedDate - 1)))?null:(System.DateTime?)reader[((int)QuestionnaireInitialContactAuditColumn.ModifiedDate - 1)];
					c.ModifiedByUserId = (reader.IsDBNull(((int)QuestionnaireInitialContactAuditColumn.ModifiedByUserId - 1)))?null:(System.Int32?)reader[((int)QuestionnaireInitialContactAuditColumn.ModifiedByUserId - 1)];
					c.CreatedByUserId = (reader.IsDBNull(((int)QuestionnaireInitialContactAuditColumn.CreatedByUserId - 1)))?null:(System.Int32?)reader[((int)QuestionnaireInitialContactAuditColumn.CreatedByUserId - 1)];
					c.VerifiedDate = (reader.IsDBNull(((int)QuestionnaireInitialContactAuditColumn.VerifiedDate - 1)))?null:(System.DateTime?)reader[((int)QuestionnaireInitialContactAuditColumn.VerifiedDate - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.QuestionnaireInitialContactAudit"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.QuestionnaireInitialContactAudit"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.QuestionnaireInitialContactAudit entity)
		{
			if (!reader.Read()) return;
			
			entity.AuditId = (System.Int32)reader[((int)QuestionnaireInitialContactAuditColumn.AuditId - 1)];
			entity.AuditedOn = (System.DateTime)reader[((int)QuestionnaireInitialContactAuditColumn.AuditedOn - 1)];
			entity.AuditEventId = (System.String)reader[((int)QuestionnaireInitialContactAuditColumn.AuditEventId - 1)];
			entity.QuestionnaireInitialContactId = (reader.IsDBNull(((int)QuestionnaireInitialContactAuditColumn.QuestionnaireInitialContactId - 1)))?null:(System.Int32?)reader[((int)QuestionnaireInitialContactAuditColumn.QuestionnaireInitialContactId - 1)];
			entity.QuestionnaireId = (reader.IsDBNull(((int)QuestionnaireInitialContactAuditColumn.QuestionnaireId - 1)))?null:(System.Int32?)reader[((int)QuestionnaireInitialContactAuditColumn.QuestionnaireId - 1)];
			entity.ContactFirstName = (reader.IsDBNull(((int)QuestionnaireInitialContactAuditColumn.ContactFirstName - 1)))?null:(System.String)reader[((int)QuestionnaireInitialContactAuditColumn.ContactFirstName - 1)];
			entity.ContactLastName = (reader.IsDBNull(((int)QuestionnaireInitialContactAuditColumn.ContactLastName - 1)))?null:(System.String)reader[((int)QuestionnaireInitialContactAuditColumn.ContactLastName - 1)];
			entity.ContactEmail = (reader.IsDBNull(((int)QuestionnaireInitialContactAuditColumn.ContactEmail - 1)))?null:(System.String)reader[((int)QuestionnaireInitialContactAuditColumn.ContactEmail - 1)];
			entity.ContactTitle = (reader.IsDBNull(((int)QuestionnaireInitialContactAuditColumn.ContactTitle - 1)))?null:(System.String)reader[((int)QuestionnaireInitialContactAuditColumn.ContactTitle - 1)];
			entity.ContactPhone = (reader.IsDBNull(((int)QuestionnaireInitialContactAuditColumn.ContactPhone - 1)))?null:(System.String)reader[((int)QuestionnaireInitialContactAuditColumn.ContactPhone - 1)];
			entity.ContactStatusId = (reader.IsDBNull(((int)QuestionnaireInitialContactAuditColumn.ContactStatusId - 1)))?null:(System.Int32?)reader[((int)QuestionnaireInitialContactAuditColumn.ContactStatusId - 1)];
			entity.ModifiedDate = (reader.IsDBNull(((int)QuestionnaireInitialContactAuditColumn.ModifiedDate - 1)))?null:(System.DateTime?)reader[((int)QuestionnaireInitialContactAuditColumn.ModifiedDate - 1)];
			entity.ModifiedByUserId = (reader.IsDBNull(((int)QuestionnaireInitialContactAuditColumn.ModifiedByUserId - 1)))?null:(System.Int32?)reader[((int)QuestionnaireInitialContactAuditColumn.ModifiedByUserId - 1)];
			entity.CreatedByUserId = (reader.IsDBNull(((int)QuestionnaireInitialContactAuditColumn.CreatedByUserId - 1)))?null:(System.Int32?)reader[((int)QuestionnaireInitialContactAuditColumn.CreatedByUserId - 1)];
			entity.VerifiedDate = (reader.IsDBNull(((int)QuestionnaireInitialContactAuditColumn.VerifiedDate - 1)))?null:(System.DateTime?)reader[((int)QuestionnaireInitialContactAuditColumn.VerifiedDate - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.QuestionnaireInitialContactAudit"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.QuestionnaireInitialContactAudit"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.QuestionnaireInitialContactAudit entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.AuditId = (System.Int32)dataRow["AuditId"];
			entity.AuditedOn = (System.DateTime)dataRow["AuditedOn"];
			entity.AuditEventId = (System.String)dataRow["AuditEventId"];
			entity.QuestionnaireInitialContactId = Convert.IsDBNull(dataRow["QuestionnaireInitialContactId"]) ? null : (System.Int32?)dataRow["QuestionnaireInitialContactId"];
			entity.QuestionnaireId = Convert.IsDBNull(dataRow["QuestionnaireId"]) ? null : (System.Int32?)dataRow["QuestionnaireId"];
			entity.ContactFirstName = Convert.IsDBNull(dataRow["ContactFirstName"]) ? null : (System.String)dataRow["ContactFirstName"];
			entity.ContactLastName = Convert.IsDBNull(dataRow["ContactLastName"]) ? null : (System.String)dataRow["ContactLastName"];
			entity.ContactEmail = Convert.IsDBNull(dataRow["ContactEmail"]) ? null : (System.String)dataRow["ContactEmail"];
			entity.ContactTitle = Convert.IsDBNull(dataRow["ContactTitle"]) ? null : (System.String)dataRow["ContactTitle"];
			entity.ContactPhone = Convert.IsDBNull(dataRow["ContactPhone"]) ? null : (System.String)dataRow["ContactPhone"];
			entity.ContactStatusId = Convert.IsDBNull(dataRow["ContactStatusId"]) ? null : (System.Int32?)dataRow["ContactStatusId"];
			entity.ModifiedDate = Convert.IsDBNull(dataRow["ModifiedDate"]) ? null : (System.DateTime?)dataRow["ModifiedDate"];
			entity.ModifiedByUserId = Convert.IsDBNull(dataRow["ModifiedByUserId"]) ? null : (System.Int32?)dataRow["ModifiedByUserId"];
			entity.CreatedByUserId = Convert.IsDBNull(dataRow["CreatedByUserId"]) ? null : (System.Int32?)dataRow["CreatedByUserId"];
			entity.VerifiedDate = Convert.IsDBNull(dataRow["VerifiedDate"]) ? null : (System.DateTime?)dataRow["VerifiedDate"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.QuestionnaireInitialContactAudit"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.QuestionnaireInitialContactAudit Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.QuestionnaireInitialContactAudit entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.QuestionnaireInitialContactAudit object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.QuestionnaireInitialContactAudit instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.QuestionnaireInitialContactAudit Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.QuestionnaireInitialContactAudit entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region QuestionnaireInitialContactAuditChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.QuestionnaireInitialContactAudit</c>
	///</summary>
	public enum QuestionnaireInitialContactAuditChildEntityTypes
	{
	}
	
	#endregion QuestionnaireInitialContactAuditChildEntityTypes
	
	#region QuestionnaireInitialContactAuditFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;QuestionnaireInitialContactAuditColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireInitialContactAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireInitialContactAuditFilterBuilder : SqlFilterBuilder<QuestionnaireInitialContactAuditColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialContactAuditFilterBuilder class.
		/// </summary>
		public QuestionnaireInitialContactAuditFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialContactAuditFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireInitialContactAuditFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialContactAuditFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireInitialContactAuditFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireInitialContactAuditFilterBuilder
	
	#region QuestionnaireInitialContactAuditParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;QuestionnaireInitialContactAuditColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireInitialContactAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireInitialContactAuditParameterBuilder : ParameterizedSqlFilterBuilder<QuestionnaireInitialContactAuditColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialContactAuditParameterBuilder class.
		/// </summary>
		public QuestionnaireInitialContactAuditParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialContactAuditParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireInitialContactAuditParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialContactAuditParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireInitialContactAuditParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireInitialContactAuditParameterBuilder
	
	#region QuestionnaireInitialContactAuditSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;QuestionnaireInitialContactAuditColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireInitialContactAudit"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class QuestionnaireInitialContactAuditSortBuilder : SqlSortBuilder<QuestionnaireInitialContactAuditColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialContactAuditSqlSortBuilder class.
		/// </summary>
		public QuestionnaireInitialContactAuditSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion QuestionnaireInitialContactAuditSortBuilder
	
} // end namespace
