﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Adminv2_UserControls_MedicalTS" Codebehind="MedicalTS.ascx.cs" %>

<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register assembly="DevExpress.Web.v14.1" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<dxwgv:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False"
    DataSourceID="FileDbDS" KeyFieldName="FileId" 
    CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
    CssPostfix="Office2003Blue">
    <Columns>
        <dxwgv:GridViewCommandColumn VisibleIndex="0">
            <EditButton Visible="True">
            </EditButton>
            <DeleteButton Visible="True">
            </DeleteButton>
            <ClearFilterButton Visible="True">
            </ClearFilterButton>
        </dxwgv:GridViewCommandColumn>
        <dxwgv:GridViewDataTextColumn FieldName="FileId" ReadOnly="True" Visible="False">
            <EditFormSettings Visible="False" />
        </dxwgv:GridViewDataTextColumn>
        
        
        <dxwgv:GridViewDataComboBoxColumn Caption="Schedule" FieldName="Type" VisibleIndex="1" SortIndex="0" SortOrder="Ascending" GroupIndex="0">
                        <PropertiesComboBox ValueType="System.String">
                            <Items>
                                <dxe:ListEditItem Text="Medical" Value="MS">
                                </dxe:ListEditItem>
                                <dxe:ListEditItem Text="Training" Value="TS">
                                </dxe:ListEditItem>
                            </Items>
                        </PropertiesComboBox>
            <Settings SortMode="DisplayText" />
                    </dxwgv:GridViewDataComboBoxColumn>
        
        <%--<dxwgv:GridViewDataComboBoxColumn Caption="Company" FieldName="CompanyId" Name="CompanyBox"
                        VisibleIndex="2">
                        <PropertiesComboBox DataSourceID="CompaniesDataSource" DropDownHeight="150px" TextField="CompanyName"
                          ValueField="CompanyId">
                        </PropertiesComboBox>
                        <EditFormSettings Visible="False" />
            <Settings SortMode="DisplayText" />
                    </dxwgv:GridViewDataComboBoxColumn>--%>
                    <dx:GridViewDataComboBoxColumn Caption="Company" FieldName="CompanyId" 
                                             VisibleIndex="2" Name="CompanyBox">
                                            <PropertiesComboBox DataSourceID="CompaniesDataSource1"  
                                                TextField="CompanyName" ValueField="CompanyId">
                                            </PropertiesComboBox>
                                            <Settings SortMode="DisplayText" FilterMode="Value"/>
                                           <EditFormSettings Visible="False" />
                                        </dx:GridViewDataComboBoxColumn>

                    
                    <dxwgv:GridViewDataHyperLinkColumn Caption="File Name" FieldName="FileId" VisibleIndex="5">
                            <PropertiesHyperLinkEdit NavigateUrlFormatString="../../Common/GetAttachment.aspx?ID={0}&amp;Medical=1" TextField="FileName">
                                </PropertiesHyperLinkEdit>
                                <CellStyle ForeColor="Orange" HorizontalAlign="Left">
                                </CellStyle>
                                <EditFormSettings Visible="False" />
                                <Settings FilterMode="DisplayText" />
                            </dxwgv:GridViewDataHyperLinkColumn>
                            

        <%--<dxwgv:GridViewDataTextColumn FieldName="Description" VisibleIndex="3">
        </dxwgv:GridViewDataTextColumn>--%>
         <dx:GridViewDataComboBoxColumn FieldName="Description" VisibleIndex="3" 
                                                >
                                                <PropertiesComboBox DataSourceID="getDistinctDescription" TextField="Description" IncrementalFilteringMode="StartsWith">
                                                </PropertiesComboBox>
                                            </dx:GridViewDataComboBoxColumn>


        <dxwgv:GridViewDataDateColumn FieldName="ModifiedDate" ReadOnly="True" VisibleIndex="4">
            <EditFormSettings Visible="False" />
        </dxwgv:GridViewDataDateColumn>
    </Columns>
    <%--<SettingsPager Mode="ShowAllRecords">
    </SettingsPager>--%>
    <SettingsPager Visible="true" PageSize="20" Mode="ShowPager">
                    <AllButton Visible="true">
                    </AllButton>
                </SettingsPager>
    <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
        <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
        </LoadingPanelOnStatusBar>
        <CollapsedButton Height="12px"
            Width="11px" />
        <ExpandedButton Height="12px"
            Width="11px" />
        <DetailCollapsedButton Height="12px"
            Width="11px" />
        <DetailExpandedButton Height="12px"
            Width="11px" />
        <LoadingPanel Url="~/App_Themes/Office2003BlueOld/GridView/Loading.gif"><%--Enhancement_023:change by debashis for testing platform upgradation--%>
        </LoadingPanel>
    </Images>
    <Settings ShowFilterRow="True" ShowGroupPanel="True" ShowGroupedColumns="True" ShowHeaderFilterButton="true" />
    
    <SettingsEditing Mode="EditFormAndDisplayRow" />
    <ImagesFilterControl>
        <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
        </LoadingPanel>
    </ImagesFilterControl>
    <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
        CssPostfix="Office2003Blue">
        <Header ImageSpacing="5px" SortingImageSpacing="5px">
        </Header>
        <LoadingPanel ImageSpacing="10px">
        </LoadingPanel>
    </Styles>
    <SettingsBehavior ColumnResizeMode="NextColumn" ConfirmDelete="True"></SettingsBehavior>
</dxwgv:ASPxGridView>
    
        <data:FileDbMedicalTrainingDataSource
            ID="FileDbMedicalTrainingDataSource" runat="server"
            EnableDeepLoad="False" EnableSorting="true"
            SelectMethod="GetAll">
        </data:FileDbMedicalTrainingDataSource>
        
        <data:CompaniesDataSource
                    ID="CompaniesDataSource"
                    runat="server"
                    SelectMethod="GetAll"
                    EnableSorting="true"
                    EnableDeepLoad="False" Sort="CompanyName ASC">
                </data:CompaniesDataSource>
                <asp:sqlDataSource ID="getDistinctDescription" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="_GetDistinctDescriptionFileDBMedicalTraining" SelectCommandType="StoredProcedure">
</asp:sqlDataSource>
<asp:sqlDataSource ID="FileDbDS" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="FileDbMedicalTraining_GetAll" SelectCommandType="StoredProcedure"
     UpdateCommand="FileDbMedicalTraining_UpdateGrid" UpdateCommandType="StoredProcedure" 
     DeleteCommand="FileDbMedicalTraining_Delete" DeleteCommandType="StoredProcedure">
     <DeleteParameters>
     <asp:Parameter Name="FileId" Type="Int32" />
     </DeleteParameters>
     <UpdateParameters>
     <asp:Parameter Name="FileId" Type="Int32" />
     <asp:Parameter Name="Description" Type="String" />
     <asp:Parameter Name="Type" Type="String" />
     </UpdateParameters>
     
</asp:sqlDataSource>
<data:CompaniesDataSource ID="CompaniesDataSource1" runat="server" Sort="CompanyName ASC"></data:CompaniesDataSource>