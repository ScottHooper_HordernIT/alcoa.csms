using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;
using System.Globalization;
using System.Text;
using System.Security.Cryptography;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Services;
using KaiZen.CSMS.Data;
using KaiZen.Library;

using DevExpress.Web.ASPxEditors;

public partial class UserControls_SafetyPQ_QuestionnaireCompareAssess : System.Web.UI.UserControl
{
    //string lightGreen = "#80FF80";
    //string lightRed = "#FF8080";
    //string lightYellow = "#FFFFCC";

    string PreviousAnswer = "";
    string CurrentAnswer = "";
    Auth auth = new Auth();

    protected void Page_Load(object sender, EventArgs e) {  Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); }

    private void moduleInit(bool postBack)
    {
        if (!postBack)
        {
            if (Request.QueryString["q"] != null)
            {
                ASPxPageControl1.TabPages[0].Enabled = false;
                ASPxPageControl1.TabPages[1].Enabled = false;
                ASPxPageControl1.TabPages[2].Enabled = false;
                ASPxPageControl1.TabPages[3].Enabled = false;
                btnSave.Enabled = false;
                compare(Convert.ToInt32(Request.QueryString["q"].ToString())); //http://www.mathertel.de/Diff/default.aspx (Not Used Anymore)
                LinkButton1.PostBackUrl = String.Format("~/SafetyPQ_QuestionnaireModify.aspx{0}", QueryStringModule.Encrypt("q=" + Request.QueryString["q"].ToString()));

                try
                {
                    if (Request.QueryString["saved"] != null)
                    {
                        if (Request.QueryString["saved"] == "1")
                        {
                            lblError.Text += " Saved: " + DateTime.Now;
                        }
                    }
                }
                catch (Exception ex)
                { Elmah.ErrorSignal.FromContext(Context).Raise(ex); }
            }
            else
            {
                errorOccured("Error: No Questionnaire Specified");
            }
        }
        switch (auth.RoleId)
        {
            case ((int)RoleList.Reader):
                break;
            case ((int)RoleList.Administrator):
                break;
            default:
                Response.Redirect(String.Format("AccessDenied.aspx{0}",
                                    QueryStringModule.Encrypt("error=" + Server.UrlEncode("Unrecognised User."))));
                break;
        }

        if (auth.RoleId == (int)RoleList.Administrator || Helper.General.isEhsConsultant(auth.UserId))
        {
            //lols.
        }
        else
        {
            lblError.Text = "Read Only";
            ApprovalSupplierHide();
            ApprovalVerificationHide();
            btnSave.Enabled = false;
        }
    }

    protected void errorOccured(string error)
    {
        //ASPxButton1.Visible = false;
        btnPrint.Visible = false;
        ASPxPageControl1.Visible = false;
        lblError.Text = error;
    }

    protected void compare(int qNo)
    {
        try
        {
            QuestionnaireService qService = new QuestionnaireService();
            CompaniesService cService = new CompaniesService();
            UsersService uService = new UsersService();

            Questionnaire qNew = qService.GetByQuestionnaireId(qNo);
            if (qNew == null) throw new Exception("No Questionnaire Found.");
            //find previous questionnaire to compare against
            int count = 0;
            QuestionnaireFilters qFilters = new QuestionnaireFilters();
            qFilters.Append(QuestionnaireColumn.CompanyId, qNew.CompanyId.ToString());
            qFilters.Append(QuestionnaireColumn.Status, ((int)QuestionnaireStatusList.AssessmentComplete).ToString());
            qFilters.AppendLessThan(QuestionnaireColumn.CreatedDate, qNew.CreatedDate.ToString("yyyy-MM-dd"));
            TList<Questionnaire> qList = DataRepository.QuestionnaireProvider.GetPaged(qFilters.ToString(), "CreatedDate DESC", 0, 100, out count);
            if (count > 0)
            {
                Questionnaire qOld = (Questionnaire)qList[0];

                /*
                 * Questionnaire
                 */

                //-Company Name
                Companies c = cService.GetByCompanyId(qNew.CompanyId);
                compare(lblCompany_Previous, lblCompany_Current, c.CompanyName, c.CompanyName);
                lblCompanyName.Text = c.CompanyName;
                if (qNew.Status != (int)QuestionnaireStatusList.BeingAssessed)
                {
                    lblError.Text = "Read Only (Approval & Approval Comments hidden as current questionnaire is not 'Being Assessed')";
                    btnSave.Enabled = false;
                    ApprovalSupplierHide();
                    ApprovalVerificationHide();
                }
                if (qNew.Status == (int)QuestionnaireStatusList.BeingAssessed)
                {
                    lblError.Text = "Being Assessed";
                    btnSave.Enabled = true;
                }

                //-Created By
                Users uOld = uService.GetByUserId(qOld.CreatedByUserId);
                Users uNew = uService.GetByUserId(qNew.CreatedByUserId);
                Companies cOld = cService.GetByCompanyId(uOld.CompanyId);
                Companies cNew = cService.GetByCompanyId(uNew.CompanyId);
                if (uOld != null && cOld != null) PreviousAnswer = String.Format("{0}, {1} ({2}) at {3}", uOld.LastName, uOld.FirstName, cOld.CompanyName, qOld.CreatedDate);
                if (uNew != null && cNew != null) CurrentAnswer = String.Format("{0}, {1} ({2}) at {3}", uNew.LastName, uNew.FirstName, cNew.CompanyName, qNew.CreatedDate);
                compare(lblCreatedBy_Previous, lblCreatedBy_Current);

                //-Modified By
                uOld = null;
                uNew = null;
                uOld = uService.GetByUserId(qOld.ModifiedByUserId);
                uNew = uService.GetByUserId(qNew.ModifiedByUserId);
                cOld = cService.GetByCompanyId(uOld.CompanyId);
                cNew = cService.GetByCompanyId(uNew.CompanyId);
                if (uOld != null && cOld != null) PreviousAnswer = String.Format("{0}, {1} ({2}) at {3}", uOld.LastName, uOld.FirstName, cOld.CompanyName, qOld.ModifiedDate);
                if (uNew != null && cNew != null) CurrentAnswer = String.Format("{0}, {1} ({2}) at {3}", uNew.LastName, uNew.FirstName, cNew.CompanyName, qNew.ModifiedDate);
                compare(lblModifiedBy_Previous, lblModifiedBy_Current);
                uOld = null;
                uNew = null;
                cOld = null;
                cNew = null;

                //-Assessed By
                if (qOld.MainAssessmentByUserId != null)
                {
                    uOld = uService.GetByUserId(Convert.ToInt32(qOld.MainAssessmentByUserId));
                    if (uOld != null) cOld = cService.GetByCompanyId(uOld.CompanyId);
                }
                if (qNew.MainAssessmentByUserId != null)
                {
                    uNew = uService.GetByUserId(Convert.ToInt32(qNew.MainAssessmentByUserId));
                    if (uNew != null) cNew = cService.GetByCompanyId(uNew.CompanyId);
                }
                if (cOld != null && qOld.MainAssessmentDate != null) PreviousAnswer = String.Format("{0}, {1} ({2}) at {3}", uOld.LastName, uOld.FirstName, cOld.CompanyName, qOld.MainAssessmentDate);
                if (cNew != null && qNew.MainAssessmentDate != null) CurrentAnswer = String.Format("{0}, {1} ({2}) at {3}", uNew.LastName, uNew.FirstName, cNew.CompanyName, qNew.MainAssessmentDate);
                compare(lblAssessedBy_Previous, lblAssessedBy_Current);

                ////-Questionnaire Status
                QuestionnaireStatusService qsService = new QuestionnaireStatusService();
                QuestionnaireStatus qsOld = qsService.GetByQuestionnaireStatusId(qOld.Status);
                QuestionnaireStatus qsNew = qsService.GetByQuestionnaireStatusId(qNew.Status);
                compare(lblStatus_Previous, lblStatus_Current, qsOld.QuestionnaireStatusDesc, qsNew.QuestionnaireStatusDesc);

                //-Procurement Risk Rating
                compare(lblProcurementRiskRating_Previous, lblProcurementRiskRating_Current, qOld.InitialRiskAssessment, qNew.InitialRiskAssessment);
                //-33.055.1 Risk Rating
                compare(lbl_330551RiskRating_Previous, lbl_330551RiskRating_Current, qOld.MainAssessmentRiskRating, qNew.MainAssessmentRiskRating);
                //-Recommended
                if (qOld.Recommended == true) PreviousAnswer = "Yes";
                if (qOld.Recommended == false) PreviousAnswer = "No";
                if (qNew.Recommended == true) CurrentAnswer = "Yes";
                if (qNew.Recommended == false) CurrentAnswer = "No";
                compare(lblRecommended_Previous, lblRecommended_Current, PreviousAnswer, CurrentAnswer);
                //-Recommendation Comments
                compare(lblRecommendationComments_Previous, lblRecommendationComments_Current, qOld.RecommendedComments, qNew.RecommendedComments);
                //-Questionnaire Type
                PreviousAnswer = "Pre-Qualification";
                CurrentAnswer = "Pre-Qualification";
                if (qOld.IsReQualification == true) PreviousAnswer = "Re-Qualification";
                if (qNew.IsReQualification == true) CurrentAnswer = "Re-Qualification";
                compare(lblType_Previous, lblType_Current, PreviousAnswer, CurrentAnswer);

                ASPxPageControl1.TabPages[0].Enabled = true;

                /*
                 * Procurement
                 */
                compare(lbl1FirstName_Previous, lbl1FirstName_Current, getProcurementValue(qOld.QuestionnaireId, "1_1"), getProcurementValue(qNew.QuestionnaireId, "1_1"));
                compare(lbl1LastName_Previous, lbl1LastName_Current, getProcurementValue(qOld.QuestionnaireId, "1_2"), getProcurementValue(qNew.QuestionnaireId, "1_2"));
                compare(lbl1JobTitle_Previous, lbl1JobTitle_Current, getProcurementValue(qOld.QuestionnaireId, "1_3"), getProcurementValue(qNew.QuestionnaireId, "1_3"));
                compare(lbl1Telephone_Previous, lbl1Telephone_Current, getProcurementValue(qOld.QuestionnaireId, "1_4"), getProcurementValue(qNew.QuestionnaireId, "1_4"));
                compare(lbl2EhsConsultant_Previous, lbl2EhsConsultant_Current, EhsConsultantFullName_ByCompanyId(qOld.CompanyId), EhsConsultantFullName_ByCompanyId(qNew.CompanyId));
                compare(lbl2ProcurementContact_Previous, lbl2ProcurementContact_Current, getProcurementValue_User(qOld.QuestionnaireId, "2"), getProcurementValue_User(qNew.QuestionnaireId, "2"));
                compare(lbl2ContractManager_Previous, lbl2ContractManager_Current, getProcurementValue_User(qOld.QuestionnaireId, "2_1"), getProcurementValue_User(qNew.QuestionnaireId, "2_1"));
                compare(lbl2OriginalRequesterName_Previous, lbl2OriginalRequesterName_Current, getProcurementValue(qOld.QuestionnaireId, "2_2"), getProcurementValue(qNew.QuestionnaireId, "2_2"));
                compare(lbl2OriginalRequesterTelephone_Previous, lbl2OriginalRequesterTelephone_Current, getProcurementValue(qOld.QuestionnaireId, "2_3"), getProcurementValue(qNew.QuestionnaireId, "2_3"));
                compare(lbl3WorkDescription_Previous, lbl3WorkDescription_Current, getProcurementValue(qOld.QuestionnaireId, "3"), getProcurementValue(qNew.QuestionnaireId, "3"));
                //-Q4
                QuestionnaireServicesCategoryService qscService = new QuestionnaireServicesCategoryService();
                QuestionnaireServicesSelectedService qssService = new QuestionnaireServicesSelectedService();
                TList<QuestionnaireServicesSelected> qssListOld = qssService.GetByQuestionnaireIdQuestionnaireTypeId(qOld.QuestionnaireId, (int)QuestionnaireTypeList.Initial_1);
                TList<QuestionnaireServicesSelected> qssListNew = qssService.GetByQuestionnaireIdQuestionnaireTypeId(qNew.QuestionnaireId, (int)QuestionnaireTypeList.Initial_1);
                if (qssListOld.Count > 0)
                {
                    QuestionnaireServicesCategory qscOld = qscService.GetByCategoryId(qssListOld[0].CategoryId);
                    PreviousAnswer = qscOld.CategoryText;
                }
                if (qssListNew.Count > 0)
                {
                    QuestionnaireServicesCategory qscNew = qscService.GetByCategoryId(qssListNew[0].CategoryId);
                    CurrentAnswer = qscNew.CategoryText;
                }
                compare(lbl4PrimaryService_Previous, lbl4PrimaryService_Current, PreviousAnswer, CurrentAnswer);
                //-4-Other
                compare(lbl4Other_Previous, lbl4Other_Current, getProcurementValue(qOld.QuestionnaireId, "4_1"), getProcurementValue(qNew.QuestionnaireId, "4_1"));
                //-5
                compare(lbl5NoPeople_Previous, lbl5NoPeople_Current, getProcurementValue(qOld.QuestionnaireId, "5"), getProcurementValue(qNew.QuestionnaireId, "5"));
                //-6 ********************SITES******
                //-7
                compare(lbl7WhenServiceRequired_Previous, lbl7WhenServiceRequired_Current, getProcurementValue(qOld.QuestionnaireId, "8"), getProcurementValue(qNew.QuestionnaireId, "8"));
                //-8
                compare(lbl8_Q1_Previous, lbl8_Q1_Current, getProcurementValue(qOld.QuestionnaireId, "11_1"), getProcurementValue(qNew.QuestionnaireId, "11_1"));
                compare(lbl8_Q2_Previous, lbl8_Q2_Current, getProcurementValue(qOld.QuestionnaireId, "11_2"), getProcurementValue(qNew.QuestionnaireId, "11_2"));
                compare(lbl8_Q3_Previous, lbl8_Q3_Current, getProcurementValue(qOld.QuestionnaireId, "11_3"), getProcurementValue(qNew.QuestionnaireId, "11_3"));
                compare(lbl8_Q4_Previous, lbl8_Q4_Current, getProcurementValue(qOld.QuestionnaireId, "11_4"), getProcurementValue(qNew.QuestionnaireId, "11_4"));
                compare(lbl8_Q5_Previous, lbl8_Q5_Current, getProcurementValue(qOld.QuestionnaireId, "11_5"), getProcurementValue(qNew.QuestionnaireId, "11_5"));
                compare(lbl8_Q6_Previous, lbl8_Q6_Current, getProcurementValue(qOld.QuestionnaireId, "11_6"), getProcurementValue(qNew.QuestionnaireId, "11_6"));
                //-9
                compare(lbl9_Q1_Previous, lbl9_Q1_Current, getProcurementValue(qOld.QuestionnaireId, "10_1"), getProcurementValue(qNew.QuestionnaireId, "10_1"));
                compare(lbl9_Q2_Previous, lbl9_Q2_Current, getProcurementValue(qOld.QuestionnaireId, "10_2"), getProcurementValue(qNew.QuestionnaireId, "10_2"));
                compare(lbl9_Q3_Previous, lbl9_Q3_Current, getProcurementValue(qOld.QuestionnaireId, "10_3"), getProcurementValue(qNew.QuestionnaireId, "10_3"));
                compare(lbl9_Q4_Previous, lbl9_Q4_Current, getProcurementValue(qOld.QuestionnaireId, "10_4"), getProcurementValue(qNew.QuestionnaireId, "10_4"));
                compare(lbl9_Q5_Previous, lbl9_Q5_Current, getProcurementValue(qOld.QuestionnaireId, "10_5"), getProcurementValue(qNew.QuestionnaireId, "10_5"));
                compare(lbl9_Q6_Previous, lbl9_Q6_Current, getProcurementValue(qOld.QuestionnaireId, "10_6"), getProcurementValue(qNew.QuestionnaireId, "10_6"));
                compare(lbl9_Q7_Previous, lbl9_Q7_Current, getProcurementValue(qOld.QuestionnaireId, "10_7"), getProcurementValue(qNew.QuestionnaireId, "10_7"));
                compare(lbl9_Q8_Previous, lbl9_Q8_Current, getProcurementValue(qOld.QuestionnaireId, "10_8"), getProcurementValue(qNew.QuestionnaireId, "10_8"));
                compare(lbl9_Q9_Previous, lbl9_Q9_Current, getProcurementValue(qOld.QuestionnaireId, "10_9"), getProcurementValue(qNew.QuestionnaireId, "10_9"));
                compare(lbl9_Q10_Previous, lbl9_Q10_Current, getProcurementValue(qOld.QuestionnaireId, "10_10"), getProcurementValue(qNew.QuestionnaireId, "10_10"));
                compare(lbl9_Q11_Previous, lbl9_Q11_Current, getProcurementValue(qOld.QuestionnaireId, "10_11"), getProcurementValue(qNew.QuestionnaireId, "10_11"));

                ASPxPageControl1.TabPages[1].Enabled = true;

                /* 
                 * Supplier
                 */
                if (qOld.IsMainRequired == true || qNew.IsMainRequired == true)
                {
                    //-10
                    compare(slbl1Date_Previous, slbl1Date_Current,
                               DateTime.ParseExact(Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("01", "")), "dd/MM/yyyy", CultureInfo.InvariantCulture).ToString("dd/MM/yyyy"),
                               DateTime.ParseExact(Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("01", "")), "dd/MM/yyyy", CultureInfo.InvariantCulture).ToString("dd/MM/yyyy"));
                    compare(slbl2LegalCompanyName_Previous, slbl2LegalCompanyName_Current,
                                Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("02", "")),
                                Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("02", "")));

                    cOld = null;
                    cNew = null;
                    cOld = cService.GetByCompanyId(Convert.ToInt32(Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("03", ""))));
                    cNew = cService.GetByCompanyId(Convert.ToInt32(Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("03", ""))));
                    if (cOld != null) PreviousAnswer = cOld.CompanyName;
                    if (cNew != null) CurrentAnswer = cNew.CompanyName;
                    compare(slbl3Company_Previous, slbl3Company_Current, PreviousAnswer, CurrentAnswer);

                    compare(slbl4ABN_Previous, slbl4ABN_Current,
                            Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("04", "")),
                            Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("04", "")));

                    compare(slbl5AddressStreet_Previous, slbl5AddressStreet_Current,
                            Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("05", "A")),
                            Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("05", "A")));

                    compare(slbl5AddressSuburb_Previous, slbl5AddressSuburb_Current,
                            Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("05", "B")),
                            Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("05", "B")));

                    compare(slbl5AddressState_Previous, slbl5AddressState_Current,
                            Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("05", "C")),
                            Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("05", "C")));

                    compare(slbl5AddressPostCode_Previous, slbl5AddressPostCode_Current,
                            Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("05", "D")),
                            Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("05", "D")));

                    compare(slbl5AddressCountry_Previous, slbl5AddressCountry_Current,
                            Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("05", "E")),
                            Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("05", "E")));

                    compare(slbl6ContactName_Previous, slbl6ContactName_Current,
                            Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("06", "")),
                            Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("06", "")));

                    compare(slbl7ContactTitle_Previous, slbl7ContactTitle_Current,
                            Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("07", "")),
                            Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("07", "")));

                    compare(slbl8ContactEmail_Previous, slbl8ContactEmail_Current,
                            Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("08", "")),
                            Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("08", "")));

                    compare(slbl9ContactPhone_Previous, slbl9ContactPhone_Current,
                            Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("09", "")),
                            Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("09", "")));

                    compare(slbl10ContactFax_Previous, slbl10ContactFax_Current,
                            Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("10", "")),
                            Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("10", "")));

                    //-11
                    qssListOld = null;
                    qssListOld = qssService.GetByQuestionnaireIdQuestionnaireTypeId(qOld.QuestionnaireId, (int)QuestionnaireTypeList.Main_1);
                    if (qssListOld.Count > 0)
                    {
                        int i = 1;
                        foreach (QuestionnaireServicesSelected _qs in qssListOld)
                        {
                            QuestionnaireServicesCategory qsc = qscService.GetByCategoryId(_qs.CategoryId);
                            if (qssListOld.Count != i)
                            {
                                PreviousAnswer += qsc.CategoryText + ", ";
                                i++;
                            }
                            else
                            {
                                PreviousAnswer += qsc.CategoryText;
                            }
                        }
                    }
                    qssListNew = null;
                    qssListNew = qssService.GetByQuestionnaireIdQuestionnaireTypeId(qNew.QuestionnaireId, (int)QuestionnaireTypeList.Main_1);
                    if (qssListNew.Count > 0)
                    {
                        int i = 1;
                        foreach (QuestionnaireServicesSelected _qs in qssListNew)
                        {
                            QuestionnaireServicesCategory qsc = qscService.GetByCategoryId(_qs.CategoryId);
                            if (qssListNew.Count != i)
                            {
                                CurrentAnswer += qsc.CategoryText + ", ";
                                i++;
                            }
                            else
                            {
                                CurrentAnswer += qsc.CategoryText;
                            }
                        }
                    }
                    compare(slbl11Services_Previous, slbl11Services_Current, PreviousAnswer, CurrentAnswer);

                    //-12
                    compare(slbl12_Previous, slbl12_Current,
                        supplierselectedoption("ABC", qOld.QuestionnaireId, "12"),
                        supplierselectedoption("ABC", qNew.QuestionnaireId, "12"));

                    compare(slbl12Comments_Previous, slbl12Comments_Current,
                        Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("12", "Comments")),
                        Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("12", "Comments")));

                    compareSupplierAttachment(shl12Example_Previous, shl12Example_Current,
                        qOld.QuestionnaireId, qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("12", "AExample"));
                    
                    //-13
                    compare(slbl13_Previous, slbl13_Current,
                        supplierselectedoption("ABCDE", qOld.QuestionnaireId, "13"),
                        supplierselectedoption("ABCDE", qNew.QuestionnaireId, "13"));

                    compare(slbl13Comments_Previous, slbl13Comments_Current,
                        Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("13", "Comments")),
                        Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("13", "Comments")));

                    //-14
                    compare(slbl14a_Previous, slbl14a_Current,
                        supplierselectedoption("A", qOld.QuestionnaireId, "14"),
                        supplierselectedoption("A", qNew.QuestionnaireId, "14"));
                    compare(slbl14b_Previous, slbl14b_Current,
                        supplierselectedoption("B", qOld.QuestionnaireId, "14"),
                        supplierselectedoption("B", qNew.QuestionnaireId, "14"));
                    compare(slbl14c_Previous, slbl14c_Current,
                        supplierselectedoption("C", qOld.QuestionnaireId, "14"),
                        supplierselectedoption("C", qNew.QuestionnaireId, "14"));
                    compare(slbl14d_Previous, slbl14d_Current,
                        supplierselectedoption("D", qOld.QuestionnaireId, "14"),
                        supplierselectedoption("D", qNew.QuestionnaireId, "14"));
                    compare(slbl14Comments_Previous, slbl14Comments_Current,
                        Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("14", "Comments")),
                        Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("14", "Comments")));
                    
                    //-15
                    compare(slbl15a_Previous, slbl15a_Current,
                        supplierselectedoption("A", qOld.QuestionnaireId, "15"),
                        supplierselectedoption("A", qNew.QuestionnaireId, "15"));
                    compare(slbl15aComments_Previous, slbl15aComments_Current,
                        Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("15", "AComments")),
                        Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("15", "AComments")));
                    compare(slbl15b_Previous, slbl15b_Current,
                        supplierselectedoption("B", qOld.QuestionnaireId, "15"),
                        supplierselectedoption("B", qNew.QuestionnaireId, "15"));
                    compare(slbl15bComments_Previous, slbl15bComments_Current,
                        Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("15", "BComments")),
                        Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("15", "BComments")));
                    compare(slbl15c_Previous, slbl15c_Current,
                        supplierselectedoption("C", qOld.QuestionnaireId, "15"),
                        supplierselectedoption("C", qNew.QuestionnaireId, "15"));
                    compare(slbl15cComments_Previous, slbl15cComments_Current,
                        Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("15", "CComments")),
                        Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("15", "CComments")));
                    
                    //-16                          
                    compare(slbl16a_Previous, slbl16a_Current,
                        supplierselectedoption("A", qOld.QuestionnaireId, "16"),
                        supplierselectedoption("A", qNew.QuestionnaireId, "16"));
                    compare(slbl16b_Previous, slbl16b_Current,
                        supplierselectedoption("B", qOld.QuestionnaireId, "16"),
                        supplierselectedoption("B", qNew.QuestionnaireId, "16"));
                    compare(slbl16bComments_Previous, slbl16bComments_Current,
                        Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("16", "BComments")),
                        Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("16", "BComments")));
                    compare(slbl16c_Previous, slbl16c_Current,
                        supplierselectedoption("C", qOld.QuestionnaireId, "16"),
                        supplierselectedoption("C", qNew.QuestionnaireId, "16"));
                    compare(slbl16cComments_Previous, slbl16cComments_Current,
                        Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("16", "CComments")),
                        Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("16", "CComments")));
                    
                    //-17
                    compare(slbl17a_Previous, slbl17a_Current,
                            supplierselectedoption("A", qOld.QuestionnaireId, "17"),
                            supplierselectedoption("A", qNew.QuestionnaireId, "17"));
                    compare(slbl17b_Previous, slbl17b_Current,
                            supplierselectedoption("B", qOld.QuestionnaireId, "17"),
                            supplierselectedoption("B", qNew.QuestionnaireId, "17"));
                    compareSupplierAttachment(shl17bExample_Previous, shl17bExample_Current,
                        qOld.QuestionnaireId, qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("17", "BExample"));
                    
                    //-18
                    compare(slbl18_Previous, slbl18_Current,
                        supplierselectedoption("ABCD", qOld.QuestionnaireId, "18"),
                        supplierselectedoption("ABCD", qNew.QuestionnaireId, "18"));
                    compare(slbl18Comments_Previous, slbl18Comments_Current,
                        Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("18", "Comments")),
                        Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("18", "Comments")));
                    
                    //-19
                    compare(slbl19_Previous, slbl19_Current,
                        supplierselectedoption("ABCD", qOld.QuestionnaireId, "19"),
                        supplierselectedoption("ABCD", qNew.QuestionnaireId, "19"));
                    compare(slbl19Comments_Previous, slbl19Comments_Current,
                        Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("19", "Comments")),
                        Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("19", "Comments")));

                    //-20
                    compare(slbl20a_Previous, slbl20a_Current,
                            supplierselectedoption("A", qOld.QuestionnaireId, "20"),
                            supplierselectedoption("A", qNew.QuestionnaireId, "20"));
                    compare(slbl20aComments_Previous, slbl20aComments_Current,
                        Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("20", "AComments")),
                        Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("20", "AComments")));
                    compare(slbl20b_Previous, slbl20b_Current,
                            supplierselectedoption("B", qOld.QuestionnaireId, "20"),
                            supplierselectedoption("B", qNew.QuestionnaireId, "20"));
                    compare(slbl20bComments_Previous, slbl20bComments_Current,
                        Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("20", "BComments")),
                        Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("20", "BComments")));
                    
                    //-21
                    compare(slbl21_Previous, slbl21_Current,
                        supplierselectedoption("ABCD", qOld.QuestionnaireId, "21"),
                        supplierselectedoption("ABCD", qNew.QuestionnaireId, "21"));
                    compare(slbl21Comments_Previous, slbl21Comments_Current,
                        Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("21", "Comments")),
                        Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("21", "Comments")));

                    //-22
                    compare(slbl22a_Previous, slbl22a_Current,
                            supplierselectedoption("A", qOld.QuestionnaireId, "22"),
                            supplierselectedoption("A", qNew.QuestionnaireId, "22"));
                    compare(slbl22b_Previous, slbl22b_Current,
                            supplierselectedoption("B", qOld.QuestionnaireId, "22"),
                            supplierselectedoption("B", qNew.QuestionnaireId, "22"));
                    compare(slbl22c_Previous, slbl22c_Current,
                            supplierselectedoption("C", qOld.QuestionnaireId, "22"),
                            supplierselectedoption("C", qNew.QuestionnaireId, "22"));
                    compare(slbl22Comments_Previous, slbl22Comments_Current,
                        Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("22", "Comments")),
                        Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("22", "Comments")));
                    
                    //-23
                    compare(slbl23_Previous, slbl23_Current,
                        supplierselectedoption("ABCD", qOld.QuestionnaireId, "23"),
                        supplierselectedoption("ABCD", qNew.QuestionnaireId, "23"));
                    compare(slbl23Comments_Previous, slbl23Comments_Current,
                        Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("23", "Comments")),
                        Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("23", "Comments")));
                    compareSupplierAttachment(shl23Example_Previous, shl23Example_Current,
                        qOld.QuestionnaireId, qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("23", "Example"));
                    
                    //-24
                    compare(slbl24_Previous, slbl24_Current,
                        supplierselectedoption("ABCD", qOld.QuestionnaireId, "24"), supplierselectedoption("ABCD", qNew.QuestionnaireId, "24"));
                    compare(slbl24Comments_Previous, slbl24Comments_Current,
                        Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("24", "Comments")),
                        Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("24", "Comments")));
                    compare(slbl25a_Previous, slbl25a_Current,
                        supplierselectedoption("A", qOld.QuestionnaireId, "25"), supplierselectedoption("A", qNew.QuestionnaireId, "25"));
                    compareSupplierAttachment(shl25aExample_Previous, shl25aExample_Current,
                        qOld.QuestionnaireId, qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("25", "AExample"));
                    compare(slbl25b_Previous, slbl25b_Current,
                        supplierselectedoption("B", qOld.QuestionnaireId, "25"), supplierselectedoption("B", qNew.QuestionnaireId, "25"));
                    compareSupplierAttachment(shl25bExample_Previous, shl25bExample_Current,
                        qOld.QuestionnaireId, qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("25", "BExample"));
                    compare(slbl25c_Previous, slbl25c_Current,
                        supplierselectedoption("C", qOld.QuestionnaireId, "25"), supplierselectedoption("C", qNew.QuestionnaireId, "25"));
                    compareSupplierAttachment(shl25cExample_Previous, shl25cExample_Current,
                        qOld.QuestionnaireId, qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("25", "CExample"));
                    
                    //-25 
                    compare(slbl25a_Previous, slbl25a_Current,
                        supplierselectedoption("A", qOld.QuestionnaireId, "25"), supplierselectedoption("A", qNew.QuestionnaireId, "25"));
                    compareSupplierAttachment(shl25aExample_Previous, shl25aExample_Current,
                        qOld.QuestionnaireId, qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("25", "AExample"));
                    compare(slbl25b_Previous, slbl25b_Current,
                        supplierselectedoption("B", qOld.QuestionnaireId, "25"), supplierselectedoption("B", qNew.QuestionnaireId, "25"));
                    compareSupplierAttachment(shl25bExample_Previous, shl25bExample_Current,
                        qOld.QuestionnaireId, qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("25", "BExample"));
                    compare(slbl25c_Previous, slbl25c_Current,
                        supplierselectedoption("C", qOld.QuestionnaireId, "25"), supplierselectedoption("C", qNew.QuestionnaireId, "25"));
                    compareSupplierAttachment(shl25cExample_Previous, shl25cExample_Current,
                        qOld.QuestionnaireId, qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("25", "CExample"));
                    
                    //-26
                    compare(slbl26_Previous, slbl26_Current,
                        supplierselectedoption("A", qOld.QuestionnaireId, "26"), supplierselectedoption("A", qNew.QuestionnaireId, "26"));
                    compare(slbl26Comments_Previous, slbl26Comments_Current,
                        Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("26", "AComments")),
                        Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("26", "AComments")));
                    compareSupplierAttachment(shl26Example_Previous, shl26Example_Current,
                        qOld.QuestionnaireId, qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("26", "AExample"));
                    
                    //-27
                    if (qOld.MainModifiedDate.HasValue)
                    {
                        DateTime dtPrevious = (DateTime)qOld.MainModifiedDate;
                        int Year1 = dtPrevious.Year - 3;
                        int Year2 = dtPrevious.Year - 2;
                        int Year3 = dtPrevious.Year - 1;
                        PreviousAnswer = String.Format("{0}, {1}, {2}", Year1, Year2, Year3);
                    }
                    if (qNew.MainModifiedDate != null)
                    {
                        DateTime dtCurrent = (DateTime)qNew.MainModifiedDate;
                        int Year1 = dtCurrent.Year - 3;
                        int Year2 = dtCurrent.Year - 2;
                        int Year3 = dtCurrent.Year - 1;
                        CurrentAnswer = String.Format("{0}, {1}, {2}", Year1, Year2, Year3);
                    }

                    compare(slbl27_Previous, slbl27_Current, PreviousAnswer, CurrentAnswer);
                    compare(slbl27a_Previous, slbl27a_Current,
                        Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("27", "A1")) + ", " +
                        Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("27", "A2")) + ", " +
                        Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("27", "A3")),
                        Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("27", "A1")) + ", " +
                        Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("27", "A2")) + ", " +
                        Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("27", "A3")));
                    compare(slbl27b_Previous, slbl27b_Current,
                        Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("27", "B1")) + ", " +
                        Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("27", "B2")) + ", " +
                        Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("27", "B3")),
                        Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("27", "B1")) + ", " +
                        Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("27", "B2")) + ", " +
                        Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("27", "B3")));
                    compare(slbl27c_Previous, slbl27c_Current,
                        Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("27", "C1")) + ", " +
                        Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("27", "C2")) + ", " +
                        Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("27", "C3")),
                        Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("27", "C1")) + ", " +
                        Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("27", "C2")) + ", " +
                        Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("27", "C3")));
                    compare(slbl27d_Previous, slbl27d_Current,
                        Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("27", "D1")) + ", " +
                        Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("27", "D2")) + ", " +
                        Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("27", "D3")),
                        Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("27", "D1")) + ", " +
                        Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("27", "D2")) + ", " +
                        Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("27", "D3")));
                    compare(slbl27e_Previous, slbl27e_Current,
                        Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("27", "E1")) + ", " +
                        Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("27", "E2")) + ", " +
                        Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("27", "E3")),
                        Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("27", "E1")) + ", " +
                        Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("27", "E2")) + ", " +
                        Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("27", "E3")));
                    
                    //-28
                    if (qOld.MainModifiedDate.HasValue)
                    {
                        DateTime dtPrevious = (DateTime)qOld.MainModifiedDate;
                        int Year1 = dtPrevious.Year - 3;
                        int Year2 = dtPrevious.Year - 2;
                        int Year3 = dtPrevious.Year - 1;
                        PreviousAnswer = String.Format("{0}, {1}, {2}", Year1, Year2, Year3);
                    }
                    if (qNew.MainModifiedDate != null)
                    {
                        DateTime dtCurrent = (DateTime)qNew.MainModifiedDate;
                        int Year1 = dtCurrent.Year - 3;
                        int Year2 = dtCurrent.Year - 2;
                        int Year3 = dtCurrent.Year - 1;
                        CurrentAnswer = String.Format("{0}, {1}, {2}", Year1, Year2, Year3);
                    }

                    compare(slbl28_Previous, slbl28_Current, PreviousAnswer, CurrentAnswer);
                    compare(slbl28a_Previous, slbl28a_Current,
                        Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("28", "A2")) + ", " +
                        Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("28", "A3")) + ", " +
                        Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("28", "A6")),
                        Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("28", "A2")) + ", " +
                        Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("28", "A3")) + ", " +
                        Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("28", "A6")));
                    compare(slbl28b_Previous, slbl28b_Current,
                        Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("28", "B2")) + ", " +
                        Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("28", "B3")) + ", " +
                        Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("28", "B6")),
                        Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("28", "B2")) + ", " +
                        Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("28", "B3")) + ", " +
                        Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("28", "B6")));
                    compare(slbl28c_Previous, slbl28c_Current,
                        Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("28", "C2")) + ", " +
                        Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("28", "C3")) + ", " +
                        Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("28", "C6")),
                        Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("28", "C2")) + ", " +
                        Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("28", "C3")) + ", " +
                        Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("28", "C6")));
                    
                    //-29i
                    if (qOld.MainModifiedDate.HasValue)
                    {
                        DateTime dtPrevious = (DateTime)qOld.MainModifiedDate;
                        int Year1 = dtPrevious.Year - 3;
                        int Year2 = dtPrevious.Year - 2;
                        int Year3 = dtPrevious.Year - 1;
                        PreviousAnswer = String.Format("{0}, {1}, {2}", Year1, Year2, Year3);
                    }
                    if (qNew.MainModifiedDate != null)
                    {
                        DateTime dtCurrent = (DateTime)qNew.MainModifiedDate;
                        int Year1 = dtCurrent.Year - 3;
                        int Year2 = dtCurrent.Year - 2;
                        int Year3 = dtCurrent.Year - 1;
                        CurrentAnswer = String.Format("{0}, {1}, {2}", Year1, Year2, Year3);
                    }
                    compare(slbl29i_Previous, slbl29i_Current, PreviousAnswer, CurrentAnswer);
                    compare(slbl29ia_Previous, slbl29ia_Current,
                        supplierselectedoption("A", qOld.QuestionnaireId, "29i"), supplierselectedoption("A", qNew.QuestionnaireId, "29i"));
                    compare(slbl29iaComments_Previous, slbl29iaComments_Current,
                        Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("29i", "AComments")),
                        Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("29i", "AComments")));
                    compare(slbl29ib_Previous, slbl29ib_Current,
                        supplierselectedoption("B", qOld.QuestionnaireId, "29i"), supplierselectedoption("B", qNew.QuestionnaireId, "29i"));
                    compare(slbl29ibComments_Previous, slbl29ibComments_Current,
                        Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("29i", "BComments")),
                        Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("29i", "BComments")));
                    compare(slbl29ic_Previous, slbl29ic_Current,
                        supplierselectedoption("C", qOld.QuestionnaireId, "29i"), supplierselectedoption("C", qNew.QuestionnaireId, "29i"));
                    compare(slbl29icComments_Previous, slbl29icComments_Current,
                        Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("29i", "CComments")),
                        Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("29i", "CComments")));
                    compare(slbl29id_Previous, slbl29id_Current,
                        supplierselectedoption("D", qOld.QuestionnaireId, "29i"), supplierselectedoption("D", qNew.QuestionnaireId, "29i"));
                    compare(slbl29idComments_Previous, slbl29idComments_Current,
                        Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("29i", "DComments")),
                        Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("29i", "DComments")));
                    
                    //-29ii
                    compare(slbl29ii_Previous, slbl29ii_Current,
                        supplierselectedoption("A", qOld.QuestionnaireId, "29ii"), supplierselectedoption("A", qNew.QuestionnaireId, "29ii"));
                    compare(slbl29iiComments_Previous, slbl29iiComments_Current,
                        Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("29ii", "Comments")),
                        Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("29ii", "Comments")));
                    compareSupplierAttachment(shl29iiExample_Previous, shl29iiExample_Current,
                        qOld.QuestionnaireId, qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("29ii", "AExample"));
                    
                    //-30
                    compare(slbl30abc_Previous, slbl30abc_Current,
                        supplierselectedoption("ABC", qOld.QuestionnaireId, "30"), supplierselectedoption("ABC", qNew.QuestionnaireId, "30"));

                    string SiteId1_Old = Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("30", "E1"));
                    string SiteId2_Old = Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("30", "F1"));
                    string SiteId3_Old = Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("30", "G1"));
                    string SiteId1_New = Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("30", "E1"));
                    string SiteId2_New = Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("30", "F1"));
                    string SiteId3_New = Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("30", "G1"));

                    SitesService sService = new SitesService();
                    if (!String.IsNullOrEmpty(SiteId1_Old))
                    {
                        Sites s = sService.GetBySiteId(Convert.ToInt32(SiteId1_Old));
                        SiteId1_Old = s.SiteName;
                    }
                    else
                    {
                        SiteId1_Old = "-";
                    }
                    if (!String.IsNullOrEmpty(SiteId2_Old))
                    {
                        Sites s = sService.GetBySiteId(Convert.ToInt32(SiteId2_Old));
                        SiteId2_Old = s.SiteName;
                    }
                    else
                    {
                        SiteId2_Old = "-";
                    }
                    if (!String.IsNullOrEmpty(SiteId3_Old))
                    {
                        Sites s = sService.GetBySiteId(Convert.ToInt32(SiteId3_Old));
                        SiteId3_Old = s.SiteName;
                    }
                    else
                    {
                        SiteId3_Old = "-";
                    }
                    if (!String.IsNullOrEmpty(SiteId1_New))
                    {
                        Sites s = sService.GetBySiteId(Convert.ToInt32(SiteId1_New));
                        SiteId1_New = s.SiteName;
                    }
                    else
                    {
                        SiteId1_New = "-";
                    }
                    if (!String.IsNullOrEmpty(SiteId2_New))
                    {
                        Sites s = sService.GetBySiteId(Convert.ToInt32(SiteId2_New));
                        SiteId2_New = s.SiteName;
                    }
                    else
                    {
                        SiteId2_New = "-";
                    }
                    if (!String.IsNullOrEmpty(SiteId3_New))
                    {
                        Sites s = sService.GetBySiteId(Convert.ToInt32(SiteId3_New));
                        SiteId3_New = s.SiteName;
                    }
                    else
                    {
                        SiteId3_New = "-";
                    }

                    PreviousAnswer = SiteId1_Old + ", " + Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("30", "E2")) + ", " +
                        Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("30", "E3")) + ", " +
                        Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("30", "E3B")) + ", " +
                        Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("30", "E4"));
                    CurrentAnswer = SiteId1_New + ", " + Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("30", "E2")) + ", " +
                        Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("30", "E3")) + ", " +
                        Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("30", "E3B")) + ", " +
                        Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("30", "E4"));
                    compare(slbl30_1_Previous, slbl30_1_Current, PreviousAnswer, CurrentAnswer);

                    PreviousAnswer = SiteId2_Old + ", " + Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("30", "F2")) + ", " +
                        Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("30", "F3")) + ", " +
                        Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("30", "F3B")) + ", " +
                        Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("30", "F4"));
                    CurrentAnswer = SiteId2_New + ", " + Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("30", "F2")) + ", " +
                        Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("30", "F3")) + ", " +
                        Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("30", "F3B")) + ", " +
                        Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("30", "F4"));
                    compare(slbl30_2_Previous, slbl30_2_Current, PreviousAnswer, CurrentAnswer);

                    PreviousAnswer = SiteId3_Old + ", " + Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("30", "G2")) + ", " +
                        Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("30", "G3")) + ", " +
                        Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("30", "G3B")) + ", " +
                        Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("30", "G4"));
                    CurrentAnswer = SiteId3_New + ", " + Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("30", "G2")) + ", " +
                        Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("30", "G3")) + ", " +
                        Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("30", "G3B")) + ", " +
                        Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("30", "G4"));
                    compare(slbl30_3_Previous, slbl30_3_Current, PreviousAnswer, CurrentAnswer);


                    if (qNew.IsMainRequired && qOld.IsMainRequired)
                    {
                        ASPxPageControl1.TabPages[2].Enabled = true;
                    }

                    if (!qNew.IsMainRequired)
                    {
                        ApprovalSupplierHide();
                        slbl1Date_Previous.ForeColor = Color.Black;
                        slbl2LegalCompanyName_Previous.ForeColor = Color.Black;
                        slbl3Company_Previous.ForeColor = Color.Black;
                        slbl4ABN_Previous.ForeColor = Color.Black;
                        slbl5AddressCountry_Previous.ForeColor = Color.Black;
                        slbl5AddressPostCode_Previous.ForeColor = Color.Black;
                        slbl5AddressState_Previous.ForeColor = Color.Black;
                        slbl5AddressStreet_Previous.ForeColor = Color.Black;
                        slbl5AddressSuburb_Previous.ForeColor = Color.Black;
                        slbl6ContactName_Previous.ForeColor = Color.Black;
                        slbl7ContactTitle_Previous.ForeColor = Color.Black;
                        slbl8ContactEmail_Previous.ForeColor = Color.Black;
                        slbl9ContactPhone_Previous.ForeColor = Color.Black;
                        slbl10ContactFax_Previous.ForeColor = Color.Black;
                        slbl11Other_Previous.ForeColor = Color.Black;
                        slbl11Services_Previous.ForeColor = Color.Black;
                        slbl12_Previous.ForeColor = Color.Black;
                        slbl12Comments_Previous.ForeColor = Color.Black;
                        slbl13_Previous.ForeColor = Color.Black;
                        slbl13Comments_Previous.ForeColor = Color.Black;
                        slbl14a_Previous.ForeColor = Color.Black;
                        slbl14b_Previous.ForeColor = Color.Black;
                        slbl14c_Previous.ForeColor = Color.Black;
                        slbl14Comments_Previous.ForeColor = Color.Black;
                        slbl14d_Previous.ForeColor = Color.Black;
                        slbl15a_Previous.ForeColor = Color.Black;
                        slbl15aComments_Previous.ForeColor = Color.Black;
                        slbl15b_Previous.ForeColor = Color.Black;
                        slbl15bComments_Previous.ForeColor = Color.Black;
                        slbl15c_Previous.ForeColor = Color.Black;
                        slbl15cComments_Previous.ForeColor = Color.Black;
                        slbl16a_Previous.ForeColor = Color.Black;
                        slbl16b_Previous.ForeColor = Color.Black;
                        slbl16bComments_Previous.ForeColor = Color.Black;
                        slbl16c_Previous.ForeColor = Color.Black;
                        slbl16cComments_Previous.ForeColor = Color.Black;
                        slbl17a_Previous.ForeColor = Color.Black;
                        slbl17b_Previous.ForeColor = Color.Black;
                        slbl18_Previous.ForeColor = Color.Black;
                        slbl18Comments_Previous.ForeColor = Color.Black;
                        slbl19_Previous.ForeColor = Color.Black;
                        slbl19Comments_Previous.ForeColor = Color.Black;
                        slbl20a_Previous.ForeColor = Color.Black;
                        slbl20aComments_Previous.ForeColor = Color.Black;
                        slbl20b_Previous.ForeColor = Color.Black;
                        slbl20bComments_Previous.ForeColor = Color.Black;
                        slbl21_Previous.ForeColor = Color.Black;
                        slbl21Comments_Previous.ForeColor = Color.Black;
                        slbl22a_Previous.ForeColor = Color.Black;
                        slbl22b_Previous.ForeColor = Color.Black;
                        slbl22c_Previous.ForeColor = Color.Black;
                        slbl22Comments_Previous.ForeColor = Color.Black;
                        slbl23_Previous.ForeColor = Color.Black;
                        slbl23Comments_Previous.ForeColor = Color.Black;
                        slbl24_Previous.ForeColor = Color.Black;
                        slbl24Comments_Previous.ForeColor = Color.Black;
                        slbl25a_Previous.ForeColor = Color.Black;
                        slbl25b_Previous.ForeColor = Color.Black;
                        slbl25c_Previous.ForeColor = Color.Black;
                        slbl26_Previous.ForeColor = Color.Black;
                        slbl26Comments_Previous.ForeColor = Color.Black;
                        slbl27_Previous.ForeColor = Color.Black;
                        slbl27a_Previous.ForeColor = Color.Black;
                        slbl27b_Previous.ForeColor = Color.Black;
                        slbl27c_Previous.ForeColor = Color.Black;
                        slbl27d_Previous.ForeColor = Color.Black;
                        slbl27e_Previous.ForeColor = Color.Black;
                        slbl28_Previous.ForeColor = Color.Black;
                        slbl28a_Previous.ForeColor = Color.Black;
                        slbl28b_Previous.ForeColor = Color.Black;
                        slbl28c_Previous.ForeColor = Color.Black;
                        slbl29i_Previous.ForeColor = Color.Black;
                        slbl29ia_Previous.ForeColor = Color.Black;
                        slbl29iaComments_Previous.ForeColor = Color.Black;
                        slbl29ib_Previous.ForeColor = Color.Black;
                        slbl29ibComments_Previous.ForeColor = Color.Black;
                        slbl29ic_Previous.ForeColor = Color.Black;
                        slbl29icComments_Previous.ForeColor = Color.Black;
                        slbl29id_Previous.ForeColor = Color.Black;
                        slbl29idComments_Previous.ForeColor = Color.Black;
                        slbl29ii_Previous.ForeColor = Color.Black;
                        slbl29iiComments_Previous.ForeColor = Color.Black;
                        slbl30_1_Previous.ForeColor = Color.Black;
                        slbl30_2_Previous.ForeColor = Color.Black;
                        slbl30_3_Previous.ForeColor = Color.Black;
                        slbl30abc_Previous.ForeColor = Color.Black;

                        if (qOld.IsMainRequired)
                        {
                            slbl1Date_Current.ForeColor = Color.Black;
                            slbl2LegalCompanyName_Current.ForeColor = Color.Black;
                            slbl3Company_Current.ForeColor = Color.Black;
                            slbl4ABN_Current.ForeColor = Color.Black;
                            slbl5AddressCountry_Current.ForeColor = Color.Black;
                            slbl5AddressPostCode_Current.ForeColor = Color.Black;
                            slbl5AddressState_Current.ForeColor = Color.Black;
                            slbl5AddressStreet_Current.ForeColor = Color.Black;
                            slbl5AddressSuburb_Current.ForeColor = Color.Black;
                            slbl6ContactName_Current.ForeColor = Color.Black;
                            slbl7ContactTitle_Current.ForeColor = Color.Black;
                            slbl8ContactEmail_Current.ForeColor = Color.Black;
                            slbl9ContactPhone_Current.ForeColor = Color.Black;
                            slbl10ContactFax_Current.ForeColor = Color.Black;
                            slbl11Other_Current.ForeColor = Color.Black;
                            slbl11Services_Current.ForeColor = Color.Black;
                            slbl12_Current.ForeColor = Color.Black;
                            slbl12Comments_Current.ForeColor = Color.Black;
                            slbl13_Current.ForeColor = Color.Black;
                            slbl13Comments_Current.ForeColor = Color.Black;
                            slbl14a_Current.ForeColor = Color.Black;
                            slbl14b_Current.ForeColor = Color.Black;
                            slbl14c_Current.ForeColor = Color.Black;
                            slbl14Comments_Current.ForeColor = Color.Black;
                            slbl14d_Current.ForeColor = Color.Black;
                            slbl15a_Current.ForeColor = Color.Black;
                            slbl15aComments_Current.ForeColor = Color.Black;
                            slbl15b_Current.ForeColor = Color.Black;
                            slbl15bComments_Current.ForeColor = Color.Black;
                            slbl15c_Current.ForeColor = Color.Black;
                            slbl15cComments_Current.ForeColor = Color.Black;
                            slbl16a_Current.ForeColor = Color.Black;
                            slbl16b_Current.ForeColor = Color.Black;
                            slbl16bComments_Current.ForeColor = Color.Black;
                            slbl16c_Current.ForeColor = Color.Black;
                            slbl16cComments_Current.ForeColor = Color.Black;
                            slbl17a_Current.ForeColor = Color.Black;
                            slbl17b_Current.ForeColor = Color.Black;
                            slbl18_Current.ForeColor = Color.Black;
                            slbl18Comments_Current.ForeColor = Color.Black;
                            slbl19_Current.ForeColor = Color.Black;
                            slbl19Comments_Current.ForeColor = Color.Black;
                            slbl20a_Current.ForeColor = Color.Black;
                            slbl20aComments_Current.ForeColor = Color.Black;
                            slbl20b_Current.ForeColor = Color.Black;
                            slbl20bComments_Current.ForeColor = Color.Black;
                            slbl21_Current.ForeColor = Color.Black;
                            slbl21Comments_Current.ForeColor = Color.Black;
                            slbl22a_Current.ForeColor = Color.Black;
                            slbl22b_Current.ForeColor = Color.Black;
                            slbl22c_Current.ForeColor = Color.Black;
                            slbl22Comments_Current.ForeColor = Color.Black;
                            slbl23_Current.ForeColor = Color.Black;
                            slbl23Comments_Current.ForeColor = Color.Black;
                            slbl24_Current.ForeColor = Color.Black;
                            slbl24Comments_Current.ForeColor = Color.Black;
                            slbl25a_Current.ForeColor = Color.Black;
                            slbl25b_Current.ForeColor = Color.Black;
                            slbl25c_Current.ForeColor = Color.Black;
                            slbl26_Current.ForeColor = Color.Black;
                            slbl26Comments_Current.ForeColor = Color.Black;
                            slbl27_Current.ForeColor = Color.Black;
                            slbl27a_Current.ForeColor = Color.Black;
                            slbl27b_Current.ForeColor = Color.Black;
                            slbl27c_Current.ForeColor = Color.Black;
                            slbl27d_Current.ForeColor = Color.Black;
                            slbl27e_Current.ForeColor = Color.Black;
                            slbl28_Current.ForeColor = Color.Black;
                            slbl28a_Current.ForeColor = Color.Black;
                            slbl28b_Current.ForeColor = Color.Black;
                            slbl28c_Current.ForeColor = Color.Black;
                            slbl29i_Current.ForeColor = Color.Black;
                            slbl29ia_Current.ForeColor = Color.Black;
                            slbl29iaComments_Current.ForeColor = Color.Black;
                            slbl29ib_Current.ForeColor = Color.Black;
                            slbl29ibComments_Current.ForeColor = Color.Black;
                            slbl29ic_Current.ForeColor = Color.Black;
                            slbl29icComments_Current.ForeColor = Color.Black;
                            slbl29id_Current.ForeColor = Color.Black;
                            slbl29idComments_Current.ForeColor = Color.Black;
                            slbl29ii_Current.ForeColor = Color.Black;
                            slbl29iiComments_Current.ForeColor = Color.Black;
                            slbl30_1_Current.ForeColor = Color.Black;
                            slbl30_2_Current.ForeColor = Color.Black;
                            slbl30_3_Current.ForeColor = Color.Black;
                            slbl30abc_Current.ForeColor = Color.Black;
                        }
                    }
                    else
                    {
                        loadSupplierApproval(scb10Approval_Current, smb10ApprovalComments_Current, qNew.QuestionnaireId, "1");
                        loadSupplierApproval(scb11Approval_Current, smb11ApprovalComments_Current, qNew.QuestionnaireId, "11");
                        loadSupplierApproval(scb12Approval_Current, smb12ApprovalComments_Current, qNew.QuestionnaireId, "12");
                        loadSupplierApproval(scb13Approval_Current, smb13ApprovalComments_Current, qNew.QuestionnaireId, "13");
                        loadSupplierApproval(scb14Approval_Current, smb14ApprovalComments_Current, qNew.QuestionnaireId, "14");
                        loadSupplierApproval(scb15Approval_Current, smb15ApprovalComments_Current, qNew.QuestionnaireId, "15");
                        loadSupplierApproval(scb16Approval_Current, smb16ApprovalComments_Current, qNew.QuestionnaireId, "16");
                        loadSupplierApproval(scb17Approval_Current, smb17ApprovalComments_Current, qNew.QuestionnaireId, "17");
                        loadSupplierApproval(scb18Approval_Current, smb18ApprovalComments_Current, qNew.QuestionnaireId, "18");
                        loadSupplierApproval(scb19Approval_Current, smb19ApprovalComments_Current, qNew.QuestionnaireId, "19");
                        loadSupplierApproval(scb20Approval_Current, smb20ApprovalComments_Current, qNew.QuestionnaireId, "20");
                        loadSupplierApproval(scb21Approval_Current, smb21ApprovalComments_Current, qNew.QuestionnaireId, "21");
                        loadSupplierApproval(scb22Approval_Current, smb22ApprovalComments_Current, qNew.QuestionnaireId, "22");
                        loadSupplierApproval(scb23Approval_Current, smb23ApprovalComments_Current, qNew.QuestionnaireId, "23");
                        loadSupplierApproval(scb24Approval_Current, smb24ApprovalComments_Current, qNew.QuestionnaireId, "24");
                        loadSupplierApproval(scb25Approval_Current, smb25ApprovalComments_Current, qNew.QuestionnaireId, "25");
                        loadSupplierApproval(scb26Approval_Current, smb26ApprovalComments_Current, qNew.QuestionnaireId, "26");
                        loadSupplierApproval(scb27Approval_Current, smb27ApprovalComments_Current, qNew.QuestionnaireId, "27");
                        loadSupplierApproval(scb28Approval_Current, smb28ApprovalComments_Current, qNew.QuestionnaireId, "28");
                        loadSupplierApproval(scb29iApproval_Current, smb29iApprovalComments_Current, qNew.QuestionnaireId, "29i");
                        loadSupplierApproval(scb29iiApproval_Current, smb29iiApprovalComments_Current, qNew.QuestionnaireId, "29ii");
                        loadSupplierApproval(scb30Approval_Current, smb30ApprovalComments_Current, qNew.QuestionnaireId, "30");
                    }
                }

                /*
                 * Verification
                 */
                if (qOld.IsVerificationRequired == true && qNew.IsVerificationRequired == true)
                {
                    compare(vlbl1_1_Previous, vlbl1_1_Current, verificationvalue(qOld.QuestionnaireId, 1, 1), verificationvalue(qNew.QuestionnaireId, 1, 1));
                    compare(vlbl1_2_Previous, vlbl1_2_Current, verificationvalue(qOld.QuestionnaireId, 1, 2), verificationvalue(qNew.QuestionnaireId, 1, 2));
                    compare(vlbl1_3_Previous, vlbl1_3_Current, verificationvalue(qOld.QuestionnaireId, 1, 3), verificationvalue(qNew.QuestionnaireId, 1, 3));
                    compare(vlbl1_4_Previous, vlbl1_4_Current, verificationvalue(qOld.QuestionnaireId, 1, 4), verificationvalue(qNew.QuestionnaireId, 1, 4));
                    compareVerificationAttachment(vhl1_1_Previous, vhl1_1_Current, qOld.QuestionnaireId, qNew.QuestionnaireId, 1, 1);
                    compareVerificationAttachment(vhl1_2_Previous, vhl1_2_Current, qOld.QuestionnaireId, qNew.QuestionnaireId, 1, 2);
                    compareVerificationAttachment(vhl1_3_Previous, vhl1_3_Current, qOld.QuestionnaireId, qNew.QuestionnaireId, 1, 3);
                    compareVerificationAttachment(vhl1_4_Previous, vhl1_4_Current, qOld.QuestionnaireId, qNew.QuestionnaireId, 1, 4);

                    compare(vlbl2_1_Previous, vlbl2_1_Current, verificationvalue(qOld.QuestionnaireId, 2, 1), verificationvalue(qNew.QuestionnaireId, 2, 1));
                    compare(vlbl2_2_Previous, vlbl2_2_Current, verificationvalue(qOld.QuestionnaireId, 2, 2), verificationvalue(qNew.QuestionnaireId, 2, 2));
                    compare(vlbl2_3_Previous, vlbl2_3_Current, verificationvalue(qOld.QuestionnaireId, 2, 3), verificationvalue(qNew.QuestionnaireId, 2, 3));
                    compare(vlbl2_4_Previous, vlbl2_4_Current, verificationvalue(qOld.QuestionnaireId, 2, 4), verificationvalue(qNew.QuestionnaireId, 2, 4));
                    compare(vlbl2_5_Previous, vlbl2_5_Current, verificationvalue(qOld.QuestionnaireId, 2, 5), verificationvalue(qNew.QuestionnaireId, 2, 5));
                    compareVerificationAttachment(vhl2_1_Previous, vhl2_1_Current, qOld.QuestionnaireId, qNew.QuestionnaireId, 2, 1);
                    compareVerificationAttachment(vhl2_2_Previous, vhl2_2_Current, qOld.QuestionnaireId, qNew.QuestionnaireId, 2, 2);
                    compareVerificationAttachment(vhl2_3_Previous, vhl2_3_Current, qOld.QuestionnaireId, qNew.QuestionnaireId, 2, 3);
                    compareVerificationAttachment(vhl2_4_Previous, vhl2_4_Current, qOld.QuestionnaireId, qNew.QuestionnaireId, 2, 4);
                    compareVerificationAttachment(vhl2_5_Previous, vhl2_5_Current, qOld.QuestionnaireId, qNew.QuestionnaireId, 2, 5);

                    compare(vlbl3_1_Previous, vlbl3_1_Current, verificationvalue(qOld.QuestionnaireId, 4, 1), verificationvalue(qNew.QuestionnaireId, 4, 1));
                    compare(vlbl3_2_Previous, vlbl3_2_Current, verificationvalue(qOld.QuestionnaireId, 4, 2), verificationvalue(qNew.QuestionnaireId, 4, 2));
                    compare(vlbl3_3_Previous, vlbl3_3_Current, verificationvalue(qOld.QuestionnaireId, 4, 3), verificationvalue(qNew.QuestionnaireId, 4, 3));
                    compare(vlbl3_4_Previous, vlbl3_4_Current, verificationvalue(qOld.QuestionnaireId, 4, 4), verificationvalue(qNew.QuestionnaireId, 4, 4));
                    compare(vlbl3_5_Previous, vlbl3_5_Current, verificationvalue(qOld.QuestionnaireId, 4, 5), verificationvalue(qNew.QuestionnaireId, 4, 5));
                    compareVerificationAttachment(vhl3_1_Previous, vhl3_1_Current, qOld.QuestionnaireId, qNew.QuestionnaireId, 4, 1);
                    compareVerificationAttachment(vhl3_2_Previous, vhl3_2_Current, qOld.QuestionnaireId, qNew.QuestionnaireId, 4, 2);
                    compareVerificationAttachment(vhl3_3_Previous, vhl3_3_Current, qOld.QuestionnaireId, qNew.QuestionnaireId, 4, 3);
                    compareVerificationAttachment(vhl3_4_Previous, vhl3_4_Current, qOld.QuestionnaireId, qNew.QuestionnaireId, 4, 4);
                    compareVerificationAttachment(vhl3_5_Previous, vhl3_5_Current, qOld.QuestionnaireId, qNew.QuestionnaireId, 4, 5);

                    compare(vlbl4_1_Previous, vlbl4_1_Current, verificationvalue(qOld.QuestionnaireId, 5, 1), verificationvalue(qNew.QuestionnaireId, 5, 1));
                    compare(vlbl4_2_Previous, vlbl4_2_Current, verificationvalue(qOld.QuestionnaireId, 5, 2), verificationvalue(qNew.QuestionnaireId, 5, 2));
                    compare(vlbl4_3_Previous, vlbl4_3_Current, verificationvalue(qOld.QuestionnaireId, 5, 3), verificationvalue(qNew.QuestionnaireId, 5, 3));
                    compare(vlbl4_4_Previous, vlbl4_4_Current, verificationvalue(qOld.QuestionnaireId, 5, 4), verificationvalue(qNew.QuestionnaireId, 5, 4));
                    compare(vlbl4_5_Previous, vlbl4_5_Current, verificationvalue(qOld.QuestionnaireId, 5, 5), verificationvalue(qNew.QuestionnaireId, 5, 5));
                    compare(vlbl4_6_Previous, vlbl4_6_Current, verificationvalue(qOld.QuestionnaireId, 5, 6), verificationvalue(qNew.QuestionnaireId, 5, 6));
                    compareVerificationAttachment(vhl4_1_Previous, vhl4_1_Current, qOld.QuestionnaireId, qNew.QuestionnaireId, 5, 1);
                    compareVerificationAttachment(vhl4_2_Previous, vhl4_2_Current, qOld.QuestionnaireId, qNew.QuestionnaireId, 5, 2);
                    compareVerificationAttachment(vhl4_3_Previous, vhl4_3_Current, qOld.QuestionnaireId, qNew.QuestionnaireId, 5, 3);
                    compareVerificationAttachment(vhl4_4_Previous, vhl4_4_Current, qOld.QuestionnaireId, qNew.QuestionnaireId, 5, 4);
                    compareVerificationAttachment(vhl4_5_Previous, vhl4_5_Current, qOld.QuestionnaireId, qNew.QuestionnaireId, 5, 5);
                    compareVerificationAttachment(vhl4_6_Previous, vhl4_6_Current, qOld.QuestionnaireId, qNew.QuestionnaireId, 5, 6);

                    compare(vlbl5_1_Previous, vlbl5_1_Current, verificationvalue(qOld.QuestionnaireId, 6, 1), verificationvalue(qNew.QuestionnaireId, 6, 1));
                    compare(vlbl5_2_Previous, vlbl5_2_Current, verificationvalue(qOld.QuestionnaireId, 6, 2), verificationvalue(qNew.QuestionnaireId, 6, 2));
                    compare(vlbl5_3_Previous, vlbl5_3_Current, verificationvalue(qOld.QuestionnaireId, 6, 3), verificationvalue(qNew.QuestionnaireId, 6, 3));
                    compare(vlbl5_4_Previous, vlbl5_4_Current, verificationvalue(qOld.QuestionnaireId, 6, 4), verificationvalue(qNew.QuestionnaireId, 6, 4));
                    compareVerificationAttachment(vhl5_1_Previous, vhl5_1_Current, qOld.QuestionnaireId, qNew.QuestionnaireId, 6, 1);
                    compareVerificationAttachment(vhl5_2_Previous, vhl5_2_Current, qOld.QuestionnaireId, qNew.QuestionnaireId, 6, 2);
                    compareVerificationAttachment(vhl5_3_Previous, vhl5_3_Current, qOld.QuestionnaireId, qNew.QuestionnaireId, 6, 3);
                    compareVerificationAttachment(vhl5_4_Previous, vhl5_4_Current, qOld.QuestionnaireId, qNew.QuestionnaireId, 6, 4);

                    compare(vlbl6_1_Previous, vlbl6_1_Current, verificationvalue(qOld.QuestionnaireId, 7, 1), verificationvalue(qNew.QuestionnaireId, 7, 1));
                    compare(vlbl6_2_Previous, vlbl6_2_Current, verificationvalue(qOld.QuestionnaireId, 7, 2), verificationvalue(qNew.QuestionnaireId, 7, 2));
                    compare(vlbl6_3_Previous, vlbl6_3_Current, verificationvalue(qOld.QuestionnaireId, 7, 3), verificationvalue(qNew.QuestionnaireId, 7, 3));
                    compareVerificationAttachment(vhl6_1_Previous, vhl6_1_Current, qOld.QuestionnaireId, qNew.QuestionnaireId, 7, 1);
                    compareVerificationAttachment(vhl6_2_Previous, vhl6_2_Current, qOld.QuestionnaireId, qNew.QuestionnaireId, 7, 2);
                    compareVerificationAttachment(vhl6_3_Previous, vhl6_3_Current, qOld.QuestionnaireId, qNew.QuestionnaireId, 7, 3);

                    compare(vlbl7_1_Previous, vlbl7_1_Current, verificationvalue(qOld.QuestionnaireId, 8, 1), verificationvalue(qNew.QuestionnaireId, 8, 1));
                    compare(vlbl7_2_Previous, vlbl7_2_Current, verificationvalue(qOld.QuestionnaireId, 8, 2), verificationvalue(qNew.QuestionnaireId, 8, 2));
                    compare(vlbl7_3_Previous, vlbl7_3_Current, verificationvalue(qOld.QuestionnaireId, 8, 4), verificationvalue(qNew.QuestionnaireId, 8, 4));
                    compareVerificationAttachment(vhl7_1_Previous, vhl7_1_Current, qOld.QuestionnaireId, qNew.QuestionnaireId, 8, 1);
                    compareVerificationAttachment(vhl7_2_Previous, vhl7_2_Current, qOld.QuestionnaireId, qNew.QuestionnaireId, 8, 2);
                    compareVerificationAttachment(vhl7_3_Previous, vhl7_3_Current, qOld.QuestionnaireId, qNew.QuestionnaireId, 8, 4);

                    if (!qNew.IsVerificationRequired)
                    {
                        ApprovalVerificationHide();

                        vlbl1_1_Previous.ForeColor = Color.Black;
                        vlbl1_2_Previous.ForeColor = Color.Black;
                        vlbl1_3_Previous.ForeColor = Color.Black;
                        vlbl1_4_Previous.ForeColor = Color.Black;
                        vlbl2_1_Previous.ForeColor = Color.Black;
                        vlbl2_2_Previous.ForeColor = Color.Black;
                        vlbl2_3_Previous.ForeColor = Color.Black;
                        vlbl2_4_Previous.ForeColor = Color.Black;
                        vlbl2_5_Previous.ForeColor = Color.Black;
                        vlbl3_1_Previous.ForeColor = Color.Black;
                        vlbl3_2_Previous.ForeColor = Color.Black;
                        vlbl3_3_Previous.ForeColor = Color.Black;
                        vlbl3_4_Previous.ForeColor = Color.Black;
                        vlbl3_5_Previous.ForeColor = Color.Black;
                        vlbl4_1_Previous.ForeColor = Color.Black;
                        vlbl4_2_Previous.ForeColor = Color.Black;
                        vlbl4_3_Previous.ForeColor = Color.Black;
                        vlbl4_4_Previous.ForeColor = Color.Black;
                        vlbl4_5_Previous.ForeColor = Color.Black;
                        vlbl4_6_Previous.ForeColor = Color.Black;
                        vlbl5_1_Previous.ForeColor = Color.Black;
                        vlbl5_2_Previous.ForeColor = Color.Black;
                        vlbl5_3_Previous.ForeColor = Color.Black;
                        vlbl5_4_Previous.ForeColor = Color.Black;
                        vlbl6_1_Previous.ForeColor = Color.Black;
                        vlbl6_2_Previous.ForeColor = Color.Black;
                        vlbl6_3_Previous.ForeColor = Color.Black;
                        vlbl7_1_Previous.ForeColor = Color.Black;
                        vlbl7_2_Previous.ForeColor = Color.Black;
                        vlbl7_3_Previous.ForeColor = Color.Black;

                        if (qOld.IsVerificationRequired)
                        {
                            vlbl1_1_Current.ForeColor = Color.Black;
                            vlbl1_2_Current.ForeColor = Color.Black;
                            vlbl1_3_Current.ForeColor = Color.Black;
                            vlbl1_4_Current.ForeColor = Color.Black;
                            vlbl2_1_Current.ForeColor = Color.Black;
                            vlbl2_2_Current.ForeColor = Color.Black;
                            vlbl2_3_Current.ForeColor = Color.Black;
                            vlbl2_4_Current.ForeColor = Color.Black;
                            vlbl2_5_Current.ForeColor = Color.Black;
                            vlbl3_1_Current.ForeColor = Color.Black;
                            vlbl3_2_Current.ForeColor = Color.Black;
                            vlbl3_3_Current.ForeColor = Color.Black;
                            vlbl3_4_Current.ForeColor = Color.Black;
                            vlbl3_5_Current.ForeColor = Color.Black;
                            vlbl4_1_Current.ForeColor = Color.Black;
                            vlbl4_2_Current.ForeColor = Color.Black;
                            vlbl4_3_Current.ForeColor = Color.Black;
                            vlbl4_4_Current.ForeColor = Color.Black;
                            vlbl4_5_Current.ForeColor = Color.Black;
                            vlbl4_6_Current.ForeColor = Color.Black;
                            vlbl5_1_Current.ForeColor = Color.Black;
                            vlbl5_2_Current.ForeColor = Color.Black;
                            vlbl5_3_Current.ForeColor = Color.Black;
                            vlbl5_4_Current.ForeColor = Color.Black;
                            vlbl6_1_Current.ForeColor = Color.Black;
                            vlbl6_2_Current.ForeColor = Color.Black;
                            vlbl6_3_Current.ForeColor = Color.Black;
                            vlbl7_1_Current.ForeColor = Color.Black;
                            vlbl7_2_Current.ForeColor = Color.Black;
                            vlbl7_3_Current.ForeColor = Color.Black;
                        }
                    }
                    else
                    {
                        loadVerificationApproval(vcb1_1_Approval_Current, vmb1_1_Approval_Current, qNew.QuestionnaireId, 1, 1);
                        loadVerificationApproval(vcb1_2_Approval_Current, vmb1_2_Approval_Current, qNew.QuestionnaireId, 1, 2);
                        loadVerificationApproval(vcb1_3_Approval_Current, vmb1_3_Approval_Current, qNew.QuestionnaireId, 1, 3);
                        loadVerificationApproval(vcb1_4_Approval_Current, vmb1_4_Approval_Current, qNew.QuestionnaireId, 1, 4);
                        loadVerificationApproval(vcb2_1_Approval_Current, vmb2_1_Approval_Current, qNew.QuestionnaireId, 2, 1);
                        loadVerificationApproval(vcb2_2_Approval_Current, vmb2_2_Approval_Current, qNew.QuestionnaireId, 2, 2);
                        loadVerificationApproval(vcb2_3_Approval_Current, vmb2_3_Approval_Current, qNew.QuestionnaireId, 2, 3);
                        loadVerificationApproval(vcb2_4_Approval_Current, vmb2_4_Approval_Current, qNew.QuestionnaireId, 2, 4);
                        loadVerificationApproval(vcb2_5_Approval_Current, vmb2_5_Approval_Current, qNew.QuestionnaireId, 2, 5);
                        loadVerificationApproval(vcb3_1_Approval_Current, vmb3_1_Approval_Current, qNew.QuestionnaireId, 4, 1);
                        loadVerificationApproval(vcb3_2_Approval_Current, vmb3_2_Approval_Current, qNew.QuestionnaireId, 4, 2);
                        loadVerificationApproval(vcb3_3_Approval_Current, vmb3_3_Approval_Current, qNew.QuestionnaireId, 4, 3);
                        loadVerificationApproval(vcb3_4_Approval_Current, vmb3_4_Approval_Current, qNew.QuestionnaireId, 4, 4);
                        loadVerificationApproval(vcb3_5_Approval_Current, vmb3_5_Approval_Current, qNew.QuestionnaireId, 4, 5);
                        loadVerificationApproval(vcb4_1_Approval_Current, vmb4_1_Approval_Current, qNew.QuestionnaireId, 5, 1);
                        loadVerificationApproval(vcb4_2_Approval_Current, vmb4_2_Approval_Current, qNew.QuestionnaireId, 5, 2);
                        loadVerificationApproval(vcb4_3_Approval_Current, vmb4_3_Approval_Current, qNew.QuestionnaireId, 5, 3);
                        loadVerificationApproval(vcb4_4_Approval_Current, vmb4_4_Approval_Current, qNew.QuestionnaireId, 5, 4);
                        loadVerificationApproval(vcb4_5_Approval_Current, vmb4_5_Approval_Current, qNew.QuestionnaireId, 5, 5);
                        loadVerificationApproval(vcb4_6_Approval_Current, vmb4_6_Approval_Current, qNew.QuestionnaireId, 5, 6);
                        loadVerificationApproval(vcb5_1_Approval_Current, vmb5_1_Approval_Current, qNew.QuestionnaireId, 6, 1);
                        loadVerificationApproval(vcb5_2_Approval_Current, vmb5_2_Approval_Current, qNew.QuestionnaireId, 6, 2);
                        loadVerificationApproval(vcb5_3_Approval_Current, vmb5_3_Approval_Current, qNew.QuestionnaireId, 6, 3);
                        loadVerificationApproval(vcb5_4_Approval_Current, vmb5_4_Approval_Current, qNew.QuestionnaireId, 6, 4);
                        loadVerificationApproval(vcb6_1_Approval_Current, vmb6_1_Approval_Current, qNew.QuestionnaireId, 7, 1);
                        loadVerificationApproval(vcb6_2_Approval_Current, vmb6_2_Approval_Current, qNew.QuestionnaireId, 7, 2);
                        loadVerificationApproval(vcb6_3_Approval_Current, vmb6_3_Approval_Current, qNew.QuestionnaireId, 7, 3);
                        loadVerificationApproval(vcb7_1_Approval_Current, vmb7_1_Approval_Current, qNew.QuestionnaireId, 8, 1);
                        loadVerificationApproval(vcb7_2_Approval_Current, vmb7_2_Approval_Current, qNew.QuestionnaireId, 8, 2);
                        loadVerificationApproval(vcb7_3_Approval_Current, vmb7_3_Approval_Current, qNew.QuestionnaireId, 8, 3);
                    }


                    ASPxPageControl1.TabPages[3].Enabled = true;
                }
            }
            else
            {
                errorOccured("No previous questionnaire of status 'Assessment Complete' exists to compare against.");
            }
        }
        catch (Exception ex)
        {
            Elmah.ErrorSignal.FromContext(Context).Raise(ex);
            errorOccured(ex.Message.ToString());
        }
    }

    protected static string getProcurementValue(int questionnaireId, string questionId)
    {
        string value;
        QuestionnaireInitialResponseService qirService = new QuestionnaireInitialResponseService();
        QuestionnaireInitialResponse qir = qirService.GetByQuestionnaireIdQuestionId(questionnaireId, questionId);
        if (qir != null)
            value = qir.AnswerText;
        else
            value = "";
        return value;
    }
    protected static string getProcurementValue_User(int questionnaireId, string questionId)
    {
        string value;
        QuestionnaireInitialResponseService qirService = new QuestionnaireInitialResponseService();
        QuestionnaireInitialResponse qir = qirService.GetByQuestionnaireIdQuestionId(questionnaireId, questionId);
        if (qir != null)
            value = UserFullName_ByUserId(Convert.ToInt32(qir.AnswerText));
        else
            value = " ";
        return value;

    }

    protected static string UserFullName_ByUserId(int u)
    {
        string name;
        UsersService uService = new UsersService();
        Users _u = uService.GetByUserId(u);
        if (_u != null)
            name = String.Format("{0}, {1}", _u.LastName, _u.FirstName);
        else
            name = "";
        return name;
    }
    protected static string EhsConsultantFullName_ByCompanyId(int c)
    {
        string name = "";
        CompaniesService cService = new CompaniesService();
        Companies _c = cService.GetByCompanyId(c);

        EhsConsultantService eService = new EhsConsultantService();
        if (_c.EhsConsultantId != null)
        {
            EhsConsultant e = eService.GetByEhsConsultantId(Convert.ToInt32(_c.EhsConsultantId));

            name = UserFullName_ByUserId(e.UserId);
        }
        return name;
    }

    protected void compare(ASPxLabel lblPrevious, ASPxLabel lblCurrent)
    {
        lblPrevious.Text = PreviousAnswer;
        lblCurrent.Text = CurrentAnswer;
        if (lblPrevious.Text != lblCurrent.Text) { lblPrevious.ForeColor = Color.Gray; lblCurrent.ForeColor = Color.Red; };
        PreviousAnswer = "";
        CurrentAnswer = "";
    }
    protected void compare(ASPxLabel lblPrevious, ASPxLabel lblCurrent, string _PreviousAnswer, string _CurrentAnswer)
    {
        lblPrevious.Text = _PreviousAnswer;
        lblCurrent.Text = _CurrentAnswer;
        if (lblPrevious.Text != lblCurrent.Text) { lblPrevious.ForeColor = Color.Gray; lblCurrent.ForeColor = Color.Red; };
        PreviousAnswer = "";
        CurrentAnswer = "";
    }
    protected static void loadSupplierApproval(ASPxComboBox cbCurrent, ASPxMemo mbCurrent, int QuestionnaireId_Current, string QuestionNo)
    {
        QuestionnaireMainAssessmentService qmaService = new QuestionnaireMainAssessmentService();
        QuestionnaireMainAssessment qmaCurrent = qmaService.GetByQuestionnaireIdQuestionNo(QuestionnaireId_Current, QuestionNo);

        if (qmaCurrent != null)
        {
            cbCurrent.Value = NumberUtilities.Convert.BoolToInt(qmaCurrent.AssessorApproval);
            mbCurrent.Text = qmaCurrent.AssessorComment;
        }
    }
    protected static void loadVerificationApproval(ASPxComboBox cbCurrent, ASPxMemo mbCurrent, int QuestionnaireId_Current, int SectionId, int QuestionId)
    {
        QuestionnaireVerificationAssessmentService qvaService = new QuestionnaireVerificationAssessmentService();
        QuestionnaireVerificationAssessment qvaCurrent = qvaService.GetByQuestionnaireIdSectionIdQuestionId(QuestionnaireId_Current, SectionId, QuestionId);

        if (qvaCurrent != null)
        {
            cbCurrent.Value = NumberUtilities.Convert.BoolToInt(qvaCurrent.AssessorApproval);
            mbCurrent.Text = qvaCurrent.AssessorComment;
        }
    }

    protected static void compareSupplierAttachment(ASPxHyperLink hlPrevious, ASPxHyperLink hlCurrent, int QuestionnaireId_Previous, int QuestionnaireId_Current, int AnswerId)
    {
        QuestionnaireMainAttachmentService qas = new QuestionnaireMainAttachmentService();
        QuestionnaireMainAttachment qaPrevious = qas.GetByQuestionnaireIdAnswerId(QuestionnaireId_Previous, AnswerId);
        QuestionnaireMainAttachment qaCurrent = qas.GetByQuestionnaireIdAnswerId(QuestionnaireId_Current, AnswerId);
        hlPrevious.Text = "(No File Uploaded)";
        hlCurrent.Text = "(No File Uploaded)";
        hlPrevious.ForeColor = Color.Black;
        hlCurrent.ForeColor = Color.Black;

        if (qaPrevious != null)
        {
            MD5 md5 = MD5.Create();
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < qaPrevious.FileHash.Length; i++)
            {
                sb.Append(qaPrevious.FileHash[i].ToString("X2"));
            }

            hlPrevious.Text = String.Format("{0} ({1})", qaPrevious.FileName, sb);
            hlPrevious.NavigateUrl = String.Format("~/Common/GetExample.aspx?ID={0}", qaPrevious.AttachmentId);
            hlPrevious.Target = "_blank";
        }
        if (qaCurrent != null)
        {
            MD5 md5 = MD5.Create();
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < qaCurrent.FileHash.Length; i++)
            {
                sb.Append(qaCurrent.FileHash[i].ToString("X2"));
            }

            hlCurrent.Text = String.Format("{0} ({1})", qaCurrent.FileName, sb);
            hlCurrent.NavigateUrl = String.Format("~/Common/GetExample.aspx?ID={0}", qaCurrent.AttachmentId);
            hlCurrent.Target = "_blank";
        }
        if (hlPrevious.Text != hlCurrent.Text) { hlPrevious.ForeColor = Color.Gray; hlCurrent.ForeColor = Color.Red; };
    }
    protected static void compareVerificationAttachment(ASPxHyperLink hlPrevious, ASPxHyperLink hlCurrent, int QuestionnaireId_Previous, int QuestionnaireId_Current, int SectionId, int QuestionId)
    {
        QuestionnaireVerificationAttachmentService qvas = new QuestionnaireVerificationAttachmentService();
        QuestionnaireVerificationAttachment qvaPrevious = qvas.GetByQuestionnaireIdSectionIdQuestionId(QuestionnaireId_Previous, SectionId, QuestionId);
        QuestionnaireVerificationAttachment qvaCurrent = qvas.GetByQuestionnaireIdSectionIdQuestionId(QuestionnaireId_Current, SectionId, QuestionId);
        hlPrevious.Text = "(No File Uploaded)";
        hlCurrent.Text = "(No File Uploaded)";
        hlPrevious.ForeColor = Color.Black;
        hlCurrent.ForeColor = Color.Black;

        if (qvaPrevious != null)
        {
            MD5 md5 = MD5.Create();
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < qvaPrevious.FileHash.Length; i++)
            {
                sb.Append(qvaPrevious.FileHash[i].ToString("X2"));
            }

            hlPrevious.Text = String.Format("{0} ({1})", qvaPrevious.FileName, sb);
            hlPrevious.NavigateUrl = String.Format("~/Common/GetExample2.aspx?ID={0}", qvaPrevious.AttachmentId);
            hlPrevious.Target = "_blank";
        }
        if (qvaCurrent != null)
        {
            MD5 md5 = MD5.Create();
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < qvaCurrent.FileHash.Length; i++)
            {
                sb.Append(qvaCurrent.FileHash[i].ToString("X2"));
            }

            hlCurrent.Text = String.Format("{0} ({1})", qvaCurrent.FileName, sb);
            hlCurrent.NavigateUrl = String.Format("~/Common/GetExample2.aspx?ID={0}", qvaCurrent.AttachmentId);
            hlCurrent.Target = "_blank";
        }
        QuestionnaireService qService = new QuestionnaireService();
        Questionnaire qNew = qService.GetByQuestionnaireId(QuestionnaireId_Current);
        if (qNew.IsVerificationRequired)
        {
            if (hlPrevious.Text != hlCurrent.Text) { hlPrevious.ForeColor = Color.Gray; hlCurrent.ForeColor = Color.Red; };
        }
        else
        {
            if (hlCurrent.Text == "(No File Uploaded)") hlCurrent.Visible = false;
        }
    }
    protected static string supplierselectedoption(string options, int q, string QuestionId)
    {
        string _selected = "";
        bool sel;
        if (options.Length == 1)
            sel = true;
        else
            sel = false;
        if (options.Contains("A"))
        {
            if (Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId(QuestionId, "A")) == "Yes") { _selected = "A"; };
        }
        if (options.Contains("B"))
        {
            if (Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId(QuestionId, "B")) == "Yes") { _selected = "B"; };
        }
        if (options.Contains("C"))
        {
            if (Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId(QuestionId, "C")) == "Yes") { _selected = "C"; };
        }
        if (options.Contains("D"))
        {
            if (Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId(QuestionId, "D")) == "Yes") { _selected = "D"; };
        }
        if (options.Contains("E"))
        {
            if (Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId(QuestionId, "E")) == "Yes") { _selected = "E"; };
        }
        if (options.Contains("F"))
        {
            if (Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId(QuestionId, "F")) == "Yes") { _selected = "F"; };
        }

        if (sel)
        {
            if (!String.IsNullOrEmpty(_selected))
            {
                _selected = "Yes";
            }
            else
            {
                _selected = "No";
            }
        }
        return _selected;
    }

    protected static string verificationvalue(int q, int sectionId, int questionId)
    {
        string a;
        if (!String.IsNullOrEmpty(Helper.Questionnaire.Supplier.Verification.LoadAnswerText(q, sectionId, questionId)))
            a = Helper.Questionnaire.Supplier.Verification.LoadAnswerText(q, sectionId, questionId);
        else
            a = "";
        return a;
        //qvHelper.LoadAnswerByte(q, sNo, 1);
    }
    protected static string verificationvalue_comments(int q, int sectionId, int questionId)
    {
        string comment;
        QuestionnaireVerificationResponseService qrs = new QuestionnaireVerificationResponseService();
        QuestionnaireVerificationResponse qr = new QuestionnaireVerificationResponse();
        qr = qrs.GetByQuestionnaireIdSectionIdQuestionId(q, sectionId, questionId);
        if (qr != null)
            comment = Encoding.ASCII.GetString(qr.AdditionalEvidence);
        else
            comment = "";
        return comment;
    }

    protected void ApprovalSupplierHide()
    {
        scb10Approval_Current.Visible = false;
        scb11Approval_Current.Visible = false;
        scb12Approval_Current.Visible = false;
        scb13Approval_Current.Visible = false;
        scb14Approval_Current.Visible = false;
        scb15Approval_Current.Visible = false;
        scb16Approval_Current.Visible = false;
        scb17Approval_Current.Visible = false;
        scb18Approval_Current.Visible = false;
        scb19Approval_Current.Visible = false;
        scb20Approval_Current.Visible = false;
        scb21Approval_Current.Visible = false;
        scb22Approval_Current.Visible = false;
        scb23Approval_Current.Visible = false;
        scb24Approval_Current.Visible = false;
        scb25Approval_Current.Visible = false;
        scb26Approval_Current.Visible = false;
        scb27Approval_Current.Visible = false;
        scb28Approval_Current.Visible = false;
        scb29iApproval_Current.Visible = false;
        scb29iiApproval_Current.Visible = false;
        scb30Approval_Current.Visible = false;
        smb10ApprovalComments_Current.Visible = false;
        smb11ApprovalComments_Current.Visible = false;
        smb12ApprovalComments_Current.Visible = false;
        smb13ApprovalComments_Current.Visible = false;
        smb14ApprovalComments_Current.Visible = false;
        smb15ApprovalComments_Current.Visible = false;
        smb16ApprovalComments_Current.Visible = false;
        smb17ApprovalComments_Current.Visible = false;
        smb18ApprovalComments_Current.Visible = false;
        smb19ApprovalComments_Current.Visible = false;
        smb20ApprovalComments_Current.Visible = false;
        smb21ApprovalComments_Current.Visible = false;
        smb22ApprovalComments_Current.Visible = false;
        smb23ApprovalComments_Current.Visible = false;
        smb24ApprovalComments_Current.Visible = false;
        smb25ApprovalComments_Current.Visible = false;
        smb26ApprovalComments_Current.Visible = false;
        smb27ApprovalComments_Current.Visible = false;
        smb28ApprovalComments_Current.Visible = false;
        smb29iApprovalComments_Current.Visible = false;
        smb29iiApprovalComments_Current.Visible = false;
        smb30ApprovalComments_Current.Visible = false;
    }
    protected void ApprovalVerificationHide()
    {
        vcb1_1_Approval_Current.Visible = false;
        vcb1_2_Approval_Current.Visible = false;
        vcb1_3_Approval_Current.Visible = false;
        vcb1_4_Approval_Current.Visible = false;
        vcb2_1_Approval_Current.Visible = false;
        vcb2_2_Approval_Current.Visible = false;
        vcb2_3_Approval_Current.Visible = false;
        vcb2_4_Approval_Current.Visible = false;
        vcb2_5_Approval_Current.Visible = false;
        vcb3_1_Approval_Current.Visible = false;
        vcb3_2_Approval_Current.Visible = false;
        vcb3_3_Approval_Current.Visible = false;
        vcb3_4_Approval_Current.Visible = false;
        vcb3_5_Approval_Current.Visible = false;
        vcb4_1_Approval_Current.Visible = false;
        vcb4_2_Approval_Current.Visible = false;
        vcb4_3_Approval_Current.Visible = false;
        vcb4_4_Approval_Current.Visible = false;
        vcb4_5_Approval_Current.Visible = false;
        vcb4_6_Approval_Current.Visible = false;
        vcb5_1_Approval_Current.Visible = false;
        vcb5_2_Approval_Current.Visible = false;
        vcb5_3_Approval_Current.Visible = false;
        vcb5_4_Approval_Current.Visible = false;
        vcb6_1_Approval_Current.Visible = false;
        vcb6_2_Approval_Current.Visible = false;
        vcb6_3_Approval_Current.Visible = false;
        vcb7_1_Approval_Current.Visible = false;
        vcb7_2_Approval_Current.Visible = false;
        vcb7_3_Approval_Current.Visible = false;
        vmb1_1_Approval_Current.Visible = false;
        vmb1_2_Approval_Current.Visible = false;
        vmb1_3_Approval_Current.Visible = false;
        vmb1_4_Approval_Current.Visible = false;
        vmb2_1_Approval_Current.Visible = false;
        vmb2_2_Approval_Current.Visible = false;
        vmb2_3_Approval_Current.Visible = false;
        vmb2_4_Approval_Current.Visible = false;
        vmb2_5_Approval_Current.Visible = false;
        vmb3_1_Approval_Current.Visible = false;
        vmb3_2_Approval_Current.Visible = false;
        vmb3_3_Approval_Current.Visible = false;
        vmb3_4_Approval_Current.Visible = false;
        vmb3_5_Approval_Current.Visible = false;
        vmb4_1_Approval_Current.Visible = false;
        vmb4_2_Approval_Current.Visible = false;
        vmb4_3_Approval_Current.Visible = false;
        vmb4_4_Approval_Current.Visible = false;
        vmb4_5_Approval_Current.Visible = false;
        vmb4_6_Approval_Current.Visible = false;
        vmb5_1_Approval_Current.Visible = false;
        vmb5_2_Approval_Current.Visible = false;
        vmb5_3_Approval_Current.Visible = false;
        vmb5_4_Approval_Current.Visible = false;
        vmb6_1_Approval_Current.Visible = false;
        vmb6_2_Approval_Current.Visible = false;
        vmb6_3_Approval_Current.Visible = false;
        vmb7_1_Approval_Current.Visible = false;
        vmb7_2_Approval_Current.Visible = false;
        vmb7_3_Approval_Current.Visible = false;
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (Page.IsPostBack)
        {
            if (auth.RoleId == (int)RoleList.Administrator || Helper.General.isEhsConsultant(auth.UserId))
            {
                QuestionnaireService qService = new QuestionnaireService();
                CompaniesService cService = new CompaniesService();
                UsersService uService = new UsersService();
                int QuestionnaireId = Convert.ToInt32(Request.QueryString["q"].ToString());
                Questionnaire qNew = qService.GetByQuestionnaireId(QuestionnaireId);

                if (qNew != null)
                {
                    if (scb10Approval_Current.Visible == true)
                    {
                        Helper.Questionnaire.Supplier.Main.SaveAssessment(QuestionnaireId, "1", scb10Approval_Current.Value, smb10ApprovalComments_Current.Text, auth.UserId);
                        Helper.Questionnaire.Supplier.Main.SaveAssessment(QuestionnaireId, "11", scb11Approval_Current.Value, smb11ApprovalComments_Current.Text, auth.UserId);
                        Helper.Questionnaire.Supplier.Main.SaveAssessment(QuestionnaireId, "12", scb12Approval_Current.Value, smb12ApprovalComments_Current.Text, auth.UserId);
                        Helper.Questionnaire.Supplier.Main.SaveAssessment(QuestionnaireId, "13", scb13Approval_Current.Value, smb13ApprovalComments_Current.Text, auth.UserId);
                        Helper.Questionnaire.Supplier.Main.SaveAssessment(QuestionnaireId, "14", scb14Approval_Current.Value, smb14ApprovalComments_Current.Text, auth.UserId);
                        Helper.Questionnaire.Supplier.Main.SaveAssessment(QuestionnaireId, "15", scb15Approval_Current.Value, smb15ApprovalComments_Current.Text, auth.UserId);
                        Helper.Questionnaire.Supplier.Main.SaveAssessment(QuestionnaireId, "16", scb16Approval_Current.Value, smb16ApprovalComments_Current.Text, auth.UserId);
                        Helper.Questionnaire.Supplier.Main.SaveAssessment(QuestionnaireId, "17", scb17Approval_Current.Value, smb17ApprovalComments_Current.Text, auth.UserId);
                        Helper.Questionnaire.Supplier.Main.SaveAssessment(QuestionnaireId, "18", scb18Approval_Current.Value, smb18ApprovalComments_Current.Text, auth.UserId);
                        Helper.Questionnaire.Supplier.Main.SaveAssessment(QuestionnaireId, "19", scb19Approval_Current.Value, smb19ApprovalComments_Current.Text, auth.UserId);
                        Helper.Questionnaire.Supplier.Main.SaveAssessment(QuestionnaireId, "20", scb20Approval_Current.Value, smb20ApprovalComments_Current.Text, auth.UserId);
                        Helper.Questionnaire.Supplier.Main.SaveAssessment(QuestionnaireId, "21", scb21Approval_Current.Value, smb21ApprovalComments_Current.Text, auth.UserId);
                        Helper.Questionnaire.Supplier.Main.SaveAssessment(QuestionnaireId, "22", scb22Approval_Current.Value, smb22ApprovalComments_Current.Text, auth.UserId);
                        Helper.Questionnaire.Supplier.Main.SaveAssessment(QuestionnaireId, "23", scb23Approval_Current.Value, smb23ApprovalComments_Current.Text, auth.UserId);
                        Helper.Questionnaire.Supplier.Main.SaveAssessment(QuestionnaireId, "24", scb24Approval_Current.Value, smb24ApprovalComments_Current.Text, auth.UserId);
                        Helper.Questionnaire.Supplier.Main.SaveAssessment(QuestionnaireId, "25", scb25Approval_Current.Value, smb25ApprovalComments_Current.Text, auth.UserId);
                        Helper.Questionnaire.Supplier.Main.SaveAssessment(QuestionnaireId, "26", scb26Approval_Current.Value, smb26ApprovalComments_Current.Text, auth.UserId);
                        Helper.Questionnaire.Supplier.Main.SaveAssessment(QuestionnaireId, "27", scb27Approval_Current.Value, smb27ApprovalComments_Current.Text, auth.UserId);
                        Helper.Questionnaire.Supplier.Main.SaveAssessment(QuestionnaireId, "28", scb28Approval_Current.Value, smb28ApprovalComments_Current.Text, auth.UserId);
                        Helper.Questionnaire.Supplier.Main.SaveAssessment(QuestionnaireId, "29i", scb29iApproval_Current.Value, smb29iApprovalComments_Current.Text, auth.UserId);
                        Helper.Questionnaire.Supplier.Main.SaveAssessment(QuestionnaireId, "29ii", scb29iiApproval_Current.Value, smb29iiApprovalComments_Current.Text, auth.UserId);
                        Helper.Questionnaire.Supplier.Main.SaveAssessment(QuestionnaireId, "30", scb30Approval_Current.Value, smb30ApprovalComments_Current.Text, auth.UserId);
                    }
                    if (vcb1_1_Approval_Current.Visible == true)
                    {
                        Helper.Questionnaire.Supplier.Verification.SaveAssessment(QuestionnaireId, 1, 1, vcb1_1_Approval_Current.Value, vmb1_1_Approval_Current.Text, auth.UserId);
                        Helper.Questionnaire.Supplier.Verification.SaveAssessment(QuestionnaireId, 1, 2, vcb1_2_Approval_Current.Value, vmb1_2_Approval_Current.Text, auth.UserId);
                        Helper.Questionnaire.Supplier.Verification.SaveAssessment(QuestionnaireId, 1, 3, vcb1_3_Approval_Current.Value, vmb1_3_Approval_Current.Text, auth.UserId);
                        Helper.Questionnaire.Supplier.Verification.SaveAssessment(QuestionnaireId, 1, 4, vcb1_4_Approval_Current.Value, vmb1_4_Approval_Current.Text, auth.UserId);

                        Helper.Questionnaire.Supplier.Verification.SaveAssessment(QuestionnaireId, 2, 1, vcb2_1_Approval_Current.Value, vmb2_1_Approval_Current.Text, auth.UserId);
                        Helper.Questionnaire.Supplier.Verification.SaveAssessment(QuestionnaireId, 2, 2, vcb2_2_Approval_Current.Value, vmb2_2_Approval_Current.Text, auth.UserId);
                        Helper.Questionnaire.Supplier.Verification.SaveAssessment(QuestionnaireId, 2, 3, vcb2_3_Approval_Current.Value, vmb2_3_Approval_Current.Text, auth.UserId);
                        Helper.Questionnaire.Supplier.Verification.SaveAssessment(QuestionnaireId, 2, 4, vcb2_4_Approval_Current.Value, vmb2_4_Approval_Current.Text, auth.UserId);
                        Helper.Questionnaire.Supplier.Verification.SaveAssessment(QuestionnaireId, 2, 5, vcb2_5_Approval_Current.Value, vmb2_5_Approval_Current.Text, auth.UserId);

                        Helper.Questionnaire.Supplier.Verification.SaveAssessment(QuestionnaireId, 4, 1, vcb3_1_Approval_Current.Value, vmb3_1_Approval_Current.Text, auth.UserId);
                        Helper.Questionnaire.Supplier.Verification.SaveAssessment(QuestionnaireId, 4, 2, vcb3_2_Approval_Current.Value, vmb3_2_Approval_Current.Text, auth.UserId);
                        Helper.Questionnaire.Supplier.Verification.SaveAssessment(QuestionnaireId, 4, 3, vcb3_3_Approval_Current.Value, vmb3_3_Approval_Current.Text, auth.UserId);
                        Helper.Questionnaire.Supplier.Verification.SaveAssessment(QuestionnaireId, 4, 4, vcb3_4_Approval_Current.Value, vmb3_4_Approval_Current.Text, auth.UserId);
                        Helper.Questionnaire.Supplier.Verification.SaveAssessment(QuestionnaireId, 4, 5, vcb3_5_Approval_Current.Value, vmb3_5_Approval_Current.Text, auth.UserId);

                        Helper.Questionnaire.Supplier.Verification.SaveAssessment(QuestionnaireId, 5, 1, vcb4_1_Approval_Current.Value, vmb4_1_Approval_Current.Text, auth.UserId);
                        Helper.Questionnaire.Supplier.Verification.SaveAssessment(QuestionnaireId, 5, 2, vcb4_2_Approval_Current.Value, vmb4_2_Approval_Current.Text, auth.UserId);
                        Helper.Questionnaire.Supplier.Verification.SaveAssessment(QuestionnaireId, 5, 3, vcb4_3_Approval_Current.Value, vmb4_3_Approval_Current.Text, auth.UserId);
                        Helper.Questionnaire.Supplier.Verification.SaveAssessment(QuestionnaireId, 5, 4, vcb4_4_Approval_Current.Value, vmb4_4_Approval_Current.Text, auth.UserId);
                        Helper.Questionnaire.Supplier.Verification.SaveAssessment(QuestionnaireId, 5, 5, vcb4_5_Approval_Current.Value, vmb4_5_Approval_Current.Text, auth.UserId);
                        Helper.Questionnaire.Supplier.Verification.SaveAssessment(QuestionnaireId, 5, 6, vcb4_6_Approval_Current.Value, vmb4_6_Approval_Current.Text, auth.UserId);

                        Helper.Questionnaire.Supplier.Verification.SaveAssessment(QuestionnaireId, 6, 1, vcb5_1_Approval_Current.Value, vmb5_1_Approval_Current.Text, auth.UserId);
                        Helper.Questionnaire.Supplier.Verification.SaveAssessment(QuestionnaireId, 6, 2, vcb5_2_Approval_Current.Value, vmb5_2_Approval_Current.Text, auth.UserId);
                        Helper.Questionnaire.Supplier.Verification.SaveAssessment(QuestionnaireId, 6, 3, vcb5_3_Approval_Current.Value, vmb5_3_Approval_Current.Text, auth.UserId);
                        Helper.Questionnaire.Supplier.Verification.SaveAssessment(QuestionnaireId, 6, 4, vcb5_4_Approval_Current.Value, vmb5_4_Approval_Current.Text, auth.UserId);

                        Helper.Questionnaire.Supplier.Verification.SaveAssessment(QuestionnaireId, 7, 1, vcb6_1_Approval_Current.Value, vmb6_1_Approval_Current.Text, auth.UserId);
                        Helper.Questionnaire.Supplier.Verification.SaveAssessment(QuestionnaireId, 7, 2, vcb6_2_Approval_Current.Value, vmb6_2_Approval_Current.Text, auth.UserId);
                        Helper.Questionnaire.Supplier.Verification.SaveAssessment(QuestionnaireId, 7, 3, vcb6_3_Approval_Current.Value, vmb6_3_Approval_Current.Text, auth.UserId);

                        Helper.Questionnaire.Supplier.Verification.SaveAssessment(QuestionnaireId, 8, 1, vcb7_1_Approval_Current.Value, vmb7_1_Approval_Current.Text, auth.UserId);
                        Helper.Questionnaire.Supplier.Verification.SaveAssessment(QuestionnaireId, 8, 2, vcb7_2_Approval_Current.Value, vmb7_2_Approval_Current.Text, auth.UserId);
                        Helper.Questionnaire.Supplier.Verification.SaveAssessment(QuestionnaireId, 8, 3, vcb7_3_Approval_Current.Value, vmb7_3_Approval_Current.Text, auth.UserId);
                    }
                }
                string q = Request.QueryString["q"].ToString();
                Response.Redirect("~/SafetyPQ_QuestionnaireCompareAssess.aspx" + QueryStringModule.Encrypt(String.Format("q={0}&saved=1", q)), true);
            }
        }
    }
    protected void btnHome_Click(object sender, EventArgs e)
    {
        if (Page.IsPostBack)
        {
            //System.Threading.Thread.Sleep(1000);
            string q = Request.QueryString["q"].ToString();
            Response.Redirect("~/SafetyPQ_QuestionnaireModify.aspx" + QueryStringModule.Encrypt("q=" + q), true);
        }
    }
}
