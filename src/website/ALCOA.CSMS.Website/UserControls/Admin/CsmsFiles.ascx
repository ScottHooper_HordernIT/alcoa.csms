﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CsmsFiles.ascx.cs" Inherits="ALCOA.CSMS.Website.UserControls.Admin.CsmsFiles" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxTabControl" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxClasses" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxUploadControl" tagprefix="dx" %>
<p>
    <a href="admin.aspx?s=CsmsFileUpload">Add New File</a><br /><br />
    <dx:ASPxGridView ID="grid" runat="server" AutoGenerateColumns="False" 
        CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
        CssPostfix="Office2003Blue" DataSourceID="CsmsFileDataSource1" 
        KeyFieldName="CsmsFileId" Width="900px" style="margin-top: 0px" OnHtmlRowCreated="grid_RowCreated">
        <Columns>
            <dx:GridViewCommandColumn Caption="Action" VisibleIndex="0" Width="55px">
                <DeleteButton Visible="True">
                </DeleteButton>
            </dx:GridViewCommandColumn>
            <dx:GridViewDataComboBoxColumn Caption="File Type" FieldName="CsmsFileTypeId" 
                VisibleIndex="1" GroupIndex="0">
                <PropertiesComboBox DataSourceID="CsmsFileTypeDataSource1" 
                    TextField="CsmsFileTypeName" ValueField="CsmsFileTypeId" 
                    ValueType="System.Int32">
                </PropertiesComboBox>
            </dx:GridViewDataComboBoxColumn>
            <dx:GridViewDataTextColumn FieldName="FileName" ShowInCustomizationForm="True" 
                VisibleIndex="2" Width="440px" Settings-ShowInFilterControl="True">
                <DataItemTemplate>
                                        <asp:HyperLink ID="hlFile" runat="server" Text="File" NavigateUrl=""></asp:HyperLink>
                                    </DataItemTemplate>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Description" 
                ShowInCustomizationForm="True" VisibleIndex="3">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataCheckColumn Caption="Visible" FieldName="IsVisible" 
                VisibleIndex="4" Width="50px">
            </dx:GridViewDataCheckColumn>
        </Columns>
        <SettingsPager PageSize="100">
            <AllButton Visible="True">
            </AllButton>
        </SettingsPager>
        <Settings ShowGroupPanel="True" ShowHeaderFilterButton="true" ShowFilterRow="true" ShowFilterBar="Visible"/>
        <SettingsBehavior ColumnResizeMode="NextColumn" ConfirmDelete="True"></SettingsBehavior>
        <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
            <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
            </LoadingPanelOnStatusBar>
            <LoadingPanel Url="~/App_Themes/Office2003BlueOld/GridView/Loading.gif"><%--Enhancement_023:change by debashis for testing platform upgradation--%>
            </LoadingPanel>
        </Images>
        <ImagesFilterControl>
            <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
            </LoadingPanel>
        </ImagesFilterControl>
        <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
            CssPostfix="Office2003Blue">
            <Header ImageSpacing="5px" SortingImageSpacing="5px">
            </Header>
            <LoadingPanel ImageSpacing="10px">
            </LoadingPanel>
        </Styles>
        <StylesEditors>
            <ProgressBar Height="25px">
            </ProgressBar>
        </StylesEditors>
    </dx:ASPxGridView>
</p>

<data:CsmsFileDataSource ID="CsmsFileDataSource1" runat="server">
</data:CsmsFileDataSource>

<data:CsmsFileTypeDataSource ID="CsmsFileTypeDataSource1" runat="server" Sort="CsmsFileTypeName ASC">
</data:CsmsFileTypeDataSource>


