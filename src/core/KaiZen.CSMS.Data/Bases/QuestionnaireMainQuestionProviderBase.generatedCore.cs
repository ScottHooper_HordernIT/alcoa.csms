﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="QuestionnaireMainQuestionProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class QuestionnaireMainQuestionProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.QuestionnaireMainQuestion, KaiZen.CSMS.Entities.QuestionnaireMainQuestionKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.QuestionnaireMainQuestionKey key)
		{
			return Delete(transactionManager, key.QuestionNo);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_questionNo">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.String _questionNo)
		{
			return Delete(null, _questionNo);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionNo">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.String _questionNo);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.QuestionnaireMainQuestion Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.QuestionnaireMainQuestionKey key, int start, int pageLength)
		{
			return GetByQuestionNo(transactionManager, key.QuestionNo, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_QuestionnaireQuestion_1 index.
		/// </summary>
		/// <param name="_questionNo"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireMainQuestion"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireMainQuestion GetByQuestionNo(System.String _questionNo)
		{
			int count = -1;
			return GetByQuestionNo(null,_questionNo, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireQuestion_1 index.
		/// </summary>
		/// <param name="_questionNo"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireMainQuestion"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireMainQuestion GetByQuestionNo(System.String _questionNo, int start, int pageLength)
		{
			int count = -1;
			return GetByQuestionNo(null, _questionNo, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireQuestion_1 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionNo"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireMainQuestion"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireMainQuestion GetByQuestionNo(TransactionManager transactionManager, System.String _questionNo)
		{
			int count = -1;
			return GetByQuestionNo(transactionManager, _questionNo, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireQuestion_1 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionNo"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireMainQuestion"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireMainQuestion GetByQuestionNo(TransactionManager transactionManager, System.String _questionNo, int start, int pageLength)
		{
			int count = -1;
			return GetByQuestionNo(transactionManager, _questionNo, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireQuestion_1 index.
		/// </summary>
		/// <param name="_questionNo"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireMainQuestion"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireMainQuestion GetByQuestionNo(System.String _questionNo, int start, int pageLength, out int count)
		{
			return GetByQuestionNo(null, _questionNo, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireQuestion_1 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionNo"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireMainQuestion"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.QuestionnaireMainQuestion GetByQuestionNo(TransactionManager transactionManager, System.String _questionNo, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;QuestionnaireMainQuestion&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;QuestionnaireMainQuestion&gt;"/></returns>
		public static TList<QuestionnaireMainQuestion> Fill(IDataReader reader, TList<QuestionnaireMainQuestion> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.QuestionnaireMainQuestion c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("QuestionnaireMainQuestion")
					.Append("|").Append((System.String)reader[((int)QuestionnaireMainQuestionColumn.QuestionNo - 1)]).ToString();
					c = EntityManager.LocateOrCreate<QuestionnaireMainQuestion>(
					key.ToString(), // EntityTrackingKey
					"QuestionnaireMainQuestion",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.QuestionnaireMainQuestion();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.QuestionNo = (System.String)reader[((int)QuestionnaireMainQuestionColumn.QuestionNo - 1)];
					c.OriginalQuestionNo = c.QuestionNo;
					c.QuestionText = (System.String)reader[((int)QuestionnaireMainQuestionColumn.QuestionText - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.QuestionnaireMainQuestion"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.QuestionnaireMainQuestion"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.QuestionnaireMainQuestion entity)
		{
			if (!reader.Read()) return;
			
			entity.QuestionNo = (System.String)reader[((int)QuestionnaireMainQuestionColumn.QuestionNo - 1)];
			entity.OriginalQuestionNo = (System.String)reader["QuestionNo"];
			entity.QuestionText = (System.String)reader[((int)QuestionnaireMainQuestionColumn.QuestionText - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.QuestionnaireMainQuestion"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.QuestionnaireMainQuestion"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.QuestionnaireMainQuestion entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.QuestionNo = (System.String)dataRow["QuestionNo"];
			entity.OriginalQuestionNo = (System.String)dataRow["QuestionNo"];
			entity.QuestionText = (System.String)dataRow["QuestionText"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.QuestionnaireMainQuestion"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.QuestionnaireMainQuestion Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.QuestionnaireMainQuestion entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetByQuestionNo methods when available
			
			#region QuestionnaireMainAnswerCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<QuestionnaireMainAnswer>|QuestionnaireMainAnswerCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'QuestionnaireMainAnswerCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.QuestionnaireMainAnswerCollection = DataRepository.QuestionnaireMainAnswerProvider.GetByQuestionNo(transactionManager, entity.QuestionNo);

				if (deep && entity.QuestionnaireMainAnswerCollection.Count > 0)
				{
					deepHandles.Add("QuestionnaireMainAnswerCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<QuestionnaireMainAnswer>) DataRepository.QuestionnaireMainAnswerProvider.DeepLoad,
						new object[] { transactionManager, entity.QuestionnaireMainAnswerCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.QuestionnaireMainQuestion object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.QuestionnaireMainQuestion instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.QuestionnaireMainQuestion Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.QuestionnaireMainQuestion entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<QuestionnaireMainAnswer>
				if (CanDeepSave(entity.QuestionnaireMainAnswerCollection, "List<QuestionnaireMainAnswer>|QuestionnaireMainAnswerCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(QuestionnaireMainAnswer child in entity.QuestionnaireMainAnswerCollection)
					{
						if(child.QuestionNoSource != null)
						{
							child.QuestionNo = child.QuestionNoSource.QuestionNo;
						}
						else
						{
							child.QuestionNo = entity.QuestionNo;
						}

					}

					if (entity.QuestionnaireMainAnswerCollection.Count > 0 || entity.QuestionnaireMainAnswerCollection.DeletedItems.Count > 0)
					{
						//DataRepository.QuestionnaireMainAnswerProvider.Save(transactionManager, entity.QuestionnaireMainAnswerCollection);
						
						deepHandles.Add("QuestionnaireMainAnswerCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< QuestionnaireMainAnswer >) DataRepository.QuestionnaireMainAnswerProvider.DeepSave,
							new object[] { transactionManager, entity.QuestionnaireMainAnswerCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region QuestionnaireMainQuestionChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.QuestionnaireMainQuestion</c>
	///</summary>
	public enum QuestionnaireMainQuestionChildEntityTypes
	{

		///<summary>
		/// Collection of <c>QuestionnaireMainQuestion</c> as OneToMany for QuestionnaireMainAnswerCollection
		///</summary>
		[ChildEntityType(typeof(TList<QuestionnaireMainAnswer>))]
		QuestionnaireMainAnswerCollection,
	}
	
	#endregion QuestionnaireMainQuestionChildEntityTypes
	
	#region QuestionnaireMainQuestionFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;QuestionnaireMainQuestionColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireMainQuestion"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireMainQuestionFilterBuilder : SqlFilterBuilder<QuestionnaireMainQuestionColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainQuestionFilterBuilder class.
		/// </summary>
		public QuestionnaireMainQuestionFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainQuestionFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireMainQuestionFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainQuestionFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireMainQuestionFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireMainQuestionFilterBuilder
	
	#region QuestionnaireMainQuestionParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;QuestionnaireMainQuestionColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireMainQuestion"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireMainQuestionParameterBuilder : ParameterizedSqlFilterBuilder<QuestionnaireMainQuestionColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainQuestionParameterBuilder class.
		/// </summary>
		public QuestionnaireMainQuestionParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainQuestionParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireMainQuestionParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainQuestionParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireMainQuestionParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireMainQuestionParameterBuilder
	
	#region QuestionnaireMainQuestionSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;QuestionnaireMainQuestionColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireMainQuestion"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class QuestionnaireMainQuestionSortBuilder : SqlSortBuilder<QuestionnaireMainQuestionColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainQuestionSqlSortBuilder class.
		/// </summary>
		public QuestionnaireMainQuestionSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion QuestionnaireMainQuestionSortBuilder
	
} // end namespace
