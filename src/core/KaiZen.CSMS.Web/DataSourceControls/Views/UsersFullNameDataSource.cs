﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.UsersFullNameProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[CLSCompliant(true)]
	[Designer(typeof(UsersFullNameDataSourceDesigner))]
	public class UsersFullNameDataSource : ReadOnlyDataSource<UsersFullName>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UsersFullNameDataSource class.
		/// </summary>
		public UsersFullNameDataSource() : base(new UsersFullNameService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the UsersFullNameDataSourceView used by the UsersFullNameDataSource.
		/// </summary>
		protected UsersFullNameDataSourceView UsersFullNameView
		{
			get { return ( View as UsersFullNameDataSourceView ); }
		}
		
		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the UsersFullNameDataSourceView class that is to be
		/// used by the UsersFullNameDataSource.
		/// </summary>
		/// <returns>An instance of the UsersFullNameDataSourceView class.</returns>
		protected override BaseDataSourceView<UsersFullName, Object> GetNewDataSourceView()
		{
			return new UsersFullNameDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the UsersFullNameDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class UsersFullNameDataSourceView : ReadOnlyDataSourceView<UsersFullName>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UsersFullNameDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the UsersFullNameDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public UsersFullNameDataSourceView(UsersFullNameDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal UsersFullNameDataSource UsersFullNameOwner
		{
			get { return Owner as UsersFullNameDataSource; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal UsersFullNameService UsersFullNameProvider
		{
			get { return Provider as UsersFullNameService; }
		}

		#endregion Properties
		
		#region Methods
		
		#endregion Methods
	}

	#region UsersFullNameDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the UsersFullNameDataSource class.
	/// </summary>
	public class UsersFullNameDataSourceDesigner : ReadOnlyDataSourceDesigner<UsersFullName>
	{
	}

	#endregion UsersFullNameDataSourceDesigner

	#region UsersFullNameFilter

	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="UsersFullName"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UsersFullNameFilter : SqlFilter<UsersFullNameColumn>
	{
	}

	#endregion UsersFullNameFilter

	#region UsersFullNameExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="UsersFullName"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UsersFullNameExpressionBuilder : SqlExpressionBuilder<UsersFullNameColumn>
	{
	}
	
	#endregion UsersFullNameExpressionBuilder		
}

