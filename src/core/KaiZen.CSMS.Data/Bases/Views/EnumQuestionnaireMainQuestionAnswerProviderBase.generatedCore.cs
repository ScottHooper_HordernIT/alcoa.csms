﻿#region Using directives

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="EnumQuestionnaireMainQuestionAnswerProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract class EnumQuestionnaireMainQuestionAnswerProviderBaseCore : EntityViewProviderBase<EnumQuestionnaireMainQuestionAnswer>
	{
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions
		
		/*
		///<summary>
		/// Fill an VList&lt;EnumQuestionnaireMainQuestionAnswer&gt; From a DataSet
		///</summary>
		/// <param name="dataSet">the DataSet</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList&lt;EnumQuestionnaireMainQuestionAnswer&gt;"/></returns>
		protected static VList&lt;EnumQuestionnaireMainQuestionAnswer&gt; Fill(DataSet dataSet, VList<EnumQuestionnaireMainQuestionAnswer> rows, int start, int pagelen)
		{
			if (dataSet.Tables.Count == 1)
			{
				return Fill(dataSet.Tables[0], rows, start, pagelen);
			}
			else
			{
				return new VList<EnumQuestionnaireMainQuestionAnswer>();
			}	
		}
		
		
		///<summary>
		/// Fill an VList&lt;EnumQuestionnaireMainQuestionAnswer&gt; From a DataTable
		///</summary>
		/// <param name="dataTable">the DataTable that hold the data.</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList<EnumQuestionnaireMainQuestionAnswer>"/></returns>
		protected static VList&lt;EnumQuestionnaireMainQuestionAnswer&gt; Fill(DataTable dataTable, VList<EnumQuestionnaireMainQuestionAnswer> rows, int start, int pagelen)
		{
			int recordnum = 0;
			
			System.Collections.IEnumerator dataRows =  dataTable.Rows.GetEnumerator();
			
			while (dataRows.MoveNext() && (pagelen != 0))
			{
				if(recordnum >= start)
				{
					DataRow row = (DataRow)dataRows.Current;
				
					EnumQuestionnaireMainQuestionAnswer c = new EnumQuestionnaireMainQuestionAnswer();
					c.AnswerId = (Convert.IsDBNull(row["AnswerId"]))?(int)0:(System.Int32)row["AnswerId"];
					c.QuestionAnswerNo = (Convert.IsDBNull(row["QuestionAnswerNo"]))?string.Empty:(System.String)row["QuestionAnswerNo"];
					c.QuestionText = (Convert.IsDBNull(row["QuestionText"]))?string.Empty:(System.String)row["QuestionText"];
					c.AnswerText = (Convert.IsDBNull(row["AnswerText"]))?string.Empty:(System.String)row["AnswerText"];
					c.AcceptChanges();
					rows.Add(c);
					pagelen -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		*/	
						
		///<summary>
		/// Fill an <see cref="VList&lt;EnumQuestionnaireMainQuestionAnswer&gt;"/> From a DataReader.
		///</summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pageLength">number of row.</param>
		///<returns>a <see cref="VList&lt;EnumQuestionnaireMainQuestionAnswer&gt;"/></returns>
		protected VList<EnumQuestionnaireMainQuestionAnswer> Fill(IDataReader reader, VList<EnumQuestionnaireMainQuestionAnswer> rows, int start, int pageLength)
		{
			int recordnum = 0;
			while (reader.Read() && (pageLength != 0))
			{
				if(recordnum >= start)
				{
					EnumQuestionnaireMainQuestionAnswer entity = null;
					if (DataRepository.Provider.UseEntityFactory)
					{
						entity = EntityManager.CreateViewEntity<EnumQuestionnaireMainQuestionAnswer>("EnumQuestionnaireMainQuestionAnswer",  DataRepository.Provider.EntityCreationalFactoryType); 
					}
					else
					{
						entity = new EnumQuestionnaireMainQuestionAnswer();
					}
					
					entity.SuppressEntityEvents = true;

					entity.AnswerId = (System.Int32)reader[((int)EnumQuestionnaireMainQuestionAnswerColumn.AnswerId)];
					//entity.AnswerId = (Convert.IsDBNull(reader["AnswerId"]))?(int)0:(System.Int32)reader["AnswerId"];
					entity.QuestionAnswerNo = (System.String)reader[((int)EnumQuestionnaireMainQuestionAnswerColumn.QuestionAnswerNo)];
					//entity.QuestionAnswerNo = (Convert.IsDBNull(reader["QuestionAnswerNo"]))?string.Empty:(System.String)reader["QuestionAnswerNo"];
					entity.QuestionText = (System.String)reader[((int)EnumQuestionnaireMainQuestionAnswerColumn.QuestionText)];
					//entity.QuestionText = (Convert.IsDBNull(reader["QuestionText"]))?string.Empty:(System.String)reader["QuestionText"];
					entity.AnswerText = (reader.IsDBNull(((int)EnumQuestionnaireMainQuestionAnswerColumn.AnswerText)))?null:(System.String)reader[((int)EnumQuestionnaireMainQuestionAnswerColumn.AnswerText)];
					//entity.AnswerText = (Convert.IsDBNull(reader["AnswerText"]))?string.Empty:(System.String)reader["AnswerText"];
					entity.AcceptChanges();
					entity.SuppressEntityEvents = false;
					
					rows.Add(entity);
					pageLength -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		
		
		/// <summary>
		/// Refreshes the <see cref="EnumQuestionnaireMainQuestionAnswer"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="EnumQuestionnaireMainQuestionAnswer"/> object to refresh.</param>
		protected void RefreshEntity(IDataReader reader, EnumQuestionnaireMainQuestionAnswer entity)
		{
			reader.Read();
			entity.AnswerId = (System.Int32)reader[((int)EnumQuestionnaireMainQuestionAnswerColumn.AnswerId)];
			//entity.AnswerId = (Convert.IsDBNull(reader["AnswerId"]))?(int)0:(System.Int32)reader["AnswerId"];
			entity.QuestionAnswerNo = (System.String)reader[((int)EnumQuestionnaireMainQuestionAnswerColumn.QuestionAnswerNo)];
			//entity.QuestionAnswerNo = (Convert.IsDBNull(reader["QuestionAnswerNo"]))?string.Empty:(System.String)reader["QuestionAnswerNo"];
			entity.QuestionText = (System.String)reader[((int)EnumQuestionnaireMainQuestionAnswerColumn.QuestionText)];
			//entity.QuestionText = (Convert.IsDBNull(reader["QuestionText"]))?string.Empty:(System.String)reader["QuestionText"];
			entity.AnswerText = (reader.IsDBNull(((int)EnumQuestionnaireMainQuestionAnswerColumn.AnswerText)))?null:(System.String)reader[((int)EnumQuestionnaireMainQuestionAnswerColumn.AnswerText)];
			//entity.AnswerText = (Convert.IsDBNull(reader["AnswerText"]))?string.Empty:(System.String)reader["AnswerText"];
			reader.Close();
	
			entity.AcceptChanges();
		}
		
		/*
		/// <summary>
		/// Refreshes the <see cref="EnumQuestionnaireMainQuestionAnswer"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="EnumQuestionnaireMainQuestionAnswer"/> object.</param>
		protected static void RefreshEntity(DataSet dataSet, EnumQuestionnaireMainQuestionAnswer entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.AnswerId = (Convert.IsDBNull(dataRow["AnswerId"]))?(int)0:(System.Int32)dataRow["AnswerId"];
			entity.QuestionAnswerNo = (Convert.IsDBNull(dataRow["QuestionAnswerNo"]))?string.Empty:(System.String)dataRow["QuestionAnswerNo"];
			entity.QuestionText = (Convert.IsDBNull(dataRow["QuestionText"]))?string.Empty:(System.String)dataRow["QuestionText"];
			entity.AnswerText = (Convert.IsDBNull(dataRow["AnswerText"]))?string.Empty:(System.String)dataRow["AnswerText"];
			entity.AcceptChanges();
		}
		*/
			
		#endregion Helper Functions
	}//end class

	#region EnumQuestionnaireMainQuestionAnswerFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EnumQuestionnaireMainQuestionAnswer"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EnumQuestionnaireMainQuestionAnswerFilterBuilder : SqlFilterBuilder<EnumQuestionnaireMainQuestionAnswerColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EnumQuestionnaireMainQuestionAnswerFilterBuilder class.
		/// </summary>
		public EnumQuestionnaireMainQuestionAnswerFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the EnumQuestionnaireMainQuestionAnswerFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public EnumQuestionnaireMainQuestionAnswerFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the EnumQuestionnaireMainQuestionAnswerFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public EnumQuestionnaireMainQuestionAnswerFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion EnumQuestionnaireMainQuestionAnswerFilterBuilder

	#region EnumQuestionnaireMainQuestionAnswerParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EnumQuestionnaireMainQuestionAnswer"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EnumQuestionnaireMainQuestionAnswerParameterBuilder : ParameterizedSqlFilterBuilder<EnumQuestionnaireMainQuestionAnswerColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EnumQuestionnaireMainQuestionAnswerParameterBuilder class.
		/// </summary>
		public EnumQuestionnaireMainQuestionAnswerParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the EnumQuestionnaireMainQuestionAnswerParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public EnumQuestionnaireMainQuestionAnswerParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the EnumQuestionnaireMainQuestionAnswerParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public EnumQuestionnaireMainQuestionAnswerParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion EnumQuestionnaireMainQuestionAnswerParameterBuilder
	
	#region EnumQuestionnaireMainQuestionAnswerSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EnumQuestionnaireMainQuestionAnswer"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class EnumQuestionnaireMainQuestionAnswerSortBuilder : SqlSortBuilder<EnumQuestionnaireMainQuestionAnswerColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EnumQuestionnaireMainQuestionAnswerSqlSortBuilder class.
		/// </summary>
		public EnumQuestionnaireMainQuestionAnswerSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion EnumQuestionnaireMainQuestionAnswerSortBuilder

} // end namespace
