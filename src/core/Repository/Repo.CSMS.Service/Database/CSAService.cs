﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface ICSAService
    {
        CSA Get(Expression<Func<CSA, bool>> where, List<Expression<Func<CSA, object>>> includes);
        List<CSA> GetMany(int? top, Expression<Func<CSA, bool>> where, Func<IQueryable<CSA>, IOrderedQueryable<CSA>> orderBy, List<Expression<Func<CSA, object>>> includes);
        List<CSA> GetManyPaged(int pageNumber, int pageSize, Expression<Func<CSA, bool>> where, Func<IQueryable<CSA>, IOrderedQueryable<CSA>> orderBy, List<Expression<Func<CSA, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<CSA, bool>> where, List<Expression<Func<CSA, object>>> includes);
        void Insert(CSA item, bool saveChanges = true);
        void Update(CSA item, bool saveChanges = true);
        void Delete(CSA item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class CSAService : ICSAService
    {
        private readonly IRepository<CSA> _repository;

        public CSAService(IRepository<CSA> repository)
        {
            this._repository = repository;
        }

        public CSA Get(Expression<Func<CSA, bool>> where, List<Expression<Func<CSA, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<CSA> GetMany(int? top, Expression<Func<CSA, bool>> where, Func<IQueryable<CSA>, IOrderedQueryable<CSA>> orderBy, List<Expression<Func<CSA, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<CSA> GetManyPaged(int pageNumber, int pageSize, Expression<Func<CSA, bool>> where, Func<IQueryable<CSA>, IOrderedQueryable<CSA>> orderBy, List<Expression<Func<CSA, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<CSA, bool>> where, List<Expression<Func<CSA, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(CSA item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("CSA");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(CSA item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("CSA");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(CSA item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("CSA");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
