﻿#region Using directives

using System;

#endregion

namespace KaiZen.CSMS.Entities
{	
	///<summary>
	/// An object representation of the 'QuestionnaireWithLocationApprovalView' view. [No description found in the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class QuestionnaireWithLocationApprovalView : QuestionnaireWithLocationApprovalViewBase
	{
		#region Constructors

		///<summary>
		/// Creates a new <see cref="QuestionnaireWithLocationApprovalView"/> instance.
		///</summary>
		public QuestionnaireWithLocationApprovalView():base(){}	
		
		#endregion
	}
}
