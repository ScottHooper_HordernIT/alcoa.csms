﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------



public partial class UserControls_Admin_ebi {
    
    /// <summary>
    /// ASPxPageControl1 control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::DevExpress.Web.ASPxTabControl.ASPxPageControl ASPxPageControl1;
    
    /// <summary>
    /// cbAutoEmail control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::DevExpress.Web.ASPxEditors.ASPxCheckBox cbAutoEmail;
    
    /// <summary>
    /// btnAutoEmail control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::DevExpress.Web.ASPxEditors.ASPxButton btnAutoEmail;
    
    /// <summary>
    /// gridSites control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::DevExpress.Web.ASPxGridView.ASPxGridView gridSites;
    
    /// <summary>
    /// CmbRawDataCompany control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::DevExpress.Web.ASPxEditors.ASPxComboBox CmbRawDataCompany;
    
    /// <summary>
    /// CmbRawDataSite control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::DevExpress.Web.ASPxEditors.ASPxComboBox CmbRawDataSite;
    
    /// <summary>
    /// cmbRawDataMonth control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::DevExpress.Web.ASPxEditors.ASPxComboBox cmbRawDataMonth;
    
    /// <summary>
    /// cbRawData_Year control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::DevExpress.Web.ASPxEditors.ASPxComboBox cbRawData_Year;
    
    /// <summary>
    /// ASPxButton1 control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::DevExpress.Web.ASPxEditors.ASPxButton ASPxButton1;
    
    /// <summary>
    /// grid2 control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::DevExpress.Web.ASPxGridView.ASPxGridView grid2;
    
    /// <summary>
    /// dsEbi_RawData_ByYearMonthCompanySite control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::System.Web.UI.WebControls.SqlDataSource dsEbi_RawData_ByYearMonthCompanySite;
    
    /// <summary>
    /// ExportButtons3 control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::UserControls_Tiny_ExportButtons ExportButtons3;
    
    /// <summary>
    /// deByDay control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::DevExpress.Web.ASPxEditors.ASPxDateEdit deByDay;
    
    /// <summary>
    /// cbpByDay control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::DevExpress.Web.ASPxCallbackPanel.ASPxCallbackPanel cbpByDay;
    
    /// <summary>
    /// gridByDay control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::DevExpress.Web.ASPxGridView.ASPxGridView gridByDay;
    
    /// <summary>
    /// ExportButtons1 control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::UserControls_Tiny_ExportButtons ExportButtons1;
    
    /// <summary>
    /// Ebi_YearMonthDayCompanies_ByDay control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::System.Web.UI.WebControls.SqlDataSource Ebi_YearMonthDayCompanies_ByDay;
    
    /// <summary>
    /// cbByMonth_Year control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::DevExpress.Web.ASPxEditors.ASPxComboBox cbByMonth_Year;
    
    /// <summary>
    /// cbByMonth_Month control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::DevExpress.Web.ASPxEditors.ASPxComboBox cbByMonth_Month;
    
    /// <summary>
    /// btnByMonth_GoFilter control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::DevExpress.Web.ASPxEditors.ASPxButton btnByMonth_GoFilter;
    
    /// <summary>
    /// gridByMonth control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::DevExpress.Web.ASPxGridView.ASPxGridView gridByMonth;
    
    /// <summary>
    /// Ebi_YearMonthDayCompanies_ByYearMonth control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::System.Web.UI.WebControls.SqlDataSource Ebi_YearMonthDayCompanies_ByYearMonth;
    
    /// <summary>
    /// ExportButtons2 control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::UserControls_Tiny_ExportButtons ExportButtons2;
    
    /// <summary>
    /// cmbHourWorkedCompany control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::DevExpress.Web.ASPxEditors.ASPxComboBox cmbHourWorkedCompany;
    
    /// <summary>
    /// cmbHourWorkedSite control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::DevExpress.Web.ASPxEditors.ASPxComboBox cmbHourWorkedSite;
    
    /// <summary>
    /// cmbHourWorkedMonth control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::DevExpress.Web.ASPxEditors.ASPxComboBox cmbHourWorkedMonth;
    
    /// <summary>
    /// cmbHourWorkedYear control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::DevExpress.Web.ASPxEditors.ASPxComboBox cmbHourWorkedYear;
    
    /// <summary>
    /// btnHoursWorked control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::DevExpress.Web.ASPxEditors.ASPxButton btnHoursWorked;
    
    /// <summary>
    /// gridHourWorked_ByMonthYear control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::DevExpress.Web.ASPxGridView.ASPxGridView gridHourWorked_ByMonthYear;
    
    /// <summary>
    /// ExportButtons4 control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::UserControls_Tiny_ExportButtons ExportButtons4;
    
    /// <summary>
    /// EbiHoursWorkedDS control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::System.Web.UI.WebControls.SqlDataSource EbiHoursWorkedDS;
    
    /// <summary>
    /// cmbKPECompany control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::DevExpress.Web.ASPxEditors.ASPxComboBox cmbKPECompany;
    
    /// <summary>
    /// cmbKPESite control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::DevExpress.Web.ASPxEditors.ASPxComboBox cmbKPESite;
    
    /// <summary>
    /// cmbKPEMonth control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::DevExpress.Web.ASPxEditors.ASPxComboBox cmbKPEMonth;
    
    /// <summary>
    /// cmbKPEYear control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::DevExpress.Web.ASPxEditors.ASPxComboBox cmbKPEYear;
    
    /// <summary>
    /// btnKPE control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::DevExpress.Web.ASPxEditors.ASPxButton btnKPE;
    
    /// <summary>
    /// gridKPE_ByMonthYear control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::DevExpress.Web.ASPxGridView.ASPxGridView gridKPE_ByMonthYear;
    
    /// <summary>
    /// ExportButtons5 control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::UserControls_Tiny_ExportButtons ExportButtons5;
    
    /// <summary>
    /// KpiVsEbiDS control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::System.Web.UI.WebControls.SqlDataSource KpiVsEbiDS;
    
   
    
    /// <summary>
    /// CompaniesDataSource4 control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::System.Web.UI.WebControls.SqlDataSource CompaniesDataSource4;
    
    /// <summary>
    /// SitesDtSource control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::System.Web.UI.WebControls.SqlDataSource SitesDtSource;
    
    /// <summary>
    /// SiteNameEbiDS control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::System.Web.UI.WebControls.SqlDataSource SiteNameEbiDS;
    
    /// <summary>
    /// CompanyNameDS control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::System.Web.UI.WebControls.SqlDataSource CompanyNameDS;
    
    /// <summary>
    /// SitesDt control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::System.Web.UI.WebControls.SqlDataSource SitesDt;
}
