﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using KaiZen.CSMS.Data;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Services;
using KaiZen.CSMS.Web;
using System.Data;
using System.ComponentModel;
using DevExpress.Web.ASPxGridView;

namespace ALCOA.CSMS.Website.UserControls.Main
{
    public partial class EbiMetrics : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                deFrom.Value = DateTime.Now;
                LoadData(DateTime.Now);
            }
            Helper.ExportGrid.Settings(ucExportButtons, "CSMS - EBI Metrics");
            BindData();
        }

        protected void LoadData(DateTime dt)
        {
            EbiMetricService emService = new EbiMetricService();
            DataSet ds = emService.GetLast30Days_From(addzero(dt.Year), addzero(dt.Month), addzero(dt.Day));
            Session["dtEbiMetric"] = ds;

            int i = 0;
            while (i < 30)
            {
                grid.Columns[String.Format("Day{0}", i + 1)].Caption = String.Format("{0}/{1}", addzero(dt.AddDays(-1 * i).Day), addzero(dt.AddDays(-1 * i).Month));
                i++;
            }
        }

        protected void BindData()
        {
            grid.DataSource = ((DataSet)(Session["dtEbiMetric"])).Tables[0];
            grid.DataBind();
        }

        protected string addzero (int i)
        {
            if (i < 10)
                return "0" + i.ToString();
            else
                return i.ToString();
        }

        protected void btnFilter_Click(object sender, EventArgs e)
        {
            DateTime dt = (DateTime)deFrom.Value;
            LoadData(dt);
            BindData();
        }

    }
}