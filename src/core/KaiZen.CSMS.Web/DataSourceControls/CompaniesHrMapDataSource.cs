﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.CompaniesHrMapProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(CompaniesHrMapDataSourceDesigner))]
	public class CompaniesHrMapDataSource : ProviderDataSource<CompaniesHrMap, CompaniesHrMapKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompaniesHrMapDataSource class.
		/// </summary>
		public CompaniesHrMapDataSource() : base(new CompaniesHrMapService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the CompaniesHrMapDataSourceView used by the CompaniesHrMapDataSource.
		/// </summary>
		protected CompaniesHrMapDataSourceView CompaniesHrMapView
		{
			get { return ( View as CompaniesHrMapDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the CompaniesHrMapDataSource control invokes to retrieve data.
		/// </summary>
		public CompaniesHrMapSelectMethod SelectMethod
		{
			get
			{
				CompaniesHrMapSelectMethod selectMethod = CompaniesHrMapSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (CompaniesHrMapSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the CompaniesHrMapDataSourceView class that is to be
		/// used by the CompaniesHrMapDataSource.
		/// </summary>
		/// <returns>An instance of the CompaniesHrMapDataSourceView class.</returns>
		protected override BaseDataSourceView<CompaniesHrMap, CompaniesHrMapKey> GetNewDataSourceView()
		{
			return new CompaniesHrMapDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the CompaniesHrMapDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class CompaniesHrMapDataSourceView : ProviderDataSourceView<CompaniesHrMap, CompaniesHrMapKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompaniesHrMapDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the CompaniesHrMapDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public CompaniesHrMapDataSourceView(CompaniesHrMapDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal CompaniesHrMapDataSource CompaniesHrMapOwner
		{
			get { return Owner as CompaniesHrMapDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal CompaniesHrMapSelectMethod SelectMethod
		{
			get { return CompaniesHrMapOwner.SelectMethod; }
			set { CompaniesHrMapOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal CompaniesHrMapService CompaniesHrMapProvider
		{
			get { return Provider as CompaniesHrMapService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<CompaniesHrMap> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<CompaniesHrMap> results = null;
			CompaniesHrMap item;
			count = 0;
			
			System.Int32 _companiesHrMapId;
			System.Int32 _companyId;
			System.Int32 _companyNameHr;

			switch ( SelectMethod )
			{
				case CompaniesHrMapSelectMethod.Get:
					CompaniesHrMapKey entityKey  = new CompaniesHrMapKey();
					entityKey.Load(values);
					item = CompaniesHrMapProvider.Get(entityKey);
					results = new TList<CompaniesHrMap>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case CompaniesHrMapSelectMethod.GetAll:
                    results = CompaniesHrMapProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case CompaniesHrMapSelectMethod.GetPaged:
					results = CompaniesHrMapProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case CompaniesHrMapSelectMethod.Find:
					if ( FilterParameters != null )
						results = CompaniesHrMapProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = CompaniesHrMapProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case CompaniesHrMapSelectMethod.GetByCompaniesHrMapId:
					_companiesHrMapId = ( values["CompaniesHrMapId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["CompaniesHrMapId"], typeof(System.Int32)) : (int)0;
					item = CompaniesHrMapProvider.GetByCompaniesHrMapId(_companiesHrMapId);
					results = new TList<CompaniesHrMap>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				case CompaniesHrMapSelectMethod.GetByCompanyId:
					_companyId = ( values["CompanyId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["CompanyId"], typeof(System.Int32)) : (int)0;
					item = CompaniesHrMapProvider.GetByCompanyId(_companyId);
					results = new TList<CompaniesHrMap>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case CompaniesHrMapSelectMethod.GetByCompanyNameHr:
					_companyNameHr = ( values["CompanyNameHr"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["CompanyNameHr"], typeof(System.Int32)) : (int)0;
					item = CompaniesHrMapProvider.GetByCompanyNameHr(_companyNameHr);
					results = new TList<CompaniesHrMap>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// FK
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == CompaniesHrMapSelectMethod.Get || SelectMethod == CompaniesHrMapSelectMethod.GetByCompaniesHrMapId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				CompaniesHrMap entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					CompaniesHrMapProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<CompaniesHrMap> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			CompaniesHrMapProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region CompaniesHrMapDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the CompaniesHrMapDataSource class.
	/// </summary>
	public class CompaniesHrMapDataSourceDesigner : ProviderDataSourceDesigner<CompaniesHrMap, CompaniesHrMapKey>
	{
		/// <summary>
		/// Initializes a new instance of the CompaniesHrMapDataSourceDesigner class.
		/// </summary>
		public CompaniesHrMapDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public CompaniesHrMapSelectMethod SelectMethod
		{
			get { return ((CompaniesHrMapDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new CompaniesHrMapDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region CompaniesHrMapDataSourceActionList

	/// <summary>
	/// Supports the CompaniesHrMapDataSourceDesigner class.
	/// </summary>
	internal class CompaniesHrMapDataSourceActionList : DesignerActionList
	{
		private CompaniesHrMapDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the CompaniesHrMapDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public CompaniesHrMapDataSourceActionList(CompaniesHrMapDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public CompaniesHrMapSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion CompaniesHrMapDataSourceActionList
	
	#endregion CompaniesHrMapDataSourceDesigner
	
	#region CompaniesHrMapSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the CompaniesHrMapDataSource.SelectMethod property.
	/// </summary>
	public enum CompaniesHrMapSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByCompaniesHrMapId method.
		/// </summary>
		GetByCompaniesHrMapId,
		/// <summary>
		/// Represents the GetByCompanyId method.
		/// </summary>
		GetByCompanyId,
		/// <summary>
		/// Represents the GetByCompanyNameHr method.
		/// </summary>
		GetByCompanyNameHr
	}
	
	#endregion CompaniesHrMapSelectMethod

	#region CompaniesHrMapFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CompaniesHrMap"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompaniesHrMapFilter : SqlFilter<CompaniesHrMapColumn>
	{
	}
	
	#endregion CompaniesHrMapFilter

	#region CompaniesHrMapExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CompaniesHrMap"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompaniesHrMapExpressionBuilder : SqlExpressionBuilder<CompaniesHrMapColumn>
	{
	}
	
	#endregion CompaniesHrMapExpressionBuilder	

	#region CompaniesHrMapProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;CompaniesHrMapChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="CompaniesHrMap"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompaniesHrMapProperty : ChildEntityProperty<CompaniesHrMapChildEntityTypes>
	{
	}
	
	#endregion CompaniesHrMapProperty
}

