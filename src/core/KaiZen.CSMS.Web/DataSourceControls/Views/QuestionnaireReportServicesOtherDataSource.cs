﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.QuestionnaireReportServicesOtherProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[CLSCompliant(true)]
	[Designer(typeof(QuestionnaireReportServicesOtherDataSourceDesigner))]
	public class QuestionnaireReportServicesOtherDataSource : ReadOnlyDataSource<QuestionnaireReportServicesOther>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportServicesOtherDataSource class.
		/// </summary>
		public QuestionnaireReportServicesOtherDataSource() : base(new QuestionnaireReportServicesOtherService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the QuestionnaireReportServicesOtherDataSourceView used by the QuestionnaireReportServicesOtherDataSource.
		/// </summary>
		protected QuestionnaireReportServicesOtherDataSourceView QuestionnaireReportServicesOtherView
		{
			get { return ( View as QuestionnaireReportServicesOtherDataSourceView ); }
		}
		
		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the QuestionnaireReportServicesOtherDataSourceView class that is to be
		/// used by the QuestionnaireReportServicesOtherDataSource.
		/// </summary>
		/// <returns>An instance of the QuestionnaireReportServicesOtherDataSourceView class.</returns>
		protected override BaseDataSourceView<QuestionnaireReportServicesOther, Object> GetNewDataSourceView()
		{
			return new QuestionnaireReportServicesOtherDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the QuestionnaireReportServicesOtherDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class QuestionnaireReportServicesOtherDataSourceView : ReadOnlyDataSourceView<QuestionnaireReportServicesOther>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportServicesOtherDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the QuestionnaireReportServicesOtherDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public QuestionnaireReportServicesOtherDataSourceView(QuestionnaireReportServicesOtherDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal QuestionnaireReportServicesOtherDataSource QuestionnaireReportServicesOtherOwner
		{
			get { return Owner as QuestionnaireReportServicesOtherDataSource; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal QuestionnaireReportServicesOtherService QuestionnaireReportServicesOtherProvider
		{
			get { return Provider as QuestionnaireReportServicesOtherService; }
		}

		#endregion Properties
		
		#region Methods
		
		#endregion Methods
	}

	#region QuestionnaireReportServicesOtherDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the QuestionnaireReportServicesOtherDataSource class.
	/// </summary>
	public class QuestionnaireReportServicesOtherDataSourceDesigner : ReadOnlyDataSourceDesigner<QuestionnaireReportServicesOther>
	{
	}

	#endregion QuestionnaireReportServicesOtherDataSourceDesigner

	#region QuestionnaireReportServicesOtherFilter

	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireReportServicesOther"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireReportServicesOtherFilter : SqlFilter<QuestionnaireReportServicesOtherColumn>
	{
	}

	#endregion QuestionnaireReportServicesOtherFilter

	#region QuestionnaireReportServicesOtherExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireReportServicesOther"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireReportServicesOtherExpressionBuilder : SqlExpressionBuilder<QuestionnaireReportServicesOtherColumn>
	{
	}
	
	#endregion QuestionnaireReportServicesOtherExpressionBuilder		
}

