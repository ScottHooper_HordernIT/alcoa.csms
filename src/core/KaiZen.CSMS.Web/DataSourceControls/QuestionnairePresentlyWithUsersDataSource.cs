﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.QuestionnairePresentlyWithUsersProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(QuestionnairePresentlyWithUsersDataSourceDesigner))]
	public class QuestionnairePresentlyWithUsersDataSource : ProviderDataSource<QuestionnairePresentlyWithUsers, QuestionnairePresentlyWithUsersKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnairePresentlyWithUsersDataSource class.
		/// </summary>
		public QuestionnairePresentlyWithUsersDataSource() : base(new QuestionnairePresentlyWithUsersService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the QuestionnairePresentlyWithUsersDataSourceView used by the QuestionnairePresentlyWithUsersDataSource.
		/// </summary>
		protected QuestionnairePresentlyWithUsersDataSourceView QuestionnairePresentlyWithUsersView
		{
			get { return ( View as QuestionnairePresentlyWithUsersDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the QuestionnairePresentlyWithUsersDataSource control invokes to retrieve data.
		/// </summary>
		public QuestionnairePresentlyWithUsersSelectMethod SelectMethod
		{
			get
			{
				QuestionnairePresentlyWithUsersSelectMethod selectMethod = QuestionnairePresentlyWithUsersSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (QuestionnairePresentlyWithUsersSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the QuestionnairePresentlyWithUsersDataSourceView class that is to be
		/// used by the QuestionnairePresentlyWithUsersDataSource.
		/// </summary>
		/// <returns>An instance of the QuestionnairePresentlyWithUsersDataSourceView class.</returns>
		protected override BaseDataSourceView<QuestionnairePresentlyWithUsers, QuestionnairePresentlyWithUsersKey> GetNewDataSourceView()
		{
			return new QuestionnairePresentlyWithUsersDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the QuestionnairePresentlyWithUsersDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class QuestionnairePresentlyWithUsersDataSourceView : ProviderDataSourceView<QuestionnairePresentlyWithUsers, QuestionnairePresentlyWithUsersKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnairePresentlyWithUsersDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the QuestionnairePresentlyWithUsersDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public QuestionnairePresentlyWithUsersDataSourceView(QuestionnairePresentlyWithUsersDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal QuestionnairePresentlyWithUsersDataSource QuestionnairePresentlyWithUsersOwner
		{
			get { return Owner as QuestionnairePresentlyWithUsersDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal QuestionnairePresentlyWithUsersSelectMethod SelectMethod
		{
			get { return QuestionnairePresentlyWithUsersOwner.SelectMethod; }
			set { QuestionnairePresentlyWithUsersOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal QuestionnairePresentlyWithUsersService QuestionnairePresentlyWithUsersProvider
		{
			get { return Provider as QuestionnairePresentlyWithUsersService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<QuestionnairePresentlyWithUsers> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<QuestionnairePresentlyWithUsers> results = null;
			QuestionnairePresentlyWithUsers item;
			count = 0;
			
			System.Int32 _questionnairePresentlyWithUserId;
			System.String _userName;

			switch ( SelectMethod )
			{
				case QuestionnairePresentlyWithUsersSelectMethod.Get:
					QuestionnairePresentlyWithUsersKey entityKey  = new QuestionnairePresentlyWithUsersKey();
					entityKey.Load(values);
					item = QuestionnairePresentlyWithUsersProvider.Get(entityKey);
					results = new TList<QuestionnairePresentlyWithUsers>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case QuestionnairePresentlyWithUsersSelectMethod.GetAll:
                    results = QuestionnairePresentlyWithUsersProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case QuestionnairePresentlyWithUsersSelectMethod.GetPaged:
					results = QuestionnairePresentlyWithUsersProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case QuestionnairePresentlyWithUsersSelectMethod.Find:
					if ( FilterParameters != null )
						results = QuestionnairePresentlyWithUsersProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = QuestionnairePresentlyWithUsersProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case QuestionnairePresentlyWithUsersSelectMethod.GetByQuestionnairePresentlyWithUserId:
					_questionnairePresentlyWithUserId = ( values["QuestionnairePresentlyWithUserId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["QuestionnairePresentlyWithUserId"], typeof(System.Int32)) : (int)0;
					item = QuestionnairePresentlyWithUsersProvider.GetByQuestionnairePresentlyWithUserId(_questionnairePresentlyWithUserId);
					results = new TList<QuestionnairePresentlyWithUsers>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				case QuestionnairePresentlyWithUsersSelectMethod.GetByUserName:
					_userName = ( values["UserName"] != null ) ? (System.String) EntityUtil.ChangeType(values["UserName"], typeof(System.String)) : string.Empty;
					item = QuestionnairePresentlyWithUsersProvider.GetByUserName(_userName);
					results = new TList<QuestionnairePresentlyWithUsers>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// FK
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == QuestionnairePresentlyWithUsersSelectMethod.Get || SelectMethod == QuestionnairePresentlyWithUsersSelectMethod.GetByQuestionnairePresentlyWithUserId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				QuestionnairePresentlyWithUsers entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					QuestionnairePresentlyWithUsersProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<QuestionnairePresentlyWithUsers> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			QuestionnairePresentlyWithUsersProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region QuestionnairePresentlyWithUsersDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the QuestionnairePresentlyWithUsersDataSource class.
	/// </summary>
	public class QuestionnairePresentlyWithUsersDataSourceDesigner : ProviderDataSourceDesigner<QuestionnairePresentlyWithUsers, QuestionnairePresentlyWithUsersKey>
	{
		/// <summary>
		/// Initializes a new instance of the QuestionnairePresentlyWithUsersDataSourceDesigner class.
		/// </summary>
		public QuestionnairePresentlyWithUsersDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public QuestionnairePresentlyWithUsersSelectMethod SelectMethod
		{
			get { return ((QuestionnairePresentlyWithUsersDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new QuestionnairePresentlyWithUsersDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region QuestionnairePresentlyWithUsersDataSourceActionList

	/// <summary>
	/// Supports the QuestionnairePresentlyWithUsersDataSourceDesigner class.
	/// </summary>
	internal class QuestionnairePresentlyWithUsersDataSourceActionList : DesignerActionList
	{
		private QuestionnairePresentlyWithUsersDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the QuestionnairePresentlyWithUsersDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public QuestionnairePresentlyWithUsersDataSourceActionList(QuestionnairePresentlyWithUsersDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public QuestionnairePresentlyWithUsersSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion QuestionnairePresentlyWithUsersDataSourceActionList
	
	#endregion QuestionnairePresentlyWithUsersDataSourceDesigner
	
	#region QuestionnairePresentlyWithUsersSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the QuestionnairePresentlyWithUsersDataSource.SelectMethod property.
	/// </summary>
	public enum QuestionnairePresentlyWithUsersSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByQuestionnairePresentlyWithUserId method.
		/// </summary>
		GetByQuestionnairePresentlyWithUserId,
		/// <summary>
		/// Represents the GetByUserName method.
		/// </summary>
		GetByUserName
	}
	
	#endregion QuestionnairePresentlyWithUsersSelectMethod

	#region QuestionnairePresentlyWithUsersFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnairePresentlyWithUsers"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnairePresentlyWithUsersFilter : SqlFilter<QuestionnairePresentlyWithUsersColumn>
	{
	}
	
	#endregion QuestionnairePresentlyWithUsersFilter

	#region QuestionnairePresentlyWithUsersExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnairePresentlyWithUsers"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnairePresentlyWithUsersExpressionBuilder : SqlExpressionBuilder<QuestionnairePresentlyWithUsersColumn>
	{
	}
	
	#endregion QuestionnairePresentlyWithUsersExpressionBuilder	

	#region QuestionnairePresentlyWithUsersProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;QuestionnairePresentlyWithUsersChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnairePresentlyWithUsers"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnairePresentlyWithUsersProperty : ChildEntityProperty<QuestionnairePresentlyWithUsersChildEntityTypes>
	{
	}
	
	#endregion QuestionnairePresentlyWithUsersProperty
}

