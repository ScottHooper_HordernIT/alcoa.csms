﻿
using System;

namespace KaiZen.CSMS.Entities
{
	/// <summary>
	/// An enum representation of the 'CsmsFileType' table. [No description found in the database]
	/// </summary>
	/// <remark>this enumeration contains the items contained in the table CsmsFileType</remark>
	[Serializable]
	public enum CsmsFileTypeList
	{
		/// <summary> 
		/// Help File - All
		/// </summary>
		[EnumTextValue(@"Help File - All")]
		Help_File_All = 1, 

		/// <summary> 
		/// Help File - Procurement
		/// </summary>
		[EnumTextValue(@"Help File - Procurement")]
		Help_File_Procurement = 2, 

		/// <summary> 
		/// Help File - Alcoa Reader
		/// </summary>
		[EnumTextValue(@"Help File - Alcoa Reader")]
		Help_File_Alcoa_Reader = 3, 

		/// <summary> 
		/// Help File - H&S Assessor
		/// </summary>
		[EnumTextValue(@"Help File - H&S Assessor")]
		Help_File_H_S_Assessor = 4, 

		/// <summary> 
		/// Help File - Training Package Trainer
		/// </summary>
		[EnumTextValue(@"Help File - Training Package Trainer")]
		Help_File_Training_Package_Trainer = 5, 

		/// <summary> 
		/// Help File - Contractor
		/// </summary>
		[EnumTextValue(@"Help File - Contractor")]
		Help_File_Contractor = 6, 

		/// <summary> 
		/// Help File - EBI Data Access
		/// </summary>
		[EnumTextValue(@"Help File - EBI Data Access")]
		Help_File_EBI_Data_Access = 7, 

		/// <summary> 
		/// Help File - Administrator
		/// </summary>
		[EnumTextValue(@"Help File - Administrator")]
		Help_File_Administrator = 8, 

		/// <summary> 
		/// Help File - Financial Reporting Access
		/// </summary>
		[EnumTextValue(@"Help File - Financial Reporting Access")]
		Help_File_Financial_Reporting_Access = 9, 

		/// <summary> 
		/// Help File - View Engineering Project Hours
		/// </summary>
		[EnumTextValue(@"Help File - View Engineering Project Hours")]
		Help_File_View_Engineering_Project_Hours = 10, 

		/// <summary> 
		/// Help File - Pre-Qual
		/// </summary>
		[EnumTextValue(@"Help File - Pre-Qual")]
		Help_File_Pre_Qual = 11

	}
}
