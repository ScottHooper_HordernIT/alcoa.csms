﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SafetyDashboardPlan.ascx.cs" Inherits="SafetyDashboardPlan" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
    
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxRoundPanel" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxPanel" tagprefix="dx" %>
    <%@ Register assembly="DevExpress.Web.v14.1" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
   

   <table border="0" cellpadding="2" cellspacing="0" style="width: 92%">
 
   <tr>
                    <%--<td style="text-align: right" class="style4">
                        <strong>Site:</strong>
                    </td>
                    <td style="text-align: right; width: 200px" class="style3">
                        <dxe:ASPxComboBox ID="cbRegionSite" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                            CssPostfix="Office2003Blue" IncrementalFilteringMode="StartsWith" SelectedIndex="0"
                            ValueType="System.Int32" Width="200px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                            <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                            </LoadingPanelImage>
                            <ButtonStyle Width="13px">
                            </ButtonStyle>
                        </dxe:ASPxComboBox>
                    </td>--%>
                    
                       <td style="width: 11%; text-align: right">
                                                    <strong>Month/Year:</strong>
                                                 </td>
                                                <td style="width: 11%; height: 10px; text-align: left">
                                                    <dx:ASPxComboBox runat="server" IncrementalFilteringMode="StartsWith" SelectedIndex="0"
                                                        ValueType="System.Int32" CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                        Width="90px" ID="ddlMonth" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                                        <Items>
                                                            <dx:ListEditItem Text="January" Value="1"></dx:ListEditItem>
                                                            <dx:ListEditItem Text="February" Value="2"></dx:ListEditItem>
                                                            <dx:ListEditItem Text="March" Value="3"></dx:ListEditItem>
                                                            <dx:ListEditItem Text="April" Value="4"></dx:ListEditItem>
                                                            <dx:ListEditItem Text="May" Value="5"></dx:ListEditItem>
                                                            <dx:ListEditItem Text="June" Value="6"></dx:ListEditItem>
                                                            <dx:ListEditItem Text="July" Value="7"></dx:ListEditItem>
                                                            <dx:ListEditItem Text="August" Value="8"></dx:ListEditItem>
                                                            <dx:ListEditItem Text="September" Value="9"></dx:ListEditItem>
                                                            <dx:ListEditItem Text="October" Value="10"></dx:ListEditItem>
                                                            <dx:ListEditItem Text="November" Value="11"></dx:ListEditItem>
                                                            <dx:ListEditItem Text="December" Value="12"></dx:ListEditItem>
                                                        </Items>
                                                        <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                                                        </LoadingPanelImage>
                                                        <ButtonStyle Width="13px">
                                                        </ButtonStyle>
                                                    </dx:ASPxComboBox>
                                                </td>

                    <%--<td style="width: 46px; height: 10px; text-align: right">
                        <strong>Year:</strong>
                    </td>
                    --%>
                    <%--<td width="20px"/>--%>
                    <td style="text-align: right; width: 8%" class="style2">
                        <dxe:ASPxComboBox ID="ddlYear" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                            CssPostfix="Office2003Blue" IncrementalFilteringMode="StartsWith" ValueType="System.Int32"
                            Width="60px" 
                            SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" 
                            style="margin-left: 0px">
                            <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                            </LoadingPanelImage>
                            <ButtonStyle Cursor="pointer" Width="13px">
                            </ButtonStyle>
                        </dxe:ASPxComboBox>
                      
                    </td>
                    <td style="text-align: left; width: 70%" class="style1">
                        <dxe:ASPxButton ID="ASPxButton1" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                            CssPostfix="Office2003Blue" Text="Go / Refresh" 
                            SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" onclick="ASPxButton1_Click"  
                            >
                        </dxe:ASPxButton>
                    </td>
                </tr>
  
 
  </table>
  <br />
    <dxwgv:ASPxGridView
    ID="grid" 
    runat="server" AutoGenerateColumns="False"
    CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
    CssPostfix="Office2003Blue"
    

    KeyFieldName="DashboardId" 
    Width="900px" OnAfterPerformCallback="grid_AfterPerformCallback" 
    OnRowUpdating="grid_RowUpdating" OnStartRowEditing="grid_StartRowEditing" OnRowValidating="grid_RowValidating">
    <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
        <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
        </LoadingPanelOnStatusBar>
        <CollapsedButton Height="12px" Width="11px" />
        <ExpandedButton Height="12px" Width="11px" />
        <DetailCollapsedButton Height="12px" Width="11px" />
        <DetailExpandedButton Height="12px" Width="11px" />
        <LoadingPanel Url="~/App_Themes/Office2003BlueOld/GridView/Loading.gif"><%--Enhancement_023:change by debashis for testing platform upgradation--%>
        </LoadingPanel>
    </Images>
    <ImagesFilterControl>
        <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
        </LoadingPanel>
    </ImagesFilterControl>
    <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
        CssPostfix="Office2003Blue">
        <Header ImageSpacing="5px" SortingImageSpacing="5px" />
        <LoadingPanel ImageSpacing="10px" />
    </Styles>
    <Columns>
        <dxwgv:GridViewCommandColumn Caption="Action" VisibleIndex="0" Width="100px">
            <EditButton Visible="True">
            </EditButton>
                        <ClearFilterButton Visible="True">
            </ClearFilterButton>
            <HeaderStyle HorizontalAlign="Center" />
        </dxwgv:GridViewCommandColumn>
        <dxwgv:GridViewDataTextColumn FieldName="DashboardPlanId" ReadOnly="True" Visible="False" VisibleIndex="0">
            <EditFormSettings Visible="False" />
            <HeaderStyle HorizontalAlign="Center" />
        </dxwgv:GridViewDataTextColumn>
        <dxwgv:GridViewDataTextColumn FieldName="DashboardId" ReadOnly="True" Visible="False" VisibleIndex="1">
            <EditFormSettings Visible="False" />
            <HeaderStyle HorizontalAlign="Center" />
        </dxwgv:GridViewDataTextColumn>
        <dxwgv:GridViewDataTextColumn Caption="Metric" FieldName="FieldName"
         ReadOnly="True" VisibleIndex="2" Width="280px">
         <HeaderStyle HorizontalAlign="Center" />
        </dxwgv:GridViewDataTextColumn>
        
        <dxwgv:GridViewDataComboBoxColumn Caption="Operators" FieldName="Operator" Name="Operator" VisibleIndex="3">
            <PropertiesComboBox IncrementalFilteringMode="StartsWith" DataSourceID="sqldsOperatorsList"
                TextField="Operator" ValueField="OperatorId" ValueType="System.Int32" DropDownHeight="150px">
                
            </PropertiesComboBox>
            <HeaderStyle HorizontalAlign="Center" />
            <%--<EditItemTemplate>

                <dxe:ASPxComboBox ID="ddlOperator" runat="server" DataSourceID="sqldsOperatorsList" 
                 TextField="Operator" ValueField="OperatorId" ValueType="System.Int32"  
                 IncrementalFilteringMode="StartsWith" Width="150px"
                CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"></dxe:ASPxComboBox>
            </EditItemTemplate>--%>

            <%--<DataItemTemplate>
                <asp:TextBox id="txtOperators" runat="server" Text='<%# Eval("Operator")%>'>></asp:TextBox>
            </DataItemTemplate>--%>
        </dxwgv:GridViewDataComboBoxColumn>
        
        <dxwgv:GridViewDataTextColumn Caption="Plan Value" FieldName="PlanValue" VisibleIndex="4">
            <%--<DataItemTemplate>
                <asp:TextBox id="txtOperators" runat="server" Text='<%# Eval("PlanValue")%>'>></asp:TextBox>
            </DataItemTemplate>--%>
            <HeaderStyle HorizontalAlign="Center" />
        </dxwgv:GridViewDataTextColumn>
        
        <dxwgv:GridViewDataTextColumn Caption="Plan Text" FieldName="PlanText" VisibleIndex="5">
            <%--<DataItemTemplate>
                <asp:TextBox id="txtOperators" runat="server" Text='<%# Eval("PlanText")%>'>></asp:TextBox>
            </DataItemTemplate>--%>
            <HeaderStyle HorizontalAlign="Center" />
        </dxwgv:GridViewDataTextColumn>



    </Columns>
    <Settings ShowHeaderFilterButton="false" />
    <SettingsPager Visible="False" Mode="ShowAllRecords" />
    <SettingsEditing Mode="Inline" />
    <SettingsBehavior ColumnResizeMode="NextColumn" ConfirmDelete="True"></SettingsBehavior>
</dxwgv:ASPxGridView>

<div style="text-align:right">
   
    <dxe:ASPxButton ID="ASPxButton2" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
        CssPostfix="Office2003Blue" Text="Save" visible="false"
        SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" onclick="ASPxButton2_Click"  
        >
    </dxe:ASPxButton>
</div>

    <asp:sqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="Get_AdminPlan_DashboardName" SelectCommandType="StoredProcedure"
    >
    <%--UpdateCommand="SafetyDashBoardPlan_UpdateGrid" UpdateCommandType="StoredProcedure"--%>
    <SelectParameters>
        <asp:ControlParameter ControlID="ddlMonth" Name="Month" Type="Int32" />
        <asp:ControlParameter ControlID="ddlYear" Name="Year" Type="Int32" />
    </SelectParameters>
    <%--<UpdateParameters>
        <asp:Parameter Name="PlanText" Type="String" />
        <asp:Parameter Name="DashboardId" Type="Int32" />
    </UpdateParameters>--%>
</asp:sqlDataSource>

<asp:SqlDataSource ID="sqldsOperatorsList" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="_GetOperatorList" SelectCommandType="StoredProcedure">
   

</asp:SqlDataSource>
