﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.OrphanQuestionnaireProcurementContactProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[CLSCompliant(true)]
	[Designer(typeof(OrphanQuestionnaireProcurementContactDataSourceDesigner))]
	public class OrphanQuestionnaireProcurementContactDataSource : ReadOnlyDataSource<OrphanQuestionnaireProcurementContact>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the OrphanQuestionnaireProcurementContactDataSource class.
		/// </summary>
		public OrphanQuestionnaireProcurementContactDataSource() : base(new OrphanQuestionnaireProcurementContactService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the OrphanQuestionnaireProcurementContactDataSourceView used by the OrphanQuestionnaireProcurementContactDataSource.
		/// </summary>
		protected OrphanQuestionnaireProcurementContactDataSourceView OrphanQuestionnaireProcurementContactView
		{
			get { return ( View as OrphanQuestionnaireProcurementContactDataSourceView ); }
		}
		
		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the OrphanQuestionnaireProcurementContactDataSourceView class that is to be
		/// used by the OrphanQuestionnaireProcurementContactDataSource.
		/// </summary>
		/// <returns>An instance of the OrphanQuestionnaireProcurementContactDataSourceView class.</returns>
		protected override BaseDataSourceView<OrphanQuestionnaireProcurementContact, Object> GetNewDataSourceView()
		{
			return new OrphanQuestionnaireProcurementContactDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the OrphanQuestionnaireProcurementContactDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class OrphanQuestionnaireProcurementContactDataSourceView : ReadOnlyDataSourceView<OrphanQuestionnaireProcurementContact>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the OrphanQuestionnaireProcurementContactDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the OrphanQuestionnaireProcurementContactDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public OrphanQuestionnaireProcurementContactDataSourceView(OrphanQuestionnaireProcurementContactDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal OrphanQuestionnaireProcurementContactDataSource OrphanQuestionnaireProcurementContactOwner
		{
			get { return Owner as OrphanQuestionnaireProcurementContactDataSource; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal OrphanQuestionnaireProcurementContactService OrphanQuestionnaireProcurementContactProvider
		{
			get { return Provider as OrphanQuestionnaireProcurementContactService; }
		}

		#endregion Properties
		
		#region Methods
		
		#endregion Methods
	}

	#region OrphanQuestionnaireProcurementContactDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the OrphanQuestionnaireProcurementContactDataSource class.
	/// </summary>
	public class OrphanQuestionnaireProcurementContactDataSourceDesigner : ReadOnlyDataSourceDesigner<OrphanQuestionnaireProcurementContact>
	{
	}

	#endregion OrphanQuestionnaireProcurementContactDataSourceDesigner

	#region OrphanQuestionnaireProcurementContactFilter

	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="OrphanQuestionnaireProcurementContact"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class OrphanQuestionnaireProcurementContactFilter : SqlFilter<OrphanQuestionnaireProcurementContactColumn>
	{
	}

	#endregion OrphanQuestionnaireProcurementContactFilter

	#region OrphanQuestionnaireProcurementContactExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="OrphanQuestionnaireProcurementContact"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class OrphanQuestionnaireProcurementContactExpressionBuilder : SqlExpressionBuilder<OrphanQuestionnaireProcurementContactColumn>
	{
	}
	
	#endregion OrphanQuestionnaireProcurementContactExpressionBuilder		
}

