
/****** Object:  StoredProcedure [dbo].[Ebi_UpdateVendorNo]    Script Date: 3/08/2015 4:44:34 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Ebi_UpdateVendorNo]    
(    
 @ClockId int,    
 @EbiId  int,  
 @SwipeDateTime DATETIME  
)    
AS    
 DECLARE @VendorNo VARCHAR(MAX);    
   
 SELECT TOP 1 @VendorNo=CWK_VENDOR_NO  
 FROM HR..XXHR_CWK_HISTORY_V
 WHERE CWK_NBR=@ClockId AND EFFECTIVE_END_DATE>=@SwipeDateTime  
 ORDER BY EFFECTIVE_END_DATE ASC  
        
 -- Modify the updatable columns    
 UPDATE    
 [dbo].[Ebi]    
 SET    
 [Vendor_Number] = @VendorNo    
 WHERE    
 [EbiId] = @EbiId;    

-----------------------------------------------------------------------------------------------------

GO


