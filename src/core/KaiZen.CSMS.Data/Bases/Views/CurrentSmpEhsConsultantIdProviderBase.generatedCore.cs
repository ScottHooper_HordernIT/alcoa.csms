﻿#region Using directives

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="CurrentSmpEhsConsultantIdProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract class CurrentSmpEhsConsultantIdProviderBaseCore : EntityViewProviderBase<CurrentSmpEhsConsultantId>
	{
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions
		
		/*
		///<summary>
		/// Fill an VList&lt;CurrentSmpEhsConsultantId&gt; From a DataSet
		///</summary>
		/// <param name="dataSet">the DataSet</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList&lt;CurrentSmpEhsConsultantId&gt;"/></returns>
		protected static VList&lt;CurrentSmpEhsConsultantId&gt; Fill(DataSet dataSet, VList<CurrentSmpEhsConsultantId> rows, int start, int pagelen)
		{
			if (dataSet.Tables.Count == 1)
			{
				return Fill(dataSet.Tables[0], rows, start, pagelen);
			}
			else
			{
				return new VList<CurrentSmpEhsConsultantId>();
			}	
		}
		
		
		///<summary>
		/// Fill an VList&lt;CurrentSmpEhsConsultantId&gt; From a DataTable
		///</summary>
		/// <param name="dataTable">the DataTable that hold the data.</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList<CurrentSmpEhsConsultantId>"/></returns>
		protected static VList&lt;CurrentSmpEhsConsultantId&gt; Fill(DataTable dataTable, VList<CurrentSmpEhsConsultantId> rows, int start, int pagelen)
		{
			int recordnum = 0;
			
			System.Collections.IEnumerator dataRows =  dataTable.Rows.GetEnumerator();
			
			while (dataRows.MoveNext() && (pagelen != 0))
			{
				if(recordnum >= start)
				{
					DataRow row = (DataRow)dataRows.Current;
				
					CurrentSmpEhsConsultantId c = new CurrentSmpEhsConsultantId();
					c.EhsConsultantId = (Convert.IsDBNull(row["EHSConsultantId"]))?(int)0:(System.Int32)row["EHSConsultantId"];
					c.UserFullName = (Convert.IsDBNull(row["UserFullName"]))?string.Empty:(System.String)row["UserFullName"];
					c.AcceptChanges();
					rows.Add(c);
					pagelen -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		*/	
						
		///<summary>
		/// Fill an <see cref="VList&lt;CurrentSmpEhsConsultantId&gt;"/> From a DataReader.
		///</summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pageLength">number of row.</param>
		///<returns>a <see cref="VList&lt;CurrentSmpEhsConsultantId&gt;"/></returns>
		protected VList<CurrentSmpEhsConsultantId> Fill(IDataReader reader, VList<CurrentSmpEhsConsultantId> rows, int start, int pageLength)
		{
			int recordnum = 0;
			while (reader.Read() && (pageLength != 0))
			{
				if(recordnum >= start)
				{
					CurrentSmpEhsConsultantId entity = null;
					if (DataRepository.Provider.UseEntityFactory)
					{
						entity = EntityManager.CreateViewEntity<CurrentSmpEhsConsultantId>("CurrentSmpEhsConsultantId",  DataRepository.Provider.EntityCreationalFactoryType); 
					}
					else
					{
						entity = new CurrentSmpEhsConsultantId();
					}
					
					entity.SuppressEntityEvents = true;

					entity.EhsConsultantId = (System.Int32)reader[((int)CurrentSmpEhsConsultantIdColumn.EhsConsultantId)];
					//entity.EhsConsultantId = (Convert.IsDBNull(reader["EHSConsultantId"]))?(int)0:(System.Int32)reader["EHSConsultantId"];
					entity.UserFullName = (System.String)reader[((int)CurrentSmpEhsConsultantIdColumn.UserFullName)];
					//entity.UserFullName = (Convert.IsDBNull(reader["UserFullName"]))?string.Empty:(System.String)reader["UserFullName"];
					entity.AcceptChanges();
					entity.SuppressEntityEvents = false;
					
					rows.Add(entity);
					pageLength -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		
		
		/// <summary>
		/// Refreshes the <see cref="CurrentSmpEhsConsultantId"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="CurrentSmpEhsConsultantId"/> object to refresh.</param>
		protected void RefreshEntity(IDataReader reader, CurrentSmpEhsConsultantId entity)
		{
			reader.Read();
			entity.EhsConsultantId = (System.Int32)reader[((int)CurrentSmpEhsConsultantIdColumn.EhsConsultantId)];
			//entity.EhsConsultantId = (Convert.IsDBNull(reader["EHSConsultantId"]))?(int)0:(System.Int32)reader["EHSConsultantId"];
			entity.UserFullName = (System.String)reader[((int)CurrentSmpEhsConsultantIdColumn.UserFullName)];
			//entity.UserFullName = (Convert.IsDBNull(reader["UserFullName"]))?string.Empty:(System.String)reader["UserFullName"];
			reader.Close();
	
			entity.AcceptChanges();
		}
		
		/*
		/// <summary>
		/// Refreshes the <see cref="CurrentSmpEhsConsultantId"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="CurrentSmpEhsConsultantId"/> object.</param>
		protected static void RefreshEntity(DataSet dataSet, CurrentSmpEhsConsultantId entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.EhsConsultantId = (Convert.IsDBNull(dataRow["EHSConsultantId"]))?(int)0:(System.Int32)dataRow["EHSConsultantId"];
			entity.UserFullName = (Convert.IsDBNull(dataRow["UserFullName"]))?string.Empty:(System.String)dataRow["UserFullName"];
			entity.AcceptChanges();
		}
		*/
			
		#endregion Helper Functions
	}//end class

	#region CurrentSmpEhsConsultantIdFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CurrentSmpEhsConsultantId"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CurrentSmpEhsConsultantIdFilterBuilder : SqlFilterBuilder<CurrentSmpEhsConsultantIdColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CurrentSmpEhsConsultantIdFilterBuilder class.
		/// </summary>
		public CurrentSmpEhsConsultantIdFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the CurrentSmpEhsConsultantIdFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CurrentSmpEhsConsultantIdFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CurrentSmpEhsConsultantIdFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CurrentSmpEhsConsultantIdFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CurrentSmpEhsConsultantIdFilterBuilder

	#region CurrentSmpEhsConsultantIdParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CurrentSmpEhsConsultantId"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CurrentSmpEhsConsultantIdParameterBuilder : ParameterizedSqlFilterBuilder<CurrentSmpEhsConsultantIdColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CurrentSmpEhsConsultantIdParameterBuilder class.
		/// </summary>
		public CurrentSmpEhsConsultantIdParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the CurrentSmpEhsConsultantIdParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CurrentSmpEhsConsultantIdParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CurrentSmpEhsConsultantIdParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CurrentSmpEhsConsultantIdParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CurrentSmpEhsConsultantIdParameterBuilder
	
	#region CurrentSmpEhsConsultantIdSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CurrentSmpEhsConsultantId"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class CurrentSmpEhsConsultantIdSortBuilder : SqlSortBuilder<CurrentSmpEhsConsultantIdColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CurrentSmpEhsConsultantIdSqlSortBuilder class.
		/// </summary>
		public CurrentSmpEhsConsultantIdSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion CurrentSmpEhsConsultantIdSortBuilder

} // end namespace
