﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.SafetyPlanStatusProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(SafetyPlanStatusDataSourceDesigner))]
	public class SafetyPlanStatusDataSource : ProviderDataSource<SafetyPlanStatus, SafetyPlanStatusKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SafetyPlanStatusDataSource class.
		/// </summary>
		public SafetyPlanStatusDataSource() : base(new SafetyPlanStatusService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the SafetyPlanStatusDataSourceView used by the SafetyPlanStatusDataSource.
		/// </summary>
		protected SafetyPlanStatusDataSourceView SafetyPlanStatusView
		{
			get { return ( View as SafetyPlanStatusDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the SafetyPlanStatusDataSource control invokes to retrieve data.
		/// </summary>
		public SafetyPlanStatusSelectMethod SelectMethod
		{
			get
			{
				SafetyPlanStatusSelectMethod selectMethod = SafetyPlanStatusSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (SafetyPlanStatusSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the SafetyPlanStatusDataSourceView class that is to be
		/// used by the SafetyPlanStatusDataSource.
		/// </summary>
		/// <returns>An instance of the SafetyPlanStatusDataSourceView class.</returns>
		protected override BaseDataSourceView<SafetyPlanStatus, SafetyPlanStatusKey> GetNewDataSourceView()
		{
			return new SafetyPlanStatusDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the SafetyPlanStatusDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class SafetyPlanStatusDataSourceView : ProviderDataSourceView<SafetyPlanStatus, SafetyPlanStatusKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SafetyPlanStatusDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the SafetyPlanStatusDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public SafetyPlanStatusDataSourceView(SafetyPlanStatusDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal SafetyPlanStatusDataSource SafetyPlanStatusOwner
		{
			get { return Owner as SafetyPlanStatusDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal SafetyPlanStatusSelectMethod SelectMethod
		{
			get { return SafetyPlanStatusOwner.SelectMethod; }
			set { SafetyPlanStatusOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal SafetyPlanStatusService SafetyPlanStatusProvider
		{
			get { return Provider as SafetyPlanStatusService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<SafetyPlanStatus> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<SafetyPlanStatus> results = null;
			SafetyPlanStatus item;
			count = 0;
			
			System.Int32 _statusId;
			System.String _statusName;

			switch ( SelectMethod )
			{
				case SafetyPlanStatusSelectMethod.Get:
					SafetyPlanStatusKey entityKey  = new SafetyPlanStatusKey();
					entityKey.Load(values);
					item = SafetyPlanStatusProvider.Get(entityKey);
					results = new TList<SafetyPlanStatus>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case SafetyPlanStatusSelectMethod.GetAll:
                    results = SafetyPlanStatusProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case SafetyPlanStatusSelectMethod.GetPaged:
					results = SafetyPlanStatusProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case SafetyPlanStatusSelectMethod.Find:
					if ( FilterParameters != null )
						results = SafetyPlanStatusProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = SafetyPlanStatusProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case SafetyPlanStatusSelectMethod.GetByStatusId:
					_statusId = ( values["StatusId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["StatusId"], typeof(System.Int32)) : (int)0;
					item = SafetyPlanStatusProvider.GetByStatusId(_statusId);
					results = new TList<SafetyPlanStatus>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				case SafetyPlanStatusSelectMethod.GetByStatusName:
					_statusName = ( values["StatusName"] != null ) ? (System.String) EntityUtil.ChangeType(values["StatusName"], typeof(System.String)) : string.Empty;
					item = SafetyPlanStatusProvider.GetByStatusName(_statusName);
					results = new TList<SafetyPlanStatus>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// FK
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == SafetyPlanStatusSelectMethod.Get || SelectMethod == SafetyPlanStatusSelectMethod.GetByStatusId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				SafetyPlanStatus entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					SafetyPlanStatusProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<SafetyPlanStatus> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			SafetyPlanStatusProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region SafetyPlanStatusDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the SafetyPlanStatusDataSource class.
	/// </summary>
	public class SafetyPlanStatusDataSourceDesigner : ProviderDataSourceDesigner<SafetyPlanStatus, SafetyPlanStatusKey>
	{
		/// <summary>
		/// Initializes a new instance of the SafetyPlanStatusDataSourceDesigner class.
		/// </summary>
		public SafetyPlanStatusDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public SafetyPlanStatusSelectMethod SelectMethod
		{
			get { return ((SafetyPlanStatusDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new SafetyPlanStatusDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region SafetyPlanStatusDataSourceActionList

	/// <summary>
	/// Supports the SafetyPlanStatusDataSourceDesigner class.
	/// </summary>
	internal class SafetyPlanStatusDataSourceActionList : DesignerActionList
	{
		private SafetyPlanStatusDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the SafetyPlanStatusDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public SafetyPlanStatusDataSourceActionList(SafetyPlanStatusDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public SafetyPlanStatusSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion SafetyPlanStatusDataSourceActionList
	
	#endregion SafetyPlanStatusDataSourceDesigner
	
	#region SafetyPlanStatusSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the SafetyPlanStatusDataSource.SelectMethod property.
	/// </summary>
	public enum SafetyPlanStatusSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByStatusId method.
		/// </summary>
		GetByStatusId,
		/// <summary>
		/// Represents the GetByStatusName method.
		/// </summary>
		GetByStatusName
	}
	
	#endregion SafetyPlanStatusSelectMethod

	#region SafetyPlanStatusFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SafetyPlanStatus"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SafetyPlanStatusFilter : SqlFilter<SafetyPlanStatusColumn>
	{
	}
	
	#endregion SafetyPlanStatusFilter

	#region SafetyPlanStatusExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SafetyPlanStatus"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SafetyPlanStatusExpressionBuilder : SqlExpressionBuilder<SafetyPlanStatusColumn>
	{
	}
	
	#endregion SafetyPlanStatusExpressionBuilder	

	#region SafetyPlanStatusProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;SafetyPlanStatusChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="SafetyPlanStatus"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SafetyPlanStatusProperty : ChildEntityProperty<SafetyPlanStatusChildEntityTypes>
	{
	}
	
	#endregion SafetyPlanStatusProperty
}

