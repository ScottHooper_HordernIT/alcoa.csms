﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="EbiMetricProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class EbiMetricProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.EbiMetric, KaiZen.CSMS.Entities.EbiMetricKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.EbiMetricKey key)
		{
			return Delete(transactionManager, key.EbiMetricId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_ebiMetricId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _ebiMetricId)
		{
			return Delete(null, _ebiMetricId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_ebiMetricId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _ebiMetricId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_EbiMetric_Sites key.
		///		FK_EbiMetric_Sites Description: 
		/// </summary>
		/// <param name="_siteId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.EbiMetric objects.</returns>
		public TList<EbiMetric> GetBySiteId(System.Int32 _siteId)
		{
			int count = -1;
			return GetBySiteId(_siteId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_EbiMetric_Sites key.
		///		FK_EbiMetric_Sites Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_siteId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.EbiMetric objects.</returns>
		/// <remarks></remarks>
		public TList<EbiMetric> GetBySiteId(TransactionManager transactionManager, System.Int32 _siteId)
		{
			int count = -1;
			return GetBySiteId(transactionManager, _siteId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_EbiMetric_Sites key.
		///		FK_EbiMetric_Sites Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_siteId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.EbiMetric objects.</returns>
		public TList<EbiMetric> GetBySiteId(TransactionManager transactionManager, System.Int32 _siteId, int start, int pageLength)
		{
			int count = -1;
			return GetBySiteId(transactionManager, _siteId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_EbiMetric_Sites key.
		///		fkEbiMetricSites Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_siteId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.EbiMetric objects.</returns>
		public TList<EbiMetric> GetBySiteId(System.Int32 _siteId, int start, int pageLength)
		{
			int count =  -1;
			return GetBySiteId(null, _siteId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_EbiMetric_Sites key.
		///		fkEbiMetricSites Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_siteId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.EbiMetric objects.</returns>
		public TList<EbiMetric> GetBySiteId(System.Int32 _siteId, int start, int pageLength,out int count)
		{
			return GetBySiteId(null, _siteId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_EbiMetric_Sites key.
		///		FK_EbiMetric_Sites Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_siteId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.EbiMetric objects.</returns>
		public abstract TList<EbiMetric> GetBySiteId(TransactionManager transactionManager, System.Int32 _siteId, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.EbiMetric Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.EbiMetricKey key, int start, int pageLength)
		{
			return GetByEbiMetricId(transactionManager, key.EbiMetricId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_EbiMetric index.
		/// </summary>
		/// <param name="_ebiMetricId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.EbiMetric"/> class.</returns>
		public KaiZen.CSMS.Entities.EbiMetric GetByEbiMetricId(System.Int32 _ebiMetricId)
		{
			int count = -1;
			return GetByEbiMetricId(null,_ebiMetricId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_EbiMetric index.
		/// </summary>
		/// <param name="_ebiMetricId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.EbiMetric"/> class.</returns>
		public KaiZen.CSMS.Entities.EbiMetric GetByEbiMetricId(System.Int32 _ebiMetricId, int start, int pageLength)
		{
			int count = -1;
			return GetByEbiMetricId(null, _ebiMetricId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_EbiMetric index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_ebiMetricId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.EbiMetric"/> class.</returns>
		public KaiZen.CSMS.Entities.EbiMetric GetByEbiMetricId(TransactionManager transactionManager, System.Int32 _ebiMetricId)
		{
			int count = -1;
			return GetByEbiMetricId(transactionManager, _ebiMetricId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_EbiMetric index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_ebiMetricId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.EbiMetric"/> class.</returns>
		public KaiZen.CSMS.Entities.EbiMetric GetByEbiMetricId(TransactionManager transactionManager, System.Int32 _ebiMetricId, int start, int pageLength)
		{
			int count = -1;
			return GetByEbiMetricId(transactionManager, _ebiMetricId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_EbiMetric index.
		/// </summary>
		/// <param name="_ebiMetricId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.EbiMetric"/> class.</returns>
		public KaiZen.CSMS.Entities.EbiMetric GetByEbiMetricId(System.Int32 _ebiMetricId, int start, int pageLength, out int count)
		{
			return GetByEbiMetricId(null, _ebiMetricId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_EbiMetric index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_ebiMetricId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.EbiMetric"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.EbiMetric GetByEbiMetricId(TransactionManager transactionManager, System.Int32 _ebiMetricId, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#region _EbiMetric_GetLast30Days_From 
		
		/// <summary>
		///	This method wrap the '_EbiMetric_GetLast30Days_From' stored procedure. 
		/// </summary>
		/// <param name="year"> A <c>System.String</c> instance.</param>
		/// <param name="month"> A <c>System.String</c> instance.</param>
		/// <param name="day"> A <c>System.String</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetLast30Days_From(System.String year, System.String month, System.String day)
		{
			return GetLast30Days_From(null, 0, int.MaxValue , year, month, day);
		}
		
		/// <summary>
		///	This method wrap the '_EbiMetric_GetLast30Days_From' stored procedure. 
		/// </summary>
		/// <param name="year"> A <c>System.String</c> instance.</param>
		/// <param name="month"> A <c>System.String</c> instance.</param>
		/// <param name="day"> A <c>System.String</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetLast30Days_From(int start, int pageLength, System.String year, System.String month, System.String day)
		{
			return GetLast30Days_From(null, start, pageLength , year, month, day);
		}
				
		/// <summary>
		///	This method wrap the '_EbiMetric_GetLast30Days_From' stored procedure. 
		/// </summary>
		/// <param name="year"> A <c>System.String</c> instance.</param>
		/// <param name="month"> A <c>System.String</c> instance.</param>
		/// <param name="day"> A <c>System.String</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetLast30Days_From(TransactionManager transactionManager, System.String year, System.String month, System.String day)
		{
			return GetLast30Days_From(transactionManager, 0, int.MaxValue , year, month, day);
		}
		
		/// <summary>
		///	This method wrap the '_EbiMetric_GetLast30Days_From' stored procedure. 
		/// </summary>
		/// <param name="year"> A <c>System.String</c> instance.</param>
		/// <param name="month"> A <c>System.String</c> instance.</param>
		/// <param name="day"> A <c>System.String</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet GetLast30Days_From(TransactionManager transactionManager, int start, int pageLength , System.String year, System.String month, System.String day);
		
		#endregion
		
		#region _EbiMetric_GetLast7Days_From 
		
		/// <summary>
		///	This method wrap the '_EbiMetric_GetLast7Days_From' stored procedure. 
		/// </summary>
		/// <param name="year"> A <c>System.String</c> instance.</param>
		/// <param name="month"> A <c>System.String</c> instance.</param>
		/// <param name="day"> A <c>System.String</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetLast7Days_From(System.String year, System.String month, System.String day)
		{
			return GetLast7Days_From(null, 0, int.MaxValue , year, month, day);
		}
		
		/// <summary>
		///	This method wrap the '_EbiMetric_GetLast7Days_From' stored procedure. 
		/// </summary>
		/// <param name="year"> A <c>System.String</c> instance.</param>
		/// <param name="month"> A <c>System.String</c> instance.</param>
		/// <param name="day"> A <c>System.String</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetLast7Days_From(int start, int pageLength, System.String year, System.String month, System.String day)
		{
			return GetLast7Days_From(null, start, pageLength , year, month, day);
		}
				
		/// <summary>
		///	This method wrap the '_EbiMetric_GetLast7Days_From' stored procedure. 
		/// </summary>
		/// <param name="year"> A <c>System.String</c> instance.</param>
		/// <param name="month"> A <c>System.String</c> instance.</param>
		/// <param name="day"> A <c>System.String</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetLast7Days_From(TransactionManager transactionManager, System.String year, System.String month, System.String day)
		{
			return GetLast7Days_From(transactionManager, 0, int.MaxValue , year, month, day);
		}
		
		/// <summary>
		///	This method wrap the '_EbiMetric_GetLast7Days_From' stored procedure. 
		/// </summary>
		/// <param name="year"> A <c>System.String</c> instance.</param>
		/// <param name="month"> A <c>System.String</c> instance.</param>
		/// <param name="day"> A <c>System.String</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet GetLast7Days_From(TransactionManager transactionManager, int start, int pageLength , System.String year, System.String month, System.String day);
		
		#endregion
		
		#region _EbiMetric_GetLast7Days 
		
		/// <summary>
		///	This method wrap the '_EbiMetric_GetLast7Days' stored procedure. 
		/// </summary>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetLast7Days()
		{
			return GetLast7Days(null, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_EbiMetric_GetLast7Days' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetLast7Days(int start, int pageLength)
		{
			return GetLast7Days(null, start, pageLength );
		}
				
		/// <summary>
		///	This method wrap the '_EbiMetric_GetLast7Days' stored procedure. 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetLast7Days(TransactionManager transactionManager)
		{
			return GetLast7Days(transactionManager, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_EbiMetric_GetLast7Days' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet GetLast7Days(TransactionManager transactionManager, int start, int pageLength );
		
		#endregion
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;EbiMetric&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;EbiMetric&gt;"/></returns>
		public static TList<EbiMetric> Fill(IDataReader reader, TList<EbiMetric> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.EbiMetric c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("EbiMetric")
					.Append("|").Append((System.Int32)reader[((int)EbiMetricColumn.EbiMetricId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<EbiMetric>(
					key.ToString(), // EntityTrackingKey
					"EbiMetric",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.EbiMetric();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.EbiMetricId = (System.Int32)reader[((int)EbiMetricColumn.EbiMetricId - 1)];
					c.Date = (System.DateTime)reader[((int)EbiMetricColumn.Date - 1)];
					c.SiteId = (System.Int32)reader[((int)EbiMetricColumn.SiteId - 1)];
					c.Metric1 = (System.Int32)reader[((int)EbiMetricColumn.Metric1 - 1)];
					c.Metric2 = (System.Int32)reader[((int)EbiMetricColumn.Metric2 - 1)];
					c.Metric3 = (System.Int32)reader[((int)EbiMetricColumn.Metric3 - 1)];
					c.Metric4 = (System.Int32)reader[((int)EbiMetricColumn.Metric4 - 1)];
					c.Metric5 = (System.Int32)reader[((int)EbiMetricColumn.Metric5 - 1)];
					c.Metric6 = (System.Int32)reader[((int)EbiMetricColumn.Metric6 - 1)];
					c.Metric7 = (System.Int32)reader[((int)EbiMetricColumn.Metric7 - 1)];
					c.Metric8 = (System.Int32)reader[((int)EbiMetricColumn.Metric8 - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.EbiMetric"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.EbiMetric"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.EbiMetric entity)
		{
			if (!reader.Read()) return;
			
			entity.EbiMetricId = (System.Int32)reader[((int)EbiMetricColumn.EbiMetricId - 1)];
			entity.Date = (System.DateTime)reader[((int)EbiMetricColumn.Date - 1)];
			entity.SiteId = (System.Int32)reader[((int)EbiMetricColumn.SiteId - 1)];
			entity.Metric1 = (System.Int32)reader[((int)EbiMetricColumn.Metric1 - 1)];
			entity.Metric2 = (System.Int32)reader[((int)EbiMetricColumn.Metric2 - 1)];
			entity.Metric3 = (System.Int32)reader[((int)EbiMetricColumn.Metric3 - 1)];
			entity.Metric4 = (System.Int32)reader[((int)EbiMetricColumn.Metric4 - 1)];
			entity.Metric5 = (System.Int32)reader[((int)EbiMetricColumn.Metric5 - 1)];
			entity.Metric6 = (System.Int32)reader[((int)EbiMetricColumn.Metric6 - 1)];
			entity.Metric7 = (System.Int32)reader[((int)EbiMetricColumn.Metric7 - 1)];
			entity.Metric8 = (System.Int32)reader[((int)EbiMetricColumn.Metric8 - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.EbiMetric"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.EbiMetric"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.EbiMetric entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.EbiMetricId = (System.Int32)dataRow["EbiMetricId"];
			entity.Date = (System.DateTime)dataRow["Date"];
			entity.SiteId = (System.Int32)dataRow["SiteId"];
			entity.Metric1 = (System.Int32)dataRow["Metric1"];
			entity.Metric2 = (System.Int32)dataRow["Metric2"];
			entity.Metric3 = (System.Int32)dataRow["Metric3"];
			entity.Metric4 = (System.Int32)dataRow["Metric4"];
			entity.Metric5 = (System.Int32)dataRow["Metric5"];
			entity.Metric6 = (System.Int32)dataRow["Metric6"];
			entity.Metric7 = (System.Int32)dataRow["Metric7"];
			entity.Metric8 = (System.Int32)dataRow["Metric8"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.EbiMetric"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.EbiMetric Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.EbiMetric entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region SiteIdSource	
			if (CanDeepLoad(entity, "Sites|SiteIdSource", deepLoadType, innerList) 
				&& entity.SiteIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.SiteId;
				Sites tmpEntity = EntityManager.LocateEntity<Sites>(EntityLocator.ConstructKeyFromPkItems(typeof(Sites), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.SiteIdSource = tmpEntity;
				else
					entity.SiteIdSource = DataRepository.SitesProvider.GetBySiteId(transactionManager, entity.SiteId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'SiteIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.SiteIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.SitesProvider.DeepLoad(transactionManager, entity.SiteIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion SiteIdSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.EbiMetric object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.EbiMetric instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.EbiMetric Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.EbiMetric entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region SiteIdSource
			if (CanDeepSave(entity, "Sites|SiteIdSource", deepSaveType, innerList) 
				&& entity.SiteIdSource != null)
			{
				DataRepository.SitesProvider.Save(transactionManager, entity.SiteIdSource);
				entity.SiteId = entity.SiteIdSource.SiteId;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region EbiMetricChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.EbiMetric</c>
	///</summary>
	public enum EbiMetricChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>Sites</c> at SiteIdSource
		///</summary>
		[ChildEntityType(typeof(Sites))]
		Sites,
		}
	
	#endregion EbiMetricChildEntityTypes
	
	#region EbiMetricFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EbiMetricColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EbiMetric"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EbiMetricFilterBuilder : SqlFilterBuilder<EbiMetricColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EbiMetricFilterBuilder class.
		/// </summary>
		public EbiMetricFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the EbiMetricFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public EbiMetricFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the EbiMetricFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public EbiMetricFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion EbiMetricFilterBuilder
	
	#region EbiMetricParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EbiMetricColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EbiMetric"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EbiMetricParameterBuilder : ParameterizedSqlFilterBuilder<EbiMetricColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EbiMetricParameterBuilder class.
		/// </summary>
		public EbiMetricParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the EbiMetricParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public EbiMetricParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the EbiMetricParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public EbiMetricParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion EbiMetricParameterBuilder
	
	#region EbiMetricSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EbiMetricColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EbiMetric"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class EbiMetricSortBuilder : SqlSortBuilder<EbiMetricColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EbiMetricSqlSortBuilder class.
		/// </summary>
		public EbiMetricSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion EbiMetricSortBuilder
	
} // end namespace
