﻿#region Using directives

using System;

#endregion

namespace KaiZen.CSMS.Entities
{	
	///<summary>
	/// An object representation of the 'TemplateType' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class TemplateType : TemplateTypeBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="TemplateType"/> instance.
		///</summary>
		public TemplateType():base(){}	
		
		#endregion
	}
}
