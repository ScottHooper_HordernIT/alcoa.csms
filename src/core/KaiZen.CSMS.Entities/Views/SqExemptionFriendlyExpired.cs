﻿#region Using directives

using System;

#endregion

namespace KaiZen.CSMS.Entities
{	
	///<summary>
	/// An object representation of the 'SqExemption_Friendly_Expired' view. [No description found in the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class SqExemptionFriendlyExpired : SqExemptionFriendlyExpiredBase
	{
		#region Constructors

		///<summary>
		/// Creates a new <see cref="SqExemptionFriendlyExpired"/> instance.
		///</summary>
		public SqExemptionFriendlyExpired():base(){}	
		
		#endregion
	}
}
