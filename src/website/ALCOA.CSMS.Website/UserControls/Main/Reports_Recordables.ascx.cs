using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using DevExpress.XtraCharts;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxEditors;
using DevExpress.XtraPrinting;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Services;
using KaiZen.CSMS.Data;

public partial class UserControls_Reports_Recordables : System.Web.UI.UserControl
{
    Auth auth = new Auth();
    protected void Page_Load(object sender, EventArgs e) {  Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); }

    private void moduleInit(bool postBack)
    {
        if (!postBack)
        {
            if (auth.RoleId == (int)RoleList.Reader || auth.RoleId == (int)RoleList.Administrator)
            {
            }
            else
            {
                grid.GroupBy((GridViewDataColumn)grid.Columns["SiteName"], 1);
                grid.ExpandAll();
            }

            string group = "AU";
            if (Request.QueryString["Group"] != null) { group = Request.QueryString["Group"]; }
            RegionsService rService = new RegionsService();
            Regions rList = rService.GetByRegionInternalName(group);
            cbRegion.Value = rList.RegionId;

            RDataBind(rList.RegionId);
            RDataBind();

            // Export
            String exportFileName = @"ALCOA CSMS - Incident Performance Report"; //Chrome & IE is fine.
            String userAgent = Request.Headers.Get("User-Agent");
            if (
                (userAgent.Contains("Mozilla") && !userAgent.Contains("AppleWebKit") && !userAgent.Contains("MSIE")) || //Mozilla
                (userAgent.Contains("AppleWebKit") && !userAgent.Contains("Chrome"))      //Safari
                )
            {
                exportFileName = exportFileName.Replace(" ", "_");
            }
            Helper.ExportGrid.Settings(ucExportButtons, exportFileName);
        }
        else
        {
            RDataBind();
        }
    }

    protected void RDataBind(int? RegionId)
    {
        int? CompanyId;
        if (auth.RoleId == (int)RoleList.Reader || auth.RoleId == (int)RoleList.Administrator)
        {
            CompanyId = -1;
        }
        else
        {
            CompanyId = auth.CompanyId;
        }

        DataSet ds = DataRepository.KpiProvider.GetReport_Recordables(CompanyId, RegionId);
        Session["dtRecordables"] = ds.Tables[0];
    }
    protected void RDataBind()
    {
        grid.DataSource = (DataTable)Session["dtRecordables"];
        grid.DataBind();
    }

    protected void cbRegion_SelectedIndexChanged(object sender, EventArgs e)
    {
        int RegionId = 1;
        if (cbRegion.Value != null) int.TryParse(cbRegion.Value.ToString(), out RegionId);
        int? _RegionId = RegionId;
        RDataBind(_RegionId);
        RDataBind();
    }
}