﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.CompaniesHrCrpDataProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(CompaniesHrCrpDataDataSourceDesigner))]
	public class CompaniesHrCrpDataDataSource : ProviderDataSource<CompaniesHrCrpData, CompaniesHrCrpDataKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompaniesHrCrpDataDataSource class.
		/// </summary>
		public CompaniesHrCrpDataDataSource() : base(new CompaniesHrCrpDataService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the CompaniesHrCrpDataDataSourceView used by the CompaniesHrCrpDataDataSource.
		/// </summary>
		protected CompaniesHrCrpDataDataSourceView CompaniesHrCrpDataView
		{
			get { return ( View as CompaniesHrCrpDataDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the CompaniesHrCrpDataDataSource control invokes to retrieve data.
		/// </summary>
		public CompaniesHrCrpDataSelectMethod SelectMethod
		{
			get
			{
				CompaniesHrCrpDataSelectMethod selectMethod = CompaniesHrCrpDataSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (CompaniesHrCrpDataSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the CompaniesHrCrpDataDataSourceView class that is to be
		/// used by the CompaniesHrCrpDataDataSource.
		/// </summary>
		/// <returns>An instance of the CompaniesHrCrpDataDataSourceView class.</returns>
		protected override BaseDataSourceView<CompaniesHrCrpData, CompaniesHrCrpDataKey> GetNewDataSourceView()
		{
			return new CompaniesHrCrpDataDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the CompaniesHrCrpDataDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class CompaniesHrCrpDataDataSourceView : ProviderDataSourceView<CompaniesHrCrpData, CompaniesHrCrpDataKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompaniesHrCrpDataDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the CompaniesHrCrpDataDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public CompaniesHrCrpDataDataSourceView(CompaniesHrCrpDataDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal CompaniesHrCrpDataDataSource CompaniesHrCrpDataOwner
		{
			get { return Owner as CompaniesHrCrpDataDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal CompaniesHrCrpDataSelectMethod SelectMethod
		{
			get { return CompaniesHrCrpDataOwner.SelectMethod; }
			set { CompaniesHrCrpDataOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal CompaniesHrCrpDataService CompaniesHrCrpDataProvider
		{
			get { return Provider as CompaniesHrCrpDataService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<CompaniesHrCrpData> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<CompaniesHrCrpData> results = null;
			CompaniesHrCrpData item;
			count = 0;
			
			System.Int32 _companiesHrCrpDataId;
			System.Int32 _companyId;
			System.Int32 _siteId;

			switch ( SelectMethod )
			{
				case CompaniesHrCrpDataSelectMethod.Get:
					CompaniesHrCrpDataKey entityKey  = new CompaniesHrCrpDataKey();
					entityKey.Load(values);
					item = CompaniesHrCrpDataProvider.Get(entityKey);
					results = new TList<CompaniesHrCrpData>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case CompaniesHrCrpDataSelectMethod.GetAll:
                    results = CompaniesHrCrpDataProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case CompaniesHrCrpDataSelectMethod.GetPaged:
					results = CompaniesHrCrpDataProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case CompaniesHrCrpDataSelectMethod.Find:
					if ( FilterParameters != null )
						results = CompaniesHrCrpDataProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = CompaniesHrCrpDataProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case CompaniesHrCrpDataSelectMethod.GetByCompaniesHrCrpDataId:
					_companiesHrCrpDataId = ( values["CompaniesHrCrpDataId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["CompaniesHrCrpDataId"], typeof(System.Int32)) : (int)0;
					item = CompaniesHrCrpDataProvider.GetByCompaniesHrCrpDataId(_companiesHrCrpDataId);
					results = new TList<CompaniesHrCrpData>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				// FK
				case CompaniesHrCrpDataSelectMethod.GetByCompanyId:
					_companyId = ( values["CompanyId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["CompanyId"], typeof(System.Int32)) : (int)0;
					results = CompaniesHrCrpDataProvider.GetByCompanyId(_companyId, this.StartIndex, this.PageSize, out count);
					break;
				case CompaniesHrCrpDataSelectMethod.GetBySiteId:
					_siteId = ( values["SiteId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["SiteId"], typeof(System.Int32)) : (int)0;
					results = CompaniesHrCrpDataProvider.GetBySiteId(_siteId, this.StartIndex, this.PageSize, out count);
					break;
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == CompaniesHrCrpDataSelectMethod.Get || SelectMethod == CompaniesHrCrpDataSelectMethod.GetByCompaniesHrCrpDataId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				CompaniesHrCrpData entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					CompaniesHrCrpDataProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<CompaniesHrCrpData> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			CompaniesHrCrpDataProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region CompaniesHrCrpDataDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the CompaniesHrCrpDataDataSource class.
	/// </summary>
	public class CompaniesHrCrpDataDataSourceDesigner : ProviderDataSourceDesigner<CompaniesHrCrpData, CompaniesHrCrpDataKey>
	{
		/// <summary>
		/// Initializes a new instance of the CompaniesHrCrpDataDataSourceDesigner class.
		/// </summary>
		public CompaniesHrCrpDataDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public CompaniesHrCrpDataSelectMethod SelectMethod
		{
			get { return ((CompaniesHrCrpDataDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new CompaniesHrCrpDataDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region CompaniesHrCrpDataDataSourceActionList

	/// <summary>
	/// Supports the CompaniesHrCrpDataDataSourceDesigner class.
	/// </summary>
	internal class CompaniesHrCrpDataDataSourceActionList : DesignerActionList
	{
		private CompaniesHrCrpDataDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the CompaniesHrCrpDataDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public CompaniesHrCrpDataDataSourceActionList(CompaniesHrCrpDataDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public CompaniesHrCrpDataSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion CompaniesHrCrpDataDataSourceActionList
	
	#endregion CompaniesHrCrpDataDataSourceDesigner
	
	#region CompaniesHrCrpDataSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the CompaniesHrCrpDataDataSource.SelectMethod property.
	/// </summary>
	public enum CompaniesHrCrpDataSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByCompaniesHrCrpDataId method.
		/// </summary>
		GetByCompaniesHrCrpDataId,
		/// <summary>
		/// Represents the GetByCompanyId method.
		/// </summary>
		GetByCompanyId,
		/// <summary>
		/// Represents the GetBySiteId method.
		/// </summary>
		GetBySiteId
	}
	
	#endregion CompaniesHrCrpDataSelectMethod

	#region CompaniesHrCrpDataFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CompaniesHrCrpData"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompaniesHrCrpDataFilter : SqlFilter<CompaniesHrCrpDataColumn>
	{
	}
	
	#endregion CompaniesHrCrpDataFilter

	#region CompaniesHrCrpDataExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CompaniesHrCrpData"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompaniesHrCrpDataExpressionBuilder : SqlExpressionBuilder<CompaniesHrCrpDataColumn>
	{
	}
	
	#endregion CompaniesHrCrpDataExpressionBuilder	

	#region CompaniesHrCrpDataProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;CompaniesHrCrpDataChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="CompaniesHrCrpData"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompaniesHrCrpDataProperty : ChildEntityProperty<CompaniesHrCrpDataChildEntityTypes>
	{
	}
	
	#endregion CompaniesHrCrpDataProperty
}

