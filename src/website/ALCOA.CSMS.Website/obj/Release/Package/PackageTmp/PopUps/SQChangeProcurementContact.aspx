﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/popup.master" AutoEventWireup="true" Inherits="PopUps_SQChangeProcurementContact" Codebehind="SQChangeProcurementContact.aspx.cs" %>

<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxRoundPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .style1
        {
            width: 204px;
            color: #FF0000;
        }
        .style2
        {
            width: 204px;
            font-weight: bold;
            color: #006600;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
        <dx:ASPxRoundPanel ID="ASPxRoundPanel1" runat="server" BackColor="#DDECFE" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
            CssPostfix="Office2003Blue" EnableDefaultAppearance="False" HeaderText="Change Procurement Contact"
            SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" 
            Width="420px">
            <TopEdge>
                <BackgroundImage HorizontalPosition="left" ImageUrl="~/App_Themes/Office2003Blue/Web/rpTopEdge.png"
                    Repeat="RepeatX" VerticalPosition="top" />
            </TopEdge>
            <ContentPaddings Padding="2px" PaddingBottom="10px" PaddingTop="10px" />
            <HeaderRightEdge>
                <BackgroundImage HorizontalPosition="left" ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png"
                    Repeat="RepeatX" VerticalPosition="top" />
            </HeaderRightEdge>
            <Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px" />
            <HeaderLeftEdge>
                <BackgroundImage HorizontalPosition="left" ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png"
                    Repeat="RepeatX" VerticalPosition="top" />
            </HeaderLeftEdge>
            <HeaderStyle BackColor="#7BA4E0">
                <Paddings Padding="0px" PaddingBottom="7px" PaddingLeft="2px" PaddingRight="2px" />
                <BorderBottom BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px" />
            </HeaderStyle>
            <HeaderContent>
                <BackgroundImage HorizontalPosition="left" ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png"
                    Repeat="RepeatX" VerticalPosition="top" />
            </HeaderContent>
            <DisabledStyle ForeColor="Gray">
            </DisabledStyle>
            <NoHeaderTopEdge BackColor="#DDECFE">
            </NoHeaderTopEdge>
            <PanelCollection>
                <dx:PanelContent runat="server">
                    <table width="420px">
                        <tr>
                            <td style="text-align: right; width: 65px">
                                <b>Company:</b>
                            </td>
                            <td>
                                <dx:ASPxLabel ID="lblCompany" runat="server" Text="Name Of Company Goes Here">
                                </dx:ASPxLabel>
                            </td>
                        </tr>
                    </table>
                    <table width="420px">
                        <tr>
                            <td class="style1" style="text-align: right;">
                                <b>Current Procurement Contact: </b>
                            </td>
                            <td>
                                <dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="Smith, John">
                                </dx:ASPxLabel>
                            </td>
                        </tr>
                        <tr>
                            <td class="style2" style="text-align: right;">
                                New Procurement Contact:
                            </td>
                            <td>
                                <dx:ASPxComboBox ID="cbProcurementContact" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                    CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                    ValueType="System.String" Width="200px">
                                    <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                                    </LoadingPanelImage>
                                    <ButtonStyle Width="13px">
                                    </ButtonStyle>
                                </dx:ASPxComboBox>
                            </td>
                        </tr>
                    </table>
                    <br />
                    <b>Additional Comments (used in Optional Contact E-Mail sent to New Procurement 
                    Contact)</b><dx:ASPxMemo
                    ID="ASPxMemo1" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                    CssPostfix="Office2003Blue" Height="53px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                    Width="100%"> </dx:ASPxMemo>
                    <dx:ASPxCheckBox ID="ASPxCheckBox1" runat="server" Checked="True" Text="Send E-Mail to New Procurement Contact (Optional)">
                    </dx:ASPxCheckBox>
                    <br />
                    <div align="center">
                        <dx:ASPxButton ID="btnChange" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                            CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                            Text="Change Procurement Contact" Width="236px">
                        </dx:ASPxButton>
                    </div>
                </dx:PanelContent>
            </PanelCollection>
        </dx:ASPxRoundPanel>
</asp:Content>
