﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="QuestionnaireContractorRsProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class QuestionnaireContractorRsProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.QuestionnaireContractorRs, KaiZen.CSMS.Entities.QuestionnaireContractorRsKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.QuestionnaireContractorRsKey key)
		{
			return Delete(transactionManager, key.QuestionnaireContractorRsId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_questionnaireContractorRsId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _questionnaireContractorRsId)
		{
			return Delete(null, _questionnaireContractorRsId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireContractorRsId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _questionnaireContractorRsId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireContractorRs_Companies key.
		///		FK_QuestionnaireContractorRs_Companies Description: 
		/// </summary>
		/// <param name="_contractorCompanyId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireContractorRs objects.</returns>
		public TList<QuestionnaireContractorRs> GetByContractorCompanyId(System.Int32 _contractorCompanyId)
		{
			int count = -1;
			return GetByContractorCompanyId(_contractorCompanyId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireContractorRs_Companies key.
		///		FK_QuestionnaireContractorRs_Companies Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_contractorCompanyId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireContractorRs objects.</returns>
		/// <remarks></remarks>
		public TList<QuestionnaireContractorRs> GetByContractorCompanyId(TransactionManager transactionManager, System.Int32 _contractorCompanyId)
		{
			int count = -1;
			return GetByContractorCompanyId(transactionManager, _contractorCompanyId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireContractorRs_Companies key.
		///		FK_QuestionnaireContractorRs_Companies Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_contractorCompanyId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireContractorRs objects.</returns>
		public TList<QuestionnaireContractorRs> GetByContractorCompanyId(TransactionManager transactionManager, System.Int32 _contractorCompanyId, int start, int pageLength)
		{
			int count = -1;
			return GetByContractorCompanyId(transactionManager, _contractorCompanyId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireContractorRs_Companies key.
		///		fkQuestionnaireContractorRsCompanies Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_contractorCompanyId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireContractorRs objects.</returns>
		public TList<QuestionnaireContractorRs> GetByContractorCompanyId(System.Int32 _contractorCompanyId, int start, int pageLength)
		{
			int count =  -1;
			return GetByContractorCompanyId(null, _contractorCompanyId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireContractorRs_Companies key.
		///		fkQuestionnaireContractorRsCompanies Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_contractorCompanyId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireContractorRs objects.</returns>
		public TList<QuestionnaireContractorRs> GetByContractorCompanyId(System.Int32 _contractorCompanyId, int start, int pageLength,out int count)
		{
			return GetByContractorCompanyId(null, _contractorCompanyId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireContractorRs_Companies key.
		///		FK_QuestionnaireContractorRs_Companies Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_contractorCompanyId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireContractorRs objects.</returns>
		public abstract TList<QuestionnaireContractorRs> GetByContractorCompanyId(TransactionManager transactionManager, System.Int32 _contractorCompanyId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireContractorRs_Companies1 key.
		///		FK_QuestionnaireContractorRs_Companies1 Description: 
		/// </summary>
		/// <param name="_subContractorCompanyId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireContractorRs objects.</returns>
		public TList<QuestionnaireContractorRs> GetBySubContractorCompanyId(System.Int32 _subContractorCompanyId)
		{
			int count = -1;
			return GetBySubContractorCompanyId(_subContractorCompanyId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireContractorRs_Companies1 key.
		///		FK_QuestionnaireContractorRs_Companies1 Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_subContractorCompanyId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireContractorRs objects.</returns>
		/// <remarks></remarks>
		public TList<QuestionnaireContractorRs> GetBySubContractorCompanyId(TransactionManager transactionManager, System.Int32 _subContractorCompanyId)
		{
			int count = -1;
			return GetBySubContractorCompanyId(transactionManager, _subContractorCompanyId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireContractorRs_Companies1 key.
		///		FK_QuestionnaireContractorRs_Companies1 Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_subContractorCompanyId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireContractorRs objects.</returns>
		public TList<QuestionnaireContractorRs> GetBySubContractorCompanyId(TransactionManager transactionManager, System.Int32 _subContractorCompanyId, int start, int pageLength)
		{
			int count = -1;
			return GetBySubContractorCompanyId(transactionManager, _subContractorCompanyId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireContractorRs_Companies1 key.
		///		fkQuestionnaireContractorRsCompanies1 Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_subContractorCompanyId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireContractorRs objects.</returns>
		public TList<QuestionnaireContractorRs> GetBySubContractorCompanyId(System.Int32 _subContractorCompanyId, int start, int pageLength)
		{
			int count =  -1;
			return GetBySubContractorCompanyId(null, _subContractorCompanyId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireContractorRs_Companies1 key.
		///		fkQuestionnaireContractorRsCompanies1 Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_subContractorCompanyId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireContractorRs objects.</returns>
		public TList<QuestionnaireContractorRs> GetBySubContractorCompanyId(System.Int32 _subContractorCompanyId, int start, int pageLength,out int count)
		{
			return GetBySubContractorCompanyId(null, _subContractorCompanyId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireContractorRs_Companies1 key.
		///		FK_QuestionnaireContractorRs_Companies1 Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_subContractorCompanyId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireContractorRs objects.</returns>
		public abstract TList<QuestionnaireContractorRs> GetBySubContractorCompanyId(TransactionManager transactionManager, System.Int32 _subContractorCompanyId, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.QuestionnaireContractorRs Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.QuestionnaireContractorRsKey key, int start, int pageLength)
		{
			return GetByQuestionnaireContractorRsId(transactionManager, key.QuestionnaireContractorRsId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_QuestionnaireContractorRs index.
		/// </summary>
		/// <param name="_questionnaireContractorRsId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireContractorRs"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireContractorRs GetByQuestionnaireContractorRsId(System.Int32 _questionnaireContractorRsId)
		{
			int count = -1;
			return GetByQuestionnaireContractorRsId(null,_questionnaireContractorRsId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireContractorRs index.
		/// </summary>
		/// <param name="_questionnaireContractorRsId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireContractorRs"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireContractorRs GetByQuestionnaireContractorRsId(System.Int32 _questionnaireContractorRsId, int start, int pageLength)
		{
			int count = -1;
			return GetByQuestionnaireContractorRsId(null, _questionnaireContractorRsId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireContractorRs index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireContractorRsId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireContractorRs"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireContractorRs GetByQuestionnaireContractorRsId(TransactionManager transactionManager, System.Int32 _questionnaireContractorRsId)
		{
			int count = -1;
			return GetByQuestionnaireContractorRsId(transactionManager, _questionnaireContractorRsId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireContractorRs index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireContractorRsId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireContractorRs"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireContractorRs GetByQuestionnaireContractorRsId(TransactionManager transactionManager, System.Int32 _questionnaireContractorRsId, int start, int pageLength)
		{
			int count = -1;
			return GetByQuestionnaireContractorRsId(transactionManager, _questionnaireContractorRsId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireContractorRs index.
		/// </summary>
		/// <param name="_questionnaireContractorRsId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireContractorRs"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireContractorRs GetByQuestionnaireContractorRsId(System.Int32 _questionnaireContractorRsId, int start, int pageLength, out int count)
		{
			return GetByQuestionnaireContractorRsId(null, _questionnaireContractorRsId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireContractorRs index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireContractorRsId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireContractorRs"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.QuestionnaireContractorRs GetByQuestionnaireContractorRsId(TransactionManager transactionManager, System.Int32 _questionnaireContractorRsId, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_QuestionnaireContractorRs index.
		/// </summary>
		/// <param name="_contractorCompanyId"></param>
		/// <param name="_subContractorCompanyId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireContractorRs"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireContractorRs GetByContractorCompanyIdSubContractorCompanyId(System.Int32 _contractorCompanyId, System.Int32 _subContractorCompanyId)
		{
			int count = -1;
			return GetByContractorCompanyIdSubContractorCompanyId(null,_contractorCompanyId, _subContractorCompanyId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireContractorRs index.
		/// </summary>
		/// <param name="_contractorCompanyId"></param>
		/// <param name="_subContractorCompanyId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireContractorRs"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireContractorRs GetByContractorCompanyIdSubContractorCompanyId(System.Int32 _contractorCompanyId, System.Int32 _subContractorCompanyId, int start, int pageLength)
		{
			int count = -1;
			return GetByContractorCompanyIdSubContractorCompanyId(null, _contractorCompanyId, _subContractorCompanyId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireContractorRs index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_contractorCompanyId"></param>
		/// <param name="_subContractorCompanyId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireContractorRs"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireContractorRs GetByContractorCompanyIdSubContractorCompanyId(TransactionManager transactionManager, System.Int32 _contractorCompanyId, System.Int32 _subContractorCompanyId)
		{
			int count = -1;
			return GetByContractorCompanyIdSubContractorCompanyId(transactionManager, _contractorCompanyId, _subContractorCompanyId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireContractorRs index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_contractorCompanyId"></param>
		/// <param name="_subContractorCompanyId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireContractorRs"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireContractorRs GetByContractorCompanyIdSubContractorCompanyId(TransactionManager transactionManager, System.Int32 _contractorCompanyId, System.Int32 _subContractorCompanyId, int start, int pageLength)
		{
			int count = -1;
			return GetByContractorCompanyIdSubContractorCompanyId(transactionManager, _contractorCompanyId, _subContractorCompanyId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireContractorRs index.
		/// </summary>
		/// <param name="_contractorCompanyId"></param>
		/// <param name="_subContractorCompanyId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireContractorRs"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireContractorRs GetByContractorCompanyIdSubContractorCompanyId(System.Int32 _contractorCompanyId, System.Int32 _subContractorCompanyId, int start, int pageLength, out int count)
		{
			return GetByContractorCompanyIdSubContractorCompanyId(null, _contractorCompanyId, _subContractorCompanyId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireContractorRs index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_contractorCompanyId"></param>
		/// <param name="_subContractorCompanyId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireContractorRs"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.QuestionnaireContractorRs GetByContractorCompanyIdSubContractorCompanyId(TransactionManager transactionManager, System.Int32 _contractorCompanyId, System.Int32 _subContractorCompanyId, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;QuestionnaireContractorRs&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;QuestionnaireContractorRs&gt;"/></returns>
		public static TList<QuestionnaireContractorRs> Fill(IDataReader reader, TList<QuestionnaireContractorRs> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.QuestionnaireContractorRs c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("QuestionnaireContractorRs")
					.Append("|").Append((System.Int32)reader[((int)QuestionnaireContractorRsColumn.QuestionnaireContractorRsId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<QuestionnaireContractorRs>(
					key.ToString(), // EntityTrackingKey
					"QuestionnaireContractorRs",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.QuestionnaireContractorRs();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.QuestionnaireContractorRsId = (System.Int32)reader[((int)QuestionnaireContractorRsColumn.QuestionnaireContractorRsId - 1)];
					c.ContractorCompanyId = (System.Int32)reader[((int)QuestionnaireContractorRsColumn.ContractorCompanyId - 1)];
					c.SubContractorCompanyId = (System.Int32)reader[((int)QuestionnaireContractorRsColumn.SubContractorCompanyId - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.QuestionnaireContractorRs"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.QuestionnaireContractorRs"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.QuestionnaireContractorRs entity)
		{
			if (!reader.Read()) return;
			
			entity.QuestionnaireContractorRsId = (System.Int32)reader[((int)QuestionnaireContractorRsColumn.QuestionnaireContractorRsId - 1)];
			entity.ContractorCompanyId = (System.Int32)reader[((int)QuestionnaireContractorRsColumn.ContractorCompanyId - 1)];
			entity.SubContractorCompanyId = (System.Int32)reader[((int)QuestionnaireContractorRsColumn.SubContractorCompanyId - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.QuestionnaireContractorRs"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.QuestionnaireContractorRs"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.QuestionnaireContractorRs entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.QuestionnaireContractorRsId = (System.Int32)dataRow["QuestionnaireContractorRsId"];
			entity.ContractorCompanyId = (System.Int32)dataRow["ContractorCompanyId"];
			entity.SubContractorCompanyId = (System.Int32)dataRow["SubContractorCompanyId"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.QuestionnaireContractorRs"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.QuestionnaireContractorRs Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.QuestionnaireContractorRs entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region ContractorCompanyIdSource	
			if (CanDeepLoad(entity, "Companies|ContractorCompanyIdSource", deepLoadType, innerList) 
				&& entity.ContractorCompanyIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.ContractorCompanyId;
				Companies tmpEntity = EntityManager.LocateEntity<Companies>(EntityLocator.ConstructKeyFromPkItems(typeof(Companies), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.ContractorCompanyIdSource = tmpEntity;
				else
					entity.ContractorCompanyIdSource = DataRepository.CompaniesProvider.GetByCompanyId(transactionManager, entity.ContractorCompanyId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ContractorCompanyIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.ContractorCompanyIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.CompaniesProvider.DeepLoad(transactionManager, entity.ContractorCompanyIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion ContractorCompanyIdSource

			#region SubContractorCompanyIdSource	
			if (CanDeepLoad(entity, "Companies|SubContractorCompanyIdSource", deepLoadType, innerList) 
				&& entity.SubContractorCompanyIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.SubContractorCompanyId;
				Companies tmpEntity = EntityManager.LocateEntity<Companies>(EntityLocator.ConstructKeyFromPkItems(typeof(Companies), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.SubContractorCompanyIdSource = tmpEntity;
				else
					entity.SubContractorCompanyIdSource = DataRepository.CompaniesProvider.GetByCompanyId(transactionManager, entity.SubContractorCompanyId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'SubContractorCompanyIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.SubContractorCompanyIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.CompaniesProvider.DeepLoad(transactionManager, entity.SubContractorCompanyIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion SubContractorCompanyIdSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.QuestionnaireContractorRs object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.QuestionnaireContractorRs instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.QuestionnaireContractorRs Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.QuestionnaireContractorRs entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region ContractorCompanyIdSource
			if (CanDeepSave(entity, "Companies|ContractorCompanyIdSource", deepSaveType, innerList) 
				&& entity.ContractorCompanyIdSource != null)
			{
				DataRepository.CompaniesProvider.Save(transactionManager, entity.ContractorCompanyIdSource);
				entity.ContractorCompanyId = entity.ContractorCompanyIdSource.CompanyId;
			}
			#endregion 
			
			#region SubContractorCompanyIdSource
			if (CanDeepSave(entity, "Companies|SubContractorCompanyIdSource", deepSaveType, innerList) 
				&& entity.SubContractorCompanyIdSource != null)
			{
				DataRepository.CompaniesProvider.Save(transactionManager, entity.SubContractorCompanyIdSource);
				entity.SubContractorCompanyId = entity.SubContractorCompanyIdSource.CompanyId;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region QuestionnaireContractorRsChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.QuestionnaireContractorRs</c>
	///</summary>
	public enum QuestionnaireContractorRsChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>Companies</c> at ContractorCompanyIdSource
		///</summary>
		[ChildEntityType(typeof(Companies))]
		Companies,
		}
	
	#endregion QuestionnaireContractorRsChildEntityTypes
	
	#region QuestionnaireContractorRsFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;QuestionnaireContractorRsColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireContractorRs"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireContractorRsFilterBuilder : SqlFilterBuilder<QuestionnaireContractorRsColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireContractorRsFilterBuilder class.
		/// </summary>
		public QuestionnaireContractorRsFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireContractorRsFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireContractorRsFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireContractorRsFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireContractorRsFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireContractorRsFilterBuilder
	
	#region QuestionnaireContractorRsParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;QuestionnaireContractorRsColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireContractorRs"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireContractorRsParameterBuilder : ParameterizedSqlFilterBuilder<QuestionnaireContractorRsColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireContractorRsParameterBuilder class.
		/// </summary>
		public QuestionnaireContractorRsParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireContractorRsParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireContractorRsParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireContractorRsParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireContractorRsParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireContractorRsParameterBuilder
	
	#region QuestionnaireContractorRsSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;QuestionnaireContractorRsColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireContractorRs"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class QuestionnaireContractorRsSortBuilder : SqlSortBuilder<QuestionnaireContractorRsColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireContractorRsSqlSortBuilder class.
		/// </summary>
		public QuestionnaireContractorRsSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion QuestionnaireContractorRsSortBuilder
	
} // end namespace
