﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.CompaniesHrCrpDataListAllProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[CLSCompliant(true)]
	[Designer(typeof(CompaniesHrCrpDataListAllDataSourceDesigner))]
	public class CompaniesHrCrpDataListAllDataSource : ReadOnlyDataSource<CompaniesHrCrpDataListAll>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompaniesHrCrpDataListAllDataSource class.
		/// </summary>
		public CompaniesHrCrpDataListAllDataSource() : base(new CompaniesHrCrpDataListAllService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the CompaniesHrCrpDataListAllDataSourceView used by the CompaniesHrCrpDataListAllDataSource.
		/// </summary>
		protected CompaniesHrCrpDataListAllDataSourceView CompaniesHrCrpDataListAllView
		{
			get { return ( View as CompaniesHrCrpDataListAllDataSourceView ); }
		}
		
		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the CompaniesHrCrpDataListAllDataSourceView class that is to be
		/// used by the CompaniesHrCrpDataListAllDataSource.
		/// </summary>
		/// <returns>An instance of the CompaniesHrCrpDataListAllDataSourceView class.</returns>
		protected override BaseDataSourceView<CompaniesHrCrpDataListAll, Object> GetNewDataSourceView()
		{
			return new CompaniesHrCrpDataListAllDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the CompaniesHrCrpDataListAllDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class CompaniesHrCrpDataListAllDataSourceView : ReadOnlyDataSourceView<CompaniesHrCrpDataListAll>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompaniesHrCrpDataListAllDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the CompaniesHrCrpDataListAllDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public CompaniesHrCrpDataListAllDataSourceView(CompaniesHrCrpDataListAllDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal CompaniesHrCrpDataListAllDataSource CompaniesHrCrpDataListAllOwner
		{
			get { return Owner as CompaniesHrCrpDataListAllDataSource; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal CompaniesHrCrpDataListAllService CompaniesHrCrpDataListAllProvider
		{
			get { return Provider as CompaniesHrCrpDataListAllService; }
		}

		#endregion Properties
		
		#region Methods
		
		#endregion Methods
	}

	#region CompaniesHrCrpDataListAllDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the CompaniesHrCrpDataListAllDataSource class.
	/// </summary>
	public class CompaniesHrCrpDataListAllDataSourceDesigner : ReadOnlyDataSourceDesigner<CompaniesHrCrpDataListAll>
	{
	}

	#endregion CompaniesHrCrpDataListAllDataSourceDesigner

	#region CompaniesHrCrpDataListAllFilter

	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CompaniesHrCrpDataListAll"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompaniesHrCrpDataListAllFilter : SqlFilter<CompaniesHrCrpDataListAllColumn>
	{
	}

	#endregion CompaniesHrCrpDataListAllFilter

	#region CompaniesHrCrpDataListAllExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CompaniesHrCrpDataListAll"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompaniesHrCrpDataListAllExpressionBuilder : SqlExpressionBuilder<CompaniesHrCrpDataListAllColumn>
	{
	}
	
	#endregion CompaniesHrCrpDataListAllExpressionBuilder		
}

