﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface IAuditFileVaultTableService
    {
        AuditFileVaultTable Get(Expression<Func<AuditFileVaultTable, bool>> where, List<Expression<Func<AuditFileVaultTable, object>>> includes);
        List<AuditFileVaultTable> GetMany(int? top, Expression<Func<AuditFileVaultTable, bool>> where, Func<IQueryable<AuditFileVaultTable>, IOrderedQueryable<AuditFileVaultTable>> orderBy, List<Expression<Func<AuditFileVaultTable, object>>> includes);
        List<AuditFileVaultTable> GetManyPaged(int pageNumber, int pageSize, Expression<Func<AuditFileVaultTable, bool>> where, Func<IQueryable<AuditFileVaultTable>, IOrderedQueryable<AuditFileVaultTable>> orderBy, List<Expression<Func<AuditFileVaultTable, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<AuditFileVaultTable, bool>> where, List<Expression<Func<AuditFileVaultTable, object>>> includes);
        void Insert(AuditFileVaultTable item, bool saveChanges = true);
        void Update(AuditFileVaultTable item, bool saveChanges = true);
        void Delete(AuditFileVaultTable item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class AuditFileVaultTableService : IAuditFileVaultTableService
    {
        private readonly IRepository<AuditFileVaultTable> _repository;

        public AuditFileVaultTableService(IRepository<AuditFileVaultTable> repository)
        {
            this._repository = repository;
        }

        public AuditFileVaultTable Get(Expression<Func<AuditFileVaultTable, bool>> where, List<Expression<Func<AuditFileVaultTable, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<AuditFileVaultTable> GetMany(int? top, Expression<Func<AuditFileVaultTable, bool>> where, Func<IQueryable<AuditFileVaultTable>, IOrderedQueryable<AuditFileVaultTable>> orderBy, List<Expression<Func<AuditFileVaultTable, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<AuditFileVaultTable> GetManyPaged(int pageNumber, int pageSize, Expression<Func<AuditFileVaultTable, bool>> where, Func<IQueryable<AuditFileVaultTable>, IOrderedQueryable<AuditFileVaultTable>> orderBy, List<Expression<Func<AuditFileVaultTable, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<AuditFileVaultTable, bool>> where, List<Expression<Func<AuditFileVaultTable, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(AuditFileVaultTable item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("AuditFileVaultTable");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(AuditFileVaultTable item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("AuditFileVaultTable");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(AuditFileVaultTable item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("AuditFileVaultTable");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
