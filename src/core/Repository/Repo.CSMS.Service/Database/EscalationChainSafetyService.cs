﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface IEscalationChainSafetyService
    {
        EscalationChainSafety Get(Expression<Func<EscalationChainSafety, bool>> where, List<Expression<Func<EscalationChainSafety, object>>> includes);
        List<EscalationChainSafety> GetMany(int? top, Expression<Func<EscalationChainSafety, bool>> where, Func<IQueryable<EscalationChainSafety>, IOrderedQueryable<EscalationChainSafety>> orderBy, List<Expression<Func<EscalationChainSafety, object>>> includes);
        List<EscalationChainSafety> GetManyPaged(int pageNumber, int pageSize, Expression<Func<EscalationChainSafety, bool>> where, Func<IQueryable<EscalationChainSafety>, IOrderedQueryable<EscalationChainSafety>> orderBy, List<Expression<Func<EscalationChainSafety, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<EscalationChainSafety, bool>> where, List<Expression<Func<EscalationChainSafety, object>>> includes);
        void Insert(EscalationChainSafety item, bool saveChanges = true);
        void Update(EscalationChainSafety item, bool saveChanges = true);
        void Delete(EscalationChainSafety item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class EscalationChainSafetyService : IEscalationChainSafetyService
    {
        private readonly IRepository<EscalationChainSafety> _repository;

        public EscalationChainSafetyService(IRepository<EscalationChainSafety> repository)
        {
            this._repository = repository;
        }

        public EscalationChainSafety Get(Expression<Func<EscalationChainSafety, bool>> where, List<Expression<Func<EscalationChainSafety, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<EscalationChainSafety> GetMany(int? top, Expression<Func<EscalationChainSafety, bool>> where, Func<IQueryable<EscalationChainSafety>, IOrderedQueryable<EscalationChainSafety>> orderBy, List<Expression<Func<EscalationChainSafety, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<EscalationChainSafety> GetManyPaged(int pageNumber, int pageSize, Expression<Func<EscalationChainSafety, bool>> where, Func<IQueryable<EscalationChainSafety>, IOrderedQueryable<EscalationChainSafety>> orderBy, List<Expression<Func<EscalationChainSafety, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<EscalationChainSafety, bool>> where, List<Expression<Func<EscalationChainSafety, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(EscalationChainSafety item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("EscalationChainSafety");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(EscalationChainSafety item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("EscalationChainSafety");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(EscalationChainSafety item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("EscalationChainSafety");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
