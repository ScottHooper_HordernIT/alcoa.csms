USE [AuA_CSMS_Dev]
GO

/****** Object:  StoredProcedure [dbo].[DashBoard_CSA_Insert]    Script Date: 3/08/2015 1:22:22 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[DashBoard_CSA_Insert]        
(        
 @Site int        
 ,@Month int        
 ,@Year int        
         
 ,@CASEmbeddedRequired decimal(18,2)        
 ,@CSAEmbeddedRequiredtodate decimal(18,2)        
 ,@EmbeddedActualcompleted decimal(18,2)        
 ,@CSANonEmbedded1TotalRequired decimal(18,2)        
 ,@CSANonEmbedded1Requiredtodate decimal(18,2)        
 ,@NonEmbedded1Actualcompleted decimal(18,2)        
        
)        
AS        
if exists(Select * from  DashBoard_CSA where SiteId=@Site and [Month]=@Month  
 and [Year]=@Year)  
 begin   
 Update DashBoard_CSA Set  
   [CAS Embedded �Required] =@CASEmbeddedRequired       
     ,[CSA Embedded � Required to date]  =@CSAEmbeddedRequiredtodate      
     ,[Embedded � Actual completed]  = @EmbeddedActualcompleted      
     ,[CSA Non-Embedded 1 � Total Required]=@CSANonEmbedded1TotalRequired        
     ,[CSA � Non-Embedded 1 � Required to date] =@CSANonEmbedded1Requiredtodate       
     ,[Non-Embedded 1 � Actual completed]=@NonEmbedded1Actualcompleted 
     ,[LastModifiedDate] = GETDATE() 
    where SiteId=@Site AND [Month]=@Month AND [Year]=@Year  
   end    
     else   
         begin   
    INSERT INTO [dbo].[DashBoard_CSA]        
     (        
     [SiteId]        
     ,[Month]        
     ,[Year]        
     ,[CAS Embedded �Required]        
     ,[CSA Embedded � Required to date]        
     ,[Embedded � Actual completed]        
     ,[CSA Non-Embedded 1 � Total Required]        
     ,[CSA � Non-Embedded 1 � Required to date]        
     ,[Non-Embedded 1 � Actual completed]
     ,[LastModifiedDate]        
   )        
    VALUES        
     (        
     @Site        
     ,@Month        
     ,@Year        
     ,@CASEmbeddedRequired        
     ,@CSAEmbeddedRequiredtodate        
     ,@EmbeddedActualcompleted        
     ,@CSANonEmbedded1TotalRequired        
     ,@CSANonEmbedded1Requiredtodate        
     ,@NonEmbedded1Actualcompleted
     ,GETDATE()        
     )      
end    
----------------------------------------

GO


