﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface IFileVaultAuditService
    {
        FileVaultAudit Get(Expression<Func<FileVaultAudit, bool>> where, List<Expression<Func<FileVaultAudit, object>>> includes);
        List<FileVaultAudit> GetMany(int? top, Expression<Func<FileVaultAudit, bool>> where, Func<IQueryable<FileVaultAudit>, IOrderedQueryable<FileVaultAudit>> orderBy, List<Expression<Func<FileVaultAudit, object>>> includes);
        List<FileVaultAudit> GetManyPaged(int pageNumber, int pageSize, Expression<Func<FileVaultAudit, bool>> where, Func<IQueryable<FileVaultAudit>, IOrderedQueryable<FileVaultAudit>> orderBy, List<Expression<Func<FileVaultAudit, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<FileVaultAudit, bool>> where, List<Expression<Func<FileVaultAudit, object>>> includes);
        void Insert(FileVaultAudit item, bool saveChanges = true);
        void Update(FileVaultAudit item, bool saveChanges = true);
        void Delete(FileVaultAudit item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class FileVaultAuditService : IFileVaultAuditService
    {
        private readonly IRepository<FileVaultAudit> _repository;

        public FileVaultAuditService(IRepository<FileVaultAudit> repository)
        {
            this._repository = repository;
        }

        public FileVaultAudit Get(Expression<Func<FileVaultAudit, bool>> where, List<Expression<Func<FileVaultAudit, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<FileVaultAudit> GetMany(int? top, Expression<Func<FileVaultAudit, bool>> where, Func<IQueryable<FileVaultAudit>, IOrderedQueryable<FileVaultAudit>> orderBy, List<Expression<Func<FileVaultAudit, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<FileVaultAudit> GetManyPaged(int pageNumber, int pageSize, Expression<Func<FileVaultAudit, bool>> where, Func<IQueryable<FileVaultAudit>, IOrderedQueryable<FileVaultAudit>> orderBy, List<Expression<Func<FileVaultAudit, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<FileVaultAudit, bool>> where, List<Expression<Func<FileVaultAudit, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(FileVaultAudit item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("FileVaultAudit");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(FileVaultAudit item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("FileVaultAudit");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(FileVaultAudit item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("FileVaultAudit");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
