﻿#region Using directives

using System;

#endregion

namespace KaiZen.CSMS.Entities
{	
	///<summary>
	/// An object representation of the 'TwentyOnePointAudit_Qtr' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class TwentyOnePointAuditQtr : TwentyOnePointAuditQtrBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="TwentyOnePointAuditQtr"/> instance.
		///</summary>
		public TwentyOnePointAuditQtr():base(){}	
		
		#endregion
	}
}
