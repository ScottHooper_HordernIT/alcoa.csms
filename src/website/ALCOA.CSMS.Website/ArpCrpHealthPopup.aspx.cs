﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Text;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using KaiZen.CSMS.Web;
using KaiZen.CSMS.Services;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxTabControl;
using DevExpress.Web.Data;
using DevExpress.Web.ASPxGridView;
using DevExpress.XtraPrinting;
using System.Collections.Generic;

using KaiZen.Library;
using System.Data.SqlClient;
using System.ComponentModel;
using System.Reflection;
using DevExpress.Web.ASPxPopupControl;



namespace ALCOA.CSMS.Website
{
    public partial class ArpCrpHelthPopup : System.Web.UI.Page
    {
        TList<Sites> sitesList;
        Auth auth = new Auth();
        protected void Page_Load(object sender, EventArgs e)
        {
          //  ddlCompanies.SelectedItem = null;            
            Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack);

        }

        private void moduleInit(bool postBack)
        {
            
            if (!postBack)
            {
                int _i = 0;

                cbRegionSite.Items.Clear();
                SitesFilters sitesFilters = new SitesFilters();
                sitesFilters.Append(SitesColumn.IsVisible, "1");
                sitesList = DataRepository.SitesProvider.GetPaged(sitesFilters.ToString(), "SiteName ASC", 0, 100, out _i);
                Session["SiteList"] = sitesList;
                foreach (Sites s in sitesList)
                {
                    cbRegionSite.Items.Add(s.SiteName, s.SiteId);
                }
                cbRegionSite.Items.Add("----------------", 0);

                RegionsFilters regionsFilters = new RegionsFilters();
                regionsFilters.Append(RegionsColumn.IsVisible, "1");
                TList<Regions> regionsList = DataRepository.RegionsProvider.GetPaged(regionsFilters.ToString(), "Ordinal ASC", 0, 100, out _i);
                foreach (Regions r in regionsList)
                {
                    int NegRegionId = -1 * r.RegionId;

                    cbRegionSite.Items.Add(r.RegionName, NegRegionId);

                }
                int indexAus = cbRegionSite.Items.IndexOfText("Australia");

                cbRegionSite.SelectedIndex = indexAus;

                string getCmp = (Request.QueryString["cmpId"] == null ? "0" : Request.QueryString["cmpId"]);

                ddlCompanies.DataSourceID = "dsCompanies";
                ddlCompanies.TextField = "CompanyName";
                ddlCompanies.ValueField = "CompanyId";
                ddlCompanies.DataBind();

                ddlCompanies.Items.Add(" < Select Company... >", -1);
                

                if (Convert.ToInt32(getCmp) != -1)
                {
                    ddlCompanies.Value = Convert.ToInt32(getCmp);
                    Session["CmpId"] = Convert.ToInt32(getCmp);
                }
                else
                {
                    ddlCompanies.SelectedIndex = -1;
                    ddlCompanies.Value = -1;
                    ddlCompanies.Text = " < Select Company... >";
                    Session["CmpId"] = 0;
                }

                switch (auth.RoleId)
                {
                    
                    case ((int)RoleList.Contractor):
                        //Panel1.Visible = true;
                        ddlCompanies.Visible = true;
                        Session["CmpId"] = auth.CompanyId.ToString();
                        
                        ddlCompanies.Text = auth.CompanyName;
                        ddlCompanies.Value = auth.CompanyId;


                        ddlCompanies.Enabled = false;
                        break;
                    default:
                        break;
                }
                Session["SiteIdN"] = -2;


                if (Convert.ToInt32(Session["CmpId"]) > 0)
                {

                    DataSet dsCRP = DataRepository.KpiProvider.Get_CRP(Convert.ToInt32(Session["CmpId"]));

                    gridCRP.DataSource = dsCRP.Tables[0];
                    gridCRP.DataBind();

                    getArpByCompanySite(Convert.ToInt32(Session["CmpId"]), Convert.ToInt32(Session["SiteIdN"]));
                }
            }
        }

        protected void ddlCompanies_SelectedIndexChanged(object sender, EventArgs e)
        {
            int CompanyId = Convert.ToInt32(ddlCompanies.SelectedItem.Value);
            int SiteId = Convert.ToInt32(cbRegionSite.SelectedItem.Value);
            Session["CmpId"] = CompanyId;
            Session["SiteIdN"] = SiteId;

            if (CompanyId > 0)
            {  
                DataSet dsCRP = DataRepository.KpiProvider.Get_CRP(CompanyId);

                gridCRP.DataSource = dsCRP.Tables[0];
                gridCRP.DataBind();

                getArpByCompanySite(CompanyId, SiteId);
            }
            else
            {
                //throw new Exception("Please Select a Company");
                PopupWindow pcWindow = new PopupWindow("Please Select a Company");
                pcWindow.FooterText = "";
                pcWindow.ShowOnPageLoad = true;
                pcWindow.Modal = true;
                ASPxPopupControl1.Windows.Add(pcWindow);
                gridCRP.DataSource = null;
                gridCRP.DataBind();
            }
        }

        protected void cbRegionSite_SelectedIndexChanged(object sender, EventArgs e)
        {
            int CompanyId = Convert.ToInt32(ddlCompanies.SelectedItem.Value);
            int SiteId = Convert.ToInt32(cbRegionSite.SelectedItem.Value);
            Session["CmpId"] = CompanyId;
            Session["SiteIdN"] = SiteId;

            if (CompanyId > 0)
            {
                if (SiteId != 0)
                {
                    getArpByCompanySite(CompanyId, SiteId);
                }
                else
                {
                    PopupWindow pcWindow = new PopupWindow("Please Select a Site");
                    pcWindow.FooterText = "";
                    pcWindow.ShowOnPageLoad = true;
                    pcWindow.Modal = true;
                    ASPxPopupControl1.Windows.Add(pcWindow);
                    gridARP.DataSource = null;
                    gridARP.DataBind();
                }
            }
            else
            {
                //throw new Exception("Please Select a Company");

                PopupWindow pcWindow = new PopupWindow("Please Select a Company");
                pcWindow.FooterText = "";
                pcWindow.ShowOnPageLoad = true;
                pcWindow.Modal = true;
                ASPxPopupControl1.Windows.Add(pcWindow);
                gridARP.DataSource = null;
                gridARP.DataBind();
            }
        }

        public void getArpByCompanySite(int CompanyId, int SiteId)
        {
            DataSet dsARP = DataRepository.KpiProvider.Get_ARP(CompanyId, SiteId);

            gridARP.DataSource = dsARP.Tables[0];
            gridARP.DataBind();
        }
    }
}