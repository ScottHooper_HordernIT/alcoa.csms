﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.CompanyStatusChangeApprovalProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(CompanyStatusChangeApprovalDataSourceDesigner))]
	public class CompanyStatusChangeApprovalDataSource : ProviderDataSource<CompanyStatusChangeApproval, CompanyStatusChangeApprovalKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompanyStatusChangeApprovalDataSource class.
		/// </summary>
		public CompanyStatusChangeApprovalDataSource() : base(new CompanyStatusChangeApprovalService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the CompanyStatusChangeApprovalDataSourceView used by the CompanyStatusChangeApprovalDataSource.
		/// </summary>
		protected CompanyStatusChangeApprovalDataSourceView CompanyStatusChangeApprovalView
		{
			get { return ( View as CompanyStatusChangeApprovalDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the CompanyStatusChangeApprovalDataSource control invokes to retrieve data.
		/// </summary>
		public CompanyStatusChangeApprovalSelectMethod SelectMethod
		{
			get
			{
				CompanyStatusChangeApprovalSelectMethod selectMethod = CompanyStatusChangeApprovalSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (CompanyStatusChangeApprovalSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the CompanyStatusChangeApprovalDataSourceView class that is to be
		/// used by the CompanyStatusChangeApprovalDataSource.
		/// </summary>
		/// <returns>An instance of the CompanyStatusChangeApprovalDataSourceView class.</returns>
		protected override BaseDataSourceView<CompanyStatusChangeApproval, CompanyStatusChangeApprovalKey> GetNewDataSourceView()
		{
			return new CompanyStatusChangeApprovalDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the CompanyStatusChangeApprovalDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class CompanyStatusChangeApprovalDataSourceView : ProviderDataSourceView<CompanyStatusChangeApproval, CompanyStatusChangeApprovalKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompanyStatusChangeApprovalDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the CompanyStatusChangeApprovalDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public CompanyStatusChangeApprovalDataSourceView(CompanyStatusChangeApprovalDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal CompanyStatusChangeApprovalDataSource CompanyStatusChangeApprovalOwner
		{
			get { return Owner as CompanyStatusChangeApprovalDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal CompanyStatusChangeApprovalSelectMethod SelectMethod
		{
			get { return CompanyStatusChangeApprovalOwner.SelectMethod; }
			set { CompanyStatusChangeApprovalOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal CompanyStatusChangeApprovalService CompanyStatusChangeApprovalProvider
		{
			get { return Provider as CompanyStatusChangeApprovalService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<CompanyStatusChangeApproval> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<CompanyStatusChangeApproval> results = null;
			CompanyStatusChangeApproval item;
			count = 0;
			
			System.Int32 _companyStatusChangeApprovalId;
			System.Int32 _requestedByUserId;
			System.Int32? _assessedByUserId_nullable;
			System.Int32 _companyId;
			System.Int32 _currentCompanyStatusId;
			System.Int32 _questionnaireId;
			System.Int32 _requestedCompanyStatusId;

			switch ( SelectMethod )
			{
				case CompanyStatusChangeApprovalSelectMethod.Get:
					CompanyStatusChangeApprovalKey entityKey  = new CompanyStatusChangeApprovalKey();
					entityKey.Load(values);
					item = CompanyStatusChangeApprovalProvider.Get(entityKey);
					results = new TList<CompanyStatusChangeApproval>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case CompanyStatusChangeApprovalSelectMethod.GetAll:
                    results = CompanyStatusChangeApprovalProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case CompanyStatusChangeApprovalSelectMethod.GetPaged:
					results = CompanyStatusChangeApprovalProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case CompanyStatusChangeApprovalSelectMethod.Find:
					if ( FilterParameters != null )
						results = CompanyStatusChangeApprovalProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = CompanyStatusChangeApprovalProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case CompanyStatusChangeApprovalSelectMethod.GetByCompanyStatusChangeApprovalId:
					_companyStatusChangeApprovalId = ( values["CompanyStatusChangeApprovalId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["CompanyStatusChangeApprovalId"], typeof(System.Int32)) : (int)0;
					item = CompanyStatusChangeApprovalProvider.GetByCompanyStatusChangeApprovalId(_companyStatusChangeApprovalId);
					results = new TList<CompanyStatusChangeApproval>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				// FK
				case CompanyStatusChangeApprovalSelectMethod.GetByRequestedByUserId:
					_requestedByUserId = ( values["RequestedByUserId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["RequestedByUserId"], typeof(System.Int32)) : (int)0;
					results = CompanyStatusChangeApprovalProvider.GetByRequestedByUserId(_requestedByUserId, this.StartIndex, this.PageSize, out count);
					break;
				case CompanyStatusChangeApprovalSelectMethod.GetByAssessedByUserId:
					_assessedByUserId_nullable = (System.Int32?) EntityUtil.ChangeType(values["AssessedByUserId"], typeof(System.Int32?));
					results = CompanyStatusChangeApprovalProvider.GetByAssessedByUserId(_assessedByUserId_nullable, this.StartIndex, this.PageSize, out count);
					break;
				case CompanyStatusChangeApprovalSelectMethod.GetByCompanyId:
					_companyId = ( values["CompanyId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["CompanyId"], typeof(System.Int32)) : (int)0;
					results = CompanyStatusChangeApprovalProvider.GetByCompanyId(_companyId, this.StartIndex, this.PageSize, out count);
					break;
				case CompanyStatusChangeApprovalSelectMethod.GetByCurrentCompanyStatusId:
					_currentCompanyStatusId = ( values["CurrentCompanyStatusId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["CurrentCompanyStatusId"], typeof(System.Int32)) : (int)0;
					results = CompanyStatusChangeApprovalProvider.GetByCurrentCompanyStatusId(_currentCompanyStatusId, this.StartIndex, this.PageSize, out count);
					break;
				case CompanyStatusChangeApprovalSelectMethod.GetByQuestionnaireId:
					_questionnaireId = ( values["QuestionnaireId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["QuestionnaireId"], typeof(System.Int32)) : (int)0;
					results = CompanyStatusChangeApprovalProvider.GetByQuestionnaireId(_questionnaireId, this.StartIndex, this.PageSize, out count);
					break;
				case CompanyStatusChangeApprovalSelectMethod.GetByRequestedCompanyStatusId:
					_requestedCompanyStatusId = ( values["RequestedCompanyStatusId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["RequestedCompanyStatusId"], typeof(System.Int32)) : (int)0;
					results = CompanyStatusChangeApprovalProvider.GetByRequestedCompanyStatusId(_requestedCompanyStatusId, this.StartIndex, this.PageSize, out count);
					break;
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == CompanyStatusChangeApprovalSelectMethod.Get || SelectMethod == CompanyStatusChangeApprovalSelectMethod.GetByCompanyStatusChangeApprovalId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				CompanyStatusChangeApproval entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					CompanyStatusChangeApprovalProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<CompanyStatusChangeApproval> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			CompanyStatusChangeApprovalProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region CompanyStatusChangeApprovalDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the CompanyStatusChangeApprovalDataSource class.
	/// </summary>
	public class CompanyStatusChangeApprovalDataSourceDesigner : ProviderDataSourceDesigner<CompanyStatusChangeApproval, CompanyStatusChangeApprovalKey>
	{
		/// <summary>
		/// Initializes a new instance of the CompanyStatusChangeApprovalDataSourceDesigner class.
		/// </summary>
		public CompanyStatusChangeApprovalDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public CompanyStatusChangeApprovalSelectMethod SelectMethod
		{
			get { return ((CompanyStatusChangeApprovalDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new CompanyStatusChangeApprovalDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region CompanyStatusChangeApprovalDataSourceActionList

	/// <summary>
	/// Supports the CompanyStatusChangeApprovalDataSourceDesigner class.
	/// </summary>
	internal class CompanyStatusChangeApprovalDataSourceActionList : DesignerActionList
	{
		private CompanyStatusChangeApprovalDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the CompanyStatusChangeApprovalDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public CompanyStatusChangeApprovalDataSourceActionList(CompanyStatusChangeApprovalDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public CompanyStatusChangeApprovalSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion CompanyStatusChangeApprovalDataSourceActionList
	
	#endregion CompanyStatusChangeApprovalDataSourceDesigner
	
	#region CompanyStatusChangeApprovalSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the CompanyStatusChangeApprovalDataSource.SelectMethod property.
	/// </summary>
	public enum CompanyStatusChangeApprovalSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByCompanyStatusChangeApprovalId method.
		/// </summary>
		GetByCompanyStatusChangeApprovalId,
		/// <summary>
		/// Represents the GetByRequestedByUserId method.
		/// </summary>
		GetByRequestedByUserId,
		/// <summary>
		/// Represents the GetByAssessedByUserId method.
		/// </summary>
		GetByAssessedByUserId,
		/// <summary>
		/// Represents the GetByCompanyId method.
		/// </summary>
		GetByCompanyId,
		/// <summary>
		/// Represents the GetByCurrentCompanyStatusId method.
		/// </summary>
		GetByCurrentCompanyStatusId,
		/// <summary>
		/// Represents the GetByQuestionnaireId method.
		/// </summary>
		GetByQuestionnaireId,
		/// <summary>
		/// Represents the GetByRequestedCompanyStatusId method.
		/// </summary>
		GetByRequestedCompanyStatusId
	}
	
	#endregion CompanyStatusChangeApprovalSelectMethod

	#region CompanyStatusChangeApprovalFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CompanyStatusChangeApproval"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompanyStatusChangeApprovalFilter : SqlFilter<CompanyStatusChangeApprovalColumn>
	{
	}
	
	#endregion CompanyStatusChangeApprovalFilter

	#region CompanyStatusChangeApprovalExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CompanyStatusChangeApproval"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompanyStatusChangeApprovalExpressionBuilder : SqlExpressionBuilder<CompanyStatusChangeApprovalColumn>
	{
	}
	
	#endregion CompanyStatusChangeApprovalExpressionBuilder	

	#region CompanyStatusChangeApprovalProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;CompanyStatusChangeApprovalChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="CompanyStatusChangeApproval"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompanyStatusChangeApprovalProperty : ChildEntityProperty<CompanyStatusChangeApprovalChildEntityTypes>
	{
	}
	
	#endregion CompanyStatusChangeApprovalProperty
}

