﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.AdminTaskHistoryProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[CLSCompliant(true)]
	[Designer(typeof(AdminTaskHistoryDataSourceDesigner))]
	public class AdminTaskHistoryDataSource : ReadOnlyDataSource<AdminTaskHistory>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AdminTaskHistoryDataSource class.
		/// </summary>
		public AdminTaskHistoryDataSource() : base(new AdminTaskHistoryService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the AdminTaskHistoryDataSourceView used by the AdminTaskHistoryDataSource.
		/// </summary>
		protected AdminTaskHistoryDataSourceView AdminTaskHistoryView
		{
			get { return ( View as AdminTaskHistoryDataSourceView ); }
		}
		
		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the AdminTaskHistoryDataSourceView class that is to be
		/// used by the AdminTaskHistoryDataSource.
		/// </summary>
		/// <returns>An instance of the AdminTaskHistoryDataSourceView class.</returns>
		protected override BaseDataSourceView<AdminTaskHistory, Object> GetNewDataSourceView()
		{
			return new AdminTaskHistoryDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the AdminTaskHistoryDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class AdminTaskHistoryDataSourceView : ReadOnlyDataSourceView<AdminTaskHistory>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AdminTaskHistoryDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the AdminTaskHistoryDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public AdminTaskHistoryDataSourceView(AdminTaskHistoryDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal AdminTaskHistoryDataSource AdminTaskHistoryOwner
		{
			get { return Owner as AdminTaskHistoryDataSource; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal AdminTaskHistoryService AdminTaskHistoryProvider
		{
			get { return Provider as AdminTaskHistoryService; }
		}

		#endregion Properties
		
		#region Methods
		
		#endregion Methods
	}

	#region AdminTaskHistoryDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the AdminTaskHistoryDataSource class.
	/// </summary>
	public class AdminTaskHistoryDataSourceDesigner : ReadOnlyDataSourceDesigner<AdminTaskHistory>
	{
	}

	#endregion AdminTaskHistoryDataSourceDesigner

	#region AdminTaskHistoryFilter

	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AdminTaskHistory"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AdminTaskHistoryFilter : SqlFilter<AdminTaskHistoryColumn>
	{
	}

	#endregion AdminTaskHistoryFilter

	#region AdminTaskHistoryExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AdminTaskHistory"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AdminTaskHistoryExpressionBuilder : SqlExpressionBuilder<AdminTaskHistoryColumn>
	{
	}
	
	#endregion AdminTaskHistoryExpressionBuilder		
}

