﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface IQuestionnaireMainQuestionService
    {
        QuestionnaireMainQuestion Get(Expression<Func<QuestionnaireMainQuestion, bool>> where, List<Expression<Func<QuestionnaireMainQuestion, object>>> includes);
        List<QuestionnaireMainQuestion> GetMany(int? top, Expression<Func<QuestionnaireMainQuestion, bool>> where, Func<IQueryable<QuestionnaireMainQuestion>, IOrderedQueryable<QuestionnaireMainQuestion>> orderBy, List<Expression<Func<QuestionnaireMainQuestion, object>>> includes);
        List<QuestionnaireMainQuestion> GetManyPaged(int pageNumber, int pageSize, Expression<Func<QuestionnaireMainQuestion, bool>> where, Func<IQueryable<QuestionnaireMainQuestion>, IOrderedQueryable<QuestionnaireMainQuestion>> orderBy, List<Expression<Func<QuestionnaireMainQuestion, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<QuestionnaireMainQuestion, bool>> where, List<Expression<Func<QuestionnaireMainQuestion, object>>> includes);
        void Insert(QuestionnaireMainQuestion item, bool saveChanges = true);
        void Update(QuestionnaireMainQuestion item, bool saveChanges = true);
        void Delete(QuestionnaireMainQuestion item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class QuestionnaireMainQuestionService : IQuestionnaireMainQuestionService
    {
        private readonly IRepository<QuestionnaireMainQuestion> _repository;

        public QuestionnaireMainQuestionService(IRepository<QuestionnaireMainQuestion> repository)
        {
            this._repository = repository;
        }

        public QuestionnaireMainQuestion Get(Expression<Func<QuestionnaireMainQuestion, bool>> where, List<Expression<Func<QuestionnaireMainQuestion, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<QuestionnaireMainQuestion> GetMany(int? top, Expression<Func<QuestionnaireMainQuestion, bool>> where, Func<IQueryable<QuestionnaireMainQuestion>, IOrderedQueryable<QuestionnaireMainQuestion>> orderBy, List<Expression<Func<QuestionnaireMainQuestion, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<QuestionnaireMainQuestion> GetManyPaged(int pageNumber, int pageSize, Expression<Func<QuestionnaireMainQuestion, bool>> where, Func<IQueryable<QuestionnaireMainQuestion>, IOrderedQueryable<QuestionnaireMainQuestion>> orderBy, List<Expression<Func<QuestionnaireMainQuestion, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<QuestionnaireMainQuestion, bool>> where, List<Expression<Func<QuestionnaireMainQuestion, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(QuestionnaireMainQuestion item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("QuestionnaireMainQuestion");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(QuestionnaireMainQuestion item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("QuestionnaireMainQuestion");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(QuestionnaireMainQuestion item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("QuestionnaireMainQuestion");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
