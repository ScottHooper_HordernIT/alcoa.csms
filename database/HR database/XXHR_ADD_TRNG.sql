USE [ALCOA_WAO_CSMWP]
GO

/****** Object:  Table [dbo].[XXHR_ADD_TRNG]    Script Date: 8/07/2015 4:57:39 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[XXHR_ADD_TRNG](
	[PERSON_ID] [numeric](10, 0) NOT NULL,
	[FULL_NAME] [nvarchar](240) NULL,
	[EMPL_ID] [nvarchar](30) NULL,
	[COURSE_NAME] [nvarchar](80) NOT NULL,
	[TRAINING_TITLE] [nvarchar](80) NOT NULL,
	[COURSE_END_DATE] [datetime] NOT NULL,
	[EXPIRATION_DATE] [nvarchar](19) NULL,
	[DURATION] [nvarchar](384) NULL,
	[DURATION_UNITS] [nvarchar](30) NULL,
	[PER_TYPE_ID] [nvarchar](80) NOT NULL,
	[RECORD_TYPE] [nchar](19) NULL,
	[LAST_UPDATE_DATE] [datetime] NULL
) ON [PRIMARY]

GO

