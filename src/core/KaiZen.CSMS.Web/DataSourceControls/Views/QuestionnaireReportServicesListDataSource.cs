﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.QuestionnaireReportServicesListProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[CLSCompliant(true)]
	[Designer(typeof(QuestionnaireReportServicesListDataSourceDesigner))]
	public class QuestionnaireReportServicesListDataSource : ReadOnlyDataSource<QuestionnaireReportServicesList>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportServicesListDataSource class.
		/// </summary>
		public QuestionnaireReportServicesListDataSource() : base(new QuestionnaireReportServicesListService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the QuestionnaireReportServicesListDataSourceView used by the QuestionnaireReportServicesListDataSource.
		/// </summary>
		protected QuestionnaireReportServicesListDataSourceView QuestionnaireReportServicesListView
		{
			get { return ( View as QuestionnaireReportServicesListDataSourceView ); }
		}
		
		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the QuestionnaireReportServicesListDataSourceView class that is to be
		/// used by the QuestionnaireReportServicesListDataSource.
		/// </summary>
		/// <returns>An instance of the QuestionnaireReportServicesListDataSourceView class.</returns>
		protected override BaseDataSourceView<QuestionnaireReportServicesList, Object> GetNewDataSourceView()
		{
			return new QuestionnaireReportServicesListDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the QuestionnaireReportServicesListDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class QuestionnaireReportServicesListDataSourceView : ReadOnlyDataSourceView<QuestionnaireReportServicesList>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportServicesListDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the QuestionnaireReportServicesListDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public QuestionnaireReportServicesListDataSourceView(QuestionnaireReportServicesListDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal QuestionnaireReportServicesListDataSource QuestionnaireReportServicesListOwner
		{
			get { return Owner as QuestionnaireReportServicesListDataSource; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal QuestionnaireReportServicesListService QuestionnaireReportServicesListProvider
		{
			get { return Provider as QuestionnaireReportServicesListService; }
		}

		#endregion Properties
		
		#region Methods
		
		#endregion Methods
	}

	#region QuestionnaireReportServicesListDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the QuestionnaireReportServicesListDataSource class.
	/// </summary>
	public class QuestionnaireReportServicesListDataSourceDesigner : ReadOnlyDataSourceDesigner<QuestionnaireReportServicesList>
	{
	}

	#endregion QuestionnaireReportServicesListDataSourceDesigner

	#region QuestionnaireReportServicesListFilter

	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireReportServicesList"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireReportServicesListFilter : SqlFilter<QuestionnaireReportServicesListColumn>
	{
	}

	#endregion QuestionnaireReportServicesListFilter

	#region QuestionnaireReportServicesListExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireReportServicesList"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireReportServicesListExpressionBuilder : SqlExpressionBuilder<QuestionnaireReportServicesListColumn>
	{
	}
	
	#endregion QuestionnaireReportServicesListExpressionBuilder		
}

