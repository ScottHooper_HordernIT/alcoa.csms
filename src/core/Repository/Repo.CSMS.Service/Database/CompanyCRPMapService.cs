﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface ICompanyCRPMapService
    {
        CompanyCRPMap Get(Expression<Func<CompanyCRPMap, bool>> where, List<Expression<Func<CompanyCRPMap, object>>> includes);
        List<CompanyCRPMap> GetMany(int? top, Expression<Func<CompanyCRPMap, bool>> where, Func<IQueryable<CompanyCRPMap>, IOrderedQueryable<CompanyCRPMap>> orderBy, List<Expression<Func<CompanyCRPMap, object>>> includes);
        List<CompanyCRPMap> GetManyPaged(int pageNumber, int pageSize, Expression<Func<CompanyCRPMap, bool>> where, Func<IQueryable<CompanyCRPMap>, IOrderedQueryable<CompanyCRPMap>> orderBy, List<Expression<Func<CompanyCRPMap, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<CompanyCRPMap, bool>> where, List<Expression<Func<CompanyCRPMap, object>>> includes);
        void Insert(CompanyCRPMap item, bool saveChanges = true);
        void Update(CompanyCRPMap item, bool saveChanges = true);
        void Delete(CompanyCRPMap item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class CompanyCRPMapService : ICompanyCRPMapService
    {
        private readonly IRepository<CompanyCRPMap> _repository;

        public CompanyCRPMapService(IRepository<CompanyCRPMap> repository)
        {
            this._repository = repository;
        }

        public CompanyCRPMap Get(Expression<Func<CompanyCRPMap, bool>> where, List<Expression<Func<CompanyCRPMap, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<CompanyCRPMap> GetMany(int? top, Expression<Func<CompanyCRPMap, bool>> where, Func<IQueryable<CompanyCRPMap>, IOrderedQueryable<CompanyCRPMap>> orderBy, List<Expression<Func<CompanyCRPMap, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<CompanyCRPMap> GetManyPaged(int pageNumber, int pageSize, Expression<Func<CompanyCRPMap, bool>> where, Func<IQueryable<CompanyCRPMap>, IOrderedQueryable<CompanyCRPMap>> orderBy, List<Expression<Func<CompanyCRPMap, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<CompanyCRPMap, bool>> where, List<Expression<Func<CompanyCRPMap, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(CompanyCRPMap item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("CompanyCRPMap");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(CompanyCRPMap item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("CompanyCRPMap");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(CompanyCRPMap item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("CompanyCRPMap");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
