﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/user.master" AutoEventWireup="true" Inherits="SafetyPQ_QuestionnaireVerificationSection" Codebehind="SafetyPQ_QuestionnaireVerificationSection.aspx.cs" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
    
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="Server">
    <table border="0" cellpadding="2" cellspacing="0" width="100%">
        <tr>
            <td class="pageName" colspan="3" style="height: 17px">
                <a name="save"></a><span class="bodycopy"><span class="title">Safety Qualification</span><br />
                    <span class="date">Verification Questionnaire</span><br />
                    <img height="1" src="images/grfc_dottedline.gif" width="24" />&nbsp;</span>
            </td>
        </tr>
    </table>
    <asp:PlaceHolder ID="PlaceHolder1" runat="server"></asp:PlaceHolder>
    <table width="900">
        <tbody>
            <tr width="900px">
                <td style="text-align: right" class="pageName">
                    <a href="#top">Scroll to top of page</a>
                    <br />
                    <br />
                </td>
            </tr>
        </tbody>
    </table>
</asp:Content>
