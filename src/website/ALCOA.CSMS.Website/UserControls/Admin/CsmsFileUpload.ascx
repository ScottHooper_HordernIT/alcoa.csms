﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CsmsFileUpload.ascx.cs"
    Inherits="ALCOA.CSMS.Website.UserControls.Admin.CsmsFileUpload" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxUploadControl" TagPrefix="dx" %>
<p>
    <a href="admin.aspx?s=CsmsFiles">Back to CSMS Files</a><br />
</p>
<dx:ASPxPageControl ID="ASPxPageControl1" runat="server" ActiveTabIndex="0" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
    CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
    Width="900px">
    <TabPages>
        <dx:TabPage Text="Upload Csms File">
            <ContentCollection>
                <dx:ContentControl runat="server" SupportsDisabledAttribute="True">
                    <asp:UpdatePanel ID="UpdatePanel10" runat="server" RenderMode="Inline" UpdateMode="Conditional">
                        <ContentTemplate>
                            <table cellpadding="0" cellspacing="0" class="style1">
                                <tr>
                                    <td>
                                        Select File for Upload:
                                    </td>
                                    <td>
                                        <dx:ASPxUploadControl ID="ucNewFile" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                            CssPostfix="Office2003Blue" Width="400px">
                                        </dx:ASPxUploadControl>
                                    </td>
                                </tr>
                                <tr>
                                <td>
                                &nbsp;
                                </td>
                                <td>
                                    <dx:ASPxLabel ID="lblFile" runat="server" ForeColor="Red" Text="Note: File Already Uploaded. Uploading Another file will replace this: "
                                            Visible="false">
                                        </dx:ASPxLabel><br />
                                        <dx:ASPxHyperLink ID="hlFile" runat="server" Text="">
                                        </dx:ASPxHyperLink>
                                </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        File Name:
                                    </td>
                                    <td>
                                        <dx:ASPxTextBox ID="tbNewFileName" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                            CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                            Width="400px">
                                            <ValidationSettings ValidationGroup="Upload">
                                                <RequiredField IsRequired="True" />
                                            </ValidationSettings>
                                        </dx:ASPxTextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        File Description:
                                    </td>
                                    <td>
                                        <dx:ASPxTextBox ID="tbNewFileDescription" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                            CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                            Width="400px">
                                            <ValidationSettings ValidationGroup="Upload">
                                                <RequiredField IsRequired="True" />
                                            </ValidationSettings>
                                        </dx:ASPxTextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        File Type:
                                    </td>
                                    <td>
                                        <dx:ASPxComboBox ID="cbNewFileType" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                            CssPostfix="Office2003Blue" DataSourceID="CsmsFileTypeDataSource1" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                            TextField="CsmsFileTypeName" ValueField="CsmsFileTypeId" ValueType="System.Int32"
                                            Width="400px">
                                            <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                                            </LoadingPanelImage>
                                            <ValidationSettings ValidationGroup="Upload">
                                                <RequiredField IsRequired="True" />
                                            </ValidationSettings>
                                        </dx:ASPxComboBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Visible (shown in help):
                                    </td>
                                    <td>
                                        <dx:ASPxCheckBox ID="cbVisible" runat="server" Checked="true">
                                        </dx:ASPxCheckBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                        <dx:ASPxButton ID="btnUpload" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                            CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                            Text="Upload" OnClick="btnUpload_Click" ValidationGroup="Upload">
                                        </dx:ASPxButton>
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                        <Triggers>
                            <asp:PostBackTrigger ControlID="btnUpload"></asp:PostBackTrigger>
                        </Triggers>
                    </asp:UpdatePanel>
                </dx:ContentControl>
            </ContentCollection>
        </dx:TabPage>
    </TabPages>
    <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
    </LoadingPanelImage>
    <LoadingPanelStyle ImageSpacing="6px">
    </LoadingPanelStyle>
    <ContentStyle>
        <Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px" />
    </ContentStyle>
</dx:ASPxPageControl>
<data:CsmsFileTypeDataSource ID="CsmsFileTypeDataSource1" runat="server">
</data:CsmsFileTypeDataSource>
