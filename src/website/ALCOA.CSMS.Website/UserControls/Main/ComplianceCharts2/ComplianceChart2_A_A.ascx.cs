using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using KaiZen.CSMS.Services;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

using DevExpress.Web.ASPxPopupControl;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxEditors;

using System.Drawing;
using System.Linq;

//Add By Bishwajit for Item#31
using System.Web.Configuration;
using System.Text;
using System.IO;
using iTextSharp.text;
using System.Collections.Generic;
using System.ComponentModel;
using iTextSharp.text.pdf;
//End Add By Bishwajit for Item#31

public partial class UserControls_Tiny_ComplianceCharts_ComplianceChart2_A_A : System.Web.UI.UserControl
{
    int SiteId, YearId, MonthId;
    int grid_VisibleIndex = 0;

    bool E, NE1, NE2, NE3;

    Auth auth = new Auth();

    //Add By Bishwajit for Item#31
    private enum TrafficLightColor
    {
        [Description("0")]
        Grey = 0,
        [Description("1")]
        Red = 1,
        [Description("2")]
        Yellow = 2,
        [Description("3")]
        Green = 3,
    }
    //End Add By Bishwajit for Item#31

    protected void Page_Load(object sender, EventArgs e) { Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); }
    private void moduleInit(bool postBack)
    {
        try
        {
            // DT 3224 Changes : added the new session
            Session["IsUserControlToAdd"] = false;
            if (!postBack)
            {

                //Helper.Radar.CheckInputVariables(true);
                //Helper.Radar.CheckAnnualTargets(Helper.Radar.GetResidentialCategories(), Convert.ToInt32(SessionHandler.spVar_Year));
                //BindData(GenerateTableData());

                //0. Reset
                ResetData();

                //1. Check Session Variables
                Helper.Radar.CheckInputVariables(true);

                //2. Assign Session Variables
                SiteId = Convert.ToInt32(SessionHandler.spVar_SiteId);
                YearId = Convert.ToInt32(SessionHandler.spVar_Year);
                MonthId = Convert.ToInt32(SessionHandler.spVar_MonthId);

                //3. Generate Table Data
                GenerateData(SiteId, YearId, MonthId);
                ShowSafetyStatsAll2(SiteId, YearId, MonthId); //Show LWDFR/TRIFR/AIFR/IFE:Injury Ratio/DART (All)

                //4. Bind Table Data
                BindData();
                //Add By Bishwajit for Item#31
                //5. Add values to Session value for PDF file Item:31
                if (E)
                { Session["E"] = true; }
                else { Session["E"] = false; }

                if (NE1)
                { Session["NE1"] = true; }
                else { Session["NE1"] = false; }

                if (NE2)
                { Session["NE2"] = true; }
                else { Session["NE2"] = false; }

                if (NE3)
                { Session["NE3"] = true; }
                else { Session["NE3"] = false; }

                // add Session to display Last valid At Time in PDF Item:31
                Session["LastViewedAt_E"] = lblLastViewedAt_E.Text;
                Session["LastViewedAt_NE1"] = lblLastViewedAt_NE1.Text;
                Session["LastViewedAt_NE2"] = lblLastViewedAt_NE2.Text;
                Session["LastViewedAt_NE3"] = lblLastViewedAt_NE3.Text;
                //END add Session to display Last valid  At Time in PDF Item:31
                //End Add By Bishwajit for Item#31  
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void ResetData()
    {
        E = false;
        NE1 = false;
        NE2 = false;
        NE3 = false;

        lblLastViewedAt_E.Text = "Data Valid As of: n/a";
        lblLastViewedAt_NE1.Text = lblLastViewedAt_E.Text;
        lblLastViewedAt_NE2.Text = lblLastViewedAt_E.Text;
        lblLastViewedAt_NE3.Text = lblLastViewedAt_E.Text;

        Configuration config = new Configuration();

        lblUpdateTime_E.Text = config.GetValue(ConfigList.UpdateRadarTime);
        lblUpdateTime_NE1.Text = lblUpdateTime_E.Text;
        lblUpdateTime_NE2.Text = lblUpdateTime_E.Text;
        lblUpdateTime_NE3.Text = lblUpdateTime_E.Text;

        ASPxPageControl1.TabPages[0].Enabled = false;
        ASPxPageControl1.TabPages[1].Enabled = false;
        ASPxPageControl1.TabPages[2].Enabled = false;
        ASPxPageControl1.TabPages[3].Enabled = false;
    }

    protected void ShowSafetyStatsAll(int SiteId, int YearId)
    {
        var storedProcedureService = ALCOA.CSMS.Website.Global.GetInstance<Repo.CSMS.Service.Database.IStoredProcedureService>();
        var companySiteCategoryStandardService = ALCOA.CSMS.Website.Global.GetInstance<Repo.CSMS.Service.Database.ICompanySiteCategoryStandardService>();

        KpiCompanySiteCategoryService kcscService = new KpiCompanySiteCategoryService();
        DataSet dsWAO = kcscService.Compliance_Table_AA(1, YearId, "%");
        DataSet dsVICOPS = kcscService.Compliance_Table_AA(4, YearId, "%");
        ////Added by Vikas Naidu for <DT 2460, 28-Sep-2012> - Start
        DataSet dsWAM = kcscService.Compliance_Table_AA(3, YearId, "%");
        //End
        DataTable dtYtdSummary = new DataTable();

        #region Create Grid Columns & DataTable Columns
        int grid_VisibleIndex = 0;
        grid_All.Width = Unit.Percentage(100);
        grid_All.KeyFieldName = "KpiMeasure";
        //Changed by Pankaj Dikshit for <DT 2399, 06-Aug-2012>
        grid_VisibleIndex = Helper.Radar.AddGridColumn(grid_All, "KpiMeasure", "Key Performance Indicators", "Safety Rates &nbsp;&nbsp;&nbsp;&nbsp;", 300, grid_VisibleIndex);
        dtYtdSummary.Columns.Add("KpiMeasure", typeof(string));

        string FieldName = "-WAO";
        string SiteName = "WAO";
        grid_VisibleIndex = Helper.Radar.AddGridColumn(grid_All, String.Format("KpiScoreYtd{0}", FieldName), SiteName, SiteName, 200, grid_VisibleIndex);
        dtYtdSummary.Columns.Add(String.Format("KpiScoreYtd{0}", "-WAO", typeof(string)));

        SitesService sService = new SitesService();
        RegionsSitesService rService = new RegionsSitesService();
        CompanySiteCategoryStandardService cscsService = new CompanySiteCategoryStandardService();

        var sitesWAO = storedProcedureService.SP_CompanySiteCategoryStandard_ByRegionId(1);
        var sitesVIC = storedProcedureService.SP_CompanySiteCategoryStandard_ByRegionId(4);
        //////Added by Vikas Naidu for <DT 2460, 28-Sep-2012> - Start
        var sitesMines = storedProcedureService.SP_CompanySiteCategoryStandard_ByRegionId(3);
        //End

        ////Adding by Pankaj Dikshit for <DT 2823, 19-Feb-2013> - Start
        DataSet dsGridCols = sService.GetByYearAdHocRadarId(YearId, '%');
        DataRow[] drTmp = dsGridCols.Tables[0].Select("SiteAbbrev = '-AM'");
        ////Adding by Pankaj Dikshit for <DT 2823, 19-Feb-2013> - End
        bool site = false;
        bool IsWAM = false; //<DT 2823, 19-Feb-2013>
        ////Added If-condition by Pankaj Dikshit for <DT 2823, 19-Feb-2013>
        //if (drTmp.Length > 0)
        //{
        //    FieldName = "-WAM";
        //    SiteName = "WAM";
        //    grid_VisibleIndex = Helper.Radar.AddGridColumn(grid_All, String.Format("KpiScoreYtd{0}", FieldName), SiteName, SiteName, 200, grid_VisibleIndex);
        //    dtYtdSummary.Columns.Add(String.Format("KpiScoreYtd{0}", "-WAM", typeof(string)));
        //    IsWAM = true;
        //}

        var siteIdWAOList = sitesWAO.Select(e => e.SiteId).ToList();
        var companySiteCategoryStandardsWAOGroupCount = companySiteCategoryStandardService.GetGroupCount(e => siteIdWAOList.Contains(e.SiteId), e => e.SiteId);

        var siteIdVICList = sitesVIC.Select(e => e.SiteId).ToList();
        var companySiteCategoryStandardsVICGroupCount = companySiteCategoryStandardService.GetGroupCount(e => siteIdVICList.Contains(e.SiteId), e => e.SiteId);

        foreach (var siteWAO in sitesWAO)
        {
            //TList<CompanySiteCategoryStandard> cscs = cscsService.GetBySiteId(siteWAO.SiteId);
            var cscs = companySiteCategoryStandardsWAOGroupCount.Where(e => System.Convert.ToInt32(e.Name) == siteWAO.SiteId).FirstOrDefault();

            //Added by Vikas Naidu for <DT 2460, 28-Sep-2012> - Start
            if (sitesMines[0].SiteId == siteWAO.SiteId)
            {
                //Added If-condition by Pankaj Dikshit for <DT 2823, 19-Feb-2013>
                if (drTmp.Length > 0)
                {
                    FieldName = "-WAM";
                    SiteName = "WAM";
                    grid_VisibleIndex = Helper.Radar.AddGridColumn(grid_All, String.Format("KpiScoreYtd{0}", FieldName), SiteName, SiteName, 200, grid_VisibleIndex);
                    dtYtdSummary.Columns.Add(String.Format("KpiScoreYtd{0}", "-WAM", typeof(string)));
                    IsWAM = true;
                }
            }
            //End
            //Adding by Pankaj Dikshit for <DT 2823, 19-Feb-2013>
            DataRow[] drTemp = dsGridCols.Tables[0].Select("SiteId = " + siteWAO.SiteId);

            if (cscs != null && drTemp.Length > 0)
            {
                if (cscs.Count > 0)
                {
                    site = true;

                    SiteName = siteWAO.SiteName; //SiteName
                    if (SiteName.Length > 1) SiteName = siteWAO.SiteAbbrev; //SiteAbbrev

                    grid_VisibleIndex = Helper.Radar.AddGridColumn(grid_All, String.Format("KpiScoreYtd{0}", SiteName), SiteName, SiteName, 100, grid_VisibleIndex);
                    dtYtdSummary.Columns.Add(String.Format("KpiScoreYtd{0}", SiteName, typeof(string)));

                }
            }
        }


        FieldName = "-VIC";
        SiteName = "VIC";
        grid_VisibleIndex = Helper.Radar.AddGridColumn(grid_All, String.Format("KpiScoreYtd{0}", FieldName), SiteName, SiteName, 200, grid_VisibleIndex);
        dtYtdSummary.Columns.Add(String.Format("KpiScoreYtd{0}", "-VIC", typeof(string)));

        foreach (var siteVIC in sitesVIC)
        {
            //TList<CompanySiteCategoryStandard> cscs = cscsService.GetBySiteId(siteVIC.SiteId);
            var cscs = companySiteCategoryStandardsVICGroupCount.Where(e => System.Convert.ToInt32(e.Name) == siteVIC.SiteId).FirstOrDefault();

            if (cscs != null)
            {
                if (cscs.Count > 0)
                {
                    site = true;

                    SiteName = siteVIC.SiteName; //SiteName
                    if (SiteName.Length > 1) SiteName = siteVIC.SiteAbbrev; //SiteAbbrev

                    grid_VisibleIndex = Helper.Radar.AddGridColumn(grid_All, String.Format("KpiScoreYtd{0}", SiteName), SiteName, SiteName, 100, grid_VisibleIndex);
                    dtYtdSummary.Columns.Add(String.Format("KpiScoreYtd{0}", SiteName, typeof(string)));

                }
            }
        }
        #endregion

        DataRow drYtdSummary = dtYtdSummary.NewRow();

        //LWDFR
        drYtdSummary["KpiMeasure"] = "LWDFR";
        foreach (DataRow _dr in dsWAO.Tables[0].Rows) drYtdSummary["KpiScoreYtd-WAO"] = _dr["LWDFR"].ToString();
        foreach (var siteWAO in sitesWAO)
        {
            //TList<CompanySiteCategoryStandard> cscs = cscsService.GetBySiteId(siteWAO.SiteId);
            var cscs = companySiteCategoryStandardsWAOGroupCount.Where(e => System.Convert.ToInt32(e.Name) == siteWAO.SiteId).FirstOrDefault();

            if (cscs != null)
            {
                if (cscs.Count > 0)
                {
                    DataSet _ds = kcscService.Compliance_Table_AS(siteWAO.SiteId, YearId, "%");
                    foreach (DataRow _dr2 in _ds.Tables[0].Rows) drYtdSummary[String.Format("KpiScoreYtd{0}", siteWAO.SiteAbbrev)] = _dr2["LWDFR"].ToString();
                }
            }
        }
        //Added by Vikas Naidu for <DT 2460, 28-Sep-2012> - Start
        //Added If-condition by Pankaj Dikshit for <DT 2823, 19-Feb-2013>
        if (IsWAM)
            foreach (DataRow _dr in dsWAM.Tables[0].Rows) drYtdSummary["KpiScoreYtd-WAM"] = _dr["LWDFR"].ToString();
        //End
        foreach (DataRow _dr in dsVICOPS.Tables[0].Rows) drYtdSummary["KpiScoreYtd-VIC"] = _dr["LWDFR"].ToString();
        foreach (var siteVIC in sitesVIC)
        {
            //TList<CompanySiteCategoryStandard> cscs = cscsService.GetBySiteId(siteVIC.SiteId);
            var cscs = companySiteCategoryStandardsVICGroupCount.Where(e => System.Convert.ToInt32(e.Name) == siteVIC.SiteId).FirstOrDefault();

            if (cscs != null)
            {
                if (cscs.Count > 0)
                {
                    DataSet _ds = kcscService.Compliance_Table_AS(siteVIC.SiteId, YearId, "%");
                    foreach (DataRow _dr2 in _ds.Tables[0].Rows) drYtdSummary[String.Format("KpiScoreYtd{0}", siteVIC.SiteAbbrev)] = _dr2["LWDFR"].ToString();
                }
            }
        }
        dtYtdSummary.Rows.Add(drYtdSummary);
        drYtdSummary = dtYtdSummary.NewRow();

        //TRIFR
        drYtdSummary["KpiMeasure"] = "TRIFR";
        foreach (DataRow _dr in dsWAO.Tables[0].Rows) drYtdSummary["KpiScoreYtd-WAO"] = _dr["TRIFR"].ToString();
        foreach (var siteWAO in sitesWAO)
        {
            //TList<CompanySiteCategoryStandard> cscs = cscsService.GetBySiteId(siteWAO.SiteId);
            var cscs = companySiteCategoryStandardsWAOGroupCount.Where(e => System.Convert.ToInt32(e.Name) == siteWAO.SiteId).FirstOrDefault();

            if (cscs != null)
            {
                if (cscs.Count > 0)
                {
                    DataSet _ds = kcscService.Compliance_Table_AS(siteWAO.SiteId, YearId, "%");
                    foreach (DataRow _dr2 in _ds.Tables[0].Rows) drYtdSummary[String.Format("KpiScoreYtd{0}", siteWAO.SiteAbbrev)] = _dr2["TRIFR"].ToString();
                }
            }
        }

        ////Added by Vikas Naidu for <DT 2460, 28-Sep-2012> - Start
        //Added If-condition by Pankaj Dikshit for <DT 2823, 19-Feb-2013>
        if (IsWAM)
            foreach (DataRow _dr in dsWAM.Tables[0].Rows) drYtdSummary["KpiScoreYtd-WAM"] = _dr["TRIFR"].ToString();
        //End
        foreach (DataRow _dr in dsVICOPS.Tables[0].Rows) drYtdSummary["KpiScoreYtd-VIC"] = _dr["TRIFR"].ToString();

        foreach (var siteVIC in sitesVIC)
        {
            //TList<CompanySiteCategoryStandard> cscs = cscsService.GetBySiteId(siteVIC.SiteId);
            var cscs = companySiteCategoryStandardsVICGroupCount.Where(e => System.Convert.ToInt32(e.Name) == siteVIC.SiteId).FirstOrDefault();

            if (cscs != null)
            {
                if (cscs.Count > 0)
                {
                    DataSet _ds = kcscService.Compliance_Table_AS(siteVIC.SiteId, YearId, "%");
                    foreach (DataRow _dr2 in _ds.Tables[0].Rows) drYtdSummary[String.Format("KpiScoreYtd{0}", siteVIC.SiteAbbrev)] = _dr2["TRIFR"].ToString();
                }
            }
        }
        dtYtdSummary.Rows.Add(drYtdSummary);
        drYtdSummary = dtYtdSummary.NewRow();
        //AIFR
        drYtdSummary["KpiMeasure"] = "AIFR";
        foreach (DataRow _dr in dsWAO.Tables[0].Rows) drYtdSummary["KpiScoreYtd-WAO"] = _dr["AIFR"].ToString();
        foreach (var siteWAO in sitesWAO)
        {
            //TList<CompanySiteCategoryStandard> cscs = cscsService.GetBySiteId(siteWAO.SiteId);
            var cscs = companySiteCategoryStandardsWAOGroupCount.Where(e => System.Convert.ToInt32(e.Name) == siteWAO.SiteId).FirstOrDefault();

            if (cscs != null)
            {
                if (cscs.Count > 0)
                {
                    DataSet _ds = kcscService.Compliance_Table_AS(siteWAO.SiteId, YearId, "%");
                    foreach (DataRow _dr2 in _ds.Tables[0].Rows) drYtdSummary[String.Format("KpiScoreYtd{0}", siteWAO.SiteAbbrev)] = _dr2["AIFR"].ToString();
                }
            }
        }
        ////Added by Vikas Naidu for <DT 2460, 28-Sep-2012> - Start
        //Added If-condition by Pankaj Dikshit for <DT 2823, 19-Feb-2013>
        if (IsWAM)
            foreach (DataRow _dr in dsWAM.Tables[0].Rows) drYtdSummary["KpiScoreYtd-WAM"] = _dr["AIFR"].ToString();
        //End
        foreach (DataRow _dr in dsVICOPS.Tables[0].Rows) drYtdSummary["KpiScoreYtd-VIC"] = _dr["AIFR"].ToString();
        foreach (var siteVIC in sitesVIC)
        {
            //TList<CompanySiteCategoryStandard> cscs = cscsService.GetBySiteId(siteVIC.SiteId);
            var cscs = companySiteCategoryStandardsVICGroupCount.Where(e => System.Convert.ToInt32(e.Name) == siteVIC.SiteId).FirstOrDefault();

            if (cscs != null)
            {
                if (cscs.Count > 0)
                {
                    DataSet _ds = kcscService.Compliance_Table_AS(siteVIC.SiteId, YearId, "%");
                    foreach (DataRow _dr2 in _ds.Tables[0].Rows) drYtdSummary[String.Format("KpiScoreYtd{0}", siteVIC.SiteAbbrev)] = _dr2["AIFR"].ToString();
                }
            }
        }
        dtYtdSummary.Rows.Add(drYtdSummary);
        drYtdSummary = dtYtdSummary.NewRow();
        //IFE : Injury Ratio
        drYtdSummary["KpiMeasure"] = "IFE : Injury Ratio";
        foreach (DataRow _dr in dsWAO.Tables[0].Rows) drYtdSummary["KpiScoreYtd-WAO"] = _dr["IFEInjuryRatio"].ToString();
        foreach (var siteWAO in sitesWAO)
        {
            //TList<CompanySiteCategoryStandard> cscs = cscsService.GetBySiteId(siteWAO.SiteId);
            var cscs = companySiteCategoryStandardsWAOGroupCount.Where(e => System.Convert.ToInt32(e.Name) == siteWAO.SiteId).FirstOrDefault();

            if (cscs != null)
            {
                if (cscs.Count > 0)
                {
                    DataSet _ds = kcscService.Compliance_Table_AS(siteWAO.SiteId, YearId, "%");
                    foreach (DataRow _dr2 in _ds.Tables[0].Rows) drYtdSummary[String.Format("KpiScoreYtd{0}", siteWAO.SiteAbbrev)] = _dr2["IFEInjuryRatio"].ToString();
                }
            }
        }
        //Added by Vikas Naidu for <DT 2460, 28-Sep-2012> - Start
        //Added If-condition by Pankaj Dikshit for <DT 2823, 19-Feb-2013>
        if (IsWAM)
            foreach (DataRow _dr in dsWAM.Tables[0].Rows) drYtdSummary["KpiScoreYtd-WAM"] = _dr["IFEInjuryRatio"].ToString();
        //End
        foreach (DataRow _dr in dsVICOPS.Tables[0].Rows) drYtdSummary["KpiScoreYtd-VIC"] = _dr["IFEInjuryRatio"].ToString();
        foreach (var siteVIC in sitesVIC)
        {
            //TList<CompanySiteCategoryStandard> cscs = cscsService.GetBySiteId(siteVIC.SiteId);
            var cscs = companySiteCategoryStandardsVICGroupCount.Where(e => System.Convert.ToInt32(e.Name) == siteVIC.SiteId).FirstOrDefault();

            if (cscs != null)
            {
                if (cscs.Count > 0)
                {
                    DataSet _ds = kcscService.Compliance_Table_AS(siteVIC.SiteId, YearId, "%");
                    foreach (DataRow _dr2 in _ds.Tables[0].Rows) drYtdSummary[String.Format("KpiScoreYtd{0}", siteVIC.SiteAbbrev)] = _dr2["IFEInjuryRatio"].ToString();
                }
            }
        }
        dtYtdSummary.Rows.Add(drYtdSummary);
        drYtdSummary = dtYtdSummary.NewRow();
        //Adding by Pankaj Dikshit for <DT 2459, 14-Aug-2012> - Start
        //DART
        drYtdSummary["KpiMeasure"] = "DART";
        foreach (DataRow _dr in dsWAO.Tables[0].Rows) drYtdSummary["KpiScoreYtd-WAO"] = _dr["DART"].ToString();
        foreach (var siteWAO in sitesWAO)
        {
            //TList<CompanySiteCategoryStandard> cscs = cscsService.GetBySiteId(siteWAO.SiteId);
            var cscs = companySiteCategoryStandardsWAOGroupCount.Where(e => System.Convert.ToInt32(e.Name) == siteWAO.SiteId).FirstOrDefault();

            if (cscs != null)
            {
                if (cscs.Count > 0)
                {
                    DataSet _ds = kcscService.Compliance_Table_AS(siteWAO.SiteId, YearId, "%");
                    foreach (DataRow _dr2 in _ds.Tables[0].Rows) drYtdSummary[String.Format("KpiScoreYtd{0}", siteWAO.SiteAbbrev)] = _dr2["DART"].ToString();
                }
            }
        }
        //Added by Vikas Naidu for <DT 2460, 28-Sep-2012> - Start
        //Added If-condition by Pankaj Dikshit for <DT 2823, 19-Feb-2013>
        if (IsWAM)
            foreach (DataRow _dr in dsWAM.Tables[0].Rows) drYtdSummary["KpiScoreYtd-WAM"] = _dr["DART"].ToString();
        //End
        foreach (DataRow _dr in dsVICOPS.Tables[0].Rows) drYtdSummary["KpiScoreYtd-VIC"] = _dr["DART"].ToString();
        foreach (var siteVIC in sitesVIC)
        {
            //TList<CompanySiteCategoryStandard> cscs = cscsService.GetBySiteId(siteVIC.SiteId);
            var cscs = companySiteCategoryStandardsVICGroupCount.Where(e => System.Convert.ToInt32(e.Name) == siteVIC.SiteId).FirstOrDefault();

            if (cscs != null)
            {
                if (cscs.Count > 0)
                {
                    DataSet _ds = kcscService.Compliance_Table_AS(siteVIC.SiteId, YearId, "%");
                    foreach (DataRow _dr2 in _ds.Tables[0].Rows) drYtdSummary[String.Format("KpiScoreYtd{0}", siteVIC.SiteAbbrev)] = _dr2["DART"].ToString();
                }
            }
        }

        dtYtdSummary.Rows.Add(drYtdSummary);
        drYtdSummary = dtYtdSummary.NewRow();

        //Added By Sayani Sil
        //RN

        drYtdSummary["KpiMeasure"] = "Risk Notifications";
        foreach (DataRow _dr in dsWAO.Tables[0].Rows) drYtdSummary["KpiScoreYtd-WAO"] = _dr["Risk Notifications"].ToString();
        foreach (var siteWAO in sitesWAO)
        {
            //TList<CompanySiteCategoryStandard> cscs = cscsService.GetBySiteId(siteWAO.SiteId);
            var cscs = companySiteCategoryStandardsWAOGroupCount.Where(e => System.Convert.ToInt32(e.Name) == siteWAO.SiteId).FirstOrDefault();

            if (cscs != null)
            {
                if (cscs.Count > 0)
                {
                    DataSet _ds = kcscService.Compliance_Table_AS(siteWAO.SiteId, YearId, "%");
                    foreach (DataRow _dr2 in _ds.Tables[0].Rows) drYtdSummary[String.Format("KpiScoreYtd{0}", siteWAO.SiteAbbrev)] = _dr2["Risk Notifications"].ToString();
                }
            }
        }
        //Added If-condition by Pankaj Dikshit for <DT 2823, 19-Feb-2013>
        if (IsWAM)
            foreach (DataRow _dr in dsWAM.Tables[0].Rows) drYtdSummary["KpiScoreYtd-WAM"] = _dr["Risk Notifications"].ToString();

        foreach (DataRow _dr in dsVICOPS.Tables[0].Rows) drYtdSummary["KpiScoreYtd-VIC"] = _dr["Risk Notifications"].ToString();
        foreach (var siteVIC in sitesVIC)
        {
            //TList<CompanySiteCategoryStandard> cscs = cscsService.GetBySiteId(siteVIC.SiteId);
            var cscs = companySiteCategoryStandardsVICGroupCount.Where(e => System.Convert.ToInt32(e.Name) == siteVIC.SiteId).FirstOrDefault();

            if (cscs != null)
            {
                if (cscs.Count > 0)
                {
                    DataSet _ds = kcscService.Compliance_Table_AS(siteVIC.SiteId, YearId, "%");
                    foreach (DataRow _dr2 in _ds.Tables[0].Rows) drYtdSummary[String.Format("KpiScoreYtd{0}", siteVIC.SiteAbbrev)] = _dr2["Risk Notifications"].ToString();
                }
            }
        }

        //End of Addition
        dtYtdSummary.Rows.Add(drYtdSummary);
        Session["Radar_Table0"] = dtYtdSummary;
    }

    protected void ShowSafetyStatsAll2(int SiteId, int YearId, int MonthId)
    {
        var storedProcedureService = ALCOA.CSMS.Website.Global.GetInstance<Repo.CSMS.Service.Database.IStoredProcedureService>();
        var companySiteCategoryStandardService = ALCOA.CSMS.Website.Global.GetInstance<Repo.CSMS.Service.Database.ICompanySiteCategoryStandardService>();
        var adhocRadarItemService = ALCOA.CSMS.Website.Global.GetInstance<Repo.CSMS.Service.Database.IAdhocRadarItemService>();



        //var dsWAO = storedProcedureService.sp_
        var dsWAO = storedProcedureService.SP_Compliance_Table_AA(1, YearId, "%");
        var dsVICOPS = storedProcedureService.SP_Compliance_Table_AA(4, YearId, "%");
        ////Added by Vikas Naidu for <DT 2460, 28-Sep-2012> - Start
        var dsWAM = storedProcedureService.SP_Compliance_Table_AA(3, YearId, "%");
        //End


        DataTable dtYtdSummary = new DataTable();

        #region Create Grid Columns & DataTable Columns
        int grid_VisibleIndex = 0;
        grid_All.Width = Unit.Percentage(100);
        grid_All.KeyFieldName = "KpiMeasure";
        //Changed by Pankaj Dikshit for <DT 2399, 06-Aug-2012>
        grid_VisibleIndex = Helper.Radar.AddGridColumn(grid_All, "KpiMeasure", "Key Performance Indicators", "Safety Rates &nbsp;&nbsp;&nbsp;&nbsp;", 300, grid_VisibleIndex);
        dtYtdSummary.Columns.Add("KpiMeasure", typeof(string));

        string FieldName = "-WAO";
        string SiteName = "WAO";
        grid_VisibleIndex = Helper.Radar.AddGridColumn(grid_All, String.Format("KpiScoreYtd{0}", FieldName), SiteName, SiteName, 200, grid_VisibleIndex);
        dtYtdSummary.Columns.Add(String.Format("KpiScoreYtd{0}", "-WAO", typeof(string)));

        SitesService sService = new SitesService();
        RegionsSitesService rService = new RegionsSitesService();
        CompanySiteCategoryStandardService cscsService = new CompanySiteCategoryStandardService();


        var sitesWAO = storedProcedureService.SP_CompanySiteCategoryStandard_ByRegionId(1);
        var sitesVIC = storedProcedureService.SP_CompanySiteCategoryStandard_ByRegionId(4);
        //////Added by Vikas Naidu for <DT 2460, 28-Sep-2012> - Start
        var sitesMines = storedProcedureService.SP_CompanySiteCategoryStandard_ByRegionId(3);
        //End


        ////Adding by Pankaj Dikshit for <DT 2823, 19-Feb-2013> - Start
        DataSet dsGridCols = sService.GetByYearAdHocRadarId(YearId, '%');
        DataRow[] drTmp = dsGridCols.Tables[0].Select("SiteAbbrev = '-AM'");
        ////Adding by Pankaj Dikshit for <DT 2823, 19-Feb-2013> - End
        bool site = false;
        bool IsWAM = false; //<DT 2823, 19-Feb-2013>
        ////Added If-condition by Pankaj Dikshit for <DT 2823, 19-Feb-2013>
        //if (drTmp.Length > 0)
        //{
        //    FieldName = "-WAM";
        //    SiteName = "WAM";
        //    grid_VisibleIndex = Helper.Radar.AddGridColumn(grid_All, String.Format("KpiScoreYtd{0}", FieldName), SiteName, SiteName, 200, grid_VisibleIndex);
        //    dtYtdSummary.Columns.Add(String.Format("KpiScoreYtd{0}", "-WAM", typeof(string)));
        //    IsWAM = true;
        //}

        var siteIdWAOList = sitesWAO.Select(e => e.SiteId).ToList();
        var companySiteCategoryStandardsWAOGroupCount = companySiteCategoryStandardService.GetGroupCount(e => siteIdWAOList.Contains(e.SiteId), e => e.SiteId);

        var siteIdVICList = sitesVIC.Select(e => e.SiteId).ToList();
        var companySiteCategoryStandardsVICGroupCount = companySiteCategoryStandardService.GetGroupCount(e => siteIdVICList.Contains(e.SiteId), e => e.SiteId);

        foreach (var siteWAO in sitesWAO)
        {
            //TList<CompanySiteCategoryStandard> cscs = cscsService.GetBySiteId(siteWAO.SiteId);
            var cscs = companySiteCategoryStandardsWAOGroupCount.Where(e => System.Convert.ToInt32(e.Name) == siteWAO.SiteId).FirstOrDefault();

            //Added by Vikas Naidu for <DT 2460, 28-Sep-2012> - Start
            if (sitesMines[0].SiteId == siteWAO.SiteId)
            {
                //Added If-condition by Pankaj Dikshit for <DT 2823, 19-Feb-2013>
                if (drTmp.Length > 0)
                {
                    FieldName = "-WAM";
                    SiteName = "WAM";
                    grid_VisibleIndex = Helper.Radar.AddGridColumn(grid_All, String.Format("KpiScoreYtd{0}", FieldName), SiteName, SiteName, 200, grid_VisibleIndex);
                    dtYtdSummary.Columns.Add(String.Format("KpiScoreYtd{0}", "-WAM", typeof(string)));
                    IsWAM = true;
                }
            }
            //End
            //Adding by Pankaj Dikshit for <DT 2823, 19-Feb-2013>
            DataRow[] drTemp = dsGridCols.Tables[0].Select("SiteId = " + siteWAO.SiteId);

            if (cscs != null && drTemp.Length > 0)
            {
                if (cscs.Count > 0)
                {
                    site = true;

                    SiteName = siteWAO.SiteName; //SiteName
                    if (SiteName.Length > 1) SiteName = siteWAO.SiteAbbrev; //SiteAbbrev

                    grid_VisibleIndex = Helper.Radar.AddGridColumn(grid_All, String.Format("KpiScoreYtd{0}", SiteName), SiteName, SiteName, 100, grid_VisibleIndex);
                    dtYtdSummary.Columns.Add(String.Format("KpiScoreYtd{0}", SiteName, typeof(string)));

                }
            }
        }


        FieldName = "-VIC";
        SiteName = "VIC";
        grid_VisibleIndex = Helper.Radar.AddGridColumn(grid_All, String.Format("KpiScoreYtd{0}", FieldName), SiteName, SiteName, 200, grid_VisibleIndex);
        dtYtdSummary.Columns.Add(String.Format("KpiScoreYtd{0}", "-VIC", typeof(string)));

        foreach (var siteVIC in sitesVIC)
        {
            //TList<CompanySiteCategoryStandard> cscs = cscsService.GetBySiteId(siteVIC.SiteId);
            var cscs = companySiteCategoryStandardsVICGroupCount.Where(e => System.Convert.ToInt32(e.Name) == siteVIC.SiteId).FirstOrDefault();

            if (cscs != null)
            {
                if (cscs.Count > 0)
                {
                    site = true;

                    SiteName = siteVIC.SiteName; //SiteName
                    if (SiteName.Length > 1) SiteName = siteVIC.SiteAbbrev; //SiteAbbrev

                    grid_VisibleIndex = Helper.Radar.AddGridColumn(grid_All, String.Format("KpiScoreYtd{0}", SiteName), SiteName, SiteName, 100, grid_VisibleIndex);
                    dtYtdSummary.Columns.Add(String.Format("KpiScoreYtd{0}", SiteName, typeof(string)));

                }
            }
        }
        #endregion

        var waoSiteIdList = sitesWAO.Select(e => e.SiteId).ToList();
        var waoAhriList = adhocRadarItemService.GetMany(null, e => e.Year == YearId && waoSiteIdList.Contains(e.SiteId), null, new List<System.Linq.Expressions.Expression<Func<Repo.CSMS.DAL.EntityModels.AdHoc_Radar_Items, object>>>() { e => e.AdHoc_Radar });

        var vicopsSiteIdList = sitesVIC.Select(e => e.SiteId).ToList();
        var vicopsAhriList = adhocRadarItemService.GetMany(null, e => e.Year == YearId && vicopsSiteIdList.Contains(e.SiteId), null, new List<System.Linq.Expressions.Expression<Func<Repo.CSMS.DAL.EntityModels.AdHoc_Radar_Items, object>>>() { e => e.AdHoc_Radar });

        DataRow drYtdSummary = dtYtdSummary.NewRow();

        //LWDFR
        drYtdSummary["KpiMeasure"] = "LWDFR";

        if (MonthId == 0) //DT419 22/1/16 Cindi Thornton - only do this for YTD, need to calculate months
            foreach (var _dr in dsWAO) drYtdSummary["KpiScoreYtd-WAO"] = _dr.LWDFR.ToString("N2");
        else
            drYtdSummary["KpiScoreYtd-WAO"] = GetRegionKPI(adhocRadarItemService, "LWDFR", YearId, MonthId, 20); //20 == WAO

        foreach (var siteWAO in sitesWAO)
        {
            //TList<CompanySiteCategoryStandard> cscs = cscsService.GetBySiteId(siteWAO.SiteId);
            var cscs = companySiteCategoryStandardsWAOGroupCount.Where(e => System.Convert.ToInt32(e.Name) == siteWAO.SiteId).FirstOrDefault();

            if (cscs != null)
            {
                if (cscs.Count > 0)
                {
                    decimal? totalSum = null;
                    var _ds = waoAhriList.Where(e => e.SiteId == siteWAO.SiteId && e.Year == YearId && e.AdHoc_Radar.KpiName == "LWDFR");
                    foreach (var _dr2 in _ds)
                    {
                        decimal? score = _dr2.KpiScore(MonthId);
                        if (score != null)
                            totalSum = (totalSum ?? 0) + score.Value;
                        else
                        {
                            totalSum = null;
                            break;
                        }
                    }
                    try
                    {
                        drYtdSummary[String.Format("KpiScoreYtd{0}", siteWAO.SiteAbbrev)] = (totalSum != null ? totalSum.Value.ToString("N2") : "0.00");
                    }
                    catch { }
                }
            }
        }
        //Added by Vikas Naidu for <DT 2460, 28-Sep-2012> - Start
        //Added If-condition by Pankaj Dikshit for <DT 2823, 19-Feb-2013>
        if (IsWAM)
            if (MonthId == 0) //DT419 22/1/16 Cindi Thornton - only do this for YTD, need to calculate months
                foreach (var _dr in dsWAM) drYtdSummary["KpiScoreYtd-WAM"] = _dr.LWDFR.ToString("N2");
            else
                drYtdSummary["KpiScoreYtd-WAM"] = GetRegionKPI(adhocRadarItemService, "LWDFR", YearId, MonthId, 21); //21 == WAM
        //End

        if (MonthId == 0) //DT419 22/1/16 Cindi Thornton - only do this for YTD, need to calculate months
            foreach (var _dr in dsVICOPS) drYtdSummary["KpiScoreYtd-VIC"] = _dr.LWDFR.ToString("N2");
        else
            drYtdSummary["KpiScoreYtd-VIC"] = GetRegionKPI(adhocRadarItemService, "LWDFR", YearId, MonthId, 22); //22 == VIC

        foreach (var siteVIC in sitesVIC)
        {
            //TList<CompanySiteCategoryStandard> cscs = cscsService.GetBySiteId(siteVIC.SiteId);
            var cscs = companySiteCategoryStandardsVICGroupCount.Where(e => System.Convert.ToInt32(e.Name) == siteVIC.SiteId).FirstOrDefault();

            if (cscs != null)
            {
                if (cscs.Count > 0)
                {
                    decimal? totalSum = null;
                    var _ds = vicopsAhriList.Where(e => e.SiteId == siteVIC.SiteId && e.Year == YearId && e.AdHoc_Radar.KpiName == "LWDFR");
                    foreach (var _dr2 in _ds)
                    {
                        decimal? score = _dr2.KpiScore(MonthId);
                        if (score != null)
                            totalSum = (totalSum ?? 0) + score.Value;
                        else
                        {
                            totalSum = null;
                            break;
                        }
                    }
                    try
                    {
                        drYtdSummary[String.Format("KpiScoreYtd{0}", siteVIC.SiteAbbrev)] = (totalSum != null ? totalSum.Value.ToString("N2") : "0.00"); //Changed from n/a to 0.00 AG
                    }
                    catch { }
                }
            }
        }
        dtYtdSummary.Rows.Add(drYtdSummary);
        drYtdSummary = dtYtdSummary.NewRow();

        //TRIFR
        drYtdSummary["KpiMeasure"] = "TRIFR";

        if (MonthId == 0) //DT419 22/1/16 Cindi Thornton - only do this for YTD, need to calculate months
            foreach (var _dr in dsWAO) drYtdSummary["KpiScoreYtd-WAO"] = _dr.TRIFR.ToString("N2");
        else
            drYtdSummary["KpiScoreYtd-WAO"] = GetRegionKPI(adhocRadarItemService, "TRIFR", YearId, MonthId, 20); //20 == WAO


        foreach (var siteWAO in sitesWAO)
        {
            //TList<CompanySiteCategoryStandard> cscs = cscsService.GetBySiteId(siteWAO.SiteId);
            var cscs = companySiteCategoryStandardsWAOGroupCount.Where(e => System.Convert.ToInt32(e.Name) == siteWAO.SiteId).FirstOrDefault();

            if (cscs != null)
            {
                if (cscs.Count > 0)
                {
                    decimal? totalSum = null;
                    var _ds = waoAhriList.Where(e => e.SiteId == siteWAO.SiteId && e.Year == YearId && e.AdHoc_Radar.KpiName == "TRIFR");
                    foreach (var _dr2 in _ds)
                    {
                        decimal? score = _dr2.KpiScore(MonthId);
                        if (score != null)
                            totalSum = (totalSum ?? 0) + score.Value;
                        else
                        {
                            totalSum = null;
                            break;
                        }
                    }
                    try
                    {
                        drYtdSummary[String.Format("KpiScoreYtd{0}", siteWAO.SiteAbbrev)] = (totalSum != null ? totalSum.Value.ToString("N2") : "0.00"); //Changed from n/a to 0.00 AG
                    }
                    catch { }
                }
            }
        }

        ////Added by Vikas Naidu for <DT 2460, 28-Sep-2012> - Start
        //Added If-condition by Pankaj Dikshit for <DT 2823, 19-Feb-2013>
        if (IsWAM)
            if (MonthId == 0) //DT419 22/1/16 Cindi Thornton - only do this for YTD, need to calculate months
                foreach (var _dr in dsWAM) drYtdSummary["KpiScoreYtd-WAM"] = _dr.TRIFR.ToString("N2");
            else
                drYtdSummary["KpiScoreYtd-WAM"] = GetRegionKPI(adhocRadarItemService, "TRIFR", YearId, MonthId, 21); //21 == WAM
        //End

        if (MonthId == 0) //DT419 22/1/16 Cindi Thornton - only do this for YTD, need to calculate months
            foreach (var _dr in dsVICOPS) drYtdSummary["KpiScoreYtd-VIC"] = _dr.TRIFR.ToString("N2");
        else
            drYtdSummary["KpiScoreYtd-VIC"] = GetRegionKPI(adhocRadarItemService, "TRIFR", YearId, MonthId, 22); //22 == VIC
        

        foreach (var siteVIC in sitesVIC)
        {
            //TList<CompanySiteCategoryStandard> cscs = cscsService.GetBySiteId(siteVIC.SiteId);
            var cscs = companySiteCategoryStandardsVICGroupCount.Where(e => System.Convert.ToInt32(e.Name) == siteVIC.SiteId).FirstOrDefault();

            if (cscs != null)
            {
                if (cscs.Count > 0)
                {
                    decimal? totalSum = null;
                    var _ds = vicopsAhriList.Where(e => e.SiteId == siteVIC.SiteId && e.Year == YearId && e.AdHoc_Radar.KpiName == "TRIFR");
                    foreach (var _dr2 in _ds)
                    {
                        decimal? score = _dr2.KpiScore(MonthId);
                        if (score != null)
                            totalSum = (totalSum ?? 0) + score.Value;
                        else
                        {
                            totalSum = null;
                            break;
                        }
                    }
                    try
                    {
                        drYtdSummary[String.Format("KpiScoreYtd{0}", siteVIC.SiteAbbrev)] = (totalSum != null ? totalSum.Value.ToString("N2") : "0.00"); //Changed from n/a to 0.00 AG
                    }
                    catch { }
                }
            }
        }
        dtYtdSummary.Rows.Add(drYtdSummary);
        drYtdSummary = dtYtdSummary.NewRow();
        //AIFR
        drYtdSummary["KpiMeasure"] = "AIFR";
        if (MonthId == 0) //DT419 22/1/16 Cindi Thornton - only do this for YTD, need to calculate months
            foreach (var _dr in dsWAO) drYtdSummary["KpiScoreYtd-WAO"] = _dr.AIFR.ToString("N2");
        else
            drYtdSummary["KpiScoreYtd-WAO"] = GetRegionKPI(adhocRadarItemService, "AIFR", YearId, MonthId, 20); //20 == WAO


        foreach (var siteWAO in sitesWAO)
        {
            //TList<CompanySiteCategoryStandard> cscs = cscsService.GetBySiteId(siteWAO.SiteId);
            var cscs = companySiteCategoryStandardsWAOGroupCount.Where(e => System.Convert.ToInt32(e.Name) == siteWAO.SiteId).FirstOrDefault();

            if (cscs != null)
            {
                if (cscs.Count > 0)
                {
                    decimal? totalSum = null;
                    var _ds = waoAhriList.Where(e => e.SiteId == siteWAO.SiteId && e.Year == YearId && e.AdHoc_Radar.KpiName == "AIFR");
                    foreach (var _dr2 in _ds)
                    {
                        decimal? score = _dr2.KpiScore(MonthId);
                        if (score != null)
                            totalSum = (totalSum ?? 0) + score.Value;
                        else
                        {
                            totalSum = null;
                            break;
                        }
                    }
                    try
                    {
                        drYtdSummary[String.Format("KpiScoreYtd{0}", siteWAO.SiteAbbrev)] = (totalSum != null ? totalSum.Value.ToString("N2") : "0.00");//Changed from n/a to 0.00 AG
                    }
                    catch { }
                }
            }
        }
        ////Added by Vikas Naidu for <DT 2460, 28-Sep-2012> - Start
        //Added If-condition by Pankaj Dikshit for <DT 2823, 19-Feb-2013>
        if (IsWAM)
            if (MonthId == 0) //DT419 22/1/16 Cindi Thornton - only do this for YTD, need to calculate months
                foreach (var _dr in dsWAM) drYtdSummary["KpiScoreYtd-WAM"] = _dr.AIFR.ToString("N2");
            else
                drYtdSummary["KpiScoreYtd-WAM"] = GetRegionKPI(adhocRadarItemService, "AIFR", YearId, MonthId, 21); //21 == WAM
           
        //End

        if (MonthId == 0) //DT419 22/1/16 Cindi Thornton - only do this for YTD, need to calculate months
            foreach (var _dr in dsVICOPS) drYtdSummary["KpiScoreYtd-VIC"] = _dr.AIFR.ToString("N2");
        else
            drYtdSummary["KpiScoreYtd-VIC"] = GetRegionKPI(adhocRadarItemService, "AIFR", YearId, MonthId, 22); //22 == VIC

        foreach (var siteVIC in sitesVIC)
        {
            //TList<CompanySiteCategoryStandard> cscs = cscsService.GetBySiteId(siteVIC.SiteId);
            var cscs = companySiteCategoryStandardsVICGroupCount.Where(e => System.Convert.ToInt32(e.Name) == siteVIC.SiteId).FirstOrDefault();

            if (cscs != null)
            {
                if (cscs.Count > 0)
                {
                    decimal? totalSum = null;
                    var _ds = vicopsAhriList.Where(e => e.SiteId == siteVIC.SiteId && e.Year == YearId && e.AdHoc_Radar.KpiName == "AIFR");
                    foreach (var _dr2 in _ds)
                    {
                        decimal? score = _dr2.KpiScore(MonthId);
                        if (score != null)
                            totalSum = (totalSum ?? 0) + score.Value;
                        else
                        {
                            totalSum = null;
                            break;
                        }
                    }
                    try
                    {
                        drYtdSummary[String.Format("KpiScoreYtd{0}", siteVIC.SiteAbbrev)] = (totalSum != null ? totalSum.Value.ToString("N2") : "0.00");//Changed from n/a to 0.00 AG
                    }
                    catch { }
                }
            }
        }
        dtYtdSummary.Rows.Add(drYtdSummary);
        drYtdSummary = dtYtdSummary.NewRow();
        //IFE : Injury Ratio
        drYtdSummary["KpiMeasure"] = "IFE : Injury Ratio";

        if (MonthId == 0) //DT419 22/1/16 Cindi Thornton - only do this for YTD, need to calculate months
            foreach (var _dr in dsWAO) drYtdSummary["KpiScoreYtd-WAO"] = (_dr.IFEInjuryRatio != null ? _dr.IFEInjuryRatio.Value.ToString("N2") : "0.00");//Changed from n/a to 0.00 AG
        else
            drYtdSummary["KpiScoreYtd-WAO"] = GetRegionKPI(adhocRadarItemService, "IFE : Injury Ratio", YearId, MonthId, 20); //20 == WAO


        foreach (var siteWAO in sitesWAO)
        {
            //TList<CompanySiteCategoryStandard> cscs = cscsService.GetBySiteId(siteWAO.SiteId);
            var cscs = companySiteCategoryStandardsWAOGroupCount.Where(e => System.Convert.ToInt32(e.Name) == siteWAO.SiteId).FirstOrDefault();

            if (cscs != null)
            {
                if (cscs.Count > 0)
                {
                    decimal? totalSum = null;
                    var _ds = waoAhriList.Where(e => e.SiteId == siteWAO.SiteId && e.Year == YearId && e.AdHoc_Radar.KpiName == "IFE : Injury Ratio");
                    foreach (var _dr2 in _ds)
                    {
                        decimal? score = _dr2.KpiScore(MonthId);
                        if (score != null)
                            totalSum = (totalSum ?? 0) + score.Value;
                        else
                        {
                            totalSum = null;
                            break;
                        }
                    }
                    try
                    {
                        drYtdSummary[String.Format("KpiScoreYtd{0}", siteWAO.SiteAbbrev)] = (totalSum != null ? totalSum.Value.ToString("N2") : "0.00"); //Changed rom n/a to 0.00 AG
                    }
                    catch { }
                }
            }
        }
        //Added by Vikas Naidu for <DT 2460, 28-Sep-2012> - Start
        //Added If-condition by Pankaj Dikshit for <DT 2823, 19-Feb-2013>
        if (IsWAM)
            if (MonthId == 0) //DT419 22/1/16 Cindi Thornton - only do this for YTD, need to calculate months
                foreach (var _dr in dsWAM) drYtdSummary["KpiScoreYtd-WAM"] = (_dr.IFEInjuryRatio != null ? _dr.IFEInjuryRatio.Value.ToString("N2") : "0.00");//Changed rom n/a to 0.00 AG
            else
                drYtdSummary["KpiScoreYtd-WAM"] = GetRegionKPI(adhocRadarItemService, "IFE : Injury Ratio", YearId, MonthId, 21); //21 == WAM
            
        //End

        if (MonthId == 0) //DT419 22/1/16 Cindi Thornton - only do this for YTD, need to calculate months
            foreach (var _dr in dsVICOPS) drYtdSummary["KpiScoreYtd-VIC"] = (_dr.IFEInjuryRatio != null ? _dr.IFEInjuryRatio.Value.ToString("N2") : "0.00");//Changed rom n/a to 0.00 AG
        else
            drYtdSummary["KpiScoreYtd-VIC"] = GetRegionKPI(adhocRadarItemService, "IFE : Injury Ratio", YearId, MonthId, 22); //22 == VIC

        foreach (var siteVIC in sitesVIC)
        {
            //TList<CompanySiteCategoryStandard> cscs = cscsService.GetBySiteId(siteVIC.SiteId);
            var cscs = companySiteCategoryStandardsVICGroupCount.Where(e => System.Convert.ToInt32(e.Name) == siteVIC.SiteId).FirstOrDefault();

            if (cscs != null)
            {
                if (cscs.Count > 0)
                {
                    decimal? totalSum = null;
                    var _ds = vicopsAhriList.Where(e => e.SiteId == siteVIC.SiteId && e.Year == YearId && e.AdHoc_Radar.KpiName == "IFE : Injury Ratio");
                    foreach (var _dr2 in _ds)
                    {
                        decimal? score = _dr2.KpiScore(MonthId);
                        if (score != null)
                            totalSum = (totalSum ?? 0) + score.Value;
                        else
                        {
                            totalSum = null;
                            break;
                        }
                    }
                    try
                    {
                        drYtdSummary[String.Format("KpiScoreYtd{0}", siteVIC.SiteAbbrev)] = (totalSum != null ? totalSum.Value.ToString("N2") : "0.00");//Changed rom n/a to 0.00 AG
                    }
                    catch { }
                }
            }
        }
        dtYtdSummary.Rows.Add(drYtdSummary);
        drYtdSummary = dtYtdSummary.NewRow();
        //Adding by Pankaj Dikshit for <DT 2459, 14-Aug-2012> - Start
        //DART
        drYtdSummary["KpiMeasure"] = "DART";

        if (MonthId == 0) //DT419 22/1/16 Cindi Thornton - only do this for YTD, need to calculate months
            foreach (var _dr in dsWAO) drYtdSummary["KpiScoreYtd-WAO"] = (_dr.DART != null ? _dr.DART.Value.ToString("N2") : "0.00");//Changed rom n/a to 0.00 AG
        else
            drYtdSummary["KpiScoreYtd-WAO"] = GetRegionKPI(adhocRadarItemService, "DART", YearId, MonthId, 20); //20 == WAO


        foreach (var siteWAO in sitesWAO)
        {
            //TList<CompanySiteCategoryStandard> cscs = cscsService.GetBySiteId(siteWAO.SiteId);
            var cscs = companySiteCategoryStandardsWAOGroupCount.Where(e => System.Convert.ToInt32(e.Name) == siteWAO.SiteId).FirstOrDefault();

            if (cscs != null)
            {
                if (cscs.Count > 0)
                {
                    decimal? totalSum = null;
                    var _ds = waoAhriList.Where(e => e.SiteId == siteWAO.SiteId && e.Year == YearId && e.AdHoc_Radar.KpiName == "DART");
                    foreach (var _dr2 in _ds)
                    {
                        decimal? score = _dr2.KpiScore(MonthId);
                        if (score != null)
                            totalSum = (totalSum ?? 0) + score.Value;
                        else
                        {
                            totalSum = null;
                            break;
                        }
                    }
                    try
                    {
                        drYtdSummary[String.Format("KpiScoreYtd{0}", siteWAO.SiteAbbrev)] = (totalSum != null ? totalSum.Value.ToString("N2") : "0.00");//Changed rom n/a to 0.00 AG
                    }
                    catch { }
                }
            }
        }
        //Added by Vikas Naidu for <DT 2460, 28-Sep-2012> - Start
        //Added If-condition by Pankaj Dikshit for <DT 2823, 19-Feb-2013>
        if (IsWAM)
            if (MonthId == 0) //DT419 22/1/16 Cindi Thornton - only do this for YTD, need to calculate months
                foreach (var _dr in dsWAM) drYtdSummary["KpiScoreYtd-WAM"] = (_dr.DART != null ? _dr.DART.Value.ToString("N2") : "0.00");//Changed rom n/a to 0.00 AG
            else
                drYtdSummary["KpiScoreYtd-WAM"] = GetRegionKPI(adhocRadarItemService, "DART", YearId, MonthId, 21); //21 == WAM
            
        //End
        
        if (MonthId == 0) //DT419 22/1/16 Cindi Thornton - only do this for YTD, need to calculate months
            foreach (var _dr in dsVICOPS) drYtdSummary["KpiScoreYtd-VIC"] = (_dr.DART != null ? _dr.DART.Value.ToString("N2") : "0.00");//Changed rom n/a to 0.00 AG
        else
            drYtdSummary["KpiScoreYtd-VIC"] = GetRegionKPI(adhocRadarItemService, "DART", YearId, MonthId, 22); //22 == VIC

        foreach (var siteVIC in sitesVIC)
        {
            //TList<CompanySiteCategoryStandard> cscs = cscsService.GetBySiteId(siteVIC.SiteId);
            var cscs = companySiteCategoryStandardsVICGroupCount.Where(e => System.Convert.ToInt32(e.Name) == siteVIC.SiteId).FirstOrDefault();

            if (cscs != null)
            {
                if (cscs.Count > 0)
                {
                    decimal? totalSum = null;
                    var _ds = vicopsAhriList.Where(e => e.SiteId == siteVIC.SiteId && e.Year == YearId && e.AdHoc_Radar.KpiName == "DART");
                    foreach (var _dr2 in _ds)
                    {
                        decimal? score = _dr2.KpiScore(MonthId);
                        if (score != null)
                            totalSum = (totalSum ?? 0) + score.Value;
                        else
                        {
                            totalSum = null;
                            break;
                        }
                    }
                    try
                    {
                        drYtdSummary[String.Format("KpiScoreYtd{0}", siteVIC.SiteAbbrev)] = (totalSum != null ? totalSum.Value.ToString("N2") : "0.00");//Changed rom n/a to 0.00 AG
                    }
                    catch { }
                }
            }
        }

        dtYtdSummary.Rows.Add(drYtdSummary);
        drYtdSummary = dtYtdSummary.NewRow();

        //Added By Sayani Sil
        //RN

        drYtdSummary["KpiMeasure"] = "Risk Notifications";


        if (MonthId == 0) //DT419 22/1/16 Cindi Thornton - only do this for YTD, need to calculate months
            foreach (var _dr in dsWAO) drYtdSummary["KpiScoreYtd-WAO"] = (_dr.Risk_Notifications != null ? _dr.Risk_Notifications.Value.ToString("N0") : "0.00");//Changed rom n/a to 0.00 AG
        else
            drYtdSummary["KpiScoreYtd-WAO"] = GetRegionKPI(adhocRadarItemService, "Risk Notifications", YearId, MonthId, 20, "N0"); //20 == WAO


        foreach (var siteWAO in sitesWAO)
        {
            //TList<CompanySiteCategoryStandard> cscs = cscsService.GetBySiteId(siteWAO.SiteId);
            var cscs = companySiteCategoryStandardsWAOGroupCount.Where(e => System.Convert.ToInt32(e.Name) == siteWAO.SiteId).FirstOrDefault();

            if (cscs != null)
            {
                if (cscs.Count > 0)
                {
                    decimal? totalSum = null;
                    var _ds = waoAhriList.Where(e => e.SiteId == siteWAO.SiteId && e.Year == YearId && e.AdHoc_Radar.KpiName == "Risk Notifications");
                    foreach (var _dr2 in _ds)
                    {
                        decimal? score = _dr2.KpiScore(MonthId);
                        if (score != null)
                            totalSum = (totalSum ?? 0) + score.Value;
                        else
                        {
                            totalSum = null;
                            break;
                        }
                    }
                    try
                    {
                        drYtdSummary[String.Format("KpiScoreYtd{0}", siteWAO.SiteAbbrev)] = (totalSum != null ? totalSum.Value.ToString("N0") : "0.00");//Changed rom n/a to 0.00 AG
                    }
                    catch { }
                }
            }
        }
        //Added If-condition by Pankaj Dikshit for <DT 2823, 19-Feb-2013>
        if (IsWAM)
            if (MonthId == 0) //DT419 22/1/16 Cindi Thornton - only do this for YTD, need to calculate months
                foreach (var _dr in dsWAM) drYtdSummary["KpiScoreYtd-WAM"] = (_dr.Risk_Notifications != null ? _dr.Risk_Notifications.Value.ToString("N0") : "0.00");//Changed rom n/a to 0.00 AG
            else
                drYtdSummary["KpiScoreYtd-WAM"] = GetRegionKPI(adhocRadarItemService, "Risk Notifications", YearId, MonthId, 21, "N0"); //21 == WAM

        if (MonthId == 0) //DT419 22/1/16 Cindi Thornton - only do this for YTD, need to calculate months
            foreach (var _dr in dsVICOPS) drYtdSummary["KpiScoreYtd-VIC"] = (_dr.Risk_Notifications != null ? _dr.Risk_Notifications.Value.ToString("N0") : "0.00");//Changed rom n/a to 0.00 AG
        else
            drYtdSummary["KpiScoreYtd-VIC"] = GetRegionKPI(adhocRadarItemService, "Risk Notifications", YearId, MonthId, 22, "N0"); //22 == VIC

        foreach (var siteVIC in sitesVIC)
        {
            //TList<CompanySiteCategoryStandard> cscs = cscsService.GetBySiteId(siteVIC.SiteId);
            var cscs = companySiteCategoryStandardsVICGroupCount.Where(e => System.Convert.ToInt32(e.Name) == siteVIC.SiteId).FirstOrDefault();

            if (cscs != null)
            {
                if (cscs.Count > 0)
                {
                    decimal? totalSum = null;
                    var _ds = vicopsAhriList.Where(e => e.SiteId == siteVIC.SiteId && e.Year == YearId && e.AdHoc_Radar.KpiName == "Risk Notifications");
                    foreach (var _dr2 in _ds)
                    {
                        decimal? score = _dr2.KpiScore(MonthId);
                        if (score != null)
                            totalSum = (totalSum ?? 0) + score.Value;
                        else
                        {
                            totalSum = null;
                            break;
                        }
                    }
                    try
                    {
                        drYtdSummary[String.Format("KpiScoreYtd{0}", siteVIC.SiteAbbrev)] = (totalSum != null ? totalSum.Value.ToString("N0") : "0.00");//Changed rom n/a to 0.00 AG
                    }
                    catch { }
                }
            }
        }

        //End of Addition
        dtYtdSummary.Rows.Add(drYtdSummary);
        Session["Radar_Table0"] = dtYtdSummary;
    }

    ////Changed by Pankaj Dikshit for <DT 2823, 19-Feb-2013> to add parameter dsGridCols
    public void CreateGridColumns(int _CategoryId, int _SiteId, ASPxGridView _grid, ASPxGridView _grid2, DataSet dsGridCols)
    {
        //Instantiate services
        SitesService sService = new SitesService();
        RegionsSitesService rService = new RegionsSitesService();
        CompanySiteCategoryStandardService cscsService = new CompanySiteCategoryStandardService();

        //Instantiate Variables
        int grid_VisibleIndex = 0;
        int grid2_VisibleIndex = 0;

        //Create Grid 1 - Summary
        _grid.Width = Unit.Percentage(100);
        _grid.KeyFieldName = "KpiMeasure";
        //Changed by Pankaj Dikshit for <DT 2399, 06-Aug-2012>
        grid_VisibleIndex = Helper.Radar.AddGridColumn(_grid, "KpiMeasure", "Key Performance Indicators", "Safety Rates &nbsp;&nbsp;&nbsp;&nbsp;", 300, grid_VisibleIndex);

        _grid2.Width = Unit.Percentage(100);
        _grid2.KeyFieldName = "KpiMeasure";
        grid2_VisibleIndex = Helper.Radar.AddGridColumn(_grid2, "KpiMeasure", "Key Performance Indicators", "Key Performance Indicators (KPI)", 300, grid2_VisibleIndex);


        bool site = false;
        if (_SiteId < 0) //Region
        {
            if (_SiteId == -2)
            {
                string FieldName = "-WAO";
                //string SiteName = "Western Australian Operations";
                string SiteName = "WAO";
                grid_VisibleIndex = Helper.Radar.AddGridColumn(_grid, String.Format("KpiScoreYtd{0}", FieldName), SiteName, SiteName, 200, grid_VisibleIndex);
                grid2_VisibleIndex = Helper.Radar.AddGridColumn(_grid2, String.Format("KpiScoreYtd{0}", FieldName), SiteName, SiteName, null, grid2_VisibleIndex);

                DataSet dsSitesWAO = sService.CompanySiteCategoryStandard_ByRegionId(1);
                DataSet dsSitesVIC = sService.CompanySiteCategoryStandard_ByRegionId(4);
                //Vikas
                DataSet dsSitesWAM = sService.CompanySiteCategoryStandard_ByRegionId(3);
                //End
                ////Added by Pankaj Dikshit for <DT 2823, 19-Feb-2013>
                DataRow[] drTmp = dsGridCols.Tables[0].Select("SiteAbbrev = '-AM'");
                ////Added If-condition by Pankaj Dikshit for <DT 2823, 19-Feb-2013>
                //if (drTmp.Length > 0)
                //{
                //    FieldName = "-AM";
                //    SiteName = "WAM";
                //    grid_VisibleIndex = Helper.Radar.AddGridColumn(_grid, String.Format("KpiScoreYtd{0}", FieldName), SiteName, SiteName, 200, grid_VisibleIndex);
                //    grid2_VisibleIndex = Helper.Radar.AddGridColumn(_grid2, String.Format("KpiScoreYtd{0}", FieldName), SiteName, SiteName, null, grid2_VisibleIndex);
                //}
                foreach (DataRow dr in dsSitesWAO.Tables[0].Rows)
                {
                    int _SiteId2 = Convert.ToInt32(dr[0].ToString());
                    TList<CompanySiteCategoryStandard> cscs = cscsService.GetBySiteIdCompanySiteCategoryId(_SiteId2, _CategoryId);
                    //Vikas
                    if (Convert.ToInt32(dsSitesWAM.Tables[0].Rows[0][0].ToString()) == _SiteId2)
                    {
                        ////Added If-condition by Pankaj Dikshit for <DT 2823, 19-Feb-2013>
                        if (drTmp.Length > 0)
                        {
                            FieldName = "-AM";
                            SiteName = "WAM";
                            grid_VisibleIndex = Helper.Radar.AddGridColumn(_grid, String.Format("KpiScoreYtd{0}", FieldName), SiteName, SiteName, 200, grid_VisibleIndex);
                            grid2_VisibleIndex = Helper.Radar.AddGridColumn(_grid2, String.Format("KpiScoreYtd{0}", FieldName), SiteName, SiteName, null, grid2_VisibleIndex);
                        }
                    }
                    //End
                    if (cscs != null)
                    {
                        if (cscs.Count > 0)
                        {
                            site = true;

                            SiteName = dr[2].ToString(); //SiteName
                            if (SiteName.Length > 1) SiteName = dr[2].ToString(); //SiteAbbrev

                            grid_VisibleIndex = Helper.Radar.AddGridColumn(_grid, String.Format("KpiScoreYtd{0}", SiteName), SiteName, SiteName, 200, grid_VisibleIndex);
                            grid2_VisibleIndex = Helper.Radar.AddGridColumn(_grid2, String.Format("KpiScoreYtd{0}", SiteName), SiteName, SiteName, null, grid2_VisibleIndex);

                        }
                    }
                }


                FieldName = "-VIC";
                //SiteName = "Victorian Operations";
                SiteName = "VIC";
                grid_VisibleIndex = Helper.Radar.AddGridColumn(_grid, String.Format("KpiScoreYtd{0}", FieldName), SiteName, SiteName, 200, grid_VisibleIndex);
                grid2_VisibleIndex = Helper.Radar.AddGridColumn(_grid2, String.Format("KpiScoreYtd{0}", FieldName), SiteName, SiteName, null, grid2_VisibleIndex);

                foreach (DataRow dr in dsSitesVIC.Tables[0].Rows)
                {
                    int _SiteId2 = Convert.ToInt32(dr[0].ToString());
                    TList<CompanySiteCategoryStandard> cscs = cscsService.GetBySiteIdCompanySiteCategoryId(_SiteId2, _CategoryId);
                    if (cscs != null)
                    {
                        if (cscs.Count > 0)
                        {
                            site = true;

                            SiteName = dr[1].ToString(); //SiteName
                            if (SiteName.Length > 1) SiteName = dr[2].ToString(); //SiteAbbrev

                            grid_VisibleIndex = Helper.Radar.AddGridColumn(_grid, String.Format("KpiScoreYtd{0}", SiteName), SiteName, SiteName, 200, grid_VisibleIndex);
                            grid2_VisibleIndex = Helper.Radar.AddGridColumn(_grid2, String.Format("KpiScoreYtd{0}", SiteName), SiteName, SiteName, null, grid2_VisibleIndex);

                        }
                    }
                }

                site = true;
            }
            else
            {
                DataSet dsSites = sService.CompanySiteCategoryStandard_ByRegionId(-1 * _SiteId);
                ////Added by Pankaj Dikshit for <DT 2823, 19-Feb-2013>
                DataRow[] drTmp = dsGridCols.Tables[0].Select("SiteAbbrev = '-AM'");

                if (dsSites.Tables.Count == 0) throw new Exception("Error Occured whilst Loading Site Columns 1");
                if (dsSites.Tables[0].Rows.Count == 0) throw new Exception("Error Occured whilst Loading Site Columns 2");


                RegionsService regionService = new RegionsService();
                Regions rWAO = regionService.GetByRegionInternalName("WAO");
                Regions rVIC = regionService.GetByRegionInternalName("VICOPS");
                Regions rARP = regionService.GetByRegionInternalName("ARP");
                Regions rWAM = regionService.GetByRegionInternalName("Mines");
                //WAO
                DataSet dsTemp = ((DataSet)Session["Radar_Table1-Summary"]);

                string _FieldName = "";
                string _SiteName = "";

                if ((_SiteId * -1) == rWAO.RegionId)
                {
                    _FieldName = "-WAO";
                    _SiteName = "WAO";
                }
                else if ((_SiteId * -1) == rVIC.RegionId)
                {
                    _FieldName = "-VIC";
                    _SiteName = "VIC";
                }
                else if ((_SiteId * -1) == rARP.RegionId)
                {
                    _FieldName = "-ARP";
                    //SiteName = "Victorian Operations";
                    _SiteName = "ARP";
                }
                else if ((_SiteId * -1) == rWAM.RegionId && drTmp.Length > 0) ////Added condition 'drTmp.Length > 0' by Pankaj Dikshit for <DT 2823, 19-Feb-2013>
                {
                    _FieldName = "-AM";
                    _SiteName = "WAM";
                }


                if (!String.IsNullOrEmpty(_FieldName) && !String.IsNullOrEmpty(_SiteName))
                {
                    grid_VisibleIndex = Helper.Radar.AddGridColumn(_grid, String.Format("KpiScoreYtd{0}", _FieldName), _SiteName, _SiteName, 200, grid_VisibleIndex);
                    grid2_VisibleIndex = Helper.Radar.AddGridColumn(_grid2, String.Format("KpiScoreYtd{0}", _FieldName), _SiteName, _SiteName, null, grid2_VisibleIndex);
                }


                //VIC

                //ARP


                foreach (DataRow dr in dsSites.Tables[0].Rows)
                {
                    int _SiteId2 = Convert.ToInt32(dr[0].ToString());
                    TList<CompanySiteCategoryStandard> cscs = cscsService.GetBySiteIdCompanySiteCategoryId(_SiteId2, _CategoryId);
                    if (cscs != null)
                    {
                        if (cscs.Count > 0)
                        {
                            site = true;

                            string SiteName = dr[1].ToString(); //SiteName
                            if (SiteName.Length > 1) SiteName = dr[2].ToString(); //SiteAbbrev

                            grid_VisibleIndex = Helper.Radar.AddGridColumn(_grid, String.Format("KpiScoreYtd{0}", SiteName), SiteName, SiteName, 200, grid_VisibleIndex);
                            grid2_VisibleIndex = Helper.Radar.AddGridColumn(_grid2, String.Format("KpiScoreYtd{0}", SiteName), SiteName, SiteName, null, grid2_VisibleIndex);

                        }
                    }
                }
            }
        }
        else //Site
        {
            TList<CompanySiteCategoryStandard> cscs = cscsService.GetBySiteIdCompanySiteCategoryId(_SiteId, _CategoryId);
            if (cscs != null)
            {
                if (cscs.Count > 0)
                {
                    site = true;
                    Sites s = sService.GetBySiteId(_SiteId);
                    if (s == null) throw new Exception("Error Occured whilst Loading Site Columns 1");
                    string SiteName = s.SiteName;
                    if (SiteName.Length > 1) SiteName = s.SiteAbbrev;

                    grid_VisibleIndex = Helper.Radar.AddGridColumn(_grid, String.Format("KpiScoreYtd{0}", SiteName), SiteName, SiteName, 200, grid_VisibleIndex);
                    grid2_VisibleIndex = Helper.Radar.AddGridColumn(_grid2, String.Format("KpiScoreYtd{0}", SiteName), SiteName, SiteName, 50, grid2_VisibleIndex);
                    grid2_VisibleIndex = Helper.Radar.AddGridColumn(_grid2, String.Format("CompaniesNotCompliant{0}", SiteName), "Companies Not Compliant", "Companies Not Compliant", null, grid2_VisibleIndex);
                    //grid2_VisibleIndex = Helper.Radar.AddGridColumn(_grid2, "...", "...", "...", 10, grid2_VisibleIndex);
                    //grid2_VisibleIndex = Helper.Radar.AddGridColumn(_grid2, "CompaniesNotCompliant", "...", "...", 10, grid2_VisibleIndex);
                    //grid2_VisibleIndex = Helper.Radar.AddGridColumn_Hyperlink(_grid2, String.Format("CompaniesNotCompliant{0}", SiteName), "Companies Not Compliant", "...", 10, grid2_VisibleIndex);

                }
            }
        }

        if (site)
        {
            switch (_CategoryId)
            {
                case (int)CompanySiteCategoryList.E:
                    E = true;
                    break;
                case (int)CompanySiteCategoryList.NE1:
                    NE1 = true;
                    break;
                case (int)CompanySiteCategoryList.NE2:
                    NE2 = true;
                    break;
                case (int)CompanySiteCategoryList.NE3:
                    NE3 = true;
                    break;
                default:
                    break;
            }
        }

        grid_VisibleIndex = 0;

    }

    //DT419 22/1/16 Cindi Thornton - calculate month values
    private string GetRegionKPI(Repo.CSMS.Service.Database.IAdhocRadarItemService adhocRadarItemService, string KPI, int YearId, int MonthId, int SiteId, string Format = "N02")
    {
        decimal? totalSumWAO = null;
        var waoAhriRegionList = adhocRadarItemService.GetMany(null, e => e.Year == YearId && e.SiteId == SiteId && e.AdHoc_Radar.KpiName == KPI, null, null);
        foreach (var _dr2 in waoAhriRegionList)
        {
            decimal? score = _dr2.KpiScore(MonthId);
            if (score != null)
                totalSumWAO = (totalSumWAO ?? 0) + score.Value;
            else
            {
                totalSumWAO = null;
                break;
            }
        }
        return totalSumWAO != null ? totalSumWAO.Value.ToString(Format) : "0.00"; //Changed from n/a to 0.00 AG
    } //End DT419


    protected void GenerateData(int _SiteId, int _YearId, int _MonthId)
    {
        SitesService sService = new SitesService();
        var siteService = ALCOA.CSMS.Website.Global.GetInstance<Repo.CSMS.Service.Database.ISiteService>();
        var adhocRadarItemService = ALCOA.CSMS.Website.Global.GetInstance<Repo.CSMS.Service.Database.IAdhocRadarItemService>();

        AdHocRadarService ahrService = new AdHocRadarService();

        int _count = 0;
        AdHocRadarFilters ahrFilters = new AdHocRadarFilters();
        ahrFilters.Append(AdHocRadarColumn.CompanySiteCategoryId, ((int)CompanySiteCategoryList.E).ToString());
        TList<AdHocRadar> ahrTlist_E = ahrService.GetPaged(ahrFilters.ToString(), "KpiOrdinal ASC", 0, 100, out _count);

        ahrFilters.Clear();
        ahrFilters.Append(AdHocRadarColumn.CompanySiteCategoryId, ((int)CompanySiteCategoryList.NE1).ToString());
        TList<AdHocRadar> ahrTlist_NE1 = ahrService.GetPaged(ahrFilters.ToString(), "KpiOrdinal ASC", 0, 100, out _count);

        ahrFilters.Clear();
        ahrFilters.Append(AdHocRadarColumn.CompanySiteCategoryId, ((int)CompanySiteCategoryList.NE2).ToString());
        TList<AdHocRadar> ahrTlist_NE2 = ahrService.GetPaged(ahrFilters.ToString(), "KpiOrdinal ASC", 0, 100, out _count);

        ahrFilters.Clear();
        ahrFilters.Append(AdHocRadarColumn.CompanySiteCategoryId, ((int)CompanySiteCategoryList.NE3).ToString());
        TList<AdHocRadar> ahrTlist_NE3 = ahrService.GetPaged(ahrFilters.ToString(), "KpiOrdinal ASC", 0, 100, out _count);

        DataSet dsSites_E = sService.GetByYearAdHocRadarId(_YearId, ahrTlist_E[0].AdHocRadarId);
        DataSet dsSites_NE1 = sService.GetByYearAdHocRadarId(_YearId, ahrTlist_E[0].AdHocRadarId);
        DataSet dsSites_NE2 = sService.GetByYearAdHocRadarId(_YearId, ahrTlist_E[0].AdHocRadarId);
        DataSet dsSites_NE3 = sService.GetByYearAdHocRadarId(_YearId, ahrTlist_E[0].AdHocRadarId);

        CreateGridColumns((int)CompanySiteCategoryList.E, SiteId, grid_E, grid2_E, dsSites_E);
        CreateGridColumns((int)CompanySiteCategoryList.NE1, SiteId, grid_NE1, grid2_NE1, dsSites_NE1);
        CreateGridColumns((int)CompanySiteCategoryList.NE2, SiteId, grid_NE2, grid2_NE2, dsSites_NE2);
        CreateGridColumns((int)CompanySiteCategoryList.NE3, SiteId, grid_NE3, grid2_NE3, dsSites_NE3);


        //TList<AdHocRadar> ahrTlist_E = ahrService.GetByCompanySiteCategoryId((int)CompanySiteCategoryList.E);
        //TList<AdHocRadar> ahrTlist_NE1 = ahrService.GetByCompanySiteCategoryId((int)CompanySiteCategoryList.NE1);
        //TList<AdHocRadar> ahrTlist_NE2 = ahrService.GetByCompanySiteCategoryId((int)CompanySiteCategoryList.NE2);
        //TList<AdHocRadar> ahrTlist_NE3 = ahrService.GetByCompanySiteCategoryId((int)CompanySiteCategoryList.NE3);

        //GetData
        DataSet dsYtdSummary = new DataSet();
        DataTable dtYtdSummary_E = new DataTable();
        DataTable dtYtdSummary_NE1 = new DataTable();
        DataTable dtYtdSummary_NE2 = new DataTable();
        DataTable dtYtdSummary_NE3 = new DataTable();

        DataSet dsYtd = new DataSet();
        DataTable dtYtd_E = new DataTable();
        DataTable dtYtd_NE1 = new DataTable();
        DataTable dtYtd_NE2 = new DataTable();
        DataTable dtYtd_NE3 = new DataTable();


        dtYtdSummary_E.Columns.Add("KpiMeasure", typeof(string));
        dtYtd_E.Columns.Add("KpiMeasure", typeof(string));
        dtYtdSummary_NE1.Columns.Add("KpiMeasure", typeof(string));
        dtYtd_NE1.Columns.Add("KpiMeasure", typeof(string));
        dtYtdSummary_NE2.Columns.Add("KpiMeasure", typeof(string));
        dtYtd_NE2.Columns.Add("KpiMeasure", typeof(string));
        dtYtdSummary_NE3.Columns.Add("KpiMeasure", typeof(string));
        dtYtd_NE3.Columns.Add("KpiMeasure", typeof(string));

        if (dsSites_E.Tables[0].Rows.Count > 0)
        {
            foreach (DataRow dr in dsSites_E.Tables[0].Rows)
            {
                //if (dr[0].ToString() == "4")
                //{
                //    dtYtdSummary_E.Columns.Add("KpiScoreYtd-WAM", typeof(String));
                //    dtYtd_E.Columns.Add("KpiScoreYtd-WAM", typeof(string));
                //    dtYtd_E.Columns.Add("CompaniesNotCompliant-WAM", typeof(string));
                //}
                dtYtdSummary_E.Columns.Add(String.Format("KpiScoreYtd{0}", dr[2].ToString()), typeof(string));
                dtYtd_E.Columns.Add(String.Format("KpiScoreYtd{0}", dr[2].ToString()), typeof(string));
                dtYtd_E.Columns.Add(String.Format("CompaniesNotCompliant{0}", dr[2].ToString()), typeof(string));
            }
        }

        if (dsSites_NE1.Tables[0].Rows.Count > 0)
        {
            foreach (DataRow dr in dsSites_NE1.Tables[0].Rows)
            {
                //if (dr[0].ToString() == "4")
                //{
                //    dtYtdSummary_NE1.Columns.Add("KpiScoreYtd-WAM", typeof(String));
                //    dtYtd_NE1.Columns.Add("KpiScoreYtd-WAM", typeof(string));
                //    dtYtd_NE1.Columns.Add("CompaniesNotCompliant-WAM", typeof(string));
                //}
                dtYtdSummary_NE1.Columns.Add(String.Format("KpiScoreYtd{0}", dr[2].ToString()), typeof(string));
                dtYtd_NE1.Columns.Add(String.Format("KpiScoreYtd{0}", dr[2].ToString()), typeof(string));
                dtYtd_NE1.Columns.Add(String.Format("CompaniesNotCompliant{0}", dr[2].ToString()), typeof(string));
            }
        }

        if (dsSites_NE2.Tables[0].Rows.Count > 0)
        {
            foreach (DataRow dr in dsSites_NE2.Tables[0].Rows)
            {
                //if (dr[0].ToString() == "4")
                //{
                //    dtYtdSummary_NE2.Columns.Add("KpiScoreYtd-WAM", typeof(String));
                //    dtYtd_NE2.Columns.Add("KpiScoreYtd-WAM", typeof(string));
                //    dtYtd_NE2.Columns.Add("CompaniesNotCompliant-WAM", typeof(string));
                //}
                dtYtdSummary_NE2.Columns.Add(String.Format("KpiScoreYtd{0}", dr[2].ToString()), typeof(string));
                dtYtd_NE2.Columns.Add(String.Format("KpiScoreYtd{0}", dr[2].ToString()), typeof(string));
                dtYtd_NE2.Columns.Add(String.Format("CompaniesNotCompliant{0}", dr[2].ToString()), typeof(string));
            }
        }

        if (dsSites_NE3.Tables[0].Rows.Count > 0)
        {
            foreach (DataRow dr in dsSites_NE3.Tables[0].Rows)
            {
                //if (dr[0].ToString() == "4")
                //{
                //    dtYtdSummary_NE3.Columns.Add("KpiScoreYtd-WAM", typeof(String));
                //    dtYtd_NE3.Columns.Add("KpiScoreYtd-WAM", typeof(string));
                //    dtYtd_NE3.Columns.Add("CompaniesNotCompliant-WAM", typeof(string));
                //}
                dtYtdSummary_NE3.Columns.Add(String.Format("KpiScoreYtd{0}", dr[2].ToString()), typeof(string));
                dtYtd_NE3.Columns.Add(String.Format("KpiScoreYtd{0}", dr[2].ToString()), typeof(string));
                dtYtd_NE3.Columns.Add(String.Format("CompaniesNotCompliant{0}", dr[2].ToString()), typeof(string));
            }
        }

        int count = 0;

        var adHocRadarIdList = ahrTlist_E.Select(e => e.AdHocRadarId).ToList();
        var ahriTlist = adhocRadarItemService.GetMany(null, e => e.Year == _YearId && adHocRadarIdList.Contains(e.AdHoc_RadarId), null, null);
        var ahriTlistSiteIdList = ahriTlist.Select(e => e.SiteId).ToList();
        var ahriTlistSiteList = siteService.GetMany(null, e => ahriTlistSiteIdList.Contains(e.SiteId), null, null);

        //Addition of rows Vikas
        foreach (AdHocRadar ahr in ahrTlist_E)
        {
            //count++;

            //AdHocRadarItemsService ahriService = new AdHocRadarItemsService();
            //TList<AdHocRadarItems> ahriTlist = ahriService.GetByYearAdHocRadarId(_YearId, ahr.AdHocRadarId);

            //Changed by Pankaj Dikshit for <DT 2459, 14-Aug-2012> - if-condition is changed for DART
            if (ahr.KpiName == "Peak No Employees (Previous Month)" ||
                ahr.KpiName == "Avg No Employees (YTD)" ||
                ahr.KpiName == "Average No. Of People On Site" ||
                ahr.KpiName == "Total Man Hours" ||
                ahr.KpiName == "LWDFR" || ahr.KpiName == "TRIFR" || ahr.KpiName == "AIFR" || ahr.KpiName == "IFE : Injury Ratio" || ahr.KpiName == "DART" || ahr.KpiName == "Risk Notifications")
            {
                if (ahr.KpiName == "Peak No Employees (Previous Month)" || ahr.KpiName == "Avg No Employees (YTD)" || ahr.KpiName == "Average No. Of People On Site" ||
                    ahr.KpiName == "Total Man Hours" || ahr.KpiName == "Number of Companies On Site:")
                {
                    //temporarily don't load! Peter Wisdom requested. Monday 16 May 2011


                }
                else
                {
                    //Comment by Sayani 
                    //need to add WAM in AdHoc_Radar_Items table
                    DataRow dr = dtYtdSummary_E.NewRow();
                    dr["KpiMeasure"] = ahr.KpiName;

                    foreach (var ahri in ahriTlist.Where(e => e.Year == _YearId && e.AdHoc_RadarId == ahr.AdHocRadarId))
                    {
                        var site = ahriTlistSiteList.Where(e => e.SiteId == ahri.SiteId).FirstOrDefault();

                        try
                        {
                            decimal? score = ahri.KpiScore(MonthId);
                            dr[String.Format("KpiScoreYtd{0}", site.SiteAbbrev)] = (score != null ? score.Value.ToString("N0") : "0");  //Changed from n/a to 0 AG
                        }
                        catch { }
                    }

                    dtYtdSummary_E.Rows.Add(dr);
                }
            }
            else
            {
                DataRow dr = dtYtd_E.NewRow();
                dr["KpiMeasure"] = ahr.KpiName;

                DateTime? dt = null;
                foreach (var ahri in ahriTlist.Where(e => e.Year == _YearId && e.AdHoc_RadarId == ahr.AdHocRadarId))
                {
                    var site = ahriTlistSiteList.Where(e => e.SiteId == ahri.SiteId).FirstOrDefault();

                    try
                    {
                        decimal? score = ahri.KpiScore(MonthId);
                        if (ahr.KpiName == "CSA - Self Audit (Conducted)")
                          dr[String.Format("KpiScoreYtd{0}", site.SiteAbbrev)] = (score != null ? score.Value.ToString("N0") : "n/a"); //Changed from n/a to 0 AG
                        else
                          dr[String.Format("KpiScoreYtd{0}", site.SiteAbbrev)] = (score != null ? score.Value.ToString("N0") : "0");
                    }
                    catch { }
                    try
                    {
                        string companiesNotCompliant = ahri.CompaniesNotCompliantMonth(MonthId);  //This Section Modified by Ashley Goldstraw to show monthly companies not compliant
                        //dr[String.Format("CompaniesNotCompliant{0}", site.SiteAbbrev)] = ahri.CompaniesNotCompliant;
                        dr[String.Format("CompaniesNotCompliant{0}", site.SiteAbbrev)] = companiesNotCompliant; 
                    }
                    catch { }
                    if (dt == null)
                    {
                        dt = ahri.ModifiedDate;
                    }
                    else
                    {
                        if (ahri.ModifiedDate < dt)
                        {
                            dt = ahri.ModifiedDate;
                        }
                    }
                    //count++;
                    //if (count == ahrTlist_E.Count)
                    //{
                    //    lblLastViewedAt_E.Text = String.Format("Data Valid As of: {0}", ahri.ModifiedDate);
                    //}
                }
                lblLastViewedAt_E.Text = String.Format("Data Valid As of: {0}", dt);

                dtYtd_E.Rows.Add(dr);
            }
        }

        adHocRadarIdList = ahrTlist_NE1.Select(e => e.AdHocRadarId).ToList();
        ahriTlist = adhocRadarItemService.GetMany(null, e => e.Year == _YearId && adHocRadarIdList.Contains(e.AdHoc_RadarId), null, null);
        ahriTlistSiteIdList = ahriTlist.Select(e => e.SiteId).ToList();
        ahriTlistSiteList = siteService.GetMany(null, e => ahriTlistSiteIdList.Contains(e.SiteId), null, null);

        count = 0;
        foreach (AdHocRadar ahr in ahrTlist_NE1)
        {
            count++;

            //AdHocRadarItemsService ahriService = new AdHocRadarItemsService();
            //TList<AdHocRadarItems> ahriTlist = ahriService.GetByYearAdHocRadarId(_YearId, ahr.AdHocRadarId);

            //Changed by Pankaj Dikshit for <DT 2459, 14-Aug-2012> - if-condition is changed for DART    
            if (ahr.KpiName == "Peak No Employees (Previous Month)" ||
                    ahr.KpiName == "Avg No Employees (YTD)" ||
                    ahr.KpiName == "Average No. Of People On Site" ||
                    ahr.KpiName == "Total Man Hours" ||
                    ahr.KpiName == "LWDFR" || ahr.KpiName == "TRIFR" || ahr.KpiName == "AIFR" || ahr.KpiName == "IFE : Injury Ratio" || ahr.KpiName == "DART" || ahr.KpiName == "Risk Notifications")
            {


                if (ahr.KpiName == "Peak No Employees (Previous Month)" || ahr.KpiName == "Avg No Employees (YTD)" || ahr.KpiName == "Average No. Of People On Site" ||
                    ahr.KpiName == "Total Man Hours" || ahr.KpiName == "Number of Companies On Site:")
                {
                    //temporarily don't load! Peter Wisdom requested. Monday 16 May 2011


                }
                else
                {
                    DataRow dr = dtYtdSummary_NE1.NewRow();
                    dr["KpiMeasure"] = ahr.KpiName;

                    foreach (var ahri in ahriTlist.Where(e => e.Year == _YearId && e.AdHoc_RadarId == ahr.AdHocRadarId))
                    {
                        var site = ahriTlistSiteList.Where(e => e.SiteId == ahri.SiteId).FirstOrDefault();

                        try
                        {
                            decimal? score = ahri.KpiScore(MonthId);
                            dr[String.Format("KpiScoreYtd{0}", site.SiteAbbrev)] = (score != null ? score.Value.ToString("N0") : "0");  //Changed from n/a to 0 AG
                        }
                        catch { }

                    }

                    dtYtdSummary_NE1.Rows.Add(dr);
                }

            }
            else
            {
                DataRow dr = dtYtd_NE1.NewRow();
                dr["KpiMeasure"] = ahr.KpiName;

                foreach (var ahri in ahriTlist.Where(e => e.Year == _YearId && e.AdHoc_RadarId == ahr.AdHocRadarId))
                {
                    var site = ahriTlistSiteList.Where(e => e.SiteId == ahri.SiteId).FirstOrDefault();

                    try
                    {
                        decimal? score = ahri.KpiScore(MonthId);
                        if (ahr.KpiName == "CSA - Self Audit (Conducted)")
                            dr[String.Format("KpiScoreYtd{0}", site.SiteAbbrev)] = (score != null ? score.Value.ToString("N0") : "n/a"); //Changed from n/a to 0 AG
                        else
                            dr[String.Format("KpiScoreYtd{0}", site.SiteAbbrev)] = (score != null ? score.Value.ToString("N0") : "0");


                    }
                    catch { }
                    try
                    {
                        string companiesNotCompliant = ahri.CompaniesNotCompliantMonth(MonthId);  //This Section Modified by Ashley Goldstraw to show monthly companies not compliant
                        //dr[String.Format("CompaniesNotCompliant{0}", site.SiteAbbrev)] = ahri.CompaniesNotCompliant;
                        dr[String.Format("CompaniesNotCompliant{0}", site.SiteAbbrev)] = companiesNotCompliant;
                    }
                    catch { }


                    count++;
                    if (count == ahrTlist_NE1.Count)
                    {
                        lblLastViewedAt_NE1.Text = String.Format("Data Valid As of: {0}", ahri.ModifiedDate);
                    }
                }

                dtYtd_NE1.Rows.Add(dr);
            }
        }

        adHocRadarIdList = ahrTlist_NE2.Select(e => e.AdHocRadarId).ToList();
        ahriTlist = adhocRadarItemService.GetMany(null, e => e.Year == _YearId && adHocRadarIdList.Contains(e.AdHoc_RadarId), null, null);
        ahriTlistSiteIdList = ahriTlist.Select(e => e.SiteId).ToList();
        ahriTlistSiteList = siteService.GetMany(null, e => ahriTlistSiteIdList.Contains(e.SiteId), null, null);

        count = 0;
        foreach (AdHocRadar ahr in ahrTlist_NE2)
        {
            count++;

            //AdHocRadarItemsService ahriService = new AdHocRadarItemsService();
            //TList<AdHocRadarItems> ahriTlist = ahriService.GetByYearAdHocRadarId(_YearId, ahr.AdHocRadarId);

            //Changed by Pankaj Dikshit for <DT 2459, 14-Aug-2012> - if-condition is changed for DART
            if (ahr.KpiName == "Peak No Employees (Previous Month)" ||
                ahr.KpiName == "Avg No Employees (YTD)" ||
                ahr.KpiName == "Average No. Of People On Site" ||
                ahr.KpiName == "Total Man Hours" ||
                ahr.KpiName == "LWDFR" || ahr.KpiName == "TRIFR" || ahr.KpiName == "AIFR" || ahr.KpiName == "IFE : Injury Ratio" || ahr.KpiName == "DART" || ahr.KpiName == "Risk Notifications")
            {
                if (ahr.KpiName == "Peak No Employees (Previous Month)" || ahr.KpiName == "Avg No Employees (YTD)" || ahr.KpiName == "Average No. Of People On Site" ||
                    ahr.KpiName == "Total Man Hours" || ahr.KpiName == "Number of Companies On Site:")
                {
                    //temporarily don't load! Peter Wisdom requested. Monday 16 May 2011

                }
                else
                {
                    DataRow dr = dtYtdSummary_NE2.NewRow();
                    dr["KpiMeasure"] = ahr.KpiName;

                    foreach (var ahri in ahriTlist.Where(e => e.Year == _YearId && e.AdHoc_RadarId == ahr.AdHocRadarId))
                    {
                        var site = ahriTlistSiteList.Where(e => e.SiteId == ahri.SiteId).FirstOrDefault();

                        try
                        {
                            decimal? score = ahri.KpiScore(MonthId);
                            dr[String.Format("KpiScoreYtd{0}", site.SiteAbbrev)] = (score != null ? score.Value.ToString("N0") : "0"); //Changed from n/a to 0 AG
                        }
                        catch { }
                    }

                    dtYtdSummary_NE2.Rows.Add(dr);
                }
            }
            else
            {
                DataRow dr = dtYtd_NE2.NewRow();
                dr["KpiMeasure"] = ahr.KpiName;
                foreach (var ahri in ahriTlist.Where(e => e.Year == _YearId && e.AdHoc_RadarId == ahr.AdHocRadarId))
                {
                    var site = ahriTlistSiteList.Where(e => e.SiteId == ahri.SiteId).FirstOrDefault();

                    try
                    {
                        decimal? score = ahri.KpiScore(MonthId);
                        if (ahr.KpiName == "CSA - Self Audit (Conducted)")
                            dr[String.Format("KpiScoreYtd{0}", site.SiteAbbrev)] = (score != null ? score.Value.ToString("N0") : "n/a"); //Changed from n/a to 0 AG
                        else
                            dr[String.Format("KpiScoreYtd{0}", site.SiteAbbrev)] = (score != null ? score.Value.ToString("N0") : "0");


                    }
                    catch { }

                    try
                    {
                        string companiesNotCompliant = ahri.CompaniesNotCompliantMonth(MonthId);  //This Section Modified by Ashley Goldstraw to show monthly companies not compliant
                        //dr[String.Format("CompaniesNotCompliant{0}", site.SiteAbbrev)] = ahri.CompaniesNotCompliant;
                        dr[String.Format("CompaniesNotCompliant{0}", site.SiteAbbrev)] = companiesNotCompliant;
                    }
                    catch { }


                    

                    count++;
                    if (count == ahrTlist_NE2.Count)
                    {
                        lblLastViewedAt_NE2.Text = String.Format("Data Valid As of: {0}", ahri.ModifiedDate);
                    }
                }

                dtYtd_NE2.Rows.Add(dr);
            }
        }

        adHocRadarIdList = ahrTlist_NE3.Select(e => e.AdHocRadarId).ToList();
        ahriTlist = adhocRadarItemService.GetMany(null, e => e.Year == _YearId && adHocRadarIdList.Contains(e.AdHoc_RadarId), null, null);
        ahriTlistSiteIdList = ahriTlist.Select(e => e.SiteId).ToList();
        ahriTlistSiteList = siteService.GetMany(null, e => ahriTlistSiteIdList.Contains(e.SiteId), null, null);

        count = 0;
        foreach (AdHocRadar ahr in ahrTlist_NE3)
        {
            count++;

            //AdHocRadarItemsService ahriService = new AdHocRadarItemsService();
            //TList<AdHocRadarItems> ahriTlist = ahriService.GetByYearAdHocRadarId(_YearId, ahr.AdHocRadarId);

            //Changed by Pankaj Dikshit for <DT 2459, 14-Aug-2012> - if-condition is changed for DART
            if (ahr.KpiName == "Peak No Employees (Previous Month)" ||
                ahr.KpiName == "Avg No Employees (YTD)" ||
                ahr.KpiName == "Average No. Of People On Site" ||
                ahr.KpiName == "Total Man Hours" ||
                ahr.KpiName == "LWDFR" || ahr.KpiName == "TRIFR" || ahr.KpiName == "AIFR" || ahr.KpiName == "IFE : Injury Ratio" || ahr.KpiName == "DART" || ahr.KpiName == "Risk Notifications")
            {
                if (ahr.KpiName == "Peak No Employees (Previous Month)" || ahr.KpiName == "Avg No Employees (YTD)" || ahr.KpiName == "Average No. Of People On Site" ||
                    ahr.KpiName == "Total Man Hours")
                {
                    //temporarily don't load! Peter Wisdom requested. Monday 16 May 2011

                }
                else
                {
                    DataRow dr = dtYtdSummary_NE3.NewRow();
                    dr["KpiMeasure"] = ahr.KpiName;

                    foreach (var ahri in ahriTlist.Where(e => e.Year == _YearId && e.AdHoc_RadarId == ahr.AdHocRadarId))
                    {
                        var site = ahriTlistSiteList.Where(e => e.SiteId == ahri.SiteId).FirstOrDefault();

                        try
                        {
                            decimal? score = ahri.KpiScore(MonthId);
                            dr[String.Format("KpiScoreYtd{0}", site.SiteAbbrev)] = (score != null ? score.Value.ToString("N0") : "0");//Changed from n/a to 0 AG
                        }
                        catch { }
                    }

                    dtYtdSummary_NE3.Rows.Add(dr);
                }
            }
            else
            {
                DataRow dr = dtYtd_NE3.NewRow();
                dr["KpiMeasure"] = ahr.KpiName;

                foreach (var ahri in ahriTlist.Where(e => e.Year == _YearId && e.AdHoc_RadarId == ahr.AdHocRadarId))
                {
                    var site = ahriTlistSiteList.Where(e => e.SiteId == ahri.SiteId).FirstOrDefault();

                    try
                    {
                        decimal? score = ahri.KpiScore(MonthId);
                        if (ahr.KpiName == "CSA - Self Audit (Conducted)")
                            dr[String.Format("KpiScoreYtd{0}", site.SiteAbbrev)] = (score != null ? score.Value.ToString("N0") : "n/a"); //Changed from n/a to 0 AG
                        else
                            dr[String.Format("KpiScoreYtd{0}", site.SiteAbbrev)] = (score != null ? score.Value.ToString("N0") : "0");

                    }
                    catch { }
                    try
                    {
                        string companiesNotCompliant = ahri.CompaniesNotCompliantMonth(MonthId);  //This Section Modified by Ashley Goldstraw to show monthly companies not compliant
                        //dr[String.Format("CompaniesNotCompliant{0}", site.SiteAbbrev)] = ahri.CompaniesNotCompliant;
                        dr[String.Format("CompaniesNotCompliant{0}", site.SiteAbbrev)] = companiesNotCompliant;
                    }
                    catch { }


                    count++;
                    if (count == ahrTlist_NE3.Count)
                    {
                        lblLastViewedAt_NE3.Text = String.Format("Data Valid As of: {0}", ahri.ModifiedDate);
                    }
                }

                dtYtd_NE3.Rows.Add(dr);
            }
        }

        dsYtdSummary.Tables.Add(dtYtdSummary_E);
        dsYtdSummary.Tables.Add(dtYtdSummary_NE1);
        dsYtdSummary.Tables.Add(dtYtdSummary_NE2);
        dsYtdSummary.Tables.Add(dtYtdSummary_NE3);

        dsYtd.Tables.Add(dtYtd_E);
        dsYtd.Tables.Add(dtYtd_NE1);
        dsYtd.Tables.Add(dtYtd_NE2);
        dsYtd.Tables.Add(dtYtd_NE3);

        Session["Radar_Table1-Summary"] = dsYtdSummary;
        Session["Radar_Table2-Ytd"] = dsYtd;


    }

    protected void BindData()
    {
        bool ok = false;
        try
        {
            if (NE3)
            {
                grid_NE3.DataSource = (DataTable)(((DataSet)Session["Radar_Table1-Summary"]).Tables[3]);
                grid_NE3.DataBind();
                grid2_NE3.DataSource = (DataTable)(((DataSet)Session["Radar_Table2-Ytd"]).Tables[3]);
                grid2_NE3.DataBind();
                ASPxPageControl1.TabPages[3].Enabled = true;
                ASPxPageControl1.ActiveTabIndex = 3;
                ok = true;
            }
        }
        catch (Exception ex)
        {
        }

        try
        {
            if (NE2)
            {
                grid_NE2.DataSource = (DataTable)(((DataSet)Session["Radar_Table1-Summary"]).Tables[2]);
                grid_NE2.DataBind();
                grid2_NE2.DataSource = (DataTable)(((DataSet)Session["Radar_Table2-Ytd"]).Tables[2]);
                grid2_NE2.DataBind();
                ASPxPageControl1.TabPages[2].Enabled = true;
                ASPxPageControl1.ActiveTabIndex = 2;
                ok = true;
            }
        }
        catch (Exception ex)
        {
        }

        try
        {
            if (NE1)
            {
                grid_NE1.DataSource = (DataTable)(((DataSet)Session["Radar_Table1-Summary"]).Tables[1]);
                grid_NE1.DataBind();
                grid2_NE1.DataSource = (DataTable)(((DataSet)Session["Radar_Table2-Ytd"]).Tables[1]);
                grid2_NE1.DataBind();
                ASPxPageControl1.TabPages[1].Enabled = true;
                ASPxPageControl1.ActiveTabIndex = 1;
                ok = true;
            }
        }
        catch (Exception ex)
        {
        }

        try
        {
            if (E)
            {
                grid_E.DataSource = (DataTable)(((DataSet)Session["Radar_Table1-Summary"]).Tables[0]);
                grid_E.DataBind();
                grid2_E.DataSource = (DataTable)(((DataSet)Session["Radar_Table2-Ytd"]).Tables[0]);
                grid2_E.DataBind();
                ASPxPageControl1.TabPages[0].Enabled = true;
                ASPxPageControl1.ActiveTabIndex = 0;
                ok = true;
            }
        }
        catch (Exception ex)
        {
        }

        if (ok)
        {
            grid_All.DataSource = (DataTable)Session["Radar_Table0"];
            grid_All.DataBind();
        }
        else
        {
            grid_All.Visible = false;
        }

        //lblLastViewedAt_E.Text = String.Format("Data Valid As of: {0}", DateTime.Now);
        //lblLastViewedAt_NE1.Text = lblLastViewedAt_E.Text;
        //lblLastViewedAt_NE2.Text = lblLastViewedAt_E.Text;
        //lblLastViewedAt_NE3.Text = lblLastViewedAt_E.Text;
    }


    /*
    protected DataSet GenerateTableData()
    {
        grid_VisibleIndex = 0;
        grid.KeyFieldName = "KPI";

        DataTable dt = new DataTable();
        dt.Columns.Add("KPI", typeof(string));
        AddGridColumn(grid, "KPI", "Key Performance Indicators", "Key Performance Indicators (KPI)", 250, false);
        DataTable dt2 = new DataTable();
        dt2.Columns.Add("KPI", typeof(string));
        AddGridColumn(grid2, "KPI", "Key Performance Indicators", "Key Performance Indicators (KPI)", 250, false);

        SitesService sService = new SitesService();

        if (Convert.ToInt32(SessionHandler.spVar_SiteId) > 0)
        {
            SiteId = Convert.ToInt32(SessionHandler.spVar_SiteId);
        }
        else
        {
            RegionId = -1 * Convert.ToInt32(SessionHandler.spVar_SiteId);
        }

        if (RegionId == null)
        {
            
            Sites s = sService.GetBySiteId(Convert.ToInt32(SiteId));
            if (s == null) throw new Exception("Error Occurred whilst Loading Site Columns");
            dt.Columns.Add(s.SiteName);
            AddGridColumn(grid, s.SiteName, null, true);
            dt2.Columns.Add(s.SiteName);
            AddGridColumn(grid2, s.SiteName, null, true);
            grid.Width = Unit.Pixel(270 + (6 * s.SiteName.Length)); //Dynamic Grid Width
            grid2.Width = grid.Width; //Dynamic Grid Width

            //dt.Rows.Add(CreateDataRow(dt.NewRow(), "Average No. Of People On Site", s.SiteName));
            //dt.Rows.Add(CreateDataRow(dt.NewRow(), "Total Man Hours", s.SiteName));
            //dt.Rows.Add(CreateDataRow(dt.NewRow(), "LWDFR", s.SiteName));
            //dt.Rows.Add(CreateDataRow(dt.NewRow(), "TRIFR", s.SiteName));
            //dt.Rows.Add(CreateDataRow(dt.NewRow(), "AIFR", s.SiteName));
            //dt2.Rows.Add(CreateDataRow(dt2.NewRow(), "Contractor Services - Self Audit (%)", s.SiteName));
            //dt2.Rows.Add(CreateDataRow(dt2.NewRow(), "IFE : Injury Ratio", s.SiteName));
            //dt2.Rows.Add(CreateDataRow(dt2.NewRow(), "Workplace Safety & Compliance", s.SiteName));
            //dt2.Rows.Add(CreateDataRow(dt2.NewRow(), "Management Health Safety Work Contacts", s.SiteName));
            //dt2.Rows.Add(CreateDataRow(dt2.NewRow(), "Behavioural Observations (%)", s.SiteName));
            //dt2.Rows.Add(CreateDataRow(dt2.NewRow(), "Fatality Prevention Program", s.SiteName));
            //dt2.Rows.Add(CreateDataRow(dt2.NewRow(), "JSA Field Audit Verifications", s.SiteName));
            //dt2.Rows.Add(CreateDataRow(dt2.NewRow(), "Toolbox Meetings", s.SiteName));
            //dt2.Rows.Add(CreateDataRow(dt2.NewRow(), "Alcoa Weekly Contractors Meeting", s.SiteName));
            //dt2.Rows.Add(CreateDataRow(dt2.NewRow(), "Alcoa Monthly Contractors Meeting", s.SiteName));
            //dt2.Rows.Add(CreateDataRow(dt2.NewRow(), "Safety Plan(s) Submitted", s.SiteName));
            //dt2.Rows.Add(CreateDataRow(dt2.NewRow(), "% Mandated Training", s.SiteName));
        }
        else
        {
            RegionsService rService = new RegionsService();
            Regions r = rService.GetByRegionInternalName("AU");
            Regions rWAO = rService.GetByRegionInternalName("WAO");
            Regions rVICOPS = rService.GetByRegionInternalName("VICOPS");

            if (RegionId == r.RegionId)
            { //Australia
                dt.Columns.Add(rWAO.RegionName, typeof(string));
                dt.Columns.Add(rVICOPS.RegionName, typeof(string));
                AddGridColumn(grid, rWAO.RegionName, (6 * rWAO.RegionName.Length), true);
                AddGridColumn(grid, rVICOPS.RegionName, (6 * rVICOPS.RegionName.Length), true);
                dt2.Columns.Add(rWAO.RegionName, typeof(string));
                dt2.Columns.Add(rVICOPS.RegionName, typeof(string));
                AddGridColumn(grid2, rWAO.RegionName, (6 * rWAO.RegionName.Length), true);
                AddGridColumn(grid2, rVICOPS.RegionName, (6 * rVICOPS.RegionName.Length), true);
                grid.Width = Unit.Pixel(290 + (6 * rWAO.RegionName.Length) + (6 * rVICOPS.RegionName.Length)); //Dynamic Grid Width
                grid2.Width = grid.Width; //Dynamic Grid Width

                dt.Rows.Add(CreateDataRow_Australia(dt.NewRow(), "Average No. Of People On Site", 
                                                                    (int)CompanySiteCategoryList.E, Convert.ToInt32(SessionHandler.spVar_Year)));
                dt.Rows.Add(CreateDataRow_Australia(dt.NewRow(), "Total Man Hours",
                                                                    (int)CompanySiteCategoryList.E, Convert.ToInt32(SessionHandler.spVar_Year)));
                dt.Rows.Add(CreateDataRow_Australia(dt.NewRow(), "LWDFR",
                                                                    (int)CompanySiteCategoryList.E, Convert.ToInt32(SessionHandler.spVar_Year)));
                dt.Rows.Add(CreateDataRow_Australia(dt.NewRow(), "TRIFR",
                                                                    (int)CompanySiteCategoryList.E, Convert.ToInt32(SessionHandler.spVar_Year)));
                dt.Rows.Add(CreateDataRow_Australia(dt.NewRow(), "AIFR",
                                                                    (int)CompanySiteCategoryList.E, Convert.ToInt32(SessionHandler.spVar_Year)));
                dt2.Rows.Add(CreateDataRow_Australia(dt2.NewRow(), "Contractor Services - Self Audit (%)",
                                                                    (int)CompanySiteCategoryList.E, Convert.ToInt32(SessionHandler.spVar_Year)));
                dt2.Rows.Add(CreateDataRow_Australia(dt2.NewRow(), "IFE : Injury Ratio",
                                                                    (int)CompanySiteCategoryList.E, Convert.ToInt32(SessionHandler.spVar_Year)));
                dt2.Rows.Add(CreateDataRow_Australia(dt2.NewRow(), "Workplace Safety & Compliance",
                                                                    (int)CompanySiteCategoryList.E, Convert.ToInt32(SessionHandler.spVar_Year)));
                dt2.Rows.Add(CreateDataRow_Australia(dt2.NewRow(), "Management Health Safety Work Contacts",
                                                                    (int)CompanySiteCategoryList.E, Convert.ToInt32(SessionHandler.spVar_Year)));
                dt2.Rows.Add(CreateDataRow_Australia(dt2.NewRow(), "Behavioural Observations (%)",
                                                                    (int)CompanySiteCategoryList.E, Convert.ToInt32(SessionHandler.spVar_Year)));
                dt2.Rows.Add(CreateDataRow_Australia(dt2.NewRow(), "Fatality Prevention Program",
                                                                    (int)CompanySiteCategoryList.E, Convert.ToInt32(SessionHandler.spVar_Year)));
                dt2.Rows.Add(CreateDataRow_Australia(dt2.NewRow(), "JSA Field Audit Verifications",
                                                                    (int)CompanySiteCategoryList.E, Convert.ToInt32(SessionHandler.spVar_Year)));
                dt2.Rows.Add(CreateDataRow_Australia(dt2.NewRow(), "Toolbox Meetings",
                                                                    (int)CompanySiteCategoryList.E, Convert.ToInt32(SessionHandler.spVar_Year)));
                dt2.Rows.Add(CreateDataRow_Australia(dt2.NewRow(), "Alcoa Weekly Contractors Meeting",
                                                                    (int)CompanySiteCategoryList.E, Convert.ToInt32(SessionHandler.spVar_Year)));
                dt2.Rows.Add(CreateDataRow_Australia(dt2.NewRow(), "Alcoa Monthly Contractors Meeting",
                                                                    (int)CompanySiteCategoryList.E, Convert.ToInt32(SessionHandler.spVar_Year)));
                dt2.Rows.Add(CreateDataRow_Australia(dt2.NewRow(), "Safety Plan(s) Submitted",
                                                                    (int)CompanySiteCategoryList.E, Convert.ToInt32(SessionHandler.spVar_Year)));
                dt2.Rows.Add(CreateDataRow_Australia(dt2.NewRow(), "% Mandated Training",
                                                                    (int)CompanySiteCategoryList.E, Convert.ToInt32(SessionHandler.spVar_Year)));
            }
            else
            {
                RegionsSitesService rsService = new RegionsSitesService();
                TList<RegionsSites> rsTlist = rsService.GetByRegionId(Convert.ToInt32(RegionId));
                if (rsTlist == null) throw new Exception("Error Occurred whilst Loading Site (Region) Columns");
                int Width = 0;
                if (RegionId == rWAO.RegionId)
                {
                    Width = 350;
                }
                else if (RegionId == rVICOPS.RegionId)
                {
                    Width = 450;
                }
                else
                {
                    Width = 300;
                }
                foreach (RegionsSites rs in rsTlist)
                {
                    Sites s = sService.GetBySiteId(rs.SiteId);
                    dt.Columns.Add(s.SiteAbbrev, typeof(string));
                    dt2.Columns.Add(s.SiteAbbrev, typeof(string));
                    if (RegionId == rWAO.RegionId || RegionId == rVICOPS.RegionId)
                    {
                        int multipler = 6;
                        if (RegionId == rWAO.RegionId) multipler = 18;
                        AddGridColumn(grid, s.SiteAbbrev, s.SiteName, (multipler * s.SiteAbbrev.Length), true);
                        AddGridColumn(grid2, s.SiteAbbrev, s.SiteName, (multipler * s.SiteAbbrev.Length), true);
                        Width += multipler * s.SiteAbbrev.Length;
                    }
                    else
                    {
                        AddGridColumn(grid, s.SiteAbbrev, s.SiteName, s.SiteName, (6 * s.SiteName.Length), true);
                        AddGridColumn(grid2, s.SiteAbbrev, s.SiteName, s.SiteName, (6 * s.SiteName.Length), true);
                        Width += 6 * s.SiteName.Length;
                    }
                }
                grid.Width = Unit.Pixel(Width);
                grid2.Width = grid.Width;

                //dt.Rows.Add(CreateDataRow(dt.NewRow(), "Average No. Of People On Site", Convert.ToInt32(RegionId)));
                //dt.Rows.Add(CreateDataRow(dt.NewRow(), "Total Man Hours", Convert.ToInt32(RegionId)));
                //dt.Rows.Add(CreateDataRow(dt.NewRow(), "LWDFR", Convert.ToInt32(RegionId)));
                //dt.Rows.Add(CreateDataRow(dt.NewRow(), "TRIFR", Convert.ToInt32(RegionId)));
                //dt.Rows.Add(CreateDataRow(dt.NewRow(), "AIFR", Convert.ToInt32(RegionId)));
                //dt2.Rows.Add(CreateDataRow(dt2.NewRow(), "Contractor Services - Self Audit (%)", Convert.ToInt32(RegionId)));
                //dt2.Rows.Add(CreateDataRow(dt2.NewRow(), "IFE : Injury Ratio", Convert.ToInt32(RegionId)));
                //dt2.Rows.Add(CreateDataRow(dt2.NewRow(), "Workplace Safety & Compliance", Convert.ToInt32(RegionId)));
                //dt2.Rows.Add(CreateDataRow(dt2.NewRow(), "Management Health Safety Work Contacts", Convert.ToInt32(RegionId)));
                //dt2.Rows.Add(CreateDataRow(dt2.NewRow(), "Behavioural Observations (%)", Convert.ToInt32(RegionId)));
                //dt2.Rows.Add(CreateDataRow(dt2.NewRow(), "Fatality Prevention Program", Convert.ToInt32(RegionId)));
                //dt2.Rows.Add(CreateDataRow(dt2.NewRow(), "JSA Field Audit Verifications", Convert.ToInt32(RegionId)));
                //dt2.Rows.Add(CreateDataRow(dt2.NewRow(), "Toolbox Meetings", Convert.ToInt32(RegionId)));
                //dt2.Rows.Add(CreateDataRow(dt2.NewRow(), "Alcoa Weekly Contractors Meeting", Convert.ToInt32(RegionId)));
                //dt2.Rows.Add(CreateDataRow(dt2.NewRow(), "Alcoa Monthly Contractors Meeting", Convert.ToInt32(RegionId)));
                //dt2.Rows.Add(CreateDataRow(dt2.NewRow(), "Safety Plan(s) Submitted", Convert.ToInt32(RegionId)));
                //dt2.Rows.Add(CreateDataRow(dt2.NewRow(), "% Mandated Training", Convert.ToInt32(RegionId)));
            }
        }
        DataSet ds = new DataSet();
        ds.Tables.Add(dt);
        ds.Tables.Add(dt2);
        return ds;
    }

    protected void BindData(DataSet ds)
    {
        Session["Radar_ComplianceTable"] = ds.Tables[0];
        Session["Radar_ComplianceTable2"] = ds.Tables[1];
        BindData();
        
    }
    protected void BindData()
    {
        grid.DataSource = (DataTable)Session["Radar_ComplianceTable"];
        grid.DataBind();
        grid2.DataSource = (DataTable)Session["Radar_ComplianceTable2"];
        grid2.DataBind();



    }

    protected void AddGridColumn(ASPxGridView grid, string FieldName, string ToolTip, string Caption, int? Width, bool template)
    {
        GridViewDataTextColumn gvCol = new GridViewDataTextColumn();
        gvCol.VisibleIndex = grid_VisibleIndex;
        if (Width != null) gvCol.Width = Unit.Pixel(Convert.ToInt32(Width));
        gvCol.FieldName = FieldName;
        gvCol.Name = FieldName;
        gvCol.ToolTip = ToolTip;
        gvCol.Caption = Caption;

        if (FieldName == "KPI")
        {
            gvCol.CellStyle.HorizontalAlign = HorizontalAlign.Left;
        }
        else
        {
            gvCol.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
            gvCol.CellStyle.HorizontalAlign = HorizontalAlign.Center;
        }

        grid.Columns.Add(gvCol);
        grid_VisibleIndex++;
    }
    protected void AddGridColumn(ASPxGridView grid, string FieldName, string ToolTip, int? Width, bool template)
    {
        AddGridColumn(grid, FieldName, ToolTip, "", Width, template);
    }
    protected void AddGridColumn(ASPxGridView grid, string FieldName, int? Width, bool template)
    {
        AddGridColumn(grid, FieldName, "", "", Width, template);
    }
    */

    protected DataRow CreateDataRow_Australia(DataRow dr, string KpiMeasure, int CategoryId, int Year)
    {
        RegionsService rService = new RegionsService();
        Regions rWAO = rService.GetByRegionInternalName("WAO");
        Regions rVICOPS = rService.GetByRegionInternalName("VICOPS");

        dr["KPI"] = KpiMeasure;
        dr[rWAO.RegionName] = "n/a";
        dr[rVICOPS.RegionName] = "n/a";

        AdHocRadarService ahrService = new AdHocRadarService();
        AdHocRadar ahr = ahrService.GetByCompanySiteCategoryIdKpiName(CategoryId, KpiMeasure);

        if (ahr != null)
        {
            AdHocRadarItemsService ahriService = new AdHocRadarItemsService();
            AdHocRadarItems ahri_WAO = ahriService.GetByYearAdHocRadarIdSiteId(Year, ahr.AdHocRadarId, 20);
            if (ahri_WAO != null)
            {
                dr[rWAO.RegionName] = ahri_WAO.KpiScoreYtd;
                //lblLastValidAt.Text = "Data Valid As of: " + ahri_WAO.ModifiedDate.ToString();
            }
            AdHocRadarItems ahri_VICOPS = ahriService.GetByYearAdHocRadarIdSiteId(Year, ahr.AdHocRadarId, 22);
            if (ahri_VICOPS != null)
            {
                dr[rVICOPS.RegionName] = ahri_VICOPS.KpiScoreYtd;
                //lblLastValidAt.Text = "Data Valid As of: " + ahri_VICOPS.ModifiedDate.ToString();
            }

        }

        return dr;
    }

    protected void grid2_E_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {
        if (E) ColorYtdGridCells(grid2_E, sender, e);
    }
    protected void grid2_NE1_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {
        if (NE1) ColorYtdGridCells(grid2_NE1, sender, e);
    }
    protected void grid2_NE2_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {
        if (NE2) ColorYtdGridCells(grid2_NE2, sender, e);
    }
    protected void grid2_NE3_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {
        if (NE3) ColorYtdGridCells(grid2_NE3, sender, e);
    }
    protected void ColorYtdGridCells(ASPxGridView grid, object sender, ASPxGridViewTableRowEventArgs e)
    {
        if (e.RowType != DevExpress.Web.ASPxGridView.GridViewRowType.Data) return;
        string Kpi = (string)grid.GetRowValues(e.VisibleIndex, "KpiMeasure");
        //e.Row.Cells[0].Style.Add("color", "Blue");

        for (int i = 1; i < e.Row.Cells.Count; i++)
        {
            if (!grid.Columns[i].Name.Contains("CompaniesNotCompliant"))
            {
                string color = "";
                //Changed by Pankaj Dikshit for <DT 2459, 14-Aug-2012> - Convert.ToString is used to handle null
                string _KpiValue = Convert.ToString(grid.GetRowValues(e.VisibleIndex, grid.Columns[i].Name));

                if (!Kpi.Contains("Number of Companies") && !Kpi.Contains("CSA - Self Audit (Conducted)"))
                {
                    if (!String.IsNullOrEmpty(_KpiValue))
                    {
                        if (_KpiValue != "-" && _KpiValue != "n/a")
                        {
                            int KpiValue = Convert.ToInt32(_KpiValue);

                            color = "Red";
                            if (KpiValue >= 80)
                            {
                                color = "Yellow";
                                if (KpiValue == 100) color = "Lime";
                            }

                        }
                    }

                    if (!String.IsNullOrEmpty(color))
                    {
                        e.Row.Cells[i].Style.Add("background-color", color);
                    }
                }
            }
        }
    }



}


