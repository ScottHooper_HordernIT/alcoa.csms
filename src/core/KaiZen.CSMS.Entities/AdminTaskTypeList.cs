﻿
using System;

namespace KaiZen.CSMS.Entities
{
	/// <summary>
	/// An enum representation of the 'AdminTaskType' table. [No description found in the database]
	/// </summary>
	/// <remark>this enumeration contains the items contained in the table AdminTaskType</remark>
	[Serializable]
	public enum AdminTaskTypeList
	{
		/// <summary> 
		/// Create User
		/// </summary>
		[EnumTextValue(@"Create User")]
		Contractor_Safety_Qualification_Pre_Qualification = 1, 

		/// <summary> 
		/// Create/Update User
		/// </summary>
		[EnumTextValue(@"Create/Update User")]
		Contractor_Safety_Qualification_Re_Qualification = 2, 

		/// <summary> 
		/// Create User
		/// </summary>
		[EnumTextValue(@"Create User")]
		Contractor_Request_for_Access = 3, 

		/// <summary> 
		/// Create User
		/// </summary>
		[EnumTextValue(@"Create User")]
		Alcoa_Reader_Access_Enquiry = 4, 

		/// <summary> 
		/// Create/Update User
		/// </summary>
		[EnumTextValue(@"Create/Update User")]
		Alcoa_Procurement_Access = 5, 

		/// <summary> 
		/// Create/Update User
		/// </summary>
		[EnumTextValue(@"Create/Update User")]
		Alcoa_H_S_Assessor_Access = 6, 

		/// <summary> 
		/// Create/Update User
		/// </summary>
		[EnumTextValue(@"Create/Update User")]
		Alcoa_EBI_Data_User = 7, 

		/// <summary> 
		/// Create/Update User
		/// </summary>
		[EnumTextValue(@"Create/Update User")]
		Alcoa_Financial_Reports_User = 8, 

		/// <summary> 
		/// Create/Update User
		/// </summary>
		[EnumTextValue(@"Create/Update User")]
		Alcoa_Training_Package_User = 9

	}
}
