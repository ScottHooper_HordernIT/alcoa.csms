﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface ICompanySiteCategoryExceptionService
    {
        CompanySiteCategoryException Get(Expression<Func<CompanySiteCategoryException, bool>> where, List<Expression<Func<CompanySiteCategoryException, object>>> includes);
        List<CompanySiteCategoryException> GetMany(int? top, Expression<Func<CompanySiteCategoryException, bool>> where, Func<IQueryable<CompanySiteCategoryException>, IOrderedQueryable<CompanySiteCategoryException>> orderBy, List<Expression<Func<CompanySiteCategoryException, object>>> includes);
        List<CompanySiteCategoryException> GetManyPaged(int pageNumber, int pageSize, Expression<Func<CompanySiteCategoryException, bool>> where, Func<IQueryable<CompanySiteCategoryException>, IOrderedQueryable<CompanySiteCategoryException>> orderBy, List<Expression<Func<CompanySiteCategoryException, object>>> includes, out int pageCount, out int totalRecords);
        List<CompanySiteCategoryException> GetAll();
        int? GetCount(Expression<Func<CompanySiteCategoryException, bool>> where, List<Expression<Func<CompanySiteCategoryException, object>>> includes);
        void Insert(CompanySiteCategoryException item, bool saveChanges = true);
        void Update(CompanySiteCategoryException item, bool saveChanges = true);
        void Delete(CompanySiteCategoryException item, bool saveChanges = true);
        void Delete(int companySiteCategoryExceptionId,bool saveChanges = true);
        CompanySiteCategoryException Get(int companyId, int siteId, DateTime monthYear);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class CompanySiteCategoryExceptionService : ICompanySiteCategoryExceptionService
    {
        private readonly IRepository<CompanySiteCategoryException> _repository;

        public CompanySiteCategoryExceptionService(IRepository<CompanySiteCategoryException> repository)
        {
            this._repository = repository;
        }

        public CompanySiteCategoryException Get(Expression<Func<CompanySiteCategoryException, bool>> where, List<Expression<Func<CompanySiteCategoryException, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<CompanySiteCategoryException> GetMany(int? top, Expression<Func<CompanySiteCategoryException, bool>> where, Func<IQueryable<CompanySiteCategoryException>, IOrderedQueryable<CompanySiteCategoryException>> orderBy, List<Expression<Func<CompanySiteCategoryException, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<CompanySiteCategoryException> GetManyPaged(int pageNumber, int pageSize, Expression<Func<CompanySiteCategoryException, bool>> where, Func<IQueryable<CompanySiteCategoryException>, IOrderedQueryable<CompanySiteCategoryException>> orderBy, List<Expression<Func<CompanySiteCategoryException, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<CompanySiteCategoryException, bool>> where, List<Expression<Func<CompanySiteCategoryException, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(CompanySiteCategoryException item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("CompanySiteCategoryException");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(CompanySiteCategoryException item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("CompanySiteCategoryException");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(CompanySiteCategoryException item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("CompanySiteCategoryException");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }


        public List<CompanySiteCategoryException> GetAll()
        {
            return this.GetMany(null, null, null, null).ToList();
        }


        public void Delete(int companySiteCategoryExceptionId, bool saveChanges = true)
        {
            _repository.Delete(_repository.Get(x => x.CompanySiteCategoryExceptionId == companySiteCategoryExceptionId,null));

            if (saveChanges)
                _repository.SaveChanges();
        }


        public CompanySiteCategoryException Get(int companyId, int siteId, DateTime monthYear)
        {
            //Make sure date is first day of month
            DateTime firstDayOfMonth = new DateTime(monthYear.Year, monthYear.Month, 1);
            return this.Get(i => i.CompanyId == companyId && i.SiteId == siteId && firstDayOfMonth >= i.MonthYearFrom && firstDayOfMonth <= i.MonthYearTo, null);
        }
    }

    #endregion
}
