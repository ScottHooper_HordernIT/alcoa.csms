﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="UserControls_Other_help" Codebehind="help.ascx.cs" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxTabControl" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxClasses" tagprefix="dx" %>
<table border="0" cellpadding="0" cellspacing="0" width="550px">
        <tr>
            <td style="height: 12px; text-align: center">
                If you require further assistance please contact us via email at</td>
        </tr>
        <tr>
            <td style="height: 11px; text-align: center">
                <a href="mailto:AUAAlcoaContractorServices@alcoa.com.au">AUAAlcoaContractorServices@alcoa.com.au</a></td>
        </tr>
        <tr>
            <td style="padding-bottom: 6px; height: 11px; text-align: left">
            </td>
        </tr>
        <tr>
            <td style="height: 11px; text-align: left; padding-bottom: 8px;">
                <strong>
                Currently Available Help Documents:</strong></td>
        </tr>
        </table>
<dx:ASPxGridView ID="gridHelp" runat="server" AutoGenerateColumns="False" 
    CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
    CssPostfix="Office2003Blue" Width="550px" OnHtmlRowCreated="gridHelp_RowCreated">
    <Columns>
        <dx:GridViewDataTextColumn FieldName="FileVaultId" 
            Visible="false" SortOrder="Ascending">
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn Caption="File Name" FieldName="FileName" 
            VisibleIndex="0" SortIndex="1" SortOrder="Ascending">
            <DataItemTemplate>
                <dx:ASPxHyperLink ID="hlFile" runat="server" Text=""></dx:ASPxHyperLink>
            </DataItemTemplate>
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn Caption="Applicable to" FieldName="SubCategoryDesc" 
            VisibleIndex="1" GroupIndex="0" SortIndex="0" SortOrder="Ascending" 
            Width="200px">
        </dx:GridViewDataTextColumn>
    </Columns>
    <SettingsPager NumericButtonCount="20" PageSize="20">
        <AllButton Visible="True"></AllButton>
    </SettingsPager>
    <Settings ShowVerticalScrollBar="True" VerticalScrollableHeight="180" />
    <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
        <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
        </LoadingPanelOnStatusBar>
        <LoadingPanel Url="~/App_Themes/Office2003Blue/GridView/Loading.gif">
        </LoadingPanel>
    </Images>
    <ImagesFilterControl>
        <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
        </LoadingPanel>
    </ImagesFilterControl>
    <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
        CssPostfix="Office2003Blue">
        <Header ImageSpacing="5px" SortingImageSpacing="5px">
        </Header>
        <LoadingPanel ImageSpacing="10px">
        </LoadingPanel>
    </Styles>
    <StylesEditors>
        <ProgressBar Height="25px">
        </ProgressBar>
    </StylesEditors>
</dx:ASPxGridView>
