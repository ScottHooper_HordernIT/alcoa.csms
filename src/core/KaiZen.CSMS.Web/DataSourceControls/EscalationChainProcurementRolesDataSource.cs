﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.EscalationChainProcurementRolesProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(EscalationChainProcurementRolesDataSourceDesigner))]
	public class EscalationChainProcurementRolesDataSource : ProviderDataSource<EscalationChainProcurementRoles, EscalationChainProcurementRolesKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EscalationChainProcurementRolesDataSource class.
		/// </summary>
		public EscalationChainProcurementRolesDataSource() : base(new EscalationChainProcurementRolesService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the EscalationChainProcurementRolesDataSourceView used by the EscalationChainProcurementRolesDataSource.
		/// </summary>
		protected EscalationChainProcurementRolesDataSourceView EscalationChainProcurementRolesView
		{
			get { return ( View as EscalationChainProcurementRolesDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the EscalationChainProcurementRolesDataSource control invokes to retrieve data.
		/// </summary>
		public EscalationChainProcurementRolesSelectMethod SelectMethod
		{
			get
			{
				EscalationChainProcurementRolesSelectMethod selectMethod = EscalationChainProcurementRolesSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (EscalationChainProcurementRolesSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the EscalationChainProcurementRolesDataSourceView class that is to be
		/// used by the EscalationChainProcurementRolesDataSource.
		/// </summary>
		/// <returns>An instance of the EscalationChainProcurementRolesDataSourceView class.</returns>
		protected override BaseDataSourceView<EscalationChainProcurementRoles, EscalationChainProcurementRolesKey> GetNewDataSourceView()
		{
			return new EscalationChainProcurementRolesDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the EscalationChainProcurementRolesDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class EscalationChainProcurementRolesDataSourceView : ProviderDataSourceView<EscalationChainProcurementRoles, EscalationChainProcurementRolesKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EscalationChainProcurementRolesDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the EscalationChainProcurementRolesDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public EscalationChainProcurementRolesDataSourceView(EscalationChainProcurementRolesDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal EscalationChainProcurementRolesDataSource EscalationChainProcurementRolesOwner
		{
			get { return Owner as EscalationChainProcurementRolesDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal EscalationChainProcurementRolesSelectMethod SelectMethod
		{
			get { return EscalationChainProcurementRolesOwner.SelectMethod; }
			set { EscalationChainProcurementRolesOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal EscalationChainProcurementRolesService EscalationChainProcurementRolesProvider
		{
			get { return Provider as EscalationChainProcurementRolesService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<EscalationChainProcurementRoles> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<EscalationChainProcurementRoles> results = null;
			EscalationChainProcurementRoles item;
			count = 0;
			
			System.Int32 _escalationChainProcurementRoleId;
			System.Int32 _level;

			switch ( SelectMethod )
			{
				case EscalationChainProcurementRolesSelectMethod.Get:
					EscalationChainProcurementRolesKey entityKey  = new EscalationChainProcurementRolesKey();
					entityKey.Load(values);
					item = EscalationChainProcurementRolesProvider.Get(entityKey);
					results = new TList<EscalationChainProcurementRoles>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case EscalationChainProcurementRolesSelectMethod.GetAll:
                    results = EscalationChainProcurementRolesProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case EscalationChainProcurementRolesSelectMethod.GetPaged:
					results = EscalationChainProcurementRolesProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case EscalationChainProcurementRolesSelectMethod.Find:
					if ( FilterParameters != null )
						results = EscalationChainProcurementRolesProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = EscalationChainProcurementRolesProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case EscalationChainProcurementRolesSelectMethod.GetByEscalationChainProcurementRoleId:
					_escalationChainProcurementRoleId = ( values["EscalationChainProcurementRoleId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["EscalationChainProcurementRoleId"], typeof(System.Int32)) : (int)0;
					item = EscalationChainProcurementRolesProvider.GetByEscalationChainProcurementRoleId(_escalationChainProcurementRoleId);
					results = new TList<EscalationChainProcurementRoles>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				case EscalationChainProcurementRolesSelectMethod.GetByLevel:
					_level = ( values["Level"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["Level"], typeof(System.Int32)) : (int)0;
					item = EscalationChainProcurementRolesProvider.GetByLevel(_level);
					results = new TList<EscalationChainProcurementRoles>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// FK
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == EscalationChainProcurementRolesSelectMethod.Get || SelectMethod == EscalationChainProcurementRolesSelectMethod.GetByEscalationChainProcurementRoleId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				EscalationChainProcurementRoles entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					EscalationChainProcurementRolesProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<EscalationChainProcurementRoles> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			EscalationChainProcurementRolesProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region EscalationChainProcurementRolesDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the EscalationChainProcurementRolesDataSource class.
	/// </summary>
	public class EscalationChainProcurementRolesDataSourceDesigner : ProviderDataSourceDesigner<EscalationChainProcurementRoles, EscalationChainProcurementRolesKey>
	{
		/// <summary>
		/// Initializes a new instance of the EscalationChainProcurementRolesDataSourceDesigner class.
		/// </summary>
		public EscalationChainProcurementRolesDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public EscalationChainProcurementRolesSelectMethod SelectMethod
		{
			get { return ((EscalationChainProcurementRolesDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new EscalationChainProcurementRolesDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region EscalationChainProcurementRolesDataSourceActionList

	/// <summary>
	/// Supports the EscalationChainProcurementRolesDataSourceDesigner class.
	/// </summary>
	internal class EscalationChainProcurementRolesDataSourceActionList : DesignerActionList
	{
		private EscalationChainProcurementRolesDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the EscalationChainProcurementRolesDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public EscalationChainProcurementRolesDataSourceActionList(EscalationChainProcurementRolesDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public EscalationChainProcurementRolesSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion EscalationChainProcurementRolesDataSourceActionList
	
	#endregion EscalationChainProcurementRolesDataSourceDesigner
	
	#region EscalationChainProcurementRolesSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the EscalationChainProcurementRolesDataSource.SelectMethod property.
	/// </summary>
	public enum EscalationChainProcurementRolesSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByEscalationChainProcurementRoleId method.
		/// </summary>
		GetByEscalationChainProcurementRoleId,
		/// <summary>
		/// Represents the GetByLevel method.
		/// </summary>
		GetByLevel
	}
	
	#endregion EscalationChainProcurementRolesSelectMethod

	#region EscalationChainProcurementRolesFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EscalationChainProcurementRoles"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EscalationChainProcurementRolesFilter : SqlFilter<EscalationChainProcurementRolesColumn>
	{
	}
	
	#endregion EscalationChainProcurementRolesFilter

	#region EscalationChainProcurementRolesExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EscalationChainProcurementRoles"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EscalationChainProcurementRolesExpressionBuilder : SqlExpressionBuilder<EscalationChainProcurementRolesColumn>
	{
	}
	
	#endregion EscalationChainProcurementRolesExpressionBuilder	

	#region EscalationChainProcurementRolesProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;EscalationChainProcurementRolesChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="EscalationChainProcurementRoles"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EscalationChainProcurementRolesProperty : ChildEntityProperty<EscalationChainProcurementRolesChildEntityTypes>
	{
	}
	
	#endregion EscalationChainProcurementRolesProperty
}

