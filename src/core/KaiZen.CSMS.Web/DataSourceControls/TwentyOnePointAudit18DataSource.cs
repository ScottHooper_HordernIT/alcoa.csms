﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.TwentyOnePointAudit18Provider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(TwentyOnePointAudit18DataSourceDesigner))]
	public class TwentyOnePointAudit18DataSource : ProviderDataSource<TwentyOnePointAudit18, TwentyOnePointAudit18Key>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit18DataSource class.
		/// </summary>
		public TwentyOnePointAudit18DataSource() : base(new TwentyOnePointAudit18Service())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the TwentyOnePointAudit18DataSourceView used by the TwentyOnePointAudit18DataSource.
		/// </summary>
		protected TwentyOnePointAudit18DataSourceView TwentyOnePointAudit18View
		{
			get { return ( View as TwentyOnePointAudit18DataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the TwentyOnePointAudit18DataSource control invokes to retrieve data.
		/// </summary>
		public TwentyOnePointAudit18SelectMethod SelectMethod
		{
			get
			{
				TwentyOnePointAudit18SelectMethod selectMethod = TwentyOnePointAudit18SelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (TwentyOnePointAudit18SelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the TwentyOnePointAudit18DataSourceView class that is to be
		/// used by the TwentyOnePointAudit18DataSource.
		/// </summary>
		/// <returns>An instance of the TwentyOnePointAudit18DataSourceView class.</returns>
		protected override BaseDataSourceView<TwentyOnePointAudit18, TwentyOnePointAudit18Key> GetNewDataSourceView()
		{
			return new TwentyOnePointAudit18DataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the TwentyOnePointAudit18DataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class TwentyOnePointAudit18DataSourceView : ProviderDataSourceView<TwentyOnePointAudit18, TwentyOnePointAudit18Key>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit18DataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the TwentyOnePointAudit18DataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public TwentyOnePointAudit18DataSourceView(TwentyOnePointAudit18DataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal TwentyOnePointAudit18DataSource TwentyOnePointAudit18Owner
		{
			get { return Owner as TwentyOnePointAudit18DataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal TwentyOnePointAudit18SelectMethod SelectMethod
		{
			get { return TwentyOnePointAudit18Owner.SelectMethod; }
			set { TwentyOnePointAudit18Owner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal TwentyOnePointAudit18Service TwentyOnePointAudit18Provider
		{
			get { return Provider as TwentyOnePointAudit18Service; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<TwentyOnePointAudit18> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<TwentyOnePointAudit18> results = null;
			TwentyOnePointAudit18 item;
			count = 0;
			
			System.Int32 _id;

			switch ( SelectMethod )
			{
				case TwentyOnePointAudit18SelectMethod.Get:
					TwentyOnePointAudit18Key entityKey  = new TwentyOnePointAudit18Key();
					entityKey.Load(values);
					item = TwentyOnePointAudit18Provider.Get(entityKey);
					results = new TList<TwentyOnePointAudit18>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case TwentyOnePointAudit18SelectMethod.GetAll:
                    results = TwentyOnePointAudit18Provider.GetAll(StartIndex, PageSize, out count);
                    break;
				case TwentyOnePointAudit18SelectMethod.GetPaged:
					results = TwentyOnePointAudit18Provider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case TwentyOnePointAudit18SelectMethod.Find:
					if ( FilterParameters != null )
						results = TwentyOnePointAudit18Provider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = TwentyOnePointAudit18Provider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case TwentyOnePointAudit18SelectMethod.GetById:
					_id = ( values["Id"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["Id"], typeof(System.Int32)) : (int)0;
					item = TwentyOnePointAudit18Provider.GetById(_id);
					results = new TList<TwentyOnePointAudit18>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				// FK
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == TwentyOnePointAudit18SelectMethod.Get || SelectMethod == TwentyOnePointAudit18SelectMethod.GetById )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				TwentyOnePointAudit18 entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					TwentyOnePointAudit18Provider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<TwentyOnePointAudit18> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			TwentyOnePointAudit18Provider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region TwentyOnePointAudit18DataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the TwentyOnePointAudit18DataSource class.
	/// </summary>
	public class TwentyOnePointAudit18DataSourceDesigner : ProviderDataSourceDesigner<TwentyOnePointAudit18, TwentyOnePointAudit18Key>
	{
		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit18DataSourceDesigner class.
		/// </summary>
		public TwentyOnePointAudit18DataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public TwentyOnePointAudit18SelectMethod SelectMethod
		{
			get { return ((TwentyOnePointAudit18DataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new TwentyOnePointAudit18DataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region TwentyOnePointAudit18DataSourceActionList

	/// <summary>
	/// Supports the TwentyOnePointAudit18DataSourceDesigner class.
	/// </summary>
	internal class TwentyOnePointAudit18DataSourceActionList : DesignerActionList
	{
		private TwentyOnePointAudit18DataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit18DataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public TwentyOnePointAudit18DataSourceActionList(TwentyOnePointAudit18DataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public TwentyOnePointAudit18SelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion TwentyOnePointAudit18DataSourceActionList
	
	#endregion TwentyOnePointAudit18DataSourceDesigner
	
	#region TwentyOnePointAudit18SelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the TwentyOnePointAudit18DataSource.SelectMethod property.
	/// </summary>
	public enum TwentyOnePointAudit18SelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetById method.
		/// </summary>
		GetById
	}
	
	#endregion TwentyOnePointAudit18SelectMethod

	#region TwentyOnePointAudit18Filter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit18"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAudit18Filter : SqlFilter<TwentyOnePointAudit18Column>
	{
	}
	
	#endregion TwentyOnePointAudit18Filter

	#region TwentyOnePointAudit18ExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit18"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAudit18ExpressionBuilder : SqlExpressionBuilder<TwentyOnePointAudit18Column>
	{
	}
	
	#endregion TwentyOnePointAudit18ExpressionBuilder	

	#region TwentyOnePointAudit18Property
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;TwentyOnePointAudit18ChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit18"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAudit18Property : ChildEntityProperty<TwentyOnePointAudit18ChildEntityTypes>
	{
	}
	
	#endregion TwentyOnePointAudit18Property
}

