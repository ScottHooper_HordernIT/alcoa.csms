﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="QuestionnaireCommentsAuditProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class QuestionnaireCommentsAuditProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.QuestionnaireCommentsAudit, KaiZen.CSMS.Entities.QuestionnaireCommentsAuditKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.QuestionnaireCommentsAuditKey key)
		{
			return Delete(transactionManager, key.QuestionnaireCommenAudittId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_questionnaireCommenAudittId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _questionnaireCommenAudittId)
		{
			return Delete(null, _questionnaireCommenAudittId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireCommenAudittId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _questionnaireCommenAudittId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.QuestionnaireCommentsAudit Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.QuestionnaireCommentsAuditKey key, int start, int pageLength)
		{
			return GetByQuestionnaireCommenAudittId(transactionManager, key.QuestionnaireCommenAudittId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_QuestionnaireCommentsAudit index.
		/// </summary>
		/// <param name="_questionnaireCommenAudittId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireCommentsAudit"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireCommentsAudit GetByQuestionnaireCommenAudittId(System.Int32 _questionnaireCommenAudittId)
		{
			int count = -1;
			return GetByQuestionnaireCommenAudittId(null,_questionnaireCommenAudittId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireCommentsAudit index.
		/// </summary>
		/// <param name="_questionnaireCommenAudittId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireCommentsAudit"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireCommentsAudit GetByQuestionnaireCommenAudittId(System.Int32 _questionnaireCommenAudittId, int start, int pageLength)
		{
			int count = -1;
			return GetByQuestionnaireCommenAudittId(null, _questionnaireCommenAudittId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireCommentsAudit index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireCommenAudittId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireCommentsAudit"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireCommentsAudit GetByQuestionnaireCommenAudittId(TransactionManager transactionManager, System.Int32 _questionnaireCommenAudittId)
		{
			int count = -1;
			return GetByQuestionnaireCommenAudittId(transactionManager, _questionnaireCommenAudittId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireCommentsAudit index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireCommenAudittId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireCommentsAudit"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireCommentsAudit GetByQuestionnaireCommenAudittId(TransactionManager transactionManager, System.Int32 _questionnaireCommenAudittId, int start, int pageLength)
		{
			int count = -1;
			return GetByQuestionnaireCommenAudittId(transactionManager, _questionnaireCommenAudittId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireCommentsAudit index.
		/// </summary>
		/// <param name="_questionnaireCommenAudittId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireCommentsAudit"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireCommentsAudit GetByQuestionnaireCommenAudittId(System.Int32 _questionnaireCommenAudittId, int start, int pageLength, out int count)
		{
			return GetByQuestionnaireCommenAudittId(null, _questionnaireCommenAudittId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireCommentsAudit index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireCommenAudittId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireCommentsAudit"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.QuestionnaireCommentsAudit GetByQuestionnaireCommenAudittId(TransactionManager transactionManager, System.Int32 _questionnaireCommenAudittId, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;QuestionnaireCommentsAudit&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;QuestionnaireCommentsAudit&gt;"/></returns>
		public static TList<QuestionnaireCommentsAudit> Fill(IDataReader reader, TList<QuestionnaireCommentsAudit> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.QuestionnaireCommentsAudit c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("QuestionnaireCommentsAudit")
					.Append("|").Append((System.Int32)reader[((int)QuestionnaireCommentsAuditColumn.QuestionnaireCommenAudittId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<QuestionnaireCommentsAudit>(
					key.ToString(), // EntityTrackingKey
					"QuestionnaireCommentsAudit",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.QuestionnaireCommentsAudit();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.QuestionnaireCommenAudittId = (System.Int32)reader[((int)QuestionnaireCommentsAuditColumn.QuestionnaireCommenAudittId - 1)];
					c.QuestionnaireCommentId = (reader.IsDBNull(((int)QuestionnaireCommentsAuditColumn.QuestionnaireCommentId - 1)))?null:(System.Int32?)reader[((int)QuestionnaireCommentsAuditColumn.QuestionnaireCommentId - 1)];
					c.QuestionnaireId = (reader.IsDBNull(((int)QuestionnaireCommentsAuditColumn.QuestionnaireId - 1)))?null:(System.Int32?)reader[((int)QuestionnaireCommentsAuditColumn.QuestionnaireId - 1)];
					c.Comment = (reader.IsDBNull(((int)QuestionnaireCommentsAuditColumn.Comment - 1)))?null:(System.String)reader[((int)QuestionnaireCommentsAuditColumn.Comment - 1)];
					c.CreatedByUserId = (reader.IsDBNull(((int)QuestionnaireCommentsAuditColumn.CreatedByUserId - 1)))?null:(System.Int32?)reader[((int)QuestionnaireCommentsAuditColumn.CreatedByUserId - 1)];
					c.ModifiedByUserId = (reader.IsDBNull(((int)QuestionnaireCommentsAuditColumn.ModifiedByUserId - 1)))?null:(System.Int32?)reader[((int)QuestionnaireCommentsAuditColumn.ModifiedByUserId - 1)];
					c.CreatedDate = (reader.IsDBNull(((int)QuestionnaireCommentsAuditColumn.CreatedDate - 1)))?null:(System.DateTime?)reader[((int)QuestionnaireCommentsAuditColumn.CreatedDate - 1)];
					c.ModifiedDate = (reader.IsDBNull(((int)QuestionnaireCommentsAuditColumn.ModifiedDate - 1)))?null:(System.DateTime?)reader[((int)QuestionnaireCommentsAuditColumn.ModifiedDate - 1)];
					c.AuditedOn = (System.DateTime)reader[((int)QuestionnaireCommentsAuditColumn.AuditedOn - 1)];
					c.AuditEventId = (System.String)reader[((int)QuestionnaireCommentsAuditColumn.AuditEventId - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.QuestionnaireCommentsAudit"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.QuestionnaireCommentsAudit"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.QuestionnaireCommentsAudit entity)
		{
			if (!reader.Read()) return;
			
			entity.QuestionnaireCommenAudittId = (System.Int32)reader[((int)QuestionnaireCommentsAuditColumn.QuestionnaireCommenAudittId - 1)];
			entity.QuestionnaireCommentId = (reader.IsDBNull(((int)QuestionnaireCommentsAuditColumn.QuestionnaireCommentId - 1)))?null:(System.Int32?)reader[((int)QuestionnaireCommentsAuditColumn.QuestionnaireCommentId - 1)];
			entity.QuestionnaireId = (reader.IsDBNull(((int)QuestionnaireCommentsAuditColumn.QuestionnaireId - 1)))?null:(System.Int32?)reader[((int)QuestionnaireCommentsAuditColumn.QuestionnaireId - 1)];
			entity.Comment = (reader.IsDBNull(((int)QuestionnaireCommentsAuditColumn.Comment - 1)))?null:(System.String)reader[((int)QuestionnaireCommentsAuditColumn.Comment - 1)];
			entity.CreatedByUserId = (reader.IsDBNull(((int)QuestionnaireCommentsAuditColumn.CreatedByUserId - 1)))?null:(System.Int32?)reader[((int)QuestionnaireCommentsAuditColumn.CreatedByUserId - 1)];
			entity.ModifiedByUserId = (reader.IsDBNull(((int)QuestionnaireCommentsAuditColumn.ModifiedByUserId - 1)))?null:(System.Int32?)reader[((int)QuestionnaireCommentsAuditColumn.ModifiedByUserId - 1)];
			entity.CreatedDate = (reader.IsDBNull(((int)QuestionnaireCommentsAuditColumn.CreatedDate - 1)))?null:(System.DateTime?)reader[((int)QuestionnaireCommentsAuditColumn.CreatedDate - 1)];
			entity.ModifiedDate = (reader.IsDBNull(((int)QuestionnaireCommentsAuditColumn.ModifiedDate - 1)))?null:(System.DateTime?)reader[((int)QuestionnaireCommentsAuditColumn.ModifiedDate - 1)];
			entity.AuditedOn = (System.DateTime)reader[((int)QuestionnaireCommentsAuditColumn.AuditedOn - 1)];
			entity.AuditEventId = (System.String)reader[((int)QuestionnaireCommentsAuditColumn.AuditEventId - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.QuestionnaireCommentsAudit"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.QuestionnaireCommentsAudit"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.QuestionnaireCommentsAudit entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.QuestionnaireCommenAudittId = (System.Int32)dataRow["QuestionnaireCommenAudittId"];
			entity.QuestionnaireCommentId = Convert.IsDBNull(dataRow["QuestionnaireCommentId"]) ? null : (System.Int32?)dataRow["QuestionnaireCommentId"];
			entity.QuestionnaireId = Convert.IsDBNull(dataRow["QuestionnaireId"]) ? null : (System.Int32?)dataRow["QuestionnaireId"];
			entity.Comment = Convert.IsDBNull(dataRow["Comment"]) ? null : (System.String)dataRow["Comment"];
			entity.CreatedByUserId = Convert.IsDBNull(dataRow["CreatedByUserId"]) ? null : (System.Int32?)dataRow["CreatedByUserId"];
			entity.ModifiedByUserId = Convert.IsDBNull(dataRow["ModifiedByUserId"]) ? null : (System.Int32?)dataRow["ModifiedByUserId"];
			entity.CreatedDate = Convert.IsDBNull(dataRow["CreatedDate"]) ? null : (System.DateTime?)dataRow["CreatedDate"];
			entity.ModifiedDate = Convert.IsDBNull(dataRow["ModifiedDate"]) ? null : (System.DateTime?)dataRow["ModifiedDate"];
			entity.AuditedOn = (System.DateTime)dataRow["AuditedOn"];
			entity.AuditEventId = (System.String)dataRow["AuditEventId"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.QuestionnaireCommentsAudit"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.QuestionnaireCommentsAudit Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.QuestionnaireCommentsAudit entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.QuestionnaireCommentsAudit object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.QuestionnaireCommentsAudit instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.QuestionnaireCommentsAudit Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.QuestionnaireCommentsAudit entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region QuestionnaireCommentsAuditChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.QuestionnaireCommentsAudit</c>
	///</summary>
	public enum QuestionnaireCommentsAuditChildEntityTypes
	{
	}
	
	#endregion QuestionnaireCommentsAuditChildEntityTypes
	
	#region QuestionnaireCommentsAuditFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;QuestionnaireCommentsAuditColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireCommentsAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireCommentsAuditFilterBuilder : SqlFilterBuilder<QuestionnaireCommentsAuditColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireCommentsAuditFilterBuilder class.
		/// </summary>
		public QuestionnaireCommentsAuditFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireCommentsAuditFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireCommentsAuditFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireCommentsAuditFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireCommentsAuditFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireCommentsAuditFilterBuilder
	
	#region QuestionnaireCommentsAuditParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;QuestionnaireCommentsAuditColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireCommentsAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireCommentsAuditParameterBuilder : ParameterizedSqlFilterBuilder<QuestionnaireCommentsAuditColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireCommentsAuditParameterBuilder class.
		/// </summary>
		public QuestionnaireCommentsAuditParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireCommentsAuditParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireCommentsAuditParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireCommentsAuditParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireCommentsAuditParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireCommentsAuditParameterBuilder
	
	#region QuestionnaireCommentsAuditSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;QuestionnaireCommentsAuditColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireCommentsAudit"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class QuestionnaireCommentsAuditSortBuilder : SqlSortBuilder<QuestionnaireCommentsAuditColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireCommentsAuditSqlSortBuilder class.
		/// </summary>
		public QuestionnaireCommentsAuditSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion QuestionnaireCommentsAuditSortBuilder
	
} // end namespace
