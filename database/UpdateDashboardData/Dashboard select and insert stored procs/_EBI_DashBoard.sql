

 Object  StoredProcedure [dbo].[_EBI_DashBoard]    Script Date 3082015 13652 PM 
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[_EBI_DashBoard]                                    
(                                    
 @SiteId int,                                    
 @Month int,                                    
 @Year int                                    
)                                    
AS                
DECLARE @vSiteId INT, @vMonth INT, @vYear INT                  
SET @vSiteId = @SiteId                  
SET @vMonth = @Month                  
SET @vYear = @Year                
                
DECLARE @YesSQCurrent INT, @NoCurrentSQNotFound INT, @NoSQExpired INT,                
@NoAccesstoSiteNotGranted INT,@NoSafetyQualificationNotfound INT, @NoInformationCouldNnotBeFound INT,                 
@CompanyhasvalidSQExemption INT                
                
--DECLARE @cDay varchar(2),@cMonth varchar(2), @cYear varchar(4)                
                
--SELECT @cDay = [dbo].[GetDayByMonthYearDB](@vMonth, @vYear)                
--SET  @cMonth = CAST(@Month AS VARCHAR(2))                
--SET  @cYear = CAST(@Year AS VARCHAR(4))                  
                    
IF (@SiteId  0)                                   
 BEGIN                        
                       
  SELECT @YesSQCurrent = COUNT(DISTINCT A.CompanyId)FROM dbo.Questionnaire AS A                       
  LEFT OUTER JOIN QuestionnaireReportExpiry B                
  ON A.QuestionnaireId = B.QuestionnaireId                 
  LEFT OUTER JOIN dbo.QuestionnairePresentlyWithAction AS C                
  ON B.QuestionnairePresentlyWithActionId = C.QuestionnairePresentlyWithActionId                   
  LEFT OUTER JOIN dbo.CompanySiteCategoryStandard D                 
  ON D.CompanyId = A.CompanyId                 
  LEFT OUTER JOIN Kpi E                 
  ON E.CompanyId = A.CompanyId AND E.SiteId = D.SiteId                
  WHERE C.ActionName = 'ProcurementExpiring'                
  AND E.EbiOnSite = 1                
  AND D.CompanySiteCategoryId IS NOT NULL                
  AND D.Approved = 1                
  AND D.SiteId = @vSiteId                
  AND YEAR(E.kpiDateTime) = @vYear                    
  AND MONTH(E.kpiDateTime) = @vMonth                
  AND YEAR(GETDATE()) = @vYear                    
  AND MONTH(GETDATE()) = @vMonth                
  ------------------------                
  SELECT @NoCurrentSQNotFound = COUNT(DISTINCT A.CompanyId)FROM dbo.Questionnaire AS A                       
  LEFT OUTER JOIN dbo.CompanySiteCategoryStandard D                 
  ON D.CompanyId = A.CompanyId                 
  LEFT OUTER JOIN Kpi E                 
  ON E.CompanyId = A.CompanyId AND E.SiteId = D.SiteId                
  WHERE E.EbiOnSite = 1                 
  AND A.MainAssessmentValidTo IS NULL                
  AND D.SiteId = @vSiteId                
  AND YEAR(E.kpiDateTime) = @vYear                    
  AND MONTH(E.kpiDateTime) = @vMonth               
  AND A.QuestionnaireId = (SELECT MAX(QuestionnaireId) FROM dbo.Questionnaire WHERE CompanyId = A.CompanyId)                 
  AND YEAR(GETDATE()) = @vYear                    
  AND MONTH(GETDATE()) = @vMonth                 
  ------------------------                
  SELECT @NoSQExpired  = COUNT(DISTINCT A.CompanyId)FROM dbo.Questionnaire AS A                       
  LEFT OUTER JOIN dbo.CompanySiteCategoryStandard D                 
  ON D.CompanyId = A.CompanyId                 
  LEFT OUTER JOIN Kpi E                 
  ON E.CompanyId = A.CompanyId AND E.SiteId = D.SiteId                
  WHERE E.EbiOnSite = 1                
  AND D.SiteId = @vSiteId                
  AND YEAR(E.kpiDateTime) = @vYear                    
  AND MONTH(E.kpiDateTime) = @vMonth                
  AND A.QuestionnaireId = (SELECT MAX(QuestionnaireId) FROM dbo.Questionnaire WHERE CompanyId = A.CompanyId)                 
  --AND YEAR(CreatedDate) = @vYear                 
  --AND MONTH(CreatedDate) = @vMonth)                
  AND DATEDIFF(DAY, A.MainAssessmentValidTo, CONVERT(DATETIME, GETDATE(), 103))0             
  AND YEAR(GETDATE()) = @vYear        
  AND MONTH(GETDATE()) = @vMonth                
  ------------------------                
  SELECT @NoAccesstoSiteNotGranted = COUNT(DISTINCT A.CompanyId)FROM dbo.Questionnaire AS A                       
  LEFT OUTER JOIN dbo.CompanySiteCategoryStandard D                 
  ON D.CompanyId = A.CompanyId                 
  LEFT OUTER JOIN Kpi E                 
  ON E.CompanyId = A.CompanyId AND E.SiteId = D.SiteId                
  WHERE E.EbiOnSite = 1                
  AND D.SiteId = @vSiteId                
  AND YEAR(E.kpiDateTime) = @vYear                    
  AND MONTH(E.kpiDateTime) = @vMonth                
  AND A.QuestionnaireId = (SELECT MAX(QuestionnaireId) FROM dbo.Questionnaire WHERE CompanyId = A.CompanyId)                 
  --AND YEAR(ModifiedDate) = @vYear                 
  --AND MONTH(ModifiedDate) = @vMonth)                
  AND DATEDIFF(DAY, A.MainAssessmentValidTo, CONVERT(DATETIME, GETDATE(), 103))=0                
  AND (D.Approved = 0 OR D.Approved IS NULL)            
  AND YEAR(GETDATE()) = @vYear                    
  AND MONTH(GETDATE()) = @vMonth                 
  ------------------------                
  SELECT @NoSafetyQualificationNotfound  = COUNT(DISTINCT A.CompanyId)FROM Companies AS A                       
  LEFT OUTER JOIN dbo.CompanySiteCategoryStandard D                 
  ON D.CompanyId = A.CompanyId                 
  LEFT OUTER JOIN Kpi E                 
  ON E.CompanyId = A.CompanyId AND E.SiteId = D.SiteId                
  LEFT OUTER JOIN dbo.Questionnaire F                
  ON F.CompanyId = A.CompanyId                
  WHERE E.EbiOnSite = 1                
  AND D.SiteId = @vSiteId                
  AND YEAR(E.kpiDateTime) = @vYear                    
  AND MONTH(E.kpiDateTime) = @vMonth                
  AND F.QuestionnaireId IS NULL             
  AND YEAR(GETDATE()) = @vYear                    
  AND MONTH(GETDATE()) = @vMonth                
  ------------------------ 
	SELECT  @NoInformationCouldNnotBeFound  = COUNT(DISTINCT companyName) FROM 
	(SELECT DISTINCT companyName, vendor_number FROM EBI A
	WHERE  A.SwipeSite = (SELECT SiteNameEbi FROM Sites WHERE SiteId = @vSiteId)                
	AND  YEAR(A.SwipeDateTime) = @vYear    
	AND MONTH(A.SwipeDateTime) =  @vMonth    
	AND A.CompanyName IS NOT NULL                
	AND A.CompanyName '' and vendor_Number is not null and vendor_number  '') T1 inner  Join    
	(select vendor_number FROM CompanyVendor B WHERE cast(B.TAX_REGISTRATION_NUMBER AS CHAR(11)) NOT IN (
	SELECT CompanyAbn FROM Companies C))  T2
	ON T1.Vendor_Number = T2.Vendor_Number
  ------------------------                
  SELECT @CompanyhasvalidSQExemption  = COUNT(DISTINCT A.CompanyId)FROM Companies AS A                       
  LEFT OUTER JOIN dbo.CompanySiteCategoryStandard D                 
  ON D.CompanyId = A.CompanyId                 
  LEFT OUTER JOIN Kpi E                 
  ON E.CompanyId = A.CompanyId AND E.SiteId = D.SiteId                
  LEFT OUTER JOIN SqExemption_Friendly_All F                
  ON F.CompanyId = A.CompanyId                
  WHERE E.EbiOnSite = 1                
  AND D.SiteId = @vSiteId                
  AND YEAR(E.kpiDateTime) = @vYear                    
  AND MONTH(E.kpiDateTime) = @vMonth                
  AND (E.kpiDateTime BETWEEN F.ValidFrom AND F.ValidTo)             
  AND YEAR(GETDATE()) = @vYear                    
  AND MONTH(GETDATE()) = @vMonth                
                  
  SELECT @YesSQCurrent  AS [Yes - SQ Current],                 
  @NoCurrentSQNotFound  AS [No - Current SQ not found],                
  @NoSQExpired  AS  [No - SQ Expired],                
  @NoAccesstoSiteNotGranted  AS [No - Access to Site Not Granted],                
  @NoSafetyQualificationNotfound  AS [No - Safety Qualification not found],                     @NoInformationCouldNnotBeFound  AS  [No - Information could not be found],                
  @CompanyhasvalidSQExemption  AS  [Company has a valid SQ Exemption]                
 END                            
ELSE                 
 BEGIN                        
  SELECT @YesSQCurrent = COUNT(DISTINCT A.CompanyId)FROM dbo.Questionnaire AS A                       
  LEFT OUTER JOIN QuestionnaireReportExpiry B                
  ON A.QuestionnaireId = B.QuestionnaireId                 
  LEFT OUTER JOIN dbo.QuestionnairePresentlyWithAction AS C                
  ON B.QuestionnairePresentlyWithActionId = C.QuestionnairePresentlyWithActionId                   
  LEFT OUTER JOIN dbo.CompanySiteCategoryStandard D                 
  ON D.CompanyId = A.CompanyId                 
  LEFT OUTER JOIN Kpi E                 
  ON E.CompanyId = A.CompanyId AND E.SiteId = D.SiteId                
  WHERE C.ActionName = 'ProcurementExpiring'                
  AND E.EbiOnSite = 1              
  AND D.CompanySiteCategoryId IS NOT NULL                
  AND D.Approved = 1                
  AND ((Select COUNT() FROM RegionsSites WHERE (-1  dbo.RegionsSites.RegionId) = @vSiteId and dbo.RegionsSites.SiteId = D.SiteId)  0)              
  AND YEAR(E.kpiDateTime) = @vYear                    
  AND MONTH(E.kpiDateTime) = @vMonth             
  AND YEAR(GETDATE()) = @vYear                    
  AND MONTH(GETDATE()) = @vMonth                
  ------------------------                
  SELECT @NoCurrentSQNotFound = COUNT(DISTINCT A.CompanyId)FROM dbo.Questionnaire AS A                       
  LEFT OUTER JOIN dbo.CompanySiteCategoryStandard D                 
  ON D.CompanyId = A.CompanyId                 
  LEFT OUTER JOIN Kpi E                 
  ON E.CompanyId = A.CompanyId AND E.SiteId = D.SiteId                
  WHERE E.EbiOnSite = 1                 
  AND A.MainAssessmentValidTo IS NULL                
  AND ((Select COUNT() FROM RegionsSites WHERE (-1  dbo.RegionsSites.RegionId) = @vSiteId and dbo.RegionsSites.SiteId = D.SiteId)  0)              
  AND YEAR(E.kpiDateTime) = @vYear                    
  AND MONTH(E.kpiDateTime) = @vMonth               
  AND A.QuestionnaireId = (SELECT MAX(QuestionnaireId) FROM dbo.Questionnaire WHERE CompanyId = A.CompanyId)                 
  AND YEAR(GETDATE()) = @vYear                    
  AND MONTH(GETDATE()) = @vMonth                 
  ------------------------                
  SELECT @NoSQExpired  = COUNT(DISTINCT A.CompanyId)FROM dbo.Questionnaire AS A                       
  LEFT OUTER JOIN dbo.CompanySiteCategoryStandard D                 
  ON D.CompanyId = A.CompanyId                 
  LEFT OUTER JOIN Kpi E                 
  ON E.CompanyId = A.CompanyId AND E.SiteId = D.SiteId                
  WHERE E.EbiOnSite = 1                
  AND ((Select COUNT() FROM RegionsSites WHERE (-1  dbo.RegionsSites.RegionId) = @vSiteId and dbo.RegionsSites.SiteId = D.SiteId)  0)              
  AND YEAR(E.kpiDateTime) = @vYear                    
  AND MONTH(E.kpiDateTime) = @vMonth                
  AND A.QuestionnaireId = (SELECT MAX(QuestionnaireId) FROM dbo.Questionnaire WHERE CompanyId = A.CompanyId)                 
  --AND YEAR(CreatedDate) = @vYear                 
  --AND MONTH(CreatedDate) = @vMonth)                
  AND DATEDIFF(DAY, A.MainAssessmentValidTo, CONVERT(DATETIME, GETDATE(), 103))0            
  AND YEAR(GETDATE()) = @vYear                    
  AND MONTH(GETDATE()) = @vMonth                 
  ------------------------                
  SELECT @NoAccesstoSiteNotGranted = COUNT(DISTINCT A.CompanyId)FROM dbo.Questionnaire AS A                       
  LEFT OUTER JOIN dbo.CompanySiteCategoryStandard D                 
  ON D.CompanyId = A.CompanyId                 
  LEFT OUTER JOIN Kpi E                 
  ON E.CompanyId = A.CompanyId AND E.SiteId = D.SiteId                
  WHERE E.EbiOnSite = 1                
  AND ((Select COUNT() FROM RegionsSites WHERE (-1  dbo.RegionsSites.RegionId) = @vSiteId and dbo.RegionsSites.SiteId = D.SiteId)  0)              
  AND YEAR(E.kpiDateTime) = @vYear                    
  AND MONTH(E.kpiDateTime) = @vMonth                
  AND A.QuestionnaireId = (SELECT MAX(QuestionnaireId) FROM dbo.Questionnaire WHERE CompanyId = A.CompanyId)                 
  --AND YEAR(ModifiedDate) = @vYear                 
  --AND MONTH(ModifiedDate) = @vMonth)                
  AND DATEDIFF(DAY, A.MainAssessmentValidTo, CONVERT(DATETIME, GETDATE(), 103))=0                
  AND (D.Approved = 0 OR D.Approved IS NULL)            
  AND YEAR(GETDATE()) = @vYear                    
  AND MONTH(GETDATE()) = @vMonth             
  ------------------------                
  SELECT @NoSafetyQualificationNotfound  = COUNT(DISTINCT A.CompanyId)FROM Companies AS A                       
  LEFT OUTER JOIN dbo.CompanySiteCategoryStandard D                 
  ON D.CompanyId = A.CompanyId                 
  LEFT OUTER JOIN Kpi E                 
  ON E.CompanyId = A.CompanyId AND E.SiteId = D.SiteId                
  LEFT OUTER JOIN dbo.Questionnaire F                
  ON F.CompanyId = A.CompanyId                
  WHERE E.EbiOnSite = 1                
  AND ((Select COUNT() FROM RegionsSites WHERE (-1  dbo.RegionsSites.RegionId) = @vSiteId and dbo.RegionsSites.SiteId = D.SiteId)  0)              
  AND YEAR(E.kpiDateTime) = @vYear                    
  AND MONTH(E.kpiDateTime) = @vMonth                
  AND F.QuestionnaireId IS NULL             
  AND YEAR(GETDATE()) = @vYear                    
  AND MONTH(GETDATE()) = @vMonth               
  ------------------------ 
	SELECT  @NoInformationCouldNnotBeFound  = COUNT(DISTINCT companyName) FROM 
	(SELECT DISTINCT companyName, vendor_number FROM EBI A
	WHERE  A.SwipeSite IN (SELECT SiteNameEbi FROM Sites WHERE SiteId IN (SELECT SITEID FROM RegionsSites where RegionId = -1  @vSiteId) and sitenameEbi is not null)
	AND  YEAR(A.SwipeDateTime) = @vYear    
	AND MONTH(A.SwipeDateTime) =  @vMonth   
	AND A.CompanyName IS NOT NULL                
	AND A.CompanyName '' and vendor_Number is not null and vendor_number  '') T1 inner  Join    
	(select vendor_number FROM CompanyVendor B WHERE cast(B.TAX_REGISTRATION_NUMBER AS CHAR(11)) NOT IN (
	SELECT CompanyAbn FROM Companies C))  T2
	ON T1.Vendor_Number = T2.Vendor_Number               
  ------------------------                
  SELECT @CompanyhasvalidSQExemption  = COUNT(DISTINCT A.CompanyId)FROM Companies AS A                       
  LEFT OUTER JOIN dbo.CompanySiteCategoryStandard D                 
  ON D.CompanyId = A.CompanyId                 
  LEFT OUTER JOIN Kpi E                 
  ON E.CompanyId = A.CompanyId AND E.SiteId = D.SiteId                
  LEFT OUTER JOIN SqExemption_Friendly_All F                
  ON F.CompanyId = A.CompanyId                
  WHERE E.EbiOnSite = 1                
  AND ((Select COUNT() FROM RegionsSites WHERE (-1  dbo.RegionsSites.RegionId) = @vSiteId and dbo.RegionsSites.SiteId = D.SiteId)  0)                
  AND YEAR(E.kpiDateTime) = @vYear                    
  AND MONTH(E.kpiDateTime) = @vMonth                
  AND (E.kpiDateTime BETWEEN F.ValidFrom AND F.ValidTo)            
  AND YEAR(GETDATE()) = @vYear                    
  AND MONTH(GETDATE()) = @vMonth                
                  
  SELECT @YesSQCurrent  AS [Yes - SQ Current],                 
  @NoCurrentSQNotFound  AS [No - Current SQ not found],                
  @NoSQExpired  AS  [No - SQ Expired],                
  @NoAccesstoSiteNotGranted  AS [No - Access to Site Not Granted],                
  @NoSafetyQualificationNotfound  AS [No - Safety Qualification not found],                   
  @NoInformationCouldNnotBeFound  AS  [No - Information could not be found],       
  @CompanyhasvalidSQExemption  AS  [Company has a valid SQ Exemption]                
 END                           
----------------------------------------  
GO


