﻿#region Using directives

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="AdminTaskEmailTemplateListProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract class AdminTaskEmailTemplateListProviderBaseCore : EntityViewProviderBase<AdminTaskEmailTemplateList>
	{
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions
		
		/*
		///<summary>
		/// Fill an VList&lt;AdminTaskEmailTemplateList&gt; From a DataSet
		///</summary>
		/// <param name="dataSet">the DataSet</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList&lt;AdminTaskEmailTemplateList&gt;"/></returns>
		protected static VList&lt;AdminTaskEmailTemplateList&gt; Fill(DataSet dataSet, VList<AdminTaskEmailTemplateList> rows, int start, int pagelen)
		{
			if (dataSet.Tables.Count == 1)
			{
				return Fill(dataSet.Tables[0], rows, start, pagelen);
			}
			else
			{
				return new VList<AdminTaskEmailTemplateList>();
			}	
		}
		
		
		///<summary>
		/// Fill an VList&lt;AdminTaskEmailTemplateList&gt; From a DataTable
		///</summary>
		/// <param name="dataTable">the DataTable that hold the data.</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList<AdminTaskEmailTemplateList>"/></returns>
		protected static VList&lt;AdminTaskEmailTemplateList&gt; Fill(DataTable dataTable, VList<AdminTaskEmailTemplateList> rows, int start, int pagelen)
		{
			int recordnum = 0;
			
			System.Collections.IEnumerator dataRows =  dataTable.Rows.GetEnumerator();
			
			while (dataRows.MoveNext() && (pagelen != 0))
			{
				if(recordnum >= start)
				{
					DataRow row = (DataRow)dataRows.Current;
				
					AdminTaskEmailTemplateList c = new AdminTaskEmailTemplateList();
					c.AdminTaskEmailTemplateId = (Convert.IsDBNull(row["AdminTaskEmailTemplateId"]))?(int)0:(System.Int32)row["AdminTaskEmailTemplateId"];
					c.AdminTaskTypeId = (Convert.IsDBNull(row["AdminTaskTypeId"]))?(int)0:(System.Int32)row["AdminTaskTypeId"];
					c.AdminTaskTypeName = (Convert.IsDBNull(row["AdminTaskTypeName"]))?string.Empty:(System.String)row["AdminTaskTypeName"];
					c.AdminTaskTypeDesc = (Convert.IsDBNull(row["AdminTaskTypeDesc"]))?string.Empty:(System.String)row["AdminTaskTypeDesc"];
					c.EmailSubject = (Convert.IsDBNull(row["EmailSubject"]))?string.Empty:(System.String)row["EmailSubject"];
					c.AcceptChanges();
					rows.Add(c);
					pagelen -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		*/	
						
		///<summary>
		/// Fill an <see cref="VList&lt;AdminTaskEmailTemplateList&gt;"/> From a DataReader.
		///</summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pageLength">number of row.</param>
		///<returns>a <see cref="VList&lt;AdminTaskEmailTemplateList&gt;"/></returns>
		protected VList<AdminTaskEmailTemplateList> Fill(IDataReader reader, VList<AdminTaskEmailTemplateList> rows, int start, int pageLength)
		{
			int recordnum = 0;
			while (reader.Read() && (pageLength != 0))
			{
				if(recordnum >= start)
				{
					AdminTaskEmailTemplateList entity = null;
					if (DataRepository.Provider.UseEntityFactory)
					{
						entity = EntityManager.CreateViewEntity<AdminTaskEmailTemplateList>("AdminTaskEmailTemplateList",  DataRepository.Provider.EntityCreationalFactoryType); 
					}
					else
					{
						entity = new AdminTaskEmailTemplateList();
					}
					
					entity.SuppressEntityEvents = true;

					entity.AdminTaskEmailTemplateId = (System.Int32)reader[((int)AdminTaskEmailTemplateListColumn.AdminTaskEmailTemplateId)];
					//entity.AdminTaskEmailTemplateId = (Convert.IsDBNull(reader["AdminTaskEmailTemplateId"]))?(int)0:(System.Int32)reader["AdminTaskEmailTemplateId"];
					entity.AdminTaskTypeId = (System.Int32)reader[((int)AdminTaskEmailTemplateListColumn.AdminTaskTypeId)];
					//entity.AdminTaskTypeId = (Convert.IsDBNull(reader["AdminTaskTypeId"]))?(int)0:(System.Int32)reader["AdminTaskTypeId"];
					entity.AdminTaskTypeName = (System.String)reader[((int)AdminTaskEmailTemplateListColumn.AdminTaskTypeName)];
					//entity.AdminTaskTypeName = (Convert.IsDBNull(reader["AdminTaskTypeName"]))?string.Empty:(System.String)reader["AdminTaskTypeName"];
					entity.AdminTaskTypeDesc = (reader.IsDBNull(((int)AdminTaskEmailTemplateListColumn.AdminTaskTypeDesc)))?null:(System.String)reader[((int)AdminTaskEmailTemplateListColumn.AdminTaskTypeDesc)];
					//entity.AdminTaskTypeDesc = (Convert.IsDBNull(reader["AdminTaskTypeDesc"]))?string.Empty:(System.String)reader["AdminTaskTypeDesc"];
					entity.EmailSubject = (reader.IsDBNull(((int)AdminTaskEmailTemplateListColumn.EmailSubject)))?null:(System.String)reader[((int)AdminTaskEmailTemplateListColumn.EmailSubject)];
					//entity.EmailSubject = (Convert.IsDBNull(reader["EmailSubject"]))?string.Empty:(System.String)reader["EmailSubject"];
					entity.AcceptChanges();
					entity.SuppressEntityEvents = false;
					
					rows.Add(entity);
					pageLength -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		
		
		/// <summary>
		/// Refreshes the <see cref="AdminTaskEmailTemplateList"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="AdminTaskEmailTemplateList"/> object to refresh.</param>
		protected void RefreshEntity(IDataReader reader, AdminTaskEmailTemplateList entity)
		{
			reader.Read();
			entity.AdminTaskEmailTemplateId = (System.Int32)reader[((int)AdminTaskEmailTemplateListColumn.AdminTaskEmailTemplateId)];
			//entity.AdminTaskEmailTemplateId = (Convert.IsDBNull(reader["AdminTaskEmailTemplateId"]))?(int)0:(System.Int32)reader["AdminTaskEmailTemplateId"];
			entity.AdminTaskTypeId = (System.Int32)reader[((int)AdminTaskEmailTemplateListColumn.AdminTaskTypeId)];
			//entity.AdminTaskTypeId = (Convert.IsDBNull(reader["AdminTaskTypeId"]))?(int)0:(System.Int32)reader["AdminTaskTypeId"];
			entity.AdminTaskTypeName = (System.String)reader[((int)AdminTaskEmailTemplateListColumn.AdminTaskTypeName)];
			//entity.AdminTaskTypeName = (Convert.IsDBNull(reader["AdminTaskTypeName"]))?string.Empty:(System.String)reader["AdminTaskTypeName"];
			entity.AdminTaskTypeDesc = (reader.IsDBNull(((int)AdminTaskEmailTemplateListColumn.AdminTaskTypeDesc)))?null:(System.String)reader[((int)AdminTaskEmailTemplateListColumn.AdminTaskTypeDesc)];
			//entity.AdminTaskTypeDesc = (Convert.IsDBNull(reader["AdminTaskTypeDesc"]))?string.Empty:(System.String)reader["AdminTaskTypeDesc"];
			entity.EmailSubject = (reader.IsDBNull(((int)AdminTaskEmailTemplateListColumn.EmailSubject)))?null:(System.String)reader[((int)AdminTaskEmailTemplateListColumn.EmailSubject)];
			//entity.EmailSubject = (Convert.IsDBNull(reader["EmailSubject"]))?string.Empty:(System.String)reader["EmailSubject"];
			reader.Close();
	
			entity.AcceptChanges();
		}
		
		/*
		/// <summary>
		/// Refreshes the <see cref="AdminTaskEmailTemplateList"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="AdminTaskEmailTemplateList"/> object.</param>
		protected static void RefreshEntity(DataSet dataSet, AdminTaskEmailTemplateList entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.AdminTaskEmailTemplateId = (Convert.IsDBNull(dataRow["AdminTaskEmailTemplateId"]))?(int)0:(System.Int32)dataRow["AdminTaskEmailTemplateId"];
			entity.AdminTaskTypeId = (Convert.IsDBNull(dataRow["AdminTaskTypeId"]))?(int)0:(System.Int32)dataRow["AdminTaskTypeId"];
			entity.AdminTaskTypeName = (Convert.IsDBNull(dataRow["AdminTaskTypeName"]))?string.Empty:(System.String)dataRow["AdminTaskTypeName"];
			entity.AdminTaskTypeDesc = (Convert.IsDBNull(dataRow["AdminTaskTypeDesc"]))?string.Empty:(System.String)dataRow["AdminTaskTypeDesc"];
			entity.EmailSubject = (Convert.IsDBNull(dataRow["EmailSubject"]))?string.Empty:(System.String)dataRow["EmailSubject"];
			entity.AcceptChanges();
		}
		*/
			
		#endregion Helper Functions
	}//end class

	#region AdminTaskEmailTemplateListFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AdminTaskEmailTemplateList"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AdminTaskEmailTemplateListFilterBuilder : SqlFilterBuilder<AdminTaskEmailTemplateListColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailTemplateListFilterBuilder class.
		/// </summary>
		public AdminTaskEmailTemplateListFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailTemplateListFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AdminTaskEmailTemplateListFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailTemplateListFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AdminTaskEmailTemplateListFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AdminTaskEmailTemplateListFilterBuilder

	#region AdminTaskEmailTemplateListParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AdminTaskEmailTemplateList"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AdminTaskEmailTemplateListParameterBuilder : ParameterizedSqlFilterBuilder<AdminTaskEmailTemplateListColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailTemplateListParameterBuilder class.
		/// </summary>
		public AdminTaskEmailTemplateListParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailTemplateListParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AdminTaskEmailTemplateListParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailTemplateListParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AdminTaskEmailTemplateListParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AdminTaskEmailTemplateListParameterBuilder
	
	#region AdminTaskEmailTemplateListSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AdminTaskEmailTemplateList"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class AdminTaskEmailTemplateListSortBuilder : SqlSortBuilder<AdminTaskEmailTemplateListColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailTemplateListSqlSortBuilder class.
		/// </summary>
		public AdminTaskEmailTemplateListSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion AdminTaskEmailTemplateListSortBuilder

} // end namespace
