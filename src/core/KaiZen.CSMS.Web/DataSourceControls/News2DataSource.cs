﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.News2Provider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(News2DataSourceDesigner))]
	public class News2DataSource : ProviderDataSource<News2, News2Key>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the News2DataSource class.
		/// </summary>
		public News2DataSource() : base(new News2Service())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the News2DataSourceView used by the News2DataSource.
		/// </summary>
		protected News2DataSourceView News2View
		{
			get { return ( View as News2DataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the News2DataSource control invokes to retrieve data.
		/// </summary>
		public News2SelectMethod SelectMethod
		{
			get
			{
				News2SelectMethod selectMethod = News2SelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (News2SelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the News2DataSourceView class that is to be
		/// used by the News2DataSource.
		/// </summary>
		/// <returns>An instance of the News2DataSourceView class.</returns>
		protected override BaseDataSourceView<News2, News2Key> GetNewDataSourceView()
		{
			return new News2DataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the News2DataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class News2DataSourceView : ProviderDataSourceView<News2, News2Key>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the News2DataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the News2DataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public News2DataSourceView(News2DataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal News2DataSource News2Owner
		{
			get { return Owner as News2DataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal News2SelectMethod SelectMethod
		{
			get { return News2Owner.SelectMethod; }
			set { News2Owner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal News2Service News2Provider
		{
			get { return Provider as News2Service; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<News2> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<News2> results = null;
			News2 item;
			count = 0;
			
			System.Int32 _news2Id;

			switch ( SelectMethod )
			{
				case News2SelectMethod.Get:
					News2Key entityKey  = new News2Key();
					entityKey.Load(values);
					item = News2Provider.Get(entityKey);
					results = new TList<News2>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case News2SelectMethod.GetAll:
                    results = News2Provider.GetAll(StartIndex, PageSize, out count);
                    break;
				case News2SelectMethod.GetPaged:
					results = News2Provider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case News2SelectMethod.Find:
					if ( FilterParameters != null )
						results = News2Provider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = News2Provider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case News2SelectMethod.GetByNews2Id:
					_news2Id = ( values["News2Id"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["News2Id"], typeof(System.Int32)) : (int)0;
					item = News2Provider.GetByNews2Id(_news2Id);
					results = new TList<News2>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				// FK
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == News2SelectMethod.Get || SelectMethod == News2SelectMethod.GetByNews2Id )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				News2 entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					News2Provider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<News2> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			News2Provider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region News2DataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the News2DataSource class.
	/// </summary>
	public class News2DataSourceDesigner : ProviderDataSourceDesigner<News2, News2Key>
	{
		/// <summary>
		/// Initializes a new instance of the News2DataSourceDesigner class.
		/// </summary>
		public News2DataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public News2SelectMethod SelectMethod
		{
			get { return ((News2DataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new News2DataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region News2DataSourceActionList

	/// <summary>
	/// Supports the News2DataSourceDesigner class.
	/// </summary>
	internal class News2DataSourceActionList : DesignerActionList
	{
		private News2DataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the News2DataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public News2DataSourceActionList(News2DataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public News2SelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion News2DataSourceActionList
	
	#endregion News2DataSourceDesigner
	
	#region News2SelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the News2DataSource.SelectMethod property.
	/// </summary>
	public enum News2SelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByNews2Id method.
		/// </summary>
		GetByNews2Id
	}
	
	#endregion News2SelectMethod

	#region News2Filter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="News2"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class News2Filter : SqlFilter<News2Column>
	{
	}
	
	#endregion News2Filter

	#region News2ExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="News2"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class News2ExpressionBuilder : SqlExpressionBuilder<News2Column>
	{
	}
	
	#endregion News2ExpressionBuilder	

	#region News2Property
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;News2ChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="News2"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class News2Property : ChildEntityProperty<News2ChildEntityTypes>
	{
	}
	
	#endregion News2Property
}

