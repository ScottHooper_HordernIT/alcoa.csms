﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.ContactsAlcoaProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(ContactsAlcoaDataSourceDesigner))]
	public class ContactsAlcoaDataSource : ProviderDataSource<ContactsAlcoa, ContactsAlcoaKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ContactsAlcoaDataSource class.
		/// </summary>
		public ContactsAlcoaDataSource() : base(new ContactsAlcoaService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the ContactsAlcoaDataSourceView used by the ContactsAlcoaDataSource.
		/// </summary>
		protected ContactsAlcoaDataSourceView ContactsAlcoaView
		{
			get { return ( View as ContactsAlcoaDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the ContactsAlcoaDataSource control invokes to retrieve data.
		/// </summary>
		public ContactsAlcoaSelectMethod SelectMethod
		{
			get
			{
				ContactsAlcoaSelectMethod selectMethod = ContactsAlcoaSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (ContactsAlcoaSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the ContactsAlcoaDataSourceView class that is to be
		/// used by the ContactsAlcoaDataSource.
		/// </summary>
		/// <returns>An instance of the ContactsAlcoaDataSourceView class.</returns>
		protected override BaseDataSourceView<ContactsAlcoa, ContactsAlcoaKey> GetNewDataSourceView()
		{
			return new ContactsAlcoaDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the ContactsAlcoaDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class ContactsAlcoaDataSourceView : ProviderDataSourceView<ContactsAlcoa, ContactsAlcoaKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ContactsAlcoaDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the ContactsAlcoaDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public ContactsAlcoaDataSourceView(ContactsAlcoaDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal ContactsAlcoaDataSource ContactsAlcoaOwner
		{
			get { return Owner as ContactsAlcoaDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal ContactsAlcoaSelectMethod SelectMethod
		{
			get { return ContactsAlcoaOwner.SelectMethod; }
			set { ContactsAlcoaOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal ContactsAlcoaService ContactsAlcoaProvider
		{
			get { return Provider as ContactsAlcoaService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<ContactsAlcoa> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<ContactsAlcoa> results = null;
			ContactsAlcoa item;
			count = 0;
			
			System.Int32 _contactId;
			System.Int32 _modifiedByUserId;
			System.Int32 _siteId;

			switch ( SelectMethod )
			{
				case ContactsAlcoaSelectMethod.Get:
					ContactsAlcoaKey entityKey  = new ContactsAlcoaKey();
					entityKey.Load(values);
					item = ContactsAlcoaProvider.Get(entityKey);
					results = new TList<ContactsAlcoa>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case ContactsAlcoaSelectMethod.GetAll:
                    results = ContactsAlcoaProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case ContactsAlcoaSelectMethod.GetPaged:
					results = ContactsAlcoaProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case ContactsAlcoaSelectMethod.Find:
					if ( FilterParameters != null )
						results = ContactsAlcoaProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = ContactsAlcoaProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case ContactsAlcoaSelectMethod.GetByContactId:
					_contactId = ( values["ContactId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["ContactId"], typeof(System.Int32)) : (int)0;
					item = ContactsAlcoaProvider.GetByContactId(_contactId);
					results = new TList<ContactsAlcoa>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				// FK
				case ContactsAlcoaSelectMethod.GetByModifiedByUserId:
					_modifiedByUserId = ( values["ModifiedByUserId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["ModifiedByUserId"], typeof(System.Int32)) : (int)0;
					results = ContactsAlcoaProvider.GetByModifiedByUserId(_modifiedByUserId, this.StartIndex, this.PageSize, out count);
					break;
				case ContactsAlcoaSelectMethod.GetBySiteId:
					_siteId = ( values["SiteId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["SiteId"], typeof(System.Int32)) : (int)0;
					results = ContactsAlcoaProvider.GetBySiteId(_siteId, this.StartIndex, this.PageSize, out count);
					break;
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == ContactsAlcoaSelectMethod.Get || SelectMethod == ContactsAlcoaSelectMethod.GetByContactId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				ContactsAlcoa entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					ContactsAlcoaProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<ContactsAlcoa> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			ContactsAlcoaProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region ContactsAlcoaDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the ContactsAlcoaDataSource class.
	/// </summary>
	public class ContactsAlcoaDataSourceDesigner : ProviderDataSourceDesigner<ContactsAlcoa, ContactsAlcoaKey>
	{
		/// <summary>
		/// Initializes a new instance of the ContactsAlcoaDataSourceDesigner class.
		/// </summary>
		public ContactsAlcoaDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public ContactsAlcoaSelectMethod SelectMethod
		{
			get { return ((ContactsAlcoaDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new ContactsAlcoaDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region ContactsAlcoaDataSourceActionList

	/// <summary>
	/// Supports the ContactsAlcoaDataSourceDesigner class.
	/// </summary>
	internal class ContactsAlcoaDataSourceActionList : DesignerActionList
	{
		private ContactsAlcoaDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the ContactsAlcoaDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public ContactsAlcoaDataSourceActionList(ContactsAlcoaDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public ContactsAlcoaSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion ContactsAlcoaDataSourceActionList
	
	#endregion ContactsAlcoaDataSourceDesigner
	
	#region ContactsAlcoaSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the ContactsAlcoaDataSource.SelectMethod property.
	/// </summary>
	public enum ContactsAlcoaSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByContactId method.
		/// </summary>
		GetByContactId,
		/// <summary>
		/// Represents the GetByModifiedByUserId method.
		/// </summary>
		GetByModifiedByUserId,
		/// <summary>
		/// Represents the GetBySiteId method.
		/// </summary>
		GetBySiteId
	}
	
	#endregion ContactsAlcoaSelectMethod

	#region ContactsAlcoaFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ContactsAlcoa"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ContactsAlcoaFilter : SqlFilter<ContactsAlcoaColumn>
	{
	}
	
	#endregion ContactsAlcoaFilter

	#region ContactsAlcoaExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ContactsAlcoa"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ContactsAlcoaExpressionBuilder : SqlExpressionBuilder<ContactsAlcoaColumn>
	{
	}
	
	#endregion ContactsAlcoaExpressionBuilder	

	#region ContactsAlcoaProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;ContactsAlcoaChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="ContactsAlcoa"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ContactsAlcoaProperty : ChildEntityProperty<ContactsAlcoaChildEntityTypes>
	{
	}
	
	#endregion ContactsAlcoaProperty
}

