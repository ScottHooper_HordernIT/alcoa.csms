﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.QuestionnaireReportOverviewSubQuery3Provider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[CLSCompliant(true)]
	[Designer(typeof(QuestionnaireReportOverviewSubQuery3DataSourceDesigner))]
	public class QuestionnaireReportOverviewSubQuery3DataSource : ReadOnlyDataSource<QuestionnaireReportOverviewSubQuery3>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportOverviewSubQuery3DataSource class.
		/// </summary>
		public QuestionnaireReportOverviewSubQuery3DataSource() : base(new QuestionnaireReportOverviewSubQuery3Service())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the QuestionnaireReportOverviewSubQuery3DataSourceView used by the QuestionnaireReportOverviewSubQuery3DataSource.
		/// </summary>
		protected QuestionnaireReportOverviewSubQuery3DataSourceView QuestionnaireReportOverviewSubQuery3View
		{
			get { return ( View as QuestionnaireReportOverviewSubQuery3DataSourceView ); }
		}
		
		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the QuestionnaireReportOverviewSubQuery3DataSourceView class that is to be
		/// used by the QuestionnaireReportOverviewSubQuery3DataSource.
		/// </summary>
		/// <returns>An instance of the QuestionnaireReportOverviewSubQuery3DataSourceView class.</returns>
		protected override BaseDataSourceView<QuestionnaireReportOverviewSubQuery3, Object> GetNewDataSourceView()
		{
			return new QuestionnaireReportOverviewSubQuery3DataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the QuestionnaireReportOverviewSubQuery3DataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class QuestionnaireReportOverviewSubQuery3DataSourceView : ReadOnlyDataSourceView<QuestionnaireReportOverviewSubQuery3>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportOverviewSubQuery3DataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the QuestionnaireReportOverviewSubQuery3DataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public QuestionnaireReportOverviewSubQuery3DataSourceView(QuestionnaireReportOverviewSubQuery3DataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal QuestionnaireReportOverviewSubQuery3DataSource QuestionnaireReportOverviewSubQuery3Owner
		{
			get { return Owner as QuestionnaireReportOverviewSubQuery3DataSource; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal QuestionnaireReportOverviewSubQuery3Service QuestionnaireReportOverviewSubQuery3Provider
		{
			get { return Provider as QuestionnaireReportOverviewSubQuery3Service; }
		}

		#endregion Properties
		
		#region Methods
		
		#endregion Methods
	}

	#region QuestionnaireReportOverviewSubQuery3DataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the QuestionnaireReportOverviewSubQuery3DataSource class.
	/// </summary>
	public class QuestionnaireReportOverviewSubQuery3DataSourceDesigner : ReadOnlyDataSourceDesigner<QuestionnaireReportOverviewSubQuery3>
	{
	}

	#endregion QuestionnaireReportOverviewSubQuery3DataSourceDesigner

	#region QuestionnaireReportOverviewSubQuery3Filter

	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireReportOverviewSubQuery3"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireReportOverviewSubQuery3Filter : SqlFilter<QuestionnaireReportOverviewSubQuery3Column>
	{
	}

	#endregion QuestionnaireReportOverviewSubQuery3Filter

	#region QuestionnaireReportOverviewSubQuery3ExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireReportOverviewSubQuery3"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireReportOverviewSubQuery3ExpressionBuilder : SqlExpressionBuilder<QuestionnaireReportOverviewSubQuery3Column>
	{
	}
	
	#endregion QuestionnaireReportOverviewSubQuery3ExpressionBuilder		
}

