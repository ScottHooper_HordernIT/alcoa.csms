﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="AnnualTargetsProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class AnnualTargetsProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.AnnualTargets, KaiZen.CSMS.Entities.AnnualTargetsKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.AnnualTargetsKey key)
		{
			return Delete(transactionManager, key.AnnualTargetsId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_annualTargetsId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _annualTargetsId)
		{
			return Delete(null, _annualTargetsId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_annualTargetsId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _annualTargetsId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_AnnualTargets_CompanySiteCategory key.
		///		FK_AnnualTargets_CompanySiteCategory Description: 
		/// </summary>
		/// <param name="_companySiteCategoryId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.AnnualTargets objects.</returns>
		public TList<AnnualTargets> GetByCompanySiteCategoryId(System.Int32 _companySiteCategoryId)
		{
			int count = -1;
			return GetByCompanySiteCategoryId(_companySiteCategoryId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_AnnualTargets_CompanySiteCategory key.
		///		FK_AnnualTargets_CompanySiteCategory Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companySiteCategoryId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.AnnualTargets objects.</returns>
		/// <remarks></remarks>
		public TList<AnnualTargets> GetByCompanySiteCategoryId(TransactionManager transactionManager, System.Int32 _companySiteCategoryId)
		{
			int count = -1;
			return GetByCompanySiteCategoryId(transactionManager, _companySiteCategoryId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_AnnualTargets_CompanySiteCategory key.
		///		FK_AnnualTargets_CompanySiteCategory Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companySiteCategoryId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.AnnualTargets objects.</returns>
		public TList<AnnualTargets> GetByCompanySiteCategoryId(TransactionManager transactionManager, System.Int32 _companySiteCategoryId, int start, int pageLength)
		{
			int count = -1;
			return GetByCompanySiteCategoryId(transactionManager, _companySiteCategoryId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_AnnualTargets_CompanySiteCategory key.
		///		fkAnnualTargetsCompanySiteCategory Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_companySiteCategoryId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.AnnualTargets objects.</returns>
		public TList<AnnualTargets> GetByCompanySiteCategoryId(System.Int32 _companySiteCategoryId, int start, int pageLength)
		{
			int count =  -1;
			return GetByCompanySiteCategoryId(null, _companySiteCategoryId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_AnnualTargets_CompanySiteCategory key.
		///		fkAnnualTargetsCompanySiteCategory Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_companySiteCategoryId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.AnnualTargets objects.</returns>
		public TList<AnnualTargets> GetByCompanySiteCategoryId(System.Int32 _companySiteCategoryId, int start, int pageLength,out int count)
		{
			return GetByCompanySiteCategoryId(null, _companySiteCategoryId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_AnnualTargets_CompanySiteCategory key.
		///		FK_AnnualTargets_CompanySiteCategory Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companySiteCategoryId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.AnnualTargets objects.</returns>
		public abstract TList<AnnualTargets> GetByCompanySiteCategoryId(TransactionManager transactionManager, System.Int32 _companySiteCategoryId, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.AnnualTargets Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.AnnualTargetsKey key, int start, int pageLength)
		{
			return GetByAnnualTargetsId(transactionManager, key.AnnualTargetsId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_AnnualTargets index.
		/// </summary>
		/// <param name="_annualTargetsId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AnnualTargets"/> class.</returns>
		public KaiZen.CSMS.Entities.AnnualTargets GetByAnnualTargetsId(System.Int32 _annualTargetsId)
		{
			int count = -1;
			return GetByAnnualTargetsId(null,_annualTargetsId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_AnnualTargets index.
		/// </summary>
		/// <param name="_annualTargetsId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AnnualTargets"/> class.</returns>
		public KaiZen.CSMS.Entities.AnnualTargets GetByAnnualTargetsId(System.Int32 _annualTargetsId, int start, int pageLength)
		{
			int count = -1;
			return GetByAnnualTargetsId(null, _annualTargetsId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_AnnualTargets index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_annualTargetsId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AnnualTargets"/> class.</returns>
		public KaiZen.CSMS.Entities.AnnualTargets GetByAnnualTargetsId(TransactionManager transactionManager, System.Int32 _annualTargetsId)
		{
			int count = -1;
			return GetByAnnualTargetsId(transactionManager, _annualTargetsId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_AnnualTargets index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_annualTargetsId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AnnualTargets"/> class.</returns>
		public KaiZen.CSMS.Entities.AnnualTargets GetByAnnualTargetsId(TransactionManager transactionManager, System.Int32 _annualTargetsId, int start, int pageLength)
		{
			int count = -1;
			return GetByAnnualTargetsId(transactionManager, _annualTargetsId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_AnnualTargets index.
		/// </summary>
		/// <param name="_annualTargetsId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AnnualTargets"/> class.</returns>
		public KaiZen.CSMS.Entities.AnnualTargets GetByAnnualTargetsId(System.Int32 _annualTargetsId, int start, int pageLength, out int count)
		{
			return GetByAnnualTargetsId(null, _annualTargetsId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_AnnualTargets index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_annualTargetsId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AnnualTargets"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.AnnualTargets GetByAnnualTargetsId(TransactionManager transactionManager, System.Int32 _annualTargetsId, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_AnnualTargets index.
		/// </summary>
		/// <param name="_year"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;AnnualTargets&gt;"/> class.</returns>
		public TList<AnnualTargets> GetByYear(System.Int32 _year)
		{
			int count = -1;
			return GetByYear(null,_year, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_AnnualTargets index.
		/// </summary>
		/// <param name="_year"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;AnnualTargets&gt;"/> class.</returns>
		public TList<AnnualTargets> GetByYear(System.Int32 _year, int start, int pageLength)
		{
			int count = -1;
			return GetByYear(null, _year, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_AnnualTargets index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_year"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;AnnualTargets&gt;"/> class.</returns>
		public TList<AnnualTargets> GetByYear(TransactionManager transactionManager, System.Int32 _year)
		{
			int count = -1;
			return GetByYear(transactionManager, _year, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_AnnualTargets index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_year"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;AnnualTargets&gt;"/> class.</returns>
		public TList<AnnualTargets> GetByYear(TransactionManager transactionManager, System.Int32 _year, int start, int pageLength)
		{
			int count = -1;
			return GetByYear(transactionManager, _year, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_AnnualTargets index.
		/// </summary>
		/// <param name="_year"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;AnnualTargets&gt;"/> class.</returns>
		public TList<AnnualTargets> GetByYear(System.Int32 _year, int start, int pageLength, out int count)
		{
			return GetByYear(null, _year, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_AnnualTargets index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_year"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;AnnualTargets&gt;"/> class.</returns>
		public abstract TList<AnnualTargets> GetByYear(TransactionManager transactionManager, System.Int32 _year, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_AnnualTargets_1 index.
		/// </summary>
		/// <param name="_year"></param>
		/// <param name="_companySiteCategoryId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AnnualTargets"/> class.</returns>
		public KaiZen.CSMS.Entities.AnnualTargets GetByYearCompanySiteCategoryId(System.Int32 _year, System.Int32 _companySiteCategoryId)
		{
			int count = -1;
			return GetByYearCompanySiteCategoryId(null,_year, _companySiteCategoryId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_AnnualTargets_1 index.
		/// </summary>
		/// <param name="_year"></param>
		/// <param name="_companySiteCategoryId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AnnualTargets"/> class.</returns>
		public KaiZen.CSMS.Entities.AnnualTargets GetByYearCompanySiteCategoryId(System.Int32 _year, System.Int32 _companySiteCategoryId, int start, int pageLength)
		{
			int count = -1;
			return GetByYearCompanySiteCategoryId(null, _year, _companySiteCategoryId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_AnnualTargets_1 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_year"></param>
		/// <param name="_companySiteCategoryId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AnnualTargets"/> class.</returns>
		public KaiZen.CSMS.Entities.AnnualTargets GetByYearCompanySiteCategoryId(TransactionManager transactionManager, System.Int32 _year, System.Int32 _companySiteCategoryId)
		{
			int count = -1;
			return GetByYearCompanySiteCategoryId(transactionManager, _year, _companySiteCategoryId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_AnnualTargets_1 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_year"></param>
		/// <param name="_companySiteCategoryId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AnnualTargets"/> class.</returns>
		public KaiZen.CSMS.Entities.AnnualTargets GetByYearCompanySiteCategoryId(TransactionManager transactionManager, System.Int32 _year, System.Int32 _companySiteCategoryId, int start, int pageLength)
		{
			int count = -1;
			return GetByYearCompanySiteCategoryId(transactionManager, _year, _companySiteCategoryId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_AnnualTargets_1 index.
		/// </summary>
		/// <param name="_year"></param>
		/// <param name="_companySiteCategoryId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AnnualTargets"/> class.</returns>
		public KaiZen.CSMS.Entities.AnnualTargets GetByYearCompanySiteCategoryId(System.Int32 _year, System.Int32 _companySiteCategoryId, int start, int pageLength, out int count)
		{
			return GetByYearCompanySiteCategoryId(null, _year, _companySiteCategoryId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_AnnualTargets_1 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_year"></param>
		/// <param name="_companySiteCategoryId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AnnualTargets"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.AnnualTargets GetByYearCompanySiteCategoryId(TransactionManager transactionManager, System.Int32 _year, System.Int32 _companySiteCategoryId, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;AnnualTargets&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;AnnualTargets&gt;"/></returns>
		public static TList<AnnualTargets> Fill(IDataReader reader, TList<AnnualTargets> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.AnnualTargets c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("AnnualTargets")
					.Append("|").Append((System.Int32)reader[((int)AnnualTargetsColumn.AnnualTargetsId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<AnnualTargets>(
					key.ToString(), // EntityTrackingKey
					"AnnualTargets",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.AnnualTargets();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.AnnualTargetsId = (System.Int32)reader[((int)AnnualTargetsColumn.AnnualTargetsId - 1)];
					c.CompanySiteCategoryId = (System.Int32)reader[((int)AnnualTargetsColumn.CompanySiteCategoryId - 1)];
					c.Year = (System.Int32)reader[((int)AnnualTargetsColumn.Year - 1)];
					c.EhsAudits = (reader.IsDBNull(((int)AnnualTargetsColumn.EhsAudits - 1)))?null:(System.Int32?)reader[((int)AnnualTargetsColumn.EhsAudits - 1)];
					c.WorkplaceSafetyCompliance = (reader.IsDBNull(((int)AnnualTargetsColumn.WorkplaceSafetyCompliance - 1)))?null:(System.Int32?)reader[((int)AnnualTargetsColumn.WorkplaceSafetyCompliance - 1)];
					c.HealthSafetyWorkContacts = (reader.IsDBNull(((int)AnnualTargetsColumn.HealthSafetyWorkContacts - 1)))?null:(System.Int32?)reader[((int)AnnualTargetsColumn.HealthSafetyWorkContacts - 1)];
					c.BehaviouralSafetyProgram = (reader.IsDBNull(((int)AnnualTargetsColumn.BehaviouralSafetyProgram - 1)))?null:(System.Decimal?)reader[((int)AnnualTargetsColumn.BehaviouralSafetyProgram - 1)];
					c.QuarterlyAuditScore = (reader.IsDBNull(((int)AnnualTargetsColumn.QuarterlyAuditScore - 1)))?null:(System.Int32?)reader[((int)AnnualTargetsColumn.QuarterlyAuditScore - 1)];
					c.ToolboxMeetingsPerMonth = (reader.IsDBNull(((int)AnnualTargetsColumn.ToolboxMeetingsPerMonth - 1)))?null:(System.Int32?)reader[((int)AnnualTargetsColumn.ToolboxMeetingsPerMonth - 1)];
					c.AlcoaWeeklyContractorsMeeting = (reader.IsDBNull(((int)AnnualTargetsColumn.AlcoaWeeklyContractorsMeeting - 1)))?null:(System.Int32?)reader[((int)AnnualTargetsColumn.AlcoaWeeklyContractorsMeeting - 1)];
					c.AlcoaMonthlyContractorsMeeting = (reader.IsDBNull(((int)AnnualTargetsColumn.AlcoaMonthlyContractorsMeeting - 1)))?null:(System.Int32?)reader[((int)AnnualTargetsColumn.AlcoaMonthlyContractorsMeeting - 1)];
					c.SafetyPlan = (reader.IsDBNull(((int)AnnualTargetsColumn.SafetyPlan - 1)))?null:(System.Int32?)reader[((int)AnnualTargetsColumn.SafetyPlan - 1)];
					c.JsaScore = (reader.IsDBNull(((int)AnnualTargetsColumn.JsaScore - 1)))?null:(System.Int32?)reader[((int)AnnualTargetsColumn.JsaScore - 1)];
					c.FatalityPrevention = (reader.IsDBNull(((int)AnnualTargetsColumn.FatalityPrevention - 1)))?null:(System.Int32?)reader[((int)AnnualTargetsColumn.FatalityPrevention - 1)];
					c.Trifr = (reader.IsDBNull(((int)AnnualTargetsColumn.Trifr - 1)))?null:(System.Decimal?)reader[((int)AnnualTargetsColumn.Trifr - 1)];
					c.Aifr = (reader.IsDBNull(((int)AnnualTargetsColumn.Aifr - 1)))?null:(System.Decimal?)reader[((int)AnnualTargetsColumn.Aifr - 1)];
					c.Training = (reader.IsDBNull(((int)AnnualTargetsColumn.Training - 1)))?null:(System.Int32?)reader[((int)AnnualTargetsColumn.Training - 1)];
					c.MedicalSchedule = (reader.IsDBNull(((int)AnnualTargetsColumn.MedicalSchedule - 1)))?null:(System.Int32?)reader[((int)AnnualTargetsColumn.MedicalSchedule - 1)];
					c.TrainingSchedule = (reader.IsDBNull(((int)AnnualTargetsColumn.TrainingSchedule - 1)))?null:(System.Int32?)reader[((int)AnnualTargetsColumn.TrainingSchedule - 1)];
					//Added By Sayani
                    c.IFEInjuryRatioTargetYTD = (reader.IsDBNull(((int)AnnualTargetsColumn.IFEInjuryRatioTargetYTD - 1))) ? null : (System.Decimal?)reader[((int)AnnualTargetsColumn.IFEInjuryRatioTargetYTD - 1)];
                    c.IFEInjuryRatioTargetMon = (reader.IsDBNull(((int)AnnualTargetsColumn.IFEInjuryRatioTargetMon - 1))) ? null : (System.Decimal?)reader[((int)AnnualTargetsColumn.IFEInjuryRatioTargetMon - 1)];
                    c.RNInjuryTargetYTD = (reader.IsDBNull(((int)AnnualTargetsColumn.RNInjuryTargetYTD - 1))) ? null : (System.Decimal?)reader[((int)AnnualTargetsColumn.RNInjuryTargetYTD - 1)];
                    c.RNInjuryRatioTargetMon = (reader.IsDBNull(((int)AnnualTargetsColumn.RNInjuryRatioTargetMon - 1))) ? null : (System.Decimal?)reader[((int)AnnualTargetsColumn.RNInjuryRatioTargetMon - 1)];
					//
                    c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.AnnualTargets"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.AnnualTargets"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.AnnualTargets entity)
		{
			if (!reader.Read()) return;
			
			entity.AnnualTargetsId = (System.Int32)reader[((int)AnnualTargetsColumn.AnnualTargetsId - 1)];
			entity.CompanySiteCategoryId = (System.Int32)reader[((int)AnnualTargetsColumn.CompanySiteCategoryId - 1)];
			entity.Year = (System.Int32)reader[((int)AnnualTargetsColumn.Year - 1)];
			entity.EhsAudits = (reader.IsDBNull(((int)AnnualTargetsColumn.EhsAudits - 1)))?null:(System.Int32?)reader[((int)AnnualTargetsColumn.EhsAudits - 1)];
			entity.WorkplaceSafetyCompliance = (reader.IsDBNull(((int)AnnualTargetsColumn.WorkplaceSafetyCompliance - 1)))?null:(System.Int32?)reader[((int)AnnualTargetsColumn.WorkplaceSafetyCompliance - 1)];
			entity.HealthSafetyWorkContacts = (reader.IsDBNull(((int)AnnualTargetsColumn.HealthSafetyWorkContacts - 1)))?null:(System.Int32?)reader[((int)AnnualTargetsColumn.HealthSafetyWorkContacts - 1)];
			entity.BehaviouralSafetyProgram = (reader.IsDBNull(((int)AnnualTargetsColumn.BehaviouralSafetyProgram - 1)))?null:(System.Decimal?)reader[((int)AnnualTargetsColumn.BehaviouralSafetyProgram - 1)];
			entity.QuarterlyAuditScore = (reader.IsDBNull(((int)AnnualTargetsColumn.QuarterlyAuditScore - 1)))?null:(System.Int32?)reader[((int)AnnualTargetsColumn.QuarterlyAuditScore - 1)];
			entity.ToolboxMeetingsPerMonth = (reader.IsDBNull(((int)AnnualTargetsColumn.ToolboxMeetingsPerMonth - 1)))?null:(System.Int32?)reader[((int)AnnualTargetsColumn.ToolboxMeetingsPerMonth - 1)];
			entity.AlcoaWeeklyContractorsMeeting = (reader.IsDBNull(((int)AnnualTargetsColumn.AlcoaWeeklyContractorsMeeting - 1)))?null:(System.Int32?)reader[((int)AnnualTargetsColumn.AlcoaWeeklyContractorsMeeting - 1)];
			entity.AlcoaMonthlyContractorsMeeting = (reader.IsDBNull(((int)AnnualTargetsColumn.AlcoaMonthlyContractorsMeeting - 1)))?null:(System.Int32?)reader[((int)AnnualTargetsColumn.AlcoaMonthlyContractorsMeeting - 1)];
			entity.SafetyPlan = (reader.IsDBNull(((int)AnnualTargetsColumn.SafetyPlan - 1)))?null:(System.Int32?)reader[((int)AnnualTargetsColumn.SafetyPlan - 1)];
			entity.JsaScore = (reader.IsDBNull(((int)AnnualTargetsColumn.JsaScore - 1)))?null:(System.Int32?)reader[((int)AnnualTargetsColumn.JsaScore - 1)];
			entity.FatalityPrevention = (reader.IsDBNull(((int)AnnualTargetsColumn.FatalityPrevention - 1)))?null:(System.Int32?)reader[((int)AnnualTargetsColumn.FatalityPrevention - 1)];
			entity.Trifr = (reader.IsDBNull(((int)AnnualTargetsColumn.Trifr - 1)))?null:(System.Decimal?)reader[((int)AnnualTargetsColumn.Trifr - 1)];
			entity.Aifr = (reader.IsDBNull(((int)AnnualTargetsColumn.Aifr - 1)))?null:(System.Decimal?)reader[((int)AnnualTargetsColumn.Aifr - 1)];
			entity.Training = (reader.IsDBNull(((int)AnnualTargetsColumn.Training - 1)))?null:(System.Int32?)reader[((int)AnnualTargetsColumn.Training - 1)];
			entity.MedicalSchedule = (reader.IsDBNull(((int)AnnualTargetsColumn.MedicalSchedule - 1)))?null:(System.Int32?)reader[((int)AnnualTargetsColumn.MedicalSchedule - 1)];
			entity.TrainingSchedule = (reader.IsDBNull(((int)AnnualTargetsColumn.TrainingSchedule - 1)))?null:(System.Int32?)reader[((int)AnnualTargetsColumn.TrainingSchedule - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.AnnualTargets"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.AnnualTargets"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.AnnualTargets entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.AnnualTargetsId = (System.Int32)dataRow["AnnualTargetsId"];
			entity.CompanySiteCategoryId = (System.Int32)dataRow["CompanySiteCategoryId"];
			entity.Year = (System.Int32)dataRow["Year"];
			entity.EhsAudits = Convert.IsDBNull(dataRow["EHSAudits"]) ? null : (System.Int32?)dataRow["EHSAudits"];
			entity.WorkplaceSafetyCompliance = Convert.IsDBNull(dataRow["WorkplaceSafetyCompliance"]) ? null : (System.Int32?)dataRow["WorkplaceSafetyCompliance"];
			entity.HealthSafetyWorkContacts = Convert.IsDBNull(dataRow["HealthSafetyWorkContacts"]) ? null : (System.Int32?)dataRow["HealthSafetyWorkContacts"];
			entity.BehaviouralSafetyProgram = Convert.IsDBNull(dataRow["BehaviouralSafetyProgram"]) ? null : (System.Decimal?)dataRow["BehaviouralSafetyProgram"];
			entity.QuarterlyAuditScore = Convert.IsDBNull(dataRow["QuarterlyAuditScore"]) ? null : (System.Int32?)dataRow["QuarterlyAuditScore"];
			entity.ToolboxMeetingsPerMonth = Convert.IsDBNull(dataRow["ToolboxMeetingsPerMonth"]) ? null : (System.Int32?)dataRow["ToolboxMeetingsPerMonth"];
			entity.AlcoaWeeklyContractorsMeeting = Convert.IsDBNull(dataRow["AlcoaWeeklyContractorsMeeting"]) ? null : (System.Int32?)dataRow["AlcoaWeeklyContractorsMeeting"];
			entity.AlcoaMonthlyContractorsMeeting = Convert.IsDBNull(dataRow["AlcoaMonthlyContractorsMeeting"]) ? null : (System.Int32?)dataRow["AlcoaMonthlyContractorsMeeting"];
			entity.SafetyPlan = Convert.IsDBNull(dataRow["SafetyPlan"]) ? null : (System.Int32?)dataRow["SafetyPlan"];
			entity.JsaScore = Convert.IsDBNull(dataRow["JSAScore"]) ? null : (System.Int32?)dataRow["JSAScore"];
			entity.FatalityPrevention = Convert.IsDBNull(dataRow["FatalityPrevention"]) ? null : (System.Int32?)dataRow["FatalityPrevention"];
			entity.Trifr = Convert.IsDBNull(dataRow["TRIFR"]) ? null : (System.Decimal?)dataRow["TRIFR"];
			entity.Aifr = Convert.IsDBNull(dataRow["AIFR"]) ? null : (System.Decimal?)dataRow["AIFR"];
			entity.Training = Convert.IsDBNull(dataRow["Training"]) ? null : (System.Int32?)dataRow["Training"];
			entity.MedicalSchedule = Convert.IsDBNull(dataRow["MedicalSchedule"]) ? null : (System.Int32?)dataRow["MedicalSchedule"];
			entity.TrainingSchedule = Convert.IsDBNull(dataRow["TrainingSchedule"]) ? null : (System.Int32?)dataRow["TrainingSchedule"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.AnnualTargets"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.AnnualTargets Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.AnnualTargets entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region CompanySiteCategoryIdSource	
			if (CanDeepLoad(entity, "CompanySiteCategory|CompanySiteCategoryIdSource", deepLoadType, innerList) 
				&& entity.CompanySiteCategoryIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.CompanySiteCategoryId;
				CompanySiteCategory tmpEntity = EntityManager.LocateEntity<CompanySiteCategory>(EntityLocator.ConstructKeyFromPkItems(typeof(CompanySiteCategory), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.CompanySiteCategoryIdSource = tmpEntity;
				else
					entity.CompanySiteCategoryIdSource = DataRepository.CompanySiteCategoryProvider.GetByCompanySiteCategoryId(transactionManager, entity.CompanySiteCategoryId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CompanySiteCategoryIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.CompanySiteCategoryIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.CompanySiteCategoryProvider.DeepLoad(transactionManager, entity.CompanySiteCategoryIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion CompanySiteCategoryIdSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.AnnualTargets object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.AnnualTargets instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.AnnualTargets Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.AnnualTargets entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region CompanySiteCategoryIdSource
			if (CanDeepSave(entity, "CompanySiteCategory|CompanySiteCategoryIdSource", deepSaveType, innerList) 
				&& entity.CompanySiteCategoryIdSource != null)
			{
				DataRepository.CompanySiteCategoryProvider.Save(transactionManager, entity.CompanySiteCategoryIdSource);
				entity.CompanySiteCategoryId = entity.CompanySiteCategoryIdSource.CompanySiteCategoryId;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region AnnualTargetsChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.AnnualTargets</c>
	///</summary>
	public enum AnnualTargetsChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>CompanySiteCategory</c> at CompanySiteCategoryIdSource
		///</summary>
		[ChildEntityType(typeof(CompanySiteCategory))]
		CompanySiteCategory,
		}
	
	#endregion AnnualTargetsChildEntityTypes
	
	#region AnnualTargetsFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;AnnualTargetsColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AnnualTargets"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AnnualTargetsFilterBuilder : SqlFilterBuilder<AnnualTargetsColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AnnualTargetsFilterBuilder class.
		/// </summary>
		public AnnualTargetsFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the AnnualTargetsFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AnnualTargetsFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AnnualTargetsFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AnnualTargetsFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AnnualTargetsFilterBuilder
	
	#region AnnualTargetsParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;AnnualTargetsColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AnnualTargets"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AnnualTargetsParameterBuilder : ParameterizedSqlFilterBuilder<AnnualTargetsColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AnnualTargetsParameterBuilder class.
		/// </summary>
		public AnnualTargetsParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the AnnualTargetsParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AnnualTargetsParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AnnualTargetsParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AnnualTargetsParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AnnualTargetsParameterBuilder
	
	#region AnnualTargetsSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;AnnualTargetsColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AnnualTargets"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class AnnualTargetsSortBuilder : SqlSortBuilder<AnnualTargetsColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AnnualTargetsSqlSortBuilder class.
		/// </summary>
		public AnnualTargetsSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion AnnualTargetsSortBuilder
	
} // end namespace
