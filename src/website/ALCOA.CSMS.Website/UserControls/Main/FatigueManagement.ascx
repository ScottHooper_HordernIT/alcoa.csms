﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FatigueManagement.ascx.cs" Inherits="ALCOA.CSMS.Website.UserControls.Main.FatigueManagement" %>
<%@ Register Assembly="System.Web.Extensions" Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Src="../Other/ExportButtonsPivot.ascx" TagName="ExportButtonsPivot"
    TagPrefix="uc3" %>
<%@ Register Assembly="DevExpress.Web.ASPxPivotGrid.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPivotGrid.Export" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.XtraCharts.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.XtraCharts" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.XtraCharts.v14.1.Web, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.XtraCharts.Web" TagPrefix="dxchartsui" %>
<%@ Register Assembly="DevExpress.Web.ASPxPivotGrid.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPivotGrid" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dxtc" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dxw" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallbackPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dxpc" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>

<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Src="../Other/ExportButtons.ascx" TagName="ExportButtons" TagPrefix="uc1" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1" namespace="DevExpress.Web.ASPxTabControl" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1" namespace="DevExpress.Web.ASPxClasses" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1" namespace="DevExpress.Web.ASPxCallbackPanel" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1" namespace="DevExpress.Web.ASPxPanel" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.ASPxPivotGrid.v14.1" namespace="DevExpress.Web.ASPxPivotGrid" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.ASPxPivotGrid.v14.1" namespace="DevExpress.Web.ASPxPivotGrid.Export" tagprefix="dx" %>
<%@ Register assembly="DevExpress.XtraCharts.v14.1.Web" namespace="DevExpress.XtraCharts.Web" tagprefix="dxchartsui" %>
<%@ Register assembly="DevExpress.XtraCharts.v14.1" namespace="DevExpress.XtraCharts" tagprefix="cc2" %>
<%@ Register assembly="DevExpress.Web.v14.1" namespace="DevExpress.Web.ASPxPopupControl" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>

<table border="0" cellpadding="2" cellspacing="0" width="100%">
    <tr>
        <td class="pageName">
            <span class="title">Performance Management > Tools</span><br />
            <span class="date">Fatigue Management</span><br />
            <img height="1" src="images/grfc_dottedline.gif" width="24" /><br />
        </td>
    </tr>

    <tr>
        <td style="padding-top: 10px; width: 1170px;">

            <dxtc:ASPxPageControl ID="FatiguePageControl" runat="server" ActiveTabIndex="0" 
                CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
                CssPostfix="Office2003Blue" Width="100%">
                <TabPages>
                    <dxtc:TabPage Name="ViolationsTabPage" Text="Alcoa 16/64 Violations">
                        <ContentCollection>
                            <dxw:contentcontrol ID="ContentControl1" runat="server">
                                <dxwgv:aspxgridview id="grid" width="100%"
                                    clientinstancename="grid"
                                    runat="server"
                                    autogeneratecolumns="False"
                                    cssfilepath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                    csspostfix="Office2003Blue" datasourceid="FatigueManagementDS" onhtmlrowcreated="grid_RowCreated" keyfieldname="FatigueEntryId">
                                    <Settings ShowFilterRow="true" ShowFilterBar="Visible" ShowGroupPanel="True"  />
                                    <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                        <ExpandedButton Height="12px" Width="11px" />
                                        <CollapsedButton Height="12px" Width="11px" />
                                        <DetailCollapsedButton Height="12px" Width="11px" />
                                        <DetailExpandedButton Height="12px" Width="11px" />
                                        <LoadingPanel Url="~/App_Themes/Office2003BlueOld/GridView/Loading.gif"><%--Enhancement_023:change by debashis for testing platform upgradation--%>
                                        </LoadingPanel>
                                    </Images>
                                    <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue">
                                        <Header ImageSpacing="5px" SortingImageSpacing="5px">
                                        </Header>
                                        <AlternatingRow Enabled="True">
                                        </AlternatingRow>
                                        <LoadingPanel ImageSpacing="10px">
                                        </LoadingPanel>
                                    </Styles>
              
                                    <Columns>
                                        <dxwgv:GridViewCommandColumn width="50" VisibleIndex="0" Caption="Action">
                                            <EditButton Visible="true"></EditButton>
                                        </dxwgv:GridViewCommandColumn>

                                        <dxwgv:GridViewDataComboBoxColumn width="90" ReadOnly="true" VisibleIndex="1" Caption="Site" FieldName="SiteName" CellStyle-HorizontalAlign="Center">
                                            <PropertiesComboBox DataSourceID="FatigueSitesDS" DropDownHeight="150px" TextField="SiteName"
                                                 IncrementalFilteringMode="StartsWith" NullDisplayText=" ">
                                            </PropertiesComboBox>
                                            <EditFormSettings Visible="False" />
                                            <Settings SortMode="DisplayText"/>
                                            <CellStyle HorizontalAlign="Left"></CellStyle>
                                        </dxwgv:GridViewDataComboBoxColumn>

                                        <dxwgv:GridViewDataComboBoxColumn width="130" ReadOnly="true" VisibleIndex="2" Caption="Company Name" FieldName="CompanyName" CellStyle-HorizontalAlign="Center">
                                            <PropertiesComboBox DataSourceID="FatigueCompaniesDS" DropDownHeight="150px" TextField="CompanyName"
                                                 IncrementalFilteringMode="StartsWith" NullDisplayText=" ">
                                            </PropertiesComboBox>
                                            <EditFormSettings Visible="False" />
                                            <Settings SortMode="DisplayText"/>
                                            <CellStyle HorizontalAlign="Left"></CellStyle>
                                        </dxwgv:GridViewDataComboBoxColumn>

                        
                                        <dxwgv:GridViewDataComboBoxColumn width="75" ReadOnly="true" VisibleIndex="3" Caption="Clock Id#" FieldName="ClockId">
                                            <PropertiesComboBox DropDownHeight="150px"
                                                  TextField="ClockId" IncrementalFilteringMode="StartsWith" DropDownButton-Enabled="true" DataSourceId="FatigueClockDS">
                                            </PropertiesComboBox>
                                           <EditFormSettings Visible="false" />
                                        </dxwgv:GridViewDataComboBoxColumn>
                         
                                        <dxwgv:GridViewDataComboBoxColumn width="140" ReadOnly="true" VisibleIndex="4" Caption="Full Name" FieldName="FullName">
                                            <PropertiesComboBox DropDownHeight="150px"
                                                      TextField="FullName" IncrementalFilteringMode="StartsWith" DropDownButton-Enabled="true" DataSourceId="FatigueFullNameDS">
                                                </PropertiesComboBox>
                                                <DataItemTemplate>
                                                     <table cellpadding="0" cellspacing="0"><tr>
                                                     <td style="width:100%">
                                                         <dx:ASPxHyperLink Width="100%" ID="hlFullName" runat="server" Text="">
                                                         </dx:ASPxHyperLink>
                                                     </td>
                                                     </tr></table>
                                                </DataItemTemplate>
                                               <EditFormSettings Visible="false" />
                                        </dxwgv:GridViewDataComboBoxColumn>

                                        <dx:GridViewDataDateColumn width="150" Caption="Date/Time Exceeding Alcoa 16/64 Policy" FieldName="EntryDateTime" ReadOnly="True" VisibleIndex="5" SortOrder="Descending">
                                            <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy HH:mm:ss" >
                                            </PropertiesDateEdit>
                                            <EditFormSettings Visible="False" />
                                        </dx:GridViewDataDateColumn>

                                        <dxwgv:GridViewDataTextColumn  VisibleIndex="6" Caption="Comments" FieldName="Comments">
                                           <EditFormSettings Visible="true" />
                                        </dxwgv:GridViewDataTextColumn>             

                                    </Columns>
                    
                                    <SettingsPager AllButton-Visible="true"></SettingsPager>
                                    <SettingsBehavior ColumnResizeMode="NextColumn" />
                                    <Styles Header-Wrap="True" />

                                </dxwgv:aspxgridview> 
                            </dxw:contentcontrol>
                        </ContentCollection>
                    </dxtc:TabPage>
                    <dxtc:TabPage Name="TotalHours24hTabPage" Text="Total Hours worked for last 24 hours">
                        <ContentCollection>
                            <dxw:contentcontrol ID="ContentControl2" runat="server">
                                <div>
                                    <dxwgv:aspxgridview id="grid24" width="100%"
                                        clientinstancename="grid24"
                                        runat="server"
                                        autogeneratecolumns="False"
                                        cssfilepath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                        csspostfix="Office2003Blue" datasourceid="FatigueManagement24hDS" onhtmlrowcreated="grid24_RowCreated" 
                                        OnAutoFilterCellEditorInitialize="grid24_AutoFilterCellEditorInitialize"
                                        OnAutoFilterCellEditorCreate="grid24_AutoFilterCellEditorCreate"
                                        OnProcessColumnAutoFilter="grid24_ProcessColumnAutoFilter"
                                        keyfieldname="FatigueEntryId">
                                        <Settings ShowFilterRow="true" ShowFilterBar="Visible" ShowGroupPanel="True"  />
                                        <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                            <ExpandedButton Height="12px" Width="11px" />
                                            <CollapsedButton Height="12px" Width="11px" />
                                            <DetailCollapsedButton Height="12px" Width="11px" />
                                            <DetailExpandedButton Height="12px" Width="11px" />
                                            <LoadingPanel Url="~/App_Themes/Office2003BlueOld/GridView/Loading.gif"><%--Enhancement_023:change by debashis for testing platform upgradation--%>
                                            </LoadingPanel>
                                        </Images>
                                        <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue">
                                            <Header ImageSpacing="5px" SortingImageSpacing="5px">
                                            </Header>
                                            <AlternatingRow Enabled="True">
                                            </AlternatingRow>
                                            <LoadingPanel ImageSpacing="10px">
                                            </LoadingPanel>
                                        </Styles>
              
                                        <Columns>
                                            <dxwgv:GridViewDataComboBoxColumn width="90" ReadOnly="true" VisibleIndex="1" Caption="Site" FieldName="SiteName" CellStyle-HorizontalAlign="Center">
                                                <PropertiesComboBox DataSourceID="FatigueSites24DS" DropDownHeight="150px" TextField="SiteName"
                                                     IncrementalFilteringMode="StartsWith" NullDisplayText=" ">
                                                </PropertiesComboBox>
                                                <EditFormSettings Visible="False" />
                                                <Settings SortMode="DisplayText"/>
                                                <CellStyle HorizontalAlign="Left"></CellStyle>
                                            </dxwgv:GridViewDataComboBoxColumn>

                                            <dxwgv:GridViewDataComboBoxColumn ReadOnly="true" VisibleIndex="2" Caption="Company Name" FieldName="CompanyName" CellStyle-HorizontalAlign="Center">
                                                <PropertiesComboBox DataSourceID="FatigueCompanies24DS" DropDownHeight="150px" TextField="CompanyName"
                                                     IncrementalFilteringMode="StartsWith" NullDisplayText=" ">
                                                </PropertiesComboBox>
                                                <EditFormSettings Visible="False" />
                                                <Settings SortMode="DisplayText"/>
                                                <CellStyle HorizontalAlign="Left"></CellStyle>
                                            </dxwgv:GridViewDataComboBoxColumn>

                        
                                            <dxwgv:GridViewDataComboBoxColumn width="75" ReadOnly="true" VisibleIndex="3" Caption="Clock Id#" FieldName="ClockId">
                                                <PropertiesComboBox DropDownHeight="150px"
                                                      TextField="ClockId" IncrementalFilteringMode="StartsWith" DropDownButton-Enabled="true" DataSourceId="FatigueClock24DS">
                                                </PropertiesComboBox>
                                               <EditFormSettings Visible="false" />
                                            </dxwgv:GridViewDataComboBoxColumn>
                         
                                            <dxwgv:GridViewDataComboBoxColumn width="140" ReadOnly="true" VisibleIndex="4" Caption="Full Name" FieldName="FullName">
                                                <PropertiesComboBox DropDownHeight="150px"
                                                    TextField="FullName" IncrementalFilteringMode="StartsWith" DropDownButton-Enabled="true" DataSourceId="FatigueFullName24DS">
                                                </PropertiesComboBox>
                                                <DataItemTemplate>
                                                    <table cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td style="width:100%">
                                                                <dx:ASPxHyperLink Width="100%" ID="hl24FullName" runat="server" Text="">
                                                                </dx:ASPxHyperLink>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </DataItemTemplate>
                                                <EditFormSettings Visible="false" />
                                            </dxwgv:GridViewDataComboBoxColumn>  
                                             
                                            <dxwgv:GridViewDataTextColumn width="100" ReadOnly="true" VisibleIndex="5" Caption="Total Hours" FieldName="TotalTime">
                                                <EditFormSettings Visible="false" />
                                            </dxwgv:GridViewDataTextColumn>                                                                                    

                                        </Columns>
                    
                                        <SettingsPager AllButton-Visible="true"></SettingsPager>
                                        <SettingsBehavior ColumnResizeMode="NextColumn" />
                                        <Styles Header-Wrap="True" />

                                    </dxwgv:aspxgridview> 
                                </div>  
                                <div style="float:right;">
                                    Last Updated: <asp:Label runat="server" id="LastUpdated24hLabel"></asp:Label>
                                </div>                      
                            </dxw:contentcontrol>
                        </ContentCollection>
                    </dxtc:TabPage>
                    <dxtc:TabPage Name="TotalHours7dTabPage" Text="Total Hours worked for last 7 days">
                        <ContentCollection>
                            <dxw:contentcontrol ID="ContentControl3" runat="server">
                                <div>
                                    <dxwgv:aspxgridview id="grid7" width="100%"
                                        clientinstancename="grid7"
                                        runat="server"
                                        autogeneratecolumns="False"
                                        cssfilepath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                        csspostfix="Office2003Blue" datasourceid="FatigueManagement7dDS" onhtmlrowcreated="grid7_RowCreated" 
                                        OnAutoFilterCellEditorInitialize="grid7_AutoFilterCellEditorInitialize"
                                        OnAutoFilterCellEditorCreate="grid7_AutoFilterCellEditorCreate"
                                        OnProcessColumnAutoFilter="grid7_ProcessColumnAutoFilter"
                                        keyfieldname="FatigueEntryId">
                                        <Settings ShowFilterRow="true" ShowFilterBar="Visible" ShowGroupPanel="True"  />
                                        <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                            <ExpandedButton Height="12px" Width="11px" />
                                            <CollapsedButton Height="12px" Width="11px" />
                                            <DetailCollapsedButton Height="12px" Width="11px" />
                                            <DetailExpandedButton Height="12px" Width="11px" />
                                            <LoadingPanel Url="~/App_Themes/Office2003BlueOld/GridView/Loading.gif"><%--Enhancement_023:change by debashis for testing platform upgradation--%>
                                            </LoadingPanel>
                                        </Images>
                                        <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue">
                                            <Header ImageSpacing="5px" SortingImageSpacing="5px">
                                            </Header>
                                            <AlternatingRow Enabled="True">
                                            </AlternatingRow>
                                            <LoadingPanel ImageSpacing="10px">
                                            </LoadingPanel>
                                        </Styles>
              
                                        <Columns>
                                            <dxwgv:GridViewDataComboBoxColumn width="90" ReadOnly="true" VisibleIndex="1" Caption="Site" FieldName="SiteName" CellStyle-HorizontalAlign="Center">
                                                <PropertiesComboBox DataSourceID="FatigueSites7DS" DropDownHeight="150px" TextField="SiteName"
                                                     IncrementalFilteringMode="StartsWith" NullDisplayText=" ">
                                                </PropertiesComboBox>
                                                <EditFormSettings Visible="False" />
                                                <Settings SortMode="DisplayText"/>
                                                <CellStyle HorizontalAlign="Left"></CellStyle>
                                            </dxwgv:GridViewDataComboBoxColumn>

                                            <dxwgv:GridViewDataComboBoxColumn ReadOnly="true" VisibleIndex="2" Caption="Company Name" FieldName="CompanyName" CellStyle-HorizontalAlign="Center">
                                                <PropertiesComboBox DataSourceID="FatigueCompanies7DS" DropDownHeight="150px" TextField="CompanyName"
                                                     IncrementalFilteringMode="StartsWith" NullDisplayText=" ">
                                                </PropertiesComboBox>
                                                <EditFormSettings Visible="False" />
                                                <Settings SortMode="DisplayText"/>
                                                <CellStyle HorizontalAlign="Left"></CellStyle>
                                            </dxwgv:GridViewDataComboBoxColumn>

                        
                                            <dxwgv:GridViewDataComboBoxColumn width="75" ReadOnly="true" VisibleIndex="3" Caption="Clock Id#" FieldName="ClockId">
                                                <PropertiesComboBox DropDownHeight="150px"
                                                      TextField="ClockId" IncrementalFilteringMode="StartsWith" DropDownButton-Enabled="true" DataSourceId="FatigueClock7DS">
                                                </PropertiesComboBox>
                                               <EditFormSettings Visible="false" />
                                            </dxwgv:GridViewDataComboBoxColumn>
                         
                                            <dxwgv:GridViewDataComboBoxColumn width="140" ReadOnly="true" VisibleIndex="4" Caption="Full Name" FieldName="FullName">
                                                <PropertiesComboBox DropDownHeight="150px"
                                                    TextField="FullName" IncrementalFilteringMode="StartsWith" DropDownButton-Enabled="true" DataSourceId="FatigueFullName7DS">
                                                </PropertiesComboBox>
                                                <DataItemTemplate>
                                                    <table cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td style="width:100%">
                                                                <dx:ASPxHyperLink Width="100%" ID="hl7FullName" runat="server" Text="">
                                                                </dx:ASPxHyperLink>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </DataItemTemplate>
                                                <EditFormSettings Visible="false" />
                                            </dxwgv:GridViewDataComboBoxColumn>  
                                             
                                            <dxwgv:GridViewDataTextColumn width="100" ReadOnly="true" VisibleIndex="5" Caption="Total Hours" FieldName="TotalTime">
                                                <EditFormSettings Visible="false" />
                                            </dxwgv:GridViewDataTextColumn>                                                                                    

                                        </Columns>
                    
                                        <SettingsPager AllButton-Visible="true"></SettingsPager>
                                        <SettingsBehavior ColumnResizeMode="NextColumn" />
                                        <Styles Header-Wrap="True" />

                                    </dxwgv:aspxgridview> 
                                </div>  
                                <div style="float:right;">
                                    Last Updated: <asp:Label runat="server" id="LastUpdated7dLabel"></asp:Label>
                                </div> 
                            </dxw:contentcontrol>
                        </ContentCollection>
                    </dxtc:TabPage>
                </TabPages>
                <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                </LoadingPanelImage>
                <LoadingPanelStyle ImageSpacing="6px">
                </LoadingPanelStyle>
                <ContentStyle>
                    <Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px" />
                </ContentStyle>
            </dxtc:ASPxPageControl>

        </td>
    </tr>

</table>
<br />
<br />


<data:CompaniesDataSource ID="SqlDataSourceCompanies" runat="server"></data:CompaniesDataSource>

<asp:SqlDataSource ID="FatigueManagementDS" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
                        SelectCommand="FatigueManagement_GetAll" SelectCommandType="StoredProcedure" 
                        UpdateCommand="FatigueManagement_Update" UpdateCommandType="StoredProcedure">
                        <UpdateParameters>
                        <asp:Parameter Name="Comments" Type="String" />
                        <asp:Parameter Name="FatigueEntryId" Type="int32" />
                        </UpdateParameters>
</asp:SqlDataSource>
<asp:SqlDataSource ID="FatigueCompaniesDS" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="Select ' ' as CompanyName union
                    Select Distinct C.CompanyName from FatigueManagementDetails A
                    left outer join CompanyVendor B
                    on A.Vendor_Number=B.Vendor_Number
                    left outer join Companies C
                    on B.Tax_Registration_Number=C.CompanyABN
                    where C.CompanyName Is Not Null and  A.TotalTime>0 order by CompanyName">
</asp:SqlDataSource>
<asp:SqlDataSource ID="FatigueSitesDS" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="Select '' as SiteName union
                    SELECT distinct S.SiteName
                    from FatigueManagementDetails A                         
                    left outer join Sites S on A.SiteId = S.SiteId              
                    where A.TotalTime>=0 order by SiteName Asc">
</asp:SqlDataSource>
<asp:SqlDataSource ID="FatigueClockDS" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="Select Distinct ClockId from FatigueManagementDetails order by ClockId">
</asp:SqlDataSource>
<asp:SqlDataSource ID="FatigueFullNameDS" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="Select Distinct FullName from FatigueManagementDetails order by FullName">
</asp:SqlDataSource>
<asp:SqlDataSource ID="FatigueHoursWorkedDS" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="Select Distinct TotalTime from FatigueManagementDetails order by TotalTime">
</asp:SqlDataSource>

<asp:SqlDataSource ID="FatigueManagement24hDS" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="FatigueManagement_Get24h" SelectCommandType="StoredProcedure">
</asp:SqlDataSource>
<asp:SqlDataSource ID="FatigueCompanies24DS" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="Select ' ' as CompanyName union
                    Select Distinct C.CompanyName from EbiHoursWorked A
                    left outer join CompanyVendor B
                    on A.Vendor_Number=B.Vendor_Number
                    left outer join Companies C
                    on B.Tax_Registration_Number=C.CompanyABN
                    where C.CompanyName Is Not Null and A.TotalTime>=0 and entrydatetime between dateadd(dd, -1, getdate()) and getdate() order by CompanyName">
</asp:SqlDataSource>
<asp:SqlDataSource ID="FatigueSites24DS" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="SELECT '' AS SiteName
                    UNION
                    SELECT DISTINCT S.SiteName
                    FROM EbiHoursWorked A
                    JOIN Sites S ON A.Site = S.SiteName
                    WHERE A.TotalTime >= 0 AND entrydatetime BETWEEN dateadd(dd, - 1, getdate()) AND getdate() ORDER BY SiteName ASC">
</asp:SqlDataSource>
<asp:SqlDataSource ID="FatigueClock24DS" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="Select Distinct ClockId from EbiHoursWorked where TotalTime>=0 and entrydatetime between dateadd(dd, -1, getdate()) and getdate() order by ClockId">
</asp:SqlDataSource>
<asp:SqlDataSource ID="FatigueFullName24DS" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="Select Distinct FullName from EbiHoursWorked where TotalTime>=0 and entrydatetime between dateadd(dd, -1, getdate()) and getdate() order by FullName">
</asp:SqlDataSource>

<asp:SqlDataSource ID="FatigueManagement7dDS" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="FatigueManagement_Get7d" SelectCommandType="StoredProcedure">
</asp:SqlDataSource>
<asp:SqlDataSource ID="FatigueCompanies7DS" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="Select ' ' as CompanyName union
                    Select Distinct C.CompanyName from EbiHoursWorked A
                    left outer join CompanyVendor B
                    on A.Vendor_Number=B.Vendor_Number
                    left outer join Companies C
                    on B.Tax_Registration_Number=C.CompanyABN
                    where C.CompanyName Is Not Null and A.TotalTime>=0 and entrydatetime between dateadd(dd, -7, getdate()) and getdate() order by CompanyName">
</asp:SqlDataSource>
<asp:SqlDataSource ID="FatigueSites7DS" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="SELECT '' AS SiteName
                    UNION
                    SELECT DISTINCT S.SiteName
                    FROM EbiHoursWorked A
                    JOIN Sites S ON A.Site = S.SiteName
                    WHERE A.TotalTime >= 0 AND entrydatetime BETWEEN dateadd(dd, - 7, getdate()) AND getdate() ORDER BY SiteName ASC">
</asp:SqlDataSource>
<asp:SqlDataSource ID="FatigueClock7DS" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="Select Distinct ClockId from EbiHoursWorked where TotalTime>=0 and entrydatetime between dateadd(dd, -7, getdate()) and getdate() order by ClockId">
</asp:SqlDataSource>
<asp:SqlDataSource ID="FatigueFullName7DS" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="Select Distinct FullName from EbiHoursWorked where TotalTime>=0 and entrydatetime between dateadd(dd, -7, getdate()) and getdate() order by FullName">
</asp:SqlDataSource>
