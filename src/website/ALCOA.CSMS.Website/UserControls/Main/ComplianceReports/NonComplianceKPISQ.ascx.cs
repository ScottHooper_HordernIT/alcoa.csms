﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Text;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using KaiZen.CSMS.Web;
using KaiZen.CSMS.Services;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxTabControl;
using DevExpress.Web.Data;
using DevExpress.Web.ASPxGridView;
using System.Drawing;
using DevExpress.Web.ASPxCallbackPanel;
using DevExpress.Web.ASPxClasses;
using DevExpress.XtraPrinting;


public partial class UserControls_NonComplianceKPISQ : System.Web.UI.UserControl
{
    Auth auth = new Auth();
    protected void Page_Load(object sender, EventArgs e) {  Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); }
    private void moduleInit(bool postBack)
    {
        if (!postBack)
        {

            if (SessionHandler.spVar_Page != "SummaryCompliance")
            {
                // Export
                String exportFileName = @"ALCOA CSMS - Non Compliance Report - SQ Validation against KPI data"; //Chrome & IE is fine.
                String userAgent = Request.Headers.Get("User-Agent");
                if (
                    (userAgent.Contains("Mozilla") && !userAgent.Contains("AppleWebKit") && !userAgent.Contains("MSIE")) || //Mozilla
                    (userAgent.Contains("AppleWebKit") && !userAgent.Contains("Chrome"))      //Safari
                    )
                {
                    exportFileName = exportFileName.Replace(" ", "_");
                }
                Helper.ExportGrid.Settings(ucExportButtons, exportFileName);
            }
            else
            {
                ucExportButtons.Visible = false;
            }


            BindData();

            switch (auth.RoleId)
            {
                case ((int)RoleList.Reader):
                    //full access
                    break;
                case ((int)RoleList.Contractor):
                    //restrict to own company
                    //sqldsNonComplianceReportYearContractor.DataBind();
                    //grid.DataSourceID = "sqldsNonComplianceReportYearContractor";
                    grid.Settings.ShowFilterRow = false;
                    grid.FilterExpression = String.Format("CompanyId = {0}", auth.CompanyId);
                    grid.DataBind();
                    break;
                case ((int)RoleList.Administrator):
                    //full access

                    break;
                default:
                    // do something
                    Response.Redirect("AccessDenied.aspx?error=" + Server.UrlEncode("Unrecognised User."));
                    break;
            }
        }
        else
        {
            grid.DataSource = (DataTable)Session["dtNonComplianceKPISQ"];
            grid.DataBind();
        }
    }

    public static string Capitalize(string value)
    {
        return System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(value);

    }
    protected void grid_RowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {
        //if (e.RowType != GridViewRowType.Data) return;

        //Label lblValid = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "lblValid") as Label;
        //Label lblApprovedForSite = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "lblApprovedForSite") as Label;
        //Label lblValidity = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "lblValidity") as Label;

        //lblValid.Text = " "; //NON-ASCII
        //lblApprovedForSite.Text = " "; //NON-ASCII
        //lblValidity.Text = " "; //NON-ASCII

        //if (lblValid != null && lblApprovedForSite != null && lblValid != null)
        //{
        //    if (grid.GetRowValues(e.VisibleIndex, "SiteId") != null && grid.GetRowValues(e.VisibleIndex, "CompanyId") != null)
        //    {
        //        int SiteId = (int)grid.GetRowValues(e.VisibleIndex, "SiteId");
        //        int CompanyId = (int)grid.GetRowValues(e.VisibleIndex, "CompanyId");

        //        int _count = 0;
        //        QuestionnaireFilters qFilters = new QuestionnaireFilters();
        //        qFilters.Append(QuestionnaireColumn.CompanyId, CompanyId.ToString());
        //        TList<Questionnaire> qList = DataRepository.QuestionnaireProvider.GetPaged(String.Format("CompanyId = {0}", CompanyId), "ModifiedDate DESC", 0, 9999, out _count);
        //        if (_count > 0)
        //        {

        //            lblValid.Text = "?";
        //            lblApprovedForSite.Text = "?";
        //            lblValidity.Text = "?";
        //            QuestionnaireInitialLocationService qilService = new QuestionnaireInitialLocationService();
        //            QuestionnaireInitialLocation qil = qilService.GetByQuestionnaireIdSiteId(qList[0].QuestionnaireId, SiteId);
        //            if (qil != null)
        //            {
        //                if (qil.Approved == true)
        //                {
        //                    lblApprovedForSite.Text = "Yes";
        //                }
        //                if (qil.Approved == false)
        //                {
        //                    lblApprovedForSite.Text = "No";
        //                }
        //                if (qil.Approved == null)
        //                {
        //                    lblApprovedForSite.Text = "Tentative";
        //                }
        //            }

        //            CompaniesService cService = new CompaniesService();
        //            Companies c = cService.GetByCompanyId(CompanyId);
        //            CompanyStatusService csService = new CompanyStatusService();
        //            CompanyStatus cs = csService.GetByCompanyStatusId(c.CompanyStatusId);


        //            if ((cs.CompanyStatusDesc.StartsWith("Active") || cs.CompanyStatusDesc.StartsWith("Acceptable")) 
        //                    && qList[0].MainAssessmentValidTo > DateTime.Now &&
        //                        qList[0].Status == (int)QuestionnaireStatusList.AssessmentComplete)
        //            {
        //                lblValid.Text = "Yes";
        //            }

        //            if (qList[0].MainAssessmentValidTo != null)
        //            {
        //                DateTime expiry = (DateTime)qList[0].MainAssessmentValidTo;
        //                if (DateTime.Now >= expiry)
        //                {
        //                    lblValidity.Text = "Expired";
        //                }
        //                if (expiry > DateTime.Now)
        //                {
        //                    TimeSpan span = expiry - DateTime.Now;
        //                    if (span.TotalDays <= 60)
        //                    {
        //                        lblValidity.Text = "Expiring";
        //                    }
        //                    else
        //                    {
        //                        lblValidity.Text = "Current";
        //                    }
        //                }
        //            }
        //        }
        //    }
        //}
    }

    protected void BindData()
    {
        DataSet ds = DataRepository.KpiProvider.GetReport_Compliance_SQ(1);

        DataTable dt = new DataTable();

        dt.Columns.Add("CompanyId", typeof(int));
        dt.Columns.Add("CompanyName2", typeof(string));
        dt.Columns.Add("SiteId", typeof(int));
        dt.Columns.Add("SiteName2", typeof(string));
        dt.Columns.Add("LastEbiSwipe", typeof(DateTime));
        dt.Columns.Add("LastKpiEntered", typeof(DateTime));
        dt.Columns.Add("Type", typeof(string));
        dt.Columns.Add("SqValidTo", typeof(DateTime));
        dt.Columns.Add("CompanySqStatus", typeof(string));

        dt.Columns.Add("ValidForSite", typeof(string));
        dt.Columns.Add("ApprovedForSite", typeof(string));
        dt.Columns.Add("SqValidity", typeof(string));

        dt.Columns.Add("ProcurementContact", typeof(string));
        dt.Columns.Add("HSAssessor", typeof(string));

        foreach (DataRow dr in ds.Tables[0].Rows)
        {
            DataRow newdr = dt.NewRow();
            newdr["CompanyId"] = dr["CompanyId"];
            newdr["CompanyName2"] = dr["CompanyName2"];
            newdr["SiteId"] = dr["SiteId"];
            newdr["SiteName2"] = dr["SiteName2"];
            newdr["LastEbiSwipe"] = dr["LastEbiSwipe"];
            newdr["LastKpiEntered"] = dr["LastKpiEntered"];
            newdr["Type"] = dr["Type"];
            newdr["SqValidTo"] = dr["SqValidTo"];
            newdr["CompanySqStatus"] = dr["CompanySqStatus"];

            string ProcurementContact = "(n/a)";
            string HSAssessor = "(n/a)";

            CompaniesService cService = new CompaniesService();
            CompanyStatusService csService = new CompanyStatusService();

            try
            {
                string Valid = "";
                string ApprovedForSite = "";
                string Validity = "";

                if (newdr["CompanyId"] != null)
                {
                    int CompanyId = (int)newdr["CompanyId"];
                    if (CompanyId != -1)
                    {
                        QuestionnaireWithLocationApprovalViewLatestService qwlavlService = new QuestionnaireWithLocationApprovalViewLatestService();
                        DataSet DSqwlavl = qwlavlService.GetProcurementContactHsAssessor_ByCompanyId(CompanyId);
                        if (DSqwlavl.Tables.Count > 0)
                        {
                            if (DSqwlavl.Tables[0].Rows.Count > 0)
                            {
                                foreach (DataRow DRqwlavl in DSqwlavl.Tables[0].Rows)
                                {
                                    ProcurementContact = DRqwlavl[0].ToString();
                                    HSAssessor = DRqwlavl[1].ToString();
                                }
                            }
                        }
                    }
                }

                if (newdr["SiteId"] != null && newdr["CompanyId"] != null)
                {
                    int SiteId = (int)newdr["SiteId"];
                    int CompanyId = (int)newdr["CompanyId"];

                    int _count = 0;
                    QuestionnaireFilters qFilters = new QuestionnaireFilters();
                    qFilters.Append(QuestionnaireColumn.CompanyId, CompanyId.ToString());
                    TList<Questionnaire> qList = DataRepository.QuestionnaireProvider.GetPaged(String.Format("CompanyId = {0}", CompanyId), "ModifiedDate DESC", 0, 9999, out _count);
                    if (_count > 0)
                    {

                        Valid = "?";
                        ApprovedForSite = "?";
                        Validity = "?";
                        CompanySiteCategoryStandardService cscsService = new CompanySiteCategoryStandardService();
                        CompanySiteCategoryStandard cscs = cscsService.GetByCompanyIdSiteId(qList[0].CompanyId, SiteId);
                        if (cscs != null)
                        {
                            if (cscs.Approved == true)
                            {
                                ApprovedForSite = "Yes";
                            }
                            if (cscs.Approved == false)
                            {
                                ApprovedForSite = "No";
                            }
                            if (cscs.Approved == null)
                            {
                                ApprovedForSite = "Tentative";
                            }
                        }

                        Companies c = cService.GetByCompanyId(CompanyId);
                        CompanyStatus cs = csService.GetByCompanyStatusId(c.CompanyStatusId);


                        if ((cs.CompanyStatusDesc.StartsWith("Active") || cs.CompanyStatusDesc.StartsWith("Acceptable"))
                                && qList[0].MainAssessmentValidTo > DateTime.Now &&
                                    qList[0].Status == (int)QuestionnaireStatusList.AssessmentComplete)
                        {
                            Valid = "Yes";
                        }

                        if (qList[0].MainAssessmentValidTo != null)
                        {
                            DateTime expiry = (DateTime)qList[0].MainAssessmentValidTo;
                            if (DateTime.Now >= expiry)
                            {
                                Validity = "Expired";
                            }
                            if (expiry > DateTime.Now)
                            {
                                TimeSpan span = expiry - DateTime.Now;
                                if (span.TotalDays <= 60)
                                {
                                    Validity = "Expiring";
                                }
                                else
                                {
                                    Validity = "Current";
                                }
                            }
                        }
                    }
                }

                newdr["ProcurementContact"] = ProcurementContact;
                newdr["HSAssessor"] = HSAssessor;

                newdr["ValidForSite"] = Valid;
                newdr["ApprovedForSite"] = ApprovedForSite;
                newdr["SqValidity"] = Validity;

                dt.Rows.Add(newdr);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromContext(Context).Raise(ex);
            }
        }

        Session["dtNonComplianceKPISQ"] = dt;
        grid.DataSource = (DataTable)Session["dtNonComplianceKPISQ"];
        grid.DataBind();
    }

    protected void grid_AutoFilterCellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
    {
        if (e.Column.FieldName == "CompanyId" || e.Column.FieldName == "SiteName")
        {
            ASPxComboBox comboBox = e.Editor as ASPxComboBox;
            comboBox.ClientSideEvents.Init = "function(s, e) {s.InsertItem(0, '(ALL)', ''); s.RemoveItem(-1);}";
        }
    }
 
}
