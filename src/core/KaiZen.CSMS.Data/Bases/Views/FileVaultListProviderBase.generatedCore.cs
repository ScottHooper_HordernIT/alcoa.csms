﻿#region Using directives

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="FileVaultListProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract class FileVaultListProviderBaseCore : EntityViewProviderBase<FileVaultList>
	{
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions
		
		/*
		///<summary>
		/// Fill an VList&lt;FileVaultList&gt; From a DataSet
		///</summary>
		/// <param name="dataSet">the DataSet</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList&lt;FileVaultList&gt;"/></returns>
		protected static VList&lt;FileVaultList&gt; Fill(DataSet dataSet, VList<FileVaultList> rows, int start, int pagelen)
		{
			if (dataSet.Tables.Count == 1)
			{
				return Fill(dataSet.Tables[0], rows, start, pagelen);
			}
			else
			{
				return new VList<FileVaultList>();
			}	
		}
		
		
		///<summary>
		/// Fill an VList&lt;FileVaultList&gt; From a DataTable
		///</summary>
		/// <param name="dataTable">the DataTable that hold the data.</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList<FileVaultList>"/></returns>
		protected static VList&lt;FileVaultList&gt; Fill(DataTable dataTable, VList<FileVaultList> rows, int start, int pagelen)
		{
			int recordnum = 0;
			
			System.Collections.IEnumerator dataRows =  dataTable.Rows.GetEnumerator();
			
			while (dataRows.MoveNext() && (pagelen != 0))
			{
				if(recordnum >= start)
				{
					DataRow row = (DataRow)dataRows.Current;
				
					FileVaultList c = new FileVaultList();
					c.FileVaultId = (Convert.IsDBNull(row["FileVaultId"]))?(int)0:(System.Int32)row["FileVaultId"];
					c.FileName = (Convert.IsDBNull(row["FileName"]))?string.Empty:(System.String)row["FileName"];
					c.FileHash = (Convert.IsDBNull(row["FileHash"]))?new byte[] {}:(System.Byte[])row["FileHash"];
					c.ContentLength = (Convert.IsDBNull(row["ContentLength"]))?(int)0:(System.Int32)row["ContentLength"];
					c.ModifiedDate = (Convert.IsDBNull(row["ModifiedDate"]))?DateTime.MinValue:(System.DateTime)row["ModifiedDate"];
					c.ModifiedByUserId = (Convert.IsDBNull(row["ModifiedByUserId"]))?(int)0:(System.Int32)row["ModifiedByUserId"];
					c.FileVaultCategoryId = (Convert.IsDBNull(row["FileVaultCategoryId"]))?(int)0:(System.Int32)row["FileVaultCategoryId"];
					c.CategoryDesc = (Convert.IsDBNull(row["CategoryDesc"]))?string.Empty:(System.String)row["CategoryDesc"];
					c.CategoryName = (Convert.IsDBNull(row["CategoryName"]))?string.Empty:(System.String)row["CategoryName"];
					c.AcceptChanges();
					rows.Add(c);
					pagelen -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		*/	
						
		///<summary>
		/// Fill an <see cref="VList&lt;FileVaultList&gt;"/> From a DataReader.
		///</summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pageLength">number of row.</param>
		///<returns>a <see cref="VList&lt;FileVaultList&gt;"/></returns>
		protected VList<FileVaultList> Fill(IDataReader reader, VList<FileVaultList> rows, int start, int pageLength)
		{
			int recordnum = 0;
			while (reader.Read() && (pageLength != 0))
			{
				if(recordnum >= start)
				{
					FileVaultList entity = null;
					if (DataRepository.Provider.UseEntityFactory)
					{
						entity = EntityManager.CreateViewEntity<FileVaultList>("FileVaultList",  DataRepository.Provider.EntityCreationalFactoryType); 
					}
					else
					{
						entity = new FileVaultList();
					}
					
					entity.SuppressEntityEvents = true;

					entity.FileVaultId = (System.Int32)reader[((int)FileVaultListColumn.FileVaultId)];
					//entity.FileVaultId = (Convert.IsDBNull(reader["FileVaultId"]))?(int)0:(System.Int32)reader["FileVaultId"];
					entity.FileName = (System.String)reader[((int)FileVaultListColumn.FileName)];
					//entity.FileName = (Convert.IsDBNull(reader["FileName"]))?string.Empty:(System.String)reader["FileName"];
					entity.FileHash = (System.Byte[])reader[((int)FileVaultListColumn.FileHash)];
					//entity.FileHash = (Convert.IsDBNull(reader["FileHash"]))?new byte[] {}:(System.Byte[])reader["FileHash"];
					entity.ContentLength = (System.Int32)reader[((int)FileVaultListColumn.ContentLength)];
					//entity.ContentLength = (Convert.IsDBNull(reader["ContentLength"]))?(int)0:(System.Int32)reader["ContentLength"];
					entity.ModifiedDate = (System.DateTime)reader[((int)FileVaultListColumn.ModifiedDate)];
					//entity.ModifiedDate = (Convert.IsDBNull(reader["ModifiedDate"]))?DateTime.MinValue:(System.DateTime)reader["ModifiedDate"];
					entity.ModifiedByUserId = (System.Int32)reader[((int)FileVaultListColumn.ModifiedByUserId)];
					//entity.ModifiedByUserId = (Convert.IsDBNull(reader["ModifiedByUserId"]))?(int)0:(System.Int32)reader["ModifiedByUserId"];
					entity.FileVaultCategoryId = (System.Int32)reader[((int)FileVaultListColumn.FileVaultCategoryId)];
					//entity.FileVaultCategoryId = (Convert.IsDBNull(reader["FileVaultCategoryId"]))?(int)0:(System.Int32)reader["FileVaultCategoryId"];
					entity.CategoryDesc = (System.String)reader[((int)FileVaultListColumn.CategoryDesc)];
					//entity.CategoryDesc = (Convert.IsDBNull(reader["CategoryDesc"]))?string.Empty:(System.String)reader["CategoryDesc"];
					entity.CategoryName = (System.String)reader[((int)FileVaultListColumn.CategoryName)];
					//entity.CategoryName = (Convert.IsDBNull(reader["CategoryName"]))?string.Empty:(System.String)reader["CategoryName"];
					entity.AcceptChanges();
					entity.SuppressEntityEvents = false;
					
					rows.Add(entity);
					pageLength -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		
		
		/// <summary>
		/// Refreshes the <see cref="FileVaultList"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="FileVaultList"/> object to refresh.</param>
		protected void RefreshEntity(IDataReader reader, FileVaultList entity)
		{
			reader.Read();
			entity.FileVaultId = (System.Int32)reader[((int)FileVaultListColumn.FileVaultId)];
			//entity.FileVaultId = (Convert.IsDBNull(reader["FileVaultId"]))?(int)0:(System.Int32)reader["FileVaultId"];
			entity.FileName = (System.String)reader[((int)FileVaultListColumn.FileName)];
			//entity.FileName = (Convert.IsDBNull(reader["FileName"]))?string.Empty:(System.String)reader["FileName"];
			entity.FileHash = (System.Byte[])reader[((int)FileVaultListColumn.FileHash)];
			//entity.FileHash = (Convert.IsDBNull(reader["FileHash"]))?new byte[] {}:(System.Byte[])reader["FileHash"];
			entity.ContentLength = (System.Int32)reader[((int)FileVaultListColumn.ContentLength)];
			//entity.ContentLength = (Convert.IsDBNull(reader["ContentLength"]))?(int)0:(System.Int32)reader["ContentLength"];
			entity.ModifiedDate = (System.DateTime)reader[((int)FileVaultListColumn.ModifiedDate)];
			//entity.ModifiedDate = (Convert.IsDBNull(reader["ModifiedDate"]))?DateTime.MinValue:(System.DateTime)reader["ModifiedDate"];
			entity.ModifiedByUserId = (System.Int32)reader[((int)FileVaultListColumn.ModifiedByUserId)];
			//entity.ModifiedByUserId = (Convert.IsDBNull(reader["ModifiedByUserId"]))?(int)0:(System.Int32)reader["ModifiedByUserId"];
			entity.FileVaultCategoryId = (System.Int32)reader[((int)FileVaultListColumn.FileVaultCategoryId)];
			//entity.FileVaultCategoryId = (Convert.IsDBNull(reader["FileVaultCategoryId"]))?(int)0:(System.Int32)reader["FileVaultCategoryId"];
			entity.CategoryDesc = (System.String)reader[((int)FileVaultListColumn.CategoryDesc)];
			//entity.CategoryDesc = (Convert.IsDBNull(reader["CategoryDesc"]))?string.Empty:(System.String)reader["CategoryDesc"];
			entity.CategoryName = (System.String)reader[((int)FileVaultListColumn.CategoryName)];
			//entity.CategoryName = (Convert.IsDBNull(reader["CategoryName"]))?string.Empty:(System.String)reader["CategoryName"];
			reader.Close();
	
			entity.AcceptChanges();
		}
		
		/*
		/// <summary>
		/// Refreshes the <see cref="FileVaultList"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="FileVaultList"/> object.</param>
		protected static void RefreshEntity(DataSet dataSet, FileVaultList entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.FileVaultId = (Convert.IsDBNull(dataRow["FileVaultId"]))?(int)0:(System.Int32)dataRow["FileVaultId"];
			entity.FileName = (Convert.IsDBNull(dataRow["FileName"]))?string.Empty:(System.String)dataRow["FileName"];
			entity.FileHash = (Convert.IsDBNull(dataRow["FileHash"]))?new byte[] {}:(System.Byte[])dataRow["FileHash"];
			entity.ContentLength = (Convert.IsDBNull(dataRow["ContentLength"]))?(int)0:(System.Int32)dataRow["ContentLength"];
			entity.ModifiedDate = (Convert.IsDBNull(dataRow["ModifiedDate"]))?DateTime.MinValue:(System.DateTime)dataRow["ModifiedDate"];
			entity.ModifiedByUserId = (Convert.IsDBNull(dataRow["ModifiedByUserId"]))?(int)0:(System.Int32)dataRow["ModifiedByUserId"];
			entity.FileVaultCategoryId = (Convert.IsDBNull(dataRow["FileVaultCategoryId"]))?(int)0:(System.Int32)dataRow["FileVaultCategoryId"];
			entity.CategoryDesc = (Convert.IsDBNull(dataRow["CategoryDesc"]))?string.Empty:(System.String)dataRow["CategoryDesc"];
			entity.CategoryName = (Convert.IsDBNull(dataRow["CategoryName"]))?string.Empty:(System.String)dataRow["CategoryName"];
			entity.AcceptChanges();
		}
		*/
			
		#endregion Helper Functions
	}//end class

	#region FileVaultListFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="FileVaultList"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FileVaultListFilterBuilder : SqlFilterBuilder<FileVaultListColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FileVaultListFilterBuilder class.
		/// </summary>
		public FileVaultListFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the FileVaultListFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public FileVaultListFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the FileVaultListFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public FileVaultListFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion FileVaultListFilterBuilder

	#region FileVaultListParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="FileVaultList"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FileVaultListParameterBuilder : ParameterizedSqlFilterBuilder<FileVaultListColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FileVaultListParameterBuilder class.
		/// </summary>
		public FileVaultListParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the FileVaultListParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public FileVaultListParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the FileVaultListParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public FileVaultListParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion FileVaultListParameterBuilder
	
	#region FileVaultListSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="FileVaultList"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class FileVaultListSortBuilder : SqlSortBuilder<FileVaultListColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FileVaultListSqlSortBuilder class.
		/// </summary>
		public FileVaultListSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion FileVaultListSortBuilder

} // end namespace
