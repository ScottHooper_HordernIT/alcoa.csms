﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.CurrentSmpEhsConsultantIdProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[CLSCompliant(true)]
	[Designer(typeof(CurrentSmpEhsConsultantIdDataSourceDesigner))]
	public class CurrentSmpEhsConsultantIdDataSource : ReadOnlyDataSource<CurrentSmpEhsConsultantId>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CurrentSmpEhsConsultantIdDataSource class.
		/// </summary>
		public CurrentSmpEhsConsultantIdDataSource() : base(new CurrentSmpEhsConsultantIdService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the CurrentSmpEhsConsultantIdDataSourceView used by the CurrentSmpEhsConsultantIdDataSource.
		/// </summary>
		protected CurrentSmpEhsConsultantIdDataSourceView CurrentSmpEhsConsultantIdView
		{
			get { return ( View as CurrentSmpEhsConsultantIdDataSourceView ); }
		}
		
		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the CurrentSmpEhsConsultantIdDataSourceView class that is to be
		/// used by the CurrentSmpEhsConsultantIdDataSource.
		/// </summary>
		/// <returns>An instance of the CurrentSmpEhsConsultantIdDataSourceView class.</returns>
		protected override BaseDataSourceView<CurrentSmpEhsConsultantId, Object> GetNewDataSourceView()
		{
			return new CurrentSmpEhsConsultantIdDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the CurrentSmpEhsConsultantIdDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class CurrentSmpEhsConsultantIdDataSourceView : ReadOnlyDataSourceView<CurrentSmpEhsConsultantId>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CurrentSmpEhsConsultantIdDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the CurrentSmpEhsConsultantIdDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public CurrentSmpEhsConsultantIdDataSourceView(CurrentSmpEhsConsultantIdDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal CurrentSmpEhsConsultantIdDataSource CurrentSmpEhsConsultantIdOwner
		{
			get { return Owner as CurrentSmpEhsConsultantIdDataSource; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal CurrentSmpEhsConsultantIdService CurrentSmpEhsConsultantIdProvider
		{
			get { return Provider as CurrentSmpEhsConsultantIdService; }
		}

		#endregion Properties
		
		#region Methods
		
		#endregion Methods
	}

	#region CurrentSmpEhsConsultantIdDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the CurrentSmpEhsConsultantIdDataSource class.
	/// </summary>
	public class CurrentSmpEhsConsultantIdDataSourceDesigner : ReadOnlyDataSourceDesigner<CurrentSmpEhsConsultantId>
	{
	}

	#endregion CurrentSmpEhsConsultantIdDataSourceDesigner

	#region CurrentSmpEhsConsultantIdFilter

	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CurrentSmpEhsConsultantId"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CurrentSmpEhsConsultantIdFilter : SqlFilter<CurrentSmpEhsConsultantIdColumn>
	{
	}

	#endregion CurrentSmpEhsConsultantIdFilter

	#region CurrentSmpEhsConsultantIdExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CurrentSmpEhsConsultantId"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CurrentSmpEhsConsultantIdExpressionBuilder : SqlExpressionBuilder<CurrentSmpEhsConsultantIdColumn>
	{
	}
	
	#endregion CurrentSmpEhsConsultantIdExpressionBuilder		
}

