<%@ Control Language="C#" AutoEventWireup="true"
    Inherits="UserControls_SqQuestionnaire_Questionnaire3Verification" Codebehind="Questionnaire3Verification.ascx.cs" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallback" TagPrefix="dxcb" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxNavBar" TagPrefix="dxnb" %>
<%@ Register Assembly="DevExpress.Web.ASPxGauges.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGauges" TagPrefix="dxg" %>
<%@ Register Assembly="DevExpress.Web.ASPxGauges.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGauges.Gauges" TagPrefix="dxg" %>
<%@ Register Assembly="DevExpress.Web.ASPxGauges.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGauges.Gauges.Linear" TagPrefix="dxg" %>
<%@ Register Assembly="DevExpress.Web.ASPxGauges.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGauges.Gauges.Circular" TagPrefix="dxg" %>
<%@ Register Assembly="DevExpress.Web.ASPxGauges.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGauges.Gauges.State" TagPrefix="dxg" %>
<%@ Register Assembly="DevExpress.Web.ASPxGauges.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGauges.Gauges.Digital" TagPrefix="dxg" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallbackPanel" TagPrefix="dxcp" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxRoundPanel" TagPrefix="dxrp" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dxp" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<table border="0" cellpadding="2" cellspacing="0" width="100%">
    <tr>
        <td class="pageName" colspan="3" style="height: 17px">
            <span class="bodycopy"><span class="title">Safety Qualification</span><br />
                <span class="date">
                    <dxe:ASPxHyperLink ID="hlQuestionnaire" runat="server" Text="Questionnaire" ToolTip="Go back to Safety Qualification Page"
                        NavigateUrl="~/SafetyPQ_Questionnaire.aspx">
                    </dxe:ASPxHyperLink>
                    >
                    <asp:LinkButton ID="LinkButton1" runat="server">Assess/Review</asp:LinkButton></span>&nbsp;
                &gt; Verification Questionnaire<br />
                <img height="1" src="images/grfc_dottedline.gif" width="24" />&nbsp;</span>
        </td>
    </tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" style="width: 900px">
    <tr>
        <td style="width: 900px; height: 46px; text-align: center">
            <div align="center">
                <table border="0" cellpadding="1" cellspacing="1">
                    <tr>
                        <td style="height: 40px">
                            <dxe:ASPxButton ID="btnHome" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                CssPostfix="Office2003Blue" OnClick="btnHome_Click" Text="Home" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                            </dxe:ASPxButton>
                        </td>
                        <td style="width: 60px; height: 40px">
                            <dxe:ASPxButton ID="btnSubmit" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                CssPostfix="Office2003Blue" OnClick="btnSubmit_Click" Text="Submit" ValidationGroup="Submit"
                                SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                            </dxe:ASPxButton>
                        </td>
                    </tr>
                </table>
                <table>
                <tr>
                <td colspan="2">
                <dxe:ASPxCheckBox ID="ASPxCheckBox1" Text="By submitting this questionnaire I hereby certify that all statements provided herein are a true and correct representation of company wide information. I also acknowledge that all other previous submissions will become null and void."
                            runat="server" Width="726px" ForeColor="Maroon" Enabled="false">
                            <ValidationSettings ErrorDisplayMode="ImageWithTooltip" CausesValidation="True" RequiredField-ErrorText="Submission Verification - You are required to check this."
                                SetFocusOnError="False" ValidationGroup="Submit">
                                <RequiredField IsRequired="True"></RequiredField>
                            </ValidationSettings>
                        </dxe:ASPxCheckBox>
                </td>
                </tr>
                </table>
            </div>
            <asp:Label ID="lblSave" runat="server" Font-Italic="True" ForeColor="Red"></asp:Label>
        </td>
    </tr>
    <tr>
        <td style="width: 900px; height: 46px; text-align: left">
            <table border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 198px; height: 13px">
                        <strong><span style="font-size: 12pt">Company Name: </span></strong>
                    </td>
                    <td style="width: 157px; height: 13px">
                        <asp:Label ID="lblCompanyName" runat="server" Font-Size="12pt" Width="291px"></asp:Label>
                    </td>
                    <td style="width: 570px; height: 13px; text-align: right">
                        <strong>Status: </strong>
                    </td>
                    <td style="width: 100px; height: 13px">
                        <dxe:ASPxLabel ID="lblStatus" runat="server" Font-Bold="False" ForeColor="#C04000"
                            Text="Not Saved" Font-Size="12pt" Width="100%">
                        </dxe:ASPxLabel>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>

<asp:Panel ID="pnlContainer" runat="server" Width="900px">
    <table style="font-size: 14pt" width="900">
        <tbody>
            <tr>
                <td colspan="4" style="text-align: center">
                    <span style="color: #660033"><strong>
                        <asp:Label ID="Label3" runat="server" Font-Bold="True" ForeColor="Maroon" Text="Click section number or text to answer section."></asp:Label><br />
                        <asp:Label ID="Label2" runat="server" Font-Bold="True" ForeColor="Maroon" Text="Please note that you can only submit this questionnaire once all sections have been completed."></asp:Label></strong></span>
                </td>
            </tr>
        </tbody>
    </table>
    <table id="Table1" border="0" cellpadding="0" cellspacing="0" class="dxgvControl_Office2003_Blue"
        style="border-collapse: separate">
        <tbody>
            <tr>
                <td style="width: 900px; text-align: center">
                    <table border="0" cellpadding="0" cellspacing="0" class="dxgvControl_Office2003_Blue"
                        style="width: 100%; border-collapse: collapse; empty-cells: show">
                        <tbody>
                            <tr>
                                <td class="dxgvHeader_Office2003_Blue" colspan="1" style="border-top-width: 0px;
                                    font-weight: bold; border-left-width: 0px; white-space: normal; height: 13px;">
                                </td>
                                <td class="dxgvHeader_Office2003_Blue" colspan="1" style="border-top-width: 0px;
                                    font-weight: bold; border-left-width: 0px; white-space: normal; text-align: center;
                                    width: 972px; height: 13px;">
                                    Section
                                </td>
                                <td class="dxgvHeader_Office2003_Blue" colspan="1" style="border-top-width: 0px;
                                    font-weight: bold; border-left-width: 0px; width: 216px; white-space: normal;
                                    height: 13px; text-align: center">
                                    <asp:Label ID="lblApproved" runat="server" Font-Bold="True" ForeColor="Black" Text="Approved"></asp:Label>
                                </td>
                                <td class="dxgvHeader_Office2003_Blue" colspan="1" style="border-top-width: 0px;
                                    font-weight: bold; border-left-width: 0px; white-space: normal; text-align: center;
                                    width: 216px; height: 13px;">
                                    Complete
                                </td>
                                <td class="dxgvHeader_Office2003_Blue" colspan="1" style="border-top-width: 0px;
                                    font-weight: bold; border-left-width: 0px; width: 480px; white-space: normal;
                                    text-align: center; height: 13px;">
                                    <asp:Label ID="lblComments" runat="server" Font-Bold="True" ForeColor="Black" Text="Comments"></asp:Label>
                                </td>
                            </tr>
                            <tr class="dxgvDataRow_Office2003_Blue dxgvDataRow_Office2003_Blue">
                                <td class="dxgv" style="width: 20px; height: 20px; text-align: center">
                                    <asp:LinkButton ID="lb1_" runat="server" Font-Size="10pt">1</asp:LinkButton>
                                </td>
                                <td class="dxgv" style="width: 972px; height: 20px; text-align: left">
                                    &nbsp;<asp:LinkButton ID="lb1" runat="server" Font-Size="10pt">Management Commitment to Safety</asp:LinkButton>
                                </td>
                                <td class="dxgv" style="width: 216px; height: 20px; text-align: center">
                                    <asp:Label ID="lblApproval1" runat="server" ForeColor="Black" Text="-"></asp:Label>
                                </td>
                                <td class="dxgv" style="width: 216px; height: 20px; text-align: center">
                                    <asp:Label ID="lbl1a" runat="server" Text="No" ForeColor="Red"></asp:Label>
                                </td>
                                <td class="dxgv" style="width: 480px; height: 20px; text-align: center">
                                    <asp:Label ID="lbl1b" runat="server" ForeColor="Red" Text="No evidence or comment supplied"
                                        Visible="False"></asp:Label>
                                </td>
                            </tr>
                            <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                <td class="dxgv" style="width: 20px; height: 20px; text-align: center">
                                    <asp:LinkButton ID="lb2_" runat="server" Font-Size="10pt">2</asp:LinkButton>
                                </td>
                                <td class="dxgv" style="width: 972px; height: 20px; text-align: left">
                                    &nbsp;<asp:LinkButton ID="lb2" runat="server" Font-Size="10pt">Hazard & Risk Management</asp:LinkButton>
                                </td>
                                <td class="dxgv" style="width: 216px; height: 20px; text-align: center">
                                    <asp:Label ID="lblApproval2" runat="server" ForeColor="Black" Text="-"></asp:Label>
                                </td>
                                <td class="dxgv" style="width: 216px; height: 20px; text-align: center">
                                    <asp:Label ID="lbl2a" runat="server" Text="No" ForeColor="Red"></asp:Label>
                                </td>
                                <td class="dxgv" style="width: 480px; height: 20px; text-align: center">
                                    <asp:Label ID="lbl2b" runat="server" ForeColor="Red" Text="No evidence or comment supplied"
                                        Visible="False"></asp:Label>
                                </td>
                            </tr>
                            <tr class="dxgvDataRow_Office2003_Blue dxgvDataRow_Office2003_Blue">
                                <td class="dxgv" style="width: 20px; height: 20px; text-align: center">
                                    <asp:LinkButton ID="lb4_" runat="server" Font-Size="10pt">3</asp:LinkButton>
                                </td>
                                <td class="dxgv" style="width: 972px; height: 20px; text-align: left">
                                    &nbsp;<asp:LinkButton ID="lb4" runat="server" Font-Size="10pt">People Skills and Training</asp:LinkButton>
                                </td>
                                <td class="dxgv" style="width: 216px; height: 20px; text-align: center">
                                    <asp:Label ID="lblApproval4" runat="server" ForeColor="Black" Text="-"></asp:Label>
                                </td>
                                <td class="dxgv" style="width: 216px; height: 20px; text-align: center">
                                    <asp:Label ID="lbl4a" runat="server" Text="No" ForeColor="Red"></asp:Label>
                                </td>
                                <td class="dxgv" style="width: 480px; height: 20px; text-align: center">
                                    <asp:Label ID="lbl4b" runat="server" ForeColor="Red" Text="No evidence or comment supplied"
                                        Visible="False"></asp:Label>
                                </td>
                            </tr>
                            <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                <td class="dxgv" style="width: 20px; height: 16px; text-align: center">
                                    <asp:LinkButton ID="lb5_" runat="server" Font-Size="10pt">4</asp:LinkButton>
                                </td>
                                <td class="dxgv" style="width: 972px; height: 16px; text-align: left">
                                    &nbsp;<asp:LinkButton ID="lb5" runat="server" Font-Size="10pt">Health & Safety Performance Management</asp:LinkButton>
                                </td>
                                <td class="dxgv" style="width: 216px; height: 16px; text-align: center">
                                    <asp:Label ID="lblApproval5" runat="server" ForeColor="Black" Text="-"></asp:Label>
                                </td>
                                <td class="dxgv" style="width: 216px; height: 16px; text-align: center">
                                    <asp:Label ID="lbl5a" runat="server" Text="No" ForeColor="Red"></asp:Label>
                                </td>
                                <td class="dxgv" style="width: 480px; height: 16px; text-align: center">
                                    <asp:Label ID="lbl5b" runat="server" ForeColor="Red" Text="No evidence or comment supplied"
                                        Visible="False"></asp:Label>
                                </td>
                            </tr>
                            <tr class="dxgvDataRow_Office2003_Blue dxgvDataRow_Office2003_Blue">
                                <td class="dxgv" style="width: 20px; height: 20px; text-align: center">
                                    <asp:LinkButton ID="lb6_" runat="server" Font-Size="10pt">5</asp:LinkButton>
                                </td>
                                <td class="dxgv" style="width: 972px; height: 20px; text-align: left">
                                    &nbsp;<asp:LinkButton ID="lb6" runat="server" Font-Size="10pt">Communication</asp:LinkButton>
                                </td>
                                <td class="dxgv" style="width: 216px; height: 20px; text-align: center">
                                    <asp:Label ID="lblApproval6" runat="server" ForeColor="Black" Text="-"></asp:Label>
                                </td>
                                <td class="dxgv" style="width: 216px; height: 20px; text-align: center">
                                    <asp:Label ID="lbl6a" runat="server" Text="No" ForeColor="Red"></asp:Label>
                                </td>
                                <td class="dxgv" style="width: 480px; height: 20px; text-align: center">
                                    <asp:Label ID="lbl6b" runat="server" ForeColor="Red" Text="No evidence or comment supplied"
                                        Visible="False"></asp:Label>
                                </td>
                            </tr>
                            <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                <td class="dxgv" style="width: 20px; height: 20px; text-align: center">
                                    <asp:LinkButton ID="lb7_" runat="server" Font-Size="10pt">6</asp:LinkButton>
                                </td>
                                <td class="dxgv" style="width: 972px; height: 20px; text-align: left">
                                    &nbsp;<asp:LinkButton ID="lb7" runat="server" Font-Size="10pt">Incident and Injury Investigation</asp:LinkButton>
                                </td>
                                <td class="dxgv" style="width: 216px; height: 20px; text-align: center">
                                    <asp:Label ID="lblApproval7" runat="server" ForeColor="Black" Text="-"></asp:Label>
                                </td>
                                <td class="dxgv" style="width: 216px; height: 20px; text-align: center">
                                    <asp:Label ID="lbl7a" runat="server" Text="No" ForeColor="Red"></asp:Label>
                                </td>
                                <td class="dxgv" style="width: 480px; height: 20px; text-align: center">
                                    <asp:Label ID="lbl7b" runat="server" ForeColor="Red" Text="No evidence or comment supplied"
                                        Visible="False"></asp:Label>
                                </td>
                            </tr>
                            <tr class="dxgvDataRow_Office2003_Blue dxgvDataRow_Office2003_Blue">
                                <td class="dxgv" style="width: 20px; height: 20px; text-align: center">
                                    <asp:LinkButton ID="lb8_" runat="server" Font-Size="10pt">7</asp:LinkButton>
                                </td>
                                <td class="dxgv" style="width: 972px; height: 20px; text-align: left">
                                    &nbsp;<asp:LinkButton ID="lb8" runat="server" Font-Size="10pt">Health & Safety Documents and Records Management</asp:LinkButton>
                                </td>
                                <td class="dxgv" style="width: 216px; height: 20px; text-align: center">
                                    <asp:Label ID="lblApproval8" runat="server" ForeColor="Black" Text="-"></asp:Label>
                                </td>
                                <td class="dxgv" style="width: 216px; height: 20px; text-align: center">
                                    <asp:Label ID="lbl8a" runat="server" Text="No" ForeColor="Red"></asp:Label>
                                </td>
                                <td class="dxgv" style="width: 480px; height: 20px; text-align: center">
                                    <asp:Label ID="lbl8b" runat="server" ForeColor="Red" Text="No evidence or comment supplied"
                                        Visible="False"></asp:Label>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
    <br />
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td style="width: 100%; height: 13px; text-align: center">
                <table border="0" cellpadding="0" cellspacing="0" class="dxgvControl_Office2003_Blue"
                    style="width: 100%; border-collapse: collapse; empty-cells: show">
                    <tbody>
                        <tr>
                            <td class="dxgvHeader_Office2003_Blue" colspan="2" style="border-top-width: 0px;
                                font-weight: bold; border-left-width: 0px; white-space: normal; height: 13px;
                                text-align: left;">
                                <span style="color: maroon">What does the approved column mean?</span>
                            </td>
                        </tr>
                        <tr class="dxgvDataRow_Office2003_Blue">
                            <td class="dxgv" style="width: 30px; height: 20px; text-align: center">
                                <span style="color: #008000">Yes</span>
                            </td>
                            <td class="dxgv" style="width: 972px; height: 20px; text-align: left">
                                All questions in this section have been approved by an H&amp;S Assessor.
                            </td>
                        </tr>
                        <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                            <td class="dxgv" style="width: 30px; height: 20px; text-align: center">
                                <span style="color: #ff0000">No</span>
                            </td>
                            <td class="dxgv" style="width: 972px; height: 20px; text-align: left">
                                One or more questions in this section have been not approved by an H&amp;S Assessor
                                and are required to be edited/changed per any approval comments specified.
                            </td>
                        </tr>
                        <tr class="dxgvDataRow_Office2003_Blue">
                            <td class="dxgv" style="width: 30px; height: 20px; text-align: center">
                                -
                            </td>
                            <td class="dxgv" style="width: 972px; height: 20px; text-align: left">
                                This section is currently being assessed or if this questionnaire is already classified
                                as [Assessment Complete], indicates that assessment was not conducted as the assessment
                                process was only recently introduced.
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </table>
    <br />
</asp:Panel>
