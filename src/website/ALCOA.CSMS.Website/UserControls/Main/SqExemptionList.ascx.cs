﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxEditors;

namespace ALCOA.CSMS.Website.UserControls.Main
{
    public partial class SqExemptionList : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Helper.ExportGrid.Settings(ExportButtons1, "Current", "gvCurrent");
            Helper.ExportGrid.Settings(ExportButtons2, "Expired", "gvExpired");

        }

        protected void gvCurrent_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
        {
            if (e.RowType != GridViewRowType.Data) return;
            ASPxHyperLink hlView = gvCurrent.FindRowCellTemplateControl(e.VisibleIndex, null, "hlView") as ASPxHyperLink;
            if (hlView != null)
            {
                hlView.Text = (string)gvCurrent.GetRowValues(e.VisibleIndex, "CompanyName");

                hlView.NavigateUrl = String.Format("javascript:popUp('../PopUps/EditSqExemption.aspx{0}');",
                                            QueryStringModule.Encrypt(String.Format("q={0}&c={1}",
                                                        (int)gvCurrent.GetRowValues(e.VisibleIndex, "SqExemptionId"),
                                                        (int)gvCurrent.GetRowValues(e.VisibleIndex, "CompanyId"))));
            }
        }

        protected void gvExpired_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
        {
            if (e.RowType != GridViewRowType.Data) return;
            ASPxHyperLink hlView = gvExpired.FindRowCellTemplateControl(e.VisibleIndex, null, "hlView") as ASPxHyperLink;
            if (hlView != null)
            {
                hlView.Text = (string)gvExpired.GetRowValues(e.VisibleIndex, "CompanyName");

                hlView.NavigateUrl = String.Format("javascript:popUp('../PopUps/EditSqExemption.aspx{0}');",
                                            QueryStringModule.Encrypt(String.Format("q={0}&c={1}",
                                                        (int)gvExpired.GetRowValues(e.VisibleIndex, "SqExemptionId"),
                                                        (int)gvExpired.GetRowValues(e.VisibleIndex, "CompanyId"))));
            }
        }
    }
}