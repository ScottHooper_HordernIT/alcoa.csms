﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface ISystemLogService
    {
        SystemLog Get(Expression<Func<SystemLog, bool>> where, List<Expression<Func<SystemLog, object>>> includes);
        List<SystemLog> GetMany(int? top, Expression<Func<SystemLog, bool>> where, Func<IQueryable<SystemLog>, IOrderedQueryable<SystemLog>> orderBy, List<Expression<Func<SystemLog, object>>> includes);
        List<SystemLog> GetManyPaged(int pageNumber, int pageSize, Expression<Func<SystemLog, bool>> where, Func<IQueryable<SystemLog>, IOrderedQueryable<SystemLog>> orderBy, List<Expression<Func<SystemLog, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<SystemLog, bool>> where, List<Expression<Func<SystemLog, object>>> includes);
        void Insert(SystemLog item, bool saveChanges = true);
        void Update(SystemLog item, bool saveChanges = true);
        void Delete(SystemLog item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class SystemLogService : ISystemLogService
    {
        private readonly IRepository<SystemLog> _repository;

        public SystemLogService(IRepository<SystemLog> repository)
        {
            this._repository = repository;
        }

        public SystemLog Get(Expression<Func<SystemLog, bool>> where, List<Expression<Func<SystemLog, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<SystemLog> GetMany(int? top, Expression<Func<SystemLog, bool>> where, Func<IQueryable<SystemLog>, IOrderedQueryable<SystemLog>> orderBy, List<Expression<Func<SystemLog, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<SystemLog> GetManyPaged(int pageNumber, int pageSize, Expression<Func<SystemLog, bool>> where, Func<IQueryable<SystemLog>, IOrderedQueryable<SystemLog>> orderBy, List<Expression<Func<SystemLog, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<SystemLog, bool>> where, List<Expression<Func<SystemLog, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(SystemLog item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("SystemLog");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(SystemLog item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("SystemLog");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(SystemLog item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("SystemLog");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
