﻿<%@ Control Language="C#" AutoEventWireup="true"
    Inherits="UserControls_Reports_Recordables" Codebehind="Reports_Recordables.ascx.cs" %>
<%@ Register Src="../Other/ExportButtons.ascx" TagName="ExportButtons" TagPrefix="uc1" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<table border="0" cellpadding="2" cellspacing="0" width="100%">
    <tr>
        <td class="pageName" colspan="7" style="text-align: left; width: 846px;">
            <span class="title">Reports</span><br />
            <span class="date">Incident Performance Report</span><br />
            <img height="1" src="images/grfc_dottedline.gif" width="24" />
        </td>
    </tr>
    <tr>
        <td>
            <table border="0" cellpadding="0" cellspacing="0" width="900px" align="left">
                <tr>
                    <td style="width: 73px; height: 13px; text-align: right">
                        <span style="color: #808080"><strong>Operation:</span>&nbsp;
                    </td>
                    <td style="width: 827px; height: 13px" align="left">
                        <dxe:ASPxComboBox ID="cbRegion" runat="server" AutoPostBack="True" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                            CssPostfix="Office2003Blue" DataSourceID="dsRegionsFilter" OnSelectedIndexChanged="cbRegion_SelectedIndexChanged"
                            TextField="RegionName" ValueField="RegionId" ValueType="System.Int32" Width="200px"
                            SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                            <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                            </LoadingPanelImage>
                            <ButtonStyle Width="13px">
                            </ButtonStyle>
                            <ValidationSettings SetFocusOnError="True" ValidationGroup="Filter">
                                <RequiredField IsRequired="True" />
                            </ValidationSettings>
                        </dxe:ASPxComboBox>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="padding-top: 5px; padding-bottom: 5px;">
                        <span style="color: #808080">Hint</strong>: </span><em><span style="color: #808080">
                            After grouping by a column, you can right click on any row to expand or collapse
                            all rows.</em></span>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <dxwgv:ASPxGridView ID="grid" runat="server" AutoGenerateColumns="False" ClientInstanceName="grid"
                            CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue"
                             Width="100%">
                            <ImagesFilterControl>
                                <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
                                </LoadingPanel>
                            </ImagesFilterControl>
                            <Styles CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css">
                                <Header SortingImageSpacing="5px" ImageSpacing="5px">
                                </Header>
                                <LoadingPanel ImageSpacing="10px">
                                </LoadingPanel>
                            </Styles>
                            <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
                                </LoadingPanelOnStatusBar>
                                <CollapsedButton Width="11px">
                                </CollapsedButton>
                                <ExpandedButton Width="11px">
                                </ExpandedButton>
                                <DetailCollapsedButton Width="11px">
                                </DetailCollapsedButton>
                                <DetailExpandedButton Width="11px">
                                </DetailExpandedButton>
                                <FilterRowButton Width="13px">
                                </FilterRowButton>
                                <LoadingPanel Url="~/App_Themes/Office2003BlueOld/GridView/Loading.gif"><%--Enhancement_023:change by debashis for testing platform upgradation--%>
                                </LoadingPanel>
                            </Images>
                            <Columns>
                                <dxwgv:GridViewDataTextColumn FieldName="CompanyName" VisibleIndex="0" SortIndex="0"
                                    SortOrder="Ascending" Width="220px">
                                </dxwgv:GridViewDataTextColumn>
                                <dxwgv:GridViewDataTextColumn FieldName="SiteName" VisibleIndex="1" SortIndex="1"
                                    SortOrder="Ascending" Width="93px">
                                </dxwgv:GridViewDataTextColumn>
                                <dxwgv:GridViewDataTextColumn FieldName="Year" ReadOnly="True" UnboundType="Integer"
                                    VisibleIndex="2" SortIndex="2" SortOrder="Ascending" Width="60px">
                                </dxwgv:GridViewDataTextColumn>
                                <dxwgv:GridViewDataComboBoxColumn Name="Month" Caption="Month" FieldName="Month"
                                    SortIndex="3" SortOrder="Ascending" UnboundType="String" VisibleIndex="3" Width="90px">
                                    <PropertiesComboBox DataSourceID="dsMonths" TextField="MonthName" ValueField="MonthId"
                                        ValueType="System.Int32">
                                    </PropertiesComboBox>
                                </dxwgv:GridViewDataComboBoxColumn>
                                <dxwgv:GridViewDataTextColumn FieldName="ipFATI" Caption="FA" UnboundType="Integer"
                                    VisibleIndex="4" Width="45px">
                                </dxwgv:GridViewDataTextColumn>
                                <dxwgv:GridViewDataTextColumn FieldName="ipMTI" Caption="MT" UnboundType="Integer"
                                    VisibleIndex="5" Width="45px">
                                </dxwgv:GridViewDataTextColumn>
                                <dxwgv:GridViewDataTextColumn FieldName="ipRDI" Caption="RD" UnboundType="Integer"
                                    VisibleIndex="6" Width="45px">
                                </dxwgv:GridViewDataTextColumn>
                                <dxwgv:GridViewDataTextColumn FieldName="ipLTI" Caption="LWD" UnboundType="Integer"
                                    VisibleIndex="7" Width="45px">
                                </dxwgv:GridViewDataTextColumn>
                                <dxwgv:GridViewDataTextColumn FieldName="ipIFE" Caption="IFE" UnboundType="Integer"
                                    VisibleIndex="8" Width="45px">
                                </dxwgv:GridViewDataTextColumn>
                                <dxwgv:GridViewDataTextColumn FieldName="Total" Caption="Total" UnboundType="Integer"
                                    VisibleIndex="9" Width="60px">
                                </dxwgv:GridViewDataTextColumn>
                            </Columns>
                            <Settings ShowHeaderFilterButton="True" ShowGroupPanel="True" ShowFooter="True" ShowPreview="True"
                                ShowVerticalScrollBar="True" VerticalScrollableHeight="215"></Settings>
                            <TotalSummary>
                                <dxwgv:ASPxSummaryItem SummaryType="Sum" FieldName="ipFATI" DisplayFormat="{0}" ShowInColumn="ipFATI"
                                    ShowInGroupFooterColumn="ipFATI"></dxwgv:ASPxSummaryItem>
                                <dxwgv:ASPxSummaryItem SummaryType="Sum" FieldName="ipLTI" DisplayFormat="{0}" ShowInColumn="ipLTI"
                                    ShowInGroupFooterColumn="ipLTI"></dxwgv:ASPxSummaryItem>
                                <dxwgv:ASPxSummaryItem SummaryType="Sum" FieldName="ipMTI" DisplayFormat="{0}" ShowInColumn="ipMTI"
                                    ShowInGroupFooterColumn="ipMTI"></dxwgv:ASPxSummaryItem>
                                <dxwgv:ASPxSummaryItem SummaryType="Sum" FieldName="ipRDI" DisplayFormat="{0}" ShowInColumn="ipRDI"
                                    ShowInGroupFooterColumn="ipRDI"></dxwgv:ASPxSummaryItem>
                                <dxwgv:ASPxSummaryItem SummaryType="Sum" FieldName="ipIFE" DisplayFormat="{0}" ShowInColumn="ipIFE"
                                    ShowInGroupFooterColumn="ipIFE"></dxwgv:ASPxSummaryItem>
                                <dxwgv:ASPxSummaryItem SummaryType="Sum" FieldName="Total" DisplayFormat="{0}" ShowInColumn="Total"
                                    ShowInGroupFooterColumn="Total"></dxwgv:ASPxSummaryItem>
                            </TotalSummary>
                            <GroupSummary>
                                <dxwgv:ASPxSummaryItem SummaryType="Sum" FieldName="ipFATI" DisplayFormat="{0}" ShowInColumn="ipFATI"
                                    ShowInGroupFooterColumn="ipFATI"></dxwgv:ASPxSummaryItem>
                                <dxwgv:ASPxSummaryItem SummaryType="Sum" FieldName="ipLTI" DisplayFormat="{0}" ShowInColumn="ipLTI"
                                    ShowInGroupFooterColumn="ipLTI"></dxwgv:ASPxSummaryItem>
                                <dxwgv:ASPxSummaryItem SummaryType="Sum" FieldName="ipMTI" DisplayFormat="{0}" ShowInColumn="ipMTI"
                                    ShowInGroupFooterColumn="ipMTI"></dxwgv:ASPxSummaryItem>
                                <dxwgv:ASPxSummaryItem SummaryType="Sum" FieldName="ipRDI" DisplayFormat="{0}" ShowInColumn="ipRDI"
                                    ShowInGroupFooterColumn="ipRDI"></dxwgv:ASPxSummaryItem>
                                <dxwgv:ASPxSummaryItem SummaryType="Sum" FieldName="ipIFE" DisplayFormat="{0}" ShowInColumn="ipIFE"
                                    ShowInGroupFooterColumn="ipIFE"></dxwgv:ASPxSummaryItem>
                                <dxwgv:ASPxSummaryItem SummaryType="Sum" FieldName="Total" DisplayFormat="{0}" ShowInColumn="Total"
                                    ShowInGroupFooterColumn="Total"></dxwgv:ASPxSummaryItem>
                            </GroupSummary>
                            <SettingsPager>
                                <AllButton Visible="True">
                                </AllButton>
                            </SettingsPager>
                            <ClientSideEvents ContextMenu="function(s, e) {
                if (expanded == true)
                {
                    grid.CollapseAll();
                    expanded = false;
                }
                else
                {
	                grid.ExpandAll();
	                expanded = true;
	            }
}" />
                            <StylesEditors>
                                <ProgressBar Height="25px">
                                </ProgressBar>
                            </StylesEditors>
                        </dxwgv:ASPxGridView>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="padding-top: 5px; padding-bottom: 5px;">
                        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                            <tr align="right">
                                <td align="right" class="pageName" style="height: 21px; text-align: right; text-align: -moz-right;
                                    width: 900px;">
                                    <uc1:ExportButtons ID="ucExportButtons" runat="server" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <data:RegionsDataSource ID="dsRegionsFilter" runat="server" Filter="IsVisible = True"
                Sort="Ordinal ASC">
            </data:RegionsDataSource>
            <data:MonthsDataSource ID="dsMonths" runat="server" Sort="MonthId" SelectMethod="GetAll">
            </data:MonthsDataSource>
        </td>
    </tr>
</table>
