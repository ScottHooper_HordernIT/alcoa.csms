﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.CompaniesAuditProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(CompaniesAuditDataSourceDesigner))]
	public class CompaniesAuditDataSource : ProviderDataSource<CompaniesAudit, CompaniesAuditKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompaniesAuditDataSource class.
		/// </summary>
		public CompaniesAuditDataSource() : base(new CompaniesAuditService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the CompaniesAuditDataSourceView used by the CompaniesAuditDataSource.
		/// </summary>
		protected CompaniesAuditDataSourceView CompaniesAuditView
		{
			get { return ( View as CompaniesAuditDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the CompaniesAuditDataSource control invokes to retrieve data.
		/// </summary>
		public CompaniesAuditSelectMethod SelectMethod
		{
			get
			{
				CompaniesAuditSelectMethod selectMethod = CompaniesAuditSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (CompaniesAuditSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the CompaniesAuditDataSourceView class that is to be
		/// used by the CompaniesAuditDataSource.
		/// </summary>
		/// <returns>An instance of the CompaniesAuditDataSourceView class.</returns>
		protected override BaseDataSourceView<CompaniesAudit, CompaniesAuditKey> GetNewDataSourceView()
		{
			return new CompaniesAuditDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the CompaniesAuditDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class CompaniesAuditDataSourceView : ProviderDataSourceView<CompaniesAudit, CompaniesAuditKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompaniesAuditDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the CompaniesAuditDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public CompaniesAuditDataSourceView(CompaniesAuditDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal CompaniesAuditDataSource CompaniesAuditOwner
		{
			get { return Owner as CompaniesAuditDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal CompaniesAuditSelectMethod SelectMethod
		{
			get { return CompaniesAuditOwner.SelectMethod; }
			set { CompaniesAuditOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal CompaniesAuditService CompaniesAuditProvider
		{
			get { return Provider as CompaniesAuditService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<CompaniesAudit> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<CompaniesAudit> results = null;
			CompaniesAudit item;
			count = 0;
			
			System.Int32 _auditId;
			System.Int32 _companyId;

			switch ( SelectMethod )
			{
				case CompaniesAuditSelectMethod.Get:
					CompaniesAuditKey entityKey  = new CompaniesAuditKey();
					entityKey.Load(values);
					item = CompaniesAuditProvider.Get(entityKey);
					results = new TList<CompaniesAudit>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case CompaniesAuditSelectMethod.GetAll:
                    results = CompaniesAuditProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case CompaniesAuditSelectMethod.GetPaged:
					results = CompaniesAuditProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case CompaniesAuditSelectMethod.Find:
					if ( FilterParameters != null )
						results = CompaniesAuditProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = CompaniesAuditProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case CompaniesAuditSelectMethod.GetByAuditId:
					_auditId = ( values["AuditId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["AuditId"], typeof(System.Int32)) : (int)0;
					item = CompaniesAuditProvider.GetByAuditId(_auditId);
					results = new TList<CompaniesAudit>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				case CompaniesAuditSelectMethod.GetByCompanyId:
					_companyId = ( values["CompanyId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["CompanyId"], typeof(System.Int32)) : (int)0;
					results = CompaniesAuditProvider.GetByCompanyId(_companyId, this.StartIndex, this.PageSize, out count);
					break;
				// FK
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == CompaniesAuditSelectMethod.Get || SelectMethod == CompaniesAuditSelectMethod.GetByAuditId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				CompaniesAudit entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					CompaniesAuditProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<CompaniesAudit> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			CompaniesAuditProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region CompaniesAuditDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the CompaniesAuditDataSource class.
	/// </summary>
	public class CompaniesAuditDataSourceDesigner : ProviderDataSourceDesigner<CompaniesAudit, CompaniesAuditKey>
	{
		/// <summary>
		/// Initializes a new instance of the CompaniesAuditDataSourceDesigner class.
		/// </summary>
		public CompaniesAuditDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public CompaniesAuditSelectMethod SelectMethod
		{
			get { return ((CompaniesAuditDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new CompaniesAuditDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region CompaniesAuditDataSourceActionList

	/// <summary>
	/// Supports the CompaniesAuditDataSourceDesigner class.
	/// </summary>
	internal class CompaniesAuditDataSourceActionList : DesignerActionList
	{
		private CompaniesAuditDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the CompaniesAuditDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public CompaniesAuditDataSourceActionList(CompaniesAuditDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public CompaniesAuditSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion CompaniesAuditDataSourceActionList
	
	#endregion CompaniesAuditDataSourceDesigner
	
	#region CompaniesAuditSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the CompaniesAuditDataSource.SelectMethod property.
	/// </summary>
	public enum CompaniesAuditSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByAuditId method.
		/// </summary>
		GetByAuditId,
		/// <summary>
		/// Represents the GetByCompanyId method.
		/// </summary>
		GetByCompanyId
	}
	
	#endregion CompaniesAuditSelectMethod

	#region CompaniesAuditFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CompaniesAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompaniesAuditFilter : SqlFilter<CompaniesAuditColumn>
	{
	}
	
	#endregion CompaniesAuditFilter

	#region CompaniesAuditExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CompaniesAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompaniesAuditExpressionBuilder : SqlExpressionBuilder<CompaniesAuditColumn>
	{
	}
	
	#endregion CompaniesAuditExpressionBuilder	

	#region CompaniesAuditProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;CompaniesAuditChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="CompaniesAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompaniesAuditProperty : ChildEntityProperty<CompaniesAuditChildEntityTypes>
	{
	}
	
	#endregion CompaniesAuditProperty
}

