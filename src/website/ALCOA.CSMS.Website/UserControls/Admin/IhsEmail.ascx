﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="IhsEmail.ascx.cs" Inherits="ALCOA.CSMS.Website.UserControls.Admin.IhsEmail" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.ASPxHtmlEditor.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxHtmlEditor" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.ASPxSpellChecker.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxSpellChecker" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxTabControl" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxClasses" tagprefix="dx" %>

<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>

<p>
    The following submits all contractor hours to IHS. See the &#39;Error Logs&#39; to 
    determine if there were any issues whilst generating the IHS logs.</p>
<p>
    <strong>DataSource:</strong><dx:ASPxRadioButtonList ID="rblDataSource" 
        runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
        CssPostfix="Office2003Blue" AutoPostBack="true" 
        onselectedindexchanged="rblDataSource_SelectedIndexChanged" SelectedIndex="0" 
        SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" 
        ValueType="System.Int32">
        <Items>
            <dx:ListEditItem Text="Last Month Only" Value="0" />
            <dx:ListEditItem Text="Whole Year" Value="1" />
        </Items>
    </dx:ASPxRadioButtonList>
</p>
<p>
    <dx:ASPxPageControl ID="ASPxPageControl1" runat="server" ActiveTabIndex="0" 
        CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
        CssPostfix="Office2003Blue" 
        SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" 
        style="margin-right: 0px">
        <TabPages>
            <dx:TabPage Text="Send E-Mail">
                <ContentCollection>
                    <dx:ContentControl runat="server" SupportsDisabledAttribute="True">
                        <p>
                            <strong>To: </strong>
                            <dx:ASPxTextBox ID="tbTo" runat="server" 
                                CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
                                CssPostfix="Office2003Blue" 
                                SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" Width="500px">
                                <ValidationSettings CausesValidation="True" ValidationGroup="SendEmail">
                                    <RequiredField IsRequired="True" />
                                </ValidationSettings>
                            </dx:ASPxTextBox>
                        </p>
                        <p>
                            <strong>Cc:</strong></p>
                        <dx:ASPxTextBox ID="tbCc" runat="server" 
                            CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
                            CssPostfix="Office2003Blue" 
                            SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" Width="500px">
                            <ValidationSettings CausesValidation="True" ValidationGroup="SendEmail">
                                <RequiredField IsRequired="True" />
                            </ValidationSettings>
                        </dx:ASPxTextBox>
                        <p>
                            <strong>&nbsp;Attachments
                            <dx:ASPxHyperLink ID="hlShowFolder" runat="server" 
                                CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
                                CssPostfix="Office2003Blue" Text="(Show in Folder)" Target="_blank">
                            </dx:ASPxHyperLink>
                            :</strong></p>
                        <p>
                            <dx:ASPxLabel ID="lblAttachments" runat="server" EncodeHtml="False" 
                                style="color: #FF0000" Text="Error: No Files Found">
                            </dx:ASPxLabel>
                        </p>
                        <p>
                            <strong>Body:</strong></p>
                        <dx:ASPxHtmlEditor ID="heBody" runat="server" 
                            CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
                            CssPostfix="Office2003Blue" Width="689px">
                            <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
                                CssPostfix="Office2003Blue">
                                <ViewArea>
                                    <Border BorderColor="#002D96" />
                                </ViewArea>
                            </Styles>
                            <Settings AllowHtmlView="False" AllowPreview="False" />
                            <SettingsImageUpload>
                                <ValidationSettings AllowedFileExtensions=".jpe, .jpeg, .jpg, .gif, .png">
                                </ValidationSettings>
                            </SettingsImageUpload>
                            <SettingsImageSelector>
                                <CommonSettings AllowedFileExtensions=".jpe, .jpeg, .jpg, .gif, .png" />
                            </SettingsImageSelector>
                            <SettingsDocumentSelector>
                                <CommonSettings AllowedFileExtensions=".rtf, .pdf, .doc, .docx, .odt, .txt, .xls, .xlsx, .ods, .ppt, .pptx, .odp" />
                            </SettingsDocumentSelector>
                            <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                <LoadingPanel Url="~/App_Themes/Office2003Blue/HtmlEditor/Loading.gif">
                                </LoadingPanel>
                            </Images>
                            <ImagesFileManager>
                                <FolderContainerNodeLoadingPanel Url="~/App_Themes/Office2003Blue/Web/tvNodeLoading.gif">
                                </FolderContainerNodeLoadingPanel>
                                <LoadingPanel Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                                </LoadingPanel>
                            </ImagesFileManager>
                        </dx:ASPxHtmlEditor>
                        <p>
                            &nbsp;</p>
                        <dx:ASPxButton ID="btnSendEmail" runat="server" 
                            CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
                            CssPostfix="Office2003Blue" OnClick="btnSendEmail_Click" 
                            SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" 
                            Text="Send E-Mail" ValidationGroup="SendEmail" Width="700px">
                        </dx:ASPxButton>
                    </dx:ContentControl>
                </ContentCollection>
            </dx:TabPage>
            <dx:TabPage Text="Error Logs">
                <ContentCollection>
                    <dx:ContentControl runat="server" SupportsDisabledAttribute="True">
                        <dx:ASPxLabel ID="lblCreatedAt" runat="server" Text="Last Modified At (Script Ran at): ">
                        </dx:ASPxLabel>
                        <br />
                        <dx:ASPxMemo ID="ASPxMemo1" runat="server" Height="500px" Width="900px">
                        </dx:ASPxMemo>
                    </dx:ContentControl>
                </ContentCollection>
            </dx:TabPage>
        </TabPages>
        <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
        </LoadingPanelImage>
        <LoadingPanelStyle ImageSpacing="6px">
        </LoadingPanelStyle>
        <ContentStyle>
            <Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px" />
        </ContentStyle>
    </dx:ASPxPageControl>
</p>

