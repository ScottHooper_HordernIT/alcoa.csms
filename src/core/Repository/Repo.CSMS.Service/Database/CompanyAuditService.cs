﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface ICompanyAuditService
    {
        CompaniesAudit Get(Expression<Func<CompaniesAudit, bool>> where, List<Expression<Func<CompaniesAudit, object>>> includes);
        List<CompaniesAudit> GetMany(int? top, Expression<Func<CompaniesAudit, bool>> where, Func<IQueryable<CompaniesAudit>, IOrderedQueryable<CompaniesAudit>> orderBy, List<Expression<Func<CompaniesAudit, object>>> includes);
        List<CompaniesAudit> GetManyPaged(int pageNumber, int pageSize, Expression<Func<CompaniesAudit, bool>> where, Func<IQueryable<CompaniesAudit>, IOrderedQueryable<CompaniesAudit>> orderBy, List<Expression<Func<CompaniesAudit, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<CompaniesAudit, bool>> where, List<Expression<Func<CompaniesAudit, object>>> includes);
        void Insert(CompaniesAudit item, bool saveChanges = true);
        void Update(CompaniesAudit item, bool saveChanges = true);
        void Delete(CompaniesAudit item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class CompanyAuditService : ICompanyAuditService
    {
        private readonly IRepository<CompaniesAudit> _repository;

        public CompanyAuditService(IRepository<CompaniesAudit> repository)
        {
            this._repository = repository;
        }

        public CompaniesAudit Get(Expression<Func<CompaniesAudit, bool>> where, List<Expression<Func<CompaniesAudit, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<CompaniesAudit> GetMany(int? top, Expression<Func<CompaniesAudit, bool>> where, Func<IQueryable<CompaniesAudit>, IOrderedQueryable<CompaniesAudit>> orderBy, List<Expression<Func<CompaniesAudit, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<CompaniesAudit> GetManyPaged(int pageNumber, int pageSize, Expression<Func<CompaniesAudit, bool>> where, Func<IQueryable<CompaniesAudit>, IOrderedQueryable<CompaniesAudit>> orderBy, List<Expression<Func<CompaniesAudit, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<CompaniesAudit, bool>> where, List<Expression<Func<CompaniesAudit, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(CompaniesAudit item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("CompaniesAudit");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(CompaniesAudit item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("CompaniesAudit");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(CompaniesAudit item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("CompaniesAudit");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
