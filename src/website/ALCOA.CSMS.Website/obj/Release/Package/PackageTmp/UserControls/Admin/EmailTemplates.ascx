﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EmailTemplates.ascx.cs" Inherits="ALCOA.CSMS.Website.UserControls.Admin.EmailTemplates" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxTabControl" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxClasses" tagprefix="dx" %>
<%@ Register src="AdminTaskEmailTemplates.ascx" tagname="AdminTaskEmailTemplates" tagprefix="uc1" %>
<%@ Register src="BatchJobEmailTemplates.ascx" tagname="BatchJobEmailTemplates" tagprefix="uc2" %>
<%@ Register src="OtherEmailTemplates.ascx" tagname="OtherEmailTemplates" tagprefix="uc3" %>

<dx:ASPxPageControl ID="ASPxPageControl1" runat="server" ActiveTabIndex="2" 
    CssFilePath="~/App_Themes/Office2010BlueNew/{0}/styles.css" 
    CssPostfix="Office2010Blue" 
    SpriteCssFilePath="~/App_Themes/Office2010BlueNew/{0}/sprite.css" TabSpacing="0px" 
    Width="900px">
    <TabPages>
        <dx:TabPage Text="Admin Task Emails">
            <ContentCollection>
                <dx:ContentControl runat="server" SupportsDisabledAttribute="True">
                    <uc1:AdminTaskEmailTemplates ID="AdminTaskEmailTemplates1" runat="server" />
                </dx:ContentControl>
            </ContentCollection>
        </dx:TabPage>
        <dx:TabPage Text="Batch Job Emails">
            <ContentCollection>
                <dx:ContentControl runat="server" SupportsDisabledAttribute="True">
                    <uc2:BatchJobEmailTemplates ID="BatchJobEmailTemplates1" runat="server" />
                </dx:ContentControl>
            </ContentCollection>
        </dx:TabPage>
        <dx:TabPage Text="Other Emails">
            <ContentCollection>
                <dx:ContentControl ID="ContentControl1" runat="server" SupportsDisabledAttribute="True">
                    <uc3:OtherEmailTemplates ID="OtherEmailTemplates1" runat="server" />
                </dx:ContentControl>
            </ContentCollection>
        </dx:TabPage>

    </TabPages>
    <LoadingPanelImage Url="~/App_Themes/Office2010BlueNew/Web/Loading.gif">
    </LoadingPanelImage>
    <LoadingPanelStyle ImageSpacing="5px">
    </LoadingPanelStyle>
    <Paddings Padding="2px" PaddingLeft="5px" PaddingRight="5px" />
    <ContentStyle>
        <Paddings Padding="12px" />
        <Border BorderColor="#859EBF" BorderStyle="Solid" BorderWidth="1px" />
    </ContentStyle>
</dx:ASPxPageControl>

