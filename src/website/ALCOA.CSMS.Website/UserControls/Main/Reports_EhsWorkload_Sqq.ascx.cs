using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Text;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using KaiZen.CSMS.Web;
using KaiZen.CSMS.Services;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxTabControl;
using DevExpress.Web.Data;
using DevExpress.Web.ASPxGridView;
using System.Drawing;
using DevExpress.XtraPrinting;
using System.Data.SqlClient;

using DevExpress.Web.ASPxCallbackPanel;
using DevExpress.Web.ASPxClasses;
using DevExpress.XtraCharts.Native;
using DevExpress.XtraPrintingLinks;
using DevExpress.Web.ASPxPivotGrid;
using System.IO;
using DevExpress.XtraCharts;


public partial class UserControls_Reports_EhsWorkload_Sqq : System.Web.UI.UserControl
{
    Auth auth = new Auth();
    const string fileName = "ALCOA CSMS - Work Levelling Tool - Safety Qualification Questionnaires - Days Since Activity";

    protected void Page_Load(object sender, EventArgs e) { Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack, Page.IsCallback); }

    private void moduleInit(bool postBack, bool callBack)
    {
        if (!postBack)
        {

            SessionHandler.spVar_Year = "%";
            //LoadData();

            switch (auth.RoleId)
            {
                case ((int)RoleList.Reader):
                    //full access
                    break;
                case ((int)RoleList.Contractor):
                    //full access
                    break;
                case ((int)RoleList.Administrator):
                    //full access
                    break;
                default:
                    // do something
                    Response.Redirect("AccessDenied.aspx?error=" + Server.UrlEncode("Unrecognised User."));
                    break;
            }

            //Control ucSqMetrics = LoadControl("~/UserControls/Main/SqMetrics.ascx");
            //phSqMetrics.Controls.Add(ucSqMetrics);
            cbYear1.Items.Add((Convert.ToInt32(DateTime.Now.Year) - 2).ToString(), Convert.ToInt32(DateTime.Now.Year - 2));
            cbYear1.Items.Add((Convert.ToInt32(DateTime.Now.Year) - 1).ToString(), Convert.ToInt32(DateTime.Now.Year - 1));
            cbYear1.Items.Add(DateTime.Now.Year.ToString(), (int)DateTime.Now.Year);
            cbYear1.Value = DateTime.Now.Year;
            cbYear2.Items.Add((Convert.ToInt32(DateTime.Now.Year) - 2).ToString(), Convert.ToInt32(DateTime.Now.Year - 2));
            cbYear2.Items.Add((Convert.ToInt32(DateTime.Now.Year) - 1).ToString(), Convert.ToInt32(DateTime.Now.Year - 1));
            cbYear2.Items.Add(DateTime.Now.Year.ToString(), (int)DateTime.Now.Year);
            cbYear2.Value = DateTime.Now.Year;
            LoadData1((int)cbYear1.Value);
            LoadData2((int)cbYear2.Value);

            try
            {
                if (Request.QueryString["Area"] != null && Request.QueryString["Id"] != null)
                {
                    string Area = Request.QueryString["Area"].ToString();
                    string ID = Request.QueryString["ID"].ToString();

                    LoadLayout(Convert.ToInt32(ID));
                    ASPxPageControl1.ActiveTabIndex = 2;

                    
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }

            gridPivot.Prefilter.CriteriaString = "[CompanyStatusDesc] not like 'InActive%'";
            gridsqm.FilterExpression = "[CompanyStatusName] not like 'InActive%'";
            gridsqm2.FilterExpression = "[CompanyStatusName] not like 'InActive%'";
        }
        else
        {
            gridPivot.DataBind();
        }
        
        Helper.ExportGrid.Settings(ucExportButtons, "CSMS - SQ Metrics - Summary", "gridsqm");
        Helper.ExportGrid.Settings(ucExportButtons2, "CSMS - SQ Metrics - Detailed", "gridsqm2");
        BindData1();
        BindData2();
    }


    protected void imgBtnWord_Click(object sender, ImageClickEventArgs e) //export to word document format 
    {
        using (MemoryStream stream = new MemoryStream())
        {
            gridPivotExporter.ExportToRtf(stream);
            byte[] buffer = stream.GetBuffer();
            Response.Clear();
            Response.Buffer = false;
            Response.AppendHeader("Content-Type", "application/ms-excel");
            Response.AppendHeader("Content-Transfer-Encoding", "binary");
            Response.AppendHeader("Content-Disposition", String.Format("attachment; filename={0}.doc", fileName));
            Response.BinaryWrite(buffer);
            Response.End();
        }
    }
    protected void imgBtnPdf_Click(object sender, ImageClickEventArgs e) //export to adobe acrobat pdf format 
    {
        PrintingSystem ps = new PrintingSystem();

        using (CompositeLink compositeLink = new CompositeLink())
        {
            using (PrintableComponentLink link1 = new PrintableComponentLink())
            {
                link1.Component = gridPivotExporter;
                link1.Landscape = true;
                link1.PrintingSystem = ps;
                using (PrintableComponentLink link2 = new PrintableComponentLink())
                {
                    WebChartControl1.DataBind();
                    link2.Component = ((IChartContainer)WebChartControl1).Chart;
                    link2.Landscape = true;
                    link2.PrintingSystem = ps;

                    compositeLink.Links.AddRange(new object[] { link1, link2 });


                    compositeLink.Landscape = true;
                    compositeLink.PrintingSystem = ps;
                    compositeLink.PageHeaderFooter = new PageHeaderFooter(
                        new PageHeaderArea(new string[] { 
                            String.Format("Exported by: {0}, {1}", auth.LastName, auth.FirstName), 
                            String.Format("at: {0}", DateTime.Now), "[Page # of Pages #]" }, 
                            SystemFonts.DefaultFont, BrickAlignment.Center), 
                        new PageFooterArea(new string[] { 
                            String.Format("Exported by: {0}, {1}", auth.LastName, auth.FirstName), 
                            String.Format("at: {0}", DateTime.Now), "[Page # of Pages #]" }, 
                            SystemFonts.DefaultFont, BrickAlignment.Center));
                    compositeLink.CreateDocument();
                    compositeLink.Landscape = true;
                    compositeLink.PrintingSystem.ExportOptions.Pdf.DocumentOptions.Author = "ALCOA CSMS";

                    using (MemoryStream stream = new MemoryStream())
                    {
                        compositeLink.PrintingSystem.ExportToPdf(stream);
                        Response.Clear();
                        Response.Buffer = false;
                        Response.AppendHeader("Content-Type", "application/pdf");
                        Response.AppendHeader("Content-Transfer-Encoding", "binary");
                        Response.AppendHeader("Content-Disposition", String.Format("attachment; filename={0}.pdf", fileName));
                        Response.BinaryWrite(stream.GetBuffer());
                        Response.End();
                    }
                }
            }
        }
        ps.Dispose();
    }
    protected void imgBtnExcel_Click(object sender, ImageClickEventArgs e) //export to excel spreadsheet format 
    {
        using (MemoryStream stream = new MemoryStream())
        {
            gridPivotExporter.ExportToXls(stream);
            byte[] buffer = stream.GetBuffer();
            Response.Clear();
            Response.Buffer = false;
            Response.AppendHeader("Content-Type", "application/ms-excel");
            Response.AppendHeader("Content-Transfer-Encoding", "binary");
            Response.AppendHeader("Content-Disposition", String.Format("attachment; filename={0}.xls", fileName));
            Response.BinaryWrite(buffer);
            Response.End();
        }
    }

    protected void OnHyperLinkDataBinding(object sender, EventArgs e)
    {
        ASPxHyperLink link = (ASPxHyperLink)sender;
        link.NavigateUrl = "~/SafetyPQ_QuestionnaireModify.aspx" + QueryStringModule.Encrypt("q=" + ((PivotGridFieldValueTemplateContainer)link.Parent).Text);
        link.Text = "Link";
    }



    //report manager

    protected void LoadLayout(int ReportId)
    {
        CustomReportingLayoutsService crlService = new CustomReportingLayoutsService();
        TList<CustomReportingLayouts> crlList = crlService.GetAll();
        TList<CustomReportingLayouts> crlList2 = crlList.FindAll(
            delegate(CustomReportingLayouts c2)
            {
                return
                    c2.ReportId == ReportId;
            }
        );
        string ReportName = "";
        if (crlList2.Count != 0)
        {
            foreach (CustomReportingLayouts crl in crlList2)
            {
                gridPivot.LoadLayoutFromString(crl.ReportLayout);
                lblLayout.Text = ReportName;
            }
        }
        else
        {
            lblLayout.Text = "(Default) | Selected Layout could not be loaded.";
        }
    }

    protected void grid2_RowUpdating(object sender, ASPxDataUpdatingEventArgs e)
    {
        e.NewValues["Area"] = "WLT_SQQ_DSA";
        e.NewValues["ModifiedByUserId"] = auth.UserId;
        e.NewValues["ModifiedDate"] = DateTime.Now;
        e.NewValues["ReportLayout"] = gridPivot.SaveLayoutToString();
    }
    protected void grid2_RowInserting(object sender, ASPxDataInsertingEventArgs e)
    {
        e.NewValues["Area"] = "WLT_SQQ_DSA";
        e.NewValues["ModifiedByUserId"] = auth.UserId;
        e.NewValues["ModifiedDate"] = DateTime.Now;
        e.NewValues["ReportLayout"] = gridPivot.SaveLayoutToString();
    }
    protected void grid2_InitNewRow(object sender, ASPxDataInitNewRowEventArgs e) //Default Values 
    {
        e.NewValues["Area"] = "WLT_SQQ_DSA";
        e.NewValues["ModifiedByUserId"] = auth.UserId;
        e.NewValues["ModifiedDate"] = DateTime.Now;
        e.NewValues["ReportLayout"] = gridPivot.SaveLayoutToString();
        //}
    }
    protected void grid2_RowValidating(object sender, ASPxDataValidationEventArgs e)
    {
        //REQUIRED FIELDS (EVERYTHING ELSE IS DEFAULT TO 0)
        if (e.NewValues["ReportName"] == null) { e.Errors[ASPxGridView1.Columns["ReportName"]] = "Required Field."; }

        //int temp = 0;

        if (e.Errors.Count > 0) e.RowError = "Report Name can not be empty.";
    }
    protected void grid2_StartRowEditing(object sender, ASPxStartRowEditingEventArgs e)
    {
        if (!ASPxGridView1.IsNewRowEditing)
        {
            ASPxGridView1.DoRowValidation();
        }
    }
    protected void grid2_RowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {
        if (e.RowType != GridViewRowType.Data) return;

        int ReportId = (int)ASPxGridView1.GetRowValues(e.VisibleIndex, "ReportId");
        ASPxHyperLink hl = ASPxGridView1.FindRowCellTemplateControl(e.VisibleIndex, null, "hlLoad") as ASPxHyperLink;
        if (hl != null)
        {
            hl.NavigateUrl = String.Format("~/Reports_EhsWorkload_Sqq.aspx{0}", QueryStringModule.Encrypt(String.Format("Area=DSA&ID={0}", ReportId)));
        }
    }

    protected void LoadData1(int Year)
    {
        QuestionnairePresentlyWithMetricService qpwmService = new QuestionnairePresentlyWithMetricService();
        DataSet ds = qpwmService.GetYear2(Year);
        Session["dtSqMetric"] = ds;

        int i = 0;
        DateTime dtFirst = new DateTime(Year, 1, 1);
        while (i < 52)
        {
            i++;
            string _i = i.ToString();
            if (i < 10) _i = "0" + _i;
            if(dtFirst.Year > Year) dtFirst = new DateTime(Year, 12, 31);
            if (gridsqm.Columns[String.Format("{0}", _i)] != null)
                gridsqm.Columns[String.Format("{0}", _i)].Caption = dtFirst.ToString("dd-MMM");
            dtFirst = dtFirst.AddDays(7);
        }
    }
    protected void LoadData2(int Year)
    {
        QuestionnairePresentlyWithMetricService qpwmService = new QuestionnairePresentlyWithMetricService();
        DataSet ds2 = qpwmService.GetYear(Year);
        Session["dtSqMetric2"] = ds2;

        int i = 0;
        DateTime dtFirst = new DateTime(Year, 1, 1);
        while (i < 52)
        {
            i++;
            string _i = i.ToString();
            if (i < 10) _i = "0" + _i;
            if (dtFirst.Year > Year) dtFirst = new DateTime(Year, 12, 31);
            if (gridsqm2.Columns[String.Format("{0}", _i)] != null)
                gridsqm2.Columns[String.Format("{0}", _i)].Caption = dtFirst.ToString("dd-MMM");
            dtFirst = dtFirst.AddDays(7);
        }
    }

    protected void BindData1()
    {
        gridsqm.DataSource = ((DataSet)(Session["dtSqMetric"])).Tables[0];
        gridsqm.DataBind();
    }
    protected void BindData2()
    {
        gridsqm2.DataSource = ((DataSet)(Session["dtSqMetric2"])).Tables[0];
        gridsqm2.DataBind();
    }

    protected void btnFilter_Click(object sender, EventArgs e)
    {
        LoadData1((int)cbYear1.SelectedItem.Value);
        BindData1();
        //grid.ExpandAll();
    }
    protected void btnFilter2_Click(object sender, EventArgs e)
    {
        LoadData2((int)cbYear2.SelectedItem.Value);
        BindData2();
        //grid2.ExpandAll();
    }
    Hashtable hstFields = new Hashtable();
    int ChartWidth = 200;
    protected void gridPivot_OnHtmlFieldValuePrepared(object sender, PivotHtmlFieldValuePreparedEventArgs e)
    {
        if (e.Field != null)
        {
            WebChartControl1.Visible = true;
            if (e.Field.Visible == true && e.Field.Area == DevExpress.XtraPivotGrid.PivotArea.RowArea)
            {
                if (!hstFields.Contains(e.Field.FieldName))
                {
                    ChartWidth = ChartWidth + 150;
                    hstFields.Add(e.Field.FieldName, e.Field.Caption);
                }
            }
        }
        else
        {
            WebChartControl1.Visible = false;
        }
            WebChartControl1.Width = Unit.Pixel((int)ChartWidth);
            WebChartControl1.RefreshData();
        
    }
}
