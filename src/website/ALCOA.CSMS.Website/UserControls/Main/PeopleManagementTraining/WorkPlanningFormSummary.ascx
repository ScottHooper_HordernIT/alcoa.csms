﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WorkPlanningFormSummary.ascx.cs" Inherits="ALCOA.CSMS.Website.UserControls.Main.PeopleManagementTraining.WorkPlanningFormSummary" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Src="~/UserControls/Other/ExportButtons.ascx" TagName="ExportButtons" TagPrefix="uc1" %>

<table border="0" cellpadding="2" cellspacing="0" width="100%">
    <tr>
        
             <td class="pageName" colspan="3" style="height: 16px; width: 1170px;">
        <span class="bodycopy"><span class="title">People Management - Training</span><br />
                <span class="date">Work Planning Form Summary</span><br />
                <img src="images/grfc_dottedline.gif" width="24" height="1"><br />
            </span></td>
    </tr>

    <tr>
        <td style="width:100%; text-align:left; vertical-align:top">
            <div style="font-weight:bold; float:left; padding:5px 0px 0px 0px">Date From:</div>
            <div style="float:left; padding:0px 10px 0px 3px">
            <dx:ASPxDateEdit runat="server" AllowNull="False" Width="95px" OnInit="deFrom_Init" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" ClientInstanceName="deFrom" ID="deFrom">
            
<ButtonStyle Width="13px"></ButtonStyle>
</dx:ASPxDateEdit>                
            </div>

            <div style="font-weight:bold; float:left; padding:5px 0px 0px 10px">Date To:</div>
            <div style="float:left; padding:0px 0px 0px 3px;">
            <dx:ASPxDateEdit runat="server" AllowNull="False" OnInit="deTo_Init" Width="95px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" ClientInstanceName="deTo" ID="deTo">

            <ButtonStyle Width="13px"></ButtonStyle>
            </dx:ASPxDateEdit>               
            </div>

            <div style="font-weight:bold; float:left;  padding:5px 0px 0px 10px">Company:</div>
            <div style="float:left;  padding:0px 0px 0px 3px">
               <dx:ASPxComboBox ID="cmbCompanies" ReadOnly="false"
                       ClientInstanceName="cmbCompanies" runat="server"
                        CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" IncrementalFilteringMode="StartsWith"
                        CssPostfix="Office2003Blue" EnableSynchronization="False" 
                         SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                          OnSelectedIndexChanged="cmbCompanies_SelectedIndexChanged"
                        ValueType="System.Int32">
                        <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                        </LoadingPanelImage>
                        <ButtonStyle Width="13px">
                        </ButtonStyle>
                        <ClientSideEvents SelectedIndexChanged="function(s, e) {
	                                                                    cmbSites.PerformCallback(s.GetValue());
                                                                    }" />
                    </dx:ASPxComboBox>
            </div>

            <div style="font-weight:bold; float:left; padding:5px 0px 0px 10px">Sites/Regions:</div>
            <div style="float:left; padding:0px 0px 0px 3px">
                <dx:ASPxComboBox ID="cmbSites"  ClientInstanceName="cmbSites"
                       runat="server" EnableSynchronization="False" OnCallback="cmbSites_Callback"
                       CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" IncrementalFilteringMode="StartsWith"
                        CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                        ValueType="System.Int32">
                        <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                        </LoadingPanelImage>
                        <ButtonStyle Width="13px">
                        </ButtonStyle>
                         <ValidationSettings Display="Dynamic" ValidationGroup="ShowData"  ErrorDisplayMode="ImageWithTooltip" ErrorText="Please select a Company!" ErrorImage-AlternateText="Please select a Site!">
                                                        <RequiredField IsRequired="True" ErrorText="Please select a Site!" />
                         </ValidationSettings>
                    </dx:ASPxComboBox>
            </div>
            
        </td>
    </tr>
    <tr>
        <td style="width:100%; text-align:right; vertical-align:top">
            
            <div style="float:right;  padding:6px 40px 0px 5px">
               <asp:Button ID="btnPrint" runat="server" OnClientClick="window:print();" Text="Print" />
            </div>
            <div  style="float:right;  padding:5px 0px 0px 10px">
             <dx:ASPxButton ID="btnGoFilter" runat="server" ValidationGroup="ShowData" OnClick="btnGoFilter_click" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                        CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                        Text="Go / Refresh">
                        
                    </dx:ASPxButton>
            
            </div>
        </td>
    </tr>

    <tr>
        <td style="width:100%">
        <div id="divHeader" runat="server" style="display:none">
            <table style="width:100%; text-align:left; padding:3px; vertical-align:top">
                <tr>
                    <td style="width:50%; text-align:right; padding:3px; vertical-align:top">
                    <b>Company Name</b>
                    </td>
                    <td style="width:50%; text-align:left; padding:3px; vertical-align:top">  
                        <dx:ASPxLabel ID="lblCompanyName" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                 SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">                        
            </dx:ASPxLabel>                  
                    </td>
                </tr>

                <tr>
                    <td style="width:50%; text-align:right; padding:3px; vertical-align:top">
                    <b>Company Type</b>
                    </td>
                    <td style="width:50%; text-align:left; padding:3px; vertical-align:top">
                        <dx:ASPxLabel ID="lblCompanyType" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                 SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">                        
            </dx:ASPxLabel>
                    </td>
                </tr>
                <tr>
                    <td style="width:50%; text-align:right; padding:3px; vertical-align:top">
                    <b>Supervision Requirements</b>
                    </td>
                    <td style="width:50%; text-align:left; padding:3px; vertical-align:top">
                        <dx:ASPxLabel ID="lblSupReq" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                 SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">                        
            </dx:ASPxLabel>
                    </td>
                </tr>
                <tr>
                    <td style="width:50%; text-align:right; padding:3px; vertical-align:top">
                    <b>Safety Qualification Valid To</b>
                    </td>
                    <td style="width:50%; text-align:left; padding:3px; vertical-align:top">
                        <dx:ASPxLabel ID="lblSafetyQValid" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                 SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">                        
            </dx:ASPxLabel>
                    </td>
                </tr>
                <tr>
                    <td style="width:50%; text-align:right; padding:3px; vertical-align:top">
                    <b>Approved to Engage Sub Contractors</b>
                    </td>
                    <td style="width:50%; text-align:left; padding:3px; vertical-align:top">
                        <dx:ASPxLabel ID="lblApprovedEng" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                 SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">                        
            </dx:ASPxLabel>
                    </td>
                </tr>


            </table>
            </div>
        </td>
    </tr>
    <tr>
        <td style="width:100%; text-align:left; vertical-align:top">
             <dxwgv:ASPxGridView Id="grid" Width="100%"
                ClientInstanceName="grid" Visible="false"
                runat="server" OnHtmlRowCreated="grid_RowCreated"
                AutoGenerateColumns="False"                  
                CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                CssPostfix="Office2003Blue">
                <Settings ShowFilterRow="true"  ShowFilterBar="Visible" ShowHorizontalScrollBar="true" />
               <SettingsBehavior ColumnResizeMode="Control" />
                <SettingsPager PageSize="10" AllButton-Visible="true"></SettingsPager>
                <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue">
                    <Header ImageSpacing="5px" SortingImageSpacing="5px">
                    </Header>
                    <AlternatingRow Enabled="True">
                    </AlternatingRow>
                    <LoadingPanel ImageSpacing="10px">
                    </LoadingPanel>
                </Styles>
              
                    <Columns>
                   
                        <dxwgv:GridViewDataComboBoxColumn ReadOnly="true" VisibleIndex="1" Caption="Company Name" FieldName="CompanyId" Name="CompanyId" SortIndex="2" SortOrder="Ascending" FixedStyle="Left">
                        <PropertiesComboBox DataSourceID="CompaniesDataSource3" DropDownHeight="150px" TextField="CompanyName"
                            ValueField="CompanyId" ValueType="System.Int32" IncrementalFilteringMode="StartsWith">
                        </PropertiesComboBox>
                        <HeaderStyle Wrap="True" />                       
                        <EditFormSettings Visible="True"/>
                        <Settings SortMode="DisplayText"/>
                      
                    </dxwgv:GridViewDataComboBoxColumn>

                    <dxwgv:GridViewDataComboBoxColumn ReadOnly="true" VisibleIndex="2" Caption="Site Name" FieldName="SiteId" Name="SiteId" FixedStyle="Left">
                        <PropertiesComboBox DataSourceID="SqlDataSite1" DropDownHeight="150px" TextField="SiteName"
                            ValueField="SiteId" ValueType="System.Int32" IncrementalFilteringMode="StartsWith">
                        </PropertiesComboBox>
                        <HeaderStyle Wrap="True" />
                        <EditFormSettings Visible="True"/>
                        <Settings SortMode="DisplayText"/>
                    </dxwgv:GridViewDataComboBoxColumn>

                      
                     <dxwgv:GridViewDataTextColumn FieldName="FULL_NAME"  Caption="Contractor Full Name" FixedStyle="Left"
                                            SortIndex="0" SortOrder="Ascending" VisibleIndex="3">
                                            <Settings AutoFilterCondition="Contains" />
                                            <EditFormSettings Visible="True"  />
                                            <HeaderStyle Wrap="True" />
                     </dxwgv:GridViewDataTextColumn>

                    <%--  
                    <dx:GridViewBandColumn Caption="Training Competency Valid To" Name="BandHeader" VisibleIndex="3">
                    <HeaderStyle HorizontalAlign="Center" />
                    <Columns>

                    <dxwgv:GridViewDataTextColumn FieldName="OneInduction" Name="OneInduction" Caption="One Induction" VisibleIndex="1">
                    <CellStyle HorizontalAlign="Center"></CellStyle>
                    <DataItemTemplate>                          
                     <asp:Image ID="imgOneInduction" runat="server" ImageUrl="~/Images/spacer.gif" Visible="false" GenerateEmptyAlternateText="True" />
                    </DataItemTemplate>

                    </dxwgv:GridViewDataTextColumn>

                     <dxwgv:GridViewDataTextColumn FieldName="ARPCRP" Name="ARPCRP" Caption="ARP / CRP" VisibleIndex="2">
                    <CellStyle HorizontalAlign="Center"></CellStyle>
                    <DataItemTemplate>                          
                     <asp:Image ID="imgArpCrp" runat="server" ImageUrl="~/Images/spacer.gif" Visible="false" GenerateEmptyAlternateText="True" />
                    </DataItemTemplate>

                    </dxwgv:GridViewDataTextColumn>

                    <dxwgv:GridViewDataTextColumn FieldName="AreaInduction" Name="AreaInduction" Caption="Area Induction" VisibleIndex="3">
                    <CellStyle HorizontalAlign="Center"></CellStyle>
                    <DataItemTemplate>                          
                     <asp:Image ID="imgAreaInduction" runat="server" ImageUrl="~/Images/spacer.gif" Visible="false" GenerateEmptyAlternateText="True" />
                    </DataItemTemplate>

                    </dxwgv:GridViewDataTextColumn>

                    <dxwgv:GridViewDataTextColumn FieldName="CSE" Name="CSE" Caption="CSE" VisibleIndex="4">
                    <CellStyle HorizontalAlign="Center"></CellStyle>
                    <DataItemTemplate>                          
                     <asp:Image ID="imgCSE" runat="server" ImageUrl="~/Images/spacer.gif" Visible="false" GenerateEmptyAlternateText="True" />
                    </DataItemTemplate>

                    </dxwgv:GridViewDataTextColumn>

                    <dxwgv:GridViewDataTextColumn FieldName="LOTOV" Name="LOTOV" Caption="LOTOV" VisibleIndex="5">
                    <CellStyle HorizontalAlign="Center"></CellStyle>
                    <DataItemTemplate>                          
                     <asp:Image ID="imgLOTOV" runat="server" ImageUrl="~/Images/spacer.gif" Visible="false" GenerateEmptyAlternateText="True" />
                    </DataItemTemplate>

                    </dxwgv:GridViewDataTextColumn>    
                    
                     
                    <dxwgv:GridViewDataTextColumn FieldName="Fall" Name="Fall" Caption="Fall" VisibleIndex="6">
                    <CellStyle HorizontalAlign="Center"></CellStyle>
                    <DataItemTemplate>                          
                     <asp:Image ID="imgFall" runat="server" ImageUrl="~/Images/spacer.gif" Visible="false" GenerateEmptyAlternateText="True" />
                    </DataItemTemplate>

                    </dxwgv:GridViewDataTextColumn>
                    
                    <dxwgv:GridViewDataTextColumn FieldName="ME" Name="ME" Caption="ME" VisibleIndex="7">
                    <CellStyle HorizontalAlign="Center"></CellStyle>
                    <DataItemTemplate>                          
                     <asp:Image ID="imgME" runat="server" ImageUrl="~/Images/spacer.gif" Visible="false" GenerateEmptyAlternateText="True" />
                    </DataItemTemplate>

                    </dxwgv:GridViewDataTextColumn>

                    <dxwgv:GridViewDataTextColumn FieldName="MG" Name="MG" Caption="MG" VisibleIndex="8">
                    <CellStyle HorizontalAlign="Center"></CellStyle>
                    <DataItemTemplate>                          
                     <asp:Image ID="imgMG" runat="server" ImageUrl="~/Images/spacer.gif" Visible="false" GenerateEmptyAlternateText="True" />
                    </DataItemTemplate>

                    </dxwgv:GridViewDataTextColumn>
                    
                      <dxwgv:GridViewDataTextColumn FieldName="Resp" Name="Resp" Caption="Resp." VisibleIndex="9">
                    <CellStyle HorizontalAlign="Center"></CellStyle>
                    <DataItemTemplate>                          
                     <asp:Image ID="imgResp" runat="server" ImageUrl="~/Images/spacer.gif" Visible="false" GenerateEmptyAlternateText="True" />
                    </DataItemTemplate>

                    </dxwgv:GridViewDataTextColumn>               
                     </Columns>
                                          
                    </dx:GridViewBandColumn>
                     --%>  
                     

                    </Columns>


                  </dxwgv:ASPxGridView>
                   <table width="900px">
                                                        <tr align="right">
                                                            <td colspan="3" style="padding-top: 6px; text-align: right; text-align: -moz-right;
                                                                width: 100%" align="right">
                                                                <div align="right">
                                                                    <uc1:ExportButtons ID="ucExportButtons2" Visible="false" runat="server" />
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
        </td>
    </tr>
    <tr>
    <td  style="width:100%; text-align:left; vertical-align:top">
    <br />
        
        <div align="center" runat="server" id="divLegend" style="text-align: center; display:none">
        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
            <tr>
                <td style="width: 100%; height: 13px; text-align: center">
                    <table border="0" cellpadding="0" cellspacing="0" class="dxgvControl_Office2003_Blue"
                        style="width: 100%; border-collapse: collapse; border:1px solid #4f93e3; empty-cells: show">
                        <tbody>
                            <tr>
                                <td class="dxgvHeader_Office2003_Blue" colspan="2" style="
                                    font-weight: bold; white-space: normal; height: 13px;
                                    text-align: left; background:url(gvGradient.gif) #94b6e8 repeat-x center top; padding:4px;
                                    border:1px solid #4f93e3; border-top-width: 0px; border-left-width: 0px; height:13px;
                                    color:black; empty-cells:show; font-family:verdana, arial, sans-serif;
                                    ">
                                    <span style="color: maroon">Legend</span>
                                </td>
                            </tr>
                            <tr class="dxgvDataRow_Office2003_Blue">
                                <td class="dxgv" style="width: 80px; height:20px; text-align:center; border-bottom:#bfd3ee 1px solid; 
                                     border-collapse:collapse; border-left:#bfd3ee 1px solid; border-right:#bfd3ee 1px solid; border-top:0px; 
                                     color:black; empty-cells:show; font-family:verdana, arial, sans-serif;
                                      padding:2px 6px 6px 5px;">
                                     27/10/2012
                                </td>
                                <td class="dxgv" style="width: 952px; height:20px; text-align: left; border-bottom:#bfd3ee 1px solid; 
                                     border-collapse:collapse; border-left:0px; border-right:1px solid #bfd3ee; border-top:0px; 
                                     color:black; empty-cells:show; font-family:verdana, arial, sans-serif;
                                      padding:2px 6px 6px 5px; ">
                                    Where a date is shown then this indicates the date upon which the specified Training Competency is valid to.
                                </td>
                            </tr>
                            <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue" style="background-color:#EDF5FF">
                                <td class="dxgv" style="width: 80px; height:20px; text-align:center; border-bottom:#bfd3ee 1px solid; 
                                     border-collapse:collapse; border-left:#bfd3ee 1px solid; border-right:#bfd3ee 1px solid; border-top:0px; 
                                     color:black; empty-cells:show; font-family:verdana, arial, sans-serif;
                                      padding:2px 6px 2px 5px;">
                                    <img style="text-align:center; height:17px;" id="imgCross" src="" runat="server" alt="Cross"/>
                                </td>
                                <td class="dxgv" style="width: 952px; height:20px; text-align: left; border-bottom:#bfd3ee 1px solid; 
                                     border-collapse:collapse; border-left:0px; border-right:#bfd3ee 1px solid; border-top:0px; 
                                     color:black; empty-cells:show; font-family:verdana, arial, sans-serif;
                                      padding:2px 6px 2px 5px;">
                                   Where a red cross is displayed, there exists no valid training competency.
                                </td>
                            </tr>
                            
                        </tbody>
                    </table>
                </td>
            </tr>
        </table>
    </div>

        
    </td>
    </tr>

</table>
<br />
<data:CompaniesDataSource ID="CompaniesDataSource3" runat="server" Sort="CompanyName ASC" Filter="CompanyId <> 0"></data:CompaniesDataSource>


<asp:SqlDataSource ID="SqlDataSourcEWorkPlanningSummary" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="WorkPlanningFormSummary_Select3" SelectCommandType="StoredProcedure">
    <SelectParameters>
        <asp:Parameter Name="DateFrom" Type="DateTime" />
        <asp:Parameter Name="DateTo" Type="DateTime" />
        <asp:Parameter Name="CompanyId" Type="Int32" />
        <asp:Parameter Name="SiteId" Type="Int32" />
    </SelectParameters>
</asp:SqlDataSource>

<data:SitesDataSource ID="SitesDataSource1" runat="server" Sort="SiteName ASC"></data:SitesDataSource>

<asp:SqlDataSource ID="SqlDataSite1" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="select SiteId,SiteName from Sites where SiteId=2"
     SelectCommandType="Text">    
</asp:SqlDataSource>

<asp:SqlDataSource ID="sqldaCometency" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="select distinct Package_ColumnName from TrainingCompetency" SelectCommandType="Text">
    
</asp:SqlDataSource>