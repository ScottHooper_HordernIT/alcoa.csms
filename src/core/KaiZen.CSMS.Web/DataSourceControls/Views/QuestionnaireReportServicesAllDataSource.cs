﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.QuestionnaireReportServicesAllProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[CLSCompliant(true)]
	[Designer(typeof(QuestionnaireReportServicesAllDataSourceDesigner))]
	public class QuestionnaireReportServicesAllDataSource : ReadOnlyDataSource<QuestionnaireReportServicesAll>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportServicesAllDataSource class.
		/// </summary>
		public QuestionnaireReportServicesAllDataSource() : base(new QuestionnaireReportServicesAllService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the QuestionnaireReportServicesAllDataSourceView used by the QuestionnaireReportServicesAllDataSource.
		/// </summary>
		protected QuestionnaireReportServicesAllDataSourceView QuestionnaireReportServicesAllView
		{
			get { return ( View as QuestionnaireReportServicesAllDataSourceView ); }
		}
		
		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the QuestionnaireReportServicesAllDataSourceView class that is to be
		/// used by the QuestionnaireReportServicesAllDataSource.
		/// </summary>
		/// <returns>An instance of the QuestionnaireReportServicesAllDataSourceView class.</returns>
		protected override BaseDataSourceView<QuestionnaireReportServicesAll, Object> GetNewDataSourceView()
		{
			return new QuestionnaireReportServicesAllDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the QuestionnaireReportServicesAllDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class QuestionnaireReportServicesAllDataSourceView : ReadOnlyDataSourceView<QuestionnaireReportServicesAll>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportServicesAllDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the QuestionnaireReportServicesAllDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public QuestionnaireReportServicesAllDataSourceView(QuestionnaireReportServicesAllDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal QuestionnaireReportServicesAllDataSource QuestionnaireReportServicesAllOwner
		{
			get { return Owner as QuestionnaireReportServicesAllDataSource; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal QuestionnaireReportServicesAllService QuestionnaireReportServicesAllProvider
		{
			get { return Provider as QuestionnaireReportServicesAllService; }
		}

		#endregion Properties
		
		#region Methods
		
		#endregion Methods
	}

	#region QuestionnaireReportServicesAllDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the QuestionnaireReportServicesAllDataSource class.
	/// </summary>
	public class QuestionnaireReportServicesAllDataSourceDesigner : ReadOnlyDataSourceDesigner<QuestionnaireReportServicesAll>
	{
	}

	#endregion QuestionnaireReportServicesAllDataSourceDesigner

	#region QuestionnaireReportServicesAllFilter

	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireReportServicesAll"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireReportServicesAllFilter : SqlFilter<QuestionnaireReportServicesAllColumn>
	{
	}

	#endregion QuestionnaireReportServicesAllFilter

	#region QuestionnaireReportServicesAllExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireReportServicesAll"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireReportServicesAllExpressionBuilder : SqlExpressionBuilder<QuestionnaireReportServicesAllColumn>
	{
	}
	
	#endregion QuestionnaireReportServicesAllExpressionBuilder		
}

