﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface IQuestionnaireMainAttachmentAuditService
    {
        QuestionnaireMainAttachmentAudit Get(Expression<Func<QuestionnaireMainAttachmentAudit, bool>> where, List<Expression<Func<QuestionnaireMainAttachmentAudit, object>>> includes);
        List<QuestionnaireMainAttachmentAudit> GetMany(int? top, Expression<Func<QuestionnaireMainAttachmentAudit, bool>> where, Func<IQueryable<QuestionnaireMainAttachmentAudit>, IOrderedQueryable<QuestionnaireMainAttachmentAudit>> orderBy, List<Expression<Func<QuestionnaireMainAttachmentAudit, object>>> includes);
        List<QuestionnaireMainAttachmentAudit> GetManyPaged(int pageNumber, int pageSize, Expression<Func<QuestionnaireMainAttachmentAudit, bool>> where, Func<IQueryable<QuestionnaireMainAttachmentAudit>, IOrderedQueryable<QuestionnaireMainAttachmentAudit>> orderBy, List<Expression<Func<QuestionnaireMainAttachmentAudit, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<QuestionnaireMainAttachmentAudit, bool>> where, List<Expression<Func<QuestionnaireMainAttachmentAudit, object>>> includes);
        void Insert(QuestionnaireMainAttachmentAudit item, bool saveChanges = true);
        void Update(QuestionnaireMainAttachmentAudit item, bool saveChanges = true);
        void Delete(QuestionnaireMainAttachmentAudit item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class QuestionnaireMainAttachmentAuditService : IQuestionnaireMainAttachmentAuditService
    {
        private readonly IRepository<QuestionnaireMainAttachmentAudit> _repository;

        public QuestionnaireMainAttachmentAuditService(IRepository<QuestionnaireMainAttachmentAudit> repository)
        {
            this._repository = repository;
        }

        public QuestionnaireMainAttachmentAudit Get(Expression<Func<QuestionnaireMainAttachmentAudit, bool>> where, List<Expression<Func<QuestionnaireMainAttachmentAudit, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<QuestionnaireMainAttachmentAudit> GetMany(int? top, Expression<Func<QuestionnaireMainAttachmentAudit, bool>> where, Func<IQueryable<QuestionnaireMainAttachmentAudit>, IOrderedQueryable<QuestionnaireMainAttachmentAudit>> orderBy, List<Expression<Func<QuestionnaireMainAttachmentAudit, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<QuestionnaireMainAttachmentAudit> GetManyPaged(int pageNumber, int pageSize, Expression<Func<QuestionnaireMainAttachmentAudit, bool>> where, Func<IQueryable<QuestionnaireMainAttachmentAudit>, IOrderedQueryable<QuestionnaireMainAttachmentAudit>> orderBy, List<Expression<Func<QuestionnaireMainAttachmentAudit, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<QuestionnaireMainAttachmentAudit, bool>> where, List<Expression<Func<QuestionnaireMainAttachmentAudit, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(QuestionnaireMainAttachmentAudit item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("QuestionnaireMainAttachmentAudit");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(QuestionnaireMainAttachmentAudit item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("QuestionnaireMainAttachmentAudit");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(QuestionnaireMainAttachmentAudit item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("QuestionnaireMainAttachmentAudit");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
