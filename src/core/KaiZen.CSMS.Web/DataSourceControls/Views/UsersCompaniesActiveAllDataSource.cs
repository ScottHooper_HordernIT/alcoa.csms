﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.UsersCompaniesActiveAllProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[CLSCompliant(true)]
	[Designer(typeof(UsersCompaniesActiveAllDataSourceDesigner))]
	public class UsersCompaniesActiveAllDataSource : ReadOnlyDataSource<UsersCompaniesActiveAll>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UsersCompaniesActiveAllDataSource class.
		/// </summary>
		public UsersCompaniesActiveAllDataSource() : base(new UsersCompaniesActiveAllService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the UsersCompaniesActiveAllDataSourceView used by the UsersCompaniesActiveAllDataSource.
		/// </summary>
		protected UsersCompaniesActiveAllDataSourceView UsersCompaniesActiveAllView
		{
			get { return ( View as UsersCompaniesActiveAllDataSourceView ); }
		}
		
		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the UsersCompaniesActiveAllDataSourceView class that is to be
		/// used by the UsersCompaniesActiveAllDataSource.
		/// </summary>
		/// <returns>An instance of the UsersCompaniesActiveAllDataSourceView class.</returns>
		protected override BaseDataSourceView<UsersCompaniesActiveAll, Object> GetNewDataSourceView()
		{
			return new UsersCompaniesActiveAllDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the UsersCompaniesActiveAllDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class UsersCompaniesActiveAllDataSourceView : ReadOnlyDataSourceView<UsersCompaniesActiveAll>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UsersCompaniesActiveAllDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the UsersCompaniesActiveAllDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public UsersCompaniesActiveAllDataSourceView(UsersCompaniesActiveAllDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal UsersCompaniesActiveAllDataSource UsersCompaniesActiveAllOwner
		{
			get { return Owner as UsersCompaniesActiveAllDataSource; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal UsersCompaniesActiveAllService UsersCompaniesActiveAllProvider
		{
			get { return Provider as UsersCompaniesActiveAllService; }
		}

		#endregion Properties
		
		#region Methods
		
		#endregion Methods
	}

	#region UsersCompaniesActiveAllDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the UsersCompaniesActiveAllDataSource class.
	/// </summary>
	public class UsersCompaniesActiveAllDataSourceDesigner : ReadOnlyDataSourceDesigner<UsersCompaniesActiveAll>
	{
	}

	#endregion UsersCompaniesActiveAllDataSourceDesigner

	#region UsersCompaniesActiveAllFilter

	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="UsersCompaniesActiveAll"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UsersCompaniesActiveAllFilter : SqlFilter<UsersCompaniesActiveAllColumn>
	{
	}

	#endregion UsersCompaniesActiveAllFilter

	#region UsersCompaniesActiveAllExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="UsersCompaniesActiveAll"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UsersCompaniesActiveAllExpressionBuilder : SqlExpressionBuilder<UsersCompaniesActiveAllColumn>
	{
	}
	
	#endregion UsersCompaniesActiveAllExpressionBuilder		
}

