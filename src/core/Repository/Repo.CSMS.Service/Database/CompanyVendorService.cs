﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface ICompanyVendorService
    {
        CompanyVendor Get(Expression<Func<CompanyVendor, bool>> where, List<Expression<Func<CompanyVendor, object>>> includes);
        List<CompanyVendor> GetMany(int? top, Expression<Func<CompanyVendor, bool>> where, Func<IQueryable<CompanyVendor>, IOrderedQueryable<CompanyVendor>> orderBy, List<Expression<Func<CompanyVendor, object>>> includes);
        List<CompanyVendor> GetManyPaged(int pageNumber, int pageSize, Expression<Func<CompanyVendor, bool>> where, Func<IQueryable<CompanyVendor>, IOrderedQueryable<CompanyVendor>> orderBy, List<Expression<Func<CompanyVendor, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<CompanyVendor, bool>> where, List<Expression<Func<CompanyVendor, object>>> includes);
        void Insert(CompanyVendor item, bool saveChanges = true);
        void Update(CompanyVendor item, bool saveChanges = true);
        void Delete(CompanyVendor item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class CompanyVendorService : ICompanyVendorService
    {
        private readonly IRepository<CompanyVendor> _repository;

        public CompanyVendorService(IRepository<CompanyVendor> repository)
        {
            this._repository = repository;
        }

        public CompanyVendor Get(Expression<Func<CompanyVendor, bool>> where, List<Expression<Func<CompanyVendor, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<CompanyVendor> GetMany(int? top, Expression<Func<CompanyVendor, bool>> where, Func<IQueryable<CompanyVendor>, IOrderedQueryable<CompanyVendor>> orderBy, List<Expression<Func<CompanyVendor, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<CompanyVendor> GetManyPaged(int pageNumber, int pageSize, Expression<Func<CompanyVendor, bool>> where, Func<IQueryable<CompanyVendor>, IOrderedQueryable<CompanyVendor>> orderBy, List<Expression<Func<CompanyVendor, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<CompanyVendor, bool>> where, List<Expression<Func<CompanyVendor, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(CompanyVendor item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("CompanyVendor");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(CompanyVendor item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("CompanyVendor");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(CompanyVendor item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("CompanyVendor");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
