﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/user.master" AutoEventWireup="true" Inherits="SafetyPQ_QuestionnaireInitial" Codebehind="SafetyPQ_QuestionnaireInitial.aspx.cs" %>

<%@ Register Src="UserControls/Main/SqQuestionnaire/Questionnaire1Procurement.ascx"
    TagName="Questionnaire1Procurement" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script language="JavaScript">
        var needToConfirm = false;
        window.onbeforeunload = confirmExit;

     //Is Pre-Qualification Required Questions:
        var sNa = '-';
        var sYesPqRequired = 'Yes, PQ is Required';
        var sNoPqRequired = 'No, PQ is not Required';
        var sYesPqRequiredOverall = 'Yes, Pre-Qualification is Required';
        var sNoPqRequiredOverall = 'No, Pre-Qualification is not Required';
        var colourGood = 'Green';
        var colourBad = 'Red';
        var colourNeutral = 'Black';


     //Low, Medium, High, Forced High Risk Questions
        var sLowRisk = 'Low Risk';
        var sMediumRisk = 'Medium Risk';
        var sHighRisk = 'High Risk';
        var sForcedHighRisk = 'Forced High Risk';



        function confirmExit() {
            if (needToConfirm)
                return "You have attempted to leave this page.  If you have made any changes to the fields without clicking the Save or Submit button, your changes will be lost.  Are you sure you want to exit this page?";
        }
        function confirmSave() {
            needtoConfirm = false;
            e.processOnServer = confirm('Are you sure you wish to save your changes?');
        }

        function SetPqRequired(lbl, PqRequired) {
            if (PqRequired == true) {
                lbl.innerHTML = sYesPqRequired;
                lbl.style.color = colourBad;
            }
            else {
                lbl.innerHTML = sNoPqRequired;
                lbl.style.color = colourGood;
            }
        }
        function SetPqRequiredNa(lbl) {
            lbl.innerHTML = "-";
            lbl.style.colour = colourNeutral;
        }

        function MoreThanOneCategorySelected() {
            var _SelectedCategories = 0;
            var Cat1 = ctl00_body_Questionnaire1Procurement1_Aspxroundpanel11_rbl2_7.GetValue();
            var Cat2 = ctl00_body_Questionnaire1Procurement1_Aspxroundpanel11_rbl2_8.GetValue();
            var Cat3 = ctl00_body_Questionnaire1Procurement1_Aspxroundpanel11_rbl2_9.GetValue();
            var Cat4 = ctl00_body_Questionnaire1Procurement1_Aspxroundpanel11_rbl2_10.GetValue();
            var Cat5 = ctl00_body_Questionnaire1Procurement1_Aspxroundpanel11_rbl2_11.GetValue();
            var Cat6 = ctl00_body_Questionnaire1Procurement1_Aspxroundpanel11_rbl2_12.GetValue();

            if (Cat1 == "Yes") _SelectedCategories++;
            if (Cat2 == "Yes") _SelectedCategories++;
            if (Cat3 == "Yes") _SelectedCategories++;
            if (Cat4 == "Yes") _SelectedCategories++;
            if (Cat5 == "Yes") _SelectedCategories++;
            if (Cat6 == "Yes") _SelectedCategories++;
            if (_SelectedCategories > 1) {
                return true;
            }
            else {
                return false;
            }
        }

        function AllCategoriesNo() {
            var _SelectedCategories = 0;
            var Cat1 = ctl00_body_Questionnaire1Procurement1_Aspxroundpanel11_rbl2_7.GetValue();
            var Cat2 = ctl00_body_Questionnaire1Procurement1_Aspxroundpanel11_rbl2_8.GetValue();
            var Cat3 = ctl00_body_Questionnaire1Procurement1_Aspxroundpanel11_rbl2_9.GetValue();
            var Cat4 = ctl00_body_Questionnaire1Procurement1_Aspxroundpanel11_rbl2_10.GetValue();
            var Cat5 = ctl00_body_Questionnaire1Procurement1_Aspxroundpanel11_rbl2_11.GetValue();
            var Cat6 = ctl00_body_Questionnaire1Procurement1_Aspxroundpanel11_rbl2_12.GetValue();

            if (Cat1 == "No") _SelectedCategories++;
            if (Cat2 == "No") _SelectedCategories++;
            if (Cat3 == "No") _SelectedCategories++;
            if (Cat4 == "No") _SelectedCategories++;
            if (Cat5 == "No") _SelectedCategories++;
            if (Cat6 == "No") _SelectedCategories++;
            if (_SelectedCategories == 6) {
                return true;
            }
            else {
                return false;
            }
        }

        function CalculateIfPqRequired() {
            var PqRequired = new Boolean();
            var lblOverall = document.getElementById('ctl00_body_Questionnaire1Procurement1_Aspxroundpanel11_lbl2_Overall');
            // Changed by Pankaj Dikshit for <Issue ID# 2381, 04-May-12> - Start
            var PqrPqRequired = 0;
            var PqrPqNotRequired = 0;

            var lblCat1 = document.getElementById('ctl00_body_Questionnaire1Procurement1_Aspxroundpanel11_lbl2_7');
            var lblCat2 = document.getElementById('ctl00_body_Questionnaire1Procurement1_Aspxroundpanel11_lbl2_8');
            var lblCat3 = document.getElementById('ctl00_body_Questionnaire1Procurement1_Aspxroundpanel11_lbl2_9');
            var lblCat4 = document.getElementById('ctl00_body_Questionnaire1Procurement1_Aspxroundpanel11_lbl2_10');
            var lblCat5 = document.getElementById('ctl00_body_Questionnaire1Procurement1_Aspxroundpanel11_lbl2_11');
            var lblCat6 = document.getElementById('ctl00_body_Questionnaire1Procurement1_Aspxroundpanel11_lbl2_12');

            var Cat1 = ctl00_body_Questionnaire1Procurement1_Aspxroundpanel11_rbl2_7.GetValue();
            var Cat2 = ctl00_body_Questionnaire1Procurement1_Aspxroundpanel11_rbl2_8.GetValue();
            var Cat3 = ctl00_body_Questionnaire1Procurement1_Aspxroundpanel11_rbl2_9.GetValue();
            var Cat4 = ctl00_body_Questionnaire1Procurement1_Aspxroundpanel11_rbl2_10.GetValue();
            var Cat5 = ctl00_body_Questionnaire1Procurement1_Aspxroundpanel11_rbl2_11.GetValue();
            var Cat6 = ctl00_body_Questionnaire1Procurement1_Aspxroundpanel11_rbl2_12.GetValue();

            var CatPqRequired = 0;
            var CatPqNotRequired = 0;

            //if (Cat1 == "Yes") { SetPqRequired(lblCat1, false); CatPqRequired++; SetPqRequiredNa(lblPqr1); SetPqRequiredNa(lblPqr2); SetPqRequiredNa(lblPqr3); SetPqRequiredNa(lblPqr4); SetPqRequiredNa(lblPqr5); SetPqRequiredNa(lblPqr6); } else { SetPqRequiredNa(lblCat1); }
            //if (Cat2 == "Yes") { SetPqRequired(lblCat2, false); CatPqNotRequired++; SetPqRequiredNa(lblPqr1); SetPqRequiredNa(lblPqr2); SetPqRequiredNa(lblPqr3); SetPqRequiredNa(lblPqr4); SetPqRequiredNa(lblPqr5); SetPqRequiredNa(lblPqr6); } else { SetPqRequiredNa(lblCat2); }
            //if (Cat3 == "Yes") { SetPqRequired(lblCat3, true); CatPqRequired++; SetPqRequiredNa(lblPqr1); SetPqRequiredNa(lblPqr2); SetPqRequiredNa(lblPqr3); SetPqRequiredNa(lblPqr4); SetPqRequiredNa(lblPqr5); SetPqRequiredNa(lblPqr6); } else { SetPqRequiredNa(lblCat3); }
            //if (Cat4 == "Yes") { SetPqRequired(lblCat4, false); CatPqNotRequired++; SetPqRequiredNa(lblPqr1); SetPqRequiredNa(lblPqr2); SetPqRequiredNa(lblPqr3); SetPqRequiredNa(lblPqr4); SetPqRequiredNa(lblPqr5); SetPqRequiredNa(lblPqr6); } else { SetPqRequiredNa(lblCat4); }
            //if (Cat5 == "Yes") { SetPqRequired(lblCat5, true); CatPqRequired++; SetPqRequiredNa(lblPqr1); SetPqRequiredNa(lblPqr2); SetPqRequiredNa(lblPqr3); SetPqRequiredNa(lblPqr4); SetPqRequiredNa(lblPqr5); SetPqRequiredNa(lblPqr6); } else { SetPqRequiredNa(lblCat5); }
            //if (Cat6 == "Yes") { SetPqRequiredNa(lblCat1); SetPqRequiredNa(lblCat2); SetPqRequiredNa(lblCat3); SetPqRequiredNa(lblCat4); SetPqRequiredNa(lblCat5); SetPqRequiredNa(lblCat6); } else { SetPqRequiredNa(lblCat6); }

            if (Cat1 == "Yes") { SetPqRequired(lblCat1, false); CatPqRequired++; SetPqRequiredNa(lblCat2); SetPqRequiredNa(lblCat3); SetPqRequiredNa(lblCat4); SetPqRequiredNa(lblCat5); SetPqRequiredNa(lblCat6); } else { SetPqRequiredNa(lblCat1); }
            if (Cat2 == "Yes") { SetPqRequired(lblCat2, false); CatPqNotRequired++; SetPqRequiredNa(lblCat1); SetPqRequiredNa(lblCat3); SetPqRequiredNa(lblCat4); SetPqRequiredNa(lblCat5); SetPqRequiredNa(lblCat6); } else { SetPqRequiredNa(lblCat2); }
            if (Cat3 == "Yes") { SetPqRequired(lblCat3, true); CatPqRequired++; SetPqRequiredNa(lblCat1); SetPqRequiredNa(lblCat2); SetPqRequiredNa(lblCat4); SetPqRequiredNa(lblCat5); SetPqRequiredNa(lblCat6); } else { SetPqRequiredNa(lblCat3); }
            if (Cat4 == "Yes") { SetPqRequired(lblCat4, false); CatPqNotRequired++; SetPqRequiredNa(lblCat1); SetPqRequiredNa(lblCat2); SetPqRequiredNa(lblCat3); SetPqRequiredNa(lblCat5); SetPqRequiredNa(lblCat6); } else { SetPqRequiredNa(lblCat4); }
            if (Cat5 == "Yes") { SetPqRequired(lblCat5, true); CatPqRequired++; SetPqRequiredNa(lblCat1); SetPqRequiredNa(lblCat2); SetPqRequiredNa(lblCat3); SetPqRequiredNa(lblCat4); SetPqRequiredNa(lblCat6); } else { SetPqRequiredNa(lblCat5); }
            if (Cat6 == "Yes") { SetPqRequired(lblCat6, true); CatPqRequired++; SetPqRequiredNa(lblCat1); SetPqRequiredNa(lblCat2); SetPqRequiredNa(lblCat3); SetPqRequiredNa(lblCat4); SetPqRequiredNa(lblCat5); } else { SetPqRequiredNa(lblCat6); }

            if (lblOverall != null) {
                var lblPqr1 = document.getElementById('ctl00_body_Questionnaire1Procurement1_Aspxroundpanel11_lbl2_1');
                var lblPqr2 = document.getElementById('ctl00_body_Questionnaire1Procurement1_Aspxroundpanel11_lbl2_2');
                var lblPqr3 = document.getElementById('ctl00_body_Questionnaire1Procurement1_Aspxroundpanel11_lbl2_3');
                var lblPqr4 = document.getElementById('ctl00_body_Questionnaire1Procurement1_Aspxroundpanel11_lbl2_4');
                var lblPqr5 = document.getElementById('ctl00_body_Questionnaire1Procurement1_Aspxroundpanel11_lbl2_5');
                var lblPqr6 = document.getElementById('ctl00_body_Questionnaire1Procurement1_Aspxroundpanel11_lbl2_6');
                // Question added - Jolly - Enhancement Spec - 026
                var lblPqr13 = document.getElementById('ctl00_body_Questionnaire1Procurement1_Aspxroundpanel11_lbl2_13');

            
                var Pqr1 = ctl00_body_Questionnaire1Procurement1_Aspxroundpanel11_rbl2_1.GetValue();
                var Pqr2 = ctl00_body_Questionnaire1Procurement1_Aspxroundpanel11_rbl2_2.GetValue();
                var Pqr3 = ctl00_body_Questionnaire1Procurement1_Aspxroundpanel11_rbl2_3.GetValue();
                var Pqr4 = ctl00_body_Questionnaire1Procurement1_Aspxroundpanel11_rbl2_4.GetValue();
                var Pqr5 = ctl00_body_Questionnaire1Procurement1_Aspxroundpanel11_rbl2_5.GetValue();
                var Pqr6 = ctl00_body_Questionnaire1Procurement1_Aspxroundpanel11_rbl2_6.GetValue();
                // Question Added - Jolly - Enhancement Spec - 026
                var Pqr13 = ctl00_body_Questionnaire1Procurement1_Aspxroundpanel11_rbl2_13.GetValue();


                if (Cat6 == "Yes") { CatPqRequired--; SetPqRequiredNa(lblCat1); SetPqRequiredNa(lblCat2); SetPqRequiredNa(lblCat3); SetPqRequiredNa(lblCat4); SetPqRequiredNa(lblCat5); SetPqRequiredNa(lblCat6); } else { SetPqRequiredNa(lblCat6); }

                if (Cat6 == "Yes") {
                    if (Pqr1 == "No") { SetPqRequired(lblPqr1, true); PqrPqRequired++; } else { if (Pqr1 == "Yes") { SetPqRequired(lblPqr1, false); PqrPqNotRequired++; } else { SetPqRequiredNa(lblPqr1); } }
                    if (Pqr2 == "No") { SetPqRequired(lblPqr2, true); PqrPqRequired++; } else { if (Pqr2 == "Yes") { SetPqRequired(lblPqr2, false); PqrPqNotRequired++; } else { SetPqRequiredNa(lblPqr2); } }
                    if (Pqr3 == "Yes") { SetPqRequired(lblPqr3, true); PqrPqRequired++; } else { if (Pqr3 == "No") { SetPqRequired(lblPqr3, false); PqrPqNotRequired++; } else { SetPqRequiredNa(lblPqr3); } }
                    if (Pqr4 == "Yes") { SetPqRequired(lblPqr4, true); PqrPqRequired++; } else { if (Pqr4 == "No") { SetPqRequired(lblPqr4, false); PqrPqNotRequired++; } else { SetPqRequiredNa(lblPqr4); } }
                    if (Pqr5 == "Yes") { SetPqRequired(lblPqr5, true); PqrPqRequired++; } else { if (Pqr5 == "No") { SetPqRequired(lblPqr5, false); PqrPqNotRequired++; } else { SetPqRequiredNa(lblPqr5); } }
                    if (Pqr6 == "No") { SetPqRequired(lblPqr6, true); PqrPqRequired++; } else { if (Pqr6 == "Yes") { SetPqRequired(lblPqr6, false); PqrPqNotRequired++; } else { SetPqRequiredNa(lblPqr6); } }
                    //Jolly - Enhancement Spec - 026
                    if (Pqr13 == "Yes") { SetPqRequired(lblPqr13, true); PqrPqRequired++; } else { if (Pqr13 == "No") { SetPqRequired(lblPqr13, false); PqrPqNotRequired++; } else { SetPqRequiredNa(lblPqr13); } }
                }
                else {
                    if (Pqr1 == "No") { SetPqRequiredNa(lblPqr1); PqrPqRequired++; } else { if (Pqr1 == "Yes") { SetPqRequiredNa(lblPqr1); PqrPqNotRequired++; } else { SetPqRequiredNa(lblPqr1); } }
                    if (Pqr2 == "No") { SetPqRequiredNa(lblPqr2); PqrPqRequired++; } else { if (Pqr2 == "Yes") { SetPqRequiredNa(lblPqr2); PqrPqNotRequired++; } else { SetPqRequiredNa(lblPqr2); } }
                    if (Pqr3 == "Yes") { SetPqRequiredNa(lblPqr3); PqrPqRequired++; } else { if (Pqr3 == "No") { SetPqRequiredNa(lblPqr3); PqrPqNotRequired++; } else { SetPqRequiredNa(lblPqr3); } }
                    if (Pqr4 == "Yes") { SetPqRequiredNa(lblPqr4); PqrPqRequired++; } else { if (Pqr4 == "No") { SetPqRequiredNa(lblPqr4); PqrPqNotRequired++; } else { SetPqRequiredNa(lblPqr4); } }
                    if (Pqr5 == "Yes") { SetPqRequiredNa(lblPqr5); PqrPqRequired++; } else { if (Pqr5 == "No") { SetPqRequiredNa(lblPqr5); PqrPqNotRequired++; } else { SetPqRequiredNa(lblPqr5); } }
                    if (Pqr6 == "No") { SetPqRequiredNa(lblPqr6); PqrPqRequired++; } else { if (Pqr6 == "Yes") { SetPqRequiredNa(lblPqr6); PqrPqNotRequired++; } else { SetPqRequiredNa(lblPqr6); } }
                    //Jolly - Enhancement Spec - 026
                    if (Pqr13 == "Yes") { SetPqRequiredNa(lblPqr13); PqrPqRequired++; } else { if (Pqr13 == "No") { SetPqRequiredNa(lblPqr13); PqrPqNotRequired++; } else { SetPqRequiredNa(lblPqr13); } }
                }

                
            }
            // Changed by Pankaj Dikshit for <Issue ID# 2381, 04-May-12> - End
            
            var PqRequired = new Boolean();
            if (Cat1 == "Yes" || Cat2 == "Yes" || Cat4 == "Yes" ) {
                PqRequired = false;
            }
            else {
                if (CatPqRequired > 0) {
                    PqRequired = true;
                }
                else {
                    if (PqrPqRequired > 0) {
                        PqRequired = true;
                    }
                    else {
                        PqRequired = false;
                    }
                }

            }

            if (lblOverall != null) {
                if (PqRequired == true) {
                    lblOverall.innerHTML = sYesPqRequiredOverall;
                    lblOverall.style.color = colourBad;
                }
                else {
                    if (PqRequired == false) {
                        lblOverall.innerHTML = sNoPqRequiredOverall;
                        lblOverall.style.color = colourGood;
                    }
                    else {
                        lblOverall.innerHTML = "-";
                        lblOverall.style.color = colourNeutral;
                    }
                }
            }
        }

        function setRiskLevel() {
            var lbl1 = document.getElementById('ctl00_body_Questionnaire1Procurement1_Aspxroundpanel10_lbl1');
            var lbl2 = document.getElementById('ctl00_body_Questionnaire1Procurement1_Aspxroundpanel10_lbl2');
            var lbl3 = document.getElementById('ctl00_body_Questionnaire1Procurement1_Aspxroundpanel10_lbl3');
            var lbl4 = document.getElementById('ctl00_body_Questionnaire1Procurement1_Aspxroundpanel10_lbl4');
            var lbl5 = document.getElementById('ctl00_body_Questionnaire1Procurement1_Aspxroundpanel10_lbl5');
            var lbl6 = document.getElementById('ctl00_body_Questionnaire1Procurement1_Aspxroundpanel10_lbl6');
            var lbl7 = document.getElementById('ctl00_body_Questionnaire1Procurement1_Aspxroundpanel10_lbl7');
            var lbl8 = document.getElementById('ctl00_body_Questionnaire1Procurement1_Aspxroundpanel10_lbl8');
            var lbl9 = document.getElementById('ctl00_body_Questionnaire1Procurement1_Aspxroundpanel10_lbl9');
            var lbl10 = document.getElementById('ctl00_body_Questionnaire1Procurement1_Aspxroundpanel10_lbl10');
            var lbl11 = document.getElementById('ctl00_body_Questionnaire1Procurement1_Aspxroundpanel10_lbl11');
            var lblOverall = document.getElementById('ctl00_body_Questionnaire1Procurement1_Aspxroundpanel10_lblOverall');
            lblOverall.innerHTML = sLowRisk;
            lblOverall.style.color = colourGood;

            if (lbl1.innerHTML == sLowRisk) {
                lblOverall.innerHTML = lbl1.innerHTML;
                lblOverall.style.color = lbl1.style.color;
            }
            if (lbl2.innerHTML == sLowRisk) {
                lblOverall.innerHTML = lbl2.innerHTML;
                lblOverall.style.color = lbl2.style.color;
            }
            if (lbl3.innerHTML == sLowRisk) {
                lblOverall.innerHTML = lbl3.innerHTML;
                lblOverall.style.color = lbl3.style.color;
            }
            if (lbl4.innerHTML == sMediumRisk) {
                lblOverall.innerHTML = lbl4.innerHTML;
                lblOverall.style.color = lbl4.style.color;
            }
            if (lbl5.innerHTML == sMediumRisk) {
                lblOverall.innerHTML = lbl5.innerHTML;
                lblOverall.style.color = lbl5.style.color;
            }
            if (lbl6.innerHTML == sHighRisk) {
                lblOverall.innerHTML = lbl6.innerHTML;
                lblOverall.style.color = lbl6.style.color;
            }
            if (lbl7.innerHTML == sHighRisk) {
                lblOverall.innerHTML = lbl7.innerHTML;
                lblOverall.style.color = lbl7.style.color;
            }
            if (lbl8.innerHTML == sForcedHighRisk) {
                lblOverall.innerHTML = lbl8.innerHTML;
                lblOverall.style.color = lbl8.style.color;
            }
            if (lbl9.innerHTML == sForcedHighRisk) {
                lblOverall.innerHTML = lbl9.innerHTML;
                lblOverall.style.color = lbl9.style.color;
            }
            if (lbl10.innerHTML == sForcedHighRisk) {
                lblOverall.innerHTML = lbl10.innerHTML;
                lblOverall.style.color = lbl10.style.color;
            }
            if (lbl11.innerHTML == sForcedHighRisk) {
                lblOverall.innerHTML = lbl11.innerHTML;
                lblOverall.style.color = lbl11.style.color;
            }
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="Server">
    <uc1:Questionnaire1Procurement ID="Questionnaire1Procurement1" runat="server" />
</asp:Content>
