﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ArpCrpHealthPopup.aspx.cs" Inherits="ALCOA.CSMS.Website.ArpCrpHelthPopup" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
    
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxRoundPanel" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxPanel" tagprefix="dx" %>
    <%@ Register assembly="DevExpress.Web.v14.1" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dxpc" %>

<%@ Register assembly="DevExpress.Web.v14.1" namespace="DevExpress.Web.ASPxPopupControl" tagprefix="dx" %>
<%@ Register Assembly="AjaxControls" Namespace="AjaxControls" TagPrefix="cc1" %>
    <%@ Register Src="~/UserControls/Other/LoadingScreen.ascx" TagName="LoadingScreen"
    TagPrefix="uc3" %>


<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .style3
        {
            height: 8px;
        }
        .style4
        {
            width: 284px;
        }
       
    </style>
    <script type="text/javascript" language="javascript">
        function doClose(e) // note: takes the event as an arg (IE doesn't)  
        {
            if (!e) e = window.event; // fix IE  

            if (e.keyCode) // IE  
            {
                if (e.keyCode == "27") window.close();
            }
            else if (e.charCode) // Netscape/Firefox/Opera  
            {
                if (e.keyCode == "27") window.close();
            }
        }
        document.onkeydown = doClose;  
 </script>
</head>
<body>
    
    <form id="form1" runat="server">
    <asp:ScriptManager ID="scriptmanager1" runat="Server" EnablePageMethods="true" EnablePartialRendering="true" AsyncPostBackTimeout="600" LoadScriptsBeforeUI="true">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">

    <ContentTemplate>

    <div>
    
    <table border="0"  cellspacing="0" 
            style="width: 614px;" >
 
     <tr>
     <td style="text-align:right;width: 260px; " align="right"  ></td>
     <td style="text-align:right;width: 100px; " align="right"  >
   
       <dx:ASPxButton ID="ASPxButton1" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
            CssPostfix="Office2003Blue" PostBackUrl="javascript:window.close();" 
            Text="Close (ESC)" 
             SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" 
             Paddings-PaddingLeft="4px">
        </dx:ASPxButton>
        
     <%--<asp:LinkButton ID="btnClose" ForeColor="Black" runat="server" OnClientClick="window.close();" Text="[Exit]" />--%>
     </td>
     <td style="text-align:left; width:300px" align="left" >
     <dx:ASPxButton ID="ASPxButton2" Width="90px" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
            CssPostfix="Office2003Blue"   PostBackUrl="javascript:window.print();" 
            Text="Print" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" 
             style="margin-left: 0px" >
        </dx:ASPxButton>
        
     <%--<asp:LinkButton ID="btnPrint" ForeColor="Black" runat="server" OnClientClick="window.print();" Text="[Print]" />--%>
    </td>
     </tr>
     
    
     </table>
     <table border="0" cellpadding="2" cellspacing="0" 
            style="width: 614px; ">
     <tr><td 
      style="text-align:center; " class="style4" ></td>
      <td  ></td></tr>
       <tr><td colspan="2" style="text-align:left; font-size:12pt; font-weight:bold">CRP - Contractor Responsible Persons (Contractors)</td></tr>
       
       <tr><td colspan="2" style="text-align:center; "></td></tr>
       <tr>

       <td style="text-align: right" >
                <strong>Company:</strong>
       </td>
       <td style="text-align: left">
      
       <dxe:ASPxComboBox ID="ddlCompanies" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                            CssPostfix="Office2003Blue" 
                            ShowLoadingPanel="true"
                            IncrementalFilteringMode="StartsWith"
                            ValueField="CompanyId" AutoPostBack="true"
               TextField="CompanyName" SelectedIndex="0"
                            ValueType="System.Int32" Width="500px" 
               SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" 
               onselectedindexchanged="ddlCompanies_SelectedIndexChanged">
                            <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                            </LoadingPanelImage>
                            <ButtonStyle Width="13px">
                            </ButtonStyle>
       </dxe:ASPxComboBox>

       </td>
       
       
       </tr>

       <tr><td colspan="2" style="text-align:center; "></td></tr>
       <tr>
       <td style="text-align: right" valign="top" >
       <strong>CRP(s):</strong>
       </td>
       <td style="text-align: left" valign="top">
        
            <dxwgv:ASPxGridView
            ID="gridCRP"
            runat="server" AutoGenerateColumns="False"
            CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
            CssPostfix="Office2003Blue"
           
            Width="500px"
            >
            
            <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
                </LoadingPanelOnStatusBar>
                <CollapsedButton Height="12px" Width="11px" />
                <ExpandedButton Height="12px" Width="11px" />
                <DetailCollapsedButton Height="12px" Width="11px" />
                <DetailExpandedButton Height="12px" Width="11px" />
                <LoadingPanel Url="~/App_Themes/Office2003Blue/GridView/Loading.gif">
                </LoadingPanel>
            </Images>
            <ImagesFilterControl>
                <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
                </LoadingPanel>
            </ImagesFilterControl>
            <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
                CssPostfix="Office2003Blue">
                <Header ImageSpacing="5px" SortingImageSpacing="5px" />
                <LoadingPanel ImageSpacing="10px" />
            </Styles>
             <SettingsText  EmptyDataRow="No CRP's were found."/>
            <Columns>
               
                <dxwgv:GridViewDataTextColumn Caption="Name" FieldName="Full_Name" ReadOnly="True" Visible="true" 
                VisibleIndex="0" >
                    
                </dxwgv:GridViewDataTextColumn>
                <dxwgv:GridViewDataTextColumn Caption="Training Valid Until" FieldName="Valid_Until"  VisibleIndex="1" 
                SortIndex="1" SortOrder="Descending">
                 <PropertiesTextEdit DisplayFormatString="dd/MM/yyyy"></PropertiesTextEdit>
                 <CellStyle HorizontalAlign="Center"></CellStyle>
                 <HeaderStyle HorizontalAlign="Center" />
                </dxwgv:GridViewDataTextColumn>
                
            </Columns>
            <Settings ShowHeaderFilterButton="true" />
            <SettingsPager Visible="False" Mode="ShowAllRecords" />
            <SettingsEditing Mode="Inline" />
            <SettingsBehavior ColumnResizeMode="NextColumn" ConfirmDelete="True"></SettingsBehavior>
        </dxwgv:ASPxGridView>
       </td>
       </tr>

      
       <tr><td colspan="2" style="height:30px"></td></tr>
       <tr><td colspan="2" style="text-align:left; font-size:12pt; font-weight:bold">ARP – Alcoa Responsible Persons</td></tr>
       <tr><td colspan="2" style="text-align:center; "></td></tr>
       <tr>
        <td style="text-align: right" >
                        <strong>Site/Region:</strong>
                    </td>
                    <td style="text-align: right; width: 200px" class="style3">
                        <dxe:ASPxComboBox ID="cbRegionSite" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                            CssPostfix="Office2003Blue" IncrementalFilteringMode="StartsWith" SelectedIndex="0"
                            ValueType="System.Int32" Width="200px" AutoPostBack="true" ShowLoadingPanel="true"
                            SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" 
                            onselectedindexchanged="cbRegionSite_SelectedIndexChanged">
                            <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                            </LoadingPanelImage>
                            <ButtonStyle Width="13px">
                            </ButtonStyle>
                        </dxe:ASPxComboBox>
                    </td>
       </tr>

       <tr><td colspan="2" style="text-align:center; "></td></tr>
       <tr>
       <td style="text-align: right" valign="top" >
       <strong> ARP(s): </strong>
       </td>
       <td valign="top">
        
            <dxwgv:ASPxGridView
            ID="gridARP"
            runat="server" AutoGenerateColumns="False"
            CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
            CssPostfix="Office2003Blue"
           
            Width="500px"
            >
            <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
                </LoadingPanelOnStatusBar>
                <CollapsedButton Height="12px" Width="11px" />
                <ExpandedButton Height="12px" Width="11px" />
                <DetailCollapsedButton Height="12px" Width="11px" />
                <DetailExpandedButton Height="12px" Width="11px" />
                <LoadingPanel Url="~/App_Themes/Office2003Blue/GridView/Loading.gif">
                </LoadingPanel>
            </Images>
            <ImagesFilterControl>
                <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
                </LoadingPanel>
            </ImagesFilterControl>
            <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
                CssPostfix="Office2003Blue">
                <Header ImageSpacing="5px" SortingImageSpacing="5px" />
                <LoadingPanel ImageSpacing="10px" />
            </Styles>
             <SettingsText  EmptyDataRow="No ARP's were found."/> 
            <Columns>

               
                <dxwgv:GridViewDataTextColumn Caption="Name" FieldName="Full_Name" ReadOnly="True" Visible="true"
                 VisibleIndex="0" Settings-AllowAutoFilter="False">
                    
                </dxwgv:GridViewDataTextColumn>
                <dxwgv:GridViewDataTextColumn Caption="Training Valid Until" FieldName="Valid_Until" 
                 VisibleIndex="1" Settings-AllowAutoFilter="False" Width="150px" SortOrder="Descending" SortIndex="3">
                 <PropertiesTextEdit DisplayFormatString="dd/MM/yyyy"></PropertiesTextEdit>
                 <CellStyle HorizontalAlign="Center"></CellStyle>
                 <HeaderStyle HorizontalAlign="Center" ></HeaderStyle>
                </dxwgv:GridViewDataTextColumn>
                
                <dxwgv:GridViewDataComboBoxColumn Caption="Site" FieldName="SiteName"  VisibleIndex="2" 
                SortIndex="1" SortOrder="Descending" >
                <PropertiesComboBox DataSourceID="dsSitesFilter" TextField="SiteName" ValueField="SiteName"
                    ValueType="System.String" IncrementalFilteringMode="StartsWith">
                </PropertiesComboBox>

                <HeaderStyle HorizontalAlign="Center" ></HeaderStyle>
                 <CellStyle HorizontalAlign="Left"></CellStyle>
                </dxwgv:GridViewDataComboBoxColumn>



               <dxwgv:GridViewDataTextColumn Caption="Op Centre" FieldName="op_centre"  VisibleIndex="2"
                SortIndex="2" SortOrder="Descending" Visible="false">
                <HeaderStyle HorizontalAlign="Center" ></HeaderStyle>
                 <CellStyle HorizontalAlign="Left"></CellStyle>
                </dxwgv:GridViewDataTextColumn>

                
            </Columns>
            <Settings ShowHeaderFilterButton="true" />
            <SettingsPager Visible="False" Mode="ShowAllRecords" />
            <SettingsEditing Mode="Inline" />
            <Settings ShowFilterBar="Visible" ShowFilterRow="True" ShowGroupPanel="True" />
            <SettingsBehavior ColumnResizeMode="NextColumn" ConfirmDelete="True"></SettingsBehavior>
        </dxwgv:ASPxGridView>
       </td>
       </tr>
    </table>
    </div>
    </ContentTemplate>
    <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="scriptmanager1" EventName="Load" />
                        </Triggers>

    </asp:UpdatePanel>
<asp:sqlDataSource ID="dsCompanies" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="select CompanyId,CompanyName from companies" SelectCommandType="Text" 
   >
   
</asp:sqlDataSource>

<asp:SqlDataSource ID="dsSitesFilter" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="getAllSite" SelectCommandType="StoredProcedure">
</asp:SqlDataSource>

  <dxpc:ASPxPopupControl ID="ASPxPopupControl1" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
    CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
    EnableHotTrack="False" HeaderText="Warning" Height="111px" Modal="True" PopupHorizontalAlign="WindowCenter"
    PopupVerticalAlign="WindowCenter" Width="439px">
    <HeaderStyle>
        <Paddings PaddingRight="6px"></Paddings>
    </HeaderStyle>
    <ContentCollection>
        <dxpc:PopupControlContentControl ID="PopupControlContentControl1" runat="server"
            SupportsDisabledAttribute="True">
            <dxe:ASPxLabel runat="server" ID="ASPxLabel1" Font-Size="14px" Text=""
                Height="30px">
                <Border BorderWidth="10px" BorderColor="White"></Border>
            </dxe:ASPxLabel>
        </dxpc:PopupControlContentControl>
    </ContentCollection>
</dxpc:ASPxPopupControl>
<cc1:ModalUpdateProgress ID="ModalUpdateProgress1" runat="server" BackgroundCssClass="modalProgressGreyBackground"
            DisplayAfter="0">
            <ProgressTemplate>
                <uc3:LoadingScreen ID="LoadingScreen1" runat="server" />
            </ProgressTemplate>
        </cc1:ModalUpdateProgress>

    </form>
</body>
</html>
