﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface IQuestionnaireVerificationAssessmentAuditService
    {
        QuestionnaireVerificationAssessmentAudit Get(Expression<Func<QuestionnaireVerificationAssessmentAudit, bool>> where, List<Expression<Func<QuestionnaireVerificationAssessmentAudit, object>>> includes);
        List<QuestionnaireVerificationAssessmentAudit> GetMany(int? top, Expression<Func<QuestionnaireVerificationAssessmentAudit, bool>> where, Func<IQueryable<QuestionnaireVerificationAssessmentAudit>, IOrderedQueryable<QuestionnaireVerificationAssessmentAudit>> orderBy, List<Expression<Func<QuestionnaireVerificationAssessmentAudit, object>>> includes);
        List<QuestionnaireVerificationAssessmentAudit> GetManyPaged(int pageNumber, int pageSize, Expression<Func<QuestionnaireVerificationAssessmentAudit, bool>> where, Func<IQueryable<QuestionnaireVerificationAssessmentAudit>, IOrderedQueryable<QuestionnaireVerificationAssessmentAudit>> orderBy, List<Expression<Func<QuestionnaireVerificationAssessmentAudit, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<QuestionnaireVerificationAssessmentAudit, bool>> where, List<Expression<Func<QuestionnaireVerificationAssessmentAudit, object>>> includes);
        void Insert(QuestionnaireVerificationAssessmentAudit item, bool saveChanges = true);
        void Update(QuestionnaireVerificationAssessmentAudit item, bool saveChanges = true);
        void Delete(QuestionnaireVerificationAssessmentAudit item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class QuestionnaireVerificationAssessmentAuditService : IQuestionnaireVerificationAssessmentAuditService
    {
        private readonly IRepository<QuestionnaireVerificationAssessmentAudit> _repository;

        public QuestionnaireVerificationAssessmentAuditService(IRepository<QuestionnaireVerificationAssessmentAudit> repository)
        {
            this._repository = repository;
        }

        public QuestionnaireVerificationAssessmentAudit Get(Expression<Func<QuestionnaireVerificationAssessmentAudit, bool>> where, List<Expression<Func<QuestionnaireVerificationAssessmentAudit, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<QuestionnaireVerificationAssessmentAudit> GetMany(int? top, Expression<Func<QuestionnaireVerificationAssessmentAudit, bool>> where, Func<IQueryable<QuestionnaireVerificationAssessmentAudit>, IOrderedQueryable<QuestionnaireVerificationAssessmentAudit>> orderBy, List<Expression<Func<QuestionnaireVerificationAssessmentAudit, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<QuestionnaireVerificationAssessmentAudit> GetManyPaged(int pageNumber, int pageSize, Expression<Func<QuestionnaireVerificationAssessmentAudit, bool>> where, Func<IQueryable<QuestionnaireVerificationAssessmentAudit>, IOrderedQueryable<QuestionnaireVerificationAssessmentAudit>> orderBy, List<Expression<Func<QuestionnaireVerificationAssessmentAudit, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<QuestionnaireVerificationAssessmentAudit, bool>> where, List<Expression<Func<QuestionnaireVerificationAssessmentAudit, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(QuestionnaireVerificationAssessmentAudit item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("QuestionnaireVerificationAssessmentAudit");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(QuestionnaireVerificationAssessmentAudit item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("QuestionnaireVerificationAssessmentAudit");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(QuestionnaireVerificationAssessmentAudit item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("QuestionnaireVerificationAssessmentAudit");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
