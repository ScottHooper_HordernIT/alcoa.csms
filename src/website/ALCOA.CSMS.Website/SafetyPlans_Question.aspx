﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/user.master" AutoEventWireup="true" Inherits="SafetyPlans_Question" Codebehind="SafetyPlans_Question.aspx.cs" %>

<%@ Register src="UserControls/Main/SafetyPlans_Question.ascx" tagname="SafetyPlans_Question" tagprefix="uc1" %>

<asp:Content ID="Content2" ContentPlaceHolderID="body" Runat="Server">
    <uc1:SafetyPlans_Question ID="SafetyPlans_Question1" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="bodybottom" Runat="Server">
    <%: System.Web.Optimization.Scripts.Render("~/bundles/countdown_timer_scripts") %>
</asp:Content>
