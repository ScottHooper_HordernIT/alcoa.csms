using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using KaiZen.CSMS.Services;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Web;
using DevExpress.Web.ASPxPopupControl;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxCallbackPanel;
using DevExpress.Web.ASPxClasses;
using DevExpress.Web.ASPxTabControl;

using System.Drawing;
//http://www.dotnet2themax.com/ShowContent.aspx?ID=00fa0fd6-4970-4ddf-aa6b-335c3d1259be

public partial class UserControls_KPI_Record : System.Web.UI.UserControl
{
    #region Services

    public Repo.CSMS.Service.Database.IStoredProcedureService StoredProcedureService { get; set; }
    public Repo.CSMS.Service.Database.IKpiService KpiService { get; set; }

    #endregion

    int currentMonth, currentYear;
    const int LENGTH_TEXT = 1000;

    Auth auth = new Auth();
    protected void Page_Load(object sender, EventArgs e) { Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); }

    private void moduleInit(bool postBack)
    {
        string lengthFunction = "function isMaxLength(txtBox) {";
        lengthFunction += " if(txtBox) { ";
        lengthFunction += "     return ( txtBox.value.length <=" + LENGTH_TEXT + ");";
        lengthFunction += " }";
        lengthFunction += "}";

        this.tbmtOthers.Attributes.Add("onkeypress", "return isMaxLength(this);");
        Page.ClientScript.RegisterClientScriptBlock(
            this.GetType(),
            "txtLength",
            lengthFunction, true);



        if (!postBack && !Page.IsCallback)
        { //first time load
            lblCode.Visible = false;
            currentMonth = System.DateTime.Today.Month;
            currentYear = System.DateTime.Today.Year;

            //SetAllInputControlsColors(this, SystemColors.Window, SystemColors.WindowText, Color.Yellow, Color.Blue);
            SetAllInputControlsClassName(this, "","ActiveInputControl");


            panelKPI_Record.Visible = false;
            panelKPIinfo.Visible = false;
            //EnableDisable_Controls(false);
            ddlCompanies.Enabled = false;

            //ddlYear.Items.Add(new ListEditItem((Convert.ToInt32(DateTime.Now.Year) - 3).ToString(),(Convert.ToInt32(DateTime.Now.Year) - 3)));
            //ddlYear.Items.Add(new ListEditItem((Convert.ToInt32(DateTime.Now.Year) - 2).ToString(), (Convert.ToInt32(DateTime.Now.Year) - 2)));
            //ddlYear.Items.Add(new ListEditItem((Convert.ToInt32(DateTime.Now.Year) - 1).ToString(), (Convert.ToInt32(DateTime.Now.Year) - 1)));
            //ddlYear.Items.Add(new ListEditItem(DateTime.Now.Year.ToString(), DateTime.Now.Year.ToString()));

            //Changed for ChangeItem#29
            int Current_year = DateTime.Now.Year;
            while (Current_year >= 2007)
            {
                ddlYear.Items.Add(new ListEditItem(Current_year.ToString(), Convert.ToInt32(Current_year)));
                Current_year--;
            } //End
            ddlYear.Value = DateTime.Now.Year;

            SetEnterToTab();

            ddlMonth.Value = currentMonth;
            ddlCompanies.Text = "< Select Company >";

            ddlCompanies.DataSourceID = "sqldsCompaniesList";
            ddlCompanies.TextField = "CompanyName";
            ddlCompanies.ValueField = "CompanyId";
            ddlCompanies.DataBind();

            SessionHandler.spVar_CompanyId = auth.CompanyId.ToString();
            FillSitesCombo(SessionHandler.spVar_CompanyId);
            //trCategory.Visible = false;
            //Load Kpi Projects Combo Box
            //LoadKpiProjectComboBoxs();
            
            switch (auth.RoleId)
            {
                case ((int)RoleList.Reader):
                    ddlCompanies.Enabled = true;
                    DisableAllInputBoxs();
                    btnSearchGo.Text = "View";
                    break;
                case ((int)RoleList.Contractor):
                    ddlCompanies.Value = auth.CompanyId;
                    ddlSites.SelectedIndex = 0;
                    //KPILoad_Init();
                    break;
                case ((int)RoleList.Administrator):
                    ddlCompanies.Enabled = true;
                    
                    break;
                default:
                    // do something
                    Response.Redirect("AccessDenied.aspx?error=" + Server.UrlEncode("Unrecognised User."));
                    break;
            }
            //FillSitesCombo(ddlCompanies2.Value.ToString());
        }
        else
        { //postback

            //CalcManHours();
        }
    }


    //protected void ddlSites_Callback(object source, CallbackEventArgsBase e)
    //{
    //    FillSitesCombo(e.Parameter);
    //    //ddlSites.SelectedIndex = 0;
    //}
    protected void ddlSites_SelectedIndexChanged(object sender, EventArgs e)
    {
        FilllblCategory(); 
    }
    //Added by Vikas For Item#10
    protected void FilllblCategory()
    {
        if (!String.IsNullOrEmpty(GetCategory()))
        {
            lblCategory.Text = GetCategory();
            trCategory.Visible = true;
        }
    }
    protected string GetCategory()
    {
        int errorCount = 0;
        try
        {
            if (String.IsNullOrEmpty(ddlCompanies.SelectedItem.Text)) { errorCount++; };
            if (String.IsNullOrEmpty(ddlSites.SelectedItem.Text)) { errorCount++; };
        }
        catch (Exception ex)
        {
            //ToDo: Handle Exception
            Elmah.ErrorSignal.FromContext(Context).Raise(ex);
            errorCount = 1;
            PopupWindow pcWindow = new PopupWindow("You haven't selected a company and/or site. If no site exists for your company when it should, please contact the AUA Contractor Services Team.");
            pcWindow.FooterText = "";
            pcWindow.ShowOnPageLoad = true;
            pcWindow.Modal = true;
            ASPxPopupControl1.Windows.Add(pcWindow);
           
        }

        if (errorCount > 0)
        {
            PopupWindow pcWindow = new PopupWindow("You haven't selected a company and/or site. If no site exists for your company when it should, please contact the AUA Contractor Services Team.");
            pcWindow.FooterText = "";
            pcWindow.ShowOnPageLoad = true;
            pcWindow.Modal = true;
            ASPxPopupControl1.Windows.Add(pcWindow);
            return null;
        }
       
        else
        {
            CompanySiteCategoryStandardService cscsService1 = new CompanySiteCategoryStandardService();
            CompanySiteCategoryStandard cscStd = cscsService1.GetByCompanyIdSiteId(Convert.ToInt32(ddlCompanies.SelectedItem.Value), Convert.ToInt32(ddlSites.SelectedItem.Value));
            CompanySiteCategoryService cscService = new CompanySiteCategoryService();
            CompanySiteCategory currentCategory = cscService.GetByCompanySiteCategoryId(Convert.ToInt32(cscStd.CompanySiteCategoryId));
            return currentCategory.CategoryDesc;
        }
        
    }
    //End
    // internal
    protected void FillSitesCombo(string companyId)
    {
        if (string.IsNullOrEmpty(companyId)) return;
        SessionHandler.spVar_CompanyId = companyId;

        SitesService sService = new SitesService();
        CompanySiteCategoryStandardService cscsService = new CompanySiteCategoryStandardService();
        CompanySiteCategory category=new CompanySiteCategory();
       
        TList<CompanySiteCategoryStandard> cscsTlist = cscsService.GetByCompanyId(Convert.ToInt32(companyId));

        ddlSites.Items.Clear();
       
        int _i = 0;
        int j = 0;
        if (Convert.ToInt32(companyId) > 0)
        {
            if (cscsTlist != null)
            {
                if (cscsTlist.Count > 0)
                {
                    SitesFilters sitesFilters = new SitesFilters();
                    sitesFilters.Append(SitesColumn.IsVisible, "1");
                    TList<Sites> sitesList = DataRepository.SitesProvider.GetPaged(sitesFilters.ToString(), "SiteName ASC", 0, 100, out _i);
                    foreach (Sites s in sitesList)
                    {
                        foreach (CompanySiteCategoryStandard cscs in cscsTlist)
                        {
                            if (cscs.CompanySiteCategoryId != null)
                            {
                                if (s.SiteId == cscs.SiteId)
                                {
                                    ddlSites.Items.Add(s.SiteName, cscs.SiteId);
                                    if (j == 0)
                                    {
                                        ddlSites.SelectedIndex = 0;
                                    }
                                    j++;
                                }
                            }
                        }
                    }
                    if (j == 0)
                    {
                        ddlSites.SelectedItem = null;
                    }
                    if (ddlCompanies.SelectedIndex != -1)
                    {
                        //CompanySiteCategory sCategory = DataRepository.CompanySiteCategoryProvider.GetByCompanySiteCategoryId(Convert.ToInt32(cscsTlist[0].CompanySiteCategoryId));
                        //lblCategory.Text = "Safety Compliance Category:" + sCategory.CategoryDesc;
                        //trCategory.Visible = true;
                        FilllblCategory();
                    }
                }
                else 
                {
                    ddlSites.SelectedItem = null;
                }

            }
        }

       
            //ddlSites.DataSourceID = "sqldsResidential_Sites_ByCompany";
        //ddlSites.TextField = "SiteName";
        //ddlSites.ValueField = "SiteId";
        //sqldsResidential_Sites_ByCompany.SelectParameters[0].DefaultValue = companyId;
        //sqldsResidential_Sites_ByCompany.DataBind();
        //ddlSites.DataBind();
    }

    void SetInputControlClassName(WebControl ctl, string className, string focusClassName)
    {
        string jsOnFocus = String.Format(
           "this.className = '{0}';", focusClassName);
        string jsOnBlur = String.Format(
           "this.className = '{0}';", className);
        ctl.Attributes.Add("onfocus", jsOnFocus);
        ctl.Attributes.Add("onblur", jsOnBlur);
    }

    void SetAllInputControlsClassName(Control parent,
       string className, string focusClassName)
    {
        foreach (Control ctl in parent.Controls)
        {
            if (ctl is TextBox)
                //if (ctl is TextBox || ctl is ListBox || ctl is DropDownList)
            {
                SetInputControlClassName(
                   ctl as WebControl,
                   className, focusClassName);
            }
            else
            {
                SetAllInputControlsClassName(ctl,
                   className, focusClassName);
            }

        }
    }


    protected void btnSave_Click(object sender, EventArgs e)
    {
        lblErrors.Text = "";
        lblCode.Visible = false;
        if (Page.IsValid)
        {
            bool _inval = false;
            bool _inval2 = false;
            if (!String.IsNullOrEmpty(tbGeneral.Text))
            {
                int Hours = Convert.ToInt32(tbGeneral.Text);
                if (Hours > 0)
                {
                    int People = Convert.ToInt32(tbPeakNoEmp.Text);
                    if (People == 0) _inval = true;
                }
            }

            if (!String.IsNullOrEmpty(tbPeakNoEmp.Text))
            {
                int PeakPeople = Convert.ToInt32(tbPeakNoEmp.Text);
                if (PeakPeople > 0)
                {
                    int AvgPeople = Convert.ToInt32(tbAvgNoEmp.Text);
                    if (AvgPeople == 0) _inval2 = true;
                }
            }

            if (_inval || _inval2)
            {
                if (_inval)
                {
                    lblErrors.Text = "'Peak No. of Employees' can not be 0 when 'General Hours' is greater than 0. Please correct the 'Peak No of Employees' field.";
                    lblErrors.Visible = true;
                }
                else if (_inval2)
                {
                    lblErrors.Text = "'Avg No. of Employees' can not be 0 when 'Peak No. of Employees' is greater than 0. Please correct the 'Avg. No of Employees' field.";
                    lblErrors.Visible = true;
                }
            }
            else
            {
                bool projectsvalid = true;


                if (!Validate())
                {
                    lblErrors.Text = "One or more Projects have not been entered in correctly. If a PO is selected (not blank) please ensure that the Line Number has been selected and the Project Hours entered.";
                    lblErrors.Visible = true;
                }
                else
                {

                    lblErrors.Visible = false;

                    //string jscript;
                    //jscript = string.Format("needToConfirm = false;");
                    //Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "redirect", jscript, true);

                    int SiteId = (int)ddlSites.SelectedItem.Value;
                    int siteIndex = ddlSites.SelectedIndex;
                    int CompanyId = (int)ddlCompanies.SelectedItem.Value;

                    saveKPI();

                    FillSitesCombo(CompanyId.ToString());
                    ddlSites.SelectedIndex = siteIndex;
                    FilllblCategory();
                }
            }
        }
        else
        {
            //lblCode.Visible = true;
            //lblCode.Text = "NOT Saved. Please check input fields for invalid input";
            //lblCode.Focus();
            lblErrors.Visible = true;
            ValidationSummary1.Visible = true;
        }
    }
    protected void btnSearchGo_Click(object sender, EventArgs e)
    {
        int errorCount = 0;
        try
        {
            if (String.IsNullOrEmpty(ddlCompanies.SelectedItem.Text)) { errorCount++; };
            if (String.IsNullOrEmpty(ddlSites.SelectedItem.Text)) { errorCount++; };
        }
        catch (Exception ex)
        {
            //ToDo: Handle Exception
            Elmah.ErrorSignal.FromContext(Context).Raise(ex);
            errorCount = 1;
            PopupWindow pcWindow = new PopupWindow("You haven't selected a company and/or site. If no site exists for your company when it should, please contact the AUA Contractor Services Team.");
            pcWindow.FooterText = "";
            pcWindow.ShowOnPageLoad = true;
            pcWindow.Modal = true;
            ASPxPopupControl1.Windows.Add(pcWindow);
        }

        if (errorCount > 0)
        {
            PopupWindow pcWindow = new PopupWindow("You haven't selected a company and/or site. If no site exists for your company when it should, please contact the AUA Contractor Services Team.");
            pcWindow.FooterText = "";
            pcWindow.ShowOnPageLoad = true;
            pcWindow.Modal = true;
            ASPxPopupControl1.Windows.Add(pcWindow);
        }
        else
        {
            DateTime sDateTime = new DateTime(Convert.ToInt32(ddlYear.SelectedItem.Value.ToString()), Convert.ToInt32(ddlMonth.SelectedItem.Value.ToString()), 1);
            if (sDateTime > DateTime.Now)
            {
                //Response.Redirect("javascript:alert('You can't choose a date in the future...')");
                PopupWindow pcWindow = new PopupWindow("You cannot enter monthly KPI information for a future month.");
                //pcWindow.ContentStyle.Font.Size =
                pcWindow.FooterText = "";
                pcWindow.ShowOnPageLoad = true;
                pcWindow.Modal = true;
                //ASPxLabel1.Text = "Unable to comply with your request as the date range you have selected is in the future.";
                //ASPxLabel1.CssClass = "Code InactiveColor";
                ASPxPopupControl1.Windows.Add(pcWindow);

            }
            else
            {
                //System.Threading.Thread.Sleep(1000);
                int SiteId = (int)ddlSites.SelectedItem.Value;
                int siteIndex = ddlSites.SelectedIndex;
                int CompanyId = (int)ddlCompanies.SelectedItem.Value;

                //Added by Vikas for Item#10  Change#3
                lblAlcoaPeakSite.Text = String.Format("the Alcoa {0} site",ddlSites.SelectedItem.Text);
                lblAlcoaAdvancedSite.Text = String.Format("the Alcoa {0} site", ddlSites.SelectedItem.Text);
                //End

                KPILoad_Init();
                SetKPILinks();
                ValidationSummary1.Visible = false;

                FillSitesCombo(CompanyId.ToString());
                ddlSites.SelectedIndex = siteIndex;
                FilllblCategory();
                //ddlSites.SelectedItem.Value = SiteId;

               
                
            }
        }
    }

    public void SetKPILinks()
    {
        DocumentsService dsservice = new DocumentsService();
        //Documents ds = dsservice.GetByDocumentId(249);
        string fileName;
        //if (ds.DocumentFileName == "Default")
        //{
        //    JSALink.HRef = "javascript:popUp('PopUps/KpiHelpFilePopup.aspx?Help=Default');";
        //}
        //else 
        //{
        //    fileName = ds.DocumentFileName;
        //    JSALink.HRef = String.Format("javascript:popUp2('Common/GetFile.aspx{0}')", QueryStringModule.Encrypt("Type=APSS&SubType=MR&File=" + fileName));
        //}
        //ds = dsservice.GetByDocumentId(250);
        //if (ds.DocumentFileName == "Default")
        //{
           
        //   WSCLink.HRef = "javascript:popUp('PopUps/KpiHelpFilePopup.aspx?Help=Default');";
        //}
        //else
        //{
        //    fileName = ds.DocumentFileName;
        //    WSCLink.HRef = String.Format("javascript:popUp2('Common/GetFile.aspx{0}')", QueryStringModule.Encrypt("Type=APSS&SubType=MR&File=" + fileName));
        //}
        //ds = dsservice.GetByDocumentId(252);
        //if (ds.DocumentFileName == "Default")
        //{
            
        //    MHLink.HRef = "javascript:popUp('PopUps/KpiHelpFilePopup.aspx?Help=Default');";
        //}
        //else
        //{
        //    fileName = ds.DocumentFileName;
        //    MHLink.HRef = String.Format("javascript:popUp2('Common/GetFile.aspx{0}')", QueryStringModule.Encrypt("Type=APSS&SubType=MR&File=" + fileName));
        //}
        //ds = dsservice.GetByDocumentId(253);
        //if (ds.DocumentFileName == "Default")
        //{
            
        //    BOLink.HRef = "javascript:popUp('PopUps/KpiHelpFilePopup.aspx?Help=Default');";
        //}
        //else
        //{
        //    fileName = ds.DocumentFileName;
        //    BOLink.HRef = String.Format("javascript:popUp2('Common/GetFile.aspx{0}')", QueryStringModule.Encrypt("Type=APSS&SubType=MR&File=" + fileName));
        //}
        //ds = dsservice.GetByDocumentId(254);
        //if (ds.DocumentFileName == "Default")
        //{
            
        //    TBMLink.HRef = "javascript:popUp('PopUps/KpiHelpFilePopup.aspx?Help=Default');";
        //}
        //else
        //{
        //    fileName = ds.DocumentFileName;
        //    TBMLink.HRef = String.Format("javascript:popUp2('Common/GetFile.aspx{0}')", QueryStringModule.Encrypt("Type=APSS&SubType=MR&File=" + fileName));
        //}
        //ds = dsservice.GetByDocumentId(256);
        //if (ds.DocumentFileName == "Default")
        //{
            
        //    AWCLink.HRef = "javascript:popUp('PopUps/KpiHelpFilePopup.aspx?Help=Default');";
        //}
        //else
        //{
        //    fileName = ds.DocumentFileName;
        //    AWCLink.HRef = String.Format("javascript:popUp2('Common/GetFile.aspx{0}')", QueryStringModule.Encrypt("Type=APSS&SubType=MR&File=" + fileName));
        //}
        //ds = dsservice.GetByDocumentId(257);
        //if (ds.DocumentFileName == "Default")
        //{
            
        //    AMCLink.HRef = "javascript:popUp('PopUps/KpiHelpFilePopup.aspx?Help=Default');";
        //}
        //else
        //{
        //    fileName = ds.DocumentFileName;
        //    AMCLink.HRef = String.Format("javascript:popUp2('Common/GetFile.aspx{0}')", QueryStringModule.Encrypt("Type=APSS&SubType=MR&File=" + fileName));
        //}
        //ds = dsservice.GetByDocumentId(258);
        //if (ds.DocumentFileName == "Default")
        //{
            
        //    FPLink.HRef = "javascript:popUp('PopUps/KpiHelpFilePopup.aspx?Help=Default');";
        //}
        //else
        //{
        //    fileName = ds.DocumentFileName;
        //    FPLink.HRef = String.Format("javascript:popUp2('Common/GetFile.aspx{0}')", QueryStringModule.Encrypt("Type=APSS&SubType=MR&File=" + fileName));
        //}
        TList<Documents> dslist = dsservice.GetByDocumentType("KH");
        foreach (Documents ds in dslist)
        {
            switch (ds.DocumentFileDescription)
            {
                case "JSA Field Audit Verifications":if (ds.DocumentFileName == "Default")
                    {
                        JSALink.HRef = "javascript:popUp('PopUps/KpiHelpFilePopup.aspx?Help=Default');";
                    }
                    else
                    {
                        fileName = ds.DocumentFileName;
                        JSALink.HRef = String.Format("javascript:popUp2('Common/GetFile.aspx{0}')", QueryStringModule.Encrypt("Type=APSS&SubType=MR&File=" + fileName));
                    }
                    break;
                case "Workplace Safety & Compliance":if (ds.DocumentFileName == "Default")
                    {

                        WSCLink.HRef = "javascript:popUp('PopUps/KpiHelpFilePopup.aspx?Help=Default');";
                    }
                    else
                    {
                        fileName = ds.DocumentFileName;
                        WSCLink.HRef = String.Format("javascript:popUp2('Common/GetFile.aspx{0}')", QueryStringModule.Encrypt("Type=APSS&SubType=MR&File=" + fileName));
                    }
                    break;
                case "Management Health Safety work contacts": if (ds.DocumentFileName == "Default")
                    {

                    MHLink.HRef = "javascript:popUp('PopUps/KpiHelpFilePopup.aspx?Help=Default');";
                    }
                    else
                    {
                    fileName = ds.DocumentFileName;
                    MHLink.HRef = String.Format("javascript:popUp2('Common/GetFile.aspx{0}')", QueryStringModule.Encrypt("Type=APSS&SubType=MR&File=" + fileName));
                    }
                    break;
                case "Behavioural Observations": if (ds.DocumentFileName == "Default")
                     {

                        BOLink.HRef = "javascript:popUp('PopUps/KpiHelpFilePopup.aspx?Help=Default');";
                     }
                    else
                    {
                    fileName = ds.DocumentFileName;
                    BOLink.HRef = String.Format("javascript:popUp2('Common/GetFile.aspx{0}')", QueryStringModule.Encrypt("Type=APSS&SubType=MR&File=" + fileName));
                    }
                    break;
                case "Tool box meetings per month": if (ds.DocumentFileName == "Default")
                    {

                        TBMLink.HRef = "javascript:popUp('PopUps/KpiHelpFilePopup.aspx?Help=Default');";
                    }
                    else
                    {
                        fileName = ds.DocumentFileName;
                        TBMLink.HRef = String.Format("javascript:popUp2('Common/GetFile.aspx{0}')", QueryStringModule.Encrypt("Type=APSS&SubType=MR&File=" + fileName));
                    }
                    break;
                case "Alcoa weekly contractors meeting":if (ds.DocumentFileName == "Default")
                    {

                        AWCLink.HRef = "javascript:popUp('PopUps/KpiHelpFilePopup.aspx?Help=Default');";
                    }
                    else
                    {
                        fileName = ds.DocumentFileName;
                        AWCLink.HRef = String.Format("javascript:popUp2('Common/GetFile.aspx{0}')", QueryStringModule.Encrypt("Type=APSS&SubType=MR&File=" + fileName));
                    }
                    break;
                case "Alcoa monthly contractors meeting": if (ds.DocumentFileName == "Default")
                    {

                        AMCLink.HRef = "javascript:popUp('PopUps/KpiHelpFilePopup.aspx?Help=Default');";
                    }
                    else
                    {
                        fileName = ds.DocumentFileName;
                        AMCLink.HRef = String.Format("javascript:popUp2('Common/GetFile.aspx{0}')", QueryStringModule.Encrypt("Type=APSS&SubType=MR&File=" + fileName));
                    }
                    break;
                case "Fatality Prevention discussion and presentation": if (ds.DocumentFileName == "Default")
                    {

                        FPLink.HRef = "javascript:popUp('PopUps/KpiHelpFilePopup.aspx?Help=Default');";
                    }
                    else
                    {
                        fileName = ds.DocumentFileName;
                        FPLink.HRef = String.Format("javascript:popUp2('Common/GetFile.aspx{0}')", QueryStringModule.Encrypt("Type=APSS&SubType=MR&File=" + fileName));
                    }
                    break;
            }
        
        }
    }

    public static void TabOnEnterKey(Control TextBoxToModify)
    {
        // This is our javascript - we fire the client-side click event of the button if the enter key is pressed.
        const string jsString = "if ((event.which && event.which == 13) || (event.keyCode && event.keyCode == 13)) {event.keyCode=9;} ";
        // We attach this to the onkeydown attribute - we have to cater for HtmlControl or WebControl.
        if (TextBoxToModify is HtmlControl)
            ((HtmlControl)TextBoxToModify).Attributes.Add("onkeydown", jsString);
        else if (TextBoxToModify is WebControl)
            ((WebControl)TextBoxToModify).Attributes.Add("onkeydown", jsString);
        else
        {
            // We throw an exception if TextBoxToTie is not of type HtmlControl or WebControl.
            throw new ArgumentException("Control TextBoxToModify should be derived from either System.Web.UI.HtmlControls.HtmlControl or System.Web.UI.WebControls.WebControl", "TextBoxToModify");
        }
    }

    public decimal? NullableStringToDecimal(string input)
    {
        if (String.IsNullOrEmpty(input)) { return null; }
        else { return Convert.ToDecimal(input); }
    }
    public int? NullableStringToInt(string input)
    {
        if (String.IsNullOrEmpty(input)) { return null; }
        else { return Convert.ToInt32(input); }
    }
    public int? NullableStringToInt(string input, int maxInt)
    {
        if (String.IsNullOrEmpty(input)) { return null; }
        else
        {
            int i = Convert.ToInt32(input);
            if (i > maxInt) i = maxInt;
            return i;
        }
    }
    public int? StringToInt(string input)
    {
        if (String.IsNullOrEmpty(input)) { return 0; }
        else { return Convert.ToInt32(input); }
    }
    public decimal? StringToDecimal(string input)
    {
        if (String.IsNullOrEmpty(input)) { return 0.0m; }
        else { return Convert.ToDecimal(input); }
    }

    public void CalcManHours()
    {
        decimal? totalManHours = 0.0m;
        totalManHours = StringToDecimal(tbGeneral.Text) + 
                        StringToDecimal(tbProject1.Text) + StringToDecimal(tbProject2.Text) + StringToDecimal(tbProject3.Text) + StringToDecimal(tbProject4.Text) +
                            StringToDecimal(tbProject5.Text) + StringToDecimal(tbProject6.Text) + StringToDecimal(tbProject7.Text) + StringToDecimal(tbProject8.Text) + StringToDecimal(tbProject9.Text) + StringToDecimal(tbProject10.Text) + StringToDecimal(tbProject11.Text) + StringToDecimal(tbProject12.Text) + StringToDecimal(tbProject13.Text) + StringToDecimal(tbProject14.Text) + StringToDecimal(tbProject15.Text) +
                        StringToDecimal(tbRefineryWork.Text) + StringToDecimal(tbResidueWork.Text) + StringToDecimal(tbSmeltingWork.Text) +
                            StringToDecimal(tbPowerGenerationWork.Text) +
                        StringToDecimal(tbCalcinerExpense.Text) + StringToDecimal(tbCalcinerCapital.Text) + StringToDecimal(tbResidue.Text);
        lblTotalManHours.Text = totalManHours.ToString();
        lblTotalManHours.Text = lblTotalManHours.Text.Replace(".0", "");
        if (String.IsNullOrEmpty(totalManHours.ToString()) == true) { lblTotalManHours.Text = "0"; }
    }
    public void CalcAlcoaAnnualAuditScore(int CompanyId)
    {
        lblqQAS.Text = "Yet to be conducted.";

        if (Convert.ToInt32(ddlYear.SelectedItem.Value) < 2009)
        {
            
            TwentyOnePointAuditService twentyOnePointAuditService = new TwentyOnePointAuditService();
            TList<TwentyOnePointAudit> twentyOnePointAuditList = twentyOnePointAuditService.GetAll(); //All KPI
            TList <TwentyOnePointAudit> twentyOnePointAuditList2 = twentyOnePointAuditList.FindAll( //Find KPI We Need
                delegate(TwentyOnePointAudit t2)
                {
                    return
                        t2.CompanyId == CompanyId &&
                        t2.Year == Convert.ToInt32(ddlYear.SelectedItem.Value) &&
                        t2.SiteId == Convert.ToInt32(ddlSites.SelectedItem.Value) &&
                        t2.QtrId == 5;
                }
            );



            if (twentyOnePointAuditList2.Count != 0)
            {
                foreach (TwentyOnePointAudit t1 in twentyOnePointAuditList2)
                {
                    lblqQAS.Text = String.Format("{0}%", t1.TotalRating);
                }
            }
        }
        else
        {
            CsaService csaService = new CsaService();
            Csa csa = csaService.GetByCompanyIdSiteIdQtrIdYear(CompanyId,
                                                                Convert.ToInt32(ddlSites.SelectedItem.Value),
                                                                5,
                                                                Convert.ToInt32(ddlYear.SelectedItem.Value));
            if (csa != null)
            {
                lblqQAS.Text = String.Format("{0}%", csa.TotalRating);
            }
        }
    }
    public void SetEnterToTab()
    {
        TabOnEnterKey(tbGeneral);
        //TabOnEnterKey(cbPoNumber1);
        //TabOnEnterKey(tbProject1);
        //TabOnEnterKey(cbProjectTitle2);
        //TabOnEnterKey(tbProject2);
        //TabOnEnterKey(cbProjectTitle3);
        //TabOnEnterKey(tbProject3);
        //TabOnEnterKey(cbProjectTitle4);
        //TabOnEnterKey(tbProject4);
        //TabOnEnterKey(cbProjectTitle5);
        //TabOnEnterKey(tbProject5);
        TabOnEnterKey(tbRefineryWork);
        TabOnEnterKey(tbResidueWork);
        TabOnEnterKey(tbSmeltingWork);
        TabOnEnterKey(tbPowerGenerationWork);
        TabOnEnterKey(tbCalcinerCapital);
        TabOnEnterKey(tbCalcinerExpense);
        TabOnEnterKey(tbResidue);
        TabOnEnterKey(tbPeakNoEmp);
        TabOnEnterKey(tbAvgNoEmp);
        TabOnEnterKey(tbEmployeesExcess);
        
        TabOnEnterKey(tbehsNoLWD);
        TabOnEnterKey(tbehspNoRD);
        TabOnEnterKey(tbCorrective);
        TabOnEnterKey(tbJSA);
        TabOnEnterKey(tbiWSC);
        TabOnEnterKey(tboNoHSWC);
        TabOnEnterKey(tboNoBSP);
        //TabOnEnterKey(tbqQAS);
        TabOnEnterKey(tbqNoNCI);
        TabOnEnterKey(tbmTbmpm);
        TabOnEnterKey(tbmAwcm);
        TabOnEnterKey(tbmAmcm);
        TabOnEnterKey(tbFatality);
        TabOnEnterKey(tbTraining);

        TabOnEnterKey(tbBloodAlcoholTests);
        TabOnEnterKey(tbPositiveBACResults);
        TabOnEnterKey(tbDrugTests);
        TabOnEnterKey(tbNonNegativeResults);
        TabOnEnterKey(tbPositiveResultsMedDeclared);
        TabOnEnterKey(tbPositiveResultsMedNotDeclared);
    }
    private void EnableDisable_Controls(bool EnabledFlag)
    {
        if (EnabledFlag)
        {
            //KPI
            tbGeneral.Enabled = false;
            //Projects
            int i = 1;
            while (i <= 15)
            {
                ASPxTextBox Hours = (ASPxTextBox)this.FindControl(String.Format("tbProject{0}", i));
                ASPxPageControl Tabs = (ASPxPageControl)this.FindControl(String.Format("pcProject{0}", i));
                ASPxTextBox ProjectNumber = (ASPxTextBox)Tabs.FindControl(String.Format("tbProjectNumber{0}", i));
                ASPxTextBox PoNumber = (ASPxTextBox)Tabs.FindControl(String.Format("tbPoNumber{0}", i));
                ASPxTextBox LineNumber = (ASPxTextBox)Tabs.FindControl(String.Format("tbLineNumber{0}", i));

                Hours.Enabled = false;
                ProjectNumber.Enabled = false;
                PoNumber.Enabled = false; ;
                LineNumber.Enabled = false;

                i++;
            }

            tbRefineryWork.Enabled = false;
            tbResidueWork.Enabled = false;
            tbSmeltingWork.Enabled = false;
            tbPowerGenerationWork.Enabled = false;
            //WAO Other Hours
            tbCalcinerExpense.Enabled = false;
            tbCalcinerCapital.Enabled = false;
            tbResidue.Enabled = false;
            //lblTotalManHours.Enabled = false;
            tbPeakNoEmp.Enabled = false;
            tbAvgNoEmp.Enabled = false;
            tbEmployeesExcess.Enabled = false;
        }
        else
        {
            //KPI
            tbGeneral.Enabled = true;
            //Projects
            int i = 1;
            while (i <= 15)
            {
                ASPxTextBox Hours = (ASPxTextBox)this.FindControl(String.Format("tbProject{0}", i));
                ASPxPageControl Tabs = (ASPxPageControl)this.FindControl(String.Format("pcProject{0}", i));
                ASPxTextBox ProjectNumber = (ASPxTextBox)Tabs.FindControl(String.Format("tbProjectNumber{0}", i));
                ASPxTextBox PoNumber = (ASPxTextBox)Tabs.FindControl(String.Format("tbPoNumber{0}", i));
                ASPxTextBox LineNumber = (ASPxTextBox)Tabs.FindControl(String.Format("tbLineNumber{0}", i));

                Hours.Enabled = true;
                ProjectNumber.Enabled = true;
                PoNumber.Enabled = true;
                LineNumber.Enabled = true;

                i++;
            }

            tbRefineryWork.Enabled = true;
            tbResidueWork.Enabled = true;
            tbSmeltingWork.Enabled = true;
            tbPowerGenerationWork.Enabled = true;
            //WAO - Other Hours
            tbCalcinerExpense.Enabled = true;
            tbCalcinerCapital.Enabled = true;
            tbResidue.Enabled = true;
            //lblTotalManHours.Enabled = true;
            tbPeakNoEmp.Enabled = true;
            tbAvgNoEmp.Enabled = true;
            tbEmployeesExcess.Enabled = true;
        }

        //TODO: Fix Bug in the below code so can use it. Innerhtml error
        //TextBox oTextBoxCtl;
        //HtmlForm oForm;

        //oForm = (HtmlForm)this.Page.FindControl("Form1");
        //foreach (Control oCtl in oForm.Controls)
        //{
        //    switch (oCtl.GetType().ToString())
        //    {
        //        case "System.Web.UI.WebControls.TextBox":
        //            oTextBoxCtl = (TextBox)oCtl;
        //            oTextBoxCtl.ReadOnly = EnabledFlag;
        //            break;
        //    }
        //}
    }

    private void InitValuesIfNull()
    {
        if (tbGeneral.Enabled == false)
        {
            if (String.IsNullOrEmpty(tbGeneral.Text)) { tbGeneral.Text = "0"; };

            if (String.IsNullOrEmpty(tbRefineryWork.Text)) { tbRefineryWork.Text = "0"; };
            if (String.IsNullOrEmpty(tbResidueWork.Text)) { tbResidueWork.Text = "0"; };
            if (String.IsNullOrEmpty(tbSmeltingWork.Text)) { tbSmeltingWork.Text = "0"; };
            if (String.IsNullOrEmpty(tbPowerGenerationWork.Text)) { tbPowerGenerationWork.Text = "0"; };

            if (String.IsNullOrEmpty(tbCalcinerExpense.Text)) { tbCalcinerExpense.Text = "0"; };
            if (String.IsNullOrEmpty(tbCalcinerCapital.Text)) { tbCalcinerCapital.Text = "0"; };
            if (String.IsNullOrEmpty(tbResidue.Text)) { tbResidue.Text = "0"; };

            if (String.IsNullOrEmpty(lblTotalManHours.Text)) { lblTotalManHours.Text = "0"; };
            if (String.IsNullOrEmpty(tbPeakNoEmp.Text)) { tbPeakNoEmp.Text = "0"; };
            if (String.IsNullOrEmpty(tbAvgNoEmp.Text)) { tbAvgNoEmp.Text = "0"; };
            if (String.IsNullOrEmpty(tbEmployeesExcess.Text)) { tbEmployeesExcess.Text = "0"; };
        }
    }
    public void CleanUp()
    {
        //KPI
        tbGeneral.Text = "";
        //Projects

        int i = 1;
        while (i <= 15)
        {
            ASPxLabel SelectedProject = (ASPxLabel)this.FindControl(String.Format("lblSelectedProject{0}", i));
            ASPxTextBox Hours = (ASPxTextBox)this.FindControl(String.Format("tbProject{0}", i));
            ASPxPageControl Tabs = (ASPxPageControl)this.FindControl(String.Format("pcProject{0}", i));
            ASPxTextBox ProjectNumber = (ASPxTextBox)Tabs.FindControl(String.Format("tbProjectNumber{0}", i));
            ASPxTextBox PoNumber = (ASPxTextBox)Tabs.FindControl(String.Format("tbPoNumber{0}", i));
            ASPxTextBox LineNumber = (ASPxTextBox)Tabs.FindControl(String.Format("tbLineNumber{0}", i));

            SelectedProject.Text = "(n/a)";
            Hours.Text = "";
            ProjectNumber.Text = "";
            PoNumber.Text = "";
            LineNumber.Text = "";

            i++;
        }

        tbRefineryWork.Text = "";
        tbResidueWork.Text = "";
        tbSmeltingWork.Text = "";
        tbPowerGenerationWork.Text = "";
        //WAO - Other Hours
        tbCalcinerExpense.Text = "";
        tbCalcinerCapital.Text = "";
        tbResidue.Text = "";
        lblTotalManHours.Text = "0";
        //Employees
        tbPeakNoEmp.Text = "";
        tbAvgNoEmp.Text = "";
        tbEmployeesExcess.Text = "";
        //Incident Performance
        lblipFATI.Text = "-";
        lblipMTI.Text = "-";
        lblipRDI.Text = "-";
        lblipLTI.Text = "-";
        lblipIFE.Text = "-";
        lblipRN.Text = "-";
        //EHS Performance
        tbehsNoLWD.Text = "";
        tbehspNoRD.Text = "";
        //Audits
        tbCorrective.Text = "";
        tbJSA.Text = "";
        //Inspections
        tbiWSC.Text = "";
        //Observations Number of
        tboNoHSWC.Text = "";
        tboNoBSP.Text = "";
        //Quality
        lblqQAS.Text = "Yet to be conducted.";
        tbqNoNCI.Text = "";
        //Meetings
        tbmTbmpm.Text = "";
        tbmAwcm.Text = "";
        tbmAmcm.Text = "";
        tbFatality.Text = "";
        //Mandatory Training
        tbTraining.Text = "";
        tbmtOthers.Text = "";
        //Fitness for work
        tbBloodAlcoholTests.Text = "";
        tbPositiveBACResults.Text = "";
        tbDrugTests.Text = "";
        tbNonNegativeResults.Text = "";
        tbPositiveResultsMedDeclared.Text = "";
        tbPositiveResultsMedNotDeclared.Text = "";

        //tbProjectTitle1.Text = "Enter Project Title 1";
        //tbProjectTitle2.Text = "Enter Project Title 2";
        //tbProjectTitle3.Text = "Enter Project Title 3";
        //tbProjectTitle4.Text = "Enter Project Title 4";
        //tbProjectTitle5.Text = "Enter Project Title 5";

        lblSafetyPlanApproved.Text = "";
        lblSafetyPlanSubmitted.Text = "";
    }

    public string monthNotoName(int month)
    {
        if (month == 1) { return "January"; };
        if (month == 2) { return "February"; };
        if (month == 3) { return "March"; };
        if (month == 4) { return "April"; };
        if (month == 5) { return "May"; };
        if (month == 6) { return "June"; };
        if (month == 7) { return "July"; };
        if (month == 8) { return "August"; };
        if (month == 9) { return "September"; };
        if (month == 10) { return "October"; };
        if (month == 11) { return "November"; };
        if (month == 12) { return "December"; };
        return "Unknown";
    }

    protected void KPILoad_Init()
    {
        lblCode.Text = "";
        lblRestriction.Visible = false;
        //int KpiContractorCutOff = -12;
        //DateTime dtCurrent = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
        //DateTime dtAllowed = dtCurrent.AddMonths(KpiContractorCutOff);
        //DateTime dtSelected = new DateTime(Convert.ToInt32(ddlYear.SelectedValue), Convert.ToInt32(ddlMonth.SelectedValue), 1);
        //if (dtSelected >= dtAllowed)
        //{
        //    Response.Write("good");
        //}
        int SiteId = (int)ddlSites.SelectedItem.Value;
        int CompanyId = (int)ddlCompanies.SelectedItem.Value;

        if (auth.RoleId == (int)RoleList.Contractor)
        {
            //if ((Convert.ToInt32(ddlMonth.SelectedValue) < currentMonth) || (Convert.ToInt32(ddlYear.SelectedValue) < currentYear))
            //{ EnableDisable_Controls(true); }
            //else { EnableDisable_Controls(false); }

            int KpiContractorCutOff = -1; //default

            ConfigService configService = new ConfigService();
            TList<Config> configList = configService.GetAll();
            TList<Config> configList2 = configList.FindAll(delegate(Config c2) { return c2.Key == "KpiContractorCutOff"; });

            if (configList2.Count != 0)
            {
                foreach (Config t1 in configList2)
                {
                    int.TryParse(t1.Value, out KpiContractorCutOff);
                }
            }

            DateTime dtCurrent = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            DateTime dtAllowed = dtCurrent.AddMonths(KpiContractorCutOff);
            DateTime dtSelected = new DateTime((int)ddlYear.SelectedItem.Value, (int)ddlMonth.SelectedItem.Value, 1);
            if (dtSelected >= dtAllowed)
            {
                EnableDisable_Controls(false);
            }
            else
            {
                EnableDisable_Controls(true);
                lblRestriction.Visible = true;
                lblRestriction.Text = String.Format("You are not allowed to modify the core KPI's because you have selected a time that is older than the last allowed date: {0} {1}", monthNotoName(dtAllowed.Month), dtAllowed.Year);
            }

            panelKPI_Record.Visible = true;
            lbldCompanyname.Text = ddlCompanies.SelectedItem.ToString();
            lbldSite.Text = ddlSites.SelectedItem.ToString();
            lbldTime.Text = String.Format("{0} {1}", ddlMonth.SelectedItem, ddlYear.SelectedItem);
            panelKPIinfo.Visible = true;
            btnSave.Visible = true;
            KPILoad(CompanyId, SiteId, (int)ddlMonth.SelectedItem.Value, (int)ddlYear.SelectedItem.Value);
            InitValuesIfNull();
        }

        if (auth.RoleId == (int)RoleList.Administrator )
        {
            panelKPI_Record.Visible = true;
            lbldCompanyname.Text = ddlCompanies.SelectedItem.Text;
            lbldSite.Text = ddlSites.SelectedItem.Text;
            lbldTime.Text = String.Format("{0} {1}", ddlMonth.SelectedItem.Text, ddlYear.SelectedItem.Text);
            panelKPIinfo.Visible = true;
            KPILoad(CompanyId, SiteId, (int)ddlMonth.SelectedItem.Value, (int)ddlYear.SelectedItem.Value);
            InitValuesIfNull();
        }


        if (auth.RoleId == (int)RoleList.Reader)
        {
            panelKPI_Record.Visible = true;
            lbldCompanyname.Text = ddlCompanies.SelectedItem.Text;
            lbldSite.Text = ddlSites.SelectedItem.Text;
            lbldTime.Text = String.Format("{0} {1}", ddlMonth.SelectedItem.Text, ddlYear.SelectedItem.Text);
            panelKPIinfo.Visible = true;
            btnSave.Visible = false;
            KPILoad(CompanyId, SiteId, (int)ddlMonth.SelectedItem.Value, (int)ddlYear.SelectedItem.Value);
            InitValuesIfNull();
        }


        SetVisibililtyByCategory();


    }

    private void SetVisibililtyByCategory()
    {
        trInspections.Visible = true;
        trWSC.Visible = true;
        trObservations.Visible = true;
        trMHS.Visible = true;
        trBO.Visible = true;
        trJSAAudit.Visible = true;
        trAlcoaWCM.Visible = true;
        trAlcoaMCM.Visible = true;
        trSMPSubmitted.Visible = true;
        trSMPApproved.Visible = true;
        if (!String.IsNullOrEmpty(GetCategory()))
        {
            if (GetCategory() == "Embedded")
            {
                //These added by Ashley Goldstraw 4/8/2016 DT530 - to make sure they are reset back to visible if a lesser residential category is selected and then set back to this
                //that they will be visible again
                trInspections.Visible = true;
                trWSC.Visible = true;
                trObservations.Visible = true;
                trMHS.Visible = true;
                trBO.Visible = true;
                trJSAAudit.Visible = true;
                trAlcoaWCM.Visible = true;
                trAlcoaMCM.Visible = true;
                trSMPSubmitted.Visible = true;
                trSMPApproved.Visible = true;
                trFatality.Visible = true;
            }
            else if (GetCategory() == "Non-Embedded 1")
            {
                //These added by Ashley Goldstraw 4/8/2016 DT530 - to make sure they are reset back to visible if a lesser residential category is selected and then set back to this
                //that they will be visible again
                trInspections.Visible = true;
                trWSC.Visible = true;
                trObservations.Visible = true;
                trMHS.Visible = true;
                trBO.Visible = true;
                trJSAAudit.Visible = true;
                trAlcoaWCM.Visible = true;
                trAlcoaMCM.Visible = true;
                trSMPSubmitted.Visible = true;
                trSMPApproved.Visible = true;
                trFatality.Visible = true;
             
            }
            else if (GetCategory() == "Non-Embedded 2")
            {
                trAlcoaWCM.Visible = false;
                trSMPSubmitted.Visible = false;
                trSMPApproved.Visible = false;
                trFatality.Visible = false;//CSMS Enhancement: Spec#15

                //These added by Ashley Goldstraw 4/8/2016 DT530 - to make sure they are reset back to visible if a lesser residential category is selected and then set back to this
                //that they will be visible again
                trInspections.Visible = true;
                trWSC.Visible = true;
                trObservations.Visible = true;
                trMHS.Visible = true;
                trBO.Visible = true;
                trJSAAudit.Visible = true;

            }
            else if (GetCategory() == "Non-Embedded 3")
            {
                trInspections.Visible = false;
                trWSC.Visible = false;
                trObservations.Visible = false;
                trMHS.Visible = false;
                trBO.Visible = false;
                trJSAAudit.Visible = false;
                trAlcoaWCM.Visible = false;
                trAlcoaMCM.Visible = false;
                trSMPSubmitted.Visible = false;
                trSMPApproved.Visible = false;
                trFatality.Visible = false;//CSMS Enhancement: Spec#15
            }
            else 
            {
                //do nothing
            }
        }
        
    }

    private void KPILoad(Repo.CSMS.DAL.EntityModels.Kpi_Result kpi, int? CompanyId, int? SiteId, DateTime? dt)
    {
        if (kpi != null)
        {
            lblKpiID.Text = kpi.KpiId.ToString();
            UsersService usersService = new UsersService();
            Users userEntity;
            userEntity = usersService.GetByUserId(kpi.CreatedbyUserId);
            lbldCreatedBy.Text = String.Format("{0} {1} [{2}] at {3}", userEntity.FirstName, userEntity.LastName, userEntity.UserLogon, kpi.DateAdded);
            userEntity = usersService.GetByUserId(kpi.ModifiedbyUserId);
            lbldLastModifiedBy.Text = String.Format("{0} {1} [{2}] at {3}", userEntity.FirstName, userEntity.LastName, userEntity.UserLogon, kpi.Datemodified);
            //KPI
            tbGeneral.Text = kpi.kpiGeneral.ToString().Replace(".0", "");
            tbCalcinerExpense.Text = kpi.kpiCalcinerExpense.ToString().Replace(".0", "");
            tbCalcinerCapital.Text = kpi.kpiCalcinerCapital.ToString().Replace(".0", "");
            tbResidue.Text = kpi.kpiResidue.ToString().Replace(".0", "");
            //Projects
            

            //KpiProjectListService kplService = new KpiProjectListService();
            //if(!String.IsNullOrEmpty(kpi.ProjectCapital1Title))
            //{
            //    KpiProjectList kpl = kplService.GetByProjectDescription(kpi.ProjectCapital1Title);
            //    if (kpl != null)
            //    {
            //        cbProjectTitle1.Value = kpi.ProjectCapital1Title;
            //    }
            //    else
            //    {
            //        cbProjectTitle1.Items.Add(kpi.ProjectCapital1Title, kpi.ProjectCapital1Title);
            //        cbProjectTitle1.Value = kpi.ProjectCapital1Title;
            //    }
            //}

            //LoadKpiProject(kpi.ProjectCapital1Po, cbPoNumber1, kpi.ProjectCapital1Line, cbLineNumber1, kpi.ProjectCapital1Title, cbProjectTitle1);

            int i = 1;
            while (i <= 15)
            {
                string _Hours = "";
                string _Title = "";
                string _Po = "";
                string _Line = "";
                if (i == 1) { _Hours = kpi.projectCapital1Hours.ToString(); _Title = kpi.projectCapital1Title; _Po = kpi.projectCapital1Po; _Line = kpi.projectCapital1Line; };
                if (i == 2) { _Hours = kpi.projectCapital2Hours.ToString(); _Title = kpi.projectCapital2Title; _Po = kpi.projectCapital2Po; _Line = kpi.projectCapital2Line; };
                if (i == 3) { _Hours = kpi.projectCapital3Hours.ToString(); _Title = kpi.projectCapital3Title; _Po = kpi.projectCapital3Po; _Line = kpi.projectCapital3Line; };
                if (i == 4) { _Hours = kpi.projectCapital4Hours.ToString(); _Title = kpi.projectCapital4Title; _Po = kpi.projectCapital4Po; _Line = kpi.projectCapital4Line; };
                if (i == 5) { _Hours = kpi.projectCapital5Hours.ToString(); _Title = kpi.projectCapital5Title; _Po = kpi.projectCapital5Po; _Line = kpi.projectCapital5Line; };
                if (i == 6) { _Hours = kpi.projectCapital6Hours.ToString(); _Title = kpi.projectCapital6Title; _Po = kpi.projectCapital6Po; _Line = kpi.projectCapital6Line; };
                if (i == 7) { _Hours = kpi.projectCapital7Hours.ToString(); _Title = kpi.projectCapital7Title; _Po = kpi.projectCapital7Po; _Line = kpi.projectCapital7Line; };
                if (i == 8) { _Hours = kpi.projectCapital8Hours.ToString(); _Title = kpi.projectCapital8Title; _Po = kpi.projectCapital8Po; _Line = kpi.projectCapital8Line; };
                if (i == 9) { _Hours = kpi.projectCapital9Hours.ToString(); _Title = kpi.projectCapital9Title; _Po = kpi.projectCapital9Po; _Line = kpi.projectCapital9Line; };
                if (i == 10) { _Hours = kpi.projectCapital10Hours.ToString(); _Title = kpi.projectCapital10Title; _Po = kpi.projectCapital10Po; _Line = kpi.projectCapital10Line; };
                if (i == 11) { _Hours = kpi.projectCapital11Hours.ToString(); _Title = kpi.projectCapital11Title; _Po = kpi.projectCapital11Po; _Line = kpi.projectCapital11Line; };
                if (i == 12) { _Hours = kpi.projectCapital12Hours.ToString(); _Title = kpi.projectCapital12Title; _Po = kpi.projectCapital12Po; _Line = kpi.projectCapital12Line; };
                if (i == 13) { _Hours = kpi.projectCapital13Hours.ToString(); _Title = kpi.projectCapital13Title; _Po = kpi.projectCapital13Po; _Line = kpi.projectCapital13Line; };
                if (i == 14) { _Hours = kpi.projectCapital14Hours.ToString(); _Title = kpi.projectCapital14Title; _Po = kpi.projectCapital14Po; _Line = kpi.projectCapital14Line; };
                if (i == 15) { _Hours = kpi.projectCapital15Hours.ToString(); _Title = kpi.projectCapital15Title; _Po = kpi.projectCapital15Po; _Line = kpi.projectCapital15Line; };


                ASPxLabel SelectedProject = (ASPxLabel)this.FindControl(String.Format("lblSelectedProject{0}", i));
                ASPxTextBox Hours = (ASPxTextBox)this.FindControl(String.Format("tbProject{0}", i));
                ASPxPageControl Tabs = (ASPxPageControl)this.FindControl(String.Format("pcProject{0}", i));
                ASPxTextBox ProjectNumber = (ASPxTextBox)Tabs.FindControl(String.Format("tbProjectNumber{0}", i));
                ASPxTextBox PoNumber = (ASPxTextBox)Tabs.FindControl(String.Format("tbPoNumber{0}", i));
                ASPxTextBox LineNumber = (ASPxTextBox)Tabs.FindControl(String.Format("tbLineNumber{0}", i));

                LoadKpiProject(_Po, PoNumber, _Line, LineNumber, _Title, ProjectNumber, SelectedProject);

                if (!String.IsNullOrEmpty(_Po) && !String.IsNullOrEmpty(_Line))
                {
                    Tabs.ActiveTabIndex = 1;
                }
                else
                {
                    Tabs.ActiveTabIndex = 0;
                }

                Hours.Text = _Hours.Replace(".0", "");

                i++;
            }
            //AHEA Managed Projects (Capital)
            tbRefineryWork.Text = kpi.aheaRefineryWork.ToString().Replace(".0", "");
            tbResidueWork.Text = kpi.aheaResidueWork.ToString().Replace(".0", "");
            tbSmeltingWork.Text = kpi.aheaSmeltingWork.ToString().Replace(".0", "");
            tbPowerGenerationWork.Text = kpi.aheaPowerGenerationWork.ToString().Replace(".0", "");
            lblTotalManHours.Text = kpi.aheaTotalManHours.ToString().Replace(".0", "");
            //decimal totalManhours = 0.0m;
            //totalManhours = StringToDecimal(tbGeneral.Text) + StringToDecimal(tbCalcinerExpense.Text) + StringToDecimal(tbCalcinerCapital.Text)
            //                    + StringToDecimal(tbResidue.Text) + StringToDecimal(tbProject1.Text) + StringToDecimal(tbProject2.Text)
            //                    + StringToDecimal(tbProject3.Text) + StringToDecimal(tbProject4.Text) + StringToDecimal(tbProject5.Text)
            //                    + StringToDecimal(tbRefineryWork.Text) + StringToDecimal(tbResidueWork.Text);
            //tbTotalManHours.Text = totalManHours;
            tbPeakNoEmp.Text = kpi.aheaPeakNopplSiteWeek.ToString();
            tbAvgNoEmp.Text = kpi.aheaAvgNopplSiteMonth.ToString();
            tbEmployeesExcess.Text = kpi.aheaNoEmpExcessMonth.ToString();
            //Incident Performance
            lblipFATI.Text = kpi.ipFATI.ToString();
            lblipMTI.Text = kpi.ipMTI.ToString();
            lblipRDI.Text = kpi.ipRDI.ToString();
            lblipLTI.Text = kpi.ipLTI.ToString();
            lblipIFE.Text = kpi.ipIFE.ToString();
            lblipRN.Text = kpi.ipRN.ToString();
            //EHS Performance
            tbehsNoLWD.Text = kpi.ehspNoLWD.ToString();
            tbehspNoRD.Text = kpi.ehspNoRD.ToString();
            //Audits
            tbCorrective.Text = kpi.ehsCorrective.ToString();
            tbJSA.Text = kpi.JSAAudits.ToString();
            //Inspections
            tbiWSC.Text = kpi.iWSC.ToString();
            //Observations Number of
            tboNoHSWC.Text = kpi.oNoHSWC.ToString();
            tboNoBSP.Text = kpi.oNoBSP.ToString();
            //Quality
            lblqQAS.Text = kpi.qQAS.ToString();
            if (String.IsNullOrEmpty(kpi.qQAS.ToString()) == true) { lblqQAS.Text = "Yet to be conducted."; }
            tbqNoNCI.Text = kpi.qNoNCI.ToString();
            //Meetings
            tbmTbmpm.Text = kpi.mTbmpm.ToString();
            tbmAwcm.Text = kpi.mAwcm.ToString();
            tbmAmcm.Text = kpi.mAmcm.ToString();
            tbFatality.Text = kpi.mFatality.ToString();
            //Mandatory Training
            tbTraining.Text = kpi.Training.ToString();
            tbmtOthers.Text = kpi.mtOthers.ToString();
            //Fitness for work
            tbBloodAlcoholTests.Text = kpi.ffwBloodAlcoholTests.ToString();
            tbPositiveBACResults.Text = kpi.ffwPositiveBACResults.ToString();
            tbDrugTests.Text = kpi.ffwDrugTests.ToString();
            tbNonNegativeResults.Text = kpi.ffwNonNegativeResults.ToString();
            tbPositiveResultsMedDeclared.Text = kpi.ffwPositiveResultsMedDeclared.ToString();
            tbPositiveResultsMedNotDeclared.Text = kpi.ffwPositiveResultsMedNotDeclared.ToString();

            // Safety Plan submitted for year
            FileDbFilters query = new FileDbFilters();
            query.Append(FileDbColumn.CompanyId, kpi.CompanyId.ToString());
            //DT 3266 Changes
            //query.Append(FileDbColumn.ModifiedDate, String.Format("%{0}%", ddlYear.Value));
            query.Append(FileDbColumn.Description, String.Format("%{0}%", ddlYear.Value));

            int count = 0;
            TList<FileDb> list = DataRepository.FileDbProvider.GetPaged(query.ToString(), null, 0, 100, out count);

            if (count > 0)
            {
                lblSafetyPlanSubmitted.Text = "YES";
                lblSafetyPlanSubmitted.ForeColor = Color.Green;
                lblSafetyPlanSubmittedMsg.Visible = false;
            }
            else
            {
                lblSafetyPlanSubmitted.Text = "NO";
                lblSafetyPlanSubmitted.ForeColor = Color.Red;
                lblSafetyPlanSubmittedMsg.Visible = true;
            }

            // Safety Plan approved for year
            FileDbFilters query2 = new FileDbFilters();
            query2.Append(FileDbColumn.CompanyId, kpi.CompanyId.ToString());
            //DT 3266 Changes
            //query2.Append(FileDbColumn.ModifiedDate, String.Format("%{0}%", ddlYear.Value));
            query2.Append(FileDbColumn.Description, String.Format("%{0}%", ddlYear.Value));
            query2.Append(FileDbColumn.StatusId, ((int)SafetyPlanStatusList.Approved).ToString());

            count = 0;
            TList<FileDb> list2 = DataRepository.FileDbProvider.GetPaged(query2.ToString(), null, 0, 100, out count);

            if (count > 0)
            {
                lblSafetyPlanApproved.Text = "YES";
                lblSafetyPlanApproved.ForeColor = Color.Green;
            }
            else
            {
                FileDbFilters query3 = new FileDbFilters();
                query3.Append(FileDbColumn.CompanyId, kpi.CompanyId.ToString());
                query3.Append(FileDbColumn.ModifiedDate, String.Format("%{0}%", DateTime.Now.Year));
                query3.Append(FileDbColumn.StatusId, ((int)SafetyPlanStatusList.Being_Assessed).ToString());

                int count2 = 0;
                TList<FileDb> list3 = DataRepository.FileDbProvider.GetPaged(query3.ToString(), null, 0, 100, out count2);

                if (count2 > 0)
                {
                    lblSafetyPlanApproved.Text = "Being Assessed";
                    lblSafetyPlanApproved.ForeColor = Color.Orange;
                }
                else
                {
                    lblSafetyPlanApproved.Text = "NO";
                    lblSafetyPlanApproved.ForeColor = Color.Red;
                }
            }

            CalcManHours();
            CalcAlcoaAnnualAuditScore(kpi.CompanyId);

            InitValuesIfNull();

            SessionHandler.KPIState = "Update";
            btnSave.Text = "Update KPI Record";
            Label1.Visible = false;
        }
        else
        {
            SessionHandler.KPIState = "New";
            btnSave.Text = "Save New KPI Record";
            newKPI();
            CalcAlcoaAnnualAuditScore(Convert.ToInt32(CompanyId));
        }
    }
    private void KPILoad(int CompanyId, int SiteId, int KpiMonth, int KpiYear)
    {
        CleanUp();
        //KpiService kpiService = new KpiService();
        var kpi = this.StoredProcedureService.SP_GetByCompanyIdSiteIdKpiDateTime(CompanyId, SiteId, new DateTime(KpiYear, KpiMonth, 1)).FirstOrDefault();
        //Uncommented by Swagato
        KPILoad(kpi, CompanyId, null, null);
    }
    private void KPILoadbyId(int _KpiId)
    {
      //  CleanUp();
        //KpiService kpiService = new KpiService();
        var kpi = this.StoredProcedureService.SP_GetByKpiId(_KpiId).FirstOrDefault();
        //Uncommented by Swagato
        KPILoad(kpi, kpi.CompanyId, null, null);
    }

    private void saveKPI()
    {
        //System.Threading.Thread.Sleep(500);
        KpiService kpiService = new KpiService();
        var kpi = new Repo.CSMS.DAL.EntityModels.Kpi();

        //kpi.ModifiedbyUserId = auth.UserId;
        //kpi.Datemodified = DateTime.Now;
        //KPI
        kpi.kpiGeneral = Convert.ToDecimal(tbGeneral.Text);
        kpi.kpiCalcinerExpense = Convert.ToDecimal(tbCalcinerExpense.Text);
        kpi.kpiCalcinerCapital = Convert.ToDecimal(tbCalcinerCapital.Text);
        kpi.kpiResidue = Convert.ToDecimal(tbResidue.Text);

        //Projects
        int i = 1;
        while (i <= 15)
        {
            Decimal? _Hours = null;
            string _Title = "";
            string _Po = "";
            string _Line = "";

            ASPxLabel SelectedProject = (ASPxLabel)this.FindControl(String.Format("lblSelectedProject{0}", i));
            ASPxTextBox Hours = (ASPxTextBox)this.FindControl(String.Format("tbProject{0}", i));
            ASPxPageControl Tabs = (ASPxPageControl)this.FindControl(String.Format("pcProject{0}", i));
            ASPxTextBox ProjectNumber = (ASPxTextBox)Tabs.FindControl(String.Format("tbProjectNumber{0}", i));
            ASPxTextBox PoNumber = (ASPxTextBox)Tabs.FindControl(String.Format("tbPoNumber{0}", i));
            ASPxTextBox LineNumber = (ASPxTextBox)Tabs.FindControl(String.Format("tbLineNumber{0}", i));

            _Hours = NullableStringToDecimal(Hours.Text);
            if (Tabs.ActiveTabIndex == 0)
            {
                if (!String.IsNullOrEmpty(ProjectNumber.Text)) _Title = ProjectNumber.Text;
            }
            else if (Tabs.ActiveTabIndex == 1)
            {
                if (!String.IsNullOrEmpty(PoNumber.Text) && !String.IsNullOrEmpty(LineNumber.Text))
                {
                    _Po = PoNumber.Text;
                    _Line = LineNumber.Text;
                    _Title = GetProjectNumber(_Po, _Line);
                }
            }

            if (i == 1) { kpi.projectCapital1Hours = _Hours; kpi.projectCapital1Title = _Title; kpi.projectCapital1Po = _Po; kpi.projectCapital1Line = _Line; };
            if (i == 2) { kpi.projectCapital2Hours = _Hours; kpi.projectCapital2Title = _Title; kpi.projectCapital2Po = _Po; kpi.projectCapital2Line = _Line; };
            if (i == 3) { kpi.projectCapital3Hours = _Hours; kpi.projectCapital3Title = _Title; kpi.projectCapital3Po = _Po; kpi.projectCapital3Line = _Line; };
            if (i == 4) { kpi.projectCapital4Hours = _Hours; kpi.projectCapital4Title = _Title; kpi.projectCapital4Po = _Po; kpi.projectCapital4Line = _Line; };
            if (i == 5) { kpi.projectCapital5Hours = _Hours; kpi.projectCapital5Title = _Title; kpi.projectCapital5Po = _Po; kpi.projectCapital5Line = _Line; };
            if (i == 6) { kpi.projectCapital6Hours = _Hours; kpi.projectCapital6Title = _Title; kpi.projectCapital6Po = _Po; kpi.projectCapital6Line = _Line; };
            if (i == 7) { kpi.projectCapital7Hours = _Hours; kpi.projectCapital7Title = _Title; kpi.projectCapital7Po = _Po; kpi.projectCapital7Line = _Line; };
            if (i == 8) { kpi.projectCapital8Hours = _Hours; kpi.projectCapital8Title = _Title; kpi.projectCapital8Po = _Po; kpi.projectCapital8Line = _Line; };
            if (i == 9) { kpi.projectCapital9Hours = _Hours; kpi.projectCapital9Title = _Title; kpi.projectCapital9Po = _Po; kpi.projectCapital9Line = _Line; };
            if (i == 10) { kpi.projectCapital10Hours = _Hours; kpi.projectCapital10Title = _Title; kpi.projectCapital10Po = _Po; kpi.projectCapital10Line = _Line; };
            if (i == 11) { kpi.projectCapital11Hours = _Hours; kpi.projectCapital11Title = _Title; kpi.projectCapital11Po = _Po; kpi.projectCapital11Line = _Line; };
            if (i == 12) { kpi.projectCapital12Hours = _Hours; kpi.projectCapital12Title = _Title; kpi.projectCapital12Po = _Po; kpi.projectCapital12Line = _Line; };
            if (i == 13) { kpi.projectCapital13Hours = _Hours; kpi.projectCapital13Title = _Title; kpi.projectCapital13Po = _Po; kpi.projectCapital13Line = _Line; };
            if (i == 14) { kpi.projectCapital14Hours = _Hours; kpi.projectCapital14Title = _Title; kpi.projectCapital14Po = _Po; kpi.projectCapital14Line = _Line; };
            if (i == 15) { kpi.projectCapital15Hours = _Hours; kpi.projectCapital15Title = _Title; kpi.projectCapital15Po = _Po; kpi.projectCapital15Line = _Line; };

            i++;
        }

       
        //AHEA Managed Projects (Capital)
        kpi.aheaRefineryWork = Convert.ToDecimal(tbRefineryWork.Text);
        kpi.aheaResidueWork = Convert.ToDecimal(tbResidueWork.Text);
        kpi.aheaSmeltingWork = Convert.ToDecimal(tbSmeltingWork.Text);
        kpi.aheaPowerGenerationWork = Convert.ToDecimal(tbPowerGenerationWork.Text);
        CalcManHours();
        decimal AheaTotalManHours = kpi.kpiGeneral + kpi.kpiCalcinerExpense + kpi.kpiCalcinerCapital + kpi.kpiResidue +
                                    kpi.aheaRefineryWork + kpi.aheaResidueWork;
        decimal? ProjectHours = 0;
        if (kpi.projectCapital1Hours.HasValue) { ProjectHours += kpi.projectCapital1Hours; };
        if (kpi.projectCapital2Hours.HasValue) { ProjectHours += kpi.projectCapital2Hours; };
        if (kpi.projectCapital3Hours.HasValue) { ProjectHours += kpi.projectCapital3Hours; };
        if (kpi.projectCapital4Hours.HasValue) { ProjectHours += kpi.projectCapital4Hours; };
        if (kpi.projectCapital5Hours.HasValue) { ProjectHours += kpi.projectCapital5Hours; };
        if (kpi.projectCapital6Hours.HasValue) { ProjectHours += kpi.projectCapital6Hours; };
        if (kpi.projectCapital7Hours.HasValue) { ProjectHours += kpi.projectCapital7Hours; };
        if (kpi.projectCapital8Hours.HasValue) { ProjectHours += kpi.projectCapital8Hours; };
        if (kpi.projectCapital9Hours.HasValue) { ProjectHours += kpi.projectCapital9Hours; };
        if (kpi.projectCapital10Hours.HasValue) { ProjectHours += kpi.projectCapital10Hours; };
        if (kpi.projectCapital11Hours.HasValue) { ProjectHours += kpi.projectCapital11Hours; };
        if (kpi.projectCapital12Hours.HasValue) { ProjectHours += kpi.projectCapital12Hours; };
        if (kpi.projectCapital13Hours.HasValue) { ProjectHours += kpi.projectCapital13Hours; };
        if (kpi.projectCapital14Hours.HasValue) { ProjectHours += kpi.projectCapital14Hours; };
        if (kpi.projectCapital15Hours.HasValue) { ProjectHours += kpi.projectCapital15Hours; };
        if (kpi.aheaSmeltingWork.HasValue) { ProjectHours += kpi.aheaSmeltingWork; };
        if (kpi.aheaPowerGenerationWork.HasValue) { ProjectHours += kpi.aheaPowerGenerationWork; };

        if (ProjectHours.HasValue) { AheaTotalManHours = AheaTotalManHours + Convert.ToDecimal(ProjectHours); };
        kpi.aheaTotalManHours = AheaTotalManHours;
        kpi.aheaPeakNopplSiteWeek = Convert.ToInt32(tbPeakNoEmp.Text);
        kpi.aheaAvgNopplSiteMonth = Convert.ToInt32(tbAvgNoEmp.Text);
        kpi.aheaNoEmpExcessMonth = Convert.ToInt32(tbEmployeesExcess.Text);
        //Incident Performance
        //--No Save
        //EHS Performance
        kpi.ehspNoLWD = NullableStringToDecimal(tbehsNoLWD.Text);
        kpi.ehspNoRD = NullableStringToDecimal(tbehspNoRD.Text);
        //Audits
        kpi.ehsCorrective = NullableStringToInt(tbCorrective.Text);
        kpi.JSAAudits = NullableStringToInt(tbJSA.Text);
        //Inspections
        kpi.iWSC = NullableStringToInt(tbiWSC.Text);
        //Observations Number of
        kpi.oNoHSWC = NullableStringToInt(tboNoHSWC.Text);
        kpi.oNoBSP = NullableStringToInt(tboNoBSP.Text);
        //Quality
        //kpi.QQas = NullableStringToDecimal(tbqQAS.Text);
        kpi.qQAS = 0.0m;
        kpi.qNoNCI = NullableStringToInt(tbqNoNCI.Text);
        //Meetings
        kpi.mTbmpm = NullableStringToInt(tbmTbmpm.Text);
        kpi.mAwcm = NullableStringToInt(tbmAwcm.Text);
        kpi.mAmcm = NullableStringToInt(tbmAmcm.Text);
        kpi.mFatality = NullableStringToInt(tbFatality.Text);
        //Mandatory Training
        kpi.Training = NullableStringToInt(tbTraining.Text, 100);
        kpi.mtOthers = tbmtOthers.Text;
        //Fitness for work
        kpi.ffwBloodAlcoholTests = NullableStringToInt(tbBloodAlcoholTests.Text);
        kpi.ffwPositiveBACResults = NullableStringToInt(tbPositiveBACResults.Text);
        kpi.ffwDrugTests = NullableStringToInt(tbDrugTests.Text);
        kpi.ffwNonNegativeResults = NullableStringToInt(tbNonNegativeResults.Text);
        kpi.ffwPositiveResultsMedDeclared = NullableStringToInt(tbPositiveResultsMedDeclared.Text);
        kpi.ffwPositiveResultsMedNotDeclared = NullableStringToInt(tbPositiveResultsMedNotDeclared.Text);

        if (lblSafetyPlanSubmitted.Text == "Yes")
        {
            kpi.SafetyPlansSubmitted = true;
        }
        else
        {
            kpi.SafetyPlansSubmitted = false;
        }

        string kpiState = "";
        if(SessionHandler.KPIState != null) kpiState = SessionHandler.KPIState;

        if ( kpiState == "New")
        {
            //save
            kpi.SiteId = Convert.ToInt32(ddlSites.SelectedItem.Value.ToString());
            kpi.CreatedbyUserId = Convert.ToInt32(auth.UserId.ToString());
            //Response.Write("|" + kpi.CreatedbyUserId.ToString() + "|");
            kpi.DateAdded = DateTime.Now;
            kpi.ModifiedbyUserId = Convert.ToInt32(auth.UserId.ToString());
            kpi.Datemodified = DateTime.Now;
            kpi.CompanyId = Convert.ToInt32(ddlCompanies.SelectedItem.Value);
            kpi.kpiDateTime = new DateTime((int)ddlYear.SelectedItem.Value, (int)ddlMonth.SelectedItem.Value, 1);

            this.KpiService.Insert(kpi);
            lblCode.Visible = true;
            Label1.Visible = false;
            lblCode.Text = "Successfully Added new KPI";
            lblKpiID.Text = kpi.KpiId.ToString();
            KPILoadbyId(kpi.KpiId);
        }

        if ( kpiState == "Update")
        {
            //update
            kpi.KpiId = Convert.ToInt32(lblKpiID.Text);

            var kpi2 = kpiService.GetByKpiId(kpi.KpiId);
            kpi.SiteId = kpi2.SiteId;
            kpi.CreatedbyUserId = kpi2.CreatedbyUserId;
            kpi.DateAdded = kpi2.DateAdded;
            kpi.ModifiedbyUserId = Convert.ToInt32(auth.UserId.ToString());
            kpi.Datemodified = DateTime.Now;
            kpi.CompanyId = kpi2.CompanyId;
            kpi.kpiDateTime = new DateTime((int)ddlYear.SelectedItem.Value, (int)ddlMonth.SelectedItem.Value, 1);
            kpi.IsSystemEdit = false;

            kpi.ipIFE = kpi2.IpIfe;
            kpi.ipFATI = kpi2.IpFati;
            kpi.ipLTI = kpi2.IpLti;
            kpi.ipMTI = kpi2.IpMti;
            kpi.ipRDI = kpi2.IpRdi;
            kpi.EbiOnSite = kpi2.EbiOnSite;
            kpi.ipRN = kpi2.IpRN;

            this.KpiService.Update(kpi);
            KPILoadbyId(kpi.KpiId);
            InitValuesIfNull();
            lblCode.Visible = true;
            Label1.Visible = false;
            lblCode.Text = "Successfully Updated KPI";
            lblCode.Focus();
        }
    }
    protected void newKPI()
    {
        panelKPIinfo.Visible = true;
        panelKPI_Record.Visible = true;
        lbldCreatedBy.Text = "Not Saved Yet.";
        lbldLastModifiedBy.Text = "Not Saved Yet.";
        lblKpiID.Text = "Not Saved Yet.";
        lbldCompanyname.Text = ddlCompanies.SelectedItem.ToString();
        lbldSite.Text = ddlSites.SelectedItem.ToString();
        lbldTime.Text = ddlMonth.SelectedItem.ToString() + " " + ddlYear.SelectedItem.ToString();
        Label1.Visible = true;

        // Safety Plan submitted for year
        int? CompanyId = Convert.ToInt32(ddlCompanies.SelectedItem.Value);
        FileDbFilters query = new FileDbFilters();
        query.Append(FileDbColumn.CompanyId, CompanyId.ToString());
        query.Append(FileDbColumn.ModifiedDate, "%" + ddlYear.SelectedItem.Value.ToString() + "%");

        int count = 0;
        TList<FileDb> list = DataRepository.FileDbProvider.GetPaged(query.ToString(), null, 0, 100, out count);

        if (count > 0)
        {
            lblSafetyPlanSubmitted.Text = "YES";
            lblSafetyPlanSubmitted.ForeColor = Color.Green;
            lblSafetyPlanSubmittedMsg.Visible = false;
        }
        else
        {
            lblSafetyPlanSubmitted.Text = "NO";
            lblSafetyPlanSubmitted.ForeColor = Color.Red;
            lblSafetyPlanSubmittedMsg.Visible = true;
        }

        // Safety Plan approved for year
        FileDbFilters query2 = new FileDbFilters();
        query2.Append(FileDbColumn.CompanyId, CompanyId.ToString());
        query2.Append(FileDbColumn.ModifiedDate, String.Format("%{0}%", DateTime.Now.Year));
        query2.Append(FileDbColumn.StatusId, ((int)SafetyPlanStatusList.Approved).ToString());

        count = 0;
        TList<FileDb> list2 = DataRepository.FileDbProvider.GetPaged(query2.ToString(), null, 0, 100, out count);

        if (count > 0)
        {
            lblSafetyPlanApproved.Text = "YES";
            lblSafetyPlanApproved.ForeColor = Color.Green;
        }
        else
        {
            FileDbFilters query3 = new FileDbFilters();
            query3.Append(FileDbColumn.CompanyId, CompanyId.ToString());
            query3.Append(FileDbColumn.ModifiedDate, String.Format("%{0}%", DateTime.Now.Year));
            query3.Append(FileDbColumn.StatusId, ((int)SafetyPlanStatusList.Being_Assessed).ToString());

            int count2 = 0;
            TList<FileDb> list3 = DataRepository.FileDbProvider.GetPaged(query3.ToString(), null, 0, 100, out count2);

            if (count2 > 0)
            {
                lblSafetyPlanApproved.Text = "Being Assessed";
                lblSafetyPlanApproved.ForeColor = Color.Orange;
            }
            else
            {
                lblSafetyPlanApproved.Text = "NO";
                lblSafetyPlanApproved.ForeColor = Color.Red;
            }
        }

    }

    protected void ddlCompanies_SelectedIndexChanged(object sender, EventArgs e)
    {
        trCategory.Visible = false;
        FillSitesCombo(ddlCompanies.SelectedItem.Value.ToString());
        
        //ddlSites.SelectedIndex = 0;
    }

    

    protected void DisableAllInputBoxs()
    {
        //KPI
        tbGeneral.Enabled = false;
        tbCalcinerExpense.Enabled = false;
        tbCalcinerCapital.Enabled = false;
        tbResidue.Enabled = false;
        //Projects
        int i = 1;
        while (i <= 15)
        {
            ASPxTextBox Hours = (ASPxTextBox)this.FindControl(String.Format("tbProject{0}", i));
            ASPxPageControl Tabs = (ASPxPageControl)this.FindControl(String.Format("pcProject{0}", i));
            ASPxTextBox ProjectNumber = (ASPxTextBox)Tabs.FindControl(String.Format("tbProjectNumber{0}", i));
            ASPxTextBox PoNumber = (ASPxTextBox)Tabs.FindControl(String.Format("tbPoNumber{0}", i));
            ASPxTextBox LineNumber = (ASPxTextBox)Tabs.FindControl(String.Format("tbLineNumber{0}", i));

            Hours.Enabled = false;
            ProjectNumber.Enabled = false;
            PoNumber.Enabled = false; ;
            LineNumber.Enabled = false;

            i++;
        }

        //AHEA Managed Projects (Capital)
        tbRefineryWork.Enabled = false;
        tbResidueWork.Enabled = false;
        tbSmeltingWork.Enabled = false;
        tbPowerGenerationWork.Enabled = false;
        tbPeakNoEmp.Enabled = false;
        tbAvgNoEmp.Enabled = false;
        tbEmployeesExcess.Enabled = false;

        lblTotalManHours.Enabled = false;
        tbPeakNoEmp.Enabled = false;
        tbAvgNoEmp.Enabled = false;
        tbEmployeesExcess.Enabled = false;
        //Incident Performance
        lblipFATI.Enabled = false;
        lblipMTI.Enabled = false;
        lblipRDI.Enabled = false;
        lblipLTI.Enabled = false;
        lblipIFE.Enabled = false;
        lblipRN.Enabled = false;
        //EHS Performance
        tbehsNoLWD.Enabled = false;
        tbehspNoRD.Enabled = false;
        //Audits
        tbCorrective.Enabled = false;
        tbJSA.Enabled = false;
        //Inspections
        tbiWSC.Enabled = false;
        //Observations Number of
        tboNoHSWC.Enabled = false;
        tboNoBSP.Enabled = false;
        //Quality
        lblqQAS.Enabled = false;
        tbqNoNCI.Enabled = false;
        //Meetings
        tbmTbmpm.Enabled = false;
        tbmAwcm.Enabled = false;
        tbmAmcm.Enabled = false;
        tbFatality.Enabled = false;
        //Mandatory Training
        tbTraining.Enabled = false;
        tbmtOthers.Enabled = false;
        //Fitness for work
        tbBloodAlcoholTests.Enabled = false;
        tbPositiveBACResults.Enabled = false;
        tbDrugTests.Enabled = false;
        tbNonNegativeResults.Enabled = false;
        tbPositiveResultsMedDeclared.Enabled = false;
        tbPositiveResultsMedNotDeclared.Enabled = false;
    }



    protected string GetProjectNumber(string Po, string Line)
    {
        string ret = "";

        KpiPurchaseOrderListService kpolService = new KpiPurchaseOrderListService();
        TList<KpiPurchaseOrderList> kpolTlist = kpolService.GetByPurchaseOrderNumberPurchaseOrderLineNumber(Po, Line);
        //Dt 3142
        foreach (KpiPurchaseOrderList kpol in kpolTlist)
        {
            if (kpol.ProjectStatus == "Open") ret = kpol.ProjectNumber;
        }

        return ret;
 
    }


    protected void LoadKpiProject(string Po, ASPxTextBox tbPo, string Line, ASPxTextBox tbLine, string Title, ASPxTextBox tbTitle, ASPxLabel ProjectSelected)
    {
        if (!String.IsNullOrEmpty(Po) && !string.IsNullOrEmpty(Line))
        {
            tbPo.Value = Po;
            tbPo.Text = Po;

            tbLine.Value = Line;
            tbLine.Text = Line;

            KpiPurchaseOrderListService kpolService = new KpiPurchaseOrderListService();
            TList<KpiPurchaseOrderList> kpolList = kpolService.GetByPurchaseOrderNumberPurchaseOrderLineNumber(Po, Line);
            if (kpolList.Count > 0)
            {
                ProjectSelected.Text = kpolList[0].ProjectNumber + " - " + kpolList[0].ProjectDesc;
            }
        }

        if (!String.IsNullOrEmpty(Title))
        {
            tbTitle.Value = Title;
            tbTitle.Text = Title;

            KpiPurchaseOrderListService kpolService = new KpiPurchaseOrderListService();
            TList<KpiPurchaseOrderList> kpolList = kpolService.GetByProjectNumber(Title);
            if (kpolList.Count > 0)
            {
                ProjectSelected.Text = kpolList[0].ProjectNumber + " - " + kpolList[0].ProjectDesc;
            }
            else
            {
                ProjectSelected.Text = Title;
            }
        }
    }



    protected bool Validate()
    {
        bool valid = true;

        int i = 1;
        while (i <= 15)
        {
            ASPxTextBox Hours = (ASPxTextBox)this.FindControl(String.Format("tbProject{0}", i));
            ASPxPageControl Tabs = (ASPxPageControl)this.FindControl(String.Format("pcProject{0}", i));
            ASPxTextBox ProjectNumber = (ASPxTextBox)Tabs.FindControl(String.Format("tbProjectNumber{0}", i));
            ASPxTextBox PoNumber = (ASPxTextBox)Tabs.FindControl(String.Format("tbPoNumber{0}", i));
            ASPxTextBox LineNumber = (ASPxTextBox)Tabs.FindControl(String.Format("tbLineNumber{0}", i));

            ProjectNumber.IsValid = true;
            PoNumber.IsValid = true;
            LineNumber.IsValid = true;

            if (Check(i, Hours, Tabs, ProjectNumber, PoNumber, LineNumber) == false) valid = false;
            i++;
        }

        return valid;
    }

    protected bool Check(int No, ASPxTextBox Hours, ASPxPageControl Tabs, ASPxTextBox ProjectNumber, ASPxTextBox PoNumber, ASPxTextBox LineNumber)
    {
        bool valid = true;

        if (Tabs.ActiveTabIndex == 0) valid = Valid(Hours, ProjectNumber, 1);
        else if (Tabs.ActiveTabIndex == 1) valid = Valid(Hours, PoNumber, LineNumber, 1);
        return valid;
    }
    protected bool Valid(ASPxTextBox Hours, ASPxTextBox ProjectNumber, int No)
    {
        bool valid = false;
        if (!String.IsNullOrEmpty(Hours.Text))
        {
            ProjectNumber.IsValid = true;
            Hours.IsValid = true;
            if (!String.IsNullOrEmpty(ProjectNumber.Text))
            {
                KpiPurchaseOrderListService kpolService = new KpiPurchaseOrderListService();
                TList<KpiPurchaseOrderList> kpolList = kpolService.GetByProjectNumber(ProjectNumber.Text);
                if (kpolList.Count > 0)
                {
                    ProjectNumber.IsValid = true;
                }
                else
                {
                    ProjectNumber.IsValid = false;
                    ProjectNumber.ValidationSettings.ErrorText = String.Format("Project #{0} - Project not found based upon entered Project Number.", No);
                }

                int i = -1;
                Int32.TryParse(Hours.Text, out i);
                if (i >= 0)
                {
                    Hours.IsValid = true;
                }
                else
                {
                    Hours.IsValid = false;
                    Hours.ValidationSettings.ErrorText = String.Format("Project #{0} - Entered Hours must be a number greater than 0.", No);
                }
            }
            else
            {
                ProjectNumber.IsValid = false;
                ProjectNumber.ValidationSettings.ErrorText = String.Format("Project #{0} - Project Number was expected but not entered.", No);
            }
            valid = ProjectNumber.IsValid;
        }
        else
        {
            if (!String.IsNullOrEmpty(ProjectNumber.Text))
            {
                Hours.IsValid = false;
                Hours.ValidationSettings.ErrorText = String.Format("Project #{0} - Hours must be entered if a Project Number is entered.", No);

                valid = Hours.IsValid;
            }
            else
            {
                Hours.IsValid = true;
                valid = Hours.IsValid;
            }
        }

        return valid;
    }
    protected bool Valid(ASPxTextBox Hours, ASPxTextBox PoNumber, ASPxTextBox LineNumber, int No)
    {
        bool valid = false;

        if (!String.IsNullOrEmpty(Hours.Text))
        {
            PoNumber.IsValid = true;
            LineNumber.IsValid = true;

            if (!String.IsNullOrEmpty(PoNumber.Text))
            {
                KpiPurchaseOrderListService kpolService = new KpiPurchaseOrderListService();
                TList<KpiPurchaseOrderList> kpolList = kpolService.GetByPurchaseOrderNumber(PoNumber.Text);
                if (kpolList.Count > 0)
                {
                    PoNumber.IsValid = true;

                    if (!String.IsNullOrEmpty(LineNumber.Text))
                    {
                        kpolList.Clear();
                        kpolList = kpolService.GetByPurchaseOrderNumberPurchaseOrderLineNumber(PoNumber.Text, LineNumber.Text);
                        if (kpolList.Count > 0)
                        {
                            LineNumber.IsValid = true;
                        }
                        else
                        {
                            LineNumber.IsValid = false;
                            LineNumber.ValidationSettings.ErrorText = String.Format("Project #{0} - A valid Purchase Order with Line #{1} was not found.", No, LineNumber.Text);
                        }
                    }
                    else
                    {
                        LineNumber.IsValid = false;
                        LineNumber.ValidationSettings.ErrorText = String.Format("Project #{0} - The Purchase Order Line Number must be entered.", No);
                    }
                }
                else
                {
                    PoNumber.IsValid = false;
                    PoNumber.ValidationSettings.ErrorText = String.Format("Project #{0} - A valid Purchase Order was not found with Purchase Order Number [{1}].", No, PoNumber.Text);
                }

                int i = -1;
                Int32.TryParse(Hours.Text, out i);
                if (i >= 0)
                {
                    Hours.IsValid = true;
                }
                else
                {
                    Hours.IsValid = false;
                    Hours.ValidationSettings.ErrorText = String.Format("Project #{0} - Entered Hours must be a number greater than 0.", No);
                }
            }
            else
            {
                PoNumber.IsValid = false;
                PoNumber.ValidationSettings.ErrorText = String.Format("Project #{0} - A Purchase Order Number (and corressponding Line Number) must be entered.", No, PoNumber.Text);
            }

            if (PoNumber.IsValid && LineNumber.IsValid) valid = true;
        }
        else
        {
            if (!String.IsNullOrEmpty(PoNumber.Text) || !String.IsNullOrEmpty(LineNumber.Text))
            {
                Hours.IsValid = false;
                Hours.ValidationSettings.ErrorText = String.Format("Project #{0} - Hours must be entered if a Purchase Order Number & Line Number is entered.", No);

                valid = Hours.IsValid;
            }
            else
            {
                Hours.IsValid = true;
                valid = Hours.IsValid;
            }
        }

        return valid;
    }

}
