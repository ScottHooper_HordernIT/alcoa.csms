﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/popup.master" AutoEventWireup="true" Inherits="PopUps_SafetyPQ_ProcessOverview" Codebehind="SafetyPQ_ProcessOverview.aspx.cs" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <script type="text/javascript" language="javascript">  
         function doClose(e) // note: takes the event as an arg (IE doesn't)  
         {  
             if (!e) e = window.event; // fix IE  

             if (e.keyCode) // IE  
             {  
                 if (e.keyCode == "27") window.close();  
             }  
             else if (e.charCode) // Netscape/Firefox/Opera  
             {  
                 if (e.keyCode == "27") window.close();  
             } 
         }
         document.onkeydown = doClose;  
 </script> 
    <table border="0" cellpadding="2" cellspacing="0" width="100%">
    <tr>
        <td class="pageName" colspan="3" style="height: 17px">
        <span class="bodycopy"><span class="title">Safety Pre-Qualification</span><br />
        <span class="date">Questionnaire > Process Overview</span><br />
            <img src="images/grfc_dottedline.gif" width="24" height="1">&nbsp;</span>
                </td>
    </tr>
</table>
<span style="color: maroon"> </span>
<p class="MsoNormal" style="margin: 0cm 0cm 0pt 35.45pt; text-align: justify">
    <b style="mso-bidi-font-weight: normal"><span style="font-family: Verdana"><span
        style="font-size: 10pt"><span style="color: #000000"></span><span style="color: #000000"><?xml namespace=""
                ns="urn:schemas-microsoft-com:office:office" prefix="o" ?></span></span></span></b></p>
    <p style="text-align: center">
        <dx:ASPxButton ID="ASPxButton1" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
            CssPostfix="Office2003Blue" PostBackUrl="javascript:window.close();" 
            Text="Close (ESC)" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
        </dx:ASPxButton>
        &nbsp;</p>
    <dx:ASPxPanel ID="pnlContractor" runat="server">
        <PanelCollection>
            <dx:PanelContent runat="server">
        <table border="0" cellpadding="0" cellspacing="0" style="padding-right: 20px; padding-left: 20px"
            width="100%">
            <tr>
                <td style="width: 100%; height: 13px">
                    <ul>
                    <li class="MsoPlainText" style="margin: 0cm 0cm 10pt 0pt; text-align: justify"><span
                        style="font-size: 10pt"><span>You have been identified as someone from a company that
                            Alcoa may want to enter into a contract with to supply services on one or more of
                            our locations.</span> </span></li>
                        <li class="MsoPlainText" style="margin: 0cm 0cm 10pt 0pt; text-align: justify"><span
                            style="font-size: 10pt"><span>You have been given instructions about how to logon and
                            access this web portal.</span> </span></li>
                        <li class="MsoPlainText" style="margin: 0cm 0cm 10pt 0pt; text-align: justify"><span
                            style="font-size: 10pt"><span>Our Procurement department has completed an initial questionnaire
                            that asks some basic questions about the service your company provides, completed
                            an Initial Risk Assessment and answered questions which help determine whether or
                            not your company is required to answer a Supplier Questionnaire about your company
                            work practices and safety.</span> </span></li>
                        <li class="MsoPlainText" style="margin: 0cm 0cm 10pt 0pt; text-align: justify"><span
                            style="font-size: 10pt"><span>By clicking on the ‘Online Questionnaire’ button 
                            on the previous page you will be able to complete your Supplier Questionnaire. 
                            Help is built into the supplier (and verification) questionnaire. Please see the 
                            &#39;Why&#39; and &#39;How&#39; links which appear above each question in the questionnaire 
                            page.</span></span></li>
                        <li class="MsoPlainText" style="margin: 0cm 0cm 10pt 0pt; text-align: justify"><span
                            style="font-size: 10pt"><span>Depending on your answers to the Supplier Questionnaire
                            you may also be required to complete a Verification Questionnaire which asks more
                            specific questions about the potentially higher risk aspects of the service(s) you
                            provide.</span> </span></li>
                        <li class="MsoPlainText" style="margin: 0cm 0cm 10pt 0pt; text-align: justify"><span
                            style="font-size: 10pt"><span>At the end of the questionnaire process you will be required
                            to Submit your answers to be assessed by one of our Safety Assessors. This person
                            may or may not ask you for further information in order to
        clarify one or more of
                            your answers or information supplied.</span> </span></li>
                        <li class="MsoPlainText" style="margin: 0cm 0cm 0pt; text-align: justify"><span style="font-size: 10pt">
                            After the Safety Assessment is complete our Procurement department will contact you
                            about the outcome of the Safety
        Pre-qualification process.
                        </span></li>
                    </ul>
                </td>
            </tr>
        </table>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxPanel>
    <dx:ASPxPanel ID="pnlSubContractor" runat="server" Visible="False">
        <PanelCollection>
            <dx:PanelContent runat="server">
                <table border="0" cellpadding="0" cellspacing="0" style="padding-right: 20px; padding-left: 20px"
            width="100%">
                    <tr>
                        <td style="width: 100%; height: 13px">
                            <ul>
                                <li class="MsoPlainText" style="margin: 0cm 0cm 10pt 0pt; text-align: justify"><span
                        style="font-size: 10pt"><span>You have been identified as someone from a company that
                                        supplies services on one or more of our locations on a subcontract basis to a direct
                                        contractor to Alcoa.</span></span></li>
                                <li class="MsoPlainText" style="margin: 0cm 0cm 10pt 0pt; text-align: justify"><span
                                    style="font-size: 10pt"><span></span></span><span
                            style="font-size: 10pt"><span>You
                                        have been given instructions about how to logon and access this web portal.</span>
                                    </span></li>
                                <li class="MsoPlainText" style="margin: 0cm 0cm 10pt 0pt; text-align: justify"><span
                            style="font-size: 10pt"><span>In accordance with Alcoa Corporate Policy your company
                                        is required to answer a Supplier Questionnaire about your company work practices
                                        and safety.</span></span></li>
                                <li class="MsoPlainText" style="margin: 0cm 0cm 10pt 0pt; text-align: justify"><span
                                    style="font-size: 10pt"><span></span></span><span
                            style="font-size: 10pt"><span>By clicking
                                        on the ‘Online Questionnaire’ button on the previous page you will be able to complete
                                        your Supplier Questionnaire.</span></span></li>
                                <li class="MsoPlainText" style="margin: 0cm 0cm 10pt 0pt; text-align: justify"><span
                                    style="font-size: 10pt"><span></span></span><span
                            style="font-size: 10pt"><span>NB.
                                        Please read the Quick Reference Guide to assist you in completing this online questionnaire.</span></span></li>
                                <li class="MsoPlainText" style="margin: 0cm 0cm 10pt 0pt; text-align: justify"><span
                                    style="font-size: 10pt"><span></span></span><span
                            style="font-size: 10pt"><span>At the
                                        end of the questionnaire process you will be required to Submit your answers to
                                        be reviewed by one of our Safety Assessors. This person may or may not ask you for
                                        further information in order to clarify one or more of your answers or information
                                        supplied.</span></span></li>
                            </ul>
                        </td>
                    </tr>
                </table>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxPanel>
</asp:Content>

