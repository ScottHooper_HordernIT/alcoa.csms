﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using KaiZen.CSMS.Services;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.Library;
using System.Drawing;

using System.Text;

using System.Net.Mail;

using System.IO;

using DevExpress.Web.ASPxEditors;

namespace ALCOA.CSMS.Website.UserControls.Admin
{
    public partial class AdminTaskClose : System.Web.UI.UserControl
    {
        Auth auth = new Auth();

        protected void Page_Load(object sender, EventArgs e) { Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); }

        private void moduleInit(bool postBack)
        {
            if (!postBack)
            {
                if (auth.RoleId != (int)RoleList.Administrator) Response.Redirect("default.aspx");

                Prefill();
            }
        }

        protected void Prefill()
        {
            int AdminTaskId = 0;

            Int32.TryParse(Request.QueryString["q"].ToString(), out AdminTaskId);

            lblTaskNo.Text = "(NEW)";

            if (AdminTaskId > 0)
            {
                AdminTaskService atService = new AdminTaskService();
                KaiZen.CSMS.Entities.AdminTask at = atService.GetByAdminTaskId(AdminTaskId);

                lblTaskNo.Text = at.AdminTaskId.ToString();

                //TODO: Load DateOpened, OpenedByUserId

                cbAccess.Value = at.CsmsAccessId;
                cbCompany.Value = at.CompanyId;
                cbSource.Value = at.AdminTaskSourceId;
                cbType.Value = at.AdminTaskTypeId;

                tbLogin.Text = at.Login;
                tbEmail.Text = at.EmailAddress;
                tbFirstName.Text = at.FirstName;
                tbLastName.Text = at.LastName;
                tbJobTitle.Text = at.JobTitle;
                tbMobileNumber.Text = at.MobileNo;
                tbTelephoneNo.Text = at.TelephoneNo;
                tbFaxNo.Text = at.FaxNo;

            }
            else
            {
                btnCloseTask.Enabled = false;
                throw new Exception("Invalid Admin Task");
            }
        }

        protected void btnCloseTask_Click(object sender, EventArgs e)
        {
            try
            {
                int AdminTaskId = 0;
                Int32.TryParse(Request.QueryString["q"].ToString(), out AdminTaskId);

                AdminTaskService atService = new AdminTaskService();
                KaiZen.CSMS.Entities.AdminTask at = atService.GetByAdminTaskId(AdminTaskId);

                at.AdminTaskComments = mTaskComments.Text;
                at.AdminTaskStatusId = (int)AdminTaskStatusList.Closed;
                at.ClosedByUserId = auth.UserId;
                at.DateClosed = DateTime.Now;
                atService.Save(at);
                Redirect("Task Closed Successfully.", "AdminTasks.aspx");
            }
            catch (Exception ex)
            {
                throw new Exception("Error Occured: " + ex.Message);
            }
        }

        public void Redirect(string l_msg, string l_sURL)
        {
            ScriptManager l_oSM = ScriptManager.GetCurrent(Page);
            if (l_oSM != null)
            {
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "wsReturnToPreviousPage", String.Format("alert('{0}');window.location.href='{1}';", l_msg, l_sURL), true);
            }
            else
            {
                Page.ClientScript.RegisterStartupScript(Page.GetType(), "wsReturnToPreviousPage", String.Format("alert('{0}');window.location.href='{1}';", l_msg, l_sURL), true);
            }
        }
        public void Redirect(string l_sURL)
        {
            ScriptManager l_oSM = ScriptManager.GetCurrent(Page);
            if (l_oSM != null)
            {
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "wsReturnToPreviousPage", String.Format("window.location.href='{0}';", l_sURL), true);
            }
            else
            {
                Page.ClientScript.RegisterStartupScript(Page.GetType(), "wsReturnToPreviousPage", String.Format("window.location.href='{0}';", l_sURL), true);
            }
        }
    }
}