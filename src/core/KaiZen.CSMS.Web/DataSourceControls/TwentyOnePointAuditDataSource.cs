﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.TwentyOnePointAuditProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(TwentyOnePointAuditDataSourceDesigner))]
	public class TwentyOnePointAuditDataSource : ProviderDataSource<TwentyOnePointAudit, TwentyOnePointAuditKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAuditDataSource class.
		/// </summary>
		public TwentyOnePointAuditDataSource() : base(new TwentyOnePointAuditService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the TwentyOnePointAuditDataSourceView used by the TwentyOnePointAuditDataSource.
		/// </summary>
		protected TwentyOnePointAuditDataSourceView TwentyOnePointAuditView
		{
			get { return ( View as TwentyOnePointAuditDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the TwentyOnePointAuditDataSource control invokes to retrieve data.
		/// </summary>
		public TwentyOnePointAuditSelectMethod SelectMethod
		{
			get
			{
				TwentyOnePointAuditSelectMethod selectMethod = TwentyOnePointAuditSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (TwentyOnePointAuditSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the TwentyOnePointAuditDataSourceView class that is to be
		/// used by the TwentyOnePointAuditDataSource.
		/// </summary>
		/// <returns>An instance of the TwentyOnePointAuditDataSourceView class.</returns>
		protected override BaseDataSourceView<TwentyOnePointAudit, TwentyOnePointAuditKey> GetNewDataSourceView()
		{
			return new TwentyOnePointAuditDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the TwentyOnePointAuditDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class TwentyOnePointAuditDataSourceView : ProviderDataSourceView<TwentyOnePointAudit, TwentyOnePointAuditKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAuditDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the TwentyOnePointAuditDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public TwentyOnePointAuditDataSourceView(TwentyOnePointAuditDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal TwentyOnePointAuditDataSource TwentyOnePointAuditOwner
		{
			get { return Owner as TwentyOnePointAuditDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal TwentyOnePointAuditSelectMethod SelectMethod
		{
			get { return TwentyOnePointAuditOwner.SelectMethod; }
			set { TwentyOnePointAuditOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal TwentyOnePointAuditService TwentyOnePointAuditProvider
		{
			get { return Provider as TwentyOnePointAuditService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<TwentyOnePointAudit> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<TwentyOnePointAudit> results = null;
			TwentyOnePointAudit item;
			count = 0;
			
			System.Int32 _id;
			System.Int32 _companyId;
			System.Int32 _siteId;
			System.Int32 _qtrId;
			System.Int64 _year;
			System.Int32? _point01Id_nullable;
			System.Int32? _point02Id_nullable;
			System.Int32? _point03Id_nullable;
			System.Int32? _point04Id_nullable;
			System.Int32? _point05Id_nullable;
			System.Int32? _point06Id_nullable;
			System.Int32? _point07Id_nullable;
			System.Int32? _point08Id_nullable;
			System.Int32? _point09Id_nullable;
			System.Int32? _point10Id_nullable;
			System.Int32? _point11Id_nullable;
			System.Int32? _point12Id_nullable;
			System.Int32? _point13Id_nullable;
			System.Int32? _point14Id_nullable;
			System.Int32? _point15Id_nullable;
			System.Int32? _point16Id_nullable;
			System.Int32? _point17Id_nullable;
			System.Int32? _point18Id_nullable;
			System.Int32? _point19Id_nullable;
			System.Int32? _point20Id_nullable;
			System.Int32? _point21Id_nullable;
			System.Int32 _createdbyUserId;
			System.Int32 _modifiedbyUserId;

			switch ( SelectMethod )
			{
				case TwentyOnePointAuditSelectMethod.Get:
					TwentyOnePointAuditKey entityKey  = new TwentyOnePointAuditKey();
					entityKey.Load(values);
					item = TwentyOnePointAuditProvider.Get(entityKey);
					results = new TList<TwentyOnePointAudit>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case TwentyOnePointAuditSelectMethod.GetAll:
                    results = TwentyOnePointAuditProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case TwentyOnePointAuditSelectMethod.GetPaged:
					results = TwentyOnePointAuditProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case TwentyOnePointAuditSelectMethod.Find:
					if ( FilterParameters != null )
						results = TwentyOnePointAuditProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = TwentyOnePointAuditProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case TwentyOnePointAuditSelectMethod.GetById:
					_id = ( values["Id"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["Id"], typeof(System.Int32)) : (int)0;
					item = TwentyOnePointAuditProvider.GetById(_id);
					results = new TList<TwentyOnePointAudit>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				case TwentyOnePointAuditSelectMethod.GetByCompanyIdSiteIdQtrIdYear:
					_companyId = ( values["CompanyId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["CompanyId"], typeof(System.Int32)) : (int)0;
					_siteId = ( values["SiteId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["SiteId"], typeof(System.Int32)) : (int)0;
					_qtrId = ( values["QtrId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["QtrId"], typeof(System.Int32)) : (int)0;
					_year = ( values["Year"] != null ) ? (System.Int64) EntityUtil.ChangeType(values["Year"], typeof(System.Int64)) : (long)0;
					item = TwentyOnePointAuditProvider.GetByCompanyIdSiteIdQtrIdYear(_companyId, _siteId, _qtrId, _year);
					results = new TList<TwentyOnePointAudit>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// FK
				case TwentyOnePointAuditSelectMethod.GetBySiteId:
					_siteId = ( values["SiteId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["SiteId"], typeof(System.Int32)) : (int)0;
					results = TwentyOnePointAuditProvider.GetBySiteId(_siteId, this.StartIndex, this.PageSize, out count);
					break;
				case TwentyOnePointAuditSelectMethod.GetByPoint01Id:
					_point01Id_nullable = (System.Int32?) EntityUtil.ChangeType(values["Point01Id"], typeof(System.Int32?));
					results = TwentyOnePointAuditProvider.GetByPoint01Id(_point01Id_nullable, this.StartIndex, this.PageSize, out count);
					break;
				case TwentyOnePointAuditSelectMethod.GetByPoint02Id:
					_point02Id_nullable = (System.Int32?) EntityUtil.ChangeType(values["Point02Id"], typeof(System.Int32?));
					results = TwentyOnePointAuditProvider.GetByPoint02Id(_point02Id_nullable, this.StartIndex, this.PageSize, out count);
					break;
				case TwentyOnePointAuditSelectMethod.GetByPoint03Id:
					_point03Id_nullable = (System.Int32?) EntityUtil.ChangeType(values["Point03Id"], typeof(System.Int32?));
					results = TwentyOnePointAuditProvider.GetByPoint03Id(_point03Id_nullable, this.StartIndex, this.PageSize, out count);
					break;
				case TwentyOnePointAuditSelectMethod.GetByPoint04Id:
					_point04Id_nullable = (System.Int32?) EntityUtil.ChangeType(values["Point04Id"], typeof(System.Int32?));
					results = TwentyOnePointAuditProvider.GetByPoint04Id(_point04Id_nullable, this.StartIndex, this.PageSize, out count);
					break;
				case TwentyOnePointAuditSelectMethod.GetByPoint05Id:
					_point05Id_nullable = (System.Int32?) EntityUtil.ChangeType(values["Point05Id"], typeof(System.Int32?));
					results = TwentyOnePointAuditProvider.GetByPoint05Id(_point05Id_nullable, this.StartIndex, this.PageSize, out count);
					break;
				case TwentyOnePointAuditSelectMethod.GetByPoint06Id:
					_point06Id_nullable = (System.Int32?) EntityUtil.ChangeType(values["Point06Id"], typeof(System.Int32?));
					results = TwentyOnePointAuditProvider.GetByPoint06Id(_point06Id_nullable, this.StartIndex, this.PageSize, out count);
					break;
				case TwentyOnePointAuditSelectMethod.GetByPoint07Id:
					_point07Id_nullable = (System.Int32?) EntityUtil.ChangeType(values["Point07Id"], typeof(System.Int32?));
					results = TwentyOnePointAuditProvider.GetByPoint07Id(_point07Id_nullable, this.StartIndex, this.PageSize, out count);
					break;
				case TwentyOnePointAuditSelectMethod.GetByPoint08Id:
					_point08Id_nullable = (System.Int32?) EntityUtil.ChangeType(values["Point08Id"], typeof(System.Int32?));
					results = TwentyOnePointAuditProvider.GetByPoint08Id(_point08Id_nullable, this.StartIndex, this.PageSize, out count);
					break;
				case TwentyOnePointAuditSelectMethod.GetByPoint09Id:
					_point09Id_nullable = (System.Int32?) EntityUtil.ChangeType(values["Point09Id"], typeof(System.Int32?));
					results = TwentyOnePointAuditProvider.GetByPoint09Id(_point09Id_nullable, this.StartIndex, this.PageSize, out count);
					break;
				case TwentyOnePointAuditSelectMethod.GetByPoint10Id:
					_point10Id_nullable = (System.Int32?) EntityUtil.ChangeType(values["Point10Id"], typeof(System.Int32?));
					results = TwentyOnePointAuditProvider.GetByPoint10Id(_point10Id_nullable, this.StartIndex, this.PageSize, out count);
					break;
				case TwentyOnePointAuditSelectMethod.GetByPoint11Id:
					_point11Id_nullable = (System.Int32?) EntityUtil.ChangeType(values["Point11Id"], typeof(System.Int32?));
					results = TwentyOnePointAuditProvider.GetByPoint11Id(_point11Id_nullable, this.StartIndex, this.PageSize, out count);
					break;
				case TwentyOnePointAuditSelectMethod.GetByPoint12Id:
					_point12Id_nullable = (System.Int32?) EntityUtil.ChangeType(values["Point12Id"], typeof(System.Int32?));
					results = TwentyOnePointAuditProvider.GetByPoint12Id(_point12Id_nullable, this.StartIndex, this.PageSize, out count);
					break;
				case TwentyOnePointAuditSelectMethod.GetByPoint13Id:
					_point13Id_nullable = (System.Int32?) EntityUtil.ChangeType(values["Point13Id"], typeof(System.Int32?));
					results = TwentyOnePointAuditProvider.GetByPoint13Id(_point13Id_nullable, this.StartIndex, this.PageSize, out count);
					break;
				case TwentyOnePointAuditSelectMethod.GetByPoint14Id:
					_point14Id_nullable = (System.Int32?) EntityUtil.ChangeType(values["Point14Id"], typeof(System.Int32?));
					results = TwentyOnePointAuditProvider.GetByPoint14Id(_point14Id_nullable, this.StartIndex, this.PageSize, out count);
					break;
				case TwentyOnePointAuditSelectMethod.GetByPoint15Id:
					_point15Id_nullable = (System.Int32?) EntityUtil.ChangeType(values["Point15Id"], typeof(System.Int32?));
					results = TwentyOnePointAuditProvider.GetByPoint15Id(_point15Id_nullable, this.StartIndex, this.PageSize, out count);
					break;
				case TwentyOnePointAuditSelectMethod.GetByPoint16Id:
					_point16Id_nullable = (System.Int32?) EntityUtil.ChangeType(values["Point16Id"], typeof(System.Int32?));
					results = TwentyOnePointAuditProvider.GetByPoint16Id(_point16Id_nullable, this.StartIndex, this.PageSize, out count);
					break;
				case TwentyOnePointAuditSelectMethod.GetByPoint17Id:
					_point17Id_nullable = (System.Int32?) EntityUtil.ChangeType(values["Point17Id"], typeof(System.Int32?));
					results = TwentyOnePointAuditProvider.GetByPoint17Id(_point17Id_nullable, this.StartIndex, this.PageSize, out count);
					break;
				case TwentyOnePointAuditSelectMethod.GetByPoint18Id:
					_point18Id_nullable = (System.Int32?) EntityUtil.ChangeType(values["Point18Id"], typeof(System.Int32?));
					results = TwentyOnePointAuditProvider.GetByPoint18Id(_point18Id_nullable, this.StartIndex, this.PageSize, out count);
					break;
				case TwentyOnePointAuditSelectMethod.GetByPoint19Id:
					_point19Id_nullable = (System.Int32?) EntityUtil.ChangeType(values["Point19Id"], typeof(System.Int32?));
					results = TwentyOnePointAuditProvider.GetByPoint19Id(_point19Id_nullable, this.StartIndex, this.PageSize, out count);
					break;
				case TwentyOnePointAuditSelectMethod.GetByPoint20Id:
					_point20Id_nullable = (System.Int32?) EntityUtil.ChangeType(values["Point20Id"], typeof(System.Int32?));
					results = TwentyOnePointAuditProvider.GetByPoint20Id(_point20Id_nullable, this.StartIndex, this.PageSize, out count);
					break;
				case TwentyOnePointAuditSelectMethod.GetByPoint21Id:
					_point21Id_nullable = (System.Int32?) EntityUtil.ChangeType(values["Point21Id"], typeof(System.Int32?));
					results = TwentyOnePointAuditProvider.GetByPoint21Id(_point21Id_nullable, this.StartIndex, this.PageSize, out count);
					break;
				case TwentyOnePointAuditSelectMethod.GetByCreatedbyUserId:
					_createdbyUserId = ( values["CreatedbyUserId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["CreatedbyUserId"], typeof(System.Int32)) : (int)0;
					results = TwentyOnePointAuditProvider.GetByCreatedbyUserId(_createdbyUserId, this.StartIndex, this.PageSize, out count);
					break;
				case TwentyOnePointAuditSelectMethod.GetByCompanyId:
					_companyId = ( values["CompanyId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["CompanyId"], typeof(System.Int32)) : (int)0;
					results = TwentyOnePointAuditProvider.GetByCompanyId(_companyId, this.StartIndex, this.PageSize, out count);
					break;
				case TwentyOnePointAuditSelectMethod.GetByModifiedbyUserId:
					_modifiedbyUserId = ( values["ModifiedbyUserId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["ModifiedbyUserId"], typeof(System.Int32)) : (int)0;
					results = TwentyOnePointAuditProvider.GetByModifiedbyUserId(_modifiedbyUserId, this.StartIndex, this.PageSize, out count);
					break;
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == TwentyOnePointAuditSelectMethod.Get || SelectMethod == TwentyOnePointAuditSelectMethod.GetById )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				TwentyOnePointAudit entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					TwentyOnePointAuditProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<TwentyOnePointAudit> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			TwentyOnePointAuditProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region TwentyOnePointAuditDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the TwentyOnePointAuditDataSource class.
	/// </summary>
	public class TwentyOnePointAuditDataSourceDesigner : ProviderDataSourceDesigner<TwentyOnePointAudit, TwentyOnePointAuditKey>
	{
		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAuditDataSourceDesigner class.
		/// </summary>
		public TwentyOnePointAuditDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public TwentyOnePointAuditSelectMethod SelectMethod
		{
			get { return ((TwentyOnePointAuditDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new TwentyOnePointAuditDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region TwentyOnePointAuditDataSourceActionList

	/// <summary>
	/// Supports the TwentyOnePointAuditDataSourceDesigner class.
	/// </summary>
	internal class TwentyOnePointAuditDataSourceActionList : DesignerActionList
	{
		private TwentyOnePointAuditDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAuditDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public TwentyOnePointAuditDataSourceActionList(TwentyOnePointAuditDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public TwentyOnePointAuditSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion TwentyOnePointAuditDataSourceActionList
	
	#endregion TwentyOnePointAuditDataSourceDesigner
	
	#region TwentyOnePointAuditSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the TwentyOnePointAuditDataSource.SelectMethod property.
	/// </summary>
	public enum TwentyOnePointAuditSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetById method.
		/// </summary>
		GetById,
		/// <summary>
		/// Represents the GetByCompanyIdSiteIdQtrIdYear method.
		/// </summary>
		GetByCompanyIdSiteIdQtrIdYear,
		/// <summary>
		/// Represents the GetBySiteId method.
		/// </summary>
		GetBySiteId,
		/// <summary>
		/// Represents the GetByPoint01Id method.
		/// </summary>
		GetByPoint01Id,
		/// <summary>
		/// Represents the GetByPoint02Id method.
		/// </summary>
		GetByPoint02Id,
		/// <summary>
		/// Represents the GetByPoint03Id method.
		/// </summary>
		GetByPoint03Id,
		/// <summary>
		/// Represents the GetByPoint04Id method.
		/// </summary>
		GetByPoint04Id,
		/// <summary>
		/// Represents the GetByPoint05Id method.
		/// </summary>
		GetByPoint05Id,
		/// <summary>
		/// Represents the GetByPoint06Id method.
		/// </summary>
		GetByPoint06Id,
		/// <summary>
		/// Represents the GetByPoint07Id method.
		/// </summary>
		GetByPoint07Id,
		/// <summary>
		/// Represents the GetByPoint08Id method.
		/// </summary>
		GetByPoint08Id,
		/// <summary>
		/// Represents the GetByPoint09Id method.
		/// </summary>
		GetByPoint09Id,
		/// <summary>
		/// Represents the GetByPoint10Id method.
		/// </summary>
		GetByPoint10Id,
		/// <summary>
		/// Represents the GetByPoint11Id method.
		/// </summary>
		GetByPoint11Id,
		/// <summary>
		/// Represents the GetByPoint12Id method.
		/// </summary>
		GetByPoint12Id,
		/// <summary>
		/// Represents the GetByPoint13Id method.
		/// </summary>
		GetByPoint13Id,
		/// <summary>
		/// Represents the GetByPoint14Id method.
		/// </summary>
		GetByPoint14Id,
		/// <summary>
		/// Represents the GetByPoint15Id method.
		/// </summary>
		GetByPoint15Id,
		/// <summary>
		/// Represents the GetByPoint16Id method.
		/// </summary>
		GetByPoint16Id,
		/// <summary>
		/// Represents the GetByPoint17Id method.
		/// </summary>
		GetByPoint17Id,
		/// <summary>
		/// Represents the GetByPoint18Id method.
		/// </summary>
		GetByPoint18Id,
		/// <summary>
		/// Represents the GetByPoint19Id method.
		/// </summary>
		GetByPoint19Id,
		/// <summary>
		/// Represents the GetByPoint20Id method.
		/// </summary>
		GetByPoint20Id,
		/// <summary>
		/// Represents the GetByPoint21Id method.
		/// </summary>
		GetByPoint21Id,
		/// <summary>
		/// Represents the GetByCreatedbyUserId method.
		/// </summary>
		GetByCreatedbyUserId,
		/// <summary>
		/// Represents the GetByCompanyId method.
		/// </summary>
		GetByCompanyId,
		/// <summary>
		/// Represents the GetByModifiedbyUserId method.
		/// </summary>
		GetByModifiedbyUserId
	}
	
	#endregion TwentyOnePointAuditSelectMethod

	#region TwentyOnePointAuditFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAuditFilter : SqlFilter<TwentyOnePointAuditColumn>
	{
	}
	
	#endregion TwentyOnePointAuditFilter

	#region TwentyOnePointAuditExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAuditExpressionBuilder : SqlExpressionBuilder<TwentyOnePointAuditColumn>
	{
	}
	
	#endregion TwentyOnePointAuditExpressionBuilder	

	#region TwentyOnePointAuditProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;TwentyOnePointAuditChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAuditProperty : ChildEntityProperty<TwentyOnePointAuditChildEntityTypes>
	{
	}
	
	#endregion TwentyOnePointAuditProperty
}

