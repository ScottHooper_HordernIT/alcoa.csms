﻿using System;

namespace ALCOA.CSMS.Website
{
    public partial class ContractorDataManagementDetail : System.Web.UI.Page
    {
        Auth auth = new Auth();

        protected void Page_Load(object sender, EventArgs e)
        {
            Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack);
        }

        private void moduleInit(bool postBack)
        {
            if (!postBack)
            { //first time load
                if (auth.isAlcoaDirect)
                {
                    Response.Redirect("AccessDenied.aspx?error=" + Server.UrlEncode("Unrecognised User."));
                }
            }
        }
    }
}