﻿using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface ILabourClassTypeService
    {
        LabourClassType Get(Expression<Func<LabourClassType, bool>> where, List<Expression<Func<LabourClassType, object>>> includes);
        List<LabourClassType> GetMany(int? top, Expression<Func<LabourClassType, bool>> where, Func<IQueryable<LabourClassType>, IOrderedQueryable<LabourClassType>> orderBy, List<Expression<Func<LabourClassType, object>>> includes);
        List<LabourClassType> GetManyPaged(int pageNumber, int pageSize, Expression<Func<LabourClassType, bool>> where, Func<IQueryable<LabourClassType>, IOrderedQueryable<LabourClassType>> orderBy, List<Expression<Func<LabourClassType, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<LabourClassType, bool>> where, List<Expression<Func<LabourClassType, object>>> includes);
        int? GetMax(Expression<Func<LabourClassType, bool>> where, Expression<Func<LabourClassType, int>> selector, List<Expression<Func<LabourClassType, object>>> includes);
        void Insert(LabourClassType item, bool saveChanges = true);
        void Update(LabourClassType item, bool saveChanges = true);
        void Delete(LabourClassType item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class LabourClassTypeService : ILabourClassTypeService
    {
        private readonly IRepository<LabourClassType> _repository;

        public LabourClassTypeService(IRepository<LabourClassType> repository)
        {
            this._repository = repository;
        }

        public LabourClassType Get(Expression<Func<LabourClassType, bool>> where, List<Expression<Func<LabourClassType, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<LabourClassType> GetMany(int? top, Expression<Func<LabourClassType, bool>> where, Func<IQueryable<LabourClassType>, IOrderedQueryable<LabourClassType>> orderBy, List<Expression<Func<LabourClassType, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<LabourClassType> GetManyPaged(int pageNumber, int pageSize, Expression<Func<LabourClassType, bool>> where, Func<IQueryable<LabourClassType>, IOrderedQueryable<LabourClassType>> orderBy, List<Expression<Func<LabourClassType, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<LabourClassType, bool>> where, List<Expression<Func<LabourClassType, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public int? GetMax(Expression<Func<LabourClassType, bool>> where, Expression<Func<LabourClassType, int>> selector, List<Expression<Func<LabourClassType, object>>> includes)
        {
            return _repository.GetMax(where, selector, includes);
        }

        public void Insert(LabourClassType item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("LabourClassType");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(LabourClassType item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("LabourClassType");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(LabourClassType item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("LabourClassType");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
