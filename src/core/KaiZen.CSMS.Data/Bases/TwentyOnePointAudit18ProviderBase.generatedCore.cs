﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="TwentyOnePointAudit18ProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class TwentyOnePointAudit18ProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.TwentyOnePointAudit18, KaiZen.CSMS.Entities.TwentyOnePointAudit18Key>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.TwentyOnePointAudit18Key key)
		{
			return Delete(transactionManager, key.Id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _id)
		{
			return Delete(null, _id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _id);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.TwentyOnePointAudit18 Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.TwentyOnePointAudit18Key key, int start, int pageLength)
		{
			return GetById(transactionManager, key.Id, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_TwentyOnePointAudit_18 index.
		/// </summary>
		/// <param name="_id"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.TwentyOnePointAudit18"/> class.</returns>
		public KaiZen.CSMS.Entities.TwentyOnePointAudit18 GetById(System.Int32 _id)
		{
			int count = -1;
			return GetById(null,_id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_TwentyOnePointAudit_18 index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.TwentyOnePointAudit18"/> class.</returns>
		public KaiZen.CSMS.Entities.TwentyOnePointAudit18 GetById(System.Int32 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(null, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_TwentyOnePointAudit_18 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.TwentyOnePointAudit18"/> class.</returns>
		public KaiZen.CSMS.Entities.TwentyOnePointAudit18 GetById(TransactionManager transactionManager, System.Int32 _id)
		{
			int count = -1;
			return GetById(transactionManager, _id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_TwentyOnePointAudit_18 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.TwentyOnePointAudit18"/> class.</returns>
		public KaiZen.CSMS.Entities.TwentyOnePointAudit18 GetById(TransactionManager transactionManager, System.Int32 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(transactionManager, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_TwentyOnePointAudit_18 index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.TwentyOnePointAudit18"/> class.</returns>
		public KaiZen.CSMS.Entities.TwentyOnePointAudit18 GetById(System.Int32 _id, int start, int pageLength, out int count)
		{
			return GetById(null, _id, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_TwentyOnePointAudit_18 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.TwentyOnePointAudit18"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.TwentyOnePointAudit18 GetById(TransactionManager transactionManager, System.Int32 _id, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;TwentyOnePointAudit18&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;TwentyOnePointAudit18&gt;"/></returns>
		public static TList<TwentyOnePointAudit18> Fill(IDataReader reader, TList<TwentyOnePointAudit18> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.TwentyOnePointAudit18 c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("TwentyOnePointAudit18")
					.Append("|").Append((System.Int32)reader[((int)TwentyOnePointAudit18Column.Id - 1)]).ToString();
					c = EntityManager.LocateOrCreate<TwentyOnePointAudit18>(
					key.ToString(), // EntityTrackingKey
					"TwentyOnePointAudit18",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.TwentyOnePointAudit18();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.Id = (System.Int32)reader[((int)TwentyOnePointAudit18Column.Id - 1)];
					c.Achieved18a = (reader.IsDBNull(((int)TwentyOnePointAudit18Column.Achieved18a - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit18Column.Achieved18a - 1)];
					c.Achieved18b = (reader.IsDBNull(((int)TwentyOnePointAudit18Column.Achieved18b - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit18Column.Achieved18b - 1)];
					c.Achieved18c = (reader.IsDBNull(((int)TwentyOnePointAudit18Column.Achieved18c - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit18Column.Achieved18c - 1)];
					c.Achieved18d = (reader.IsDBNull(((int)TwentyOnePointAudit18Column.Achieved18d - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit18Column.Achieved18d - 1)];
					c.Achieved18e = (reader.IsDBNull(((int)TwentyOnePointAudit18Column.Achieved18e - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit18Column.Achieved18e - 1)];
					c.Achieved18f = (reader.IsDBNull(((int)TwentyOnePointAudit18Column.Achieved18f - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit18Column.Achieved18f - 1)];
					c.Achieved18g = (reader.IsDBNull(((int)TwentyOnePointAudit18Column.Achieved18g - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit18Column.Achieved18g - 1)];
					c.Achieved18h = (reader.IsDBNull(((int)TwentyOnePointAudit18Column.Achieved18h - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit18Column.Achieved18h - 1)];
					c.Achieved18i = (reader.IsDBNull(((int)TwentyOnePointAudit18Column.Achieved18i - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit18Column.Achieved18i - 1)];
					c.Achieved18j = (reader.IsDBNull(((int)TwentyOnePointAudit18Column.Achieved18j - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit18Column.Achieved18j - 1)];
					c.Achieved18k = (reader.IsDBNull(((int)TwentyOnePointAudit18Column.Achieved18k - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit18Column.Achieved18k - 1)];
					c.Achieved18l = (reader.IsDBNull(((int)TwentyOnePointAudit18Column.Achieved18l - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit18Column.Achieved18l - 1)];
					c.Achieved18m = (reader.IsDBNull(((int)TwentyOnePointAudit18Column.Achieved18m - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit18Column.Achieved18m - 1)];
					c.Achieved18n = (reader.IsDBNull(((int)TwentyOnePointAudit18Column.Achieved18n - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit18Column.Achieved18n - 1)];
					c.Achieved18o = (reader.IsDBNull(((int)TwentyOnePointAudit18Column.Achieved18o - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit18Column.Achieved18o - 1)];
					c.Achieved18p = (reader.IsDBNull(((int)TwentyOnePointAudit18Column.Achieved18p - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit18Column.Achieved18p - 1)];
					c.Observation18a = (reader.IsDBNull(((int)TwentyOnePointAudit18Column.Observation18a - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit18Column.Observation18a - 1)];
					c.Observation18b = (reader.IsDBNull(((int)TwentyOnePointAudit18Column.Observation18b - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit18Column.Observation18b - 1)];
					c.Observation18c = (reader.IsDBNull(((int)TwentyOnePointAudit18Column.Observation18c - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit18Column.Observation18c - 1)];
					c.Observation18d = (reader.IsDBNull(((int)TwentyOnePointAudit18Column.Observation18d - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit18Column.Observation18d - 1)];
					c.Observation18e = (reader.IsDBNull(((int)TwentyOnePointAudit18Column.Observation18e - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit18Column.Observation18e - 1)];
					c.Observation18f = (reader.IsDBNull(((int)TwentyOnePointAudit18Column.Observation18f - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit18Column.Observation18f - 1)];
					c.Observation18g = (reader.IsDBNull(((int)TwentyOnePointAudit18Column.Observation18g - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit18Column.Observation18g - 1)];
					c.Observation18h = (reader.IsDBNull(((int)TwentyOnePointAudit18Column.Observation18h - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit18Column.Observation18h - 1)];
					c.Observation18i = (reader.IsDBNull(((int)TwentyOnePointAudit18Column.Observation18i - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit18Column.Observation18i - 1)];
					c.Observation18j = (reader.IsDBNull(((int)TwentyOnePointAudit18Column.Observation18j - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit18Column.Observation18j - 1)];
					c.Observation18k = (reader.IsDBNull(((int)TwentyOnePointAudit18Column.Observation18k - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit18Column.Observation18k - 1)];
					c.Observation18l = (reader.IsDBNull(((int)TwentyOnePointAudit18Column.Observation18l - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit18Column.Observation18l - 1)];
					c.Observation18m = (reader.IsDBNull(((int)TwentyOnePointAudit18Column.Observation18m - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit18Column.Observation18m - 1)];
					c.Observation18n = (reader.IsDBNull(((int)TwentyOnePointAudit18Column.Observation18n - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit18Column.Observation18n - 1)];
					c.Observation18o = (reader.IsDBNull(((int)TwentyOnePointAudit18Column.Observation18o - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit18Column.Observation18o - 1)];
					c.Observation18p = (reader.IsDBNull(((int)TwentyOnePointAudit18Column.Observation18p - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit18Column.Observation18p - 1)];
					c.TotalScore = (reader.IsDBNull(((int)TwentyOnePointAudit18Column.TotalScore - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit18Column.TotalScore - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.TwentyOnePointAudit18"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.TwentyOnePointAudit18"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.TwentyOnePointAudit18 entity)
		{
			if (!reader.Read()) return;
			
			entity.Id = (System.Int32)reader[((int)TwentyOnePointAudit18Column.Id - 1)];
			entity.Achieved18a = (reader.IsDBNull(((int)TwentyOnePointAudit18Column.Achieved18a - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit18Column.Achieved18a - 1)];
			entity.Achieved18b = (reader.IsDBNull(((int)TwentyOnePointAudit18Column.Achieved18b - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit18Column.Achieved18b - 1)];
			entity.Achieved18c = (reader.IsDBNull(((int)TwentyOnePointAudit18Column.Achieved18c - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit18Column.Achieved18c - 1)];
			entity.Achieved18d = (reader.IsDBNull(((int)TwentyOnePointAudit18Column.Achieved18d - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit18Column.Achieved18d - 1)];
			entity.Achieved18e = (reader.IsDBNull(((int)TwentyOnePointAudit18Column.Achieved18e - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit18Column.Achieved18e - 1)];
			entity.Achieved18f = (reader.IsDBNull(((int)TwentyOnePointAudit18Column.Achieved18f - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit18Column.Achieved18f - 1)];
			entity.Achieved18g = (reader.IsDBNull(((int)TwentyOnePointAudit18Column.Achieved18g - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit18Column.Achieved18g - 1)];
			entity.Achieved18h = (reader.IsDBNull(((int)TwentyOnePointAudit18Column.Achieved18h - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit18Column.Achieved18h - 1)];
			entity.Achieved18i = (reader.IsDBNull(((int)TwentyOnePointAudit18Column.Achieved18i - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit18Column.Achieved18i - 1)];
			entity.Achieved18j = (reader.IsDBNull(((int)TwentyOnePointAudit18Column.Achieved18j - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit18Column.Achieved18j - 1)];
			entity.Achieved18k = (reader.IsDBNull(((int)TwentyOnePointAudit18Column.Achieved18k - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit18Column.Achieved18k - 1)];
			entity.Achieved18l = (reader.IsDBNull(((int)TwentyOnePointAudit18Column.Achieved18l - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit18Column.Achieved18l - 1)];
			entity.Achieved18m = (reader.IsDBNull(((int)TwentyOnePointAudit18Column.Achieved18m - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit18Column.Achieved18m - 1)];
			entity.Achieved18n = (reader.IsDBNull(((int)TwentyOnePointAudit18Column.Achieved18n - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit18Column.Achieved18n - 1)];
			entity.Achieved18o = (reader.IsDBNull(((int)TwentyOnePointAudit18Column.Achieved18o - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit18Column.Achieved18o - 1)];
			entity.Achieved18p = (reader.IsDBNull(((int)TwentyOnePointAudit18Column.Achieved18p - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit18Column.Achieved18p - 1)];
			entity.Observation18a = (reader.IsDBNull(((int)TwentyOnePointAudit18Column.Observation18a - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit18Column.Observation18a - 1)];
			entity.Observation18b = (reader.IsDBNull(((int)TwentyOnePointAudit18Column.Observation18b - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit18Column.Observation18b - 1)];
			entity.Observation18c = (reader.IsDBNull(((int)TwentyOnePointAudit18Column.Observation18c - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit18Column.Observation18c - 1)];
			entity.Observation18d = (reader.IsDBNull(((int)TwentyOnePointAudit18Column.Observation18d - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit18Column.Observation18d - 1)];
			entity.Observation18e = (reader.IsDBNull(((int)TwentyOnePointAudit18Column.Observation18e - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit18Column.Observation18e - 1)];
			entity.Observation18f = (reader.IsDBNull(((int)TwentyOnePointAudit18Column.Observation18f - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit18Column.Observation18f - 1)];
			entity.Observation18g = (reader.IsDBNull(((int)TwentyOnePointAudit18Column.Observation18g - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit18Column.Observation18g - 1)];
			entity.Observation18h = (reader.IsDBNull(((int)TwentyOnePointAudit18Column.Observation18h - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit18Column.Observation18h - 1)];
			entity.Observation18i = (reader.IsDBNull(((int)TwentyOnePointAudit18Column.Observation18i - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit18Column.Observation18i - 1)];
			entity.Observation18j = (reader.IsDBNull(((int)TwentyOnePointAudit18Column.Observation18j - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit18Column.Observation18j - 1)];
			entity.Observation18k = (reader.IsDBNull(((int)TwentyOnePointAudit18Column.Observation18k - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit18Column.Observation18k - 1)];
			entity.Observation18l = (reader.IsDBNull(((int)TwentyOnePointAudit18Column.Observation18l - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit18Column.Observation18l - 1)];
			entity.Observation18m = (reader.IsDBNull(((int)TwentyOnePointAudit18Column.Observation18m - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit18Column.Observation18m - 1)];
			entity.Observation18n = (reader.IsDBNull(((int)TwentyOnePointAudit18Column.Observation18n - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit18Column.Observation18n - 1)];
			entity.Observation18o = (reader.IsDBNull(((int)TwentyOnePointAudit18Column.Observation18o - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit18Column.Observation18o - 1)];
			entity.Observation18p = (reader.IsDBNull(((int)TwentyOnePointAudit18Column.Observation18p - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit18Column.Observation18p - 1)];
			entity.TotalScore = (reader.IsDBNull(((int)TwentyOnePointAudit18Column.TotalScore - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit18Column.TotalScore - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.TwentyOnePointAudit18"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.TwentyOnePointAudit18"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.TwentyOnePointAudit18 entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.Id = (System.Int32)dataRow["ID"];
			entity.Achieved18a = Convert.IsDBNull(dataRow["Achieved18a"]) ? null : (System.Int32?)dataRow["Achieved18a"];
			entity.Achieved18b = Convert.IsDBNull(dataRow["Achieved18b"]) ? null : (System.Int32?)dataRow["Achieved18b"];
			entity.Achieved18c = Convert.IsDBNull(dataRow["Achieved18c"]) ? null : (System.Int32?)dataRow["Achieved18c"];
			entity.Achieved18d = Convert.IsDBNull(dataRow["Achieved18d"]) ? null : (System.Int32?)dataRow["Achieved18d"];
			entity.Achieved18e = Convert.IsDBNull(dataRow["Achieved18e"]) ? null : (System.Int32?)dataRow["Achieved18e"];
			entity.Achieved18f = Convert.IsDBNull(dataRow["Achieved18f"]) ? null : (System.Int32?)dataRow["Achieved18f"];
			entity.Achieved18g = Convert.IsDBNull(dataRow["Achieved18g"]) ? null : (System.Int32?)dataRow["Achieved18g"];
			entity.Achieved18h = Convert.IsDBNull(dataRow["Achieved18h"]) ? null : (System.Int32?)dataRow["Achieved18h"];
			entity.Achieved18i = Convert.IsDBNull(dataRow["Achieved18i"]) ? null : (System.Int32?)dataRow["Achieved18i"];
			entity.Achieved18j = Convert.IsDBNull(dataRow["Achieved18j"]) ? null : (System.Int32?)dataRow["Achieved18j"];
			entity.Achieved18k = Convert.IsDBNull(dataRow["Achieved18k"]) ? null : (System.Int32?)dataRow["Achieved18k"];
			entity.Achieved18l = Convert.IsDBNull(dataRow["Achieved18l"]) ? null : (System.Int32?)dataRow["Achieved18l"];
			entity.Achieved18m = Convert.IsDBNull(dataRow["Achieved18m"]) ? null : (System.Int32?)dataRow["Achieved18m"];
			entity.Achieved18n = Convert.IsDBNull(dataRow["Achieved18n"]) ? null : (System.Int32?)dataRow["Achieved18n"];
			entity.Achieved18o = Convert.IsDBNull(dataRow["Achieved18o"]) ? null : (System.Int32?)dataRow["Achieved18o"];
			entity.Achieved18p = Convert.IsDBNull(dataRow["Achieved18p"]) ? null : (System.Int32?)dataRow["Achieved18p"];
			entity.Observation18a = Convert.IsDBNull(dataRow["Observation18a"]) ? null : (System.String)dataRow["Observation18a"];
			entity.Observation18b = Convert.IsDBNull(dataRow["Observation18b"]) ? null : (System.String)dataRow["Observation18b"];
			entity.Observation18c = Convert.IsDBNull(dataRow["Observation18c"]) ? null : (System.String)dataRow["Observation18c"];
			entity.Observation18d = Convert.IsDBNull(dataRow["Observation18d"]) ? null : (System.String)dataRow["Observation18d"];
			entity.Observation18e = Convert.IsDBNull(dataRow["Observation18e"]) ? null : (System.String)dataRow["Observation18e"];
			entity.Observation18f = Convert.IsDBNull(dataRow["Observation18f"]) ? null : (System.String)dataRow["Observation18f"];
			entity.Observation18g = Convert.IsDBNull(dataRow["Observation18g"]) ? null : (System.String)dataRow["Observation18g"];
			entity.Observation18h = Convert.IsDBNull(dataRow["Observation18h"]) ? null : (System.String)dataRow["Observation18h"];
			entity.Observation18i = Convert.IsDBNull(dataRow["Observation18i"]) ? null : (System.String)dataRow["Observation18i"];
			entity.Observation18j = Convert.IsDBNull(dataRow["Observation18j"]) ? null : (System.String)dataRow["Observation18j"];
			entity.Observation18k = Convert.IsDBNull(dataRow["Observation18k"]) ? null : (System.String)dataRow["Observation18k"];
			entity.Observation18l = Convert.IsDBNull(dataRow["Observation18l"]) ? null : (System.String)dataRow["Observation18l"];
			entity.Observation18m = Convert.IsDBNull(dataRow["Observation18m"]) ? null : (System.String)dataRow["Observation18m"];
			entity.Observation18n = Convert.IsDBNull(dataRow["Observation18n"]) ? null : (System.String)dataRow["Observation18n"];
			entity.Observation18o = Convert.IsDBNull(dataRow["Observation18o"]) ? null : (System.String)dataRow["Observation18o"];
			entity.Observation18p = Convert.IsDBNull(dataRow["Observation18p"]) ? null : (System.String)dataRow["Observation18p"];
			entity.TotalScore = Convert.IsDBNull(dataRow["TotalScore"]) ? null : (System.Int32?)dataRow["TotalScore"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.TwentyOnePointAudit18"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.TwentyOnePointAudit18 Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.TwentyOnePointAudit18 entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetById methods when available
			
			#region TwentyOnePointAuditCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<TwentyOnePointAudit>|TwentyOnePointAuditCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'TwentyOnePointAuditCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.TwentyOnePointAuditCollection = DataRepository.TwentyOnePointAuditProvider.GetByPoint18Id(transactionManager, entity.Id);

				if (deep && entity.TwentyOnePointAuditCollection.Count > 0)
				{
					deepHandles.Add("TwentyOnePointAuditCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<TwentyOnePointAudit>) DataRepository.TwentyOnePointAuditProvider.DeepLoad,
						new object[] { transactionManager, entity.TwentyOnePointAuditCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.TwentyOnePointAudit18 object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.TwentyOnePointAudit18 instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.TwentyOnePointAudit18 Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.TwentyOnePointAudit18 entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<TwentyOnePointAudit>
				if (CanDeepSave(entity.TwentyOnePointAuditCollection, "List<TwentyOnePointAudit>|TwentyOnePointAuditCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(TwentyOnePointAudit child in entity.TwentyOnePointAuditCollection)
					{
						if(child.Point18IdSource != null)
						{
							child.Point18Id = child.Point18IdSource.Id;
						}
						else
						{
							child.Point18Id = entity.Id;
						}

					}

					if (entity.TwentyOnePointAuditCollection.Count > 0 || entity.TwentyOnePointAuditCollection.DeletedItems.Count > 0)
					{
						//DataRepository.TwentyOnePointAuditProvider.Save(transactionManager, entity.TwentyOnePointAuditCollection);
						
						deepHandles.Add("TwentyOnePointAuditCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< TwentyOnePointAudit >) DataRepository.TwentyOnePointAuditProvider.DeepSave,
							new object[] { transactionManager, entity.TwentyOnePointAuditCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region TwentyOnePointAudit18ChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.TwentyOnePointAudit18</c>
	///</summary>
	public enum TwentyOnePointAudit18ChildEntityTypes
	{

		///<summary>
		/// Collection of <c>TwentyOnePointAudit18</c> as OneToMany for TwentyOnePointAuditCollection
		///</summary>
		[ChildEntityType(typeof(TList<TwentyOnePointAudit>))]
		TwentyOnePointAuditCollection,
	}
	
	#endregion TwentyOnePointAudit18ChildEntityTypes
	
	#region TwentyOnePointAudit18FilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;TwentyOnePointAudit18Column&gt;"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit18"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAudit18FilterBuilder : SqlFilterBuilder<TwentyOnePointAudit18Column>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit18FilterBuilder class.
		/// </summary>
		public TwentyOnePointAudit18FilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit18FilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public TwentyOnePointAudit18FilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit18FilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public TwentyOnePointAudit18FilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion TwentyOnePointAudit18FilterBuilder
	
	#region TwentyOnePointAudit18ParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;TwentyOnePointAudit18Column&gt;"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit18"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAudit18ParameterBuilder : ParameterizedSqlFilterBuilder<TwentyOnePointAudit18Column>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit18ParameterBuilder class.
		/// </summary>
		public TwentyOnePointAudit18ParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit18ParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public TwentyOnePointAudit18ParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit18ParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public TwentyOnePointAudit18ParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion TwentyOnePointAudit18ParameterBuilder
	
	#region TwentyOnePointAudit18SortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;TwentyOnePointAudit18Column&gt;"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit18"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class TwentyOnePointAudit18SortBuilder : SqlSortBuilder<TwentyOnePointAudit18Column>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit18SqlSortBuilder class.
		/// </summary>
		public TwentyOnePointAudit18SortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion TwentyOnePointAudit18SortBuilder
	
} // end namespace
