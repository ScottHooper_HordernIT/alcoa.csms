<%@ Control Language="C#" AutoEventWireup="true" Inherits="Adminv2_UserControls_Ehsims" Codebehind="Ehsims.ascx.cs" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dxtc" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dxw" %>

<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
    
<%@ Register Src="../Other/ExportButtons.ascx" TagName="ExportButtons" TagPrefix="uc1" %>

<style type="text/css">
    .style1
    {
        width: 900px;
    }
</style>

<table border="0" cellpadding="0" cellspacing="0" style="width: 900px">
    <tr>
        <td style="text-align: left; " class="style1">
            <b>Note:</b> EHSIMS is used for data before 2011, whilst IHS (new system) is 
            used for data from 2011 onwards.<br /><br />
        </td>
    </tr>
    <tr>
        <td style="width: 900px; text-align: left; height: 331px;">
            <dxwgv:ASPxGridView ID="grid"
            runat="server" AutoGenerateColumns="False" ClientInstanceName="grid"
            CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
            CssPostfix="Office2003Blue" DataSourceID="CompaniesEhsimsMapDataSource" 
                KeyFieldName="EhssimsMapId" Width="900px"
            >

<SettingsCustomizationWindow Enabled="True"></SettingsCustomizationWindow>

<SettingsBehavior ColumnResizeMode="NextColumn" ConfirmDelete="True"></SettingsBehavior>

                <ImagesFilterControl>
                    <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
                    </LoadingPanel>
                </ImagesFilterControl>

<Styles CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css">
<Header SortingImageSpacing="5px" ImageSpacing="5px"></Header>

<LoadingPanel ImageSpacing="10px"></LoadingPanel>
</Styles>

<SettingsPager>
<AllButton Visible="True"></AllButton>
</SettingsPager>

<Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
    <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
    </LoadingPanelOnStatusBar>
<CollapsedButton Height="12px" Width="11px"></CollapsedButton>

<ExpandedButton Height="12px" Width="11px"></ExpandedButton>

<DetailCollapsedButton Height="12px" Width="11px"></DetailCollapsedButton>

<DetailExpandedButton Height="12px" Width="11px"></DetailExpandedButton>
    <LoadingPanel Url="~/App_Themes/Office2003Blue/GridView/Loading.gif">
    </LoadingPanel>
</Images>
<Columns>
    <dxwgv:GridViewCommandColumn Caption="Action" VisibleIndex="0" Width="115px">
        <EditButton Visible="True">
        </EditButton>
        <NewButton Visible="True">
        </NewButton>
        <DeleteButton Visible="True">
        </DeleteButton>
        <ClearFilterButton Visible="True">
        </ClearFilterButton>
    </dxwgv:GridViewCommandColumn>
    <dxwgv:GridViewDataTextColumn FieldName="EhssimsMapId" ReadOnly="True" Visible="False"
        VisibleIndex="0">
        <EditFormSettings Visible="False" />
    </dxwgv:GridViewDataTextColumn>
    <dxwgv:GridViewDataTextColumn Caption="Company Code" FieldName="ContCompanyCode" VisibleIndex="1" Width="90px">
        <CellStyle HorizontalAlign="Center">
        </CellStyle>
    </dxwgv:GridViewDataTextColumn>
    <dxwgv:GridViewDataComboBoxColumn Caption="Site - EHSIMS - IHS" FieldName="SiteId" 
        VisibleIndex="2" Width="235px" SortIndex="0" Settings-SortMode="DisplayText" SortOrder="Ascending">
        <PropertiesComboBox DataSourceID="SitesEhsimsIhsListDataSource" IncrementalFilteringMode="StartsWith"
            TextField="SiteNameLocCodeSiteNameIhs" ValueField="SiteId" ValueType="System.Int32">
        </PropertiesComboBox>
        <CellStyle HorizontalAlign="Left">
        </CellStyle>
    </dxwgv:GridViewDataComboBoxColumn>
    <dxwgv:GridViewDataComboBoxColumn Caption="Company Name" FieldName="CompanyId" UnboundType="String"
        VisibleIndex="3" SortIndex="1" SortOrder="Ascending">
        <PropertiesComboBox DataSourceID="sqldsCompaniesList" DropDownHeight="150px" IncrementalFilteringMode="StartsWith"
            TextField="CompanyName" ValueField="CompanyId" ValueType="System.String">
        </PropertiesComboBox>
        <Settings SortMode="DisplayText" />
        <EditFormSettings Visible="True" />
        <CellStyle HorizontalAlign="Left">
        </CellStyle>
    </dxwgv:GridViewDataComboBoxColumn>
</Columns>

<Settings ShowFilterRow="True" ShowFilterBar="Visible" ShowGroupPanel="True"></Settings>
</dxwgv:ASPxGridView>
        </td>
    </tr>
</table>

<asp:SqlDataSource ID="sqldsCompaniesList" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="_Companies_GetCompanyNameList" SelectCommandType="StoredProcedure">
</asp:SqlDataSource>
                    
<data:SitesDataSource ID="dsSitesFilter" runat="server" Filter="IsVisible = True AND Editable = True" Sort="SiteName ASC"></data:SitesDataSource> 

<data:CompaniesEhsimsMapDataSource ID="CompaniesEhsimsMapDataSource" runat="server"></data:CompaniesEhsimsMapDataSource>

<data:SitesEhsimsIhsListDataSource ID="SitesEhsimsIhsListDataSource" Sort="SiteNameLocCodeSiteNameIhs ASC" runat="server"></data:SitesEhsimsIhsListDataSource>

<div align="right" style="padding-top: 2px;">
    <uc1:ExportButtons ID="ucExportButtons" runat="server" />
</div>