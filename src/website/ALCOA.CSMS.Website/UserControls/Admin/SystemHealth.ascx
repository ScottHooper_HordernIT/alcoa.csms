﻿<%@ Control Language="C#" AutoEventWireup="true"
    Inherits="UserControls_Admin_SystemHealth" Codebehind="SystemHealth.ascx.cs" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
    <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
    CssPostfix="Office2003Blue" DataSourceID="SystemLogDataSource1" KeyFieldName="SystemLogId"
    Width="900px"  OnHtmlRowCreated="grid_RowCreated">
    <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue">
        <Header ImageSpacing="5px" SortingImageSpacing="5px">
        </Header>
        <LoadingPanel ImageSpacing="10px">
        </LoadingPanel>
    </Styles>
        <SettingsPager AlwaysShowPager="True">
            <AllButton Visible="True">
            </AllButton>
        </SettingsPager>
    <ImagesFilterControl>
        <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
        </LoadingPanel>
    </ImagesFilterControl>
    <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
        <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
        </LoadingPanelOnStatusBar>
        <LoadingPanel Url="~/App_Themes/Office2003BlueOld/GridView/Loading.gif"><%--Enhancement_023:change by debashis for testing platform upgradation--%>
        </LoadingPanel>
    </Images>
    <Columns>
        <dx:GridViewDataTextColumn FieldName="SystemLogId" ReadOnly="True" Visible="False"
            VisibleIndex="0">
            <EditFormSettings Visible="False" />
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataDateColumn Caption="Date Time" FieldName="DateTime" VisibleIndex="0"
            Width="135px" PropertiesDateEdit-DisplayFormatString="dd/MM/yyyy hh:ss:mm" SortIndex="0"
            SortOrder="Descending">
<PropertiesDateEdit DisplayFormatString="dd/MM/yyyy hh:ss:mm"></PropertiesDateEdit>
        </dx:GridViewDataDateColumn>
        <%--<dx:GridViewDataTextColumn FieldName="Activity" VisibleIndex="1">
        </dx:GridViewDataTextColumn>--%>
        <dx:GridViewDataComboBoxColumn FieldName="Activity" VisibleIndex="1" 
                                                >
                                                <PropertiesComboBox DataSourceID="getDistinctActivity" TextField="Activity" IncrementalFilteringMode="StartsWith">
                                                </PropertiesComboBox>
                                            </dx:GridViewDataComboBoxColumn>

        <dx:GridViewDataCheckColumn FieldName="Success" VisibleIndex="2" Width="75px">
            <DataItemTemplate>
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <asp:Image ID="cImage" runat="server" ImageUrl="~/Images/spacer.gif" Visible="true"
                                GenerateEmptyAlternateText="True" />
                        </td>
                    </tr>
                </table>
            </DataItemTemplate>
            <HeaderStyle HorizontalAlign="Center" />
            <CellStyle HorizontalAlign="Center">
            </CellStyle>
        </dx:GridViewDataCheckColumn>
        <dx:GridViewDataTextColumn FieldName="Notes" VisibleIndex="3">
        </dx:GridViewDataTextColumn>
    </Columns>

<Settings  ShowFilterRow="true" ShowFilterBar="Visible"></Settings>

    <StylesEditors>
        <ProgressBar Height="25px">
        </ProgressBar>
    </StylesEditors>
    <SettingsBehavior ColumnResizeMode="NextColumn" ConfirmDelete="True"></SettingsBehavior>
</dx:ASPxGridView>
<data:SystemLogDataSource ID="SystemLogDataSource1" runat="server">
</data:SystemLogDataSource>

<asp:sqlDataSource ID="getDistinctActivity" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="_GetDistinctActivitySystemLog" SelectCommandType="StoredProcedure">
</asp:sqlDataSource>
