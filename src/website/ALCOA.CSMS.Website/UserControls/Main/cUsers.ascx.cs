using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Text;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using KaiZen.CSMS.Web;
using KaiZen.CSMS.Services;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxTabControl;
using DevExpress.Web.Data;
using DevExpress.Web.ASPxGridView;
using DevExpress.XtraPrinting;

public partial class UserControls_cUsers : System.Web.UI.UserControl
{
    Auth auth = new Auth();
    protected void Page_Load(object sender, EventArgs e) {  Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); }

    private void moduleInit(bool postBack)
    {
        
        if (!postBack)
        {
            ASPxCompany.Text = auth.CompanyName;
            Configuration configuration = new Configuration();
            ASPxHyperLinkContact.NavigateUrl = "mailto:" + configuration.GetValue(ConfigList.ContactEmail);
            BindData();
            switch (auth.RoleId)
            {
                case ((int)RoleList.Reader):
                    ASPxlblInfo1.Text = "The following is a list of users currently granted access to this system.";
                    break;
                case ((int)RoleList.Contractor):
                    ASPxlblInfo1.Text = "The following is a list of users currently granted access to this system for your Company.";
                    break;
                case ((int)RoleList.Administrator):
                    ASPxlblInfo1.Text = "The following is a list of users currently granted access to this system.";
                    break;
                default:
                    Response.Redirect("AccessDenied.aspx?error=" + Server.UrlEncode("Unrecognised User."));
                    break;
            }

            // Export
            String exportFileName = @"ALCOA CSMS - Who Has Access"; //Chrome & IE is fine.
            String userAgent = Request.Headers.Get("User-Agent");
            if (
                (userAgent.Contains("Mozilla") && !userAgent.Contains("AppleWebKit") && !userAgent.Contains("MSIE")) || //Mozilla
                (userAgent.Contains("AppleWebKit") && !userAgent.Contains("Chrome"))      //Safari
                )
            {
                exportFileName = exportFileName.Replace(" ", "_");
            }
            Helper.ExportGrid.Settings(ucExportButtons, exportFileName);
        }
        else
        {
            grid.DataSource = (SqlDataSource)Session["dtWhoHasAccess"];
            grid.DataBind();
        }
    }

    protected void BindData()
    {
        if (auth.RoleId == (int)RoleList.Contractor)
        {
            int selectedValue = auth.CompanyId;
            CompaniesDataSource.SelectMethod = KaiZen.CSMS.Web.Data.CompaniesSelectMethod.GetByCompanyId;
            CompaniesDataSource.Parameters.Clear();
            CompaniesDataSource.Parameters.Add("CompanyId", auth.CompanyId.ToString());
            CompaniesDataSource.Sort = "CompanyName ASC";
            CompaniesDataSource.DataBind();
           
            sqldsUsers.SelectCommand = String.Format("SELECT UserId, CompanyId As [CompanyId], UserLogon, LastName, FirstName, RoleId As [RoleId], Title, Email, PhoneNo, MobNo, ModifiedByUserId FROM Users WHERE (UserId <> 0) and (RoleId <> 4) and CompanyId = {0}", selectedValue);
            sqldsUsers.DataBind();

            grid.Columns[1].Visible = false;
            grid.Columns[5].Visible = false;
        }
        else
        {
            if (auth.CompanyId == 0)
            {
                CompaniesDataSource.SelectMethod = KaiZen.CSMS.Web.Data.CompaniesSelectMethod.GetPaged;
                CompaniesDataSource.Parameters.Clear();
                CompaniesDataSource.Sort = "CompanyName ASC";
                CompaniesDataSource.DataBind();

                sqldsUsers.DataBind();
                grid.Columns[1].Visible = true;
                //grid.Columns[5].Visible = false;
            }
            else
            {
                int selectedValue = auth.CompanyId;
                CompaniesDataSource.SelectMethod = KaiZen.CSMS.Web.Data.CompaniesSelectMethod.GetByCompanyId;
                CompaniesDataSource.Parameters.Clear();
                CompaniesDataSource.Parameters.Add("CompanyId", auth.CompanyId.ToString());
                CompaniesDataSource.Sort = "CompanyName ASC";
                CompaniesDataSource.DataBind();

                sqldsUsers.SelectCommand = String.Format("SELECT UserId, CompanyId As [CompanyId], UserLogon, LastName, FirstName, RoleId As [RoleId], Title, Email, PhoneNo, MobNo, ModifiedByUserId FROM Users WHERE (UserId <> 0) and (RoleId <> 4) and CompanyId = {0}", selectedValue);
                sqldsUsers.DataBind();

                grid.Columns[1].Visible = false;
                grid.Columns[5].Visible = false;
            }
        }

        Session["dtWhoHasAccess"] = sqldsUsers;
        grid.DataSource = (SqlDataSource)Session["dtWhoHasAccess"];
        grid.DataBind();
    }

    protected void grid_AutoFilterCellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
    {
        if (e.Column.FieldName == "CompanyId" || e.Column.FieldName == "RoleId")
        {
            ASPxComboBox comboBox = e.Editor as ASPxComboBox;
            comboBox.ClientSideEvents.Init = "function(s, e) {s.InsertItem(0, '(ALL)', ''); s.RemoveItem(-1);}";
        }
    }
}
