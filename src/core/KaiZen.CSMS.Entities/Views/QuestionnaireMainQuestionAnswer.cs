﻿#region Using directives

using System;

#endregion

namespace KaiZen.CSMS.Entities
{	
	///<summary>
	/// An object representation of the 'QuestionnaireMainQuestionAnswer' view. [No description found in the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class QuestionnaireMainQuestionAnswer : QuestionnaireMainQuestionAnswerBase
	{
		#region Constructors

		///<summary>
		/// Creates a new <see cref="QuestionnaireMainQuestionAnswer"/> instance.
		///</summary>
		public QuestionnaireMainQuestionAnswer():base(){}	
		
		#endregion
	}
}
