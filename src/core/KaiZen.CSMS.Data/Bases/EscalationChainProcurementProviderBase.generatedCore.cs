﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="EscalationChainProcurementProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class EscalationChainProcurementProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.EscalationChainProcurement, KaiZen.CSMS.Entities.EscalationChainProcurementKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.EscalationChainProcurementKey key)
		{
			return Delete(transactionManager, key.EscalationChainProcurementId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_escalationChainProcurementId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _escalationChainProcurementId)
		{
			return Delete(null, _escalationChainProcurementId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_escalationChainProcurementId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _escalationChainProcurementId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_EscalationChainProcurement_EscalationChainProcurementRoles key.
		///		FK_EscalationChainProcurement_EscalationChainProcurementRoles Description: 
		/// </summary>
		/// <param name="_level"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.EscalationChainProcurement objects.</returns>
		public TList<EscalationChainProcurement> GetByLevel(System.Int32 _level)
		{
			int count = -1;
			return GetByLevel(_level, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_EscalationChainProcurement_EscalationChainProcurementRoles key.
		///		FK_EscalationChainProcurement_EscalationChainProcurementRoles Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_level"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.EscalationChainProcurement objects.</returns>
		/// <remarks></remarks>
		public TList<EscalationChainProcurement> GetByLevel(TransactionManager transactionManager, System.Int32 _level)
		{
			int count = -1;
			return GetByLevel(transactionManager, _level, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_EscalationChainProcurement_EscalationChainProcurementRoles key.
		///		FK_EscalationChainProcurement_EscalationChainProcurementRoles Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_level"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.EscalationChainProcurement objects.</returns>
		public TList<EscalationChainProcurement> GetByLevel(TransactionManager transactionManager, System.Int32 _level, int start, int pageLength)
		{
			int count = -1;
			return GetByLevel(transactionManager, _level, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_EscalationChainProcurement_EscalationChainProcurementRoles key.
		///		fkEscalationChainProcurementEscalationChainProcurementRoles Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_level"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.EscalationChainProcurement objects.</returns>
		public TList<EscalationChainProcurement> GetByLevel(System.Int32 _level, int start, int pageLength)
		{
			int count =  -1;
			return GetByLevel(null, _level, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_EscalationChainProcurement_EscalationChainProcurementRoles key.
		///		fkEscalationChainProcurementEscalationChainProcurementRoles Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_level"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.EscalationChainProcurement objects.</returns>
		public TList<EscalationChainProcurement> GetByLevel(System.Int32 _level, int start, int pageLength,out int count)
		{
			return GetByLevel(null, _level, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_EscalationChainProcurement_EscalationChainProcurementRoles key.
		///		FK_EscalationChainProcurement_EscalationChainProcurementRoles Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_level"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.EscalationChainProcurement objects.</returns>
		public abstract TList<EscalationChainProcurement> GetByLevel(TransactionManager transactionManager, System.Int32 _level, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_EscalationChainProcurement_Users key.
		///		FK_EscalationChainProcurement_Users Description: 
		/// </summary>
		/// <param name="_userId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.EscalationChainProcurement objects.</returns>
		public TList<EscalationChainProcurement> GetByUserId(System.Int32 _userId)
		{
			int count = -1;
			return GetByUserId(_userId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_EscalationChainProcurement_Users key.
		///		FK_EscalationChainProcurement_Users Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_userId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.EscalationChainProcurement objects.</returns>
		/// <remarks></remarks>
		public TList<EscalationChainProcurement> GetByUserId(TransactionManager transactionManager, System.Int32 _userId)
		{
			int count = -1;
			return GetByUserId(transactionManager, _userId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_EscalationChainProcurement_Users key.
		///		FK_EscalationChainProcurement_Users Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_userId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.EscalationChainProcurement objects.</returns>
		public TList<EscalationChainProcurement> GetByUserId(TransactionManager transactionManager, System.Int32 _userId, int start, int pageLength)
		{
			int count = -1;
			return GetByUserId(transactionManager, _userId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_EscalationChainProcurement_Users key.
		///		fkEscalationChainProcurementUsers Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_userId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.EscalationChainProcurement objects.</returns>
		public TList<EscalationChainProcurement> GetByUserId(System.Int32 _userId, int start, int pageLength)
		{
			int count =  -1;
			return GetByUserId(null, _userId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_EscalationChainProcurement_Users key.
		///		fkEscalationChainProcurementUsers Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_userId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.EscalationChainProcurement objects.</returns>
		public TList<EscalationChainProcurement> GetByUserId(System.Int32 _userId, int start, int pageLength,out int count)
		{
			return GetByUserId(null, _userId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_EscalationChainProcurement_Users key.
		///		FK_EscalationChainProcurement_Users Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_userId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.EscalationChainProcurement objects.</returns>
		public abstract TList<EscalationChainProcurement> GetByUserId(TransactionManager transactionManager, System.Int32 _userId, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.EscalationChainProcurement Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.EscalationChainProcurementKey key, int start, int pageLength)
		{
			return GetByEscalationChainProcurementId(transactionManager, key.EscalationChainProcurementId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_EscalationChainProcurement index.
		/// </summary>
		/// <param name="_escalationChainProcurementId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.EscalationChainProcurement"/> class.</returns>
		public KaiZen.CSMS.Entities.EscalationChainProcurement GetByEscalationChainProcurementId(System.Int32 _escalationChainProcurementId)
		{
			int count = -1;
			return GetByEscalationChainProcurementId(null,_escalationChainProcurementId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_EscalationChainProcurement index.
		/// </summary>
		/// <param name="_escalationChainProcurementId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.EscalationChainProcurement"/> class.</returns>
		public KaiZen.CSMS.Entities.EscalationChainProcurement GetByEscalationChainProcurementId(System.Int32 _escalationChainProcurementId, int start, int pageLength)
		{
			int count = -1;
			return GetByEscalationChainProcurementId(null, _escalationChainProcurementId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_EscalationChainProcurement index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_escalationChainProcurementId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.EscalationChainProcurement"/> class.</returns>
		public KaiZen.CSMS.Entities.EscalationChainProcurement GetByEscalationChainProcurementId(TransactionManager transactionManager, System.Int32 _escalationChainProcurementId)
		{
			int count = -1;
			return GetByEscalationChainProcurementId(transactionManager, _escalationChainProcurementId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_EscalationChainProcurement index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_escalationChainProcurementId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.EscalationChainProcurement"/> class.</returns>
		public KaiZen.CSMS.Entities.EscalationChainProcurement GetByEscalationChainProcurementId(TransactionManager transactionManager, System.Int32 _escalationChainProcurementId, int start, int pageLength)
		{
			int count = -1;
			return GetByEscalationChainProcurementId(transactionManager, _escalationChainProcurementId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_EscalationChainProcurement index.
		/// </summary>
		/// <param name="_escalationChainProcurementId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.EscalationChainProcurement"/> class.</returns>
		public KaiZen.CSMS.Entities.EscalationChainProcurement GetByEscalationChainProcurementId(System.Int32 _escalationChainProcurementId, int start, int pageLength, out int count)
		{
			return GetByEscalationChainProcurementId(null, _escalationChainProcurementId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_EscalationChainProcurement index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_escalationChainProcurementId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.EscalationChainProcurement"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.EscalationChainProcurement GetByEscalationChainProcurementId(TransactionManager transactionManager, System.Int32 _escalationChainProcurementId, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_EscalationChainProcurement_SiteId index.
		/// </summary>
		/// <param name="_siteId"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;EscalationChainProcurement&gt;"/> class.</returns>
		public TList<EscalationChainProcurement> GetBySiteId(System.Int32 _siteId)
		{
			int count = -1;
			return GetBySiteId(null,_siteId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_EscalationChainProcurement_SiteId index.
		/// </summary>
		/// <param name="_siteId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;EscalationChainProcurement&gt;"/> class.</returns>
		public TList<EscalationChainProcurement> GetBySiteId(System.Int32 _siteId, int start, int pageLength)
		{
			int count = -1;
			return GetBySiteId(null, _siteId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_EscalationChainProcurement_SiteId index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_siteId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;EscalationChainProcurement&gt;"/> class.</returns>
		public TList<EscalationChainProcurement> GetBySiteId(TransactionManager transactionManager, System.Int32 _siteId)
		{
			int count = -1;
			return GetBySiteId(transactionManager, _siteId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_EscalationChainProcurement_SiteId index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_siteId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;EscalationChainProcurement&gt;"/> class.</returns>
		public TList<EscalationChainProcurement> GetBySiteId(TransactionManager transactionManager, System.Int32 _siteId, int start, int pageLength)
		{
			int count = -1;
			return GetBySiteId(transactionManager, _siteId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_EscalationChainProcurement_SiteId index.
		/// </summary>
		/// <param name="_siteId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;EscalationChainProcurement&gt;"/> class.</returns>
		public TList<EscalationChainProcurement> GetBySiteId(System.Int32 _siteId, int start, int pageLength, out int count)
		{
			return GetBySiteId(null, _siteId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_EscalationChainProcurement_SiteId index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_siteId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;EscalationChainProcurement&gt;"/> class.</returns>
		public abstract TList<EscalationChainProcurement> GetBySiteId(TransactionManager transactionManager, System.Int32 _siteId, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key UK_EscalationChainProcurement index.
		/// </summary>
		/// <param name="_siteId"></param>
		/// <param name="_level"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.EscalationChainProcurement"/> class.</returns>
		public KaiZen.CSMS.Entities.EscalationChainProcurement GetBySiteIdLevel(System.Int32 _siteId, System.Int32 _level)
		{
			int count = -1;
			return GetBySiteIdLevel(null,_siteId, _level, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the UK_EscalationChainProcurement index.
		/// </summary>
		/// <param name="_siteId"></param>
		/// <param name="_level"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.EscalationChainProcurement"/> class.</returns>
		public KaiZen.CSMS.Entities.EscalationChainProcurement GetBySiteIdLevel(System.Int32 _siteId, System.Int32 _level, int start, int pageLength)
		{
			int count = -1;
			return GetBySiteIdLevel(null, _siteId, _level, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the UK_EscalationChainProcurement index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_siteId"></param>
		/// <param name="_level"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.EscalationChainProcurement"/> class.</returns>
		public KaiZen.CSMS.Entities.EscalationChainProcurement GetBySiteIdLevel(TransactionManager transactionManager, System.Int32 _siteId, System.Int32 _level)
		{
			int count = -1;
			return GetBySiteIdLevel(transactionManager, _siteId, _level, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the UK_EscalationChainProcurement index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_siteId"></param>
		/// <param name="_level"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.EscalationChainProcurement"/> class.</returns>
		public KaiZen.CSMS.Entities.EscalationChainProcurement GetBySiteIdLevel(TransactionManager transactionManager, System.Int32 _siteId, System.Int32 _level, int start, int pageLength)
		{
			int count = -1;
			return GetBySiteIdLevel(transactionManager, _siteId, _level, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the UK_EscalationChainProcurement index.
		/// </summary>
		/// <param name="_siteId"></param>
		/// <param name="_level"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.EscalationChainProcurement"/> class.</returns>
		public KaiZen.CSMS.Entities.EscalationChainProcurement GetBySiteIdLevel(System.Int32 _siteId, System.Int32 _level, int start, int pageLength, out int count)
		{
			return GetBySiteIdLevel(null, _siteId, _level, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the UK_EscalationChainProcurement index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_siteId"></param>
		/// <param name="_level"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.EscalationChainProcurement"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.EscalationChainProcurement GetBySiteIdLevel(TransactionManager transactionManager, System.Int32 _siteId, System.Int32 _level, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;EscalationChainProcurement&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;EscalationChainProcurement&gt;"/></returns>
		public static TList<EscalationChainProcurement> Fill(IDataReader reader, TList<EscalationChainProcurement> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.EscalationChainProcurement c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("EscalationChainProcurement")
					.Append("|").Append((System.Int32)reader[((int)EscalationChainProcurementColumn.EscalationChainProcurementId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<EscalationChainProcurement>(
					key.ToString(), // EntityTrackingKey
					"EscalationChainProcurement",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.EscalationChainProcurement();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.EscalationChainProcurementId = (System.Int32)reader[((int)EscalationChainProcurementColumn.EscalationChainProcurementId - 1)];
					c.Level = (System.Int32)reader[((int)EscalationChainProcurementColumn.Level - 1)];
					c.SiteId = (System.Int32)reader[((int)EscalationChainProcurementColumn.SiteId - 1)];
					c.UserId = (System.Int32)reader[((int)EscalationChainProcurementColumn.UserId - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.EscalationChainProcurement"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.EscalationChainProcurement"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.EscalationChainProcurement entity)
		{
			if (!reader.Read()) return;
			
			entity.EscalationChainProcurementId = (System.Int32)reader[((int)EscalationChainProcurementColumn.EscalationChainProcurementId - 1)];
			entity.Level = (System.Int32)reader[((int)EscalationChainProcurementColumn.Level - 1)];
			entity.SiteId = (System.Int32)reader[((int)EscalationChainProcurementColumn.SiteId - 1)];
			entity.UserId = (System.Int32)reader[((int)EscalationChainProcurementColumn.UserId - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.EscalationChainProcurement"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.EscalationChainProcurement"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.EscalationChainProcurement entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.EscalationChainProcurementId = (System.Int32)dataRow["EscalationChainProcurementId"];
			entity.Level = (System.Int32)dataRow["Level"];
			entity.SiteId = (System.Int32)dataRow["SiteId"];
			entity.UserId = (System.Int32)dataRow["UserId"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.EscalationChainProcurement"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.EscalationChainProcurement Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.EscalationChainProcurement entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region LevelSource	
			if (CanDeepLoad(entity, "EscalationChainProcurementRoles|LevelSource", deepLoadType, innerList) 
				&& entity.LevelSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.Level;
				EscalationChainProcurementRoles tmpEntity = EntityManager.LocateEntity<EscalationChainProcurementRoles>(EntityLocator.ConstructKeyFromPkItems(typeof(EscalationChainProcurementRoles), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.LevelSource = tmpEntity;
				else
					entity.LevelSource = DataRepository.EscalationChainProcurementRolesProvider.GetByLevel(transactionManager, entity.Level);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'LevelSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.LevelSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.EscalationChainProcurementRolesProvider.DeepLoad(transactionManager, entity.LevelSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion LevelSource

			#region SiteIdSource	
			if (CanDeepLoad(entity, "Sites|SiteIdSource", deepLoadType, innerList) 
				&& entity.SiteIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.SiteId;
				Sites tmpEntity = EntityManager.LocateEntity<Sites>(EntityLocator.ConstructKeyFromPkItems(typeof(Sites), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.SiteIdSource = tmpEntity;
				else
					entity.SiteIdSource = DataRepository.SitesProvider.GetBySiteId(transactionManager, entity.SiteId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'SiteIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.SiteIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.SitesProvider.DeepLoad(transactionManager, entity.SiteIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion SiteIdSource

			#region UserIdSource	
			if (CanDeepLoad(entity, "Users|UserIdSource", deepLoadType, innerList) 
				&& entity.UserIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.UserId;
				Users tmpEntity = EntityManager.LocateEntity<Users>(EntityLocator.ConstructKeyFromPkItems(typeof(Users), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.UserIdSource = tmpEntity;
				else
					entity.UserIdSource = DataRepository.UsersProvider.GetByUserId(transactionManager, entity.UserId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'UserIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.UserIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.UsersProvider.DeepLoad(transactionManager, entity.UserIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion UserIdSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.EscalationChainProcurement object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.EscalationChainProcurement instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.EscalationChainProcurement Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.EscalationChainProcurement entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region LevelSource
			if (CanDeepSave(entity, "EscalationChainProcurementRoles|LevelSource", deepSaveType, innerList) 
				&& entity.LevelSource != null)
			{
				DataRepository.EscalationChainProcurementRolesProvider.Save(transactionManager, entity.LevelSource);
				entity.Level = entity.LevelSource.Level;
			}
			#endregion 
			
			#region SiteIdSource
			if (CanDeepSave(entity, "Sites|SiteIdSource", deepSaveType, innerList) 
				&& entity.SiteIdSource != null)
			{
				DataRepository.SitesProvider.Save(transactionManager, entity.SiteIdSource);
				entity.SiteId = entity.SiteIdSource.SiteId;
			}
			#endregion 
			
			#region UserIdSource
			if (CanDeepSave(entity, "Users|UserIdSource", deepSaveType, innerList) 
				&& entity.UserIdSource != null)
			{
				DataRepository.UsersProvider.Save(transactionManager, entity.UserIdSource);
				entity.UserId = entity.UserIdSource.UserId;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region EscalationChainProcurementChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.EscalationChainProcurement</c>
	///</summary>
	public enum EscalationChainProcurementChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>EscalationChainProcurementRoles</c> at LevelSource
		///</summary>
		[ChildEntityType(typeof(EscalationChainProcurementRoles))]
		EscalationChainProcurementRoles,
			
		///<summary>
		/// Composite Property for <c>Sites</c> at SiteIdSource
		///</summary>
		[ChildEntityType(typeof(Sites))]
		Sites,
			
		///<summary>
		/// Composite Property for <c>Users</c> at UserIdSource
		///</summary>
		[ChildEntityType(typeof(Users))]
		Users,
		}
	
	#endregion EscalationChainProcurementChildEntityTypes
	
	#region EscalationChainProcurementFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EscalationChainProcurementColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EscalationChainProcurement"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EscalationChainProcurementFilterBuilder : SqlFilterBuilder<EscalationChainProcurementColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EscalationChainProcurementFilterBuilder class.
		/// </summary>
		public EscalationChainProcurementFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the EscalationChainProcurementFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public EscalationChainProcurementFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the EscalationChainProcurementFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public EscalationChainProcurementFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion EscalationChainProcurementFilterBuilder
	
	#region EscalationChainProcurementParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EscalationChainProcurementColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EscalationChainProcurement"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EscalationChainProcurementParameterBuilder : ParameterizedSqlFilterBuilder<EscalationChainProcurementColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EscalationChainProcurementParameterBuilder class.
		/// </summary>
		public EscalationChainProcurementParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the EscalationChainProcurementParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public EscalationChainProcurementParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the EscalationChainProcurementParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public EscalationChainProcurementParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion EscalationChainProcurementParameterBuilder
	
	#region EscalationChainProcurementSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EscalationChainProcurementColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EscalationChainProcurement"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class EscalationChainProcurementSortBuilder : SqlSortBuilder<EscalationChainProcurementColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EscalationChainProcurementSqlSortBuilder class.
		/// </summary>
		public EscalationChainProcurementSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion EscalationChainProcurementSortBuilder
	
} // end namespace
