﻿#region Using directives

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="QuestionnaireEscalationProcurementListProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract class QuestionnaireEscalationProcurementListProviderBaseCore : EntityViewProviderBase<QuestionnaireEscalationProcurementList>
	{
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions
		
		/*
		///<summary>
		/// Fill an VList&lt;QuestionnaireEscalationProcurementList&gt; From a DataSet
		///</summary>
		/// <param name="dataSet">the DataSet</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList&lt;QuestionnaireEscalationProcurementList&gt;"/></returns>
		protected static VList&lt;QuestionnaireEscalationProcurementList&gt; Fill(DataSet dataSet, VList<QuestionnaireEscalationProcurementList> rows, int start, int pagelen)
		{
			if (dataSet.Tables.Count == 1)
			{
				return Fill(dataSet.Tables[0], rows, start, pagelen);
			}
			else
			{
				return new VList<QuestionnaireEscalationProcurementList>();
			}	
		}
		
		
		///<summary>
		/// Fill an VList&lt;QuestionnaireEscalationProcurementList&gt; From a DataTable
		///</summary>
		/// <param name="dataTable">the DataTable that hold the data.</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList<QuestionnaireEscalationProcurementList>"/></returns>
		protected static VList&lt;QuestionnaireEscalationProcurementList&gt; Fill(DataTable dataTable, VList<QuestionnaireEscalationProcurementList> rows, int start, int pagelen)
		{
			int recordnum = 0;
			
			System.Collections.IEnumerator dataRows =  dataTable.Rows.GetEnumerator();
			
			while (dataRows.MoveNext() && (pagelen != 0))
			{
				if(recordnum >= start)
				{
					DataRow row = (DataRow)dataRows.Current;
				
					QuestionnaireEscalationProcurementList c = new QuestionnaireEscalationProcurementList();
					c.QuestionnaireId = (Convert.IsDBNull(row["QuestionnaireId"]))?(int)0:(System.Int32)row["QuestionnaireId"];
					c.ProcurementUserId = (Convert.IsDBNull(row["ProcurementUserId"]))?(int)0:(System.Int32?)row["ProcurementUserId"];
					c.ProcurementRegionId = (Convert.IsDBNull(row["ProcurementRegionId"]))?(int)0:(System.Int32?)row["ProcurementRegionId"];
					c.ProcurementSiteId = (Convert.IsDBNull(row["ProcurementSiteId"]))?(int)0:(System.Int32?)row["ProcurementSiteId"];
					c.SupervisorUserId = (Convert.IsDBNull(row["SupervisorUserId"]))?(int)0:(System.Int32?)row["SupervisorUserId"];
					c.Level3UserId = (Convert.IsDBNull(row["Level3UserId"]))?(int)0:(System.Int32?)row["Level3UserId"];
					c.Level4UserId = (Convert.IsDBNull(row["Level4UserId"]))?(int)0:(System.Int32?)row["Level4UserId"];
					c.Level5UserId = (Convert.IsDBNull(row["Level5UserId"]))?(int)0:(System.Int32?)row["Level5UserId"];
					c.Level6UserId = (Convert.IsDBNull(row["Level6UserId"]))?(int)0:(System.Int32?)row["Level6UserId"];
					c.Level7UserId = (Convert.IsDBNull(row["Level7UserId"]))?(int)0:(System.Int32?)row["Level7UserId"];
					c.Level8UserId = (Convert.IsDBNull(row["Level8UserId"]))?(int)0:(System.Int32?)row["Level8UserId"];
					c.Level9UserId = (Convert.IsDBNull(row["Level9UserId"]))?(int)0:(System.Int32?)row["Level9UserId"];
					c.ProcurementContactFirstLastName = (Convert.IsDBNull(row["ProcurementContactFirstLastName"]))?string.Empty:(System.String)row["ProcurementContactFirstLastName"];
					c.ProcurementContactLastFirstName = (Convert.IsDBNull(row["ProcurementContactLastFirstName"]))?string.Empty:(System.String)row["ProcurementContactLastFirstName"];
					c.ProcurementContactEmail = (Convert.IsDBNull(row["ProcurementContactEmail"]))?string.Empty:(System.String)row["ProcurementContactEmail"];
					c.CompanyName = (Convert.IsDBNull(row["CompanyName"]))?string.Empty:(System.String)row["CompanyName"];
					c.ActionDescription = (Convert.IsDBNull(row["ActionDescription"]))?string.Empty:(System.String)row["ActionDescription"];
					c.UserDescription = (Convert.IsDBNull(row["UserDescription"]))?string.Empty:(System.String)row["UserDescription"];
					c.QuestionnairePresentlyWithSince = (Convert.IsDBNull(row["QuestionnairePresentlyWithSince"]))?DateTime.MinValue:(System.DateTime?)row["QuestionnairePresentlyWithSince"];
					c.DaysWith = (Convert.IsDBNull(row["DaysWith"]))?(int)0:(System.Int32?)row["DaysWith"];
					c.AcceptChanges();
					rows.Add(c);
					pagelen -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		*/	
						
		///<summary>
		/// Fill an <see cref="VList&lt;QuestionnaireEscalationProcurementList&gt;"/> From a DataReader.
		///</summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pageLength">number of row.</param>
		///<returns>a <see cref="VList&lt;QuestionnaireEscalationProcurementList&gt;"/></returns>
		protected VList<QuestionnaireEscalationProcurementList> Fill(IDataReader reader, VList<QuestionnaireEscalationProcurementList> rows, int start, int pageLength)
		{
			int recordnum = 0;
			while (reader.Read() && (pageLength != 0))
			{
				if(recordnum >= start)
				{
					QuestionnaireEscalationProcurementList entity = null;
					if (DataRepository.Provider.UseEntityFactory)
					{
						entity = EntityManager.CreateViewEntity<QuestionnaireEscalationProcurementList>("QuestionnaireEscalationProcurementList",  DataRepository.Provider.EntityCreationalFactoryType); 
					}
					else
					{
						entity = new QuestionnaireEscalationProcurementList();
					}
					
					entity.SuppressEntityEvents = true;

					entity.QuestionnaireId = (System.Int32)reader[((int)QuestionnaireEscalationProcurementListColumn.QuestionnaireId)];
					//entity.QuestionnaireId = (Convert.IsDBNull(reader["QuestionnaireId"]))?(int)0:(System.Int32)reader["QuestionnaireId"];
					entity.ProcurementUserId = (reader.IsDBNull(((int)QuestionnaireEscalationProcurementListColumn.ProcurementUserId)))?null:(System.Int32?)reader[((int)QuestionnaireEscalationProcurementListColumn.ProcurementUserId)];
					//entity.ProcurementUserId = (Convert.IsDBNull(reader["ProcurementUserId"]))?(int)0:(System.Int32?)reader["ProcurementUserId"];
					entity.ProcurementRegionId = (reader.IsDBNull(((int)QuestionnaireEscalationProcurementListColumn.ProcurementRegionId)))?null:(System.Int32?)reader[((int)QuestionnaireEscalationProcurementListColumn.ProcurementRegionId)];
					//entity.ProcurementRegionId = (Convert.IsDBNull(reader["ProcurementRegionId"]))?(int)0:(System.Int32?)reader["ProcurementRegionId"];
					entity.ProcurementSiteId = (reader.IsDBNull(((int)QuestionnaireEscalationProcurementListColumn.ProcurementSiteId)))?null:(System.Int32?)reader[((int)QuestionnaireEscalationProcurementListColumn.ProcurementSiteId)];
					//entity.ProcurementSiteId = (Convert.IsDBNull(reader["ProcurementSiteId"]))?(int)0:(System.Int32?)reader["ProcurementSiteId"];
					entity.SupervisorUserId = (reader.IsDBNull(((int)QuestionnaireEscalationProcurementListColumn.SupervisorUserId)))?null:(System.Int32?)reader[((int)QuestionnaireEscalationProcurementListColumn.SupervisorUserId)];
					//entity.SupervisorUserId = (Convert.IsDBNull(reader["SupervisorUserId"]))?(int)0:(System.Int32?)reader["SupervisorUserId"];
					entity.Level3UserId = (reader.IsDBNull(((int)QuestionnaireEscalationProcurementListColumn.Level3UserId)))?null:(System.Int32?)reader[((int)QuestionnaireEscalationProcurementListColumn.Level3UserId)];
					//entity.Level3UserId = (Convert.IsDBNull(reader["Level3UserId"]))?(int)0:(System.Int32?)reader["Level3UserId"];
					entity.Level4UserId = (reader.IsDBNull(((int)QuestionnaireEscalationProcurementListColumn.Level4UserId)))?null:(System.Int32?)reader[((int)QuestionnaireEscalationProcurementListColumn.Level4UserId)];
					//entity.Level4UserId = (Convert.IsDBNull(reader["Level4UserId"]))?(int)0:(System.Int32?)reader["Level4UserId"];
					entity.Level5UserId = (reader.IsDBNull(((int)QuestionnaireEscalationProcurementListColumn.Level5UserId)))?null:(System.Int32?)reader[((int)QuestionnaireEscalationProcurementListColumn.Level5UserId)];
					//entity.Level5UserId = (Convert.IsDBNull(reader["Level5UserId"]))?(int)0:(System.Int32?)reader["Level5UserId"];
					entity.Level6UserId = (reader.IsDBNull(((int)QuestionnaireEscalationProcurementListColumn.Level6UserId)))?null:(System.Int32?)reader[((int)QuestionnaireEscalationProcurementListColumn.Level6UserId)];
					//entity.Level6UserId = (Convert.IsDBNull(reader["Level6UserId"]))?(int)0:(System.Int32?)reader["Level6UserId"];
					entity.Level7UserId = (reader.IsDBNull(((int)QuestionnaireEscalationProcurementListColumn.Level7UserId)))?null:(System.Int32?)reader[((int)QuestionnaireEscalationProcurementListColumn.Level7UserId)];
					//entity.Level7UserId = (Convert.IsDBNull(reader["Level7UserId"]))?(int)0:(System.Int32?)reader["Level7UserId"];
					entity.Level8UserId = (reader.IsDBNull(((int)QuestionnaireEscalationProcurementListColumn.Level8UserId)))?null:(System.Int32?)reader[((int)QuestionnaireEscalationProcurementListColumn.Level8UserId)];
					//entity.Level8UserId = (Convert.IsDBNull(reader["Level8UserId"]))?(int)0:(System.Int32?)reader["Level8UserId"];
					entity.Level9UserId = (reader.IsDBNull(((int)QuestionnaireEscalationProcurementListColumn.Level9UserId)))?null:(System.Int32?)reader[((int)QuestionnaireEscalationProcurementListColumn.Level9UserId)];
					//entity.Level9UserId = (Convert.IsDBNull(reader["Level9UserId"]))?(int)0:(System.Int32?)reader["Level9UserId"];
					entity.ProcurementContactFirstLastName = (reader.IsDBNull(((int)QuestionnaireEscalationProcurementListColumn.ProcurementContactFirstLastName)))?null:(System.String)reader[((int)QuestionnaireEscalationProcurementListColumn.ProcurementContactFirstLastName)];
					//entity.ProcurementContactFirstLastName = (Convert.IsDBNull(reader["ProcurementContactFirstLastName"]))?string.Empty:(System.String)reader["ProcurementContactFirstLastName"];
					entity.ProcurementContactLastFirstName = (reader.IsDBNull(((int)QuestionnaireEscalationProcurementListColumn.ProcurementContactLastFirstName)))?null:(System.String)reader[((int)QuestionnaireEscalationProcurementListColumn.ProcurementContactLastFirstName)];
					//entity.ProcurementContactLastFirstName = (Convert.IsDBNull(reader["ProcurementContactLastFirstName"]))?string.Empty:(System.String)reader["ProcurementContactLastFirstName"];
					entity.ProcurementContactEmail = (reader.IsDBNull(((int)QuestionnaireEscalationProcurementListColumn.ProcurementContactEmail)))?null:(System.String)reader[((int)QuestionnaireEscalationProcurementListColumn.ProcurementContactEmail)];
					//entity.ProcurementContactEmail = (Convert.IsDBNull(reader["ProcurementContactEmail"]))?string.Empty:(System.String)reader["ProcurementContactEmail"];
					entity.CompanyName = (reader.IsDBNull(((int)QuestionnaireEscalationProcurementListColumn.CompanyName)))?null:(System.String)reader[((int)QuestionnaireEscalationProcurementListColumn.CompanyName)];
					//entity.CompanyName = (Convert.IsDBNull(reader["CompanyName"]))?string.Empty:(System.String)reader["CompanyName"];
					entity.ActionDescription = (reader.IsDBNull(((int)QuestionnaireEscalationProcurementListColumn.ActionDescription)))?null:(System.String)reader[((int)QuestionnaireEscalationProcurementListColumn.ActionDescription)];
					//entity.ActionDescription = (Convert.IsDBNull(reader["ActionDescription"]))?string.Empty:(System.String)reader["ActionDescription"];
					entity.UserDescription = (reader.IsDBNull(((int)QuestionnaireEscalationProcurementListColumn.UserDescription)))?null:(System.String)reader[((int)QuestionnaireEscalationProcurementListColumn.UserDescription)];
					//entity.UserDescription = (Convert.IsDBNull(reader["UserDescription"]))?string.Empty:(System.String)reader["UserDescription"];
					entity.QuestionnairePresentlyWithSince = (reader.IsDBNull(((int)QuestionnaireEscalationProcurementListColumn.QuestionnairePresentlyWithSince)))?null:(System.DateTime?)reader[((int)QuestionnaireEscalationProcurementListColumn.QuestionnairePresentlyWithSince)];
					//entity.QuestionnairePresentlyWithSince = (Convert.IsDBNull(reader["QuestionnairePresentlyWithSince"]))?DateTime.MinValue:(System.DateTime?)reader["QuestionnairePresentlyWithSince"];
					entity.DaysWith = (reader.IsDBNull(((int)QuestionnaireEscalationProcurementListColumn.DaysWith)))?null:(System.Int32?)reader[((int)QuestionnaireEscalationProcurementListColumn.DaysWith)];
					//entity.DaysWith = (Convert.IsDBNull(reader["DaysWith"]))?(int)0:(System.Int32?)reader["DaysWith"];
					entity.AcceptChanges();
					entity.SuppressEntityEvents = false;
					
					rows.Add(entity);
					pageLength -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		
		
		/// <summary>
		/// Refreshes the <see cref="QuestionnaireEscalationProcurementList"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="QuestionnaireEscalationProcurementList"/> object to refresh.</param>
		protected void RefreshEntity(IDataReader reader, QuestionnaireEscalationProcurementList entity)
		{
			reader.Read();
			entity.QuestionnaireId = (System.Int32)reader[((int)QuestionnaireEscalationProcurementListColumn.QuestionnaireId)];
			//entity.QuestionnaireId = (Convert.IsDBNull(reader["QuestionnaireId"]))?(int)0:(System.Int32)reader["QuestionnaireId"];
			entity.ProcurementUserId = (reader.IsDBNull(((int)QuestionnaireEscalationProcurementListColumn.ProcurementUserId)))?null:(System.Int32?)reader[((int)QuestionnaireEscalationProcurementListColumn.ProcurementUserId)];
			//entity.ProcurementUserId = (Convert.IsDBNull(reader["ProcurementUserId"]))?(int)0:(System.Int32?)reader["ProcurementUserId"];
			entity.ProcurementRegionId = (reader.IsDBNull(((int)QuestionnaireEscalationProcurementListColumn.ProcurementRegionId)))?null:(System.Int32?)reader[((int)QuestionnaireEscalationProcurementListColumn.ProcurementRegionId)];
			//entity.ProcurementRegionId = (Convert.IsDBNull(reader["ProcurementRegionId"]))?(int)0:(System.Int32?)reader["ProcurementRegionId"];
			entity.ProcurementSiteId = (reader.IsDBNull(((int)QuestionnaireEscalationProcurementListColumn.ProcurementSiteId)))?null:(System.Int32?)reader[((int)QuestionnaireEscalationProcurementListColumn.ProcurementSiteId)];
			//entity.ProcurementSiteId = (Convert.IsDBNull(reader["ProcurementSiteId"]))?(int)0:(System.Int32?)reader["ProcurementSiteId"];
			entity.SupervisorUserId = (reader.IsDBNull(((int)QuestionnaireEscalationProcurementListColumn.SupervisorUserId)))?null:(System.Int32?)reader[((int)QuestionnaireEscalationProcurementListColumn.SupervisorUserId)];
			//entity.SupervisorUserId = (Convert.IsDBNull(reader["SupervisorUserId"]))?(int)0:(System.Int32?)reader["SupervisorUserId"];
			entity.Level3UserId = (reader.IsDBNull(((int)QuestionnaireEscalationProcurementListColumn.Level3UserId)))?null:(System.Int32?)reader[((int)QuestionnaireEscalationProcurementListColumn.Level3UserId)];
			//entity.Level3UserId = (Convert.IsDBNull(reader["Level3UserId"]))?(int)0:(System.Int32?)reader["Level3UserId"];
			entity.Level4UserId = (reader.IsDBNull(((int)QuestionnaireEscalationProcurementListColumn.Level4UserId)))?null:(System.Int32?)reader[((int)QuestionnaireEscalationProcurementListColumn.Level4UserId)];
			//entity.Level4UserId = (Convert.IsDBNull(reader["Level4UserId"]))?(int)0:(System.Int32?)reader["Level4UserId"];
			entity.Level5UserId = (reader.IsDBNull(((int)QuestionnaireEscalationProcurementListColumn.Level5UserId)))?null:(System.Int32?)reader[((int)QuestionnaireEscalationProcurementListColumn.Level5UserId)];
			//entity.Level5UserId = (Convert.IsDBNull(reader["Level5UserId"]))?(int)0:(System.Int32?)reader["Level5UserId"];
			entity.Level6UserId = (reader.IsDBNull(((int)QuestionnaireEscalationProcurementListColumn.Level6UserId)))?null:(System.Int32?)reader[((int)QuestionnaireEscalationProcurementListColumn.Level6UserId)];
			//entity.Level6UserId = (Convert.IsDBNull(reader["Level6UserId"]))?(int)0:(System.Int32?)reader["Level6UserId"];
			entity.Level7UserId = (reader.IsDBNull(((int)QuestionnaireEscalationProcurementListColumn.Level7UserId)))?null:(System.Int32?)reader[((int)QuestionnaireEscalationProcurementListColumn.Level7UserId)];
			//entity.Level7UserId = (Convert.IsDBNull(reader["Level7UserId"]))?(int)0:(System.Int32?)reader["Level7UserId"];
			entity.Level8UserId = (reader.IsDBNull(((int)QuestionnaireEscalationProcurementListColumn.Level8UserId)))?null:(System.Int32?)reader[((int)QuestionnaireEscalationProcurementListColumn.Level8UserId)];
			//entity.Level8UserId = (Convert.IsDBNull(reader["Level8UserId"]))?(int)0:(System.Int32?)reader["Level8UserId"];
			entity.Level9UserId = (reader.IsDBNull(((int)QuestionnaireEscalationProcurementListColumn.Level9UserId)))?null:(System.Int32?)reader[((int)QuestionnaireEscalationProcurementListColumn.Level9UserId)];
			//entity.Level9UserId = (Convert.IsDBNull(reader["Level9UserId"]))?(int)0:(System.Int32?)reader["Level9UserId"];
			entity.ProcurementContactFirstLastName = (reader.IsDBNull(((int)QuestionnaireEscalationProcurementListColumn.ProcurementContactFirstLastName)))?null:(System.String)reader[((int)QuestionnaireEscalationProcurementListColumn.ProcurementContactFirstLastName)];
			//entity.ProcurementContactFirstLastName = (Convert.IsDBNull(reader["ProcurementContactFirstLastName"]))?string.Empty:(System.String)reader["ProcurementContactFirstLastName"];
			entity.ProcurementContactLastFirstName = (reader.IsDBNull(((int)QuestionnaireEscalationProcurementListColumn.ProcurementContactLastFirstName)))?null:(System.String)reader[((int)QuestionnaireEscalationProcurementListColumn.ProcurementContactLastFirstName)];
			//entity.ProcurementContactLastFirstName = (Convert.IsDBNull(reader["ProcurementContactLastFirstName"]))?string.Empty:(System.String)reader["ProcurementContactLastFirstName"];
			entity.ProcurementContactEmail = (reader.IsDBNull(((int)QuestionnaireEscalationProcurementListColumn.ProcurementContactEmail)))?null:(System.String)reader[((int)QuestionnaireEscalationProcurementListColumn.ProcurementContactEmail)];
			//entity.ProcurementContactEmail = (Convert.IsDBNull(reader["ProcurementContactEmail"]))?string.Empty:(System.String)reader["ProcurementContactEmail"];
			entity.CompanyName = (reader.IsDBNull(((int)QuestionnaireEscalationProcurementListColumn.CompanyName)))?null:(System.String)reader[((int)QuestionnaireEscalationProcurementListColumn.CompanyName)];
			//entity.CompanyName = (Convert.IsDBNull(reader["CompanyName"]))?string.Empty:(System.String)reader["CompanyName"];
			entity.ActionDescription = (reader.IsDBNull(((int)QuestionnaireEscalationProcurementListColumn.ActionDescription)))?null:(System.String)reader[((int)QuestionnaireEscalationProcurementListColumn.ActionDescription)];
			//entity.ActionDescription = (Convert.IsDBNull(reader["ActionDescription"]))?string.Empty:(System.String)reader["ActionDescription"];
			entity.UserDescription = (reader.IsDBNull(((int)QuestionnaireEscalationProcurementListColumn.UserDescription)))?null:(System.String)reader[((int)QuestionnaireEscalationProcurementListColumn.UserDescription)];
			//entity.UserDescription = (Convert.IsDBNull(reader["UserDescription"]))?string.Empty:(System.String)reader["UserDescription"];
			entity.QuestionnairePresentlyWithSince = (reader.IsDBNull(((int)QuestionnaireEscalationProcurementListColumn.QuestionnairePresentlyWithSince)))?null:(System.DateTime?)reader[((int)QuestionnaireEscalationProcurementListColumn.QuestionnairePresentlyWithSince)];
			//entity.QuestionnairePresentlyWithSince = (Convert.IsDBNull(reader["QuestionnairePresentlyWithSince"]))?DateTime.MinValue:(System.DateTime?)reader["QuestionnairePresentlyWithSince"];
			entity.DaysWith = (reader.IsDBNull(((int)QuestionnaireEscalationProcurementListColumn.DaysWith)))?null:(System.Int32?)reader[((int)QuestionnaireEscalationProcurementListColumn.DaysWith)];
			//entity.DaysWith = (Convert.IsDBNull(reader["DaysWith"]))?(int)0:(System.Int32?)reader["DaysWith"];
			reader.Close();
	
			entity.AcceptChanges();
		}
		
		/*
		/// <summary>
		/// Refreshes the <see cref="QuestionnaireEscalationProcurementList"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="QuestionnaireEscalationProcurementList"/> object.</param>
		protected static void RefreshEntity(DataSet dataSet, QuestionnaireEscalationProcurementList entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.QuestionnaireId = (Convert.IsDBNull(dataRow["QuestionnaireId"]))?(int)0:(System.Int32)dataRow["QuestionnaireId"];
			entity.ProcurementUserId = (Convert.IsDBNull(dataRow["ProcurementUserId"]))?(int)0:(System.Int32?)dataRow["ProcurementUserId"];
			entity.ProcurementRegionId = (Convert.IsDBNull(dataRow["ProcurementRegionId"]))?(int)0:(System.Int32?)dataRow["ProcurementRegionId"];
			entity.ProcurementSiteId = (Convert.IsDBNull(dataRow["ProcurementSiteId"]))?(int)0:(System.Int32?)dataRow["ProcurementSiteId"];
			entity.SupervisorUserId = (Convert.IsDBNull(dataRow["SupervisorUserId"]))?(int)0:(System.Int32?)dataRow["SupervisorUserId"];
			entity.Level3UserId = (Convert.IsDBNull(dataRow["Level3UserId"]))?(int)0:(System.Int32?)dataRow["Level3UserId"];
			entity.Level4UserId = (Convert.IsDBNull(dataRow["Level4UserId"]))?(int)0:(System.Int32?)dataRow["Level4UserId"];
			entity.Level5UserId = (Convert.IsDBNull(dataRow["Level5UserId"]))?(int)0:(System.Int32?)dataRow["Level5UserId"];
			entity.Level6UserId = (Convert.IsDBNull(dataRow["Level6UserId"]))?(int)0:(System.Int32?)dataRow["Level6UserId"];
			entity.Level7UserId = (Convert.IsDBNull(dataRow["Level7UserId"]))?(int)0:(System.Int32?)dataRow["Level7UserId"];
			entity.Level8UserId = (Convert.IsDBNull(dataRow["Level8UserId"]))?(int)0:(System.Int32?)dataRow["Level8UserId"];
			entity.Level9UserId = (Convert.IsDBNull(dataRow["Level9UserId"]))?(int)0:(System.Int32?)dataRow["Level9UserId"];
			entity.ProcurementContactFirstLastName = (Convert.IsDBNull(dataRow["ProcurementContactFirstLastName"]))?string.Empty:(System.String)dataRow["ProcurementContactFirstLastName"];
			entity.ProcurementContactLastFirstName = (Convert.IsDBNull(dataRow["ProcurementContactLastFirstName"]))?string.Empty:(System.String)dataRow["ProcurementContactLastFirstName"];
			entity.ProcurementContactEmail = (Convert.IsDBNull(dataRow["ProcurementContactEmail"]))?string.Empty:(System.String)dataRow["ProcurementContactEmail"];
			entity.CompanyName = (Convert.IsDBNull(dataRow["CompanyName"]))?string.Empty:(System.String)dataRow["CompanyName"];
			entity.ActionDescription = (Convert.IsDBNull(dataRow["ActionDescription"]))?string.Empty:(System.String)dataRow["ActionDescription"];
			entity.UserDescription = (Convert.IsDBNull(dataRow["UserDescription"]))?string.Empty:(System.String)dataRow["UserDescription"];
			entity.QuestionnairePresentlyWithSince = (Convert.IsDBNull(dataRow["QuestionnairePresentlyWithSince"]))?DateTime.MinValue:(System.DateTime?)dataRow["QuestionnairePresentlyWithSince"];
			entity.DaysWith = (Convert.IsDBNull(dataRow["DaysWith"]))?(int)0:(System.Int32?)dataRow["DaysWith"];
			entity.AcceptChanges();
		}
		*/
			
		#endregion Helper Functions
	}//end class

	#region QuestionnaireEscalationProcurementListFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireEscalationProcurementList"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireEscalationProcurementListFilterBuilder : SqlFilterBuilder<QuestionnaireEscalationProcurementListColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireEscalationProcurementListFilterBuilder class.
		/// </summary>
		public QuestionnaireEscalationProcurementListFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireEscalationProcurementListFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireEscalationProcurementListFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireEscalationProcurementListFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireEscalationProcurementListFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireEscalationProcurementListFilterBuilder

	#region QuestionnaireEscalationProcurementListParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireEscalationProcurementList"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireEscalationProcurementListParameterBuilder : ParameterizedSqlFilterBuilder<QuestionnaireEscalationProcurementListColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireEscalationProcurementListParameterBuilder class.
		/// </summary>
		public QuestionnaireEscalationProcurementListParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireEscalationProcurementListParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireEscalationProcurementListParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireEscalationProcurementListParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireEscalationProcurementListParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireEscalationProcurementListParameterBuilder
	
	#region QuestionnaireEscalationProcurementListSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireEscalationProcurementList"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class QuestionnaireEscalationProcurementListSortBuilder : SqlSortBuilder<QuestionnaireEscalationProcurementListColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireEscalationProcurementListSqlSortBuilder class.
		/// </summary>
		public QuestionnaireEscalationProcurementListSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion QuestionnaireEscalationProcurementListSortBuilder

} // end namespace
