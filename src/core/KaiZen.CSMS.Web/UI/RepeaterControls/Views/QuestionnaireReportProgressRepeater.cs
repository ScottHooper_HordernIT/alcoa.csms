﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.ComponentModel;
using System.Web.UI.Design.WebControls;
using System.Web.UI.Design;
using System.Web.UI.WebControls;

namespace KaiZen.CSMS.Web.UI
{
    /// <summary>
    /// A designer class for a strongly typed repeater <c>QuestionnaireReportProgressRepeater</c>
    /// </summary>
	public class QuestionnaireReportProgressRepeaterDesigner : System.Web.UI.Design.ControlDesigner
	{
	    /// <summary>
        /// Initializes a new instance of the <see cref="T:QuestionnaireReportProgressRepeaterDesigner"/> class.
        /// </summary>
		public QuestionnaireReportProgressRepeaterDesigner()
		{
		}

        /// <summary>
        /// Initializes the control designer and loads the specified component.
        /// </summary>
        /// <param name="component">The control being designed.</param>
		public override void Initialize(IComponent component)
		{
			if (!(component is QuestionnaireReportProgressRepeater))
			{ 
				throw new ArgumentException("Component is not a QuestionnaireReportProgressRepeater."); 
			} 
			base.Initialize(component); 
			base.SetViewFlags(ViewFlags.TemplateEditing, true); 
		}


		/// <summary>
		/// Generate HTML for the designer
		/// </summary>
		/// <returns>a string of design time HTML</returns>
		public override string GetDesignTimeHtml()
		{

			// Get the instance this designer applies to
			//
			QuestionnaireReportProgressRepeater z = (QuestionnaireReportProgressRepeater)Component;
			z.DataBind();

			return base.GetDesignTimeHtml();
		}
	}

    /// <summary>
    /// A strongly typed repeater control for the <see cref="QuestionnaireReportProgressRepeater"/> Type.
    /// </summary>
	[Designer(typeof(QuestionnaireReportProgressRepeaterDesigner))]
	[ParseChildren(true)]
	[ToolboxData("<{0}:QuestionnaireReportProgressRepeater runat=\"server\"></{0}:QuestionnaireReportProgressRepeater>")]
	public class QuestionnaireReportProgressRepeater : CompositeDataBoundControl, System.Web.UI.INamingContainer
	{
	    /// <summary>
        /// Initializes a new instance of the <see cref="T:QuestionnaireReportProgressRepeater"/> class.
        /// </summary>
		public QuestionnaireReportProgressRepeater()
		{
		}

		/// <summary>
        /// Gets a <see cref="T:System.Web.UI.ControlCollection"></see> object that represents the child controls for a specified server control in the UI hierarchy.
        /// </summary>
        /// <value></value>
        /// <returns>The collection of child controls for the specified server control.</returns>
		public override ControlCollection Controls
		{
			get
			{
				this.EnsureChildControls();
				return base.Controls;
			}
		}

		private ITemplate m_headerTemplate;
		/// <summary>
        /// Gets or sets the header template.
        /// </summary>
        /// <value>The header template.</value>
		[Browsable(false)]
		[TemplateContainer(typeof(QuestionnaireReportProgressItem))]
		[PersistenceMode(PersistenceMode.InnerDefaultProperty)]
		public ITemplate HeaderTemplate
		{
			get { return m_headerTemplate; }
			set { m_headerTemplate = value; }
		}

		private ITemplate m_itemTemplate;
		/// <summary>
        /// Gets or sets the item template.
        /// </summary>
        /// <value>The item template.</value>
		[Browsable(false)]
		[TemplateContainer(typeof(QuestionnaireReportProgressItem))]
		[PersistenceMode(PersistenceMode.InnerDefaultProperty)]
		public ITemplate ItemTemplate
		{
			get { return m_itemTemplate; }
			set { m_itemTemplate = value; }
		}

		private ITemplate m_seperatorTemplate;
		/// <summary>
        /// Gets or sets the Seperator Template
        /// </summary>
        [Browsable(false)]
        [TemplateContainer(typeof(QuestionnaireReportProgressItem))]
        [PersistenceMode(PersistenceMode.InnerDefaultProperty)]
        public ITemplate SeperatorTemplate
        {
            get { return m_seperatorTemplate; }
            set { m_seperatorTemplate = value; }
        }

		private ITemplate m_altenateItemTemplate;
        /// <summary>
        /// Gets or sets the alternating item template.
        /// </summary>
        /// <value>The alternating item template.</value>
		[Browsable(false)]
		[TemplateContainer(typeof(QuestionnaireReportProgressItem))]
		[PersistenceMode(PersistenceMode.InnerDefaultProperty)]
		public ITemplate AlternatingItemTemplate
		{
			get { return m_altenateItemTemplate; }
			set { m_altenateItemTemplate = value; }
		}

		private ITemplate m_footerTemplate;
        /// <summary>
        /// Gets or sets the footer template.
        /// </summary>
        /// <value>The footer template.</value>
		[Browsable(false)]
		[TemplateContainer(typeof(QuestionnaireReportProgressItem))]
		[PersistenceMode(PersistenceMode.InnerDefaultProperty)]
		public ITemplate FooterTemplate
		{
			get { return m_footerTemplate; }
			set { m_footerTemplate = value; }
		}
		
		
		/// <summary>
        /// Overridden and Empty so that span tags are not written
        /// </summary>
        /// <param name="writer"></param>
        public override void RenderBeginTag(HtmlTextWriter writer)
        {
            
        }

        /// <summary>
        /// Overridden and Empty so that span tags are not written
        /// </summary>
        /// <param name="writer"></param>
        public override void RenderEndTag(HtmlTextWriter writer)
        {
                
        }		

//      /// <summary>
//      /// Called by the ASP.NET page framework to notify server controls that use composition-based implementation to create any child controls they contain in preparation for posting back or rendering.
//      /// </summary>
//		protected override void CreateChildControls()
//		{
//			if (ChildControlsCreated)
//			{
//				return;
//			}
//			Controls.Clear();
//
//			if (m_headerTemplate != null)
//			{
//				Control headerItem = new Control();
//				m_headerTemplate.InstantiateIn(headerItem);
//				Controls.Add(headerItem);
//			}
//
//			
//			if (m_footerTemplate != null)
//			{
//				Control footerItem = new Control();
//				m_footerTemplate.InstantiateIn(footerItem);
//				Controls.Add(footerItem);
//			}
//			ChildControlsCreated = true;
//		}
		
		/// <summary>
      /// Called by the ASP.NET page framework to notify server controls that use composition-based implementation to create any child controls they contain in preparation for posting back or rendering.
      /// </summary>
		protected override int CreateChildControls(System.Collections.IEnumerable dataSource, bool dataBinding)
      {
         int pos = 0;

         if (dataBinding)
         {
            //Instantiate the Header template (if exists)
            if (m_headerTemplate != null)
            {
                Control headerItem = new Control();
                m_headerTemplate.InstantiateIn(headerItem);
                Controls.Add(headerItem);
            }
			if (dataSource != null)
			{
				foreach (object o in dataSource)
				{
						KaiZen.CSMS.Entities.QuestionnaireReportProgress entity = o as KaiZen.CSMS.Entities.QuestionnaireReportProgress;
						QuestionnaireReportProgressItem container = new QuestionnaireReportProgressItem(entity);
	
						if (m_itemTemplate != null && (pos % 2) == 0)
						{
							m_itemTemplate.InstantiateIn(container);
							
							if (m_seperatorTemplate != null)
							{
								m_seperatorTemplate.InstantiateIn(container);
							}
						}
						else
						{
							if (m_altenateItemTemplate != null)
							{
								m_altenateItemTemplate.InstantiateIn(container);
								if (m_seperatorTemplate != null)
								{
									m_seperatorTemplate.InstantiateIn(container);
								}

							}
							else if (m_itemTemplate != null)
							{
								m_itemTemplate.InstantiateIn(container);
								if (m_seperatorTemplate != null)
								{
									m_seperatorTemplate.InstantiateIn(container);
								}

							}
							else
							{
								// no template !!!
							}
						}
						Controls.Add(container);
						
						container.DataBind();
						
						pos++;
				}
			}            
			//Instantiate the Footer template (if exists)
            if (m_footerTemplate != null)
            {
                Control footerItem = new Control();
                m_footerTemplate.InstantiateIn(footerItem);
                Controls.Add(footerItem);
            }				
		 }
			
			return pos;
		}
		
      /// <summary>
      /// Raises the <see cref="E:System.Web.UI.Control.PreRender"></see> event.
      /// </summary>
      /// <param name="e">An <see cref="T:System.EventArgs"></see> object that contains the event data.</param>
		protected override void OnPreRender(EventArgs e)
		{
			base.DataBind();
		}

		#region Design time
        /// <summary>
        /// Renders at design time.
        /// </summary>
        /// <returns>a  string of the Designed HTML</returns>
		internal string RenderAtDesignTime()
		{			
			return "Designer currently not implemented"; 
		}

		#endregion
	}

    /// <summary>
    /// A wrapper type for the entity
    /// </summary>
	[System.ComponentModel.ToolboxItem(false)]
	public class QuestionnaireReportProgressItem : System.Web.UI.Control, System.Web.UI.INamingContainer
	{
		private KaiZen.CSMS.Entities.QuestionnaireReportProgress _entity;

        /// <summary>
        /// Initializes a new instance of the <see cref="T:QuestionnaireReportProgressItem"/> class.
        /// </summary>
		public QuestionnaireReportProgressItem()
			: base()
		{ }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:QuestionnaireReportProgressItem"/> class.
        /// </summary>
		public QuestionnaireReportProgressItem(KaiZen.CSMS.Entities.QuestionnaireReportProgress entity)
			: base()
		{
			_entity = entity;
		}
		
        /// <summary>
        /// Gets the CompanyId
        /// </summary>
        /// <value>The CompanyId.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32 CompanyId
		{
			get { return _entity.CompanyId; }
		}
        /// <summary>
        /// Gets the CompanyName
        /// </summary>
        /// <value>The CompanyName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String CompanyName
		{
			get { return _entity.CompanyName; }
		}
        /// <summary>
        /// Gets the Status
        /// </summary>
        /// <value>The Status.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32 Status
		{
			get { return _entity.Status; }
		}
        /// <summary>
        /// Gets the InitialModifiedDate
        /// </summary>
        /// <value>The InitialModifiedDate.</value>
		[System.ComponentModel.Bindable(true)]
		public System.DateTime? InitialModifiedDate
		{
			get { return _entity.InitialModifiedDate; }
		}
        /// <summary>
        /// Gets the SupplierContact
        /// </summary>
        /// <value>The SupplierContact.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String SupplierContact
		{
			get { return _entity.SupplierContact; }
		}
        /// <summary>
        /// Gets the SupplierContactEmail
        /// </summary>
        /// <value>The SupplierContactEmail.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String SupplierContactEmail
		{
			get { return _entity.SupplierContactEmail; }
		}
        /// <summary>
        /// Gets the SupplierContactPhone
        /// </summary>
        /// <value>The SupplierContactPhone.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String SupplierContactPhone
		{
			get { return _entity.SupplierContactPhone; }
		}
        /// <summary>
        /// Gets the SupplierContactEmailPhone
        /// </summary>
        /// <value>The SupplierContactEmailPhone.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String SupplierContactEmailPhone
		{
			get { return _entity.SupplierContactEmailPhone; }
		}
        /// <summary>
        /// Gets the Deactivated
        /// </summary>
        /// <value>The Deactivated.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Boolean? Deactivated
		{
			get { return _entity.Deactivated; }
		}
        /// <summary>
        /// Gets the Deactivated2
        /// </summary>
        /// <value>The Deactivated2.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Boolean Deactivated2
		{
			get { return _entity.Deactivated2; }
		}
        /// <summary>
        /// Gets the PresentlyWith
        /// </summary>
        /// <value>The PresentlyWith.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String PresentlyWith
		{
			get { return _entity.PresentlyWith; }
		}
        /// <summary>
        /// Gets the PresentlyWithDetails
        /// </summary>
        /// <value>The PresentlyWithDetails.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String PresentlyWithDetails
		{
			get { return _entity.PresentlyWithDetails; }
		}
        /// <summary>
        /// Gets the DaysWith
        /// </summary>
        /// <value>The DaysWith.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? DaysWith
		{
			get { return _entity.DaysWith; }
		}
        /// <summary>
        /// Gets the QmaLastModified
        /// </summary>
        /// <value>The QmaLastModified.</value>
		[System.ComponentModel.Bindable(true)]
		public System.DateTime? QmaLastModified
		{
			get { return _entity.QmaLastModified; }
		}
        /// <summary>
        /// Gets the QvaLastModified
        /// </summary>
        /// <value>The QvaLastModified.</value>
		[System.ComponentModel.Bindable(true)]
		public System.DateTime? QvaLastModified
		{
			get { return _entity.QvaLastModified; }
		}
        /// <summary>
        /// Gets the Type
        /// </summary>
        /// <value>The Type.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Type
		{
			get { return _entity.Type; }
		}
        /// <summary>
        /// Gets the InitialSpa
        /// </summary>
        /// <value>The InitialSpa.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String InitialSpa
		{
			get { return _entity.InitialSpa; }
		}
        /// <summary>
        /// Gets the MainModifiedDate
        /// </summary>
        /// <value>The MainModifiedDate.</value>
		[System.ComponentModel.Bindable(true)]
		public System.DateTime? MainModifiedDate
		{
			get { return _entity.MainModifiedDate; }
		}
        /// <summary>
        /// Gets the MainModifiedByUserId
        /// </summary>
        /// <value>The MainModifiedByUserId.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? MainModifiedByUserId
		{
			get { return _entity.MainModifiedByUserId; }
		}
        /// <summary>
        /// Gets the MainModifiedByUser
        /// </summary>
        /// <value>The MainModifiedByUser.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String MainModifiedByUser
		{
			get { return _entity.MainModifiedByUser; }
		}
        /// <summary>
        /// Gets the VerificationModifiedDate
        /// </summary>
        /// <value>The VerificationModifiedDate.</value>
		[System.ComponentModel.Bindable(true)]
		public System.DateTime? VerificationModifiedDate
		{
			get { return _entity.VerificationModifiedDate; }
		}
        /// <summary>
        /// Gets the EhsConsultantId
        /// </summary>
        /// <value>The EhsConsultantId.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? EhsConsultantId
		{
			get { return _entity.EhsConsultantId; }
		}
        /// <summary>
        /// Gets the IsMainRequired
        /// </summary>
        /// <value>The IsMainRequired.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Boolean IsMainRequired
		{
			get { return _entity.IsMainRequired; }
		}
        /// <summary>
        /// Gets the IsVerificationRequired
        /// </summary>
        /// <value>The IsVerificationRequired.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Boolean IsVerificationRequired
		{
			get { return _entity.IsVerificationRequired; }
		}
        /// <summary>
        /// Gets the MainAssessmentRiskRating
        /// </summary>
        /// <value>The MainAssessmentRiskRating.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String MainAssessmentRiskRating
		{
			get { return _entity.MainAssessmentRiskRating; }
		}
        /// <summary>
        /// Gets the FinalRiskRating
        /// </summary>
        /// <value>The FinalRiskRating.</value>
        [System.ComponentModel.Bindable(true)]
        public System.String FinalRiskRating
        {
            get { return _entity.FinalRiskRating; }
        }
        /// <summary>
        /// Gets the Recommended
        /// </summary>
        /// <value>The Recommended.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Boolean? Recommended
		{
			get { return _entity.Recommended; }
		}
        /// <summary>
        /// Gets the QuestionnaireId
        /// </summary>
        /// <value>The QuestionnaireId.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32 QuestionnaireId
		{
			get { return _entity.QuestionnaireId; }
		}
        /// <summary>
        /// Gets the MainScorePoverall
        /// </summary>
        /// <value>The MainScorePoverall.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? MainScorePoverall
		{
			get { return _entity.MainScorePoverall; }
		}
        /// <summary>
        /// Gets the CompanyStatusId
        /// </summary>
        /// <value>The CompanyStatusId.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32 CompanyStatusId
		{
			get { return _entity.CompanyStatusId; }
		}
        /// <summary>
        /// Gets the CompanyStatusDesc
        /// </summary>
        /// <value>The CompanyStatusDesc.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String CompanyStatusDesc
		{
			get { return _entity.CompanyStatusDesc; }
		}
        /// <summary>
        /// Gets the CompanyStatusName
        /// </summary>
        /// <value>The CompanyStatusName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String CompanyStatusName
		{
			get { return _entity.CompanyStatusName; }
		}
        /// <summary>
        /// Gets the CreatedByUser
        /// </summary>
        /// <value>The CreatedByUser.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String CreatedByUser
		{
			get { return _entity.CreatedByUser; }
		}
        /// <summary>
        /// Gets the CreatedByUserId
        /// </summary>
        /// <value>The CreatedByUserId.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32 CreatedByUserId
		{
			get { return _entity.CreatedByUserId; }
		}
        /// <summary>
        /// Gets the ModifiedByUser
        /// </summary>
        /// <value>The ModifiedByUser.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String ModifiedByUser
		{
			get { return _entity.ModifiedByUser; }
		}
        /// <summary>
        /// Gets the ModifiedByUserId
        /// </summary>
        /// <value>The ModifiedByUserId.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32 ModifiedByUserId
		{
			get { return _entity.ModifiedByUserId; }
		}
        /// <summary>
        /// Gets the InitialRiskAssessment
        /// </summary>
        /// <value>The InitialRiskAssessment.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String InitialRiskAssessment
		{
			get { return _entity.InitialRiskAssessment; }
		}
        /// <summary>
        /// Gets the SafetyAssessor
        /// </summary>
        /// <value>The SafetyAssessor.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String SafetyAssessor
		{
			get { return _entity.SafetyAssessor; }
		}
        /// <summary>
        /// Gets the SafetyAssessorPhone
        /// </summary>
        /// <value>The SafetyAssessorPhone.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String SafetyAssessorPhone
		{
			get { return _entity.SafetyAssessorPhone; }
		}
        /// <summary>
        /// Gets the SafetyAssessorSite
        /// </summary>
        /// <value>The SafetyAssessorSite.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String SafetyAssessorSite
		{
			get { return _entity.SafetyAssessorSite; }
		}
        /// <summary>
        /// Gets the SafetyAssessorRegion
        /// </summary>
        /// <value>The SafetyAssessorRegion.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String SafetyAssessorRegion
		{
			get { return _entity.SafetyAssessorRegion; }
		}
        /// <summary>
        /// Gets the ProcurementContact
        /// </summary>
        /// <value>The ProcurementContact.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String ProcurementContact
		{
			get { return _entity.ProcurementContact; }
		}
        /// <summary>
        /// Gets the ProcurementSite
        /// </summary>
        /// <value>The ProcurementSite.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String ProcurementSite
		{
			get { return _entity.ProcurementSite; }
		}
        /// <summary>
        /// Gets the ProcurementRegion
        /// </summary>
        /// <value>The ProcurementRegion.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String ProcurementRegion
		{
			get { return _entity.ProcurementRegion; }
		}
        /// <summary>
        /// Gets the ProcurementContactUser
        /// </summary>
        /// <value>The ProcurementContactUser.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String ProcurementContactUser
		{
			get { return _entity.ProcurementContactUser; }
		}
        /// <summary>
        /// Gets the ContractManager
        /// </summary>
        /// <value>The ContractManager.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String ContractManager
		{
			get { return _entity.ContractManager; }
		}
        /// <summary>
        /// Gets the ContractManagerUser
        /// </summary>
        /// <value>The ContractManagerUser.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String ContractManagerUser
		{
			get { return _entity.ContractManagerUser; }
		}
        /// <summary>
        /// Gets the QuestionnairePresentlyWithActionId
        /// </summary>
        /// <value>The QuestionnairePresentlyWithActionId.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? QuestionnairePresentlyWithActionId
		{
			get { return _entity.QuestionnairePresentlyWithActionId; }
		}
        /// <summary>
        /// Gets the QuestionnairePresentlyWithSince
        /// </summary>
        /// <value>The QuestionnairePresentlyWithSince.</value>
		[System.ComponentModel.Bindable(true)]
		public System.DateTime? QuestionnairePresentlyWithSince
		{
			get { return _entity.QuestionnairePresentlyWithSince; }
		}
        /// <summary>
        /// Gets the ProcessNo
        /// </summary>
        /// <value>The ProcessNo.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? ProcessNo
		{
			get { return _entity.ProcessNo; }
		}
        /// <summary>
        /// Gets the UserName
        /// </summary>
        /// <value>The UserName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String UserName
		{
			get { return _entity.UserName; }
		}
        /// <summary>
        /// Gets the ActionName
        /// </summary>
        /// <value>The ActionName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String ActionName
		{
			get { return _entity.ActionName; }
		}
        /// <summary>
        /// Gets the UserDescription
        /// </summary>
        /// <value>The UserDescription.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String UserDescription
		{
			get { return _entity.UserDescription; }
		}
        /// <summary>
        /// Gets the ActionDescription
        /// </summary>
        /// <value>The ActionDescription.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String ActionDescription
		{
			get { return _entity.ActionDescription; }
		}
        /// <summary>
        /// Gets the QuestionnairePresentlyWithSinceDaysCount
        /// </summary>
        /// <value>The QuestionnairePresentlyWithSinceDaysCount.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? QuestionnairePresentlyWithSinceDaysCount
		{
			get { return _entity.QuestionnairePresentlyWithSinceDaysCount; }
		}
        /// <summary>
        /// Gets the RequestingCompanyId
        /// </summary>
        /// <value>The RequestingCompanyId.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? RequestingCompanyId
		{
			get { return _entity.RequestingCompanyId; }
		}
        /// <summary>
        /// Gets the RequestingCompanyName
        /// </summary>
        /// <value>The RequestingCompanyName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String RequestingCompanyName
		{
			get { return _entity.RequestingCompanyName; }
		}
        /// <summary>
        /// Gets the ReasonContractor
        /// </summary>
        /// <value>The ReasonContractor.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String ReasonContractor
		{
			get { return _entity.ReasonContractor; }
		}
        /// <summary>
        /// Gets the ServicesMain
        /// </summary>
        /// <value>The ServicesMain.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String ServicesMain
		{
			get { return _entity.ServicesMain; }
		}
        /// <summary>
        /// Gets the ServicesOther
        /// </summary>
        /// <value>The ServicesOther.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String ServicesOther
		{
			get { return _entity.ServicesOther; }
		}
        /// <summary>
        /// Gets the CreatedDate
        /// </summary>
        /// <value>The CreatedDate.</value>
		[System.ComponentModel.Bindable(true)]
		public System.DateTime CreatedDate
		{
			get { return _entity.CreatedDate; }
		}
        /// <summary>
        /// Gets the IsReQualification
        /// </summary>
        /// <value>The IsReQualification.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Boolean IsReQualification
		{
			get { return _entity.IsReQualification; }
		}
        /// <summary>
        /// Gets the MainAssessmentValidTo
        /// </summary>
        /// <value>The MainAssessmentValidTo.</value>
		[System.ComponentModel.Bindable(true)]
		public System.DateTime? MainAssessmentValidTo
		{
			get { return _entity.MainAssessmentValidTo; }
		}

	}
}
