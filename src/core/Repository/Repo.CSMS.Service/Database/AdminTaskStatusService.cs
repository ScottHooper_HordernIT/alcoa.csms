﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface IAdminTaskStatusService
    {
        AdminTaskStatus Get(Expression<Func<AdminTaskStatus, bool>> where, List<Expression<Func<AdminTaskStatus, object>>> includes);
        List<AdminTaskStatus> GetMany(int? top, Expression<Func<AdminTaskStatus, bool>> where, Func<IQueryable<AdminTaskStatus>, IOrderedQueryable<AdminTaskStatus>> orderBy, List<Expression<Func<AdminTaskStatus, object>>> includes);
        List<AdminTaskStatus> GetManyPaged(int pageNumber, int pageSize, Expression<Func<AdminTaskStatus, bool>> where, Func<IQueryable<AdminTaskStatus>, IOrderedQueryable<AdminTaskStatus>> orderBy, List<Expression<Func<AdminTaskStatus, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<AdminTaskStatus, bool>> where, List<Expression<Func<AdminTaskStatus, object>>> includes);
        void Insert(AdminTaskStatus item, bool saveChanges = true);
        void Update(AdminTaskStatus item, bool saveChanges = true);
        void Delete(AdminTaskStatus item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class AdminTaskStatusService : IAdminTaskStatusService
    {
        private readonly IRepository<AdminTaskStatus> _repository;

        public AdminTaskStatusService(IRepository<AdminTaskStatus> repository)
        {
            this._repository = repository;
        }

        public AdminTaskStatus Get(Expression<Func<AdminTaskStatus, bool>> where, List<Expression<Func<AdminTaskStatus, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<AdminTaskStatus> GetMany(int? top, Expression<Func<AdminTaskStatus, bool>> where, Func<IQueryable<AdminTaskStatus>, IOrderedQueryable<AdminTaskStatus>> orderBy, List<Expression<Func<AdminTaskStatus, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<AdminTaskStatus> GetManyPaged(int pageNumber, int pageSize, Expression<Func<AdminTaskStatus, bool>> where, Func<IQueryable<AdminTaskStatus>, IOrderedQueryable<AdminTaskStatus>> orderBy, List<Expression<Func<AdminTaskStatus, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<AdminTaskStatus, bool>> where, List<Expression<Func<AdminTaskStatus, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(AdminTaskStatus item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("AdminTaskStatus");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(AdminTaskStatus item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("AdminTaskStatus");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(AdminTaskStatus item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("AdminTaskStatus");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
