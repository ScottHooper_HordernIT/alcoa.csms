﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.QuestionnaireReportTimeDelayBeingAssessedProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[CLSCompliant(true)]
	[Designer(typeof(QuestionnaireReportTimeDelayBeingAssessedDataSourceDesigner))]
	public class QuestionnaireReportTimeDelayBeingAssessedDataSource : ReadOnlyDataSource<QuestionnaireReportTimeDelayBeingAssessed>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportTimeDelayBeingAssessedDataSource class.
		/// </summary>
		public QuestionnaireReportTimeDelayBeingAssessedDataSource() : base(new QuestionnaireReportTimeDelayBeingAssessedService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the QuestionnaireReportTimeDelayBeingAssessedDataSourceView used by the QuestionnaireReportTimeDelayBeingAssessedDataSource.
		/// </summary>
		protected QuestionnaireReportTimeDelayBeingAssessedDataSourceView QuestionnaireReportTimeDelayBeingAssessedView
		{
			get { return ( View as QuestionnaireReportTimeDelayBeingAssessedDataSourceView ); }
		}
		
		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the QuestionnaireReportTimeDelayBeingAssessedDataSourceView class that is to be
		/// used by the QuestionnaireReportTimeDelayBeingAssessedDataSource.
		/// </summary>
		/// <returns>An instance of the QuestionnaireReportTimeDelayBeingAssessedDataSourceView class.</returns>
		protected override BaseDataSourceView<QuestionnaireReportTimeDelayBeingAssessed, Object> GetNewDataSourceView()
		{
			return new QuestionnaireReportTimeDelayBeingAssessedDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the QuestionnaireReportTimeDelayBeingAssessedDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class QuestionnaireReportTimeDelayBeingAssessedDataSourceView : ReadOnlyDataSourceView<QuestionnaireReportTimeDelayBeingAssessed>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportTimeDelayBeingAssessedDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the QuestionnaireReportTimeDelayBeingAssessedDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public QuestionnaireReportTimeDelayBeingAssessedDataSourceView(QuestionnaireReportTimeDelayBeingAssessedDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal QuestionnaireReportTimeDelayBeingAssessedDataSource QuestionnaireReportTimeDelayBeingAssessedOwner
		{
			get { return Owner as QuestionnaireReportTimeDelayBeingAssessedDataSource; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal QuestionnaireReportTimeDelayBeingAssessedService QuestionnaireReportTimeDelayBeingAssessedProvider
		{
			get { return Provider as QuestionnaireReportTimeDelayBeingAssessedService; }
		}

		#endregion Properties
		
		#region Methods
		
		#endregion Methods
	}

	#region QuestionnaireReportTimeDelayBeingAssessedDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the QuestionnaireReportTimeDelayBeingAssessedDataSource class.
	/// </summary>
	public class QuestionnaireReportTimeDelayBeingAssessedDataSourceDesigner : ReadOnlyDataSourceDesigner<QuestionnaireReportTimeDelayBeingAssessed>
	{
	}

	#endregion QuestionnaireReportTimeDelayBeingAssessedDataSourceDesigner

	#region QuestionnaireReportTimeDelayBeingAssessedFilter

	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireReportTimeDelayBeingAssessed"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireReportTimeDelayBeingAssessedFilter : SqlFilter<QuestionnaireReportTimeDelayBeingAssessedColumn>
	{
	}

	#endregion QuestionnaireReportTimeDelayBeingAssessedFilter

	#region QuestionnaireReportTimeDelayBeingAssessedExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireReportTimeDelayBeingAssessed"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireReportTimeDelayBeingAssessedExpressionBuilder : SqlExpressionBuilder<QuestionnaireReportTimeDelayBeingAssessedColumn>
	{
	}
	
	#endregion QuestionnaireReportTimeDelayBeingAssessedExpressionBuilder		
}

