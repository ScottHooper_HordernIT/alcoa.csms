﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.ComponentModel;
using System.Web.UI.Design.WebControls;
using System.Web.UI.Design;
using System.Web.UI.WebControls;

namespace KaiZen.CSMS.Web.UI
{
    /// <summary>
    /// A designer class for a strongly typed repeater <c>KpiEngineeringProjectHoursRepeater</c>
    /// </summary>
	public class KpiEngineeringProjectHoursRepeaterDesigner : System.Web.UI.Design.ControlDesigner
	{
	    /// <summary>
        /// Initializes a new instance of the <see cref="T:KpiEngineeringProjectHoursRepeaterDesigner"/> class.
        /// </summary>
		public KpiEngineeringProjectHoursRepeaterDesigner()
		{
		}

        /// <summary>
        /// Initializes the control designer and loads the specified component.
        /// </summary>
        /// <param name="component">The control being designed.</param>
		public override void Initialize(IComponent component)
		{
			if (!(component is KpiEngineeringProjectHoursRepeater))
			{ 
				throw new ArgumentException("Component is not a KpiEngineeringProjectHoursRepeater."); 
			} 
			base.Initialize(component); 
			base.SetViewFlags(ViewFlags.TemplateEditing, true); 
		}


		/// <summary>
		/// Generate HTML for the designer
		/// </summary>
		/// <returns>a string of design time HTML</returns>
		public override string GetDesignTimeHtml()
		{

			// Get the instance this designer applies to
			//
			KpiEngineeringProjectHoursRepeater z = (KpiEngineeringProjectHoursRepeater)Component;
			z.DataBind();

			return base.GetDesignTimeHtml();
		}
	}

    /// <summary>
    /// A strongly typed repeater control for the <see cref="KpiEngineeringProjectHoursRepeater"/> Type.
    /// </summary>
	[Designer(typeof(KpiEngineeringProjectHoursRepeaterDesigner))]
	[ParseChildren(true)]
	[ToolboxData("<{0}:KpiEngineeringProjectHoursRepeater runat=\"server\"></{0}:KpiEngineeringProjectHoursRepeater>")]
	public class KpiEngineeringProjectHoursRepeater : CompositeDataBoundControl, System.Web.UI.INamingContainer
	{
	    /// <summary>
        /// Initializes a new instance of the <see cref="T:KpiEngineeringProjectHoursRepeater"/> class.
        /// </summary>
		public KpiEngineeringProjectHoursRepeater()
		{
		}

		/// <summary>
        /// Gets a <see cref="T:System.Web.UI.ControlCollection"></see> object that represents the child controls for a specified server control in the UI hierarchy.
        /// </summary>
        /// <value></value>
        /// <returns>The collection of child controls for the specified server control.</returns>
		public override ControlCollection Controls
		{
			get
			{
				this.EnsureChildControls();
				return base.Controls;
			}
		}

		private ITemplate m_headerTemplate;
		/// <summary>
        /// Gets or sets the header template.
        /// </summary>
        /// <value>The header template.</value>
		[Browsable(false)]
		[TemplateContainer(typeof(KpiEngineeringProjectHoursItem))]
		[PersistenceMode(PersistenceMode.InnerDefaultProperty)]
		public ITemplate HeaderTemplate
		{
			get { return m_headerTemplate; }
			set { m_headerTemplate = value; }
		}

		private ITemplate m_itemTemplate;
		/// <summary>
        /// Gets or sets the item template.
        /// </summary>
        /// <value>The item template.</value>
		[Browsable(false)]
		[TemplateContainer(typeof(KpiEngineeringProjectHoursItem))]
		[PersistenceMode(PersistenceMode.InnerDefaultProperty)]
		public ITemplate ItemTemplate
		{
			get { return m_itemTemplate; }
			set { m_itemTemplate = value; }
		}

		private ITemplate m_seperatorTemplate;
		/// <summary>
        /// Gets or sets the Seperator Template
        /// </summary>
        [Browsable(false)]
        [TemplateContainer(typeof(KpiEngineeringProjectHoursItem))]
        [PersistenceMode(PersistenceMode.InnerDefaultProperty)]
        public ITemplate SeperatorTemplate
        {
            get { return m_seperatorTemplate; }
            set { m_seperatorTemplate = value; }
        }

		private ITemplate m_altenateItemTemplate;
        /// <summary>
        /// Gets or sets the alternating item template.
        /// </summary>
        /// <value>The alternating item template.</value>
		[Browsable(false)]
		[TemplateContainer(typeof(KpiEngineeringProjectHoursItem))]
		[PersistenceMode(PersistenceMode.InnerDefaultProperty)]
		public ITemplate AlternatingItemTemplate
		{
			get { return m_altenateItemTemplate; }
			set { m_altenateItemTemplate = value; }
		}

		private ITemplate m_footerTemplate;
        /// <summary>
        /// Gets or sets the footer template.
        /// </summary>
        /// <value>The footer template.</value>
		[Browsable(false)]
		[TemplateContainer(typeof(KpiEngineeringProjectHoursItem))]
		[PersistenceMode(PersistenceMode.InnerDefaultProperty)]
		public ITemplate FooterTemplate
		{
			get { return m_footerTemplate; }
			set { m_footerTemplate = value; }
		}
		
		
		/// <summary>
        /// Overridden and Empty so that span tags are not written
        /// </summary>
        /// <param name="writer"></param>
        public override void RenderBeginTag(HtmlTextWriter writer)
        {
            
        }

        /// <summary>
        /// Overridden and Empty so that span tags are not written
        /// </summary>
        /// <param name="writer"></param>
        public override void RenderEndTag(HtmlTextWriter writer)
        {
                
        }		

//      /// <summary>
//      /// Called by the ASP.NET page framework to notify server controls that use composition-based implementation to create any child controls they contain in preparation for posting back or rendering.
//      /// </summary>
//		protected override void CreateChildControls()
//		{
//			if (ChildControlsCreated)
//			{
//				return;
//			}
//			Controls.Clear();
//
//			if (m_headerTemplate != null)
//			{
//				Control headerItem = new Control();
//				m_headerTemplate.InstantiateIn(headerItem);
//				Controls.Add(headerItem);
//			}
//
//			
//			if (m_footerTemplate != null)
//			{
//				Control footerItem = new Control();
//				m_footerTemplate.InstantiateIn(footerItem);
//				Controls.Add(footerItem);
//			}
//			ChildControlsCreated = true;
//		}
		
		/// <summary>
      /// Called by the ASP.NET page framework to notify server controls that use composition-based implementation to create any child controls they contain in preparation for posting back or rendering.
      /// </summary>
		protected override int CreateChildControls(System.Collections.IEnumerable dataSource, bool dataBinding)
      {
         int pos = 0;

         if (dataBinding)
         {
            //Instantiate the Header template (if exists)
            if (m_headerTemplate != null)
            {
                Control headerItem = new Control();
                m_headerTemplate.InstantiateIn(headerItem);
                Controls.Add(headerItem);
            }
			if (dataSource != null)
			{
				foreach (object o in dataSource)
				{
						KaiZen.CSMS.Entities.KpiEngineeringProjectHours entity = o as KaiZen.CSMS.Entities.KpiEngineeringProjectHours;
						KpiEngineeringProjectHoursItem container = new KpiEngineeringProjectHoursItem(entity);
	
						if (m_itemTemplate != null && (pos % 2) == 0)
						{
							m_itemTemplate.InstantiateIn(container);
							
							if (m_seperatorTemplate != null)
							{
								m_seperatorTemplate.InstantiateIn(container);
							}
						}
						else
						{
							if (m_altenateItemTemplate != null)
							{
								m_altenateItemTemplate.InstantiateIn(container);
								if (m_seperatorTemplate != null)
								{
									m_seperatorTemplate.InstantiateIn(container);
								}

							}
							else if (m_itemTemplate != null)
							{
								m_itemTemplate.InstantiateIn(container);
								if (m_seperatorTemplate != null)
								{
									m_seperatorTemplate.InstantiateIn(container);
								}

							}
							else
							{
								// no template !!!
							}
						}
						Controls.Add(container);
						
						container.DataBind();
						
						pos++;
				}
			}            
			//Instantiate the Footer template (if exists)
            if (m_footerTemplate != null)
            {
                Control footerItem = new Control();
                m_footerTemplate.InstantiateIn(footerItem);
                Controls.Add(footerItem);
            }				
		 }
			
			return pos;
		}
		
      /// <summary>
      /// Raises the <see cref="E:System.Web.UI.Control.PreRender"></see> event.
      /// </summary>
      /// <param name="e">An <see cref="T:System.EventArgs"></see> object that contains the event data.</param>
		protected override void OnPreRender(EventArgs e)
		{
			base.DataBind();
		}

		#region Design time
        /// <summary>
        /// Renders at design time.
        /// </summary>
        /// <returns>a  string of the Designed HTML</returns>
		internal string RenderAtDesignTime()
		{			
			return "Designer currently not implemented"; 
		}

		#endregion
	}

    /// <summary>
    /// A wrapper type for the entity
    /// </summary>
	[System.ComponentModel.ToolboxItem(false)]
	public class KpiEngineeringProjectHoursItem : System.Web.UI.Control, System.Web.UI.INamingContainer
	{
		private KaiZen.CSMS.Entities.KpiEngineeringProjectHours _entity;

        /// <summary>
        /// Initializes a new instance of the <see cref="T:KpiEngineeringProjectHoursItem"/> class.
        /// </summary>
		public KpiEngineeringProjectHoursItem()
			: base()
		{ }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:KpiEngineeringProjectHoursItem"/> class.
        /// </summary>
		public KpiEngineeringProjectHoursItem(KaiZen.CSMS.Entities.KpiEngineeringProjectHours entity)
			: base()
		{
			_entity = entity;
		}
		
        /// <summary>
        /// Gets the CompanyName
        /// </summary>
        /// <value>The CompanyName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String CompanyName
		{
			get { return _entity.CompanyName; }
		}
        /// <summary>
        /// Gets the SiteName
        /// </summary>
        /// <value>The SiteName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String SiteName
		{
			get { return _entity.SiteName; }
		}
        /// <summary>
        /// Gets the CompanySiteCategoryId
        /// </summary>
        /// <value>The CompanySiteCategoryId.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? CompanySiteCategoryId
		{
			get { return _entity.CompanySiteCategoryId; }
		}
        /// <summary>
        /// Gets the CompanySiteCategoryDesc
        /// </summary>
        /// <value>The CompanySiteCategoryDesc.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String CompanySiteCategoryDesc
		{
			get { return _entity.CompanySiteCategoryDesc; }
		}
        /// <summary>
        /// Gets the KpiDateTimeMonth
        /// </summary>
        /// <value>The KpiDateTimeMonth.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? KpiDateTimeMonth
		{
			get { return _entity.KpiDateTimeMonth; }
		}
        /// <summary>
        /// Gets the KpiDateTimeYear
        /// </summary>
        /// <value>The KpiDateTimeYear.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? KpiDateTimeYear
		{
			get { return _entity.KpiDateTimeYear; }
		}
        /// <summary>
        /// Gets the KpiDateTime
        /// </summary>
        /// <value>The KpiDateTime.</value>
		[System.ComponentModel.Bindable(true)]
		public System.DateTime KpiDateTime
		{
			get { return _entity.KpiDateTime; }
		}
        /// <summary>
        /// Gets the ProjectCapital1Title
        /// </summary>
        /// <value>The ProjectCapital1Title.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String ProjectCapital1Title
		{
			get { return _entity.ProjectCapital1Title; }
		}
        /// <summary>
        /// Gets the ProjectCapital1Hours
        /// </summary>
        /// <value>The ProjectCapital1Hours.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Decimal? ProjectCapital1Hours
		{
			get { return _entity.ProjectCapital1Hours; }
		}
        /// <summary>
        /// Gets the ProjectCapital2Title
        /// </summary>
        /// <value>The ProjectCapital2Title.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String ProjectCapital2Title
		{
			get { return _entity.ProjectCapital2Title; }
		}
        /// <summary>
        /// Gets the ProjectCapital2Hours
        /// </summary>
        /// <value>The ProjectCapital2Hours.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Decimal? ProjectCapital2Hours
		{
			get { return _entity.ProjectCapital2Hours; }
		}
        /// <summary>
        /// Gets the ProjectCapital3Title
        /// </summary>
        /// <value>The ProjectCapital3Title.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String ProjectCapital3Title
		{
			get { return _entity.ProjectCapital3Title; }
		}
        /// <summary>
        /// Gets the ProjectCapital3Hours
        /// </summary>
        /// <value>The ProjectCapital3Hours.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Decimal? ProjectCapital3Hours
		{
			get { return _entity.ProjectCapital3Hours; }
		}
        /// <summary>
        /// Gets the ProjectCapital4Title
        /// </summary>
        /// <value>The ProjectCapital4Title.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String ProjectCapital4Title
		{
			get { return _entity.ProjectCapital4Title; }
		}
        /// <summary>
        /// Gets the ProjectCapital4Hours
        /// </summary>
        /// <value>The ProjectCapital4Hours.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Decimal? ProjectCapital4Hours
		{
			get { return _entity.ProjectCapital4Hours; }
		}
        /// <summary>
        /// Gets the ProjectCapital5Title
        /// </summary>
        /// <value>The ProjectCapital5Title.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String ProjectCapital5Title
		{
			get { return _entity.ProjectCapital5Title; }
		}
        /// <summary>
        /// Gets the ProjectCapital5Hours
        /// </summary>
        /// <value>The ProjectCapital5Hours.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Decimal? ProjectCapital5Hours
		{
			get { return _entity.ProjectCapital5Hours; }
		}
        /// <summary>
        /// Gets the ProjectCapital6Title
        /// </summary>
        /// <value>The ProjectCapital6Title.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String ProjectCapital6Title
		{
			get { return _entity.ProjectCapital6Title; }
		}
        /// <summary>
        /// Gets the ProjectCapital6Hours
        /// </summary>
        /// <value>The ProjectCapital6Hours.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Decimal? ProjectCapital6Hours
		{
			get { return _entity.ProjectCapital6Hours; }
		}
        /// <summary>
        /// Gets the ProjectCapital7Title
        /// </summary>
        /// <value>The ProjectCapital7Title.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String ProjectCapital7Title
		{
			get { return _entity.ProjectCapital7Title; }
		}
        /// <summary>
        /// Gets the ProjectCapital7Hours
        /// </summary>
        /// <value>The ProjectCapital7Hours.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Decimal? ProjectCapital7Hours
		{
			get { return _entity.ProjectCapital7Hours; }
		}
        /// <summary>
        /// Gets the ProjectCapital8Title
        /// </summary>
        /// <value>The ProjectCapital8Title.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String ProjectCapital8Title
		{
			get { return _entity.ProjectCapital8Title; }
		}
        /// <summary>
        /// Gets the ProjectCapital8Hours
        /// </summary>
        /// <value>The ProjectCapital8Hours.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Decimal? ProjectCapital8Hours
		{
			get { return _entity.ProjectCapital8Hours; }
		}
        /// <summary>
        /// Gets the ProjectCapital9Title
        /// </summary>
        /// <value>The ProjectCapital9Title.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String ProjectCapital9Title
		{
			get { return _entity.ProjectCapital9Title; }
		}
        /// <summary>
        /// Gets the ProjectCapital9Hours
        /// </summary>
        /// <value>The ProjectCapital9Hours.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Decimal? ProjectCapital9Hours
		{
			get { return _entity.ProjectCapital9Hours; }
		}
        /// <summary>
        /// Gets the ProjectCapital10Title
        /// </summary>
        /// <value>The ProjectCapital10Title.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String ProjectCapital10Title
		{
			get { return _entity.ProjectCapital10Title; }
		}
        /// <summary>
        /// Gets the ProjectCapital10Hours
        /// </summary>
        /// <value>The ProjectCapital10Hours.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Decimal? ProjectCapital10Hours
		{
			get { return _entity.ProjectCapital10Hours; }
		}
        /// <summary>
        /// Gets the ProjectCapital11Title
        /// </summary>
        /// <value>The ProjectCapital11Title.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String ProjectCapital11Title
		{
			get { return _entity.ProjectCapital11Title; }
		}
        /// <summary>
        /// Gets the ProjectCapital11Hours
        /// </summary>
        /// <value>The ProjectCapital11Hours.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Decimal? ProjectCapital11Hours
		{
			get { return _entity.ProjectCapital11Hours; }
		}
        /// <summary>
        /// Gets the ProjectCapital12Title
        /// </summary>
        /// <value>The ProjectCapital12Title.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String ProjectCapital12Title
		{
			get { return _entity.ProjectCapital12Title; }
		}
        /// <summary>
        /// Gets the ProjectCapital12Hours
        /// </summary>
        /// <value>The ProjectCapital12Hours.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Decimal? ProjectCapital12Hours
		{
			get { return _entity.ProjectCapital12Hours; }
		}
        /// <summary>
        /// Gets the ProjectCapital13Title
        /// </summary>
        /// <value>The ProjectCapital13Title.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String ProjectCapital13Title
		{
			get { return _entity.ProjectCapital13Title; }
		}
        /// <summary>
        /// Gets the ProjectCapital13Hours
        /// </summary>
        /// <value>The ProjectCapital13Hours.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Decimal? ProjectCapital13Hours
		{
			get { return _entity.ProjectCapital13Hours; }
		}
        /// <summary>
        /// Gets the ProjectCapital14Title
        /// </summary>
        /// <value>The ProjectCapital14Title.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String ProjectCapital14Title
		{
			get { return _entity.ProjectCapital14Title; }
		}
        /// <summary>
        /// Gets the ProjectCapital14Hours
        /// </summary>
        /// <value>The ProjectCapital14Hours.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Decimal? ProjectCapital14Hours
		{
			get { return _entity.ProjectCapital14Hours; }
		}
        /// <summary>
        /// Gets the ProjectCapital15Title
        /// </summary>
        /// <value>The ProjectCapital15Title.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String ProjectCapital15Title
		{
			get { return _entity.ProjectCapital15Title; }
		}
        /// <summary>
        /// Gets the ProjectCapital15Hours
        /// </summary>
        /// <value>The ProjectCapital15Hours.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Decimal? ProjectCapital15Hours
		{
			get { return _entity.ProjectCapital15Hours; }
		}

	}
}
