﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="EmailLogProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class EmailLogProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.EmailLog, KaiZen.CSMS.Entities.EmailLogKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.EmailLogKey key)
		{
			return Delete(transactionManager, key.EmailLogId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_emailLogId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _emailLogId)
		{
			return Delete(null, _emailLogId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_emailLogId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _emailLogId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_EmailLog_EmailLogType key.
		///		FK_EmailLog_EmailLogType Description: 
		/// </summary>
		/// <param name="_emailLogTypeId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.EmailLog objects.</returns>
		public TList<EmailLog> GetByEmailLogTypeId(System.Int32 _emailLogTypeId)
		{
			int count = -1;
			return GetByEmailLogTypeId(_emailLogTypeId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_EmailLog_EmailLogType key.
		///		FK_EmailLog_EmailLogType Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_emailLogTypeId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.EmailLog objects.</returns>
		/// <remarks></remarks>
		public TList<EmailLog> GetByEmailLogTypeId(TransactionManager transactionManager, System.Int32 _emailLogTypeId)
		{
			int count = -1;
			return GetByEmailLogTypeId(transactionManager, _emailLogTypeId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_EmailLog_EmailLogType key.
		///		FK_EmailLog_EmailLogType Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_emailLogTypeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.EmailLog objects.</returns>
		public TList<EmailLog> GetByEmailLogTypeId(TransactionManager transactionManager, System.Int32 _emailLogTypeId, int start, int pageLength)
		{
			int count = -1;
			return GetByEmailLogTypeId(transactionManager, _emailLogTypeId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_EmailLog_EmailLogType key.
		///		fkEmailLogEmailLogType Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_emailLogTypeId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.EmailLog objects.</returns>
		public TList<EmailLog> GetByEmailLogTypeId(System.Int32 _emailLogTypeId, int start, int pageLength)
		{
			int count =  -1;
			return GetByEmailLogTypeId(null, _emailLogTypeId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_EmailLog_EmailLogType key.
		///		fkEmailLogEmailLogType Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_emailLogTypeId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.EmailLog objects.</returns>
		public TList<EmailLog> GetByEmailLogTypeId(System.Int32 _emailLogTypeId, int start, int pageLength,out int count)
		{
			return GetByEmailLogTypeId(null, _emailLogTypeId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_EmailLog_EmailLogType key.
		///		FK_EmailLog_EmailLogType Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_emailLogTypeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.EmailLog objects.</returns>
		public abstract TList<EmailLog> GetByEmailLogTypeId(TransactionManager transactionManager, System.Int32 _emailLogTypeId, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.EmailLog Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.EmailLogKey key, int start, int pageLength)
		{
			return GetByEmailLogId(transactionManager, key.EmailLogId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_EmailLog index.
		/// </summary>
		/// <param name="_emailLogId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.EmailLog"/> class.</returns>
		public KaiZen.CSMS.Entities.EmailLog GetByEmailLogId(System.Int32 _emailLogId)
		{
			int count = -1;
			return GetByEmailLogId(null,_emailLogId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_EmailLog index.
		/// </summary>
		/// <param name="_emailLogId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.EmailLog"/> class.</returns>
		public KaiZen.CSMS.Entities.EmailLog GetByEmailLogId(System.Int32 _emailLogId, int start, int pageLength)
		{
			int count = -1;
			return GetByEmailLogId(null, _emailLogId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_EmailLog index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_emailLogId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.EmailLog"/> class.</returns>
		public KaiZen.CSMS.Entities.EmailLog GetByEmailLogId(TransactionManager transactionManager, System.Int32 _emailLogId)
		{
			int count = -1;
			return GetByEmailLogId(transactionManager, _emailLogId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_EmailLog index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_emailLogId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.EmailLog"/> class.</returns>
		public KaiZen.CSMS.Entities.EmailLog GetByEmailLogId(TransactionManager transactionManager, System.Int32 _emailLogId, int start, int pageLength)
		{
			int count = -1;
			return GetByEmailLogId(transactionManager, _emailLogId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_EmailLog index.
		/// </summary>
		/// <param name="_emailLogId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.EmailLog"/> class.</returns>
		public KaiZen.CSMS.Entities.EmailLog GetByEmailLogId(System.Int32 _emailLogId, int start, int pageLength, out int count)
		{
			return GetByEmailLogId(null, _emailLogId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_EmailLog index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_emailLogId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.EmailLog"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.EmailLog GetByEmailLogId(TransactionManager transactionManager, System.Int32 _emailLogId, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_EmailLog index.
		/// </summary>
		/// <param name="_emailTo"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;EmailLog&gt;"/> class.</returns>
		public TList<EmailLog> GetByEmailTo(System.String _emailTo)
		{
			int count = -1;
			return GetByEmailTo(null,_emailTo, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_EmailLog index.
		/// </summary>
		/// <param name="_emailTo"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;EmailLog&gt;"/> class.</returns>
		public TList<EmailLog> GetByEmailTo(System.String _emailTo, int start, int pageLength)
		{
			int count = -1;
			return GetByEmailTo(null, _emailTo, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_EmailLog index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_emailTo"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;EmailLog&gt;"/> class.</returns>
		public TList<EmailLog> GetByEmailTo(TransactionManager transactionManager, System.String _emailTo)
		{
			int count = -1;
			return GetByEmailTo(transactionManager, _emailTo, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_EmailLog index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_emailTo"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;EmailLog&gt;"/> class.</returns>
		public TList<EmailLog> GetByEmailTo(TransactionManager transactionManager, System.String _emailTo, int start, int pageLength)
		{
			int count = -1;
			return GetByEmailTo(transactionManager, _emailTo, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_EmailLog index.
		/// </summary>
		/// <param name="_emailTo"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;EmailLog&gt;"/> class.</returns>
		public TList<EmailLog> GetByEmailTo(System.String _emailTo, int start, int pageLength, out int count)
		{
			return GetByEmailTo(null, _emailTo, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_EmailLog index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_emailTo"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;EmailLog&gt;"/> class.</returns>
		public abstract TList<EmailLog> GetByEmailTo(TransactionManager transactionManager, System.String _emailTo, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;EmailLog&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;EmailLog&gt;"/></returns>
		public static TList<EmailLog> Fill(IDataReader reader, TList<EmailLog> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.EmailLog c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("EmailLog")
					.Append("|").Append((System.Int32)reader[((int)EmailLogColumn.EmailLogId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<EmailLog>(
					key.ToString(), // EntityTrackingKey
					"EmailLog",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.EmailLog();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.EmailLogId = (System.Int32)reader[((int)EmailLogColumn.EmailLogId - 1)];
					c.EmailLogTypeId = (System.Int32)reader[((int)EmailLogColumn.EmailLogTypeId - 1)];
					c.EmailDateTime = (System.DateTime)reader[((int)EmailLogColumn.EmailDateTime - 1)];
					c.EmailFrom = (reader.IsDBNull(((int)EmailLogColumn.EmailFrom - 1)))?null:(System.String)reader[((int)EmailLogColumn.EmailFrom - 1)];
					c.EmailTo = (System.String)reader[((int)EmailLogColumn.EmailTo - 1)];
					c.EmailCc = (reader.IsDBNull(((int)EmailLogColumn.EmailCc - 1)))?null:(System.String)reader[((int)EmailLogColumn.EmailCc - 1)];
					c.EmailBcc = (reader.IsDBNull(((int)EmailLogColumn.EmailBcc - 1)))?null:(System.String)reader[((int)EmailLogColumn.EmailBcc - 1)];
					c.EmailLogMessageSubject = (System.String)reader[((int)EmailLogColumn.EmailLogMessageSubject - 1)];
					c.EmailLogMessageBody = (reader.IsDBNull(((int)EmailLogColumn.EmailLogMessageBody - 1)))?null:(System.Byte[])reader[((int)EmailLogColumn.EmailLogMessageBody - 1)];
					c.SentByUserId = (reader.IsDBNull(((int)EmailLogColumn.SentByUserId - 1)))?null:(System.Int32?)reader[((int)EmailLogColumn.SentByUserId - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.EmailLog"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.EmailLog"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.EmailLog entity)
		{
			if (!reader.Read()) return;
			
			entity.EmailLogId = (System.Int32)reader[((int)EmailLogColumn.EmailLogId - 1)];
			entity.EmailLogTypeId = (System.Int32)reader[((int)EmailLogColumn.EmailLogTypeId - 1)];
			entity.EmailDateTime = (System.DateTime)reader[((int)EmailLogColumn.EmailDateTime - 1)];
			entity.EmailFrom = (reader.IsDBNull(((int)EmailLogColumn.EmailFrom - 1)))?null:(System.String)reader[((int)EmailLogColumn.EmailFrom - 1)];
			entity.EmailTo = (System.String)reader[((int)EmailLogColumn.EmailTo - 1)];
			entity.EmailCc = (reader.IsDBNull(((int)EmailLogColumn.EmailCc - 1)))?null:(System.String)reader[((int)EmailLogColumn.EmailCc - 1)];
			entity.EmailBcc = (reader.IsDBNull(((int)EmailLogColumn.EmailBcc - 1)))?null:(System.String)reader[((int)EmailLogColumn.EmailBcc - 1)];
			entity.EmailLogMessageSubject = (System.String)reader[((int)EmailLogColumn.EmailLogMessageSubject - 1)];
			entity.EmailLogMessageBody = (reader.IsDBNull(((int)EmailLogColumn.EmailLogMessageBody - 1)))?null:(System.Byte[])reader[((int)EmailLogColumn.EmailLogMessageBody - 1)];
			entity.SentByUserId = (reader.IsDBNull(((int)EmailLogColumn.SentByUserId - 1)))?null:(System.Int32?)reader[((int)EmailLogColumn.SentByUserId - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.EmailLog"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.EmailLog"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.EmailLog entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.EmailLogId = (System.Int32)dataRow["EmailLogId"];
			entity.EmailLogTypeId = (System.Int32)dataRow["EmailLogTypeId"];
			entity.EmailDateTime = (System.DateTime)dataRow["EmailDateTime"];
			entity.EmailFrom = Convert.IsDBNull(dataRow["EmailFrom"]) ? null : (System.String)dataRow["EmailFrom"];
			entity.EmailTo = (System.String)dataRow["EmailTo"];
			entity.EmailCc = Convert.IsDBNull(dataRow["EmailCc"]) ? null : (System.String)dataRow["EmailCc"];
			entity.EmailBcc = Convert.IsDBNull(dataRow["EmailBcc"]) ? null : (System.String)dataRow["EmailBcc"];
			entity.EmailLogMessageSubject = (System.String)dataRow["EmailLogMessageSubject"];
			entity.EmailLogMessageBody = Convert.IsDBNull(dataRow["EmailLogMessageBody"]) ? null : (System.Byte[])dataRow["EmailLogMessageBody"];
			entity.SentByUserId = Convert.IsDBNull(dataRow["SentByUserId"]) ? null : (System.Int32?)dataRow["SentByUserId"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.EmailLog"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.EmailLog Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.EmailLog entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region EmailLogTypeIdSource	
			if (CanDeepLoad(entity, "EmailLogType|EmailLogTypeIdSource", deepLoadType, innerList) 
				&& entity.EmailLogTypeIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.EmailLogTypeId;
				EmailLogType tmpEntity = EntityManager.LocateEntity<EmailLogType>(EntityLocator.ConstructKeyFromPkItems(typeof(EmailLogType), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.EmailLogTypeIdSource = tmpEntity;
				else
					entity.EmailLogTypeIdSource = DataRepository.EmailLogTypeProvider.GetByEmailLogTypeId(transactionManager, entity.EmailLogTypeId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'EmailLogTypeIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.EmailLogTypeIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.EmailLogTypeProvider.DeepLoad(transactionManager, entity.EmailLogTypeIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion EmailLogTypeIdSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.EmailLog object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.EmailLog instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.EmailLog Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.EmailLog entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region EmailLogTypeIdSource
			if (CanDeepSave(entity, "EmailLogType|EmailLogTypeIdSource", deepSaveType, innerList) 
				&& entity.EmailLogTypeIdSource != null)
			{
				DataRepository.EmailLogTypeProvider.Save(transactionManager, entity.EmailLogTypeIdSource);
				entity.EmailLogTypeId = entity.EmailLogTypeIdSource.EmailLogTypeId;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region EmailLogChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.EmailLog</c>
	///</summary>
	public enum EmailLogChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>EmailLogType</c> at EmailLogTypeIdSource
		///</summary>
		[ChildEntityType(typeof(EmailLogType))]
		EmailLogType,
		}
	
	#endregion EmailLogChildEntityTypes
	
	#region EmailLogFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EmailLogColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EmailLog"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EmailLogFilterBuilder : SqlFilterBuilder<EmailLogColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EmailLogFilterBuilder class.
		/// </summary>
		public EmailLogFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the EmailLogFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public EmailLogFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the EmailLogFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public EmailLogFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion EmailLogFilterBuilder
	
	#region EmailLogParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EmailLogColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EmailLog"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EmailLogParameterBuilder : ParameterizedSqlFilterBuilder<EmailLogColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EmailLogParameterBuilder class.
		/// </summary>
		public EmailLogParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the EmailLogParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public EmailLogParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the EmailLogParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public EmailLogParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion EmailLogParameterBuilder
	
	#region EmailLogSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EmailLogColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EmailLog"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class EmailLogSortBuilder : SqlSortBuilder<EmailLogColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EmailLogSqlSortBuilder class.
		/// </summary>
		public EmailLogSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion EmailLogSortBuilder
	
} // end namespace
