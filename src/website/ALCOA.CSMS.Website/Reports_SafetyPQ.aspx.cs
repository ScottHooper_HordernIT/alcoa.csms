﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Reports_SafetyPQ : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            String qNo = Request.QueryString["q"].ToString();
            Control uc;
            switch (qNo)
            {
                case "progress":
                    uc = LoadControl("~/UserControls/Main/SqReports/QuestionnaireReport_Progress.ascx");
                    break;
                case "expire":
                    uc = LoadControl("~/UserControls/Main/SqReports/QuestionnaireReport_Expire.ascx");
                    break;
                case "overview":
                    uc = LoadControl("~/UserControls/Main/SqReports/QuestionnaireReport_Overview.ascx");
                    break;
                case "overview_subcontractors":
                    uc = LoadControl("~/UserControls/Main/SqReports/QuestionnaireReport_Overview_SubContractors.ascx");
                    break;
                case "services":
                    uc = LoadControl("~/UserControls/Main/SqReports/QuestionnaireReport_Services.ascx");
                    break;
                case "services2":
                    uc = LoadControl("~/UserControls/Main/SqReports/QuestionnaireReport_Services2.ascx");
                    break;
                default:
                    throw new Exception("Could not load Report.");
            }

            PlaceHolder1.Controls.Add(uc);
        }
        catch (Exception ex)
        {
            throw new Exception("Could not load Report. Error: " + ex.Message.ToString());
        }
    }
}
