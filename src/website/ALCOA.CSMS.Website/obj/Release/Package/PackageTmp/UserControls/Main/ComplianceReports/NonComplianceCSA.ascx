<%@ Control Language="C#" AutoEventWireup="true" Inherits="UserControls_NonComplianceCSA" Codebehind="NonComplianceCSA.ascx.cs" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
    <%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"  Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Src="../../Other/ExportButtons.ascx" TagName="ExportButtons" TagPrefix="uc1" %>

<%@ Register assembly="DevExpress.Web.v14.1" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>

<div class="pageName" style="width: 900px">
    <span class="bodycopy"><span class="title">Compliance Report</span><br />
    <span class="date">Contractor Services Audit</span><br />
    <img src="images/grfc_dottedline.gif" width="24" height="1"></span><br />
    <br />

    <table style="width: 668px">
        <tr>
            <td style="width: 76px; text-align: right; height: 38px;">
                <strong>Operation:&nbsp;</strong></td>
            <td style="width: 201px; text-align: left; height: 38px;">
                <dxe:ASPxComboBox id="cbRegion" runat="server"
                     datasourceid="dsRegionsFilter" textfield="RegionName"
                     autopostback="True" valuefield="RegionId"
                     valuetype="System.Int32"
                     cssfilepath="~/App_Themes/Office2003Blue/{0}/styles.css"
                     csspostfix="Office2003Blue" width="200px"
                     onselectedindexchanged="cbRegion_SelectedIndexChanged" 
                    SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                    <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                    </LoadingPanelImage>
                    <ButtonStyle Width="13px">
                    </ButtonStyle>
                    <ValidationSettings
                     setfocusonerror="True" validationgroup="Filter" Display="Dynamic">
                  <RequiredField isrequired="True"></RequiredField></ValidationSettings></dxe:ASPxComboBox>
            </td>
            <td style="width: 38px; text-align: right; height: 38px;">
                <strong>Year:</strong></td>
            <td style="width: 71px; text-align: left; height: 38px;">
                <dxe:ASPxComboBox id="cbYear" runat="server"
                 autopostback="True" clientinstancename="cmbYear"
                 cssfilepath="~/App_Themes/Office2003Blue/{0}/styles.css"
                 csspostfix="Office2003Blue" IncrementalFilteringMode="StartsWith"
                 onselectedindexchanged="cbYear_SelectedIndexChanged"
                 valuetype="System.String" width="60px" 
                    SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                    <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                    </LoadingPanelImage>
                    <ButtonStyle Width="13px">
                    </ButtonStyle>
                    <ValidationSettings Display="Dynamic">
                    </ValidationSettings>
                </dxe:ASPxComboBox>
            </td>
            <td align="right" style="width: 85px; height: 38px">
                <strong>Status:</strong></td>
            <td align="right" style="width: 73px; height: 38px">
                <dxe:aspxcombobox id="cbStatus" runat="server" autopostback="True" clientinstancename="cmbYear"
                    cssfilepath="~/App_Themes/Office2003Blue/{0}/styles.css" csspostfix="Office2003Blue"
                    IncrementalFilteringMode="StartsWith"
                    onselectedindexchanged="cbStatus_SelectedIndexChanged" selectedindex="0" valuetype="System.String"
                    width="120px" 
                    SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"><Items>
                <dxe:ListEditItem Text="Embedded" Value="1"></dxe:ListEditItem>
                <dxe:ListEditItem Text="Non-Embedded 1" Value="2"></dxe:ListEditItem>
                <dxe:ListEditItem Text="All" Value="%"></dxe:ListEditItem>
                </Items>

                    <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                    </LoadingPanelImage>
                    <ButtonStyle Width="13px">
                    </ButtonStyle>

                <ValidationSettings Display="Dynamic"></ValidationSettings>
                </dxe:aspxcombobox>
            </td>
            <td align="right" style="width: 286px; height: 38px">
                <button name="btnPrint" onclick="javascript:print();" style="font-weight: bold; width: 80px;
                    height: 25px" type="button">
                    Print</button>
            </td>
        </tr>
    </table>
            
    <dxwgv:ASPxGridView ID="grid"
        ClientInstanceName="grid"
        runat="server"
        AutoGenerateColumns="False"
        CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
        CssPostfix="Office2003Blue"
        DataSourceID="sqldsNonComplianceReportYearCSA"
        Width="900px"
        OnHtmlRowCreated="grid_RowCreated" KeyFieldName="CompanyId"
        OnAutoFilterCellEditorInitialize="grid_AutoFilterCellEditorInitialize"
     >
        <Columns>
            <dxwgv:GridViewDataComboBoxColumn Caption="Company Name" FieldName="CompanyId" Name="CompanyBox"  SortIndex="0" SortOrder="Ascending"
                VisibleIndex="0">
                <PropertiesComboBox DataSourceID="sqldsCompaniesList" DropDownHeight="150px" TextField="CompanyName"
                    ValueField="CompanyId" ValueType="System.Int32" IncrementalFilteringMode="StartsWith">
                </PropertiesComboBox>
                <EditFormSettings Visible="True" />
                <Settings SortMode="DisplayText" />
            </dxwgv:GridViewDataComboBoxColumn>
            <dxwgv:GridViewDataComboBoxColumn Caption="Site Name" FieldName="SiteId" Name="SiteBox"
                VisibleIndex="1" SortIndex="1" SortOrder="Ascending">
                <PropertiesComboBox DataSourceID="sqldsResidentialSites" DropDownHeight="150px" TextField="SiteName"
                    ValueField="SiteId" ValueType="System.Int32" IncrementalFilteringMode="StartsWith">
                </PropertiesComboBox>
                <EditFormSettings Visible="True" />
                <Settings SortMode="DisplayText" />
            </dxwgv:GridViewDataComboBoxColumn>
            
            
            <dxwgv:GridViewDataTextColumn Name="Qtr 1" FieldName="Qtr 1" ReadOnly="True"
         VisibleIndex="2">
         <DataItemTemplate>
         <div style="margin: 0px auto; text-align: center; float: left; padding: 0px">
            <asp:Image ID="changeImage1" runat="server" ImageUrl="~/Images/spacer.gif" Visible="true" GenerateEmptyAlternateText="True" />
            <asp:Label ID="noKPISubmitted1" runat="server" Text=""></asp:Label>
         </div>
         </DataItemTemplate>
         <HeaderStyle HorizontalAlign="Center" />
        <CellStyle HorizontalAlign="Center">
        </CellStyle>
     </dxwgv:GridViewDataTextColumn>
     
     
     <dxwgv:GridViewDataTextColumn Name="Qtr 2" FieldName="Qtr 2" ReadOnly="True"
         VisibleIndex="3">
         <DataItemTemplate>
         <div style="margin: 0px auto; text-align: center; float: left; padding: 0px">
            <asp:Image ID="changeImage2" runat="server" ImageUrl="~/Images/spacer.gif" Visible="true" GenerateEmptyAlternateText="True" />
            <asp:Label ID="noKPISubmitted2" runat="server" Text=""></asp:Label>
         </div>
         </DataItemTemplate>
         <HeaderStyle HorizontalAlign="Center" />
                <CellStyle HorizontalAlign="Center">
                </CellStyle>
     </dxwgv:GridViewDataTextColumn>
     
    <dxwgv:GridViewDataTextColumn Name="Qtr 3" FieldName="Qtr 3" ReadOnly="True"
         VisibleIndex="4">
         <DataItemTemplate>
         <div style="margin: 0px auto; text-align: center; float: left; padding: 0px">
            <asp:Image ID="changeImage3" runat="server" ImageUrl="~/Images/spacer.gif" Visible="true" GenerateEmptyAlternateText="True" />
            <asp:Label ID="noKPISubmitted3" runat="server" Text=""></asp:Label>
         </div>
         </DataItemTemplate>
         <HeaderStyle HorizontalAlign="Center" />
                <CellStyle HorizontalAlign="Center">
                </CellStyle>
     </dxwgv:GridViewDataTextColumn>
     
        
     <dxwgv:GridViewDataTextColumn Name="Qtr 4" FieldName="Qtr 4" ReadOnly="True"
         VisibleIndex="5">
         <DataItemTemplate>
         <div style="margin: 0px auto; text-align: center; float: left; padding: 0px">
             <asp:Image ID="changeImage4" runat="server" ImageUrl="~/Images/spacer.gif" Visible="true" GenerateEmptyAlternateText="True" />
             <asp:Label ID="noKPISubmitted4" runat="server" Text=""></asp:Label>
         </div>
         </DataItemTemplate>
         <HeaderStyle HorizontalAlign="Center" />
                <CellStyle HorizontalAlign="Center">
                </CellStyle>
     </dxwgv:GridViewDataTextColumn>   

     <%--Added By Sayani Sil for task:6--%>
       <dxwgv:GridViewDataTextColumn Name="Half 1" FieldName="Half 1" ReadOnly="True"
         VisibleIndex="6"  Width="70px">
         <DataItemTemplate>
         <div style="margin: 0px auto; text-align: center; float: left; padding: 0px" >
            <asp:Image ID="changeImage6" runat="server" ImageUrl="~/Images/spacer.gif" Visible="true" GenerateEmptyAlternateText="True" />
            <%--<asp:Image ID="changeImage2" runat="server" ImageUrl="~/Images/spacer.gif" Visible="true" GenerateEmptyAlternateText="True" />--%>
            <asp:Label ID="noKPISubmittedHalf1" runat="server" Text=""></asp:Label>
         </div>
         </DataItemTemplate>
         <HeaderStyle HorizontalAlign="Center" />
        <CellStyle HorizontalAlign="Center">
        </CellStyle>
     </dxwgv:GridViewDataTextColumn>


       <dxwgv:GridViewDataTextColumn Name="Half 2" FieldName="Half 2" ReadOnly="True"
         VisibleIndex="7"  Width="70px">
         <DataItemTemplate>
         <div style="margin: 0px auto; text-align: center; float: left; padding: 0px ">
            <asp:Image ID="changeImage7" runat="server" ImageUrl="~/Images/spacer.gif" Visible="true" GenerateEmptyAlternateText="True" />
            <%--<asp:Image ID="changeImage4" runat="server" ImageUrl="~/Images/spacer.gif" Visible="true" GenerateEmptyAlternateText="True" />--%>
            <asp:Label ID="noKPISubmittedHalf2" runat="server" Text=""></asp:Label>
         </div>
         </DataItemTemplate>
         <HeaderStyle HorizontalAlign="Center" />
        <CellStyle HorizontalAlign="Center">
        </CellStyle>
     </dxwgv:GridViewDataTextColumn>
     
       
     <dxwgv:GridViewDataTextColumn Name="Alcoa Annual Audit" FieldName="Alcoa Audit" ReadOnly="True"
         VisibleIndex="8" Width="70px">
         <DataItemTemplate>
             <div style="margin: 0px auto; text-align: center; float: left; padding: 0px">
                <asp:Image ID="changeImage5" runat="server" ImageUrl="~/Images/spacer.gif" Visible="true" GenerateEmptyAlternateText="True" />
                <asp:Label ID="noKPISubmitted5" runat="server" Text=""></asp:Label>
            </div>
         </DataItemTemplate>
         <HeaderStyle HorizontalAlign="Center" />
                <CellStyle HorizontalAlign="Center">
                </CellStyle>
     </dxwgv:GridViewDataTextColumn>
     
        </Columns>
        <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
            <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
            </LoadingPanelOnStatusBar>
            <LoadingPanel Url="~/App_Themes/Office2003BlueOld/GridView/Loading.gif"><%--Enhancement_023:change by debashis for testing platform upgradation--%>
            </LoadingPanel>
        </Images>
        <Settings ShowGroupedColumns="True" ShowFilterRow="True" ShowGroupPanel="True" />
        <SettingsPager Visible="False" PageSize="999" Mode="ShowAllRecords">
            <AllButton Visible="true">
            </AllButton>
        </SettingsPager>
        <SettingsCustomizationWindow Enabled="True" />
        <ImagesFilterControl>
            <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
            </LoadingPanel>
        </ImagesFilterControl>
        <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
            CssPostfix="Office2003Blue">
            <Header ImageSpacing="5px" SortingImageSpacing="5px">
            </Header>
            <AlternatingRow Enabled="True">
            </AlternatingRow>
            <LoadingPanel ImageSpacing="10px">
            </LoadingPanel>
        </Styles>
        <SettingsBehavior ConfirmDelete="True"/>
        <SettingsCookies CookiesID="NonComplianceCSA" Version="0.1" />
        <StylesEditors>
            <ProgressBar Height="25px">
            </ProgressBar>
        </StylesEditors>
    </dxwgv:ASPxGridView>        
</div>

<table width="900px">
<tr align="right">
        <td align="right" class="pageName" style="height: 30px; text-align: right; text-align:-moz-right; width: 900px;">
            <uc1:ExportButtons ID="ucExportButtons" runat="server" />
        </td>
</tr>
</table>


<asp:SqlDataSource ID="sqldsNonComplianceReportYearCSA" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="_CSA_GetReport_NonCompliance" SelectCommandType="StoredProcedure">
    <SelectParameters>
        <asp:SessionParameter DefaultValue="2007" Name="CURRENTYEAR" SessionField="spVar_Year"
            Type="Int32" />
        <asp:SessionParameter DefaultValue="1" Name="RegionId" SessionField="spVar_RegionId"
            Type="Int32" />
            <asp:SessionParameter DefaultValue="1" Name="Status" SessionField="spVar_CategoryId2"
            Type="String" />
    </SelectParameters>
</asp:SqlDataSource>

<asp:SqlDataSource ID="sqldsCompaniesList" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="_Companies_GetActiveCompanyNameList" SelectCommandType="StoredProcedure">
</asp:SqlDataSource>

<asp:SqlDataSource ID="sqldsResidentialSites" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    ProviderName="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString.ProviderName %>"
    SelectCommand="SELECT DISTINCT(dbo.CompanySiteCategoryStandard.SiteId) As [SiteId], dbo.Sites.SiteName As [SiteName] FROM dbo.CompanySiteCategoryStandard INNER JOIN dbo.Sites on dbo.CompanySiteCategoryStandard.SiteId = dbo.Sites.SiteId WHERE dbo.CompanySiteCategoryStandard.CompanySiteCategoryId <= 2 ORDER BY [SiteName] ASC">
</asp:SqlDataSource>
<asp:SqlDataSource ID="sqldsCSAYear" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    ProviderName="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString.ProviderName %>"
    SelectCommand="SELECT DISTINCT(YEAR) As [Year] FROM dbo.CSA ORDER BY [Year] ASC">
</asp:SqlDataSource>
<data:RegionsDataSource ID="dsRegionsFilter" runat="server" Filter="IsVisible = True" Sort="Ordinal ASC"></data:RegionsDataSource>