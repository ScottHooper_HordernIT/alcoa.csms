﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.QuestionnaireReportOverviewSubQuery2Provider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[CLSCompliant(true)]
	[Designer(typeof(QuestionnaireReportOverviewSubQuery2DataSourceDesigner))]
	public class QuestionnaireReportOverviewSubQuery2DataSource : ReadOnlyDataSource<QuestionnaireReportOverviewSubQuery2>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportOverviewSubQuery2DataSource class.
		/// </summary>
		public QuestionnaireReportOverviewSubQuery2DataSource() : base(new QuestionnaireReportOverviewSubQuery2Service())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the QuestionnaireReportOverviewSubQuery2DataSourceView used by the QuestionnaireReportOverviewSubQuery2DataSource.
		/// </summary>
		protected QuestionnaireReportOverviewSubQuery2DataSourceView QuestionnaireReportOverviewSubQuery2View
		{
			get { return ( View as QuestionnaireReportOverviewSubQuery2DataSourceView ); }
		}
		
		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the QuestionnaireReportOverviewSubQuery2DataSourceView class that is to be
		/// used by the QuestionnaireReportOverviewSubQuery2DataSource.
		/// </summary>
		/// <returns>An instance of the QuestionnaireReportOverviewSubQuery2DataSourceView class.</returns>
		protected override BaseDataSourceView<QuestionnaireReportOverviewSubQuery2, Object> GetNewDataSourceView()
		{
			return new QuestionnaireReportOverviewSubQuery2DataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the QuestionnaireReportOverviewSubQuery2DataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class QuestionnaireReportOverviewSubQuery2DataSourceView : ReadOnlyDataSourceView<QuestionnaireReportOverviewSubQuery2>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportOverviewSubQuery2DataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the QuestionnaireReportOverviewSubQuery2DataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public QuestionnaireReportOverviewSubQuery2DataSourceView(QuestionnaireReportOverviewSubQuery2DataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal QuestionnaireReportOverviewSubQuery2DataSource QuestionnaireReportOverviewSubQuery2Owner
		{
			get { return Owner as QuestionnaireReportOverviewSubQuery2DataSource; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal QuestionnaireReportOverviewSubQuery2Service QuestionnaireReportOverviewSubQuery2Provider
		{
			get { return Provider as QuestionnaireReportOverviewSubQuery2Service; }
		}

		#endregion Properties
		
		#region Methods
		
		#endregion Methods
	}

	#region QuestionnaireReportOverviewSubQuery2DataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the QuestionnaireReportOverviewSubQuery2DataSource class.
	/// </summary>
	public class QuestionnaireReportOverviewSubQuery2DataSourceDesigner : ReadOnlyDataSourceDesigner<QuestionnaireReportOverviewSubQuery2>
	{
	}

	#endregion QuestionnaireReportOverviewSubQuery2DataSourceDesigner

	#region QuestionnaireReportOverviewSubQuery2Filter

	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireReportOverviewSubQuery2"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireReportOverviewSubQuery2Filter : SqlFilter<QuestionnaireReportOverviewSubQuery2Column>
	{
	}

	#endregion QuestionnaireReportOverviewSubQuery2Filter

	#region QuestionnaireReportOverviewSubQuery2ExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireReportOverviewSubQuery2"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireReportOverviewSubQuery2ExpressionBuilder : SqlExpressionBuilder<QuestionnaireReportOverviewSubQuery2Column>
	{
	}
	
	#endregion QuestionnaireReportOverviewSubQuery2ExpressionBuilder		
}

