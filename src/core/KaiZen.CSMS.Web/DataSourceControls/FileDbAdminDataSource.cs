﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.FileDbAdminProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(FileDbAdminDataSourceDesigner))]
	public class FileDbAdminDataSource : ProviderDataSource<FileDbAdmin, FileDbAdminKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FileDbAdminDataSource class.
		/// </summary>
		public FileDbAdminDataSource() : base(new FileDbAdminService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the FileDbAdminDataSourceView used by the FileDbAdminDataSource.
		/// </summary>
		protected FileDbAdminDataSourceView FileDbAdminView
		{
			get { return ( View as FileDbAdminDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the FileDbAdminDataSource control invokes to retrieve data.
		/// </summary>
		public FileDbAdminSelectMethod SelectMethod
		{
			get
			{
				FileDbAdminSelectMethod selectMethod = FileDbAdminSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (FileDbAdminSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the FileDbAdminDataSourceView class that is to be
		/// used by the FileDbAdminDataSource.
		/// </summary>
		/// <returns>An instance of the FileDbAdminDataSourceView class.</returns>
		protected override BaseDataSourceView<FileDbAdmin, FileDbAdminKey> GetNewDataSourceView()
		{
			return new FileDbAdminDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the FileDbAdminDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class FileDbAdminDataSourceView : ProviderDataSourceView<FileDbAdmin, FileDbAdminKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FileDbAdminDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the FileDbAdminDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public FileDbAdminDataSourceView(FileDbAdminDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal FileDbAdminDataSource FileDbAdminOwner
		{
			get { return Owner as FileDbAdminDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal FileDbAdminSelectMethod SelectMethod
		{
			get { return FileDbAdminOwner.SelectMethod; }
			set { FileDbAdminOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal FileDbAdminService FileDbAdminProvider
		{
			get { return Provider as FileDbAdminService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<FileDbAdmin> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<FileDbAdmin> results = null;
			FileDbAdmin item;
			count = 0;
			
			System.Int32 _fileId;

			switch ( SelectMethod )
			{
				case FileDbAdminSelectMethod.Get:
					FileDbAdminKey entityKey  = new FileDbAdminKey();
					entityKey.Load(values);
					item = FileDbAdminProvider.Get(entityKey);
					results = new TList<FileDbAdmin>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case FileDbAdminSelectMethod.GetAll:
                    results = FileDbAdminProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case FileDbAdminSelectMethod.GetPaged:
					results = FileDbAdminProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case FileDbAdminSelectMethod.Find:
					if ( FilterParameters != null )
						results = FileDbAdminProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = FileDbAdminProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case FileDbAdminSelectMethod.GetByFileId:
					_fileId = ( values["FileId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["FileId"], typeof(System.Int32)) : (int)0;
					item = FileDbAdminProvider.GetByFileId(_fileId);
					results = new TList<FileDbAdmin>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				// FK
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == FileDbAdminSelectMethod.Get || SelectMethod == FileDbAdminSelectMethod.GetByFileId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				FileDbAdmin entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					FileDbAdminProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<FileDbAdmin> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			FileDbAdminProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region FileDbAdminDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the FileDbAdminDataSource class.
	/// </summary>
	public class FileDbAdminDataSourceDesigner : ProviderDataSourceDesigner<FileDbAdmin, FileDbAdminKey>
	{
		/// <summary>
		/// Initializes a new instance of the FileDbAdminDataSourceDesigner class.
		/// </summary>
		public FileDbAdminDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public FileDbAdminSelectMethod SelectMethod
		{
			get { return ((FileDbAdminDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new FileDbAdminDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region FileDbAdminDataSourceActionList

	/// <summary>
	/// Supports the FileDbAdminDataSourceDesigner class.
	/// </summary>
	internal class FileDbAdminDataSourceActionList : DesignerActionList
	{
		private FileDbAdminDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the FileDbAdminDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public FileDbAdminDataSourceActionList(FileDbAdminDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public FileDbAdminSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion FileDbAdminDataSourceActionList
	
	#endregion FileDbAdminDataSourceDesigner
	
	#region FileDbAdminSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the FileDbAdminDataSource.SelectMethod property.
	/// </summary>
	public enum FileDbAdminSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByFileId method.
		/// </summary>
		GetByFileId
	}
	
	#endregion FileDbAdminSelectMethod

	#region FileDbAdminFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="FileDbAdmin"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FileDbAdminFilter : SqlFilter<FileDbAdminColumn>
	{
	}
	
	#endregion FileDbAdminFilter

	#region FileDbAdminExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="FileDbAdmin"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FileDbAdminExpressionBuilder : SqlExpressionBuilder<FileDbAdminColumn>
	{
	}
	
	#endregion FileDbAdminExpressionBuilder	

	#region FileDbAdminProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;FileDbAdminChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="FileDbAdmin"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FileDbAdminProperty : ChildEntityProperty<FileDbAdminChildEntityTypes>
	{
	}
	
	#endregion FileDbAdminProperty
}

