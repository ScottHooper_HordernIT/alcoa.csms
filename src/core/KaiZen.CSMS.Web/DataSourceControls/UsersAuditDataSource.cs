﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.UsersAuditProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(UsersAuditDataSourceDesigner))]
	public class UsersAuditDataSource : ProviderDataSource<UsersAudit, UsersAuditKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UsersAuditDataSource class.
		/// </summary>
		public UsersAuditDataSource() : base(new UsersAuditService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the UsersAuditDataSourceView used by the UsersAuditDataSource.
		/// </summary>
		protected UsersAuditDataSourceView UsersAuditView
		{
			get { return ( View as UsersAuditDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the UsersAuditDataSource control invokes to retrieve data.
		/// </summary>
		public UsersAuditSelectMethod SelectMethod
		{
			get
			{
				UsersAuditSelectMethod selectMethod = UsersAuditSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (UsersAuditSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the UsersAuditDataSourceView class that is to be
		/// used by the UsersAuditDataSource.
		/// </summary>
		/// <returns>An instance of the UsersAuditDataSourceView class.</returns>
		protected override BaseDataSourceView<UsersAudit, UsersAuditKey> GetNewDataSourceView()
		{
			return new UsersAuditDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the UsersAuditDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class UsersAuditDataSourceView : ProviderDataSourceView<UsersAudit, UsersAuditKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UsersAuditDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the UsersAuditDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public UsersAuditDataSourceView(UsersAuditDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal UsersAuditDataSource UsersAuditOwner
		{
			get { return Owner as UsersAuditDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal UsersAuditSelectMethod SelectMethod
		{
			get { return UsersAuditOwner.SelectMethod; }
			set { UsersAuditOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal UsersAuditService UsersAuditProvider
		{
			get { return Provider as UsersAuditService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<UsersAudit> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<UsersAudit> results = null;
			UsersAudit item;
			count = 0;
			
			System.Int32 _auditId;

			switch ( SelectMethod )
			{
				case UsersAuditSelectMethod.Get:
					UsersAuditKey entityKey  = new UsersAuditKey();
					entityKey.Load(values);
					item = UsersAuditProvider.Get(entityKey);
					results = new TList<UsersAudit>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case UsersAuditSelectMethod.GetAll:
                    results = UsersAuditProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case UsersAuditSelectMethod.GetPaged:
					results = UsersAuditProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case UsersAuditSelectMethod.Find:
					if ( FilterParameters != null )
						results = UsersAuditProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = UsersAuditProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case UsersAuditSelectMethod.GetByAuditId:
					_auditId = ( values["AuditId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["AuditId"], typeof(System.Int32)) : (int)0;
					item = UsersAuditProvider.GetByAuditId(_auditId);
					results = new TList<UsersAudit>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				// FK
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == UsersAuditSelectMethod.Get || SelectMethod == UsersAuditSelectMethod.GetByAuditId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				UsersAudit entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					UsersAuditProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<UsersAudit> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			UsersAuditProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region UsersAuditDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the UsersAuditDataSource class.
	/// </summary>
	public class UsersAuditDataSourceDesigner : ProviderDataSourceDesigner<UsersAudit, UsersAuditKey>
	{
		/// <summary>
		/// Initializes a new instance of the UsersAuditDataSourceDesigner class.
		/// </summary>
		public UsersAuditDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public UsersAuditSelectMethod SelectMethod
		{
			get { return ((UsersAuditDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new UsersAuditDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region UsersAuditDataSourceActionList

	/// <summary>
	/// Supports the UsersAuditDataSourceDesigner class.
	/// </summary>
	internal class UsersAuditDataSourceActionList : DesignerActionList
	{
		private UsersAuditDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the UsersAuditDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public UsersAuditDataSourceActionList(UsersAuditDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public UsersAuditSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion UsersAuditDataSourceActionList
	
	#endregion UsersAuditDataSourceDesigner
	
	#region UsersAuditSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the UsersAuditDataSource.SelectMethod property.
	/// </summary>
	public enum UsersAuditSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByAuditId method.
		/// </summary>
		GetByAuditId
	}
	
	#endregion UsersAuditSelectMethod

	#region UsersAuditFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="UsersAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UsersAuditFilter : SqlFilter<UsersAuditColumn>
	{
	}
	
	#endregion UsersAuditFilter

	#region UsersAuditExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="UsersAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UsersAuditExpressionBuilder : SqlExpressionBuilder<UsersAuditColumn>
	{
	}
	
	#endregion UsersAuditExpressionBuilder	

	#region UsersAuditProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;UsersAuditChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="UsersAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UsersAuditProperty : ChildEntityProperty<UsersAuditChildEntityTypes>
	{
	}
	
	#endregion UsersAuditProperty
}

