﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface ICompanyStatusChangeApprovalService
    {
        CompanyStatusChangeApproval Get(Expression<Func<CompanyStatusChangeApproval, bool>> where, List<Expression<Func<CompanyStatusChangeApproval, object>>> includes);
        List<CompanyStatusChangeApproval> GetMany(int? top, Expression<Func<CompanyStatusChangeApproval, bool>> where, Func<IQueryable<CompanyStatusChangeApproval>, IOrderedQueryable<CompanyStatusChangeApproval>> orderBy, List<Expression<Func<CompanyStatusChangeApproval, object>>> includes);
        List<CompanyStatusChangeApproval> GetManyPaged(int pageNumber, int pageSize, Expression<Func<CompanyStatusChangeApproval, bool>> where, Func<IQueryable<CompanyStatusChangeApproval>, IOrderedQueryable<CompanyStatusChangeApproval>> orderBy, List<Expression<Func<CompanyStatusChangeApproval, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<CompanyStatusChangeApproval, bool>> where, List<Expression<Func<CompanyStatusChangeApproval, object>>> includes);
        void Insert(CompanyStatusChangeApproval item, bool saveChanges = true);
        void Update(CompanyStatusChangeApproval item, bool saveChanges = true);
        void Delete(CompanyStatusChangeApproval item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class CompanyStatusChangeApprovalService : ICompanyStatusChangeApprovalService
    {
        private readonly IRepository<CompanyStatusChangeApproval> _repository;

        public CompanyStatusChangeApprovalService(IRepository<CompanyStatusChangeApproval> repository)
        {
            this._repository = repository;
        }

        public CompanyStatusChangeApproval Get(Expression<Func<CompanyStatusChangeApproval, bool>> where, List<Expression<Func<CompanyStatusChangeApproval, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<CompanyStatusChangeApproval> GetMany(int? top, Expression<Func<CompanyStatusChangeApproval, bool>> where, Func<IQueryable<CompanyStatusChangeApproval>, IOrderedQueryable<CompanyStatusChangeApproval>> orderBy, List<Expression<Func<CompanyStatusChangeApproval, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<CompanyStatusChangeApproval> GetManyPaged(int pageNumber, int pageSize, Expression<Func<CompanyStatusChangeApproval, bool>> where, Func<IQueryable<CompanyStatusChangeApproval>, IOrderedQueryable<CompanyStatusChangeApproval>> orderBy, List<Expression<Func<CompanyStatusChangeApproval, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<CompanyStatusChangeApproval, bool>> where, List<Expression<Func<CompanyStatusChangeApproval, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(CompanyStatusChangeApproval item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("CompanyStatusChangeApproval");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(CompanyStatusChangeApproval item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("CompanyStatusChangeApproval");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(CompanyStatusChangeApproval item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("CompanyStatusChangeApproval");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
