﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Workload_Sqq_HsAssessor.ascx.cs"
    Inherits="ALCOA.CSMS.Website.UserControls.Main.WorkLevelling.Workload_Sqq_HsAssessor" %>
<%@ Register Src="~/UserControls/Other/ExportButtons.ascx" TagName="ExportButtons"
    TagPrefix="uc1" %>
<%@ Register Assembly="DevExpress.Web.ASPxPivotGrid.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPivotGrid.Export" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.XtraCharts.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.XtraCharts" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.XtraCharts.v14.1.Web, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.XtraCharts.Web" TagPrefix="dxchartsui" %>
<%@ Register Assembly="DevExpress.Web.ASPxPivotGrid.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPivotGrid" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dxtc" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dxw" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallbackPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dxpc" %>
<table style="width: 100%" cellspacing="0" cellpadding="0" border="0">
    <tbody>
        <tr>
            <td style="height: 28px; text-align: left">
                <table cellspacing="0" cellpadding="0" border="0">
                    <tbody>
                        <tr>
                            <td style="width: 48px; text-align: right">
                                &nbsp;<strong>Year:</strong>&nbsp;
                            </td>
                            <td style="width: 100px">
                                <dxe:ASPxComboBox runat="server" IncrementalFilteringMode="StartsWith" ValueType="System.String"
                                    Width="55px" AutoPostBack="True" CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                    ClientInstanceName="cmbYear" ID="cmbYear" OnSelectedIndexChanged="cmbYear_SelectedIndexChanged"
                                    SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                    <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                                    </LoadingPanelImage>
                                    <ButtonStyle Width="13px">
                                    </ButtonStyle>
                                </dxe:ASPxComboBox>
                            </td>
                            <td style="width: 750px; text-align: right">
                                <div align="right">
                                    <a style="color: #0000ff; text-align: right; text-decoration: none" href="javascript:ShowHideCustomizationWindow()">
                                        Customize</a>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
                &nbsp;&nbsp;
            </td>
        </tr>
        <tr>
            <td style="height: 28px; text-align: left">
                <dxwgv:ASPxGridView runat="server" ClientInstanceName="grid" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                    CssPostfix="Office2003Blue" AutoGenerateColumns="False" DataSourceID="dsQuestionnaire_GetEhsConsultantsWorkLoad"
                    Width="870px" ID="grid" OnAutoFilterCellEditorInitialize="grid_AutoFilterCellEditorInitialize"
                    KeyFieldName="UserId">
                    <SettingsCustomizationWindow Enabled="True"></SettingsCustomizationWindow>
                    <TotalSummary>
                        <dxwgv:ASPxSummaryItem SummaryType="Sum" FieldName="NoAllocated" DisplayFormat="{0}"
                            ShowInColumn="# Allocated" ShowInGroupFooterColumn="# Allocated"></dxwgv:ASPxSummaryItem>
                        <%--<dxwgv:ASPxSummaryItem SummaryType="Sum" FieldName="NoIncomplete" DisplayFormat="{0}"
                            ShowInColumn="# Incomplete" ShowInGroupFooterColumn="# Incomplete"></dxwgv:ASPxSummaryItem>--%>
                        <dxwgv:ASPxSummaryItem SummaryType="Sum" FieldName="NoBeingAssessed" DisplayFormat="{0}"
                            ShowInColumn="# Being Assessed" ShowInGroupFooterColumn="# Being Assessed"></dxwgv:ASPxSummaryItem>
                        <dxwgv:ASPxSummaryItem SummaryType="Sum" FieldName="NoAssessmentComplete" DisplayFormat="{0}"
                            ShowInColumn="# Assessment Complete" ShowInGroupFooterColumn="# Assessment Complete">
                        </dxwgv:ASPxSummaryItem>
                        <dxwgv:ASPxSummaryItem SummaryType="Sum" FieldName="NoRecommended" DisplayFormat="{0}"
                            ShowInColumn="# Recommended" ShowInGroupFooterColumn="# Recommended"></dxwgv:ASPxSummaryItem>
                        <dxwgv:ASPxSummaryItem SummaryType="Sum" FieldName="NoNotRecommended" DisplayFormat="{0}"
                            ShowInColumn="# Not Recommended" ShowInGroupFooterColumn="# Not Recommended">
                        </dxwgv:ASPxSummaryItem>
                    </TotalSummary>
                    <Columns>
                        <dxwgv:GridViewDataTextColumn Caption="Site" FieldName="SiteName" Visible="false"
                            Width="75px">
                            <HeaderStyle HorizontalAlign="Center" Wrap="True"></HeaderStyle>
                            <CellStyle HorizontalAlign="Center">
                            </CellStyle>
                            <FooterCellStyle HorizontalAlign="Center">
                            </FooterCellStyle>
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Region" FieldName="RegionNameAbbrev" Visible="false"
                            Width="75px">
                            <HeaderStyle HorizontalAlign="Center" Wrap="True"></HeaderStyle>
                            <CellStyle HorizontalAlign="Center">
                            </CellStyle>
                            <FooterCellStyle HorizontalAlign="Center">
                            </FooterCellStyle>
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Company Status" FieldName="CompanyStatusName" Visible="false" 
                            Settings-HeaderFilterMode="CheckedList" Width="100px">
                            <HeaderStyle HorizontalAlign="Center" Wrap="True"></HeaderStyle>
                            <CellStyle HorizontalAlign="Center">
                            </CellStyle>
                            <FooterCellStyle HorizontalAlign="Center">
                            </FooterCellStyle>
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataComboBoxColumn FieldName="UserFullNameLogon" UnboundType="String"
                            ReadOnly="True" Name="EHS Consultant" Width="200px" Caption="EHS Consultant"
                            VisibleIndex="0" SortIndex="0" SortOrder="Ascending" Settings-SortMode="DisplayText">
                            <PropertiesComboBox DataSourceID="UsersEhsConsultantsDataSource" TextField="UserFullName"
                                ValueField="UserFullNameLogon" ValueType="System.String" DropDownHeight="150px"
                                NullDisplayText="(Not Assigned)">
                            </PropertiesComboBox>
                            <FooterCellStyle HorizontalAlign="Right">
                            </FooterCellStyle>
                            <FooterTemplate>
                                Total:
                            </FooterTemplate>
                        </dxwgv:GridViewDataComboBoxColumn>
                        
                        <dxwgv:GridViewDataCheckColumn FieldName="Enabled" VisibleIndex="1">
                        </dxwgv:GridViewDataCheckColumn>
                        <dxwgv:GridViewDataTextColumn FieldName="NoAllocated" ReadOnly="True" Caption="# Allocated"
                            VisibleIndex="2">
                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                            <CellStyle HorizontalAlign="Center">
                            </CellStyle>
                            <FooterCellStyle HorizontalAlign="Center">
                            </FooterCellStyle>
                        </dxwgv:GridViewDataTextColumn>
<%--                        <dxwgv:GridViewDataTextColumn FieldName="NoIncomplete" ReadOnly="True" Caption="# Incomplete"
                            Visible="false" ShowInCustomizationForm="false">
                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                            <CellStyle HorizontalAlign="Center">
                            </CellStyle>
                            <FooterCellStyle HorizontalAlign="Center">
                            </FooterCellStyle>
                        </dxwgv:GridViewDataTextColumn>--%>
                        <dxwgv:GridViewDataTextColumn FieldName="NoBeingAssessed" ReadOnly="True" Caption="# Being Assessed"
                            VisibleIndex="4">
                            <HeaderStyle HorizontalAlign="Center" Wrap="True"></HeaderStyle>
                            <CellStyle HorizontalAlign="Center">
                            </CellStyle>
                            <FooterCellStyle HorizontalAlign="Center">
                            </FooterCellStyle>
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn FieldName="NoAssessmentComplete" ReadOnly="True" Caption="# Assessment Complete"
                            VisibleIndex="5">
                            <HeaderStyle HorizontalAlign="Center" Wrap="True"></HeaderStyle>
                            <CellStyle HorizontalAlign="Center">
                            </CellStyle>
                            <FooterCellStyle HorizontalAlign="Center">
                            </FooterCellStyle>
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn FieldName="NoRecommended" ReadOnly="True" Caption="# Recommended"
                            VisibleIndex="6">
                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                            <CellStyle HorizontalAlign="Center">
                            </CellStyle>
                            <FooterCellStyle HorizontalAlign="Center">
                            </FooterCellStyle>
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn FieldName="NoNotRecommended" ReadOnly="True" Caption="# Not Recommended"
                            VisibleIndex="7">
                            <HeaderStyle HorizontalAlign="Center" Wrap="True"></HeaderStyle>
                            <CellStyle HorizontalAlign="Center">
                            </CellStyle>
                            <FooterCellStyle HorizontalAlign="Center">
                            </FooterCellStyle>
                        </dxwgv:GridViewDataTextColumn>
                    </Columns>
                    <SettingsPager>
                        <AllButton Visible="True">
                        </AllButton>
                    </SettingsPager>
                    <SettingsBehavior ColumnResizeMode="NextColumn" />
                    <Settings ShowFilterRow="True" ShowHeaderFilterButton="True" ShowFilterBar="Auto" ShowGroupPanel="True" ShowFooter="True" ShowGroupFooter="VisibleIfExpanded"
                        ShowVerticalScrollBar="True" VerticalScrollableHeight="232"></Settings>
                    <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                        <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
                        </LoadingPanelOnStatusBar>
                        <CollapsedButton Width="11px">
                        </CollapsedButton>
                        <ExpandedButton Width="11px">
                        </ExpandedButton>
                        <DetailCollapsedButton Width="11px">
                        </DetailCollapsedButton>
                        <DetailExpandedButton Width="11px">
                        </DetailExpandedButton>
                        <LoadingPanel Url="~/App_Themes/Office2003BlueOld/GridView/Loading.gif"> <%--Change by Debashis--%>
                        </LoadingPanel>
                    </Images>
                    <ImagesFilterControl>
                        <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
                        </LoadingPanel>
                    </ImagesFilterControl>
                    <Styles CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css">
                        <Header SortingImageSpacing="5px" ImageSpacing="5px">
                        </Header>
                        <LoadingPanel ImageSpacing="10px">
                        </LoadingPanel>
                    </Styles>
                    <StylesEditors>
                        <ProgressBar Height="25px">
                        </ProgressBar>
                    </StylesEditors>
                </dxwgv:ASPxGridView>
            </td>
        </tr>
        <tr>
            <td style="text-align: right; padding-top: 6px;">
                <div align="right">
                    <uc1:exportbuttons runat="server" id="ucExportButtons"></uc1:exportbuttons>
                </div>
            </td>
        </tr>
    </tbody>
</table>
<asp:ObjectDataSource ID="dsQuestionnaire_GetEhsConsultantsWorkLoad" runat="server"
    TypeName="KaiZen.CSMS.Services.QuestionnaireService" SelectMethod="GetEhsConsultantsWorkload">
    <SelectParameters>
        <asp:SessionParameter DefaultValue="%" Name="CurrentYear" SessionField="spVar_Year"
            Type="String" />
    </SelectParameters>
</asp:ObjectDataSource>
