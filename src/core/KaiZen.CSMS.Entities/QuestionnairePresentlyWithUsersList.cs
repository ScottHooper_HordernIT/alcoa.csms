﻿
using System;

namespace KaiZen.CSMS.Entities
{
	/// <summary>
	/// An enum representation of the 'QuestionnairePresentlyWithUsers' table. [No description found in the database]
	/// </summary>
	/// <remark>this enumeration contains the items contained in the table QuestionnairePresentlyWithUsers</remark>
	[Serializable]
	public enum QuestionnairePresentlyWithUsersList
	{
		/// <summary> 
		/// Procurement
		/// </summary>
		[EnumTextValue(@"Procurement")]
		Procurement = 1, 

		/// <summary> 
		/// H&S Assessor
		/// </summary>
		[EnumTextValue(@"H&S Assessor")]
		HSAssessor = 2, 

		/// <summary> 
		/// Supplier
		/// </summary>
		[EnumTextValue(@"Supplier")]
		Supplier = 3, 

		/// <summary> 
		/// CSMS
		/// </summary>
		[EnumTextValue(@"CSMS")]
		CSMS = 4, 

		/// <summary> 
		/// -
		/// </summary>
		[EnumTextValue(@"-")]
		Nobody = 5

	}
}
