﻿#region Using directives

using System;

#endregion

namespace KaiZen.CSMS.Entities
{	
	///<summary>
	/// An object representation of the 'EbiLastTimeOnSiteDistinct_AnySite' view. [No description found in the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class EbiLastTimeOnSiteDistinctAnySite : EbiLastTimeOnSiteDistinctAnySiteBase
	{
		#region Constructors

		///<summary>
		/// Creates a new <see cref="EbiLastTimeOnSiteDistinctAnySite"/> instance.
		///</summary>
		public EbiLastTimeOnSiteDistinctAnySite():base(){}	
		
		#endregion
	}
}
