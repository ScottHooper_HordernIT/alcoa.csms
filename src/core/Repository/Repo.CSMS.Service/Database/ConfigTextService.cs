﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface IConfigTextService
    {
        ConfigText Get(Expression<Func<ConfigText, bool>> where, List<Expression<Func<ConfigText, object>>> includes);
        List<ConfigText> GetMany(int? top, Expression<Func<ConfigText, bool>> where, Func<IQueryable<ConfigText>, IOrderedQueryable<ConfigText>> orderBy, List<Expression<Func<ConfigText, object>>> includes);
        List<ConfigText> GetManyPaged(int pageNumber, int pageSize, Expression<Func<ConfigText, bool>> where, Func<IQueryable<ConfigText>, IOrderedQueryable<ConfigText>> orderBy, List<Expression<Func<ConfigText, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<ConfigText, bool>> where, List<Expression<Func<ConfigText, object>>> includes);
        void Insert(ConfigText item, bool saveChanges = true);
        void Update(ConfigText item, bool saveChanges = true);
        void Delete(ConfigText item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class ConfigTextService : IConfigTextService
    {
        private readonly IRepository<ConfigText> _repository;

        public ConfigTextService(IRepository<ConfigText> repository)
        {
            this._repository = repository;
        }

        public ConfigText Get(Expression<Func<ConfigText, bool>> where, List<Expression<Func<ConfigText, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<ConfigText> GetMany(int? top, Expression<Func<ConfigText, bool>> where, Func<IQueryable<ConfigText>, IOrderedQueryable<ConfigText>> orderBy, List<Expression<Func<ConfigText, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<ConfigText> GetManyPaged(int pageNumber, int pageSize, Expression<Func<ConfigText, bool>> where, Func<IQueryable<ConfigText>, IOrderedQueryable<ConfigText>> orderBy, List<Expression<Func<ConfigText, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<ConfigText, bool>> where, List<Expression<Func<ConfigText, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(ConfigText item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("ConfigText");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(ConfigText item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("ConfigText");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(ConfigText item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("ConfigText");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
