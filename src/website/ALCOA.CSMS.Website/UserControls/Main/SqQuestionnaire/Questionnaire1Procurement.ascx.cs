using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using System.Drawing;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Services;
using KaiZen.CSMS.Data;
using KaiZen.Library;

using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxCallbackPanel;
using DevExpress.Web.ASPxClasses;
using DevExpress.Web.Data;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxPopupControl;
using model = Repo.CSMS.DAL.EntityModels;
using repo = Repo.CSMS.Service.Database;

using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.ComponentModel;

public partial class UserControls_SqQuestionnaire_Questionnaire1Procurement : System.Web.UI.UserControl
{
    Auth auth = new Auth();

    #region Repo
    repo.IEmailTemplateService emailTemplateService = ALCOA.CSMS.Website.Global.GetInstance<repo.IEmailTemplateService>();
    repo.IEmailLogService emailLogService = ALCOA.CSMS.Website.Global.GetInstance<repo.IEmailLogService>();

    repo.IQuestionnaireService questionnaireService = ALCOA.CSMS.Website.Global.GetInstance<repo.IQuestionnaireService>();
    repo.ICompanyService companyService = ALCOA.CSMS.Website.Global.GetInstance<repo.ICompanyService>();
    repo.IQuestionnaireInitialContactAuditService qInitialContactAuditService = ALCOA.CSMS.Website.Global.GetInstance<repo.IQuestionnaireInitialContactAuditService>();
    repo.IConfigService configService = ALCOA.CSMS.Website.Global.GetInstance<repo.IConfigService>();
    repo.IQuestionnaireInitialContactService qicService = ALCOA.CSMS.Website.Global.GetInstance<repo.IQuestionnaireInitialContactService>();
    repo.IQuestionnaireInitialResponseService qirService = ALCOA.CSMS.Website.Global.GetInstance<repo.IQuestionnaireInitialResponseService>();
    repo.IUserService userService = ALCOA.CSMS.Website.Global.GetInstance<repo.IUserService>();
    #endregion

    const string pqRequired = "Yes, PQ is Required";
    const string pqNotRequired = "No, PQ is not Required";
    const string forcedHigh = "Forced High Risk";

    protected void Page_Load(object sender, EventArgs e) { Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); }

    const string sNa = "-";
    const string sYesPqRequired = "Yes, PQ is Required";
    const string sNoPqRequired = "No, PQ is not Required";
    const string sYesPqRequiredOverall = "Yes, Pre-Qualification is Required";
    const string sNoPqRequiredOverall = "No, Pre-Qualification is not Required";
    Color colourGood = Color.Green;
    Color colourBad = Color.Red;
    Color colourNeutral = Color.Black;

    protected void SetPqRequired(ASPxLabel lbl, bool pqRequired)
    {
        if (pqRequired == true)
        {
            lbl.Text = sYesPqRequired;
            lbl.ForeColor = colourBad;
        }
        else
        {
            lbl.Text = sNoPqRequired;
            lbl.ForeColor = colourGood;
        }
    }
    protected void SetPqRequiredNa(ASPxLabel lbl)
    {
        lbl.Text = "-";
        lbl.ForeColor = colourNeutral;
    }
    protected bool MoreThanOneCategorySelected()
    {
        int _SelectedCategories = 0;
        string Cat1 = StringUtilities.Convert.NullSafeObjectToString(rbl2_7.Value);
        string Cat2 = StringUtilities.Convert.NullSafeObjectToString(rbl2_8.Value);
        string Cat3 = StringUtilities.Convert.NullSafeObjectToString(rbl2_9.Value);
        string Cat4 = StringUtilities.Convert.NullSafeObjectToString(rbl2_10.Value);
        string Cat5 = StringUtilities.Convert.NullSafeObjectToString(rbl2_11.Value);
        string Cat6 = StringUtilities.Convert.NullSafeObjectToString(rbl2_12.Value);

        if (Cat1 == "Yes") _SelectedCategories++;
        if (Cat2 == "Yes") _SelectedCategories++;
        if (Cat3 == "Yes") _SelectedCategories++;
        if (Cat4 == "Yes") _SelectedCategories++;
        if (Cat5 == "Yes") _SelectedCategories++;
        if (Cat6 == "Yes") _SelectedCategories++;
        if (_SelectedCategories > 1)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    protected bool AllCategoriesNo()
    {
        int _SelectedCategories = 0;
        string Cat1 = StringUtilities.Convert.NullSafeObjectToString(rbl2_7.Value);
        string Cat2 = StringUtilities.Convert.NullSafeObjectToString(rbl2_8.Value);
        string Cat3 = StringUtilities.Convert.NullSafeObjectToString(rbl2_9.Value);
        string Cat4 = StringUtilities.Convert.NullSafeObjectToString(rbl2_10.Value);
        string Cat5 = StringUtilities.Convert.NullSafeObjectToString(rbl2_11.Value);
        string Cat6 = StringUtilities.Convert.NullSafeObjectToString(rbl2_12.Value);

        if (Cat1 == "No") _SelectedCategories++;
        if (Cat2 == "No") _SelectedCategories++;
        if (Cat3 == "No") _SelectedCategories++;
        if (Cat4 == "No") _SelectedCategories++;
        if (Cat5 == "No") _SelectedCategories++;
        if (Cat6 == "No") _SelectedCategories++;
        if (_SelectedCategories == 6)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    protected bool? CalculateIfPqRequired()
    {
        ASPxLabel lblPqr1 = lbl2_1;
        ASPxLabel lblPqr2 = lbl2_2;
        ASPxLabel lblPqr3 = lbl2_3;
        ASPxLabel lblPqr4 = lbl2_4;
        ASPxLabel lblPqr5 = lbl2_5;
        ASPxLabel lblPqr6 = lbl2_6;
        ASPxLabel lblPqr13 = lbl2_13; //Jolly - Enhancement Spec - 026
        ASPxLabel lblCat1 = lbl2_7;
        ASPxLabel lblCat2 = lbl2_8;
        ASPxLabel lblCat3 = lbl2_9;
        ASPxLabel lblCat4 = lbl2_10;
        ASPxLabel lblCat5 = lbl2_11;
        ASPxLabel lblCat6 = lbl2_12;
        string Pqr1 = StringUtilities.Convert.NullSafeObjectToString(rbl2_1.Value);
        string Pqr2 = StringUtilities.Convert.NullSafeObjectToString(rbl2_2.Value);
        string Pqr3 = StringUtilities.Convert.NullSafeObjectToString(rbl2_3.Value);
        string Pqr4 = StringUtilities.Convert.NullSafeObjectToString(rbl2_4.Value);
        string Pqr5 = StringUtilities.Convert.NullSafeObjectToString(rbl2_5.Value);
        string Pqr6 = StringUtilities.Convert.NullSafeObjectToString(rbl2_6.Value);
        string Pqr13 = StringUtilities.Convert.NullSafeObjectToString(rbl2_13.Value); // Jolly - Enhancement Spec - 026
        string Cat1 = StringUtilities.Convert.NullSafeObjectToString(rbl2_7.Value);
        string Cat2 = StringUtilities.Convert.NullSafeObjectToString(rbl2_8.Value);
        string Cat3 = StringUtilities.Convert.NullSafeObjectToString(rbl2_9.Value);
        string Cat4 = StringUtilities.Convert.NullSafeObjectToString(rbl2_10.Value);
        string Cat5 = StringUtilities.Convert.NullSafeObjectToString(rbl2_11.Value);
        string Cat6 = StringUtilities.Convert.NullSafeObjectToString(rbl2_12.Value);
        int PqrPqRequired = 0;
        int PqrPqNotRequired = 0;

        int CatPqRequired = 0;
        int CatPqNotRequired = 0;
        //if (Cat1 == "Yes") { SetPqRequired(lbl2_7, false); CatPqRequired++; SetPqRequiredNa(lblPqr1); SetPqRequiredNa(lblPqr2); SetPqRequiredNa(lblPqr3); SetPqRequiredNa(lblPqr4); SetPqRequiredNa(lblPqr5); SetPqRequiredNa(lblPqr6); } else { SetPqRequiredNa(lblCat1); }
        //if (Cat2 == "Yes") { SetPqRequired(lbl2_8, false); CatPqNotRequired++; SetPqRequiredNa(lblPqr1); SetPqRequiredNa(lblPqr2); SetPqRequiredNa(lblPqr3); SetPqRequiredNa(lblPqr4); SetPqRequiredNa(lblPqr5); SetPqRequiredNa(lblPqr6); } else { SetPqRequiredNa(lblCat2); }
        //if (Cat3 == "Yes") { SetPqRequired(lbl2_9, true); CatPqRequired++; SetPqRequiredNa(lblPqr1); SetPqRequiredNa(lblPqr2); SetPqRequiredNa(lblPqr3); SetPqRequiredNa(lblPqr4); SetPqRequiredNa(lblPqr5); SetPqRequiredNa(lblPqr6); } else { SetPqRequiredNa(lblCat3); }
        //if (Cat4 == "Yes") { SetPqRequired(lbl2_10, false); CatPqNotRequired++; SetPqRequiredNa(lblPqr1); SetPqRequiredNa(lblPqr2); SetPqRequiredNa(lblPqr3); SetPqRequiredNa(lblPqr4); SetPqRequiredNa(lblPqr5); SetPqRequiredNa(lblPqr6); } else { SetPqRequiredNa(lblCat4); }
        //if (Cat5 == "Yes") { SetPqRequired(lbl2_11, true); CatPqRequired++; SetPqRequiredNa(lblPqr1); SetPqRequiredNa(lblPqr2); SetPqRequiredNa(lblPqr3); SetPqRequiredNa(lblPqr4); SetPqRequiredNa(lblPqr5); SetPqRequiredNa(lblPqr6); } else { SetPqRequiredNa(lblCat5); }
        //if (Cat6 == "Yes") { SetPqRequiredNa(lblCat1); SetPqRequiredNa(lblCat2); SetPqRequiredNa(lblCat3); SetPqRequiredNa(lblCat4); SetPqRequiredNa(lblCat5); SetPqRequiredNa(lblCat6); } else { SetPqRequiredNa(lblCat6); }

        if (Cat1 == "Yes") { SetPqRequired(lblCat1, false); CatPqRequired++; SetPqRequiredNa(lblCat2); SetPqRequiredNa(lblCat3); SetPqRequiredNa(lblCat4); SetPqRequiredNa(lblCat5); SetPqRequiredNa(lblCat6); } else { SetPqRequiredNa(lblCat1); }
        if (Cat2 == "Yes") { SetPqRequired(lblCat2, false); CatPqNotRequired++; SetPqRequiredNa(lblCat1); SetPqRequiredNa(lblCat3); SetPqRequiredNa(lblCat4); SetPqRequiredNa(lblCat5); SetPqRequiredNa(lblCat6); } else { SetPqRequiredNa(lblCat2); }
        if (Cat3 == "Yes") { SetPqRequired(lblCat3, true); CatPqRequired++; SetPqRequiredNa(lblCat1); SetPqRequiredNa(lblCat2); SetPqRequiredNa(lblCat4); SetPqRequiredNa(lblCat5); SetPqRequiredNa(lblCat6); } else { SetPqRequiredNa(lblCat3); }
        if (Cat4 == "Yes") { SetPqRequired(lblCat4, false); CatPqNotRequired++; SetPqRequiredNa(lblCat1); SetPqRequiredNa(lblCat2); SetPqRequiredNa(lblCat3); SetPqRequiredNa(lblCat5); SetPqRequiredNa(lblCat6); } else { SetPqRequiredNa(lblCat4); }
        if (Cat5 == "Yes") { SetPqRequired(lblCat5, true); CatPqRequired++; SetPqRequiredNa(lblCat1); SetPqRequiredNa(lblCat2); SetPqRequiredNa(lblCat3); SetPqRequiredNa(lblCat4); SetPqRequiredNa(lblCat6); } else { SetPqRequiredNa(lblCat5); }

        if (!tblPreQualRequirement.Visible)
        {
            if (Cat6 == "Yes") { SetPqRequired(lblCat6, true); CatPqRequired++; SetPqRequiredNa(lblCat1); SetPqRequiredNa(lblCat2); SetPqRequiredNa(lblCat3); SetPqRequiredNa(lblCat4); SetPqRequiredNa(lblCat5); } else { SetPqRequiredNa(lblCat6); }
        }
        else
        {
            if (Cat6 == "Yes") { SetPqRequiredNa(lblCat1); SetPqRequiredNa(lblCat2); SetPqRequiredNa(lblCat3); SetPqRequiredNa(lblCat4); SetPqRequiredNa(lblCat5); SetPqRequiredNa(lblCat6); } else { SetPqRequiredNa(lblCat6); }
        }

        if (Cat6 == "Yes")
        {
            if (Pqr1 == "No") { SetPqRequired(lblPqr1, true); PqrPqRequired++; } else { if (Pqr1 == "Yes") { SetPqRequired(lblPqr1, false); PqrPqNotRequired++; } else { SetPqRequiredNa(lblPqr1); } }
            if (Pqr2 == "No") { SetPqRequired(lblPqr2, true); PqrPqRequired++; } else { if (Pqr2 == "Yes") { SetPqRequired(lblPqr2, false); PqrPqNotRequired++; } else { SetPqRequiredNa(lblPqr2); } }
            if (Pqr3 == "Yes") { SetPqRequired(lblPqr3, true); PqrPqRequired++; } else { if (Pqr3 == "No") { SetPqRequired(lblPqr3, false); PqrPqNotRequired++; } else { SetPqRequiredNa(lblPqr3); } }
            if (Pqr4 == "Yes") { SetPqRequired(lblPqr4, true); PqrPqRequired++; } else { if (Pqr4 == "No") { SetPqRequired(lblPqr4, false); PqrPqNotRequired++; } else { SetPqRequiredNa(lblPqr4); } }
            if (Pqr5 == "Yes") { SetPqRequired(lblPqr5, true); PqrPqRequired++; } else { if (Pqr5 == "No") { SetPqRequired(lblPqr5, false); PqrPqNotRequired++; } else { SetPqRequiredNa(lblPqr5); } }
            if (Pqr6 == "No") { SetPqRequired(lblPqr6, true); PqrPqRequired++; } else { if (Pqr6 == "Yes") { SetPqRequired(lblPqr6, false); PqrPqNotRequired++; } else { SetPqRequiredNa(lblPqr6); } }
            if (Pqr13 == "Yes") { SetPqRequired(lblPqr13, true); PqrPqRequired++; } else { if (Pqr13 == "No") { SetPqRequired(lblPqr13, false); PqrPqNotRequired++; } else { SetPqRequiredNa(lblPqr13); } }

        }
        else
        {
            if (Pqr1 == "No") { SetPqRequiredNa(lblPqr1); PqrPqRequired++; } else { if (Pqr1 == "Yes") { SetPqRequiredNa(lblPqr1); PqrPqNotRequired++; } else { SetPqRequiredNa(lblPqr1); } }
            if (Pqr2 == "No") { SetPqRequiredNa(lblPqr2); PqrPqRequired++; } else { if (Pqr2 == "Yes") { SetPqRequiredNa(lblPqr2); PqrPqNotRequired++; } else { SetPqRequiredNa(lblPqr2); } }
            if (Pqr3 == "Yes") { SetPqRequiredNa(lblPqr3); PqrPqRequired++; } else { if (Pqr3 == "No") { SetPqRequiredNa(lblPqr3); PqrPqNotRequired++; } else { SetPqRequiredNa(lblPqr3); } }
            if (Pqr4 == "Yes") { SetPqRequiredNa(lblPqr4); PqrPqRequired++; } else { if (Pqr4 == "No") { SetPqRequiredNa(lblPqr4); PqrPqNotRequired++; } else { SetPqRequiredNa(lblPqr4); } }
            if (Pqr5 == "Yes") { SetPqRequiredNa(lblPqr5); PqrPqRequired++; } else { if (Pqr5 == "No") { SetPqRequiredNa(lblPqr5); PqrPqNotRequired++; } else { SetPqRequiredNa(lblPqr5); } }
            if (Pqr6 == "No") { SetPqRequiredNa(lblPqr6); PqrPqRequired++; } else { if (Pqr6 == "Yes") { SetPqRequiredNa(lblPqr6); PqrPqNotRequired++; } else { SetPqRequiredNa(lblPqr6); } }
            if (Pqr13 == "Yes") { SetPqRequiredNa(lblPqr13); PqrPqRequired++; } else { if (Pqr13 == "No") { SetPqRequiredNa(lblPqr13); PqrPqNotRequired++; } else { SetPqRequiredNa(lblPqr13); } }
        }

        bool? PqRequired;
        if (Cat1 == "Yes" || Cat2 == "Yes" || Cat4 == "Yes")
        {
            PqRequired = false;
        }
        else
        {
            if (CatPqRequired > 0)
            {
                PqRequired = true;
            }
            else
            {
                if (PqrPqRequired > 0)
                {
                    PqRequired = true;
                }
                else
                {
                    PqRequired = false;
                }
            }
        }

        if (PqRequired == true)
        {
            lbl2_Overall.Text = sYesPqRequiredOverall;
            lbl2_Overall.ForeColor = colourBad;
        }
        else
        {
            if (PqRequired == false)
            {
                lbl2_Overall.Text = sNoPqRequiredOverall;
                lbl2_Overall.ForeColor = colourGood;
            }
            else
            {
                lbl2_Overall.Text = "-";
                lbl2_Overall.ForeColor = colourNeutral;
            }
        }

        return PqRequired;
    }

    private void moduleInit(bool postBack)
    {
        switch (SessionHandler.spVar_Questionnaire_FrontPageLink)
        {
            case "QuestionnaireReport_Overview":
                hlQuestionnaire.Text = "Australian Safety Qualification Information > Qualified Contractors";
                hlQuestionnaire.ToolTip = "Go back to Qualified Contractors Page";
                hlQuestionnaire.NavigateUrl = "~/Reports_SafetyPQOverview.aspx";
                break;
            case "QuestionnaireReport_Expire":
                hlQuestionnaire.Text = "Australian Safety Qualification Information > Expiry Date";
                hlQuestionnaire.ToolTip = "Go back to SQ Expiry Date Page";
                hlQuestionnaire.NavigateUrl = "~/Reports_SafetyPQ.aspx?q=expire";
                break;
            case "QuestionnaireReport_Progress":
                hlQuestionnaire.Text = "Australian Safety Qualification Information > Status";
                hlQuestionnaire.ToolTip = "Go back to SQ Status Page";
                hlQuestionnaire.NavigateUrl = "~/Reports_SafetyPQStatus.aspx";
                break;
            case "EbiContractorsOnSite":
                hlQuestionnaire.Text = "Reports / Charts > Compliance Reports > Contractors On-Site (EBI)";
                hlQuestionnaire.ToolTip = "Go back to Contractors On-Site (EBI) Page";
                hlQuestionnaire.NavigateUrl = "~/NonComplianceEbiOnSite.aspx";
                break;
            default: //Questionnaire
                hlQuestionnaire.Text = "Australian Safety Qualification Process";
                hlQuestionnaire.ToolTip = "Go back to Safety Qualification Page";
                hlQuestionnaire.NavigateUrl = "~/SafetyPQ_Questionnaire.aspx";
                break;
        }

        if (!Page.IsCallback)
        {
            qAnswer9_1.ClientEnabled = false;
        }

        if (!postBack)
        {
            bool print = false;
            string q = "";
            try
            {
                q = Request.QueryString["q"];
                if (Request.QueryString["printlayout"] != null)
                {
                    if (Request.QueryString["printlayout"] == "yes")
                    {
                        print = true;
                    }
                }

                LinkButton1.PostBackUrl = String.Format("~/SafetyPQ_QuestionnaireModify.aspx{0}", QueryStringModule.Encrypt("q=" + q));
                qAnswer9_1.DataSourceID = "CompaniesDataSource";
                qAnswer9_1.TextField = "CompanyName";
                qAnswer9_1.ValueField = "CompanyId";
                qAnswer9_1.DataBind();
                qAnswer9_1.Text = "Alcoa";

                qAnswer2_0.DataSourceID = "UsersEhsConsultantsDataSource2";
                qAnswer2_0.TextField = "UserFullName";
                qAnswer2_0.ValueField = "EhsConsultantId";
                qAnswer2_0.DataBind();
                qAnswer2_0.Items.Add("(Not Assigned)", null);
                //qAnswer2_0.ClientSideEvents.Init = "function(s, e) {s.InsertItem('Null', '(Not Assigned)', '');}";

                LoadEHS(Convert.ToInt32(q));

                if (q != "New")
                {
                    QuestionnaireService _qs = new QuestionnaireService();
                    Questionnaire _q = _qs.GetByQuestionnaireId(Convert.ToInt32(q));

                    if (_q == null)
                    {
                        throw new Exception("Invalid Questionnaire Id");
                    }
                    else
                    {
                        BindLocationData(_q.CompanyId);
                        Configuration configuration = new Configuration();
                        if (auth.RoleId == (int)RoleList.Administrator || auth.Email == configuration.GetValue(ConfigList.DefaultEhsConsultant_WAO)
                                                                      || auth.Email == configuration.GetValue(ConfigList.DefaultEhsConsultant_VICOPS))
                        {
                            if (_q.InitialStatus == (int)QuestionnaireStatusList.BeingAssessed)
                            {
                                qAnswer2_0.ReadOnly = false;
                                qAnswer2_0.Enabled = true;
                            }
                        }

                        if (_q.SubContractor == true)
                        {
                            ASPxRoundPanel2.Visible = true;
                            ASPxRoundPanel3.Visible = true;
                            ASPxRoundPanel4.Visible = true;


                            rpbr5.Visible = false;
                            Aspxroundpanel5.Visible = false;

                            Aspxroundpanel6.Visible = true; //subcontractor needs to select site their working on!
                            Aspxroundpanel6.HeaderText = "5";

                            gridLocations.Columns["Location Sponsor"].Visible = false;
                            gridLocations.Columns["Area"].Visible = true;
                            lblLocationText.Text = "Location(s) Requesting Service including area:";

                            //Changed by Pankaj Dikshit for <Issue ID# 2381, 04-May-12>
                            rpbr7.Visible = true;

                            rpbr8.Visible = false;
                            rpbr9.Visible = false;
                            rpbr10.Visible = false;
                            rpbr11.Visible = false;
                            Aspxroundpanel7.Visible = false;
                            Aspxroundpanel8.Visible = false;
                            Aspxroundpanel9.Visible = false;
                            Aspxroundpanel10.Visible = false;

                            //Changed by Pankaj Dikshit for <Issue ID# 2381, 04-May-12> - Start
                            Aspxroundpanel11.Visible = true;
                            Aspxroundpanel11.HeaderText = "6";
                            tblPreQualRequirement.Visible = false;
                            tblIsPreQualRequired.Visible = false;

                            //ASPxRoundPanel12.Visible = true;
                            //ASPxRoundPanel12.HeaderText = "6";
                            //qAnswer12_1.ValidationSettings.RequiredField.ErrorText = qAnswer12_1.ValidationSettings.RequiredField.ErrorText.Replace("12", "6");

                            //ASPxRoundPanel13.Visible = true;
                            //ASPxRoundPanel13.HeaderText = "7";
                            //qAnswer13.ValidationSettings.RequiredField.ErrorText = qAnswer13.ValidationSettings.RequiredField.ErrorText.Replace("11", "7");

                            ASPxRoundPanel12.Visible = true;
                            ASPxRoundPanel12.HeaderText = "7";
                            qAnswer12_1.ValidationSettings.RequiredField.ErrorText = qAnswer12_1.ValidationSettings.RequiredField.ErrorText.Replace("12", "7");

                            ASPxRoundPanel13.Visible = true;
                            ASPxRoundPanel13.HeaderText = "8";
                            qAnswer13.ValidationSettings.RequiredField.ErrorText = qAnswer13.ValidationSettings.RequiredField.ErrorText.Replace("11", "8");

                            ASPxRoundPanel14.Visible = true;
                            ASPxRoundPanel14.HeaderText = "9";//Added by Pankaj
                            //Changed by Pankaj Dikshit for <Issue ID# 2381, 04-May-12> - End


                            setHealthCheckTableVisibility(); //DT 228 (Enhancement 086) Cindi Thornton 21/9/15

                            if (auth.CompanyStatus2Id == (int)CompanyStatus2List.SubContractor)
                            {
                                hlQuestionnaire.Text = "WA SubContractor Process";
                                hlQuestionnaire.ToolTip = "Go back to WA SubContractor Process Page";
                                hlQuestionnaire.NavigateUrl = "~/Default.aspx";
                            }

                            btnHome2.Visible = false;
                        }

                        //Requalify Time.

                        SessionHandler.spVar_SafetyPlans_QuestionnaireId = q;
                        if (auth.RoleId == (int)RoleList.Contractor || auth.RoleId == (int)RoleList.PreQual)
                        {
                            int RsCount = 0;
                            QuestionnaireContractorRsFilters queryContractorRs = new QuestionnaireContractorRsFilters();
                            queryContractorRs.Append(QuestionnaireContractorRsColumn.ContractorCompanyId, auth.CompanyId.ToString());
                            queryContractorRs.Append(QuestionnaireContractorRsColumn.SubContractorCompanyId, _q.CompanyId.ToString());
                            TList<QuestionnaireContractorRs> listContractorRs = DataRepository.QuestionnaireContractorRsProvider.GetPaged(queryContractorRs.ToString(), null, 0, 100, out RsCount);

                            if (_q.CompanyId != auth.CompanyId && RsCount == 0)
                            {
                                throw new Exception("You are not authorised to view this page.");
                            }
                        }

                        QuestionnaireStatus qs = new QuestionnaireStatus();
                        QuestionnaireStatusService qss = new QuestionnaireStatusService();
                        qs = qss.GetByQuestionnaireStatusId(_q.InitialStatus);
                        lblStatus.Text = qs.QuestionnaireStatusDesc;

                        CompaniesService companiesService = new CompaniesService();
                        Companies companiesEntity = companiesService.GetByCompanyId(_q.CompanyId);
                        lblCompanyName.Text = companiesEntity.CompanyName;

                        if (qs.QuestionnaireStatusDesc == "Being Assessed") lblStatus.Text = "Complete";

                        if (_q.Status == (int)QuestionnaireStatusList.BeingAssessed || _q.InitialStatus >= (int)QuestionnaireStatusList.BeingAssessed || companiesEntity.Deactivated == true)
                        {
                            //Read Only
                            ReadOnly(true);
                            gridLocations.Columns[0].Visible = false;
                            gridLocations.Enabled = false;
                            lblSave.Text = "Read Only";
                            if (companiesEntity.Deactivated == true) lblSave.Text += " - Company De-Activated";

                            if (_q.InitialStatus == (int)QuestionnaireStatusList.BeingAssessed)
                            {
                                lblSave.Text = "Read Only. Awaiting Reviewal by Lead H&S Consultants.";
                                if (auth.RoleId == (int)RoleList.Administrator || auth.Email == configuration.GetValue(ConfigList.DefaultEhsConsultant_WAO)
                                                                      || auth.Email == configuration.GetValue(ConfigList.DefaultEhsConsultant_VICOPS))
                                {
                                    ReadOnly(false);
                                    //btnSubmit.Enabled = true;
                                    //btnSubmitTop.Enabled = true;
                                    btnSubmit.Visible = false;
                                    btnSubmitTop.Visible = false;
                                    btnApproveTop.Visible = true;
                                    btnApproveBottom.Visible = true;
                                    btnNotApproveTop.Visible = true;
                                    btnNotApproveBottom.Visible = true;
                                    gridLocations.Columns[0].Visible = true;
                                    gridLocations.Enabled = true;
                                }
                            }
                        }


                        int count = 0;
                        QuestionnaireInitialResponseFilters qirFilters = new QuestionnaireInitialResponseFilters();
                        qirFilters.Append(QuestionnaireInitialResponseColumn.QuestionnaireId, _q.QuestionnaireId.ToString());
                        TList<QuestionnaireInitialResponse> qirTList =
                                DataRepository.QuestionnaireInitialResponseProvider.GetPaged(qirFilters.ToString(), null, 0, 100, out count);

                        bool SubContractor = false;
                        if (_q.SubContractor == true) SubContractor = true;

                        PopulateCompanyDetails(_q); //DT231 - new fields

                        if (count > 0) LoadAnswers(Convert.ToInt32(q), SubContractor);
                        if (SubContractor == true)
                            setHealthCheckTableVisibility(); //DT 228 (Enhancement 086) Cindi Thornton 21/9/15 - Moved from line 402 by Ashley Goldstraw 6/11/2015
                        if (auth.RoleId == (int)RoleList.PreQual || auth.RoleId == (int)RoleList.Contractor)
                        //throw new Exception("Only Procurement Personal are allowed to do this.");
                        {
                            ASPxPopupControl1.PopupElementID = "";
                            ASPxPopupControl2.PopupElementID = "";
                            ASPxButton1.Enabled = false;
                            btnEdit.Enabled = false;
                            btnVerify.Enabled = false;
                            btnVerifyConfirm.Enabled = false;
                        }

                        if (Request.QueryString["s"] != null)
                        {
                            if (Request.QueryString["s"] == "1") lblSave.Text = "Saved Successfully.";
                            if (Request.QueryString["s"] == "2") lblSave.Text = "Contact Verification E-Mail Sent!";
                            if (Request.QueryString["s"] == "3") lblSave.Text = "Contact Verified!";
                        }

                        CompaniesService cService = new CompaniesService();
                        Companies c = cService.GetByCompanyId(_q.CompanyId);
                        if (c.Deactivated == true
                            || c.CompanyStatusId == (int)CompanyStatusList.InActive_SQ_Incomplete
                            || c.CompanyStatusId == (int)CompanyStatusList.InActive_No_Requal_Required)
                        {
                            ReadOnly(true);
                        }
                    }
                }


                switch (auth.RoleId)
                {
                    case ((int)RoleList.Reader):
                        //Readers can do as they wish..
                        //btnSave.Enabled = false;
                        //btnSubmit.Enabled = false;
                        break;
                    case ((int)RoleList.Contractor):
                        //AutoFill();
                        break;
                    case ((int)RoleList.Administrator):

                        break;
                    case ((int)RoleList.PreQual):
                        btnSave.Enabled = false;
                        btnSubmit.Enabled = false;
                        btnSubmitTop.Enabled = false;
                        break;
                    default:
                        // RoleList.Disabled or No Role
                        Response.Redirect(String.Format("AccessDenied.aspx?error={0}", Server.UrlEncode("Unrecognised User.")));
                        break;
                }
                if (print)
                {
                    //disable save options etc.
                    btnHome.Enabled = false;
                    btnSave.Enabled = false;
                    btnHome2.Enabled = false;
                    btnSave2.Enabled = false;
                    btnSubmit.Enabled = false;
                    btnSubmitTop.Enabled = false;
                    hlQuestionnaire.Enabled = false;
                    LinkButton1.Enabled = false;
                    pnlPrint.Enabled = false;
                    //hide print button:todo
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromContext(Context).Raise(ex);
                Response.Redirect(String.Format("AccessDenied.aspx?error={0}", ex.Message));
            }
        }
        else
        {
            BindLocationData();
        }
    }

    protected void ReadOnly(bool status)
    {
        if (!status) // Not Read Only
        {
            btnSave.Enabled = true;
            btnSave2.Enabled = true;
            //btnSubmit.Enabled = true;
        }
        else // Read Only
        {
            btnSave.Enabled = false;
            btnSave2.Enabled = false;
            btnSubmit.Enabled = false;
            btnSubmitTop.Enabled = false;
        }

        //qAnswer1_1.ReadOnly = status;
        //qAnswer1_2.ReadOnly = status;
        //qAnswer1_3.ReadOnly = status;
        //qAnswer1_4.ReadOnly = status;
        //qAnswer1_5.ReadOnly = status;
        qAnswer2_0.ReadOnly = status;
        qAnswer2.ReadOnly = status;
        qAnswer2_1.ReadOnly = status;
        qAnswer2_2.ReadOnly = status;
        qAnswer2_3.ReadOnly = status;
        qAnswer3.ReadOnly = status;
        qAnswer4.ReadOnly = status;
        qAnswer4_1.ReadOnly = status;
        qAnswer5.ReadOnly = status;
        qAnswer7.ReadOnly = status;
        qAnswer8.ReadOnly = status;
        qAnswer9.ReadOnly = status;
        qAnswer9_1.ReadOnly = status;
        qAnswer12_1.ReadOnly = status;
        qAnswer13.ReadOnly = status;
        qAnswer14.ReadOnly = status;

        rbl2_1.ReadOnly = status;
        rbl2_2.ReadOnly = status;
        rbl2_3.ReadOnly = status;
        rbl2_4.ReadOnly = status;
        rbl2_5.ReadOnly = status;
        rbl2_6.ReadOnly = status;
        rbl2_13.ReadOnly = status; //Cindi Thornton DT216 (change 026) - add new question
        rbl2_7.ReadOnly = status;
        rbl2_8.ReadOnly = status;
        rbl2_9.ReadOnly = status;
        rbl2_10.ReadOnly = status;
        rbl2_11.ReadOnly = status;
        rbl2_12.ReadOnly = status;
        rbl1.ReadOnly = status;
        rbl2.ReadOnly = status;
        rbl3.ReadOnly = status;
        rbl4.ReadOnly = status;
        rbl5.ReadOnly = status;
        rbl6.ReadOnly = status;
        rbl7.ReadOnly = status;
        rbl8.ReadOnly = status;
        rbl9.ReadOnly = status;
        rbl10.ReadOnly = status;
        rbl11.ReadOnly = status;

    }
    protected static void SetLabel(String Selected, ASPxLabel lbl, String YesText)
    {
        Color clr = Color.Black;
        switch (YesText)
        {
            case "Low Risk":
                clr = Color.Green;
                break;
            case "Medium Risk":
                clr = Color.Gold;
                break;
            case "High Risk":
                clr = Color.Orange;
                break;
            case "Forced High Risk": //forcedHigh
                clr = Color.Red;
                break;
            default:
                break;
        }

        switch (Selected)
        {
            case "Yes":
                lbl.ForeColor = clr;
                lbl.Text = YesText;
                break;
            default:
                lbl.ForeColor = System.Drawing.Color.Black;
                lbl.Text = "-";
                break;
        }
    }

    protected void SetOverall()
    {
        if (GetVal(rbl11.Value) == "Yes") { SetLabel("Yes", lbl11, forcedHigh); };
        if (GetVal(rbl10.Value) == "Yes") { SetLabel("Yes", lbl10, forcedHigh); };
        if (GetVal(rbl9.Value) == "Yes") { SetLabel("Yes", lbl9, forcedHigh); };
        if (GetVal(rbl8.Value) == "Yes") { SetLabel("Yes", lbl8, forcedHigh); };
        if (GetVal(rbl7.Value) == "Yes") { SetLabel("Yes", lbl7, "High Risk"); };
        if (GetVal(rbl6.Value) == "Yes") { SetLabel("Yes", lbl6, "High Risk"); };
        if (GetVal(rbl5.Value) == "Yes") { SetLabel("Yes", lbl5, "Medium Risk"); };
        if (GetVal(rbl4.Value) == "Yes") { SetLabel("Yes", lbl4, "Medium Risk"); };
        if (GetVal(rbl3.Value) == "Yes") { SetLabel("Yes", lbl3, "Low Risk"); };
        if (GetVal(rbl2.Value) == "Yes") { SetLabel("Yes", lbl2, "Low Risk"); };
        if (GetVal(rbl1.Value) == "Yes") { SetLabel("Yes", lbl1, "Low Risk"); };

        if (GetVal(rbl11.Value) == "Yes") { SetLabel("Yes", lblOverall, forcedHigh); return; };
        if (GetVal(rbl10.Value) == "Yes") { SetLabel("Yes", lblOverall, forcedHigh); return; };
        if (GetVal(rbl9.Value) == "Yes") { SetLabel("Yes", lblOverall, forcedHigh); return; };
        if (GetVal(rbl8.Value) == "Yes") { SetLabel("Yes", lblOverall, forcedHigh); return; };
        if (GetVal(rbl7.Value) == "Yes") { SetLabel("Yes", lblOverall, "High Risk"); return; };
        if (GetVal(rbl6.Value) == "Yes") { SetLabel("Yes", lblOverall, "High Risk"); return; };
        if (GetVal(rbl5.Value) == "Yes") { SetLabel("Yes", lblOverall, "Medium Risk"); return; };
        if (GetVal(rbl4.Value) == "Yes") { SetLabel("Yes", lblOverall, "Medium Risk"); return; };
        if (GetVal(rbl3.Value) == "Yes") { SetLabel("Yes", lblOverall, "Low Risk"); return; };
        if (GetVal(rbl2.Value) == "Yes") { SetLabel("Yes", lblOverall, "Low Risk"); return; };
        if (GetVal(rbl1.Value) == "Yes") { SetLabel("Yes", lblOverall, "Low Risk"); return; };

    }
    protected void SetOverall2(ASPxLabel lbl)
    {
        lblOverall.Text = lbl.Text;
        lblOverall.ForeColor = lbl.ForeColor;
        //ASPxCallBackPanelOverall.DataBind();
    }
    protected void SetLabelInit(ASPxRadioButtonList rb, String Selected, ASPxLabel lbl, String YesText)
    {
        rb.Value = Selected;
        SetLabel(Selected, lbl, YesText);
        SetOverall();
    }

    protected void ASPxCallbackPanel1_Callback(object source, CallbackEventArgsBase e)
    {
        SetLabel(e.Parameter, lbl1, "Low Risk");
        SetOverall();
    }
    protected void ASPxCallbackPanel2_Callback(object source, CallbackEventArgsBase e)
    {
        SetLabel(e.Parameter, lbl2, "Low Risk");
        SetOverall();
    }
    protected void ASPxCallbackPanel3_Callback(object source, CallbackEventArgsBase e)
    {
        SetLabel(e.Parameter, lbl3, "Low Risk");
        SetOverall();
    }
    protected void ASPxCallbackPanel4_Callback(object source, CallbackEventArgsBase e)
    {
        SetLabel(e.Parameter, lbl4, "Medium Risk");
        SetOverall();
    }
    protected void ASPxCallbackPanel5_Callback(object source, CallbackEventArgsBase e)
    {
        SetLabel(e.Parameter, lbl5, "Medium Risk");
        SetOverall();
    }
    protected void ASPxCallbackPanel6_Callback(object source, CallbackEventArgsBase e)
    {
        SetLabel(e.Parameter, lbl6, "High Risk");
        SetOverall();
    }
    protected void ASPxCallbackPanel7_Callback(object source, CallbackEventArgsBase e)
    {
        SetLabel(e.Parameter, lbl7, "High Risk");
        SetOverall();
    }
    protected void ASPxCallbackPanel8_Callback(object source, CallbackEventArgsBase e)
    {
        SetLabel(e.Parameter, lbl8, forcedHigh);
        SetOverall();
    }
    protected void ASPxCallbackPanel9_Callback(object source, CallbackEventArgsBase e)
    {
        SetLabel(e.Parameter, lbl9, forcedHigh);
        SetOverall();
    }
    protected void ASPxCallbackPanel10_Callback(object source, CallbackEventArgsBase e)
    {
        SetLabel(e.Parameter, lbl10, forcedHigh);
        SetOverall();
    }
    protected void ASPxCallbackPanel11_Callback(object source, CallbackEventArgsBase e)
    {
        SetLabel(e.Parameter, lbl11, forcedHigh);
        SetOverall();
    }
    protected void ASPxCallBackPanelOverall_Callback(object source, CallbackEventArgsBase e)
    {
        SetOverall();
    }


    //DT 228 Cindi Thornton 21/9/15 - new callback
    protected void pnlHealthCheck_Callback(object source, CallbackEventArgsBase e)
    {
        setHealthCheckTableVisibility();
    }

    //protected void SetLabel2(string Selected, ASPxLabel lbl2_, int qNo, bool overall)
    //{
    //    Color clr = Color.Black;
    //    string text = "";
    //    bool PQRequired = false;

    //    string _pqNotRequired = pqNotRequired;
    //    string _pqRequired = pqRequired;

    //    switch (qNo)
    //    {
    //        case 1:
    //            if (Selected == "No") { PQRequired = true; };
    //            break;
    //        case 2:
    //            if (Selected == "No") { PQRequired = true; };
    //            break;
    //        case 3:
    //            if (Selected == "Yes") { PQRequired = true; };
    //            break;
    //        case 4:
    //            if (Selected == "Yes") { PQRequired = true; };
    //            break;
    //        case 5:
    //            if (Selected == "Yes") { PQRequired = true; };
    //            break;
    //        case 6:
    //            if (Selected == "No") { PQRequired = true; };
    //            break;
    //        default:
    //            break;
    //    }

    //    switch (PQRequired)
    //    {
    //        case false:
    //            clr = Color.Green;
    //            text = _pqNotRequired;
    //            break;
    //        case true:
    //            clr = Color.Red;
    //            text = _pqRequired;
    //            break;
    //        default:
    //            break;
    //    }

    //    lbl2_.ForeColor = clr;

    //    if (lbl2_ == lbl2_Overall)
    //            lbl2_.Text = text.Replace("PQ", "Pre-Qualification");
    //    else
    //            lbl2_.Text = text;
    //}
    //protected void SetOverall2()
    //{
    //    lbl2_Overall.Text = pqNotRequired.Replace("PQ", "Pre-Qualification") ;
    //    lbl2_Overall.ForeColor = Color.Green;

    //    if(!string.IsNullOrEmpty(GetVal(rbl2_6.Value))) { SetLabel2(GetVal(rbl2_6.Value), lbl2_6, 6, false); };
    //    if (!string.IsNullOrEmpty(GetVal(rbl2_5.Value))) { SetLabel2(GetVal(rbl2_5.Value), lbl2_5, 5, false); };
    //    if (!string.IsNullOrEmpty(GetVal(rbl2_4.Value))) { SetLabel2(GetVal(rbl2_4.Value), lbl2_4, 4, false); };
    //    if (!string.IsNullOrEmpty(GetVal(rbl2_3.Value))) { SetLabel2(GetVal(rbl2_3.Value), lbl2_3, 3, false); };
    //    if (!string.IsNullOrEmpty(GetVal(rbl2_2.Value))) { SetLabel2(GetVal(rbl2_2.Value), lbl2_2, 2, false); };
    //    if (!string.IsNullOrEmpty(GetVal(rbl2_1.Value))) { SetLabel2(GetVal(rbl2_1.Value), lbl2_1, 1, false); };

    //    if (GetVal(rbl2_6.Value) == "No") { SetLabel2("No", lbl2_Overall, 6, true); return; };
    //    if (GetVal(rbl2_5.Value) == "Yes") { SetLabel2("Yes", lbl2_Overall, 5, true); return; };
    //    if (GetVal(rbl2_4.Value) == "Yes") { SetLabel2("Yes", lbl2_Overall, 4, true); return; };
    //    if (GetVal(rbl2_3.Value) == "Yes") { SetLabel2("Yes", lbl2_Overall, 3, true); return; };
    //    if (GetVal(rbl2_2.Value) == "No") { SetLabel2("No", lbl2_Overall, 2, true); return; };
    //    if (GetVal(rbl2_1.Value) == "No") { SetLabel2("No", lbl2_Overall, 1, true); return; };

    //}
    //protected void SetLabelInit2(ASPxRadioButtonList rb, String Selected, ASPxLabel lbl2_, int qNo)
    //{
    //    rb.Value = Selected;
    //    SetLabel2(Selected, lbl2_, qNo, false);
    //}

    //protected void ASPxCallbackPanel2_1_Callback(object source, CallbackEventArgsBase e)
    //{
    //    SetLabel2(e.Parameter, lbl2_1, 1, false);
    //    SetOverall2();
    //}
    //protected void ASPxCallbackPanel2_2_Callback(object source, CallbackEventArgsBase e)
    //{
    //    SetLabel2(e.Parameter, lbl2_2, 2, false);
    //    SetOverall2();
    //}
    //protected void ASPxCallbackPanel2_3_Callback(object source, CallbackEventArgsBase e)
    //{
    //    SetLabel2(e.Parameter, lbl2_3, 3, false);
    //    SetOverall2();
    //}
    //protected void ASPxCallbackPanel2_4_Callback(object source, CallbackEventArgsBase e)
    //{
    //    SetLabel2(e.Parameter, lbl2_4, 4, false);
    //    SetOverall2();
    //}
    //protected void ASPxCallbackPanel2_5_Callback(object source, CallbackEventArgsBase e)
    //{
    //    SetLabel2(e.Parameter, lbl2_5, 5, false);
    //    SetOverall2();
    //}
    //protected void ASPxCallbackPanel2_6_Callback(object source, CallbackEventArgsBase e)
    //{
    //    SetLabel2(e.Parameter, lbl2_6, 6, false);
    //    SetOverall2();
    //}
    //protected void ASPxCallbackPanelOverall2_Callback(object source, CallbackEventArgsBase e)
    //{
    //    SetOverall2();
    //}

    protected void btnHome_Click(object sender, EventArgs e)
    {
        //System.Threading.Thread.Sleep(500);
        Response.Redirect(String.Format("~/SafetyPQ_QuestionnaireModify.aspx?q={0}", Request.QueryString["q"]), true);
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (Page.IsPostBack)
        {
            //System.Threading.Thread.Sleep(1000);
            bool SubContractor = false;

            string q = Request.QueryString["q"];
            QuestionnaireService qs = new QuestionnaireService();
            Questionnaire _q = qs.GetByQuestionnaireId(Convert.ToInt32(q));
            if (_q.SubContractor == true) SubContractor = true;

            Save(false, SubContractor);
        }
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (Page.IsPostBack)
        {
            //bool isValid = DevExpress.Web.ASPxEditors.ASPxComboBox.ValidateEditorsInContainer(this);
            //if (isValid)
            //{
            //System.Threading.Thread.Sleep(1000);
            bool SubContractor = false;

            string q = Request.QueryString["q"];
            QuestionnaireService qs = new QuestionnaireService();
            Questionnaire _q = qs.GetByQuestionnaireId(Convert.ToInt32(q));
            if (_q.SubContractor == true) SubContractor = true;

            Save(true, SubContractor);

            //}
        }
    }

    protected void btnNotApproveOnPopup_click(object sender, EventArgs e)
    {
        try
        {
            string q = Request.QueryString["q"];
            if (q != "New")
            {
                string msg;
                if (mbReasonForNotApprovingTop.Text != null && mbReasonForNotApprovingTop.Text != "")
                { 
                    msg = mbReasonForNotApprovingTop.Text;
                }
                else
                {
                    msg = mbReasonForNotApprovingBottom.Text;
                }
                ResetToProcurementStage(Convert.ToInt32(Request.QueryString["q"]), false, msg);

                Helper.General.Redirect(Page, "Questionnaire reset to Procurement Stage", String.Format("SafetyPQ_QuestionnaireModify.aspx{0}", QueryStringModule.Encrypt("q=" + q)));
            }
        }
        catch (Exception ex)
        {
            Elmah.ErrorSignal.FromContext(Context).Raise(ex);
            Response.Write(String.Format("Error: {0}", ex.Message));
        }
    }
    protected void ResetToProcurementStage(int q, bool reactivating, string msg)
    {
        
        //QuestionnaireService qService = new QuestionnaireService();
        //qService.SendBackToProcurementStage(q);
        // Old repository for sendbacktoprocurementstage replaced with call below as clashes between repositories.
        //DT222 Ashley Goldstraw 11/01/2016
        
        model.Questionnaire questionnaire = questionnaireService.Get(qu => qu.QuestionnaireId == q, null);
        //Send questionnaire back to supplier
        questionnaire.Status = 1;
        questionnaire.InitialStatus = 1;
        questionnaire.MainStatus = 1;
        questionnaire.VerificationStatus = 1;
        questionnaire.QuestionnairePresentlyWithActionId = 4;
        questionnaire.QuestionnairePresentlyWithSince = DateTime.Now;
        questionnaireService.Update(questionnaire, true);

        //Create and send email to procurement contact saying that questionnaire has been rejected
        model.EmailTemplate emailTemplate = emailTemplateService.Get(i => i.EmailTemplateName == "Procurement Questionnaire Requires Amendment", null);
        model.Company company = companyService.Get(c => c.CompanyId == questionnaire.CompanyId, null);
        String url = HttpContext.Current.Request.Url.AbsoluteUri;
        String subject = emailTemplate.EmailTemplateSubject;
        subject = subject.Replace("{CompanyName}", company.CompanyName);

        string mailbody;
        byte[] bodyByte = emailTemplate.EmailBody;
        mailbody = System.Text.Encoding.ASCII.GetString(bodyByte);
        mailbody = mailbody.Replace("{CompanyName}", company.CompanyName);
        mailbody = mailbody.Replace("{Reason}", msg);
        mailbody = mailbody.Replace("{url}",url);
        string strProcContactId = GetVal(qAnswer2.Value);
        int procContactId;
        int.TryParse(strProcContactId, out procContactId);
        model.User procContact = userService.Get(u => u.UserId == procContactId,null);
        //Send the email.
        emailLogService.Insert(procContact.Email, null, null, subject, mailbody, "Questionnaire", true, null, null);

        //Put Details in log
        Helper.Questionnaire.ActionLog.Add((int)QuestionnaireActionList.ProcurementQuestionnaireRejected, q,
        auth.UserId,
        auth.FirstName,
        auth.LastName,
        auth.RoleId,
        auth.CompanyId,
        auth.CompanyName,
        msg,
        null, null, null, null, null);
        //End Added by Ashley Goldstraw DT222
    }
    protected void Save(bool Submit, bool SubContractor)
    {
        int sites;
        if (!Submit)
            sites = 1;
        else
        {
            sites = countNoSitesEntered();

            if (qAnswer4.SelectedItem.Text == "Other" && qAnswer4_1.Text == "")
            {
                throw new Exception("Questionnaire could not be saved. If 'Other' is selected as the primary service, please describe it (Q4).");
            }

            if (SubContractor)
            {
                string q = Request.QueryString["q"];
                QuestionnaireService qService = new QuestionnaireService();
                Questionnaire qu = qService.GetByQuestionnaireId(Convert.ToInt32(q));

                CompanySiteCategoryStandardService cscsService = new CompanySiteCategoryStandardService();
                TList<CompanySiteCategoryStandard> cscsTlist = cscsService.GetByCompanyId(qu.CompanyId);
                foreach (CompanySiteCategoryStandard cscs in cscsTlist)
                {
                    if (cscs.SiteId <= 3 && cscs.Area == "(n/a)")
                    {
                        throw new Exception("Questionnaire could not be saved. If you select either Kwinana, Pinjarra or Wagerup as a site, (n/a) cannot be chosen as the Area (Q5).");
                    }
                }

                if (!Helper.Questionnaire.General.IsValidForSubContractors(Convert.ToInt32(qAnswer14.SelectedItem.Value.ToString()))) throw new Exception("Questionnaire could not be saved. The Company requesting this Sub Contractor is not authorised to engage sub contractors because they have not successfully completed the verification questionnaire process, is a InDirectly supervised company and/or completed their Safety Qualification.");
            }
        }

        if (sites == 0)
        {
            string questionNo = "Question 6";
            if (SubContractor)
            {
                questionNo = "Question 3";
            }
            throw new Exception(String.Format("Questionnaire could not be saved. Please enter at least one site in {0}.\n NB. You must click on 'Update' to save your selection.", questionNo));
        }
        else
        {
            try
            {
                string OldProcurementContact = "-";
                string OldProcurementFunctionalManager = "-";

                string NewProcurementContact = "-";
                string NewProcurementFunctionalManager = "-";

                UsersService uService = new UsersService();
                QuestionnaireInitialResponseService qirService = new QuestionnaireInitialResponseService();
                //

                string q = Request.QueryString["q"];
                QuestionnaireService qs = new QuestionnaireService();
                Questionnaire qu = qs.GetByQuestionnaireId(Convert.ToInt32(q));
                Configuration configuration = new Configuration();
                if (qu != null)
                {
                    if (qu.InitialStatus != (int)QuestionnaireStatusList.AssessmentComplete)
                    {
                        bool beingAssessed = false;
                        if (qu.InitialStatus == (int)QuestionnaireStatusList.BeingAssessed)
                        {
                            beingAssessed = true;
                        }

                        if (qu.InitialCreatedByUserId == null)
                        {
                            qu.InitialCreatedByUserId = auth.UserId;
                            qu.InitialCreatedDate = DateTime.Now;
                        }

                        qu.InitialModifiedByUserId = auth.UserId;
                        qu.InitialModifiedDate = DateTime.Now;
                        qu.ModifiedDate = DateTime.Now;
                        qu.IsVerificationRequired = false; //default

                        if (Submit == false)
                        {
                            qu.InitialStatus = (int)QuestionnaireStatusList.Incomplete;
                        }
                        else
                        {


                            if (!beingAssessed)
                            {
                                qu.InitialStatus = (int)QuestionnaireStatusList.BeingAssessed;
                                qu.InitialSubmittedByUserId = auth.UserId;
                                qu.InitialSubmittedDate = DateTime.Now;
                            }
                            else
                            {
                                qu.InitialStatus = (int)QuestionnaireStatusList.AssessmentComplete;
                            }

                            if (!SubContractor)
                            {
                                SetOverall();
                                qu.IsMainRequired = false;
                                bool? IsPqRequiredOverall = CalculateIfPqRequired();

                                if (lblOverall.Text == "-") { lblOverall.Text = "Low Risk"; };
                                qu.InitialRiskAssessment = lblOverall.Text;

                                if (lblOverall.Text == "High Risk" || lblOverall.Text == forcedHigh)
                                {
                                    qu.IsVerificationRequired = true;
                                }

                                if (IsPqRequiredOverall == false)
                                {
                                    qu.IsMainRequired = false;
                                    qu.IsVerificationRequired = false;
                                }
                                //else if (IsPqRequiredOverall == true)
                                //{
                                //    qu.IsMainRequired = true;
                                //}
                                //Adding by Pankaj Dikshit for <Issue ID# 2398, 16-May-12> - Start
                                if (GetVal(rbl2_7.Value) == "Yes" || GetVal(rbl2_8.Value) == "Yes" || GetVal(rbl2_10.Value) == "Yes")
                                {
                                    qu.IsMainRequired = false;
                                }
                                else if (GetVal(rbl2_9.Value) == "Yes" || GetVal(rbl2_11.Value) == "Yes")
                                {
                                    qu.IsMainRequired = true;
                                }
                                else if (GetVal(rbl2_12.Value) == "Yes")
                                {
                                    if (IsPqRequiredOverall == true)
                                    {
                                        qu.IsMainRequired = true;
                                    }
                                }
                                if (GetVal(qAnswer12_1.Value) == "Yes")
                                {
                                    qu.IsMainRequired = true;
                                    qu.IsVerificationRequired = true;
                                }
                                //Adding by Pankaj Dikshit for <Issue ID# 2398, 16-May-12> - End
                            }
                            else //Changed by Pankaj Dikshit for <Issue ID# 2381, 04-May-12> - Start
                            {
                                qu.InitialRiskAssessment = "SubContractor";
                                //Changed by Pankaj Dikshit for <Issue ID# 2398, 16-May-12> - Start
                                qu.IsMainRequired = false;
                                SetOverall();
                                bool? IsPqRequiredOverall = CalculateIfPqRequired();

                                if (!String.IsNullOrEmpty(GetVal(qAnswer12_1.Value)))
                                {
                                    if ((GetVal(rbl2_9.Value) == "Yes" || GetVal(rbl2_11.Value) == "Yes" || GetVal(rbl2_12.Value) == "Yes") && GetVal(qAnswer12_1.Value) == "No")
                                    {
                                        qu.IsMainRequired = true;
                                    }
                                    else if (GetVal(qAnswer12_1.Value) == "Yes")
                                    {
                                        qu.IsMainRequired = true;
                                        qu.IsVerificationRequired = true;
                                    }
                                }

                                //qu.IsMainRequired = true;
                                //if (StringUtilities.Convert.NullSafeObjectToString(rbl2_12.Value) == "Yes")
                                //{
                                //    qu.IsMainRequired = true;
                                //}
                                //else
                                //{
                                //    if (!String.IsNullOrEmpty(GetVal(qAnswer12_1.Value)))
                                //    {
                                //        if (GetVal(qAnswer12_1.Value) == "Yes")
                                //        {
                                //            qu.IsMainRequired = true;
                                //            qu.IsVerificationRequired = true;
                                //        }
                                //        else if ((GetVal(rbl2_9.Value) == "Yes" || GetVal(rbl2_11.Value) == "Yes" || GetVal(rbl2_12.Value) == "Yes") && GetVal(qAnswer12_1.Value) == "NO")
                                //        { //Added this 'else - block' by Pankaj Dikshit for <Issue ID# 2398, 16-May-12> - Start
                                //            qu.IsMainRequired = true;
                                //        }
                                //    }
                                //    else
                                //    {
                                //        SetOverall();
                                //        bool? IsPqRequiredOverall = CalculateIfPqRequired();

                                //        if (IsPqRequiredOverall == false)
                                //        {
                                //            qu.IsMainRequired = false;
                                //            qu.IsVerificationRequired = false;
                                //        }
                                //        else if (IsPqRequiredOverall == true)
                                //        {
                                //            qu.IsMainRequired = true;
                                //        }
                                //    }
                                //}
                            }
                            //Changed by Pankaj Dikshit for <Issue ID# 2381, 04-May-12> - End

                            bool HighRisk = false;
                            if (!SubContractor)
                            {
                                try
                                {
                                    if (!String.IsNullOrEmpty(GetVal(qAnswer4.Value)))
                                    {
                                        QuestionnaireServicesCategoryService qscS = new QuestionnaireServicesCategoryService();
                                        QuestionnaireServicesCategory qsc = qscS.GetByCategoryId(Convert.ToInt32(GetVal(qAnswer4.Value)));
                                        if (qsc.HighRisk == true) { HighRisk = true; qu.InitialRiskAssessment = forcedHigh; };
                                    }
                                }
                                catch (Exception ex) { Elmah.ErrorSignal.FromContext(Context).Raise(ex); }
                            }
                            qu.InitialCategoryHigh = HighRisk;

                        }

                        qu.ModifiedByUserId = auth.UserId;
                        qu.ModifiedDate = DateTime.Now;

                        if (Submit)
                        {
                            if (!beingAssessed)
                            {
                                qu.QuestionnairePresentlyWithActionId = (int)QuestionnairePresentlyWithActionList.HSAssessorQuestionnaireProcurement;
                                qu.QuestionnairePresentlyWithSince = DateTime.Now;
                            }
                            else
                            {
                                qu.QuestionnairePresentlyWithSince = DateTime.Now;

                                bool usersExist = false;
                                TList<Users> usersList = uService.GetByCompanyId(qu.CompanyId);
                                if (usersList != null)
                                {
                                    if (usersList.Count > 0) usersExist = true;
                                }
                                if (usersExist)
                                {
                                    qu.QuestionnairePresentlyWithActionId = (int)QuestionnairePresentlyWithActionList.SupplierQuestionnaireSupplier;
                                }
                                else
                                {
                                    qu.QuestionnairePresentlyWithActionId = (int)QuestionnairePresentlyWithActionList.CsmsCreateCompanyLogins;
                                }
                            }
                        }

                        //DT 3280 Changes
                        if (beingAssessed)
                        {
                            if (auth.RoleId == (int)RoleList.Administrator || auth.Email == configuration.GetValue(ConfigList.DefaultEhsConsultant_WAO)
                                                                  || auth.Email == configuration.GetValue(ConfigList.DefaultEhsConsultant_VICOPS))
                            {
                                //Proceed with the usual workflow. I.e. valid to submit the procurement.
                            }
                            else
                            {
                                btnSubmit.Enabled = false;
                                throw new Exception("It appears that you are not authorize to Approve this procurement questionnaire. Please check if you are having a role as EHS Consultant or Administrator.");
                            }
                        }

                        qs.Save(qu);

                        QuestionnaireInitialResponse qir_procurementContact_old = qirService.GetByQuestionnaireIdQuestionId(qu.QuestionnaireId, "2");
                        if (qir_procurementContact_old != null)
                        {
                            if (!String.IsNullOrEmpty(qir_procurementContact_old.AnswerText))
                            {
                                Users u = uService.GetByUserId(Convert.ToInt32(qir_procurementContact_old.AnswerText));
                                if (u != null)
                                {
                                    OldProcurementContact = u.FirstName + " " + u.LastName;
                                }
                            }
                        }
                        QuestionnaireInitialResponse qir_procurementFunctionalManager_old = qirService.GetByQuestionnaireIdQuestionId(qu.QuestionnaireId, "2_1");
                        if (qir_procurementFunctionalManager_old != null)
                        {
                            if (!String.IsNullOrEmpty(qir_procurementFunctionalManager_old.AnswerText))
                            {
                                Users u = uService.GetByUserId(Convert.ToInt32(qir_procurementFunctionalManager_old.AnswerText));
                                if (u != null)
                                {
                                    OldProcurementFunctionalManager = u.FirstName + " " + u.LastName;
                                }
                            }
                        }

                        SaveAnswers(Convert.ToInt32(q), SubContractor, Submit);

                        QuestionnaireInitialResponse qir_procurementContact_new = qirService.GetByQuestionnaireIdQuestionId(qu.QuestionnaireId, "2");
                        if (qir_procurementContact_new != null)
                        {
                            if (!String.IsNullOrEmpty(qir_procurementContact_new.AnswerText))
                            {
                                Users u = uService.GetByUserId(Convert.ToInt32(qir_procurementContact_new.AnswerText));
                                if (u != null)
                                {
                                    NewProcurementContact = u.FirstName + " " + u.LastName;
                                }
                            }
                        }
                        QuestionnaireInitialResponse qir_procurementFunctionalManager_new = qirService.GetByQuestionnaireIdQuestionId(qu.QuestionnaireId, "2_1");
                        if (qir_procurementFunctionalManager_new != null)
                        {
                            if (!String.IsNullOrEmpty(qir_procurementFunctionalManager_new.AnswerText))
                            {
                                Users u = uService.GetByUserId(Convert.ToInt32(qir_procurementFunctionalManager_new.AnswerText));
                                if (u != null)
                                {
                                    NewProcurementFunctionalManager = u.FirstName + " " + u.LastName;
                                }
                            }
                        }


                        if (qir_procurementContact_new != null && OldProcurementContact != NewProcurementContact)
                        {
                            if (!String.IsNullOrEmpty(qir_procurementContact_new.AnswerText))
                            {
                                if (Helper.Questionnaire.ActionLog.Add((int)QuestionnaireActionList.ProcurementContactAssigned,
                                                   Convert.ToInt32(q),                         //QuestionnaireId
                                                   auth.UserId,                                //UserId
                                                   auth.FirstName,                             //FirstName
                                                   auth.LastName,                              //LastName
                                                   auth.RoleId,                                //RoleId
                                                   auth.CompanyId,                             //CompanyId
                                                   auth.CompanyName,                           //CompanyName
                                                   "Changed per Procurement Questionnaire",    //Comments
                                                   OldProcurementContact,                      //OldAssigned
                                                   NewProcurementContact,                      //NewAssigned
                                                   null,                                       //NewAssigned2
                                                   null,                                       //NewAssigned3
                                                   null)                                       //NewAssigned4
                                                   == false)
                                {
                                    //for now we do nothing if action log fails.
                                }
                            }
                        }

                        if (qir_procurementFunctionalManager_new != null && OldProcurementFunctionalManager != NewProcurementFunctionalManager)
                        {
                            if (!String.IsNullOrEmpty(qir_procurementFunctionalManager_new.AnswerText))
                            {
                                if (Helper.Questionnaire.ActionLog.Add((int)QuestionnaireActionList.ProcurementFunctionalManagerAssigned,
                                                   Convert.ToInt32(q),                         //QuestionnaireId
                                                   auth.UserId,                                //UserId
                                                   auth.FirstName,                             //FirstName
                                                   auth.LastName,                              //LastName
                                                   auth.RoleId,                                //RoleId
                                                   auth.CompanyId,                             //CompanyId
                                                   auth.CompanyName,                           //CompanyName
                                                   "Changed per Procurement Questionnaire",    //Comments
                                                   OldProcurementFunctionalManager,            //OldAssigned
                                                   NewProcurementFunctionalManager,             //NewAssigned
                                                   null,                                       //NewAssigned2
                                                   null,                                       //NewAssigned3
                                                   null)                                       //NewAssigned4
                                                   == false)
                                {
                                    //for now we do nothing if action log fails.
                                }
                            }
                        }

                        lblSave.Text = "Saved Successfully. Click Home to return to the list of Questionnaires.";

                        if (Submit)
                        {

                            UsersService usersService = new UsersService();
                            Users userEntity;
                            userEntity = usersService.GetByUserId(auth.UserId);

                            CompaniesService companiesService = new CompaniesService();
                            Companies companiesEntity = companiesService.GetByCompanyId(qu.CompanyId);
                            Companies companiesEntity2 = companiesService.GetByCompanyId(auth.CompanyId);

                            //save ehs
                            if (beingAssessed)
                            {
                                if (qAnswer2_0.Value != null)
                                {
                                    if (auth.RoleId == (int)RoleList.Administrator || auth.Email == configuration.GetValue(ConfigList.DefaultEhsConsultant_WAO)
                                                                          || auth.Email == configuration.GetValue(ConfigList.DefaultEhsConsultant_VICOPS))
                                    {
                                        companiesEntity.EhsConsultantId = (int)qAnswer2_0.Value;
                                        companiesService.Save(companiesEntity);
                                    }
                                }
                            }


                            QuestionnaireInitialResponse qir = new QuestionnaireInitialResponse();
                            int intq = Convert.ToInt32(q);
                            qir = qirService.GetByQuestionnaireIdQuestionId(intq, "1_1");
                            string firstname = qir.AnswerText;
                            qir = qirService.GetByQuestionnaireIdQuestionId(intq, "1_2");
                            string lastname = qir.AnswerText;
                            qir = qirService.GetByQuestionnaireIdQuestionId(intq, "1_3");
                            string emailAddress = qir.AnswerText;
                            qir = qirService.GetByQuestionnaireIdQuestionId(intq, "1_4");
                            string jobtitle = qir.AnswerText;
                            qir = qirService.GetByQuestionnaireIdQuestionId(intq, "1_5");
                            string telephone = qir.AnswerText;



                            string required = "";
                            if (qu.IsMainRequired == true) required = "Yes";
                            if (qu.IsMainRequired == false) required = "No";
                            //Email Time
                            string defaultehs_vic = configuration.GetValue(ConfigList.DefaultEhsConsultant_VICOPS);
                            string defaultehs_wao = configuration.GetValue(ConfigList.DefaultEhsConsultant_WAO);
                            //string subject = "";
                            //string body = "";
                            string _c = "Contractor";
                            bool defaultEhsConsultant = false;

                            CompanySiteCategoryStandardService cscsService = new CompanySiteCategoryStandardService();
                            TList<CompanySiteCategoryStandard> cscsTlist = cscsService.GetByCompanyId(qu.CompanyId);
                            SitesService sService = new SitesService();

                            ArrayList _to = new ArrayList();
                            string[] cc = { configuration.GetValue(ConfigList.ContactEmail) };

                            //Changed by Ashley Goldstraw to call use the email template that can be editted by admin.
                            string PreReSq = "Contractor Safety Pre-Qualification";
                            if (qu.IsReQualification) PreReSq = "Contractor Safety Re-Qualification";
                            if (qu.SubContractor == true) PreReSq = "Sub " + PreReSq;

                            string subject1;
                            string mailbody;
                            model.EmailTemplate eMailTemplate = new model.EmailTemplate();
                            if (!beingAssessed)
                            {
                                eMailTemplate = emailTemplateService.Get(i => i.EmailTemplateName == "Contractor Safety Pre-Qualification", null);
                            }
                            else
                            {
                                eMailTemplate = emailTemplateService.Get(i => i.EmailTemplateName == "Contractor Safety Pre-Qualification - Being Assessed", null);
                            }
                            subject1 = eMailTemplate.EmailTemplateSubject;
                            subject1 = subject1.Replace("{CompanyName}", companiesEntity.CompanyName);
                            subject1 = subject1.Replace("{PreOrReQualification}", PreReSq);
                            subject1 = subject1.Replace("{FirstName}", firstname);
                            subject1 = subject1.Replace("{LastName}", lastname);
                            subject1 = subject1.Replace("{EmailAddress}", emailAddress);
                            subject1 = subject1.Replace("{JobTitle}", jobtitle);
                            subject1 = subject1.Replace("{Telephone}", telephone);
                            subject1 = subject1.Replace("{InitialRiskRating}", qu.InitialRiskAssessment);
                            subject1 = subject1.Replace("{SQRequired}", required);
                            subject1 = subject1.Replace("{InitiatedByLastName}", userEntity.LastName);
                            subject1 = subject1.Replace("{InitiatedByFirstName}", userEntity.FirstName);
                            subject1 = subject1.Replace("{InitiatedByCompanyName}", companiesEntity2.CompanyName);
                            subject1 = subject1.Replace("{DateInitiated}", qu.ModifiedDate.ToString());
                            if (SubContractor) subject1 = subject1.Replace("Contractor", "Sub Contractor");

                            byte[] bodyByte = eMailTemplate.EmailBody;
                            mailbody = System.Text.Encoding.ASCII.GetString(bodyByte);
                            mailbody = mailbody.Replace("{CompanyName}", companiesEntity.CompanyName);
                            mailbody = mailbody.Replace("{FirstName}", firstname);
                            mailbody = mailbody.Replace("{LastName}", lastname);
                            mailbody = mailbody.Replace("{EmailAddress}", emailAddress);
                            mailbody = mailbody.Replace("{JobTitle}", jobtitle);
                            mailbody = mailbody.Replace("{Telephone}", telephone);
                            mailbody = mailbody.Replace("{InitialRiskRating}", qu.InitialRiskAssessment);
                            mailbody = mailbody.Replace("{SQRequired}", required);
                            mailbody = mailbody.Replace("{InitiatedByLastName}", userEntity.LastName);
                            mailbody = mailbody.Replace("{InitiatedByFirstName}", userEntity.FirstName);
                            mailbody = mailbody.Replace("{InitiatedByCompanyName}", companiesEntity2.CompanyName);
                            mailbody = mailbody.Replace("{DateInitiated}", qu.ModifiedDate.ToString());
                            /*if (SubContractor) _c = "Sub" + _c;
                            
                            
                           

                            subject = String.Format("{0}: 1 - Procurement Questionnaire submitted for {1}", PreReSq, companiesEntity.CompanyName);
                            if (!beingAssessed) subject = String.Format("{0}: 0 - Procurement Questionnaire submitted for reviewal for {1}", PreReSq, companiesEntity.CompanyName);
                            body = "A " + _c + " Questionnaire has been initiated as follows:\n" +
                                          "First Name: " + firstname + "\n" +
                                          "Last Name: " + lastname + "\n" +
                                          "E-Mail Address: " + emailAddress + "\n" +
                                          "Job Title: " + jobtitle + "\n" +
                                          "Telephone: " + telephone + "\n\n" +
                                          "Initial Risk Rating: " + qu.InitialRiskAssessment + "\n" +
                                          "Is Safety Pre-Qual Required: " + required + "\n" +
                                          "\n\n" +
                                          "Questionnaire Initiated by: " + userEntity.LastName + ", " + userEntity.FirstName + " (" + companiesEntity2.CompanyName + ") at " + qu.ModifiedDate.ToString() + "\n" +
                                          "\n\n";
                            if (!beingAssessed)
                            {
                                body = body.Replace("Questionnaire has been initiated as follows:", "Questionnaire has been initiated and requires your reviewal/approval as follows:");
                                body += "A CONTRACT HAS BEEN INITIATED AND AS THE RELEVANT LEAD SAFETY ASSESSOR YOU HAVE BEEN ASSIGNED TO THIS COMPANY TO BE EVALUATED.";
                            }
                            else
                            {
                                body += "THE SYSTEM ADMINISTRATOR WILL NOW CHECK THAT THE ABOVE CONTRACTOR HAS ACCESS TO THE SYSTEM AND SEND THEM INFORMATION THAT THEY NEED TO COMPLETE THEIR SAFETY QUALIFICATION QUESTIONNAIRE.";
                            }

                            */

                            if (!beingAssessed)
                            {
                                if (cscsTlist.Count > 0)
                                {
                                    bool vic = false;
                                    bool wao = false;
                                    foreach (CompanySiteCategoryStandard cscs in cscsTlist)
                                    {
                                        Sites s = sService.GetBySiteId(cscs.SiteId);
                                        RegionsSitesService rsService = new RegionsSitesService();
                                        TList<RegionsSites> rList = rsService.GetBySiteId(cscs.SiteId);
                                        foreach (RegionsSites _r in rList)
                                        {
                                            if (_r.RegionId == 1) wao = true;
                                            if (_r.RegionId == 4) vic = true;
                                        }
                                    }

                                    if (wao)
                                    {
                                        _to.Add(defaultehs_wao);
                                        if (vic) _to.Add(defaultehs_vic);
                                    }
                                    else
                                    {
                                        if (vic) _to.Add(defaultehs_vic);
                                    }
                                }
                                else //should never happen.
                                {
                                    _to.Add(defaultehs_wao);
                                    _to.Add(defaultehs_vic);
                                }
                                if (_to.Count == 0)
                                {
                                    _to.Add(defaultehs_wao);
                                    _to.Add(defaultehs_vic);
                                }
                            }
                            else
                            {
                                if (qu.IsMainRequired)
                                {
                                    CompaniesService cService = new CompaniesService();
                                    Companies c = cService.GetByCompanyId(qu.CompanyId);

                                    if (c.EhsConsultantId != null) //send to default company ehs consultant
                                    {
                                        defaultEhsConsultant = true;
                                        string[] toEhsCon = { Helper.General.getEhsConsultantEmailByCompany(qu.CompanyId, true) };
                                        if (configuration.GetValue(ConfigList.ContactEmailReceivesEmails).ToString() == "1")
                                        {
                                            //Helper.Email.notifyEHSConsultant(qu.CompanyId, cc, null, subject, body, null);
                                            emailLogService.Insert(toEhsCon, cc, null, subject1, mailbody, "Questionnaire", true, null, null);
                                        }
                                        else
                                        {
                                            //Helper.Email.notifyEHSConsultant(qu.CompanyId, null, null, subject, body, null);
                                            emailLogService.Insert(toEhsCon, null, null, subject1, mailbody, "Questionnaire", true, null, null);
                                        }
                                    }
                                    else //find location.
                                    {
                                        if (!SubContractor)
                                        {
                                            if (cscsTlist.Count > 0)
                                            {
                                                bool vic = false;
                                                bool wao = false;
                                                foreach (CompanySiteCategoryStandard cscs in cscsTlist)
                                                {
                                                    Sites s = sService.GetBySiteId(cscs.SiteId);
                                                    RegionsSitesService rsService = new RegionsSitesService();
                                                    TList<RegionsSites> rList = rsService.GetBySiteId(cscs.SiteId);
                                                    foreach (RegionsSites _r in rList)
                                                    {
                                                        if (_r.RegionId == 1) wao = true;
                                                        if (_r.RegionId == 4) vic = true;
                                                    }
                                                }

                                                if (wao)
                                                {
                                                    _to.Add(defaultehs_wao);
                                                    if (vic) _to.Add(defaultehs_vic);
                                                }
                                                else
                                                {
                                                    if (vic) _to.Add(defaultehs_vic);
                                                }
                                            }
                                            else
                                            {
                                                _to.Add(cc[0]);
                                                cc[0] = "";
                                            }
                                        }
                                    }
                                    if (_to.Count == 0)
                                    {
                                        _to.Add(defaultehs_wao);
                                        _to.Add(defaultehs_vic);
                                    }
                                }
                                else
                                {
                                    if (cscsTlist.Count > 0 && !SubContractor)
                                    {
                                        Sites s = sService.GetBySiteId(cscsTlist[0].SiteId);
                                        if (s.EhsConsultantId != null)
                                        {
                                            EhsConsultantService eService = new EhsConsultantService();
                                            EhsConsultant e = eService.GetByEhsConsultantId(Convert.ToInt32(s.EhsConsultantId));
                                            Users u = uService.GetByUserId(e.UserId);
                                            if (u.Email != null)
                                            {
                                                _to.Add(u.Email);
                                            }
                                            else
                                            {
                                                _to.Add(cc[0]);
                                                cc[0] = "";
                                            }
                                        }
                                    }
                                }
                            }

                            string[] to = (string[])_to.ToArray(typeof(string));

                            if (!defaultEhsConsultant)
                            {
                                if (configuration.GetValue(ConfigList.ContactEmailReceivesEmails).ToString() == "1")
                                {
                                    //Helper.Email.sendEmail(to, cc, null, subject, body, null);
                                    //Helper.Email.logEmail(null, null, to, cc, null, subject, body, EmailLogTypeList.Questionnaire);
                                    emailLogService.Insert(to, cc, null, subject1, mailbody, "Questionnaire", true, null, null);//Modified by Ashley Goldstraw
                                }
                                else
                                {
                                    //Helper.Email.sendEmail(to, null, null, subject, body, null);
                                    //Helper.Email.logEmail(null, null, to, null, null, subject, body, EmailLogTypeList.Questionnaire);
                                    emailLogService.Insert(to, null, null, subject1, mailbody, "Questionnaire", true, null, null);//Modified by Ashley Goldstraw
                                }
                            }


                            if (!beingAssessed)
                            {

                                if (Helper.Questionnaire.ActionLog.Add((int)QuestionnaireActionList.ProcurementQuestionnaireSubmitted,
                                                    qu.QuestionnaireId,                         //QuestionnaireId
                                                    auth.UserId,                                //UserId
                                                    auth.FirstName,                             //FirstName
                                                    auth.LastName,                              //LastName
                                                    auth.RoleId,                                //RoleId
                                                    auth.CompanyId,                             //CompanyId
                                                    auth.CompanyName,                           //CompanyName
                                                    null,                                       //Comments
                                                    null,                                       //OldAssigned
                                                    auth.FirstName + " " + auth.LastName,       //NewAssigned
                                                    null,                                       //NewAssigned2
                                                    null,                                       //NewAssigned3
                                                    null)                                       //NewAssigned4
                                                    == false)
                                {
                                    //for now we do nothing if action log fails.
                                }
                            }
                            else
                            {
                                if (Helper.Questionnaire.ActionLog.Add((int)QuestionnaireActionList.ProcurementQuestionnaireApproved,
                                                    qu.QuestionnaireId,                         //QuestionnaireId
                                                    auth.UserId,                                //UserId
                                                    auth.FirstName,                             //FirstName
                                                    auth.LastName,                              //LastName
                                                    auth.RoleId,                                //RoleId
                                                    auth.CompanyId,                             //CompanyId
                                                    auth.CompanyName,                           //CompanyName
                                                    null,                                       //Comments
                                                    null,                                       //OldAssigned
                                                    auth.FirstName + " " + auth.LastName,       //NewAssigned
                                                    null,                                       //NewAssigned2
                                                    null,                                       //NewAssigned3
                                                    null)                                       //NewAssigned4
                                                    == false)
                                {
                                    //for now we do nothing if action log fails.
                                }

                                //Add Admin Task
                                AdminTaskService atService = new AdminTaskService();

                                using (KaiZen.CSMS.Entities.AdminTask at = new KaiZen.CSMS.Entities.AdminTask())
                                {
                                    at.AdminTaskStatusId = (int)AdminTaskStatusList.Open;
                                    at.AdminTaskSourceId = (int)AdminTaskSourceList.System;
                                    at.CsmsAccessId = (int)CsmsAccessList.AlcoaDirect; //default is AlcoaDirect
                                    if (qu.IsReQualification)
                                    {
                                        at.AdminTaskTypeId = (int)AdminTaskTypeList.Contractor_Safety_Qualification_Re_Qualification;
                                    }
                                    else
                                    {
                                        at.AdminTaskTypeId = (int)AdminTaskTypeList.Contractor_Safety_Qualification_Pre_Qualification;
                                    }
                                    at.DateOpened = DateTime.Now;
                                    at.OpenedByUserId = 0; //system

                                    //at.Login = qAnswer1_3.Text;
                                    at.EmailAddress = qAnswer1_3.Text;
                                    at.JobTitle = qAnswer1_4.Text;
                                    at.FirstName = qAnswer1_1.Text;
                                    at.LastName = qAnswer1_2.Text;
                                    at.CompanyId = qu.CompanyId;
                                    at.TelephoneNo = qAnswer1_5.Text;
                                    //at.MobileNo = tbAddNewTaskMobileNumber.Text;
                                    //at.FaxNo = tbAddNewTaskFaxNumber.Text;
                                    //at.AdminTaskComments = "";

                                    atService.Insert(at);
                                }
                            }

                            Response.Redirect(String.Format("~/SafetyPQ_QuestionnaireModify.aspx?q={0}", q), false);
                        }
                        else
                        {
                            Response.Redirect(String.Format("~/SafetyPQ_QuestionnaireInitial.aspx?q={0}&s=1", q), false);
                        }
                    }
                    else
                    {
                        throw new Exception("It appears that this procurement questionnaire has already been submitted. Please check the main page.");
                    }
                }
                else
                {
                    throw new Exception("Questionnaire does not exist. Are you authorised to view this page?");
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromContext(Context).Raise(ex);
                lblSave.Text = String.Format("Save Unsuccessful. Error: {0}", ex.Message);
            }
        }
    }

    protected void VerifyContact(int QuestionnaireId, bool SendEmail, bool ConfirmEmail)
    {
        //disable send email section
        //enable confirm section
        btnVerifyEmail.Enabled = SendEmail;
        btnVerifyConfirm.Enabled = false;
        if (ConfirmEmail)
        {
            btnVerifyConfirm.Enabled = true;
        }
    }

    protected static string GetSelectedCategories(ASPxGridView grid)
    {
        System.Collections.Generic.List<object> keyValues = grid.GetSelectedFieldValues("CategoryId");
        string selected = "";
        Hashtable ht = new Hashtable();
        foreach (int _key in keyValues)
        {
            string key = _key.ToString();
            string add;
            if ((key == null) || (key == "")) { add = ""; }
            else { add = key.ToString(); }

            if (!ht.ContainsKey(add)) //Anti-Duplication
            {
                ht[add] = 0;
                selected += String.Format(";{0}", add);
            }
        }
        return selected;
    }
    protected void SaveAnswers(int q, bool SubContractor, bool Submit)
    {
        Helper.Questionnaire.Procurement.SaveAnswer(q, "2", GetVal(qAnswer2.Value), auth.UserId);
        Helper.Questionnaire.Procurement.SaveAnswer(q, "2_1", GetVal(qAnswer2_1.Value), auth.UserId);
        Helper.Questionnaire.Procurement.SaveAnswer(q, "2_2", GetVal(qAnswer2_2.Value), auth.UserId);
        Helper.Questionnaire.Procurement.SaveAnswer(q, "2_3", GetVal(qAnswer2_3.Value), auth.UserId);
        Helper.Questionnaire.Procurement.SaveAnswer(q, "3", GetVal(qAnswer3.Text), auth.UserId);

        //fix this! insert error.
        try
        {
            if (!String.IsNullOrEmpty(GetVal(qAnswer4.Value)))
            {
                QuestionnaireServicesSelectedService qssService = new QuestionnaireServicesSelectedService();
                TList<QuestionnaireServicesSelected> qssList = qssService.GetByQuestionnaireIdQuestionnaireTypeId(q, (int)QuestionnaireTypeList.Initial_1);
                QuestionnaireServicesSelected qss = new QuestionnaireServicesSelected();
                if (qssList.Count == 1)
                {
                    qss = (QuestionnaireServicesSelected)qssList[0];
                }
                else
                {
                    qss.QuestionnaireTypeId = (int)QuestionnaireTypeList.Initial_1;
                    qss.QuestionnaireId = q;
                }
                qss.CategoryId = Convert.ToInt32(GetVal(qAnswer4.Value));
                qss.ModifiedByUserId = auth.UserId;
                qss.ModifiedDate = DateTime.Now;
                qssService.Save(qss);
            }
        }
        catch (Exception ex)
        { Elmah.ErrorSignal.FromContext(Context).Raise(ex); }

        Helper.Questionnaire.Procurement.SaveAnswer(q, "4_1", GetVal(qAnswer4_1.Text), auth.UserId);

        Helper.Questionnaire.Procurement.SaveAnswer(q, "12_1", GetVal(qAnswer12_1.Value), auth.UserId);

        if (!String.IsNullOrEmpty(GetVal(qAnswer12_1.Value)))
        {
            if (GetVal(qAnswer12_1.Value) == "Yes")
            {
                try
                {
                    QuestionnaireService qService = new QuestionnaireService();
                    Questionnaire questionnaire = qService.GetByQuestionnaireId(q);
                    questionnaire.IsVerificationRequired = true;
                    qService.Save(questionnaire);
                }
                catch (Exception ex)
                {
                    Elmah.ErrorSignal.FromContext(Context).Raise(ex);
                }
            }
        }

        Helper.Questionnaire.Procurement.SaveAnswer(q, "13", GetVal(qAnswer13.Text), auth.UserId);

        //----------------------------------------------------------------------------------------------------------

        if (SubContractor)
        {
            Helper.Questionnaire.Procurement.SaveAnswer(q, "9", "Direct", auth.UserId);
            Helper.Questionnaire.Procurement.SaveAnswer(q, "14", GetVal(qAnswer14.Value), auth.UserId);
            //Adding by Pankaj Dikshit for <Issue ID# 2381, 04-May-12> - Start
            Helper.Questionnaire.Procurement.SaveAnswer(q, "11_7", GetVal(rbl2_7.Value), auth.UserId);
            Helper.Questionnaire.Procurement.SaveAnswer(q, "11_8", GetVal(rbl2_8.Value), auth.UserId);
            Helper.Questionnaire.Procurement.SaveAnswer(q, "11_9", GetVal(rbl2_9.Value), auth.UserId);
            Helper.Questionnaire.Procurement.SaveAnswer(q, "11_10", GetVal(rbl2_10.Value), auth.UserId);
            Helper.Questionnaire.Procurement.SaveAnswer(q, "11_11", GetVal(rbl2_11.Value), auth.UserId);
            Helper.Questionnaire.Procurement.SaveAnswer(q, "11_12", GetVal(rbl2_12.Value), auth.UserId);
            //Adding by Pankaj Dikshit for <Issue ID# 2381, 04-May-12> - End
        }
        else
        {

            Helper.Questionnaire.Procurement.SaveAnswer(q, "5", GetVal(qAnswer5.Text), auth.UserId);
            //6 - Saves by itself.
            Helper.Questionnaire.Procurement.SaveAnswer(q, "7", GetVal(qAnswer7.Value), auth.UserId);

            Helper.Questionnaire.Procurement.SaveAnswer(q, "8", GetVal(qAnswer8.Text), auth.UserId);

            Helper.Questionnaire.Procurement.SaveAnswer(q, "9", GetVal(qAnswer9.Value), auth.UserId);
            Helper.Questionnaire.Procurement.SaveAnswer(q, "9_1", GetVal(qAnswer9_1.Value), auth.UserId);
            Helper.Questionnaire.Procurement.SaveAnswer(q, "10_1", GetVal(rbl1.Value), auth.UserId);
            Helper.Questionnaire.Procurement.SaveAnswer(q, "10_2", GetVal(rbl2.Value), auth.UserId);
            Helper.Questionnaire.Procurement.SaveAnswer(q, "10_3", GetVal(rbl3.Value), auth.UserId);
            Helper.Questionnaire.Procurement.SaveAnswer(q, "10_4", GetVal(rbl4.Value), auth.UserId);
            Helper.Questionnaire.Procurement.SaveAnswer(q, "10_5", GetVal(rbl5.Value), auth.UserId);
            Helper.Questionnaire.Procurement.SaveAnswer(q, "10_6", GetVal(rbl6.Value), auth.UserId);
            Helper.Questionnaire.Procurement.SaveAnswer(q, "10_7", GetVal(rbl7.Value), auth.UserId);
            Helper.Questionnaire.Procurement.SaveAnswer(q, "10_8", GetVal(rbl8.Value), auth.UserId);
            Helper.Questionnaire.Procurement.SaveAnswer(q, "10_9", GetVal(rbl9.Value), auth.UserId);
            Helper.Questionnaire.Procurement.SaveAnswer(q, "10_10", GetVal(rbl10.Value), auth.UserId);
            Helper.Questionnaire.Procurement.SaveAnswer(q, "10_11", GetVal(rbl11.Value), auth.UserId);

            Helper.Questionnaire.Procurement.SaveAnswer(q, "11_1", GetVal(rbl2_1.Value), auth.UserId);
            Helper.Questionnaire.Procurement.SaveAnswer(q, "11_2", GetVal(rbl2_2.Value), auth.UserId);
            Helper.Questionnaire.Procurement.SaveAnswer(q, "11_3", GetVal(rbl2_3.Value), auth.UserId);
            Helper.Questionnaire.Procurement.SaveAnswer(q, "11_4", GetVal(rbl2_4.Value), auth.UserId);
            Helper.Questionnaire.Procurement.SaveAnswer(q, "11_5", GetVal(rbl2_5.Value), auth.UserId);
            Helper.Questionnaire.Procurement.SaveAnswer(q, "11_6", GetVal(rbl2_6.Value), auth.UserId);
            Helper.Questionnaire.Procurement.SaveAnswer(q, "11_7", GetVal(rbl2_7.Value), auth.UserId);
            Helper.Questionnaire.Procurement.SaveAnswer(q, "11_8", GetVal(rbl2_8.Value), auth.UserId);
            Helper.Questionnaire.Procurement.SaveAnswer(q, "11_9", GetVal(rbl2_9.Value), auth.UserId);
            Helper.Questionnaire.Procurement.SaveAnswer(q, "11_10", GetVal(rbl2_10.Value), auth.UserId);
            Helper.Questionnaire.Procurement.SaveAnswer(q, "11_11", GetVal(rbl2_11.Value), auth.UserId);
            Helper.Questionnaire.Procurement.SaveAnswer(q, "11_12", GetVal(rbl2_12.Value), auth.UserId);
            Helper.Questionnaire.Procurement.SaveAnswer(q, "11_13", GetVal(rbl2_13.Value), auth.UserId); //Jolly - Enhancement Spec - 026

            ////Question 10) Hidden per request 19/01/2011 until Peter Wisdom discusses with Warren Sharp.

        }
    }
    protected void LoadAnswers(int q, bool SubContractor)
    {
        if (SubContractor)
        {
            lblSub1.Text = "Sub";
            int qAnswer14_Selected = 0;
            Int32.TryParse(Helper.Questionnaire.Procurement.LoadAnswer(q, "14"), out qAnswer14_Selected);
            if (qAnswer14_Selected != 0)
            {
                qAnswer14.Value = qAnswer14_Selected;
            }
        }
        else
        {
            lblSub1.Text = "";
        }

        qAnswer1_1.Value = Helper.Questionnaire.Procurement.LoadAnswer(q, "1_1");
        qAnswer1_2.Value = Helper.Questionnaire.Procurement.LoadAnswer(q, "1_2");
        qAnswer1_3.Value = Helper.Questionnaire.Procurement.LoadAnswer(q, "1_3");
        qAnswer1_4.Value = Helper.Questionnaire.Procurement.LoadAnswer(q, "1_4");
        qAnswer1_5.Value = Helper.Questionnaire.Procurement.LoadAnswer(q, "1_5");
        //DT231 Cindi Thornton 28/9/15 - answers for 1_6 and 1_7 are done in module_init (calls PopulateCompanyDetails). 


        qAnswer1_1_.Value = qAnswer1_1.Value;
        qAnswer1_2_.Value = qAnswer1_2.Value;
        qAnswer1_3_.Value = qAnswer1_3.Value;
        qAnswer1_4_.Value = qAnswer1_4.Value;
        qAnswer1_5_.Value = qAnswer1_5.Value;
        //DT231 Cindi Thornton 28/9/15 - answers for 1_6_ and 1_7_ are done in module_init (calls PopulateCompanyDetails).

        tbVerifyFrom.Text = String.Format("{0}, {1} [{2}]", auth.LastName, auth.FirstName, auth.Email);

        Configuration configuration = new Configuration();
        tbVerifyCC.Text = String.Format("AUA Alcoa Contractor Services [{0}]", configuration.GetValue(ConfigList.ContactEmail));

        if (qAnswer1_1.Value != null && qAnswer1_2 != null && qAnswer1_3 != null)
        {
            tbVerifyTo.Text = String.Format("{0}, {1} [{2}]", qAnswer1_2.Value, qAnswer1_1.Value, qAnswer1_3.Value);
        }
        string PreReSq = "Contractor Safety Pre-Qualification";
        QuestionnaireService qService = new QuestionnaireService();
        Questionnaire _questionnaire = qService.GetByQuestionnaireId(q);

        if (_questionnaire.InitialStatus != (int)QuestionnaireStatusList.Incomplete)
        {
            lblSubmit.Visible = false;
            lblSubmit2.Visible = false;
        }

        //email template created by Ashley Goldstraw 29/09/2015 to replace the hard coded email that is commented out below.
        string RequalText = "";
        if (_questionnaire.IsReQualification)
        {
            PreReSq = "Contractor Safety Re-Qualification";
            RequalText = "A Re-Qualification has been initiated against your company. You (or the correct H&S representative of your organisation, see below) will be contacted again shortly to complete the supplier questionnaire part of this Safety Re-Qualification process.<br/><br/>";
        }
        if (_questionnaire.SubContractor == true) PreReSq = "Sub " + PreReSq;
        CompaniesService cService = new CompaniesService();
        Companies _companies = cService.GetByCompanyId(_questionnaire.CompanyId);

        model.EmailTemplate eMailTemplate = emailTemplateService.Get(i => i.EmailTemplateName == "A PreRe-Qualification has been initiated against your company", null);
        byte[] bodyByte = eMailTemplate.EmailBody;
        string mailbody = System.Text.Encoding.ASCII.GetString(bodyByte);
        string subject = eMailTemplate.EmailTemplateSubject;
        subject = subject.Replace("{CompanyName}", _companies.CompanyName);
        subject = subject.Replace("{PreOrReQualification}", PreReSq);
        tbVerifySubject.Text = subject;
        mailbody = mailbody.Replace("{CompanyName}", _companies.CompanyName);
        mailbody = mailbody.Replace("{FirstName}", qAnswer1_1.Text);
        mailbody = mailbody.Replace("{LastName}", qAnswer1_2.Text);
        mailbody = mailbody.Replace("{RequalText}", RequalText);
        mailbody = mailbody.Replace("{EmailAddress}", qAnswer1_3.Text);
        mailbody = mailbody.Replace("{JobTitle}", qAnswer1_4.Text);
        mailbody = mailbody.Replace("{TelephoneNumber}", qAnswer1_5.Text);
        mailbody = mailbody.Replace("{UserFirstName}", auth.FirstName);
        mailbody = mailbody.Replace("{UserLastName}", auth.LastName);
        mailbody = mailbody.Replace("{ABN}", qAnswer1_7.Text);
        mailbody = mailbody.Replace("{NewCompanyName}", qAnswer1_6.Text);

        //tbVerifySubject.Text = String.Format("{0}: 0 - Contact Verification Required - {1}", PreReSq, _companies.CompanyName);
        //lblVerifyNotes0.Text = "0";
        /*mbVerifyMessage.Text = "Hi " + qAnswer1_1.Value + " " + qAnswer1_2.Value + ",\n" +
                                "\n" +
                                RequalText +
                                "You have been nominated as the Health and Safety representative of your organisation to complete the Alcoa Safety Qualification process. Would you please 'reply to all' on this e-mail to confirm that the information we have is correct.\n" +
                                "\n" +
                                "If you are not the correct person to complete this task would you please 'reply to all' to inform Alcoa by correcting the information below:\n" +
                                "\n" +
                                "Name (first , last): " + qAnswer1_1.Text + " " + qAnswer1_2.Text + "\n" +
                                "E-Mail address: " + qAnswer1_3.Text + "\n" +
                                "Job title: " + qAnswer1_4.Text + "\n" +
                                "Telephone details: " + qAnswer1_5.Text + "\n" +
                                "\n" +
                                "\n" +
                                "Thanks,\n" +
                                auth.FirstName + " " + auth.LastName;*/

        //mbVerifyMessage.Text = mailbody;
        //mbVerifyMessage.Text = "some text";
        //divVerifyMessage.InnerHtml = mailbody;

        //html editor added by AShley Goldstraw 8/10/2015.  DT254
        hteVerifyMessage.Html = mailbody;



        QuestionnaireInitialContactService qicService = new QuestionnaireInitialContactService();
        QuestionnaireInitialContactStatusService qicsService = new QuestionnaireInitialContactStatusService();
        QuestionnaireInitialContact qic = qicService.GetByQuestionnaireId(q);
        QuestionnaireInitialContactStatus qics = qicsService.GetByQuestionnaireInitialContactStatusId((int)QuestionnaireInitialContactStatusList.NotVerified);

        lblVerifyNotes0.ForeColor = Color.Red;
        lblVerifyNotes1.ForeColor = Color.Red;

        if (qic != null)
        {
            ASPxPopupControl2.PopupElementID = btnVerify.ClientID;
            if (qic.ContactStatusId == (int)QuestionnaireInitialContactStatusList.Verified)
            {
                lblEditSaveWarning.Visible = true;
                lblVerify.ForeColor = Color.Green;

                if (_questionnaire.InitialStatus == 1)
                {
                    btnSubmit.Enabled = true;
                    btnSubmitTop.Enabled = true;
                    btnSubmit.Text = "Submit";
                    btnSubmitTop.Text = "Submit";
                    lblSubmit.Visible = true;
                    lblSubmit2.Visible = true;
                }
                ASPxPopupControl2.PopupElementID = "";
                btnVerify.Enabled = false;

                lblVerifyNotes0.ForeColor = Color.Green;
                lblVerifyNotes1.ForeColor = Color.Green;
                lblConfirmTime.ForeColor = Color.Green;
                lblConfirmTime.Text = "Confirmed at: " + qic.VerifiedDate.ToString();

            }

            if (qAnswer1_3.Value != null)
            {
                if (!String.IsNullOrEmpty(qAnswer1_3.Value.ToString()))
                {
                    if (qic.ContactEmail == qAnswer1_3.Value.ToString())
                    {
                        qics = null;
                        qics = qicsService.GetByQuestionnaireInitialContactStatusId(qic.ContactStatusId);

                        int countNoEmails = 0;
                        QuestionnaireInitialContactEmailService qiceService = new QuestionnaireInitialContactEmailService();
                        QuestionnaireInitialContactEmailFilters qiceFilters = new QuestionnaireInitialContactEmailFilters();
                        qiceFilters.Append(QuestionnaireInitialContactEmailColumn.EmailTypeId,
                                                ((int)QuestionnaireInitialContactEmailTypeList.CorrectContactCheck).ToString());
                        qiceFilters.Append(QuestionnaireInitialContactEmailColumn.ContactId, qic.QuestionnaireInitialContactId.ToString());
                        qiceFilters.Append(QuestionnaireInitialContactEmailColumn.ContactEmail, qAnswer1_3.Value.ToString());
                        TList<QuestionnaireInitialContactEmail> qiceList =
                            DataRepository.QuestionnaireInitialContactEmailProvider.GetPaged(
                            qiceFilters.ToString(),
                            "EmailSentDateTime DESC",
                            0,
                            99999,
                            out countNoEmails);

                        if (countNoEmails > 0)
                        {
                            lblVerifyNotes0.Text = countNoEmails.ToString();
                            lblVerifyNotes1.Text = "(Last E-Mail Sent: " + qiceList[0].EmailSentDateTime.ToString() + ")";
                        }

                        if (qic.ContactStatusId == (int)QuestionnaireInitialContactStatusList.NotVerified)
                        {
                            btnVerify.Enabled = true;
                            VerifyContact(q, true, false);

                            if (countNoEmails > 0)
                            {
                                VerifyContact(q, true, true); //e-mail sent and allow them to send another..
                            }
                        }
                        else
                        {
                            VerifyContact(q, false, false);
                        }
                    }
                    else
                    {
                        btnVerify.Enabled = true;
                        VerifyContact(q, true, false);
                    }
                }
            }
        }
        else
        {
            btnVerify.Enabled = false;
            ASPxPopupControl2.PopupElementID = "";
            VerifyContact(q, false, false);
        }
        lblVerify.Text = qics.StatusDesc;


        try
        {
            if (Helper.Questionnaire.Procurement.LoadAnswer(q, "2") != "")
                qAnswer2.Value = Convert.ToInt32(Helper.Questionnaire.Procurement.LoadAnswer(q, "2"));
        }
        catch (Exception ex) { Elmah.ErrorSignal.FromContext(Context).Raise(ex); };
        try
        {
            if (Helper.Questionnaire.Procurement.LoadAnswer(q, "2_1") != "")
                qAnswer2_1.Value = Convert.ToInt32(Helper.Questionnaire.Procurement.LoadAnswer(q, "2_1"));
        }
        catch (Exception ex) { Elmah.ErrorSignal.FromContext(Context).Raise(ex); };
        try
        {
            if (Helper.Questionnaire.Procurement.LoadAnswer(q, "2_2") != "")
                qAnswer2_2.Value = Helper.Questionnaire.Procurement.LoadAnswer(q, "2_2");
        }
        catch (Exception ex) { Elmah.ErrorSignal.FromContext(Context).Raise(ex); };
        try
        {
            if (Helper.Questionnaire.Procurement.LoadAnswer(q, "2_3") != "")
                qAnswer2_3.Value = Helper.Questionnaire.Procurement.LoadAnswer(q, "2_3");
        }
        catch (Exception ex) { Elmah.ErrorSignal.FromContext(Context).Raise(ex); };

        if (!String.IsNullOrEmpty(Helper.Questionnaire.Procurement.LoadAnswer(q, "3")))
        {
            qAnswer3.Text = Helper.Questionnaire.Procurement.LoadAnswer(q, "3");
        }

        QuestionnaireServicesSelectedService qssS = new QuestionnaireServicesSelectedService();
        TList<QuestionnaireServicesSelected> qssT = qssS.GetByQuestionnaireIdQuestionnaireTypeId(q, (int)QuestionnaireTypeList.Initial_1);
        if (qssT.Count > 0) qAnswer4.Value = qssT[0].CategoryId;

        if (!String.IsNullOrEmpty(Helper.Questionnaire.Procurement.LoadAnswer(q, "4_1")))
        {
            qAnswer4_1.Text = Helper.Questionnaire.Procurement.LoadAnswer(q, "4_1");
        }

        if (!String.IsNullOrEmpty(Helper.Questionnaire.Procurement.LoadAnswer(q, "12_1")))
        {
            qAnswer12_1.Value = Helper.Questionnaire.Procurement.LoadAnswer(q, "12_1");
        }

        if (!String.IsNullOrEmpty(Helper.Questionnaire.Procurement.LoadAnswer(q, "13")))
        {
            qAnswer13.Value = Helper.Questionnaire.Procurement.LoadAnswer(q, "13");
        }

        LoadEHS(q);

        if (!SubContractor)
        {


            //try
            //{
            //    qAnswer2_0.Value = Convert.ToInt32(LoadAnswer(q, "2_0"));
            //}
            //catch { };

            qAnswer5.Text = Helper.Questionnaire.Procurement.LoadAnswer(q, "5");
            //6 - Loads by itself.
            qAnswer7.Value = Helper.Questionnaire.Procurement.LoadAnswer(q, "7");
            try
            {
                if (Helper.Questionnaire.Procurement.LoadAnswer(q, "8") != "")
                    qAnswer8.Date = DateTime.ParseExact(Helper.Questionnaire.Procurement.LoadAnswer(q, "8"), "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
            }
            catch (Exception ex) { Elmah.ErrorSignal.FromContext(Context).Raise(ex); }
            qAnswer9.Value = Helper.Questionnaire.Procurement.LoadAnswer(q, "9");

            if (Helper.Questionnaire.Procurement.LoadAnswer(q, "9") == "Sub-Contract") { qAnswer9_1.ClientEnabled = true; };
            if (!String.IsNullOrEmpty(Helper.Questionnaire.Procurement.LoadAnswer(q, "9_1")))
            {
                qAnswer9_1.Value = Convert.ToInt32(Helper.Questionnaire.Procurement.LoadAnswer(q, "9_1"));
            }

            rbl1.Value = Helper.Questionnaire.Procurement.LoadAnswer(q, "10_1");
            rbl2.Value = Helper.Questionnaire.Procurement.LoadAnswer(q, "10_2");
            rbl3.Value = Helper.Questionnaire.Procurement.LoadAnswer(q, "10_3");
            rbl4.Value = Helper.Questionnaire.Procurement.LoadAnswer(q, "10_4");
            rbl5.Value = Helper.Questionnaire.Procurement.LoadAnswer(q, "10_5");
            rbl6.Value = Helper.Questionnaire.Procurement.LoadAnswer(q, "10_6");
            rbl7.Value = Helper.Questionnaire.Procurement.LoadAnswer(q, "10_7");
            rbl8.Value = Helper.Questionnaire.Procurement.LoadAnswer(q, "10_8");
            rbl9.Value = Helper.Questionnaire.Procurement.LoadAnswer(q, "10_9");
            rbl10.Value = Helper.Questionnaire.Procurement.LoadAnswer(q, "10_10");
            rbl11.Value = Helper.Questionnaire.Procurement.LoadAnswer(q, "10_11");

            rbl2_1.Value = Helper.Questionnaire.Procurement.LoadAnswer(q, "11_1");
            rbl2_2.Value = Helper.Questionnaire.Procurement.LoadAnswer(q, "11_2");
            rbl2_3.Value = Helper.Questionnaire.Procurement.LoadAnswer(q, "11_3");
            rbl2_4.Value = Helper.Questionnaire.Procurement.LoadAnswer(q, "11_4");
            rbl2_5.Value = Helper.Questionnaire.Procurement.LoadAnswer(q, "11_5");
            rbl2_6.Value = Helper.Questionnaire.Procurement.LoadAnswer(q, "11_6");
            rbl2_7.Value = Helper.Questionnaire.Procurement.LoadAnswer(q, "11_7");
            rbl2_8.Value = Helper.Questionnaire.Procurement.LoadAnswer(q, "11_8");
            rbl2_9.Value = Helper.Questionnaire.Procurement.LoadAnswer(q, "11_9");
            rbl2_10.Value = Helper.Questionnaire.Procurement.LoadAnswer(q, "11_10");
            rbl2_11.Value = Helper.Questionnaire.Procurement.LoadAnswer(q, "11_11");
            rbl2_12.Value = Helper.Questionnaire.Procurement.LoadAnswer(q, "11_12");
            rbl2_13.Value = Helper.Questionnaire.Procurement.LoadAnswer(q, "11_13");  //Jolly - Enhancement Spec - 026

            ////Question 10) Hidden per request 19/01/2011 until Peter Wisdom discusses with Warren Sharp.


            SetOverall();
            CalculateIfPqRequired();
        }
        else if (SubContractor)
        {
            //Adding by Pankaj Dikshit for <Issue ID# 2381, 04-May-12> - Start
            rbl2_7.Value = Helper.Questionnaire.Procurement.LoadAnswer(q, "11_7");
            rbl2_8.Value = Helper.Questionnaire.Procurement.LoadAnswer(q, "11_8");
            rbl2_9.Value = Helper.Questionnaire.Procurement.LoadAnswer(q, "11_9");
            rbl2_10.Value = Helper.Questionnaire.Procurement.LoadAnswer(q, "11_10");
            rbl2_11.Value = Helper.Questionnaire.Procurement.LoadAnswer(q, "11_11");
            rbl2_12.Value = Helper.Questionnaire.Procurement.LoadAnswer(q, "11_12");

            SetOverall();
            CalculateIfPqRequired();
            //Adding by Pankaj Dikshit for <Issue ID# 2381, 04-May-12> - End
        }
    }

    //DT231 Cindi Thornton - populate company name and ABN from the Companies table if they haven't got a saved answer for these fields
    private void PopulateCompanyDetails(Questionnaire questionnaire)
    {
        qAnswer1_6.Value = Helper.Questionnaire.Procurement.LoadAnswer(questionnaire.QuestionnaireId, "1_6");
        qAnswer1_7.Value = Helper.Questionnaire.Procurement.LoadAnswer(questionnaire.QuestionnaireId, "1_7");

        //get the company details
        Repo.CSMS.Service.Database.ICompanyService companyService = ALCOA.CSMS.Website.Global.GetInstance<Repo.CSMS.Service.Database.ICompanyService>();
        model.Company c = companyService.Get(i => i.CompanyId == questionnaire.CompanyId, null);


        if (qAnswer1_6.Value == null || qAnswer1_6.Text == "")  //if there isn't a value for Company Name, get it from the Companies table
            qAnswer1_6.Value = c.CompanyName;

        if (qAnswer1_7.Value == null || qAnswer1_7.Text == "")  //if there isn't a value for Company ABN, get it from the Companies table
            qAnswer1_7.Value = c.CompanyAbn;

        qAnswer1_6_.Value = qAnswer1_6.Value;
        qAnswer1_7_.Value = qAnswer1_7.Value;
    }
    //End DT231

    protected static string GetVal(object o)
    {
        try
        {
            return o.ToString();
        }
        catch (Exception)
        {
            Elmah.ErrorSignal.FromCurrentContext();
            return "";
        }
    }

    protected void BindLocationData(int CompanyId)
    {
        CompanySiteCategoryStandardService cscsService = new CompanySiteCategoryStandardService();
        TList<CompanySiteCategoryStandard> cscsTlist = cscsService.GetByCompanyId(CompanyId);
        DataSet ds = cscsTlist.ToDataSet(false);
        if (ds.Tables[0] != null)
        {
            Session["dtLocationSq"] = ds.Tables[0];
            BindLocationData();
        }
        else
        {
            Session["dtLocationSq"] = null;
        }
    }
    protected void BindLocationData()
    {
        if (Session["dtLocationSq"] != null)
        {
            gridLocations.DataSource = (DataTable)Session["dtLocationSq"];
            gridLocations.DataBind();
        }
    }
    protected void gridLocations_RowUpdating(object sender, ASPxDataUpdatingEventArgs e)
    {
        try
        {
            int QuestionnaireId = Convert.ToInt32(Request.QueryString["q"].ToString());
            QuestionnaireService qService = new QuestionnaireService();
            Questionnaire q = qService.GetByQuestionnaireId(QuestionnaireId);

            int CompanyId = q.CompanyId;
            int SiteId = Convert.ToInt32(e.OldValues["SiteId"]);

            CompanySiteCategoryStandardService cscsService = new CompanySiteCategoryStandardService();
            CompanySiteCategoryStandard cscs = cscsService.GetByCompanyIdSiteId(CompanyId, SiteId);
            if (cscs == null) throw new Exception("Unexpected Error. Please refresh (or re-open) this page.");
            cscs.CompanyId = q.CompanyId;
            cscs.SiteId = Convert.ToInt32(e.NewValues["SiteId"]);
            if (e.NewValues["LocationSponsorUserId"] != DBNull.Value)
            {
                cscs.LocationSponsorUserId = Convert.ToInt32(e.NewValues["LocationSponsorUserId"]);
            }
            else
            {
                cscs.LocationSponsorUserId = null;
            }

            string Area = "";
            if (e.OldValues["Area"] != null) Area = e.OldValues["Area"].ToString();  //This line and next line changed from dbnull.value to null.  DT370 Ashley Goldstraw 22/12/2015
            if (e.NewValues["Area"] != null)
            {
                if (Area != e.NewValues["Area"].ToString())
                {
                    Area = e.NewValues["Area"].ToString();
                    cscs.Area = Area;
                }

                if (cscs.Area == "(n/a)")
                {
                    if (cscs.SiteId <= 3) // kwi pin wgp
                    {
                        throw new Exception("If you select either Kwinana, Pinjarra or Wagerup as a site, (n/a) cannot be chosen as the Area");
                    }
                }
            }


            cscs.CompanySiteCategoryId = null;

            cscs.ModifiedByUserId = auth.UserId;
            cscs.ModifiedDate = DateTime.Now;


            cscsService.Save(cscs);

            e.Cancel = true;
            gridLocations.CancelEdit();
            BindLocationData(q.CompanyId);

            string SiteName = "-";
            SitesService sService = new SitesService();
            Sites s = sService.GetBySiteId(cscs.SiteId);
            if (s != null) SiteName = s.SiteName;

            if (e.OldValues["SiteId"] != DBNull.Value)
            {
                Sites sOld = sService.GetBySiteId(Convert.ToInt32(e.OldValues["SiteId"]));
                if (sOld != null)
                {
                    SiteName += " (Previously: " + sOld.SiteName + ")";
                }
            }

            string SafetyCategory = "-";
            UsersService uService = new UsersService();

            string LocationSponsorUser = "-";
            if (cscs.LocationSponsorUserId != null)
            {

                Users u = uService.GetByUserId(Convert.ToInt32(cscs.LocationSponsorUserId));
                if (u != null) LocationSponsorUser = u.FirstName + " " + u.LastName;
            }

            if (e.OldValues["LocationSponsorUserId"] != DBNull.Value)
            {
                Users uOld = uService.GetByUserId(Convert.ToInt32(e.OldValues["LocationSponsorUserId"]));
                if (uOld != null)
                {
                    LocationSponsorUser += " (Previously: " + uOld.FirstName + " " + uOld.LastName + ")";
                }
            }

            string Approval = "-";

            if (Helper.Questionnaire.ActionLog.Add((int)QuestionnaireActionList.LocationApprovalModified,
                                                    q.QuestionnaireId,                         //QuestionnaireId
                                                    auth.UserId,                                //UserId
                                                    auth.FirstName,                             //FirstName
                                                    auth.LastName,                              //LastName
                                                    auth.RoleId,                                //RoleId
                                                    auth.CompanyId,                             //CompanyId
                                                    auth.CompanyName,                           //CompanyName
                                                    null,                                       //Comments
                                                    null,                                       //OldAssigned
                                                    SiteName,                                   //NewAssigned
                                                    SafetyCategory,                             //NewAssigned2
                                                    LocationSponsorUser,                        //NewAssigned3
                                                    Approval)                                   //NewAssigned4
                                                    == false)
            {
                //for now we do nothing if action log fails.
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    protected void gridLocations_RowInserting(object sender, ASPxDataInsertingEventArgs e)
    {
        try
        {
            int QuestionnaireId = Convert.ToInt32(Request.QueryString["q"].ToString());
            QuestionnaireService qService = new QuestionnaireService();
            Questionnaire q = qService.GetByQuestionnaireId(QuestionnaireId);

            CompanySiteCategoryStandardService cscsService = new CompanySiteCategoryStandardService();
            CompanySiteCategoryStandard cscs = new CompanySiteCategoryStandard();
            cscs.CompanyId = q.CompanyId;
            cscs.SiteId = Convert.ToInt32(e.NewValues["SiteId"]);
            if (e.NewValues["LocationSponsorUserId"] != null)
            {
                cscs.LocationSponsorUserId = Convert.ToInt32(e.NewValues["LocationSponsorUserId"]);
            }

            string Area = "";
            if (e.NewValues["Area"] != null)
            {
                Area = e.NewValues["Area"].ToString();
                cscs.Area = Area;

                if (cscs.Area == "(n/a)")
                {
                    if (cscs.SiteId <= 3) // kwi pin wgp
                    {
                        throw new Exception("If you select either Kwinana, Pinjarra or Wagerup as a site, (n/a) cannot be chosen as the Area");
                    }
                }
            }


            cscs.ModifiedByUserId = auth.UserId;
            cscs.ModifiedDate = DateTime.Now;

            cscsService.Save(cscs);

            e.Cancel = true;
            gridLocations.CancelEdit();
            BindLocationData(q.CompanyId);

            string SiteName = "-";
            SitesService sService = new SitesService();
            Sites s = sService.GetBySiteId(cscs.SiteId);
            if (s != null) SiteName = s.SiteName;

            string SafetyCategory = "-";
            UsersService uService = new UsersService();

            string LocationSponsorUser = "-";
            if (cscs.LocationSponsorUserId != null)
            {

                Users u = uService.GetByUserId(Convert.ToInt32(cscs.LocationSponsorUserId));
                if (u != null) LocationSponsorUser = u.FirstName + " " + u.LastName;
            }

            string Approval = "-";

            if (Helper.Questionnaire.ActionLog.Add((int)QuestionnaireActionList.LocationApprovalAdded,
                                                    q.QuestionnaireId,                         //QuestionnaireId
                                                    auth.UserId,                                //UserId
                                                    auth.FirstName,                             //FirstName
                                                    auth.LastName,                              //LastName
                                                    auth.RoleId,                                //RoleId
                                                    auth.CompanyId,                             //CompanyId
                                                    auth.CompanyName,                           //CompanyName
                                                    null,                                       //Comments
                                                    null,                                       //OldAssigned
                                                    SiteName,                                   //NewAssigned
                                                    SafetyCategory,                             //NewAssigned2
                                                    LocationSponsorUser,                        //NewAssigned3
                                                    Approval)                                   //NewAssigned4
                                                    == false)
            {
                //for now we do nothing if action log fails.
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    protected void gridLocations_RowDeleting(object sender, ASPxDataDeletingEventArgs e)
    {
        try
        {
            int QuestionnaireId = Convert.ToInt32(Request.QueryString["q"].ToString());
            QuestionnaireService qService = new QuestionnaireService();
            Questionnaire q = qService.GetByQuestionnaireId(QuestionnaireId);

            int CompanyId = q.CompanyId;
            int SiteId = Convert.ToInt32(e.Values["SiteId"]);

            CompanySiteCategoryStandardService cscsService = new CompanySiteCategoryStandardService();
            CompanySiteCategoryStandard cscs = cscsService.GetByCompanyIdSiteId(CompanyId, SiteId);
            if (cscs == null) throw new Exception("Unexpected Error. Please refresh (or re-open) this page.");

            cscsService.Delete(cscs);

            e.Cancel = true;
            gridLocations.CancelEdit();
            BindLocationData(q.CompanyId);

            string SiteName = "-";
            SitesService sService = new SitesService();
            Sites s = sService.GetBySiteId(cscs.SiteId);
            if (s != null) SiteName = s.SiteName;


            if (Helper.Questionnaire.ActionLog.Add((int)QuestionnaireActionList.LocationApprovalDeleted,
                                                    q.QuestionnaireId,                         //QuestionnaireId
                                                    auth.UserId,                                //UserId
                                                    auth.FirstName,                             //FirstName
                                                    auth.LastName,                              //LastName
                                                    auth.RoleId,                                //RoleId
                                                    auth.CompanyId,                             //CompanyId
                                                    auth.CompanyName,                           //CompanyName
                                                    null,                                       //Comments
                                                    null,                                       //OldAssigned
                                                    SiteName,                                   //NewAssigned
                                                    null,                                       //NewAssigned2
                                                    null,                                       //NewAssigned3
                                                    null)                                       //NewAssigned4
                                                    == false)
            {
                //for now we do nothing if action log fails.
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void gridServicesSecondary_RowUpdating(object sender, ASPxDataUpdatingEventArgs e)
    {
        e.NewValues["QuestionnaireId"] = Convert.ToInt32(Request.QueryString["q"]);
        e.NewValues["QuestionnaireTypeId"] = 2;
    }
    protected void gridServicesSecondary_RowInserting(object sender, ASPxDataInsertingEventArgs e)
    {
        e.NewValues["QuestionnaireId"] = Convert.ToInt32(Request.QueryString["q"]);
        e.NewValues["QuestionnaireTypeId"] = 2;
    }

    //protected void qAnswer9_1_DataBound(object sender, EventArgs e)
    //{
    //    //SetNullItem(sender);
    //}

    //protected void SetNullItem(object control)
    //{
    //    ASPxComboBox cb = (ASPxComboBox)control;
    //    if (cb != null)
    //    {
    //        ListEditItem item = new ListEditItem("Not Applicable", "");
    //        cb.Items.Insert(0, item);
    //        cb.SelectedItem = item;
    //    }
    //}

    protected void LoadEHS(int q)
    {
        QuestionnaireService questionnaireService = new QuestionnaireService();
        Questionnaire questionnaireEntity = questionnaireService.GetByQuestionnaireId(q);
        //Start:change by Debashis for deployment issue
        if (questionnaireEntity != null)
        {
            CompaniesService companiesService = new CompaniesService();
            Companies companiesEntity = companiesService.GetByCompanyId(questionnaireEntity.CompanyId);


            if (companiesEntity != null && companiesEntity.EhsConsultantId.HasValue != null)
            {
                //save ehs
                if (companiesEntity.EhsConsultantId.HasValue)
                {
                    qAnswer2_0.Value = companiesEntity.EhsConsultantId;
                }
            }
        }
        //End:change by Debashis for deployment issue
    }

    protected int countNoSitesEntered()
    {
        int countNoSitesEntered = 0;

        try
        {
            int QuestionnaireId = Convert.ToInt32(Request.QueryString["q"].ToString());
            QuestionnaireService qService = new QuestionnaireService();
            Questionnaire q = qService.GetByQuestionnaireId(QuestionnaireId);

            if (q != null)
            {
                CompanySiteCategoryStandardService cscsService = new CompanySiteCategoryStandardService();
                CompanySiteCategoryStandardFilters cscsFilters = new CompanySiteCategoryStandardFilters();
                cscsFilters.Append(CompanySiteCategoryStandardColumn.CompanyId, q.CompanyId.ToString());
                TList<CompanySiteCategoryStandard> cscsFilter = DataRepository.CompanySiteCategoryStandardProvider.GetPaged(cscsFilters.ToString(), null, 0, 100, out countNoSitesEntered);
            }
        }
        catch (Exception ex)
        {
            Elmah.ErrorSignal.FromContext(Context).Raise(ex);
        }
        return countNoSitesEntered;
    }
    protected void ASPxButton1_Click(object sender, EventArgs e)
    {
        if (auth.RoleId == (int)RoleList.PreQual || auth.RoleId == (int)RoleList.Contractor)
            throw new Exception("Only Procurement Personal are allowed to do this.");
        try
        {
            bool SubContractor = false;

            string q = Request.QueryString["q"];
            QuestionnaireService qs = new QuestionnaireService();
            Questionnaire _q = qs.GetByQuestionnaireId(Convert.ToInt32(q));


            Helper.Questionnaire.Procurement.SaveAnswer(Convert.ToInt32(q), "1_1", qAnswer1_1.Text, auth.UserId);
            Helper.Questionnaire.Procurement.SaveAnswer(Convert.ToInt32(q), "1_2", qAnswer1_2.Text, auth.UserId);
            Helper.Questionnaire.Procurement.SaveAnswer(Convert.ToInt32(q), "1_3", qAnswer1_3.Text, auth.UserId);
            Helper.Questionnaire.Procurement.SaveAnswer(Convert.ToInt32(q), "1_4", qAnswer1_4.Text, auth.UserId);
            Helper.Questionnaire.Procurement.SaveAnswer(Convert.ToInt32(q), "1_5", qAnswer1_5.Text, auth.UserId);
            Helper.Questionnaire.Procurement.SaveAnswer(Convert.ToInt32(q), "1_6", qAnswer1_6.Text, auth.UserId); //DT231 - new fields
            Helper.Questionnaire.Procurement.SaveAnswer(Convert.ToInt32(q), "1_7", qAnswer1_7.Text, auth.UserId); //DT231 - new fields

            QuestionnaireInitialContactService qicService = new QuestionnaireInitialContactService();
            QuestionnaireInitialContact qic = qicService.GetByQuestionnaireId(Convert.ToInt32(q));
            if (qic == null)
            {
                qic = new QuestionnaireInitialContact();
                qic.QuestionnaireId = Convert.ToInt32(q);
                qic.CreatedByUserId = auth.UserId;
                qic.ContactStatusId = (int)QuestionnaireInitialContactStatusList.NotVerified;
            }

            qic.ContactFirstName = qAnswer1_1.Text;
            qic.ContactLastName = qAnswer1_2.Text;

            if (qic.ContactEmail != qAnswer1_3.Text) //Not Verified if Email Different
            {
                qic.ContactStatusId = (int)QuestionnaireInitialContactStatusList.NotVerified;
                qic.ContactEmail = qAnswer1_3.Text;
            }

            qic.ContactTitle = qAnswer1_4.Text;
            qic.ContactPhone = qAnswer1_5.Text;

            qic.ModifiedDate = DateTime.Now;
            qic.ModifiedByUserId = auth.UserId;

            qicService.Save(qic);

            //DT231 - save company details to Companies table if required
            //get the company details
            Repo.CSMS.Service.Database.ICompanyService companyService = ALCOA.CSMS.Website.Global.GetInstance<Repo.CSMS.Service.Database.ICompanyService>();
            model.Company c = companyService.Get(i => i.CompanyId == _q.CompanyId, null);

            if (c.CompanyName != qAnswer1_6.Text || c.CompanyAbn != qAnswer1_7.Text) // if the company details have changed, save them
            {
                c.CompanyName = qAnswer1_6.Text;
                c.CompanyAbn = qAnswer1_7.Text;
                companyService.Update(c);
            }
            //End DT231



            if (_q.InitialStatus == (int)QuestionnaireStatusList.Incomplete)
            {
                if (_q.SubContractor == true) SubContractor = true;

                Save(false, SubContractor);
            }
            else
            {
                Response.Redirect(String.Format("~/SafetyPQ_QuestionnaireInitial.aspx?q={0}&s=1", q), false);
            }
        }
        catch (Exception ex)
        {
            Elmah.ErrorSignal.FromContext(Context).Raise(ex);
            throw new Exception("Error Occurred: " + ex.Message);
        }
    }
    protected void btnVerifyEmail_Click(object sender, EventArgs e)
    {
        if (auth.RoleId == (int)RoleList.PreQual || auth.RoleId == (int)RoleList.Contractor)
            throw new Exception("Only Procurement Personal are allowed to do this.");
        try
        {
            string q = Request.QueryString["q"];
            QuestionnaireService qs = new QuestionnaireService();
            Questionnaire _q = qs.GetByQuestionnaireId(Convert.ToInt32(q));
            //log email
            QuestionnaireInitialContactService qicService = new QuestionnaireInitialContactService();
            QuestionnaireInitialContact qic = qicService.GetByQuestionnaireId(Convert.ToInt32(q));

            QuestionnaireInitialContactEmailService qiceService = new QuestionnaireInitialContactEmailService();
            QuestionnaireInitialContactEmail qice = new QuestionnaireInitialContactEmail();

            qice.QuestionnaireId = Convert.ToInt32(q);
            qice.EmailTypeId = (int)QuestionnaireInitialContactEmailTypeList.CorrectContactCheck;
            qice.EmailSentDateTime = DateTime.Now;
            qice.ContactId = qic.QuestionnaireInitialContactId;
            qice.ContactEmail = qAnswer1_3.Value.ToString();
            qice.Subject = tbVerifySubject.Text;
            //qice.Message = mbVerifyMessage.Text;
            qice.Message = hteVerifyMessage.Html;
            qice.SentByUserId = auth.UserId;

            qiceService.Save(qice);

            //send email
            string[] to = { qAnswer1_3.Value.ToString() };
            Configuration configuration = new Configuration();
            //string[] cc = { configuration.GetValue(ConfigList.ContactEmail) };
            // cc - AUACSMS Team cc Removed 9.Mar.11
            //Helper.Email.sendEmailFrom("", auth.Email, to, null, null, tbVerifySubject.Text, mbVerifyMessage.Text, null);
            //Helper.Email.logEmail("", auth.Email, to, null, null, tbVerifySubject.Text, mbVerifyMessage.Text, EmailLogTypeList.Questionnaire);

            //Modifiedy by Ashley Goldstraw to log email to table.  Email will be sent at a later point in time.  8/9/2015
            emailLogService.Insert(auth.Email, to, null, null, tbVerifySubject.Text, hteVerifyMessage.Html, "Questionnaire", true, null, null);
            if (Helper.Questionnaire.ActionLog.Add((int)QuestionnaireActionList.SupplierContactVerificationEmailSent,
                                                        _q.QuestionnaireId,                         //QuestionnaireId
                                                        auth.UserId,                                //UserId
                                                        auth.FirstName,                             //FirstName
                                                        auth.LastName,                              //LastName
                                                        auth.RoleId,                                //RoleId
                                                        auth.CompanyId,                             //CompanyId
                                                        auth.CompanyName,                           //CompanyName
                                                        null,                                       //Comments
                                                        null,                                       //OldAssigned
                                                        qice.ContactEmail,                          //NewAssigned
                                                        null,                                       //NewAssigned2
                                                        null,                                       //NewAssigned3
                                                        null)                                       //NewAssigned4
                                                        == false)
            {
                //for now we do nothing if action log fails.
            }

            Redirect("Contact Verification E-Mail Sent to Supplier.", String.Format("SafetyPQ_QuestionnaireInitial.aspx?q={0}&s=2", q));
        }
        catch (Exception ex)
        {
            Elmah.ErrorSignal.FromContext(Context).Raise(ex);
            throw new Exception("Error Occurred, Could not Send E-Mail. Please Contact your Administrator.");
        }
    }
    protected void btnVerifyConfirm_Click(object sender, EventArgs e)
    {
        if (auth.RoleId == (int)RoleList.PreQual || auth.RoleId == (int)RoleList.Contractor)
            throw new Exception("Only Procurement Personal are allowed to do this.");
        try
        {
            bool sendEmailToProcurer = false; //DT 228 Cindi Thornton 23/9/15 - new email

            string q = Request.QueryString["q"];
            //QuestionnaireInitialContactService qicService = new QuestionnaireInitialContactService();
            //QuestionnaireInitialContact qic = qicService.GetByQuestionnaireId(Convert.ToInt32(q));
            int questionnaireId = Convert.ToInt32(q);

            model.QuestionnaireInitialContact qic = qicService.Get(i => i.QuestionnaireId == questionnaireId, null);//DT 226 Cindi Thornton 23/9/15 - change to Hordern repo from Kaizen, was causing a locking issue

            if (qic != null && qAnswer1_3.Value != null)
            {
                if (qic.ContactEmail == qAnswer1_3.Value.ToString())
                {
                    //DT 226 Cindi Thornton 23/9/15 - determine if questionnaire has already been submitted and is now being re-verified
                    model.Questionnaire questionnaire = questionnaireService.Get(i => i.QuestionnaireId == qic.QuestionnaireId, null);

                    //questionnaire has been submitted AND the status is now unverified (to submit it must be verified, so shows it has changed simce submitted)
                    if (questionnaire.InitialSubmittedDate != null && qic.ContactStatusId == (int)QuestionnaireInitialContactStatusList.NotVerified && questionnaire.InitialStatus  ==(int)QuestionnaireStatusList.AssessmentComplete)
                    {
                        sendEmailToProcurer = true;
                    }
                    //End DT226

                    qic.ContactStatusId = (int)QuestionnaireInitialContactStatusList.Verified;
                    qic.VerifiedDate = DateTime.Now;
                    qic.ModifiedByUserId = auth.UserId;

                    // DT 226 Cindi Thornton 23/9/15 - change to new repo & send email
                    qicService.Update(qic); //qicService.Save(qic);


                    if (sendEmailToProcurer)
                    {
                        SendEmailOnContactReverification(qic, questionnaire);
                    }
                    //End DT226


                    if (Helper.Questionnaire.ActionLog.Add((int)QuestionnaireActionList.SupplierContactVerificationEmailReceiptConfirmed,
                                                        Convert.ToInt32(q),                         //QuestionnaireId
                                                        auth.UserId,                                //UserId
                                                        auth.FirstName,                             //FirstName
                                                        auth.LastName,                              //LastName
                                                        auth.RoleId,                                //RoleId
                                                        auth.CompanyId,                             //CompanyId
                                                        auth.CompanyName,                           //CompanyName
                                                        null,                                       //Comments
                                                        null,                                       //OldAssigned
                                                        null,                                       //NewAssigned
                                                        null,                                       //NewAssigned2
                                                        null,                                       //NewAssigned3
                                                        null)                                       //NewAssigned4
                                                        == false)
                    {
                        //for now we do nothing if action log fails.
                    }

                    //send email to procurement notifying them that its' been verified.
                    //string PreReSq = "Contractor Safety Pre-Qualification";
                    //QuestionnaireService qService = new QuestionnaireService();
                    //Questionnaire _questionnaire = qService.GetByQuestionnaireId(Convert.ToInt32(q));
                    //if (_questionnaire.IsReQualification) PreReSq = "Contractor Safety Re-Qualification";
                    //if (_questionnaire.SubContractor == true) PreReSq = "Sub " + PreReSq;
                    //CompaniesService cService = new CompaniesService();
                    //Companies _companies = cService.GetByCompanyId(_questionnaire.CompanyId);
                    //string Subject = String.Format("{0}: 0 - Contact Verified - {1}", PreReSq, _companies.CompanyName);
                    //string Message = "The following company's contact has been verified.\n" +
                    //                 "You may now go back and [submit] the procurement questionnaire to continue the Safety Qualification process.\n\n" +
                    //                 "Company: " + _companies.CompanyName + "\n" +
                    //                 "Contact First Name: " + qic.ContactFirstName + "\n" +
                    //                 "Contact Last Name: " + qic.ContactLastName + "\n" +
                    //                 "Contact E-Mail: " + qic.ContactEmail + "\n" +
                    //                 "Contact Phone: " + qic.ContactPhone + "\n" +
                    //                 "Contact Title: " + qic.ContactTitle + "\n";

                    //UsersService uService = new UsersService();
                    //Users uCreated = uService.GetByUserId(_questionnaire.CreatedByUserId);
                    //String EmailTo = uCreated.Email;

                    //QuestionnaireInitialResponseService qirService = new QuestionnaireInitialResponseService();
                    //QuestionnaireInitialResponse qir = qirService.GetByQuestionnaireIdQuestionId(_questionnaire.QuestionnaireId, "2");
                    //if (qir != null)
                    //{
                    //    if (!String.IsNullOrEmpty(qir.AnswerText))
                    //    {
                    //        try
                    //        {
                    //            UsersProcurementListService uplService = new UsersProcurementListService();
                    //            DataSet dsUpl = uplService.GetByUserId(Convert.ToInt32(qir.AnswerText));
                    //            if (dsUpl != null)
                    //            {
                    //                if (dsUpl.Tables[0] != null)
                    //                {
                    //                    if (dsUpl.Tables[0].Rows[0].ItemArray[5] != DBNull.Value)
                    //                    {
                    //                        EmailTo = dsUpl.Tables[0].Rows[0].ItemArray[5].ToString();
                    //                    }
                    //                }
                    //            }
                    //        }
                    //        catch (Exception ex)
                    //        {
                    //            Elmah.ErrorSignal.FromContext(Context).Raise(ex);
                    //        }
                    //        qir.Dispose();
                    //    }
                    //}
                    //Configuration configuration = new Configuration();
                    //string[] to = { EmailTo };
                    //string[] cc = { configuration.GetValue(ConfigList.ContactEmail) };
                    //Helper.Email.sendEmail(to, cc, null, Subject, Message, null);

                    //DT226 Cindi Thornton 23/9/15 - add comment to popup that if the procurer email has been sent
                    string redirectText = "Contact Verified!";
                    if (sendEmailToProcurer)
                        redirectText = "Contact details changed after Procurement Questionnaire submitted. Email sent to CSMS Admin to advise. " + redirectText;

                    Redirect(redirectText, String.Format("SafetyPQ_QuestionnaireInitial.aspx?q={0}&s=3", q));
                    //Redirect("Contact Verified!", String.Format("SafetyPQ_QuestionnaireInitial.aspx?q={0}&s=3", q));
                    //End DT226
                }
            }
            else
            {
                throw new Exception("Could not Verify Contact.");
            }
        }
        catch (Exception ex)
        {
            Elmah.ErrorSignal.FromContext(Context).Raise(ex);
            throw new Exception("Error Occurred: " + ex.Message);
        }
    }

    /// <summary>
    /// // DT 226 Cindi Thornton 23/9/15 - new method to send email to procurement when a contact has been re-verified after submitting the questionnaire
    /// </summary>
    /// <param name="qic"></param>
    /// <param name="questionnaire"></param>
    private void SendEmailOnContactReverification(model.QuestionnaireInitialContact qic, model.Questionnaire questionnaire)
    {
        model.Company company = companyService.Get(i => i.CompanyId == questionnaire.CompanyId, null);
        model.EmailTemplate eMailTemplate = emailTemplateService.Get(i => i.EmailTemplateName == "Safety Qualification Questionnaire Key Contact Changed - Internal", null);


        var blah = qInitialContactAuditService.GetMany(2, i => i.QuestionnaireInitialContactId == qic.QuestionnaireInitialContactId, null, null);

        // Get all the records from the audit table
        List<model.QuestionnaireInitialContactAudit> contactAudits = qInitialContactAuditService.GetMany(null,
                i => i.QuestionnaireInitialContactId == qic.QuestionnaireInitialContactId, o => o.OrderByDescending(a => a.AuditId), null);

        string oldFirstName = null, oldLastName = null, oldEmail = null, oldTitle = null, oldPhone = null;

        //get the most recent details from the previous verification (not this one)
        //bool currentVerifiedFound = false, previousVerifiedFound = false;
        int verifiedRecordsFound = 0;

        foreach (model.QuestionnaireInitialContactAudit audit in contactAudits)
        {
            // the most recent record should be verified, skip it as we want the old details
            if (audit.ContactStatusId == (int)QuestionnaireInitialContactStatusList.Verified)
            {
                if (verifiedRecordsFound == 0)
                {
                    verifiedRecordsFound++; // found the current one we just saved, need further back
                    continue;
                }
                else if (verifiedRecordsFound == 1)
                    verifiedRecordsFound++; // found the previous verified record

                //else if (verifiedRecordsFound > 1) // we're now too far back, quit
                //    break;
            }

            //we're now in the previous rows - we don't care about the contact status for these
            else if (verifiedRecordsFound >= 2)
            {
                //Now we loop backwards and take the most recent of any values
                if (audit.ContactFirstName != null && oldFirstName == null) oldFirstName = audit.ContactFirstName;
                if (audit.ContactLastName != null && oldLastName == null) oldLastName = audit.ContactLastName;
                if (audit.ContactEmail != null && oldEmail == null) oldEmail = audit.ContactEmail;
                if (audit.ContactTitle != null && oldTitle == null) oldTitle = audit.ContactTitle;
                if (audit.ContactPhone != null && oldPhone == null) oldPhone = audit.ContactPhone;
            }
        }

        byte[] bodyByte = eMailTemplate.EmailBody;
        string mailbody = System.Text.Encoding.ASCII.GetString(bodyByte);
        string subject = eMailTemplate.EmailTemplateSubject;

        subject = subject.Replace("{CompanyName}", company.CompanyName);
        mailbody = mailbody.Replace("{CompanyName}", company.CompanyName);

        mailbody = mailbody.Replace("{FirstName_Old}", oldFirstName);
        mailbody = mailbody.Replace("{LastName_Old}", oldLastName);
        mailbody = mailbody.Replace("{Email_Old}", oldEmail);
        mailbody = mailbody.Replace("{JobTitle_Old}", oldTitle);
        mailbody = mailbody.Replace("{Telephone_Old}", oldPhone);

        mailbody = mailbody.Replace("{FirstName_New}", qic.ContactFirstName);
        mailbody = mailbody.Replace("{LastName_New}", qic.ContactLastName);
        mailbody = mailbody.Replace("{Email_New}", qic.ContactEmail);
        mailbody = mailbody.Replace("{JobTitle_New}", qic.ContactTitle);
        mailbody = mailbody.Replace("{Telephone_New}", qic.ContactPhone);

        //get the ContactEmail to set the 'To'
        model.Config config = configService.Get(c => c.Key == "ContactEmail", null);

        string to = "";
        if (config.Value != null)
        {
            to = config.Value;
        }
        emailLogService.Insert(to, null, null, subject, mailbody, "Questionnaire", true, null, null);
    }

    public void Redirect(string l_msg, string l_sURL)
    {
        ScriptManager l_oSM = ScriptManager.GetCurrent(Page);
        if (l_oSM != null)
        {
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "wsReturnToPreviousPage", String.Format("alert('{0}');window.location.href='{1}';", l_msg, l_sURL), true);
        }
        else
        {
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "wsReturnToPreviousPage", String.Format("alert('{0}');window.location.href='{1}';", l_msg, l_sURL), true);
        }
    }

    //DT 228 (Enhancement 086) Cindi Thornton 21/9/15 - new function to set visibility and values of new Health Check fields
    // The logic to determine the traffic light colour is the same as in nonCompliance2.ascx.cs, please change both.
    private void setHealthCheckTableVisibility()
    {
        string companyName = qAnswer14.Text;

        if (companyName == "")
        {
            tblHealthCheck.Visible = false;
            return;
        }

        tblHealthCheck.Visible = true;

        // determine colour of traffic light

        //get the company details and questionnaires for that company
        Repo.CSMS.Service.Database.ICompanyService companyService = ALCOA.CSMS.Website.Global.GetInstance<Repo.CSMS.Service.Database.ICompanyService>();
        model.Company c = companyService.Get(i => i.CompanyName == companyName, null);

        repo.View.IQuestionnaireWithLocationApprovalViewService qWithLocationApprovalViewService = ALCOA.CSMS.Website.Global.GetInstance<repo.View.IQuestionnaireWithLocationApprovalViewService>();
        List<model.QuestionnaireWithLocationApprovalView> qWithLocationApprovalView =
            qWithLocationApprovalViewService.GetMany(null, p => p.CompanyName == companyName, o => o.OrderByDescending(m => m.QuestionnaireId), null);

        //default colour of traffic light is red
        gcTL_SC.Value = GetImageURL(TrafficLightColor2.Red);


        //loop through the questionnaires (newest first) and use the first complete one
        foreach (model.QuestionnaireWithLocationApprovalView q in qWithLocationApprovalView)
        {
            if (q != null && q.Status == (int)QuestionnaireStatusList.AssessmentComplete)
            {
                if (q.IsVerificationRequired && c.CompanyStatus2Id == (int)CompanyStatus2List.Contractor &&
                    q.LevelOfSupervision != "Direct" && q.MainAssessmentValidTo >= DateTime.Now)
                {
                    gcTL_SC.Value = GetImageURL(TrafficLightColor2.Green);
                }
                break; // we've found a completed assessment, don't check any further.
            }
        }
    }

    public static string GetImageURL(Enum value)
    {
        FieldInfo fi = value.GetType().GetField(value.ToString());
        DescriptionAttribute[] attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);
        return (attributes.Length > 0) ? attributes[0].Description : value.ToString();
    }

    private enum TrafficLightColor2
    {
        [Description("0")]
        Grey = 0,
        [Description("1")]
        Red = 1,
        [Description("2")]
        Yellow = 2,
        [Description("3")]
        Green = 3,
    }
    //End DT228

    //protected void qAnswer4_Validation(object sender, DevExpress.Web.ASPxEditors.ValidationEventArgs e)
    //{
    //    bool isValid = true;
    //    if (qAnswer4.SelectedItem != null)
    //    {
    //        isValid = true;
    //    }
    //    else
    //    {
    //        if (qAnswer4_1.Text == "")
    //        {
    //            isValid = false;
    //        }
    //        else
    //        {
    //            isValid = true;
    //        }
    //    }
    //    e.Value = isValid;
    //}
}