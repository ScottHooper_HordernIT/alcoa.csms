using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Text;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using KaiZen.CSMS.Web;
using KaiZen.CSMS.Services;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxTabControl;
using DevExpress.Web.Data;
using DevExpress.Web.ASPxGridView;
using DevExpress.XtraPrinting;

public partial class UserControls_FinancialReport2 : System.Web.UI.UserControl
{
    Auth auth = new Auth();
    protected void Page_Load(object sender, EventArgs e) {  Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); }

    private void moduleInit(bool postBack)
    {
        if (!postBack)
        { //first time load

            if (Helper.General.isPrivilegedUser(auth.UserId) || auth.RoleId == (int)RoleList.Administrator)
            {
                //cbRegionSite
                    int _i = 0;
                    cbRegionSite.Items.Clear();
                    SitesFilters sitesFilters = new SitesFilters();
                    sitesFilters.Append(SitesColumn.IsVisible, "1");
                    TList<Sites> sitesList = DataRepository.SitesProvider.GetPaged(sitesFilters.ToString(), "SiteName ASC", 0, 100, out _i);
                    foreach (Sites s in sitesList)
                    {
                        cbRegionSite.Items.Add(s.SiteName, s.SiteId);
                    }
                    cbRegionSite.Items.Add("----------------", 0);

                    RegionsFilters regionsFilters = new RegionsFilters();
                    regionsFilters.Append(RegionsColumn.IsVisible, "1");
                    TList<Regions> regionsList = DataRepository.RegionsProvider.GetPaged(regionsFilters.ToString(), "Ordinal ASC", 0, 100, out _i);
                    foreach (Regions r in regionsList)
                    {
                        int NegRegionId = -1 * r.RegionId;
                        //if (r.RegionInternalName != "AU")
                        cbRegionSite.Items.Add(r.RegionName, NegRegionId);
                    }

                    cbRegionSite.Text = "Australia";
                    cbRegionSite.SelectedItem.Value = -2;

                int currentMonth = DateTime.Today.Month;
                int currentYear = DateTime.Today.Year;
                //DT 3179 Changes
                //ddlYear.Items.Add(new ListEditItem((Convert.ToInt32(DateTime.Now.Year) - 3).ToString(),(Convert.ToInt32(DateTime.Now.Year) - 3)));
                //ddlYear.Items.Add(new ListEditItem((Convert.ToInt32(DateTime.Now.Year) - 2).ToString(),(Convert.ToInt32(DateTime.Now.Year) - 2)));
                //ddlYear.Items.Add(new ListEditItem((Convert.ToInt32(DateTime.Now.Year) - 1).ToString(),(Convert.ToInt32(DateTime.Now.Year) - 1)));
                //ddlYear.Items.Add(new ListEditItem(DateTime.Now.Year.ToString(), DateTime.Now.Year.ToString()));
                ddlYear.DataSourceID = "sqldsYear";
                ddlYear.TextField = "Year";
                ddlYear.ValueField = "Year";
                ddlYear.SelectedIndex = 0;

                ddlYear.Value = currentYear;
                ddlMonth.Value = currentMonth;
                if (currentMonth > 1)
                {
                    ddlMonth.Value = (currentMonth - 1);
                }
                else
                {
                    ddlMonth.Value = 12;
                    ddlYear.Value = currentYear - 1;
                }
                GoGoGo(ddlYear.Value.ToString());
                grid.ExpandAll();
            }
            else
            {
                Response.Redirect(String.Format("AccessDenied.aspx?error={0}", Server.UrlEncode("Unrecognised User.")));
            }

            // Export
            String exportFileName = @"ALCOA CSMS - Financial Report - Contractor Summary - FTE"; //Chrome & IE is fine.
            String userAgent = Request.Headers.Get("User-Agent");
            if (
                (userAgent.Contains("Mozilla") && !userAgent.Contains("AppleWebKit") && !userAgent.Contains("MSIE")) || //Mozilla
                (userAgent.Contains("AppleWebKit") && !userAgent.Contains("Chrome"))      //Safari
                )
            {
                exportFileName = exportFileName.Replace(" ", "_");
            }
            Helper.ExportGrid.Settings(ucExportButtons, exportFileName);
        }
    }

    protected void GoGoGo(string Year)
    {
        //DT 3179 Changes
        if (Year != "")
        {
            SessionHandler.spVar_Year = Year;
        }
        else
        {            
            SessionHandler.spVar_Year = ddlYear.SelectedItem.Value.ToString();
        }
        SessionHandler.spVar_Month = ddlMonth.SelectedItem.Value.ToString();
        
        sqldsFR.DataBind();

        if (cbRegionSite.SelectedItem != null)
        {
            int RegionSiteId = Convert.ToInt32(cbRegionSite.SelectedItem.Value);
            if (Convert.ToInt32(cbRegionSite.SelectedItem.Value) > 0)
            {
                //Site Selected
                SitesService sService = new SitesService();
                Sites s = sService.GetBySiteId(RegionSiteId);
                grid.FilterExpression = String.Format("Location = '{0}'", s.SiteName);
            }
            else
            {
                //Region Selected
                RegionsService rService = new RegionsService();
                Regions region_AU = rService.GetByRegionInternalName("AU");
                int RegionId = (RegionSiteId * -1);
                if (RegionId == region_AU.RegionId || RegionId == 0)
                {
                    grid.FilterExpression = "";
                }
                else
                {
                    string sitesFilterString = "Location = '";
                    RegionsSitesService rsService = new RegionsSitesService();
                    SitesService sService = new SitesService();
                    TList<RegionsSites> list_RegionsSites = rsService.GetByRegionId(RegionId);
                    int i = 1;
                    foreach (RegionsSites rs in list_RegionsSites)
                    {
                        Sites s = sService.GetBySiteId(rs.SiteId);

                        if (i < list_RegionsSites.Count)
                        {
                            sitesFilterString += s.SiteName + "' OR Location = '";
                        }
                        else
                        {
                            sitesFilterString += s.SiteName + "'";

                        }
                        i++;
                    }
                    grid.FilterExpression = sitesFilterString;
                }
            }
        }

        grid.DataBind();
    }

    protected void ASPxButton1_Click(object sender, EventArgs e)
    {
        //DT 3179 Changes
        GoGoGo("");
        grid.ExpandAll();
    }
}
