﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface IARPNameMasterService
    {
        ARPNameMaster Get(Expression<Func<ARPNameMaster, bool>> where, List<Expression<Func<ARPNameMaster, object>>> includes);
        List<ARPNameMaster> GetMany(int? top, Expression<Func<ARPNameMaster, bool>> where, Func<IQueryable<ARPNameMaster>, IOrderedQueryable<ARPNameMaster>> orderBy, List<Expression<Func<ARPNameMaster, object>>> includes);
        List<ARPNameMaster> GetManyPaged(int pageNumber, int pageSize, Expression<Func<ARPNameMaster, bool>> where, Func<IQueryable<ARPNameMaster>, IOrderedQueryable<ARPNameMaster>> orderBy, List<Expression<Func<ARPNameMaster, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<ARPNameMaster, bool>> where, List<Expression<Func<ARPNameMaster, object>>> includes);
        void Insert(ARPNameMaster item, bool saveChanges = true);
        void Update(ARPNameMaster item, bool saveChanges = true);
        void Delete(ARPNameMaster item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class ARPNameMasterService : IARPNameMasterService
    {
        private readonly IRepository<ARPNameMaster> _repository;

        public ARPNameMasterService(IRepository<ARPNameMaster> repository)
        {
            this._repository = repository;
        }

        public ARPNameMaster Get(Expression<Func<ARPNameMaster, bool>> where, List<Expression<Func<ARPNameMaster, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<ARPNameMaster> GetMany(int? top, Expression<Func<ARPNameMaster, bool>> where, Func<IQueryable<ARPNameMaster>, IOrderedQueryable<ARPNameMaster>> orderBy, List<Expression<Func<ARPNameMaster, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<ARPNameMaster> GetManyPaged(int pageNumber, int pageSize, Expression<Func<ARPNameMaster, bool>> where, Func<IQueryable<ARPNameMaster>, IOrderedQueryable<ARPNameMaster>> orderBy, List<Expression<Func<ARPNameMaster, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<ARPNameMaster, bool>> where, List<Expression<Func<ARPNameMaster, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(ARPNameMaster item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("ARPNameMaster");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(ARPNameMaster item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("ARPNameMaster");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(ARPNameMaster item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("ARPNameMaster");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
