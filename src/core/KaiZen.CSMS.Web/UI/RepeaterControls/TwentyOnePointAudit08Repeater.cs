﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.ComponentModel;
using System.Web.UI.Design.WebControls;
using System.Web.UI.Design;
using System.Web.UI.WebControls;

namespace KaiZen.CSMS.Web.UI
{
    /// <summary>
    /// A designer class for a strongly typed repeater <c>TwentyOnePointAudit08Repeater</c>
    /// </summary>
	public class TwentyOnePointAudit08RepeaterDesigner : System.Web.UI.Design.ControlDesigner
	{
	    /// <summary>
        /// Initializes a new instance of the <see cref="T:TwentyOnePointAudit08RepeaterDesigner"/> class.
        /// </summary>
		public TwentyOnePointAudit08RepeaterDesigner()
		{
		}

        /// <summary>
        /// Initializes the control designer and loads the specified component.
        /// </summary>
        /// <param name="component">The control being designed.</param>
		public override void Initialize(IComponent component)
		{
			if (!(component is TwentyOnePointAudit08Repeater))
			{ 
				throw new ArgumentException("Component is not a TwentyOnePointAudit08Repeater."); 
			} 
			base.Initialize(component); 
			base.SetViewFlags(ViewFlags.TemplateEditing, true); 
		}


		/// <summary>
		/// Generate HTML for the designer
		/// </summary>
		/// <returns>a string of design time HTML</returns>
		public override string GetDesignTimeHtml()
		{

			// Get the instance this designer applies to
			//
			TwentyOnePointAudit08Repeater z = (TwentyOnePointAudit08Repeater)Component;
			z.DataBind();

			return base.GetDesignTimeHtml();

			//return z.RenderAtDesignTime();

			//	ControlCollection c = z.Controls;
			//Totem z = (Totem) Component;
			//Totem z = (Totem) Component;
			//return ("<div style='border: 1px gray dotted; background-color: lightgray'><b>TagStat :</b> zae |  qsdds</div>");

		}
	}

    /// <summary>
    /// A strongly typed repeater control for the <see cref="TwentyOnePointAudit08Repeater"/> Type.
    /// </summary>
	[Designer(typeof(TwentyOnePointAudit08RepeaterDesigner))]
	[ParseChildren(true)]
	[ToolboxData("<{0}:TwentyOnePointAudit08Repeater runat=\"server\"></{0}:TwentyOnePointAudit08Repeater>")]
	public class TwentyOnePointAudit08Repeater : CompositeDataBoundControl, System.Web.UI.INamingContainer
	{
	    /// <summary>
        /// Initializes a new instance of the <see cref="T:TwentyOnePointAudit08Repeater"/> class.
        /// </summary>
		public TwentyOnePointAudit08Repeater()
		{
		}

		/// <summary>
        /// Gets a <see cref="T:System.Web.UI.ControlCollection"></see> object that represents the child controls for a specified server control in the UI hierarchy.
        /// </summary>
        /// <value></value>
        /// <returns>The collection of child controls for the specified server control.</returns>
		public override ControlCollection Controls
		{
			get
			{
				this.EnsureChildControls();
				return base.Controls;
			}
		}

		private ITemplate m_headerTemplate;
		/// <summary>
        /// Gets or sets the header template.
        /// </summary>
        /// <value>The header template.</value>
		[Browsable(false)]
		[TemplateContainer(typeof(TwentyOnePointAudit08Item))]
		[PersistenceMode(PersistenceMode.InnerDefaultProperty)]
		public ITemplate HeaderTemplate
		{
			get { return m_headerTemplate; }
			set { m_headerTemplate = value; }
		}

		private ITemplate m_itemTemplate;
		/// <summary>
        /// Gets or sets the item template.
        /// </summary>
        /// <value>The item template.</value>
		[Browsable(false)]
		[TemplateContainer(typeof(TwentyOnePointAudit08Item))]
		[PersistenceMode(PersistenceMode.InnerDefaultProperty)]
		public ITemplate ItemTemplate
		{
			get { return m_itemTemplate; }
			set { m_itemTemplate = value; }
		}

		private ITemplate m_seperatorTemplate;
        /// <summary>
        /// Gets or sets the Seperator Template
        /// </summary>
        [Browsable(false)]
        [TemplateContainer(typeof(TwentyOnePointAudit08Item))]
        [PersistenceMode(PersistenceMode.InnerDefaultProperty)]
        public ITemplate SeperatorTemplate
        {
            get { return m_seperatorTemplate; }
            set { m_seperatorTemplate = value; }
        }
			
		private ITemplate m_altenateItemTemplate;
        /// <summary>
        /// Gets or sets the alternating item template.
        /// </summary>
        /// <value>The alternating item template.</value>
		[Browsable(false)]
		[TemplateContainer(typeof(TwentyOnePointAudit08Item))]
		[PersistenceMode(PersistenceMode.InnerDefaultProperty)]
		public ITemplate AlternatingItemTemplate
		{
			get { return m_altenateItemTemplate; }
			set { m_altenateItemTemplate = value; }
		}

		private ITemplate m_footerTemplate;
        /// <summary>
        /// Gets or sets the footer template.
        /// </summary>
        /// <value>The footer template.</value>
		[Browsable(false)]
		[TemplateContainer(typeof(TwentyOnePointAudit08Item))]
		[PersistenceMode(PersistenceMode.InnerDefaultProperty)]
		public ITemplate FooterTemplate
		{
			get { return m_footerTemplate; }
			set { m_footerTemplate = value; }
		}

//      /// <summary>
//      /// Called by the ASP.NET page framework to notify server controls that use composition-based implementation to create any child controls they contain in preparation for posting back or rendering.
//      /// </summary>
//		protected override void CreateChildControls()
//      {
//         if (ChildControlsCreated)
//         {
//            return;
//         }

//         Controls.Clear();

//         //Instantiate the Header template (if exists)
//         if (m_headerTemplate != null)
//         {
//            Control headerItem = new Control();
//            m_headerTemplate.InstantiateIn(headerItem);
//            Controls.Add(headerItem);
//         }

//         //Instantiate the Footer template (if exists)
//         if (m_footerTemplate != null)
//         {
//            Control footerItem = new Control();
//            m_footerTemplate.InstantiateIn(footerItem);
//            Controls.Add(footerItem);
//         }
//
//         ChildControlsCreated = true;
//      }
	
		/// <summary>
        /// Overridden and Empty so that span tags are not written
        /// </summary>
        /// <param name="writer"></param>
        public override void RenderBeginTag(HtmlTextWriter writer)
        {
            
        }

        /// <summary>
        /// Overridden and Empty so that span tags are not written
        /// </summary>
        /// <param name="writer"></param>
        public override void RenderEndTag(HtmlTextWriter writer)
        {
                
        }		
		
		/// <summary>
      	/// Called by the ASP.NET page framework to notify server controls that use composition-based implementation to create any child controls they contain in preparation for posting back or rendering.
      	/// </summary>
		protected override int CreateChildControls(System.Collections.IEnumerable dataSource, bool dataBinding)
      	{
         int pos = 0;

         if (dataBinding)
         {
            //Instantiate the Header template (if exists)
            if (m_headerTemplate != null)
            {
                Control headerItem = new Control();
                m_headerTemplate.InstantiateIn(headerItem);
                Controls.Add(headerItem);
            }
			if (dataSource != null)
			{
				foreach (object o in dataSource)
				{
						KaiZen.CSMS.Entities.TwentyOnePointAudit08 entity = o as KaiZen.CSMS.Entities.TwentyOnePointAudit08;
						TwentyOnePointAudit08Item container = new TwentyOnePointAudit08Item(entity);
	
						if (m_itemTemplate != null && (pos % 2) == 0)
						{
							m_itemTemplate.InstantiateIn(container);
							
							if (m_seperatorTemplate != null)
							{
								m_seperatorTemplate.InstantiateIn(container);
							}
						}
						else
						{
							if (m_altenateItemTemplate != null)
							{
								m_altenateItemTemplate.InstantiateIn(container);
								
								if (m_seperatorTemplate != null)
								{
									m_seperatorTemplate.InstantiateIn(container);
								}
								
							}
							else if (m_itemTemplate != null)
							{
								m_itemTemplate.InstantiateIn(container);
								
								if (m_seperatorTemplate != null)
								{
									m_seperatorTemplate.InstantiateIn(container);
								}
							}
							else
							{
								// no template !!!
							}
						}
						Controls.Add(container);
						
						container.DataBind();
						
						pos++;
				}
			}
            //Instantiate the Footer template (if exists)
            if (m_footerTemplate != null)
            {
                Control footerItem = new Control();
                m_footerTemplate.InstantiateIn(footerItem);
                Controls.Add(footerItem);
            }

		}
			
			return pos;
		}

        /// <summary>
        /// Raises the <see cref="E:System.Web.UI.Control.PreRender"></see> event.
        /// </summary>
        /// <param name="e">An <see cref="T:System.EventArgs"></see> object that contains the event data.</param>
		protected override void OnPreRender(EventArgs e)
		{
			base.DataBind();
		}

		#region Design time
        /// <summary>
        /// Renders at design time.
        /// </summary>
        /// <returns>a  string of the Designed HTML</returns>
		internal string RenderAtDesignTime()
		{			
			return "Designer currently not implemented"; 
		}

		#endregion
	}

    /// <summary>
    /// A wrapper type for the entity
    /// </summary>
	[System.ComponentModel.ToolboxItem(false)]
	public class TwentyOnePointAudit08Item : System.Web.UI.Control, System.Web.UI.INamingContainer
	{
		private KaiZen.CSMS.Entities.TwentyOnePointAudit08 _entity;

        /// <summary>
        /// Initializes a new instance of the <see cref="T:TwentyOnePointAudit08Item"/> class.
        /// </summary>
		public TwentyOnePointAudit08Item()
			: base()
		{ }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:TwentyOnePointAudit08Item"/> class.
        /// </summary>
		public TwentyOnePointAudit08Item(KaiZen.CSMS.Entities.TwentyOnePointAudit08 entity)
			: base()
		{
			_entity = entity;
		}
		
        /// <summary>
        /// Gets the Id
        /// </summary>
        /// <value>The Id.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32 Id
		{
			get { return _entity.Id; }
		}
        /// <summary>
        /// Gets the Achieved8a
        /// </summary>
        /// <value>The Achieved8a.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved8a
		{
			get { return _entity.Achieved8a; }
		}
        /// <summary>
        /// Gets the Achieved8b
        /// </summary>
        /// <value>The Achieved8b.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved8b
		{
			get { return _entity.Achieved8b; }
		}
        /// <summary>
        /// Gets the Achieved8c
        /// </summary>
        /// <value>The Achieved8c.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved8c
		{
			get { return _entity.Achieved8c; }
		}
        /// <summary>
        /// Gets the Achieved8d
        /// </summary>
        /// <value>The Achieved8d.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved8d
		{
			get { return _entity.Achieved8d; }
		}
        /// <summary>
        /// Gets the Achieved8e
        /// </summary>
        /// <value>The Achieved8e.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved8e
		{
			get { return _entity.Achieved8e; }
		}
        /// <summary>
        /// Gets the Achieved8f
        /// </summary>
        /// <value>The Achieved8f.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved8f
		{
			get { return _entity.Achieved8f; }
		}
        /// <summary>
        /// Gets the Achieved8g
        /// </summary>
        /// <value>The Achieved8g.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved8g
		{
			get { return _entity.Achieved8g; }
		}
        /// <summary>
        /// Gets the Achieved8h
        /// </summary>
        /// <value>The Achieved8h.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved8h
		{
			get { return _entity.Achieved8h; }
		}
        /// <summary>
        /// Gets the Achieved8i
        /// </summary>
        /// <value>The Achieved8i.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved8i
		{
			get { return _entity.Achieved8i; }
		}
        /// <summary>
        /// Gets the Observation8a
        /// </summary>
        /// <value>The Observation8a.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation8a
		{
			get { return _entity.Observation8a; }
		}
        /// <summary>
        /// Gets the Observation8b
        /// </summary>
        /// <value>The Observation8b.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation8b
		{
			get { return _entity.Observation8b; }
		}
        /// <summary>
        /// Gets the Observation8c
        /// </summary>
        /// <value>The Observation8c.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation8c
		{
			get { return _entity.Observation8c; }
		}
        /// <summary>
        /// Gets the Observation8d
        /// </summary>
        /// <value>The Observation8d.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation8d
		{
			get { return _entity.Observation8d; }
		}
        /// <summary>
        /// Gets the Observation8e
        /// </summary>
        /// <value>The Observation8e.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation8e
		{
			get { return _entity.Observation8e; }
		}
        /// <summary>
        /// Gets the Observation8f
        /// </summary>
        /// <value>The Observation8f.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation8f
		{
			get { return _entity.Observation8f; }
		}
        /// <summary>
        /// Gets the Observation8g
        /// </summary>
        /// <value>The Observation8g.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation8g
		{
			get { return _entity.Observation8g; }
		}
        /// <summary>
        /// Gets the Observation8h
        /// </summary>
        /// <value>The Observation8h.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation8h
		{
			get { return _entity.Observation8h; }
		}
        /// <summary>
        /// Gets the Observation8i
        /// </summary>
        /// <value>The Observation8i.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation8i
		{
			get { return _entity.Observation8i; }
		}
        /// <summary>
        /// Gets the TotalScore
        /// </summary>
        /// <value>The TotalScore.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? TotalScore
		{
			get { return _entity.TotalScore; }
		}

        /// <summary>
        /// Gets a <see cref="T:KaiZen.CSMS.Entities.TwentyOnePointAudit08"></see> object
        /// </summary>
        /// <value></value>
        [System.ComponentModel.Bindable(true)]
        public KaiZen.CSMS.Entities.TwentyOnePointAudit08 Entity
        {
            get { return _entity; }
        }
	}
}
