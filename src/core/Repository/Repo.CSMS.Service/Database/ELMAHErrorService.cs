﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface IELMAHErrorService
    {
        ELMAH_Error Get(Expression<Func<ELMAH_Error, bool>> where, List<Expression<Func<ELMAH_Error, object>>> includes);
        List<ELMAH_Error> GetMany(int? top, Expression<Func<ELMAH_Error, bool>> where, Func<IQueryable<ELMAH_Error>, IOrderedQueryable<ELMAH_Error>> orderBy, List<Expression<Func<ELMAH_Error, object>>> includes);
        List<ELMAH_Error> GetManyPaged(int pageNumber, int pageSize, Expression<Func<ELMAH_Error, bool>> where, Func<IQueryable<ELMAH_Error>, IOrderedQueryable<ELMAH_Error>> orderBy, List<Expression<Func<ELMAH_Error, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<ELMAH_Error, bool>> where, List<Expression<Func<ELMAH_Error, object>>> includes);
        void Insert(ELMAH_Error item, bool saveChanges = true);
        void Update(ELMAH_Error item, bool saveChanges = true);
        void Delete(ELMAH_Error item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class ELMAHErrorService : IELMAHErrorService
    {
        private readonly IRepository<ELMAH_Error> _repository;

        public ELMAHErrorService(IRepository<ELMAH_Error> repository)
        {
            this._repository = repository;
        }

        public ELMAH_Error Get(Expression<Func<ELMAH_Error, bool>> where, List<Expression<Func<ELMAH_Error, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<ELMAH_Error> GetMany(int? top, Expression<Func<ELMAH_Error, bool>> where, Func<IQueryable<ELMAH_Error>, IOrderedQueryable<ELMAH_Error>> orderBy, List<Expression<Func<ELMAH_Error, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<ELMAH_Error> GetManyPaged(int pageNumber, int pageSize, Expression<Func<ELMAH_Error, bool>> where, Func<IQueryable<ELMAH_Error>, IOrderedQueryable<ELMAH_Error>> orderBy, List<Expression<Func<ELMAH_Error, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<ELMAH_Error, bool>> where, List<Expression<Func<ELMAH_Error, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(ELMAH_Error item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("ELMAH_Error");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(ELMAH_Error item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("ELMAH_Error");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(ELMAH_Error item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("ELMAH_Error");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
