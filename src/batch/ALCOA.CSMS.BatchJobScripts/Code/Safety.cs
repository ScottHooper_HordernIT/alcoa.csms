﻿using System;
using System.Collections.Generic;
using System.Text;

using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Security.Principal;

using KaiZen.CSMS.Data;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Services;
using KaiZen.Library;

using LumenWorks.Framework.IO.Csv;
using ALCOA.CSMS.Batch.Common.Log;


namespace ALCOA.CSMS.BatchJobScripts
{
    public class Safety
    {
        public class EHSIMS
        {
            public static void UpdateDb(string csvFilePath)
            {
                ConnectionStringSettings conString = ConfigurationManager.ConnectionStrings["ALCOA_WAO_CSMWP_devConnectionString"];
                using (SqlConnection cn = new SqlConnection(conString.ConnectionString))
                {
                    cn.Open();
                    //SET Context_Info 0x55555;
                    SqlCommand cmd = new SqlCommand("UPDATE Kpi SET ipFATI = null, ipMTI = null, ipRDI = null, ipLti = null, ipIfe = null, ipRN = null, IsSystemEdit = 1 WHERE YEAR(kpiDateTime) >= 2009 AND YEAR(kpiDateTime)<=2010", cn);
                    cmd.ExecuteNonQuery();
                    cn.Close();
                }

                string errorMsg = "";

                string reset = "";
                string startHeader = "Location Code";
                //2.1 Read from the actual Header of the CSV file. (Ignore junk at start of CSV file)
                string myFile = "";
                bool start = false;

                int? CompanyId = null;
                int? SiteId = null;
                DateTime dt;

                try
                {
                    FileStream logFileStream = new FileStream(csvFilePath, FileMode.Open,
                                                                            FileAccess.Read,
                                                                            FileShare.ReadWrite);
                    FileInfo fi = new FileInfo(csvFilePath);
                    DateTime dtMidnight = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0);
                    if (fi.LastWriteTime < dtMidnight)
                    {
                        //errorMsg += "File doesn't appear to have been updated by IHS batch job today" + ";";
                        //Log4NetService.LogInfo("Error: File doesn't appear to have been updated by IHS batch job today\n");
                    }


                    StreamReader logFileReader = new StreamReader(logFileStream);

                    while (!logFileReader.EndOfStream)
                    {
                        string line = logFileReader.ReadLine();
                        if (line.Contains(startHeader)) { start = true; };
                        if (start == true)
                        {
                            myFile += line + "\n";
                        }
                    }


                    logFileReader.Close();
                    logFileStream.Close();
                }
                catch (Exception ex)
                {
                    errorMsg += ex.Message + ";";
                    Log4NetService.LogInfo(ex.Message);
                }


                if (!String.IsNullOrEmpty(myFile))
                {
                    Log4NetService.LogInfo("CSV File Ok.");
                    try
                    {
                        using (CsvReader csv = new CsvReader(new StringReader(myFile), true))
                        {
                            int fieldCount = csv.FieldCount;
                            string[] headers = csv.GetFieldHeaders();
                            Log4NetService.LogInfo("Found Headers: " + String.Join(",", headers));

                            int i = 0;
                            while (csv.ReadNextRecord())
                            {
                                try
                                {
                                    i++;
                                    string locCode = csv["Location Code"].ToString();
                                    //string locCode = locCodeTemp.Substring(locCodeTemp.IndexOf("[") + 1,
                                    //    locCodeTemp.IndexOf("]") - locCodeTemp.IndexOf("[") - 1 );
                                    //Added By Sayani
                                    //TList<Sites> listSites = sService.GetByLocCode_Iproc(locCode);


                                    string ABN = csv["Cont Company Code"].ToString(); //Vikas
                                    //string ABN = csv["ABN"].ToString();
                                    string monthBeginning = csv["Month Beginning"].ToString();

                                    string locCode2 = "";
                                    //if WAM, check Crew Desc Col for either HUN or WDL
                                    if (locCode == "WAM")
                                    {
                                        string MineSite = csv["Crew Desc"].ToString();
                                        if (MineSite.Contains("HUN "))
                                        {
                                            locCode2 = "HUN";
                                        }
                                        else
                                        {
                                            if (MineSite.Contains("WDL "))
                                            {
                                                locCode2 = "WDL";
                                            }
                                            else
                                            {
                                                throw new Exception("\n" + "[" + i + "]\t" + "Skipping: " + ABN + ", " + locCode + ", " + monthBeginning + " could not determine WA mine site.");
                                            }
                                        }
                                    }

                                    if (locCode == "WAO")
                                    {
                                        string WAOSite = csv["Crew Desc"].ToString();
                                        if (WAOSite.Contains("BGN "))
                                        {
                                            locCode2 = "BGN";
                                        }
                                        else
                                        {
                                            throw new Exception("\n" + "[" + i + "]\t" + "Skipping: " + ABN + ", " + locCode + ", " + monthBeginning + " could not determine WAO site.");
                                        }
                                    }



                                    if (String.IsNullOrEmpty(locCode) || String.IsNullOrEmpty(ABN) || String.IsNullOrEmpty(monthBeginning))
                                    {
                                        throw new Exception("\n" + "[" + i + "]\t" + "Skipping: " + ABN + ", " + locCode + ", " + monthBeginning + " as missing one or more inputs.");
                                    }
                                    if (!String.IsNullOrEmpty(locCode2))
                                    {
                                        Log4NetService.LogInfo("\n" + "[" + i + "]\t" + "Checking: " + ABN + ", " + locCode + "(" + locCode2 + "), " + monthBeginning + " ...");
                                    }
                                    else
                                    {
                                        Log4NetService.LogInfo("\n" + "[" + i + "]\t" + "Checking: " + ABN + ", " + locCode + ", " + monthBeginning + " ...");
                                    }
                                    SitesService sService = new SitesService();

                                    //Added by Sayani for task# 1
                                    TList<Sites> listSites = sService.GetByLocCode(locCode);

                                    //TList<Sites> listSites = sService.GetByLocCode_Iproc(locCode);

                                    int index = 0;
                                    if (listSites.Count >= 1)
                                    {
                                        int _i = 0;
                                        foreach (Sites s in listSites)
                                        {
                                            if (locCode2 == "WDL" || locCode2 == "HUN")
                                            {
                                                if (s.SiteAbbrev == locCode2) index = _i;
                                            }
                                            _i++;
                                        }
                                    }
                                    else
                                    {
                                        throw new Exception("Combination of Company/Site not found in CSMS.");
                                    }

                                    SiteId = listSites[index].SiteId;

                                    string splitter = "/";
                                    if (monthBeginning.Contains("-")) splitter = "-";
                                    dt = (DateTime.ParseExact(monthBeginning, "dd" + splitter + "MMM" + splitter + "yy", System.Globalization.CultureInfo.InvariantCulture));

                                    if (dt.Year < 2011) //>2011 is IHS!
                                    {
                                        if (listSites.Count == 0)
                                        {
                                            EhsimsExceptionsService eService = new EhsimsExceptionsService();
                                            EhsimsExceptions e = new EhsimsExceptions();
                                            e.ReportDateTime = DateTime.Now;
                                            e.CompanyId = CompanyId;
                                            e.SiteId = SiteId;
                                            e.KpiDateTime = dt;
                                            e.ErrorMsg = "Location Code - Match not found.";
                                            eService.Insert(e);
                                            throw new Exception("Location Code - Match not found.");
                                        }
                                        //Commented By Vikas
                                        // CompaniesEhsimsMapService cemService = new CompaniesEhsimsMapService();

                                        //DataSet dsCEM = DataRepository.CompaniesEhsimsMapSitesEhsimsIhsListProvider.GetByContCompanyCodeEhsimsLocCode(companyCode, locCode);
                                        //if (locCode == "WAM")
                                        //{
                                        //    DataSet dsCEM2 = DataRepository.CompaniesEhsimsMapSitesEhsimsIhsListProvider.GetByContCompanyCodeSiteAbbrev(
                                        //                                                                            companyCode, locCode2);
                                        //    if (dsCEM2 != null)
                                        //    {
                                        //        if (dsCEM2.Tables[0].Rows.Count == 1)
                                        //        {
                                        //            dsCEM = null;
                                        //            dsCEM = dsCEM2.Copy();
                                        //        }
                                        //    }
                                        //}
                                        //End Commented By Vikas

                                        //0 - EhsimsMapId
                                        //1 - CompanyId
                                        //2 - ContCompanyCode
                                        //3 - SiteId
                                        //4 - IHS Site Name
                                        //5 - EHSIMS Loc Code
                                        //6 - EHSIMS Loc Code - IHS Site Name
                                        //7 - Site Name - EHSIMS Loc Code - IHS Site Name
                                        //8 - Site Name
                                        //9 - Site Abbrev
                                        //10- Company Name

                                        CompaniesService cService = new CompaniesService();
                                        Companies cs = cService.GetByCompanyAbn(ABN);
                                        bool found = false;
                                        if (cs != null)
                                        {
                                            CompanyId = Convert.ToInt32(cs.CompanyId);
                                            found = true;
                                        }
                                        //if(dsCEM.Tables[0].Rows.Count > 1)
                                        //{
                                        //    if (locCode == "WAO")
                                        //    {
                                        //        found = true;
                                        //    }
                                        //}

                                        if (!found)
                                        {
                                            EhsimsExceptionsService eService = new EhsimsExceptionsService();
                                            EhsimsExceptions e = new EhsimsExceptions();
                                            e.ReportDateTime = DateTime.Now;
                                            e.CompanyId = CompanyId;
                                            e.SiteId = SiteId;
                                            e.KpiDateTime = dt;
                                            string _errorMsg = "";
                                            if (cs == null)
                                            {
                                                //e.ErrorMsg = "Company - Match not found.";
                                                e.ErrorMsg = "Company not found for the specified ABN in CSMS"; //DT420 16/12/15 Cindi Thornton - error message needs to match code found in catch.
                                                _errorMsg = e.ErrorMsg;
                                            }
                                            //if (dsCEM.Tables[0].Rows.Count > 1)
                                            //{
                                            //    e.ErrorMsg = "Company & Location Code - Duplicates Found.";
                                            //    _errorMsg = e.ErrorMsg;
                                            //}
                                            eService.Insert(e);

                                            throw new Exception(_errorMsg);
                                        }
                                        else
                                        {
                                            CompaniesService companiesService = new CompaniesService();
                                            Companies companies = companiesService.GetByCompanyId(Convert.ToInt32(CompanyId));
                                            Log4NetService.LogInfo("Found Company Match (" + companies.CompanyName + ")...");

                                            KpiService kService = new KpiService();
                                            Kpi k = kService.GetByCompanyIdSiteIdKpiDateTime(Convert.ToInt32(CompanyId), listSites[index].SiteId, dt);

                                            if (k == null)
                                            {
                                                Kpi k2 = new Kpi();
                                                k2.CompanyId = Convert.ToInt32(CompanyId);
                                                k2.SiteId = listSites[index].SiteId;
                                                k2.KpiDateTime = dt;
                                                k2.CreatedbyUserId = 0;
                                                k2.ModifiedbyUserId = 0;
                                                k2.DateAdded = DateTime.Now;
                                                k2.Datemodified = DateTime.Now;
                                                k2.KpiGeneral = 0;
                                                k2.KpiCalcinerCapital = 0;
                                                k2.KpiCalcinerExpense = 0;
                                                k2.KpiResidue = 0;
                                                k2.AheaRefineryWork = 0;
                                                k2.AheaResidueWork = 0;
                                                k2.AheaSmeltingWork = 0;
                                                k2.AheaTotalManHours = 0;
                                                k2.AheaPeakNopplSiteWeek = 0;
                                                k2.AheaAvgNopplSiteMonth = 0;
                                                k2.AheaNoEmpExcessMonth = 0;
                                                k2.MtOthers = "";


                                                // Safety Plan submitted for year
                                                FileDbFilters query = new FileDbFilters();
                                                query.Append(FileDbColumn.CompanyId, CompanyId.ToString());
                                                DateTime dt2 = k2.KpiDateTime;
                                                dt2.Year.ToString();
                                                query.Append(FileDbColumn.ModifiedDate, "%" + dt2.Year.ToString() + "%");

                                                int count = 0;
                                                TList<FileDb> list = DataRepository.FileDbProvider.GetPaged(query.ToString(), null, 0, 100, out count);

                                                if (count > 0)
                                                {
                                                    k2.SafetyPlansSubmitted = true;
                                                }
                                                else
                                                {
                                                    k2.SafetyPlansSubmitted = false;
                                                }

                                                kService.Insert(k2);
                                                k = kService.GetByCompanyIdSiteIdKpiDateTime(Convert.ToInt32(CompanyId), listSites[index].SiteId, dt);
                                                //Log4NetService.LogInfo("create kpi");
                                            }

                                            if (k != null)
                                            {
                                                Log4NetService.LogInfo("Found Kpi...");
                                                string countIncidents = csv["Count of Incidents"].ToString();
                                                string resetline = String.Format("{0}|{1}|{2}|{3}", ABN, locCode, monthBeginning, csv["Case Type Desc"]);
                                                switch (csv["Case Type Desc"])
                                                {
                                                    case "First Aid":
                                                        if (!reset.Contains(resetline)) { k.IpFati = 0; };
                                                        k.IpFati += NumberUtilities.Convert.parseStringToNullableInt(countIncidents);
                                                        Log4NetService.LogInfo(String.Format("First Aid: {0}... ", countIncidents));
                                                        break;
                                                    case "Medical Treatment":
                                                        if (!reset.Contains(resetline)) { k.IpMti = 0; };
                                                        k.IpMti += NumberUtilities.Convert.parseStringToNullableInt(countIncidents);
                                                        Log4NetService.LogInfo(String.Format("Medical Treatment: {0}... ", countIncidents));
                                                        break;
                                                    case "Restricted Work":
                                                        if (!reset.Contains(resetline)) { k.IpRdi = 0; };
                                                        k.IpRdi += NumberUtilities.Convert.parseStringToNullableInt(countIncidents);
                                                        Log4NetService.LogInfo(String.Format("Restricted Work: {0}... ", countIncidents));
                                                        break;
                                                    case "Lost Work Day":
                                                        if (!reset.Contains(resetline)) { k.IpLti = 0; };
                                                        k.IpLti += NumberUtilities.Convert.parseStringToNullableInt(countIncidents);
                                                        Log4NetService.LogInfo(String.Format("Lost Work Day: {0}... ", countIncidents));
                                                        break;
                                                    case "Injury Free Event":
                                                        if (!reset.Contains(resetline)) { k.IpIfe = 0; };
                                                        k.IpIfe += NumberUtilities.Convert.parseStringToNullableInt(countIncidents);
                                                        Log4NetService.LogInfo(String.Format("Injury Free Event: {0}... ", countIncidents));
                                                        break;

                                                    //Added by Sayani Sil for Task# 1
                                                    case "GPP Risk Notification":
                                                        if (!reset.Contains(resetline)) { k.IpRN = 0; };
                                                        k.IpRN += NumberUtilities.Convert.parseStringToNullableInt(countIncidents);
                                                        Log4NetService.LogInfo(String.Format("GPP Risk Notification: {0}... ", countIncidents));
                                                        break;

                                                    default:
                                                        //throw new Exception("Possible Case Type Desc not found for " + csv["Cont Company Code"] + " & " + csv["Location Code"]);
                                                        break;
                                                }
                                                reset += resetline + "--";
                                                k.EntityState = EntityState.Changed;
                                                k.IsSystemEdit = true;
                                                kService.Save(k);
                                                Log4NetService.LogInfo("Updated. \n");
                                                Log4NetService.LogInfo("");
                                            }
                                            else
                                            {
                                                EhsimsExceptionsService eService = new EhsimsExceptionsService();
                                                EhsimsExceptions e = new EhsimsExceptions();
                                                e.ReportDateTime = DateTime.Now;
                                                e.CompanyId = CompanyId;
                                                e.SiteId = SiteId;
                                                e.KpiDateTime = dt;
                                                e.ErrorMsg = "KPI Not Found";
                                                eService.Insert(e);
                                                Log4NetService.LogInfo("Not Found Kpi...");
                                            }

                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    bool logError = true;
                                    if (ex.Message.Contains("Skipping")) logError = false;
                                    if (ex.Message.Contains("Company not found in CSMS.")) logError = false;
                                    if (ex.Message.Contains("Company not found for the specified ABN in CSMS")) logError = false;
       
                                    if (ex.Message.Contains("Location Code - Match not found.")) logError = false;
                                    if (ex.Message.Contains("Company & Location Code - Match not found.")) logError = false;
                                    if (ex.Message.Contains("Company & Location Code - Duplicates Found.")) logError = false;
                                    if (ex.Message.Contains("Not Found Kpi...")) logError = false;

                                    if (logError)
                                        errorMsg += ex.Message + ";";

                                    Log4NetService.LogInfo(ex.Message.ToString());
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        errorMsg += ex.Message + ";";
                        Log4NetService.LogInfo(ex.Message.ToString());
                    }
                }
                else
                {
                    errorMsg += "CSV File Empty." + ";";
                    Log4NetService.LogInfo("CSV File Empty.");
                }
                if (String.IsNullOrEmpty(errorMsg))
                    HealthCheck.Update(true, "UpdateDbFromEhsims", "EHSIMS");
                else
                    HealthCheck.Update(false, "UpdateDbFromEhsims", "EHSIMS Exception: " + errorMsg);
            }

            /// <summary>
            /// Tests the ehsims codes.
            /// </summary>
            public static void TestEhsimsCodes()
            {
                string myFile = "";

                try
                {
                    using (FileStream logFileStream = new FileStream(@"C:\Temp\ehsims.csv", FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                    {
                        StreamReader logFileReader = new StreamReader(logFileStream);
                        while (!logFileReader.EndOfStream)
                        {
                            string line = logFileReader.ReadLine();
                            myFile += line + "\n";
                        }
                        logFileReader.Close();
                        logFileStream.Close();
                    }
                }
                catch (Exception ex)
                {
                    Log4NetService.LogInfo(ex.Message);
                }

                int i = 0;
                if (!String.IsNullOrEmpty(myFile))
                {
                    Log4NetService.LogInfo("CSV File Ok.");
                    try
                    {
                        using (CsvReader csv = new CsvReader(new StringReader(myFile), true))
                        {
                            int fieldCount = csv.FieldCount;
                            string[] headers = csv.GetFieldHeaders();
                            Log4NetService.LogInfo("Found Headers: " + String.Join(",", headers));

                            while (csv.ReadNextRecord())
                            {
                                i++;

                                string locCode = csv["LOCATION_CODE"].ToString();
                                //if (locCode == "WAM" || locCode == "WAO") locCode = "";
                                //search residential table..
                                string companyName = csv["CONT_COMPANY_NAME"].ToString();
                                string _companyName = companyName.ToLower();

                                string companyCode = csv["CONT_COMPANY_CODE"].ToString();

                                string dateUpdated = csv["DATE_UPDATED"].ToString();
                                try
                                {
                                    CompaniesService cService = new CompaniesService();
                                    Companies c = cService.GetByCompanyName(_companyName);

                                    SitesService sService = new SitesService();
                                    TList<Sites> s = sService.GetByLocCode(locCode);

                                    if (s[0] != null)
                                    {
                                        if (c != null)
                                        {
                                            int CompanyId = c.CompanyId;
                                            int SiteId = s[0].SiteId;

                                            ResidentialService rService = new ResidentialService();
                                            Residential r = rService.GetByCompanyIdSiteId(CompanyId, SiteId);

                                            if (r == null)
                                            {
                                                Log4NetService.LogInfo(String.Format("{0},{1},{2},1 Residency Not Found,{3},{4}", locCode, companyName, companyCode, s[0].SiteAbbrev, dateUpdated));
                                            }
                                            else
                                            {
                                                Log4NetService.LogInfo(String.Format("{0},{1},{2},{3},{4},{5}", locCode, companyName, companyCode, c.CompanyName, s[0].SiteAbbrev, dateUpdated));
                                            }
                                        }
                                        else
                                        {
                                            Log4NetService.LogInfo(String.Format("{0},{1},{2},1 Company Not Found,{3},{4}", locCode, companyName, companyCode, s[0].SiteAbbrev, dateUpdated));
                                        }
                                    }
                                    else
                                    {
                                        Log4NetService.LogInfo(String.Format("{0},{1},{2},1 Site Not Found,{3},{4}", locCode, companyName, companyCode, s[0].SiteAbbrev, dateUpdated));
                                    }
                                }
                                catch (Exception)
                                {
                                    Log4NetService.LogInfo(String.Format("{0}, {1}, {2},1 Error:No Site,{3}", locCode, companyName, companyCode, dateUpdated));
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Log4NetService.LogInfo(ex.Message.ToString());
                    }
                }
            }
        }

        //need to add change for Item# 1 Sayani
        public class IHS
        {

            //Add By Bishwajit for Item#34


            #region Add By Bishwajit for Item#34

            public static void UpdateRNCActionOpenCLosed()
            {
                // Cindi Thornton 17/9/15 - scheduled tasks now run as service account, this isn't needed. Remove all impersonation related code
                //WindowsImpersonationContext impContext = null;
                //try
                //{
                //    impContext = NetworkSecurity.Impersonation.ImpersonateUser(
                //                            System.Configuration.ConfigurationManager.AppSettings["WAOCSM_domain"].ToString(),
                //                            System.Configuration.ConfigurationManager.AppSettings["WAOCSM_user"].ToString(),
                //                            System.Configuration.ConfigurationManager.AppSettings["WAOCSM_pass"].ToString());
                //}
                //catch (ApplicationException ex)
                //{
                //    Log4NetService.LogInfo(ex.Message);
                //}

                try
                {
                    //if (null != impContext)
                    //{
                    //    using (impContext)
                    //    {
                            Configuration configuration = new Configuration();

                            UpdateRNCActionOpenClosedAction(configuration.GetValue(ConfigList.IhsRnCaOpenClosedMetricsFilePath));

                            //UpdateRNCorrectiveActions_GetYearTable();
                            //HealthCheck.Update(true, "UpdateRNCActionOpenClosed");
                        //}
                    //}
                }
                catch (Exception ex)
                {
                    //Log4NetService.LogInfo(ex.Message.ToString() + "\nServer Configuration Account Error. Contact Administrator.");
                   // HealthCheck.Update(false, "UpdateRNCActionOpenClosed", "Exception: " + ex.Message);
                    throw;
                }
                finally
                {
                    //if (impContext != null) impContext.Undo();
                }
            }

            public static void UpdateRNCActionOpenClosedAction(string csvFilePath)
            {
                string errorMsg = "";

                string LastModifiedDatetime = string.Empty;

                string startHeader = "Subbusiness_Unit_Entity";
                //2.1 Read from the actual Header of the CSV file. (Ignore junk at start of CSV file)
                string myFile = "";
                bool start = false;

                try
                {
                    FileStream logFileStream = new FileStream(csvFilePath, FileMode.Open,
                                                                            FileAccess.Read,
                                                                            FileShare.ReadWrite);
                    StreamReader logFileReader = new StreamReader(logFileStream);

                    while (!logFileReader.EndOfStream)
                    {
                        string line = logFileReader.ReadLine();
                        if (line.Contains("Run on")) { LastModifiedDatetime = line; }
                        if (line.Contains(startHeader)) { start = true; };
                        if (start == true)
                        {
                            myFile += line + "\n";
                        }
                    }


                    logFileReader.Close();
                    logFileStream.Close();
                }
                catch (Exception ex)
                {
                    Log4NetService.LogInfo(ex.Message);
                }

                if (!String.IsNullOrEmpty(myFile))
                {
                    Log4NetService.LogInfo("CSV File Ok.");

                    LastModifiedDatetime = LastModifiedDatetime.Replace("Run on ", "");
                    LastModifiedDatetime = LastModifiedDatetime.Replace("at ", "");
                    LastModifiedDatetime = LastModifiedDatetime.Replace(".", ":");
                    LastModifiedDatetime = LastModifiedDatetime.Replace("-", "/");
                    LastModifiedDatetime = LastModifiedDatetime.Remove(0, 1);
                    try
                    {
                        ConnectionStringSettings conString = ConfigurationManager.ConnectionStrings["ALCOA_WAO_CSMWP_devConnectionString"];
                        string sqldb = conString.ConnectionString;
                        //using (SqlConnection cn = new SqlConnection(sqldb))
                        //{
                        //    cn.Open();
                        //    Log4NetService.LogInfo("..Deleting data from table: AU_CSMS_CA_AND_RN_COUNTS_TEMPTABLE" + "...");
                        //    SqlCommand cmd = new SqlCommand("TRUNCATE TABLE AU_CSMS_CA_AND_RN_COUNTS_TEMPTABLE", cn);
                        //    cmd.CommandTimeout = 120;
                        //    cmd.ExecuteNonQuery();
                        //    cn.Close();
                        //}


                        using (CsvReader csv = new CsvReader(new StringReader(myFile), true))
                        {
                            int fieldCount = csv.FieldCount;
                            string[] headers = csv.GetFieldHeaders();
                            Log4NetService.LogInfo("Found Headers: " + String.Join(",", headers));
                            //DT 3127 Changes                            
                            Log4NetService.LogInfo("Reading CSV file and Updating table: AU_CSMS_CA_AND_RN_COUNTS_TEMPTABLE started..");

                            int i = 0;
                            while (csv.ReadNextRecord())
                            {
                                try
                                {
                                    i++;
                                    string subbusiness_Unit_Entity = csv["Subbusiness_Unit_Entity"].ToString();
                                    string site_Entity = csv["Site_Entity"].ToString();
                                    string contracting_Company = csv["Contracting_Company"].ToString();
                                    string month_Beginning = csv["Month_Beginning"].ToString();
                                    string week_Beginning = csv["Week_Beginning"].ToString();
                                    string count_Type = csv["Count_Type"].ToString();
                                    string total_Count = csv["Total_Count"].ToString();

                                    if (String.IsNullOrEmpty(contracting_Company)
                                         || String.IsNullOrEmpty(count_Type)
                                        || String.IsNullOrEmpty(total_Count))
                                    {
                                        //|| String.IsNullOrEmpty(site_Entity) || String.IsNullOrEmpty(month_Beginning) || String.IsNullOrEmpty(week_Beginning) ||String.IsNullOrEmpty(subbusiness_Unit_Entity) 

                                        Log4NetService.LogInfo("\n" + "[" + i + "]\t" + "Skipping: " + subbusiness_Unit_Entity + ", " + site_Entity +
                                                            "," + contracting_Company + "," + month_Beginning +
                                                            "," + week_Beginning + "," + count_Type + "," + total_Count +
                                                            " as missing one or more inputs.");
                                    }
                                    else
                                    {
                                        try
                                        {
                                            conString = ConfigurationManager.ConnectionStrings["ALCOA_WAO_CSMWP_devConnectionString"];
                                            sqldb = conString.ConnectionString;
                                            using (SqlConnection cn = new SqlConnection(sqldb))
                                            {
                                                cn.Open();
                                                //DT 3127 Changes
                                                //Log4NetService.LogInfo("Updating table: AU_CSMS_CA_AND_RN_COUNTS_TEMPTABLE" + " for SUBBUSINESS_UNIT_ENTITY: " + subbusiness_Unit_Entity + ", CONTRACTING_COMPANY: " + contracting_Company);
                                                SqlCommand cmd = new SqlCommand("AU_CSMS_CA_AND_RN_COUNTS_TEMPTABLE_Insert", cn);
                                                cmd.CommandTimeout = 60000;
                                                cmd.CommandType = CommandType.StoredProcedure;
                                                cmd.Parameters.AddWithValue("@COUNT_TYPE", count_Type);
                                                if (subbusiness_Unit_Entity == null)
                                                {
                                                    cmd.Parameters.AddWithValue("@SUBBUSINESS_UNIT_ENTITY", DBNull.Value);
                                                }
                                                else
                                                {
                                                    cmd.Parameters.AddWithValue("@SUBBUSINESS_UNIT_ENTITY", subbusiness_Unit_Entity);
                                                }

                                                if (month_Beginning == null)
                                                {
                                                    cmd.Parameters.AddWithValue("@SITE_ENTITY", DBNull.Value);
                                                }
                                                else
                                                {
                                                    cmd.Parameters.AddWithValue("@SITE_ENTITY", site_Entity);
                                                }
                                                cmd.Parameters.AddWithValue("@CONTRACTING_COMPANY", contracting_Company);
                                                if (month_Beginning == null)
                                                {
                                                    cmd.Parameters.AddWithValue("@MONTH_BEGINNING", DBNull.Value);
                                                }
                                                else
                                                {
                                                    cmd.Parameters.AddWithValue("@MONTH_BEGINNING", Convert.ToDateTime(month_Beginning));
                                                }
                                                if (week_Beginning == null)
                                                {
                                                    cmd.Parameters.AddWithValue("@WEEK_BEGINNING", DBNull.Value);
                                                }
                                                else
                                                {
                                                    cmd.Parameters.AddWithValue("@WEEK_BEGINNING", Convert.ToDateTime(week_Beginning));
                                                }
                                                cmd.Parameters.AddWithValue("@TOTAL_COUNT", Convert.ToInt32(total_Count));
                                                cmd.Parameters.AddWithValue("@MODIFIED_DATE", Convert.ToDateTime(LastModifiedDatetime));

                                                cmd.ExecuteNonQuery();

                                                cn.Close();
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            //DT 3127 Changes
                                            Log4NetService.LogInfo("Error Updating table: AU_CSMS_CA_AND_RN_COUNTS_TEMPTABLE" + " for SUBBUSINESS_UNIT_ENTITY: " + subbusiness_Unit_Entity + ", CONTRACTING_COMPANY: " + contracting_Company);
                                            Log4NetService.LogInfo("-: " + ex.Message.ToString());                                            
                                            throw;
                                        }

                                    }

                                }
                                catch (Exception ex)
                                {
                                    bool logError = true;
                                    if (logError)
                                        errorMsg += ex.Message + ";";
                                    //DT 3127 Changes
                                    Log4NetService.LogInfo(errorMsg.ToString());
                                }
                            }
                            //DT 3127 Changes
                            Log4NetService.LogInfo("Finished Reading CSV file and Finished Updating table: AU_CSMS_CA_AND_RN_COUNTS_TEMPTABLE");
                        }
                    }
                    catch (Exception ex)
                    {
                        errorMsg += ex.Message + ";";
                        Log4NetService.LogInfo(ex.Message.ToString());
                    }
                }
                else
                {
                    errorMsg += "CSV File Empty." + ";";
                    Log4NetService.LogInfo("CSV File Empty.");
                }
            }

            //To insert data to staging table : RNCorrectiveActions_GetYear
            public static void UpdateRNCorrectiveActions_GetYearTable()
            {
                string errorMsg = "";
                try
                {
                    DataSet ds = new DataSet();
                    string sqldb = ConfigurationManager.ConnectionStrings["ALCOA_WAO_CSMWP_devConnectionString"].ConnectionString;

                    int lastYear = DateTime.Now.Year;
                    int FirstYear = 2011;

                    using (SqlConnection cnDel = new SqlConnection(sqldb))
                    {
                        cnDel.Open();
                        Log4NetService.LogInfo("..Deleting data from table: RNCorrectiveActions_GetYear" + "...");
                        SqlCommand cmdDel = new SqlCommand("truncate table RNCorrectiveActions_GetYear", cnDel);

                        cmdDel.ExecuteNonQuery();


                        cnDel.Close();
                    }

                    while (FirstYear <= lastYear)
                    {
                        try
                        {
                            for (int month = 1; month <= 13; month++)
                            {
                                using (SqlConnection cn = new SqlConnection(sqldb))
                                {
                                    cn.Open();
                                    Log4NetService.LogInfo("..Fetching data using Stored Procedure to a Dataset" + "...");
                                    SqlCommand cmd = new SqlCommand("_RNCorrectiveActions_GetYear2", cn);
                                    cmd.CommandTimeout = 60000;
                                    cmd.CommandType = CommandType.StoredProcedure;
                                    cmd.Parameters.AddWithValue("@Year", FirstYear);
                                    cmd.Parameters.AddWithValue("@Month", month);

                                    SqlDataAdapter sqlda = new SqlDataAdapter(cmd);
                                    ds.Clear();
                                    sqlda.Fill(ds);



                                    cn.Close();
                                }

                                using (SqlConnection cn = new SqlConnection(sqldb))
                                {
                                    if (ds != null)
                                    {
                                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                                        {
                                            try
                                            {
                                                Log4NetService.LogInfo("..Inserting  into  RNCorrectiveActions_GetYear table" + " for SiteId: " + ds.Tables[0].Rows[i]["SiteId"].ToString() + ", RegionId: " + ds.Tables[0].Rows[i]["RegionId"].ToString() + " , Count_Type: " + ds.Tables[0].Rows[i]["RegionId"].ToString());
                                                cn.Open();

                                                SqlCommand cmd = new SqlCommand("RNCorrectiveActions_GetYear_Insert", cn);


                                                cmd.CommandTimeout = 60000;
                                                cmd.CommandType = CommandType.StoredProcedure;

                                                #region Fields
                                                if (ds.Tables[0].Rows[i]["SiteId"].ToString() == "")
                                                {
                                                    cmd.Parameters.AddWithValue("@SiteId", DBNull.Value);
                                                }
                                                else
                                                {
                                                    cmd.Parameters.AddWithValue("@SiteId", Convert.ToInt32(ds.Tables[0].Rows[i]["SiteId"]));
                                                }

                                                if (ds.Tables[0].Rows[i]["RegionId"].ToString() == "")
                                                {
                                                    cmd.Parameters.AddWithValue("@RegionId", DBNull.Value);
                                                }
                                                else
                                                {
                                                    cmd.Parameters.AddWithValue("@RegionId", Convert.ToInt32(ds.Tables[0].Rows[i]["RegionId"]));
                                                }

                                                if (ds.Tables[0].Rows[i]["Count_Type"].ToString() == "")
                                                {
                                                    cmd.Parameters.AddWithValue("@Count_Type", DBNull.Value);
                                                }
                                                else
                                                {
                                                    cmd.Parameters.AddWithValue("@Count_Type", Convert.ToString(ds.Tables[0].Rows[i]["Count_Type"]));
                                                }

                                                if (ds.Tables[0].Rows[i]["Week1"].ToString() == "")
                                                {
                                                    cmd.Parameters.AddWithValue("@Week1", DBNull.Value);
                                                }
                                                else
                                                {
                                                    cmd.Parameters.AddWithValue("@Week1", Convert.ToInt32(ds.Tables[0].Rows[i]["Week1"]));
                                                }
                                                if (ds.Tables[0].Rows[i]["Week2"].ToString() == "")
                                                {
                                                    cmd.Parameters.AddWithValue("@Week2", DBNull.Value);
                                                }
                                                else
                                                {
                                                    cmd.Parameters.AddWithValue("@Week2", Convert.ToInt32(ds.Tables[0].Rows[i]["Week2"]));
                                                }
                                                if (ds.Tables[0].Rows[i]["Week3"].ToString() == "")
                                                {
                                                    cmd.Parameters.AddWithValue("@Week3", DBNull.Value);
                                                }
                                                else
                                                {
                                                    cmd.Parameters.AddWithValue("@Week3", Convert.ToInt32(ds.Tables[0].Rows[i]["Week3"]));
                                                }
                                                if (ds.Tables[0].Rows[i]["Week4"].ToString() == "")
                                                {
                                                    cmd.Parameters.AddWithValue("@Week4", DBNull.Value);
                                                }
                                                else
                                                {
                                                    cmd.Parameters.AddWithValue("@Week4", Convert.ToInt32(ds.Tables[0].Rows[i]["Week4"]));
                                                }
                                                if (ds.Tables[0].Rows[i]["Week5"].ToString() == "")
                                                {
                                                    cmd.Parameters.AddWithValue("@Week5", DBNull.Value);
                                                }
                                                else
                                                {
                                                    cmd.Parameters.AddWithValue("@Week5", Convert.ToInt32(ds.Tables[0].Rows[i]["Week5"]));
                                                }
                                                if (ds.Tables[0].Rows[i]["Week6"].ToString() == "")
                                                {
                                                    cmd.Parameters.AddWithValue("@Week6", DBNull.Value);
                                                }
                                                else
                                                {
                                                    cmd.Parameters.AddWithValue("@Week6", Convert.ToInt32(ds.Tables[0].Rows[i]["Week6"]));
                                                }

                                                if (ds.Tables[0].Rows[i]["Week7"].ToString() == "")
                                                {
                                                    cmd.Parameters.AddWithValue("@Week7", DBNull.Value);
                                                }
                                                else
                                                {
                                                    cmd.Parameters.AddWithValue("@Week7", Convert.ToInt32(ds.Tables[0].Rows[i]["Week7"]));
                                                }
                                                if (ds.Tables[0].Rows[i]["Week8"].ToString() == "")
                                                {
                                                    cmd.Parameters.AddWithValue("@Week8", DBNull.Value);
                                                }
                                                else
                                                {
                                                    cmd.Parameters.AddWithValue("@Week8", Convert.ToInt32(ds.Tables[0].Rows[i]["Week8"]));
                                                }
                                                if (ds.Tables[0].Rows[i]["Week9"].ToString() == "")
                                                {
                                                    cmd.Parameters.AddWithValue("@Week9", DBNull.Value);
                                                }
                                                else
                                                {
                                                    cmd.Parameters.AddWithValue("@Week9", Convert.ToInt32(ds.Tables[0].Rows[i]["Week9"]));
                                                }
                                                if (ds.Tables[0].Rows[i]["Week10"].ToString() == "")
                                                {
                                                    cmd.Parameters.AddWithValue("@Week10", DBNull.Value);
                                                }
                                                else
                                                {
                                                    cmd.Parameters.AddWithValue("@Week10", Convert.ToInt32(ds.Tables[0].Rows[i]["Week10"]));
                                                }
                                                if (ds.Tables[0].Rows[i]["Week11"].ToString() == "")
                                                {
                                                    cmd.Parameters.AddWithValue("@Week11", DBNull.Value);
                                                }
                                                else
                                                {
                                                    cmd.Parameters.AddWithValue("@Week11", Convert.ToInt32(ds.Tables[0].Rows[i]["Week11"]));
                                                }
                                                if (ds.Tables[0].Rows[i]["Week12"].ToString() == "")
                                                {
                                                    cmd.Parameters.AddWithValue("@Week12", DBNull.Value);
                                                }
                                                else
                                                {
                                                    cmd.Parameters.AddWithValue("@Week12", Convert.ToInt32(ds.Tables[0].Rows[i]["Week12"]));
                                                }
                                                if (ds.Tables[0].Rows[i]["Week13"].ToString() == "")
                                                {
                                                    cmd.Parameters.AddWithValue("@Week13", DBNull.Value);
                                                }
                                                else
                                                {
                                                    cmd.Parameters.AddWithValue("@Week13", Convert.ToInt32(ds.Tables[0].Rows[i]["Week13"]));
                                                }
                                                if (ds.Tables[0].Rows[i]["Week14"].ToString() == "")
                                                {
                                                    cmd.Parameters.AddWithValue("@Week14", DBNull.Value);
                                                }
                                                else
                                                {
                                                    cmd.Parameters.AddWithValue("@Week14", Convert.ToInt32(ds.Tables[0].Rows[i]["Week14"]));
                                                }
                                                if (ds.Tables[0].Rows[i]["Week15"].ToString() == "")
                                                {
                                                    cmd.Parameters.AddWithValue("@Week15", DBNull.Value);
                                                }
                                                else
                                                {
                                                    cmd.Parameters.AddWithValue("@Week15", Convert.ToInt32(ds.Tables[0].Rows[i]["Week15"]));
                                                }
                                                if (ds.Tables[0].Rows[i]["Week16"].ToString() == "")
                                                {
                                                    cmd.Parameters.AddWithValue("@Week16", DBNull.Value);
                                                }
                                                else
                                                {
                                                    cmd.Parameters.AddWithValue("@Week16", Convert.ToInt32(ds.Tables[0].Rows[i]["Week16"]));
                                                }
                                                if (ds.Tables[0].Rows[i]["Week17"].ToString() == "")
                                                {
                                                    cmd.Parameters.AddWithValue("@Week17", DBNull.Value);
                                                }
                                                else
                                                {
                                                    cmd.Parameters.AddWithValue("@Week17", Convert.ToInt32(ds.Tables[0].Rows[i]["Week17"]));
                                                }
                                                if (ds.Tables[0].Rows[i]["Week18"].ToString() == "")
                                                {
                                                    cmd.Parameters.AddWithValue("@Week18", DBNull.Value);
                                                }
                                                else
                                                {
                                                    cmd.Parameters.AddWithValue("@Week18", Convert.ToInt32(ds.Tables[0].Rows[i]["Week18"]));
                                                }
                                                if (ds.Tables[0].Rows[i]["Week19"].ToString() == "")
                                                {
                                                    cmd.Parameters.AddWithValue("@Week19", DBNull.Value);
                                                }
                                                else
                                                {
                                                    cmd.Parameters.AddWithValue("@Week19", Convert.ToInt32(ds.Tables[0].Rows[i]["Week19"]));
                                                }
                                                if (ds.Tables[0].Rows[i]["Week20"].ToString() == "")
                                                {
                                                    cmd.Parameters.AddWithValue("@Week20", DBNull.Value);
                                                }
                                                else
                                                {
                                                    cmd.Parameters.AddWithValue("@Week20", Convert.ToInt32(ds.Tables[0].Rows[i]["Week20"]));
                                                }
                                                if (ds.Tables[0].Rows[i]["Week21"].ToString() == "")
                                                {
                                                    cmd.Parameters.AddWithValue("@Week21", DBNull.Value);
                                                }
                                                else
                                                {
                                                    cmd.Parameters.AddWithValue("@Week21", Convert.ToInt32(ds.Tables[0].Rows[i]["Week21"]));
                                                }
                                                if (ds.Tables[0].Rows[i]["Week22"].ToString() == "")
                                                {
                                                    cmd.Parameters.AddWithValue("@Week22", DBNull.Value);
                                                }
                                                else
                                                {
                                                    cmd.Parameters.AddWithValue("@Week22", Convert.ToInt32(ds.Tables[0].Rows[i]["Week22"]));
                                                }
                                                if (ds.Tables[0].Rows[i]["Week23"].ToString() == "")
                                                {
                                                    cmd.Parameters.AddWithValue("@Week23", DBNull.Value);
                                                }
                                                else
                                                {
                                                    cmd.Parameters.AddWithValue("@Week23", Convert.ToInt32(ds.Tables[0].Rows[i]["Week23"]));
                                                }
                                                if (ds.Tables[0].Rows[i]["Week24"].ToString() == "")
                                                {
                                                    cmd.Parameters.AddWithValue("@Week24", DBNull.Value);
                                                }
                                                else
                                                {
                                                    cmd.Parameters.AddWithValue("@Week24", Convert.ToInt32(ds.Tables[0].Rows[i]["Week24"]));
                                                }
                                                if (ds.Tables[0].Rows[i]["Week25"].ToString() == "")
                                                {
                                                    cmd.Parameters.AddWithValue("@Week25", DBNull.Value);
                                                }
                                                else
                                                {
                                                    cmd.Parameters.AddWithValue("@Week25", Convert.ToInt32(ds.Tables[0].Rows[i]["Week25"]));
                                                }
                                                if (ds.Tables[0].Rows[i]["Week26"].ToString() == "")
                                                {
                                                    cmd.Parameters.AddWithValue("@Week26", DBNull.Value);
                                                }
                                                else
                                                {
                                                    cmd.Parameters.AddWithValue("@Week26", Convert.ToInt32(ds.Tables[0].Rows[i]["Week26"]));
                                                }
                                                if (ds.Tables[0].Rows[i]["Week27"].ToString() == "")
                                                {
                                                    cmd.Parameters.AddWithValue("@Week27", DBNull.Value);
                                                }
                                                else
                                                {
                                                    cmd.Parameters.AddWithValue("@Week27", Convert.ToInt32(ds.Tables[0].Rows[i]["Week27"]));
                                                }
                                                if (ds.Tables[0].Rows[i]["Week28"].ToString() == "")
                                                {
                                                    cmd.Parameters.AddWithValue("@Week28", DBNull.Value);
                                                }
                                                else
                                                {
                                                    cmd.Parameters.AddWithValue("@Week28", Convert.ToInt32(ds.Tables[0].Rows[i]["Week28"]));
                                                }
                                                if (ds.Tables[0].Rows[i]["Week29"].ToString() == "")
                                                {
                                                    cmd.Parameters.AddWithValue("@Week29", DBNull.Value);
                                                }
                                                else
                                                {
                                                    cmd.Parameters.AddWithValue("@Week29", Convert.ToInt32(ds.Tables[0].Rows[i]["Week29"]));
                                                }
                                                if (ds.Tables[0].Rows[i]["Week30"].ToString() == "")
                                                {
                                                    cmd.Parameters.AddWithValue("@Week30", DBNull.Value);
                                                }
                                                else
                                                {
                                                    cmd.Parameters.AddWithValue("@Week30", Convert.ToInt32(ds.Tables[0].Rows[i]["Week30"]));
                                                }
                                                if (ds.Tables[0].Rows[i]["Week31"].ToString() == "")
                                                {
                                                    cmd.Parameters.AddWithValue("@Week31", DBNull.Value);
                                                }
                                                else
                                                {
                                                    cmd.Parameters.AddWithValue("@Week31", Convert.ToInt32(ds.Tables[0].Rows[i]["Week31"]));
                                                }
                                                if (ds.Tables[0].Rows[i]["Week32"].ToString() == "")
                                                {
                                                    cmd.Parameters.AddWithValue("@Week32", DBNull.Value);
                                                }
                                                else
                                                {
                                                    cmd.Parameters.AddWithValue("@Week32", Convert.ToInt32(ds.Tables[0].Rows[i]["Week32"]));
                                                }
                                                if (ds.Tables[0].Rows[i]["Week33"].ToString() == "")
                                                {
                                                    cmd.Parameters.AddWithValue("@Week33", DBNull.Value);
                                                }
                                                else
                                                {
                                                    cmd.Parameters.AddWithValue("@Week33", Convert.ToInt32(ds.Tables[0].Rows[i]["Week33"]));
                                                }

                                                if (ds.Tables[0].Rows[i]["Week34"].ToString() == "")
                                                {
                                                    cmd.Parameters.AddWithValue("@Week34", DBNull.Value);
                                                }
                                                else
                                                {
                                                    cmd.Parameters.AddWithValue("@Week34", Convert.ToInt32(ds.Tables[0].Rows[i]["Week34"]));
                                                }
                                                if (ds.Tables[0].Rows[i]["Week35"].ToString() == "")
                                                {
                                                    cmd.Parameters.AddWithValue("@Week35", DBNull.Value);
                                                }
                                                else
                                                {
                                                    cmd.Parameters.AddWithValue("@Week35", Convert.ToInt32(ds.Tables[0].Rows[i]["Week35"]));
                                                }
                                                if (ds.Tables[0].Rows[i]["Week36"].ToString() == "")
                                                {
                                                    cmd.Parameters.AddWithValue("@Week36", DBNull.Value);
                                                }
                                                else
                                                {
                                                    cmd.Parameters.AddWithValue("@Week36", Convert.ToInt32(ds.Tables[0].Rows[i]["Week36"]));
                                                }

                                                if (ds.Tables[0].Rows[i]["Week37"].ToString() == "")
                                                {
                                                    cmd.Parameters.AddWithValue("@Week37", DBNull.Value);
                                                }
                                                else
                                                {
                                                    cmd.Parameters.AddWithValue("@Week37", Convert.ToInt32(ds.Tables[0].Rows[i]["Week37"]));
                                                }
                                                if (ds.Tables[0].Rows[i]["Week38"].ToString() == "")
                                                {
                                                    cmd.Parameters.AddWithValue("@Week38", DBNull.Value);
                                                }
                                                else
                                                {
                                                    cmd.Parameters.AddWithValue("@Week38", Convert.ToInt32(ds.Tables[0].Rows[i]["Week38"]));
                                                }
                                                if (ds.Tables[0].Rows[i]["Week39"].ToString() == "")
                                                {
                                                    cmd.Parameters.AddWithValue("@Week39", DBNull.Value);
                                                }
                                                else
                                                {
                                                    cmd.Parameters.AddWithValue("@Week39", Convert.ToInt32(ds.Tables[0].Rows[i]["Week39"]));
                                                }

                                                if (ds.Tables[0].Rows[i]["Week40"].ToString() == "")
                                                {
                                                    cmd.Parameters.AddWithValue("@Week40", DBNull.Value);
                                                }
                                                else
                                                {
                                                    cmd.Parameters.AddWithValue("@Week40", Convert.ToInt32(ds.Tables[0].Rows[i]["Week40"]));
                                                }
                                                if (ds.Tables[0].Rows[i]["Week41"].ToString() == "")
                                                {
                                                    cmd.Parameters.AddWithValue("@Week41", DBNull.Value);
                                                }
                                                else
                                                {
                                                    cmd.Parameters.AddWithValue("@Week41", Convert.ToInt32(ds.Tables[0].Rows[i]["Week41"]));
                                                }
                                                if (ds.Tables[0].Rows[i]["Week42"].ToString() == "")
                                                {
                                                    cmd.Parameters.AddWithValue("@Week42", DBNull.Value);
                                                }
                                                else
                                                {
                                                    cmd.Parameters.AddWithValue("@Week42", Convert.ToInt32(ds.Tables[0].Rows[i]["Week42"]));
                                                }
                                                if (ds.Tables[0].Rows[i]["Week43"].ToString() == "")
                                                {
                                                    cmd.Parameters.AddWithValue("@Week43", DBNull.Value);
                                                }
                                                else
                                                {
                                                    cmd.Parameters.AddWithValue("@Week43", Convert.ToInt32(ds.Tables[0].Rows[i]["Week43"]));
                                                }
                                                if (ds.Tables[0].Rows[i]["Week44"].ToString() == "")
                                                {
                                                    cmd.Parameters.AddWithValue("@Week44", DBNull.Value);
                                                }
                                                else
                                                {
                                                    cmd.Parameters.AddWithValue("@Week44", Convert.ToInt32(ds.Tables[0].Rows[i]["Week44"]));
                                                }
                                                if (ds.Tables[0].Rows[i]["Week45"].ToString() == "")
                                                {
                                                    cmd.Parameters.AddWithValue("@Week45", DBNull.Value);
                                                }
                                                else
                                                {
                                                    cmd.Parameters.AddWithValue("@Week45", Convert.ToInt32(ds.Tables[0].Rows[i]["Week45"]));
                                                }
                                                if (ds.Tables[0].Rows[i]["Week46"].ToString() == "")
                                                {
                                                    cmd.Parameters.AddWithValue("@Week46", DBNull.Value);
                                                }
                                                else
                                                {
                                                    cmd.Parameters.AddWithValue("@Week46", Convert.ToInt32(ds.Tables[0].Rows[i]["Week46"]));
                                                }
                                                if (ds.Tables[0].Rows[i]["Week47"].ToString() == "")
                                                {
                                                    cmd.Parameters.AddWithValue("@Week47", DBNull.Value);
                                                }
                                                else
                                                {
                                                    cmd.Parameters.AddWithValue("@Week47", Convert.ToInt32(ds.Tables[0].Rows[i]["Week47"]));
                                                }
                                                if (ds.Tables[0].Rows[i]["Week48"].ToString() == "")
                                                {
                                                    cmd.Parameters.AddWithValue("@Week48", DBNull.Value);
                                                }
                                                else
                                                {
                                                    cmd.Parameters.AddWithValue("@Week48", Convert.ToInt32(ds.Tables[0].Rows[i]["Week48"]));
                                                }
                                                if (ds.Tables[0].Rows[i]["Week49"].ToString() == "")
                                                {
                                                    cmd.Parameters.AddWithValue("@Week49", DBNull.Value);
                                                }
                                                else
                                                {
                                                    cmd.Parameters.AddWithValue("@Week49", Convert.ToInt32(ds.Tables[0].Rows[i]["Week49"]));
                                                }
                                                if (ds.Tables[0].Rows[i]["Week50"].ToString() == "")
                                                {
                                                    cmd.Parameters.AddWithValue("@Week50", DBNull.Value);
                                                }
                                                else
                                                {
                                                    cmd.Parameters.AddWithValue("@Week50", Convert.ToInt32(ds.Tables[0].Rows[i]["Week50"]));
                                                }
                                                if (ds.Tables[0].Rows[i]["Week51"].ToString() == "")
                                                {
                                                    cmd.Parameters.AddWithValue("@Week51", DBNull.Value);
                                                }
                                                else
                                                {
                                                    cmd.Parameters.AddWithValue("@Week51", Convert.ToInt32(ds.Tables[0].Rows[i]["Week51"]));
                                                }

                                                if (ds.Tables[0].Rows[i]["Week52"].ToString() == "")
                                                {
                                                    cmd.Parameters.AddWithValue("@Week52", DBNull.Value);
                                                }
                                                else
                                                {
                                                    cmd.Parameters.AddWithValue("@Week52", Convert.ToInt32(ds.Tables[0].Rows[i]["Week52"]));
                                                }


                                                if (ds.Tables[0].Rows[i]["Total"].ToString() == "")
                                                {
                                                    cmd.Parameters.AddWithValue("@Total", DBNull.Value);
                                                }
                                                else
                                                {
                                                    cmd.Parameters.AddWithValue("@Total", Convert.ToInt32(ds.Tables[0].Rows[i]["Total"]));
                                                }
                                                if (ds.Tables[0].Rows[i]["Month"].ToString() == "")
                                                {
                                                    cmd.Parameters.AddWithValue("@Month", DBNull.Value);
                                                }
                                                else
                                                {
                                                    cmd.Parameters.AddWithValue("@Month", Convert.ToInt32(ds.Tables[0].Rows[i]["Month"]));
                                                }
                                                if (ds.Tables[0].Rows[i]["Year"].ToString() == "")
                                                {
                                                    cmd.Parameters.AddWithValue("@Year", DBNull.Value);
                                                }
                                                else
                                                {
                                                    cmd.Parameters.AddWithValue("@Year", Convert.ToInt32(ds.Tables[0].Rows[i]["Year"]));
                                                }
                                                #endregion

                                                cmd.ExecuteNonQuery();

                                                cn.Close();
                                            }
                                            catch (Exception ex)
                                            {
                                                errorMsg += ex.Message + ";";
                                                Log4NetService.LogInfo(ex.Message.ToString());

                                            }
                                        }
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            errorMsg += ex.Message + ";";
                            Log4NetService.LogInfo(ex.Message.ToString());

                        }
                        FirstYear++;
                    }
                }
                catch (Exception ex)
                {
                    errorMsg += ex.Message + ";";
                    Log4NetService.LogInfo(ex.Message.ToString());
                }

            }
            #endregion

            //End Add By Bishwajit for Item#34

            //Add By Bishwajit for Item#27
            public static void UpdateTraining_Management_Validity()
            {
                string errorMsg = string.Empty;
                string pollingDateTime = string.Empty;
                 //DateTime pollingDateTime=DateTime.Now;
                try
                {
                    DataSet ds = new DataSet();
                    string sqldb = ConfigurationManager.ConnectionStrings["ALCOA_WAO_CSMWP_devConnectionString"].ConnectionString;

                   
                    using (SqlConnection cnDel = new SqlConnection(sqldb))
                    {
                        cnDel.Open();
                        //Log4NetService.LogInfo("..Deleting data from table: Training_Management_Validity" + "...");
                        SqlCommand cmdDel = new SqlCommand("SELECT CONVERT(VARCHAR(30),PollingDateTime,120) from PollingTable where Description= 'Training_Management_Validity'", cnDel);
                        cmdDel.CommandTimeout = 120;
                        pollingDateTime = Convert.ToString(cmdDel.ExecuteScalar());


                        cnDel.Close();
                    }

                    using (SqlConnection cn1 = new SqlConnection(sqldb))
                    {
                        Log4NetService.LogInfo("..Selecting data from View: XXHR_HR_TO_CSMS_CWK_ENR_ADD_TRNG_MAP" + "...");

                        cn1.Open();

                        



                        string strQuery = "SELECT A.PERSON_ID, A.FULL_NAME,A.EMPL_ID, COURSE_NAME, " +
                                        " COURSE_END_DATE,MAX(EXPIRATION_DATE) EXPIRATION_DATE, B.CWK_VENDOR_NO, " +
                                        "  D.CompanyId,E.SiteId, D.CompanyName,E.SiteName,A.TRAINING_TITLE, A.LAST_UPDATE_DATE   " +

                                        " FROM dbo.[XXHR_HR_TO_CSMS_CWK_ENR_ADD_TRNG_MAP] A " +
                                        " LEFT OUTER JOIN (SELECT DISTINCT PERSON_ID,CWK_VENDOR_NO,Location_Code, " +
                                        " EFFECTIVE_START_DATE,EFFECTIVE_END_DATE  FROM HR.dbo.XXHR_CWK_HISTORY_V) B " +
                                        " ON A.PERSON_ID = B.PERSON_ID " +
                                        " Left Outer Join CompanyVendor C  " +
                                        " ON B.CWK_VENDOR_NO = C.Vendor_Number  " +
                                        " Left Outer Join Companies D " +
                                        " On C.Tax_Registration_Number=D.CompanyABN " +
                                        " Left Outer Join Sites E  " +
                                        " on B.Location_Code=E.SiteNameHr  " +
                                        " WHERE A.COURSE_END_DATE = (SELECT MAX(COURSE_END_DATE) " +
                                        " FROM dbo.[XXHR_HR_TO_CSMS_CWK_ENR_ADD_TRNG_MAP] " +
                                        " WHERE A.PERSON_ID = PERSON_ID AND A.COURSE_NAME = COURSE_NAME) " +
                                        " AND (A.COURSE_END_DATE BETWEEN B.EFFECTIVE_START_DATE AND B.EFFECTIVE_END_DATE)   " +
                                        " AND C.Vendor_Number Is Not Null " +
                                        " AND E.SiteId Is Not Null " +
                                        " AND D.CompanyId Is Not Null " +
                                        //" AND A.LAST_UPDATE_DATE >  Convert(datetime,'" + pollingDateTime + "',103) "+
                                        " AND A.LAST_UPDATE_DATE >  CAST('" + pollingDateTime + "' AS DATETIME) " +
                                        " GROUP BY A.PERSON_ID, A.FULL_NAME,A.EMPL_ID, COURSE_NAME, " +
                                       " COURSE_END_DATE, B.CWK_VENDOR_NO," +
                                        " D.CompanyId,E.SiteId, D.CompanyName,E.SiteName,A.TRAINING_TITLE,A.LAST_UPDATE_DATE ";



                        SqlCommand cmd1 = new SqlCommand(strQuery, cn1);
                        cmd1.CommandTimeout = 70000;
                        SqlDataAdapter sqlda = new SqlDataAdapter(cmd1);

                        sqlda.Fill(ds);


                        cn1.Close();
                    }

                    using (SqlConnection cn = new SqlConnection(sqldb))
                    {
                        if (ds != null)
                        {
                            Log4NetService.LogInfo("..Inserting data into Table: Training_Management_Validity" + "...");
                            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                            {
                                try
                                {
                                    //Log4NetService.LogInfo("..Inserting data into Table: Training_Management_Validity" + "...");
                                    cn.Open();

                                    SqlCommand cmd = new SqlCommand("Training_Management_Validity_Insert", cn);

                                    //SqlCommand cmd = new SqlCommand("Training_Management_ValidityAll_2", cn);
                                    cmd.CommandTimeout = 60000;
                                    cmd.CommandType = CommandType.StoredProcedure;

                                    if (ds.Tables[0].Rows[i]["PERSON_ID"].ToString() == "")
                                    {
                                        cmd.Parameters.AddWithValue("@PERSON_ID", DBNull.Value);
                                    }
                                    else
                                    {
                                        cmd.Parameters.AddWithValue("@PERSON_ID", Convert.ToInt32(ds.Tables[0].Rows[i]["PERSON_ID"]));
                                    }

                                    if (ds.Tables[0].Rows[i]["FULL_NAME"].ToString() == "")
                                    {
                                        cmd.Parameters.AddWithValue("@FULL_NAME", DBNull.Value);
                                    }
                                    else
                                    {
                                        cmd.Parameters.AddWithValue("@FULL_NAME", Convert.ToString(ds.Tables[0].Rows[i]["FULL_NAME"]));
                                    }

                                    if (ds.Tables[0].Rows[i]["EMPL_ID"].ToString() == "")
                                    {
                                        cmd.Parameters.AddWithValue("@EMPL_ID", DBNull.Value);
                                    }
                                    else
                                    {
                                        cmd.Parameters.AddWithValue("@EMPL_ID", Convert.ToString(ds.Tables[0].Rows[i]["EMPL_ID"]));
                                    }
                                    if (ds.Tables[0].Rows[i]["COURSE_NAME"].ToString() == "")
                                    {
                                        cmd.Parameters.AddWithValue("@COURSE_NAME", DBNull.Value);
                                    }
                                    else
                                    {
                                        cmd.Parameters.AddWithValue("@COURSE_NAME", Convert.ToString(ds.Tables[0].Rows[i]["COURSE_NAME"]));
                                    }

                                    if (ds.Tables[0].Rows[i]["COURSE_END_DATE"].ToString() == "")
                                    {
                                        cmd.Parameters.AddWithValue("@COURSE_END_DATE", DBNull.Value);
                                    }
                                    else
                                    {
                                        if ((Convert.ToDateTime(ds.Tables[0].Rows[i]["COURSE_END_DATE"]).Year > 1900))
                                        {
                                            cmd.Parameters.AddWithValue("@COURSE_END_DATE", Convert.ToDateTime(ds.Tables[0].Rows[i]["COURSE_END_DATE"]));
                                        }
                                        else
                                        {
                                            cmd.Parameters.AddWithValue("@COURSE_END_DATE", DBNull.Value);
                                        }
                                    }

                                    if (ds.Tables[0].Rows[i]["EXPIRATION_DATE"].ToString() == "")
                                    {
                                        cmd.Parameters.AddWithValue("@EXPIRATION_DATE", DBNull.Value);
                                    }
                                    else
                                    {
                                        if ((Convert.ToDateTime(ds.Tables[0].Rows[i]["EXPIRATION_DATE"]).Year > 1900))
                                        {
                                            cmd.Parameters.AddWithValue("@EXPIRATION_DATE", Convert.ToDateTime(ds.Tables[0].Rows[i]["EXPIRATION_DATE"]));
                                        }
                                        else
                                        {
                                            cmd.Parameters.AddWithValue("@EXPIRATION_DATE", DBNull.Value);
                                        }
                                    }

                                    if (ds.Tables[0].Rows[i]["CWK_VENDOR_NO"].ToString() == "")
                                    {
                                        cmd.Parameters.AddWithValue("@VENDOR_NO", DBNull.Value);
                                    }
                                    else
                                    {
                                        cmd.Parameters.AddWithValue("@VENDOR_NO", Convert.ToString(ds.Tables[0].Rows[i]["CWK_VENDOR_NO"]));
                                    }

                                    if (ds.Tables[0].Rows[i]["CompanyId"].ToString() == "")
                                    {
                                        cmd.Parameters.AddWithValue("@CompanyId", DBNull.Value);
                                    }
                                    else
                                    {
                                        cmd.Parameters.AddWithValue("@CompanyId", Convert.ToInt32(ds.Tables[0].Rows[i]["CompanyId"]));
                                    }

                                    if (ds.Tables[0].Rows[i]["SiteId"].ToString() == "")
                                    {
                                        cmd.Parameters.AddWithValue("@SiteId", DBNull.Value);
                                    }
                                    else
                                    {
                                        cmd.Parameters.AddWithValue("@SiteId", Convert.ToInt32(ds.Tables[0].Rows[i]["SiteId"]));
                                    }
                                    if (ds.Tables[0].Rows[i]["CompanyName"].ToString() == "")
                                    {
                                        cmd.Parameters.AddWithValue("@CompanyName", DBNull.Value);
                                    }
                                    else
                                    {
                                        cmd.Parameters.AddWithValue("@CompanyName", Convert.ToString(ds.Tables[0].Rows[i]["CompanyName"]));
                                    }
                                    if (ds.Tables[0].Rows[i]["SiteName"].ToString() == "")
                                    {
                                        cmd.Parameters.AddWithValue("@SiteName", DBNull.Value);
                                    }
                                    else
                                    {
                                        cmd.Parameters.AddWithValue("@SiteName", Convert.ToString(ds.Tables[0].Rows[i]["SiteName"]));
                                    }

                                    if (ds.Tables[0].Rows[i]["TRAINING_TITLE"].ToString() == "")
                                    {
                                        cmd.Parameters.AddWithValue("@TRAINING_TITLE", DBNull.Value);
                                    }
                                    else
                                    {
                                        cmd.Parameters.AddWithValue("@TRAINING_TITLE", Convert.ToString(ds.Tables[0].Rows[i]["TRAINING_TITLE"]));
                                    }
                                    if (ds.Tables[0].Rows[i]["LAST_UPDATE_DATE"].ToString() == "")
                                    {
                                        cmd.Parameters.AddWithValue("@LAST_UPDATE_DATE", DBNull.Value);
                                    }
                                    else
                                    {
                                        cmd.Parameters.AddWithValue("@LAST_UPDATE_DATE", Convert.ToDateTime(ds.Tables[0].Rows[i]["LAST_UPDATE_DATE"]));
                                    }



                                    cmd.ExecuteNonQuery();

                                    cn.Close();
                                }
                                catch (Exception ex)
                                {
                                    if (cn.State == ConnectionState.Open)
                                        cn.Close();
                                    errorMsg += ex.Message + ";";
                                    Log4NetService.LogInfo(ex.Message.ToString());

                                }
                            }
                        }
                    }
                    using (SqlConnection cnDel = new SqlConnection(sqldb))
                    {
                        cnDel.Open();
                        //Log4NetService.LogInfo("..Deleting data from table: Training_Management_Validity" + "...");
                        SqlCommand cmdDel = new SqlCommand("UPDATE PollingTable set PollingDateTime=getdate() where Description= 'Training_Management_Validity'", cnDel);
                        cmdDel.CommandTimeout = 120;
                        cmdDel.ExecuteNonQuery();
                        cnDel.Close();
                    }

                    //HealthCheck.Update(true, "UpdateTrainingManagementValidity");
                }
                catch (Exception ex)
                {
                    //errorMsg += ex.Message + ";";
                    //Log4NetService.LogInfo(ex.Message.ToString());
                    //HealthCheck.Update(false, "UpdateTrainingManagementValidity", "Exception: " + ex.Message);
                    throw;
                }

            }

            //End Add By Bishwajit for Item#27
            public static void UpdateDb(string csvFilePath)
            {
                ConnectionStringSettings conString = ConfigurationManager.ConnectionStrings["ALCOA_WAO_CSMWP_devConnectionString"];
                using (SqlConnection cn = new SqlConnection(conString.ConnectionString))
                {
                    cn.Open();
                    //SET Context_Info 0x55555;
                    SqlCommand cmd = new SqlCommand("UPDATE Kpi SET ipFATI = null, ipMTI = null, ipRDI = null, ipLti = null, ipIfe = null, ipRN = null, IsSystemEdit = 1 WHERE YEAR(kpiDateTime) >= 2011", cn);
                    cmd.ExecuteNonQuery();
                    cn.Close();
                }
                
                string errorMsg = "";

                string reset = "";
                string startHeader = "Location Code";
                //2.1 Read from the actual Header of the CSV file. (Ignore junk at start of CSV file)
                string myFile = "";
                bool start = false;

                int? CompanyId = null;
                int? SiteId = null;
                DateTime dt;

                try
                {
                    FileStream logFileStream = new FileStream(csvFilePath, FileMode.Open,
                                                                            FileAccess.Read,
                                                                            FileShare.ReadWrite);
                    StreamReader logFileReader = new StreamReader(logFileStream);

                    while (!logFileReader.EndOfStream)
                    {
                        string line = logFileReader.ReadLine();
                        if (line.Contains(startHeader)) { start = true; };
                        if (start == true)
                        {
                            myFile += line + "\n";
                        }
                    }


                    logFileReader.Close();
                    logFileStream.Close();
                }
                catch (Exception ex)
                {
                    Log4NetService.LogInfo(ex.Message);
                }


                if (!String.IsNullOrEmpty(myFile))
                {
                    Log4NetService.LogInfo("CSV File Ok.");
                    try
                    {
                        using (CsvReader csv = new CsvReader(new StringReader(myFile), true))
                        {
                            int fieldCount = csv.FieldCount;
                            string[] headers = csv.GetFieldHeaders();
                            Log4NetService.LogInfo("Found Headers: " + String.Join(",", headers));

                            int i = 0;
                            while (csv.ReadNextRecord())
                            {
                                try
                                {
                                    i++;
                                    string locCode = csv["Location Code"].ToString();
                                    string ABN = csv["Cont Company Code"].ToString();
                                    //string ABN = csv["ABN"].ToString();
                                    string monthBeginning = csv["Month Beginning"].ToString();
                                    

                                    if (String.IsNullOrEmpty(locCode) || String.IsNullOrEmpty(ABN) || String.IsNullOrEmpty(monthBeginning))
                                    {
                                       
                                        throw new Exception("\n" + "[" + i + "]\t" + "Skipping: " + ABN + ", " + locCode + ", " + monthBeginning + " as missing one or more inputs.");
                                    }

                                    if (locCode == "WA Ops Group [31280]")
                                    {
                                        throw new Exception("\n" + "[" + i + "]\t" + "Skipping: " + ABN + ", " + locCode + ", " + monthBeginning + " as WAO");
                                    }

                                    if (locCode.Contains("WA Mining")) locCode = "WA Mining (CE)";

                                    Log4NetService.LogInfo("\n" + "[" + i + "]\t" + "Checking: " + ABN + ", " + locCode + ", " + monthBeginning + " ...");
                                    SitesService sService = new SitesService();

                                    TList<Sites> listSites = sService.GetBySiteNameIhs(locCode);
                                    int index = 0;
                                    if (listSites.Count >= 1)
                                    {
                                        int _i = 0;
                                        foreach (Sites s in listSites)
                                            _i++;
                                    }
                                    else
                                    {
                                        throw new Exception("Combination of Company/Site not found in CSMS.");
                                    }

                                    SiteId = listSites[index].SiteId;

                                    string splitter = "/";
                                    if (monthBeginning.Contains("-")) splitter = "-";
                                    dt = (DateTime.ParseExact(monthBeginning, "dd" + splitter + "MMM" + splitter + "yyyy", System.Globalization.CultureInfo.InvariantCulture));

                                    if (dt.Year >= 2011) //>2011 is IHS!
                                    {
                                        if (listSites.Count == 0)
                                        {
                                            EhsimsExceptionsService eService = new EhsimsExceptionsService();
                                            EhsimsExceptions e = new EhsimsExceptions();
                                            e.ReportDateTime = DateTime.Now;
                                            e.CompanyId = CompanyId;
                                            e.SiteId = SiteId;
                                            e.KpiDateTime = dt;
                                            e.ErrorMsg = "Location Code - Match not found.";
                                            eService.Insert(e);
                                            throw new Exception("Location Code - Match not found.");
                                        }
                                        CompaniesService cService = new CompaniesService();
                                        Companies cns = cService.GetByCompanyAbn(ABN);

                                        // DataSet dsCEM = DataRepository.CompaniesEhsimsMapSitesEhsimsIhsListProvider.GetByContCompanyCodeSiteNameIhs(companyCode, locCode);

                                        //0 - EhsimsMapId
                                        //1 - CompanyId
                                        //2 - ContCompanyCode
                                        //3 - SiteId
                                        //4 - IHS Site Name
                                        //5 - EHSIMS Loc Code
                                        //6 - EHSIMS Loc Code - IHS Site Name
                                        //7 - Site Name - EHSIMS Loc Code - IHS Site Name
                                        //8 - Site Name
                                        //9 - Site Abbrev
                                        //10- Company Name
                                        bool found = false;
                                        if (cns != null)
                                        {
                                            CompanyId = Convert.ToInt32(cns.CompanyId);
                                            found = true;
                                        }
                                        //if(dsCEM.Tables[0].Rows.Count > 1)
                                        //{
                                        //    if (locCode == "WAO")
                                        //    {
                                        //        found = true;
                                        //    }
                                        //}

                                        if (!found)
                                        {
                                            EhsimsExceptionsService eService = new EhsimsExceptionsService();
                                            EhsimsExceptions e = new EhsimsExceptions();
                                            e.ReportDateTime = DateTime.Now;
                                            e.CompanyId = CompanyId;
                                            e.SiteId = SiteId;
                                            e.KpiDateTime = dt;
                                            string _errorMsg = "";
                                            if (cns == null)
                                            {
                                                e.ErrorMsg = "Company not found for the specified ABN in CSMS ";
                                                _errorMsg = e.ErrorMsg;
                                            }
                                            //if (dsCEM.Tables[0].Rows.Count > 1)
                                            //{
                                            //    e.ErrorMsg = "Company & Location Code - Duplicates Found.";
                                            //    _errorMsg = e.ErrorMsg;
                                            //}
                                            eService.Insert(e);

                                            throw new Exception(_errorMsg);
                                        }
                                        else
                                        {
                                            CompaniesService companiesService = new CompaniesService();
                                            Companies companies = companiesService.GetByCompanyId(Convert.ToInt32(CompanyId));
                                            Log4NetService.LogInfo("Found Company Match (" + companies.CompanyName + ")...");

                                            KpiService kService = new KpiService();
                                            Kpi k = kService.GetByCompanyIdSiteIdKpiDateTime(Convert.ToInt32(CompanyId), listSites[index].SiteId, dt);

                                            if (k == null)
                                            {
                                                Kpi k2 = new Kpi();
                                                k2.CompanyId = Convert.ToInt32(CompanyId);
                                                k2.SiteId = listSites[index].SiteId;
                                                k2.KpiDateTime = dt;
                                                k2.CreatedbyUserId = 0;
                                                k2.ModifiedbyUserId = 0;
                                                k2.DateAdded = DateTime.Now;
                                                k2.Datemodified = DateTime.Now;
                                                k2.KpiGeneral = 0;
                                                k2.KpiCalcinerCapital = 0;
                                                k2.KpiCalcinerExpense = 0;
                                                k2.KpiResidue = 0;
                                                k2.AheaRefineryWork = 0;
                                                k2.AheaResidueWork = 0;
                                                k2.AheaSmeltingWork = 0;
                                                k2.AheaTotalManHours = 0;
                                                k2.AheaPeakNopplSiteWeek = 0;
                                                k2.AheaAvgNopplSiteMonth = 0;
                                                k2.AheaNoEmpExcessMonth = 0;
                                                k2.MtOthers = "";
                                                k2.IsSystemEdit = true;

                                                // Safety Plan submitted for year
                                                FileDbFilters query = new FileDbFilters();
                                                query.Append(FileDbColumn.CompanyId, CompanyId.ToString());
                                                DateTime dt2 = k2.KpiDateTime;
                                                dt2.Year.ToString();
                                                query.Append(FileDbColumn.ModifiedDate, "%" + dt2.Year.ToString() + "%");

                                                int count = 0;
                                                TList<FileDb> list = DataRepository.FileDbProvider.GetPaged(query.ToString(), null, 0, 100, out count);

                                                if (count > 0)
                                                {
                                                    k2.SafetyPlansSubmitted = true;
                                                }
                                                else
                                                {
                                                    k2.SafetyPlansSubmitted = false;
                                                }

                                                kService.Insert(k2);
                                                k = kService.GetByCompanyIdSiteIdKpiDateTime(Convert.ToInt32(CompanyId), listSites[index].SiteId, dt);
                                                //Log4NetService.LogInfo("create kpi");
                                            }

                                            if (k != null)
                                            {
                                                Log4NetService.LogInfo("Found Kpi...");
                                                string countIncidents = csv["Count of Incidents"].ToString();
                                                string resetline = String.Format("{0}|{1}|{2}|{3}", ABN, locCode, monthBeginning, csv["Case Type Description"]);
                                                switch (csv["Case Type Description"])
                                                {
                                                    case "First Aid":
                                                        if (!reset.Contains(resetline)) { k.IpFati = 0; };
                                                        k.IpFati += NumberUtilities.Convert.parseStringToNullableInt(countIncidents);
                                                        Log4NetService.LogInfo(String.Format("First Aid: {0}... ", countIncidents));
                                                        break;
                                                    case "Medical Treatment":
                                                        if (!reset.Contains(resetline)) { k.IpMti = 0; };
                                                        k.IpMti += NumberUtilities.Convert.parseStringToNullableInt(countIncidents);
                                                        Log4NetService.LogInfo(String.Format("Medical Treatment: {0}... ", countIncidents));
                                                        break;
                                                    case "Restricted Work":
                                                        if (!reset.Contains(resetline)) { k.IpRdi = 0; };
                                                        k.IpRdi += NumberUtilities.Convert.parseStringToNullableInt(countIncidents);
                                                        Log4NetService.LogInfo(String.Format("Restricted Work: {0}... ", countIncidents));
                                                        break;
                                                    case "Lost Work Day":
                                                        if (!reset.Contains(resetline)) { k.IpLti = 0; };
                                                        k.IpLti += NumberUtilities.Convert.parseStringToNullableInt(countIncidents);
                                                        Log4NetService.LogInfo(String.Format("Lost Work Day: {0}... ", countIncidents));
                                                        break;
                                                    case "Injury Free Event":
                                                        if (!reset.Contains(resetline)) { k.IpIfe = 0; };
                                                        k.IpIfe += NumberUtilities.Convert.parseStringToNullableInt(countIncidents);
                                                        Log4NetService.LogInfo(String.Format("Injury Free Event: {0}... ", countIncidents));
                                                        break;

                                                    //Added by Sayani Sil for Task# 1
                                                    case "GPP Risk Notification":
                                                        if (!reset.Contains(resetline)) { k.IpRN = 0; };
                                                        k.IpRN += NumberUtilities.Convert.parseStringToNullableInt(countIncidents);
                                                        Log4NetService.LogInfo(String.Format("GPP Risk Notification: {0}... ", countIncidents));
                                                        break;

                                                    default:
                                                        //throw new Exception("Possible Case Type Desc not found for " + csv["Cont Company Code"] + " & " + csv["Location Code"]);
                                                        break;
                                                }
                                                reset += resetline + "--";
                                                k.EntityState = EntityState.Changed;
                                                kService.Save(k);
                                                Log4NetService.LogInfo("Updated. \n");
                                                Log4NetService.LogInfo("");
                                            }
                                            else
                                            {
                                                EhsimsExceptionsService eService = new EhsimsExceptionsService();
                                                EhsimsExceptions e = new EhsimsExceptions();
                                                e.ReportDateTime = DateTime.Now;
                                                e.CompanyId = CompanyId;
                                                e.SiteId = SiteId;
                                                e.KpiDateTime = dt;
                                                e.ErrorMsg = "KPI Not Found";
                                                eService.Insert(e);
                                                Log4NetService.LogInfo("Not Found Kpi...");
                                            }

                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    bool logError = true;
                                    if (ex.Message.Contains("missing"))
                                    {
                                        logError = false;
                                        
                                    }
                                    if (ex.Message.Contains("Skipping")) logError = false;
                                    if (ex.Message.Contains("Combination of Company/Site not found in CSMS.")) logError = false;
                                    if (ex.Message.Contains("Location Code - Match not found.")) logError = false;
                                    if (ex.Message.Contains("Company not found for the specified ABN in CSMS ")) logError = false;
                                    //if (ex.Message.Contains("Company & Location Code - Duplicates Found.")) logError = false;
                                    if (ex.Message.Contains("Not Found Kpi...")) logError = false;

                                    if (logError)
                                        errorMsg += ex.Message + ";";
                                    Log4NetService.LogInfo(ex.Message.ToString());
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        errorMsg += ex.Message + ";";
                        Log4NetService.LogInfo(ex.Message.ToString());
                    }
                }
                else
                {
                    errorMsg += "CSV File Empty." + ";";
                    Log4NetService.LogInfo("CSV File Empty.");
                }

                if (String.IsNullOrEmpty(errorMsg))
                    HealthCheck.Update(true, "UpdateDbFromIhs", "IHS");
                else
                    HealthCheck.Update(false, "UpdateDbFromIhs", "IHS Exception: " + errorMsg);
            }
        }

        public class All
        {
            public static void UpdateDb(string system)
            {
                /*
                    * Updates KPI Table in CSM Database with latest EHSIMS data.
                    * 
                    * ColumnName(CSM) = ColumnName(EHSIMS)
                    * --------------------------------------
                    * ipFATI = First Aid
                    * ipMTI = Medical Treatment
                    * ipRDI = Restricted Work
                    * ipLTI = Lost Work Day
                    * ipIFE = Injury Free Event
                    */




                // Cindi Thornton 17/9/15 - scheduled tasks now run as service account, this isn't needed. Remove all impersonation related code
                //WindowsImpersonationContext impContext = null;
                //try
                //{
                //    impContext = NetworkSecurity.Impersonation.ImpersonateUser(
                //                            System.Configuration.ConfigurationManager.AppSettings["WAOCSM_domain"].ToString(),
                //                            System.Configuration.ConfigurationManager.AppSettings["WAOCSM_user"].ToString(),
                //                            System.Configuration.ConfigurationManager.AppSettings["WAOCSM_pass"].ToString());
                //}
                //catch (ApplicationException ex)
                //{
                //    Log4NetService.LogInfo(ex.Message);
                //}
                
                try
                {
                    //if (null != impContext)
                    //{
                    //    using (impContext)
                    //    {
                            Configuration configuration = new Configuration();

                            if (system == "Ehsims")
                                Safety.EHSIMS.UpdateDb(configuration.GetValue(ConfigList.ContractorIncidentCountsFilePath));
                            else if (system == "Ihs")
                                Safety.IHS.UpdateDb(configuration.GetValue(ConfigList.AI_ContractorIncidentCountsFilePath));
                            
                        //}
                    //}
                }
                catch (Exception ex)
                {
                    Log4NetService.LogInfo(ex.Message.ToString() + "\nServer Configuration Account Error. Contact Administrator.");
                }
                finally
                {
                    //if (impContext != null) impContext.Undo();
                }
            }
        }
    }
}
