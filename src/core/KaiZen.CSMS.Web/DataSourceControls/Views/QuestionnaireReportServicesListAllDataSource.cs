﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.QuestionnaireReportServicesListAllProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[CLSCompliant(true)]
	[Designer(typeof(QuestionnaireReportServicesListAllDataSourceDesigner))]
	public class QuestionnaireReportServicesListAllDataSource : ReadOnlyDataSource<QuestionnaireReportServicesListAll>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportServicesListAllDataSource class.
		/// </summary>
		public QuestionnaireReportServicesListAllDataSource() : base(new QuestionnaireReportServicesListAllService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the QuestionnaireReportServicesListAllDataSourceView used by the QuestionnaireReportServicesListAllDataSource.
		/// </summary>
		protected QuestionnaireReportServicesListAllDataSourceView QuestionnaireReportServicesListAllView
		{
			get { return ( View as QuestionnaireReportServicesListAllDataSourceView ); }
		}
		
		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the QuestionnaireReportServicesListAllDataSourceView class that is to be
		/// used by the QuestionnaireReportServicesListAllDataSource.
		/// </summary>
		/// <returns>An instance of the QuestionnaireReportServicesListAllDataSourceView class.</returns>
		protected override BaseDataSourceView<QuestionnaireReportServicesListAll, Object> GetNewDataSourceView()
		{
			return new QuestionnaireReportServicesListAllDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the QuestionnaireReportServicesListAllDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class QuestionnaireReportServicesListAllDataSourceView : ReadOnlyDataSourceView<QuestionnaireReportServicesListAll>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportServicesListAllDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the QuestionnaireReportServicesListAllDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public QuestionnaireReportServicesListAllDataSourceView(QuestionnaireReportServicesListAllDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal QuestionnaireReportServicesListAllDataSource QuestionnaireReportServicesListAllOwner
		{
			get { return Owner as QuestionnaireReportServicesListAllDataSource; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal QuestionnaireReportServicesListAllService QuestionnaireReportServicesListAllProvider
		{
			get { return Provider as QuestionnaireReportServicesListAllService; }
		}

		#endregion Properties
		
		#region Methods
		
		#endregion Methods
	}

	#region QuestionnaireReportServicesListAllDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the QuestionnaireReportServicesListAllDataSource class.
	/// </summary>
	public class QuestionnaireReportServicesListAllDataSourceDesigner : ReadOnlyDataSourceDesigner<QuestionnaireReportServicesListAll>
	{
	}

	#endregion QuestionnaireReportServicesListAllDataSourceDesigner

	#region QuestionnaireReportServicesListAllFilter

	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireReportServicesListAll"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireReportServicesListAllFilter : SqlFilter<QuestionnaireReportServicesListAllColumn>
	{
	}

	#endregion QuestionnaireReportServicesListAllFilter

	#region QuestionnaireReportServicesListAllExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireReportServicesListAll"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireReportServicesListAllExpressionBuilder : SqlExpressionBuilder<QuestionnaireReportServicesListAllColumn>
	{
	}
	
	#endregion QuestionnaireReportServicesListAllExpressionBuilder		
}

