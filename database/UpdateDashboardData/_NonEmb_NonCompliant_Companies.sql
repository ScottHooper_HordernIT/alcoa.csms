USE [AuA_CSMS_Dev]
GO

/****** Object:  StoredProcedure [dbo].[_NonEmb_NonCompliant_Companies]    Script Date: 31/07/2015 1:02:31 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[_NonEmb_NonCompliant_Companies]                                        
(                                        
 @SiteId int,                 
 @Month int,                                        
 @Year int                                        
)                                        
AS                         
DECLARE @Companies VARCHAR(MAX), @SiteIdN INT                   
DECLARE @ErrorMsg VARCHAR(1000)            
--IF (@SiteId < 0)                                     
-- BEGIN                
--  SELECT @SiteIdN = [dbo].[GetSiteIdByComboRegionId](@SiteId)                
-- END                
--ELSE                
-- BEGIN                
--  SET @SiteIdN = @SiteId                
-- END           
      
SELECT @SiteIdN = [dbo].[GetSiteIdByComboRegionId](@SiteId)                
-------------------      
SET @Companies = ''    
             
BEGIN TRY               
                        
SELECT @Companies = CompaniesNotCompliant from AdHoc_Radar_Items  
where SiteId = @SiteIdN AND [Year] = @Year           
AND AdHoc_RadarId =26                   
                  
IF EXISTS(SELECT * FROM dbo.Dashboard_Companies_NonCompliant WHERE SiteId = @SiteId AND [Year] = @Year                         
 AND [Month] = @Month AND [ItemName] = 'NE_Number of Companies:')                        
 UPDATE [dbo].[Dashboard_Companies_NonCompliant]                        
 SET [NonCompliantCompanies] = @Companies                        
 WHERE [SiteId] = @SiteId                        
   AND [Month] = @Month                         
   AND [Year] = @Year                        
   AND [ItemName] = 'NE_Number of Companies:'                        
ELSE                        
 INSERT INTO [dbo].[Dashboard_Companies_NonCompliant]                        
           ([SiteId]                        
           ,[Month]                        
           ,[Year]                        
           ,[ItemName]                        
           ,[NonCompliantCompanies])                        
     VALUES                        
           (@SiteId                        
           ,@Month                       
           ,@Year                        
           ,'NE_Number of Companies:'                        
           ,@Companies)                   
                       
END TRY                
BEGIN CATCH                
 SET @ErrorMsg = ERROR_MESSAGE()                
END CATCH                 
-------------------------------               
      
SET @Companies = ''        
            
BEGIN TRY              
                        
 SELECT @Companies = CompaniesNotCompliant from AdHoc_Radar_Items  
where SiteId = @SiteIdN AND [Year] = @Year           
AND AdHoc_RadarId =27                    
                  
IF EXISTS(SELECT * FROM dbo.Dashboard_Companies_NonCompliant WHERE SiteId = @SiteId AND [Year] = @Year                         
 AND [Month] = @Month AND [ItemName] = 'NE_Contractor Services Audit')                        
 UPDATE [dbo].[Dashboard_Companies_NonCompliant]                        
 SET [NonCompliantCompanies] = @Companies                        
 WHERE [SiteId] = @SiteId                        
   AND [Month] = @Month                         
   AND [Year] = @Year                        
   AND [ItemName] = 'NE_Contractor Services Audit'                        
ELSE                        
 INSERT INTO [dbo].[Dashboard_Companies_NonCompliant]                        
           ([SiteId]                        
           ,[Month]                        
           ,[Year]                        
           ,[ItemName]                        
           ,[NonCompliantCompanies])                        
     VALUES                        
           (@SiteId                        
           ,@Month              
           ,@Year                        
           ,'NE_Contractor Services Audit'                        
           ,@Companies)                 
       
                       
END TRY                
BEGIN CATCH                
 SET @ErrorMsg = ERROR_MESSAGE()                
END CATCH                   
-------------------------------                
      
SET @Companies = ''      
              
BEGIN TRY               
                        
SELECT @Companies = CompaniesNotCompliant from AdHoc_Radar_Items  
where SiteId = @SiteIdN AND [Year] = @Year           
AND AdHoc_RadarId =29                   
                 
IF EXISTS(SELECT * FROM dbo.Dashboard_Companies_NonCompliant WHERE SiteId = @SiteId AND [Year] = @Year                         
 AND [Month] = @Month AND [ItemName] = 'NE_Workplace Safety Compliance')                        
 UPDATE [dbo].[Dashboard_Companies_NonCompliant]                        
 SET [NonCompliantCompanies] = @Companies                        
 WHERE [SiteId] = @SiteId                        
   AND [Month] = @Month                         
   AND [Year] = @Year                        
   AND [ItemName] = 'NE_Workplace Safety Compliance'                        
ELSE                        
 INSERT INTO [dbo].[Dashboard_Companies_NonCompliant]                        
           ([SiteId]                        
           ,[Month]                        
           ,[Year]                        
           ,[ItemName]                        
           ,[NonCompliantCompanies])                        
   VALUES                        
           (@SiteId                        
           ,@Month                       
           ,@Year                        
           ,'NE_Workplace Safety Compliance'                        
           ,@Companies)                 
                       
END TRY                
BEGIN CATCH                
 SET @ErrorMsg = ERROR_MESSAGE()                
END CATCH                  
-------------------------------                
      
SET @Companies = ''      
             
BEGIN TRY              
                 
 SELECT @Companies = CompaniesNotCompliant from AdHoc_Radar_Items  
where SiteId = @SiteIdN AND [Year] = @Year           
AND AdHoc_RadarId =30                    
               
IF EXISTS(SELECT * FROM dbo.Dashboard_Companies_NonCompliant WHERE SiteId = @SiteId AND [Year] = @Year                         
 AND [Month] = @Month AND [ItemName] = 'NE_Management Health Safety Work Contacts')                        
 UPDATE [dbo].[Dashboard_Companies_NonCompliant]                        
 SET [NonCompliantCompanies] = @Companies                        
 WHERE [SiteId] = @SiteId                        
   AND [Month] = @Month                         
   AND [Year] = @Year                        
   AND [ItemName] = 'NE_Management Health Safety Work Contacts'                        
ELSE                        
 INSERT INTO [dbo].[Dashboard_Companies_NonCompliant]                        
           ([SiteId]                        
           ,[Month]                        
           ,[Year]                        
           ,[ItemName]                        
           ,[NonCompliantCompanies])                        
     VALUES                        
           (@SiteId                        
           ,@Month                       
           ,@Year                        
           ,'NE_Management Health Safety Work Contacts'                        
           ,@Companies)                 
                
                       
END TRY                
BEGIN CATCH                
 SET @ErrorMsg = ERROR_MESSAGE()                
END CATCH                   
-------------------------------                
      
SET @Companies = ''       
             
BEGIN TRY              
                        
SELECT @Companies = CompaniesNotCompliant from AdHoc_Radar_Items  
where SiteId = @SiteIdN AND [Year] = @Year           
AND AdHoc_RadarId =31                   
                
IF EXISTS(SELECT * FROM dbo.Dashboard_Companies_NonCompliant WHERE SiteId = @SiteId AND [Year] = @Year                         
 AND [Month] = @Month AND [ItemName] = 'NE_Behavioural Observations')                        
 UPDATE [dbo].[Dashboard_Companies_NonCompliant]                        
 SET [NonCompliantCompanies] = @Companies                        
 WHERE [SiteId] = @SiteId                        
   AND [Month] = @Month                         
   AND [Year] = @Year             
   AND [ItemName] = 'NE_Behavioural Observations'                        
ELSE                        
 INSERT INTO [dbo].[Dashboard_Companies_NonCompliant]                        
           ([SiteId]                        
           ,[Month]                        
           ,[Year]                        
           ,[ItemName]                        
           ,[NonCompliantCompanies])                        
     VALUES                        
           (@SiteId                        
           ,@Month                       
           ,@Year                 
           ,'NE_Behavioural Observations'                        
           ,@Companies)                 
                
                       
END TRY                
BEGIN CATCH                
 SET @ErrorMsg = ERROR_MESSAGE()                
END CATCH                  
-------------------------------                
      
SET @Companies = ''       
                
BEGIN TRY              
             
SELECT @Companies = CompaniesNotCompliant from AdHoc_Radar_Items  
where SiteId = @SiteIdN AND [Year] = @Year           
AND AdHoc_RadarId =32                   
              
IF EXISTS(SELECT * FROM dbo.Dashboard_Companies_NonCompliant WHERE SiteId = @SiteId AND [Year] = @Year                         
 AND [Month] = @Month AND [ItemName] = 'NE_JSA Field Audit Verifications')                        
 UPDATE [dbo].[Dashboard_Companies_NonCompliant]                        
 SET [NonCompliantCompanies] = @Companies                        
 WHERE [SiteId] = @SiteId                        
   AND [Month] = @Month                         
   AND [Year] = @Year                        
   AND [ItemName] = 'NE_JSA Field Audit Verifications'                        
ELSE                        
 INSERT INTO [dbo].[Dashboard_Companies_NonCompliant]                        
           ([SiteId]                        
           ,[Month]                        
           ,[Year]                        
           ,[ItemName]                        
           ,[NonCompliantCompanies])                        
     VALUES                        
           (@SiteId                        
           ,@Month                       
           ,@Year                        
           ,'NE_JSA Field Audit Verifications'                        
     ,@Companies)                 
                       
END TRY                
BEGIN CATCH                
 SET @ErrorMsg = ERROR_MESSAGE()                
END CATCH                 
-------------------------------                
      
SET @Companies = ''      
              
BEGIN TRY                      
 SELECT @Companies = CompaniesNotCompliant from AdHoc_Radar_Items  
where SiteId = @SiteIdN AND [Year] = @Year           
AND AdHoc_RadarId =33                   
             
IF EXISTS(SELECT * FROM dbo.Dashboard_Companies_NonCompliant WHERE SiteId = @SiteId AND [Year] = @Year                         
 AND [Month] = @Month AND [ItemName] = 'NE_Toolbox Meetings')                        
 UPDATE [dbo].[Dashboard_Companies_NonCompliant]                        
 SET [NonCompliantCompanies] = @Companies                        
 WHERE [SiteId] = @SiteId                        
   AND [Month] = @Month                         
   AND [Year] = @Year                        
   AND [ItemName] = 'NE_Toolbox Meetings'                    ELSE                        
 INSERT INTO [dbo].[Dashboard_Companies_NonCompliant]                        
           ([SiteId]                        
           ,[Month]                        
           ,[Year]                        
           ,[ItemName]                        
           ,[NonCompliantCompanies])                        
     VALUES                        
           (@SiteId                        
           ,@Month                       
           ,@Year               
           ,'NE_Toolbox Meetings'                        
           ,@Companies)                 
                       
END TRY                
BEGIN CATCH                
 SET @ErrorMsg = ERROR_MESSAGE()                
END CATCH             
-------------------------------                
      
SET @Companies = ''       
            
BEGIN TRY                      
SELECT @Companies = CompaniesNotCompliant from AdHoc_Radar_Items  
where SiteId = @SiteIdN AND [Year] = @Year           
AND AdHoc_RadarId =34                    
               
IF EXISTS(SELECT * FROM dbo.Dashboard_Companies_NonCompliant WHERE SiteId = @SiteId AND [Year] = @Year                         
 AND [Month] = @Month AND [ItemName] = 'NE_Alcoa Weekly Contractors Meeting')                        
 UPDATE [dbo].[Dashboard_Companies_NonCompliant]                        
 SET [NonCompliantCompanies] = @Companies                        
 WHERE [SiteId] = @SiteId                        
   AND [Month] = @Month                         
   AND [Year] = @Year              
   AND [ItemName] = 'NE_Alcoa Weekly Contractors Meeting'                        
ELSE                        
 INSERT INTO [dbo].[Dashboard_Companies_NonCompliant]                        
           ([SiteId]                        
           ,[Month]                        
           ,[Year]                        
           ,[ItemName]                        
           ,[NonCompliantCompanies])                        
     VALUES                        
           (@SiteId                        
           ,@Month                       
           ,@Year                        
           ,'NE_Alcoa Weekly Contractors Meeting'                        
           ,@Companies)                 
                       
END TRY                
BEGIN CATCH                
 SET @ErrorMsg = ERROR_MESSAGE()                
END CATCH             
-------------------------------                
      
SET @Companies = ''     
            
BEGIN TRY                       
 SELECT @Companies = CompaniesNotCompliant from AdHoc_Radar_Items  
where SiteId = @SiteIdN AND [Year] = @Year           
AND AdHoc_RadarId =35                   
              
IF EXISTS(SELECT * FROM dbo.Dashboard_Companies_NonCompliant WHERE SiteId = @SiteId AND [Year] = @Year                         
 AND [Month] = @Month AND [ItemName] = 'NE_Alcoa Monthly Contractors Meeting')                        
 UPDATE [dbo].[Dashboard_Companies_NonCompliant]                        
 SET [NonCompliantCompanies] = @Companies                        
 WHERE [SiteId] = @SiteId                        
   AND [Month] = @Month                         
   AND [Year] = @Year                        
   AND [ItemName] = 'NE_Alcoa Monthly Contractors Meeting'           
ELSE                        
 INSERT INTO [dbo].[Dashboard_Companies_NonCompliant]                        
           ([SiteId]                        
           ,[Month]                        
           ,[Year]                        
           ,[ItemName]                        
           ,[NonCompliantCompanies])                        
     VALUES                        
           (@SiteId                        
           ,@Month                       
           ,@Year                        
           ,'NE_Alcoa Monthly Contractors Meeting'                        
           ,@Companies)                 
                       
END TRY                
BEGIN CATCH                
 SET @ErrorMsg = ERROR_MESSAGE()                
END CATCH             
-------------------------------                
      
SET @Companies = ''      
            
BEGIN TRY                    
SELECT @Companies = CompaniesNotCompliant from AdHoc_Radar_Items  
where SiteId = @SiteIdN AND [Year] = @Year           
AND AdHoc_RadarId =36                    
                
IF EXISTS(SELECT * FROM dbo.Dashboard_Companies_NonCompliant WHERE SiteId = @SiteId AND [Year] = @Year                         
 AND [Month] = @Month AND [ItemName] = 'NE_Mandated Training')                       
 UPDATE [dbo].[Dashboard_Companies_NonCompliant]                        
 SET [NonCompliantCompanies] = @Companies                        
 WHERE [SiteId] = @SiteId                        
   AND [Month] = @Month                         
   AND [Year] = @Year                        
   AND [ItemName] = 'NE_Mandated Training'                        
ELSE                      
 INSERT INTO [dbo].[Dashboard_Companies_NonCompliant]                        
           ([SiteId]                        
           ,[Month]                        
           ,[Year]                        
           ,[ItemName]                        
           ,[NonCompliantCompanies])                        
     VALUES                        
           (@SiteId                        
           ,@Month                       
           ,@Year                        
           ,'NE_Mandated Training'                        
           ,@Companies)             
                       
END TRY                
BEGIN CATCH                
 SET @ErrorMsg = ERROR_MESSAGE()                
END CATCH             
----------------------------------------

GO


