﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Linq.Expressions;
using System.Web.Mvc;
using System.Web.Routing;
using System.Text;
using System.Security.Principal;
using System.Web.Mvc.Html;
using Repo.CSMS.Common.Helpers;

namespace Repo.CSMS.Common.Mvc.HtmlHelpers
{
    public static class HtmlHelperExtensions
    {
        #region DisplayColumnNameFor

        /// <summary>
        /// Use this @Html.DisplayColumnNameFor(...) helper when using a PagedList model in your view
        /// Example:
        ///     @Html.DisplayColumnNameFor(Model.Cylinder, c => c.SERIAL_NO
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TClass"></typeparam>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="helper"></param>
        /// <param name="model"></param>
        /// <param name="expression"></param>
        /// <returns></returns>
        public static MvcHtmlString DisplayColumnNameFor<TModel, TClass, TProperty>(this HtmlHelper<TModel> helper, IEnumerable<TClass> model, Expression<Func<TClass, TProperty>> expression)
        {
            var name = ExpressionHelper.GetExpressionText(expression);
            name = helper.ViewContext.ViewData.TemplateInfo.GetFullHtmlFieldName(name);
            var metadata = ModelMetadataProviders.Current.GetMetadataForProperty(
                () => Activator.CreateInstance<TClass>(), typeof(TClass), name);

            return new MvcHtmlString(metadata.DisplayName);
        }

        #endregion

        #region AutocompleteFor

        ///// <summary>
        ///// Use this @Html.AutocompleteFor(...) function to provide autocomplete functionality
        ///// Example:
        /////     @Html.LabelFor(m=>m.SomeValue) 
        /////     @Html.AutocompleteFor(m=>m.SomeValue, "Autocomplete", "Home")
        ///// </summary>
        ///// <typeparam name="TModel"></typeparam>
        ///// <typeparam name="TProperty"></typeparam>
        ///// <param name="html"></param>
        ///// <param name="expression"></param>
        ///// <param name="actionName"></param>
        ///// <param name="controllerName"></param>
        ///// <returns></returns>
        //public static MvcHtmlString AutoCompleteFor<TModel, TProperty>(this HtmlHelper<TModel> html, Expression<Func<TModel, TProperty>> expression, string actionName, string controllerName)
        //{
        //    string autocompleteUrl = UrlHelper.GenerateUrl(null, actionName, controllerName,
        //                                                    null,
        //                                                    html.RouteCollection,
        //                                                    html.ViewContext.RequestContext,
        //                                                    includeImplicitMvcValues: true);
        //    return html.TextBoxFor(expression, new { data_autocomplete_url = autocompleteUrl });
        //}

        /// <summary>
        /// Use this @Html.AutocompleteFor(...) function to provide autocomplete functionality
        /// Example:
        ///     @Html.LabelFor(m=>m.SomeValue) 
        ///     @Html.AutocompleteFor(m=>m.SomeValue, "Autocomplete", "Home")
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="html"></param>
        /// <param name="expression"></param>
        /// <param name="actionName"></param>
        /// <param name="controllerName"></param>
        /// <param name="minLength">Minimum length before autocomplete begins</param> 
        /// <returns></returns>
        public static MvcHtmlString AutoCompleteFor<TModel, TProperty>(this HtmlHelper<TModel> html, Expression<Func<TModel, TProperty>> expression, string actionName, string controllerName, int minLength = 2)
        {
            string autocompleteUrl = UrlHelper.GenerateUrl(null, actionName, controllerName,
                                                            null,
                                                            html.RouteCollection,
                                                            html.ViewContext.RequestContext,
                                                            includeImplicitMvcValues: true);
            return html.TextBoxFor(expression, new { data_autocomplete_url = autocompleteUrl, data_min_length = minLength, autocomplete = "off" });
        }

        /// <summary>
        /// Use this @Html.AutocompleteFor(...) function to provide autocomplete functionality
        /// Example:
        ///     @Html.LabelFor(m=>m.SomeValue) 
        ///     @Html.AutocompleteFor(m=>m.SomeValue, "Autocomplete", "Home")
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="html"></param>
        /// <param name="expression"></param>
        /// <param name="actionName"></param>
        /// <param name="controllerName"></param>
        /// <param name="outputId">The id of the html markup to post results to</param> 
        /// <param name="minLength">Minimum length before autocomplete begins</param> 
        /// <returns></returns>
        public static MvcHtmlString AutoCompleteFor<TModel, TProperty>(this HtmlHelper<TModel> html, Expression<Func<TModel, TProperty>> expression, string actionName, string controllerName, string outputId, int minLength = 2)
        {
            string autocompleteUrl = UrlHelper.GenerateUrl(null, actionName, controllerName,
                                                            null,
                                                            html.RouteCollection,
                                                            html.ViewContext.RequestContext,
                                                            includeImplicitMvcValues: true);
            return html.TextBoxFor(expression, new { data_autocomplete_url = autocompleteUrl, data_min_length = minLength, data_output_id = outputId, autocomplete = "off" });
        }

        /// <summary>
        /// Use this @Html.AutocompleteFor(...) function to provide autocomplete functionality
        /// Example:
        ///     @Html.LabelFor(m=>m.SomeValue) 
        ///     @Html.AutocompleteFor(m=>m.SomeValue, "Autocomplete", "Home")
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="html"></param>
        /// <param name="expression"></param>
        /// <param name="actionName"></param>
        /// <param name="controllerName"></param>
        /// <param name="outputId">The id of the html markup to post results to</param> 
        /// <param name="minLength">Minimum length before autocomplete begins</param> 
        /// <param name="defaultValue">Default value for textbox</param> 
        /// <returns></returns>
        public static MvcHtmlString AutoCompleteFor<TModel, TProperty>(this HtmlHelper<TModel> html, Expression<Func<TModel, TProperty>> expression, string actionName, string controllerName, string outputId, int minLength = 2, string defaultValue = "")
        {
            string autocompleteUrl = UrlHelper.GenerateUrl(null, actionName, controllerName,
                                                            null,
                                                            html.RouteCollection,
                                                            html.ViewContext.RequestContext,
                                                            includeImplicitMvcValues: true);
            return html.TextBoxFor(expression, new { data_autocomplete_url = autocompleteUrl, data_min_length = minLength, data_output_id = outputId, autocomplete = "off", @Value = defaultValue });
        }

        #endregion

        #region ImageLink

        /// <summary>
        /// Use this @Html.ImageLink(...) function to provide a html image within an anchor
        /// Example:
        ///     @Html.ImageLink("Home", "Delete", "~/Content/Delete.png", "Delete Account") 
        /// </summary>
        /// <param name="helper"></param>
        /// <param name="controllerName"></param>
        /// <param name="actionName"></param>
        /// <param name="imageUrl"></param>
        /// <param name="alternateText"></param>
        /// <returns></returns>
        public static MvcHtmlString ImageLink(this HtmlHelper helper, string controllerName, string actionName, string imageUrl, string alternateText)
        {
            return ImageLink(helper, controllerName, actionName, imageUrl, alternateText, null, null, null);
        }

        /// <summary>
        /// Use this @Html.ImageLink(...) function to provide a html image within an anchor
        /// Example:
        ///     @Html.ImageLink("Home", "Delete", "~/Content/Delete.png", "Delete Account", new {AccountId=2}) 
        /// </summary>
        /// <param name="helper"></param>
        /// <param name="controllerName"></param>
        /// <param name="actionName"></param>
        /// <param name="imageUrl"></param>
        /// <param name="alternateText"></param>
        /// <param name="routeValues"></param>
        /// <returns></returns>
        public static MvcHtmlString ImageLink(this HtmlHelper helper, string controllerName, string actionName, string imageUrl, string alternateText, object routeValues)
        {
            return ImageLink(helper, controllerName, actionName, imageUrl, alternateText, routeValues, null, null);
        }

        /// <summary>
        /// Use this @Html.ImageLink(...) function to provide a html image within an anchor
        /// Example:
        ///     @Html.ImageLink("Home", "Delete", "~/Content/Delete.png", "Delete Account", new {AccountId=2}, null, new {border=0}) 
        /// </summary>
        /// <param name="helper"></param>
        /// <param name="controllerName"></param>
        /// <param name="actionName"></param>
        /// <param name="imageUrl"></param>
        /// <param name="alternateText"></param>
        /// <param name="routeValues"></param>
        /// <param name="linkHtmlAttributes"></param>
        /// <param name="imageHtmlAttributes"></param>
        /// <returns></returns>
        public static MvcHtmlString ImageLink(this HtmlHelper helper, string controllerName, string actionName, string imageUrl, string alternateText, object routeValues, object linkHtmlAttributes, object imageHtmlAttributes)
        {
            var urlHelper = new UrlHelper(helper.ViewContext.RequestContext);
            var url = urlHelper.Action(actionName, routeValues);
            if (!string.IsNullOrEmpty(controllerName))
                url = urlHelper.Action(actionName, controllerName, routeValues);

            // Create link 
            var linkTagBuilder = new TagBuilder("a");
            linkTagBuilder.MergeAttribute("href", url);
            linkTagBuilder.MergeAttributes(new RouteValueDictionary(linkHtmlAttributes));

            // Create image 
            var imageTagBuilder = new TagBuilder("img");
            imageTagBuilder.MergeAttribute("src", urlHelper.Content(imageUrl));
            imageTagBuilder.MergeAttribute("alt", urlHelper.Encode(alternateText));
            imageTagBuilder.MergeAttributes(new RouteValueDictionary(imageHtmlAttributes));

            // Add image to link 
            linkTagBuilder.InnerHtml = imageTagBuilder.ToString(TagRenderMode.SelfClosing);

            return MvcHtmlString.Create(linkTagBuilder.ToString());
        } 

        #endregion

        #region ImageActionLink

        /// <summary>
        /// Use this @Html.ImageActionLink(...) function to provide a html image within an anchor with leading or trailing text
        /// Example:
        ///     @Html.ImageActionLink("Home", "Delete", "~/Content/Delete.png", "Delete Account", new {AccountId=2}, null, new {border=0}) 
        /// </summary>
        /// <param name="helper"></param>
        /// <param name="controllerName"></param>
        /// <param name="actionName"></param>
        /// <param name="imageUrl"></param>
        /// <param name="alternateText"></param>
        /// <param name="routeValues"></param>
        /// <param name="linkHtmlAttributes"></param>
        /// <param name="imageHtmlAttributes"></param>
        /// <returns></returns>
        public static MvcHtmlString ImageActionLink(this HtmlHelper helper, string controllerName, string actionName, string imageUrl, string alternateText, string leadingText, string trailingText, object routeValues, object linkHtmlAttributes, object imageHtmlAttributes)
        {
            var urlHelper = new UrlHelper(helper.ViewContext.RequestContext);
            var url = urlHelper.Action(actionName, routeValues);
            if (!string.IsNullOrEmpty(controllerName))
                url = urlHelper.Action(actionName, controllerName, routeValues);

            // Create link 
            var linkTagBuilder = new TagBuilder("a");
            linkTagBuilder.MergeAttribute("href", url);
            linkTagBuilder.MergeAttributes(new RouteValueDictionary(linkHtmlAttributes));

            //Create leading text
            if (!string.IsNullOrEmpty(leadingText))
            {
                var textTagBuilder = new TagBuilder("label");
                //textTagBuilder.MergeAttribute("class", "leadingText");
                textTagBuilder.InnerHtml = leadingText;
                linkTagBuilder.InnerHtml += textTagBuilder.ToString();
            }

            // Create image 
            var imageTagBuilder = new TagBuilder("img");
            imageTagBuilder.MergeAttribute("src", urlHelper.Content(imageUrl));
            imageTagBuilder.MergeAttribute("alt", urlHelper.Encode(alternateText));
            imageTagBuilder.MergeAttributes(new RouteValueDictionary(imageHtmlAttributes));

            // Add image to link 
            linkTagBuilder.InnerHtml += imageTagBuilder.ToString(TagRenderMode.SelfClosing);

            //Create trailing text
            if (!string.IsNullOrEmpty(trailingText))
            {
                var textTagBuilder = new TagBuilder("span");
                //textTagBuilder.MergeAttribute("class", "trailingText");
                textTagBuilder.InnerHtml = trailingText;
                linkTagBuilder.InnerHtml += textTagBuilder.ToString();
            }

            return MvcHtmlString.Create(linkTagBuilder.ToString());
        } 

        #endregion

        #region ImageActionScriptLink

        /// <summary>
        /// Use this @Html.ImageActionScriptLink(...) function to provide a html image within an anchor with leading or trailing text
        /// Example:
        ///     @Html.ImageActionScriptLink("events_save('Hello', '#:url#')", "~/Content/Delete.png", "Delete Account", new {AccountId=2}, null, new {border=0}, "Home", "Delete") 
        /// </summary>
        /// <param name="helper"></param>
        /// <param name="imageUrl"></param>
        /// <param name="script"></param>
        /// <param name="alternateText"></param>
        /// <param name="routeValues"></param>
        /// <param name="linkHtmlAttributes"></param>
        /// <param name="imageHtmlAttributes"></param>
        /// <param name="controllerName"></param>
        /// <param name="actionName"></param>
        /// <returns></returns>
        public static MvcHtmlString ImageActionScriptLink(this HtmlHelper helper, string script, string imageUrl, string alternateText, string leadingText, string trailingText, object routeValues, object linkHtmlAttributes, object imageHtmlAttributes, string controllerName = "", string actionName = "")
        {
            var urlHelper = new UrlHelper(helper.ViewContext.RequestContext);
            
            if (script.Contains("#:url#"))
            {
                var url = urlHelper.Action(actionName, routeValues);
                if (!string.IsNullOrEmpty(controllerName))
                    url = urlHelper.Action(actionName, controllerName, routeValues);

                script = script.Replace("#:url#", url);
            }

            // Create link 
            var linkTagBuilder = new TagBuilder("a");
            linkTagBuilder.MergeAttribute("href", string.Format("javascript:{0}", script));
            linkTagBuilder.MergeAttributes(new RouteValueDictionary(linkHtmlAttributes));

            //Create leading text
            if (!string.IsNullOrEmpty(leadingText))
            {
                var textTagBuilder = new TagBuilder("label");
                //textTagBuilder.MergeAttribute("class", "leadingText");
                textTagBuilder.InnerHtml = leadingText;
                linkTagBuilder.InnerHtml += textTagBuilder.ToString();
            }

            // Create image 
            var imageTagBuilder = new TagBuilder("img");
            imageTagBuilder.MergeAttribute("src", urlHelper.Content(imageUrl));
            imageTagBuilder.MergeAttribute("alt", urlHelper.Encode(alternateText));
            imageTagBuilder.MergeAttributes(new RouteValueDictionary(imageHtmlAttributes));

            // Add image to link 
            linkTagBuilder.InnerHtml += imageTagBuilder.ToString(TagRenderMode.SelfClosing);

            //Create trailing text
            if (!string.IsNullOrEmpty(trailingText))
            {
                var textTagBuilder = new TagBuilder("span");
                //textTagBuilder.MergeAttribute("class", "trailingText");
                textTagBuilder.InnerHtml = trailingText;
                linkTagBuilder.InnerHtml += textTagBuilder.ToString();
            }

            return MvcHtmlString.Create(linkTagBuilder.ToString());
        }

        #endregion

        #region Hint

        public static MvcHtmlString ResolveUrl(this HtmlHelper htmlHelper, string url)
        {
            var urlHelper = new UrlHelper(htmlHelper.ViewContext.RequestContext);
            return MvcHtmlString.Create(urlHelper.Content(url));
        }

        public static MvcHtmlString Hint(this HtmlHelper helper, string value)
        {
            // Create tag builder
            var builder = new TagBuilder("img");

            // Add attributes
            builder.MergeAttribute("src", ResolveUrl(helper, "~/Images/ico-help.png").ToHtmlString());
            builder.MergeAttribute("alt", value);
            builder.MergeAttribute("title", value);

            // Render tag
            return MvcHtmlString.Create(builder.ToString());
        }

        #endregion

        #region CommonLabelFor

        public static MvcHtmlString CommonLabelFor<TModel, TValue>(this HtmlHelper<TModel> helper, Expression<Func<TModel, TValue>> expression, string hintText = "")
        {
            var result = new StringBuilder();

            if (!string.IsNullOrEmpty(hintText))
            {
                result.Append(helper.Hint(hintText).ToHtmlString());
            }

            result.Append(helper.LabelFor(expression));
            return MvcHtmlString.Create(result.ToString());
        }

        public static MvcHtmlString CommonLabelFor<TModel>(this HtmlHelper<TModel> helper, string expression, string text, string hintText = "")
        {
            var result = new StringBuilder();

            if (!string.IsNullOrEmpty(hintText))
            {
                result.Append(helper.Hint(hintText).ToHtmlString());
            }

            result.Append(helper.Label(expression, text));
            return MvcHtmlString.Create(result.ToString());
        }

        #endregion

        #region Evaluation

        public static MvcHtmlString If(this MvcHtmlString value, bool evaluation) 
        { 
            return evaluation ? value : MvcHtmlString.Empty; 
        } 

        #endregion

        #region ToClientTime

        //public static MvcHtmlString ToClientTime(this DateTime dt) 
        //{     
        //    // read the value from session     
        //    var timeOffSet = HttpContext.Current.Session["CSMS.timezoneoffset"];     
    
        //    if (timeOffSet != null)      
        //    {         
        //        var offset = int.Parse(timeOffSet.ToString());         
        //        dt = dt.AddMinutes(-1 * offset);           
        //        return MvcHtmlString.Create(dt.ToString());     
        //    }       
        //    // if there is no offset in session return the datetime in server timezone     
        //    return MvcHtmlString.Create(dt.ToLocalTime().ToString());
        //}

        //public static MvcHtmlString ToClientTime(this DateTime? dt)
        //{
        //    if (dt.HasValue)
        //    {
        //        // read the value from session     
        //        var timeOffSet = HttpContext.Current.Session["CSMS.timezoneoffset"];

        //        if (timeOffSet != null)
        //        {
        //            var offset = int.Parse(timeOffSet.ToString());
        //            dt = dt.Value.AddMinutes(-1 * offset);
        //            return MvcHtmlString.Create(dt.ToString());
        //        }
        //        // if there is no offset in session return the datetime in server timezone     
        //        return MvcHtmlString.Create(dt.Value.ToLocalTime().ToString());
        //    }
        //    else
        //        return MvcHtmlString.Create("");
        //}

        public static MvcHtmlString ToClientTime(this DateTime dt, string format = "")
        {
            return MvcHtmlString.Create(DateTimeHelper.ConvertToUserTime(dt, DateTimeKind.Utc).ToString(format));
        }

        public static MvcHtmlString ToClientTime(this DateTime? dt, string format = "")
        {
            if (dt.HasValue)
            {
                return MvcHtmlString.Create(DateTimeHelper.ConvertToUserTime(dt.Value, DateTimeKind.Utc).ToString(format));
            }
            else
                return MvcHtmlString.Create("");
        }

        #endregion

        #region Ellipsis

        public static MvcHtmlString Ellipses(this HtmlHelper helper, string text, int length)
        {
            if (!string.IsNullOrEmpty(text))
                return MvcHtmlString.Create(text.Length > length ? text.Substring(0, length) + "…" : text);
            return MvcHtmlString.Create("");
        }

        #endregion

        #region Users

        public static MvcHtmlString NameWithoutDomain(this WindowsIdentity identity)
        {
            return (MvcHtmlString.Create(Common.Mvc.Utils.NameWithoutDomain(identity.Name)));
        }

        #endregion

        #region For

        public static HtmlHelper<TModel> For<TModel>(this HtmlHelper helper) where TModel : class, new()
        { 
            return For<TModel>(helper.ViewContext, helper.ViewDataContainer.ViewData, helper.RouteCollection); 
        }     
        
        public static HtmlHelper<TModel> For<TModel>(this HtmlHelper helper, TModel model) 
        { 
            return For<TModel>(helper.ViewContext, helper.ViewDataContainer.ViewData, helper.RouteCollection, model); 
        }     

        public static HtmlHelper<TModel> For<TModel>(ViewContext viewContext, ViewDataDictionary viewData, RouteCollection routeCollection) where TModel : class, new() 
        { 
            TModel model = new TModel(); 
            return For<TModel>(viewContext, viewData, routeCollection, model); 
        }     

        public static HtmlHelper<TModel> For<TModel>(ViewContext viewContext, ViewDataDictionary viewData, RouteCollection routeCollection, TModel model) 
        { 
            var newViewData = new ViewDataDictionary(viewData) { Model = model }; 
            ViewContext newViewContext = new ViewContext(viewContext.Controller.ControllerContext, viewContext.View, newViewData, viewContext.TempData, viewContext.Writer); 
            var viewDataContainer = new ViewDataContainer(newViewContext.ViewData); 
            return new HtmlHelper<TModel>(newViewContext, viewDataContainer, routeCollection); 
        }

        private class ViewDataContainer : System.Web.Mvc.IViewDataContainer 
        { 
            public System.Web.Mvc.ViewDataDictionary ViewData { get; set; } 
            public ViewDataContainer(System.Web.Mvc.ViewDataDictionary viewData) { ViewData = viewData; } 
        }

        #endregion

        #region Validation Summary

        public static MvcHtmlString BootstrapValidationSummary(this HtmlHelper helper, bool excludePropertyErrors = false, string validationMessage = "")
        {
            string html = "";

            if (!helper.ViewData.ModelState.IsValid)
            {
                html = "<div class=\"alert alert-danger\">" + helper.ValidationSummary(excludePropertyErrors, validationMessage) + "</div>";
            }

            return MvcHtmlString.Create(html);
        }

        #endregion

        #region Miscellaneous

        public static MvcForm BeginFileForm(this HtmlHelper html)
        {
            return html.BeginForm(null, null, FormMethod.Post, new { enctype = "multipart/form-data" });
        }

        public static MvcHtmlString File(this HtmlHelper html, string name, bool multiple)
        {
            var tb = new TagBuilder("input");
            tb.Attributes.Add("type", "file");
            tb.Attributes.Add("name", name);
            tb.GenerateId(name);

            if (multiple)
                tb.Attributes.Add("multiple", "multiple");

            return MvcHtmlString.Create(tb.ToString(TagRenderMode.SelfClosing));
        }

        public static MvcHtmlString File(this HtmlHelper html, string name)
        {
            return html.File(name, false);
        }

        public static MvcHtmlString MultipleFileFor<TModel, TProperty>(this HtmlHelper<TModel> html,
               Expression<Func<TModel, TProperty>> expression)
        {
            string name = GetFullPropertyName(expression);
            return html.File(name, true);
        }

        public static MvcHtmlString FileFor<TModel, TProperty>(this HtmlHelper<TModel> html,
                      Expression<Func<TModel, TProperty>> expression)
        {
            string name = GetFullPropertyName(expression);
            return html.File(name);
        }

        public static MvcHtmlString ActionImage(this HtmlHelper html, string imageUrl, string action, string controller, object routeValues, object htmlAttributes)
        {
            var urlHelper = new UrlHelper(html.ViewContext.RequestContext);
            var link = new TagBuilder("a");
            link.Attributes.Add("href", urlHelper.Action(action, controller, routeValues));
            var img = new TagBuilder("img");
            img.Attributes.Add("src", imageUrl);
            img.Attributes.Add("alt", action);
            link.InnerHtml = img.ToString(TagRenderMode.SelfClosing);
            return MvcHtmlString.Create(link.ToString(TagRenderMode.Normal));
        }

        public static MvcHtmlString Email(this HtmlHelper html, string name)
        {
            return html.Email(name, string.Empty, null);
        }

        public static MvcHtmlString Email(this HtmlHelper html, string name, string value, object htmlAttributes)
        {
            var tb = new TagBuilder("input");
            tb.Attributes.Add("type", "email");
            tb.Attributes.Add("name", name);
            tb.Attributes.Add("value", value);
            tb.MergeAttributes(new RouteValueDictionary(htmlAttributes));
            tb.GenerateId(name);
            return MvcHtmlString.Create(tb.ToString(TagRenderMode.SelfClosing));
        }

        public static MvcHtmlString EmailFor<TModel, TProperty>(this HtmlHelper<TModel> html, Expression<Func<TModel, TProperty>> expression, object htmlAttributes)
        {
            var name = GetFullPropertyName(expression);
            var value = string.Empty;

            try
            {
                if (html.ViewContext.ViewData.Model != null)
                    value = expression.Compile()((TModel)html.ViewContext.ViewData.Model).ToString();
            }
            catch { }

            return html.Email(name, value, htmlAttributes);
        }

        static string GetFullPropertyName<T, TProperty>(Expression<Func<T, TProperty>> exp)
        {
            MemberExpression memberExp;

            if (!TryFindMemberExpression(exp.Body, out memberExp))
                return string.Empty;

            var memberNames = new Stack<string>();

            do
            {
                memberNames.Push(memberExp.Member.Name);
            }
            while (TryFindMemberExpression(memberExp.Expression, out memberExp));

            return string.Join(".", memberNames.ToArray());
        }

        static bool TryFindMemberExpression(Expression exp, out MemberExpression memberExp)
        {
            memberExp = exp as MemberExpression;

            if (memberExp != null)
                return true;

            if (IsConversion(exp) && exp is UnaryExpression)
            {
                memberExp = ((UnaryExpression)exp).Operand as MemberExpression;

                if (memberExp != null)
                    return true;
            }

            return false;
        }

        static bool IsConversion(Expression exp)
        {
            return (exp.NodeType == ExpressionType.Convert || exp.NodeType == ExpressionType.ConvertChecked);
        }

        #endregion
    }
}
