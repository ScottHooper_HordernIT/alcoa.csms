﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database.View
{
    #region Interface

    public interface ICompanySiteCategoryStandardRegionService
    {
        CompanySiteCategoryStandardRegion Get(Expression<Func<CompanySiteCategoryStandardRegion, bool>> where, List<Expression<Func<CompanySiteCategoryStandardRegion, object>>> includes);
        List<CompanySiteCategoryStandardRegion> GetMany(int? top, Expression<Func<CompanySiteCategoryStandardRegion, bool>> where, Func<IQueryable<CompanySiteCategoryStandardRegion>, IOrderedQueryable<CompanySiteCategoryStandardRegion>> orderBy, List<Expression<Func<CompanySiteCategoryStandardRegion, object>>> includes);
        List<CompanySiteCategoryStandardRegion> GetManyPaged(int pageNumber, int pageSize, Expression<Func<CompanySiteCategoryStandardRegion, bool>> where, Func<IQueryable<CompanySiteCategoryStandardRegion>, IOrderedQueryable<CompanySiteCategoryStandardRegion>> orderBy, List<Expression<Func<CompanySiteCategoryStandardRegion, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<CompanySiteCategoryStandardRegion, bool>> where, List<Expression<Func<CompanySiteCategoryStandardRegion, object>>> includes);
    }
    #endregion

    #region Interface Implementation

    public class CompanySiteCategoryStandardRegionService : ICompanySiteCategoryStandardRegionService
    {
        private readonly IRepository<CompanySiteCategoryStandardRegion> _repository;

        public CompanySiteCategoryStandardRegionService(IRepository<CompanySiteCategoryStandardRegion> repository)
        {
            this._repository = repository;
        }

        public CompanySiteCategoryStandardRegion Get(Expression<Func<CompanySiteCategoryStandardRegion, bool>> where, List<Expression<Func<CompanySiteCategoryStandardRegion, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<CompanySiteCategoryStandardRegion> GetMany(int? top, Expression<Func<CompanySiteCategoryStandardRegion, bool>> where, Func<IQueryable<CompanySiteCategoryStandardRegion>, IOrderedQueryable<CompanySiteCategoryStandardRegion>> orderBy, List<Expression<Func<CompanySiteCategoryStandardRegion, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<CompanySiteCategoryStandardRegion> GetManyPaged(int pageNumber, int pageSize, Expression<Func<CompanySiteCategoryStandardRegion, bool>> where, Func<IQueryable<CompanySiteCategoryStandardRegion>, IOrderedQueryable<CompanySiteCategoryStandardRegion>> orderBy, List<Expression<Func<CompanySiteCategoryStandardRegion, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<CompanySiteCategoryStandardRegion, bool>> where, List<Expression<Func<CompanySiteCategoryStandardRegion, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }
    }

    #endregion
}
