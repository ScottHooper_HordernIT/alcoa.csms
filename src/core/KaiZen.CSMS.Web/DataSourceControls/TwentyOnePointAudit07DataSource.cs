﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.TwentyOnePointAudit07Provider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(TwentyOnePointAudit07DataSourceDesigner))]
	public class TwentyOnePointAudit07DataSource : ProviderDataSource<TwentyOnePointAudit07, TwentyOnePointAudit07Key>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit07DataSource class.
		/// </summary>
		public TwentyOnePointAudit07DataSource() : base(new TwentyOnePointAudit07Service())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the TwentyOnePointAudit07DataSourceView used by the TwentyOnePointAudit07DataSource.
		/// </summary>
		protected TwentyOnePointAudit07DataSourceView TwentyOnePointAudit07View
		{
			get { return ( View as TwentyOnePointAudit07DataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the TwentyOnePointAudit07DataSource control invokes to retrieve data.
		/// </summary>
		public TwentyOnePointAudit07SelectMethod SelectMethod
		{
			get
			{
				TwentyOnePointAudit07SelectMethod selectMethod = TwentyOnePointAudit07SelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (TwentyOnePointAudit07SelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the TwentyOnePointAudit07DataSourceView class that is to be
		/// used by the TwentyOnePointAudit07DataSource.
		/// </summary>
		/// <returns>An instance of the TwentyOnePointAudit07DataSourceView class.</returns>
		protected override BaseDataSourceView<TwentyOnePointAudit07, TwentyOnePointAudit07Key> GetNewDataSourceView()
		{
			return new TwentyOnePointAudit07DataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the TwentyOnePointAudit07DataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class TwentyOnePointAudit07DataSourceView : ProviderDataSourceView<TwentyOnePointAudit07, TwentyOnePointAudit07Key>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit07DataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the TwentyOnePointAudit07DataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public TwentyOnePointAudit07DataSourceView(TwentyOnePointAudit07DataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal TwentyOnePointAudit07DataSource TwentyOnePointAudit07Owner
		{
			get { return Owner as TwentyOnePointAudit07DataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal TwentyOnePointAudit07SelectMethod SelectMethod
		{
			get { return TwentyOnePointAudit07Owner.SelectMethod; }
			set { TwentyOnePointAudit07Owner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal TwentyOnePointAudit07Service TwentyOnePointAudit07Provider
		{
			get { return Provider as TwentyOnePointAudit07Service; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<TwentyOnePointAudit07> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<TwentyOnePointAudit07> results = null;
			TwentyOnePointAudit07 item;
			count = 0;
			
			System.Int32 _id;

			switch ( SelectMethod )
			{
				case TwentyOnePointAudit07SelectMethod.Get:
					TwentyOnePointAudit07Key entityKey  = new TwentyOnePointAudit07Key();
					entityKey.Load(values);
					item = TwentyOnePointAudit07Provider.Get(entityKey);
					results = new TList<TwentyOnePointAudit07>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case TwentyOnePointAudit07SelectMethod.GetAll:
                    results = TwentyOnePointAudit07Provider.GetAll(StartIndex, PageSize, out count);
                    break;
				case TwentyOnePointAudit07SelectMethod.GetPaged:
					results = TwentyOnePointAudit07Provider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case TwentyOnePointAudit07SelectMethod.Find:
					if ( FilterParameters != null )
						results = TwentyOnePointAudit07Provider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = TwentyOnePointAudit07Provider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case TwentyOnePointAudit07SelectMethod.GetById:
					_id = ( values["Id"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["Id"], typeof(System.Int32)) : (int)0;
					item = TwentyOnePointAudit07Provider.GetById(_id);
					results = new TList<TwentyOnePointAudit07>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				// FK
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == TwentyOnePointAudit07SelectMethod.Get || SelectMethod == TwentyOnePointAudit07SelectMethod.GetById )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				TwentyOnePointAudit07 entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					TwentyOnePointAudit07Provider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<TwentyOnePointAudit07> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			TwentyOnePointAudit07Provider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region TwentyOnePointAudit07DataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the TwentyOnePointAudit07DataSource class.
	/// </summary>
	public class TwentyOnePointAudit07DataSourceDesigner : ProviderDataSourceDesigner<TwentyOnePointAudit07, TwentyOnePointAudit07Key>
	{
		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit07DataSourceDesigner class.
		/// </summary>
		public TwentyOnePointAudit07DataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public TwentyOnePointAudit07SelectMethod SelectMethod
		{
			get { return ((TwentyOnePointAudit07DataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new TwentyOnePointAudit07DataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region TwentyOnePointAudit07DataSourceActionList

	/// <summary>
	/// Supports the TwentyOnePointAudit07DataSourceDesigner class.
	/// </summary>
	internal class TwentyOnePointAudit07DataSourceActionList : DesignerActionList
	{
		private TwentyOnePointAudit07DataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit07DataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public TwentyOnePointAudit07DataSourceActionList(TwentyOnePointAudit07DataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public TwentyOnePointAudit07SelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion TwentyOnePointAudit07DataSourceActionList
	
	#endregion TwentyOnePointAudit07DataSourceDesigner
	
	#region TwentyOnePointAudit07SelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the TwentyOnePointAudit07DataSource.SelectMethod property.
	/// </summary>
	public enum TwentyOnePointAudit07SelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetById method.
		/// </summary>
		GetById
	}
	
	#endregion TwentyOnePointAudit07SelectMethod

	#region TwentyOnePointAudit07Filter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit07"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAudit07Filter : SqlFilter<TwentyOnePointAudit07Column>
	{
	}
	
	#endregion TwentyOnePointAudit07Filter

	#region TwentyOnePointAudit07ExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit07"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAudit07ExpressionBuilder : SqlExpressionBuilder<TwentyOnePointAudit07Column>
	{
	}
	
	#endregion TwentyOnePointAudit07ExpressionBuilder	

	#region TwentyOnePointAudit07Property
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;TwentyOnePointAudit07ChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit07"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAudit07Property : ChildEntityProperty<TwentyOnePointAudit07ChildEntityTypes>
	{
	}
	
	#endregion TwentyOnePointAudit07Property
}

