﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="EnumQuestionnaireRiskRatingProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class EnumQuestionnaireRiskRatingProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.EnumQuestionnaireRiskRating, KaiZen.CSMS.Entities.EnumQuestionnaireRiskRatingKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.EnumQuestionnaireRiskRatingKey key)
		{
			return Delete(transactionManager, key.EnumQuestionnaireRiskRatingId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_enumQuestionnaireRiskRatingId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _enumQuestionnaireRiskRatingId)
		{
			return Delete(null, _enumQuestionnaireRiskRatingId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_enumQuestionnaireRiskRatingId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _enumQuestionnaireRiskRatingId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.EnumQuestionnaireRiskRating Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.EnumQuestionnaireRiskRatingKey key, int start, int pageLength)
		{
			return GetByEnumQuestionnaireRiskRatingId(transactionManager, key.EnumQuestionnaireRiskRatingId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_EnumQuestionnaireRiskRating index.
		/// </summary>
		/// <param name="_enumQuestionnaireRiskRatingId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.EnumQuestionnaireRiskRating"/> class.</returns>
		public KaiZen.CSMS.Entities.EnumQuestionnaireRiskRating GetByEnumQuestionnaireRiskRatingId(System.Int32 _enumQuestionnaireRiskRatingId)
		{
			int count = -1;
			return GetByEnumQuestionnaireRiskRatingId(null,_enumQuestionnaireRiskRatingId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_EnumQuestionnaireRiskRating index.
		/// </summary>
		/// <param name="_enumQuestionnaireRiskRatingId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.EnumQuestionnaireRiskRating"/> class.</returns>
		public KaiZen.CSMS.Entities.EnumQuestionnaireRiskRating GetByEnumQuestionnaireRiskRatingId(System.Int32 _enumQuestionnaireRiskRatingId, int start, int pageLength)
		{
			int count = -1;
			return GetByEnumQuestionnaireRiskRatingId(null, _enumQuestionnaireRiskRatingId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_EnumQuestionnaireRiskRating index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_enumQuestionnaireRiskRatingId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.EnumQuestionnaireRiskRating"/> class.</returns>
		public KaiZen.CSMS.Entities.EnumQuestionnaireRiskRating GetByEnumQuestionnaireRiskRatingId(TransactionManager transactionManager, System.Int32 _enumQuestionnaireRiskRatingId)
		{
			int count = -1;
			return GetByEnumQuestionnaireRiskRatingId(transactionManager, _enumQuestionnaireRiskRatingId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_EnumQuestionnaireRiskRating index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_enumQuestionnaireRiskRatingId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.EnumQuestionnaireRiskRating"/> class.</returns>
		public KaiZen.CSMS.Entities.EnumQuestionnaireRiskRating GetByEnumQuestionnaireRiskRatingId(TransactionManager transactionManager, System.Int32 _enumQuestionnaireRiskRatingId, int start, int pageLength)
		{
			int count = -1;
			return GetByEnumQuestionnaireRiskRatingId(transactionManager, _enumQuestionnaireRiskRatingId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_EnumQuestionnaireRiskRating index.
		/// </summary>
		/// <param name="_enumQuestionnaireRiskRatingId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.EnumQuestionnaireRiskRating"/> class.</returns>
		public KaiZen.CSMS.Entities.EnumQuestionnaireRiskRating GetByEnumQuestionnaireRiskRatingId(System.Int32 _enumQuestionnaireRiskRatingId, int start, int pageLength, out int count)
		{
			return GetByEnumQuestionnaireRiskRatingId(null, _enumQuestionnaireRiskRatingId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_EnumQuestionnaireRiskRating index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_enumQuestionnaireRiskRatingId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.EnumQuestionnaireRiskRating"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.EnumQuestionnaireRiskRating GetByEnumQuestionnaireRiskRatingId(TransactionManager transactionManager, System.Int32 _enumQuestionnaireRiskRatingId, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key UK_EnumQuestionnaireRiskRating_Ordinal index.
		/// </summary>
		/// <param name="_ordinal"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.EnumQuestionnaireRiskRating"/> class.</returns>
		public KaiZen.CSMS.Entities.EnumQuestionnaireRiskRating GetByOrdinal(System.Int32 _ordinal)
		{
			int count = -1;
			return GetByOrdinal(null,_ordinal, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the UK_EnumQuestionnaireRiskRating_Ordinal index.
		/// </summary>
		/// <param name="_ordinal"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.EnumQuestionnaireRiskRating"/> class.</returns>
		public KaiZen.CSMS.Entities.EnumQuestionnaireRiskRating GetByOrdinal(System.Int32 _ordinal, int start, int pageLength)
		{
			int count = -1;
			return GetByOrdinal(null, _ordinal, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the UK_EnumQuestionnaireRiskRating_Ordinal index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_ordinal"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.EnumQuestionnaireRiskRating"/> class.</returns>
		public KaiZen.CSMS.Entities.EnumQuestionnaireRiskRating GetByOrdinal(TransactionManager transactionManager, System.Int32 _ordinal)
		{
			int count = -1;
			return GetByOrdinal(transactionManager, _ordinal, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the UK_EnumQuestionnaireRiskRating_Ordinal index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_ordinal"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.EnumQuestionnaireRiskRating"/> class.</returns>
		public KaiZen.CSMS.Entities.EnumQuestionnaireRiskRating GetByOrdinal(TransactionManager transactionManager, System.Int32 _ordinal, int start, int pageLength)
		{
			int count = -1;
			return GetByOrdinal(transactionManager, _ordinal, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the UK_EnumQuestionnaireRiskRating_Ordinal index.
		/// </summary>
		/// <param name="_ordinal"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.EnumQuestionnaireRiskRating"/> class.</returns>
		public KaiZen.CSMS.Entities.EnumQuestionnaireRiskRating GetByOrdinal(System.Int32 _ordinal, int start, int pageLength, out int count)
		{
			return GetByOrdinal(null, _ordinal, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the UK_EnumQuestionnaireRiskRating_Ordinal index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_ordinal"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.EnumQuestionnaireRiskRating"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.EnumQuestionnaireRiskRating GetByOrdinal(TransactionManager transactionManager, System.Int32 _ordinal, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;EnumQuestionnaireRiskRating&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;EnumQuestionnaireRiskRating&gt;"/></returns>
		public static TList<EnumQuestionnaireRiskRating> Fill(IDataReader reader, TList<EnumQuestionnaireRiskRating> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.EnumQuestionnaireRiskRating c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("EnumQuestionnaireRiskRating")
					.Append("|").Append((System.Int32)reader[((int)EnumQuestionnaireRiskRatingColumn.EnumQuestionnaireRiskRatingId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<EnumQuestionnaireRiskRating>(
					key.ToString(), // EntityTrackingKey
					"EnumQuestionnaireRiskRating",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.EnumQuestionnaireRiskRating();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.EnumQuestionnaireRiskRatingId = (System.Int32)reader[((int)EnumQuestionnaireRiskRatingColumn.EnumQuestionnaireRiskRatingId - 1)];
					c.Ordinal = (System.Int32)reader[((int)EnumQuestionnaireRiskRatingColumn.Ordinal - 1)];
					c.Rating = (System.String)reader[((int)EnumQuestionnaireRiskRatingColumn.Rating - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.EnumQuestionnaireRiskRating"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.EnumQuestionnaireRiskRating"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.EnumQuestionnaireRiskRating entity)
		{
			if (!reader.Read()) return;
			
			entity.EnumQuestionnaireRiskRatingId = (System.Int32)reader[((int)EnumQuestionnaireRiskRatingColumn.EnumQuestionnaireRiskRatingId - 1)];
			entity.Ordinal = (System.Int32)reader[((int)EnumQuestionnaireRiskRatingColumn.Ordinal - 1)];
			entity.Rating = (System.String)reader[((int)EnumQuestionnaireRiskRatingColumn.Rating - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.EnumQuestionnaireRiskRating"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.EnumQuestionnaireRiskRating"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.EnumQuestionnaireRiskRating entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.EnumQuestionnaireRiskRatingId = (System.Int32)dataRow["EnumQuestionnaireRiskRatingId"];
			entity.Ordinal = (System.Int32)dataRow["Ordinal"];
			entity.Rating = (System.String)dataRow["Rating"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.EnumQuestionnaireRiskRating"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.EnumQuestionnaireRiskRating Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.EnumQuestionnaireRiskRating entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.EnumQuestionnaireRiskRating object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.EnumQuestionnaireRiskRating instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.EnumQuestionnaireRiskRating Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.EnumQuestionnaireRiskRating entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region EnumQuestionnaireRiskRatingChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.EnumQuestionnaireRiskRating</c>
	///</summary>
	public enum EnumQuestionnaireRiskRatingChildEntityTypes
	{
	}
	
	#endregion EnumQuestionnaireRiskRatingChildEntityTypes
	
	#region EnumQuestionnaireRiskRatingFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EnumQuestionnaireRiskRatingColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EnumQuestionnaireRiskRating"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EnumQuestionnaireRiskRatingFilterBuilder : SqlFilterBuilder<EnumQuestionnaireRiskRatingColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EnumQuestionnaireRiskRatingFilterBuilder class.
		/// </summary>
		public EnumQuestionnaireRiskRatingFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the EnumQuestionnaireRiskRatingFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public EnumQuestionnaireRiskRatingFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the EnumQuestionnaireRiskRatingFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public EnumQuestionnaireRiskRatingFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion EnumQuestionnaireRiskRatingFilterBuilder
	
	#region EnumQuestionnaireRiskRatingParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EnumQuestionnaireRiskRatingColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EnumQuestionnaireRiskRating"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EnumQuestionnaireRiskRatingParameterBuilder : ParameterizedSqlFilterBuilder<EnumQuestionnaireRiskRatingColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EnumQuestionnaireRiskRatingParameterBuilder class.
		/// </summary>
		public EnumQuestionnaireRiskRatingParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the EnumQuestionnaireRiskRatingParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public EnumQuestionnaireRiskRatingParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the EnumQuestionnaireRiskRatingParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public EnumQuestionnaireRiskRatingParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion EnumQuestionnaireRiskRatingParameterBuilder
	
	#region EnumQuestionnaireRiskRatingSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EnumQuestionnaireRiskRatingColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EnumQuestionnaireRiskRating"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class EnumQuestionnaireRiskRatingSortBuilder : SqlSortBuilder<EnumQuestionnaireRiskRatingColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EnumQuestionnaireRiskRatingSqlSortBuilder class.
		/// </summary>
		public EnumQuestionnaireRiskRatingSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion EnumQuestionnaireRiskRatingSortBuilder
	
} // end namespace
