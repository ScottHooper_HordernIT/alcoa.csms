using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using KaiZen.CSMS.Encryption;

public partial class crypt : System.Web.UI.Page
{
    Auth auth = new Auth();
    protected void Page_Load(object sender, EventArgs e)
    {
        HyperLink1.Visible = false;
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        Label1.Text = Crypt.EncryptTripleDES(TextBox1.Text, TextBox3.Text);
        HyperLink1.Visible = true;
        HyperLink1.Text = "Click me to login as '" + TextBox1.Text + "'";
        //HyperLink1.NavigateUrl = "login.aspx?" + auth._AlcoaDirectHTTPParameter + "=" + Label1.Text;
        HyperLink1.NavigateUrl = "login.aspx?" + auth._AlcoaDirectHTTPParameter + "=" + HttpUtility.UrlEncode(Label1.Text);
    }
    protected void TextBox3_TextChanged(object sender, EventArgs e)
    {

    }
    protected void Button2_Click(object sender, EventArgs e)
    {
        Label2.Text = Crypt.DecryptTripleDES(TextBox2.Text, TextBox3.Text);
    }
    protected void Button3_Click(object sender, EventArgs e)
    {
        if (Button3.Text == "Show")
        {
            if (tbPassword.Text == "a1c0a")
            {
                Panel1.Visible = true;
                Button3.Text = "Hide";
            }
        }
        else
        {
            Panel1.Visible = false;
            Button3.Text = "Show";
        }

    }
}
