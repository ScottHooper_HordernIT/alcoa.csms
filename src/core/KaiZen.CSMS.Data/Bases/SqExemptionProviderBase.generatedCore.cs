﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="SqExemptionProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class SqExemptionProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.SqExemption, KaiZen.CSMS.Entities.SqExemptionKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.SqExemptionKey key)
		{
			return Delete(transactionManager, key.SqExemptionId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_sqExemptionId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _sqExemptionId)
		{
			return Delete(null, _sqExemptionId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_sqExemptionId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _sqExemptionId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_SqExemption_Companies key.
		///		FK_SqExemption_Companies Description: 
		/// </summary>
		/// <param name="_companyId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.SqExemption objects.</returns>
		public TList<SqExemption> GetByCompanyId(System.Int32 _companyId)
		{
			int count = -1;
			return GetByCompanyId(_companyId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_SqExemption_Companies key.
		///		FK_SqExemption_Companies Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.SqExemption objects.</returns>
		/// <remarks></remarks>
		public TList<SqExemption> GetByCompanyId(TransactionManager transactionManager, System.Int32 _companyId)
		{
			int count = -1;
			return GetByCompanyId(transactionManager, _companyId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_SqExemption_Companies key.
		///		FK_SqExemption_Companies Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.SqExemption objects.</returns>
		public TList<SqExemption> GetByCompanyId(TransactionManager transactionManager, System.Int32 _companyId, int start, int pageLength)
		{
			int count = -1;
			return GetByCompanyId(transactionManager, _companyId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_SqExemption_Companies key.
		///		fkSqExemptionCompanies Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_companyId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.SqExemption objects.</returns>
		public TList<SqExemption> GetByCompanyId(System.Int32 _companyId, int start, int pageLength)
		{
			int count =  -1;
			return GetByCompanyId(null, _companyId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_SqExemption_Companies key.
		///		fkSqExemptionCompanies Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_companyId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.SqExemption objects.</returns>
		public TList<SqExemption> GetByCompanyId(System.Int32 _companyId, int start, int pageLength,out int count)
		{
			return GetByCompanyId(null, _companyId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_SqExemption_Companies key.
		///		FK_SqExemption_Companies Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.SqExemption objects.</returns>
		public abstract TList<SqExemption> GetByCompanyId(TransactionManager transactionManager, System.Int32 _companyId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_SqExemption_Companies_Requesting key.
		///		FK_SqExemption_Companies_Requesting Description: 
		/// </summary>
		/// <param name="_requestedByCompanyId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.SqExemption objects.</returns>
		public TList<SqExemption> GetByRequestedByCompanyId(System.Int32 _requestedByCompanyId)
		{
			int count = -1;
			return GetByRequestedByCompanyId(_requestedByCompanyId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_SqExemption_Companies_Requesting key.
		///		FK_SqExemption_Companies_Requesting Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_requestedByCompanyId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.SqExemption objects.</returns>
		/// <remarks></remarks>
		public TList<SqExemption> GetByRequestedByCompanyId(TransactionManager transactionManager, System.Int32 _requestedByCompanyId)
		{
			int count = -1;
			return GetByRequestedByCompanyId(transactionManager, _requestedByCompanyId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_SqExemption_Companies_Requesting key.
		///		FK_SqExemption_Companies_Requesting Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_requestedByCompanyId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.SqExemption objects.</returns>
		public TList<SqExemption> GetByRequestedByCompanyId(TransactionManager transactionManager, System.Int32 _requestedByCompanyId, int start, int pageLength)
		{
			int count = -1;
			return GetByRequestedByCompanyId(transactionManager, _requestedByCompanyId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_SqExemption_Companies_Requesting key.
		///		fkSqExemptionCompaniesRequesting Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_requestedByCompanyId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.SqExemption objects.</returns>
		public TList<SqExemption> GetByRequestedByCompanyId(System.Int32 _requestedByCompanyId, int start, int pageLength)
		{
			int count =  -1;
			return GetByRequestedByCompanyId(null, _requestedByCompanyId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_SqExemption_Companies_Requesting key.
		///		fkSqExemptionCompaniesRequesting Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_requestedByCompanyId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.SqExemption objects.</returns>
		public TList<SqExemption> GetByRequestedByCompanyId(System.Int32 _requestedByCompanyId, int start, int pageLength,out int count)
		{
			return GetByRequestedByCompanyId(null, _requestedByCompanyId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_SqExemption_Companies_Requesting key.
		///		FK_SqExemption_Companies_Requesting Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_requestedByCompanyId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.SqExemption objects.</returns>
		public abstract TList<SqExemption> GetByRequestedByCompanyId(TransactionManager transactionManager, System.Int32 _requestedByCompanyId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_SqExemption_Sites key.
		///		FK_SqExemption_Sites Description: 
		/// </summary>
		/// <param name="_siteId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.SqExemption objects.</returns>
		public TList<SqExemption> GetBySiteId(System.Int32 _siteId)
		{
			int count = -1;
			return GetBySiteId(_siteId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_SqExemption_Sites key.
		///		FK_SqExemption_Sites Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_siteId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.SqExemption objects.</returns>
		/// <remarks></remarks>
		public TList<SqExemption> GetBySiteId(TransactionManager transactionManager, System.Int32 _siteId)
		{
			int count = -1;
			return GetBySiteId(transactionManager, _siteId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_SqExemption_Sites key.
		///		FK_SqExemption_Sites Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_siteId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.SqExemption objects.</returns>
		public TList<SqExemption> GetBySiteId(TransactionManager transactionManager, System.Int32 _siteId, int start, int pageLength)
		{
			int count = -1;
			return GetBySiteId(transactionManager, _siteId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_SqExemption_Sites key.
		///		fkSqExemptionSites Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_siteId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.SqExemption objects.</returns>
		public TList<SqExemption> GetBySiteId(System.Int32 _siteId, int start, int pageLength)
		{
			int count =  -1;
			return GetBySiteId(null, _siteId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_SqExemption_Sites key.
		///		fkSqExemptionSites Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_siteId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.SqExemption objects.</returns>
		public TList<SqExemption> GetBySiteId(System.Int32 _siteId, int start, int pageLength,out int count)
		{
			return GetBySiteId(null, _siteId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_SqExemption_Sites key.
		///		FK_SqExemption_Sites Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_siteId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.SqExemption objects.</returns>
		public abstract TList<SqExemption> GetBySiteId(TransactionManager transactionManager, System.Int32 _siteId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_SqExemption_CompanyStatus2 key.
		///		FK_SqExemption_CompanyStatus2 Description: 
		/// </summary>
		/// <param name="_companyStatus2Id"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.SqExemption objects.</returns>
		public TList<SqExemption> GetByCompanyStatus2Id(System.Int32 _companyStatus2Id)
		{
			int count = -1;
			return GetByCompanyStatus2Id(_companyStatus2Id, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_SqExemption_CompanyStatus2 key.
		///		FK_SqExemption_CompanyStatus2 Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyStatus2Id"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.SqExemption objects.</returns>
		/// <remarks></remarks>
		public TList<SqExemption> GetByCompanyStatus2Id(TransactionManager transactionManager, System.Int32 _companyStatus2Id)
		{
			int count = -1;
			return GetByCompanyStatus2Id(transactionManager, _companyStatus2Id, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_SqExemption_CompanyStatus2 key.
		///		FK_SqExemption_CompanyStatus2 Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyStatus2Id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.SqExemption objects.</returns>
		public TList<SqExemption> GetByCompanyStatus2Id(TransactionManager transactionManager, System.Int32 _companyStatus2Id, int start, int pageLength)
		{
			int count = -1;
			return GetByCompanyStatus2Id(transactionManager, _companyStatus2Id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_SqExemption_CompanyStatus2 key.
		///		fkSqExemptionCompanyStatus2 Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_companyStatus2Id"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.SqExemption objects.</returns>
		public TList<SqExemption> GetByCompanyStatus2Id(System.Int32 _companyStatus2Id, int start, int pageLength)
		{
			int count =  -1;
			return GetByCompanyStatus2Id(null, _companyStatus2Id, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_SqExemption_CompanyStatus2 key.
		///		fkSqExemptionCompanyStatus2 Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_companyStatus2Id"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.SqExemption objects.</returns>
		public TList<SqExemption> GetByCompanyStatus2Id(System.Int32 _companyStatus2Id, int start, int pageLength,out int count)
		{
			return GetByCompanyStatus2Id(null, _companyStatus2Id, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_SqExemption_CompanyStatus2 key.
		///		FK_SqExemption_CompanyStatus2 Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyStatus2Id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.SqExemption objects.</returns>
		public abstract TList<SqExemption> GetByCompanyStatus2Id(TransactionManager transactionManager, System.Int32 _companyStatus2Id, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_SqExemption_Users key.
		///		FK_SqExemption_Users Description: 
		/// </summary>
		/// <param name="_modifiedByUserId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.SqExemption objects.</returns>
		public TList<SqExemption> GetByModifiedByUserId(System.Int32 _modifiedByUserId)
		{
			int count = -1;
			return GetByModifiedByUserId(_modifiedByUserId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_SqExemption_Users key.
		///		FK_SqExemption_Users Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_modifiedByUserId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.SqExemption objects.</returns>
		/// <remarks></remarks>
		public TList<SqExemption> GetByModifiedByUserId(TransactionManager transactionManager, System.Int32 _modifiedByUserId)
		{
			int count = -1;
			return GetByModifiedByUserId(transactionManager, _modifiedByUserId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_SqExemption_Users key.
		///		FK_SqExemption_Users Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_modifiedByUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.SqExemption objects.</returns>
		public TList<SqExemption> GetByModifiedByUserId(TransactionManager transactionManager, System.Int32 _modifiedByUserId, int start, int pageLength)
		{
			int count = -1;
			return GetByModifiedByUserId(transactionManager, _modifiedByUserId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_SqExemption_Users key.
		///		fkSqExemptionUsers Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_modifiedByUserId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.SqExemption objects.</returns>
		public TList<SqExemption> GetByModifiedByUserId(System.Int32 _modifiedByUserId, int start, int pageLength)
		{
			int count =  -1;
			return GetByModifiedByUserId(null, _modifiedByUserId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_SqExemption_Users key.
		///		fkSqExemptionUsers Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_modifiedByUserId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.SqExemption objects.</returns>
		public TList<SqExemption> GetByModifiedByUserId(System.Int32 _modifiedByUserId, int start, int pageLength,out int count)
		{
			return GetByModifiedByUserId(null, _modifiedByUserId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_SqExemption_Users key.
		///		FK_SqExemption_Users Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_modifiedByUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.SqExemption objects.</returns>
		public abstract TList<SqExemption> GetByModifiedByUserId(TransactionManager transactionManager, System.Int32 _modifiedByUserId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_SqExemption_FileVault key.
		///		FK_SqExemption_FileVault Description: 
		/// </summary>
		/// <param name="_fileVaultId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.SqExemption objects.</returns>
		public TList<SqExemption> GetByFileVaultId(System.Int32 _fileVaultId)
		{
			int count = -1;
			return GetByFileVaultId(_fileVaultId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_SqExemption_FileVault key.
		///		FK_SqExemption_FileVault Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_fileVaultId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.SqExemption objects.</returns>
		/// <remarks></remarks>
		public TList<SqExemption> GetByFileVaultId(TransactionManager transactionManager, System.Int32 _fileVaultId)
		{
			int count = -1;
			return GetByFileVaultId(transactionManager, _fileVaultId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_SqExemption_FileVault key.
		///		FK_SqExemption_FileVault Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_fileVaultId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.SqExemption objects.</returns>
		public TList<SqExemption> GetByFileVaultId(TransactionManager transactionManager, System.Int32 _fileVaultId, int start, int pageLength)
		{
			int count = -1;
			return GetByFileVaultId(transactionManager, _fileVaultId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_SqExemption_FileVault key.
		///		fkSqExemptionFileVault Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_fileVaultId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.SqExemption objects.</returns>
		public TList<SqExemption> GetByFileVaultId(System.Int32 _fileVaultId, int start, int pageLength)
		{
			int count =  -1;
			return GetByFileVaultId(null, _fileVaultId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_SqExemption_FileVault key.
		///		fkSqExemptionFileVault Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_fileVaultId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.SqExemption objects.</returns>
		public TList<SqExemption> GetByFileVaultId(System.Int32 _fileVaultId, int start, int pageLength,out int count)
		{
			return GetByFileVaultId(null, _fileVaultId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_SqExemption_FileVault key.
		///		FK_SqExemption_FileVault Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_fileVaultId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.SqExemption objects.</returns>
		public abstract TList<SqExemption> GetByFileVaultId(TransactionManager transactionManager, System.Int32 _fileVaultId, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.SqExemption Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.SqExemptionKey key, int start, int pageLength)
		{
			return GetBySqExemptionId(transactionManager, key.SqExemptionId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_SqExemption index.
		/// </summary>
		/// <param name="_sqExemptionId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.SqExemption"/> class.</returns>
		public KaiZen.CSMS.Entities.SqExemption GetBySqExemptionId(System.Int32 _sqExemptionId)
		{
			int count = -1;
			return GetBySqExemptionId(null,_sqExemptionId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SqExemption index.
		/// </summary>
		/// <param name="_sqExemptionId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.SqExemption"/> class.</returns>
		public KaiZen.CSMS.Entities.SqExemption GetBySqExemptionId(System.Int32 _sqExemptionId, int start, int pageLength)
		{
			int count = -1;
			return GetBySqExemptionId(null, _sqExemptionId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SqExemption index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_sqExemptionId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.SqExemption"/> class.</returns>
		public KaiZen.CSMS.Entities.SqExemption GetBySqExemptionId(TransactionManager transactionManager, System.Int32 _sqExemptionId)
		{
			int count = -1;
			return GetBySqExemptionId(transactionManager, _sqExemptionId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SqExemption index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_sqExemptionId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.SqExemption"/> class.</returns>
		public KaiZen.CSMS.Entities.SqExemption GetBySqExemptionId(TransactionManager transactionManager, System.Int32 _sqExemptionId, int start, int pageLength)
		{
			int count = -1;
			return GetBySqExemptionId(transactionManager, _sqExemptionId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SqExemption index.
		/// </summary>
		/// <param name="_sqExemptionId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.SqExemption"/> class.</returns>
		public KaiZen.CSMS.Entities.SqExemption GetBySqExemptionId(System.Int32 _sqExemptionId, int start, int pageLength, out int count)
		{
			return GetBySqExemptionId(null, _sqExemptionId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SqExemption index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_sqExemptionId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.SqExemption"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.SqExemption GetBySqExemptionId(TransactionManager transactionManager, System.Int32 _sqExemptionId, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#region _SqExemption_Friendly_All_GetByQuestionnaireId 
		
		/// <summary>
		///	This method wrap the '_SqExemption_Friendly_All_GetByQuestionnaireId' stored procedure. 
		/// </summary>
		/// <param name="questionnaireId"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Friendly_All_GetByQuestionnaireId(System.Int32? questionnaireId)
		{
			return Friendly_All_GetByQuestionnaireId(null, 0, int.MaxValue , questionnaireId);
		}
		
		/// <summary>
		///	This method wrap the '_SqExemption_Friendly_All_GetByQuestionnaireId' stored procedure. 
		/// </summary>
		/// <param name="questionnaireId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Friendly_All_GetByQuestionnaireId(int start, int pageLength, System.Int32? questionnaireId)
		{
			return Friendly_All_GetByQuestionnaireId(null, start, pageLength , questionnaireId);
		}
				
		/// <summary>
		///	This method wrap the '_SqExemption_Friendly_All_GetByQuestionnaireId' stored procedure. 
		/// </summary>
		/// <param name="questionnaireId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Friendly_All_GetByQuestionnaireId(TransactionManager transactionManager, System.Int32? questionnaireId)
		{
			return Friendly_All_GetByQuestionnaireId(transactionManager, 0, int.MaxValue , questionnaireId);
		}
		
		/// <summary>
		///	This method wrap the '_SqExemption_Friendly_All_GetByQuestionnaireId' stored procedure. 
		/// </summary>
		/// <param name="questionnaireId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet Friendly_All_GetByQuestionnaireId(TransactionManager transactionManager, int start, int pageLength , System.Int32? questionnaireId);
		
		#endregion
		
		#region _SqExemption_GetValidByCompanyId 
		
		/// <summary>
		///	This method wrap the '_SqExemption_GetValidByCompanyId' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetValidByCompanyId(System.Int32? companyId)
		{
			return GetValidByCompanyId(null, 0, int.MaxValue , companyId);
		}
		
		/// <summary>
		///	This method wrap the '_SqExemption_GetValidByCompanyId' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetValidByCompanyId(int start, int pageLength, System.Int32? companyId)
		{
			return GetValidByCompanyId(null, start, pageLength , companyId);
		}
				
		/// <summary>
		///	This method wrap the '_SqExemption_GetValidByCompanyId' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetValidByCompanyId(TransactionManager transactionManager, System.Int32? companyId)
		{
			return GetValidByCompanyId(transactionManager, 0, int.MaxValue , companyId);
		}
		
		/// <summary>
		///	This method wrap the '_SqExemption_GetValidByCompanyId' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet GetValidByCompanyId(TransactionManager transactionManager, int start, int pageLength , System.Int32? companyId);
		
		#endregion
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;SqExemption&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;SqExemption&gt;"/></returns>
		public static TList<SqExemption> Fill(IDataReader reader, TList<SqExemption> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.SqExemption c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("SqExemption")
					.Append("|").Append((System.Int32)reader[((int)SqExemptionColumn.SqExemptionId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<SqExemption>(
					key.ToString(), // EntityTrackingKey
					"SqExemption",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.SqExemption();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.SqExemptionId = (System.Int32)reader[((int)SqExemptionColumn.SqExemptionId - 1)];
					c.CompanyId = (System.Int32)reader[((int)SqExemptionColumn.CompanyId - 1)];
					c.SiteId = (System.Int32)reader[((int)SqExemptionColumn.SiteId - 1)];
					c.DateApplied = (System.DateTime)reader[((int)SqExemptionColumn.DateApplied - 1)];
					c.ValidFrom = (System.DateTime)reader[((int)SqExemptionColumn.ValidFrom - 1)];
					c.ValidTo = (System.DateTime)reader[((int)SqExemptionColumn.ValidTo - 1)];
					c.CompanyStatus2Id = (System.Int32)reader[((int)SqExemptionColumn.CompanyStatus2Id - 1)];
					c.RequestedByCompanyId = (System.Int32)reader[((int)SqExemptionColumn.RequestedByCompanyId - 1)];
					c.FileVaultId = (System.Int32)reader[((int)SqExemptionColumn.FileVaultId - 1)];
					c.ModifiedByUserId = (System.Int32)reader[((int)SqExemptionColumn.ModifiedByUserId - 1)];
					c.ModifiedDate = (System.DateTime)reader[((int)SqExemptionColumn.ModifiedDate - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.SqExemption"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.SqExemption"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.SqExemption entity)
		{
			if (!reader.Read()) return;
			
			entity.SqExemptionId = (System.Int32)reader[((int)SqExemptionColumn.SqExemptionId - 1)];
			entity.CompanyId = (System.Int32)reader[((int)SqExemptionColumn.CompanyId - 1)];
			entity.SiteId = (System.Int32)reader[((int)SqExemptionColumn.SiteId - 1)];
			entity.DateApplied = (System.DateTime)reader[((int)SqExemptionColumn.DateApplied - 1)];
			entity.ValidFrom = (System.DateTime)reader[((int)SqExemptionColumn.ValidFrom - 1)];
			entity.ValidTo = (System.DateTime)reader[((int)SqExemptionColumn.ValidTo - 1)];
			entity.CompanyStatus2Id = (System.Int32)reader[((int)SqExemptionColumn.CompanyStatus2Id - 1)];
			entity.RequestedByCompanyId = (System.Int32)reader[((int)SqExemptionColumn.RequestedByCompanyId - 1)];
			entity.FileVaultId = (System.Int32)reader[((int)SqExemptionColumn.FileVaultId - 1)];
			entity.ModifiedByUserId = (System.Int32)reader[((int)SqExemptionColumn.ModifiedByUserId - 1)];
			entity.ModifiedDate = (System.DateTime)reader[((int)SqExemptionColumn.ModifiedDate - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.SqExemption"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.SqExemption"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.SqExemption entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.SqExemptionId = (System.Int32)dataRow["SqExemptionId"];
			entity.CompanyId = (System.Int32)dataRow["CompanyId"];
			entity.SiteId = (System.Int32)dataRow["SiteId"];
			entity.DateApplied = (System.DateTime)dataRow["DateApplied"];
			entity.ValidFrom = (System.DateTime)dataRow["ValidFrom"];
			entity.ValidTo = (System.DateTime)dataRow["ValidTo"];
			entity.CompanyStatus2Id = (System.Int32)dataRow["CompanyStatus2Id"];
			entity.RequestedByCompanyId = (System.Int32)dataRow["RequestedByCompanyId"];
			entity.FileVaultId = (System.Int32)dataRow["FileVaultId"];
			entity.ModifiedByUserId = (System.Int32)dataRow["ModifiedByUserId"];
			entity.ModifiedDate = (System.DateTime)dataRow["ModifiedDate"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.SqExemption"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.SqExemption Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.SqExemption entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region CompanyIdSource	
			if (CanDeepLoad(entity, "Companies|CompanyIdSource", deepLoadType, innerList) 
				&& entity.CompanyIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.CompanyId;
				Companies tmpEntity = EntityManager.LocateEntity<Companies>(EntityLocator.ConstructKeyFromPkItems(typeof(Companies), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.CompanyIdSource = tmpEntity;
				else
					entity.CompanyIdSource = DataRepository.CompaniesProvider.GetByCompanyId(transactionManager, entity.CompanyId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CompanyIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.CompanyIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.CompaniesProvider.DeepLoad(transactionManager, entity.CompanyIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion CompanyIdSource

			#region RequestedByCompanyIdSource	
			if (CanDeepLoad(entity, "Companies|RequestedByCompanyIdSource", deepLoadType, innerList) 
				&& entity.RequestedByCompanyIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.RequestedByCompanyId;
				Companies tmpEntity = EntityManager.LocateEntity<Companies>(EntityLocator.ConstructKeyFromPkItems(typeof(Companies), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.RequestedByCompanyIdSource = tmpEntity;
				else
					entity.RequestedByCompanyIdSource = DataRepository.CompaniesProvider.GetByCompanyId(transactionManager, entity.RequestedByCompanyId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'RequestedByCompanyIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.RequestedByCompanyIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.CompaniesProvider.DeepLoad(transactionManager, entity.RequestedByCompanyIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion RequestedByCompanyIdSource

			#region SiteIdSource	
			if (CanDeepLoad(entity, "Sites|SiteIdSource", deepLoadType, innerList) 
				&& entity.SiteIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.SiteId;
				Sites tmpEntity = EntityManager.LocateEntity<Sites>(EntityLocator.ConstructKeyFromPkItems(typeof(Sites), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.SiteIdSource = tmpEntity;
				else
					entity.SiteIdSource = DataRepository.SitesProvider.GetBySiteId(transactionManager, entity.SiteId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'SiteIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.SiteIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.SitesProvider.DeepLoad(transactionManager, entity.SiteIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion SiteIdSource

			#region CompanyStatus2IdSource	
			if (CanDeepLoad(entity, "CompanyStatus2|CompanyStatus2IdSource", deepLoadType, innerList) 
				&& entity.CompanyStatus2IdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.CompanyStatus2Id;
				CompanyStatus2 tmpEntity = EntityManager.LocateEntity<CompanyStatus2>(EntityLocator.ConstructKeyFromPkItems(typeof(CompanyStatus2), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.CompanyStatus2IdSource = tmpEntity;
				else
					entity.CompanyStatus2IdSource = DataRepository.CompanyStatus2Provider.GetByCompanyStatusId(transactionManager, entity.CompanyStatus2Id);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CompanyStatus2IdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.CompanyStatus2IdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.CompanyStatus2Provider.DeepLoad(transactionManager, entity.CompanyStatus2IdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion CompanyStatus2IdSource

			#region ModifiedByUserIdSource	
			if (CanDeepLoad(entity, "Users|ModifiedByUserIdSource", deepLoadType, innerList) 
				&& entity.ModifiedByUserIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.ModifiedByUserId;
				Users tmpEntity = EntityManager.LocateEntity<Users>(EntityLocator.ConstructKeyFromPkItems(typeof(Users), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.ModifiedByUserIdSource = tmpEntity;
				else
					entity.ModifiedByUserIdSource = DataRepository.UsersProvider.GetByUserId(transactionManager, entity.ModifiedByUserId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ModifiedByUserIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.ModifiedByUserIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.UsersProvider.DeepLoad(transactionManager, entity.ModifiedByUserIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion ModifiedByUserIdSource

			#region FileVaultIdSource	
			if (CanDeepLoad(entity, "FileVault|FileVaultIdSource", deepLoadType, innerList) 
				&& entity.FileVaultIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.FileVaultId;
				FileVault tmpEntity = EntityManager.LocateEntity<FileVault>(EntityLocator.ConstructKeyFromPkItems(typeof(FileVault), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.FileVaultIdSource = tmpEntity;
				else
					entity.FileVaultIdSource = DataRepository.FileVaultProvider.GetByFileVaultId(transactionManager, entity.FileVaultId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'FileVaultIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.FileVaultIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.FileVaultProvider.DeepLoad(transactionManager, entity.FileVaultIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion FileVaultIdSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.SqExemption object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.SqExemption instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.SqExemption Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.SqExemption entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region CompanyIdSource
			if (CanDeepSave(entity, "Companies|CompanyIdSource", deepSaveType, innerList) 
				&& entity.CompanyIdSource != null)
			{
				DataRepository.CompaniesProvider.Save(transactionManager, entity.CompanyIdSource);
				entity.CompanyId = entity.CompanyIdSource.CompanyId;
			}
			#endregion 
			
			#region RequestedByCompanyIdSource
			if (CanDeepSave(entity, "Companies|RequestedByCompanyIdSource", deepSaveType, innerList) 
				&& entity.RequestedByCompanyIdSource != null)
			{
				DataRepository.CompaniesProvider.Save(transactionManager, entity.RequestedByCompanyIdSource);
				entity.RequestedByCompanyId = entity.RequestedByCompanyIdSource.CompanyId;
			}
			#endregion 
			
			#region SiteIdSource
			if (CanDeepSave(entity, "Sites|SiteIdSource", deepSaveType, innerList) 
				&& entity.SiteIdSource != null)
			{
				DataRepository.SitesProvider.Save(transactionManager, entity.SiteIdSource);
				entity.SiteId = entity.SiteIdSource.SiteId;
			}
			#endregion 
			
			#region CompanyStatus2IdSource
			if (CanDeepSave(entity, "CompanyStatus2|CompanyStatus2IdSource", deepSaveType, innerList) 
				&& entity.CompanyStatus2IdSource != null)
			{
				DataRepository.CompanyStatus2Provider.Save(transactionManager, entity.CompanyStatus2IdSource);
				entity.CompanyStatus2Id = entity.CompanyStatus2IdSource.CompanyStatusId;
			}
			#endregion 
			
			#region ModifiedByUserIdSource
			if (CanDeepSave(entity, "Users|ModifiedByUserIdSource", deepSaveType, innerList) 
				&& entity.ModifiedByUserIdSource != null)
			{
				DataRepository.UsersProvider.Save(transactionManager, entity.ModifiedByUserIdSource);
				entity.ModifiedByUserId = entity.ModifiedByUserIdSource.UserId;
			}
			#endregion 
			
			#region FileVaultIdSource
			if (CanDeepSave(entity, "FileVault|FileVaultIdSource", deepSaveType, innerList) 
				&& entity.FileVaultIdSource != null)
			{
				DataRepository.FileVaultProvider.Save(transactionManager, entity.FileVaultIdSource);
				entity.FileVaultId = entity.FileVaultIdSource.FileVaultId;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region SqExemptionChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.SqExemption</c>
	///</summary>
	public enum SqExemptionChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>Companies</c> at CompanyIdSource
		///</summary>
		[ChildEntityType(typeof(Companies))]
		Companies,
			
		///<summary>
		/// Composite Property for <c>Sites</c> at SiteIdSource
		///</summary>
		[ChildEntityType(typeof(Sites))]
		Sites,
			
		///<summary>
		/// Composite Property for <c>CompanyStatus2</c> at CompanyStatus2IdSource
		///</summary>
		[ChildEntityType(typeof(CompanyStatus2))]
		CompanyStatus2,
			
		///<summary>
		/// Composite Property for <c>Users</c> at ModifiedByUserIdSource
		///</summary>
		[ChildEntityType(typeof(Users))]
		Users,
			
		///<summary>
		/// Composite Property for <c>FileVault</c> at FileVaultIdSource
		///</summary>
		[ChildEntityType(typeof(FileVault))]
		FileVault,
		}
	
	#endregion SqExemptionChildEntityTypes
	
	#region SqExemptionFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;SqExemptionColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SqExemption"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SqExemptionFilterBuilder : SqlFilterBuilder<SqExemptionColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SqExemptionFilterBuilder class.
		/// </summary>
		public SqExemptionFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the SqExemptionFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SqExemptionFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SqExemptionFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SqExemptionFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SqExemptionFilterBuilder
	
	#region SqExemptionParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;SqExemptionColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SqExemption"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SqExemptionParameterBuilder : ParameterizedSqlFilterBuilder<SqExemptionColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SqExemptionParameterBuilder class.
		/// </summary>
		public SqExemptionParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the SqExemptionParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SqExemptionParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SqExemptionParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SqExemptionParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SqExemptionParameterBuilder
	
	#region SqExemptionSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;SqExemptionColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SqExemption"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class SqExemptionSortBuilder : SqlSortBuilder<SqExemptionColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SqExemptionSqlSortBuilder class.
		/// </summary>
		public SqExemptionSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion SqExemptionSortBuilder
	
} // end namespace
