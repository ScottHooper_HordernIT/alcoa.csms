var sec = 0;
var min = 60;
function countDown() {
    sec--;
    if (sec == -1) {
        sec = 59; min = min - 1
    }
    else {
        min = min
    }
    if (sec <= 9) {
        sec = "0" + sec
    }
    time = (min <= 9 ? "0" + min : min) + "." + sec + "";
    if (document.getElementById) {
        theTime.innerHTML = time
    }
    SD = window.setTimeout("countDown();", 1000);
    if (min == "00" && sec == "00") {
        sec = "00"; window.clearTimeout(SD)
    }
}

function addLoadEvent(a) {
    var b = window.onload;
    if (typeof window.onload != "function") {
        window.onload = a
    }
    else {
        window.onload = function () {
            if (b)
            { b() } a()
        } 
    } 
}
addLoadEvent(function(){countDown()});