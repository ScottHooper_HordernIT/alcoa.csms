﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using KaiZen.CSMS.Services;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using System.Data.SqlClient;
using System.ComponentModel;
using System.Reflection;
using System.Data;


using DevExpress.Web.ASPxGridView;

public partial class UserControls_Other_ListSubContractorsCompliance : System.Web.UI.UserControl
{
    #region Services

    public Repo.CSMS.Service.Database.ISiteService SiteService { get; set; }

    #endregion

    Auth auth = new Auth();

    const string CellColourBad = "Red"; //"#ffb8b8";
    const string CellColourGood = "Lime"; //"#def3ca";

    private enum TrafficLightColor2
    {
        [Description("0")]
        Grey = 0,
        [Description("1")]
        Red = 1,
        [Description("2")]
        Yellow = 2,
        [Description("3")]
        Green = 3,
    }
    public static string GetImageURL(Enum value)
    {
        FieldInfo fi = value.GetType().GetField(value.ToString());
        DescriptionAttribute[] attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);
        return (attributes.Length > 0) ? attributes[0].Description : value.ToString();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack);
    }

    private void moduleInit(bool postBack)
    {
        if (!postBack)
        { //first time load
            string CompanyId = "";
            if (Request.QueryString["q"] != null)
            {
                CompanyId = Request.QueryString["q"].ToString();
            }
            CompaniesService cService = new CompaniesService();
            Companies c = cService.GetByCompanyId(Convert.ToInt32(CompanyId));
            if (c != null)
            {
                lblCompanyName.Text = c.CompanyName;
            }

            ddlCompanies.DataSourceID = "sqldsCompaniesList_SubContractors";
            ddlCompanies.TextField = "CompanyName";
            ddlCompanies.ValueField = "CompanyId";
            ddlCompanies.DataBind();
            ddlCompanies.Items.Add("--------", -2);
            ddlCompanies.Items.Add(" < Select Company... >", -1);
            ddlCompanies.SelectedIndex = -1;
            ddlCompanies.Value = " < Select Company... >";
            ddlCompanies.Text = " < Select Company... >";

            switch (auth.RoleId)
            {
                case ((int)RoleList.Reader):
                    ddlCompanies.Enabled = true;
                    break;
                case ((int)RoleList.Contractor):
                    ddlCompanies.Enabled = true;
                    break;
                case ((int)RoleList.PreQual):
                    ddlCompanies.Enabled = false;
                    break;
                case ((int)RoleList.Administrator):
                    ddlCompanies.Enabled = true;
                    break;
                default:
                    // RoleList.Disabled or No Role
                    Response.Redirect("AccessDenied.aspx?error=" + Server.UrlEncode("Unrecognised User."));
                    break;
            }
        }
    }

    protected void ddlCompanies_SelectedIndexChanged1(object sender, EventArgs e)
    {
        if ((int)ddlCompanies.SelectedItem.Value > 0)
        {
            LoadModule((int)ddlCompanies.SelectedItem.Value, DateTime.Now.Year);
        }
        else
        {
            throw new Exception("Please pick a Sub Contractor");
        }
    }



    protected void ResetTrafficLights()
    {
        lblCompanyType.Text = "?";
        lblSupervisionRequirement.Text = "?";

        gcTL_SQ.Value = GetImageURL(TrafficLightColor2.Red);
        lblTL_SQ.Text = "Safety Qualification";

        hlTL_SC.NavigateUrl = "";
        hlTL_SC.Text = "";
        gcTL_SC.Value = GetImageURL(TrafficLightColor2.Red);
        lblTL_SC.Text = "Approval to engage Sub Contractors";

    }

    private void LoadModule(int CompanyId, int sYear)
    {
        //PanelAll.Height = Unit.Pixel(500);
        pnlHealthCheck.Visible = true;

        int count = 0;
        //int fCount = 0;
        //int iCount = 0;

        ResetTrafficLights();

        CompaniesService _cService = new CompaniesService();
        Companies c = _cService.GetByCompanyId(CompanyId);

        if (c.CompanyStatus2Id == (int)CompanyStatus2List.Contractor)
        {
            lblCompanyType.Text = "Contractor";
        }
        if (c.CompanyStatus2Id == (int)CompanyStatus2List.SubContractor)
        {
            lblCompanyType.Text = "Sub Contractor";
        }

        //#region Safety Qualification & Subcontractors
        //QuestionnaireWithLocationApprovalViewLatestService qService = new QuestionnaireWithLocationApprovalViewLatestService();
        //QuestionnaireWithLocationApprovalViewLatestFilters qFilters = new QuestionnaireWithLocationApprovalViewLatestFilters();
        //qFilters.Append(QuestionnaireWithLocationApprovalViewLatestColumn.CompanyId, CompanyId.ToString());
        //VList<QuestionnaireWithLocationApprovalViewLatest> qVlist = qService.GetPaged(qFilters.ToString(), null, 0, 100, out count);

        //if (count > 0)
        //{
        //    hlTL_SQ.Text = "View";

        //    if (qVlist[0].Status == (int)QuestionnaireStatusList.Incomplete) lblTL_SQ.Text = "Safety Qualification - Incomplete";
        //    if (qVlist[0].Status == (int)QuestionnaireStatusList.BeingAssessed) lblTL_SQ.Text = "Safety Qualification - Being Assessed";
        //    if (qVlist[0].Status == (int)QuestionnaireStatusList.AssessmentComplete)
        //    {
        //        lblTL_SQ.Text = "Assessment Complete";
        //        if (qVlist[0].IsVerificationRequired == true)
        //        {
        //            //hlTL_SC.Text = "List";
        //            //hlTL_SC.NavigateUrl = "javascript:popUp('PopUps/ListApprovedSubContractors.aspx');";
        //            hlTL_SC.Visible = false;
        //            gcTL_SC.Value = GetImageURL(TrafficLightColor2.Green);
        //        }
        //    }

        //    if (qVlist[0] != null)
        //    {
        //        if (auth.RoleId == (int)RoleList.Administrator || auth.RoleId == (int)RoleList.Reader)
        //        {
        //            hlTL_SQ.Visible = true;
        //        }
        //        else
        //        {
        //            hlTL_SQ.Visible = false;
        //        }
        //        hlTL_SQ.NavigateUrl = "~/SafetyPQ_QuestionnaireModify.aspx" + QueryStringModule.Encrypt("q=" + qVlist[0].QuestionnaireId);
        //    }

        //    if (qVlist[0].MainAssessmentValidTo != null)
        //    {
        //        if (qVlist[0].MainAssessmentValidTo > DateTime.Now)
        //        {
        //            DateTime dt = (DateTime)qVlist[0].MainAssessmentValidTo;
        //            if (qVlist[0].Status == (int)QuestionnaireStatusList.AssessmentComplete)
        //            {
        //                lblTL_SQ.Text = "Safety Qualification valid until " + dt.ToString("dd/MM/yyyy");
        //                gcTL_SQ.Value = GetImageURL(TrafficLightColor2.Green);
        //                lblSupervisionRequirement.Text = qVlist[0].LevelOfSupervision;

        //                DateTime dtExpiry = Convert.ToDateTime(qVlist[0].MainAssessmentValidTo);
        //                TimeSpan ts = Convert.ToDateTime(dtExpiry) - DateTime.Now;
        //                if (ts.Days <= 60)
        //                {
        //                    gcTL_SQ.Value = GetImageURL(TrafficLightColor2.Yellow);
        //                    lblSupervisionRequirement.Text = qVlist[0].LevelOfSupervision;
        //                    if (ts.Days == 1)
        //                    {
        //                        lblTL_SQ.Text = "Safety Qualification valid until " + dt.ToString("dd/MM/yyyy") + " (Expires in " + ts.Days + " Day)";
        //                    }
        //                    else
        //                    {
        //                        lblTL_SQ.Text = "Safety Qualification valid until " + dt.ToString("dd/MM/yyyy") + " (Expires in " + ts.Days + " Days)";
        //                    }
        //                }
        //            }
        //            else
        //            {
        //                gcTL_SQ.Value = GetImageURL(TrafficLightColor2.Yellow);

        //                if (qVlist[1].Status == (int)QuestionnaireStatusList.AssessmentComplete &&
        //                    c.CompanyStatusId == (int)CompanyStatusList.Requal_Incomplete)
        //                {
        //                    DateTime dtExpiry = Convert.ToDateTime(qVlist[0].MainAssessmentValidTo);
        //                    TimeSpan ts = Convert.ToDateTime(dtExpiry) - DateTime.Now;
        //                    if (ts.Days <= 60)
        //                    {
        //                        lblSupervisionRequirement.Text = qVlist[0].LevelOfSupervision;
        //                        lblTL_SQ.Text = "Safety Qualification Valid until " + dt.ToString("dd/MM/yyyy") + " - Expires in " + ts.Days + " Days.";
        //                    }
        //                }
        //            }
        //        }
        //        else
        //        {
        //            DateTime dt = (DateTime)qVlist[0].MainAssessmentValidTo;
        //            lblTL_SQ.Text = "Safety Qualification Expired at " + dt.ToString("dd/MM/yyyy");
        //            gcTL_SQ.Value = GetImageURL(TrafficLightColor2.Red);
        //            if (qVlist.Count > 1)
        //            {
        //                if (qVlist[1] != null)
        //                {
        //                    if (qVlist[1].Status == (int)QuestionnaireStatusList.AssessmentComplete)
        //                    {
        //                        lblSupervisionRequirement.Text = qVlist[1].LevelOfSupervision;
        //                    }
        //                }
        //            }
        //        }
        //    }
        //}
        //else
        //{
        //    hlTL_SQ.Text = "";
        //    gcTL_SQ.Value = GetImageURL(TrafficLightColor2.Yellow);
        //    hlTL_SQ.NavigateUrl = "javascript:void(0);";
        //    gcTL_SQ.Visible = true;
        //    gcTL_SQ.ToolTip = "No Safety Qualification exists for your company. Please Contact your procurement contact.";
        //}

        //#endregion

        #region Safety Qualification & Subcontractors
        QuestionnaireWithLocationApprovalViewService qService = new QuestionnaireWithLocationApprovalViewService();
        QuestionnaireWithLocationApprovalViewFilters qFilters = new QuestionnaireWithLocationApprovalViewFilters();
        qFilters.Append(QuestionnaireWithLocationApprovalViewColumn.CompanyId, CompanyId.ToString());
        VList<QuestionnaireWithLocationApprovalView> qVlist = qService.GetPaged(qFilters.ToString(), "QuestionnaireId DESC", 0, 100, out count);

        if (count > 0)
        {
            hlTL_SQ.Text = "View";

            bool requaling = false;
            if (qVlist[0].Status == (int)QuestionnaireStatusList.Incomplete)
            {
                lblTL_SQ.Text = "Safety Qualification - Incomplete";
                if (count > 1) requaling = true;
            }
            if (qVlist[0].Status == (int)QuestionnaireStatusList.BeingAssessed)
            {
                lblTL_SQ.Text = "Safety Qualification - Being Assessed";
                if (count > 1) requaling = true;
            }

            if (qVlist[0] != null)
            {
                if (auth.RoleId == (int)RoleList.Administrator || auth.RoleId == (int)RoleList.Reader)
                {
                    hlTL_SQ.Visible = true;
                }
                else
                {
                    hlTL_SQ.Visible = false;
                }

                hlTL_SQ.NavigateUrl = "~/SafetyPQ_QuestionnaireModify.aspx" + QueryStringModule.Encrypt("q=" + qVlist[0].QuestionnaireId);
            }

            bool done = false;
            int no = 0;
            int i = 0;
            while (i <= qVlist.Count - 1)
            {
                if (!done)
                {
                    if (qVlist[i] != null)
                    {
                        if (qVlist[i].Status == (int)QuestionnaireStatusList.AssessmentComplete)
                        {
                            lblTL_SQ.Text = "Safety Qualification - Assessment Complete";
                            if (qVlist[i].IsVerificationRequired && (c.CompanyStatus2Id == (int)CompanyStatus2List.Contractor))
                            {
                                //Changed by Pankaj Dikshit for <DT 2646, 02-Oct-2012>, Change:- if-condition is added as on nonCompliance2.ascx.cs
                                if (qVlist[i].LevelOfSupervision != "Direct" && qVlist[i].MainAssessmentValidTo >= DateTime.Now)
                                {
                                    //hlTL_SC.Text = "List";
                                    //hlTL_SC.NavigateUrl = String.Format("~/ListSubContractorsCompliance.aspx{0}", QueryStringModule.Encrypt("q=" + c.CompanyId));
                                    gcTL_SC.Value = GetImageURL(TrafficLightColor2.Green);
                                }
                            }

                            if (qVlist[i].MainAssessmentValidTo != null)
                            {
                                if (qVlist[i].MainAssessmentValidTo > DateTime.Now)
                                {
                                    DateTime dt = (DateTime)qVlist[i].MainAssessmentValidTo;
                                    if (qVlist[i].Status == (int)QuestionnaireStatusList.AssessmentComplete)
                                    {
                                        done = true;

                                        lblTL_SQ.Text = "Safety Qualification valid until " + dt.ToString("dd/MM/yyyy");
                                        if (requaling) lblTL_SQ.Text += " (Currently Re-Qualifying)";
                                        gcTL_SQ.Value = GetImageURL(TrafficLightColor2.Green);

                                        lblSupervisionRequirement.Text = qVlist[i].LevelOfSupervision;

                                        DateTime dtExpiry = Convert.ToDateTime(qVlist[i].MainAssessmentValidTo);
                                        TimeSpan ts = Convert.ToDateTime(dtExpiry) - DateTime.Now;
                                        if (ts.Days <= 60)
                                        {
                                            gcTL_SQ.Value = GetImageURL(TrafficLightColor2.Yellow);
                                            lblSupervisionRequirement.Text = qVlist[i].LevelOfSupervision;
                                            if (ts.Days == 1)
                                            {
                                                lblTL_SQ.Text = "Safety Qualification valid until " + dt.ToString("dd/MM/yyyy") + " (Expires in " + ts.Days + " Day)";
                                            }
                                            else
                                            {
                                                lblTL_SQ.Text = "Safety Qualification valid until " + dt.ToString("dd/MM/yyyy") + " (Expires in " + ts.Days + " Days)";
                                            }
                                            no = i;
                                        }
                                    }
                                    else
                                    {
                                        gcTL_SQ.Value = GetImageURL(TrafficLightColor2.Yellow);

                                        if (qVlist[i + 1] != null)
                                        {
                                            if (qVlist[i + 1].Status == (int)QuestionnaireStatusList.AssessmentComplete &&
                                                c.CompanyStatusId == (int)CompanyStatusList.Requal_Incomplete)
                                            {
                                                DateTime dtExpiry = Convert.ToDateTime(qVlist[i].MainAssessmentValidTo);
                                                TimeSpan ts = Convert.ToDateTime(dtExpiry) - DateTime.Now;
                                                if (ts.Days <= 60)
                                                {
                                                    lblSupervisionRequirement.Text = qVlist[i].LevelOfSupervision;
                                                    lblTL_SQ.Text = "Safety Qualification Valid until " + dt.ToString("dd/MM/yyyy") + " - Expires in " + ts.Days + " Days.";
                                                    no = i;
                                                    done = true;
                                                }
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    DateTime dt = (DateTime)qVlist[i].MainAssessmentValidTo;
                                    lblTL_SQ.Text = "Safety Qualification Expired at " + dt.ToString("dd/MM/yyyy");
                                    gcTL_SQ.Value = GetImageURL(TrafficLightColor2.Red);
                                    if (qVlist.Count >= i + 2) //todo: check this...
                                    {
                                        if (qVlist[i + 1].Status == (int)QuestionnaireStatusList.AssessmentComplete)
                                        {
                                            lblSupervisionRequirement.Text = qVlist[i + 1].LevelOfSupervision;
                                        }
                                    }
                                    no = i;
                                    done = true;
                                }
                            }
                        }
                    }
                }
                i++;
            }


            //if (qVlist[0].MainAssessmentValidTo != null)
            //{
            //    if (qVlist[0].MainAssessmentValidTo > DateTime.Now)
            //    {
            //        DateTime dt = (DateTime)qVlist[0].MainAssessmentValidTo;
            //        if (qVlist[0].Status == (int)QuestionnaireStatusList.AssessmentComplete)
            //        {
            //            lblTL_SQ.Text = "Safety Qualification valid until " + dt.ToString("dd/MM/yyyy");
            //            gcTL_SQ.Value = GetImageURL(TrafficLightColor2.Green);
            //            lblSupervisionRequirement.Text = qVlist[0].LevelOfSupervision;

            //            DateTime dtExpiry = Convert.ToDateTime(qVlist[0].MainAssessmentValidTo);
            //            TimeSpan ts = Convert.ToDateTime(dtExpiry) - DateTime.Now;
            //            if (ts.Days <= 60)
            //            {
            //                gcTL_SQ.Value = GetImageURL(TrafficLightColor2.Yellow);
            //                lblSupervisionRequirement.Text = qVlist[0].LevelOfSupervision;
            //                if (ts.Days == 1)
            //                {
            //                    lblTL_SQ.Text = "Safety Qualification valid until " + dt.ToString("dd/MM/yyyy") + " (Expires in " + ts.Days + " Day)";
            //                }
            //                else
            //                {
            //                    lblTL_SQ.Text = "Safety Qualification valid until " + dt.ToString("dd/MM/yyyy") + " (Expires in " + ts.Days + " Days)";
            //                }
            //            }
            //        }
            //        else
            //        {
            //            gcTL_SQ.Value = GetImageURL(TrafficLightColor2.Yellow);

            //            if (qVlist[1].Status == (int)QuestionnaireStatusList.AssessmentComplete &&
            //                c.CompanyStatusId == (int)CompanyStatusList.Requal_Incomplete)
            //            {
            //                DateTime dtExpiry = Convert.ToDateTime(qVlist[0].MainAssessmentValidTo);
            //                TimeSpan ts = Convert.ToDateTime(dtExpiry) - DateTime.Now;
            //                if (ts.Days <= 60)
            //                {
            //                    lblSupervisionRequirement.Text = qVlist[0].LevelOfSupervision;
            //                    lblTL_SQ.Text = "Safety Qualification Valid until " + dt.ToString("dd/MM/yyyy") + " - Expires in " + ts.Days + " Days.";
            //                }
            //            }
            //        }
            //    }
            //    else
            //    {
            //        DateTime dt = (DateTime)qVlist[0].MainAssessmentValidTo;
            //        lblTL_SQ.Text = "Safety Qualification Expired at " + dt.ToString("dd/MM/yyyy");
            //        gcTL_SQ.Value = GetImageURL(TrafficLightColor2.Red);
            //        if (qVlist[1] != null)
            //        {
            //            if (qVlist[1].Status == (int)QuestionnaireStatusList.AssessmentComplete)
            //            {
            //                lblSupervisionRequirement.Text = qVlist[1].LevelOfSupervision;
            //            }
            //        }
            //    }
            //}
        }
        else
        {
            hlTL_SQ.Text = "";
            gcTL_SQ.Value = GetImageURL(TrafficLightColor2.Red);
            hlTL_SQ.NavigateUrl = "javascript:void(0);";
            gcTL_SQ.Visible = true;
            gcTL_SQ.ToolTip = "No Safety Qualification exists for your company. Please Contact your procurement contact.";
        }


        //if (CompanyId != 0)
        //{
        //    int _count = 0;
        //    QuestionnaireFilters qFilters = new QuestionnaireFilters();
        //    qFilters.Append(QuestionnaireColumn.CompanyId, CompanyId.ToString());
        //    TList<Questionnaire> qList = DataRepository.QuestionnaireProvider.GetPaged(String.Format("CompanyId = {0}", CompanyId), "CreatedDate DESC", 0, 9999, out _count);

        //    if (_count > 0)
        //    {
        //        switch (qList[0].Status)
        //        {
        //            case (int)QuestionnaireStatusList.Incomplete:
        //                imgTL_SQ.ImageUrl = GetImageURL(TrafficLightColor.Red);
        //                imgTL_SQ.ToolTip = "Your company's latest Safety Questionnaire is incomplete.";

        //                CompaniesService _cService0 = new CompaniesService();
        //                Companies _c = _cService0.GetByCompanyId(CompanyId);
        //                if (_c.CompanyStatusId == (int)CompanyStatusList.Requal_Incomplete)
        //                {
        //                    if (qList[1] != null)
        //                    {
        //                        if (qList[1].MainAssessmentValidTo != null)
        //                        {
        //                            imgTL_SQ.ImageUrl = GetImageURL(TrafficLightColor.Yellow);
        //                            imgTL_SQ.ToolTip = "Your company is being Re-Qualified but your previous questionnaire is valid until " + qList[1].MainAssessmentValidTo.ToString() + ".";
        //                        }
        //                    }
        //                }
        //                break;
        //            case (int)QuestionnaireStatusList.BeingAssessed:
        //                imgTL_SQ.ImageUrl = GetImageURL(TrafficLightColor.Yellow);
        //                imgTL_SQ.ToolTip = "Your company's latest Safety Questionnaire has been submitted and is currently being assessed.";
        //                break;
        //            case (int)QuestionnaireStatusList.AssessmentComplete:
        //                if (qList[0].MainAssessmentValidTo < DateTime.Now)
        //                {
        //                    imgTL_SQ.ImageUrl = GetImageURL(TrafficLightColor.RedCross);
        //                    imgTL_SQ.ToolTip = String.Format("Your company's latest Safety Questionnaire expired on {0}.", qList[0].MainAssessmentValidTo);
        //                }
        //                else
        //                {
        //                    if (qList[0].Recommended == true)
        //                    {
        //                        imgTL_SQ.ImageUrl = GetImageURL(TrafficLightColor.Green);
        //                        imgTL_SQ.ToolTip = "Your company's latest Safety Questionnaire has been assessed and has achieved a Recommended status.";
        //                    }
        //                    if (qList[0].Recommended == false)
        //                    {
        //                        imgTL_SQ.ImageUrl = GetImageURL(TrafficLightColor.RedCross);
        //                        imgTL_SQ.ToolTip = "Your company's latest Safety Questionnaire has been assessed and has achieved an NOT Recommended status.";
        //                    }
        //                }
        //                DateTime dtNow = DateTime.Now;
        //                if (qList[0].MainAssessmentValidTo != null)
        //                {
        //                    DateTime dtExpiry = Convert.ToDateTime(qList[0].MainAssessmentValidTo);

        //                    TimeSpan ts = Convert.ToDateTime(dtExpiry) - dtNow;
        //                    if (ts.Days <= 60)
        //                    {
        //                        if (ts.Days > 0)
        //                        {
        //                            imgTL_SQ.ImageUrl = GetImageURL(TrafficLightColor.Yellow);
        //                            imgTL_SQ.ToolTip = "Your company's latest Safety Questionnaire is due to expire in " + ts.Days.ToString() + " Days at " + dtExpiry.ToShortDateString() + ".";
        //                        }
        //                        else
        //                        {
        //                            imgTL_SQ.ImageUrl = GetImageURL(TrafficLightColor.Red);
        //                            imgTL_SQ.ToolTip = "Your company's latest Safety Questionnaire has expired.";
        //                        }
        //                    }
        //                }

        //                break;
        //        }
        //    }
        //    else
        //    {
        //        imgTL_SQ.ImageUrl = GetImageURL(TrafficLightColor.Red);
        //        imgTL_SQ.ToolTip = "Your company does NOT have a Safety Questionnaire";
        //    }
        //}
        #endregion

        count = 0;

        DataTable dtNonComplianceTable = new DataTable();
        dtNonComplianceTable.Columns.Add("CompanyId", typeof(int));
        dtNonComplianceTable.Columns.Add("SiteId", typeof(int));
        dtNonComplianceTable.Columns.Add("YearId", typeof(int));
        dtNonComplianceTable.Columns.Add("CategoryId", typeof(int));
        dtNonComplianceTable.Columns.Add("Radar", typeof(string));
        dtNonComplianceTable.Columns.Add("SiteName", typeof(string));
        dtNonComplianceTable.Columns.Add("SafetyComplianceScore_LastMonth", typeof(string));
        dtNonComplianceTable.Columns.Add("SafetyComplianceScore_Ytd", typeof(string));
        dtNonComplianceTable.Columns.Add("MandatedTraining_Ytd", typeof(string));
        dtNonComplianceTable.Columns.Add("Csa_LastQtr", typeof(string));
        dtNonComplianceTable.Columns.Add("Csa_CurrentQtr", typeof(string));
        dtNonComplianceTable.Columns.Add("LWDR", typeof(string));
        dtNonComplianceTable.Columns.Add("TRIR", typeof(string));
        dtNonComplianceTable.Columns.Add("AIFR", typeof(string));
        dtNonComplianceTable.Columns.Add("CompanySiteCategoryDesc", typeof(string));
        dtNonComplianceTable.Columns.Add("SiteAccessApproved", typeof(string));

        int Wcount = 0;
        int Vcount = 0;
        CompanySiteCategoryStandardRegionService cscsrService = new CompanySiteCategoryStandardRegionService();
        CompanySiteCategoryStandardRegionFilters cscsrFiltersWAO = new CompanySiteCategoryStandardRegionFilters();

        cscsrFiltersWAO.Append(CompanySiteCategoryStandardRegionColumn.CompanyId, CompanyId.ToString());
        cscsrFiltersWAO.Append(CompanySiteCategoryStandardRegionColumn.RegionId, "1"); //WAO

        CompanySiteCategoryStandardRegionFilters cscsrFiltersVICOPS = new CompanySiteCategoryStandardRegionFilters();
        cscsrFiltersVICOPS.Append(CompanySiteCategoryStandardRegionColumn.CompanyId, CompanyId.ToString());
        cscsrFiltersVICOPS.Append(CompanySiteCategoryStandardRegionColumn.RegionId, "4"); //VIC OPS

        VList<CompanySiteCategoryStandardRegion> cscsrWAO = cscsrService.GetPaged(cscsrFiltersWAO.ToString(), "SiteName ASC", 0, 100, out Wcount);
        VList<CompanySiteCategoryStandardRegion> cscsrVICOPS = cscsrService.GetPaged(cscsrFiltersVICOPS.ToString(), "SiteName ASC", 0, 100, out Vcount);

        //WAO
        if (Wcount > 0)
        {
            foreach (CompanySiteCategoryStandardRegion cscsr in cscsrWAO)
            {
                if (cscsr.SiteAbbrev != "HUN" && cscsr.SiteAbbrev != "WDL")
                {
                    var site = SiteService.Get(e => e.SiteId == cscsr.SiteId, null);

                    if (cscsr.CompanySiteCategoryId != null)
                        dtNonComplianceTable.Rows.Add(AddRow(dtNonComplianceTable.NewRow(), Convert.ToInt32(cscsr.CompanySiteCategoryId), site, CompanyId, sYear));
                }
            }
        }

        //MINE Sites
        if (Wcount > 0)
        {
            foreach (CompanySiteCategoryStandardRegion cscsr in cscsrWAO)
            {
                if (cscsr.SiteAbbrev == "HUN" || cscsr.SiteAbbrev == "WDL")
                {
                    var site = SiteService.Get(e => e.SiteId == cscsr.SiteId, null);

                    if (cscsr.CompanySiteCategoryId != null)
                        dtNonComplianceTable.Rows.Add(AddRow(dtNonComplianceTable.NewRow(), Convert.ToInt32(cscsr.CompanySiteCategoryId), site, CompanyId, sYear));
                }
            }
        }

        //VIC OPS
        if (Vcount > 0)
        {
            foreach (CompanySiteCategoryStandardRegion cscsr in cscsrVICOPS)
            {
                var site = SiteService.Get(e => e.SiteId == cscsr.SiteId, null);

                if (cscsr.CompanySiteCategoryId != null)
                    dtNonComplianceTable.Rows.Add(AddRow(dtNonComplianceTable.NewRow(), Convert.ToInt32(cscsr.CompanySiteCategoryId), site, CompanyId, sYear));
            }
        }

        ASPxGridView1.DataSource = dtNonComplianceTable;
        ASPxGridView1.DataBind();
    }

    protected DataRow AddRow(DataRow dr, int CategoryId, Repo.CSMS.DAL.EntityModels.Site Site, int CompanyId, int Year)
    {
        DateTime dtNowMinusOneMonth = DateTime.Now.AddMonths(-1);
        Year = dtNowMinusOneMonth.Year;

        dr["CompanyId"] = CompanyId;
        dr["SiteId"] = Site.SiteId;
        dr["YearId"] = Year;
        dr["SiteName"] = Site.SiteName;
        dr["CategoryId"] = CategoryId;
        dr["Radar"] = "~/Radar.aspx" + QueryStringModule.Encrypt(String.Format("CompanyId={0}&SiteId={1}&Year={2}", CompanyId, Site.SiteId, Year));

        CompanySiteCategoryService cscService = new CompanySiteCategoryService();
        CompanySiteCategory csc = cscService.GetByCompanySiteCategoryId(CategoryId);

        CompanySiteCategoryStandardService cscsService = new CompanySiteCategoryStandardService();
        CompanySiteCategoryStandard cscs = cscsService.GetByCompanyIdSiteId(CompanyId, Site.SiteId);




        DataSet dsSummary = new DataSet();
        dsSummary.Tables.Add(Helper.Radar.CreateDataTable(CompanyId, Site.SiteId, 1, null));

        DataSet dsYtd = new DataSet();
        dsYtd.Tables.Add(Helper.Radar.CreateDataTable(CompanyId, Site.SiteId, 2, null));
        dsYtd.Tables.Add(Helper.Radar.CreateDataTable(CompanyId, Site.SiteId, 2, null));


        foreach (string KpiMeasure in Helper.Radar.GetKpiMeasurements(-1, false, false)) //Summary
        {
            dsSummary.Tables[0].Rows.Add(Helper.Radar.CreateDataRow_DataSet1_Summary(dsSummary.Tables[0], KpiMeasure,
                                                                                    CompanyId, Site.SiteId, 0, Year, CategoryId));
        }
        foreach (string KpiMeasure in Helper.Radar.GetKpiMeasurements(CategoryId, false, false)) //Ytd
        {
            DataRow _dr = Helper.Radar.CreateDataRow_DataSet2_Ytd(dsYtd.Tables[0], KpiMeasure,
                                                                        CompanyId, Site, 0, dtNowMinusOneMonth.Year, csc);
            dsYtd.Tables[0].Rows.Add(_dr);

            DataRow _dr2 = Helper.Radar.CreateDataRow_DataSet2_Ytd(dsYtd.Tables[1], KpiMeasure,
                                                                        CompanyId, Site, dtNowMinusOneMonth.Month, dtNowMinusOneMonth.Year, csc);
            dsYtd.Tables[1].Rows.Add(_dr2);
        }

        int KpiMeasureCountYtd = 0;
        int KpiMeasureCountYtdCompliant = 0;
        int MandatedTrainingYtd = 0;
        int CsaCurrentQtr = 0;
        int CsaPreviousQtr = 0;

        bool OnSiteYtd = false;
        foreach (DataRow _dr in dsYtd.Tables[0].Rows)
        {
            string KpiMeasure = _dr["KpiMeasure"].ToString();
            if (KpiMeasure == "On site" || KpiMeasure == "Alcoa Annual Audit Score (%)" || KpiMeasure == "CSA - Self Audit (% Score) - Current Qtr")
            {
                //ignore lol
                if (KpiMeasure == "On site")
                {
                    for (int i = 1; i <= 12; i++)
                    {
                        if (_dr[String.Format("KpiScoreMonth{0}", i)].ToString() == "Yes") OnSiteYtd = true;
                    }
                }
            }
            else
            {
                KpiMeasureCountYtd++;
                if (_dr["KpiScoreYtdPercentage"].ToString() == "100")
                {
                    KpiMeasureCountYtdCompliant++;
                }

                if (KpiMeasure == "CSA - Self Audit (% Score) - Current Qtr")
                {
                    Int32.TryParse(_dr["KpiScoreYtd"].ToString(), out CsaCurrentQtr);
                }
                else if (KpiMeasure == "CSA - Self Audit (% Score) - Previous Qtr")
                {
                    Int32.TryParse(_dr["KpiScoreYtd"].ToString(), out CsaPreviousQtr);
                }
                else if (KpiMeasure == "% Mandated Training")
                {
                    Int32.TryParse(_dr["KpiScoreYtd"].ToString(), out MandatedTrainingYtd);
                }
            }
        }
        Decimal KpiYtdCompliantPercentage = Decimal.Round((Convert.ToDecimal(KpiMeasureCountYtdCompliant) / Convert.ToDecimal(KpiMeasureCountYtd)) * 100);

        bool OnSiteMonth = false;
        int KpiMeasureCountMonth = 0;
        int KpiMeasureCountMonthCompliant = 0;
        foreach (DataRow _dr in dsYtd.Tables[1].Rows)
        {
            string KpiMeasure = _dr["KpiMeasure"].ToString();
            if (KpiMeasure == "On site" || KpiMeasure == "Alcoa Annual Audit Score (%)" || KpiMeasure == "CSA - Self Audit (% Score) - Current Qtr")
            {
                //ignore lol
                if (KpiMeasure == "On site")
                {
                    if (_dr[String.Format("KpiScoreMonth{0}", dtNowMinusOneMonth.Month)].ToString() == "Yes") OnSiteMonth = true;
                }
            }
            else
            {
                KpiMeasureCountMonth++;
                if (_dr[String.Format("KpiScoreMonth{0}Percentage", dtNowMinusOneMonth.Month)].ToString() == "100")
                {
                    KpiMeasureCountMonthCompliant++;
                }
            }
        }
        Decimal KpiMonthCompliantPercentage = Decimal.Round((Convert.ToDecimal(KpiMeasureCountMonthCompliant) / Convert.ToDecimal(KpiMeasureCountMonth)) * 100);

        string LWDR = "0";
        string TRIR = "0";
        string AIFR = "0";
        foreach (DataRow _dr in dsSummary.Tables[0].Rows)
        {
            switch (_dr["KpiMeasure"].ToString())
            {
                case "LWDFR":
                    LWDR = _dr["KpiMeasure"].ToString();
                    break;
                case "TRIFR":
                    TRIR = _dr["KpiMeasure"].ToString();
                    break;
                case "AIFR":
                    AIFR = _dr["KpiMeasure"].ToString();
                    break;
                default:
                    break;
            }
        }

        string SiteApproved = "Tentative";
        if (cscs.Approved == true)
        {
            SiteApproved = "Yes";
        }
        else if (cscs.Approved == false)
        {
            SiteApproved = "No";
        }

        dr["SafetyComplianceScore_LastMonth"] = (Convert.ToInt32(KpiMonthCompliantPercentage)).ToString();
        dr["SafetyComplianceScore_Ytd"] = (Convert.ToInt32(KpiYtdCompliantPercentage)).ToString();
        dr["MandatedTraining_Ytd"] = MandatedTrainingYtd.ToString();
        dr["Csa_LastQtr"] = CsaPreviousQtr.ToString();
        dr["Csa_CurrentQtr"] = CsaCurrentQtr.ToString();
        dr["LWDR"] = LWDR;
        dr["TRIR"] = TRIR;
        dr["AIFR"] = AIFR;

        CompaniesService cService = new CompaniesService();
        Companies c = cService.GetByCompanyId(CompanyId);
        if (c.CompanyStatus2Id == (int)CompanyStatus2List.SubContractor)
        {
            dr["CompanySiteCategoryDesc"] = csc.CategoryDesc;
        }
        else
        {
            dr["CompanySiteCategoryDesc"] = csc.CategoryDesc;
        }
        dr["SiteAccessApproved"] = SiteApproved;

        if (!OnSiteYtd)
        {
            dr["SafetyComplianceScore_Ytd"] = "-";
            dr["MandatedTraining_Ytd"] = "-";
            dr["Csa_CurrentQtr"] = "-";
            dr["LWDR"] = "-";
            dr["TRIR"] = "-";
            dr["AIFR"] = "-";
        }

        if (!OnSiteMonth)
        {
            dr["SafetyComplianceScore_LastMonth"] = "-";
        }


        return dr;
    }

    protected void grid_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {
        //if (e.RowType != GridViewRowType.Data) return;
        //int SiteId = (int)ASPxGridView1.GetRowValues(e.VisibleIndex, "SiteId");
        //int count = 0;

        //int CategoryId = (int)ASPxGridView1.GetRowValues(e.VisibleIndex, "CategoryId");
        //AnnualTargetsService atService = new AnnualTargetsService();
        //AnnualTargetsFilters atFilters = new AnnualTargetsFilters();
        //atFilters.Append(AnnualTargetsColumn.CompanySiteCategoryId, CategoryId.ToString());
        //TList<AnnualTargets> atTlist = atService.GetPaged(atFilters.ToString(), "Year DESC", 0, 1000, out count);

        ////1
        //string colour = "";
        //string _KpiScoreLastMonth = (string)ASPxGridView1.GetRowValues(e.VisibleIndex, "SafetyComplianceScore_LastMonth");
        //if (_KpiScoreLastMonth == "-")
        //{
        //    colour = "";
        //}
        //else
        //{
        //    if (_KpiScoreLastMonth == "100") colour = CellColourGood; else colour = CellColourBad;
        //}
        //e.Row.Cells[1].Style.Add("background-color", colour);

        ////2
        //colour = "";
        //string _KpiScoreYtd = (string)ASPxGridView1.GetRowValues(e.VisibleIndex, "SafetyComplianceScore_Ytd");
        //if (_KpiScoreYtd == "-")
        //{
        //    colour = "";
        //}
        //else
        //{
        //    if (_KpiScoreYtd == "100") colour = CellColourGood; else colour = CellColourBad;
        //}
        //e.Row.Cells[2].Style.Add("background-color", colour);

        ////3
        //colour = "";

        //int MtTarget = 100;
        //Int32.TryParse(atTlist[0].Training.ToString(), out MtTarget);

        //string MandatedTrainingYtd = (string)ASPxGridView1.GetRowValues(e.VisibleIndex, "MandatedTraining_Ytd");
        //int _MandatedTrainingYtd = 0;

        //if (MandatedTrainingYtd == "-")
        //{
        //    colour = "";
        //}
        //else
        //{
        //    Int32.TryParse(MandatedTrainingYtd, out _MandatedTrainingYtd);
        //    if (_MandatedTrainingYtd >= MtTarget) colour = CellColourGood; else colour = CellColourBad;
        //}
        //e.Row.Cells[3].Style.Add("background-color", colour);

        ////4
        //colour = "";

        //int CsaTarget = 100;
        //Int32.TryParse(atTlist[0].QuarterlyAuditScore.ToString(), out CsaTarget);

        //string CsaLastQtr = (string)ASPxGridView1.GetRowValues(e.VisibleIndex, "Csa_LastQtr");
        //int _CsaLastQtr = 0;

        //if (CsaLastQtr == "-")
        //{
        //    colour = "";
        //}
        //else
        //{
        //    Int32.TryParse(CsaLastQtr, out _CsaLastQtr);
        //    if (_CsaLastQtr >= CsaTarget) colour = CellColourGood; else colour = CellColourBad;
        //}
        //e.Row.Cells[4].Style.Add("background-color", colour);

        ////5
        //colour = "Yellow";
        //string SiteAccessApproved = (string)ASPxGridView1.GetRowValues(e.VisibleIndex, "SiteAccessApproved");
        //if (SiteAccessApproved == "Yes")
        //{
        //    colour = CellColourGood;
        //}
        //else if (SiteAccessApproved == "No")
        //{
        //    colour = CellColourBad;
        //}
        //e.Row.Cells[10].Style.Add("background-color", colour);


        ////Radar
        //HyperLink hl = ASPxGridView1.FindRowCellTemplateControl(e.VisibleIndex, null, "hlRadar") as HyperLink;
        //if (ASPxGridView1.GetRowValues(e.VisibleIndex, "Radar") != DBNull.Value) hl.NavigateUrl = (string)ASPxGridView1.GetRowValues(e.VisibleIndex, "Radar");
    }
}
