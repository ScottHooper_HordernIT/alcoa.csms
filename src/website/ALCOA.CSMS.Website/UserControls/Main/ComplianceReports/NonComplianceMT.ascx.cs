using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Text;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using KaiZen.CSMS.Web;
using KaiZen.CSMS.Services;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxTabControl;
using DevExpress.Web.Data;
using DevExpress.Web.ASPxGridView;
using System.Drawing;
using DevExpress.XtraPrinting;

public partial class UserControls_NonComplianceMT : System.Web.UI.UserControl
{
    Auth auth = new Auth();
    int green = 0;
    int expected = 0;
    protected void Page_Load(object sender, EventArgs e) {  Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); }

    private void moduleInit(bool postBack)
    {
        Session["MS"] = "MS";
        lblMS.Text = "Medical Schedules";

        if (Request.QueryString["q"] != null)
        {
            if (Request.QueryString["q"] == "TS")
            {
                Session["MS"] = "TS";
                lblMS.Text = "Training Schedules";
            }
        }

        if (!postBack)
        { //first time load
            

            // Export
            String exportFileName = @"ALCOA CSMS - Non Compliance Report - " + @lblMS.Text; //Chrome & IE is fine.
            String userAgent = Request.Headers.Get("User-Agent");
            if (
                (userAgent.Contains("Mozilla") && !userAgent.Contains("AppleWebKit") && !userAgent.Contains("MSIE")) || //Mozilla
                (userAgent.Contains("AppleWebKit") && !userAgent.Contains("Chrome"))      //Safari
                )
            {
                exportFileName = exportFileName.Replace(" ", "_");
            }
            Helper.ExportGrid.Settings(ucExportButtons, exportFileName);


            cmbYear.DataSourceID = "sqldsKPIYear";
            cmbYear.TextField = "Year";
            cmbYear.ValueField = "Year";
            cmbYear.Value = DateTime.Now.Year.ToString();
            cmbYear.DataBind();

            

            switch (auth.RoleId)
            {
                case ((int)RoleList.Reader):
                    //full access
                    break;
                case ((int)RoleList.Contractor):
                    //restrict to own company
                    SessionHandler.spVar_CompanyId = auth.CompanyId.ToString();
                    grid.Settings.ShowFilterRow = false;
                    grid.FilterExpression = String.Format("CompanyId = {0}", auth.CompanyId);
                    grid.DataBind();
                    break;
                case ((int)RoleList.Administrator):
                    //full access
                    break;
                default:
                    // do something
                    Response.Redirect("AccessDenied.aspx?error=" + Server.UrlEncode("Unrecognised User."));
                    break;
            }

            SessionHandler.spVar_Year = DateTime.Now.Year.ToString();
            
        }
        sqldsNonComplianceReportYearMedical.DataBind();
        grid.DataBind();
    }

    protected void grid_RowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {
        int month;
        int qtr = 4;
        string selectedYear = SessionHandler.spVar_Year;

        if (String.IsNullOrEmpty(selectedYear)) { selectedYear = DateTime.Now.Year.ToString(); };
        if (selectedYear == DateTime.Now.Year.ToString())
        {
            month = DateTime.Now.Month;
            if (month <= 12) qtr = 4;
            if (month <= 9) qtr = 3;
            if (month <= 6) qtr = 2;
            if (month <= 3) qtr = 1;
            expected = qtr;
        }
        else
        {
            month = 12;
            qtr = 4;
        }

        if (e.RowType != GridViewRowType.Data) return;
        Label lblYear = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "noKPISubmitted13") as Label;
        //Start:Change By Denashis for deployment issue(Export)
        if (lblYear != null)
        {
            int yearCount = (int)grid.GetRowValues(e.VisibleIndex, "Year");
            if (yearCount > 0)
            {
                lblYear.ForeColor = Color.Green;
                lblYear.Text = yearCount.ToString();
            }
            else
            {
                if (yearCount == -1)
                {
                    lblYear.Text = "-";
                    lblYear.ForeColor = Color.Black;
                }
                else
                {
                    lblYear.ForeColor = Color.Red;
                    lblYear.Text = yearCount.ToString();
                }
            }
        }


        if (qtr >= 1)
        {
            #region Qtr1
            if (e.RowType != GridViewRowType.Data) return;
            Label label = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "noKPISubmitted1") as Label;
            int change = (int)grid.GetRowValues(e.VisibleIndex, "Qtr1");
            if (label != null) //Added by Debashis(Export issue)
            {
                label.Text = "";
                System.Web.UI.WebControls.Image img = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "changeImage1")
                                            as System.Web.UI.WebControls.Image;
                img.Visible = false;
               
                if (change != 0)
                {
                    img.Visible = true;
                    img.ImageUrl = "~/Images/greentick.gif";
                    label.ForeColor = Color.Green;
                    //label.Text = "(" + change + ")";
                    green++;
                }
                else
                {
                    img.Visible = true;
                    img.ImageUrl = "~/Images/redcross.gif";
                    label.ForeColor = Color.Red;
                }
            #endregion
                if (qtr >= 2)
                {
                    #region Qtr2
                    label = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "noKPISubmitted2") as Label;
                    change = (int)grid.GetRowValues(e.VisibleIndex, "Qtr2");
                    label.Text = "";
                    img = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "changeImage2")
                                                as System.Web.UI.WebControls.Image;
                    img.Visible = false;
                    if (change != 0)
                    {
                        img.Visible = true;
                        img.ImageUrl = "~/Images/greentick.gif";
                        label.ForeColor = Color.Green;
                        //label.Text = "(" + change + ")";
                        green++;
                    }
                    else
                    {
                        img.Visible = true;
                        img.ImageUrl = "~/Images/redcross.gif";
                        label.ForeColor = Color.Red;
                    }
                    #endregion
                    if (qtr >= 3)
                    {
                        #region Qtr3
                        label = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "noKPISubmitted3") as Label;
                        change = (int)grid.GetRowValues(e.VisibleIndex, "Qtr3");
                        label.Text = "";
                        img = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "changeImage3")
                                                    as System.Web.UI.WebControls.Image;
                        img.Visible = false;
                        if (change != 0)
                        {
                            img.Visible = true;
                            img.ImageUrl = "~/Images/greentick.gif";
                            label.ForeColor = Color.Green;
                            //label.Text = "(" + change + ")";
                            green++;
                        }
                        else
                        {
                            img.Visible = true;
                            img.ImageUrl = "~/Images/redcross.gif";
                            label.ForeColor = Color.Red;
                        }
                        #endregion
                        if (qtr >= 4)
                        {
                            #region Qtr4
                            label = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "noKPISubmitted4") as Label;
                            change = (int)grid.GetRowValues(e.VisibleIndex, "Qtr4");
                            label.Text = "";
                            img = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "changeImage4")
                                                        as System.Web.UI.WebControls.Image;
                            img.Visible = false;
                            if (change != 0)
                            {
                                img.Visible = true;
                                img.ImageUrl = "~/Images/greentick.gif";
                                label.ForeColor = Color.Green;
                                //label.Text = "(" + change + ")";
                                green++;
                            }
                            else
                            {
                                img.Visible = true;
                                img.ImageUrl = "~/Images/redcross.gif";
                                label.ForeColor = Color.Red;
                            }
                            #endregion
                        }
                    }
                }
            }
        }
    }

    protected void cmbYear_SelectedIndexChanged(object sender, EventArgs e)
    {
        SessionHandler.spVar_Year = cmbYear.SelectedItem.Value.ToString();
        sqldsNonComplianceReportYearMedical.DataBind();
        grid.DataBind();
    }

    protected void grid_AutoFilterCellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
    {
       
        if (e.Column.FieldName == "CompanyId")
        {
            ASPxComboBox comboBox = e.Editor as ASPxComboBox;
            //if(comboBox != null && !Page.IsPostBack) comboBox.Items.Clear();
            comboBox.ClientSideEvents.Init = "function(s, e) {s.InsertItem(0, '(ALL)', ''); s.RemoveItem(-1);}";
        }
        if (e.Column.FieldName == "Embedded")
        {
            ASPxComboBox comboBox = e.Editor as ASPxComboBox;
            //if(comboBox != null && !Page.IsPostBack) comboBox.Items.Clear();
            comboBox.ClientSideEvents.Init = "function(s, e) {s.InsertItem(0, '(ALL)', ''); s.InsertItem(0, 'Yes', 'Yes'); s.InsertItem(0, 'No', 'No'); s.RemoveItem(-1);}";
        }
        //if (e.Column.FieldName.StartsWith("Qtr"))
        //{
        //    ASPxComboBox comboBox = e.Editor as ASPxComboBox;
        //    //if(comboBox != null && !Page.IsPostBack) comboBox.Items.Clear();
        //    comboBox.ClientSideEvents.Init = "function(s, e) {s.InsertItem(0, '(ALL)', ''); s.InsertItem(0, '1', 'Yes'); s.InsertItem(0, '0', 'No'); s.RemoveItem(-1);}";
        //}
        //if (e.Column.FieldName == "Year")
        //{
        //    ASPxComboBox comboBox = e.Editor as ASPxComboBox;
        //    //if(comboBox != null && !Page.IsPostBack) comboBox.Items.Clear();
        //    comboBox.ClientSideEvents.Init = "function(s, e) {s.InsertItem(0, '(ALL)', ''); s.InsertItem(0, '0', '0'); s.InsertItem(0, '1', '1'); s.InsertItem(0, '2', '2'); s.InsertItem(0, '3', '3'); s.InsertItem(0, '5', '5'); s.RemoveItem(-1);}";
        //}
        
        if (e.Column.FieldName == "RegionName")
        {
            ASPxComboBox comboBox = e.Editor as ASPxComboBox;
            //if(comboBox != null && !Page.IsPostBack) comboBox.Items.Clear();
            //comboBox.ClientSideEvents.Init = "function(s, e) {s.InsertItem(0, '(ALL)', ''); s.InsertItem(0, 'Western Australian Operations', 'Western Australian Operations'); s.InsertItem(0, 'Victorian Operations', 'Victorian Operations'); s.RemoveItem(-1);}";
            comboBox.ClientSideEvents.Init = "function(s, e) {s.InsertItem(0, '(ALL)', ''); s.RemoveItem(-1);}";
        }
         
    }
}
