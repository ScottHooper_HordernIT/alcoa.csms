﻿using System;
using System.Collections.Generic;
using System.Text;

using System.Globalization;
using System.IO;
using System.Security.Principal;

using KaiZen.CSMS.Data;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Services;
using KaiZen.Library;

using LumenWorks.Framework.IO.Csv;
using ALCOA.CSMS.Batch.Common.Log;

namespace ALCOA.CSMS.BatchJobScripts
{
    public class Scripts
    {
        public class Insert
        {
            /// <summary>
            /// Add_s the companies.
            /// </summary>
            /// <param name="csvFilePath">The CSV file path.</param>
            public static void Companies(string csvFilePath)
            {
                string myFile = "";

                try
                {
                    using (FileStream logFileStream = new FileStream(csvFilePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                    {
                        StreamReader logFileReader = new StreamReader(logFileStream);
                        while (!logFileReader.EndOfStream)
                        {
                            string line = logFileReader.ReadLine();
                            myFile += line + "\n";
                        }
                        logFileReader.Close();
                        logFileStream.Close();
                    }
                }
                catch (Exception ex)
                {
                    Log4NetService.LogInfo(ex.Message);
                }


                if (!String.IsNullOrEmpty(myFile))
                {
                    Log4NetService.LogInfo("CSV File Ok.");
                    try
                    {
                        using (CsvReader csv = new CsvReader(new StringReader(myFile), true))
                        {
                            int fieldCount = csv.FieldCount;
                            string[] headers = csv.GetFieldHeaders();
                            Log4NetService.LogInfo("Found Headers: " + String.Join(",", headers));

                            string companyName = "";
                            while (csv.ReadNextRecord())
                            {
                                try
                                {
                                    companyName = csv["CompanyName"].ToString();
                                    string ABN = csv["ABN"].ToString();
                                    string SQStatus = csv["SQStatus"].ToString();
                                    string CompanyStatus = csv["CompanyStatus"].ToString();

                                    CompaniesService cService = new CompaniesService();
                                    using (Companies c = new Companies())
                                    {
                                        c.CompanyName = companyName;
                                        c.CompanyAbn = ABN;
                                        CompanyStatusService csService = new CompanyStatusService();
                                        CompanyStatus cs = csService.GetByCompanyStatusName(SQStatus);
                                        if (cs == null)
                                            throw new Exception("sq status not found");
                                        CompanyStatus2Service cs2Service = new CompanyStatus2Service();
                                        CompanyStatus2 cs2 = cs2Service.GetByCompanyStatusName(CompanyStatus);
                                        if (cs2 == null)
                                            throw new Exception("company status not found");
                                        c.CompanyStatusId = cs.CompanyStatusId;
                                        c.CompanyStatus2Id = cs2.CompanyStatusId;
                                        c.ModifiedbyUserId = 0;
                                        cService.Insert(c);
                                    }
                                    Log4NetService.LogInfo("Successfully added: " + companyName);
                                }
                                catch (Exception ex)
                                {
                                    Log4NetService.LogInfo(String.Format("Error: {0} - {1}", companyName, ex.Message));
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Log4NetService.LogInfo("Error: " + ex.Message.ToString());
                    }
                }
            }
        }
        public class Update
        {
            public static void ProcurementContacts()
            {
                ProcurementContacts(@"C:\temp\read.txt");
            }

            /// <summary>
            /// Updates the proc contact.
            /// </summary>
            public static void ProcurementContacts(string FileFullPath)
            {
                Log4NetService.LogInfo("Reading file: " + FileFullPath);
                string myFile = "";
                try
                {
                    using (FileStream logFileStream = new FileStream(FileFullPath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                    {
                        StreamReader logFileReader = new StreamReader(logFileStream);
                        while (!logFileReader.EndOfStream)
                        {
                            string line = logFileReader.ReadLine();
                            myFile += line + "\n";
                        }
                        logFileReader.Close();
                        logFileStream.Close();
                    }
                }
                catch (Exception ex)
                {
                    Log4NetService.LogInfo("[ERROR] " + ex.Message);
                }

                int i = 0;
                if (!String.IsNullOrEmpty(myFile))
                {
                    using (CsvReader csv = new CsvReader(new StringReader(myFile), true))
                    {
                        int fieldCount = csv.FieldCount;
                        string[] headers = csv.GetFieldHeaders();
                        Log4NetService.LogInfo("Found Headers: " + String.Join(",", headers));
                        i = 0; //justincase..

                        UsersService uService = new UsersService();

                        while (csv.ReadNextRecord())
                        {
                            i++;
                            try
                            {
                                string QuestionnaireId = csv["QuestionnaireId"].ToString();
                                string LastName = csv["ProcurementLastName"].ToString();
                                string FirstName = csv["ProcurementFirstName"].ToString();

                                int count = 0;
                                UsersFilters uFilter = new UsersFilters();
                                uFilter.Append(UsersColumn.LastName, LastName);
                                uFilter.Append(UsersColumn.FirstName, FirstName);
                                uFilter.Append(UsersColumn.CompanyId, "0");
                                TList<Users> usersList = DataRepository.UsersProvider.GetPaged(uFilter.ToString(), null, 0, 100, out count);
                                if (usersList.Count == 1)
                                {
                                    if (usersList[0] != null)
                                    {
                                        QuestionnaireInitialResponseService qirService = new QuestionnaireInitialResponseService();
                                        QuestionnaireInitialResponse qir = qirService.GetByQuestionnaireIdQuestionId(Convert.ToInt32(QuestionnaireId), "2");
                                        if (qir == null)
                                        {
                                            qir = new QuestionnaireInitialResponse();
                                            qir.QuestionnaireId = Convert.ToInt32(QuestionnaireId);
                                            qir.QuestionId = "2";
                                            qir.ModifiedByUserId = 0;
                                            qir.ModifiedDate = DateTime.Now;
                                            qir.AnswerText = usersList[0].UserId.ToString();
                                            qirService.Insert(qir);
                                        }
                                        else
                                        {
                                            qir.AnswerText = usersList[0].UserId.ToString();
                                            qirService.Save(qir);
                                        }
                                        Log4NetService.LogInfo(String.Format("Line {0}: Saved", i));
                                    }
                                }
                                else
                                {
                                    throw new Exception("Users Count: " + usersList.ToString());
                                }

                            }
                            catch (Exception ex)
                            {
                                Log4NetService.LogInfo(String.Format("Error on line {0}:{1}", i, ex.Message));
                            }
                        }
                    }
                }

            }

            public static void CrpContacts()
            {
                Configuration config = new Configuration();
                CrpContacts(config.GetValue(@ConfigList.CrpContactsFilePath));
            }

            public static void CrpContacts(string fileFullPath)
            {

                string startHeader = @"Location,Supplier Name,Non Preferred Supplier,Supervisor Name,Contractor Number,Contractor Name,Organisation,Course Name,Training Title,Course End Date,Expiration Date,Record Type,Comments,Person Type";

                Log4NetService.LogInfo("Reading file: " + fileFullPath);
                string myFile = "";
                try
                {
                    using (FileStream logFileStream = new FileStream(fileFullPath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                    {
                        StreamReader logFileReader = new StreamReader(logFileStream);
                        bool start = false;
                        while (!logFileReader.EndOfStream)
                        {
                            string line = logFileReader.ReadLine();
                            if (line.Contains(startHeader))
                                start = true;

                            if (start == true)
                                myFile += line + "\n";

                        }
                        logFileReader.Close();
                        logFileStream.Close();
                    }
                }
                catch (Exception ex)
                {
                    Log4NetService.LogInfo("[ERROR] " + ex.Message);
                }

                int i = 0;
                if (!String.IsNullOrEmpty(myFile))
                {
                    using (CsvReader csv = new CsvReader(new StringReader(myFile), true))
                    {
                        int fieldCount = csv.FieldCount;
                        string[] headers = csv.GetFieldHeaders();
                        Log4NetService.LogInfo("Found Headers: " + String.Join(",", headers));
                        i = 0; //justincase..

                        UsersService uService = new UsersService();

                        SitesService sService = new SitesService();
                        TList<Sites> sList = sService.GetAll();

                        CompaniesHrMapService chmService = new CompaniesHrMapService();
                        TList<CompaniesHrMap> chmList = chmService.GetAll();

                        TList<CompaniesHrCrpData> chcdList_new = new TList<CompaniesHrCrpData>();

                        while (csv.ReadNextRecord())
                        {
                            i++;
                            try
                            {
                                string location = csv["Location"].ToString();
                                string supplierName = csv["Supplier Name"].ToString();
                                string contractorName = csv["Contractor Name"].ToString();

                                if (!String.IsNullOrEmpty(location) && !String.IsNullOrEmpty(supplierName) && !String.IsNullOrEmpty(contractorName))
                                {
                                    Sites s = sList.Find(SitesColumn.SiteNameHr, location, true);
                                    if (s != null)
                                    {
                                        CompaniesHrMap chm = chmList.Find(CompaniesHrMapColumn.CompanyNameHr, contractorName, true);
                                        if (chm != null)
                                        {
                                            CompaniesHrCrpData chcd = new CompaniesHrCrpData();
                                            chcd.CompanyId = chm.CompanyId;
                                            chcd.SiteId = s.SiteId;
                                            chcd.CrpName = contractorName;
                                            chcdList_new.Add(chcd);
                                        }
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                Log4NetService.LogInfo("Error: " + ex.Message);
                            }
                        }

                        TransactionManager transactionManager = null;
                        try
                        {
                            transactionManager = ConnectionScope.ValidateOrCreateTransaction(true);
                            if (chcdList_new.Count > 0)
                            {
                                CompaniesHrCrpDataService chcdService = new CompaniesHrCrpDataService();
                                TList<CompaniesHrCrpData> chcdList = chcdService.GetAll();
                                DataRepository.CompaniesHrCrpDataProvider.Delete(transactionManager, chcdList);
                                DataRepository.CompaniesHrCrpDataProvider.Delete(transactionManager, chcdList_new);
                                transactionManager.Commit();
                            }
                        }
                        catch (Exception ex)
                        {
                            if ((transactionManager != null) && (transactionManager.IsOpen))
                                transactionManager.Rollback();
                            throw ex;
                        }
                    }
                }
            }
        }
        public class Misc
        {
            /// <summary>
            /// Resets the read flag of apss documents within the system if a new version of an apss file has been copied across by robocopy.
            /// </summary>
            public static void ResetApssReadFlagIfNewVersion()
            {
                // Cindi Thornton 17/9/15 - scheduled tasks now run as service account, this isn't needed. Remove all impersonation related code
                //WindowsImpersonationContext impContext = null;
                //try
                //{
                //    impContext = NetworkSecurity.Impersonation.ImpersonateUser(
                //                                System.Configuration.ConfigurationManager.AppSettings["WAOCSM_domain"].ToString(),
                //                                System.Configuration.ConfigurationManager.AppSettings["WAOCSM_user"].ToString(),
                //                                System.Configuration.ConfigurationManager.AppSettings["WAOCSM_pass"].ToString());
                //}
                //catch (ApplicationException ex)
                //{
                //    throw ex;
                //}

                try
                {
                    //if (null != impContext)
                    //{
                    //    using (impContext)
                    //    {
                            try
                            {

                                string listMR = KaiZen.CSMS.Data.DataRepository.DocumentsProvider.GetByDocumentType("MR").ToString();
                                //string listMFU = KaiZen.CSMS.Data.DataRepository.DocumentsProvider.GetByDocumentType("MFU").ToString();
                                //string listTP = KaiZen.CSMS.Data.DataRepository.DocumentsProvider.GetByDocumentType("TP").ToString();
                                //string listMM = KaiZen.CSMS.Data.DataRepository.DocumentsProvider.GetByDocumentType("MM").ToString();
                                string listDocs = KaiZen.CSMS.Data.DataRepository.DocumentsProvider.GetAll().ToString();

                                Log4NetService.LogInfo("Starting...\n");

                                //string[] fileEntries = Directory.GetFiles("C:\\WAOCSM");
                                string[] fileEntries = Directory.GetFiles(@Helper.Configuration.GetValue(ConfigList.RoboCopyLogsDir).ToString());
                                List<String> updatedMRFiles = new List<String>();

                                foreach (string fileName in fileEntries)
                                {
                                    if (fileName.Contains("WAOCSM_Sync"))
                                    {
                                        using (TextReader textr = new StreamReader(fileName))
                                        {
                                            string line;
                                            int lineNumber = 0;
                                            string TimeStampField = "  Started : ";
                                            string TimeStampString = "";
                                            DateTime TimeStamp = new DateTime();
                                            string divider = "------------------------------------------------------------------------------";
                                            int dividerSection = 0;
                                            int filesLine = 0;
                                            int entries = 0;
                                            bool breakOut = false;

                                            while ((line = textr.ReadLine()) != null && breakOut == false)
                                            {
                                                if (line.Contains(TimeStampField))
                                                {
                                                    TimeStampString = line.Replace(TimeStampField, "");
                                                    TimeStamp = DateTime.ParseExact(TimeStampString, "ddd MMM d HH:mm:ss yyyy", new DateTimeFormatInfo());
                                                    //check if TimeStamp already exists in database..
                                                    int Count = 0;
                                                    ApssLogsFilters queryApssLogs = new ApssLogsFilters();
                                                    queryApssLogs.AppendEquals(ApssLogsColumn.TimeStamp, TimeStamp.ToString("MM/dd/yyyy HH:mm:ss"));
                                                    //queryApssLogs.Append(ApssLogsColumn.TimeStamp, DateTime.Now.ToString("yyyy-MM-dd-HH:mm:ss"));
                                                    TList<ApssLogs> apssLogsList = DataRepository.ApssLogsProvider.GetPaged(queryApssLogs.ToString(), null, 0, 100, out Count);
                                                    if (Count > 0)
                                                    {
                                                        Log4NetService.LogInfo(String.Format("Ignoring File: {0} with TimeStamp: {1} as it already exists in the Database.\n", fileName, TimeStampString));
                                                        breakOut = true;
                                                        break;
                                                    }
                                                    else
                                                        Log4NetService.LogInfo(String.Format("Reading File: {0} with TimeStamp: {1} as it does not exist in the Database.\n", fileName, TimeStampString));
                                                }
                                                if (line.Contains(divider) == true)
                                                    dividerSection++;
                                                else
                                                    switch (dividerSection)
                                                    {
                                                        case 3:
                                                            //begin filechanges log
                                                            filesLine++;
                                                            if ((filesLine >= 3) && line.Length > 0)
                                                            {
                                                                string[] ChangedFiles = line.Split('\t');
                                                                string ApssfileTag = Helper.General.TrimString(ChangedFiles[1].ToString());
                                                                string ApssfileName = Helper.General.TrimString(ChangedFiles[4].ToString()).ToLower();
                                                                using (ApssLogs apssLogsEntry = new ApssLogs())
                                                                {
                                                                    apssLogsEntry.FileName = ApssfileName;
                                                                    apssLogsEntry.FileTag = ApssfileTag;
                                                                    apssLogsEntry.TimeStamp = TimeStamp;
                                                                    apssLogsEntry.FileType = "";
                                                                    if (listMR.Contains(ApssfileName))
                                                                        apssLogsEntry.FileType = "Must Read";
                                                                    if (listMR.Contains(ApssfileName))
                                                                        apssLogsEntry.FileType = "Most Frequently Read";
                                                                    if (listMR.Contains(ApssfileName))
                                                                        apssLogsEntry.FileType = "Training Packages";
                                                                    if (listMR.Contains(ApssfileName))
                                                                        apssLogsEntry.FileType = "Mines Medical";
                                                                    if (!String.IsNullOrEmpty(apssLogsEntry.FileType))
                                                                    {
                                                                        if (apssLogsEntry.FileType == "Must Read")
                                                                        {
                                                                            ApssLogsService apssLogsService = new ApssLogsService();
                                                                            apssLogsService.Insert(apssLogsEntry);
                                                                            Log4NetService.LogInfo(String.Format("Adding Entry... TimeStamp: {0} FileName: {1} FileTag: {2}\n", TimeStampString, ApssfileName, ApssfileTag));
                                                                        }
                                                                        else
                                                                            if (Helper.General.TrimString(ApssfileTag).ToLower() == "newer")
                                                                                Log4NetService.LogInfo(String.Format("Ignoring Entry... TimeStamp: {0} FileName: {1} FileTag: {2} because we are told to ignore file tags of 'Older/Newer (Non MR/MM/TP/MFU Type)'.\n", TimeStampString, ApssfileName, ApssfileTag));
                                                                            else
                                                                                if (Helper.General.TrimString(ApssfileTag) == "Older")
                                                                                    Log4NetService.LogInfo(String.Format("Ignoring Entry... TimeStamp: {0} FileName: {1} FileTag: {2} because we are told to ignore file tags of 'Older/Newer (Non MR/MM/TP/MFU Type)'.\n", TimeStampString, ApssfileName, ApssfileTag));
                                                                                else
                                                                                {
                                                                                    //Add to Database
                                                                                    ApssLogsService apssLogsService = new ApssLogsService();
                                                                                    apssLogsService.Insert(apssLogsEntry);
                                                                                    Log4NetService.LogInfo(String.Format("Adding Entry... TimeStamp: {0} FileName: {1} FileTag: {2}\n", TimeStampString, ApssfileName, ApssfileTag));
                                                                                }
                                                                    }
                                                                    else
                                                                        Log4NetService.LogInfo(String.Format("Ignoring Entry... TimeStamp: {0} FileName: {1} FileTag: {2} because we are told to ignore files which are not of MR/TP/MM/MFU Type.\n", TimeStampString, ApssfileName, ApssfileTag));
                                                                }
                                                                entries++;
                                                            }
                                                            break;
                                                        case 4:
                                                            //end filechanges log
                                                            break;
                                                        default:
                                                            break;
                                                    }
                                                lineNumber++;
                                            }
                                            textr.Close();
                                        }
                                    }
                                }
                                Log4NetService.LogInfo("Finished.\n");
                            }
                            catch (Exception ex)
                            {
                                Log4NetService.LogInfo("***Error: " + ex.Message.ToString());
                            }
                        //}
                    //}
                }
                catch (Exception ex)
                {
                    Log4NetService.LogInfo("***Error: " + ex.Message.ToString());
                }
                finally
                {
                    //if (impContext != null) impContext.Undo();
                }
            }

            public static void GenerateKpiProjectHoursReport()
            {
                KpiEngineeringProjectHoursService kService = new KpiEngineeringProjectHoursService();
                VList<KpiEngineeringProjectHours> kList = kService.GetAll();
                Log4NetService.LogInfo("Year,Month,Company,Site,Project Name,Project Hours");
                foreach (KpiEngineeringProjectHours k in kList)
                {
                    if (!String.IsNullOrEmpty(k.ProjectCapital1Title) || k.ProjectCapital1Hours != null)
                    {
                        Log4NetService.LogInfo(k.KpiDateTimeYear + "," + k.KpiDateTimeMonth + "," + @k.CompanyName + "," + @k.SiteName + "," + @k.ProjectCapital1Title + "," + k.ProjectCapital1Hours);
                    }
                    if (!String.IsNullOrEmpty(k.ProjectCapital2Title) || k.ProjectCapital2Hours != null)
                    {
                        Log4NetService.LogInfo(k.KpiDateTimeYear + "," + k.KpiDateTimeMonth + "," + @k.CompanyName + "," + @k.SiteName + "," + @k.ProjectCapital2Title + "," + k.ProjectCapital2Hours);
                    }
                    if (!String.IsNullOrEmpty(k.ProjectCapital3Title) || k.ProjectCapital3Hours != null)
                    {
                        Log4NetService.LogInfo(k.KpiDateTimeYear + "," + k.KpiDateTimeMonth + "," + @k.CompanyName + "," + @k.SiteName + "," + @k.ProjectCapital3Title + "," + k.ProjectCapital3Hours);
                    }
                    if (!String.IsNullOrEmpty(k.ProjectCapital4Title) || k.ProjectCapital4Hours != null)
                    {
                        Log4NetService.LogInfo(k.KpiDateTimeYear + "," + k.KpiDateTimeMonth + "," + @k.CompanyName + "," + @k.SiteName + "," + @k.ProjectCapital4Title + "," + k.ProjectCapital4Hours);
                    }
                    if (!String.IsNullOrEmpty(k.ProjectCapital5Title) || k.ProjectCapital5Hours != null)
                    {
                        Log4NetService.LogInfo(k.KpiDateTimeYear + "," + k.KpiDateTimeMonth + "," + @k.CompanyName + "," + @k.SiteName + "," + @k.ProjectCapital5Title + "," + k.ProjectCapital5Hours);
                    }
                    if (!String.IsNullOrEmpty(k.ProjectCapital6Title) || k.ProjectCapital6Hours != null)
                    {
                        Log4NetService.LogInfo(k.KpiDateTimeYear + "," + k.KpiDateTimeMonth + "," + @k.CompanyName + "," + @k.SiteName + "," + @k.ProjectCapital6Title + "," + k.ProjectCapital6Hours);
                    }
                    if (!String.IsNullOrEmpty(k.ProjectCapital7Title) || k.ProjectCapital7Hours != null)
                    {
                        Log4NetService.LogInfo(k.KpiDateTimeYear + "," + k.KpiDateTimeMonth + "," + @k.CompanyName + "," + @k.SiteName + "," + @k.ProjectCapital7Title + "," + k.ProjectCapital7Hours);
                    }
                    if (!String.IsNullOrEmpty(k.ProjectCapital8Title) || k.ProjectCapital8Hours != null)
                    {
                        Log4NetService.LogInfo(k.KpiDateTimeYear + "," + k.KpiDateTimeMonth + "," + @k.CompanyName + "," + @k.SiteName + "," + @k.ProjectCapital8Title + "," + k.ProjectCapital8Hours);
                    }
                    if (!String.IsNullOrEmpty(k.ProjectCapital9Title) || k.ProjectCapital9Hours != null)
                    {
                        Log4NetService.LogInfo(k.KpiDateTimeYear + "," + k.KpiDateTimeMonth + "," + @k.CompanyName + "," + @k.SiteName + "," + @k.ProjectCapital9Title + "," + k.ProjectCapital9Hours);
                    }
                    if (!String.IsNullOrEmpty(k.ProjectCapital10Title) || k.ProjectCapital10Hours != null)
                    {
                        Log4NetService.LogInfo(k.KpiDateTimeYear + "," + k.KpiDateTimeMonth + "," + @k.CompanyName + "," + @k.SiteName + "," + @k.ProjectCapital10Title + "," + k.ProjectCapital10Hours);
                    }
                    if (!String.IsNullOrEmpty(k.ProjectCapital11Title) || k.ProjectCapital11Hours != null)
                    {
                        Log4NetService.LogInfo(k.KpiDateTimeYear + "," + k.KpiDateTimeMonth + "," + @k.CompanyName + "," + @k.SiteName + "," + @k.ProjectCapital11Title + "," + k.ProjectCapital11Hours);
                    }
                    if (!String.IsNullOrEmpty(k.ProjectCapital12Title) || k.ProjectCapital12Hours != null)
                    {
                        Log4NetService.LogInfo(k.KpiDateTimeYear + "," + k.KpiDateTimeMonth + "," + @k.CompanyName + "," + @k.SiteName + "," + @k.ProjectCapital12Title + "," + k.ProjectCapital12Hours);
                    }
                    if (!String.IsNullOrEmpty(k.ProjectCapital13Title) || k.ProjectCapital13Hours != null)
                    {
                        Log4NetService.LogInfo(k.KpiDateTimeYear + "," + k.KpiDateTimeMonth + "," + @k.CompanyName + "," + @k.SiteName + "," + @k.ProjectCapital13Title + "," + k.ProjectCapital13Hours);
                    }
                    if (!String.IsNullOrEmpty(k.ProjectCapital14Title) || k.ProjectCapital14Hours != null)
                    {
                        Log4NetService.LogInfo(k.KpiDateTimeYear + "," + k.KpiDateTimeMonth + "," + @k.CompanyName + "," + @k.SiteName + "," + @k.ProjectCapital14Title + "," + k.ProjectCapital14Hours);
                    }
                    if (!String.IsNullOrEmpty(k.ProjectCapital15Title) || k.ProjectCapital15Hours != null)
                    {
                        Log4NetService.LogInfo(k.KpiDateTimeYear + "," + k.KpiDateTimeMonth + "," + @k.CompanyName + "," + @k.SiteName + "," + @k.ProjectCapital15Title + "," + k.ProjectCapital15Hours);
                    }

                }
            }
        }
    }
}
