﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="QuestionnaireStatusProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class QuestionnaireStatusProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.QuestionnaireStatus, KaiZen.CSMS.Entities.QuestionnaireStatusKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.QuestionnaireStatusKey key)
		{
			return Delete(transactionManager, key.QuestionnaireStatusId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_questionnaireStatusId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _questionnaireStatusId)
		{
			return Delete(null, _questionnaireStatusId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireStatusId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _questionnaireStatusId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.QuestionnaireStatus Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.QuestionnaireStatusKey key, int start, int pageLength)
		{
			return GetByQuestionnaireStatusId(transactionManager, key.QuestionnaireStatusId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_QuestionnaireStatus index.
		/// </summary>
		/// <param name="_questionnaireStatusId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireStatus"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireStatus GetByQuestionnaireStatusId(System.Int32 _questionnaireStatusId)
		{
			int count = -1;
			return GetByQuestionnaireStatusId(null,_questionnaireStatusId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireStatus index.
		/// </summary>
		/// <param name="_questionnaireStatusId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireStatus"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireStatus GetByQuestionnaireStatusId(System.Int32 _questionnaireStatusId, int start, int pageLength)
		{
			int count = -1;
			return GetByQuestionnaireStatusId(null, _questionnaireStatusId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireStatus index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireStatusId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireStatus"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireStatus GetByQuestionnaireStatusId(TransactionManager transactionManager, System.Int32 _questionnaireStatusId)
		{
			int count = -1;
			return GetByQuestionnaireStatusId(transactionManager, _questionnaireStatusId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireStatus index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireStatusId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireStatus"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireStatus GetByQuestionnaireStatusId(TransactionManager transactionManager, System.Int32 _questionnaireStatusId, int start, int pageLength)
		{
			int count = -1;
			return GetByQuestionnaireStatusId(transactionManager, _questionnaireStatusId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireStatus index.
		/// </summary>
		/// <param name="_questionnaireStatusId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireStatus"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireStatus GetByQuestionnaireStatusId(System.Int32 _questionnaireStatusId, int start, int pageLength, out int count)
		{
			return GetByQuestionnaireStatusId(null, _questionnaireStatusId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireStatus index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireStatusId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireStatus"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.QuestionnaireStatus GetByQuestionnaireStatusId(TransactionManager transactionManager, System.Int32 _questionnaireStatusId, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key UQ_QuestionnaireStatusName index.
		/// </summary>
		/// <param name="_questionnaireStatusName"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireStatus"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireStatus GetByQuestionnaireStatusName(System.String _questionnaireStatusName)
		{
			int count = -1;
			return GetByQuestionnaireStatusName(null,_questionnaireStatusName, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the UQ_QuestionnaireStatusName index.
		/// </summary>
		/// <param name="_questionnaireStatusName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireStatus"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireStatus GetByQuestionnaireStatusName(System.String _questionnaireStatusName, int start, int pageLength)
		{
			int count = -1;
			return GetByQuestionnaireStatusName(null, _questionnaireStatusName, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the UQ_QuestionnaireStatusName index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireStatusName"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireStatus"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireStatus GetByQuestionnaireStatusName(TransactionManager transactionManager, System.String _questionnaireStatusName)
		{
			int count = -1;
			return GetByQuestionnaireStatusName(transactionManager, _questionnaireStatusName, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the UQ_QuestionnaireStatusName index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireStatusName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireStatus"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireStatus GetByQuestionnaireStatusName(TransactionManager transactionManager, System.String _questionnaireStatusName, int start, int pageLength)
		{
			int count = -1;
			return GetByQuestionnaireStatusName(transactionManager, _questionnaireStatusName, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the UQ_QuestionnaireStatusName index.
		/// </summary>
		/// <param name="_questionnaireStatusName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireStatus"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireStatus GetByQuestionnaireStatusName(System.String _questionnaireStatusName, int start, int pageLength, out int count)
		{
			return GetByQuestionnaireStatusName(null, _questionnaireStatusName, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the UQ_QuestionnaireStatusName index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireStatusName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireStatus"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.QuestionnaireStatus GetByQuestionnaireStatusName(TransactionManager transactionManager, System.String _questionnaireStatusName, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;QuestionnaireStatus&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;QuestionnaireStatus&gt;"/></returns>
		public static TList<QuestionnaireStatus> Fill(IDataReader reader, TList<QuestionnaireStatus> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.QuestionnaireStatus c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("QuestionnaireStatus")
					.Append("|").Append((System.Int32)reader[((int)QuestionnaireStatusColumn.QuestionnaireStatusId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<QuestionnaireStatus>(
					key.ToString(), // EntityTrackingKey
					"QuestionnaireStatus",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.QuestionnaireStatus();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.QuestionnaireStatusId = (System.Int32)reader[((int)QuestionnaireStatusColumn.QuestionnaireStatusId - 1)];
					c.QuestionnaireStatusName = (System.String)reader[((int)QuestionnaireStatusColumn.QuestionnaireStatusName - 1)];
					c.QuestionnaireStatusDesc = (System.String)reader[((int)QuestionnaireStatusColumn.QuestionnaireStatusDesc - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.QuestionnaireStatus"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.QuestionnaireStatus"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.QuestionnaireStatus entity)
		{
			if (!reader.Read()) return;
			
			entity.QuestionnaireStatusId = (System.Int32)reader[((int)QuestionnaireStatusColumn.QuestionnaireStatusId - 1)];
			entity.QuestionnaireStatusName = (System.String)reader[((int)QuestionnaireStatusColumn.QuestionnaireStatusName - 1)];
			entity.QuestionnaireStatusDesc = (System.String)reader[((int)QuestionnaireStatusColumn.QuestionnaireStatusDesc - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.QuestionnaireStatus"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.QuestionnaireStatus"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.QuestionnaireStatus entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.QuestionnaireStatusId = (System.Int32)dataRow["QuestionnaireStatusId"];
			entity.QuestionnaireStatusName = (System.String)dataRow["QuestionnaireStatusName"];
			entity.QuestionnaireStatusDesc = (System.String)dataRow["QuestionnaireStatusDesc"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.QuestionnaireStatus"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.QuestionnaireStatus Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.QuestionnaireStatus entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetByQuestionnaireStatusId methods when available
			
			#region QuestionnaireCollectionGetByStatus
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<Questionnaire>|QuestionnaireCollectionGetByStatus", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'QuestionnaireCollectionGetByStatus' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.QuestionnaireCollectionGetByStatus = DataRepository.QuestionnaireProvider.GetByStatus(transactionManager, entity.QuestionnaireStatusId);

				if (deep && entity.QuestionnaireCollectionGetByStatus.Count > 0)
				{
					deepHandles.Add("QuestionnaireCollectionGetByStatus",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<Questionnaire>) DataRepository.QuestionnaireProvider.DeepLoad,
						new object[] { transactionManager, entity.QuestionnaireCollectionGetByStatus, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region QuestionnaireCollectionGetByInitialStatus
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<Questionnaire>|QuestionnaireCollectionGetByInitialStatus", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'QuestionnaireCollectionGetByInitialStatus' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.QuestionnaireCollectionGetByInitialStatus = DataRepository.QuestionnaireProvider.GetByInitialStatus(transactionManager, entity.QuestionnaireStatusId);

				if (deep && entity.QuestionnaireCollectionGetByInitialStatus.Count > 0)
				{
					deepHandles.Add("QuestionnaireCollectionGetByInitialStatus",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<Questionnaire>) DataRepository.QuestionnaireProvider.DeepLoad,
						new object[] { transactionManager, entity.QuestionnaireCollectionGetByInitialStatus, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region QuestionnaireCollectionGetByVerificationStatus
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<Questionnaire>|QuestionnaireCollectionGetByVerificationStatus", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'QuestionnaireCollectionGetByVerificationStatus' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.QuestionnaireCollectionGetByVerificationStatus = DataRepository.QuestionnaireProvider.GetByVerificationStatus(transactionManager, entity.QuestionnaireStatusId);

				if (deep && entity.QuestionnaireCollectionGetByVerificationStatus.Count > 0)
				{
					deepHandles.Add("QuestionnaireCollectionGetByVerificationStatus",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<Questionnaire>) DataRepository.QuestionnaireProvider.DeepLoad,
						new object[] { transactionManager, entity.QuestionnaireCollectionGetByVerificationStatus, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region QuestionnaireCollectionGetByMainStatus
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<Questionnaire>|QuestionnaireCollectionGetByMainStatus", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'QuestionnaireCollectionGetByMainStatus' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.QuestionnaireCollectionGetByMainStatus = DataRepository.QuestionnaireProvider.GetByMainStatus(transactionManager, entity.QuestionnaireStatusId);

				if (deep && entity.QuestionnaireCollectionGetByMainStatus.Count > 0)
				{
					deepHandles.Add("QuestionnaireCollectionGetByMainStatus",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<Questionnaire>) DataRepository.QuestionnaireProvider.DeepLoad,
						new object[] { transactionManager, entity.QuestionnaireCollectionGetByMainStatus, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.QuestionnaireStatus object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.QuestionnaireStatus instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.QuestionnaireStatus Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.QuestionnaireStatus entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<Questionnaire>
				if (CanDeepSave(entity.QuestionnaireCollectionGetByStatus, "List<Questionnaire>|QuestionnaireCollectionGetByStatus", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(Questionnaire child in entity.QuestionnaireCollectionGetByStatus)
					{
						if(child.StatusSource != null)
						{
							child.Status = child.StatusSource.QuestionnaireStatusId;
						}
						else
						{
							child.Status = entity.QuestionnaireStatusId;
						}

					}

					if (entity.QuestionnaireCollectionGetByStatus.Count > 0 || entity.QuestionnaireCollectionGetByStatus.DeletedItems.Count > 0)
					{
						//DataRepository.QuestionnaireProvider.Save(transactionManager, entity.QuestionnaireCollectionGetByStatus);
						
						deepHandles.Add("QuestionnaireCollectionGetByStatus",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< Questionnaire >) DataRepository.QuestionnaireProvider.DeepSave,
							new object[] { transactionManager, entity.QuestionnaireCollectionGetByStatus, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<Questionnaire>
				if (CanDeepSave(entity.QuestionnaireCollectionGetByInitialStatus, "List<Questionnaire>|QuestionnaireCollectionGetByInitialStatus", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(Questionnaire child in entity.QuestionnaireCollectionGetByInitialStatus)
					{
						if(child.InitialStatusSource != null)
						{
							child.InitialStatus = child.InitialStatusSource.QuestionnaireStatusId;
						}
						else
						{
							child.InitialStatus = entity.QuestionnaireStatusId;
						}

					}

					if (entity.QuestionnaireCollectionGetByInitialStatus.Count > 0 || entity.QuestionnaireCollectionGetByInitialStatus.DeletedItems.Count > 0)
					{
						//DataRepository.QuestionnaireProvider.Save(transactionManager, entity.QuestionnaireCollectionGetByInitialStatus);
						
						deepHandles.Add("QuestionnaireCollectionGetByInitialStatus",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< Questionnaire >) DataRepository.QuestionnaireProvider.DeepSave,
							new object[] { transactionManager, entity.QuestionnaireCollectionGetByInitialStatus, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<Questionnaire>
				if (CanDeepSave(entity.QuestionnaireCollectionGetByVerificationStatus, "List<Questionnaire>|QuestionnaireCollectionGetByVerificationStatus", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(Questionnaire child in entity.QuestionnaireCollectionGetByVerificationStatus)
					{
						if(child.VerificationStatusSource != null)
						{
							child.VerificationStatus = child.VerificationStatusSource.QuestionnaireStatusId;
						}
						else
						{
							child.VerificationStatus = entity.QuestionnaireStatusId;
						}

					}

					if (entity.QuestionnaireCollectionGetByVerificationStatus.Count > 0 || entity.QuestionnaireCollectionGetByVerificationStatus.DeletedItems.Count > 0)
					{
						//DataRepository.QuestionnaireProvider.Save(transactionManager, entity.QuestionnaireCollectionGetByVerificationStatus);
						
						deepHandles.Add("QuestionnaireCollectionGetByVerificationStatus",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< Questionnaire >) DataRepository.QuestionnaireProvider.DeepSave,
							new object[] { transactionManager, entity.QuestionnaireCollectionGetByVerificationStatus, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<Questionnaire>
				if (CanDeepSave(entity.QuestionnaireCollectionGetByMainStatus, "List<Questionnaire>|QuestionnaireCollectionGetByMainStatus", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(Questionnaire child in entity.QuestionnaireCollectionGetByMainStatus)
					{
						if(child.MainStatusSource != null)
						{
							child.MainStatus = child.MainStatusSource.QuestionnaireStatusId;
						}
						else
						{
							child.MainStatus = entity.QuestionnaireStatusId;
						}

					}

					if (entity.QuestionnaireCollectionGetByMainStatus.Count > 0 || entity.QuestionnaireCollectionGetByMainStatus.DeletedItems.Count > 0)
					{
						//DataRepository.QuestionnaireProvider.Save(transactionManager, entity.QuestionnaireCollectionGetByMainStatus);
						
						deepHandles.Add("QuestionnaireCollectionGetByMainStatus",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< Questionnaire >) DataRepository.QuestionnaireProvider.DeepSave,
							new object[] { transactionManager, entity.QuestionnaireCollectionGetByMainStatus, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region QuestionnaireStatusChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.QuestionnaireStatus</c>
	///</summary>
	public enum QuestionnaireStatusChildEntityTypes
	{

		///<summary>
		/// Collection of <c>QuestionnaireStatus</c> as OneToMany for QuestionnaireCollection
		///</summary>
		[ChildEntityType(typeof(TList<Questionnaire>))]
		QuestionnaireCollectionGetByStatus,

		///<summary>
		/// Collection of <c>QuestionnaireStatus</c> as OneToMany for QuestionnaireCollection
		///</summary>
		[ChildEntityType(typeof(TList<Questionnaire>))]
		QuestionnaireCollectionGetByInitialStatus,

		///<summary>
		/// Collection of <c>QuestionnaireStatus</c> as OneToMany for QuestionnaireCollection
		///</summary>
		[ChildEntityType(typeof(TList<Questionnaire>))]
		QuestionnaireCollectionGetByVerificationStatus,

		///<summary>
		/// Collection of <c>QuestionnaireStatus</c> as OneToMany for QuestionnaireCollection
		///</summary>
		[ChildEntityType(typeof(TList<Questionnaire>))]
		QuestionnaireCollectionGetByMainStatus,
	}
	
	#endregion QuestionnaireStatusChildEntityTypes
	
	#region QuestionnaireStatusFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;QuestionnaireStatusColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireStatus"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireStatusFilterBuilder : SqlFilterBuilder<QuestionnaireStatusColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireStatusFilterBuilder class.
		/// </summary>
		public QuestionnaireStatusFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireStatusFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireStatusFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireStatusFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireStatusFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireStatusFilterBuilder
	
	#region QuestionnaireStatusParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;QuestionnaireStatusColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireStatus"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireStatusParameterBuilder : ParameterizedSqlFilterBuilder<QuestionnaireStatusColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireStatusParameterBuilder class.
		/// </summary>
		public QuestionnaireStatusParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireStatusParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireStatusParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireStatusParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireStatusParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireStatusParameterBuilder
	
	#region QuestionnaireStatusSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;QuestionnaireStatusColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireStatus"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class QuestionnaireStatusSortBuilder : SqlSortBuilder<QuestionnaireStatusColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireStatusSqlSortBuilder class.
		/// </summary>
		public QuestionnaireStatusSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion QuestionnaireStatusSortBuilder
	
} // end namespace
