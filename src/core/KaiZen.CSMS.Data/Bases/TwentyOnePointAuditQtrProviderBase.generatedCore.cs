﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="TwentyOnePointAuditQtrProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class TwentyOnePointAuditQtrProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.TwentyOnePointAuditQtr, KaiZen.CSMS.Entities.TwentyOnePointAuditQtrKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.TwentyOnePointAuditQtrKey key)
		{
			return Delete(transactionManager, key.QtrId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_qtrId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _qtrId)
		{
			return Delete(null, _qtrId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_qtrId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _qtrId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.TwentyOnePointAuditQtr Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.TwentyOnePointAuditQtrKey key, int start, int pageLength)
		{
			return GetByQtrId(transactionManager, key.QtrId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_TwentyOnePointAudit_Qtr index.
		/// </summary>
		/// <param name="_qtrId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.TwentyOnePointAuditQtr"/> class.</returns>
		public KaiZen.CSMS.Entities.TwentyOnePointAuditQtr GetByQtrId(System.Int32 _qtrId)
		{
			int count = -1;
			return GetByQtrId(null,_qtrId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_TwentyOnePointAudit_Qtr index.
		/// </summary>
		/// <param name="_qtrId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.TwentyOnePointAuditQtr"/> class.</returns>
		public KaiZen.CSMS.Entities.TwentyOnePointAuditQtr GetByQtrId(System.Int32 _qtrId, int start, int pageLength)
		{
			int count = -1;
			return GetByQtrId(null, _qtrId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_TwentyOnePointAudit_Qtr index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_qtrId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.TwentyOnePointAuditQtr"/> class.</returns>
		public KaiZen.CSMS.Entities.TwentyOnePointAuditQtr GetByQtrId(TransactionManager transactionManager, System.Int32 _qtrId)
		{
			int count = -1;
			return GetByQtrId(transactionManager, _qtrId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_TwentyOnePointAudit_Qtr index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_qtrId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.TwentyOnePointAuditQtr"/> class.</returns>
		public KaiZen.CSMS.Entities.TwentyOnePointAuditQtr GetByQtrId(TransactionManager transactionManager, System.Int32 _qtrId, int start, int pageLength)
		{
			int count = -1;
			return GetByQtrId(transactionManager, _qtrId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_TwentyOnePointAudit_Qtr index.
		/// </summary>
		/// <param name="_qtrId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.TwentyOnePointAuditQtr"/> class.</returns>
		public KaiZen.CSMS.Entities.TwentyOnePointAuditQtr GetByQtrId(System.Int32 _qtrId, int start, int pageLength, out int count)
		{
			return GetByQtrId(null, _qtrId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_TwentyOnePointAudit_Qtr index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_qtrId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.TwentyOnePointAuditQtr"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.TwentyOnePointAuditQtr GetByQtrId(TransactionManager transactionManager, System.Int32 _qtrId, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key idx_qtrName index.
		/// </summary>
		/// <param name="_qtrName"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.TwentyOnePointAuditQtr"/> class.</returns>
		public KaiZen.CSMS.Entities.TwentyOnePointAuditQtr GetByQtrName(System.String _qtrName)
		{
			int count = -1;
			return GetByQtrName(null,_qtrName, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the idx_qtrName index.
		/// </summary>
		/// <param name="_qtrName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.TwentyOnePointAuditQtr"/> class.</returns>
		public KaiZen.CSMS.Entities.TwentyOnePointAuditQtr GetByQtrName(System.String _qtrName, int start, int pageLength)
		{
			int count = -1;
			return GetByQtrName(null, _qtrName, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the idx_qtrName index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_qtrName"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.TwentyOnePointAuditQtr"/> class.</returns>
		public KaiZen.CSMS.Entities.TwentyOnePointAuditQtr GetByQtrName(TransactionManager transactionManager, System.String _qtrName)
		{
			int count = -1;
			return GetByQtrName(transactionManager, _qtrName, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the idx_qtrName index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_qtrName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.TwentyOnePointAuditQtr"/> class.</returns>
		public KaiZen.CSMS.Entities.TwentyOnePointAuditQtr GetByQtrName(TransactionManager transactionManager, System.String _qtrName, int start, int pageLength)
		{
			int count = -1;
			return GetByQtrName(transactionManager, _qtrName, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the idx_qtrName index.
		/// </summary>
		/// <param name="_qtrName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.TwentyOnePointAuditQtr"/> class.</returns>
		public KaiZen.CSMS.Entities.TwentyOnePointAuditQtr GetByQtrName(System.String _qtrName, int start, int pageLength, out int count)
		{
			return GetByQtrName(null, _qtrName, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the idx_qtrName index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_qtrName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.TwentyOnePointAuditQtr"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.TwentyOnePointAuditQtr GetByQtrName(TransactionManager transactionManager, System.String _qtrName, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;TwentyOnePointAuditQtr&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;TwentyOnePointAuditQtr&gt;"/></returns>
		public static TList<TwentyOnePointAuditQtr> Fill(IDataReader reader, TList<TwentyOnePointAuditQtr> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.TwentyOnePointAuditQtr c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("TwentyOnePointAuditQtr")
					.Append("|").Append((System.Int32)reader[((int)TwentyOnePointAuditQtrColumn.QtrId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<TwentyOnePointAuditQtr>(
					key.ToString(), // EntityTrackingKey
					"TwentyOnePointAuditQtr",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.TwentyOnePointAuditQtr();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.QtrId = (System.Int32)reader[((int)TwentyOnePointAuditQtrColumn.QtrId - 1)];
					c.QtrName = (System.String)reader[((int)TwentyOnePointAuditQtrColumn.QtrName - 1)];
					c.QtrDesc = (System.String)reader[((int)TwentyOnePointAuditQtrColumn.QtrDesc - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.TwentyOnePointAuditQtr"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.TwentyOnePointAuditQtr"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.TwentyOnePointAuditQtr entity)
		{
			if (!reader.Read()) return;
			
			entity.QtrId = (System.Int32)reader[((int)TwentyOnePointAuditQtrColumn.QtrId - 1)];
			entity.QtrName = (System.String)reader[((int)TwentyOnePointAuditQtrColumn.QtrName - 1)];
			entity.QtrDesc = (System.String)reader[((int)TwentyOnePointAuditQtrColumn.QtrDesc - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.TwentyOnePointAuditQtr"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.TwentyOnePointAuditQtr"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.TwentyOnePointAuditQtr entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.QtrId = (System.Int32)dataRow["qtrId"];
			entity.QtrName = (System.String)dataRow["qtrName"];
			entity.QtrDesc = (System.String)dataRow["qtrDesc"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.TwentyOnePointAuditQtr"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.TwentyOnePointAuditQtr Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.TwentyOnePointAuditQtr entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.TwentyOnePointAuditQtr object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.TwentyOnePointAuditQtr instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.TwentyOnePointAuditQtr Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.TwentyOnePointAuditQtr entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region TwentyOnePointAuditQtrChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.TwentyOnePointAuditQtr</c>
	///</summary>
	public enum TwentyOnePointAuditQtrChildEntityTypes
	{
	}
	
	#endregion TwentyOnePointAuditQtrChildEntityTypes
	
	#region TwentyOnePointAuditQtrFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;TwentyOnePointAuditQtrColumn&gt;"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAuditQtr"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAuditQtrFilterBuilder : SqlFilterBuilder<TwentyOnePointAuditQtrColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAuditQtrFilterBuilder class.
		/// </summary>
		public TwentyOnePointAuditQtrFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAuditQtrFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public TwentyOnePointAuditQtrFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAuditQtrFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public TwentyOnePointAuditQtrFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion TwentyOnePointAuditQtrFilterBuilder
	
	#region TwentyOnePointAuditQtrParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;TwentyOnePointAuditQtrColumn&gt;"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAuditQtr"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAuditQtrParameterBuilder : ParameterizedSqlFilterBuilder<TwentyOnePointAuditQtrColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAuditQtrParameterBuilder class.
		/// </summary>
		public TwentyOnePointAuditQtrParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAuditQtrParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public TwentyOnePointAuditQtrParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAuditQtrParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public TwentyOnePointAuditQtrParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion TwentyOnePointAuditQtrParameterBuilder
	
	#region TwentyOnePointAuditQtrSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;TwentyOnePointAuditQtrColumn&gt;"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAuditQtr"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class TwentyOnePointAuditQtrSortBuilder : SqlSortBuilder<TwentyOnePointAuditQtrColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAuditQtrSqlSortBuilder class.
		/// </summary>
		public TwentyOnePointAuditQtrSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion TwentyOnePointAuditQtrSortBuilder
	
} // end namespace
