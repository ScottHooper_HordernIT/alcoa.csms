﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/popup.master" AutoEventWireup="true"
    Inherits="PopUps_KpiProjectsList" CodeBehind="KpiProjectsList.aspx.cs" %>

<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript" language="javascript">
        function doClose(e) // note: takes the event as an arg (IE doesn't)  
        {
            if (!e) e = window.event; // fix IE  

            if (e.keyCode) // IE  
            {
                if (e.keyCode == "27") window.close();
            }
            else if (e.charCode) // Netscape/Firefox/Opera  
            {
                if (e.keyCode == "27") window.close();
            }
        }
        document.onkeydown = doClose;  
    </script>
    <div align="center">
        <dx:ASPxButton ID="ASPxButton1" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
            CssPostfix="Office2003Blue" PostBackUrl="javascript:window.close();" Text="Close (ESC)"
            SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
        </dx:ASPxButton>
        <br />
        <strong style="font-size: 14pt">List of KPI Projects with Project Status: Open</strong>
        <br />
        <br />
        If you cannot find your project (or purchase order) in this list, please contact
        <a href="mailto:AUAAlcoaContractorServices@alcoa.com.au">AUAAlcoaContractorServices@alcoa.com.au</a>
        <br />
        <br />
        <table>
            <tr>
                <td style="text-align: left">
                    <div align="left">
                        <dx:ASPxPageControl ID="ASPxPageControl1" runat="server" ActiveTabIndex="0" Width="700px"
                            Style="text-align: left" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                            CssPostfix="Office2003Blue" 
                            SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                            <TabPages>
                                <dx:TabPage Text="Project Number List">
                                    <ContentCollection>
                                        <dx:ContentControl runat="server" SupportsDisabledAttribute="True">
                                            <dx:ASPxGridView ID="gridPurchaseOrders0" runat="server" AutoGenerateColumns="False"
                                                CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue"
                                                DataSourceID="KpiPurchaseOrderListOpenProjectsDistinctAscDataSource1" Width="700px">
                                                <Columns>
                                                    <dx:GridViewDataTextColumn FieldName="ProjectNumber" ShowInCustomizationForm="True"
                                                        VisibleIndex="0" Width="94px">
                                                        <HeaderStyle Wrap="True" />
                                                        <CellStyle HorizontalAlign="Left">
                                                        </CellStyle>
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn FieldName="ProjectDesc" ShowInCustomizationForm="True"
                                                        VisibleIndex="1">
                                                        <HeaderStyle Wrap="True" />
                                                        <CellStyle HorizontalAlign="Left">
                                                        </CellStyle>
                                                    </dx:GridViewDataTextColumn>
                                                </Columns>
                                                <SettingsBehavior ColumnResizeMode="NextColumn" />
                                                <SettingsPager>
                                                    <AllButton Visible="True">
                                                    </AllButton>
                                                </SettingsPager>
                                                <Settings ShowFilterRow="True" ShowHeaderFilterButton="True" />
                                                <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                                    <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
                                                    </LoadingPanelOnStatusBar>
                                                    <LoadingPanel Url="~/App_Themes/Office2003Blue/GridView/Loading.gif">
                                                    </LoadingPanel>
                                                </Images>
                                                <ImagesFilterControl>
                                                    <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
                                                    </LoadingPanel>
                                                </ImagesFilterControl>
                                                <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue">
                                                    <Header ImageSpacing="5px" SortingImageSpacing="5px">
                                                    </Header>
                                                    <LoadingPanel ImageSpacing="10px">
                                                    </LoadingPanel>
                                                </Styles>
                                                <StylesEditors>
                                                    <ProgressBar Height="25px">
                                                    </ProgressBar>
                                                </StylesEditors>
                                            </dx:ASPxGridView>
                                        </dx:ContentControl>
                                    </ContentCollection>
                                </dx:TabPage>
                                <dx:TabPage Text="Purchase Order Number List">
                                    <ContentCollection>
                                        <dx:ContentControl runat="server" SupportsDisabledAttribute="True">
                                            <dx:ASPxGridView ID="gridPurchaseOrders" runat="server" AutoGenerateColumns="False"
                                                CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue"
                                                DataSourceID="KpiPurchaseOrderListOpenProjectsAscDataSource1" Width="700px">
                                                <Columns>
                                                    <dx:GridViewDataTextColumn FieldName="PurchaseOrderNumber" ShowInCustomizationForm="True"
                                                        VisibleIndex="1" Width="84px">
                                                        <HeaderStyle Wrap="True" />
                                                        <CellStyle HorizontalAlign="Left">
                                                        </CellStyle>
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn Caption="Line Number" FieldName="PurchaseOrderLineNumber"
                                                        ShowInCustomizationForm="True" VisibleIndex="2" Width="76px">
                                                        <HeaderStyle Wrap="True" />
                                                        <CellStyle HorizontalAlign="Left">
                                                        </CellStyle>
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn FieldName="ProjectNumber" ShowInCustomizationForm="True"
                                                        VisibleIndex="3" Width="122px">
                                                        <HeaderStyle Wrap="True" />
                                                        <CellStyle HorizontalAlign="Left">
                                                        </CellStyle>
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn FieldName="ProjectDesc" ShowInCustomizationForm="True"
                                                        VisibleIndex="4">
                                                        <HeaderStyle Wrap="True" />
                                                        <CellStyle HorizontalAlign="Left">
                                                        </CellStyle>
                                                    </dx:GridViewDataTextColumn>
                                                </Columns>
                                                <SettingsBehavior ColumnResizeMode="NextColumn" />
                                                <SettingsPager>
                                                    <AllButton Visible="True">
                                                    </AllButton>
                                                </SettingsPager>
                                                <Settings ShowFilterRow="True" ShowHeaderFilterButton="True" />
                                                <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                                    <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
                                                    </LoadingPanelOnStatusBar>
                                                    <LoadingPanel Url="~/App_Themes/Office2003Blue/GridView/Loading.gif">
                                                    </LoadingPanel>
                                                </Images>
                                                <ImagesFilterControl>
                                                    <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
                                                    </LoadingPanel>
                                                </ImagesFilterControl>
                                                <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue">
                                                    <Header ImageSpacing="5px" SortingImageSpacing="5px">
                                                    </Header>
                                                    <LoadingPanel ImageSpacing="10px">
                                                    </LoadingPanel>
                                                </Styles>
                                                <StylesEditors>
                                                    <ProgressBar Height="25px">
                                                    </ProgressBar>
                                                </StylesEditors>
                                            </dx:ASPxGridView>
                                        </dx:ContentControl>
                                    </ContentCollection>
                                </dx:TabPage>
                            </TabPages>
                            <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                            </LoadingPanelImage>
                            <LoadingPanelStyle ImageSpacing="6px">
                            </LoadingPanelStyle>
                            <ContentStyle>
                                <Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px" />
                            </ContentStyle>
                        </dx:ASPxPageControl>
                    </div>
                </td>
            </tr>
        </table>
        <data:KpiPurchaseOrderListOpenProjectsAscDataSource ID="KpiPurchaseOrderListOpenProjectsAscDataSource1"
            runat="server" CacheDuration="30" SelectMethod="GetAll">
        </data:KpiPurchaseOrderListOpenProjectsAscDataSource>
        <data:KpiPurchaseOrderListOpenProjectsDistinctAscDataSource ID="KpiPurchaseOrderListOpenProjectsDistinctAscDataSource1"
            runat="server">
        </data:KpiPurchaseOrderListOpenProjectsDistinctAscDataSource>
        <br />
        <br />
    </div>
</asp:Content>
