﻿
using System;

namespace KaiZen.CSMS.Entities
{
	/// <summary>
	/// An enum representation of the 'CompanyStatus' table. [No description found in the database]
	/// </summary>
	/// <remark>this enumeration contains the items contained in the table CompanyStatus</remark>
	[Serializable]
	public enum CompanyStatusList
	{
		/// <summary> 
		/// InActive - SQ Complete, Not recommended (in Status Report, No Access to Site)
		/// </summary>
		[EnumTextValue(@"InActive - SQ Complete, Not recommended (in Status Report, No Access to Site)")]
		InActive = 0, 

		/// <summary> 
		/// Active - SQ Complete (will appear in database, Access to Site)
		/// </summary>
		[EnumTextValue(@"Active - SQ Complete (will appear in database, Access to Site)")]
		Active = 1, 

		/// <summary> 
		/// Being Assessed - SQ Incomplete (in Progress)
		/// </summary>
		[EnumTextValue(@"Being Assessed - SQ Incomplete (in Progress)")]
		Being_Assessed = 2, 

		/// <summary> 
		/// Acceptable - SQ Complete, Recommended (in Status Report, No Access to Site)
		/// </summary>
		[EnumTextValue(@"Acceptable - SQ Complete, Recommended (in Status Report, No Access to Site)")]
		Acceptable = 3, 

		/// <summary> 
		/// SubContractor - SQ Complete, Recommended (will appear in database)
		/// </summary>
		[EnumTextValue(@"SubContractor - SQ Complete, Recommended (will appear in database)")]
		SubContractor = 4, 

		/// <summary> 
		/// InActive - No Re-Qualification Required (in Status Report, No Access to Site)
		/// </summary>
		[EnumTextValue(@"InActive - No Re-Qualification Required (in Status Report, No Access to Site)")]
		InActive_No_Requal_Required = 5, 

		/// <summary> 
		/// InActive - SQ Incomplete (in Status Report, No Access to Site)
		/// </summary>
		[EnumTextValue(@"InActive - SQ Incomplete (in Status Report, No Access to Site)")]
		InActive_SQ_Incomplete = 6, 

		/// <summary> 
		/// Re-Qualification Incomplete (in Progress)
		/// </summary>
		[EnumTextValue(@"Re-Qualification Incomplete (in Progress)")]
		Requal_Incomplete = 7, 

		/// <summary> 
		/// SubContractor - SQ Complete, Not Recommended, No Access to Site (in Status Report)
		/// </summary>
		[EnumTextValue(@"SubContractor - SQ Complete, Not Recommended, No Access to Site (in Status Report)")]
		SubContractor_Not_Recommended = 8, 

		/// <summary> 
		/// Active - No PQ Required, No Access to Site (in Status Report)
		/// </summary>
		[EnumTextValue(@"Active - No PQ Required, No Access to Site (in Status Report)")]
		Active_No_PQ_Required = 9, 

		/// <summary> 
		/// Active - No PQ Required, Technical Professional / Low Level Delivery (in Database)
		/// </summary>
		[EnumTextValue(@"Active - No PQ Required, Technical Professional / Low Level Delivery (in Database)")]
		Active_No_PQ_Required_Restricted_Access_to_Site = 10

	}
}
