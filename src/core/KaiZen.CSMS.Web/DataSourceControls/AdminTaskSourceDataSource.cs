﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.AdminTaskSourceProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(AdminTaskSourceDataSourceDesigner))]
	public class AdminTaskSourceDataSource : ProviderDataSource<AdminTaskSource, AdminTaskSourceKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AdminTaskSourceDataSource class.
		/// </summary>
		public AdminTaskSourceDataSource() : base(new AdminTaskSourceService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the AdminTaskSourceDataSourceView used by the AdminTaskSourceDataSource.
		/// </summary>
		protected AdminTaskSourceDataSourceView AdminTaskSourceView
		{
			get { return ( View as AdminTaskSourceDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the AdminTaskSourceDataSource control invokes to retrieve data.
		/// </summary>
		public AdminTaskSourceSelectMethod SelectMethod
		{
			get
			{
				AdminTaskSourceSelectMethod selectMethod = AdminTaskSourceSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (AdminTaskSourceSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the AdminTaskSourceDataSourceView class that is to be
		/// used by the AdminTaskSourceDataSource.
		/// </summary>
		/// <returns>An instance of the AdminTaskSourceDataSourceView class.</returns>
		protected override BaseDataSourceView<AdminTaskSource, AdminTaskSourceKey> GetNewDataSourceView()
		{
			return new AdminTaskSourceDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the AdminTaskSourceDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class AdminTaskSourceDataSourceView : ProviderDataSourceView<AdminTaskSource, AdminTaskSourceKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AdminTaskSourceDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the AdminTaskSourceDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public AdminTaskSourceDataSourceView(AdminTaskSourceDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal AdminTaskSourceDataSource AdminTaskSourceOwner
		{
			get { return Owner as AdminTaskSourceDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal AdminTaskSourceSelectMethod SelectMethod
		{
			get { return AdminTaskSourceOwner.SelectMethod; }
			set { AdminTaskSourceOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal AdminTaskSourceService AdminTaskSourceProvider
		{
			get { return Provider as AdminTaskSourceService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<AdminTaskSource> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<AdminTaskSource> results = null;
			AdminTaskSource item;
			count = 0;
			
			System.Int32 _adminTaskSourceId;
			System.String _adminTaskSourceName;

			switch ( SelectMethod )
			{
				case AdminTaskSourceSelectMethod.Get:
					AdminTaskSourceKey entityKey  = new AdminTaskSourceKey();
					entityKey.Load(values);
					item = AdminTaskSourceProvider.Get(entityKey);
					results = new TList<AdminTaskSource>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case AdminTaskSourceSelectMethod.GetAll:
                    results = AdminTaskSourceProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case AdminTaskSourceSelectMethod.GetPaged:
					results = AdminTaskSourceProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case AdminTaskSourceSelectMethod.Find:
					if ( FilterParameters != null )
						results = AdminTaskSourceProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = AdminTaskSourceProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case AdminTaskSourceSelectMethod.GetByAdminTaskSourceId:
					_adminTaskSourceId = ( values["AdminTaskSourceId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["AdminTaskSourceId"], typeof(System.Int32)) : (int)0;
					item = AdminTaskSourceProvider.GetByAdminTaskSourceId(_adminTaskSourceId);
					results = new TList<AdminTaskSource>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				case AdminTaskSourceSelectMethod.GetByAdminTaskSourceName:
					_adminTaskSourceName = ( values["AdminTaskSourceName"] != null ) ? (System.String) EntityUtil.ChangeType(values["AdminTaskSourceName"], typeof(System.String)) : string.Empty;
					item = AdminTaskSourceProvider.GetByAdminTaskSourceName(_adminTaskSourceName);
					results = new TList<AdminTaskSource>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// FK
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == AdminTaskSourceSelectMethod.Get || SelectMethod == AdminTaskSourceSelectMethod.GetByAdminTaskSourceId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				AdminTaskSource entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					AdminTaskSourceProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<AdminTaskSource> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			AdminTaskSourceProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region AdminTaskSourceDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the AdminTaskSourceDataSource class.
	/// </summary>
	public class AdminTaskSourceDataSourceDesigner : ProviderDataSourceDesigner<AdminTaskSource, AdminTaskSourceKey>
	{
		/// <summary>
		/// Initializes a new instance of the AdminTaskSourceDataSourceDesigner class.
		/// </summary>
		public AdminTaskSourceDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public AdminTaskSourceSelectMethod SelectMethod
		{
			get { return ((AdminTaskSourceDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new AdminTaskSourceDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region AdminTaskSourceDataSourceActionList

	/// <summary>
	/// Supports the AdminTaskSourceDataSourceDesigner class.
	/// </summary>
	internal class AdminTaskSourceDataSourceActionList : DesignerActionList
	{
		private AdminTaskSourceDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the AdminTaskSourceDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public AdminTaskSourceDataSourceActionList(AdminTaskSourceDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public AdminTaskSourceSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion AdminTaskSourceDataSourceActionList
	
	#endregion AdminTaskSourceDataSourceDesigner
	
	#region AdminTaskSourceSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the AdminTaskSourceDataSource.SelectMethod property.
	/// </summary>
	public enum AdminTaskSourceSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByAdminTaskSourceId method.
		/// </summary>
		GetByAdminTaskSourceId,
		/// <summary>
		/// Represents the GetByAdminTaskSourceName method.
		/// </summary>
		GetByAdminTaskSourceName
	}
	
	#endregion AdminTaskSourceSelectMethod

	#region AdminTaskSourceFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AdminTaskSource"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AdminTaskSourceFilter : SqlFilter<AdminTaskSourceColumn>
	{
	}
	
	#endregion AdminTaskSourceFilter

	#region AdminTaskSourceExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AdminTaskSource"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AdminTaskSourceExpressionBuilder : SqlExpressionBuilder<AdminTaskSourceColumn>
	{
	}
	
	#endregion AdminTaskSourceExpressionBuilder	

	#region AdminTaskSourceProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;AdminTaskSourceChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="AdminTaskSource"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AdminTaskSourceProperty : ChildEntityProperty<AdminTaskSourceChildEntityTypes>
	{
	}
	
	#endregion AdminTaskSourceProperty
}

