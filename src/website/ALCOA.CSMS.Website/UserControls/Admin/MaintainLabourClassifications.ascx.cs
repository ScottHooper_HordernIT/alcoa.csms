﻿using DevExpress.Web.ASPxGridView;
using DevExpress.XtraPrinting;
using KaiZen.CSMS.Entities;
using Repo.CSMS.DAL.EntityModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using dal = Repo.CSMS.DAL.EntityModels;
using repo = Repo.CSMS.Service.Database;


namespace ALCOA.CSMS.Website.UserControls.Admin
{
    public partial class MaintainLabourClassifications : System.Web.UI.UserControl
    {
        Auth auth = new Auth();
        repo.ILabourClassTypeService labourClassTypeSvc = ALCOA.CSMS.Website.Global.GetInstance<repo.ILabourClassTypeService>();

        protected void Page_Load(object sender, EventArgs e)
        {
            Helper._Auth.PageLoad(auth);
            moduleInit(Page.IsPostBack);
        }

        private void moduleInit(bool postBack)
        {
            if(!postBack)
            {
                switch(auth.RoleId)
                {
                case (int)RoleList.Administrator:
                    break;

                case (int)RoleList.Reader:
                case (int)RoleList.Contractor:
                case (int)RoleList.PreQual:
                    Response.Redirect(String.Format("AccessDenied.aspx?error={0}", Server.UrlEncode("Unauthorized user")));
                    break;

                default:
                    // RoleList.Disabled or No Role
                    Response.Redirect(String.Format("AccessDenied.aspx?error={0}", Server.UrlEncode("Unrecognised user")));
                    break;
                }

                // Export
                String exportFileName = @"ALCOA CSMS - Maintain Labour Classifications";                                    //Chrome & IE is fine.
                String userAgent = Request.Headers.Get("User-Agent");
                if ((userAgent.Contains("Mozilla") && !userAgent.Contains("AppleWebKit") && !userAgent.Contains("MSIE")) || //Mozilla
                    (userAgent.Contains("AppleWebKit") && !userAgent.Contains("Chrome")))                                   //Safari
                {
                    exportFileName = exportFileName.Replace(" ", "_");
                }
                Helper.ExportGrid.Settings(ucExportButtons, exportFileName);
            }
            else
            {
            }
            LoadGridData();
        }

        private void LoadGridData()
        {
            grid.DataSource = labourClassTypeSvc.GetMany(null, null, o => o.OrderBy(l => l.LabourClassTypeId), null);
            grid.DataBind();
        }

        protected void grid_CellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
        {
            if(grid.IsNewRowEditing)
            {
                if(e.Column.FieldName == "LabourClassTypeIsActive")
                {
                    e.Editor.Value = true;
                }
            }
        }

        protected void grid_CommandButtonInitialize(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewCommandButtonEventArgs e)
        {
            int? labourClassTypeId = (int?)grid.GetRowValues(e.VisibleIndex, "LabourClassTypeId");
            if(!labourClassTypeId.HasValue)
                labourClassTypeId = -1;

            // dont allow edit for Unknown class
            if(labourClassTypeId == 0)
            {
                switch(e.ButtonType)
                {
                case ColumnCommandButtonType.Edit:
                case ColumnCommandButtonType.New:
                    e.Visible = false;
                    break;
                }
            }
        }

        protected void grid_RowValidating(object sender, DevExpress.Web.Data.ASPxDataValidationEventArgs e)
        {
            int? labourClassTypeId = (int?)grid.GetRowValues(e.VisibleIndex, "LabourClassTypeId");
            if(!labourClassTypeId.HasValue)
                labourClassTypeId = -1;
            string labourClassTypeDesc = e.NewValues["LabourClassTypeDesc"].ToString();

            List<LabourClassType> all = labourClassTypeSvc.GetMany(null, null, o => o.OrderBy(l => l.LabourClassTypeId), null);
            foreach(LabourClassType one in all)
            {
                if(labourClassTypeId != one.LabourClassTypeId && string.Equals(labourClassTypeDesc, one.LabourClassTypeDesc, StringComparison.OrdinalIgnoreCase))
                {
                    e.Errors[grid.Columns["LabourClassTypeDesc"]] = String.Format("Labour class '{0}' already exists", labourClassTypeDesc);
                    break;
                }
            }
        }

        protected void grid_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
        {
            int labourClassTypeId = (int)e.Keys["LabourClassTypeId"];

            dal.LabourClassType lct = labourClassTypeSvc.Get(lc => lc.LabourClassTypeId == labourClassTypeId, null);
            // this should never happen, but...
            if(lct == null)
                throw new Exception("MaintainLabourClassifications.grid_RowUpdating NULL LabourClassType returned");

            lct.LabourClassTypeDesc = e.NewValues["LabourClassTypeDesc"].ToString();
            lct.LabourClassTypeIsActive = (bool)e.NewValues["LabourClassTypeIsActive"];
            
            labourClassTypeSvc.Update(lct, true);

            e.Cancel = true;
            grid.CancelEdit();
            LoadGridData();
        }

        protected void grid_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
        {
            int? nextId = labourClassTypeSvc.GetMax(null, lc => lc.LabourClassTypeId, null);
            if(!nextId.HasValue)
                nextId = 0;
            nextId++;

            dal.LabourClassType lct = new LabourClassType();
            lct.LabourClassTypeId = (int)nextId;
            lct.LabourClassTypeDesc = e.NewValues["LabourClassTypeDesc"].ToString();
            lct.LabourClassTypeIsActive = (bool)e.NewValues["LabourClassTypeIsActive"];

            labourClassTypeSvc.Insert(lct, true);

            e.Cancel = true;
            grid.CancelEdit();
            LoadGridData();
        }
    }
}
