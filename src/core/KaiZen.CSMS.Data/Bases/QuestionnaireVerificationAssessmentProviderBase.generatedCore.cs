﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="QuestionnaireVerificationAssessmentProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class QuestionnaireVerificationAssessmentProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.QuestionnaireVerificationAssessment, KaiZen.CSMS.Entities.QuestionnaireVerificationAssessmentKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.QuestionnaireVerificationAssessmentKey key)
		{
			return Delete(transactionManager, key.ResponseId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_responseId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _responseId)
		{
			return Delete(null, _responseId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_responseId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _responseId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireVerificationAssessment_Questionnaire key.
		///		FK_QuestionnaireVerificationAssessment_Questionnaire Description: 
		/// </summary>
		/// <param name="_questionnaireId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireVerificationAssessment objects.</returns>
		public TList<QuestionnaireVerificationAssessment> GetByQuestionnaireId(System.Int32 _questionnaireId)
		{
			int count = -1;
			return GetByQuestionnaireId(_questionnaireId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireVerificationAssessment_Questionnaire key.
		///		FK_QuestionnaireVerificationAssessment_Questionnaire Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireVerificationAssessment objects.</returns>
		/// <remarks></remarks>
		public TList<QuestionnaireVerificationAssessment> GetByQuestionnaireId(TransactionManager transactionManager, System.Int32 _questionnaireId)
		{
			int count = -1;
			return GetByQuestionnaireId(transactionManager, _questionnaireId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireVerificationAssessment_Questionnaire key.
		///		FK_QuestionnaireVerificationAssessment_Questionnaire Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireVerificationAssessment objects.</returns>
		public TList<QuestionnaireVerificationAssessment> GetByQuestionnaireId(TransactionManager transactionManager, System.Int32 _questionnaireId, int start, int pageLength)
		{
			int count = -1;
			return GetByQuestionnaireId(transactionManager, _questionnaireId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireVerificationAssessment_Questionnaire key.
		///		fkQuestionnaireVerificationAssessmentQuestionnaire Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_questionnaireId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireVerificationAssessment objects.</returns>
		public TList<QuestionnaireVerificationAssessment> GetByQuestionnaireId(System.Int32 _questionnaireId, int start, int pageLength)
		{
			int count =  -1;
			return GetByQuestionnaireId(null, _questionnaireId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireVerificationAssessment_Questionnaire key.
		///		fkQuestionnaireVerificationAssessmentQuestionnaire Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_questionnaireId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireVerificationAssessment objects.</returns>
		public TList<QuestionnaireVerificationAssessment> GetByQuestionnaireId(System.Int32 _questionnaireId, int start, int pageLength,out int count)
		{
			return GetByQuestionnaireId(null, _questionnaireId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireVerificationAssessment_Questionnaire key.
		///		FK_QuestionnaireVerificationAssessment_Questionnaire Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireVerificationAssessment objects.</returns>
		public abstract TList<QuestionnaireVerificationAssessment> GetByQuestionnaireId(TransactionManager transactionManager, System.Int32 _questionnaireId, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.QuestionnaireVerificationAssessment Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.QuestionnaireVerificationAssessmentKey key, int start, int pageLength)
		{
			return GetByResponseId(transactionManager, key.ResponseId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_QuestionnaireVerificationAssessment index.
		/// </summary>
		/// <param name="_responseId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireVerificationAssessment"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireVerificationAssessment GetByResponseId(System.Int32 _responseId)
		{
			int count = -1;
			return GetByResponseId(null,_responseId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireVerificationAssessment index.
		/// </summary>
		/// <param name="_responseId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireVerificationAssessment"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireVerificationAssessment GetByResponseId(System.Int32 _responseId, int start, int pageLength)
		{
			int count = -1;
			return GetByResponseId(null, _responseId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireVerificationAssessment index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_responseId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireVerificationAssessment"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireVerificationAssessment GetByResponseId(TransactionManager transactionManager, System.Int32 _responseId)
		{
			int count = -1;
			return GetByResponseId(transactionManager, _responseId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireVerificationAssessment index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_responseId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireVerificationAssessment"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireVerificationAssessment GetByResponseId(TransactionManager transactionManager, System.Int32 _responseId, int start, int pageLength)
		{
			int count = -1;
			return GetByResponseId(transactionManager, _responseId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireVerificationAssessment index.
		/// </summary>
		/// <param name="_responseId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireVerificationAssessment"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireVerificationAssessment GetByResponseId(System.Int32 _responseId, int start, int pageLength, out int count)
		{
			return GetByResponseId(null, _responseId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireVerificationAssessment index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_responseId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireVerificationAssessment"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.QuestionnaireVerificationAssessment GetByResponseId(TransactionManager transactionManager, System.Int32 _responseId, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_QuestionnaireVerificationAssessment index.
		/// </summary>
		/// <param name="_questionnaireId"></param>
		/// <param name="_sectionId"></param>
		/// <param name="_questionId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireVerificationAssessment"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireVerificationAssessment GetByQuestionnaireIdSectionIdQuestionId(System.Int32 _questionnaireId, System.Int32 _sectionId, System.Int32 _questionId)
		{
			int count = -1;
			return GetByQuestionnaireIdSectionIdQuestionId(null,_questionnaireId, _sectionId, _questionId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireVerificationAssessment index.
		/// </summary>
		/// <param name="_questionnaireId"></param>
		/// <param name="_sectionId"></param>
		/// <param name="_questionId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireVerificationAssessment"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireVerificationAssessment GetByQuestionnaireIdSectionIdQuestionId(System.Int32 _questionnaireId, System.Int32 _sectionId, System.Int32 _questionId, int start, int pageLength)
		{
			int count = -1;
			return GetByQuestionnaireIdSectionIdQuestionId(null, _questionnaireId, _sectionId, _questionId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireVerificationAssessment index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireId"></param>
		/// <param name="_sectionId"></param>
		/// <param name="_questionId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireVerificationAssessment"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireVerificationAssessment GetByQuestionnaireIdSectionIdQuestionId(TransactionManager transactionManager, System.Int32 _questionnaireId, System.Int32 _sectionId, System.Int32 _questionId)
		{
			int count = -1;
			return GetByQuestionnaireIdSectionIdQuestionId(transactionManager, _questionnaireId, _sectionId, _questionId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireVerificationAssessment index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireId"></param>
		/// <param name="_sectionId"></param>
		/// <param name="_questionId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireVerificationAssessment"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireVerificationAssessment GetByQuestionnaireIdSectionIdQuestionId(TransactionManager transactionManager, System.Int32 _questionnaireId, System.Int32 _sectionId, System.Int32 _questionId, int start, int pageLength)
		{
			int count = -1;
			return GetByQuestionnaireIdSectionIdQuestionId(transactionManager, _questionnaireId, _sectionId, _questionId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireVerificationAssessment index.
		/// </summary>
		/// <param name="_questionnaireId"></param>
		/// <param name="_sectionId"></param>
		/// <param name="_questionId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireVerificationAssessment"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireVerificationAssessment GetByQuestionnaireIdSectionIdQuestionId(System.Int32 _questionnaireId, System.Int32 _sectionId, System.Int32 _questionId, int start, int pageLength, out int count)
		{
			return GetByQuestionnaireIdSectionIdQuestionId(null, _questionnaireId, _sectionId, _questionId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireVerificationAssessment index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireId"></param>
		/// <param name="_sectionId"></param>
		/// <param name="_questionId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireVerificationAssessment"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.QuestionnaireVerificationAssessment GetByQuestionnaireIdSectionIdQuestionId(TransactionManager transactionManager, System.Int32 _questionnaireId, System.Int32 _sectionId, System.Int32 _questionId, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_QuestionnaireVerificationAssessment_1 index.
		/// </summary>
		/// <param name="_modifiedByUserId"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;QuestionnaireVerificationAssessment&gt;"/> class.</returns>
		public TList<QuestionnaireVerificationAssessment> GetByModifiedByUserId(System.Int32 _modifiedByUserId)
		{
			int count = -1;
			return GetByModifiedByUserId(null,_modifiedByUserId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireVerificationAssessment_1 index.
		/// </summary>
		/// <param name="_modifiedByUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;QuestionnaireVerificationAssessment&gt;"/> class.</returns>
		public TList<QuestionnaireVerificationAssessment> GetByModifiedByUserId(System.Int32 _modifiedByUserId, int start, int pageLength)
		{
			int count = -1;
			return GetByModifiedByUserId(null, _modifiedByUserId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireVerificationAssessment_1 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_modifiedByUserId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;QuestionnaireVerificationAssessment&gt;"/> class.</returns>
		public TList<QuestionnaireVerificationAssessment> GetByModifiedByUserId(TransactionManager transactionManager, System.Int32 _modifiedByUserId)
		{
			int count = -1;
			return GetByModifiedByUserId(transactionManager, _modifiedByUserId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireVerificationAssessment_1 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_modifiedByUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;QuestionnaireVerificationAssessment&gt;"/> class.</returns>
		public TList<QuestionnaireVerificationAssessment> GetByModifiedByUserId(TransactionManager transactionManager, System.Int32 _modifiedByUserId, int start, int pageLength)
		{
			int count = -1;
			return GetByModifiedByUserId(transactionManager, _modifiedByUserId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireVerificationAssessment_1 index.
		/// </summary>
		/// <param name="_modifiedByUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;QuestionnaireVerificationAssessment&gt;"/> class.</returns>
		public TList<QuestionnaireVerificationAssessment> GetByModifiedByUserId(System.Int32 _modifiedByUserId, int start, int pageLength, out int count)
		{
			return GetByModifiedByUserId(null, _modifiedByUserId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireVerificationAssessment_1 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_modifiedByUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;QuestionnaireVerificationAssessment&gt;"/> class.</returns>
		public abstract TList<QuestionnaireVerificationAssessment> GetByModifiedByUserId(TransactionManager transactionManager, System.Int32 _modifiedByUserId, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_QuestionnaireVerificationAssessment_2 index.
		/// </summary>
		/// <param name="_questionnaireId"></param>
		/// <param name="_assessorApproval"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;QuestionnaireVerificationAssessment&gt;"/> class.</returns>
		public TList<QuestionnaireVerificationAssessment> GetByQuestionnaireIdAssessorApproval(System.Int32 _questionnaireId, System.Boolean? _assessorApproval)
		{
			int count = -1;
			return GetByQuestionnaireIdAssessorApproval(null,_questionnaireId, _assessorApproval, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireVerificationAssessment_2 index.
		/// </summary>
		/// <param name="_questionnaireId"></param>
		/// <param name="_assessorApproval"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;QuestionnaireVerificationAssessment&gt;"/> class.</returns>
		public TList<QuestionnaireVerificationAssessment> GetByQuestionnaireIdAssessorApproval(System.Int32 _questionnaireId, System.Boolean? _assessorApproval, int start, int pageLength)
		{
			int count = -1;
			return GetByQuestionnaireIdAssessorApproval(null, _questionnaireId, _assessorApproval, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireVerificationAssessment_2 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireId"></param>
		/// <param name="_assessorApproval"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;QuestionnaireVerificationAssessment&gt;"/> class.</returns>
		public TList<QuestionnaireVerificationAssessment> GetByQuestionnaireIdAssessorApproval(TransactionManager transactionManager, System.Int32 _questionnaireId, System.Boolean? _assessorApproval)
		{
			int count = -1;
			return GetByQuestionnaireIdAssessorApproval(transactionManager, _questionnaireId, _assessorApproval, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireVerificationAssessment_2 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireId"></param>
		/// <param name="_assessorApproval"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;QuestionnaireVerificationAssessment&gt;"/> class.</returns>
		public TList<QuestionnaireVerificationAssessment> GetByQuestionnaireIdAssessorApproval(TransactionManager transactionManager, System.Int32 _questionnaireId, System.Boolean? _assessorApproval, int start, int pageLength)
		{
			int count = -1;
			return GetByQuestionnaireIdAssessorApproval(transactionManager, _questionnaireId, _assessorApproval, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireVerificationAssessment_2 index.
		/// </summary>
		/// <param name="_questionnaireId"></param>
		/// <param name="_assessorApproval"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;QuestionnaireVerificationAssessment&gt;"/> class.</returns>
		public TList<QuestionnaireVerificationAssessment> GetByQuestionnaireIdAssessorApproval(System.Int32 _questionnaireId, System.Boolean? _assessorApproval, int start, int pageLength, out int count)
		{
			return GetByQuestionnaireIdAssessorApproval(null, _questionnaireId, _assessorApproval, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireVerificationAssessment_2 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireId"></param>
		/// <param name="_assessorApproval"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;QuestionnaireVerificationAssessment&gt;"/> class.</returns>
		public abstract TList<QuestionnaireVerificationAssessment> GetByQuestionnaireIdAssessorApproval(TransactionManager transactionManager, System.Int32 _questionnaireId, System.Boolean? _assessorApproval, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_QuestionnaireVerificationAssessment_3 index.
		/// </summary>
		/// <param name="_questionnaireId"></param>
		/// <param name="_sectionId"></param>
		/// <param name="_assessorApproval"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;QuestionnaireVerificationAssessment&gt;"/> class.</returns>
		public TList<QuestionnaireVerificationAssessment> GetByQuestionnaireIdSectionIdAssessorApproval(System.Int32 _questionnaireId, System.Int32 _sectionId, System.Boolean? _assessorApproval)
		{
			int count = -1;
			return GetByQuestionnaireIdSectionIdAssessorApproval(null,_questionnaireId, _sectionId, _assessorApproval, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireVerificationAssessment_3 index.
		/// </summary>
		/// <param name="_questionnaireId"></param>
		/// <param name="_sectionId"></param>
		/// <param name="_assessorApproval"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;QuestionnaireVerificationAssessment&gt;"/> class.</returns>
		public TList<QuestionnaireVerificationAssessment> GetByQuestionnaireIdSectionIdAssessorApproval(System.Int32 _questionnaireId, System.Int32 _sectionId, System.Boolean? _assessorApproval, int start, int pageLength)
		{
			int count = -1;
			return GetByQuestionnaireIdSectionIdAssessorApproval(null, _questionnaireId, _sectionId, _assessorApproval, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireVerificationAssessment_3 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireId"></param>
		/// <param name="_sectionId"></param>
		/// <param name="_assessorApproval"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;QuestionnaireVerificationAssessment&gt;"/> class.</returns>
		public TList<QuestionnaireVerificationAssessment> GetByQuestionnaireIdSectionIdAssessorApproval(TransactionManager transactionManager, System.Int32 _questionnaireId, System.Int32 _sectionId, System.Boolean? _assessorApproval)
		{
			int count = -1;
			return GetByQuestionnaireIdSectionIdAssessorApproval(transactionManager, _questionnaireId, _sectionId, _assessorApproval, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireVerificationAssessment_3 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireId"></param>
		/// <param name="_sectionId"></param>
		/// <param name="_assessorApproval"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;QuestionnaireVerificationAssessment&gt;"/> class.</returns>
		public TList<QuestionnaireVerificationAssessment> GetByQuestionnaireIdSectionIdAssessorApproval(TransactionManager transactionManager, System.Int32 _questionnaireId, System.Int32 _sectionId, System.Boolean? _assessorApproval, int start, int pageLength)
		{
			int count = -1;
			return GetByQuestionnaireIdSectionIdAssessorApproval(transactionManager, _questionnaireId, _sectionId, _assessorApproval, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireVerificationAssessment_3 index.
		/// </summary>
		/// <param name="_questionnaireId"></param>
		/// <param name="_sectionId"></param>
		/// <param name="_assessorApproval"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;QuestionnaireVerificationAssessment&gt;"/> class.</returns>
		public TList<QuestionnaireVerificationAssessment> GetByQuestionnaireIdSectionIdAssessorApproval(System.Int32 _questionnaireId, System.Int32 _sectionId, System.Boolean? _assessorApproval, int start, int pageLength, out int count)
		{
			return GetByQuestionnaireIdSectionIdAssessorApproval(null, _questionnaireId, _sectionId, _assessorApproval, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireVerificationAssessment_3 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireId"></param>
		/// <param name="_sectionId"></param>
		/// <param name="_assessorApproval"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;QuestionnaireVerificationAssessment&gt;"/> class.</returns>
		public abstract TList<QuestionnaireVerificationAssessment> GetByQuestionnaireIdSectionIdAssessorApproval(TransactionManager transactionManager, System.Int32 _questionnaireId, System.Int32 _sectionId, System.Boolean? _assessorApproval, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_QuestionnaireVerificationAssessment_4 index.
		/// </summary>
		/// <param name="_questionnaireId"></param>
		/// <param name="_sectionId"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;QuestionnaireVerificationAssessment&gt;"/> class.</returns>
		public TList<QuestionnaireVerificationAssessment> GetByQuestionnaireIdSectionId(System.Int32 _questionnaireId, System.Int32 _sectionId)
		{
			int count = -1;
			return GetByQuestionnaireIdSectionId(null,_questionnaireId, _sectionId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireVerificationAssessment_4 index.
		/// </summary>
		/// <param name="_questionnaireId"></param>
		/// <param name="_sectionId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;QuestionnaireVerificationAssessment&gt;"/> class.</returns>
		public TList<QuestionnaireVerificationAssessment> GetByQuestionnaireIdSectionId(System.Int32 _questionnaireId, System.Int32 _sectionId, int start, int pageLength)
		{
			int count = -1;
			return GetByQuestionnaireIdSectionId(null, _questionnaireId, _sectionId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireVerificationAssessment_4 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireId"></param>
		/// <param name="_sectionId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;QuestionnaireVerificationAssessment&gt;"/> class.</returns>
		public TList<QuestionnaireVerificationAssessment> GetByQuestionnaireIdSectionId(TransactionManager transactionManager, System.Int32 _questionnaireId, System.Int32 _sectionId)
		{
			int count = -1;
			return GetByQuestionnaireIdSectionId(transactionManager, _questionnaireId, _sectionId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireVerificationAssessment_4 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireId"></param>
		/// <param name="_sectionId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;QuestionnaireVerificationAssessment&gt;"/> class.</returns>
		public TList<QuestionnaireVerificationAssessment> GetByQuestionnaireIdSectionId(TransactionManager transactionManager, System.Int32 _questionnaireId, System.Int32 _sectionId, int start, int pageLength)
		{
			int count = -1;
			return GetByQuestionnaireIdSectionId(transactionManager, _questionnaireId, _sectionId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireVerificationAssessment_4 index.
		/// </summary>
		/// <param name="_questionnaireId"></param>
		/// <param name="_sectionId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;QuestionnaireVerificationAssessment&gt;"/> class.</returns>
		public TList<QuestionnaireVerificationAssessment> GetByQuestionnaireIdSectionId(System.Int32 _questionnaireId, System.Int32 _sectionId, int start, int pageLength, out int count)
		{
			return GetByQuestionnaireIdSectionId(null, _questionnaireId, _sectionId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireVerificationAssessment_4 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireId"></param>
		/// <param name="_sectionId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;QuestionnaireVerificationAssessment&gt;"/> class.</returns>
		public abstract TList<QuestionnaireVerificationAssessment> GetByQuestionnaireIdSectionId(TransactionManager transactionManager, System.Int32 _questionnaireId, System.Int32 _sectionId, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;QuestionnaireVerificationAssessment&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;QuestionnaireVerificationAssessment&gt;"/></returns>
		public static TList<QuestionnaireVerificationAssessment> Fill(IDataReader reader, TList<QuestionnaireVerificationAssessment> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.QuestionnaireVerificationAssessment c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("QuestionnaireVerificationAssessment")
					.Append("|").Append((System.Int32)reader[((int)QuestionnaireVerificationAssessmentColumn.ResponseId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<QuestionnaireVerificationAssessment>(
					key.ToString(), // EntityTrackingKey
					"QuestionnaireVerificationAssessment",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.QuestionnaireVerificationAssessment();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.ResponseId = (System.Int32)reader[((int)QuestionnaireVerificationAssessmentColumn.ResponseId - 1)];
					c.QuestionnaireId = (System.Int32)reader[((int)QuestionnaireVerificationAssessmentColumn.QuestionnaireId - 1)];
					c.SectionId = (System.Int32)reader[((int)QuestionnaireVerificationAssessmentColumn.SectionId - 1)];
					c.QuestionId = (System.Int32)reader[((int)QuestionnaireVerificationAssessmentColumn.QuestionId - 1)];
					c.AssessorApproval = (reader.IsDBNull(((int)QuestionnaireVerificationAssessmentColumn.AssessorApproval - 1)))?null:(System.Boolean?)reader[((int)QuestionnaireVerificationAssessmentColumn.AssessorApproval - 1)];
					c.AssessorComment = (reader.IsDBNull(((int)QuestionnaireVerificationAssessmentColumn.AssessorComment - 1)))?null:(System.String)reader[((int)QuestionnaireVerificationAssessmentColumn.AssessorComment - 1)];
					c.ModifiedByUserId = (System.Int32)reader[((int)QuestionnaireVerificationAssessmentColumn.ModifiedByUserId - 1)];
					c.ModifiedDate = (System.DateTime)reader[((int)QuestionnaireVerificationAssessmentColumn.ModifiedDate - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.QuestionnaireVerificationAssessment"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.QuestionnaireVerificationAssessment"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.QuestionnaireVerificationAssessment entity)
		{
			if (!reader.Read()) return;
			
			entity.ResponseId = (System.Int32)reader[((int)QuestionnaireVerificationAssessmentColumn.ResponseId - 1)];
			entity.QuestionnaireId = (System.Int32)reader[((int)QuestionnaireVerificationAssessmentColumn.QuestionnaireId - 1)];
			entity.SectionId = (System.Int32)reader[((int)QuestionnaireVerificationAssessmentColumn.SectionId - 1)];
			entity.QuestionId = (System.Int32)reader[((int)QuestionnaireVerificationAssessmentColumn.QuestionId - 1)];
			entity.AssessorApproval = (reader.IsDBNull(((int)QuestionnaireVerificationAssessmentColumn.AssessorApproval - 1)))?null:(System.Boolean?)reader[((int)QuestionnaireVerificationAssessmentColumn.AssessorApproval - 1)];
			entity.AssessorComment = (reader.IsDBNull(((int)QuestionnaireVerificationAssessmentColumn.AssessorComment - 1)))?null:(System.String)reader[((int)QuestionnaireVerificationAssessmentColumn.AssessorComment - 1)];
			entity.ModifiedByUserId = (System.Int32)reader[((int)QuestionnaireVerificationAssessmentColumn.ModifiedByUserId - 1)];
			entity.ModifiedDate = (System.DateTime)reader[((int)QuestionnaireVerificationAssessmentColumn.ModifiedDate - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.QuestionnaireVerificationAssessment"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.QuestionnaireVerificationAssessment"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.QuestionnaireVerificationAssessment entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.ResponseId = (System.Int32)dataRow["ResponseId"];
			entity.QuestionnaireId = (System.Int32)dataRow["QuestionnaireId"];
			entity.SectionId = (System.Int32)dataRow["SectionId"];
			entity.QuestionId = (System.Int32)dataRow["QuestionId"];
			entity.AssessorApproval = Convert.IsDBNull(dataRow["AssessorApproval"]) ? null : (System.Boolean?)dataRow["AssessorApproval"];
			entity.AssessorComment = Convert.IsDBNull(dataRow["AssessorComment"]) ? null : (System.String)dataRow["AssessorComment"];
			entity.ModifiedByUserId = (System.Int32)dataRow["ModifiedByUserId"];
			entity.ModifiedDate = (System.DateTime)dataRow["ModifiedDate"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.QuestionnaireVerificationAssessment"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.QuestionnaireVerificationAssessment Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.QuestionnaireVerificationAssessment entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region QuestionnaireIdSource	
			if (CanDeepLoad(entity, "Questionnaire|QuestionnaireIdSource", deepLoadType, innerList) 
				&& entity.QuestionnaireIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.QuestionnaireId;
				Questionnaire tmpEntity = EntityManager.LocateEntity<Questionnaire>(EntityLocator.ConstructKeyFromPkItems(typeof(Questionnaire), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.QuestionnaireIdSource = tmpEntity;
				else
					entity.QuestionnaireIdSource = DataRepository.QuestionnaireProvider.GetByQuestionnaireId(transactionManager, entity.QuestionnaireId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'QuestionnaireIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.QuestionnaireIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.QuestionnaireProvider.DeepLoad(transactionManager, entity.QuestionnaireIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion QuestionnaireIdSource

			#region ModifiedByUserIdSource	
			if (CanDeepLoad(entity, "Users|ModifiedByUserIdSource", deepLoadType, innerList) 
				&& entity.ModifiedByUserIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.ModifiedByUserId;
				Users tmpEntity = EntityManager.LocateEntity<Users>(EntityLocator.ConstructKeyFromPkItems(typeof(Users), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.ModifiedByUserIdSource = tmpEntity;
				else
					entity.ModifiedByUserIdSource = DataRepository.UsersProvider.GetByUserId(transactionManager, entity.ModifiedByUserId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ModifiedByUserIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.ModifiedByUserIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.UsersProvider.DeepLoad(transactionManager, entity.ModifiedByUserIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion ModifiedByUserIdSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.QuestionnaireVerificationAssessment object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.QuestionnaireVerificationAssessment instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.QuestionnaireVerificationAssessment Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.QuestionnaireVerificationAssessment entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region QuestionnaireIdSource
			if (CanDeepSave(entity, "Questionnaire|QuestionnaireIdSource", deepSaveType, innerList) 
				&& entity.QuestionnaireIdSource != null)
			{
				DataRepository.QuestionnaireProvider.Save(transactionManager, entity.QuestionnaireIdSource);
				entity.QuestionnaireId = entity.QuestionnaireIdSource.QuestionnaireId;
			}
			#endregion 
			
			#region ModifiedByUserIdSource
			if (CanDeepSave(entity, "Users|ModifiedByUserIdSource", deepSaveType, innerList) 
				&& entity.ModifiedByUserIdSource != null)
			{
				DataRepository.UsersProvider.Save(transactionManager, entity.ModifiedByUserIdSource);
				entity.ModifiedByUserId = entity.ModifiedByUserIdSource.UserId;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region QuestionnaireVerificationAssessmentChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.QuestionnaireVerificationAssessment</c>
	///</summary>
	public enum QuestionnaireVerificationAssessmentChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>Questionnaire</c> at QuestionnaireIdSource
		///</summary>
		[ChildEntityType(typeof(Questionnaire))]
		Questionnaire,
			
		///<summary>
		/// Composite Property for <c>Users</c> at ModifiedByUserIdSource
		///</summary>
		[ChildEntityType(typeof(Users))]
		Users,
		}
	
	#endregion QuestionnaireVerificationAssessmentChildEntityTypes
	
	#region QuestionnaireVerificationAssessmentFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;QuestionnaireVerificationAssessmentColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireVerificationAssessment"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireVerificationAssessmentFilterBuilder : SqlFilterBuilder<QuestionnaireVerificationAssessmentColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireVerificationAssessmentFilterBuilder class.
		/// </summary>
		public QuestionnaireVerificationAssessmentFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireVerificationAssessmentFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireVerificationAssessmentFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireVerificationAssessmentFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireVerificationAssessmentFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireVerificationAssessmentFilterBuilder
	
	#region QuestionnaireVerificationAssessmentParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;QuestionnaireVerificationAssessmentColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireVerificationAssessment"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireVerificationAssessmentParameterBuilder : ParameterizedSqlFilterBuilder<QuestionnaireVerificationAssessmentColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireVerificationAssessmentParameterBuilder class.
		/// </summary>
		public QuestionnaireVerificationAssessmentParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireVerificationAssessmentParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireVerificationAssessmentParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireVerificationAssessmentParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireVerificationAssessmentParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireVerificationAssessmentParameterBuilder
	
	#region QuestionnaireVerificationAssessmentSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;QuestionnaireVerificationAssessmentColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireVerificationAssessment"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class QuestionnaireVerificationAssessmentSortBuilder : SqlSortBuilder<QuestionnaireVerificationAssessmentColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireVerificationAssessmentSqlSortBuilder class.
		/// </summary>
		public QuestionnaireVerificationAssessmentSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion QuestionnaireVerificationAssessmentSortBuilder
	
} // end namespace
