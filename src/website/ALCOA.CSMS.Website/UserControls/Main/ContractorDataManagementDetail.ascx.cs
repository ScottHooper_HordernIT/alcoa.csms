﻿using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.Data;
using DevExpress.XtraPrinting;
using KaiZen.CSMS.Entities;
using OfficeOpenXml;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.Service.Database;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using dal = Repo.CSMS.DAL.EntityModels;
using repo = Repo.CSMS.Service.Database;


namespace ALCOA.CSMS.Website.UserControls.Main
{
    public partial class ContractorDataManagementDetail : System.Web.UI.UserControl
    {
        Auth auth = new Auth();
        bool isAdmin;
        repo.Hr.IXxhrCwkHistoryService cwkHistorySvc = ALCOA.CSMS.Website.Global.GetInstance<repo.Hr.IXxhrCwkHistoryService>();
        repo.Hr.IXxhrAddTrngService addTrainingSvc = ALCOA.CSMS.Website.Global.GetInstance<repo.Hr.IXxhrAddTrngService>();

        protected void Page_Load(object sender, EventArgs e)
        {
            Helper._Auth.PageLoad(auth);
            moduleInit(Page.IsPostBack);

            //ScriptManager scriptManager = ScriptManager.GetCurrent(this.Page);
            //scriptManager.RegisterPostBackControl(this.imgBtnExcelAll);
        }

        private void moduleInit(bool postBack)
        {
            isAdmin = auth.RoleId == (int)RoleList.Administrator;

            if (!postBack)
            {
                switch (auth.RoleId)
                {
                    case (int)RoleList.CWKMaintainer:
                    case (int)RoleList.Administrator:
                        //Can access
                        break;

                    case (int)RoleList.CWKEnquiry:
                        //Can access limited
                        break;

                    default:
                        Response.Redirect(String.Format("AccessDenied.aspx?error={0}", Server.UrlEncode("Unrecognised user")));
                        break;
                }
            }
            else
            {

            }

            string CWK_NBR = this.Request.QueryString["CWK_NBR"];

            if (!string.IsNullOrEmpty(CWK_NBR))
            {
                if (CWK_NBR.ToLower() == "new")
                {
                    this.ShowContent(true, "");

                    //Add contractor
                    this.BreadcrumbLiteral.Text = "Add Contractor";
                    this.ASPxPageControl1.TabPages[1].Visible = false;  //Can't add training/induction data until contractor added
                }
                else
                {
                    //Update contractor
                    this.BreadcrumbLiteral.Text = "Update Contractor";

                    if (!string.IsNullOrEmpty(this.Request.QueryString["EFFECTIVE_START_DATE"]))
                    {
                        try
                        {
                            DateTime EFFECTIVE_START_DATE = System.Convert.ToDateTime(this.Request.QueryString["EFFECTIVE_START_DATE"]).Date;

                            var cwkHistory = this.cwkHistorySvc.Get(e => e.CWK_NBR == CWK_NBR && e.EFFECTIVE_START_DATE == EFFECTIVE_START_DATE, null);

                            if (cwkHistory != null)
                            {
                                this.ShowContent(true, "");
                            }
                            else
                            {
                                this.ShowContent(false, "Could not find this contractor in the database");
                            }
                        }
                        catch (Exception ex)
                        {
                            this.ShowContent(false, ex.Message);
                        }
                    }
                    else
                    {
                        this.ShowContent(false, "Missing required querystring");
                    }
                }
            }
            else
            {
                this.ShowContent(false, "Missing required querystring");
            }
        }

        private void ShowContent(bool show = false, string msg = "")
        {
            this.ContentPlaceHolder.Visible = show;
            this.NoContentPlaceHolder.Visible = !show;
            this.NoContentLiteral.Text = msg;
        }
    }
}
