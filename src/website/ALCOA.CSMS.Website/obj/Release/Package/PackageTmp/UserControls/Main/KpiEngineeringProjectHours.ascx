﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="UserControls_Main_KpiEngineeringProjectHours" Codebehind="KpiEngineeringProjectHours.ascx.cs" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register Src="../Other/ExportButtons.ascx" TagName="ExportButtons" TagPrefix="uc1" %>
<table border="0" cellpadding="2" cellspacing="0" width="100%">
    <tr>
        <td class="pageName" colspan="3">
            <span class="bodycopy"><span class="title">Performance Management > Reports/Charts
            </span>
                <br>
                <span class="date">Kpi Engineering Project Hours</span>
                <br />
                <img src="images/grfc_dottedline.gif" alt="..." width="24" height="1">
                <br>
            </span>
        </td>
    </tr>
    <tr>
    <td>
<dx:ASPxGridView ID="grid" runat="server" AutoGenerateColumns="False" 
    CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
    CssPostfix="Office2003Blue" 
    Width="900px">
    <SettingsBehavior ColumnResizeMode="NextColumn" />
    <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
        CssPostfix="Office2003Blue">
        <Header ImageSpacing="5px" SortingImageSpacing="5px">
        </Header>
        <LoadingPanel ImageSpacing="10px">
        </LoadingPanel>
    </Styles>
    <SettingsPager>
        <AllButton Visible="True">
        </AllButton>
    </SettingsPager>
    <ImagesFilterControl>
        <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
        </LoadingPanel>
    </ImagesFilterControl>
    <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
        <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
        </LoadingPanelOnStatusBar>
        <LoadingPanel Url="~/App_Themes/Office2003BlueOld/GridView/Loading.gif"><%--Enhancement_023:change by debashis for testing platform upgradation--%>
        </LoadingPanel>
    </Images>
    <Columns>
        <dx:GridViewCommandColumn Visible="False" VisibleIndex="0">
            <ClearFilterButton Visible="True">
            </ClearFilterButton>
        </dx:GridViewCommandColumn>
        <dx:GridViewDataTextColumn Caption="Year" FieldName="kpiDateTimeYear" 
            FixedStyle="Left" VisibleIndex="1" Width="70px" SortIndex="0" SortOrder="Descending">
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn Caption="Month" FieldName="kpiDateTimeMonth" 
            FixedStyle="Left" VisibleIndex="2" Width="75px" SortIndex="1" SortOrder="Descending">
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataDateColumn Caption="Date" FieldName="kpiDateTime"
            VisibleIndex="3" FixedStyle="Left" Visible="false">
        </dx:GridViewDataDateColumn>
        <dx:GridViewDataTextColumn Caption="Company" FieldName="CompanyName" 
            FixedStyle="Left" VisibleIndex="4" SortIndex="2" SortOrder="Ascending" Width="250px">
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn Caption="Site" FieldName="SiteName" 
            FixedStyle="Left" VisibleIndex="5" SortIndex="3" SortOrder="Ascending" Width="150px">
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn Caption="Project" 
            FieldName="ProjectName" VisibleIndex="6" Width="256px">
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn Caption="Hours" 
            FieldName="ProjectHours" VisibleIndex="7" Width="65px">
        </dx:GridViewDataTextColumn>

    </Columns>
    <Settings ShowFilterRow="True" ShowGroupPanel="True" 
        ShowHorizontalScrollBar="True" ShowFilterBar="Hidden" 
        ShowFilterRowMenu="False" ShowHeaderFilterButton="True" />
    <StylesEditors>
        <ProgressBar Height="25px">
        </ProgressBar>
    </StylesEditors>
</dx:ASPxGridView>
</td>
</tr>
    <tr align="right">
        <td colspan="3" style="height: 35px; text-align: right; text-align: -moz-right;"
            align="right">
            <div align="right">
                <uc1:ExportButtons ID="ucExportButtons" runat="server" />
            </div>
        </td>
    </tr>
    </table>