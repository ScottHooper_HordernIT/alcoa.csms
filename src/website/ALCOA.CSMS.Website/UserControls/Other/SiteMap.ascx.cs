using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using DevExpress.Web.ASPxSiteMapControl;
using System.Collections.Specialized;

public partial class UserControls_Tiny_SiteMap : System.Web.UI.UserControl
{
    Auth auth = new Auth();
    protected void Page_Load(object sender, EventArgs e) {  Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); }

    private void moduleInit(bool postBack)
    {
        //int i = ASPxSiteMapControl1.RootNodes.Count;
        //ASPxMenu1.Items.FindByText("Victorian Safety Management Plan Process").Enabled = false;

        

        SiteMapDataSource dataSource = Helper.Navigation.GenerateSiteMapHierarchy(auth.RoleId, auth.UserId);
        dataSource.ShowStartingNode = false;

        ASPxSiteMapControl1.DataSource = dataSource;
        ASPxSiteMapControl1.DataBind();

        ASPxMenu.DataSource = dataSource;
        ASPxMenu.DataBind();
    }

}
