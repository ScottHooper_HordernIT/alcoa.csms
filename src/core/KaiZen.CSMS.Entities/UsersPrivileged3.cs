﻿#region Using directives

using System;

#endregion

namespace KaiZen.CSMS.Entities
{	
	///<summary>
	/// An object representation of the 'UsersPrivileged3' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class UsersPrivileged3 : UsersPrivileged3Base
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="UsersPrivileged3"/> instance.
		///</summary>
		public UsersPrivileged3():base(){}	
		
		#endregion
	}
}
