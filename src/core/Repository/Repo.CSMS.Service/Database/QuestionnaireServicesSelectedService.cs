﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface IQuestionnaireServicesSelectedService
    {
        QuestionnaireServicesSelected Get(Expression<Func<QuestionnaireServicesSelected, bool>> where, List<Expression<Func<QuestionnaireServicesSelected, object>>> includes);
        List<QuestionnaireServicesSelected> GetMany(int? top, Expression<Func<QuestionnaireServicesSelected, bool>> where, Func<IQueryable<QuestionnaireServicesSelected>, IOrderedQueryable<QuestionnaireServicesSelected>> orderBy, List<Expression<Func<QuestionnaireServicesSelected, object>>> includes);
        List<QuestionnaireServicesSelected> GetManyPaged(int pageNumber, int pageSize, Expression<Func<QuestionnaireServicesSelected, bool>> where, Func<IQueryable<QuestionnaireServicesSelected>, IOrderedQueryable<QuestionnaireServicesSelected>> orderBy, List<Expression<Func<QuestionnaireServicesSelected, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<QuestionnaireServicesSelected, bool>> where, List<Expression<Func<QuestionnaireServicesSelected, object>>> includes);
        void Insert(QuestionnaireServicesSelected item, bool saveChanges = true);
        void Update(QuestionnaireServicesSelected item, bool saveChanges = true);
        void Delete(QuestionnaireServicesSelected item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class QuestionnaireServicesSelectedService : IQuestionnaireServicesSelectedService
    {
        private readonly IRepository<QuestionnaireServicesSelected> _repository;

        public QuestionnaireServicesSelectedService(IRepository<QuestionnaireServicesSelected> repository)
        {
            this._repository = repository;
        }

        public QuestionnaireServicesSelected Get(Expression<Func<QuestionnaireServicesSelected, bool>> where, List<Expression<Func<QuestionnaireServicesSelected, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<QuestionnaireServicesSelected> GetMany(int? top, Expression<Func<QuestionnaireServicesSelected, bool>> where, Func<IQueryable<QuestionnaireServicesSelected>, IOrderedQueryable<QuestionnaireServicesSelected>> orderBy, List<Expression<Func<QuestionnaireServicesSelected, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<QuestionnaireServicesSelected> GetManyPaged(int pageNumber, int pageSize, Expression<Func<QuestionnaireServicesSelected, bool>> where, Func<IQueryable<QuestionnaireServicesSelected>, IOrderedQueryable<QuestionnaireServicesSelected>> orderBy, List<Expression<Func<QuestionnaireServicesSelected, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<QuestionnaireServicesSelected, bool>> where, List<Expression<Func<QuestionnaireServicesSelected, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(QuestionnaireServicesSelected item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("QuestionnaireServicesSelected");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(QuestionnaireServicesSelected item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("QuestionnaireServicesSelected");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(QuestionnaireServicesSelected item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("QuestionnaireServicesSelected");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
