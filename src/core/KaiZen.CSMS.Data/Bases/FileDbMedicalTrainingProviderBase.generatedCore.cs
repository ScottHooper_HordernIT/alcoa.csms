﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="FileDbMedicalTrainingProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class FileDbMedicalTrainingProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.FileDbMedicalTraining, KaiZen.CSMS.Entities.FileDbMedicalTrainingKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.FileDbMedicalTrainingKey key)
		{
			return Delete(transactionManager, key.FileId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_fileId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _fileId)
		{
			return Delete(null, _fileId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_fileId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _fileId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_FileDBMedicalTraining_Companies key.
		///		FK_FileDBMedicalTraining_Companies Description: 
		/// </summary>
		/// <param name="_companyId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.FileDbMedicalTraining objects.</returns>
		public TList<FileDbMedicalTraining> GetByCompanyId(System.Int32 _companyId)
		{
			int count = -1;
			return GetByCompanyId(_companyId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_FileDBMedicalTraining_Companies key.
		///		FK_FileDBMedicalTraining_Companies Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.FileDbMedicalTraining objects.</returns>
		/// <remarks></remarks>
		public TList<FileDbMedicalTraining> GetByCompanyId(TransactionManager transactionManager, System.Int32 _companyId)
		{
			int count = -1;
			return GetByCompanyId(transactionManager, _companyId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_FileDBMedicalTraining_Companies key.
		///		FK_FileDBMedicalTraining_Companies Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.FileDbMedicalTraining objects.</returns>
		public TList<FileDbMedicalTraining> GetByCompanyId(TransactionManager transactionManager, System.Int32 _companyId, int start, int pageLength)
		{
			int count = -1;
			return GetByCompanyId(transactionManager, _companyId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_FileDBMedicalTraining_Companies key.
		///		fkFileDbMedicalTrainingCompanies Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_companyId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.FileDbMedicalTraining objects.</returns>
		public TList<FileDbMedicalTraining> GetByCompanyId(System.Int32 _companyId, int start, int pageLength)
		{
			int count =  -1;
			return GetByCompanyId(null, _companyId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_FileDBMedicalTraining_Companies key.
		///		fkFileDbMedicalTrainingCompanies Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_companyId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.FileDbMedicalTraining objects.</returns>
		public TList<FileDbMedicalTraining> GetByCompanyId(System.Int32 _companyId, int start, int pageLength,out int count)
		{
			return GetByCompanyId(null, _companyId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_FileDBMedicalTraining_Companies key.
		///		FK_FileDBMedicalTraining_Companies Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.FileDbMedicalTraining objects.</returns>
		public abstract TList<FileDbMedicalTraining> GetByCompanyId(TransactionManager transactionManager, System.Int32 _companyId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_FileDBMedicalTraining_Users key.
		///		FK_FileDBMedicalTraining_Users Description: 
		/// </summary>
		/// <param name="_modifiedByUserId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.FileDbMedicalTraining objects.</returns>
		public TList<FileDbMedicalTraining> GetByModifiedByUserId(System.Int32 _modifiedByUserId)
		{
			int count = -1;
			return GetByModifiedByUserId(_modifiedByUserId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_FileDBMedicalTraining_Users key.
		///		FK_FileDBMedicalTraining_Users Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_modifiedByUserId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.FileDbMedicalTraining objects.</returns>
		/// <remarks></remarks>
		public TList<FileDbMedicalTraining> GetByModifiedByUserId(TransactionManager transactionManager, System.Int32 _modifiedByUserId)
		{
			int count = -1;
			return GetByModifiedByUserId(transactionManager, _modifiedByUserId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_FileDBMedicalTraining_Users key.
		///		FK_FileDBMedicalTraining_Users Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_modifiedByUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.FileDbMedicalTraining objects.</returns>
		public TList<FileDbMedicalTraining> GetByModifiedByUserId(TransactionManager transactionManager, System.Int32 _modifiedByUserId, int start, int pageLength)
		{
			int count = -1;
			return GetByModifiedByUserId(transactionManager, _modifiedByUserId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_FileDBMedicalTraining_Users key.
		///		fkFileDbMedicalTrainingUsers Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_modifiedByUserId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.FileDbMedicalTraining objects.</returns>
		public TList<FileDbMedicalTraining> GetByModifiedByUserId(System.Int32 _modifiedByUserId, int start, int pageLength)
		{
			int count =  -1;
			return GetByModifiedByUserId(null, _modifiedByUserId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_FileDBMedicalTraining_Users key.
		///		fkFileDbMedicalTrainingUsers Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_modifiedByUserId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.FileDbMedicalTraining objects.</returns>
		public TList<FileDbMedicalTraining> GetByModifiedByUserId(System.Int32 _modifiedByUserId, int start, int pageLength,out int count)
		{
			return GetByModifiedByUserId(null, _modifiedByUserId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_FileDBMedicalTraining_Users key.
		///		FK_FileDBMedicalTraining_Users Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_modifiedByUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.FileDbMedicalTraining objects.</returns>
		public abstract TList<FileDbMedicalTraining> GetByModifiedByUserId(TransactionManager transactionManager, System.Int32 _modifiedByUserId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_FileDbMedicalTraining_Regions key.
		///		FK_FileDbMedicalTraining_Regions Description: 
		/// </summary>
		/// <param name="_regionId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.FileDbMedicalTraining objects.</returns>
		public TList<FileDbMedicalTraining> GetByRegionId(System.Int32 _regionId)
		{
			int count = -1;
			return GetByRegionId(_regionId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_FileDbMedicalTraining_Regions key.
		///		FK_FileDbMedicalTraining_Regions Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_regionId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.FileDbMedicalTraining objects.</returns>
		/// <remarks></remarks>
		public TList<FileDbMedicalTraining> GetByRegionId(TransactionManager transactionManager, System.Int32 _regionId)
		{
			int count = -1;
			return GetByRegionId(transactionManager, _regionId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_FileDbMedicalTraining_Regions key.
		///		FK_FileDbMedicalTraining_Regions Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_regionId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.FileDbMedicalTraining objects.</returns>
		public TList<FileDbMedicalTraining> GetByRegionId(TransactionManager transactionManager, System.Int32 _regionId, int start, int pageLength)
		{
			int count = -1;
			return GetByRegionId(transactionManager, _regionId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_FileDbMedicalTraining_Regions key.
		///		fkFileDbMedicalTrainingRegions Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_regionId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.FileDbMedicalTraining objects.</returns>
		public TList<FileDbMedicalTraining> GetByRegionId(System.Int32 _regionId, int start, int pageLength)
		{
			int count =  -1;
			return GetByRegionId(null, _regionId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_FileDbMedicalTraining_Regions key.
		///		fkFileDbMedicalTrainingRegions Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_regionId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.FileDbMedicalTraining objects.</returns>
		public TList<FileDbMedicalTraining> GetByRegionId(System.Int32 _regionId, int start, int pageLength,out int count)
		{
			return GetByRegionId(null, _regionId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_FileDbMedicalTraining_Regions key.
		///		FK_FileDbMedicalTraining_Regions Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_regionId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.FileDbMedicalTraining objects.</returns>
		public abstract TList<FileDbMedicalTraining> GetByRegionId(TransactionManager transactionManager, System.Int32 _regionId, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.FileDbMedicalTraining Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.FileDbMedicalTrainingKey key, int start, int pageLength)
		{
			return GetByFileId(transactionManager, key.FileId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_FileDBMedicalTraining index.
		/// </summary>
		/// <param name="_fileId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.FileDbMedicalTraining"/> class.</returns>
		public KaiZen.CSMS.Entities.FileDbMedicalTraining GetByFileId(System.Int32 _fileId)
		{
			int count = -1;
			return GetByFileId(null,_fileId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_FileDBMedicalTraining index.
		/// </summary>
		/// <param name="_fileId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.FileDbMedicalTraining"/> class.</returns>
		public KaiZen.CSMS.Entities.FileDbMedicalTraining GetByFileId(System.Int32 _fileId, int start, int pageLength)
		{
			int count = -1;
			return GetByFileId(null, _fileId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_FileDBMedicalTraining index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_fileId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.FileDbMedicalTraining"/> class.</returns>
		public KaiZen.CSMS.Entities.FileDbMedicalTraining GetByFileId(TransactionManager transactionManager, System.Int32 _fileId)
		{
			int count = -1;
			return GetByFileId(transactionManager, _fileId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_FileDBMedicalTraining index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_fileId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.FileDbMedicalTraining"/> class.</returns>
		public KaiZen.CSMS.Entities.FileDbMedicalTraining GetByFileId(TransactionManager transactionManager, System.Int32 _fileId, int start, int pageLength)
		{
			int count = -1;
			return GetByFileId(transactionManager, _fileId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_FileDBMedicalTraining index.
		/// </summary>
		/// <param name="_fileId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.FileDbMedicalTraining"/> class.</returns>
		public KaiZen.CSMS.Entities.FileDbMedicalTraining GetByFileId(System.Int32 _fileId, int start, int pageLength, out int count)
		{
			return GetByFileId(null, _fileId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_FileDBMedicalTraining index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_fileId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.FileDbMedicalTraining"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.FileDbMedicalTraining GetByFileId(TransactionManager transactionManager, System.Int32 _fileId, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#region _FileDbMedicalTraining_ComplianceReport_BySite 
		
		/// <summary>
		///	This method wrap the '_FileDbMedicalTraining_ComplianceReport_BySite' stored procedure. 
		/// </summary>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="type"> A <c>System.String</c> instance.</param>
		/// <param name="siteId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companySiteCategoryId"> A <c>System.String</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet ComplianceReport_BySite(System.Int32? year, System.String type, System.Int32? siteId, System.String companySiteCategoryId)
		{
			return ComplianceReport_BySite(null, 0, int.MaxValue , year, type, siteId, companySiteCategoryId);
		}
		
		/// <summary>
		///	This method wrap the '_FileDbMedicalTraining_ComplianceReport_BySite' stored procedure. 
		/// </summary>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="type"> A <c>System.String</c> instance.</param>
		/// <param name="siteId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companySiteCategoryId"> A <c>System.String</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet ComplianceReport_BySite(int start, int pageLength, System.Int32? year, System.String type, System.Int32? siteId, System.String companySiteCategoryId)
		{
			return ComplianceReport_BySite(null, start, pageLength , year, type, siteId, companySiteCategoryId);
		}
				
		/// <summary>
		///	This method wrap the '_FileDbMedicalTraining_ComplianceReport_BySite' stored procedure. 
		/// </summary>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="type"> A <c>System.String</c> instance.</param>
		/// <param name="siteId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companySiteCategoryId"> A <c>System.String</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet ComplianceReport_BySite(TransactionManager transactionManager, System.Int32? year, System.String type, System.Int32? siteId, System.String companySiteCategoryId)
		{
			return ComplianceReport_BySite(transactionManager, 0, int.MaxValue , year, type, siteId, companySiteCategoryId);
		}
		
		/// <summary>
		///	This method wrap the '_FileDbMedicalTraining_ComplianceReport_BySite' stored procedure. 
		/// </summary>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="type"> A <c>System.String</c> instance.</param>
		/// <param name="siteId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companySiteCategoryId"> A <c>System.String</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet ComplianceReport_BySite(TransactionManager transactionManager, int start, int pageLength , System.Int32? year, System.String type, System.Int32? siteId, System.String companySiteCategoryId);
		
		#endregion
		
		#region _FileDbMedicalTraining_GetByType_Custom 
		
		/// <summary>
		///	This method wrap the '_FileDbMedicalTraining_GetByType_Custom' stored procedure. 
		/// </summary>
		/// <param name="type"> A <c>System.String</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetByType_Custom(System.String type)
		{
			return GetByType_Custom(null, 0, int.MaxValue , type);
		}
		
		/// <summary>
		///	This method wrap the '_FileDbMedicalTraining_GetByType_Custom' stored procedure. 
		/// </summary>
		/// <param name="type"> A <c>System.String</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetByType_Custom(int start, int pageLength, System.String type)
		{
			return GetByType_Custom(null, start, pageLength , type);
		}
				
		/// <summary>
		///	This method wrap the '_FileDbMedicalTraining_GetByType_Custom' stored procedure. 
		/// </summary>
		/// <param name="type"> A <c>System.String</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetByType_Custom(TransactionManager transactionManager, System.String type)
		{
			return GetByType_Custom(transactionManager, 0, int.MaxValue , type);
		}
		
		/// <summary>
		///	This method wrap the '_FileDbMedicalTraining_GetByType_Custom' stored procedure. 
		/// </summary>
		/// <param name="type"> A <c>System.String</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet GetByType_Custom(TransactionManager transactionManager, int start, int pageLength , System.String type);
		
		#endregion
		
		#region _FileDbMedicalTraining_ComplianceReport 
		
		/// <summary>
		///	This method wrap the '_FileDbMedicalTraining_ComplianceReport' stored procedure. 
		/// </summary>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="type"> A <c>System.String</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet ComplianceReport(System.Int32? year, System.String type)
		{
			return ComplianceReport(null, 0, int.MaxValue , year, type);
		}
		
		/// <summary>
		///	This method wrap the '_FileDbMedicalTraining_ComplianceReport' stored procedure. 
		/// </summary>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="type"> A <c>System.String</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet ComplianceReport(int start, int pageLength, System.Int32? year, System.String type)
		{
			return ComplianceReport(null, start, pageLength , year, type);
		}
				
		/// <summary>
		///	This method wrap the '_FileDbMedicalTraining_ComplianceReport' stored procedure. 
		/// </summary>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="type"> A <c>System.String</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet ComplianceReport(TransactionManager transactionManager, System.Int32? year, System.String type)
		{
			return ComplianceReport(transactionManager, 0, int.MaxValue , year, type);
		}
		
		/// <summary>
		///	This method wrap the '_FileDbMedicalTraining_ComplianceReport' stored procedure. 
		/// </summary>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="type"> A <c>System.String</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet ComplianceReport(TransactionManager transactionManager, int start, int pageLength , System.Int32? year, System.String type);
		
		#endregion
		
		#region _FileDbMedicalTraining_ComplianceReport_ByCompany 
		
		/// <summary>
		///	This method wrap the '_FileDbMedicalTraining_ComplianceReport_ByCompany' stored procedure. 
		/// </summary>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="type"> A <c>System.String</c> instance.</param>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="siteId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companySiteCategoryId"> A <c>System.String</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet ComplianceReport_ByCompany(System.Int32? year, System.String type, System.Int32? companyId, System.Int32? siteId, System.String companySiteCategoryId)
		{
			return ComplianceReport_ByCompany(null, 0, int.MaxValue , year, type, companyId, siteId, companySiteCategoryId);
		}
		
		/// <summary>
		///	This method wrap the '_FileDbMedicalTraining_ComplianceReport_ByCompany' stored procedure. 
		/// </summary>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="type"> A <c>System.String</c> instance.</param>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="siteId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companySiteCategoryId"> A <c>System.String</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet ComplianceReport_ByCompany(int start, int pageLength, System.Int32? year, System.String type, System.Int32? companyId, System.Int32? siteId, System.String companySiteCategoryId)
		{
			return ComplianceReport_ByCompany(null, start, pageLength , year, type, companyId, siteId, companySiteCategoryId);
		}
				
		/// <summary>
		///	This method wrap the '_FileDbMedicalTraining_ComplianceReport_ByCompany' stored procedure. 
		/// </summary>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="type"> A <c>System.String</c> instance.</param>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="siteId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companySiteCategoryId"> A <c>System.String</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet ComplianceReport_ByCompany(TransactionManager transactionManager, System.Int32? year, System.String type, System.Int32? companyId, System.Int32? siteId, System.String companySiteCategoryId)
		{
			return ComplianceReport_ByCompany(transactionManager, 0, int.MaxValue , year, type, companyId, siteId, companySiteCategoryId);
		}
		
		/// <summary>
		///	This method wrap the '_FileDbMedicalTraining_ComplianceReport_ByCompany' stored procedure. 
		/// </summary>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="type"> A <c>System.String</c> instance.</param>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="siteId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companySiteCategoryId"> A <c>System.String</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet ComplianceReport_ByCompany(TransactionManager transactionManager, int start, int pageLength , System.Int32? year, System.String type, System.Int32? companyId, System.Int32? siteId, System.String companySiteCategoryId);
		
		#endregion
		
		#region _FileDbMedicalTraining_GetByCompanyIdType_Custom 
		
		/// <summary>
		///	This method wrap the '_FileDbMedicalTraining_GetByCompanyIdType_Custom' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="type"> A <c>System.String</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetByCompanyIdType_Custom(System.Int32? companyId, System.String type)
		{
			return GetByCompanyIdType_Custom(null, 0, int.MaxValue , companyId, type);
		}
		
		/// <summary>
		///	This method wrap the '_FileDbMedicalTraining_GetByCompanyIdType_Custom' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="type"> A <c>System.String</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetByCompanyIdType_Custom(int start, int pageLength, System.Int32? companyId, System.String type)
		{
			return GetByCompanyIdType_Custom(null, start, pageLength , companyId, type);
		}
				
		/// <summary>
		///	This method wrap the '_FileDbMedicalTraining_GetByCompanyIdType_Custom' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="type"> A <c>System.String</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetByCompanyIdType_Custom(TransactionManager transactionManager, System.Int32? companyId, System.String type)
		{
			return GetByCompanyIdType_Custom(transactionManager, 0, int.MaxValue , companyId, type);
		}
		
		/// <summary>
		///	This method wrap the '_FileDbMedicalTraining_GetByCompanyIdType_Custom' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="type"> A <c>System.String</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet GetByCompanyIdType_Custom(TransactionManager transactionManager, int start, int pageLength , System.Int32? companyId, System.String type);
		
		#endregion
		
		#region _FileDbMedicalTraining_ComplianceReport_ByCompany2 
		
		/// <summary>
		///	This method wrap the '_FileDbMedicalTraining_ComplianceReport_ByCompany2' stored procedure. 
		/// </summary>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="type"> A <c>System.String</c> instance.</param>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="siteId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companySiteCategoryId"> A <c>System.String</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		public void ComplianceReport_ByCompany2(System.Int32? year, System.String type, System.Int32? companyId, System.Int32? siteId, System.String companySiteCategoryId)
		{
			 ComplianceReport_ByCompany2(null, 0, int.MaxValue , year, type, companyId, siteId, companySiteCategoryId);
		}
		
		/// <summary>
		///	This method wrap the '_FileDbMedicalTraining_ComplianceReport_ByCompany2' stored procedure. 
		/// </summary>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="type"> A <c>System.String</c> instance.</param>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="siteId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companySiteCategoryId"> A <c>System.String</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		public void ComplianceReport_ByCompany2(int start, int pageLength, System.Int32? year, System.String type, System.Int32? companyId, System.Int32? siteId, System.String companySiteCategoryId)
		{
			 ComplianceReport_ByCompany2(null, start, pageLength , year, type, companyId, siteId, companySiteCategoryId);
		}
				
		/// <summary>
		///	This method wrap the '_FileDbMedicalTraining_ComplianceReport_ByCompany2' stored procedure. 
		/// </summary>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="type"> A <c>System.String</c> instance.</param>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="siteId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companySiteCategoryId"> A <c>System.String</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		public void ComplianceReport_ByCompany2(TransactionManager transactionManager, System.Int32? year, System.String type, System.Int32? companyId, System.Int32? siteId, System.String companySiteCategoryId)
		{
			 ComplianceReport_ByCompany2(transactionManager, 0, int.MaxValue , year, type, companyId, siteId, companySiteCategoryId);
		}
		
		/// <summary>
		///	This method wrap the '_FileDbMedicalTraining_ComplianceReport_ByCompany2' stored procedure. 
		/// </summary>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="type"> A <c>System.String</c> instance.</param>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="siteId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companySiteCategoryId"> A <c>System.String</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		public abstract void ComplianceReport_ByCompany2(TransactionManager transactionManager, int start, int pageLength , System.Int32? year, System.String type, System.Int32? companyId, System.Int32? siteId, System.String companySiteCategoryId);
		
		#endregion
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;FileDbMedicalTraining&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;FileDbMedicalTraining&gt;"/></returns>
		public static TList<FileDbMedicalTraining> Fill(IDataReader reader, TList<FileDbMedicalTraining> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.FileDbMedicalTraining c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("FileDbMedicalTraining")
					.Append("|").Append((System.Int32)reader[((int)FileDbMedicalTrainingColumn.FileId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<FileDbMedicalTraining>(
					key.ToString(), // EntityTrackingKey
					"FileDbMedicalTraining",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.FileDbMedicalTraining();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.FileId = (System.Int32)reader[((int)FileDbMedicalTrainingColumn.FileId - 1)];
					c.CompanyId = (System.Int32)reader[((int)FileDbMedicalTrainingColumn.CompanyId - 1)];
					c.FileName = (System.String)reader[((int)FileDbMedicalTrainingColumn.FileName - 1)];
					c.Description = (System.String)reader[((int)FileDbMedicalTrainingColumn.Description - 1)];
					c.FileHash = (System.Byte[])reader[((int)FileDbMedicalTrainingColumn.FileHash - 1)];
					c.ContentLength = (System.Int32)reader[((int)FileDbMedicalTrainingColumn.ContentLength - 1)];
                    //Commented for DT2489
                    c.Content = (System.Byte[])reader[((int)FileDbMedicalTrainingColumn.Content - 1)]; //End
					c.Type = (System.String)reader[((int)FileDbMedicalTrainingColumn.Type - 1)];
					c.Comments = (reader.IsDBNull(((int)FileDbMedicalTrainingColumn.Comments - 1)))?null:(System.String)reader[((int)FileDbMedicalTrainingColumn.Comments - 1)];
					c.ModifiedByUserId = (System.Int32)reader[((int)FileDbMedicalTrainingColumn.ModifiedByUserId - 1)];
					c.ModifiedDate = (System.DateTime)reader[((int)FileDbMedicalTrainingColumn.ModifiedDate - 1)];
					c.RegionId = (System.Int32)reader[((int)FileDbMedicalTrainingColumn.RegionId - 1)];
					c.SiteId = (System.Int32)reader[((int)FileDbMedicalTrainingColumn.SiteId - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.FileDbMedicalTraining"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.FileDbMedicalTraining"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.FileDbMedicalTraining entity)
		{
			if (!reader.Read()) return;
			
			entity.FileId = (System.Int32)reader[((int)FileDbMedicalTrainingColumn.FileId - 1)];
			entity.CompanyId = (System.Int32)reader[((int)FileDbMedicalTrainingColumn.CompanyId - 1)];
			entity.FileName = (System.String)reader[((int)FileDbMedicalTrainingColumn.FileName - 1)];
			entity.Description = (System.String)reader[((int)FileDbMedicalTrainingColumn.Description - 1)];
			entity.FileHash = (System.Byte[])reader[((int)FileDbMedicalTrainingColumn.FileHash - 1)];
			entity.ContentLength = (System.Int32)reader[((int)FileDbMedicalTrainingColumn.ContentLength - 1)];
			entity.Content = (System.Byte[])reader[((int)FileDbMedicalTrainingColumn.Content - 1)];
			entity.Type = (System.String)reader[((int)FileDbMedicalTrainingColumn.Type - 1)];
			entity.Comments = (reader.IsDBNull(((int)FileDbMedicalTrainingColumn.Comments - 1)))?null:(System.String)reader[((int)FileDbMedicalTrainingColumn.Comments - 1)];
			entity.ModifiedByUserId = (System.Int32)reader[((int)FileDbMedicalTrainingColumn.ModifiedByUserId - 1)];
			entity.ModifiedDate = (System.DateTime)reader[((int)FileDbMedicalTrainingColumn.ModifiedDate - 1)];
			entity.RegionId = (System.Int32)reader[((int)FileDbMedicalTrainingColumn.RegionId - 1)];
			entity.SiteId = (System.Int32)reader[((int)FileDbMedicalTrainingColumn.SiteId - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.FileDbMedicalTraining"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.FileDbMedicalTraining"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.FileDbMedicalTraining entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.FileId = (System.Int32)dataRow["FileId"];
			entity.CompanyId = (System.Int32)dataRow["CompanyId"];
			entity.FileName = (System.String)dataRow["FileName"];
			entity.Description = (System.String)dataRow["Description"];
			entity.FileHash = (System.Byte[])dataRow["FileHash"];
			entity.ContentLength = (System.Int32)dataRow["ContentLength"];
			entity.Content = (System.Byte[])dataRow["Content"];
			entity.Type = (System.String)dataRow["Type"];
			entity.Comments = Convert.IsDBNull(dataRow["Comments"]) ? null : (System.String)dataRow["Comments"];
			entity.ModifiedByUserId = (System.Int32)dataRow["ModifiedByUserId"];
			entity.ModifiedDate = (System.DateTime)dataRow["ModifiedDate"];
			entity.RegionId = (System.Int32)dataRow["RegionId"];
			entity.SiteId = (System.Int32)dataRow["SiteId"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.FileDbMedicalTraining"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.FileDbMedicalTraining Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.FileDbMedicalTraining entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region CompanyIdSource	
			if (CanDeepLoad(entity, "Companies|CompanyIdSource", deepLoadType, innerList) 
				&& entity.CompanyIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.CompanyId;
				Companies tmpEntity = EntityManager.LocateEntity<Companies>(EntityLocator.ConstructKeyFromPkItems(typeof(Companies), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.CompanyIdSource = tmpEntity;
				else
					entity.CompanyIdSource = DataRepository.CompaniesProvider.GetByCompanyId(transactionManager, entity.CompanyId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CompanyIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.CompanyIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.CompaniesProvider.DeepLoad(transactionManager, entity.CompanyIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion CompanyIdSource

			#region ModifiedByUserIdSource	
			if (CanDeepLoad(entity, "Users|ModifiedByUserIdSource", deepLoadType, innerList) 
				&& entity.ModifiedByUserIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.ModifiedByUserId;
				Users tmpEntity = EntityManager.LocateEntity<Users>(EntityLocator.ConstructKeyFromPkItems(typeof(Users), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.ModifiedByUserIdSource = tmpEntity;
				else
					entity.ModifiedByUserIdSource = DataRepository.UsersProvider.GetByUserId(transactionManager, entity.ModifiedByUserId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ModifiedByUserIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.ModifiedByUserIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.UsersProvider.DeepLoad(transactionManager, entity.ModifiedByUserIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion ModifiedByUserIdSource

			#region RegionIdSource	
			if (CanDeepLoad(entity, "Regions|RegionIdSource", deepLoadType, innerList) 
				&& entity.RegionIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.RegionId;
				Regions tmpEntity = EntityManager.LocateEntity<Regions>(EntityLocator.ConstructKeyFromPkItems(typeof(Regions), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.RegionIdSource = tmpEntity;
				else
					entity.RegionIdSource = DataRepository.RegionsProvider.GetByRegionId(transactionManager, entity.RegionId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'RegionIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.RegionIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.RegionsProvider.DeepLoad(transactionManager, entity.RegionIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion RegionIdSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.FileDbMedicalTraining object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.FileDbMedicalTraining instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.FileDbMedicalTraining Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.FileDbMedicalTraining entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region CompanyIdSource
			if (CanDeepSave(entity, "Companies|CompanyIdSource", deepSaveType, innerList) 
				&& entity.CompanyIdSource != null)
			{
				DataRepository.CompaniesProvider.Save(transactionManager, entity.CompanyIdSource);
				entity.CompanyId = entity.CompanyIdSource.CompanyId;
			}
			#endregion 
			
			#region ModifiedByUserIdSource
			if (CanDeepSave(entity, "Users|ModifiedByUserIdSource", deepSaveType, innerList) 
				&& entity.ModifiedByUserIdSource != null)
			{
				DataRepository.UsersProvider.Save(transactionManager, entity.ModifiedByUserIdSource);
				entity.ModifiedByUserId = entity.ModifiedByUserIdSource.UserId;
			}
			#endregion 
			
			#region RegionIdSource
			if (CanDeepSave(entity, "Regions|RegionIdSource", deepSaveType, innerList) 
				&& entity.RegionIdSource != null)
			{
				DataRepository.RegionsProvider.Save(transactionManager, entity.RegionIdSource);
				entity.RegionId = entity.RegionIdSource.RegionId;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region FileDbMedicalTrainingChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.FileDbMedicalTraining</c>
	///</summary>
	public enum FileDbMedicalTrainingChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>Companies</c> at CompanyIdSource
		///</summary>
		[ChildEntityType(typeof(Companies))]
		Companies,
			
		///<summary>
		/// Composite Property for <c>Users</c> at ModifiedByUserIdSource
		///</summary>
		[ChildEntityType(typeof(Users))]
		Users,
			
		///<summary>
		/// Composite Property for <c>Regions</c> at RegionIdSource
		///</summary>
		[ChildEntityType(typeof(Regions))]
		Regions,
		}
	
	#endregion FileDbMedicalTrainingChildEntityTypes
	
	#region FileDbMedicalTrainingFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;FileDbMedicalTrainingColumn&gt;"/> class
	/// that is used exclusively with a <see cref="FileDbMedicalTraining"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FileDbMedicalTrainingFilterBuilder : SqlFilterBuilder<FileDbMedicalTrainingColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FileDbMedicalTrainingFilterBuilder class.
		/// </summary>
		public FileDbMedicalTrainingFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the FileDbMedicalTrainingFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public FileDbMedicalTrainingFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the FileDbMedicalTrainingFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public FileDbMedicalTrainingFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion FileDbMedicalTrainingFilterBuilder
	
	#region FileDbMedicalTrainingParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;FileDbMedicalTrainingColumn&gt;"/> class
	/// that is used exclusively with a <see cref="FileDbMedicalTraining"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FileDbMedicalTrainingParameterBuilder : ParameterizedSqlFilterBuilder<FileDbMedicalTrainingColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FileDbMedicalTrainingParameterBuilder class.
		/// </summary>
		public FileDbMedicalTrainingParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the FileDbMedicalTrainingParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public FileDbMedicalTrainingParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the FileDbMedicalTrainingParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public FileDbMedicalTrainingParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion FileDbMedicalTrainingParameterBuilder
	
	#region FileDbMedicalTrainingSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;FileDbMedicalTrainingColumn&gt;"/> class
	/// that is used exclusively with a <see cref="FileDbMedicalTraining"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class FileDbMedicalTrainingSortBuilder : SqlSortBuilder<FileDbMedicalTrainingColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FileDbMedicalTrainingSqlSortBuilder class.
		/// </summary>
		public FileDbMedicalTrainingSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion FileDbMedicalTrainingSortBuilder
	
} // end namespace
