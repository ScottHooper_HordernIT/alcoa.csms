﻿#region Using directives

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="QuestionnaireMainQuestionAnswerProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract class QuestionnaireMainQuestionAnswerProviderBaseCore : EntityViewProviderBase<QuestionnaireMainQuestionAnswer>
	{
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions
		
		/*
		///<summary>
		/// Fill an VList&lt;QuestionnaireMainQuestionAnswer&gt; From a DataSet
		///</summary>
		/// <param name="dataSet">the DataSet</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList&lt;QuestionnaireMainQuestionAnswer&gt;"/></returns>
		protected static VList&lt;QuestionnaireMainQuestionAnswer&gt; Fill(DataSet dataSet, VList<QuestionnaireMainQuestionAnswer> rows, int start, int pagelen)
		{
			if (dataSet.Tables.Count == 1)
			{
				return Fill(dataSet.Tables[0], rows, start, pagelen);
			}
			else
			{
				return new VList<QuestionnaireMainQuestionAnswer>();
			}	
		}
		
		
		///<summary>
		/// Fill an VList&lt;QuestionnaireMainQuestionAnswer&gt; From a DataTable
		///</summary>
		/// <param name="dataTable">the DataTable that hold the data.</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList<QuestionnaireMainQuestionAnswer>"/></returns>
		protected static VList&lt;QuestionnaireMainQuestionAnswer&gt; Fill(DataTable dataTable, VList<QuestionnaireMainQuestionAnswer> rows, int start, int pagelen)
		{
			int recordnum = 0;
			
			System.Collections.IEnumerator dataRows =  dataTable.Rows.GetEnumerator();
			
			while (dataRows.MoveNext() && (pagelen != 0))
			{
				if(recordnum >= start)
				{
					DataRow row = (DataRow)dataRows.Current;
				
					QuestionnaireMainQuestionAnswer c = new QuestionnaireMainQuestionAnswer();
					c.AnswerId = (Convert.IsDBNull(row["AnswerId"]))?(int)0:(System.Int32)row["AnswerId"];
					c.QuestionNo = (Convert.IsDBNull(row["QuestionNo"]))?string.Empty:(System.String)row["QuestionNo"];
					c.QuestionText = (Convert.IsDBNull(row["QuestionText"]))?string.Empty:(System.String)row["QuestionText"];
					c.AnswerNo = (Convert.IsDBNull(row["AnswerNo"]))?string.Empty:(System.String)row["AnswerNo"];
					c.AnswerText = (Convert.IsDBNull(row["AnswerText"]))?string.Empty:(System.String)row["AnswerText"];
					c.AcceptChanges();
					rows.Add(c);
					pagelen -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		*/	
						
		///<summary>
		/// Fill an <see cref="VList&lt;QuestionnaireMainQuestionAnswer&gt;"/> From a DataReader.
		///</summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pageLength">number of row.</param>
		///<returns>a <see cref="VList&lt;QuestionnaireMainQuestionAnswer&gt;"/></returns>
		protected VList<QuestionnaireMainQuestionAnswer> Fill(IDataReader reader, VList<QuestionnaireMainQuestionAnswer> rows, int start, int pageLength)
		{
			int recordnum = 0;
			while (reader.Read() && (pageLength != 0))
			{
				if(recordnum >= start)
				{
					QuestionnaireMainQuestionAnswer entity = null;
					if (DataRepository.Provider.UseEntityFactory)
					{
						entity = EntityManager.CreateViewEntity<QuestionnaireMainQuestionAnswer>("QuestionnaireMainQuestionAnswer",  DataRepository.Provider.EntityCreationalFactoryType); 
					}
					else
					{
						entity = new QuestionnaireMainQuestionAnswer();
					}
					
					entity.SuppressEntityEvents = true;

					entity.AnswerId = (System.Int32)reader[((int)QuestionnaireMainQuestionAnswerColumn.AnswerId)];
					//entity.AnswerId = (Convert.IsDBNull(reader["AnswerId"]))?(int)0:(System.Int32)reader["AnswerId"];
					entity.QuestionNo = (System.String)reader[((int)QuestionnaireMainQuestionAnswerColumn.QuestionNo)];
					//entity.QuestionNo = (Convert.IsDBNull(reader["QuestionNo"]))?string.Empty:(System.String)reader["QuestionNo"];
					entity.QuestionText = (System.String)reader[((int)QuestionnaireMainQuestionAnswerColumn.QuestionText)];
					//entity.QuestionText = (Convert.IsDBNull(reader["QuestionText"]))?string.Empty:(System.String)reader["QuestionText"];
					entity.AnswerNo = (reader.IsDBNull(((int)QuestionnaireMainQuestionAnswerColumn.AnswerNo)))?null:(System.String)reader[((int)QuestionnaireMainQuestionAnswerColumn.AnswerNo)];
					//entity.AnswerNo = (Convert.IsDBNull(reader["AnswerNo"]))?string.Empty:(System.String)reader["AnswerNo"];
					entity.AnswerText = (reader.IsDBNull(((int)QuestionnaireMainQuestionAnswerColumn.AnswerText)))?null:(System.String)reader[((int)QuestionnaireMainQuestionAnswerColumn.AnswerText)];
					//entity.AnswerText = (Convert.IsDBNull(reader["AnswerText"]))?string.Empty:(System.String)reader["AnswerText"];
					entity.AcceptChanges();
					entity.SuppressEntityEvents = false;
					
					rows.Add(entity);
					pageLength -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		
		
		/// <summary>
		/// Refreshes the <see cref="QuestionnaireMainQuestionAnswer"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="QuestionnaireMainQuestionAnswer"/> object to refresh.</param>
		protected void RefreshEntity(IDataReader reader, QuestionnaireMainQuestionAnswer entity)
		{
			reader.Read();
			entity.AnswerId = (System.Int32)reader[((int)QuestionnaireMainQuestionAnswerColumn.AnswerId)];
			//entity.AnswerId = (Convert.IsDBNull(reader["AnswerId"]))?(int)0:(System.Int32)reader["AnswerId"];
			entity.QuestionNo = (System.String)reader[((int)QuestionnaireMainQuestionAnswerColumn.QuestionNo)];
			//entity.QuestionNo = (Convert.IsDBNull(reader["QuestionNo"]))?string.Empty:(System.String)reader["QuestionNo"];
			entity.QuestionText = (System.String)reader[((int)QuestionnaireMainQuestionAnswerColumn.QuestionText)];
			//entity.QuestionText = (Convert.IsDBNull(reader["QuestionText"]))?string.Empty:(System.String)reader["QuestionText"];
			entity.AnswerNo = (reader.IsDBNull(((int)QuestionnaireMainQuestionAnswerColumn.AnswerNo)))?null:(System.String)reader[((int)QuestionnaireMainQuestionAnswerColumn.AnswerNo)];
			//entity.AnswerNo = (Convert.IsDBNull(reader["AnswerNo"]))?string.Empty:(System.String)reader["AnswerNo"];
			entity.AnswerText = (reader.IsDBNull(((int)QuestionnaireMainQuestionAnswerColumn.AnswerText)))?null:(System.String)reader[((int)QuestionnaireMainQuestionAnswerColumn.AnswerText)];
			//entity.AnswerText = (Convert.IsDBNull(reader["AnswerText"]))?string.Empty:(System.String)reader["AnswerText"];
			reader.Close();
	
			entity.AcceptChanges();
		}
		
		/*
		/// <summary>
		/// Refreshes the <see cref="QuestionnaireMainQuestionAnswer"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="QuestionnaireMainQuestionAnswer"/> object.</param>
		protected static void RefreshEntity(DataSet dataSet, QuestionnaireMainQuestionAnswer entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.AnswerId = (Convert.IsDBNull(dataRow["AnswerId"]))?(int)0:(System.Int32)dataRow["AnswerId"];
			entity.QuestionNo = (Convert.IsDBNull(dataRow["QuestionNo"]))?string.Empty:(System.String)dataRow["QuestionNo"];
			entity.QuestionText = (Convert.IsDBNull(dataRow["QuestionText"]))?string.Empty:(System.String)dataRow["QuestionText"];
			entity.AnswerNo = (Convert.IsDBNull(dataRow["AnswerNo"]))?string.Empty:(System.String)dataRow["AnswerNo"];
			entity.AnswerText = (Convert.IsDBNull(dataRow["AnswerText"]))?string.Empty:(System.String)dataRow["AnswerText"];
			entity.AcceptChanges();
		}
		*/
			
		#endregion Helper Functions
	}//end class

	#region QuestionnaireMainQuestionAnswerFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireMainQuestionAnswer"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireMainQuestionAnswerFilterBuilder : SqlFilterBuilder<QuestionnaireMainQuestionAnswerColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainQuestionAnswerFilterBuilder class.
		/// </summary>
		public QuestionnaireMainQuestionAnswerFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainQuestionAnswerFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireMainQuestionAnswerFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainQuestionAnswerFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireMainQuestionAnswerFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireMainQuestionAnswerFilterBuilder

	#region QuestionnaireMainQuestionAnswerParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireMainQuestionAnswer"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireMainQuestionAnswerParameterBuilder : ParameterizedSqlFilterBuilder<QuestionnaireMainQuestionAnswerColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainQuestionAnswerParameterBuilder class.
		/// </summary>
		public QuestionnaireMainQuestionAnswerParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainQuestionAnswerParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireMainQuestionAnswerParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainQuestionAnswerParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireMainQuestionAnswerParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireMainQuestionAnswerParameterBuilder
	
	#region QuestionnaireMainQuestionAnswerSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireMainQuestionAnswer"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class QuestionnaireMainQuestionAnswerSortBuilder : SqlSortBuilder<QuestionnaireMainQuestionAnswerColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainQuestionAnswerSqlSortBuilder class.
		/// </summary>
		public QuestionnaireMainQuestionAnswerSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion QuestionnaireMainQuestionAnswerSortBuilder

} // end namespace
