﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.AdHocRadarProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(AdHocRadarDataSourceDesigner))]
	public class AdHocRadarDataSource : ProviderDataSource<AdHocRadar, AdHocRadarKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AdHocRadarDataSource class.
		/// </summary>
		public AdHocRadarDataSource() : base(new AdHocRadarService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the AdHocRadarDataSourceView used by the AdHocRadarDataSource.
		/// </summary>
		protected AdHocRadarDataSourceView AdHocRadarView
		{
			get { return ( View as AdHocRadarDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the AdHocRadarDataSource control invokes to retrieve data.
		/// </summary>
		public AdHocRadarSelectMethod SelectMethod
		{
			get
			{
				AdHocRadarSelectMethod selectMethod = AdHocRadarSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (AdHocRadarSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the AdHocRadarDataSourceView class that is to be
		/// used by the AdHocRadarDataSource.
		/// </summary>
		/// <returns>An instance of the AdHocRadarDataSourceView class.</returns>
		protected override BaseDataSourceView<AdHocRadar, AdHocRadarKey> GetNewDataSourceView()
		{
			return new AdHocRadarDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the AdHocRadarDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class AdHocRadarDataSourceView : ProviderDataSourceView<AdHocRadar, AdHocRadarKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AdHocRadarDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the AdHocRadarDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public AdHocRadarDataSourceView(AdHocRadarDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal AdHocRadarDataSource AdHocRadarOwner
		{
			get { return Owner as AdHocRadarDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal AdHocRadarSelectMethod SelectMethod
		{
			get { return AdHocRadarOwner.SelectMethod; }
			set { AdHocRadarOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal AdHocRadarService AdHocRadarProvider
		{
			get { return Provider as AdHocRadarService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<AdHocRadar> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<AdHocRadar> results = null;
			AdHocRadar item;
			count = 0;
			
			System.Int32 _adHocRadarId;
			System.Int32 _companySiteCategoryId;
			System.String _kpiName;

			switch ( SelectMethod )
			{
				case AdHocRadarSelectMethod.Get:
					AdHocRadarKey entityKey  = new AdHocRadarKey();
					entityKey.Load(values);
					item = AdHocRadarProvider.Get(entityKey);
					results = new TList<AdHocRadar>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case AdHocRadarSelectMethod.GetAll:
                    results = AdHocRadarProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case AdHocRadarSelectMethod.GetPaged:
					results = AdHocRadarProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case AdHocRadarSelectMethod.Find:
					if ( FilterParameters != null )
						results = AdHocRadarProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = AdHocRadarProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case AdHocRadarSelectMethod.GetByAdHocRadarId:
					_adHocRadarId = ( values["AdHocRadarId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["AdHocRadarId"], typeof(System.Int32)) : (int)0;
					item = AdHocRadarProvider.GetByAdHocRadarId(_adHocRadarId);
					results = new TList<AdHocRadar>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				case AdHocRadarSelectMethod.GetByCompanySiteCategoryIdKpiName:
					_companySiteCategoryId = ( values["CompanySiteCategoryId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["CompanySiteCategoryId"], typeof(System.Int32)) : (int)0;
					_kpiName = ( values["KpiName"] != null ) ? (System.String) EntityUtil.ChangeType(values["KpiName"], typeof(System.String)) : string.Empty;
					item = AdHocRadarProvider.GetByCompanySiteCategoryIdKpiName(_companySiteCategoryId, _kpiName);
					results = new TList<AdHocRadar>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// FK
				case AdHocRadarSelectMethod.GetByCompanySiteCategoryId:
					_companySiteCategoryId = ( values["CompanySiteCategoryId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["CompanySiteCategoryId"], typeof(System.Int32)) : (int)0;
					results = AdHocRadarProvider.GetByCompanySiteCategoryId(_companySiteCategoryId, this.StartIndex, this.PageSize, out count);
					break;
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == AdHocRadarSelectMethod.Get || SelectMethod == AdHocRadarSelectMethod.GetByAdHocRadarId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				AdHocRadar entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					AdHocRadarProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<AdHocRadar> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			AdHocRadarProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region AdHocRadarDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the AdHocRadarDataSource class.
	/// </summary>
	public class AdHocRadarDataSourceDesigner : ProviderDataSourceDesigner<AdHocRadar, AdHocRadarKey>
	{
		/// <summary>
		/// Initializes a new instance of the AdHocRadarDataSourceDesigner class.
		/// </summary>
		public AdHocRadarDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public AdHocRadarSelectMethod SelectMethod
		{
			get { return ((AdHocRadarDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new AdHocRadarDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region AdHocRadarDataSourceActionList

	/// <summary>
	/// Supports the AdHocRadarDataSourceDesigner class.
	/// </summary>
	internal class AdHocRadarDataSourceActionList : DesignerActionList
	{
		private AdHocRadarDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the AdHocRadarDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public AdHocRadarDataSourceActionList(AdHocRadarDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public AdHocRadarSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion AdHocRadarDataSourceActionList
	
	#endregion AdHocRadarDataSourceDesigner
	
	#region AdHocRadarSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the AdHocRadarDataSource.SelectMethod property.
	/// </summary>
	public enum AdHocRadarSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByAdHocRadarId method.
		/// </summary>
		GetByAdHocRadarId,
		/// <summary>
		/// Represents the GetByCompanySiteCategoryIdKpiName method.
		/// </summary>
		GetByCompanySiteCategoryIdKpiName,
		/// <summary>
		/// Represents the GetByCompanySiteCategoryId method.
		/// </summary>
		GetByCompanySiteCategoryId
	}
	
	#endregion AdHocRadarSelectMethod

	#region AdHocRadarFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AdHocRadar"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AdHocRadarFilter : SqlFilter<AdHocRadarColumn>
	{
	}
	
	#endregion AdHocRadarFilter

	#region AdHocRadarExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AdHocRadar"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AdHocRadarExpressionBuilder : SqlExpressionBuilder<AdHocRadarColumn>
	{
	}
	
	#endregion AdHocRadarExpressionBuilder	

	#region AdHocRadarProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;AdHocRadarChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="AdHocRadar"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AdHocRadarProperty : ChildEntityProperty<AdHocRadarChildEntityTypes>
	{
	}
	
	#endregion AdHocRadarProperty
}

