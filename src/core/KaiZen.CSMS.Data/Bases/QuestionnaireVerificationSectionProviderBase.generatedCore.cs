﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="QuestionnaireVerificationSectionProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class QuestionnaireVerificationSectionProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.QuestionnaireVerificationSection, KaiZen.CSMS.Entities.QuestionnaireVerificationSectionKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.QuestionnaireVerificationSectionKey key)
		{
			return Delete(transactionManager, key.SectionResponseId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_sectionResponseId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _sectionResponseId)
		{
			return Delete(null, _sectionResponseId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_sectionResponseId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _sectionResponseId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.QuestionnaireVerificationSection Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.QuestionnaireVerificationSectionKey key, int start, int pageLength)
		{
			return GetBySectionResponseId(transactionManager, key.SectionResponseId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_QuestionnaireVerificationSection index.
		/// </summary>
		/// <param name="_sectionResponseId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireVerificationSection"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireVerificationSection GetBySectionResponseId(System.Int32 _sectionResponseId)
		{
			int count = -1;
			return GetBySectionResponseId(null,_sectionResponseId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireVerificationSection index.
		/// </summary>
		/// <param name="_sectionResponseId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireVerificationSection"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireVerificationSection GetBySectionResponseId(System.Int32 _sectionResponseId, int start, int pageLength)
		{
			int count = -1;
			return GetBySectionResponseId(null, _sectionResponseId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireVerificationSection index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_sectionResponseId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireVerificationSection"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireVerificationSection GetBySectionResponseId(TransactionManager transactionManager, System.Int32 _sectionResponseId)
		{
			int count = -1;
			return GetBySectionResponseId(transactionManager, _sectionResponseId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireVerificationSection index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_sectionResponseId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireVerificationSection"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireVerificationSection GetBySectionResponseId(TransactionManager transactionManager, System.Int32 _sectionResponseId, int start, int pageLength)
		{
			int count = -1;
			return GetBySectionResponseId(transactionManager, _sectionResponseId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireVerificationSection index.
		/// </summary>
		/// <param name="_sectionResponseId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireVerificationSection"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireVerificationSection GetBySectionResponseId(System.Int32 _sectionResponseId, int start, int pageLength, out int count)
		{
			return GetBySectionResponseId(null, _sectionResponseId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireVerificationSection index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_sectionResponseId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireVerificationSection"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.QuestionnaireVerificationSection GetBySectionResponseId(TransactionManager transactionManager, System.Int32 _sectionResponseId, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_QuestionnaireVerificationSection index.
		/// </summary>
		/// <param name="_questionnaireId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireVerificationSection"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireVerificationSection GetByQuestionnaireId(System.Int32 _questionnaireId)
		{
			int count = -1;
			return GetByQuestionnaireId(null,_questionnaireId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireVerificationSection index.
		/// </summary>
		/// <param name="_questionnaireId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireVerificationSection"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireVerificationSection GetByQuestionnaireId(System.Int32 _questionnaireId, int start, int pageLength)
		{
			int count = -1;
			return GetByQuestionnaireId(null, _questionnaireId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireVerificationSection index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireVerificationSection"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireVerificationSection GetByQuestionnaireId(TransactionManager transactionManager, System.Int32 _questionnaireId)
		{
			int count = -1;
			return GetByQuestionnaireId(transactionManager, _questionnaireId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireVerificationSection index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireVerificationSection"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireVerificationSection GetByQuestionnaireId(TransactionManager transactionManager, System.Int32 _questionnaireId, int start, int pageLength)
		{
			int count = -1;
			return GetByQuestionnaireId(transactionManager, _questionnaireId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireVerificationSection index.
		/// </summary>
		/// <param name="_questionnaireId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireVerificationSection"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireVerificationSection GetByQuestionnaireId(System.Int32 _questionnaireId, int start, int pageLength, out int count)
		{
			return GetByQuestionnaireId(null, _questionnaireId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireVerificationSection index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireVerificationSection"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.QuestionnaireVerificationSection GetByQuestionnaireId(TransactionManager transactionManager, System.Int32 _questionnaireId, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;QuestionnaireVerificationSection&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;QuestionnaireVerificationSection&gt;"/></returns>
		public static TList<QuestionnaireVerificationSection> Fill(IDataReader reader, TList<QuestionnaireVerificationSection> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.QuestionnaireVerificationSection c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("QuestionnaireVerificationSection")
					.Append("|").Append((System.Int32)reader[((int)QuestionnaireVerificationSectionColumn.SectionResponseId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<QuestionnaireVerificationSection>(
					key.ToString(), // EntityTrackingKey
					"QuestionnaireVerificationSection",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.QuestionnaireVerificationSection();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.SectionResponseId = (System.Int32)reader[((int)QuestionnaireVerificationSectionColumn.SectionResponseId - 1)];
					c.QuestionnaireId = (System.Int32)reader[((int)QuestionnaireVerificationSectionColumn.QuestionnaireId - 1)];
					c.Section1Complete = (reader.IsDBNull(((int)QuestionnaireVerificationSectionColumn.Section1Complete - 1)))?null:(System.Boolean?)reader[((int)QuestionnaireVerificationSectionColumn.Section1Complete - 1)];
					c.Section2Complete = (reader.IsDBNull(((int)QuestionnaireVerificationSectionColumn.Section2Complete - 1)))?null:(System.Boolean?)reader[((int)QuestionnaireVerificationSectionColumn.Section2Complete - 1)];
					c.Section3Complete = (reader.IsDBNull(((int)QuestionnaireVerificationSectionColumn.Section3Complete - 1)))?null:(System.Boolean?)reader[((int)QuestionnaireVerificationSectionColumn.Section3Complete - 1)];
					c.Section4Complete = (reader.IsDBNull(((int)QuestionnaireVerificationSectionColumn.Section4Complete - 1)))?null:(System.Boolean?)reader[((int)QuestionnaireVerificationSectionColumn.Section4Complete - 1)];
					c.Section5Complete = (reader.IsDBNull(((int)QuestionnaireVerificationSectionColumn.Section5Complete - 1)))?null:(System.Boolean?)reader[((int)QuestionnaireVerificationSectionColumn.Section5Complete - 1)];
					c.Section6Complete = (reader.IsDBNull(((int)QuestionnaireVerificationSectionColumn.Section6Complete - 1)))?null:(System.Boolean?)reader[((int)QuestionnaireVerificationSectionColumn.Section6Complete - 1)];
					c.Section7Complete = (reader.IsDBNull(((int)QuestionnaireVerificationSectionColumn.Section7Complete - 1)))?null:(System.Boolean?)reader[((int)QuestionnaireVerificationSectionColumn.Section7Complete - 1)];
					c.Section8Complete = (reader.IsDBNull(((int)QuestionnaireVerificationSectionColumn.Section8Complete - 1)))?null:(System.Boolean?)reader[((int)QuestionnaireVerificationSectionColumn.Section8Complete - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.QuestionnaireVerificationSection"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.QuestionnaireVerificationSection"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.QuestionnaireVerificationSection entity)
		{
			if (!reader.Read()) return;
			
			entity.SectionResponseId = (System.Int32)reader[((int)QuestionnaireVerificationSectionColumn.SectionResponseId - 1)];
			entity.QuestionnaireId = (System.Int32)reader[((int)QuestionnaireVerificationSectionColumn.QuestionnaireId - 1)];
			entity.Section1Complete = (reader.IsDBNull(((int)QuestionnaireVerificationSectionColumn.Section1Complete - 1)))?null:(System.Boolean?)reader[((int)QuestionnaireVerificationSectionColumn.Section1Complete - 1)];
			entity.Section2Complete = (reader.IsDBNull(((int)QuestionnaireVerificationSectionColumn.Section2Complete - 1)))?null:(System.Boolean?)reader[((int)QuestionnaireVerificationSectionColumn.Section2Complete - 1)];
			entity.Section3Complete = (reader.IsDBNull(((int)QuestionnaireVerificationSectionColumn.Section3Complete - 1)))?null:(System.Boolean?)reader[((int)QuestionnaireVerificationSectionColumn.Section3Complete - 1)];
			entity.Section4Complete = (reader.IsDBNull(((int)QuestionnaireVerificationSectionColumn.Section4Complete - 1)))?null:(System.Boolean?)reader[((int)QuestionnaireVerificationSectionColumn.Section4Complete - 1)];
			entity.Section5Complete = (reader.IsDBNull(((int)QuestionnaireVerificationSectionColumn.Section5Complete - 1)))?null:(System.Boolean?)reader[((int)QuestionnaireVerificationSectionColumn.Section5Complete - 1)];
			entity.Section6Complete = (reader.IsDBNull(((int)QuestionnaireVerificationSectionColumn.Section6Complete - 1)))?null:(System.Boolean?)reader[((int)QuestionnaireVerificationSectionColumn.Section6Complete - 1)];
			entity.Section7Complete = (reader.IsDBNull(((int)QuestionnaireVerificationSectionColumn.Section7Complete - 1)))?null:(System.Boolean?)reader[((int)QuestionnaireVerificationSectionColumn.Section7Complete - 1)];
			entity.Section8Complete = (reader.IsDBNull(((int)QuestionnaireVerificationSectionColumn.Section8Complete - 1)))?null:(System.Boolean?)reader[((int)QuestionnaireVerificationSectionColumn.Section8Complete - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.QuestionnaireVerificationSection"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.QuestionnaireVerificationSection"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.QuestionnaireVerificationSection entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.SectionResponseId = (System.Int32)dataRow["SectionResponseId"];
			entity.QuestionnaireId = (System.Int32)dataRow["QuestionnaireId"];
			entity.Section1Complete = Convert.IsDBNull(dataRow["Section1_Complete"]) ? null : (System.Boolean?)dataRow["Section1_Complete"];
			entity.Section2Complete = Convert.IsDBNull(dataRow["Section2_Complete"]) ? null : (System.Boolean?)dataRow["Section2_Complete"];
			entity.Section3Complete = Convert.IsDBNull(dataRow["Section3_Complete"]) ? null : (System.Boolean?)dataRow["Section3_Complete"];
			entity.Section4Complete = Convert.IsDBNull(dataRow["Section4_Complete"]) ? null : (System.Boolean?)dataRow["Section4_Complete"];
			entity.Section5Complete = Convert.IsDBNull(dataRow["Section5_Complete"]) ? null : (System.Boolean?)dataRow["Section5_Complete"];
			entity.Section6Complete = Convert.IsDBNull(dataRow["Section6_Complete"]) ? null : (System.Boolean?)dataRow["Section6_Complete"];
			entity.Section7Complete = Convert.IsDBNull(dataRow["Section7_Complete"]) ? null : (System.Boolean?)dataRow["Section7_Complete"];
			entity.Section8Complete = Convert.IsDBNull(dataRow["Section8_Complete"]) ? null : (System.Boolean?)dataRow["Section8_Complete"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.QuestionnaireVerificationSection"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.QuestionnaireVerificationSection Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.QuestionnaireVerificationSection entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region QuestionnaireIdSource	
			if (CanDeepLoad(entity, "Questionnaire|QuestionnaireIdSource", deepLoadType, innerList) 
				&& entity.QuestionnaireIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.QuestionnaireId;
				Questionnaire tmpEntity = EntityManager.LocateEntity<Questionnaire>(EntityLocator.ConstructKeyFromPkItems(typeof(Questionnaire), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.QuestionnaireIdSource = tmpEntity;
				else
					entity.QuestionnaireIdSource = DataRepository.QuestionnaireProvider.GetByQuestionnaireId(transactionManager, entity.QuestionnaireId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'QuestionnaireIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.QuestionnaireIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.QuestionnaireProvider.DeepLoad(transactionManager, entity.QuestionnaireIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion QuestionnaireIdSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.QuestionnaireVerificationSection object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.QuestionnaireVerificationSection instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.QuestionnaireVerificationSection Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.QuestionnaireVerificationSection entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region QuestionnaireIdSource
			if (CanDeepSave(entity, "Questionnaire|QuestionnaireIdSource", deepSaveType, innerList) 
				&& entity.QuestionnaireIdSource != null)
			{
				DataRepository.QuestionnaireProvider.Save(transactionManager, entity.QuestionnaireIdSource);
				entity.QuestionnaireId = entity.QuestionnaireIdSource.QuestionnaireId;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region QuestionnaireVerificationSectionChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.QuestionnaireVerificationSection</c>
	///</summary>
	public enum QuestionnaireVerificationSectionChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>Questionnaire</c> at QuestionnaireIdSource
		///</summary>
		[ChildEntityType(typeof(Questionnaire))]
		Questionnaire,
		}
	
	#endregion QuestionnaireVerificationSectionChildEntityTypes
	
	#region QuestionnaireVerificationSectionFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;QuestionnaireVerificationSectionColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireVerificationSection"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireVerificationSectionFilterBuilder : SqlFilterBuilder<QuestionnaireVerificationSectionColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireVerificationSectionFilterBuilder class.
		/// </summary>
		public QuestionnaireVerificationSectionFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireVerificationSectionFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireVerificationSectionFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireVerificationSectionFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireVerificationSectionFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireVerificationSectionFilterBuilder
	
	#region QuestionnaireVerificationSectionParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;QuestionnaireVerificationSectionColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireVerificationSection"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireVerificationSectionParameterBuilder : ParameterizedSqlFilterBuilder<QuestionnaireVerificationSectionColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireVerificationSectionParameterBuilder class.
		/// </summary>
		public QuestionnaireVerificationSectionParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireVerificationSectionParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireVerificationSectionParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireVerificationSectionParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireVerificationSectionParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireVerificationSectionParameterBuilder
	
	#region QuestionnaireVerificationSectionSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;QuestionnaireVerificationSectionColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireVerificationSection"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class QuestionnaireVerificationSectionSortBuilder : SqlSortBuilder<QuestionnaireVerificationSectionColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireVerificationSectionSqlSortBuilder class.
		/// </summary>
		public QuestionnaireVerificationSectionSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion QuestionnaireVerificationSectionSortBuilder
	
} // end namespace
