﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.UsersPrivileged3Provider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(UsersPrivileged3DataSourceDesigner))]
	public class UsersPrivileged3DataSource : ProviderDataSource<UsersPrivileged3, UsersPrivileged3Key>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UsersPrivileged3DataSource class.
		/// </summary>
		public UsersPrivileged3DataSource() : base(new UsersPrivileged3Service())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the UsersPrivileged3DataSourceView used by the UsersPrivileged3DataSource.
		/// </summary>
		protected UsersPrivileged3DataSourceView UsersPrivileged3View
		{
			get { return ( View as UsersPrivileged3DataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the UsersPrivileged3DataSource control invokes to retrieve data.
		/// </summary>
		public UsersPrivileged3SelectMethod SelectMethod
		{
			get
			{
				UsersPrivileged3SelectMethod selectMethod = UsersPrivileged3SelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (UsersPrivileged3SelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the UsersPrivileged3DataSourceView class that is to be
		/// used by the UsersPrivileged3DataSource.
		/// </summary>
		/// <returns>An instance of the UsersPrivileged3DataSourceView class.</returns>
		protected override BaseDataSourceView<UsersPrivileged3, UsersPrivileged3Key> GetNewDataSourceView()
		{
			return new UsersPrivileged3DataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the UsersPrivileged3DataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class UsersPrivileged3DataSourceView : ProviderDataSourceView<UsersPrivileged3, UsersPrivileged3Key>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UsersPrivileged3DataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the UsersPrivileged3DataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public UsersPrivileged3DataSourceView(UsersPrivileged3DataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal UsersPrivileged3DataSource UsersPrivileged3Owner
		{
			get { return Owner as UsersPrivileged3DataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal UsersPrivileged3SelectMethod SelectMethod
		{
			get { return UsersPrivileged3Owner.SelectMethod; }
			set { UsersPrivileged3Owner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal UsersPrivileged3Service UsersPrivileged3Provider
		{
			get { return Provider as UsersPrivileged3Service; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<UsersPrivileged3> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<UsersPrivileged3> results = null;
			UsersPrivileged3 item;
			count = 0;
			
			System.Int32 _usersPrivilegedId;
			System.Int32 _userId;

			switch ( SelectMethod )
			{
				case UsersPrivileged3SelectMethod.Get:
					UsersPrivileged3Key entityKey  = new UsersPrivileged3Key();
					entityKey.Load(values);
					item = UsersPrivileged3Provider.Get(entityKey);
					results = new TList<UsersPrivileged3>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case UsersPrivileged3SelectMethod.GetAll:
                    results = UsersPrivileged3Provider.GetAll(StartIndex, PageSize, out count);
                    break;
				case UsersPrivileged3SelectMethod.GetPaged:
					results = UsersPrivileged3Provider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case UsersPrivileged3SelectMethod.Find:
					if ( FilterParameters != null )
						results = UsersPrivileged3Provider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = UsersPrivileged3Provider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case UsersPrivileged3SelectMethod.GetByUsersPrivilegedId:
					_usersPrivilegedId = ( values["UsersPrivilegedId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["UsersPrivilegedId"], typeof(System.Int32)) : (int)0;
					item = UsersPrivileged3Provider.GetByUsersPrivilegedId(_usersPrivilegedId);
					results = new TList<UsersPrivileged3>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				case UsersPrivileged3SelectMethod.GetByUserId:
					_userId = ( values["UserId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["UserId"], typeof(System.Int32)) : (int)0;
					item = UsersPrivileged3Provider.GetByUserId(_userId);
					results = new TList<UsersPrivileged3>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// FK
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == UsersPrivileged3SelectMethod.Get || SelectMethod == UsersPrivileged3SelectMethod.GetByUsersPrivilegedId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				UsersPrivileged3 entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					UsersPrivileged3Provider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<UsersPrivileged3> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			UsersPrivileged3Provider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region UsersPrivileged3DataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the UsersPrivileged3DataSource class.
	/// </summary>
	public class UsersPrivileged3DataSourceDesigner : ProviderDataSourceDesigner<UsersPrivileged3, UsersPrivileged3Key>
	{
		/// <summary>
		/// Initializes a new instance of the UsersPrivileged3DataSourceDesigner class.
		/// </summary>
		public UsersPrivileged3DataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public UsersPrivileged3SelectMethod SelectMethod
		{
			get { return ((UsersPrivileged3DataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new UsersPrivileged3DataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region UsersPrivileged3DataSourceActionList

	/// <summary>
	/// Supports the UsersPrivileged3DataSourceDesigner class.
	/// </summary>
	internal class UsersPrivileged3DataSourceActionList : DesignerActionList
	{
		private UsersPrivileged3DataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the UsersPrivileged3DataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public UsersPrivileged3DataSourceActionList(UsersPrivileged3DataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public UsersPrivileged3SelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion UsersPrivileged3DataSourceActionList
	
	#endregion UsersPrivileged3DataSourceDesigner
	
	#region UsersPrivileged3SelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the UsersPrivileged3DataSource.SelectMethod property.
	/// </summary>
	public enum UsersPrivileged3SelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByUsersPrivilegedId method.
		/// </summary>
		GetByUsersPrivilegedId,
		/// <summary>
		/// Represents the GetByUserId method.
		/// </summary>
		GetByUserId
	}
	
	#endregion UsersPrivileged3SelectMethod

	#region UsersPrivileged3Filter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="UsersPrivileged3"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UsersPrivileged3Filter : SqlFilter<UsersPrivileged3Column>
	{
	}
	
	#endregion UsersPrivileged3Filter

	#region UsersPrivileged3ExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="UsersPrivileged3"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UsersPrivileged3ExpressionBuilder : SqlExpressionBuilder<UsersPrivileged3Column>
	{
	}
	
	#endregion UsersPrivileged3ExpressionBuilder	

	#region UsersPrivileged3Property
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;UsersPrivileged3ChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="UsersPrivileged3"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UsersPrivileged3Property : ChildEntityProperty<UsersPrivileged3ChildEntityTypes>
	{
	}
	
	#endregion UsersPrivileged3Property
}

