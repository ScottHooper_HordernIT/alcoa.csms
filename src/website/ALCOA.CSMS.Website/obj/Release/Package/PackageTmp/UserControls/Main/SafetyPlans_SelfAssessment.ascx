<%@ Control Language="C#" AutoEventWireup="true" Inherits="UserControls_SafetyPlans_SelfAssessment" Codebehind="SafetyPlans_SelfAssessment.ascx.cs" %>
<%@ Register Assembly="System.Web.Extensions" Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxUploadControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dxp" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxRoundPanel" TagPrefix="dxrp" %>


<asp:Panel ID="pnlContainer" runat="server" Width="900px"><TABLE style="FONT-SIZE: 14pt width="900"><TBODY><TR><TD style="WIDTH: 154px; TEXT-ALIGN: right" colSpan=2><STRONG>
    Company Name:</STRONG>&nbsp; </TD><TD style="WIDTH: 280px; TEXT-ALIGN: left"><asp:DropDownList id="ddlCompanies" Width="280px" runat="server" AutoPostBack="True" DataTextField="CompanyName" DataValueField="CompanyID" DataSourceID="sqldsCompaniesList" OnSelectedIndexChanged="ddlCompanies_SelectedIndexChanged" Enabled="False"></asp:DropDownList></TD><TD style="WIDTH: 467px; TEXT-ALIGN: right"><TABLE cellSpacing=0 cellPadding=1 border=0 style="width: 467px"><TBODY><TR><TD style="WIDTH: 142px; TEXT-ALIGN: center; height: 42px;" rowSpan=2>
    <dxe:ASPxButton id="ASPxButton1" runat="server" 
        CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
        CssPostfix="Office2003Blue" PostBackUrl="~/SafetyPlans.aspx" 
        Text="Save & Close" 
        SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"></dxe:ASPxButton> </TD>
    <td rowspan="2" style="width: 123px; height: 42px; text-align: left">
        <dxe:ASPxButton id="btnCompare" runat="server" 
            CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
            CssPostfix="Office2003Blue" Text="Compare" OnClick="btnCompare_Click" 
            Visible="False" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"></dxe:ASPxButton>
    </td>
    <TD style="WIDTH: 106px; TEXT-ALIGN: center; height: 42px;"><asp:Button id="btnAssess" runat="server" Text="View Feedback"></asp:Button></TD><TD style="WIDTH: 111px; TEXT-ALIGN: center; height: 42px;"><asp:Button id="btnPrint" runat="server" Text="Print Preview"></asp:Button></TD></TR></TBODY></TABLE></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=4><asp:Label id="Label1" runat="server" Font-Bold="True" Font-Size="12pt" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=4><SPAN style="COLOR: #660033"><STRONG><asp:Label id="Label3" runat="server" Text="Click question number or text to answer question." Font-Bold="True" ForeColor="Maroon"></asp:Label></STRONG></SPAN></TD></TR></TBODY></TABLE><TABLE style="BORDER-COLLAPSE: separate" id="Table1" class="dxgvControl_Office2003_Blue" cellSpacing=0 cellPadding=0 border=0><TBODY><TR><TD style="WIDTH: 900px; TEXT-ALIGN: center"><TABLE style="WIDTH: 100%; BORDER-COLLAPSE: collapse; empty-cells: show" class="dxgvControl_Office2003_Blue" cellSpacing=0 cellPadding=0 border=0><TBODY><TR><TD style="BORDER-TOP-WIDTH: 0px; FONT-WEIGHT: bold; BORDER-LEFT-WIDTH: 0px; WHITE-SPACE: normal" class="dxgvHeader_Office2003_Blue" colSpan=1></TD><TD style="BORDER-TOP-WIDTH: 0px; FONT-WEIGHT: bold; BORDER-LEFT-WIDTH: 0px; WHITE-SPACE: normal; TEXT-ALIGN: center" class="dxgvHeader_Office2003_Blue" colSpan=1>
    Question</TD><TD style="BORDER-TOP-WIDTH: 0px; FONT-WEIGHT: bold; BORDER-LEFT-WIDTH: 0px; WHITE-SPACE: normal; TEXT-ALIGN: center" class="dxgvHeader_Office2003_Blue" colSpan=1>
        Answer</TD><TD style="BORDER-TOP-WIDTH: 0px; FONT-WEIGHT: bold; BORDER-LEFT-WIDTH: 0px; WHITE-SPACE: normal; TEXT-ALIGN: center" class="dxgvHeader_Office2003_Blue" colSpan=1>
        More Info</TD>
                                <td class="dxgvHeader_Office2003_Blue" colspan="1" style="border-top-width: 0px;
                                    font-weight: bold; border-left-width: 0px; white-space: normal; text-align: center">
                                    Status</td>
                            </TR><TR class="dxgvDataRow_Office2003_Blue"><TD style="WIDTH: 20px; HEIGHT: 20px; TEXT-ALIGN: center" class="dxgv"><asp:LinkButton id="lb1_" runat="server" Font-Size="10pt">1</asp:LinkButton></TD><TD style="WIDTH: 770px; HEIGHT: 20px; TEXT-ALIGN: left" class="dxgv" id="custom">
                                <asp:LinkButton id="lb1" runat="server" Font-Size="10pt"></asp:LinkButton></TD><TD style="WIDTH: 62px; HEIGHT: 20px; TEXT-ALIGN: center" class="dxgv">
            &nbsp;<asp:Label id="lbl1a" runat="server" Text="" Visible="True"></asp:Label></TD><TD style="HEIGHT: 20px; TEXT-ALIGN: center" class="dxgv">
            &nbsp;<asp:Image id="img1m" runat="server" Visible="false" ImageUrl="~/Images/greentick.gif"></asp:Image></TD>
                                <td class="dxgv" style="height: 20px; text-align: center">
                                    <asp:Label ID="lbl1s" runat="server" Text="" Visible="True"></asp:Label></td>
                            </TR><TR class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue"><TD style="WIDTH: 20px; HEIGHT: 20px; TEXT-ALIGN: center" class="dxgv"><asp:LinkButton id="lb2_" runat="server" Font-Size="10pt">2</asp:LinkButton></TD><TD style="WIDTH: 770px; HEIGHT: 20px; TEXT-ALIGN: left" class="dxgv">
        &nbsp;<asp:LinkButton id="lb2" runat="server" Font-Size="10pt"></asp:LinkButton></TD><TD style="WIDTH: 62px; HEIGHT: 20px; TEXT-ALIGN: center" class="dxgv">
            &nbsp;<asp:Label id="lbl2a" runat="server" Text="" Visible="True"></asp:Label></TD><TD style="HEIGHT: 20px; TEXT-ALIGN: center" class="dxgv">
            &nbsp;<asp:Image id="img2m" runat="server" Visible="false" ImageUrl="~/Images/greentick.gif"></asp:Image></TD>
                                <td class="dxgv" style="height: 20px; text-align: center">
                                    <asp:Label ID="lbl2s" runat="server" Text="" Visible="True"></asp:Label></td>
                            </TR><TR class="dxgvDataRow_Office2003_Blue dxgvDataRow_Office2003_Blue"><TD style="WIDTH: 20px; TEXT-ALIGN: center" class="dxgv"><asp:LinkButton id="lb3_" runat="server" Font-Size="10pt">3</asp:LinkButton></TD><TD style="WIDTH: 770px; TEXT-ALIGN: left" class="dxgv">
        &nbsp;<asp:LinkButton id="lb3" runat="server" Font-Size="10pt"></asp:LinkButton></TD><TD style="WIDTH: 62px; TEXT-ALIGN: center" class="dxgv">
            &nbsp;<asp:Label id="lbl3a" runat="server" Text="" Visible="True"></asp:Label></TD><TD style="TEXT-ALIGN: center" class="dxgv">
            &nbsp;<asp:Image id="img3m" runat="server" Visible="false" ImageUrl="~/Images/greentick.gif"></asp:Image></TD>
                                <td class="dxgv" style="text-align: center">
                                    <asp:Label ID="lbl3s" runat="server" Text="" Visible="True"></asp:Label></td>
                            </TR><TR class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue"><TD style="WIDTH: 20px; HEIGHT: 20px; TEXT-ALIGN: center" class="dxgv"><asp:LinkButton id="lb4_" runat="server" Font-Size="10pt">4</asp:LinkButton></TD><TD style="WIDTH: 770px; HEIGHT: 20px; TEXT-ALIGN: left" class="dxgv">
        &nbsp;<asp:LinkButton id="lb4" runat="server" Font-Size="10pt"></asp:LinkButton></TD><TD style="WIDTH: 62px; HEIGHT: 20px; TEXT-ALIGN: center" class="dxgv">
            &nbsp;<asp:Label id="lbl4a" runat="server" Text="" Visible="True"></asp:Label></TD><TD style="HEIGHT: 20px; TEXT-ALIGN: center" class="dxgv">
            &nbsp;<asp:Image id="img4m" runat="server" Visible="false" ImageUrl="~/Images/greentick.gif"></asp:Image></TD>
                                <td class="dxgv" style="height: 20px; text-align: center">
                                    <asp:Label ID="lbl4s" runat="server" Text="" Visible="True"></asp:Label></td>
                            </TR><TR class="dxgvDataRow_Office2003_Blue dxgvDataRow_Office2003_Blue"><TD style="WIDTH: 20px; HEIGHT: 16px; TEXT-ALIGN: center" class="dxgv"><asp:LinkButton id="lb5_" runat="server" Font-Size="10pt">5</asp:LinkButton></TD><TD style="WIDTH: 770px; HEIGHT: 16px; TEXT-ALIGN: left" class="dxgv">
        &nbsp;<asp:LinkButton id="lb5" runat="server" Font-Size="10pt"></asp:LinkButton></TD><TD style="WIDTH: 62px; HEIGHT: 16px; TEXT-ALIGN: center" class="dxgv">
            &nbsp;<asp:Label id="lbl5a" runat="server" Text="" Visible="True"></asp:Label></TD><TD style="HEIGHT: 16px; TEXT-ALIGN: center" class="dxgv">
            &nbsp;<asp:Image id="img5m" runat="server" Visible="false" ImageUrl="~/Images/greentick.gif"></asp:Image></TD>
                                <td class="dxgv" style="height: 16px; text-align: center">
                                    <asp:Label ID="lbl5s" runat="server" Text="" Visible="True"></asp:Label></td>
                            </TR><TR class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue"><TD style="WIDTH: 20px; HEIGHT: 20px; TEXT-ALIGN: center" class="dxgv"><asp:LinkButton id="lb6_" runat="server" Font-Size="10pt">6</asp:LinkButton></TD><TD style="WIDTH: 770px; HEIGHT: 20px; TEXT-ALIGN: left" class="dxgv">
        &nbsp;<asp:LinkButton id="lb6" runat="server" Font-Size="10pt"></asp:LinkButton></TD><TD style="WIDTH: 62px; HEIGHT: 20px; TEXT-ALIGN: center" class="dxgv">
            &nbsp;<asp:Label id="lbl6a" runat="server" Text="" Visible="True"></asp:Label></TD><TD style="HEIGHT: 20px; TEXT-ALIGN: center" class="dxgv">
            &nbsp;<asp:Image id="img6m" runat="server" Visible="false" ImageUrl="~/Images/greentick.gif"></asp:Image></TD>
                                <td class="dxgv" style="height: 20px; text-align: center">
                                    <asp:Label ID="lbl6s" runat="server" Text="" Visible="True"></asp:Label></td>
                            </TR><TR class="dxgvDataRow_Office2003_Blue dxgvDataRow_Office2003_Blue"><TD style="WIDTH: 20px; TEXT-ALIGN: center; height: 20px;" class="dxgv"><asp:LinkButton id="lb7_" runat="server" Font-Size="10pt">7</asp:LinkButton></TD><TD style="WIDTH: 770px; TEXT-ALIGN: left; height: 20px;" class="dxgv">
        &nbsp;<asp:LinkButton id="lb7" runat="server" Font-Size="10pt"></asp:LinkButton></TD><TD style="WIDTH: 62px; TEXT-ALIGN: center; height: 20px;" class="dxgv">
            &nbsp;<asp:Label id="lbl7a" runat="server" Text="" Visible="True"></asp:Label></TD><TD style="TEXT-ALIGN: center; height: 20px;" class="dxgv">
            &nbsp;<asp:Image id="img7m" runat="server" Visible="false" ImageUrl="~/Images/greentick.gif"></asp:Image></TD>
                                <td class="dxgv" style="height: 20px; text-align: center">
                                    <asp:Label ID="lbl7s" runat="server" Text="" Visible="True"></asp:Label></td>
                            </TR><TR class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue"><TD style="WIDTH: 20px; HEIGHT: 20px; TEXT-ALIGN: center" class="dxgv"><asp:LinkButton id="lb8_" runat="server" Font-Size="10pt">8</asp:LinkButton></TD><TD style="WIDTH: 770px; HEIGHT: 20px; TEXT-ALIGN: left" class="dxgv">
        &nbsp;<asp:LinkButton id="lb8" runat="server" Font-Size="10pt"></asp:LinkButton></TD><TD style="WIDTH: 62px; HEIGHT: 20px; TEXT-ALIGN: center" class="dxgv">
            &nbsp;<asp:Label id="lbl8a" runat="server" Text="" Visible="True"></asp:Label></TD><TD style="HEIGHT: 20px; TEXT-ALIGN: center" class="dxgv">
            &nbsp;<asp:Image id="img8m" runat="server" Visible="false" ImageUrl="~/Images/greentick.gif"></asp:Image></TD>
                                <td class="dxgv" style="height: 20px; text-align: center">
                                    <asp:Label ID="lbl8s" runat="server" Text="" Visible="True"></asp:Label></td>
                            </TR><TR class="dxgvDataRow_Office2003_Blue dxgvDataRow_Office2003_Blue"><TD style="WIDTH: 20px; TEXT-ALIGN: center" class="dxgv"><asp:LinkButton id="lb9_" runat="server" Font-Size="10pt">9</asp:LinkButton></TD><TD style="WIDTH: 770px; TEXT-ALIGN: left" class="dxgv">
        &nbsp;<asp:LinkButton id="lb9" runat="server" Font-Size="10pt"></asp:LinkButton></TD><TD style="WIDTH: 62px; TEXT-ALIGN: center" class="dxgv">
            &nbsp;<asp:Label id="lbl9a" runat="server" Text="" Visible="True"></asp:Label></TD><TD style="TEXT-ALIGN: center" class="dxgv">
            &nbsp;<asp:Image id="img9m" runat="server" Visible="false" ImageUrl="~/Images/greentick.gif"></asp:Image></TD>
                                <td class="dxgv" style="text-align: center">
                                    <asp:Label ID="lbl9s" runat="server" Text="" Visible="True"></asp:Label></td>
                            </TR><TR class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue"><TD style="WIDTH: 20px; TEXT-ALIGN: center; height: 20px;" class="dxgv"><asp:LinkButton id="lb10_" runat="server" Font-Size="10pt">10</asp:LinkButton></TD><TD style="WIDTH: 770px; TEXT-ALIGN: left; height: 20px;" class="dxgv">
        &nbsp;<asp:LinkButton id="lb10" runat="server" Font-Size="10pt"></asp:LinkButton></TD><TD style="WIDTH: 62px; TEXT-ALIGN: center; height: 20px;" class="dxgv">
            &nbsp;<asp:Label id="lbl10a" runat="server" Text="" Visible="True"></asp:Label></TD><TD style="TEXT-ALIGN: center; height: 20px;" class="dxgv">
            &nbsp;<asp:Image id="img10m" runat="server" Visible="false" ImageUrl="~/Images/greentick.gif"></asp:Image></TD>
                                <td class="dxgv" style="height: 20px; text-align: center">
                                    <asp:Label ID="lbl10s" runat="server" Text="" Visible="True"></asp:Label></td>
                            </TR></TBODY></TABLE><asp:Label id="Label2" runat="server" Text="Please note that you can only upload your Safety Management Plan when you have answered all of the above questions." Font-Bold="True" ForeColor="Maroon"></asp:Label></TD></TR></TBODY></TABLE><asp:Panel id="Panel1" Width="900px" runat="server" Enabled="False"><BR /><asp:UpdatePanel id="UpdatePanel10" runat="server" RenderMode="Inline" UpdateMode="Conditional"><ContentTemplate>
<TABLE style="FONT-SIZE: 14pt" width=900><TBODY><TR><TD style="WIDTH: 75px; TEXT-ALIGN: center; height: 24px;" colSpan=1></TD><TD style="WIDTH: 287px; TEXT-ALIGN: right; height: 24px;" colSpan=2><STRONG>
    File Description:</STRONG></TD><TD style="WIDTH: 450px; TEXT-ALIGN: left; height: 24px;"><asp:DropDownList id="ddlFileDesc" Width="100%" runat="server" OnSelectedIndexChanged="ddlCompanies_SelectedIndexChanged"></asp:DropDownList> </TD><TD style="TEXT-ALIGN: left; height: 24px;" width=75></TD></TR>
    <TR><TD style="WIDTH: 75px; TEXT-ALIGN: center" colSpan=1></TD><TD style="WIDTH: 287px; TEXT-ALIGN: right" colSpan=2><STRONG>
        File:</STRONG></TD><TD style="TEXT-ALIGN: left">
            <dx:ASPxUploadControl ID="fUpload" runat="server" Width="100%" ShowProgressPanel="true">
            <ValidationSettings
                AllowedFileExtensions=".txt, .pdf, .xls, .xlsx, .doc, .docx, .ppt, .pptx, .zip, .jpg, .jpeg, .png"
                GeneralErrorText="File Upload Failed."
                MaxFileSize="16252928" MaxFileSizeErrorText="File size exceeds the maximum allowed size of 15Mb (maybe consider ZIPing the file?)"
                NotAllowedFileExtensionErrorText="File NOT uploaded. Please upload only files of the following file type. All Other file types will not be uploaded: Text (TXT), Excel (XLS, XLSX), Word Document (DOC, DOCX), PowerPoint (PPT, PPTX), Adobe PDF (PDF), ZIP Compressed File (ZIP), Image (PNG/JPG)">
            </ValidationSettings>
        </dx:ASPxUploadControl>
            
            </TD><TD style="TEXT-ALIGN: left" width=75></TD></TR>
    <tr>
        <td colspan="1" style="width: 75px; text-align: center">
        </td>
        <td colspan="2" style="width: 287px; text-align: right">
        </td>
        <td style="text-align: left">
            </td>
        <td style="text-align: left" width="75">
        </td>
    </tr>
    <TR><TD style="WIDTH: 75px; HEIGHT: 15px; TEXT-ALIGN: center" colSpan=1></TD><TD style="HEIGHT: 15px; TEXT-ALIGN: center" colSpan=3><asp:CheckBox id="cbNoUpload" runat="server" Text="Check this box if you elect not to upload a copy of your SMP (see below)" ForeColor="Maroon" Font-Italic="True"></asp:CheckBox></TD><TD style="HEIGHT: 15px; TEXT-ALIGN: center" width=75 colSpan=1></TD></TR><TR><TD style="WIDTH: 75px; HEIGHT: 15px; TEXT-ALIGN: left" colSpan=1></TD><TD style="HEIGHT: 15px; TEXT-ALIGN: left" colSpan=3><SPAN style="FONT-SIZE: 12pt"><SPAN style="COLOR: #c00000"><SPAN style="FONT-FAMILY: Calibri">
    You may elect not to upload a copy of your Safety Management Plan because of one 
    of the following reasons:<?xml namespace="" ns="urn:schemas-microsoft-com:office:office"
                        prefix="o" ?><?xml:namespace prefix = o /><o:p></o:p></SPAN></SPAN></SPAN></TD><TD style="HEIGHT: 15px; TEXT-ALIGN: left" width=75 colSpan=1></TD></TR><TR><TD style="WIDTH: 75px; HEIGHT: 15px; TEXT-ALIGN: center" colSpan=1></TD><TD style="HEIGHT: 15px; TEXT-ALIGN: center" colSpan=3><SPAN style="FONT-SIZE: 12pt"><SPAN style="COLOR: #c00000"><SPAN style="FONT-FAMILY: Calibri"></SPAN></SPAN></SPAN><P style="MARGIN: 0cm 0cm 0pt; TEXT-ALIGN: left" class="MsoNormal"><SPAN style="COLOR: #c00000; FONT-FAMILY: 'Calibri','sans-serif'"><SPAN style="FONT-SIZE: 12pt">
    1) Your Safety Management Plan is overly large (over 8mb is not recommended) and 
    uploading is not practical.<o:p></o:p></SPAN></SPAN></P><P style="MARGIN: 0cm 0cm 0pt; TEXT-ALIGN: left" class="MsoNormal"><SPAN style="FONT-SIZE: 12pt; COLOR: #c00000; FONT-FAMILY: 'Calibri','sans-serif'">
        2) Your Safety Management Plan is made up of more than one document.<o:p></o:p></SPAN></P><P style="MARGIN: 0cm 0cm 0pt; TEXT-ALIGN: left" class="MsoNormal"><SPAN style="FONT-SIZE: 12pt; COLOR: #c00000; FONT-FAMILY: 'Calibri','sans-serif'"><o:p>&nbsp;</o:p> </SPAN></P><P style="MARGIN: 0cm 0cm 0pt; TEXT-ALIGN: left" class="MsoNormal"><SPAN style="FONT-SIZE: 12pt; COLOR: #c00000; FONT-FAMILY: 'Calibri','sans-serif'">
    By electing not to upload your Safety Management Plan you hereby acknowledge as 
    the person responsible for completing the Safety Plan Questionnaire above that 
    you will submit your Safety Management Plan by other means, such as copying to a 
    CD or DVD and sending this to:</SPAN></P>
                            <p class="MsoNormal" style="margin: 0cm 0cm 0pt; text-align: left">
                                <span style="font-size: 10pt; color: #cc0033; font-family: Arial"><strong></strong></span>
                                &nbsp;</p>
                            <p class="MsoNormal" style="margin: 0cm 0cm 0pt; text-align: left">
                                <span style="font-size: 10pt; color: #cc0033; font-family: Arial"><strong>&nbsp; &nbsp; &nbsp; 
                                &nbsp; &nbsp;&nbsp; Contractor Management Administration Assistant</strong></span></p>
                            <p class="MsoNormal" style="margin: 0cm 0cm 0pt; text-align: left">
                                <span></span><span style="font-size: 10pt; color: #cc0033; font-family: Arial"><strong>
                                    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; Alcoa Pinjarra Refinery</strong></span></p>
                            <p class="MsoNormal" style="margin: 0cm 0cm 0pt; text-align: left">
                                <span style="font-size: 10pt; color: #cc0033; font-family: Arial"><strong>&nbsp; &nbsp; &nbsp; 
                                &nbsp; &nbsp;&nbsp; Pinjarra WA 6028</strong></span></p>
                        </TD><TD style="HEIGHT: 15px; TEXT-ALIGN: center; color: #000000;" width=75 colSpan=1></TD></TR>
    <tr style="color: #000000">
        <td colspan="1" style="width: 75px; height: 15px; text-align: center">
        </td>
        <td colspan="3" style="height: 15px; text-align: center">
        </td>
        <td colspan="1" style="height: 15px; text-align: center" width="75">
        </td>
    </tr>
    <tr style="color: #000000">
        <td colspan="1" style="width: 75px; height: 11px; text-align: center">
        </td>
        <td colspan="3" style="height: 11px; text-align: center">
            <dxe:ASPxCheckBox ID="ASPxCheckBox1" runat="server" Font-Size="Medium" ForeColor="Red"
                Text="Ticking this box verifies that you have reviewed and are submitting this SMP as the latest version for your company."><ValidationSettings CausesValidation="True" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True" ValidationGroup="popup"><RequiredField IsRequired="True" /></ValidationSettings></dxe:ASPxCheckBox>
        </td>
        <td colspan="1" style="height: 11px; text-align: center" width="75">
        </td>
    </tr>
    <TR style="color: #000000"><TD style="WIDTH: 75px; TEXT-ALIGN: center; height: 46px;" colSpan=1></TD><TD style="TEXT-ALIGN: center; height: 46px;" colSpan=3>
        <dxe:ASPxButton ID="btnUpload" runat="server" Height="31px" Native="True" OnClick="btnUpload_Click"
            Text="Upload / Submit" ValidationGroup="popup" Width="140px">
        </dxe:ASPxButton>
</TD><TD style="TEXT-ALIGN: center; height: 46px;" width=75 colSpan=1></TD></TR>
    </TBODY></TABLE>
</ContentTemplate>
<Triggers>
<asp:PostBackTrigger ControlID="btnUpload"></asp:PostBackTrigger>
</Triggers>
</asp:UpdatePanel>

<asp:SqlDataSource ID="sqldsCompaniesList" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="_Companies_GetCompanyNameList" SelectCommandType="StoredProcedure">
</asp:SqlDataSource>

</asp:Panel> </asp:Panel>
<table border="0" cellpadding="0" cellspacing="0" style="width: 900px">
    <tr>
        <td style="text-align: center">
            <br />
            <dxrp:ASPxRoundPanel ID="ASPxRoundPanel1" runat="server" BackColor="#DDECFE" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                CssPostfix="Office2003Blue" EnableDefaultAppearance="False" 
                HeaderText="Upload Safety Management Plan submitted by other means, such as CD or DVD" 
                Width="900px" Visible="False" 
                SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                <TopEdge>
                    <BackgroundImage HorizontalPosition="left" ImageUrl="~/App_Themes/Office2003Blue/Web/rpTopEdge.png"
                        Repeat="RepeatX" VerticalPosition="top" />
                </TopEdge>
                <BottomRightCorner Height="9px"
                    Width="9px" />
                <ContentPaddings Padding="2px" PaddingBottom="10px" PaddingTop="10px" />
                <NoHeaderTopRightCorner Height="9px"
                    Width="9px" />
                <HeaderRightEdge>
                    <BackgroundImage HorizontalPosition="left" ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png"
                        Repeat="RepeatX" VerticalPosition="top" />
                </HeaderRightEdge>
                <Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px" />
                <HeaderLeftEdge>
                    <BackgroundImage HorizontalPosition="left" ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png"
                        Repeat="RepeatX" VerticalPosition="top" />
                </HeaderLeftEdge>
                <HeaderStyle BackColor="#7BA4E0">
                    <Paddings Padding="0px" PaddingBottom="7px" PaddingLeft="2px" PaddingRight="2px" />
                    <BorderBottom BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px" />
                </HeaderStyle>
                <TopRightCorner Height="9px"
                    Width="9px" />
                <HeaderContent>
                    <BackgroundImage HorizontalPosition="left" ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png"
                        Repeat="RepeatX" VerticalPosition="top" />
                </HeaderContent>
                <DisabledStyle ForeColor="Gray">
                </DisabledStyle>
                <NoHeaderTopEdge BackColor="#DDECFE">
                </NoHeaderTopEdge>
                <NoHeaderTopLeftCorner Height="9px"
                    Width="9px" />
                <PanelCollection>
                    <dxp:PanelContent ID="PanelContent1" runat="server">
<TABLE style="WIDTH: 872px" cellSpacing=0 cellPadding=0 border=0><TBODY><TR><TD style="HEIGHT: 13px; TEXT-ALIGN: left">
    Currently uploaded SMP: <asp:HyperLink id="hlUploadedFile" runat="server">HyperLink</asp:HyperLink></TD></TR>
    </TBODY>
    </TABLE>
                                <br />
                                <dxp:ASPxPanel id="panelUploadModify" runat="server" Width="100%">
                                    <panelcollection>
<dxp:PanelContent ID="PanelContent2" runat="server"><asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="UpdatePanel1"><ContentTemplate>
<TABLE style="WIDTH: 872px" cellSpacing=0 cellPadding=0 border=0><TBODY><TR style="COLOR: #000000"><TD style="HEIGHT: 13px; TEXT-ALIGN: left">You may only upload a SMP where the currently uploaded SMP filename = 'SMP Not Uploaded.pdf'</TD></TR><TR><TD style="HEIGHT: 13px; TEXT-ALIGN: left">&nbsp;</TD></TR><TR><TD style="HEIGHT: 13px; TEXT-ALIGN: left"><dx:ASPxUploadControl id="fUpload_Modify" __designer:dtid="20547673299878061" Width="100%" runat="server" ShowProgressPanel="true" __designer:wfdid="w4">
<ValidationSettings AllowedFileExtensions=".txt, .pdf, .xls, .xlsx, .doc, .docx, .ppt, .pptx, .zip, .jpg, .jpeg, .png" NotAllowedFileExtensionErrorText="File NOT uploaded. Please upload only files of the following file type. All Other file types will not be uploaded: Text (TXT), Excel (XLS, XLSX), Word Document (DOC, DOCX), PowerPoint (PPT, PPTX), Adobe PDF (PDF), ZIP Compressed File (ZIP), Image (JPG/PNG)" MaxFileSize="16252928" MaxFileSizeErrorText="File size exceeds the maximum allowed size of 15Mb (maybe consider ZIPing the file?)" GeneralErrorText="File Upload Failed." __designer:dtid="20547673299878062"></ValidationSettings>
</dx:ASPxUploadControl></TD></TR><TR><TD style="HEIGHT: 13px; TEXT-ALIGN: center"><dxe:ASPxButton id="btnUpload_Modify" onclick="btnUploadModify_Click" Width="140px" Height="31px" runat="server" Text="Upload" ValidationGroup="upload" Native="True"></dxe:ASPxButton> </TD></TR></TBODY></TABLE>
</ContentTemplate>
<Triggers>
<asp:PostBackTrigger ControlID="btnUpload_Modify"></asp:PostBackTrigger>
</Triggers>
</asp:UpdatePanel>

 </dxp:PanelContent>
</panelcollection>
                                </dxp:ASPxPanel>
    

                    </dxp:PanelContent>
                </PanelCollection>
                <TopLeftCorner Height="9px"
                    Width="9px" />
                <BottomLeftCorner Height="9px"
                    Width="9px" />
            </dxrp:ASPxRoundPanel>
        </td>
    </tr>
</table>
<br />

