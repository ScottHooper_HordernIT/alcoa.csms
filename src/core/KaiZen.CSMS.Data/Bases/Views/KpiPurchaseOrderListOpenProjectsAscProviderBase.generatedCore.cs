﻿#region Using directives

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="KpiPurchaseOrderListOpenProjectsAscProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract class KpiPurchaseOrderListOpenProjectsAscProviderBaseCore : EntityViewProviderBase<KpiPurchaseOrderListOpenProjectsAsc>
	{
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions
		
		/*
		///<summary>
		/// Fill an VList&lt;KpiPurchaseOrderListOpenProjectsAsc&gt; From a DataSet
		///</summary>
		/// <param name="dataSet">the DataSet</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList&lt;KpiPurchaseOrderListOpenProjectsAsc&gt;"/></returns>
		protected static VList&lt;KpiPurchaseOrderListOpenProjectsAsc&gt; Fill(DataSet dataSet, VList<KpiPurchaseOrderListOpenProjectsAsc> rows, int start, int pagelen)
		{
			if (dataSet.Tables.Count == 1)
			{
				return Fill(dataSet.Tables[0], rows, start, pagelen);
			}
			else
			{
				return new VList<KpiPurchaseOrderListOpenProjectsAsc>();
			}	
		}
		
		
		///<summary>
		/// Fill an VList&lt;KpiPurchaseOrderListOpenProjectsAsc&gt; From a DataTable
		///</summary>
		/// <param name="dataTable">the DataTable that hold the data.</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList<KpiPurchaseOrderListOpenProjectsAsc>"/></returns>
		protected static VList&lt;KpiPurchaseOrderListOpenProjectsAsc&gt; Fill(DataTable dataTable, VList<KpiPurchaseOrderListOpenProjectsAsc> rows, int start, int pagelen)
		{
			int recordnum = 0;
			
			System.Collections.IEnumerator dataRows =  dataTable.Rows.GetEnumerator();
			
			while (dataRows.MoveNext() && (pagelen != 0))
			{
				if(recordnum >= start)
				{
					DataRow row = (DataRow)dataRows.Current;
				
					KpiPurchaseOrderListOpenProjectsAsc c = new KpiPurchaseOrderListOpenProjectsAsc();
					c.PurchaseOrderNumber = (Convert.IsDBNull(row["PurchaseOrderNumber"]))?string.Empty:(System.String)row["PurchaseOrderNumber"];
					c.PurchaseOrderLineNumber = (Convert.IsDBNull(row["PurchaseOrderLineNumber"]))?string.Empty:(System.String)row["PurchaseOrderLineNumber"];
					c.ProjectNumber = (Convert.IsDBNull(row["ProjectNumber"]))?string.Empty:(System.String)row["ProjectNumber"];
					c.ProjectDesc = (Convert.IsDBNull(row["ProjectDesc"]))?string.Empty:(System.String)row["ProjectDesc"];
					c.AcceptChanges();
					rows.Add(c);
					pagelen -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		*/	
						
		///<summary>
		/// Fill an <see cref="VList&lt;KpiPurchaseOrderListOpenProjectsAsc&gt;"/> From a DataReader.
		///</summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pageLength">number of row.</param>
		///<returns>a <see cref="VList&lt;KpiPurchaseOrderListOpenProjectsAsc&gt;"/></returns>
		protected VList<KpiPurchaseOrderListOpenProjectsAsc> Fill(IDataReader reader, VList<KpiPurchaseOrderListOpenProjectsAsc> rows, int start, int pageLength)
		{
			int recordnum = 0;
			while (reader.Read() && (pageLength != 0))
			{
				if(recordnum >= start)
				{
					KpiPurchaseOrderListOpenProjectsAsc entity = null;
					if (DataRepository.Provider.UseEntityFactory)
					{
						entity = EntityManager.CreateViewEntity<KpiPurchaseOrderListOpenProjectsAsc>("KpiPurchaseOrderListOpenProjectsAsc",  DataRepository.Provider.EntityCreationalFactoryType); 
					}
					else
					{
						entity = new KpiPurchaseOrderListOpenProjectsAsc();
					}
					
					entity.SuppressEntityEvents = true;

					entity.PurchaseOrderNumber = (System.String)reader[((int)KpiPurchaseOrderListOpenProjectsAscColumn.PurchaseOrderNumber)];
					//entity.PurchaseOrderNumber = (Convert.IsDBNull(reader["PurchaseOrderNumber"]))?string.Empty:(System.String)reader["PurchaseOrderNumber"];
					entity.PurchaseOrderLineNumber = (System.String)reader[((int)KpiPurchaseOrderListOpenProjectsAscColumn.PurchaseOrderLineNumber)];
					//entity.PurchaseOrderLineNumber = (Convert.IsDBNull(reader["PurchaseOrderLineNumber"]))?string.Empty:(System.String)reader["PurchaseOrderLineNumber"];
					entity.ProjectNumber = (System.String)reader[((int)KpiPurchaseOrderListOpenProjectsAscColumn.ProjectNumber)];
					//entity.ProjectNumber = (Convert.IsDBNull(reader["ProjectNumber"]))?string.Empty:(System.String)reader["ProjectNumber"];
					entity.ProjectDesc = (reader.IsDBNull(((int)KpiPurchaseOrderListOpenProjectsAscColumn.ProjectDesc)))?null:(System.String)reader[((int)KpiPurchaseOrderListOpenProjectsAscColumn.ProjectDesc)];
					//entity.ProjectDesc = (Convert.IsDBNull(reader["ProjectDesc"]))?string.Empty:(System.String)reader["ProjectDesc"];
					entity.AcceptChanges();
					entity.SuppressEntityEvents = false;
					
					rows.Add(entity);
					pageLength -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		
		
		/// <summary>
		/// Refreshes the <see cref="KpiPurchaseOrderListOpenProjectsAsc"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KpiPurchaseOrderListOpenProjectsAsc"/> object to refresh.</param>
		protected void RefreshEntity(IDataReader reader, KpiPurchaseOrderListOpenProjectsAsc entity)
		{
			reader.Read();
			entity.PurchaseOrderNumber = (System.String)reader[((int)KpiPurchaseOrderListOpenProjectsAscColumn.PurchaseOrderNumber)];
			//entity.PurchaseOrderNumber = (Convert.IsDBNull(reader["PurchaseOrderNumber"]))?string.Empty:(System.String)reader["PurchaseOrderNumber"];
			entity.PurchaseOrderLineNumber = (System.String)reader[((int)KpiPurchaseOrderListOpenProjectsAscColumn.PurchaseOrderLineNumber)];
			//entity.PurchaseOrderLineNumber = (Convert.IsDBNull(reader["PurchaseOrderLineNumber"]))?string.Empty:(System.String)reader["PurchaseOrderLineNumber"];
			entity.ProjectNumber = (System.String)reader[((int)KpiPurchaseOrderListOpenProjectsAscColumn.ProjectNumber)];
			//entity.ProjectNumber = (Convert.IsDBNull(reader["ProjectNumber"]))?string.Empty:(System.String)reader["ProjectNumber"];
			entity.ProjectDesc = (reader.IsDBNull(((int)KpiPurchaseOrderListOpenProjectsAscColumn.ProjectDesc)))?null:(System.String)reader[((int)KpiPurchaseOrderListOpenProjectsAscColumn.ProjectDesc)];
			//entity.ProjectDesc = (Convert.IsDBNull(reader["ProjectDesc"]))?string.Empty:(System.String)reader["ProjectDesc"];
			reader.Close();
	
			entity.AcceptChanges();
		}
		
		/*
		/// <summary>
		/// Refreshes the <see cref="KpiPurchaseOrderListOpenProjectsAsc"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KpiPurchaseOrderListOpenProjectsAsc"/> object.</param>
		protected static void RefreshEntity(DataSet dataSet, KpiPurchaseOrderListOpenProjectsAsc entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.PurchaseOrderNumber = (Convert.IsDBNull(dataRow["PurchaseOrderNumber"]))?string.Empty:(System.String)dataRow["PurchaseOrderNumber"];
			entity.PurchaseOrderLineNumber = (Convert.IsDBNull(dataRow["PurchaseOrderLineNumber"]))?string.Empty:(System.String)dataRow["PurchaseOrderLineNumber"];
			entity.ProjectNumber = (Convert.IsDBNull(dataRow["ProjectNumber"]))?string.Empty:(System.String)dataRow["ProjectNumber"];
			entity.ProjectDesc = (Convert.IsDBNull(dataRow["ProjectDesc"]))?string.Empty:(System.String)dataRow["ProjectDesc"];
			entity.AcceptChanges();
		}
		*/
			
		#endregion Helper Functions
	}//end class

	#region KpiPurchaseOrderListOpenProjectsAscFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="KpiPurchaseOrderListOpenProjectsAsc"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class KpiPurchaseOrderListOpenProjectsAscFilterBuilder : SqlFilterBuilder<KpiPurchaseOrderListOpenProjectsAscColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the KpiPurchaseOrderListOpenProjectsAscFilterBuilder class.
		/// </summary>
		public KpiPurchaseOrderListOpenProjectsAscFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the KpiPurchaseOrderListOpenProjectsAscFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public KpiPurchaseOrderListOpenProjectsAscFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the KpiPurchaseOrderListOpenProjectsAscFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public KpiPurchaseOrderListOpenProjectsAscFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion KpiPurchaseOrderListOpenProjectsAscFilterBuilder

	#region KpiPurchaseOrderListOpenProjectsAscParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="KpiPurchaseOrderListOpenProjectsAsc"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class KpiPurchaseOrderListOpenProjectsAscParameterBuilder : ParameterizedSqlFilterBuilder<KpiPurchaseOrderListOpenProjectsAscColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the KpiPurchaseOrderListOpenProjectsAscParameterBuilder class.
		/// </summary>
		public KpiPurchaseOrderListOpenProjectsAscParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the KpiPurchaseOrderListOpenProjectsAscParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public KpiPurchaseOrderListOpenProjectsAscParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the KpiPurchaseOrderListOpenProjectsAscParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public KpiPurchaseOrderListOpenProjectsAscParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion KpiPurchaseOrderListOpenProjectsAscParameterBuilder
	
	#region KpiPurchaseOrderListOpenProjectsAscSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="KpiPurchaseOrderListOpenProjectsAsc"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class KpiPurchaseOrderListOpenProjectsAscSortBuilder : SqlSortBuilder<KpiPurchaseOrderListOpenProjectsAscColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the KpiPurchaseOrderListOpenProjectsAscSqlSortBuilder class.
		/// </summary>
		public KpiPurchaseOrderListOpenProjectsAscSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion KpiPurchaseOrderListOpenProjectsAscSortBuilder

} // end namespace
