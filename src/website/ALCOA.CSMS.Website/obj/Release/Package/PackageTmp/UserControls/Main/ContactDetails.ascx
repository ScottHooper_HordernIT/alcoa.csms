﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ContactDetails.ascx.cs" Inherits="ALCOA.CSMS.Website.UserControls.Main.ContactDetails" %>

<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Src="~/UserControls/Other/ExportButtons.ascx" TagName="ExportButtons" TagPrefix="uc1" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"  Namespace="System.Web.UI" TagPrefix="asp" %>

    <table border="0" cellpadding="2" cellspacing="0" width="100%">
    <tr>
        <td class="pageName" style="height: 16px;">
            <span class="bodycopy"><span class="title">Compliance Report</span><br />
               <span class="date">Contact Details</span><br />
                <img src="images/grfc_dottedline.gif" width="24" height="1" alt=""/></span>
        </td>
        <td align="right" class="pageName" style="height: 35px;">
            <a href="javascript:ShowHideCustomizationWindow()">
                <span style="color: #0000ff; text-decoration: none">Customize</span>
            </a>
        </td>
    </tr>
    
     <tr style="height:16px">
        <td colspan="2"></td>
     </tr>
            
     <tr>
        <td style="height: 28px; width: 1170px;" colspan="2">
            
            
            <dx:ASPxGridView ID="ASPxGridView1" ClientInstanceName="grid" runat="server" AutoGenerateColumns="False"
                CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" DataSourceID="ContactDetailsDataSource" CssPostfix="Office2003Blue"
                 Width="900px" OnHtmlRowCreated="grid_RowCreated" OnCustomColumnDisplayText="grid_CustomColumnDisplayText" KeyFieldName="CompanyId">
                 <Columns>
                     <dx:GridViewDataComboBoxColumn Caption="Company" FieldName="CompanyId" 
                         SortOrder="Ascending" VisibleIndex="0" >
                        <PropertiesComboBox DataSourceID="CompaniesDataSource1" DropDownHeight="150px" TextField="CompanyName"
                            ValueField="CompanyId" ValueType="System.Int32" IncrementalFilteringMode="StartsWith">
                        </PropertiesComboBox>
                        <Settings SortMode="DisplayText" />
                        <HeaderStyle Wrap="True" />
                    </dx:GridViewDataComboBoxColumn>
                    <dx:GridViewDataCheckColumn FieldName="IsOver90Days" VisibleIndex="1" Width="75px" Caption="Updated In Last 90 Days?">
                        <PropertiesCheckEdit DisplayTextUnchecked="No" DisplayTextChecked="Yes" ValueChecked="0" ValueUnchecked="1" ValueType="System.String"></PropertiesCheckEdit>
                        <DataItemTemplate>
                        <table cellpadding="0" cellspacing="0">
                        <tr>
                            <td>
                                <asp:Image ID="cImage" runat="server" ImageUrl="~/Images/spacer.gif" Visible="true"
                                    GenerateEmptyAlternateText="True" />
                            </td>
                        </tr>
                        </table>
                        </DataItemTemplate>
                        <HeaderStyle HorizontalAlign="Center" Wrap="True"/>
                        <CellStyle HorizontalAlign="Center">
                        </CellStyle>
                    </dx:GridViewDataCheckColumn>
                    <dx:GridViewDataDateColumn Caption="Date Last Updated" FieldName="ContactsLastUpdated" VisibleIndex="2" Width="135px" PropertiesDateEdit-DisplayFormatString="dd/MM/yyyy">
                        <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy" ></PropertiesDateEdit>
                    </dx:GridViewDataDateColumn>
                    <dx:GridViewDataTextColumn FieldName="DaysContactsLastUpdatedOver90" VisibleIndex="3" Caption="Number of Days > 90 Days" Width="90px">
                        <HeaderStyle HorizontalAlign="Center" Wrap="True"/>
                        <CellStyle HorizontalAlign="Center"></CellStyle>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="KpiAdministratorDetails" VisibleIndex="4" Visible="false" Caption="KPI Administrator - Site - Details (Full Name / Email / Phone)" Width="250px">
                        <HeaderStyle Wrap="True"/>
                    </dx:GridViewDataTextColumn>
                </Columns>
                <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                    <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
                    </LoadingPanelOnStatusBar>
                    <LoadingPanel Url="~/App_Themes/Office2003Blue/GridView/Loading.gif">
                    </LoadingPanel>
                </Images>
                <Settings ShowFilterRow="true" ShowFilterBar="Visible" />
                <SettingsPager PageSize="100" AlwaysShowPager="True">
                    <AllButton Visible="True">
                    </AllButton>
                    <NextPageButton>
                    </NextPageButton>
                    <PrevPageButton>
                    </PrevPageButton>
                    
                </SettingsPager>
                
                 <ImagesFilterControl>
                    <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
                    </LoadingPanel>
                </ImagesFilterControl>
                <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue">
                    <Header ImageSpacing="5px" SortingImageSpacing="5px">
                    </Header>
                    <AlternatingRow Enabled="True">
                    </AlternatingRow>
                    <LoadingPanel ImageSpacing="10px">
                    </LoadingPanel>
                </Styles>
                <SettingsBehavior ConfirmDelete="True" ColumnResizeMode="NextColumn" />
                <SettingsText CommandClearFilter="Clear"  />
                <StylesEditors>
                    <ProgressBar Height="25px">
                    </ProgressBar>
                </StylesEditors>
                <SettingsCustomizationWindow Enabled="True" />
            </dx:ASPxGridView>
        </td>
     </tr>         
     <tr align="right">
        <td align="right" class="pageName" colspan="2" style="height: 30px; text-align: right; text-align:-moz-right;">
            <uc1:ExportButtons ID="ucExportButtons" runat="server" />
        </td>
     </tr>      
    </table>
    <asp:sqlDataSource ID="ContactDetailsDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="_Companies_GetContactDetails" SelectCommandType="StoredProcedure">
</asp:sqlDataSource>
<data:CompaniesDataSource ID="CompaniesDataSource1" runat="server" Sort="CompanyName ASC"></data:CompaniesDataSource>
