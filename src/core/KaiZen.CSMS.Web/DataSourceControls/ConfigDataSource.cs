﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.ConfigProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(ConfigDataSourceDesigner))]
	public class ConfigDataSource : ProviderDataSource<Config, ConfigKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ConfigDataSource class.
		/// </summary>
		public ConfigDataSource() : base(new ConfigService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the ConfigDataSourceView used by the ConfigDataSource.
		/// </summary>
		protected ConfigDataSourceView ConfigView
		{
			get { return ( View as ConfigDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the ConfigDataSource control invokes to retrieve data.
		/// </summary>
		public ConfigSelectMethod SelectMethod
		{
			get
			{
				ConfigSelectMethod selectMethod = ConfigSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (ConfigSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the ConfigDataSourceView class that is to be
		/// used by the ConfigDataSource.
		/// </summary>
		/// <returns>An instance of the ConfigDataSourceView class.</returns>
		protected override BaseDataSourceView<Config, ConfigKey> GetNewDataSourceView()
		{
			return new ConfigDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the ConfigDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class ConfigDataSourceView : ProviderDataSourceView<Config, ConfigKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ConfigDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the ConfigDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public ConfigDataSourceView(ConfigDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal ConfigDataSource ConfigOwner
		{
			get { return Owner as ConfigDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal ConfigSelectMethod SelectMethod
		{
			get { return ConfigOwner.SelectMethod; }
			set { ConfigOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal ConfigService ConfigProvider
		{
			get { return Provider as ConfigService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<Config> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<Config> results = null;
			Config item;
			count = 0;
			
			System.Int32 _id;
			System.String _key;

			switch ( SelectMethod )
			{
				case ConfigSelectMethod.Get:
					ConfigKey entityKey  = new ConfigKey();
					entityKey.Load(values);
					item = ConfigProvider.Get(entityKey);
					results = new TList<Config>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case ConfigSelectMethod.GetAll:
                    results = ConfigProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case ConfigSelectMethod.GetPaged:
					results = ConfigProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case ConfigSelectMethod.Find:
					if ( FilterParameters != null )
						results = ConfigProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = ConfigProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case ConfigSelectMethod.GetById:
					_id = ( values["Id"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["Id"], typeof(System.Int32)) : (int)0;
					item = ConfigProvider.GetById(_id);
					results = new TList<Config>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				case ConfigSelectMethod.GetByKey:
					_key = ( values["Key"] != null ) ? (System.String) EntityUtil.ChangeType(values["Key"], typeof(System.String)) : string.Empty;
					item = ConfigProvider.GetByKey(_key);
					results = new TList<Config>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// FK
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == ConfigSelectMethod.Get || SelectMethod == ConfigSelectMethod.GetById )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				Config entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					ConfigProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<Config> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			ConfigProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region ConfigDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the ConfigDataSource class.
	/// </summary>
	public class ConfigDataSourceDesigner : ProviderDataSourceDesigner<Config, ConfigKey>
	{
		/// <summary>
		/// Initializes a new instance of the ConfigDataSourceDesigner class.
		/// </summary>
		public ConfigDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public ConfigSelectMethod SelectMethod
		{
			get { return ((ConfigDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new ConfigDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region ConfigDataSourceActionList

	/// <summary>
	/// Supports the ConfigDataSourceDesigner class.
	/// </summary>
	internal class ConfigDataSourceActionList : DesignerActionList
	{
		private ConfigDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the ConfigDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public ConfigDataSourceActionList(ConfigDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public ConfigSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion ConfigDataSourceActionList
	
	#endregion ConfigDataSourceDesigner
	
	#region ConfigSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the ConfigDataSource.SelectMethod property.
	/// </summary>
	public enum ConfigSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetById method.
		/// </summary>
		GetById,
		/// <summary>
		/// Represents the GetByKey method.
		/// </summary>
		GetByKey
	}
	
	#endregion ConfigSelectMethod

	#region ConfigFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Config"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ConfigFilter : SqlFilter<ConfigColumn>
	{
	}
	
	#endregion ConfigFilter

	#region ConfigExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Config"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ConfigExpressionBuilder : SqlExpressionBuilder<ConfigColumn>
	{
	}
	
	#endregion ConfigExpressionBuilder	

	#region ConfigProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;ConfigChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="Config"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ConfigProperty : ChildEntityProperty<ConfigChildEntityTypes>
	{
	}
	
	#endregion ConfigProperty
}

