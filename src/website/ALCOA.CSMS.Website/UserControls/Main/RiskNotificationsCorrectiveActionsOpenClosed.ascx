﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RiskNotificationsCorrectiveActionsOpenClosed.ascx.cs" Inherits="ALCOA.CSMS.Website.UserControls.Main.RiskNotificationsCorrectiveActionsOpenClosed" %>
<%@ Register Assembly="System.Web.Extensions" Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Src="../Other/ExportButtonsPivot.ascx" TagName="ExportButtonsPivot"
    TagPrefix="uc3" %>
<%@ Register Assembly="DevExpress.Web.ASPxPivotGrid.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPivotGrid.Export" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.XtraCharts.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.XtraCharts" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.XtraCharts.v14.1.Web, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.XtraCharts.Web" TagPrefix="dxchartsui" %>
<%@ Register Assembly="DevExpress.Web.ASPxPivotGrid.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPivotGrid" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dxtc" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dxw" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallbackPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dxpc" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Src="~/UserControls/Main/WorkLevelling/Workload_Sqq_Procurement.ascx"
    TagName="Workload_Sqq_Procurement" TagPrefix="uc2" %>
<%@ Register Src="~/UserControls/Main/WorkLevelling/Workload_Sqq_HsAssessor.ascx"
    TagName="Workload_Sqq_HsAssessor" TagPrefix="uc1" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Src="../Other/ExportButtons.ascx" TagName="ExportButtons" TagPrefix="uc1" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1" namespace="DevExpress.Web.ASPxTabControl" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1" namespace="DevExpress.Web.ASPxClasses" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1" namespace="DevExpress.Web.ASPxCallbackPanel" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1" namespace="DevExpress.Web.ASPxPanel" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.ASPxPivotGrid.v14.1" namespace="DevExpress.Web.ASPxPivotGrid" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.ASPxPivotGrid.v14.1" namespace="DevExpress.Web.ASPxPivotGrid.Export" tagprefix="dx" %>
<%@ Register assembly="DevExpress.XtraCharts.v14.1.Web" namespace="DevExpress.XtraCharts.Web" tagprefix="dxchartsui" %>
<%@ Register assembly="DevExpress.XtraCharts.v14.1" namespace="DevExpress.XtraCharts" tagprefix="cc2" %>
<%@ Register assembly="DevExpress.Web.v14.1" namespace="DevExpress.Web.ASPxPopupControl" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>


<script type="text/javascript">
    var expanded1 = false;   

    function ExpandOrCollapse1() {
        if (expanded1 == true) {
            gridRNC.CollapseAll();
            expanded1 = false;
        }
        else {
            gridRNC.ExpandAll();
            expanded1 = true;
        }
    }
    </script>

<table border="0" cellpadding="2" cellspacing="0" width="100%">
        <tr>
        <td class="pageName" colspan="3" style="height: 16px; width: 1170px;">
        <span class="bodycopy"><span class="title">Contractor Management Team Shop Front Tools</span><br />
                <span class="date">Risk Notifications & Corrective Actions Open-Closed</span><br />
                <img src="images/grfc_dottedline.gif" width="24" height="1" /><br />
            </span>
            </td>
            
    </tr>
    <tr>
        <td>
            <div style="float:left; padding:5px 15px 0px 0px"><b>Select</b></div>
            <div style="float:left; padding:5px 5px 0px 0px"><b>Month:</b></div>
            <div style="float:left; padding-top:3px">
            <dx:ASPxComboBox ID="cbMonth" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                        CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                                        Width="80px" ValueType="System.Int32">
                                                                        <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                                                                        </LoadingPanelImage>
                                                                        <ButtonStyle Width="13px">
                                                                        </ButtonStyle>                                                                        
                                                                    </dx:ASPxComboBox>
                                                                    </div>
        <div style="float:left; padding:5px 5px 0px 10px">
            <b>Year:</b></div><div style="float:left; padding-top:3px"><dx:ASPxComboBox ID="cbYear1" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                        CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                                        Width="80px" ValueType="System.Int32">
                                                                        <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                                                                        </LoadingPanelImage>
                                                                        <ButtonStyle Width="13px">
                                                                        </ButtonStyle>
                                                                        <ValidationSettings CausesValidation="True" Display="Dynamic" ErrorDisplayMode="ImageWithTooltip"
                                                                            ErrorText="Year Must Be Selected" ValidationGroup="Filter1">
                                                                        </ValidationSettings>
                                                                        </dx:ASPxComboBox></div>
       
            <div style="float:left; padding-left:10px"><dx:ASPxButton ID="btnFilter" OnClick="btnFilter_Click" runat="server" Text="Filter" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                ValidationGroup="Filter1">
            </dx:ASPxButton></div>
            <div style="float:left; padding:5px 5px 0px 5px"><a href="javascript:ExpandOrCollapse1();">Expand/Collapse All Rows</a></div>
        </td>
       
    </tr>    
    <tr>
       
            <td style="padding-top:10px; width:1170px;">
              <dx:ASPxGridView ID="gridRNC" DataSourceID="SqlDSRNCorrectiveActions"
               runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                CssPostfix="Office2003Blue" AutoGenerateColumns="False" Width="900px" ClientInstanceName="gridRNC">
                                                        <Columns>
                                                            <dx:GridViewDataComboBoxColumn Caption="Region" FieldName="RegionId" VisibleIndex="0"
                                                                FixedStyle="Left" GroupIndex="0">
                                                                <PropertiesComboBox DataSourceID="sqldaRegionsDS" TextField="RegionName" ValueField="RegionId"
                                                                    ValueType="System.Int32">
                                                                </PropertiesComboBox>
                                                            </dx:GridViewDataComboBoxColumn>
                                                            <dx:GridViewDataComboBoxColumn Caption="Site" FieldName="SiteId" VisibleIndex="1"
                                                                FixedStyle="Left" GroupIndex="1">
                                                                <PropertiesComboBox DataSourceID="sqldaSitesDS" TextField="SiteName" ValueField="SiteId"
                                                                    ValueType="System.Int32">
                                                                </PropertiesComboBox>
                                                            </dx:GridViewDataComboBoxColumn>
                                                            <dx:GridViewDataComboBoxColumn Caption="Metric" FieldName="Count_Type"
                                                                VisibleIndex="2" FixedStyle="Left" Width="200px">
                                                                <PropertiesComboBox DataSourceID="SqlDataCountType" TextField="Count_Type"
                                                                    ValueField="Count_Type" ValueType="System.String">
                                                                </PropertiesComboBox>
                                                            </dx:GridViewDataComboBoxColumn>
                                                            
                                                            <dx:GridViewBandColumn Caption="Week" VisibleIndex="3">
                                                                <Columns>
                                                                    <dx:GridViewDataTextColumn Caption="Total" FieldName="Total" ShowInCustomizationForm="True"
                                                                        VisibleIndex="0" Width="55px">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn Caption="01-Jan ." FieldName="Week1" ShowInCustomizationForm="True"
                                                                        VisibleIndex="1" Width="45px">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn Caption="08-Jan ." FieldName="Week2" ShowInCustomizationForm="True"
                                                                        VisibleIndex="2" Width="45px">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week3" VisibleIndex="3" Width="45px" ShowInCustomizationForm="True"
                                                                     Caption="15-Jan .">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week4" VisibleIndex="4" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="22-Jan .">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week5" VisibleIndex="5" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="29-Jan .">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week6" VisibleIndex="6" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="05-Feb .">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week7" VisibleIndex="7" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="12-Feb .">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week8" VisibleIndex="8" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="19-Feb .">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week9" VisibleIndex="9" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="26-Feb .">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week10" VisibleIndex="10" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="04-Mar .">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week11" VisibleIndex="11" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="11-Mar .">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week12" VisibleIndex="12" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="18-Mar .">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week13" VisibleIndex="13" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="25-Mar .">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week14" VisibleIndex="14" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="01-Apr .">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week15" VisibleIndex="15" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="08-Apr .">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week16" VisibleIndex="16" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="15-Apr .">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week17" VisibleIndex="17" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="22-Apr .">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week18" VisibleIndex="18" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="29-Apr .">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week19" VisibleIndex="19" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="06-May .">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week20" VisibleIndex="20" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="13-May .">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week21" VisibleIndex="21" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="20-May .">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week22" VisibleIndex="22" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="27-May .">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week23" VisibleIndex="23" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="03-Jun .">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week24" VisibleIndex="24" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="10-Jun .">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week25" VisibleIndex="25" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="17-Jun .">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week26" VisibleIndex="26" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="24-Jun .">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week27" VisibleIndex="27" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="01-Jul .">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week28" VisibleIndex="28" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="08-Jul .">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week29" VisibleIndex="29" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="15-Jul .">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week30" VisibleIndex="30" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="22-Jul .">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week31" VisibleIndex="31" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="29-Jul .">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week32" VisibleIndex="32" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="05-Aug .">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week33" VisibleIndex="33" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="12-Aug .">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week34" VisibleIndex="34" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="19-Aug .">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week35" VisibleIndex="35" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="26-Aug .">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week36" VisibleIndex="36" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="02-Sep .">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week37" VisibleIndex="37" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="09-Sep .">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week38" VisibleIndex="38" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="16-Sep .">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week39" VisibleIndex="39" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="23-Sep .">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week40" VisibleIndex="40" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="30-Sep .">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week41" VisibleIndex="41" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="07-Oct .">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week42" VisibleIndex="42" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="14-Oct .">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week43" VisibleIndex="43" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="21-Oct .">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week44" VisibleIndex="44" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="28-Oct .">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week45" VisibleIndex="45" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="04-Nov .">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week46" VisibleIndex="46" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="11-Nov .">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week47" VisibleIndex="47" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="18-Nov .">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week48" VisibleIndex="48" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="25-Nov .">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week49" VisibleIndex="49" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="02-Dec .">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week50" VisibleIndex="50" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="09-Dec .">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week51" VisibleIndex="51" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="16-Dec .">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week52" VisibleIndex="52" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="23-Dec .">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                </Columns>
                                                            </dx:GridViewBandColumn>
                                                        </Columns>
                                                        <TotalSummary>
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Total" ShowInColumn="Total" SummaryType="Sum"
                                                                ShowInGroupFooterColumn="01" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week1" ShowInColumn="01-Jan ." SummaryType="Sum"
                                                                ShowInGroupFooterColumn="02" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week2" ShowInColumn="08-Jan ." SummaryType="Sum"
                                                                ShowInGroupFooterColumn="03" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week3" ShowInColumn="15-Jan ." SummaryType="Sum"
                                                                ShowInGroupFooterColumn="04" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week4" ShowInColumn="22-Jan ." SummaryType="Sum"
                                                                ShowInGroupFooterColumn="05" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week5" ShowInColumn="29-Jan ." SummaryType="Sum"
                                                                ShowInGroupFooterColumn="06" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week6" ShowInColumn="05-Feb ." SummaryType="Sum"
                                                                ShowInGroupFooterColumn="07" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week7" ShowInColumn="12-Feb ." SummaryType="Sum"
                                                                ShowInGroupFooterColumn="08" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week8" ShowInColumn="19-Feb ." SummaryType="Sum"
                                                                ShowInGroupFooterColumn="09" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week9" ShowInColumn="26-Feb ." SummaryType="Sum"
                                                                ShowInGroupFooterColumn="10" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week10" ShowInColumn="04-Mar ." SummaryType="Sum"
                                                                ShowInGroupFooterColumn="11" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week11" ShowInColumn="11-Mar ." SummaryType="Sum"
                                                                ShowInGroupFooterColumn="12" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week12" ShowInColumn="18-Mar ." SummaryType="Sum"
                                                                ShowInGroupFooterColumn="13" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week13" ShowInColumn="25-Mar ." SummaryType="Sum"
                                                                ShowInGroupFooterColumn="14" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week14" ShowInColumn="01-Apr ." SummaryType="Sum"
                                                                ShowInGroupFooterColumn="15" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week15" ShowInColumn="08-Apr ." SummaryType="Sum"
                                                                ShowInGroupFooterColumn="16" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week16" ShowInColumn="15-Apr ." SummaryType="Sum"
                                                                ShowInGroupFooterColumn="17" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week17" ShowInColumn="22-Apr ." SummaryType="Sum"
                                                                ShowInGroupFooterColumn="18" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week18" ShowInColumn="29-Apr ." SummaryType="Sum"
                                                                ShowInGroupFooterColumn="19" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week19" ShowInColumn="06-May ." SummaryType="Sum"
                                                                ShowInGroupFooterColumn="20" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week20" ShowInColumn="13-May ." SummaryType="Sum"
                                                                ShowInGroupFooterColumn="21" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week21" ShowInColumn="20-May ." SummaryType="Sum"
                                                                ShowInGroupFooterColumn="22" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week22" ShowInColumn="27-May ." SummaryType="Sum"
                                                                ShowInGroupFooterColumn="23" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week23" ShowInColumn="03-Jun ." SummaryType="Sum"
                                                                ShowInGroupFooterColumn="24" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week24" ShowInColumn="10-Jun ." SummaryType="Sum"
                                                                ShowInGroupFooterColumn="25" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week25" ShowInColumn="17-Jun ." SummaryType="Sum"
                                                                ShowInGroupFooterColumn="26" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week26" ShowInColumn="24-Jun ." SummaryType="Sum"
                                                                ShowInGroupFooterColumn="27" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week27" ShowInColumn="01-Jul ." SummaryType="Sum"
                                                                ShowInGroupFooterColumn="28" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week28" ShowInColumn="08-Jul ." SummaryType="Sum"
                                                                ShowInGroupFooterColumn="29" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week29" ShowInColumn="15-Jul ." SummaryType="Sum"
                                                                ShowInGroupFooterColumn="30" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week30" ShowInColumn="22-Jul ." SummaryType="Sum"
                                                                ShowInGroupFooterColumn="31" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week31" ShowInColumn="29-Jul ." SummaryType="Sum"
                                                                ShowInGroupFooterColumn="32" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week32" ShowInColumn="05-Aug ." SummaryType="Sum"
                                                                ShowInGroupFooterColumn="33" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week33" ShowInColumn="12-Aug ." SummaryType="Sum"
                                                                ShowInGroupFooterColumn="34" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week34" ShowInColumn="19-Aug ." SummaryType="Sum"
                                                                ShowInGroupFooterColumn="35" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week35" ShowInColumn="26-Aug ." SummaryType="Sum"
                                                                ShowInGroupFooterColumn="36" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week36" ShowInColumn="02-Sep ." SummaryType="Sum"
                                                                ShowInGroupFooterColumn="37" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week37" ShowInColumn="09-Sep ." SummaryType="Sum"
                                                                ShowInGroupFooterColumn="38" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week38" ShowInColumn="16-Sep ." SummaryType="Sum"
                                                                ShowInGroupFooterColumn="39" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week39" ShowInColumn="23-Sep ." SummaryType="Sum"
                                                                ShowInGroupFooterColumn="40" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week40" ShowInColumn="30-Sep ." SummaryType="Sum"
                                                                ShowInGroupFooterColumn="41" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week41" ShowInColumn="07-Oct ." SummaryType="Sum"
                                                                ShowInGroupFooterColumn="42" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week42" ShowInColumn="14-Oct ." SummaryType="Sum"
                                                                ShowInGroupFooterColumn="43" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week43" ShowInColumn="21-Oct ." SummaryType="Sum"
                                                                ShowInGroupFooterColumn="44" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week44" ShowInColumn="28-Oct ." SummaryType="Sum"
                                                                ShowInGroupFooterColumn="45" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week45" ShowInColumn="04-Nov ." SummaryType="Sum"
                                                                ShowInGroupFooterColumn="46" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week46" ShowInColumn="11-Nov ." SummaryType="Sum"
                                                                ShowInGroupFooterColumn="47" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week47" ShowInColumn="18-Nov ." SummaryType="Sum"
                                                                ShowInGroupFooterColumn="48" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week48" ShowInColumn="25-Nov ." SummaryType="Sum"
                                                                ShowInGroupFooterColumn="49" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week49" ShowInColumn="02-Dec ." SummaryType="Sum"
                                                                ShowInGroupFooterColumn="50" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week50" ShowInColumn="09-Dec ." SummaryType="Sum"
                                                                ShowInGroupFooterColumn="51" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week51" ShowInColumn="16-Dec ." SummaryType="Sum"
                                                                ShowInGroupFooterColumn="52" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week52" ShowInColumn="23-Dec ." SummaryType="Sum"
                                                                ShowInGroupFooterColumn="53" />
                                                        </TotalSummary>
                                                        <GroupSummary>
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Total" SummaryType="Sum" ShowInGroupFooterColumn="Total" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week1" SummaryType="Sum" ShowInGroupFooterColumn="01-Jan ." />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week2" SummaryType="Sum" ShowInGroupFooterColumn="08-Jan ." />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week3" SummaryType="Sum" ShowInGroupFooterColumn="15-Jan ." />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week4" SummaryType="Sum" ShowInGroupFooterColumn="22-Jan ." />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week5" SummaryType="Sum" ShowInGroupFooterColumn="29-Jan ." />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week6" SummaryType="Sum" ShowInGroupFooterColumn="05-Feb ." />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week7" SummaryType="Sum" ShowInGroupFooterColumn="12-Feb ." />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week8" SummaryType="Sum" ShowInGroupFooterColumn="19-Feb ." />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week9" SummaryType="Sum" ShowInGroupFooterColumn="26-Feb ." />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week10" SummaryType="Sum" ShowInGroupFooterColumn="04-Mar ." />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week11" SummaryType="Sum" ShowInGroupFooterColumn="11-Mar ." />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week12" SummaryType="Sum" ShowInGroupFooterColumn="18-Mar ." />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week13" SummaryType="Sum" ShowInGroupFooterColumn="25-Mar ." />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week14" SummaryType="Sum" ShowInGroupFooterColumn="01-Apr ." />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week15" SummaryType="Sum" ShowInGroupFooterColumn="08-Apr ." />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week16" SummaryType="Sum" ShowInGroupFooterColumn="15-Apr ." />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week17" SummaryType="Sum" ShowInGroupFooterColumn="22-Apr ." />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week18" SummaryType="Sum" ShowInGroupFooterColumn="29-Apr ." />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week19" SummaryType="Sum" ShowInGroupFooterColumn="06-May ." />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week20" SummaryType="Sum" ShowInGroupFooterColumn="13-May ." />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week21" SummaryType="Sum" ShowInGroupFooterColumn="20-May ." />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week22" SummaryType="Sum" ShowInGroupFooterColumn="27-May ." />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week23" SummaryType="Sum" ShowInGroupFooterColumn="03-Jun ." />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week24" SummaryType="Sum" ShowInGroupFooterColumn="10-Jun ." />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week25" SummaryType="Sum" ShowInGroupFooterColumn="17-Jun ." />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week26" SummaryType="Sum" ShowInGroupFooterColumn="24-Jun ." />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week27" SummaryType="Sum" ShowInGroupFooterColumn="01-Jul ." />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week28" SummaryType="Sum" ShowInGroupFooterColumn="08-Jul ." />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week29" SummaryType="Sum" ShowInGroupFooterColumn="15-Jul ." />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week30" SummaryType="Sum" ShowInGroupFooterColumn="22-Jul ." />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week31" SummaryType="Sum" ShowInGroupFooterColumn="29-Jul ." />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week32" SummaryType="Sum" ShowInGroupFooterColumn="05-Aug ." />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week33" SummaryType="Sum" ShowInGroupFooterColumn="12-Aug ." />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week34" SummaryType="Sum" ShowInGroupFooterColumn="19-Aug ." />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week35" SummaryType="Sum" ShowInGroupFooterColumn="26-Aug ." />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week36" SummaryType="Sum" ShowInGroupFooterColumn="02-Sep ." />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week37" SummaryType="Sum" ShowInGroupFooterColumn="09-Sep ." />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week38" SummaryType="Sum" ShowInGroupFooterColumn="16-Sep ." />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week39" SummaryType="Sum" ShowInGroupFooterColumn="23-Sep ." />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week40" SummaryType="Sum" ShowInGroupFooterColumn="30-Sep ." />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week41" SummaryType="Sum" ShowInGroupFooterColumn="07-Oct ." />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week42" SummaryType="Sum" ShowInGroupFooterColumn="14-Oct ." />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week43" SummaryType="Sum" ShowInGroupFooterColumn="21-Oct ." />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week44" SummaryType="Sum" ShowInGroupFooterColumn="28-Oct ." />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week45" SummaryType="Sum" ShowInGroupFooterColumn="04-Nov ." />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week46" SummaryType="Sum" ShowInGroupFooterColumn="11-Nov ." />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week47" SummaryType="Sum" ShowInGroupFooterColumn="18-Nov ." />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week48" SummaryType="Sum" ShowInGroupFooterColumn="25-Nov ." />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week49" SummaryType="Sum" ShowInGroupFooterColumn="02-Dec ." />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week50" SummaryType="Sum" ShowInGroupFooterColumn="09-Dec ." />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week51" SummaryType="Sum" ShowInGroupFooterColumn="16-Dec ." />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week52" SummaryType="Sum" ShowInGroupFooterColumn="23-Dec ." />
                                                        </GroupSummary>
                                                        <SettingsPager Mode="ShowAllRecords" PageSize="200">
                                                        </SettingsPager>
                                                        <Settings ShowFilterRow="True" ShowGroupPanel="True" ShowHorizontalScrollBar="True"
                                                            ShowVerticalScrollBar="True" ShowHeaderFilterButton="True" ShowGroupFooter="VisibleAlways"
                                                            ShowFooter="True" ShowFilterBar="Visible" />
                                                        <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                                            <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
                                                            </LoadingPanelOnStatusBar>
                                                            <LoadingPanel Url="~/App_Themes/Office2003BlueOld/GridView/Loading.gif"><%--Enhancement_023:change by debashis for testing platform upgradation--%>
                                                            </LoadingPanel>
                                                        </Images>
                                                        <ImagesFilterControl>
                                                            <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
                                                            </LoadingPanel>
                                                        </ImagesFilterControl>
                                                        <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue">
                                                            <Header ImageSpacing="5px" SortingImageSpacing="5px">
                                                            </Header>
                                                            <LoadingPanel ImageSpacing="10px">
                                                            </LoadingPanel>
                                                        </Styles>
                                                        <StylesEditors>
                                                            <ProgressBar Height="25px">
                                                            </ProgressBar>
                                                        </StylesEditors>
                                                    </dx:ASPxGridView>
            <table width="900px">
                <tr>
                <td width="50%" align="left">
                   <b>Last Updated At: </b><asp:Label ID="lblLastMod" runat="server"></asp:Label>
                </td>

                    <td style="padding-top: 6px; text-align: right; text-align: -moz-right;
                        width: 50%" align="right">                        
                            <div width="100%" align="right"><uc1:ExportButtons ID="ucExportButtons2" runat="server" /></div>                      
                    </td>
                </tr>
            </table>
            
            </td>
    </tr>
    </table>

    <br />
   
          
    <asp:SqlDataSource ID="SqlDSRNCorrectiveActions" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
   SelectCommand="_RNCorrectiveActions_GetYear2" SelectCommandType="StoredProcedure">
   <SelectParameters>
        <asp:Parameter Name="Year" DbType="Int32" />
        <asp:Parameter Name="Month" DbType="Int32" />
   </SelectParameters>
   </asp:SqlDataSource>

    <asp:SqlDataSource ID="SqlDataCountType" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
   SelectCommand="Select distinct Count_Type from AU_CSMS_CA_AND_RN_COUNTS_TEMPTABLE order by Count_Type">  
   </asp:SqlDataSource>

   <asp:SqlDataSource ID="sqldaModifiedDate" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
   SelectCommand="select top 10 MODIFIED_DATE from dbo.AU_CSMS_CA_AND_RN_COUNTS_TEMPTABLE">  
   </asp:SqlDataSource>

   <data:RegionsDataSource ID="RegionsDataSource1" runat="server">
                                </data:RegionsDataSource>
   <asp:SqlDataSource ID="sqldaSitesDS" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
   SelectCommand="Select SiteId,SiteName from Sites where Ordinal>0 Order By SiteName">  
   </asp:SqlDataSource>
    <asp:SqlDataSource ID="sqldaRegionsDS" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
   SelectCommand="Select RegionId,RegionName from Regions where RegionId Not in (2,3) order by RegionName">  
   </asp:SqlDataSource>


    <data:SitesDataSource ID="SitesDataSource1" runat="server">
</data:SitesDataSource>