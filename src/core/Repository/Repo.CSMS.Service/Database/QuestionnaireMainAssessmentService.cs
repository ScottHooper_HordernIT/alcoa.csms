﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface IQuestionnaireMainAssessmentService
    {
        QuestionnaireMainAssessment Get(Expression<Func<QuestionnaireMainAssessment, bool>> where, List<Expression<Func<QuestionnaireMainAssessment, object>>> includes);
        List<QuestionnaireMainAssessment> GetMany(int? top, Expression<Func<QuestionnaireMainAssessment, bool>> where, Func<IQueryable<QuestionnaireMainAssessment>, IOrderedQueryable<QuestionnaireMainAssessment>> orderBy, List<Expression<Func<QuestionnaireMainAssessment, object>>> includes);
        List<QuestionnaireMainAssessment> GetManyPaged(int pageNumber, int pageSize, Expression<Func<QuestionnaireMainAssessment, bool>> where, Func<IQueryable<QuestionnaireMainAssessment>, IOrderedQueryable<QuestionnaireMainAssessment>> orderBy, List<Expression<Func<QuestionnaireMainAssessment, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<QuestionnaireMainAssessment, bool>> where, List<Expression<Func<QuestionnaireMainAssessment, object>>> includes);
        void Insert(QuestionnaireMainAssessment item, bool saveChanges = true);
        void Update(QuestionnaireMainAssessment item, bool saveChanges = true);
        void Delete(QuestionnaireMainAssessment item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class QuestionnaireMainAssessmentService : IQuestionnaireMainAssessmentService
    {
        private readonly IRepository<QuestionnaireMainAssessment> _repository;

        public QuestionnaireMainAssessmentService(IRepository<QuestionnaireMainAssessment> repository)
        {
            this._repository = repository;
        }

        public QuestionnaireMainAssessment Get(Expression<Func<QuestionnaireMainAssessment, bool>> where, List<Expression<Func<QuestionnaireMainAssessment, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<QuestionnaireMainAssessment> GetMany(int? top, Expression<Func<QuestionnaireMainAssessment, bool>> where, Func<IQueryable<QuestionnaireMainAssessment>, IOrderedQueryable<QuestionnaireMainAssessment>> orderBy, List<Expression<Func<QuestionnaireMainAssessment, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<QuestionnaireMainAssessment> GetManyPaged(int pageNumber, int pageSize, Expression<Func<QuestionnaireMainAssessment, bool>> where, Func<IQueryable<QuestionnaireMainAssessment>, IOrderedQueryable<QuestionnaireMainAssessment>> orderBy, List<Expression<Func<QuestionnaireMainAssessment, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<QuestionnaireMainAssessment, bool>> where, List<Expression<Func<QuestionnaireMainAssessment, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(QuestionnaireMainAssessment item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("QuestionnaireMainAssessment");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(QuestionnaireMainAssessment item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("QuestionnaireMainAssessment");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(QuestionnaireMainAssessment item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("QuestionnaireMainAssessment");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
