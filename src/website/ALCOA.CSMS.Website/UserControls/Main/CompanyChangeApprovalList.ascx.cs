﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using KaiZen.CSMS.Web;
using KaiZen.CSMS.Services;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using model = Repo.CSMS.DAL.EntityModels;
using repo = Repo.CSMS.Service.Database;
namespace ALCOA.CSMS.Website.UserControls.Main
{
    public partial class CompanyChangeApprovalList : System.Web.UI.UserControl
    {
        Auth auth = new Auth();
        repo.IEmailLogService emailLogService = ALCOA.CSMS.Website.Global.GetInstance<repo.IEmailLogService>();
        repo.IEmailTemplateService emailTemplateService = ALCOA.CSMS.Website.Global.GetInstance<repo.IEmailTemplateService>();
        protected void Page_Load(object sender, EventArgs e) {  Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); }

        private void moduleInit(bool postBack)
        {
            if (!postBack)
            {
                if (auth.RoleId == (int)RoleList.Administrator || Helper.General.isRegionalManager(auth.UserId))
                    gridCcsrAwaitingApproval.Columns[0].Visible = true;
                else
                    gridCcsrAwaitingApproval.Columns[0].Visible = false;

                Helper.ExportGrid.Settings(ExportButtons1, "Company Status Change Requests - Awaiting Approval", "gridCcsrAwaitingApproval");
                Helper.ExportGrid.Settings(ExportButtons2, "Company Status Change Requests - History", "gridCcsrHistory");
            }
        }

        protected void ASPxGridView1_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
        {
            TransactionManager transactionManager = null;
            try
            {
                transactionManager = ConnectionScope.ValidateOrCreateTransaction(true);

                int CompanyStatusChangeApprovalId = (int)e.Keys["CompanyStatusChangeApprovalId"];//DT300 - (int)e.OldValues["CompanyStatusChangeApprovalId"]; //DT300 - removed this field from the edit form
                //Changed for Item #11
                string Approved_text=(string)e.NewValues["dlApproved"];
                bool Approved = false;
                bool Processed = false;
                if (Approved_text == "1")
                {
                    Approved = true;
                    Processed = true;
                }
                else if (Approved_text == "2")
                {
                    Approved = false;
                    Processed = true;
                }
                else if (Approved_text == "0")
                {
                   //do nothing
                    Processed = false;
                }
                string AssessorComments = (string)e.NewValues["AssessorComments"];

                CompanyStatusChangeApprovalService cscaService = new CompanyStatusChangeApprovalService();
                CompanyStatusChangeApproval csca = cscaService.GetByCompanyStatusChangeApprovalId(CompanyStatusChangeApprovalId);
                //
                if (Processed == true)
                {
                    csca.Approved = Approved;
                }
                else
                {
                    csca.Approved = null;
                }
                csca.AssessedByUserId = auth.UserId;
                csca.AssessedDate = DateTime.Now;
                csca.AssessorComments = AssessorComments;

                DataRepository.CompanyStatusChangeApprovalProvider.Update(transactionManager, csca);
                transactionManager.Commit();

                if (Processed)
                {
                    CompanyStatusService csService = new CompanyStatusService();
                    CompaniesService cService = new CompaniesService();
                    Companies companiesEntity = cService.GetByCompanyId(csca.CompanyId);
                    CompanyStatus csCurrent = csService.GetByCompanyStatusId((int)companiesEntity.CompanyStatusId);
                    CompanyStatus csRequested = csService.GetByCompanyStatusId(csca.RequestedCompanyStatusId);
                    int action = (int)QuestionnaireActionList.CompanyStatusChangeNotApproved;

                    //Modified by Ashley Goldstraw to use Email Templates.  14/9/2015
                    model.EmailTemplate eMailTemplate = new model.EmailTemplate(); ;
                    if (Approved)
                    {
                        eMailTemplate = emailTemplateService.Get(tp => tp.EmailTemplateName == "Company Status Change Request - Approved", null);
                    }
                    else
                    {
                        eMailTemplate = emailTemplateService.Get(tp => tp.EmailTemplateName == "Company Status Change Request - Not Approved", null);
                    }

                    byte[] bodyByte = eMailTemplate.EmailBody;
                    string mailbody = System.Text.Encoding.ASCII.GetString(bodyByte);
                    string subject = eMailTemplate.EmailTemplateSubject;
                    subject = subject.Replace("{CompanyName}", companiesEntity.CompanyName);
                    subject = subject.Replace("{OriginalCompanyStatus}", csCurrent.CompanyStatusDesc);
                    subject = subject.Replace("{RequestedCompanyStatus}", csRequested.CompanyStatusDesc);
                    subject = subject.Replace("{Comments}", csca.Comments);
                    subject = subject.Replace("{AssessorComments}", csca.AssessorComments);
                    subject = subject.Replace("{AssessedByLastName}", auth.LastName);
                    subject = subject.Replace("{AssessedByFirstName}", auth.FirstName);
                    subject = subject.Replace("{AssessedByCompanyName}", auth.CompanyName);
                    subject = subject.Replace("{CurrentDateTime}", DateTime.Now.ToString());

                    mailbody = mailbody.Replace("{CompanyName}", companiesEntity.CompanyName);
                    mailbody = mailbody.Replace("{OriginalCompanyStatus}", csCurrent.CompanyStatusDesc);
                    mailbody = mailbody.Replace("{RequestedCompanyStatus}", csRequested.CompanyStatusDesc);
                    mailbody = mailbody.Replace("{Comments}", csca.Comments);
                    mailbody = mailbody.Replace("{AssessorComments}", csca.AssessorComments);
                    mailbody = mailbody.Replace("{AssessedByLastName}", auth.LastName);
                    mailbody = mailbody.Replace("{AssessedByFirstName}", auth.FirstName);
                    mailbody = mailbody.Replace("{AssessedByCompanyName}", auth.CompanyName);
                    mailbody = mailbody.Replace("{CurrentDateTime}", DateTime.Now.ToString());
                    string qUrl = String.Format("<a href='http://csm.aua.alcoa.com/SafetyPQ_QuestionnaireModify.aspx{0}'>{1} - Safety Qualification Questionnaire</a>", QueryStringModule.Encrypt("q=" + csca.QuestionnaireId.ToString()), companiesEntity.CompanyName);
                    mailbody = mailbody.Replace("{QuestionnaireLink}", qUrl);



                    //string qUrl = "http://csm.aua.alcoa.com/SafetyPQ_QuestionnaireModify.aspx" + QueryStringModule.Encrypt("q=" + csca.QuestionnaireId.ToString());
                    //string subject = String.Format("Company Status Change Request - {0} - Not Approved", companiesEntity.CompanyName);
                    /*string body = "The below Company Status Change Request has been [<strong>NOT APPROVED</strong>] and as such this company needs to be re-qualified.<br /><br />" +
                                    "<strong>Company Name</strong>: " + companiesEntity.CompanyName + "<br />" +
                                    "<strong>Original Company Status</strong>: " + csCurrent.CompanyStatusDesc + "<br />" +
                                    "<strong>Requested Company Status</strong>: " + csRequested.CompanyStatusDesc + "<br />" +
                                    "<strong>Requester Comments</strong>: " + csca.Comments + "<br />" +
                                    "<strong>Assessor Comments</strong>: " + csca.AssessorComments + "<br />" +
                                    "<br /><br />" +
                                    "<strong>Assessed by</strong>: " + auth.LastName + ", " + auth.FirstName + " (" + auth.CompanyName + ") at " + DateTime.Now.ToString() + "<br /><br />" +
                                    "<strong>Quick Links</strong>: <br />1) <a href='" + qUrl + "'>" + companiesEntity.CompanyName + " - Safety Qualification Questionnaire</a><br />";
                    */
                    if (Approved)
                    {
                        companiesEntity.CompanyStatusId = csca.RequestedCompanyStatusId;
                        cService.Save(companiesEntity);
                        action = (int)QuestionnaireActionList.CompanyStatusChangeApproved;
                        //commented out by AG as email is now got from database EmailTemplate
                       // subject = subject.Replace("Not Approved", "Approved");
                       // body = body.Replace("The below Company Status Change Request has been [<strong>NOT APPROVED</strong>] and as such this company needs to be re-qualified.",
                       //                     "The below Company Status Change Request has been [<strong>APPROVED</strong>] as follows:");
                    }

                    //email
                    UsersService uService = new UsersService();
                    Users uRequested = uService.GetByUserId(csca.RequestedByUserId);
                    string[] to = { uRequested.Email };
                    string[] cc = { auth.Email };
                    //Helper.Email.sendEmail(to, cc, null, subject, body, null, true);
                    emailLogService.Insert(to,cc,null,subject,mailbody,"General",true,null,null);

                    if (Helper.Questionnaire.ActionLog.Add(action,
                                                                    csca.QuestionnaireId,                       //QuestionnaireId
                                                                    auth.UserId,                                //UserId
                                                                    auth.FirstName,                             //FirstName
                                                                    auth.LastName,                              //LastName
                                                                    auth.RoleId,                                //RoleId
                                                                    auth.CompanyId,                             //CompanyId
                                                                    auth.CompanyName,                           //CompanyName
                                                                    AssessorComments,                                   //Comments
                                                                    csCurrent.CompanyStatusDesc,                //OldAssigned
                                                                    csRequested.CompanyStatusDesc,              //NewAssigned
                                                                    null,                                       //NewAssigned2
                                                                    null,                                       //NewAssigned3
                                                                    null)                                       //NewAssigned4
                                                                    == false)
                    {
                        //for now we do nothing if action log fails.
                    }

                }
                e.Cancel = true;
                gridCcsrAwaitingApproval.CancelEdit();

                CompanyStatusChangeApprovalDataSource1.DataBind();
                CompanyStatusChangeApprovalDataSource2.DataBind();
                gridCcsrAwaitingApproval.DataBind();
                gridCcsrHistory.DataBind();
            }
            catch (Exception ex)
            {
                if ((transactionManager != null) && (transactionManager.IsOpen))
                    transactionManager.Rollback();
                throw ex;
            }
        }
    }
}