﻿1.1 Nuget Package Restore
----------------------
> Package Manager Console > Update-Package -Reinstall

2.1 Get /awaproxy virtual directory to work in IIS Express
----------------------
> Open C:\Users\<usernmae>\Documents\IISExpress\config\applicationhost.config
> Alter the following section as follows:
            <site name="ALCOA.CSMS.Website" id="14">
                <application path="/" applicationPool="Clr4IntegratedAppPool">
                    <virtualDirectory path="/" physicalPath="C:\Projects\CSMS_Enhancements\src\website\ALCOA.CSMS.Website" />
                </application>
				<application path="/awaproxy" applicationPool="Clr4IntegratedAppPool">
                    <virtualDirectory path="/" physicalPath="C:\Projects\CSMS_Enhancements\src\website\ALCOA.CSMS.Website" />
                </application>				
                <bindings>
                    <binding protocol="http" bindingInformation="*:63041:localhost" />
                </bindings>
            </site>

2.2 Set anonymous access in web.config
----------------------
Add the following at the bottom above </configuration>

  <location path="awaproxy">
    <system.webServer>
      <security>
        <authentication>
          <anonymousAuthentication enabled="true" />
          <windowsAuthentication enabled="false" />
        </authentication>
      </security>
    </system.webServer>
  </location> 

2.3 Allow web.config to alter authentication mode http://digitaldrummerj.me/iis-express-windows-authentication/
-----------------------
Open applicationhost.config. This file is stored at C:\Users[your user name]\Documents\IISExpress\config\applicationhost.config

Look for the following lines
<section name="windowsAuthentication" overrideModeDefault="Deny" />
<section name="anonymousAuthentication" overrideModeDefault="Deny" />
<add name="WindowsAuthenticationModule" lockItem="true" />
<add name="AnonymousAuthenticationModule" lockItem="true" />

Change those lines to
<section name="windowsAuthentication" overrideModeDefault="Allow" />
<section name="anonymousAuthentication" overrideModeDefault="Allow" />
<add name="WindowsAuthenticationModule" lockItem="false" />
<add name="AnonymousAuthenticationModule" lockItem="false" />