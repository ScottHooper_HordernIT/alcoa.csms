using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Services;
using LumenWorks.Framework.IO.Csv;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Security.Principal;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


public partial class UserControls_EbiContractorsOnSite : System.Web.UI.UserControl
{
    static DataTable dt10 = new DataTable();
    Auth auth = new Auth();
    public static WindowsIdentity ident;

    private const string YES_SAFETY_QUAL_CURRENT = "1) Yes - Safety Qualification Current / Access to Site Granted";
    private const string YES_SAFETY_QUAL_CURRENT_EXPIRING = "2) Yes - Safety Qualification Current (expiring in 60 days) / Access to Site Granted";
    private const string YES_SAFETY_QUAL_CURRENT_REQUAL = "3) Yes - Safety Qualification Current (currently Re-Qualifying) / Access to Site Granted";
    private const string NO_CURRENT_SAFETY_QUAL_NOT_FOUND = "4) No - Current Safety Qualification not found / Access to Site Not Granted";
    private const string NO_SAFETY_QUAL_EXPIRED = "5) No - Safety Qualification Expired / Access to Site Not Granted";
    private const string NO_ACCESS_TO_SITE = "6) No - Access to Site Not Granted";
    private const string NO_SAFETY_QUAL_NOT_FOUND = "7) No - Safety Qualification not found";
    private const string NO_INFO_NOT_FOUND = "8) No - Company information could not be found (Check that Company exists in CSMS)";
    private const string EXEMPTION = "9) Safety Qualification Exemption Exists";

    protected void Page_Load(object sender, EventArgs e) { Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); }

    private void moduleInit(bool postBack)
    {
        SessionHandler.spVar_Questionnaire_FrontPageLink = "EbiContractorsOnSite";
        //first time load
        if (!postBack)
        {
            if (auth.RoleId == (int)RoleList.Administrator || Helper.General.isEbiUser(auth.UserId))
            {
                //all good.
                bool live = false;
                if (Request.QueryString["q"] != null)
                {
                    string q = Request.QueryString["q"].ToString();
                    if (q == "live") live = true;
                }

                if (live)
                {
                    lblLive.Text = String.Format("nb. Data Shows Contractors that have been On-Site Today (Since Midnight until Live: {0}).",
                                                  DateTime.Now.ToString());
                    hlLive.Text = "Click Here to show the Midnight to 9am Version";
                    hlLive.NavigateUrl = "~/NonComplianceEbiOnSite.aspx";
                }
                else
                {
                    lblLive.Text = "nb. Data Shows Contractors that have been On-Site Today (Since Midnight until 9am).";
                    hlLive.Text = "Click Here to show the Live Version";
                    hlLive.NavigateUrl = "~/NonComplianceEbiOnSite.aspx?q=live";
                }
                LoadData(live);
            }
            else
            {
                Response.Redirect("AccessDenied.aspx?error=" + Server.UrlEncode("Unrecognised User."));
            }

            // Export
            String exportFileName = @"ALCOA CSMS - " + String.Format("EBI - Contractors on Site today as of {0}-{1}-{2} 9am", DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year); //Chrome & IE is fine.
            String userAgent = Request.Headers.Get("User-Agent");
            if (
                (userAgent.Contains("Mozilla") && !userAgent.Contains("AppleWebKit") && !userAgent.Contains("MSIE")) || //Mozilla
                (userAgent.Contains("AppleWebKit") && !userAgent.Contains("Chrome"))      //Safari
                )
            {
                exportFileName = exportFileName.Replace(" ", "_");
            }
            Helper.ExportGrid.Settings(ucExportButtons, exportFileName, true, true);
        }
        else
        {
            grid.DataSource = (DataTable)Session["dtEbi"];
            grid.DataBind();
        }
    }

    private void LoadData(bool live)
    {
        WindowsImpersonationContext impContext = null;
        try { impContext = Helper.ImpersonateWAOCSMUser(); }
        catch (ApplicationException ex) { Console.Write(ex.Message); }

        try
        {
            if (null != impContext)
            {
                using (impContext)
                {
                    string myFile = "";
                    if (!live)
                    {
                        if (DateTime.Now.Hour >= 9)
                            LoadData3(live);
                    }
                    else
                    {
                        string ebiData = ReadEbiData();

                        string[] strmyFile = ebiData.Split('~');
                        string dsClocks = strmyFile[1];
                        dsClocks = dsClocks.Substring(0, dsClocks.Length - 1);
                        ConnectionStringSettings conString = ConfigurationManager.ConnectionStrings["ALCOA_WAO_CSMWP_devConnectionString"];
                        string strConnString = conString.ConnectionString;

                        //Cindi Thornton - changed HR.. to HR (now in the csms db and in the HR schema)
                        string strQuery = " select * from HR.XXHR_CWK_HISTORY_V a" +
                                            " left outer join CompanyVendor b On a.CWK_VENDOR_NO=b.Vendor_Number" +
                                            " where a.CWK_NBR in (" + dsClocks + ") order by a.CWK_NBR,a.EFFECTIVE_END_DATE";
                        DataTable dtAll = new DataTable();
                        using (SqlConnection cn = new SqlConnection(strConnString))
                        {
                            try
                            {
                                cn.Open();
                                SqlCommand cmd = new SqlCommand(strQuery, cn);
                                cmd.CommandTimeout = 60000;
                                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(cmd);
                                DataTable dt = new DataTable();
                                sqlDataAdapter.Fill(dtAll);
                            }
                            catch (Exception)
                            {
                            }
                            finally
                            {
                                cn.Close();
                            }
                        }
                        DataTable dtEbiServer = new DataTable();
                        if (!String.IsNullOrEmpty(ebiData))
                        {
                            myFile += strmyFile[0];
                            dtEbiServer = LoadData2(myFile, dtAll);
                        }

                        LoadData4(true, dtEbiServer);
                    }
                }
            }
        }
        catch (Exception ex)
        {
            Elmah.ErrorSignal.FromContext(Context).Raise(ex);
            Console.Write(ex.Message + "\nServer Configuration Account Error. Contact Administrator.");
        }
        finally
        {
        }
    }
    
    private DataTable LoadData2(string myFile, DataTable dtALL)
    {
        try
        {
            DataTable dtEbi = new DataTable();
            dtEbi.Columns.Add("CompanyName", typeof(string));
            dtEbi.Columns.Add("ClockId", typeof(string));
            dtEbi.Columns.Add("FullName", typeof(string));
            dtEbi.Columns.Add("AccessCardNo", typeof(string));
            dtEbi.Columns.Add("Site", typeof(string));
            dtEbi.Columns.Add("SwipeDateTime", typeof(string));
            dtEbi.Columns.Add("CompanyName-CSMS", typeof(string));

            CompaniesService cService = new CompaniesService();
            List<String> CompaniesSitesList = new List<String>();
            List<String> CompaniesSitesNullList = new List<String>();

            DataTable dtEbi2 = new DataTable();
            dtEbi2.Columns.Add("CompanyName", typeof(string));
            dtEbi2.Columns.Add("CompanyName-CSMS", typeof(string));
            dtEbi2.Columns.Add("Site", typeof(string));
            dtEbi2.Columns.Add("ApprovedOnSite", typeof(string));
            dtEbi2.Columns.Add("Valid", typeof(string));
            dtEbi2.Columns.Add("SqValidTill", typeof(string));
            dtEbi2.Columns.Add("CompanySQStatus", typeof(string));
            dtEbi2.Columns.Add("QuestionnaireId", typeof(string));
            dtEbi2.Columns.Add("Validity", typeof(string));
            dtEbi2.Columns.Add("Type", typeof(string));

            dtEbi2.Columns.Add("ProcurementContact", typeof(string));
            dtEbi2.Columns.Add("HSAssessor", typeof(string));

            if (!String.IsNullOrEmpty(myFile))
            {
                Console.WriteLine("CSV File Ok.");
                using (CsvReader csv = new CsvReader(new StringReader(myFile), true))
                {
                    string[] headers = csv.GetFieldHeaders();
                    Console.WriteLine("Found Headers: " + String.Join(",", headers));

                    SitesService sService = new SitesService();
                    TList<Sites> sList = sService.GetAll();

                    while (csv.ReadNextRecord())
                    {
                        string CompanyName = csv["CompanyName"].ToString();
                        string ClockId = csv["ClockId"].ToString();
                        string FullName = csv["FullName"].ToString();
                        string AccessCardNo = csv["AccessCardNo"].ToString();
                        string Site = csv["Site"].ToString();
                        DateTime SwipeDateTime = DateTime.Now;

                        string live = string.Empty;
                        if (Request.QueryString["q"] == "live")
                        {
                            live = "live";
                            SwipeDateTime = Convert.ToDateTime(csv["SwipeDateTime"]);
                        }
                        else
                        {
                            live = "nonlive";
                            string[] dateArr = csv["SwipeDateTime"].ToString().Split('/');
                            string strDate = dateArr[1] + "/" + dateArr[0] + "/" + dateArr[2];
                            SwipeDateTime = Convert.ToDateTime(strDate);
                        }
                        string CompanyNameCSMS = "";

                        string ProcurementContact = "(n/a)";
                        string HSAssessor = "(n/a)";

                        int CompanyId = -1;
                        int SiteId = -1;
                        string ABN = string.Empty;
                        Sites s = sList.Find(SitesColumn.SiteNameEbi, Site);
                        if (s != null) SiteId = s.SiteId;
                        Companies cList = null;
                        try
                        {
                            if (!String.IsNullOrEmpty(ClockId))
                            {
                                DataRow[] dr = dtALL.Select("CWK_NBR = '" + ClockId + "' and EFFECTIVE_END_DATE > '" + SwipeDateTime + "'");
                                if (dr.Length > 0)
                                    if (dr[0]["TAX_REGISTRATION_NUMBER"] != null)
                                        ABN = Convert.ToString(dr[0]["TAX_REGISTRATION_NUMBER"]);
                            }

                            if (ABN != null)
                                cList = cService.GetByCompanyAbn(ABN);
                            else
                                cList = null;
                        }
                        catch (Exception)
                        {
                        }

                        if (cList != null)
                        {
                            CompanyNameCSMS = cList.CompanyName;
                            CompanyId = cList.CompanyId;
                            if (CompanyId != -1)
                            {
                                QuestionnaireWithLocationApprovalViewLatestService qwlavlService = new QuestionnaireWithLocationApprovalViewLatestService();
                                DataSet DSqwlavl = qwlavlService.GetProcurementContactHsAssessor_ByCompanyId(CompanyId);
                                if (DSqwlavl.Tables.Count > 0)
                                {
                                    if (DSqwlavl.Tables[0].Rows.Count > 0)
                                    {
                                        foreach (DataRow DRqwlavl in DSqwlavl.Tables[0].Rows)
                                        {
                                            ProcurementContact = DRqwlavl[0].ToString();
                                            HSAssessor = DRqwlavl[1].ToString();
                                        }
                                    }
                                }
                            }
                        }
                        dtEbi.Rows.Add(new object[] { CompanyName, ClockId, FullName, AccessCardNo, Site, SwipeDateTime, CompanyNameCSMS });
                        if (CompaniesSitesList.Contains(String.Format("{0}|||||{1}", CompanyName, Site)) == false && CompanyId != -1)
                        {
                            CompaniesSitesList.Add(String.Format("{0}|||||{1}", CompanyName, Site));
                            string ApprovedOnSite = "";
                            string Valid = "";
                            string SqValidTill = "";
                            string CompanySQStatus = "";
                            string QuestionnaireId = "";
                            string Validity = "?";
                            string Type = "?";
                            if (CompanyId != -1)
                            {
                                CompaniesService cService2 = new CompaniesService();
                                Companies c = cService2.GetByCompanyId(CompanyId);
                                CompanyStatusService css = new CompanyStatusService();
                                CompanyStatus cs = css.GetByCompanyStatusId(c.CompanyStatusId);
                                CompanySQStatus = cs.CompanyStatusDesc;

                                int _count = 0;
                                VList<QuestionnaireWithLocationApprovalViewLatest> qwlavlList = DataRepository.QuestionnaireWithLocationApprovalViewLatestProvider.GetPaged(
                                                                            String.Format("CompanyId = {0}", CompanyId), "CreatedDate DESC", 0, 9999, out _count);

                                if (_count == 0)
                                {
                                    Valid = NO_SAFETY_QUAL_NOT_FOUND; //DT237
                                }
                                else
                                {
                                    QuestionnaireId = qwlavlList[0].QuestionnaireId.ToString();

                                    if (qwlavlList[0].SubContractor == true)
                                        Type = "Sub Contractor";
                                    else
                                        Type = "Contractor";

                                    CompanySiteCategoryStandardService cscsService = new CompanySiteCategoryStandardService();
                                    if (SiteId != -1)
                                    {
                                        CompanySiteCategoryStandard cscs = cscsService.GetByCompanyIdSiteId(qwlavlList[0].CompanyId, SiteId);
                                        if (cscs != null)
                                        {
                                            switch (cscs.Approved)
                                            {
                                                case true:
                                                    ApprovedOnSite = "Yes";
                                                    break;
                                                case false:
                                                    ApprovedOnSite = "No";
                                                    break;
                                                case null:
                                                    ApprovedOnSite = "Tentative";
                                                    break;
                                                default:
                                                    ApprovedOnSite = "";
                                                    break;
                                            }
                                        }
                                    }

                                    bool CompanyStatusAllowsOnSite = false;
                                    if (Type == "Contractor")
                                    {
                                        //Changed by Pankaj Dikshit for <Issue# 2411, 05-July-2012> - Append next 2nd condition in If
                                        if (c.CompanyStatusId == (int)CompanyStatusList.Active || c.CompanyStatusId == (int)CompanyStatusList.Active_No_PQ_Required_Restricted_Access_to_Site)
                                            CompanyStatusAllowsOnSite = true;
                                    }
                                    else
                                    {
                                        //Changed by Pankaj Dikshit for <Issue# 2411, 05-July-2012> - Append next 2nd condition in If
                                        if (c.CompanyStatusId == (int)CompanyStatusList.SubContractor || c.CompanyStatusId == (int)CompanyStatusList.Active_No_PQ_Required_Restricted_Access_to_Site)
                                            CompanyStatusAllowsOnSite = true;
                                    }

                                    if (qwlavlList[0].MainAssessmentValidTo != null)
                                    {
                                        DateTime dtValidTo = Convert.ToDateTime(qwlavlList[0].MainAssessmentValidTo.ToString());
                                        SqValidTill = dtValidTo.ToString("dd/MM/yyyy");

                                        if (DateTime.Now >= dtValidTo)
                                        {
                                            Validity = "Expired";
                                            Valid = NO_SAFETY_QUAL_EXPIRED; //DT237
                                        }
                                        else
                                        {
                                            TimeSpan span = dtValidTo - DateTime.Now;
                                            if (span.TotalDays <= 60)
                                            {
                                                if (ApprovedOnSite == "Yes")
                                                    Valid = YES_SAFETY_QUAL_CURRENT_EXPIRING;
                                                else
                                                    Valid = NO_ACCESS_TO_SITE;
                                            }
                                            else // > 60 days
                                            {
                                                if (c.CompanyStatusId == (int)CompanyStatusList.Requal_Incomplete && ApprovedOnSite == "Yes") //DT237, >60 days and requalifying = new category
                                                {
                                                    Valid = YES_SAFETY_QUAL_CURRENT_REQUAL;
                                                }
                                                else
                                                {
                                                    Validity = "Current";
                                                    if (ApprovedOnSite == "Yes" && CompanyStatusAllowsOnSite)
                                                        Valid = YES_SAFETY_QUAL_CURRENT;
                                                    else
                                                        Valid = NO_ACCESS_TO_SITE; //DT237
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        Valid = NO_CURRENT_SAFETY_QUAL_NOT_FOUND; //DT237
                                    }
                                }

                                SqExemptionService sqeService = new SqExemptionService();
                                TList<SqExemption> sqeList = sqeService.GetByCompanyId(CompanyId);
                                sqeList = sqeList.FindAll(SqExemptionColumn.SiteId, SiteId);
                                if (sqeList.Count > 0)
                                {
                                    foreach (SqExemption sq in sqeList)
                                    {
                                        if (sq.ValidTo > DateTime.Now)
                                        {
                                            Valid = EXEMPTION; //DT237
                                            break;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                Valid = NO_INFO_NOT_FOUND;
                            }
                            if (!String.IsNullOrEmpty(CompanyName))
                            {
                                dtEbi2.Rows.Add(new object[] { CompanyName, CompanyNameCSMS, Site, ApprovedOnSite, Valid, SqValidTill, CompanySQStatus, QuestionnaireId, Validity, Type,
                                                               ProcurementContact,  HSAssessor});
                            }
                        }
                        else
                        {
                            if (CompanyId == -1 && CompaniesSitesNullList.Contains(String.Format("{0}|||||{1}", CompanyName, Site)) == false)
                            {
                                CompaniesSitesNullList.Add(String.Format("{0}|||||{1}", CompanyName, Site));
                            }
                        }
                    }
                }
                foreach (string str in CompaniesSitesNullList)
                {
                    if (!CompaniesSitesList.Contains(str))
                    {
                        string[] nullList = str.Split('|');
                        string CompanyName = nullList[0];
                        string Site = nullList[5];
                        dtEbi2.Rows.Add(new object[] { CompanyName, "", Site, "", NO_INFO_NOT_FOUND, "", "", "", "", "?",  //DT288 - changed to const
                                                       "",  ""}); //DT237
                    }
                }
            }
            return dtEbi2;
        }
        catch (Exception)
        {
            return null;
        }
    }
    
    private void LoadData3(bool live)
    {
        try
        {
            DataTable dtEbi = new DataTable();
            dtEbi.Columns.Add("CompanyName", typeof(string));
            dtEbi.Columns.Add("ClockId", typeof(string));
            dtEbi.Columns.Add("FullName", typeof(string));
            dtEbi.Columns.Add("AccessCardNo", typeof(string));
            dtEbi.Columns.Add("Site", typeof(string));
            dtEbi.Columns.Add("SwipeDateTime", typeof(string));
            dtEbi.Columns.Add("CompanyName-CSMS", typeof(string));

            CompaniesService cService = new CompaniesService();
            List<String> CompaniesSitesList = new List<String>();
            List<String> CompaniesSitesNullList = new List<String>();

            DataTable dtEbi2 = new DataTable();
            dtEbi2.Columns.Add("CompanyName", typeof(string));
            dtEbi2.Columns.Add("CompanyName-CSMS", typeof(string));
            dtEbi2.Columns.Add("Site", typeof(string));
            dtEbi2.Columns.Add("ApprovedOnSite", typeof(string));
            dtEbi2.Columns.Add("Valid", typeof(string));
            dtEbi2.Columns.Add("SqValidTill", typeof(string));
            dtEbi2.Columns.Add("CompanySQStatus", typeof(string));
            dtEbi2.Columns.Add("QuestionnaireId", typeof(string));
            dtEbi2.Columns.Add("Validity", typeof(string));
            dtEbi2.Columns.Add("Type", typeof(string));

            dtEbi2.Columns.Add("ProcurementContact", typeof(string));
            dtEbi2.Columns.Add("HSAssessor", typeof(string));

            string strQuery;
            if (!live)
                //DT346 - changed EbiCompaniesSitesMap to EbiCompaniesMap as one was a view of the other
                strQuery = "Select * from EbiCompaniesMap where SwipeDay=" + DateTime.Now.Day + " and SwipeMonth=" + DateTime.Now.Month + " and SwipeYear=" + DateTime.Now.Year + " and DATEPART(hour, SwipeDateTime)<9 order by SwipeDateTime";
            else
                strQuery = "Select * from EbiCompaniesMap where SwipeDay=" + DateTime.Now.Day + " and SwipeMonth=" + DateTime.Now.Month + " and SwipeYear=" + DateTime.Now.Year + " order by SwipeDateTime";

            ConnectionStringSettings conString = ConfigurationManager.ConnectionStrings["ALCOA_WAO_CSMWP_devConnectionString"];
            using (SqlConnection conn = new SqlConnection(conString.ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand(strQuery, conn))
                {
                    conn.Open();
                    cmd.CommandTimeout = 60000;
                    SqlDataReader csv = cmd.ExecuteReader();
                    SitesService sService = new SitesService();
                    TList<Sites> sList = sService.GetAll();

                    while (csv.Read())
                    {
                        int CompanyId = -1;
                        string CompanyNameCSMS = "";
                        string CompanyName = csv["EbiCompanyName"].ToString();
                        string ClockId = csv["ClockId"].ToString();
                        string FullName = csv["FullName"].ToString();
                        string AccessCardNo = csv["AccessCardNo"].ToString();
                        string Site = csv["SwipeSite"].ToString();
                        DateTime SwipeDateTime = DateTime.Now;
                        try
                        {
                            CompanyId = Convert.ToInt32(csv["CompanyId"]);
                        }
                        catch (Exception)
                        {
                        }
                        try
                        {
                            CompanyNameCSMS = Convert.ToString(csv["CompanyName"]);
                        }
                        catch (Exception)
                        {
                        }
                        
                        string ProcurementContact = "(n/a)";
                        string HSAssessor = "(n/a)";
                        int SiteId = -1;
                        
                        Sites s = sList.Find(SitesColumn.SiteNameEbi, Site);
                        if (s != null)
                            SiteId = s.SiteId;

                        if (CompanyId != -1)
                        {
                            QuestionnaireWithLocationApprovalViewLatestService qwlavlService = new QuestionnaireWithLocationApprovalViewLatestService();
                            DataSet DSqwlavl = qwlavlService.GetProcurementContactHsAssessor_ByCompanyId(CompanyId);
                            if (DSqwlavl.Tables.Count > 0)
                            {
                                if (DSqwlavl.Tables[0].Rows.Count > 0)
                                {
                                    foreach (DataRow DRqwlavl in DSqwlavl.Tables[0].Rows)
                                    {
                                        ProcurementContact = DRqwlavl[0].ToString();
                                        HSAssessor = DRqwlavl[1].ToString();
                                    }
                                }
                            }
                        }

                        dtEbi.Rows.Add(new object[] { CompanyName, ClockId, FullName, AccessCardNo, Site, SwipeDateTime, CompanyNameCSMS });
                        if (CompaniesSitesList.Contains(String.Format("{0}|||||{1}", CompanyName, Site)) == false && CompanyId != -1)
                        {
                            CompaniesSitesList.Add(String.Format("{0}|||||{1}", CompanyName, Site));
                            string ApprovedOnSite = "";
                            string Valid = "";
                            string SqValidTill = "";
                            string CompanySQStatus = "";
                            string QuestionnaireId = "";
                            string Validity = "?";
                            string CompanyType = "?";
                            if (CompanyId != -1)
                            {
                                CompaniesService cService2 = new CompaniesService();
                                Companies c = cService2.GetByCompanyId(CompanyId);
                                CompanyStatusService css = new CompanyStatusService();
                                CompanyStatus cs = css.GetByCompanyStatusId(c.CompanyStatusId);
                                CompanySQStatus = cs.CompanyStatusDesc;

                                int _count = 0;
                                VList<QuestionnaireWithLocationApprovalViewLatest> qwlavlList = DataRepository.QuestionnaireWithLocationApprovalViewLatestProvider.GetPaged(
                                                                            String.Format("CompanyId = {0}", CompanyId), "CreatedDate DESC", 0, 9999, out _count);

                                if (_count == 0)
                                {
                                    Valid = NO_SAFETY_QUAL_NOT_FOUND; //DT237
                                }
                                else
                                {
                                    QuestionnaireId = qwlavlList[0].QuestionnaireId.ToString();

                                    if (qwlavlList[0].SubContractor == true)
                                        CompanyType = "Sub Contractor";
                                    else
                                        CompanyType = "Contractor";

                                    CompanySiteCategoryStandardService cscsService = new CompanySiteCategoryStandardService();
                                    if (SiteId != -1)
                                    {
                                        CompanySiteCategoryStandard cscs = cscsService.GetByCompanyIdSiteId(qwlavlList[0].CompanyId, SiteId);
                                        if (cscs != null)
                                        {
                                            switch (cscs.Approved)
                                            {
                                                case true:
                                                    ApprovedOnSite = "Yes";
                                                    break;
                                                case false:
                                                    ApprovedOnSite = "No";
                                                    break;
                                                case null:
                                                    ApprovedOnSite = "Tentative";
                                                    break;
                                                default:
                                                    ApprovedOnSite = "";
                                                    break;
                                            }
                                        }
                                    }

                                    bool CompanyStatusAllowsOnSite = false;
                                    if (CompanyType == "Contractor")
                                    {
                                        //Changed by Pankaj Dikshit for <Issue# 2411, 05-July-2012> - Append next 2nd condition in If
                                        if (c.CompanyStatusId == (int)CompanyStatusList.Active || c.CompanyStatusId == (int)CompanyStatusList.Active_No_PQ_Required_Restricted_Access_to_Site)
                                            CompanyStatusAllowsOnSite = true;
                                    }
                                    else
                                    {
                                        //Changed by Pankaj Dikshit for <Issue# 2411, 05-July-2012> - Append next 2nd condition in If
                                        if (c.CompanyStatusId == (int)CompanyStatusList.SubContractor || c.CompanyStatusId == (int)CompanyStatusList.Active_No_PQ_Required_Restricted_Access_to_Site)
                                            CompanyStatusAllowsOnSite = true;
                                    }

                                    if (qwlavlList[0].MainAssessmentValidTo != null)
                                    {
                                        DateTime dtValidTo = Convert.ToDateTime(qwlavlList[0].MainAssessmentValidTo.ToString());
                                        SqValidTill = dtValidTo.ToString("dd/MM/yyyy");

                                        if (DateTime.Now >= dtValidTo)
                                        {
                                            Validity = "Expired";
                                            Valid = NO_SAFETY_QUAL_EXPIRED; //DT237
                                        }
                                        else
                                        {
                                            TimeSpan span = dtValidTo - DateTime.Now;
                                            if (span.TotalDays <= 60)
                                            {
                                                if (ApprovedOnSite == "Yes")
                                                    Valid = YES_SAFETY_QUAL_CURRENT_EXPIRING;
                                                else
                                                    Valid = NO_ACCESS_TO_SITE; //DT237
                                            }
                                            else // > 60 days
                                            {
                                                if (c.CompanyStatusId == (int)CompanyStatusList.Requal_Incomplete && ApprovedOnSite == "Yes")
                                                {
                                                        Valid = YES_SAFETY_QUAL_CURRENT_REQUAL;
                                                }
                                                else
                                                {
                                                    Validity = "Current";
                                                    if (ApprovedOnSite == "Yes" && CompanyStatusAllowsOnSite)
                                                        Valid = YES_SAFETY_QUAL_CURRENT;
                                                    else
                                                        Valid = NO_ACCESS_TO_SITE; //DT237
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        Valid = NO_CURRENT_SAFETY_QUAL_NOT_FOUND; //DT237
                                    }
                                }

                                SqExemptionService sqeService = new SqExemptionService();
                                TList<SqExemption> sqeList = sqeService.GetByCompanyId(CompanyId);
                                sqeList = sqeList.FindAll(SqExemptionColumn.SiteId, SiteId);
                                if (sqeList.Count > 0)
                                {
                                    foreach (SqExemption sq in sqeList)
                                    {
                                        if (sq.ValidTo > DateTime.Now)
                                        {
                                            Valid = EXEMPTION;  //DT237
                                            break;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                Valid = NO_INFO_NOT_FOUND; //DT288 - changed to const
                            }
                            if (!String.IsNullOrEmpty(CompanyName))
                            {
                                dtEbi2.Rows.Add(new object[] { CompanyName, CompanyNameCSMS, Site, ApprovedOnSite, Valid, SqValidTill, CompanySQStatus, QuestionnaireId, Validity, CompanyType,
                                                               ProcurementContact,  HSAssessor});
                            }
                        }
                        else
                        {
                            if (CompanyId == -1 && CompaniesSitesNullList.Contains(String.Format("{0}|||||{1}", CompanyName, Site)) == false)
                            {
                                CompaniesSitesNullList.Add(String.Format("{0}|||||{1}", CompanyName, Site));
                            }
                        }
                    }
                }
                foreach (string str in CompaniesSitesNullList)
                {
                    if (!CompaniesSitesList.Contains(str))
                    {
                        string[] nullList = str.Split('|');
                        string CompanyName = nullList[0];
                        string Site = nullList[5];
                        dtEbi2.Rows.Add(new object[] { CompanyName, "", Site, "",NO_INFO_NOT_FOUND, "", "", "", "", "?",
                                                       "",  ""}); //DT237
                    }
                }
                Session["dtEbi"] = dtEbi2;
                grid.DataSource = (DataTable)Session["dtEbi"];
                grid.DataBind();
            }
        }
        catch (Exception)
        {
            throw; //throw original errors, as we want to know the error.
        }
    }
    
    private void LoadData4(bool live, DataTable dtEbiServer)
    {
        try
        {
            DataTable dtEbi = new DataTable();
            dtEbi.Columns.Add("CompanyName", typeof(string));
            dtEbi.Columns.Add("ClockId", typeof(string));
            dtEbi.Columns.Add("FullName", typeof(string));
            dtEbi.Columns.Add("AccessCardNo", typeof(string));
            dtEbi.Columns.Add("Site", typeof(string));
            dtEbi.Columns.Add("SwipeDateTime", typeof(string));
            dtEbi.Columns.Add("CompanyName-CSMS", typeof(string));

            CompaniesService cService = new CompaniesService();
            List<String> CompaniesSitesList = new List<String>();
            List<String> CompaniesSitesNullList = new List<String>();

            DataTable dtEbi2 = new DataTable();
            dtEbi2.Columns.Add("CompanyName", typeof(string));
            dtEbi2.Columns.Add("CompanyName-CSMS", typeof(string));
            dtEbi2.Columns.Add("Site", typeof(string));
            dtEbi2.Columns.Add("ApprovedOnSite", typeof(string));
            dtEbi2.Columns.Add("Valid", typeof(string));
            dtEbi2.Columns.Add("SqValidTill", typeof(string));
            dtEbi2.Columns.Add("CompanySQStatus", typeof(string));
            dtEbi2.Columns.Add("QuestionnaireId", typeof(string));
            dtEbi2.Columns.Add("Validity", typeof(string));
            dtEbi2.Columns.Add("Type", typeof(string));

            dtEbi2.Columns.Add("ProcurementContact", typeof(string));
            dtEbi2.Columns.Add("HSAssessor", typeof(string));

            string strQuery;
            if (!live)
                //DT346 - changed EbiCompaniesSitesMap to EbiCompaniesMap as one was a view of the other
                strQuery = "Select * from EbiCompaniesMap where SwipeDay=" + DateTime.Now.Day + " and SwipeMonth=" + DateTime.Now.Month + " and SwipeYear=" + DateTime.Now.Year + " and DATEPART(hour, SwipeDateTime)<9 order by SwipeDateTime";
            else
                //Dt 3210 changes
                strQuery = "Select * from EbiCompaniesMap where SwipeDay=" + DateTime.Now.Day + " and SwipeMonth=" + DateTime.Now.Month + " and SwipeYear=" + DateTime.Now.Year + " and EbiCompanyName NOT LIKE 'Alcoa%'" + " order by SwipeDateTime";

            ConnectionStringSettings conString = ConfigurationManager.ConnectionStrings["ALCOA_WAO_CSMWP_devConnectionString"];
            using (SqlConnection conn = new SqlConnection(conString.ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand(strQuery, conn))
                {
                    conn.Open();
                    cmd.CommandTimeout = 60000;

                    SqlDataReader csv = cmd.ExecuteReader();
                    SitesService sService = new SitesService();
                    TList<Sites> sList = sService.GetAll();

                    while (csv.Read())
                    {
                        int CompanyId = -1;
                        string CompanyNameCSMS = "";
                        string CompanyName = csv["EbiCompanyName"].ToString();
                        string ClockId = csv["ClockId"].ToString();
                        string FullName = csv["FullName"].ToString();
                        string AccessCardNo = csv["AccessCardNo"].ToString();
                        string Site = csv["SwipeSite"].ToString();
                        DateTime SwipeDateTime = DateTime.Now;
                        try
                        {
                            CompanyId = Convert.ToInt32(csv["CompanyId"]);
                        }
                        catch (Exception)
                        {
                        }
                        try
                        {
                            CompanyNameCSMS = Convert.ToString(csv["CompanyName"]);
                        }
                        catch (Exception)
                        {
                        }

                        string ProcurementContact = "(n/a)";
                        string HSAssessor = "(n/a)";
                        int SiteId = -1;

                        Sites s = sList.Find(SitesColumn.SiteNameEbi, Site);
                        if (s != null)
                            SiteId = s.SiteId;

                        if (CompanyId != -1)
                        {
                            QuestionnaireWithLocationApprovalViewLatestService qwlavlService = new QuestionnaireWithLocationApprovalViewLatestService();
                            DataSet DSqwlavl = qwlavlService.GetProcurementContactHsAssessor_ByCompanyId(CompanyId);
                            if (DSqwlavl.Tables.Count > 0)
                            {
                                if (DSqwlavl.Tables[0].Rows.Count > 0)
                                {
                                    foreach (DataRow DRqwlavl in DSqwlavl.Tables[0].Rows)
                                    {
                                        ProcurementContact = DRqwlavl[0].ToString();
                                        HSAssessor = DRqwlavl[1].ToString();
                                    }
                                }
                            }
                        }

                        dtEbi.Rows.Add(new object[] { CompanyName, ClockId, FullName, AccessCardNo, Site, SwipeDateTime, CompanyNameCSMS });
                        if (CompaniesSitesList.Contains(String.Format("{0}|||||{1}", CompanyName, Site)) == false && CompanyId != -1)
                        {
                            CompaniesSitesList.Add(String.Format("{0}|||||{1}", CompanyName, Site));
                            string ApprovedOnSite = "";
                            string Valid = "";
                            string SqValidTill = "";
                            string CompanySQStatus = "";
                            string QuestionnaireId = "";
                            string Validity = "?";
                            string Type = "?";
                            if (CompanyId != -1)
                            {
                                CompaniesService cService2 = new CompaniesService();
                                Companies c = cService2.GetByCompanyId(CompanyId);
                                CompanyStatusService css = new CompanyStatusService();
                                CompanyStatus cs = css.GetByCompanyStatusId(c.CompanyStatusId);
                                CompanySQStatus = cs.CompanyStatusDesc;

                                int _count = 0;
                                VList<QuestionnaireWithLocationApprovalViewLatest> qwlavlList = DataRepository.QuestionnaireWithLocationApprovalViewLatestProvider.GetPaged(
                                                                            String.Format("CompanyId = {0}", CompanyId), "CreatedDate DESC", 0, 9999, out _count);

                                if (_count == 0)
                                {
                                    Valid = NO_SAFETY_QUAL_NOT_FOUND; //DT237
                                }
                                else
                                {
                                    QuestionnaireId = qwlavlList[0].QuestionnaireId.ToString();

                                    if (qwlavlList[0].SubContractor == true)
                                        Type = "Sub Contractor";
                                    else
                                        Type = "Contractor";

                                    CompanySiteCategoryStandardService cscsService = new CompanySiteCategoryStandardService();
                                    if (SiteId != -1)
                                    {
                                        CompanySiteCategoryStandard cscs = cscsService.GetByCompanyIdSiteId(qwlavlList[0].CompanyId, SiteId);
                                        if (cscs != null)
                                        {
                                            switch (cscs.Approved)
                                            {
                                                case true:
                                                    ApprovedOnSite = "Yes";
                                                    break;
                                                case false:
                                                    ApprovedOnSite = "No";
                                                    break;
                                                case null:
                                                    ApprovedOnSite = "Tentative";
                                                    break;
                                                default:
                                                    ApprovedOnSite = "";
                                                    break;
                                            }
                                        }
                                    }

                                    bool CompanyStatusAllowsOnSite = false;
                                    if (Type == "Contractor")
                                    {
                                        //Changed by Pankaj Dikshit for <Issue# 2411, 05-July-2012> - Append next 2nd condition in If
                                        if (c.CompanyStatusId == (int)CompanyStatusList.Active || c.CompanyStatusId == (int)CompanyStatusList.Active_No_PQ_Required_Restricted_Access_to_Site)
                                            CompanyStatusAllowsOnSite = true;
                                    }
                                    else
                                    {
                                        //Changed by Pankaj Dikshit for <Issue# 2411, 05-July-2012> - Append next 2nd condition in If
                                        if (c.CompanyStatusId == (int)CompanyStatusList.SubContractor || c.CompanyStatusId == (int)CompanyStatusList.Active_No_PQ_Required_Restricted_Access_to_Site)
                                            CompanyStatusAllowsOnSite = true;
                                    }

                                    if (qwlavlList[0].MainAssessmentValidTo != null)
                                    {
                                        DateTime dtValidTo = Convert.ToDateTime(qwlavlList[0].MainAssessmentValidTo.ToString());
                                        SqValidTill = dtValidTo.ToString("dd/MM/yyyy");

                                        if (DateTime.Now >= dtValidTo)
                                        {
                                            Validity = "Expired";
                                            Valid = NO_SAFETY_QUAL_EXPIRED; //DT237
                                        }
                                        else
                                        {
                                            TimeSpan span = dtValidTo - DateTime.Now;
                                            if (span.TotalDays <= 60)
                                            {
                                                if (ApprovedOnSite == "Yes") //Removed CompanyStatusAllowsOnSite
                                                    Valid = YES_SAFETY_QUAL_CURRENT_EXPIRING;
                                                else
                                                    Valid = NO_ACCESS_TO_SITE; //DT237
                                            }
                                            else // > 60 days
                                            {
                                                if (c.CompanyStatusId == (int)CompanyStatusList.Requal_Incomplete && ApprovedOnSite == "Yes")
                                                {
                                                    Valid = YES_SAFETY_QUAL_CURRENT_REQUAL;
                                                }
                                                else
                                                {
                                                    Validity = "Current";
                                                    if (ApprovedOnSite == "Yes" && CompanyStatusAllowsOnSite)
                                                        Valid = YES_SAFETY_QUAL_CURRENT;
                                                    else
                                                        Valid = NO_ACCESS_TO_SITE; //DT237
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        Valid = NO_CURRENT_SAFETY_QUAL_NOT_FOUND; //DT237
                                    }
                                }

                                SqExemptionService sqeService = new SqExemptionService();
                                TList<SqExemption> sqeList = sqeService.GetByCompanyId(CompanyId);
                                sqeList = sqeList.FindAll(SqExemptionColumn.SiteId, SiteId);
                                if (sqeList.Count > 0)
                                {
                                    foreach (SqExemption sq in sqeList)
                                    {
                                        if (sq.ValidTo > DateTime.Now)
                                        {
                                            Valid = EXEMPTION; //DT237
                                            break;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                Valid = NO_INFO_NOT_FOUND; //DT237
                            }
                            if (!String.IsNullOrEmpty(CompanyName))
                            {
                                dtEbi2.Rows.Add(new object[] { CompanyName, CompanyNameCSMS, Site, ApprovedOnSite, Valid, SqValidTill, CompanySQStatus, QuestionnaireId, Validity, Type,
                                                               ProcurementContact,  HSAssessor});
                            }
                        }
                        else
                        {
                            if (CompanyId == -1 && CompaniesSitesNullList.Contains(String.Format("{0}|||||{1}", CompanyName, Site)) == false)
                            {
                                CompaniesSitesNullList.Add(String.Format("{0}|||||{1}", CompanyName, Site));
                            }
                        }
                    }
                }
                foreach (string str in CompaniesSitesNullList)
                {
                    if (!CompaniesSitesList.Contains(str))
                    {
                        string[] nullList = str.Split('|');
                        string CompanyName = nullList[0];
                        string Site = nullList[5];
                        dtEbi2.Rows.Add(new object[] { CompanyName, "", Site, "", NO_INFO_NOT_FOUND, "", "", "", "", "?",
                                                       "",  ""});//DT237
                    }
                }

                for (int j = 0; j < dtEbiServer.Rows.Count; j++)
                {
                    DataRow dr = dtEbiServer.Rows[j];
                    DataRow[] dr1 = dtEbi2.Select("CompanyName='" + dr["CompanyName"] + "'and Site='" + dr["Site"] + "'");
                    if (dr1.Length == 0)
                        dtEbi2.Rows.Add(new object[] { dr["CompanyName"], dr["CompanyName-CSMS"], dr["Site"], dr["ApprovedOnSite"], dr["Valid"], dr["SqValidTill"],
                                                       dr["CompanySQStatus"], dr["QuestionnaireId"], dr["Validity"], dr["Type"], dr["ProcurementContact"],  dr["HSAssessor"]});
                }

                Session["dtEbi"] = dtEbi2;
                grid.DataSource = (DataTable)Session["dtEbi"];
                grid.DataBind();
            }
        }
        catch (Exception)
        {
            throw new Exception();
        }
    }
    
    private string ReadEbiFile(string FileFullPath)
    {
        Console.WriteLine("Reading file: " + FileFullPath);
        string myFile = "";
        try
        {
            using (FileStream logFileStream = new FileStream(FileFullPath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            {
                StreamReader logFileReader = new StreamReader(logFileStream);
                while (!logFileReader.EndOfStream)
                {
                    string line = logFileReader.ReadLine();
                    myFile += line + "\n";
                }
                logFileReader.Close();
                logFileStream.Close();
            }
            return myFile;
        }
        catch (Exception ex)
        {
            Elmah.ErrorSignal.FromCurrentContext();
            Console.WriteLine("[ERROR] " + ex.Message);
        }
        return "";
    }
    
    private string ReadEbiData()
    {
        string myFile = "CompanyName,ClockId,FullName,AccessCardNo,Site,SwipeDateTime\n";
        string dsClocks = string.Empty;

        SitesService sService = new SitesService();
        SitesFilters sFilters = new SitesFilters();
        sFilters.AppendIsNotNull(SitesColumn.EbiServerName);
        sFilters.Junction = "AND";
        sFilters.AppendIsNotNull(SitesColumn.EbiTodayViewName);
        
        int count = 0;
        TList<Sites> sTlist = sService.GetPaged(sFilters.ToString(), null, 0, 100, out count);
        if(count > 0)
        {
            DataTable[] dt = new DataTable[count];

            ident = WindowsIdentity.GetCurrent();

            List<Thread> threadLst = new List<Thread>();
            int loopIdx = 0;
            foreach(Sites site in sTlist)
            {
                int idx = loopIdx++;
                Thread myNewThread = new Thread(() => ReadEbiServerData(ref dt[idx], site.EbiServerName, site.EbiTodayViewName));
                threadLst.Add(myNewThread);
            }

            foreach(Thread thread in threadLst)
            {
                thread.Start();
            }

            foreach(Thread thread in threadLst)
            {
                thread.Join();
            }

            foreach (DataTable dtAll in dt)
            {
                if (dtAll != null)
                {
                    foreach (DataRow r in dtAll.Rows)
                    {
                        string CompanyName = (string)r["Company"];
                        if ((string)r["ClockID"] != "")
                        {
                            try
                            {
                                int test = Convert.ToInt32(r["ClockID"]);
                            }
                            catch (Exception)
                            {
                                continue;
                            }
                        }
                        string ClockId = (string)r["ClockID"];
                        string FullName = (string)r["FullName"];
                        string AccessCardNo = (string)r["CardNumber"];
                        string Site = (string)r["Site"];
                        DateTime latestDateTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Hour, 0, 0);
                        DateTime SwipeDateTime = (DateTime)r["SwipeDateTime"];
                        if (!(String.IsNullOrEmpty(ClockId) && String.IsNullOrEmpty(CompanyName)))
                        {
                            if (SwipeDateTime > latestDateTime)
                            {
                                myFile += "\"" + CompanyName + "\",\"" + ClockId + "\",\"" + FullName + "\",\"" + AccessCardNo + "\",\"" + Site + "\",\"" + SwipeDateTime.ToString() + "\"\n";
                                if (!dsClocks.Contains(ClockId))
                                    dsClocks += ClockId + ",";
                            }
                        }
                    }
                }
            }
        }
        return myFile + "~" + dsClocks;
    }

    private void ReadEbiServerData(ref DataTable dt, string EbiServername, string EbiViewName)
    {
        WindowsImpersonationContext c = null;

        try
        {
            c = ident.Impersonate();
            ConnectionStringSettings conString = ConfigurationManager.ConnectionStrings[String.Format("Alcoa.Ebi.{0}.ConnectionString", EbiServername)];
            string strConnString = conString.ConnectionString;

            using (SqlConnection cn = new SqlConnection(strConnString))
            {
                try
                {
                    cn.Open();

                    SqlCommand cmd = new SqlCommand("SELECT * FROM " + EbiViewName, cn);
                    cmd.CommandTimeout = 50000;
                    SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(cmd);
                    DataTable dt1 = new DataTable();
                    sqlDataAdapter.Fill(dt1);
                    dt = dt1.Copy();
                }
                catch (Exception)
                {
                }
                finally
                {
                    cn.Close();
                }
            }
        }
        finally
        {
            if (c != null) c.Undo();
        }
    }

    protected void grid_RowCreated(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewTableRowEventArgs e)
    {
        if (e.RowType != DevExpress.Web.ASPxGridView.GridViewRowType.Data) return;
        Image img = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "changeImage") as Image;
        if (img != null)
        {
            img.Visible = false;

            if (grid.GetRowValues(e.VisibleIndex, "Valid") != null)
            {
                string Valid = grid.GetRowValues(e.VisibleIndex, "Valid").ToString();

                switch (Valid)
                {
                    case YES_SAFETY_QUAL_CURRENT:
                        img.Visible = true;
                        img.ImageUrl = "~/Images/greentick.gif";
                        break;
                    case YES_SAFETY_QUAL_CURRENT_EXPIRING:
                        img.Visible = true;
                        img.ImageUrl = "~/Images/TrafficLights/tlYellow.gif";
                        break;
                    case YES_SAFETY_QUAL_CURRENT_REQUAL: //DT237
                        img.Visible = true;
                        img.ImageUrl = "~/Images/TrafficLights/tlYellow.gif";
                        break;
                    case NO_CURRENT_SAFETY_QUAL_NOT_FOUND: //dt237
                        img.Visible = true;
                        img.ImageUrl = "~/Images/TrafficLights/tlRed.gif";
                        break;
                    case NO_SAFETY_QUAL_EXPIRED: //DT237
                        img.Visible = true;
                        img.ImageUrl = "~/Images/TrafficLights/tlRed.gif";
                        break;
                    case NO_ACCESS_TO_SITE:  //DT237
                        img.Visible = true;
                        img.ImageUrl = "~/Images/TrafficLights/tlRed.gif";
                        break;
                    case NO_SAFETY_QUAL_NOT_FOUND: //DT237
                        img.Visible = true;
                        img.ImageUrl = "~/Images/redcross.gif";
                        break;
                    case NO_INFO_NOT_FOUND: //DT237
                        img.Visible = true;
                        img.ImageUrl = "~/Images/redcross.gif";
                        break;
                    case EXEMPTION: //DT237
                        img.Visible = true;
                        img.ImageUrl = "~/Images/TrafficLights/tlPurple.gif";
                        break;
                    default:
                        break;
                }
                img.ToolTip = Valid;
            }
        }

        HyperLink hl = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "hlq") as HyperLink;
        Label lb = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "lbq") as Label;
        if (hl != null && lb != null)
        {
            if (grid.GetRowValues(e.VisibleIndex, "CompanyName-CSMS") != null)
            {
                lb.Visible = true;
                lb.Text = grid.GetRowValues(e.VisibleIndex, "CompanyName-CSMS").ToString();

                if (grid.GetRowValues(e.VisibleIndex, "QuestionnaireId") != null)
                {
                    string QuestionnaireId = grid.GetRowValues(e.VisibleIndex, "QuestionnaireId").ToString();
                    if (!String.IsNullOrEmpty(QuestionnaireId))
                    {
                        lb.Visible = false;
                        hl.Visible = true;
                        hl.Text = grid.GetRowValues(e.VisibleIndex, "CompanyName-CSMS").ToString();
                        hl.NavigateUrl = String.Format("~/SafetyPQ_QuestionnaireModify.aspx{0}", QueryStringModule.Encrypt("q=" + QuestionnaireId));

                    }
                }
            }
        }

        HyperLink hl2 = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "hlebi") as HyperLink;
        Label lb2 = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "lblebi") as Label;
        if (hl2 != null && lb2 != null)
        {
            string CompanyNameEbi = "???";
            if (grid.GetRowValues(e.VisibleIndex, "CompanyName") != null)
            {
                CompanyNameEbi = grid.GetRowValues(e.VisibleIndex, "CompanyName").ToString();
            }

            lb2.Visible = true;
            lb2.Text = CompanyNameEbi;

            bool NotValid = false;

            if (grid.GetRowValues(e.VisibleIndex, "Valid") != null)
            {
                if (String.IsNullOrEmpty(grid.GetRowValues(e.VisibleIndex, "Valid").ToString())) NotValid = true;
            }
            else
            {
                NotValid = true;
            }

            if (NotValid)
            {
                DateTime dt = DateTime.Now.AddDays(-1);
                string SiteName = grid.GetRowValues(e.VisibleIndex, "Site").ToString();

                lb2.Visible = false;
                hl2.Visible = true;
                hl2.Text = CompanyNameEbi;
                hl2.NavigateUrl = String.Format("javascript:popUp('PopUps/Ebi_CompanyEmployeesOnSite.aspx{0}');",
                    QueryStringModule.Encrypt(String.Format("EbiCompanyName={0}&EbiSiteName={1}", Server.UrlEncode((CompanyNameEbi.Replace("???", "")).Replace("&", "[ampersand]")), SiteName)));
            }
        }

    }
    
    protected void btnReset_Click(object sender, EventArgs e)
    {
        grid.LoadClientLayout("version0.5|page1|group1|sort4|a0|a5|a4|a1|visible13|t0|t1|t2|t3|t4|t5|f-1|f-1|t5|f6|t7|t8|f-1|width13|78px|113px|108px|105px|70px|200px|150px|150px|86px|83px|104px|90px|e");
    }

    protected void grid_CellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
    {
        if (e.Column.FieldName == "Validity")
        {
            ASPxComboBox combo = (ASPxComboBox)e.Editor;
            combo.DataBindItems();

            ListEditItem item = new ListEditItem(string.Empty, null);
            combo.Items.Insert(0, item);
        }
    }
}
