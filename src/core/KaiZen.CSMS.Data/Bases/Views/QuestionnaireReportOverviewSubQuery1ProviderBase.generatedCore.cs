﻿#region Using directives

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="QuestionnaireReportOverviewSubQuery1ProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract class QuestionnaireReportOverviewSubQuery1ProviderBaseCore : EntityViewProviderBase<QuestionnaireReportOverviewSubQuery1>
	{
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions
		
		/*
		///<summary>
		/// Fill an VList&lt;QuestionnaireReportOverviewSubQuery1&gt; From a DataSet
		///</summary>
		/// <param name="dataSet">the DataSet</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList&lt;QuestionnaireReportOverviewSubQuery1&gt;"/></returns>
		protected static VList&lt;QuestionnaireReportOverviewSubQuery1&gt; Fill(DataSet dataSet, VList<QuestionnaireReportOverviewSubQuery1> rows, int start, int pagelen)
		{
			if (dataSet.Tables.Count == 1)
			{
				return Fill(dataSet.Tables[0], rows, start, pagelen);
			}
			else
			{
				return new VList<QuestionnaireReportOverviewSubQuery1>();
			}	
		}
		
		
		///<summary>
		/// Fill an VList&lt;QuestionnaireReportOverviewSubQuery1&gt; From a DataTable
		///</summary>
		/// <param name="dataTable">the DataTable that hold the data.</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList<QuestionnaireReportOverviewSubQuery1>"/></returns>
		protected static VList&lt;QuestionnaireReportOverviewSubQuery1&gt; Fill(DataTable dataTable, VList<QuestionnaireReportOverviewSubQuery1> rows, int start, int pagelen)
		{
			int recordnum = 0;
			
			System.Collections.IEnumerator dataRows =  dataTable.Rows.GetEnumerator();
			
			while (dataRows.MoveNext() && (pagelen != 0))
			{
				if(recordnum >= start)
				{
					DataRow row = (DataRow)dataRows.Current;
				
					QuestionnaireReportOverviewSubQuery1 c = new QuestionnaireReportOverviewSubQuery1();
					c.CompanyId = (Convert.IsDBNull(row["CompanyId"]))?(int)0:(System.Int32)row["CompanyId"];
					c.Kwi = (Convert.IsDBNull(row["KWI"]))?string.Empty:(System.String)row["KWI"];
					c.KwiCompanySiteCategory = (Convert.IsDBNull(row["KWI_CompanySiteCategory"]))?string.Empty:(System.String)row["KWI_CompanySiteCategory"];
					c.Pin = (Convert.IsDBNull(row["PIN"]))?string.Empty:(System.String)row["PIN"];
					c.PinCompanySiteCategory = (Convert.IsDBNull(row["PIN_CompanySiteCategory"]))?string.Empty:(System.String)row["PIN_CompanySiteCategory"];
					c.Wgp = (Convert.IsDBNull(row["WGP"]))?string.Empty:(System.String)row["WGP"];
					c.WgpCompanySiteCategory = (Convert.IsDBNull(row["WGP_CompanySiteCategory"]))?string.Empty:(System.String)row["WGP_CompanySiteCategory"];
					c.Hun = (Convert.IsDBNull(row["HUN"]))?string.Empty:(System.String)row["HUN"];
					c.HunCompanySiteCategory = (Convert.IsDBNull(row["HUN_CompanySiteCategory"]))?string.Empty:(System.String)row["HUN_CompanySiteCategory"];
					c.Wdl = (Convert.IsDBNull(row["WDL"]))?string.Empty:(System.String)row["WDL"];
					c.WdlCompanySiteCategory = (Convert.IsDBNull(row["WDL_CompanySiteCategory"]))?string.Empty:(System.String)row["WDL_CompanySiteCategory"];
					c.Bun = (Convert.IsDBNull(row["BUN"]))?string.Empty:(System.String)row["BUN"];
					c.BunCompanySiteCategory = (Convert.IsDBNull(row["BUN_CompanySiteCategory"]))?string.Empty:(System.String)row["BUN_CompanySiteCategory"];
					c.Fml = (Convert.IsDBNull(row["FML"]))?string.Empty:(System.String)row["FML"];
					c.FmlCompanySiteCategory = (Convert.IsDBNull(row["FML_CompanySiteCategory"]))?string.Empty:(System.String)row["FML_CompanySiteCategory"];
					c.Bgn = (Convert.IsDBNull(row["BGN"]))?string.Empty:(System.String)row["BGN"];
					c.BgnCompanySiteCategory = (Convert.IsDBNull(row["BGN_CompanySiteCategory"]))?string.Empty:(System.String)row["BGN_CompanySiteCategory"];
					c.Ce = (Convert.IsDBNull(row["CE"]))?string.Empty:(System.String)row["CE"];
					c.CeCompanySiteCategory = (Convert.IsDBNull(row["CE_CompanySiteCategory"]))?string.Empty:(System.String)row["CE_CompanySiteCategory"];
					c.Ang = (Convert.IsDBNull(row["ANG"]))?string.Empty:(System.String)row["ANG"];
					c.AngCompanySiteCategory = (Convert.IsDBNull(row["ANG_CompanySiteCategory"]))?string.Empty:(System.String)row["ANG_CompanySiteCategory"];
					c.Ptl = (Convert.IsDBNull(row["PTL"]))?string.Empty:(System.String)row["PTL"];
					c.PtlCompanySiteCategory = (Convert.IsDBNull(row["PTL_CompanySiteCategory"]))?string.Empty:(System.String)row["PTL_CompanySiteCategory"];
					c.Pth = (Convert.IsDBNull(row["PTH"]))?string.Empty:(System.String)row["PTH"];
					c.PthCompanySiteCategory = (Convert.IsDBNull(row["PTH_CompanySiteCategory"]))?string.Empty:(System.String)row["PTH_CompanySiteCategory"];
					c.Pel = (Convert.IsDBNull(row["PEL"]))?string.Empty:(System.String)row["PEL"];
					c.PelCompanySiteCategory = (Convert.IsDBNull(row["PEL_CompanySiteCategory"]))?string.Empty:(System.String)row["PEL_CompanySiteCategory"];
					c.Arp = (Convert.IsDBNull(row["ARP"]))?string.Empty:(System.String)row["ARP"];
					c.ArpCompanySiteCategory = (Convert.IsDBNull(row["ARP_CompanySiteCategory"]))?string.Empty:(System.String)row["ARP_CompanySiteCategory"];
					c.Yen = (Convert.IsDBNull(row["YEN"]))?string.Empty:(System.String)row["YEN"];
					c.YenCompanySiteCategory = (Convert.IsDBNull(row["YEN_CompanySiteCategory"]))?string.Empty:(System.String)row["YEN_CompanySiteCategory"];
					c.AcceptChanges();
					rows.Add(c);
					pagelen -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		*/	
						
		///<summary>
		/// Fill an <see cref="VList&lt;QuestionnaireReportOverviewSubQuery1&gt;"/> From a DataReader.
		///</summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pageLength">number of row.</param>
		///<returns>a <see cref="VList&lt;QuestionnaireReportOverviewSubQuery1&gt;"/></returns>
		protected VList<QuestionnaireReportOverviewSubQuery1> Fill(IDataReader reader, VList<QuestionnaireReportOverviewSubQuery1> rows, int start, int pageLength)
		{
			int recordnum = 0;
			while (reader.Read() && (pageLength != 0))
			{
				if(recordnum >= start)
				{
					QuestionnaireReportOverviewSubQuery1 entity = null;
					if (DataRepository.Provider.UseEntityFactory)
					{
						entity = EntityManager.CreateViewEntity<QuestionnaireReportOverviewSubQuery1>("QuestionnaireReportOverviewSubQuery1",  DataRepository.Provider.EntityCreationalFactoryType); 
					}
					else
					{
						entity = new QuestionnaireReportOverviewSubQuery1();
					}
					
					entity.SuppressEntityEvents = true;

					entity.CompanyId = (System.Int32)reader[((int)QuestionnaireReportOverviewSubQuery1Column.CompanyId)];
					//entity.CompanyId = (Convert.IsDBNull(reader["CompanyId"]))?(int)0:(System.Int32)reader["CompanyId"];
					entity.Kwi = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery1Column.Kwi)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery1Column.Kwi)];
					//entity.Kwi = (Convert.IsDBNull(reader["KWI"]))?string.Empty:(System.String)reader["KWI"];
					entity.KwiCompanySiteCategory = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery1Column.KwiCompanySiteCategory)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery1Column.KwiCompanySiteCategory)];
					//entity.KwiCompanySiteCategory = (Convert.IsDBNull(reader["KWI_CompanySiteCategory"]))?string.Empty:(System.String)reader["KWI_CompanySiteCategory"];
					entity.Pin = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery1Column.Pin)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery1Column.Pin)];
					//entity.Pin = (Convert.IsDBNull(reader["PIN"]))?string.Empty:(System.String)reader["PIN"];
					entity.PinCompanySiteCategory = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery1Column.PinCompanySiteCategory)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery1Column.PinCompanySiteCategory)];
					//entity.PinCompanySiteCategory = (Convert.IsDBNull(reader["PIN_CompanySiteCategory"]))?string.Empty:(System.String)reader["PIN_CompanySiteCategory"];
					entity.Wgp = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery1Column.Wgp)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery1Column.Wgp)];
					//entity.Wgp = (Convert.IsDBNull(reader["WGP"]))?string.Empty:(System.String)reader["WGP"];
					entity.WgpCompanySiteCategory = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery1Column.WgpCompanySiteCategory)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery1Column.WgpCompanySiteCategory)];
					//entity.WgpCompanySiteCategory = (Convert.IsDBNull(reader["WGP_CompanySiteCategory"]))?string.Empty:(System.String)reader["WGP_CompanySiteCategory"];
					entity.Hun = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery1Column.Hun)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery1Column.Hun)];
					//entity.Hun = (Convert.IsDBNull(reader["HUN"]))?string.Empty:(System.String)reader["HUN"];
					entity.HunCompanySiteCategory = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery1Column.HunCompanySiteCategory)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery1Column.HunCompanySiteCategory)];
					//entity.HunCompanySiteCategory = (Convert.IsDBNull(reader["HUN_CompanySiteCategory"]))?string.Empty:(System.String)reader["HUN_CompanySiteCategory"];
					entity.Wdl = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery1Column.Wdl)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery1Column.Wdl)];
					//entity.Wdl = (Convert.IsDBNull(reader["WDL"]))?string.Empty:(System.String)reader["WDL"];
					entity.WdlCompanySiteCategory = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery1Column.WdlCompanySiteCategory)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery1Column.WdlCompanySiteCategory)];
					//entity.WdlCompanySiteCategory = (Convert.IsDBNull(reader["WDL_CompanySiteCategory"]))?string.Empty:(System.String)reader["WDL_CompanySiteCategory"];
					entity.Bun = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery1Column.Bun)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery1Column.Bun)];
					//entity.Bun = (Convert.IsDBNull(reader["BUN"]))?string.Empty:(System.String)reader["BUN"];
					entity.BunCompanySiteCategory = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery1Column.BunCompanySiteCategory)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery1Column.BunCompanySiteCategory)];
					//entity.BunCompanySiteCategory = (Convert.IsDBNull(reader["BUN_CompanySiteCategory"]))?string.Empty:(System.String)reader["BUN_CompanySiteCategory"];
					entity.Fml = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery1Column.Fml)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery1Column.Fml)];
					//entity.Fml = (Convert.IsDBNull(reader["FML"]))?string.Empty:(System.String)reader["FML"];
					entity.FmlCompanySiteCategory = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery1Column.FmlCompanySiteCategory)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery1Column.FmlCompanySiteCategory)];
					//entity.FmlCompanySiteCategory = (Convert.IsDBNull(reader["FML_CompanySiteCategory"]))?string.Empty:(System.String)reader["FML_CompanySiteCategory"];
					entity.Bgn = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery1Column.Bgn)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery1Column.Bgn)];
					//entity.Bgn = (Convert.IsDBNull(reader["BGN"]))?string.Empty:(System.String)reader["BGN"];
					entity.BgnCompanySiteCategory = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery1Column.BgnCompanySiteCategory)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery1Column.BgnCompanySiteCategory)];
					//entity.BgnCompanySiteCategory = (Convert.IsDBNull(reader["BGN_CompanySiteCategory"]))?string.Empty:(System.String)reader["BGN_CompanySiteCategory"];
					entity.Ce = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery1Column.Ce)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery1Column.Ce)];
					//entity.Ce = (Convert.IsDBNull(reader["CE"]))?string.Empty:(System.String)reader["CE"];
					entity.CeCompanySiteCategory = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery1Column.CeCompanySiteCategory)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery1Column.CeCompanySiteCategory)];
					//entity.CeCompanySiteCategory = (Convert.IsDBNull(reader["CE_CompanySiteCategory"]))?string.Empty:(System.String)reader["CE_CompanySiteCategory"];
					entity.Ang = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery1Column.Ang)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery1Column.Ang)];
					//entity.Ang = (Convert.IsDBNull(reader["ANG"]))?string.Empty:(System.String)reader["ANG"];
					entity.AngCompanySiteCategory = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery1Column.AngCompanySiteCategory)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery1Column.AngCompanySiteCategory)];
					//entity.AngCompanySiteCategory = (Convert.IsDBNull(reader["ANG_CompanySiteCategory"]))?string.Empty:(System.String)reader["ANG_CompanySiteCategory"];
					entity.Ptl = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery1Column.Ptl)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery1Column.Ptl)];
					//entity.Ptl = (Convert.IsDBNull(reader["PTL"]))?string.Empty:(System.String)reader["PTL"];
					entity.PtlCompanySiteCategory = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery1Column.PtlCompanySiteCategory)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery1Column.PtlCompanySiteCategory)];
					//entity.PtlCompanySiteCategory = (Convert.IsDBNull(reader["PTL_CompanySiteCategory"]))?string.Empty:(System.String)reader["PTL_CompanySiteCategory"];
					entity.Pth = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery1Column.Pth)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery1Column.Pth)];
					//entity.Pth = (Convert.IsDBNull(reader["PTH"]))?string.Empty:(System.String)reader["PTH"];
					entity.PthCompanySiteCategory = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery1Column.PthCompanySiteCategory)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery1Column.PthCompanySiteCategory)];
					//entity.PthCompanySiteCategory = (Convert.IsDBNull(reader["PTH_CompanySiteCategory"]))?string.Empty:(System.String)reader["PTH_CompanySiteCategory"];
					entity.Pel = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery1Column.Pel)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery1Column.Pel)];
					//entity.Pel = (Convert.IsDBNull(reader["PEL"]))?string.Empty:(System.String)reader["PEL"];
					entity.PelCompanySiteCategory = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery1Column.PelCompanySiteCategory)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery1Column.PelCompanySiteCategory)];
					//entity.PelCompanySiteCategory = (Convert.IsDBNull(reader["PEL_CompanySiteCategory"]))?string.Empty:(System.String)reader["PEL_CompanySiteCategory"];
					entity.Arp = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery1Column.Arp)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery1Column.Arp)];
					//entity.Arp = (Convert.IsDBNull(reader["ARP"]))?string.Empty:(System.String)reader["ARP"];
					entity.ArpCompanySiteCategory = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery1Column.ArpCompanySiteCategory)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery1Column.ArpCompanySiteCategory)];
					//entity.ArpCompanySiteCategory = (Convert.IsDBNull(reader["ARP_CompanySiteCategory"]))?string.Empty:(System.String)reader["ARP_CompanySiteCategory"];
					entity.Yen = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery1Column.Yen)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery1Column.Yen)];
					//entity.Yen = (Convert.IsDBNull(reader["YEN"]))?string.Empty:(System.String)reader["YEN"];
					entity.YenCompanySiteCategory = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery1Column.YenCompanySiteCategory)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery1Column.YenCompanySiteCategory)];
					//entity.YenCompanySiteCategory = (Convert.IsDBNull(reader["YEN_CompanySiteCategory"]))?string.Empty:(System.String)reader["YEN_CompanySiteCategory"];
					entity.AcceptChanges();
					entity.SuppressEntityEvents = false;
					
					rows.Add(entity);
					pageLength -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		
		
		/// <summary>
		/// Refreshes the <see cref="QuestionnaireReportOverviewSubQuery1"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="QuestionnaireReportOverviewSubQuery1"/> object to refresh.</param>
		protected void RefreshEntity(IDataReader reader, QuestionnaireReportOverviewSubQuery1 entity)
		{
			reader.Read();
			entity.CompanyId = (System.Int32)reader[((int)QuestionnaireReportOverviewSubQuery1Column.CompanyId)];
			//entity.CompanyId = (Convert.IsDBNull(reader["CompanyId"]))?(int)0:(System.Int32)reader["CompanyId"];
			entity.Kwi = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery1Column.Kwi)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery1Column.Kwi)];
			//entity.Kwi = (Convert.IsDBNull(reader["KWI"]))?string.Empty:(System.String)reader["KWI"];
			entity.KwiCompanySiteCategory = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery1Column.KwiCompanySiteCategory)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery1Column.KwiCompanySiteCategory)];
			//entity.KwiCompanySiteCategory = (Convert.IsDBNull(reader["KWI_CompanySiteCategory"]))?string.Empty:(System.String)reader["KWI_CompanySiteCategory"];
			entity.Pin = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery1Column.Pin)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery1Column.Pin)];
			//entity.Pin = (Convert.IsDBNull(reader["PIN"]))?string.Empty:(System.String)reader["PIN"];
			entity.PinCompanySiteCategory = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery1Column.PinCompanySiteCategory)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery1Column.PinCompanySiteCategory)];
			//entity.PinCompanySiteCategory = (Convert.IsDBNull(reader["PIN_CompanySiteCategory"]))?string.Empty:(System.String)reader["PIN_CompanySiteCategory"];
			entity.Wgp = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery1Column.Wgp)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery1Column.Wgp)];
			//entity.Wgp = (Convert.IsDBNull(reader["WGP"]))?string.Empty:(System.String)reader["WGP"];
			entity.WgpCompanySiteCategory = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery1Column.WgpCompanySiteCategory)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery1Column.WgpCompanySiteCategory)];
			//entity.WgpCompanySiteCategory = (Convert.IsDBNull(reader["WGP_CompanySiteCategory"]))?string.Empty:(System.String)reader["WGP_CompanySiteCategory"];
			entity.Hun = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery1Column.Hun)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery1Column.Hun)];
			//entity.Hun = (Convert.IsDBNull(reader["HUN"]))?string.Empty:(System.String)reader["HUN"];
			entity.HunCompanySiteCategory = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery1Column.HunCompanySiteCategory)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery1Column.HunCompanySiteCategory)];
			//entity.HunCompanySiteCategory = (Convert.IsDBNull(reader["HUN_CompanySiteCategory"]))?string.Empty:(System.String)reader["HUN_CompanySiteCategory"];
			entity.Wdl = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery1Column.Wdl)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery1Column.Wdl)];
			//entity.Wdl = (Convert.IsDBNull(reader["WDL"]))?string.Empty:(System.String)reader["WDL"];
			entity.WdlCompanySiteCategory = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery1Column.WdlCompanySiteCategory)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery1Column.WdlCompanySiteCategory)];
			//entity.WdlCompanySiteCategory = (Convert.IsDBNull(reader["WDL_CompanySiteCategory"]))?string.Empty:(System.String)reader["WDL_CompanySiteCategory"];
			entity.Bun = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery1Column.Bun)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery1Column.Bun)];
			//entity.Bun = (Convert.IsDBNull(reader["BUN"]))?string.Empty:(System.String)reader["BUN"];
			entity.BunCompanySiteCategory = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery1Column.BunCompanySiteCategory)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery1Column.BunCompanySiteCategory)];
			//entity.BunCompanySiteCategory = (Convert.IsDBNull(reader["BUN_CompanySiteCategory"]))?string.Empty:(System.String)reader["BUN_CompanySiteCategory"];
			entity.Fml = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery1Column.Fml)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery1Column.Fml)];
			//entity.Fml = (Convert.IsDBNull(reader["FML"]))?string.Empty:(System.String)reader["FML"];
			entity.FmlCompanySiteCategory = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery1Column.FmlCompanySiteCategory)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery1Column.FmlCompanySiteCategory)];
			//entity.FmlCompanySiteCategory = (Convert.IsDBNull(reader["FML_CompanySiteCategory"]))?string.Empty:(System.String)reader["FML_CompanySiteCategory"];
			entity.Bgn = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery1Column.Bgn)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery1Column.Bgn)];
			//entity.Bgn = (Convert.IsDBNull(reader["BGN"]))?string.Empty:(System.String)reader["BGN"];
			entity.BgnCompanySiteCategory = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery1Column.BgnCompanySiteCategory)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery1Column.BgnCompanySiteCategory)];
			//entity.BgnCompanySiteCategory = (Convert.IsDBNull(reader["BGN_CompanySiteCategory"]))?string.Empty:(System.String)reader["BGN_CompanySiteCategory"];
			entity.Ce = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery1Column.Ce)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery1Column.Ce)];
			//entity.Ce = (Convert.IsDBNull(reader["CE"]))?string.Empty:(System.String)reader["CE"];
			entity.CeCompanySiteCategory = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery1Column.CeCompanySiteCategory)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery1Column.CeCompanySiteCategory)];
			//entity.CeCompanySiteCategory = (Convert.IsDBNull(reader["CE_CompanySiteCategory"]))?string.Empty:(System.String)reader["CE_CompanySiteCategory"];
			entity.Ang = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery1Column.Ang)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery1Column.Ang)];
			//entity.Ang = (Convert.IsDBNull(reader["ANG"]))?string.Empty:(System.String)reader["ANG"];
			entity.AngCompanySiteCategory = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery1Column.AngCompanySiteCategory)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery1Column.AngCompanySiteCategory)];
			//entity.AngCompanySiteCategory = (Convert.IsDBNull(reader["ANG_CompanySiteCategory"]))?string.Empty:(System.String)reader["ANG_CompanySiteCategory"];
			entity.Ptl = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery1Column.Ptl)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery1Column.Ptl)];
			//entity.Ptl = (Convert.IsDBNull(reader["PTL"]))?string.Empty:(System.String)reader["PTL"];
			entity.PtlCompanySiteCategory = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery1Column.PtlCompanySiteCategory)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery1Column.PtlCompanySiteCategory)];
			//entity.PtlCompanySiteCategory = (Convert.IsDBNull(reader["PTL_CompanySiteCategory"]))?string.Empty:(System.String)reader["PTL_CompanySiteCategory"];
			entity.Pth = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery1Column.Pth)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery1Column.Pth)];
			//entity.Pth = (Convert.IsDBNull(reader["PTH"]))?string.Empty:(System.String)reader["PTH"];
			entity.PthCompanySiteCategory = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery1Column.PthCompanySiteCategory)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery1Column.PthCompanySiteCategory)];
			//entity.PthCompanySiteCategory = (Convert.IsDBNull(reader["PTH_CompanySiteCategory"]))?string.Empty:(System.String)reader["PTH_CompanySiteCategory"];
			entity.Pel = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery1Column.Pel)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery1Column.Pel)];
			//entity.Pel = (Convert.IsDBNull(reader["PEL"]))?string.Empty:(System.String)reader["PEL"];
			entity.PelCompanySiteCategory = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery1Column.PelCompanySiteCategory)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery1Column.PelCompanySiteCategory)];
			//entity.PelCompanySiteCategory = (Convert.IsDBNull(reader["PEL_CompanySiteCategory"]))?string.Empty:(System.String)reader["PEL_CompanySiteCategory"];
			entity.Arp = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery1Column.Arp)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery1Column.Arp)];
			//entity.Arp = (Convert.IsDBNull(reader["ARP"]))?string.Empty:(System.String)reader["ARP"];
			entity.ArpCompanySiteCategory = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery1Column.ArpCompanySiteCategory)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery1Column.ArpCompanySiteCategory)];
			//entity.ArpCompanySiteCategory = (Convert.IsDBNull(reader["ARP_CompanySiteCategory"]))?string.Empty:(System.String)reader["ARP_CompanySiteCategory"];
			entity.Yen = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery1Column.Yen)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery1Column.Yen)];
			//entity.Yen = (Convert.IsDBNull(reader["YEN"]))?string.Empty:(System.String)reader["YEN"];
			entity.YenCompanySiteCategory = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery1Column.YenCompanySiteCategory)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery1Column.YenCompanySiteCategory)];
			//entity.YenCompanySiteCategory = (Convert.IsDBNull(reader["YEN_CompanySiteCategory"]))?string.Empty:(System.String)reader["YEN_CompanySiteCategory"];
			reader.Close();
	
			entity.AcceptChanges();
		}
		
		/*
		/// <summary>
		/// Refreshes the <see cref="QuestionnaireReportOverviewSubQuery1"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="QuestionnaireReportOverviewSubQuery1"/> object.</param>
		protected static void RefreshEntity(DataSet dataSet, QuestionnaireReportOverviewSubQuery1 entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.CompanyId = (Convert.IsDBNull(dataRow["CompanyId"]))?(int)0:(System.Int32)dataRow["CompanyId"];
			entity.Kwi = (Convert.IsDBNull(dataRow["KWI"]))?string.Empty:(System.String)dataRow["KWI"];
			entity.KwiCompanySiteCategory = (Convert.IsDBNull(dataRow["KWI_CompanySiteCategory"]))?string.Empty:(System.String)dataRow["KWI_CompanySiteCategory"];
			entity.Pin = (Convert.IsDBNull(dataRow["PIN"]))?string.Empty:(System.String)dataRow["PIN"];
			entity.PinCompanySiteCategory = (Convert.IsDBNull(dataRow["PIN_CompanySiteCategory"]))?string.Empty:(System.String)dataRow["PIN_CompanySiteCategory"];
			entity.Wgp = (Convert.IsDBNull(dataRow["WGP"]))?string.Empty:(System.String)dataRow["WGP"];
			entity.WgpCompanySiteCategory = (Convert.IsDBNull(dataRow["WGP_CompanySiteCategory"]))?string.Empty:(System.String)dataRow["WGP_CompanySiteCategory"];
			entity.Hun = (Convert.IsDBNull(dataRow["HUN"]))?string.Empty:(System.String)dataRow["HUN"];
			entity.HunCompanySiteCategory = (Convert.IsDBNull(dataRow["HUN_CompanySiteCategory"]))?string.Empty:(System.String)dataRow["HUN_CompanySiteCategory"];
			entity.Wdl = (Convert.IsDBNull(dataRow["WDL"]))?string.Empty:(System.String)dataRow["WDL"];
			entity.WdlCompanySiteCategory = (Convert.IsDBNull(dataRow["WDL_CompanySiteCategory"]))?string.Empty:(System.String)dataRow["WDL_CompanySiteCategory"];
			entity.Bun = (Convert.IsDBNull(dataRow["BUN"]))?string.Empty:(System.String)dataRow["BUN"];
			entity.BunCompanySiteCategory = (Convert.IsDBNull(dataRow["BUN_CompanySiteCategory"]))?string.Empty:(System.String)dataRow["BUN_CompanySiteCategory"];
			entity.Fml = (Convert.IsDBNull(dataRow["FML"]))?string.Empty:(System.String)dataRow["FML"];
			entity.FmlCompanySiteCategory = (Convert.IsDBNull(dataRow["FML_CompanySiteCategory"]))?string.Empty:(System.String)dataRow["FML_CompanySiteCategory"];
			entity.Bgn = (Convert.IsDBNull(dataRow["BGN"]))?string.Empty:(System.String)dataRow["BGN"];
			entity.BgnCompanySiteCategory = (Convert.IsDBNull(dataRow["BGN_CompanySiteCategory"]))?string.Empty:(System.String)dataRow["BGN_CompanySiteCategory"];
			entity.Ce = (Convert.IsDBNull(dataRow["CE"]))?string.Empty:(System.String)dataRow["CE"];
			entity.CeCompanySiteCategory = (Convert.IsDBNull(dataRow["CE_CompanySiteCategory"]))?string.Empty:(System.String)dataRow["CE_CompanySiteCategory"];
			entity.Ang = (Convert.IsDBNull(dataRow["ANG"]))?string.Empty:(System.String)dataRow["ANG"];
			entity.AngCompanySiteCategory = (Convert.IsDBNull(dataRow["ANG_CompanySiteCategory"]))?string.Empty:(System.String)dataRow["ANG_CompanySiteCategory"];
			entity.Ptl = (Convert.IsDBNull(dataRow["PTL"]))?string.Empty:(System.String)dataRow["PTL"];
			entity.PtlCompanySiteCategory = (Convert.IsDBNull(dataRow["PTL_CompanySiteCategory"]))?string.Empty:(System.String)dataRow["PTL_CompanySiteCategory"];
			entity.Pth = (Convert.IsDBNull(dataRow["PTH"]))?string.Empty:(System.String)dataRow["PTH"];
			entity.PthCompanySiteCategory = (Convert.IsDBNull(dataRow["PTH_CompanySiteCategory"]))?string.Empty:(System.String)dataRow["PTH_CompanySiteCategory"];
			entity.Pel = (Convert.IsDBNull(dataRow["PEL"]))?string.Empty:(System.String)dataRow["PEL"];
			entity.PelCompanySiteCategory = (Convert.IsDBNull(dataRow["PEL_CompanySiteCategory"]))?string.Empty:(System.String)dataRow["PEL_CompanySiteCategory"];
			entity.Arp = (Convert.IsDBNull(dataRow["ARP"]))?string.Empty:(System.String)dataRow["ARP"];
			entity.ArpCompanySiteCategory = (Convert.IsDBNull(dataRow["ARP_CompanySiteCategory"]))?string.Empty:(System.String)dataRow["ARP_CompanySiteCategory"];
			entity.Yen = (Convert.IsDBNull(dataRow["YEN"]))?string.Empty:(System.String)dataRow["YEN"];
			entity.YenCompanySiteCategory = (Convert.IsDBNull(dataRow["YEN_CompanySiteCategory"]))?string.Empty:(System.String)dataRow["YEN_CompanySiteCategory"];
			entity.AcceptChanges();
		}
		*/
			
		#endregion Helper Functions
	}//end class

	#region QuestionnaireReportOverviewSubQuery1FilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireReportOverviewSubQuery1"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireReportOverviewSubQuery1FilterBuilder : SqlFilterBuilder<QuestionnaireReportOverviewSubQuery1Column>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportOverviewSubQuery1FilterBuilder class.
		/// </summary>
		public QuestionnaireReportOverviewSubQuery1FilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportOverviewSubQuery1FilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireReportOverviewSubQuery1FilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportOverviewSubQuery1FilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireReportOverviewSubQuery1FilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireReportOverviewSubQuery1FilterBuilder

	#region QuestionnaireReportOverviewSubQuery1ParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireReportOverviewSubQuery1"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireReportOverviewSubQuery1ParameterBuilder : ParameterizedSqlFilterBuilder<QuestionnaireReportOverviewSubQuery1Column>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportOverviewSubQuery1ParameterBuilder class.
		/// </summary>
		public QuestionnaireReportOverviewSubQuery1ParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportOverviewSubQuery1ParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireReportOverviewSubQuery1ParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportOverviewSubQuery1ParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireReportOverviewSubQuery1ParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireReportOverviewSubQuery1ParameterBuilder
	
	#region QuestionnaireReportOverviewSubQuery1SortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireReportOverviewSubQuery1"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class QuestionnaireReportOverviewSubQuery1SortBuilder : SqlSortBuilder<QuestionnaireReportOverviewSubQuery1Column>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportOverviewSubQuery1SqlSortBuilder class.
		/// </summary>
		public QuestionnaireReportOverviewSubQuery1SortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion QuestionnaireReportOverviewSubQuery1SortBuilder

} // end namespace
