﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Adminv2_UserControls_Config" Codebehind="Config.ascx.cs" %>

<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
    
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxRoundPanel" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxPanel" tagprefix="dx" %>
    
<dxwgv:ASPxGridView
    ID="ASPxGridView1"
    runat="server" AutoGenerateColumns="False"
    CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
    CssPostfix="Office2003Blue"
    DataSourceID="SqlDataSource1"
    KeyFieldName="Id"
    Width="900px"
    OnRowUpdating="grid_RowUpdating">
    <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
        <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
        </LoadingPanelOnStatusBar>
        <CollapsedButton Height="12px" Width="11px" />
        <ExpandedButton Height="12px" Width="11px" />
        <DetailCollapsedButton Height="12px" Width="11px" />
        <DetailExpandedButton Height="12px" Width="11px" />
        <LoadingPanel Url="~/App_Themes/Office2003BlueOld/GridView/Loading.gif"><%--Enhancement_023:change by debashis for testing platform upgradation--%>
        </LoadingPanel>
    </Images>
    <ImagesFilterControl>
        <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
        </LoadingPanel>
    </ImagesFilterControl>
    <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
        CssPostfix="Office2003Blue">
        <Header ImageSpacing="5px" SortingImageSpacing="5px" />
        <LoadingPanel ImageSpacing="10px" />
    </Styles>
    <Columns>
        <dxwgv:GridViewCommandColumn Caption="Action" VisibleIndex="0" Width="50px">
            <EditButton Visible="True" />
            <ClearFilterButton Visible="True" />
        </dxwgv:GridViewCommandColumn>
        <dxwgv:GridViewDataTextColumn FieldName="Id" ReadOnly="True" Visible="False" VisibleIndex="0">
            <EditFormSettings Visible="False" />
        </dxwgv:GridViewDataTextColumn>
        <dxwgv:GridViewDataTextColumn Caption="Key" FieldName="Key" ReadOnly="True" VisibleIndex="1" Width="260px">
        </dxwgv:GridViewDataTextColumn>
        <dxwgv:GridViewDataTextColumn Caption="Value" FieldName="Value" VisibleIndex="2">
        </dxwgv:GridViewDataTextColumn>
    </Columns>
    <Settings ShowHeaderFilterButton="true" />
    <SettingsPager Visible="False" Mode="ShowAllRecords" />
    <SettingsEditing Mode="Inline" />
    <SettingsBehavior ColumnResizeMode="NextColumn" ConfirmDelete="True"></SettingsBehavior>
</dxwgv:ASPxGridView>

<%--  WHERE _KEY NOT LIKE 'DefaultEhsConsultantContactEmail'--%>
<asp:SqlDataSource ID="SqlDataSource1" runat="server"
    ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    OldValuesParameterFormatString="original_{0}" SelectCommand="SELECT * FROM [Config] WHERE [KEY] NOT LIKE 'DefaultEhsConsultant_%'" 
    UpdateCommand="UPDATE [Config] SET [Value] = @Value WHERE [Id] = @original_Id">
    <UpdateParameters>
        <asp:Parameter Name="Value" Type="String" />
        <asp:Parameter Name="original_Id" Type="Int32" />
    </UpdateParameters>
</asp:SqlDataSource>
<br />
<dxwgv:ASPxGridView
    ID="ASPxGridView2"
    runat="server" AutoGenerateColumns="False"
    CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
    CssPostfix="Office2003Blue"
    DataSourceID="dsConfig_APSSDir"
    KeyFieldName="Id" Visible="False"
    Width="900px">
    <ImagesFilterControl>
        <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
        </LoadingPanel>
    </ImagesFilterControl>
    <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
        CssPostfix="Office2003Blue">
        <Header ImageSpacing="5px" SortingImageSpacing="5px" />
        <LoadingPanel ImageSpacing="10px" />
    </Styles>
    <SettingsPager Visible="False" Mode="ShowAllRecords" PageSize="1" />
    <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
        <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
        </LoadingPanelOnStatusBar>
        <CollapsedButton Height="12px" Width="11px" />
        <ExpandedButton Height="12px" Width="11px" />
        <DetailCollapsedButton Height="12px" Width="11px" />
        <DetailExpandedButton Height="12px" Width="11px" />
        <LoadingPanel Url="~/App_Themes/Office2003BlueOld/GridView/Loading.gif"><%--Enhancement_023:change by debashis for testing platform upgradation--%>
        </LoadingPanel>
    </Images>
    <SettingsEditing Mode="Inline" />
    <SettingsText Title="APSS Directory" />
    <Columns>
        <dxwgv:GridViewCommandColumn Caption="Action" VisibleIndex="0" Width="50px">
            <EditButton Visible="True">
            </EditButton>
        </dxwgv:GridViewCommandColumn>
        <dxwgv:GridViewDataTextColumn FieldName="Id" ReadOnly="True" Visible="False" VisibleIndex="0">
            <EditFormSettings Visible="False" />
        </dxwgv:GridViewDataTextColumn>
        <dxwgv:GridViewDataTextColumn Caption="Key" FieldName="Key" Visible="False" ReadOnly="True">
        </dxwgv:GridViewDataTextColumn>
        <dxwgv:GridViewDataComboBoxColumn Caption="Value" FieldName="Value" VisibleIndex="1">
             <PropertiesComboBox DataSourceID="UsersFullNameDataSource" TextField="UserFullName"
                                                        ValueField="UserEmail" ValueType="System.String">
            </PropertiesComboBox>
        </dxwgv:GridViewDataComboBoxColumn>
    </Columns>
    <Settings ShowColumnHeaders="False" ShowTitlePanel="True" VerticalScrollableHeight="50" />
</dxwgv:ASPxGridView>

 <data:UsersFullNameDataSource ID="UsersFullNameDataSource" runat="server">
        </data:UsersFullNameDataSource>
<data:ConfigDataSource ID="dsConfig_APSSDir" runat="server" EnableDeepLoad="False" SelectMethod="GetPaged" Filter="Key = 'DefaultEhsConsultantContactEmail'" >
    <DeepLoadProperties Method="IncludeChildren" Recursive="False">
    </DeepLoadProperties>
</data:ConfigDataSource>
<br />

<br />
<dx:ASPxRoundPanel ID="ASPxRoundPanel1" runat="server" 
    CssFilePath="~/App_Themes/Office2010BlueNew/{0}/styles.css" 
    CssPostfix="Office2010Blue" EnableDefaultAppearance="False" 
    GroupBoxCaptionOffsetX="6px" GroupBoxCaptionOffsetY="-19px" 
    HeaderText="Escalation Chain (Enable/Disable)" 
    SpriteCssFilePath="~/App_Themes/Office2010BlueNew/{0}/sprite.css" 
    style="text-align: center" Width="200px">
    <ContentPaddings PaddingBottom="10px" PaddingLeft="9px" PaddingRight="11px" 
        PaddingTop="10px" />
    <HeaderStyle>
    <Paddings PaddingBottom="6px" PaddingLeft="9px" PaddingRight="11px" 
        PaddingTop="3px" />
    </HeaderStyle>
    <PanelCollection>
<dx:PanelContent runat="server" SupportsDisabledAttribute="True">
    <dxe:ASPxCheckBox ID="ASPxCheckBox1" runat="server" CheckState="Unchecked" 
        Text="Procurement">
    </dxe:ASPxCheckBox>
    <dxe:ASPxCheckBox ID="ASPxCheckBox2" runat="server" CheckState="Unchecked" 
        Text="Procurement (Supplier)">
    </dxe:ASPxCheckBox>
    <dxe:ASPxCheckBox ID="ASPxCheckBox3" runat="server" CheckState="Unchecked" 
        Text="H&amp;S Assessor">
    </dxe:ASPxCheckBox>
    <dxe:ASPxButton ID="ASPxButton1" runat="server" 
        CssFilePath="~/App_Themes/Office2010BlueNew/{0}/styles.css" 
        CssPostfix="Office2010Blue" OnClick="ASPxButton1_Click" 
        SpriteCssFilePath="~/App_Themes/Office2010BlueNew/{0}/sprite.css" Text="Save">
    </dxe:ASPxButton>
        </dx:PanelContent>
</PanelCollection>
</dx:ASPxRoundPanel>
<br />

<br />
<strong><span style="text-decoration: underline">Guide</span></strong>:
<br />
<br />
<strong>APSSDir</strong> = Directory where APSS documents are stored and sent from to the users of this system
<br />
<strong>TempDir</strong> = Temporary Directory used for virus scanning uploaded files
<br />
<strong>TemplatesDir</strong> = Directory where Template files are stored and sent from to the users of this system<br />
<strong>KpiContractorCutOff</strong> = # Months Contractor can edit man hours for (i.e. -12 = previous 12months, Normal Status = -1 (previous 1 month)
<br />
<strong>TrainingTemplateFileName</strong> = File name of Training Template
<br />
<strong>ContactEmail</strong> = Shared Email Account used for all general enquiries
<br />
<strong>MedicalTemplateFileName</strong> = File name of Medical Template
<br />
