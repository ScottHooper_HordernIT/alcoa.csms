﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.FileVaultTableListProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[CLSCompliant(true)]
	[Designer(typeof(FileVaultTableListDataSourceDesigner))]
	public class FileVaultTableListDataSource : ReadOnlyDataSource<FileVaultTableList>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FileVaultTableListDataSource class.
		/// </summary>
		public FileVaultTableListDataSource() : base(new FileVaultTableListService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the FileVaultTableListDataSourceView used by the FileVaultTableListDataSource.
		/// </summary>
		protected FileVaultTableListDataSourceView FileVaultTableListView
		{
			get { return ( View as FileVaultTableListDataSourceView ); }
		}
		
		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the FileVaultTableListDataSourceView class that is to be
		/// used by the FileVaultTableListDataSource.
		/// </summary>
		/// <returns>An instance of the FileVaultTableListDataSourceView class.</returns>
		protected override BaseDataSourceView<FileVaultTableList, Object> GetNewDataSourceView()
		{
			return new FileVaultTableListDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the FileVaultTableListDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class FileVaultTableListDataSourceView : ReadOnlyDataSourceView<FileVaultTableList>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FileVaultTableListDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the FileVaultTableListDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public FileVaultTableListDataSourceView(FileVaultTableListDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal FileVaultTableListDataSource FileVaultTableListOwner
		{
			get { return Owner as FileVaultTableListDataSource; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal FileVaultTableListService FileVaultTableListProvider
		{
			get { return Provider as FileVaultTableListService; }
		}

		#endregion Properties
		
		#region Methods
		
		#endregion Methods
	}

	#region FileVaultTableListDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the FileVaultTableListDataSource class.
	/// </summary>
	public class FileVaultTableListDataSourceDesigner : ReadOnlyDataSourceDesigner<FileVaultTableList>
	{
	}

	#endregion FileVaultTableListDataSourceDesigner

	#region FileVaultTableListFilter

	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="FileVaultTableList"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FileVaultTableListFilter : SqlFilter<FileVaultTableListColumn>
	{
	}

	#endregion FileVaultTableListFilter

	#region FileVaultTableListExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="FileVaultTableList"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FileVaultTableListExpressionBuilder : SqlExpressionBuilder<FileVaultTableListColumn>
	{
	}
	
	#endregion FileVaultTableListExpressionBuilder		
}

