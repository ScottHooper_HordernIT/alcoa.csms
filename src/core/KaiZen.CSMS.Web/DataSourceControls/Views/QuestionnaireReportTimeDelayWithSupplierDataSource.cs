﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.QuestionnaireReportTimeDelayWithSupplierProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[CLSCompliant(true)]
	[Designer(typeof(QuestionnaireReportTimeDelayWithSupplierDataSourceDesigner))]
	public class QuestionnaireReportTimeDelayWithSupplierDataSource : ReadOnlyDataSource<QuestionnaireReportTimeDelayWithSupplier>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportTimeDelayWithSupplierDataSource class.
		/// </summary>
		public QuestionnaireReportTimeDelayWithSupplierDataSource() : base(new QuestionnaireReportTimeDelayWithSupplierService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the QuestionnaireReportTimeDelayWithSupplierDataSourceView used by the QuestionnaireReportTimeDelayWithSupplierDataSource.
		/// </summary>
		protected QuestionnaireReportTimeDelayWithSupplierDataSourceView QuestionnaireReportTimeDelayWithSupplierView
		{
			get { return ( View as QuestionnaireReportTimeDelayWithSupplierDataSourceView ); }
		}
		
		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the QuestionnaireReportTimeDelayWithSupplierDataSourceView class that is to be
		/// used by the QuestionnaireReportTimeDelayWithSupplierDataSource.
		/// </summary>
		/// <returns>An instance of the QuestionnaireReportTimeDelayWithSupplierDataSourceView class.</returns>
		protected override BaseDataSourceView<QuestionnaireReportTimeDelayWithSupplier, Object> GetNewDataSourceView()
		{
			return new QuestionnaireReportTimeDelayWithSupplierDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the QuestionnaireReportTimeDelayWithSupplierDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class QuestionnaireReportTimeDelayWithSupplierDataSourceView : ReadOnlyDataSourceView<QuestionnaireReportTimeDelayWithSupplier>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportTimeDelayWithSupplierDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the QuestionnaireReportTimeDelayWithSupplierDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public QuestionnaireReportTimeDelayWithSupplierDataSourceView(QuestionnaireReportTimeDelayWithSupplierDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal QuestionnaireReportTimeDelayWithSupplierDataSource QuestionnaireReportTimeDelayWithSupplierOwner
		{
			get { return Owner as QuestionnaireReportTimeDelayWithSupplierDataSource; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal QuestionnaireReportTimeDelayWithSupplierService QuestionnaireReportTimeDelayWithSupplierProvider
		{
			get { return Provider as QuestionnaireReportTimeDelayWithSupplierService; }
		}

		#endregion Properties
		
		#region Methods
		
		#endregion Methods
	}

	#region QuestionnaireReportTimeDelayWithSupplierDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the QuestionnaireReportTimeDelayWithSupplierDataSource class.
	/// </summary>
	public class QuestionnaireReportTimeDelayWithSupplierDataSourceDesigner : ReadOnlyDataSourceDesigner<QuestionnaireReportTimeDelayWithSupplier>
	{
	}

	#endregion QuestionnaireReportTimeDelayWithSupplierDataSourceDesigner

	#region QuestionnaireReportTimeDelayWithSupplierFilter

	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireReportTimeDelayWithSupplier"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireReportTimeDelayWithSupplierFilter : SqlFilter<QuestionnaireReportTimeDelayWithSupplierColumn>
	{
	}

	#endregion QuestionnaireReportTimeDelayWithSupplierFilter

	#region QuestionnaireReportTimeDelayWithSupplierExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireReportTimeDelayWithSupplier"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireReportTimeDelayWithSupplierExpressionBuilder : SqlExpressionBuilder<QuestionnaireReportTimeDelayWithSupplierColumn>
	{
	}
	
	#endregion QuestionnaireReportTimeDelayWithSupplierExpressionBuilder		
}

