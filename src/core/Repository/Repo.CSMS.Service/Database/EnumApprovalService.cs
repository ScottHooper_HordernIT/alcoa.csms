﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface IEnumApprovalService
    {
        EnumApproval Get(Expression<Func<EnumApproval, bool>> where, List<Expression<Func<EnumApproval, object>>> includes);
        List<EnumApproval> GetMany(int? top, Expression<Func<EnumApproval, bool>> where, Func<IQueryable<EnumApproval>, IOrderedQueryable<EnumApproval>> orderBy, List<Expression<Func<EnumApproval, object>>> includes);
        List<EnumApproval> GetManyPaged(int pageNumber, int pageSize, Expression<Func<EnumApproval, bool>> where, Func<IQueryable<EnumApproval>, IOrderedQueryable<EnumApproval>> orderBy, List<Expression<Func<EnumApproval, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<EnumApproval, bool>> where, List<Expression<Func<EnumApproval, object>>> includes);
        void Insert(EnumApproval item, bool saveChanges = true);
        void Update(EnumApproval item, bool saveChanges = true);
        void Delete(EnumApproval item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class EnumApprovalService : IEnumApprovalService
    {
        private readonly IRepository<EnumApproval> _repository;

        public EnumApprovalService(IRepository<EnumApproval> repository)
        {
            this._repository = repository;
        }

        public EnumApproval Get(Expression<Func<EnumApproval, bool>> where, List<Expression<Func<EnumApproval, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<EnumApproval> GetMany(int? top, Expression<Func<EnumApproval, bool>> where, Func<IQueryable<EnumApproval>, IOrderedQueryable<EnumApproval>> orderBy, List<Expression<Func<EnumApproval, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<EnumApproval> GetManyPaged(int pageNumber, int pageSize, Expression<Func<EnumApproval, bool>> where, Func<IQueryable<EnumApproval>, IOrderedQueryable<EnumApproval>> orderBy, List<Expression<Func<EnumApproval, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<EnumApproval, bool>> where, List<Expression<Func<EnumApproval, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(EnumApproval item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("EnumApproval");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(EnumApproval item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("EnumApproval");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(EnumApproval item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("EnumApproval");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
