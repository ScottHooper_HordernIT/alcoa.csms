﻿#region Using directives

using System;

#endregion

namespace KaiZen.CSMS.Entities
{	
	///<summary>
	/// An object representation of the 'UsersCompanies_Active_All' view. [No description found in the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class UsersCompaniesActiveAll : UsersCompaniesActiveAllBase
	{
		#region Constructors

		///<summary>
		/// Creates a new <see cref="UsersCompaniesActiveAll"/> instance.
		///</summary>
		public UsersCompaniesActiveAll():base(){}	
		
		#endregion
	}
}
