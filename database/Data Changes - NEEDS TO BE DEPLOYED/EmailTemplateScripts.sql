


-------------------------------------------------
--Safety Assessor Changed
-------------------------------------------------

SET IDENTITY_INSERT	dbo.EmailTemplate	ON
INSERT INTO dbo.EmailTemplate
(EmailTemplateId,EmailTemplateName,EmailTemplateDesc,  EmailTemplateSubject,  EmailBody)
VALUES
(1,'Safety Assessor Changed','Sent when a user changes the safety assessor on a company','Safety Management Plan: {CompanyName}: Safety Assessor Changed',0x0)
SET IDENTITY_INSERT	dbo.EmailTemplate	OFF

Update dbo.EmailTemplate
SET EmailBody = cast(cast('<p><span style="font-size: 12pt;"><strong>A Companies Safety Management Plan Safety assessor has been changed as follows:</strong></span></p><p>Company Name: {CompanyName}</p><p>Company Assessor: {EhsConsultant_New} (Previously: {EhsConsultant_Old})</p><p><br /></p><p>Changed by: {UserLastName}, {UserFirstName} ({UserCompanyName}) at {CurrentDateTime}</p>' AS varbinary(max)) AS image)
where EmailTemplateId = 1


-----------------------------------------------
--Set up the variables
INSERT INTO dbo.EmailTemplateVariable
(     EmailTemplateId, VariableName, VariableDescription)
VALUES
(1,'{CompanyName}','Name of the company being assessed'),
(1,'{EhsConsultant_New}','Name of the new Alcoa safety assessor'),
(1,'{EhsConsultant_Old}','Name of the old Alcoa safety assessor'),
(1,'{UserLastName}','Last Name of the person making the change'),
(1,'{UserFirstName}','First Name of the person making the change'),
(1,'{UserCompanyName}','The company of the person making the change'),
(1,'{CurrentDateTime}','The date and time the change was made')


-------------------------------------------------
--Safety Management Plan assessed as INCOMPLETE
-------------------------------------------------


SET IDENTITY_INSERT	dbo.EmailTemplate	ON
INSERT INTO dbo.EmailTemplate
(EmailTemplateId,EmailTemplateName,EmailTemplateDesc,  EmailTemplateSubject,  EmailBody)
VALUES
(2,'Safety Management Plan assessed as INCOMPLETE','Sent when a Safety Management Plan is assessed as incomplete','Safety Management Plan assessed as INCOMPLETE for {CompanyName}',0x0)
SET IDENTITY_INSERT	dbo.EmailTemplate	OFF


Update dbo.EmailTemplate
SET EmailBody	= cast(cast('Your Safety Management Plan has been assessed and found to be incomplete.  You may now go back into the Alcoa Contractor Services Management System and alter/supply the relevant information.  Your H&S Assessor is {SafetyAssessorName} and is contactable via e-mail {SafetyAssessorEmail} or PHONE {SafetyAssessorPhoneNumber}' AS varbinary(max)) AS image)
where EmailTemplateId	= 2

-----------------------------------------------
--Set up the variables
INSERT INTO dbo.EmailTemplateVariable
(     EmailTemplateId, VariableName, VariableDescription)
VALUES
(2,'{CompanyName}','Name of the company being assessed'),
(2,'{SafetyAssessorName}','Name of the Alcoa safety assessor'),
(2,'{SafetyAssessorEmail}','Alcoa safety assessor email'),
(2,'{SafetyAssessorPhoneNumber}','Alcoa safety assessor phone number')


-------------------------------------------------
--Contractor Safety Pre-Qualification
-------------------------------------------------


SET IDENTITY_INSERT	dbo.EmailTemplate	ON
INSERT INTO dbo.EmailTemplate
(EmailTemplateId,EmailTemplateName,EmailTemplateDesc,  EmailTemplateSubject,  EmailBody)
VALUES
(3,'Contractor Safety Pre-Qualification','Sent when a contractor submits a safety questionnaire','{PreOrReQualification}: 0 - Procurement Questionnaire submitted for reviewal for {CompanyName}',0x0)
SET IDENTITY_INSERT	dbo.EmailTemplate	OFF


Update dbo.EmailTemplate
SET EmailBody	= cast(cast('A Contractor Questionnaire has been initiated and requires your reviewal/approval as follows: <br>First Name: {FirstName} <br>Last Name:  {LastName}<br>E-Mail Address: {EmailAddress}<br>Job Title: {JobTitle}<br>Telephone: {Telephone}<br>Initial Risk Rating: {InitialRiskRating}<br>Is Safety Pre-Qual Required: {SQRequired}<br><br>Questionnaire Initiated by: {InitiatedByLastName}, {InitiatedByFirstName} ({InitiatedByCompanyName}) at {DateInitiated}<br><br><br>A CONTRACT HAS BEEN INITIATED AND AS THE RELEVANT LEAD SAFETY ASSESSOR YOU HAVE BEEN ASSIGNED TO THIS COMPANY TO BE EVALUATED.' AS varbinary(max)) AS image)
where EmailTemplateId	= 3

-----------------------------------------------
--Set up the variables
INSERT INTO dbo.EmailTemplateVariable
(     EmailTemplateId, VariableName, VariableDescription)
VALUES
(3,'{FirstName}','First Name of the representative of the company'),
(3,'{LastName}','Last Name of the representative of the company'),
(3,'{EmailAddress}','Email Address of the representative of the company'),
(3,'{JobTitle}','Job Title of the representative of the company'),
(3,'{Telephone}','Telephone number of the representative of the company'),
(3,'{SQRequired}','The Safety Qualification Required'),
(3,'{InitiatedByLastName}','Safety Qualification Initiated By (Last Name)'),
(3,'{DateInitiated}','The Date the Safety Qualification was initiated'),
(3,'{PreOrReQualification}','If the qualification is pre-qualification or re-qualification'),
(3,'{InitialRiskRating}','Initial Risk Rating of the company'),
(3,'{InitiatedByFirstName}','Safety Qualification Initiated By (First Name)'),
(3,'{InitiatedByCompanyName}','Safety Qualification Initiated By (First Name)')
-------------------------------------------------
--Contractor Safety Pre-Qualification - Being Assessed
-------------------------------------------------


SET IDENTITY_INSERT	dbo.EmailTemplate	ON
INSERT INTO dbo.EmailTemplate
(EmailTemplateId,EmailTemplateName,EmailTemplateDesc,  EmailTemplateSubject,  EmailBody)
VALUES
(4,'Contractor Safety Pre-Qualification - Being Assessed','Sent when a contractor submits a safety questionnaire and is being assessed','{PreOrReQualification}: 1 - Procurement Questionnaire submitted for {CompanyName}',0x0)
SET IDENTITY_INSERT	dbo.EmailTemplate	OFF


Update dbo.EmailTemplate
SET EmailBody	= cast(cast('A Contractor Questionnaire has been initiated as follows:<br>First Name: {FirstName}<br>Last Name:  {LastName}<br>E-Mail Address: {EmailAddress}<br>Job Title: {JobTitle}<br>Telephone: {Telephone}<br>Initial Risk Rating: {InitialRiskRating}<br>Is Safety Pre-Qual Required: {SQRequired}<br><br>Questionnaire Initiated by: {InitiatedByLastName}, {InitiatedByFirstName} ({InitiatedByCompanyName}) at {DateInitiated}<br><br><br>THE SYSTEM ADMINISTRATOR WILL NOW CHECK THAT THE ABOVE CONTRACTOR HAS ACCESS TO THE SYSTEM AND SEND THEM INFORMATION THAT THEY NEED TO COMPLETE THEIR SAFETY QUALIFICATION QUESTIONNAIRE.' AS varbinary(max)) AS image)
where EmailTemplateId	= 4

-----------------------------------------------
--Set up the variables
INSERT INTO dbo.EmailTemplateVariable
(     EmailTemplateId, VariableName, VariableDescription)
VALUES
(4,'{FirstName}','First Name of the representative of the company'),
(4,'{LastName}','Last Name of the representative of the company'),
(4,'{EmailAddress}','Email Address of the representative of the company'),
(4,'{JobTitle}','Job Title of the representative of the company'),
(4,'{Telephone}','Telephone number of the representative of the company'),
(4,'{SQRequired}','The Safety Qualification Required'),
(4,'{InitiatedByLastName}','Safety Qualification Initiated By (Last Name)'),
(4,'{DateInitiated}','The Date the Safety Qualification was initiated'),
(4,'{PreOrReQualification}','If the qualification is pre-qualification or re-qualification'),
(4,'{InitialRiskRating}','Initial Risk Rating of the company'),
(4,'{InitiatedByFirstName}','Safety Qualification Initiated By (First Name)'),
(4,'{InitiatedByCompanyName}','Safety Qualification Initiated By (First Name)')



-------------------------------------------------
--Contractor Safety Pre-Qualification - Not Recommened for qualification
-------------------------------------------------


SET IDENTITY_INSERT	dbo.EmailTemplate	ON
INSERT INTO dbo.EmailTemplate
(EmailTemplateId,EmailTemplateName,EmailTemplateDesc,  EmailTemplateSubject,  EmailBody)
VALUES
(5,'Contractor Safety Pre-Qualification � Not Recommended for Qualification','Sent when company is not recommed for pre/re qualification','{PreOrReQualification}: 3 - Questionnaire assessed as NOT RECOMMENDED for {CompanyName}',0x0)
SET IDENTITY_INSERT	dbo.EmailTemplate	OFF


Update dbo.EmailTemplate
SET EmailBody	= cast(cast('A Questionnaire has been assessed as follows:<br/><br/>Company Name: {CompanyName}<br/>Recommended: {Recommended}<br/>Final Risk Rating: {RiskRating}<br/><br/>Questionnaire assessed by: {AssessedByLastName}, {AssessedByFirstName} ({AssessedByCompanyName}) at {DateAssessed}<br/><br/><br/>THIS QUESTIONNAIRE HAS BEEN ASSESSED BY EHS / SAFETY ASSESSOR.<br/><br/>CSMS Team Please review this not recommended status.' AS varbinary(max)) AS image)
where EmailTemplateId	= 5

-----------------------------------------------
--Set up the variables
INSERT INTO dbo.EmailTemplateVariable
(     EmailTemplateId, VariableName, VariableDescription)
VALUES
(5,'{CompanyName}','Name of the company being assessed'),
(5,'{Recommended}','The recommendation'),
(5,'{RiskRating}','The risk rating of the company'),
(5,'{AssessedByLastName}','Last name of the person doing the assessment'),
(5,'{AssessedByFirstName}','First name of the person doing the assessment'),
(5,'{AssessedByCompanyName}','The Company doing the assessment'),
(5,'{DateAssessed}','The Date of the assessment')



-------------------------------------------------
--Contractor Safety Pre-Qualification - Questionnaire Assessed as Incomplete
-------------------------------------------------


SET IDENTITY_INSERT	dbo.EmailTemplate	ON
INSERT INTO dbo.EmailTemplate
(EmailTemplateId,EmailTemplateName,EmailTemplateDesc,  EmailTemplateSubject,  EmailBody)
VALUES
(6,'Contractor Safety Pre-Qualification � Questionnaire Assessed as Incomplete','Sent when Questionnaire is assessed as incomoplete','{PreOrReQualification}: Questionnaire assessed as INCOMPLETE for {CompanyName}',0x0)
SET IDENTITY_INSERT	dbo.EmailTemplate	OFF


Update dbo.EmailTemplate
SET EmailBody	= cast(cast('Company Name:{CompanyName}<br/>Your Safety Qualification has been assessed and found to be incomplete.<br/>You may now go back into the Alcoa Contractor Services Management System and alter/supply the relevant information<br/><br/>Your H&S Assessor is {AssessorLastName}, {AssessorFirstName} and is contactable via e-mail ({AssessorEmail}) or PHONE ({AssessorPhone})<br/>' AS varbinary(max)) AS image)
where EmailTemplateId	= 6

-----------------------------------------------
--Set up the variables
INSERT INTO dbo.EmailTemplateVariable
(     EmailTemplateId, VariableName, VariableDescription)
VALUES
(6,'{PreOrReQualification}','If the qualification is pre-qualification or re-qualification'),
(6,'{CompanyName}','Name of the company being assessed'),
(6,'{AssessorLastName}','Assessors last name'),
(6,'{AssessorFirstName}','Assessors first name'),
(6,'{AssessorEmail}','Assessors Email'),
(6,'{AssessorPhone}','Assessors phone number')

-------------------------------------------------
--Questionnaire's procurement contact re-assigned
-------------------------------------------------


SET IDENTITY_INSERT	dbo.EmailTemplate	ON
INSERT INTO dbo.EmailTemplate
(EmailTemplateId,EmailTemplateName,EmailTemplateDesc,  EmailTemplateSubject,  EmailBody)
VALUES
(7,'Questionnaire Procurement Contact Re-Assigned','Sent when a procurement contact is re-assigned','{PreOrReQualification}: 0 - Procurement Contact of Safety Questionnaire for {CompanyName} re-assigned to you',0x0)
SET IDENTITY_INSERT	dbo.EmailTemplate	OFF


Update dbo.EmailTemplate
SET EmailBody	= cast(cast('A Questionnaire''s Procurement Contact has been re-assigned to you as follows:<br><br>Company Name: {CompanyName}<br>Previous Procurement Contact: {PrevProcurementContact}<br>New Procurement Contact: {NewProcurementContact}<br>{AdditionalComments}<br><br>Change submitted by: {ChangedByLastName}, {ChangedByFirstName} ({ChangedByCompanyName}) at {DateChanged}<br>' AS varbinary(max)) AS image)
where EmailTemplateId	= 7

-----------------------------------------------
--Set up the variables
INSERT INTO dbo.EmailTemplateVariable
(     EmailTemplateId, VariableName, VariableDescription)
VALUES
(7,'{PreOrReQualification}','If the qualification is pre-qualification or re-qualification'),
(7,'{CompanyName}','Name of the company being assessed'),
(7,'{PreviousProcurementContact}','Name of the previous procurement contact'),
(7,'{NewProcurementContact}','Name of the new procurement contact'),
(7,'{AdditionalComments}','Additional comments entered in the text box by the user'),
(7,'{ChangedByLastName}','The last name of the peron making the change'),
(7,'{ChangedByFirstName}','The First name of the person making the change'),
(7,'{ChangedByCompanyName}','The company name of the person making the change'),
(7,'{DateChanged}','Date and time change made')



-------------------------------------------------
--Questionnaire's procurement functional supervisor re-assigned
-------------------------------------------------


SET IDENTITY_INSERT	dbo.EmailTemplate	ON
INSERT INTO dbo.EmailTemplate
(EmailTemplateId,EmailTemplateName,EmailTemplateDesc,  EmailTemplateSubject,  EmailBody)
VALUES
(8,'Questionnaire Procurement Functional Supervisor Re-Assigned','Sent when a procurement functional supervisor is re-assigned','{PreOrReQualification}: 0 - Procurement Functional Supervisor of Safety Questionnaire for {CompanyName} re-assigned to you',0x0)
SET IDENTITY_INSERT	dbo.EmailTemplate	OFF


Update dbo.EmailTemplate
SET EmailBody	= cast(cast('A Questionnaire''s Procurement Functional Supervisor has been re-assigned to you as follows:<br><br>Company Name: {CompanyName}<br> Previous Procurement Functional Supervisor: {PrevProcurementContact}<br> Procurement Functional Supervisor: {NewProcurementContact}<br>{AdditionalComments}<br><br>Change submitted by: {ChangedByLastName}, {ChangedByFirstName} ({ChangedByCompanyName}) at {DateChanged}<br>' AS varbinary(max)) AS image)
where EmailTemplateId	= 8

-----------------------------------------------
--Set up the variables
INSERT INTO dbo.EmailTemplateVariable
(     EmailTemplateId, VariableName, VariableDescription)
VALUES
(8,'{PreOrReQualification}','If the qualification is pre-qualification or re-qualification'),
(8,'{CompanyName}','Name of the company being assessed'),
(8,'{PreviousProcurementContact}','Name of the previous procurement contact'),
(8,'{NewProcurementContact}','Name of the new procurement contact'),
(8,'{AdditionalComments}','Additional comments entered in the text box by the user'),
(8,'{ChangedByLastName}','The last name of the peron making the change'),
(8,'{ChangedByFirstName}','The First name of the person making the change'),
(8,'{ChangedByCompanyName}','The company name of the person making the change'),
(8,'{DateChanged}','Date and time change made')


-------------------------------------------------
--Safety Assessor Changed - Email to new safety assessor
-------------------------------------------------

SET IDENTITY_INSERT	dbo.EmailTemplate	ON
INSERT INTO dbo.EmailTemplate
(EmailTemplateId,EmailTemplateName,EmailTemplateDesc,  EmailTemplateSubject,  EmailBody)
VALUES
(9,'Safety Assessor Changed - Email to New Safety Assessor','Sent when a user changes the safety assessor on a company','{PreOrReQualification}: 0 - {CompanyName}: Safety Assessor Changed',0x0)
SET IDENTITY_INSERT	dbo.EmailTemplate	OFF

Update dbo.EmailTemplate
SET EmailBody = cast(cast('A Questionnaire''s Company Safety Assessor has been re-assigned to you as follows:<br><br>Company Name: {CompanyName}<br>Previous Company Assessor:{PreviousSafetyAssessor}<br>New Company Assessor: {NewSafetyAssessor}<br>{AdditionalComments}<br><br>Change submitted by:{ChangedByLastName}, {ChangedByFirstName} ({ChangedByCompanyName}) at {DateChanged}<br>' AS varbinary(max)) AS image)
where EmailTemplateId = 9


-----------------------------------------------
--Set up the variables
INSERT INTO dbo.EmailTemplateVariable
(     EmailTemplateId, VariableName, VariableDescription)
VALUES
(9,'{PreOrReQualification}','If the qualification is pre-qualification or re-qualification'),
(9,'{CompanyName}','Name of the company being assessed'),
(9,'{PreviousSafetyAssessor}','Name of the previous safety assessor'),
(9,'{NewSafetyAssessor}','Name of the new safety assessor'),
(9,'{AdditionalComments}','Additional comments entered in the text box by the user'),
(9,'{ChangedByLastName}','The last name of the peron making the change'),
(9,'{ChangedByFirstName}','The First name of the person making the change'),
(9,'{ChangedByCompanyName}','The company name of the person making the change'),
(9,'{DateChanged}','Date and time change made')


-------------------------------------------------
--Company Status Change Requested
-------------------------------------------------

SET IDENTITY_INSERT	dbo.EmailTemplate	ON
INSERT INTO dbo.EmailTemplate
(EmailTemplateId,EmailTemplateName,EmailTemplateDesc,  EmailTemplateSubject,  EmailBody)
VALUES
(10,'Company Status Change Requested','Sent when a company requests a status change','Company Status Change Request - {CompanyName}',0x0)
SET IDENTITY_INSERT	dbo.EmailTemplate	OFF

Update dbo.EmailTemplate
SET EmailBody = cast(cast('A Companies status change has been requested but <strong>requires approval</strong> as follows:<br /><br /><strong>Company Name</strong>: {CompanyName}<br /><strong>Company Status - Current</strong>: {CurrentCompanyStatus}<br /><strong>Company Status - Requested</strong>: {RequestedCompanyStatus}<br /><strong>Comments</strong>: {Comments}<br /><br /><br /><strong>Requested by</strong>: {RequestedByLastName}, {RequestedByFirstName} ({RequestedByCompanyName}) at {CurrentDateTime}<br /><br /><strong>Quick Links</strong>: <br />1) {QuestionnaireLink}<br />2) <a href=''http://csm.aua.alcoa.com/CompanyChangeApprovalList.aspx''>Manager - Procurement Operations : Approvals List</a><br />' AS varbinary(max)) AS image)
where EmailTemplateId = 10


-----------------------------------------------
--Set up the variables
INSERT INTO dbo.EmailTemplateVariable
(     EmailTemplateId, VariableName, VariableDescription)
VALUES
(10,'{PreOrReQualification}','If the qualification is pre-qualification or re-qualification'),
(10,'{CompanyName}','Name of the company being assessed'),
(10,'{CurrentCompanyStatus}','Current Status of the Company'),
(10,'{RequestedCompanyStatus}','Requested Status of the Company'),
(10,'{Comments}','Additional Comments'),
(10,'{RequestedByLastName}','The last name of the peron making the request'),
(10,'{RequestedByFirstName}','The First name of the person making the request'),
(10,'{RequestedByCompanyName}','The company name of the person making the request'),
(10,'{CurrentDateTime}','Date and time request made'),
(10,'{QuestionnaireLink}','System Generated link to questionnaire')


-------------------------------------------------
-- Safety Qualification Questionnaire Key Contact Changed
-------------------------------------------------

SET IDENTITY_INSERT	dbo.EmailTemplate	ON
INSERT INTO dbo.EmailTemplate
(EmailTemplateId,EmailTemplateName,EmailTemplateDesc,  EmailTemplateSubject,  EmailBody)
VALUES
(11,'Safety Qualification Questionnaire Key Contact Changed - Internal','Sent when a verified user contact details have been changed','{CompanyName} - Safety Qualification Questionnaire Key Contact Changed',0x0)
SET IDENTITY_INSERT	dbo.EmailTemplate	OFF

Update dbo.EmailTemplate
SET EmailBody = cast(cast('<p><span style="font-size: 12pt;"><strong>A Companies Safety Management Plan Safety assessor has been changed as follows:</strong></span></p><p>Company Name: {CompanyName}</p><p>Company Assessor: {EhsConsultant_New} (Previously: {EhsConsultant_Old})</p><p><br /></p><p>Changed by: {UserLastName}, {UserFirstName} ({UserCompanyName}) at {CurrentDateTime}</p>' AS varbinary(max)) AS image)
where EmailTemplateId = 11


-----------------------------------------------
--Set up the variables
INSERT INTO dbo.EmailTemplateVariable
(     EmailTemplateId, VariableName, VariableDescription)
VALUES
(11,'{CompanyName}','Name of the company'),
(11,'{FirstName_Old}','First Name of the old key contact'),
(11,'{LastName_Old}','Last Name of the old key contact'),
(11,'{Email_Old}','Email of the old key contact'),
(11,'{JobTitle_Old}','Job Title of the old key contact'),
(11,'{Telephone_Old}','Telephone of the old key contact'),
(11,'{FirstName_New}','First Name of the new key contact'),
(11,'{LastName_New}','Last Name of the new key contact'),
(11,'{Email_New}','Email of the new key contact'),
(11,'{JobTitle_New}','Job Title of the new key contact'),
(11,'{TelephonevNew}','Telephone of the new key contact')



-------------------------------------------------
--Company Status Change Request - Not Approved
-------------------------------------------------

SET IDENTITY_INSERT	dbo.EmailTemplate	ON
INSERT INTO dbo.EmailTemplate
(EmailTemplateId,EmailTemplateName,EmailTemplateDesc,  EmailTemplateSubject,  EmailBody)
VALUES
(12,'Company Status Change Request - Not Approved','Sent when a company requests a status change is approved','Company Status Change Request - {CompanyName} - Not Approved',0x0)
SET IDENTITY_INSERT	dbo.EmailTemplate	OFF

Update dbo.EmailTemplate
SET EmailBody = cast(cast('The below Company Status Change Request has been [<strong>NOT APPROVED</strong>] and as such this company needs to be re-qualified.<br /><br /><strong>Company Name</strong>: {CompanyName}<br /><strong>Original Company Status</strong>: {OriginalCompanyStatus}<br /><strong>Requested Company Status</strong>: {RequestedCompanyStatus}<br /><strong>Requester Comments</strong>: {Comments}<br /><strong>Assessor Comments</strong>: {AssessorComments} <br /><br /><br /><strong>Assessed by</strong>: {AssessedByLastName}, {AssessedByFirstName} ({AssessedByCompanyName}) at {CurrentDateTime}<br /><br /><strong>Quick Links</strong>: <br />1) {QuestionnaireLink}<br />' AS varbinary(max)) AS image)
where EmailTemplateId = 12


-----------------------------------------------
--Set up the variables
INSERT INTO dbo.EmailTemplateVariable
(     EmailTemplateId, VariableName, VariableDescription)
VALUES
(12,'{CompanyName}','Name of the company being assessed'),
(12,'{OriginalCompanyStatus}','Original Status of the Company'),
(12,'{RequestedCompanyStatus}','Requested Status of the Company'),
(12,'{Comments}','Comments'),
(12,'{AssessorComments}','Assessor Comments'),
(12,'{AssessedByLastName}','The last name of the peron making the assessment'),
(12,'{AssessedByFirstName}','The First name of the person making the assessment'),
(12,'{AssessedByCompanyName}','The company name of the person making the assessment'),
(12,'{CurrentDateTime}','Date and time request made'),
(12,'{QuestionnaireLink}','System Generated link to questionnaire')

-------------------------------------------------
--Company Status Change Request - Approved
-------------------------------------------------

SET IDENTITY_INSERT	dbo.EmailTemplate	ON
INSERT INTO dbo.EmailTemplate
(EmailTemplateId,EmailTemplateName,EmailTemplateDesc,  EmailTemplateSubject,  EmailBody)
VALUES
(13,'Company Status Change Request - Approved','Sent when a company requests a status change and is approved','Company Status Change Request - {CompanyName} - Approved',0x0)
SET IDENTITY_INSERT	dbo.EmailTemplate	OFF

Update dbo.EmailTemplate
SET EmailBody = cast(cast('The below Company Status Change Request has been [<strong>APPROVED</strong>] as follows:<br /><br /><strong>Company Name</strong>: {CompanyName}<br /><strong>Original Company Status</strong>: {OriginalCompanyStatus}<br /><strong>Requested Company Status</strong>: {RequestedCompanyStatus}<br /><strong>Requester Comments</strong>: {Comments}<br /><strong>Assessor Comments</strong>: {AssessorComments} <br /><br /><br /><strong>Assessed by</strong>: {AssessedByLastName}, {AssessedByFirstName} ({AssessedByCompanyName}) at {CurrentDateTime}<br /><br /><strong>Quick Links</strong>: <br />1) {QuestionnaireLink}<br />' AS varbinary(max)) AS image)
where EmailTemplateId = 13


-----------------------------------------------
--Set up the variables
INSERT INTO dbo.EmailTemplateVariable
(     EmailTemplateId, VariableName, VariableDescription)
VALUES
(13,'{CompanyName}','Name of the company being assessed'),
(13,'{OriginalCompanyStatus}','Original Status of the Company'),
(13,'{RequestedCompanyStatus}','Requested Status of the Company'),
(13,'{Comments}','Comments'),
(13,'{AssessorComments}','Assessor Comments'),
(13,'{AssessedByLastName}','The last name of the peron making the assessment'),
(13,'{AssessedByFirstName}','The First name of the person making the assessment'),
(13,'{AssessedByCompanyName}','The company name of the person making the assessment'),
(13,'{CurrentDateTime}','Date and time request made'),
(13,'{QuestionnaireLink}','System Generated link to questionnaire')


-------------------------------------------------
--Company Status Changed
-------------------------------------------------

SET IDENTITY_INSERT	dbo.EmailTemplate	ON
INSERT INTO dbo.EmailTemplate
(EmailTemplateId,EmailTemplateName,EmailTemplateDesc,  EmailTemplateSubject,  EmailBody)
VALUES
(14,'Company Status Changed','Sent when a company status has been changed','{PreOrReQualification}: 4 - {CompanyName}: Company Status Changed',0x0)
SET IDENTITY_INSERT	dbo.EmailTemplate	OFF

Update dbo.EmailTemplate
SET EmailBody = cast(cast('A Companies status has been changed as follows:<br/><br/>Company Name: {CompanyName}<br/>Company Status: {CompanyStatus}<br/><br/><br/>Changed by:  {ChangedByLastName}, {ChangedByFirstName} ({ChangedByCompanyName}) at {CurrentDateTime}<br/>' AS varbinary(max)) AS image)
where EmailTemplateId = 14


-----------------------------------------------
--Set up the variables
INSERT INTO dbo.EmailTemplateVariable
(     EmailTemplateId, VariableName, VariableDescription)
VALUES
(14,'{CompanyName}','Name of the company being assessed'),
(14,'{CompanyStatus}','Status of the Company'),
(14,'{ChangedByLastName}','The last name of the peron making the assessment'),
(14,'{ChangedByFirstName}','The First name of the person making the assessment'),
(14,'{ChangedByCompanyName}','The company name of the person making the assessment'),
(14,'{CurrentDateTime}','Date and time request made')



-------------------------------------------------
--Questionnaire Assessed - Sent to EHS Consultant
-------------------------------------------------

SET IDENTITY_INSERT	dbo.EmailTemplate	ON
INSERT INTO dbo.EmailTemplate
(EmailTemplateId,EmailTemplateName,EmailTemplateDesc,  EmailTemplateSubject,  EmailBody)
VALUES
(15,'Questionnaire Assessed � Sent To EHS Consultant','Sent to EHS Consultant when a questionnaire is assessed','{PreOrReQualification}: 3 - Questionnaire assessed for {CompanyName}',0x0)
SET IDENTITY_INSERT	dbo.EmailTemplate	OFF

Update dbo.EmailTemplate
SET EmailBody = cast(cast('A Questionnaire has been assessed as follows: <br/><br/>Company Name: {CompanyName}<br/>Recommended: {Recommended}</br>Final Risk Rating: {RiskRating}<br/><br/>Questionnaire assessed by: {AssessedByLastName}, {AssessedByFirstName} ({AssessedByCompanyName}) at {AssessedDateTime}<br/><br/><br/>THIS QUESTIONNAIRE HAS BEEN ASSESSED BY EHS / SAFETY ASSESSOR.<br/><br/>PROCUREMENT (INITIATOR) NOW NEED TO REVIEW THE ASSESSMENT AND UPDATE THE COMPANY STATUS TO REFLECT THE CONTRACTOR STATUS.' AS varbinary(max)) AS image)
where EmailTemplateId = 15


-----------------------------------------------
--Set up the variables
INSERT INTO dbo.EmailTemplateVariable
(     EmailTemplateId, VariableName, VariableDescription)
VALUES
(15,'{PreOrReQualification}','If the qualification is pre-qualification or re-qualification'),
(15,'{CompanyName}','Name of the company being assessed'),
(15,'{Recommended}','Has the company been recommended'),
(15,'{RiskRating}','Risk Rating of the company'),
(15,'{AssessedByLastName}','The last name of the peron making the assessment'),
(15,'{AssessedByFirstName}','The First name of the person making the assessment'),
(15,'{AssessedByCompanyName}','The company name of the person making the assessment'),
(15,'{AssessedDateTime}','Date and time the assessment made')


-------------------------------------------------
--Questionnaire Submitted for Assessment � Sent to EHS Consultant
-------------------------------------------------

SET IDENTITY_INSERT	dbo.EmailTemplate	ON
INSERT INTO dbo.EmailTemplate
(EmailTemplateId,EmailTemplateName,EmailTemplateDesc,  EmailTemplateSubject,  EmailBody)
VALUES
(16,'Questionnaire Submitted for Assessment � Sent to EHS Consultant','Sent to EHS Consultant when a questionnaire is submitted for assessed','{PreOrReQualification}: 2 - Questionnaire submitted for {CompanyName}',0x0)
SET IDENTITY_INSERT	dbo.EmailTemplate	OFF

Update dbo.EmailTemplate
SET EmailBody = cast(cast('A Questionnaire has been submitted for assessment as follows:<br/><br/>Company Name: {CompanyName}<br/><br/><br/>Questionnaire Submitted by: {SubmittedByLastName}, {SubmittedByFirstName} ({SubmittedByCompanyName}) at {SubmittedDateTime}<br/><br/><br/>THIS SAFETY QUALIFICATION QUESTIONNAIRE WILL NOW BE ASSESSED BY EHS / SAFETY ASSESSOR WHO WILL ASSESS THE QUESTIONNAIRE AND MAKE A RECOMMENDATION.' AS varbinary(max)) AS image)
where EmailTemplateId = 16


-----------------------------------------------
--Set up the variables
INSERT INTO dbo.EmailTemplateVariable
(     EmailTemplateId, VariableName, VariableDescription)
VALUES
(16,'{PreOrReQualification}','If the qualification is pre-qualification or re-qualification'),
(16,'{CompanyName}','Name of the company being assessed'),
(16,'{SubmittedByLastName}','The last name of the peron making the assessment'),
(16,'{SubmittedByFirstName}','The First name of the person making the assessment'),
(16,'{SubmittedByCompanyName}','The company name of the person making the assessment'),
(16,'{SubmittedDateTime}','Date and time the assessment made')



-------------------------------------------------
--Forciby Require Verification Questionnaire and Set Questionnaire Statius to [INCOMPLETE]
-------------------------------------------------

SET IDENTITY_INSERT	dbo.EmailTemplate	ON
INSERT INTO dbo.EmailTemplate
(EmailTemplateId,EmailTemplateName,EmailTemplateDesc,  EmailTemplateSubject,  EmailBody)
VALUES
(17,'Forciby Require Verification Questionnaire and Set Questionnaire Statius to [INCOMPLETE]','Sent from Questionniare Administative Controls when administrators want to force a verification questionnaire upon a user','{CompanyName} - Verification Questionnaire completion now required',0x0)
SET IDENTITY_INSERT	dbo.EmailTemplate	OFF

Update dbo.EmailTemplate
SET EmailBody = cast(cast('<strong>Company: </strong>{CompanyName}<br/><br/><strong>Reason for Verification Questionnaire Being Required:</strong> {Reason}' AS varbinary(max)) AS image)
where EmailTemplateId = 17


-----------------------------------------------
--Set up the variables
INSERT INTO dbo.EmailTemplateVariable
(     EmailTemplateId, VariableName, VariableDescription)
VALUES
(17,'{CompanyName}','Name of the company required to complete Verification Questionnaire'),
(17,'{Reason}','The reason the verification needs to be completed - entered by user at time of sending email')


