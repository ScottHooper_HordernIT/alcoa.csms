﻿<%@ Control Language="C#" AutoEventWireup="true"
    Inherits="UserControls_ComplianceChart2" Codebehind="ComplianceChart2.ascx.cs" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dxpc" %>
<%@ Register Src="../Other/Reports_DropDownListPicker2.ascx" TagName="Reports_DropDownListPicker2"
    TagPrefix="uc1" %>
<%@ Register assembly="DevExpress.Web.v14.1" namespace="DevExpress.Web.ASPxPopupControl" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<table border="0" cellpadding="2" cellspacing="0" width="900px">
    <tr>
        <td class="pageName" colspan="7" style="text-align: left; width: 900px; height: 30px;">
            <span class="bodycopy"><span class="title">Safety KPI Compliance Chart (Radar)</span><br />
                <span class="date">Quantitative Comparison of Target vs Actual</span><br />
                <img height="1" src="images/grfc_dottedline.gif" width="24"  />
            </span>
        </td>
    </tr>
    
    <tr>
        <td class="pageName" colspan="7" style="width: 900px; height: 17px; text-align: left">
            <table border="0" cellpadding="0" cellspacing="0" style="width: 900px">
                <tr>
                    <td style="width: 700px">
                        <uc1:Reports_DropDownListPicker2 ID="Reports_DropDownListPicker" runat="server" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="pageName" colspan="7" style="width: 900px; height: 17px; text-align: left">
            <asp:PlaceHolder ID="PlaceHolder1" runat="server"></asp:PlaceHolder>
        </td>
    </tr>
    <tr>
        <td align="right" style="padding:4px 0px 4px 0px">
            <asp:UpdatePanel ID="UpdatePanel7" runat="server">
                     <ContentTemplate>
                        <dxe:ASPxButton ID="btnPdf" runat="server" Visible="false" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                            CssPostfix="Office2003Blue" OnClick="btnPdf_Click" Text="Save to PDF" ValidationGroup="site"
                            Width="99px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                        </dxe:ASPxButton>
                        </ContentTemplate>
                        <Triggers>
                        <asp:PostBackTrigger ControlID="btnPdf" />
                          
                        </Triggers>
                        </asp:UpdatePanel>                       
        </td>
    </tr>
    </table>
    
     <div align="center" id="divLegend" runat="server" style="text-align: center; padding:4px 0px 4px 0px; display:none">     
        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
            <tr>
                <td style="width: 100%; height: 13px; text-align: center">
                    <table border="0" cellpadding="0" cellspacing="0" class="dxgvControl_Office2003_Blue"
                        style="width: 100%; border-collapse: collapse; empty-cells: show">
                        <tbody>
                            <tr>
                                <td class="dxgvHeader_Office2003_Blue" colspan="2" style="border-top-width: 0px;
                                    font-weight: bold; border-left-width: 0px; white-space: normal; height: 13px;
                                    text-align: left;">
                                    <span style="color: maroon">Legend</span>
                                </td>
                            </tr>
                            <tr class="dxgvDataRow_Office2003_Blue">
                                <td class="dxgv" style="width: 30px; height: 20px; text-align: center; background-color: Lime">
                                    100
                                </td>
                                <td class="dxgv" style="width: 972px; height: 20px; text-align: left">
                                    A Green Color Cell means that <strong>100%</strong> of all Companies are compliant
                                    for the specified KPI Measure.
                                </td>
                            </tr>
                            <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                <td class="dxgv" style="width: 30px; height: 20px; text-align: center; background-color: Yellow">
                                    >80
                                </td>
                                <td class="dxgv" style="width: 972px; height: 20px; text-align: left">
                                    A Yellow Colored Cell means that <strong>greater than 80%</strong> of all Companies
                                    are compliant for the specified KPI Measure.
                                </td>
                            </tr>
                            <tr class="dxgvDataRow_Office2003_Blue">
                                <td class="dxgv" style="width: 30px; height: 20px; text-align: center; background-color: Red">
                                    <80
                                </td>
                                <td class="dxgv" style="width: 972px; height: 20px; text-align: left">
                                    A Red Coloured Cell means that <strong>less than 80%</strong> of all Companies are
                                    compliant for the specified KPI Measure.
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </table>
       
    </div>
 
<dxpc:ASPxPopupControl ID="ASPxPopupControl1" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
    CssPostfix="Office2003Blue" EnableHotTrack="False" HeaderText="Warning" Height="111px"
    Modal="True" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter"
    Width="439px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
    <SizeGripImage Height="16px" Width="16px">
    </SizeGripImage>
    <HeaderStyle>
        <Paddings PaddingRight="6px"></Paddings>
    </HeaderStyle>
    <CloseButtonImage Height="12px" Width="13px">
    </CloseButtonImage>
    <ContentCollection>
        <dxpc:PopupControlContentControl runat="server" SupportsDisabledAttribute="True">
            <dxe:ASPxLabel runat="server" Height="30px" Font-Size="14px" ID="ASPxLabel1">
                <Border BorderColor="White" BorderWidth="10px"></Border>
            </dxe:ASPxLabel>
        </dxpc:PopupControlContentControl>
    </ContentCollection>
</dxpc:ASPxPopupControl>

<asp:SqlDataSource ID="sqldsKpi_GetAllYearsSubmitted" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="_Kpi_GetAllYearsSubmitted" SelectCommandType="StoredProcedure">
</asp:SqlDataSource>
