﻿<%@ Control Language="C#" AutoEventWireup="true"
	Inherits="UserControls_Reports_Variation" Codebehind="Reports_Variation.ascx.cs" %>
<%@ Register Src="../Other/ExportButtons.ascx" TagName="ExportButtons" TagPrefix="uc1" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
	Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
	Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<table border="0" cellpadding="2" cellspacing="0" width="100%">
	<tr>
		<td class="pageName" style="text-align: left; ">
			<span class="title">Reports</span><br />
			<span class="date">Incident Variation Report</span><br />
			<img height="1" src="images/grfc_dottedline.gif" width="24" />
		</td>
	</tr>
	<tr>
		<td>
			<dxwgv:ASPxGridView ID="grid" runat="server" AutoGenerateColumns="False" ClientInstanceName="grid"
				Width="100%" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue"
				DataSourceID="EhsimsExceptionsCompaniesSitesDataSource" KeyFieldName="EhsimsExceptionId">
				<ImagesFilterControl>
					<LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
					</LoadingPanel>
				</ImagesFilterControl>
				<Styles CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css">
					<Header SortingImageSpacing="5px" ImageSpacing="5px">
					</Header>
					<LoadingPanel ImageSpacing="10px">
					</LoadingPanel>
				</Styles>
				<Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
					<LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
					</LoadingPanelOnStatusBar>
					<CollapsedButton Width="11px">
					</CollapsedButton>
					<ExpandedButton Width="11px">
					</ExpandedButton>
					<DetailCollapsedButton Width="11px">
					</DetailCollapsedButton>
					<DetailExpandedButton Width="11px">
					</DetailExpandedButton>
					<FilterRowButton Width="13px">
					</FilterRowButton>
					<LoadingPanel Url="~/App_Themes/Office2003BlueOld/GridView/Loading.gif"><%--Enhancement_023:change by debashis for testing platform upgradation--%>
					</LoadingPanel>
				</Images>
				<Columns>
					<dxwgv:GridViewDataDateColumn Caption="Report Date/Time" FieldName="ReportDateTime"
						VisibleIndex="0" SortIndex="0" SortOrder="Descending">
					</dxwgv:GridViewDataDateColumn>
					<dxwgv:GridViewDataTextColumn FieldName="EhsimsExceptionId" ReadOnly="True" Visible="False"
						VisibleIndex="0">
						<EditFormSettings Visible="False" />
					</dxwgv:GridViewDataTextColumn>
					<dxwgv:GridViewDataComboBoxColumn Caption="Company" FieldName="CompanyName" VisibleIndex="1"
						SortIndex="3" SortOrder="Ascending" Settings-SortMode="DisplayText">
						<Settings SortMode="DisplayText" />
					</dxwgv:GridViewDataComboBoxColumn>
					<dxwgv:GridViewDataComboBoxColumn Caption="Site" FieldName="SiteName" VisibleIndex="2"
						SortIndex="2" SortOrder="Ascending">
						<Settings SortMode="DisplayText" />
					</dxwgv:GridViewDataComboBoxColumn>
					<dxwgv:GridViewDataDateColumn Caption="KPI Month-Year" FieldName="KpiDateTime" VisibleIndex="3"
						SortIndex="1" SortOrder="Ascending">
					</dxwgv:GridViewDataDateColumn>
					<dxwgv:GridViewDataTextColumn Caption="Error Message" FieldName="ErrorMsg" VisibleIndex="4">
					</dxwgv:GridViewDataTextColumn>
				</Columns>
				<Settings ShowHeaderFilterButton="True" ShowGroupPanel="True" ShowFooter="True" ShowPreview="True"
					ShowVerticalScrollBar="True" VerticalScrollableHeight="237"></Settings>
				<TotalSummary>
					<dxwgv:ASPxSummaryItem DisplayFormat="{0}" FieldName="ipFATI" ShowInColumn="ipFATI"
						ShowInGroupFooterColumn="FA" SummaryType="Sum" />
					<dxwgv:ASPxSummaryItem DisplayFormat="{0}" FieldName="ipLTI" ShowInColumn="ipLTI"
						ShowInGroupFooterColumn="LWD" SummaryType="Sum" />
					<dxwgv:ASPxSummaryItem DisplayFormat="{0}" FieldName="ipMTI" ShowInColumn="ipMTI"
						ShowInGroupFooterColumn="MT" SummaryType="Sum" />
					<dxwgv:ASPxSummaryItem DisplayFormat="{0}" FieldName="ipRDI" ShowInColumn="ipRDI"
						ShowInGroupFooterColumn="RD" SummaryType="Sum" />
					<dxwgv:ASPxSummaryItem DisplayFormat="{0}" FieldName="ipIFE" ShowInColumn="ipIFE"
						ShowInGroupFooterColumn="IFE" SummaryType="Sum" />
					<dxwgv:ASPxSummaryItem DisplayFormat="{0}" FieldName="Total" ShowInColumn="Total"
						ShowInGroupFooterColumn="Absolute Total Variation" SummaryType="Sum" />
				</TotalSummary>
				<GroupSummary>
					<dxwgv:ASPxSummaryItem DisplayFormat="{0}" FieldName="FA" ShowInColumn="FA" ShowInGroupFooterColumn="FA"
						SummaryType="Sum" />
					<dxwgv:ASPxSummaryItem DisplayFormat="{0}" FieldName="ipLTI" ShowInColumn="ipLTI"
						SummaryType="Sum" />
					<dxwgv:ASPxSummaryItem DisplayFormat="{0}" FieldName="ipMTI" ShowInColumn="ipMTI"
						SummaryType="Sum" />
					<dxwgv:ASPxSummaryItem DisplayFormat="{0}" FieldName="ipRDI" ShowInColumn="ipRDI"
						SummaryType="Sum" />
					<dxwgv:ASPxSummaryItem DisplayFormat="{0}" FieldName="ipIFE" ShowInColumn="ipIFE"
						SummaryType="Sum" />
					<dxwgv:ASPxSummaryItem DisplayFormat="{0}" FieldName="Total" ShowInColumn="Total"
						SummaryType="Sum" />
				</GroupSummary>
				<SettingsPager>
					<AllButton Visible="True">
					</AllButton>
				</SettingsPager>
				<ClientSideEvents ContextMenu="function(s, e) {
				if (expanded == true)
				{
					grid.CollapseAll();
					expanded = false;
				}
				else
				{
					grid.ExpandAll();
					expanded = true;
				}
}" />
				<StylesEditors>
					<ProgressBar Height="25px">
					</ProgressBar>
				</StylesEditors>
			</dxwgv:ASPxGridView>
</td>
<tr align="right">
			<td colspan="3" style="height: 35px; text-align: right; text-align:-moz-right; width:100%" align="right">
				<div align="right">
					<uc1:ExportButtons ID="ucExportButtons" runat="server" />
				</div>    
			</td>
		</tr>
</table>
			<data:EhsimsExceptionsCompaniesSitesDataSource ID="EhsimsExceptionsCompaniesSitesDataSource" runat="server" SelectMethod="GetPaged">
			</data:EhsimsExceptionsCompaniesSitesDataSource>