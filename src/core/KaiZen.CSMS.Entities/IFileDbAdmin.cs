﻿using System;
using System.ComponentModel;

namespace KaiZen.CSMS.Entities
{
	/// <summary>
	///		The data structure representation of the 'FileDbAdmin' table via interface.
	/// </summary>
	/// <remarks>
	/// 	This struct is generated by a tool and should never be modified.
	/// </remarks>
	public interface IFileDbAdmin 
	{
		/// <summary>			
		/// FileId : 
		/// </summary>
		/// <remarks>Member of the primary key of the underlying table "FileDbAdmin"</remarks>
		System.Int32 FileId { get; set; }
				
		/// <summary>
		/// keep a copy of the original so it can be used for editable primary keys.
		/// </summary>
		System.Int32 OriginalFileId { get; set; }
			
		
		
		/// <summary>
		/// FileName : 
		/// </summary>
		System.String  FileName  { get; set; }
		
		/// <summary>
		/// Description : 
		/// </summary>
		System.String  Description  { get; set; }
		
		/// <summary>
		/// Content : 
		/// </summary>
		System.Byte[]  Content  { get; set; }
		
		/// <summary>
		/// ModifiedByUserId : 
		/// </summary>
		System.Int32  ModifiedByUserId  { get; set; }
			
		/// <summary>
		/// Creates a new object that is a copy of the current instance.
		/// </summary>
		/// <returns>A new object that is a copy of this instance.</returns>
		System.Object Clone();
		
		#region Data Properties

		#endregion Data Properties

	}
}


