﻿
using System;

namespace KaiZen.CSMS.Entities
{
	/// <summary>
	/// An enum representation of the 'QuestionnaireInitialContactStatus' table. [No description found in the database]
	/// </summary>
	/// <remark>this enumeration contains the items contained in the table QuestionnaireInitialContactStatus</remark>
	[Serializable, Flags]
	public enum QuestionnaireInitialContactStatusList
	{
		/// <summary> 
		/// Supplier Contact Not Verified Yet.
		/// </summary>
		[EnumTextValue(@"Supplier Contact Not Verified Yet.")]
		NotVerified = 1, 

		/// <summary> 
		/// Supplier Contact is Verified.
		/// </summary>
		[EnumTextValue(@"Supplier Contact is Verified.")]
		Verified = 2

	}
}
