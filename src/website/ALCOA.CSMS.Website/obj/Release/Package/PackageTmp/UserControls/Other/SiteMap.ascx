<%@ Control Language="C#" AutoEventWireup="true" Inherits="UserControls_Tiny_SiteMap" Codebehind="SiteMap.ascx.cs" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxNavBar" TagPrefix="dxnb" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxSiteMapControl" TagPrefix="dxsm" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTitleIndex" TagPrefix="dxti" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxMenu" TagPrefix="dxm" %>
<table border="0" cellpadding="2" cellspacing="0" width="729">
    <tr>
        <td class="pageName" colspan="7">
            <span class="title">Site Map</span><br />
            <span class="date">Find Your Way</span>
            <br />
            <img height="1" src="images/grfc_dottedline.gif" width="24" /><br />
        </td>
        <td class="pageName" colspan="1" style="width: 41px">
        </td>
    </tr>
    <tr>
        <td colspan="8" height="110">
            
           <dxm:ASPxMenu ID="ASPxMenu" runat="server" CssFilePath="~/App_Themes/Plastic Blue (Horizontal orientation)/{0}/styles.css" CssPostfix="PlasticBlue" ImageFolder="~/App_Themes/Plastic Blue (Horizontal orientation)/{0}/" ItemSpacing="0px" SeparatorHeight="22px" Width="900px" Cursor="pointer" EnableAnimation="False" EnableViewState="False" GutterWidth="0px" Visible="False">
                <SubMenuStyle GutterWidth="0px" />
                        <RootItemSubMenuOffset FirstItemX="1" LastItemX="1" X="1" />
                        <SeparatorBackgroundImage ImageUrl="~/App_Themes/Plastic Blue (Horizontal orientation)/Web/mSeparator.gif" />
                        <ItemSubMenuOffset FirstItemY="-1" LastItemY="-1" Y="-1" />
                    
                    <BackgroundImage ImageUrl="~/Images/bckgrn_blue.gif" Repeat="RepeatX" />
                        <ItemStyle Cursor="auto" HorizontalAlign="Center" ImageSpacing="0px" VerticalAlign="Middle"
                            Wrap="False" ForeColor="White" Font-Bold="True">
                            <HoverStyle BackColor="#CCDDFF" ForeColor="Black">
                                <Border BorderColor="#003399" BorderStyle="Solid" BorderWidth="1px" />
                            </HoverStyle>
                            <SelectedStyle BackColor="#CCDDFF" ForeColor="Black">
                                <Border BorderColor="#003399" BorderStyle="Double" BorderWidth="1px" />
                            </SelectedStyle>
                            <BackgroundImage Repeat="NoRepeat" />
                        </ItemStyle>
                        <SubMenuItemStyle HorizontalAlign="Left">
                        </SubMenuItemStyle>
                    </dxm:ASPxMenu>
            <dxsm:ASPxSiteMapControl ID="ASPxSiteMapControl1" runat="server" Width="900px">
                <LinkStyle>
                    <Font Size="Small"></Font>
                </LinkStyle>
            </dxsm:ASPxSiteMapControl>
        </td>
    </tr>
</table>
