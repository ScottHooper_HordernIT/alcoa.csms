﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.CompanySiteCategoryProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(CompanySiteCategoryDataSourceDesigner))]
	public class CompanySiteCategoryDataSource : ProviderDataSource<CompanySiteCategory, CompanySiteCategoryKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompanySiteCategoryDataSource class.
		/// </summary>
		public CompanySiteCategoryDataSource() : base(new CompanySiteCategoryService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the CompanySiteCategoryDataSourceView used by the CompanySiteCategoryDataSource.
		/// </summary>
		protected CompanySiteCategoryDataSourceView CompanySiteCategoryView
		{
			get { return ( View as CompanySiteCategoryDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the CompanySiteCategoryDataSource control invokes to retrieve data.
		/// </summary>
		public CompanySiteCategorySelectMethod SelectMethod
		{
			get
			{
				CompanySiteCategorySelectMethod selectMethod = CompanySiteCategorySelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (CompanySiteCategorySelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the CompanySiteCategoryDataSourceView class that is to be
		/// used by the CompanySiteCategoryDataSource.
		/// </summary>
		/// <returns>An instance of the CompanySiteCategoryDataSourceView class.</returns>
		protected override BaseDataSourceView<CompanySiteCategory, CompanySiteCategoryKey> GetNewDataSourceView()
		{
			return new CompanySiteCategoryDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the CompanySiteCategoryDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class CompanySiteCategoryDataSourceView : ProviderDataSourceView<CompanySiteCategory, CompanySiteCategoryKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompanySiteCategoryDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the CompanySiteCategoryDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public CompanySiteCategoryDataSourceView(CompanySiteCategoryDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal CompanySiteCategoryDataSource CompanySiteCategoryOwner
		{
			get { return Owner as CompanySiteCategoryDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal CompanySiteCategorySelectMethod SelectMethod
		{
			get { return CompanySiteCategoryOwner.SelectMethod; }
			set { CompanySiteCategoryOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal CompanySiteCategoryService CompanySiteCategoryProvider
		{
			get { return Provider as CompanySiteCategoryService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<CompanySiteCategory> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<CompanySiteCategory> results = null;
			CompanySiteCategory item;
			count = 0;
			
			System.Int32 _companySiteCategoryId;
			System.String _categoryName;

			switch ( SelectMethod )
			{
				case CompanySiteCategorySelectMethod.Get:
					CompanySiteCategoryKey entityKey  = new CompanySiteCategoryKey();
					entityKey.Load(values);
					item = CompanySiteCategoryProvider.Get(entityKey);
					results = new TList<CompanySiteCategory>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case CompanySiteCategorySelectMethod.GetAll:
                    results = CompanySiteCategoryProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case CompanySiteCategorySelectMethod.GetPaged:
					results = CompanySiteCategoryProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case CompanySiteCategorySelectMethod.Find:
					if ( FilterParameters != null )
						results = CompanySiteCategoryProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = CompanySiteCategoryProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case CompanySiteCategorySelectMethod.GetByCompanySiteCategoryId:
					_companySiteCategoryId = ( values["CompanySiteCategoryId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["CompanySiteCategoryId"], typeof(System.Int32)) : (int)0;
					item = CompanySiteCategoryProvider.GetByCompanySiteCategoryId(_companySiteCategoryId);
					results = new TList<CompanySiteCategory>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				case CompanySiteCategorySelectMethod.GetByCategoryName:
					_categoryName = ( values["CategoryName"] != null ) ? (System.String) EntityUtil.ChangeType(values["CategoryName"], typeof(System.String)) : string.Empty;
					item = CompanySiteCategoryProvider.GetByCategoryName(_categoryName);
					results = new TList<CompanySiteCategory>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// FK
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == CompanySiteCategorySelectMethod.Get || SelectMethod == CompanySiteCategorySelectMethod.GetByCompanySiteCategoryId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				CompanySiteCategory entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					CompanySiteCategoryProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<CompanySiteCategory> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			CompanySiteCategoryProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region CompanySiteCategoryDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the CompanySiteCategoryDataSource class.
	/// </summary>
	public class CompanySiteCategoryDataSourceDesigner : ProviderDataSourceDesigner<CompanySiteCategory, CompanySiteCategoryKey>
	{
		/// <summary>
		/// Initializes a new instance of the CompanySiteCategoryDataSourceDesigner class.
		/// </summary>
		public CompanySiteCategoryDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public CompanySiteCategorySelectMethod SelectMethod
		{
			get { return ((CompanySiteCategoryDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new CompanySiteCategoryDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region CompanySiteCategoryDataSourceActionList

	/// <summary>
	/// Supports the CompanySiteCategoryDataSourceDesigner class.
	/// </summary>
	internal class CompanySiteCategoryDataSourceActionList : DesignerActionList
	{
		private CompanySiteCategoryDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the CompanySiteCategoryDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public CompanySiteCategoryDataSourceActionList(CompanySiteCategoryDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public CompanySiteCategorySelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion CompanySiteCategoryDataSourceActionList
	
	#endregion CompanySiteCategoryDataSourceDesigner
	
	#region CompanySiteCategorySelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the CompanySiteCategoryDataSource.SelectMethod property.
	/// </summary>
	public enum CompanySiteCategorySelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByCompanySiteCategoryId method.
		/// </summary>
		GetByCompanySiteCategoryId,
		/// <summary>
		/// Represents the GetByCategoryName method.
		/// </summary>
		GetByCategoryName
	}
	
	#endregion CompanySiteCategorySelectMethod

	#region CompanySiteCategoryFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CompanySiteCategory"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompanySiteCategoryFilter : SqlFilter<CompanySiteCategoryColumn>
	{
	}
	
	#endregion CompanySiteCategoryFilter

	#region CompanySiteCategoryExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CompanySiteCategory"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompanySiteCategoryExpressionBuilder : SqlExpressionBuilder<CompanySiteCategoryColumn>
	{
	}
	
	#endregion CompanySiteCategoryExpressionBuilder	

	#region CompanySiteCategoryProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;CompanySiteCategoryChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="CompanySiteCategory"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompanySiteCategoryProperty : ChildEntityProperty<CompanySiteCategoryChildEntityTypes>
	{
	}
	
	#endregion CompanySiteCategoryProperty
}

