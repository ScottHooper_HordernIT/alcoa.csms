<%@ Control Language="C#" AutoEventWireup="true"  Inherits="UserControls_SafetyPlans_Question" Codebehind="SafetyPlans_Question.ascx.cs" %>

<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxRoundPanel" TagPrefix="dxrp" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dxp" %>

<%@ Register Assembly="DevExpress.Web.ASPxHtmlEditor.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxHtmlEditor" TagPrefix="dxhe" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.ASPxSpellChecker.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxSpellChecker" TagPrefix="dxwsc" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
Namespace="DevExpress.Web.ASPxUploadControl" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dxpc" %>

<%@ Register assembly="DevExpress.Web.v14.1" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1" namespace="DevExpress.Web.ASPxPanel" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.ASPxHtmlEditor.v14.1" namespace="DevExpress.Web.ASPxHtmlEditor" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.ASPxSpellChecker.v14.1" namespace="DevExpress.Web.ASPxSpellChecker" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1" namespace="DevExpress.Web.ASPxUploadControl" tagprefix="dx" %>


<script type="text/javascript">
    var sec = 0;
    var min = 60;
    function countDown() {
        sec--;
        if (sec == -1) {
            sec = 59; min = min - 1
        }
        else {
            min = min
        }
        if (sec <= 9) {
            sec = "0" + sec
        }
        time = (min <= 9 ? "0" + min : min) + "." + sec + "";
        if (document.getElementById) {
            theTime.innerHTML = time
        }
        SD = window.setTimeout("countDown();", 2000);
        if (min == "00" && sec == "00") {
            sec = "00"; window.clearTimeout(SD)
        }
    }

</script>

 

 <asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="UpdatePanel1">
    <ContentTemplate>
    <table width="100%"><tr><td width="100%" align="right"><span class="timeClassHeader">Time Remaining (to Save):</span></td><td width="100%" align="right"><span id="theTime" class="timeClass"></span></td></tr></table>
        <table style="width: 900px">
            <tr>
                <td style="width: 100px; text-align: center">
                    <span><span style="font-size: 8pt"><span
                        style="color: #000000">
        Question</span> </span></span>
                        <asp:Label ID="lblNo" runat="server" Font-Bold="True" Font-Size="11pt"></asp:Label>
                </td>
                <td style="text-align: center">
                    <table>
                        <tr>
                            <td style="width: 150px;">
                            <div align="center">
                               <dxe:aspxbutton id="btnPrevious" Visible="True" onclick="btnPrevious_Click" 
        runat="server" cssfilepath="~/App_Themes/Office2003Blue/{0}/styles.css" 
        csspostfix="Office2003Blue" text="<< Previous Section (and Save)" width="202px" 
        SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" 
        ForeColor="Navy"></dxe:aspxbutton> </TD><TD width=148>
        <dxe:aspxbutton id="btnHome" onclick="btnHome_Click" runat="server" 
            cssfilepath="~/App_Themes/Office2003Blue/{0}/styles.css" 
            csspostfix="Office2003Blue" text="[ Home (and Save) ]" width="148px" font-bold="True" 
            SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" 
            ForeColor="Navy"></dxe:aspxbutton> </TD><TD style="TEXT-ALIGN: left" width=402>
        <dxe:aspxbutton id="btnNext" onclick="btnNext_Click" runat="server" 
            cssfilepath="~/App_Themes/Office2003Blue/{0}/styles.css" 
            csspostfix="Office2003Blue" text="Next Section (and Save) >>" width="182px" 
            SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" 
            ForeColor="Navy"></dxe:aspxbutton>
            </div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: center;">
                <span style="font-size: 10pt; color: red"><strong>Warning: You must save your work at least every hour (see clock) to ensure data being saved correctly.</strong></span>
                    </td>
            </tr>
        </table>
        

        <asp:Panel ID="Panel1" runat="server" Width="900px">
        <table width="900px">
            <tr>
                <td style="width: 899px">

                    <dxrp:aspxroundpanel id="ASPxRoundPanel1" runat="server" backcolor="#DDECFE" cssfilepath="~/App_Themes/Office2003Blue/{0}/styles.css"
                        csspostfix="Office2003Blue" enabledefaultappearance="False" 
                        headertext="Question/Answer" width="900px" 
                        SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
<TopEdge>
<BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpTopEdge.png" 
        Repeat="RepeatX" HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
</TopEdge>

<BottomRightCorner Height="9px" Width="9px"></BottomRightCorner>

<ContentPaddings Padding="2px" PaddingTop="10px" PaddingBottom="10px"></ContentPaddings>

<NoHeaderTopRightCorner Height="9px" Width="9px"></NoHeaderTopRightCorner>

<HeaderRightEdge>
<BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" 
        Repeat="RepeatX" HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
</HeaderRightEdge>

<Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px"></Border>

<HeaderLeftEdge>
<BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" 
        Repeat="RepeatX" HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
</HeaderLeftEdge>

<HeaderStyle BackColor="#7BA4E0">
<Paddings Padding="0px" PaddingLeft="2px" PaddingRight="2px" PaddingBottom="7px"></Paddings>

<BorderBottom BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px"></BorderBottom>
</HeaderStyle>

<TopRightCorner Height="9px" Width="9px"></TopRightCorner>

<HeaderContent>
<BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" 
        Repeat="RepeatX" HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
</HeaderContent>

<DisabledStyle ForeColor="Gray"></DisabledStyle>

<NoHeaderTopEdge BackColor="#DDECFE"></NoHeaderTopEdge>

<NoHeaderTopLeftCorner Height="9px" Width="9px"></NoHeaderTopLeftCorner>
<PanelCollection>
<dxp:PanelContent runat="server"><TABLE width="100%"><TBODY><TR><TD style="WIDTH: 850px;"><asp:Label runat="server" ForeColor="Maroon" ID="lblQuestion" Font-Names="Tahoma" Font-Size="11pt"></asp:Label></TD><TD style="WIDTH: 50px;"><asp:RadioButtonList runat="server" Font-Bold="True" ForeColor="#000099" Width="50px" ID="radioAnswer"><asp:ListItem Value="1">Yes</asp:ListItem>
<asp:ListItem Value="0">No</asp:ListItem>
</asp:RadioButtonList>
 </TD></TR></TBODY></TABLE></dxp:PanelContent>
</PanelCollection>

<TopLeftCorner Height="9px" Width="9px"></TopLeftCorner>

<BottomLeftCorner Height="9px" Width="9px"></BottomLeftCorner>
</dxrp:aspxroundpanel>













                </td>
            </tr>
            <tr>
                <td style="width: 899px;">
                    <dxrp:aspxroundpanel id="ASPxRoundPanel2" runat="server" backcolor="#DDECFE" cssfilepath="~/App_Themes/Office2003Blue/{0}/styles.css"
                        csspostfix="Office2003Blue" enabledefaultappearance="False" 
                        headertext="Explanation" width="900px" 
                        SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
<TopEdge>
<BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpTopEdge.png" 
        Repeat="RepeatX" HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
</TopEdge>

<BottomRightCorner Height="9px" Width="9px"></BottomRightCorner>

<ContentPaddings Padding="2px" PaddingTop="10px" PaddingBottom="10px"></ContentPaddings>

<NoHeaderTopRightCorner Height="9px" Width="9px"></NoHeaderTopRightCorner>

<HeaderRightEdge>
<BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" 
        Repeat="RepeatX" HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
</HeaderRightEdge>

<Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px"></Border>

<HeaderLeftEdge>
<BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" 
        Repeat="RepeatX" HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
</HeaderLeftEdge>

<HeaderStyle BackColor="#7BA4E0">
<Paddings Padding="0px" PaddingLeft="2px" PaddingRight="2px" PaddingBottom="7px"></Paddings>

<BorderBottom BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px"></BorderBottom>
</HeaderStyle>

<TopRightCorner Height="9px" Width="9px"></TopRightCorner>

<HeaderContent>
<BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" 
        Repeat="RepeatX" HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
</HeaderContent>

<DisabledStyle ForeColor="Gray"></DisabledStyle>

<NoHeaderTopEdge BackColor="#DDECFE"></NoHeaderTopEdge>

<NoHeaderTopLeftCorner Height="9px" Width="9px"></NoHeaderTopLeftCorner>
<PanelCollection>
<dxp:PanelContent runat="server"><asp:Label runat="server" ID="lblText"></asp:Label>
 </dxp:PanelContent>
</PanelCollection>

<TopLeftCorner Height="9px" Width="9px"></TopLeftCorner>

<BottomLeftCorner Height="9px" Width="9px"></BottomLeftCorner>
</dxrp:aspxroundpanel>
                </td>
            </tr>
            <tr>
                <td style="text-align: left; width: 899px;">
                    <dxrp:aspxroundpanel id="ASPxRoundPanel3" runat="server" backcolor="#DDECFE" cssfilepath="~/App_Themes/Office2003Blue/{0}/styles.css"
                        csspostfix="Office2003Blue" enabledefaultappearance="False" 
                        headertext="Comments/Additional Information" width="900px" Font-Italic="False" 
                        Font-Size="11pt" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
<TopEdge>
<BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpTopEdge.png" 
        Repeat="RepeatX" HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
</TopEdge>

<BottomRightCorner Height="9px" Width="9px"></BottomRightCorner>

<ContentPaddings Padding="2px" PaddingTop="10px" PaddingBottom="10px"></ContentPaddings>

<NoHeaderTopRightCorner Height="9px" Width="9px"></NoHeaderTopRightCorner>

<HeaderRightEdge>
<BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" 
        Repeat="RepeatX" HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
</HeaderRightEdge>

<Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px"></Border>

<HeaderLeftEdge>
<BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" 
        Repeat="RepeatX" HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
</HeaderLeftEdge>

<HeaderStyle BackColor="#7BA4E0">
<Paddings Padding="0px" PaddingLeft="2px" PaddingRight="2px" PaddingBottom="7px"></Paddings>

<BorderBottom BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px"></BorderBottom>
</HeaderStyle>

<TopRightCorner Height="9px" Width="9px"></TopRightCorner>

<HeaderContent>
<BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" 
        Repeat="RepeatX" HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
</HeaderContent>

<DisabledStyle ForeColor="Gray"></DisabledStyle>

<NoHeaderTopEdge BackColor="#DDECFE"></NoHeaderTopEdge>

<NoHeaderTopLeftCorner Height="9px" Width="9px"></NoHeaderTopLeftCorner>

<PanelCollection>
<dxp:PanelContent runat="server"><span style="font-size: 10pt">&nbsp;Please copy and paste relevant sections from your Safety Management Plan which supports your answer.<br />

</span><br /><dxhe:ASPxHtmlEditor runat="server" 
        CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
        CssPostfix="Office2003Blue" ID="ASPxHtmlEditor1" Height="250px" Width="880px">
<Styles CssPostfix="Office2003Blue" 
            CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css">
<ViewArea>
<Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px"></Border>
</ViewArea>
</Styles>

<StylesRoundPanel>
<ControlStyle BackColor="#DDECFE">
<Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px"></Border>
</ControlStyle>
</StylesRoundPanel>

<StylesStatusBar>
<ActiveTab BackColor="White"></ActiveTab>
</StylesStatusBar>
<Toolbars>
<dxhe:HtmlEditorToolbar><Items>
<dxhe:ToolbarCutButton></dxhe:ToolbarCutButton>
<dxhe:ToolbarCopyButton></dxhe:ToolbarCopyButton>
<dxhe:ToolbarPasteButton></dxhe:ToolbarPasteButton>
<dxhe:ToolbarUndoButton BeginGroup="True"></dxhe:ToolbarUndoButton>
<dxhe:ToolbarRedoButton></dxhe:ToolbarRedoButton>
<dxhe:ToolbarRemoveFormatButton BeginGroup="True"></dxhe:ToolbarRemoveFormatButton>
<dxhe:ToolbarSuperscriptButton BeginGroup="True"></dxhe:ToolbarSuperscriptButton>
<dxhe:ToolbarSubscriptButton></dxhe:ToolbarSubscriptButton>
<dxhe:ToolbarInsertOrderedListButton BeginGroup="True"></dxhe:ToolbarInsertOrderedListButton>
<dxhe:ToolbarInsertUnorderedListButton></dxhe:ToolbarInsertUnorderedListButton>
<dxhe:ToolbarIndentButton BeginGroup="True"></dxhe:ToolbarIndentButton>
<dxhe:ToolbarOutdentButton></dxhe:ToolbarOutdentButton>
<dxhe:ToolbarInsertLinkDialogButton BeginGroup="True"></dxhe:ToolbarInsertLinkDialogButton>
<dxhe:ToolbarUnlinkButton></dxhe:ToolbarUnlinkButton>
<dxhe:ToolbarCheckSpellingButton BeginGroup="True"></dxhe:ToolbarCheckSpellingButton>
    <dxhe:ToolbarPrintButton>
    </dxhe:ToolbarPrintButton>
</Items>
</dxhe:HtmlEditorToolbar>
<dxhe:HtmlEditorToolbar><Items>
<dxhe:ToolbarParagraphFormattingEdit Width="120px"><Items>
<dxhe:ToolbarListEditItem Text="Normal" Value="p"></dxhe:ToolbarListEditItem>
<dxhe:ToolbarListEditItem Text="Heading  1" Value="h1"></dxhe:ToolbarListEditItem>
<dxhe:ToolbarListEditItem Text="Heading  2" Value="h2"></dxhe:ToolbarListEditItem>
<dxhe:ToolbarListEditItem Text="Heading  3" Value="h3"></dxhe:ToolbarListEditItem>
<dxhe:ToolbarListEditItem Text="Heading  4" Value="h4"></dxhe:ToolbarListEditItem>
<dxhe:ToolbarListEditItem Text="Heading  5" Value="h5"></dxhe:ToolbarListEditItem>
<dxhe:ToolbarListEditItem Text="Heading  6" Value="h6"></dxhe:ToolbarListEditItem>
<dxhe:ToolbarListEditItem Text="Address" Value="address"></dxhe:ToolbarListEditItem>
<dxhe:ToolbarListEditItem Text="Normal (DIV)" Value="div"></dxhe:ToolbarListEditItem>
</Items>
</dxhe:ToolbarParagraphFormattingEdit>
<dxhe:ToolbarFontNameEdit><Items>
<dxhe:ToolbarListEditItem Text="Times New Roman" Value="Times New Roman"></dxhe:ToolbarListEditItem>
<dxhe:ToolbarListEditItem Text="Tahoma" Value="Tahoma"></dxhe:ToolbarListEditItem>
<dxhe:ToolbarListEditItem Text="Verdana" Value="Verdana"></dxhe:ToolbarListEditItem>
<dxhe:ToolbarListEditItem Text="Arial" Value="Arial"></dxhe:ToolbarListEditItem>
<dxhe:ToolbarListEditItem Text="MS Sans Serif" Value="MS Sans Serif"></dxhe:ToolbarListEditItem>
<dxhe:ToolbarListEditItem Text="Courier" Value="Courier"></dxhe:ToolbarListEditItem>
</Items>
</dxhe:ToolbarFontNameEdit>
<dxhe:ToolbarFontSizeEdit><Items>
<dxhe:ToolbarListEditItem Text="1 (8pt)" Value="1"></dxhe:ToolbarListEditItem>
<dxhe:ToolbarListEditItem Text="2 (10pt)" Value="2"></dxhe:ToolbarListEditItem>
<dxhe:ToolbarListEditItem Text="3 (12pt)" Value="3"></dxhe:ToolbarListEditItem>
<dxhe:ToolbarListEditItem Text="4 (14pt)" Value="4"></dxhe:ToolbarListEditItem>
<dxhe:ToolbarListEditItem Text="5 (18pt)" Value="5"></dxhe:ToolbarListEditItem>
<dxhe:ToolbarListEditItem Text="6 (24pt)" Value="6"></dxhe:ToolbarListEditItem>
<dxhe:ToolbarListEditItem Text="7 (36pt)" Value="7"></dxhe:ToolbarListEditItem>
</Items>
</dxhe:ToolbarFontSizeEdit>
<dxhe:ToolbarBoldButton BeginGroup="True"></dxhe:ToolbarBoldButton>
<dxhe:ToolbarItalicButton></dxhe:ToolbarItalicButton>
<dxhe:ToolbarUnderlineButton></dxhe:ToolbarUnderlineButton>
<dxhe:ToolbarStrikethroughButton></dxhe:ToolbarStrikethroughButton>
<dxhe:ToolbarJustifyLeftButton BeginGroup="True"></dxhe:ToolbarJustifyLeftButton>
<dxhe:ToolbarJustifyCenterButton></dxhe:ToolbarJustifyCenterButton>
<dxhe:ToolbarJustifyRightButton></dxhe:ToolbarJustifyRightButton>
<dxhe:ToolbarJustifyFullButton></dxhe:ToolbarJustifyFullButton>
<dxhe:ToolbarBackColorButton BeginGroup="True"></dxhe:ToolbarBackColorButton>
<dxhe:ToolbarFontColorButton></dxhe:ToolbarFontColorButton>
</Items>
</dxhe:HtmlEditorToolbar>
</Toolbars>

<Settings AllowHtmlView="False" AllowPreview="False"></Settings>

<SettingsImageUpload>
<ValidationSettings AllowedFileExtensions=".jpg, .jpeg, .gif, .png"></ValidationSettings>
</SettingsImageUpload>

<SettingsSpellChecker><Dictionaries>
<dxwsc:ASPxSpellCheckerOpenOfficeDictionary GrammarPath="~/Common/en_AU.aff" DictionaryPath="~/Common/en_AU.dic"></dxwsc:ASPxSpellCheckerOpenOfficeDictionary>
</Dictionaries>
</SettingsSpellChecker>

<Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
    <LoadingPanel Url="~/App_Themes/Office2003Blue/HtmlEditor/Loading.gif">
    </LoadingPanel>
        </Images>
 


<PartsRoundPanel>
<TopLeftCorner Height="9px" Width="9px"></TopLeftCorner>

<NoHeaderTopLeftCorner Height="9px" Width="9px"></NoHeaderTopLeftCorner>

<TopRightCorner Height="9px" Width="9px"></TopRightCorner>

<NoHeaderTopRightCorner Height="9px" Width="9px"></NoHeaderTopRightCorner>

<BottomRightCorner Height="9px" Width="9px"></BottomRightCorner>

<BottomLeftCorner Height="9px" Width="9px"></BottomLeftCorner>

<HeaderLeftEdge>
<BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/HtmlEditor/RoundPanel/herpHeader.png" 
        Repeat="RepeatX" HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
</HeaderLeftEdge>

<HeaderContent>
<BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/HtmlEditor/RoundPanel/herpHeader.png" 
        Repeat="RepeatX" HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
</HeaderContent>

<HeaderRightEdge>
<BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/HtmlEditor/RoundPanel/herpHeader.png" 
        Repeat="RepeatX" HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
</HeaderRightEdge>

<NoHeaderTopEdge BackColor="#DDECFE"></NoHeaderTopEdge>

<TopEdge>
<BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/HtmlEditor/RoundPanel/herpTopEdge.png" 
        Repeat="RepeatX" HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
</TopEdge>
</PartsRoundPanel>
</dxhe:ASPxHtmlEditor>
<br />
<span style="font-size: 10pt; padding:15px 0px 10px 0px">
You may provide attachment(s) to support your answer below(optional):</span>
<br />
 
        <dx:ASPxUploadControl ID="f1" runat="server" ShowAddRemoveButtons="true" Width="100%">
                
                <ValidationSettings ShowErrors="true"
                AllowedFileExtensions=".txt, .pdf, .xls, .xlsx, .doc, .docx, .ppt, .pptx, .zip, .jpg, .jpeg, .png"
                GeneralErrorText="File Upload Failed."
                MaxFileSize="16252928" 
                MaxFileSizeErrorText="File size exceeds the maximum allowed size of 15Mb (maybe consider ZIPing the file?)"
                NotAllowedFileExtensionErrorText="File NOT uploaded. Please upload only files of the following file type. All Other file types will not be uploaded: Text (TXT), Excel (XLS, XLSX), Word Document (DOC, DOCX), PowerPoint (PPT, PPTX), Adobe PDF (PDF), ZIP Compressed File (ZIP), Image (JPG/PNG)">
                </ValidationSettings>
                </dx:ASPxUploadControl>
         
         
                   <br />  
                                
       <dx:ASPxGridView Id="grid"
                ClientInstanceName="grid"
                runat="server"
                AutoGenerateColumns="False"
                CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                CssPostfix="Office2003Blue"
                Width="700px"  KeyFieldName="AttachmentId"
                  OnHtmlRowCreated="grid_RowCreated" OnRowDeleting="grid_RowDeleting" >
                  <Settings  ShowColumnHeaders="false"/>
                  <SettingsBehavior ConfirmDelete="true" />
                  <SettingsText ConfirmDelete="Are you sure you wish to delete this file?" />
                  <Border BorderWidth="0" />
                 <Columns>
                    <dx:GridViewDataTextColumn Caption="File Name" FieldName="AttachmentId" VisibleIndex="0">
                         <HeaderStyle HorizontalAlign="left" ></HeaderStyle>                            
                        <CellStyle HorizontalAlign="left"></CellStyle>
                        <DataItemTemplate>
                            <asp:HyperLink ID="FileLink" runat="server" Target="_blank" NavigateUrl="#"></asp:HyperLink>
                        </DataItemTemplate>
                    </dx:GridViewDataTextColumn>                    
                    <dx:GridViewCommandColumn Caption="Action" Visible="true" Width="50px">
                        <DeleteButton Visible="True">
                        </DeleteButton>
                    </dx:GridViewCommandColumn>

                 </Columns>
                 </dx:ASPxGridView>
 <dxpc:ASPxPopupControl ID="ASPxPopupControl2" ClientInstanceName="ASPxPopupControl2"
            runat="server" BackColor="Info" CloseAction="MouseOut" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
            CssPostfix="Office2003Blue" EnableHotTrack="False" ForeColor="Black" Modal="True"
            SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" HeaderText="Warning"
            PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" Width="800px">
            <ContentCollection>
                <dxpc:PopupControlContentControl runat="server">
                    <br />
                    <br />
                    <div style="border-right: black 1px solid; padding-right: 10px; border-top: black 1px solid;
                        padding-left: 10px; padding-bottom: 10px; border-left: black 1px solid; padding-top: 10px;
                        border-bottom: black 1px solid; background-color: #ffffcc; text-align: center">
                        <span style="font-size: 13pt; color: maroon"><strong><span style="font-family: Verdana">
                            <asp:Label ID="lblBox1_PopUp" runat="server" Text="Could not save. Error Occured."></asp:Label></span><br />
                        </strong><span style="color: maroon"><span style="font-size: 10pt; font-family: Verdana">
                            <asp:Label ID="lblBox2_PopUp" runat="server" Text="Your Answer could not be saved as one or more files you intended to upload is incorrectly formatted or sized. Please scroll down and correct this, then try saving again."></asp:Label>
                            <br />
                            <br />
                            <dxe:ASPxButton ID="ASPxButton3" Text="Close" runat="server" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                UseSubmitBehavior="False" Width="150px" Visible="true" AutoPostBack="false">
                                <ClientSideEvents Click="function(s, e) { ASPxPopupControl2.Hide();
}"></ClientSideEvents>
                            </dxe:ASPxButton>
                            <br />
                        </span></span></span>
                    </div>
                    <br />
                </dxpc:PopupControlContentControl>
            </ContentCollection>
            <HeaderStyle>
                <Paddings PaddingRight="6px" />
            </HeaderStyle>
        </dxpc:ASPxPopupControl>
        </dxp:PanelContent>

 </PanelCollection>

       

<TopLeftCorner Height="9px" Width="9px"></TopLeftCorner>

<BottomLeftCorner Height="9px" Width="9px"></BottomLeftCorner>


</dxrp:aspxroundpanel>
                </td>
            </tr>
        </table>
         <br />
        </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnPrevious" />
           
        <asp:PostBackTrigger ControlID="btnHome" />
        <asp:PostBackTrigger ControlID="btnNext" />
        
        </Triggers>
        </asp:UpdatePanel>
        
        
        