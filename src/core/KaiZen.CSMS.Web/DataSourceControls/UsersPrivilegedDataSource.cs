﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.UsersPrivilegedProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(UsersPrivilegedDataSourceDesigner))]
	public class UsersPrivilegedDataSource : ProviderDataSource<UsersPrivileged, UsersPrivilegedKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UsersPrivilegedDataSource class.
		/// </summary>
		public UsersPrivilegedDataSource() : base(new UsersPrivilegedService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the UsersPrivilegedDataSourceView used by the UsersPrivilegedDataSource.
		/// </summary>
		protected UsersPrivilegedDataSourceView UsersPrivilegedView
		{
			get { return ( View as UsersPrivilegedDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the UsersPrivilegedDataSource control invokes to retrieve data.
		/// </summary>
		public UsersPrivilegedSelectMethod SelectMethod
		{
			get
			{
				UsersPrivilegedSelectMethod selectMethod = UsersPrivilegedSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (UsersPrivilegedSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the UsersPrivilegedDataSourceView class that is to be
		/// used by the UsersPrivilegedDataSource.
		/// </summary>
		/// <returns>An instance of the UsersPrivilegedDataSourceView class.</returns>
		protected override BaseDataSourceView<UsersPrivileged, UsersPrivilegedKey> GetNewDataSourceView()
		{
			return new UsersPrivilegedDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the UsersPrivilegedDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class UsersPrivilegedDataSourceView : ProviderDataSourceView<UsersPrivileged, UsersPrivilegedKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UsersPrivilegedDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the UsersPrivilegedDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public UsersPrivilegedDataSourceView(UsersPrivilegedDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal UsersPrivilegedDataSource UsersPrivilegedOwner
		{
			get { return Owner as UsersPrivilegedDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal UsersPrivilegedSelectMethod SelectMethod
		{
			get { return UsersPrivilegedOwner.SelectMethod; }
			set { UsersPrivilegedOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal UsersPrivilegedService UsersPrivilegedProvider
		{
			get { return Provider as UsersPrivilegedService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<UsersPrivileged> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<UsersPrivileged> results = null;
			UsersPrivileged item;
			count = 0;
			
			System.Int32 _usersPrivilegedId;
			System.Int32 _userId;

			switch ( SelectMethod )
			{
				case UsersPrivilegedSelectMethod.Get:
					UsersPrivilegedKey entityKey  = new UsersPrivilegedKey();
					entityKey.Load(values);
					item = UsersPrivilegedProvider.Get(entityKey);
					results = new TList<UsersPrivileged>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case UsersPrivilegedSelectMethod.GetAll:
                    results = UsersPrivilegedProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case UsersPrivilegedSelectMethod.GetPaged:
					results = UsersPrivilegedProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case UsersPrivilegedSelectMethod.Find:
					if ( FilterParameters != null )
						results = UsersPrivilegedProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = UsersPrivilegedProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case UsersPrivilegedSelectMethod.GetByUsersPrivilegedId:
					_usersPrivilegedId = ( values["UsersPrivilegedId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["UsersPrivilegedId"], typeof(System.Int32)) : (int)0;
					item = UsersPrivilegedProvider.GetByUsersPrivilegedId(_usersPrivilegedId);
					results = new TList<UsersPrivileged>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				case UsersPrivilegedSelectMethod.GetByUserId:
					_userId = ( values["UserId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["UserId"], typeof(System.Int32)) : (int)0;
					item = UsersPrivilegedProvider.GetByUserId(_userId);
					results = new TList<UsersPrivileged>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// FK
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == UsersPrivilegedSelectMethod.Get || SelectMethod == UsersPrivilegedSelectMethod.GetByUsersPrivilegedId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				UsersPrivileged entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					UsersPrivilegedProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<UsersPrivileged> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			UsersPrivilegedProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region UsersPrivilegedDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the UsersPrivilegedDataSource class.
	/// </summary>
	public class UsersPrivilegedDataSourceDesigner : ProviderDataSourceDesigner<UsersPrivileged, UsersPrivilegedKey>
	{
		/// <summary>
		/// Initializes a new instance of the UsersPrivilegedDataSourceDesigner class.
		/// </summary>
		public UsersPrivilegedDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public UsersPrivilegedSelectMethod SelectMethod
		{
			get { return ((UsersPrivilegedDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new UsersPrivilegedDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region UsersPrivilegedDataSourceActionList

	/// <summary>
	/// Supports the UsersPrivilegedDataSourceDesigner class.
	/// </summary>
	internal class UsersPrivilegedDataSourceActionList : DesignerActionList
	{
		private UsersPrivilegedDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the UsersPrivilegedDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public UsersPrivilegedDataSourceActionList(UsersPrivilegedDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public UsersPrivilegedSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion UsersPrivilegedDataSourceActionList
	
	#endregion UsersPrivilegedDataSourceDesigner
	
	#region UsersPrivilegedSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the UsersPrivilegedDataSource.SelectMethod property.
	/// </summary>
	public enum UsersPrivilegedSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByUsersPrivilegedId method.
		/// </summary>
		GetByUsersPrivilegedId,
		/// <summary>
		/// Represents the GetByUserId method.
		/// </summary>
		GetByUserId
	}
	
	#endregion UsersPrivilegedSelectMethod

	#region UsersPrivilegedFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="UsersPrivileged"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UsersPrivilegedFilter : SqlFilter<UsersPrivilegedColumn>
	{
	}
	
	#endregion UsersPrivilegedFilter

	#region UsersPrivilegedExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="UsersPrivileged"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UsersPrivilegedExpressionBuilder : SqlExpressionBuilder<UsersPrivilegedColumn>
	{
	}
	
	#endregion UsersPrivilegedExpressionBuilder	

	#region UsersPrivilegedProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;UsersPrivilegedChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="UsersPrivileged"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UsersPrivilegedProperty : ChildEntityProperty<UsersPrivilegedChildEntityTypes>
	{
	}
	
	#endregion UsersPrivilegedProperty
}

