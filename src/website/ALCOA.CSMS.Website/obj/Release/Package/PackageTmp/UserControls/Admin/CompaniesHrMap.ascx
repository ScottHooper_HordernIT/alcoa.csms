﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CompaniesHrMap.ascx.cs" Inherits="ALCOA.CSMS.Website.UserControls.Admin.CompaniesHrMap" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>

<dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" 
    CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
    CssPostfix="Office2003Blue" DataSourceID="CompaniesHrMapDataSource1" 
    KeyFieldName="CompaniesHrMapId">
    <Columns>
        <dx:GridViewCommandColumn VisibleIndex="0">
            <EditButton Visible="True">
            </EditButton>
            <NewButton Visible="True">
            </NewButton>
            <DeleteButton Visible="True">
            </DeleteButton>
        </dx:GridViewCommandColumn>
        <dx:GridViewDataTextColumn FieldName="CompaniesHrMapId" ReadOnly="True" 
            Visible="False" VisibleIndex="1">
            <EditFormSettings Visible="False" />
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataComboBoxColumn
                    Caption="Company Name"
                    FieldName="CompanyId"
                    unboundType="String"  SortIndex="0" Settings-SortMode="DisplayText" SortOrder="Ascending"
                    VisibleIndex="2"
                    Width="400px">
                    <PropertiesComboBox
                        DataSourceID="sqldsCompaniesList"
                        DropDownHeight="150px"
                        TextField="CompanyName" 
                        ValueField="CompanyId" IncrementalFilteringMode="StartsWith"
                        ValueType="System.Int32">
                    </PropertiesComboBox>
                    </dx:GridViewDataComboBoxColumn>
        <dx:GridViewDataTextColumn Caption="Company Name (HR)" 
            FieldName="CompanyNameHr" VisibleIndex="3" Width="400px">
        </dx:GridViewDataTextColumn>
    </Columns>
    <Settings ShowHeaderFilterButton="true" />
    <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
        <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
        </LoadingPanelOnStatusBar>
        <LoadingPanel Url="~/App_Themes/Office2003Blue/GridView/Loading.gif">
        </LoadingPanel>
    </Images>
    <ImagesFilterControl>
        <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
        </LoadingPanel>
    </ImagesFilterControl>
    <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
        CssPostfix="Office2003Blue">
        <Header ImageSpacing="5px" SortingImageSpacing="5px">
        </Header>
        <LoadingPanel ImageSpacing="10px">
        </LoadingPanel>
    </Styles>
    <StylesEditors>
        <ProgressBar Height="25px">
        </ProgressBar>
    </StylesEditors>
</dx:ASPxGridView>
<data:CompaniesHrMapDataSource ID="CompaniesHrMapDataSource1" runat="server">
</data:CompaniesHrMapDataSource>

<asp:SqlDataSource ID="sqldsCompaniesList" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="_Companies_GetCompanyNameList" SelectCommandType="StoredProcedure">
</asp:SqlDataSource>
