﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.ComponentModel;
using System.Web.UI.Design.WebControls;
using System.Web.UI.Design;
using System.Web.UI.WebControls;

namespace KaiZen.CSMS.Web.UI
{
    /// <summary>
    /// A designer class for a strongly typed repeater <c>QuestionnaireReportOverviewSubQuery3Repeater</c>
    /// </summary>
	public class QuestionnaireReportOverviewSubQuery3RepeaterDesigner : System.Web.UI.Design.ControlDesigner
	{
	    /// <summary>
        /// Initializes a new instance of the <see cref="T:QuestionnaireReportOverviewSubQuery3RepeaterDesigner"/> class.
        /// </summary>
		public QuestionnaireReportOverviewSubQuery3RepeaterDesigner()
		{
		}

        /// <summary>
        /// Initializes the control designer and loads the specified component.
        /// </summary>
        /// <param name="component">The control being designed.</param>
		public override void Initialize(IComponent component)
		{
			if (!(component is QuestionnaireReportOverviewSubQuery3Repeater))
			{ 
				throw new ArgumentException("Component is not a QuestionnaireReportOverviewSubQuery3Repeater."); 
			} 
			base.Initialize(component); 
			base.SetViewFlags(ViewFlags.TemplateEditing, true); 
		}


		/// <summary>
		/// Generate HTML for the designer
		/// </summary>
		/// <returns>a string of design time HTML</returns>
		public override string GetDesignTimeHtml()
		{

			// Get the instance this designer applies to
			//
			QuestionnaireReportOverviewSubQuery3Repeater z = (QuestionnaireReportOverviewSubQuery3Repeater)Component;
			z.DataBind();

			return base.GetDesignTimeHtml();
		}
	}

    /// <summary>
    /// A strongly typed repeater control for the <see cref="QuestionnaireReportOverviewSubQuery3Repeater"/> Type.
    /// </summary>
	[Designer(typeof(QuestionnaireReportOverviewSubQuery3RepeaterDesigner))]
	[ParseChildren(true)]
	[ToolboxData("<{0}:QuestionnaireReportOverviewSubQuery3Repeater runat=\"server\"></{0}:QuestionnaireReportOverviewSubQuery3Repeater>")]
	public class QuestionnaireReportOverviewSubQuery3Repeater : CompositeDataBoundControl, System.Web.UI.INamingContainer
	{
	    /// <summary>
        /// Initializes a new instance of the <see cref="T:QuestionnaireReportOverviewSubQuery3Repeater"/> class.
        /// </summary>
		public QuestionnaireReportOverviewSubQuery3Repeater()
		{
		}

		/// <summary>
        /// Gets a <see cref="T:System.Web.UI.ControlCollection"></see> object that represents the child controls for a specified server control in the UI hierarchy.
        /// </summary>
        /// <value></value>
        /// <returns>The collection of child controls for the specified server control.</returns>
		public override ControlCollection Controls
		{
			get
			{
				this.EnsureChildControls();
				return base.Controls;
			}
		}

		private ITemplate m_headerTemplate;
		/// <summary>
        /// Gets or sets the header template.
        /// </summary>
        /// <value>The header template.</value>
		[Browsable(false)]
		[TemplateContainer(typeof(QuestionnaireReportOverviewSubQuery3Item))]
		[PersistenceMode(PersistenceMode.InnerDefaultProperty)]
		public ITemplate HeaderTemplate
		{
			get { return m_headerTemplate; }
			set { m_headerTemplate = value; }
		}

		private ITemplate m_itemTemplate;
		/// <summary>
        /// Gets or sets the item template.
        /// </summary>
        /// <value>The item template.</value>
		[Browsable(false)]
		[TemplateContainer(typeof(QuestionnaireReportOverviewSubQuery3Item))]
		[PersistenceMode(PersistenceMode.InnerDefaultProperty)]
		public ITemplate ItemTemplate
		{
			get { return m_itemTemplate; }
			set { m_itemTemplate = value; }
		}

		private ITemplate m_seperatorTemplate;
		/// <summary>
        /// Gets or sets the Seperator Template
        /// </summary>
        [Browsable(false)]
        [TemplateContainer(typeof(QuestionnaireReportOverviewSubQuery3Item))]
        [PersistenceMode(PersistenceMode.InnerDefaultProperty)]
        public ITemplate SeperatorTemplate
        {
            get { return m_seperatorTemplate; }
            set { m_seperatorTemplate = value; }
        }

		private ITemplate m_altenateItemTemplate;
        /// <summary>
        /// Gets or sets the alternating item template.
        /// </summary>
        /// <value>The alternating item template.</value>
		[Browsable(false)]
		[TemplateContainer(typeof(QuestionnaireReportOverviewSubQuery3Item))]
		[PersistenceMode(PersistenceMode.InnerDefaultProperty)]
		public ITemplate AlternatingItemTemplate
		{
			get { return m_altenateItemTemplate; }
			set { m_altenateItemTemplate = value; }
		}

		private ITemplate m_footerTemplate;
        /// <summary>
        /// Gets or sets the footer template.
        /// </summary>
        /// <value>The footer template.</value>
		[Browsable(false)]
		[TemplateContainer(typeof(QuestionnaireReportOverviewSubQuery3Item))]
		[PersistenceMode(PersistenceMode.InnerDefaultProperty)]
		public ITemplate FooterTemplate
		{
			get { return m_footerTemplate; }
			set { m_footerTemplate = value; }
		}
		
		
		/// <summary>
        /// Overridden and Empty so that span tags are not written
        /// </summary>
        /// <param name="writer"></param>
        public override void RenderBeginTag(HtmlTextWriter writer)
        {
            
        }

        /// <summary>
        /// Overridden and Empty so that span tags are not written
        /// </summary>
        /// <param name="writer"></param>
        public override void RenderEndTag(HtmlTextWriter writer)
        {
                
        }		

//      /// <summary>
//      /// Called by the ASP.NET page framework to notify server controls that use composition-based implementation to create any child controls they contain in preparation for posting back or rendering.
//      /// </summary>
//		protected override void CreateChildControls()
//		{
//			if (ChildControlsCreated)
//			{
//				return;
//			}
//			Controls.Clear();
//
//			if (m_headerTemplate != null)
//			{
//				Control headerItem = new Control();
//				m_headerTemplate.InstantiateIn(headerItem);
//				Controls.Add(headerItem);
//			}
//
//			
//			if (m_footerTemplate != null)
//			{
//				Control footerItem = new Control();
//				m_footerTemplate.InstantiateIn(footerItem);
//				Controls.Add(footerItem);
//			}
//			ChildControlsCreated = true;
//		}
		
		/// <summary>
      /// Called by the ASP.NET page framework to notify server controls that use composition-based implementation to create any child controls they contain in preparation for posting back or rendering.
      /// </summary>
		protected override int CreateChildControls(System.Collections.IEnumerable dataSource, bool dataBinding)
      {
         int pos = 0;

         if (dataBinding)
         {
            //Instantiate the Header template (if exists)
            if (m_headerTemplate != null)
            {
                Control headerItem = new Control();
                m_headerTemplate.InstantiateIn(headerItem);
                Controls.Add(headerItem);
            }
			if (dataSource != null)
			{
				foreach (object o in dataSource)
				{
						KaiZen.CSMS.Entities.QuestionnaireReportOverviewSubQuery3 entity = o as KaiZen.CSMS.Entities.QuestionnaireReportOverviewSubQuery3;
						QuestionnaireReportOverviewSubQuery3Item container = new QuestionnaireReportOverviewSubQuery3Item(entity);
	
						if (m_itemTemplate != null && (pos % 2) == 0)
						{
							m_itemTemplate.InstantiateIn(container);
							
							if (m_seperatorTemplate != null)
							{
								m_seperatorTemplate.InstantiateIn(container);
							}
						}
						else
						{
							if (m_altenateItemTemplate != null)
							{
								m_altenateItemTemplate.InstantiateIn(container);
								if (m_seperatorTemplate != null)
								{
									m_seperatorTemplate.InstantiateIn(container);
								}

							}
							else if (m_itemTemplate != null)
							{
								m_itemTemplate.InstantiateIn(container);
								if (m_seperatorTemplate != null)
								{
									m_seperatorTemplate.InstantiateIn(container);
								}

							}
							else
							{
								// no template !!!
							}
						}
						Controls.Add(container);
						
						container.DataBind();
						
						pos++;
				}
			}            
			//Instantiate the Footer template (if exists)
            if (m_footerTemplate != null)
            {
                Control footerItem = new Control();
                m_footerTemplate.InstantiateIn(footerItem);
                Controls.Add(footerItem);
            }				
		 }
			
			return pos;
		}
		
      /// <summary>
      /// Raises the <see cref="E:System.Web.UI.Control.PreRender"></see> event.
      /// </summary>
      /// <param name="e">An <see cref="T:System.EventArgs"></see> object that contains the event data.</param>
		protected override void OnPreRender(EventArgs e)
		{
			base.DataBind();
		}

		#region Design time
        /// <summary>
        /// Renders at design time.
        /// </summary>
        /// <returns>a  string of the Designed HTML</returns>
		internal string RenderAtDesignTime()
		{			
			return "Designer currently not implemented"; 
		}

		#endregion
	}

    /// <summary>
    /// A wrapper type for the entity
    /// </summary>
	[System.ComponentModel.ToolboxItem(false)]
	public class QuestionnaireReportOverviewSubQuery3Item : System.Web.UI.Control, System.Web.UI.INamingContainer
	{
		private KaiZen.CSMS.Entities.QuestionnaireReportOverviewSubQuery3 _entity;

        /// <summary>
        /// Initializes a new instance of the <see cref="T:QuestionnaireReportOverviewSubQuery3Item"/> class.
        /// </summary>
		public QuestionnaireReportOverviewSubQuery3Item()
			: base()
		{ }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:QuestionnaireReportOverviewSubQuery3Item"/> class.
        /// </summary>
		public QuestionnaireReportOverviewSubQuery3Item(KaiZen.CSMS.Entities.QuestionnaireReportOverviewSubQuery3 entity)
			: base()
		{
			_entity = entity;
		}
		
        /// <summary>
        /// Gets the CompanyId
        /// </summary>
        /// <value>The CompanyId.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32 CompanyId
		{
			get { return _entity.CompanyId; }
		}
        /// <summary>
        /// Gets the KwiSponsorName
        /// </summary>
        /// <value>The KwiSponsorName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String KwiSponsorName
		{
			get { return _entity.KwiSponsorName; }
		}
        /// <summary>
        /// Gets the KwiSponsor
        /// </summary>
        /// <value>The KwiSponsor.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? KwiSponsor
		{
			get { return _entity.KwiSponsor; }
		}
        /// <summary>
        /// Gets the KwiSpaName
        /// </summary>
        /// <value>The KwiSpaName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String KwiSpaName
		{
			get { return _entity.KwiSpaName; }
		}
        /// <summary>
        /// Gets the KwiSpa
        /// </summary>
        /// <value>The KwiSpa.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? KwiSpa
		{
			get { return _entity.KwiSpa; }
		}
        /// <summary>
        /// Gets the PinSponsorName
        /// </summary>
        /// <value>The PinSponsorName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String PinSponsorName
		{
			get { return _entity.PinSponsorName; }
		}
        /// <summary>
        /// Gets the PinSponsor
        /// </summary>
        /// <value>The PinSponsor.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? PinSponsor
		{
			get { return _entity.PinSponsor; }
		}
        /// <summary>
        /// Gets the PinSpaName
        /// </summary>
        /// <value>The PinSpaName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String PinSpaName
		{
			get { return _entity.PinSpaName; }
		}
        /// <summary>
        /// Gets the PinSpa
        /// </summary>
        /// <value>The PinSpa.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? PinSpa
		{
			get { return _entity.PinSpa; }
		}
        /// <summary>
        /// Gets the WgpSponsorName
        /// </summary>
        /// <value>The WgpSponsorName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String WgpSponsorName
		{
			get { return _entity.WgpSponsorName; }
		}
        /// <summary>
        /// Gets the WgpSponsor
        /// </summary>
        /// <value>The WgpSponsor.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? WgpSponsor
		{
			get { return _entity.WgpSponsor; }
		}
        /// <summary>
        /// Gets the WgpSpaName
        /// </summary>
        /// <value>The WgpSpaName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String WgpSpaName
		{
			get { return _entity.WgpSpaName; }
		}
        /// <summary>
        /// Gets the WgpSpa
        /// </summary>
        /// <value>The WgpSpa.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? WgpSpa
		{
			get { return _entity.WgpSpa; }
		}
        /// <summary>
        /// Gets the HunSponsorName
        /// </summary>
        /// <value>The HunSponsorName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String HunSponsorName
		{
			get { return _entity.HunSponsorName; }
		}
        /// <summary>
        /// Gets the HunSponsor
        /// </summary>
        /// <value>The HunSponsor.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? HunSponsor
		{
			get { return _entity.HunSponsor; }
		}
        /// <summary>
        /// Gets the HunSpaName
        /// </summary>
        /// <value>The HunSpaName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String HunSpaName
		{
			get { return _entity.HunSpaName; }
		}
        /// <summary>
        /// Gets the HunSpa
        /// </summary>
        /// <value>The HunSpa.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? HunSpa
		{
			get { return _entity.HunSpa; }
		}
        /// <summary>
        /// Gets the WdlSponsorName
        /// </summary>
        /// <value>The WdlSponsorName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String WdlSponsorName
		{
			get { return _entity.WdlSponsorName; }
		}
        /// <summary>
        /// Gets the WdlSponsor
        /// </summary>
        /// <value>The WdlSponsor.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? WdlSponsor
		{
			get { return _entity.WdlSponsor; }
		}
        /// <summary>
        /// Gets the WdlSpaName
        /// </summary>
        /// <value>The WdlSpaName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String WdlSpaName
		{
			get { return _entity.WdlSpaName; }
		}
        /// <summary>
        /// Gets the WdlSpa
        /// </summary>
        /// <value>The WdlSpa.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? WdlSpa
		{
			get { return _entity.WdlSpa; }
		}
        /// <summary>
        /// Gets the BunSponsorName
        /// </summary>
        /// <value>The BunSponsorName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String BunSponsorName
		{
			get { return _entity.BunSponsorName; }
		}
        /// <summary>
        /// Gets the BunSponsor
        /// </summary>
        /// <value>The BunSponsor.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? BunSponsor
		{
			get { return _entity.BunSponsor; }
		}
        /// <summary>
        /// Gets the BunSpaName
        /// </summary>
        /// <value>The BunSpaName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String BunSpaName
		{
			get { return _entity.BunSpaName; }
		}
        /// <summary>
        /// Gets the BunSpa
        /// </summary>
        /// <value>The BunSpa.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? BunSpa
		{
			get { return _entity.BunSpa; }
		}
        /// <summary>
        /// Gets the FmlSponsorName
        /// </summary>
        /// <value>The FmlSponsorName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String FmlSponsorName
		{
			get { return _entity.FmlSponsorName; }
		}
        /// <summary>
        /// Gets the FmlSponsor
        /// </summary>
        /// <value>The FmlSponsor.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? FmlSponsor
		{
			get { return _entity.FmlSponsor; }
		}
        /// <summary>
        /// Gets the FmlSpaName
        /// </summary>
        /// <value>The FmlSpaName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String FmlSpaName
		{
			get { return _entity.FmlSpaName; }
		}
        /// <summary>
        /// Gets the FmlSpa
        /// </summary>
        /// <value>The FmlSpa.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? FmlSpa
		{
			get { return _entity.FmlSpa; }
		}
        /// <summary>
        /// Gets the BgnSponsorName
        /// </summary>
        /// <value>The BgnSponsorName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String BgnSponsorName
		{
			get { return _entity.BgnSponsorName; }
		}
        /// <summary>
        /// Gets the BgnSponsor
        /// </summary>
        /// <value>The BgnSponsor.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? BgnSponsor
		{
			get { return _entity.BgnSponsor; }
		}
        /// <summary>
        /// Gets the BgnSpaName
        /// </summary>
        /// <value>The BgnSpaName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String BgnSpaName
		{
			get { return _entity.BgnSpaName; }
		}
        /// <summary>
        /// Gets the BgnSpa
        /// </summary>
        /// <value>The BgnSpa.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? BgnSpa
		{
			get { return _entity.BgnSpa; }
		}
        /// <summary>
        /// Gets the CeSponsorName
        /// </summary>
        /// <value>The CeSponsorName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String CeSponsorName
		{
			get { return _entity.CeSponsorName; }
		}
        /// <summary>
        /// Gets the CeSponsor
        /// </summary>
        /// <value>The CeSponsor.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? CeSponsor
		{
			get { return _entity.CeSponsor; }
		}
        /// <summary>
        /// Gets the CeSpaName
        /// </summary>
        /// <value>The CeSpaName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String CeSpaName
		{
			get { return _entity.CeSpaName; }
		}
        /// <summary>
        /// Gets the CeSpa
        /// </summary>
        /// <value>The CeSpa.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? CeSpa
		{
			get { return _entity.CeSpa; }
		}
        /// <summary>
        /// Gets the AngSponsorName
        /// </summary>
        /// <value>The AngSponsorName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String AngSponsorName
		{
			get { return _entity.AngSponsorName; }
		}
        /// <summary>
        /// Gets the AngSponsor
        /// </summary>
        /// <value>The AngSponsor.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? AngSponsor
		{
			get { return _entity.AngSponsor; }
		}
        /// <summary>
        /// Gets the AngSpaName
        /// </summary>
        /// <value>The AngSpaName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String AngSpaName
		{
			get { return _entity.AngSpaName; }
		}
        /// <summary>
        /// Gets the AngSpa
        /// </summary>
        /// <value>The AngSpa.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? AngSpa
		{
			get { return _entity.AngSpa; }
		}
        /// <summary>
        /// Gets the PtlSponsorName
        /// </summary>
        /// <value>The PtlSponsorName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String PtlSponsorName
		{
			get { return _entity.PtlSponsorName; }
		}
        /// <summary>
        /// Gets the PtlSponsor
        /// </summary>
        /// <value>The PtlSponsor.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? PtlSponsor
		{
			get { return _entity.PtlSponsor; }
		}
        /// <summary>
        /// Gets the PtlSpaName
        /// </summary>
        /// <value>The PtlSpaName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String PtlSpaName
		{
			get { return _entity.PtlSpaName; }
		}
        /// <summary>
        /// Gets the PtlSpa
        /// </summary>
        /// <value>The PtlSpa.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? PtlSpa
		{
			get { return _entity.PtlSpa; }
		}
        /// <summary>
        /// Gets the PthSponsorName
        /// </summary>
        /// <value>The PthSponsorName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String PthSponsorName
		{
			get { return _entity.PthSponsorName; }
		}
        /// <summary>
        /// Gets the PthSponsor
        /// </summary>
        /// <value>The PthSponsor.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? PthSponsor
		{
			get { return _entity.PthSponsor; }
		}
        /// <summary>
        /// Gets the PthSpaName
        /// </summary>
        /// <value>The PthSpaName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String PthSpaName
		{
			get { return _entity.PthSpaName; }
		}
        /// <summary>
        /// Gets the PthSpa
        /// </summary>
        /// <value>The PthSpa.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? PthSpa
		{
			get { return _entity.PthSpa; }
		}
        /// <summary>
        /// Gets the PelSponsorName
        /// </summary>
        /// <value>The PelSponsorName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String PelSponsorName
		{
			get { return _entity.PelSponsorName; }
		}
        /// <summary>
        /// Gets the PelSponsor
        /// </summary>
        /// <value>The PelSponsor.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? PelSponsor
		{
			get { return _entity.PelSponsor; }
		}
        /// <summary>
        /// Gets the PelSpaName
        /// </summary>
        /// <value>The PelSpaName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String PelSpaName
		{
			get { return _entity.PelSpaName; }
		}
        /// <summary>
        /// Gets the PelSpa
        /// </summary>
        /// <value>The PelSpa.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? PelSpa
		{
			get { return _entity.PelSpa; }
		}
        /// <summary>
        /// Gets the ArpSponsorName
        /// </summary>
        /// <value>The ArpSponsorName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String ArpSponsorName
		{
			get { return _entity.ArpSponsorName; }
		}
        /// <summary>
        /// Gets the ArpSponsor
        /// </summary>
        /// <value>The ArpSponsor.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? ArpSponsor
		{
			get { return _entity.ArpSponsor; }
		}
        /// <summary>
        /// Gets the ArpSpaName
        /// </summary>
        /// <value>The ArpSpaName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String ArpSpaName
		{
			get { return _entity.ArpSpaName; }
		}
        /// <summary>
        /// Gets the ArpSpa
        /// </summary>
        /// <value>The ArpSpa.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? ArpSpa
		{
			get { return _entity.ArpSpa; }
		}
        /// <summary>
        /// Gets the YenSponsorName
        /// </summary>
        /// <value>The YenSponsorName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String YenSponsorName
		{
			get { return _entity.YenSponsorName; }
		}
        /// <summary>
        /// Gets the YenSponsor
        /// </summary>
        /// <value>The YenSponsor.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? YenSponsor
		{
			get { return _entity.YenSponsor; }
		}
        /// <summary>
        /// Gets the YenSpaName
        /// </summary>
        /// <value>The YenSpaName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String YenSpaName
		{
			get { return _entity.YenSpaName; }
		}
        /// <summary>
        /// Gets the YenSpa
        /// </summary>
        /// <value>The YenSpa.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? YenSpa
		{
			get { return _entity.YenSpa; }
		}

	}
}
