﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="AdminTaskProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class AdminTaskProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.AdminTask, KaiZen.CSMS.Entities.AdminTaskKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.AdminTaskKey key)
		{
			return Delete(transactionManager, key.AdminTaskId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_adminTaskId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _adminTaskId)
		{
			return Delete(null, _adminTaskId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_adminTaskId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _adminTaskId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_AdminTask_AdminTaskTypeId key.
		///		FK_AdminTask_AdminTaskTypeId Description: 
		/// </summary>
		/// <param name="_adminTaskTypeId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.AdminTask objects.</returns>
		public TList<AdminTask> GetByAdminTaskTypeId(System.Int32 _adminTaskTypeId)
		{
			int count = -1;
			return GetByAdminTaskTypeId(_adminTaskTypeId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_AdminTask_AdminTaskTypeId key.
		///		FK_AdminTask_AdminTaskTypeId Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_adminTaskTypeId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.AdminTask objects.</returns>
		/// <remarks></remarks>
		public TList<AdminTask> GetByAdminTaskTypeId(TransactionManager transactionManager, System.Int32 _adminTaskTypeId)
		{
			int count = -1;
			return GetByAdminTaskTypeId(transactionManager, _adminTaskTypeId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_AdminTask_AdminTaskTypeId key.
		///		FK_AdminTask_AdminTaskTypeId Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_adminTaskTypeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.AdminTask objects.</returns>
		public TList<AdminTask> GetByAdminTaskTypeId(TransactionManager transactionManager, System.Int32 _adminTaskTypeId, int start, int pageLength)
		{
			int count = -1;
			return GetByAdminTaskTypeId(transactionManager, _adminTaskTypeId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_AdminTask_AdminTaskTypeId key.
		///		fkAdminTaskAdminTaskTypeId Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_adminTaskTypeId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.AdminTask objects.</returns>
		public TList<AdminTask> GetByAdminTaskTypeId(System.Int32 _adminTaskTypeId, int start, int pageLength)
		{
			int count =  -1;
			return GetByAdminTaskTypeId(null, _adminTaskTypeId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_AdminTask_AdminTaskTypeId key.
		///		fkAdminTaskAdminTaskTypeId Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_adminTaskTypeId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.AdminTask objects.</returns>
		public TList<AdminTask> GetByAdminTaskTypeId(System.Int32 _adminTaskTypeId, int start, int pageLength,out int count)
		{
			return GetByAdminTaskTypeId(null, _adminTaskTypeId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_AdminTask_AdminTaskTypeId key.
		///		FK_AdminTask_AdminTaskTypeId Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_adminTaskTypeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.AdminTask objects.</returns>
		public abstract TList<AdminTask> GetByAdminTaskTypeId(TransactionManager transactionManager, System.Int32 _adminTaskTypeId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_AdminTask_ClosedByUserId key.
		///		FK_AdminTask_ClosedByUserId Description: 
		/// </summary>
		/// <param name="_closedByUserId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.AdminTask objects.</returns>
		public TList<AdminTask> GetByClosedByUserId(System.Int32? _closedByUserId)
		{
			int count = -1;
			return GetByClosedByUserId(_closedByUserId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_AdminTask_ClosedByUserId key.
		///		FK_AdminTask_ClosedByUserId Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_closedByUserId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.AdminTask objects.</returns>
		/// <remarks></remarks>
		public TList<AdminTask> GetByClosedByUserId(TransactionManager transactionManager, System.Int32? _closedByUserId)
		{
			int count = -1;
			return GetByClosedByUserId(transactionManager, _closedByUserId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_AdminTask_ClosedByUserId key.
		///		FK_AdminTask_ClosedByUserId Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_closedByUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.AdminTask objects.</returns>
		public TList<AdminTask> GetByClosedByUserId(TransactionManager transactionManager, System.Int32? _closedByUserId, int start, int pageLength)
		{
			int count = -1;
			return GetByClosedByUserId(transactionManager, _closedByUserId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_AdminTask_ClosedByUserId key.
		///		fkAdminTaskClosedByUserId Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_closedByUserId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.AdminTask objects.</returns>
		public TList<AdminTask> GetByClosedByUserId(System.Int32? _closedByUserId, int start, int pageLength)
		{
			int count =  -1;
			return GetByClosedByUserId(null, _closedByUserId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_AdminTask_ClosedByUserId key.
		///		fkAdminTaskClosedByUserId Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_closedByUserId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.AdminTask objects.</returns>
		public TList<AdminTask> GetByClosedByUserId(System.Int32? _closedByUserId, int start, int pageLength,out int count)
		{
			return GetByClosedByUserId(null, _closedByUserId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_AdminTask_ClosedByUserId key.
		///		FK_AdminTask_ClosedByUserId Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_closedByUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.AdminTask objects.</returns>
		public abstract TList<AdminTask> GetByClosedByUserId(TransactionManager transactionManager, System.Int32? _closedByUserId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_AdminTask_AdminTaskSourceId key.
		///		FK_AdminTask_AdminTaskSourceId Description: 
		/// </summary>
		/// <param name="_adminTaskSourceId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.AdminTask objects.</returns>
		public TList<AdminTask> GetByAdminTaskSourceId(System.Int32 _adminTaskSourceId)
		{
			int count = -1;
			return GetByAdminTaskSourceId(_adminTaskSourceId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_AdminTask_AdminTaskSourceId key.
		///		FK_AdminTask_AdminTaskSourceId Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_adminTaskSourceId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.AdminTask objects.</returns>
		/// <remarks></remarks>
		public TList<AdminTask> GetByAdminTaskSourceId(TransactionManager transactionManager, System.Int32 _adminTaskSourceId)
		{
			int count = -1;
			return GetByAdminTaskSourceId(transactionManager, _adminTaskSourceId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_AdminTask_AdminTaskSourceId key.
		///		FK_AdminTask_AdminTaskSourceId Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_adminTaskSourceId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.AdminTask objects.</returns>
		public TList<AdminTask> GetByAdminTaskSourceId(TransactionManager transactionManager, System.Int32 _adminTaskSourceId, int start, int pageLength)
		{
			int count = -1;
			return GetByAdminTaskSourceId(transactionManager, _adminTaskSourceId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_AdminTask_AdminTaskSourceId key.
		///		fkAdminTaskAdminTaskSourceId Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_adminTaskSourceId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.AdminTask objects.</returns>
		public TList<AdminTask> GetByAdminTaskSourceId(System.Int32 _adminTaskSourceId, int start, int pageLength)
		{
			int count =  -1;
			return GetByAdminTaskSourceId(null, _adminTaskSourceId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_AdminTask_AdminTaskSourceId key.
		///		fkAdminTaskAdminTaskSourceId Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_adminTaskSourceId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.AdminTask objects.</returns>
		public TList<AdminTask> GetByAdminTaskSourceId(System.Int32 _adminTaskSourceId, int start, int pageLength,out int count)
		{
			return GetByAdminTaskSourceId(null, _adminTaskSourceId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_AdminTask_AdminTaskSourceId key.
		///		FK_AdminTask_AdminTaskSourceId Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_adminTaskSourceId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.AdminTask objects.</returns>
		public abstract TList<AdminTask> GetByAdminTaskSourceId(TransactionManager transactionManager, System.Int32 _adminTaskSourceId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_AdminTask_AdminTaskStatusId key.
		///		FK_AdminTask_AdminTaskStatusId Description: 
		/// </summary>
		/// <param name="_adminTaskStatusId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.AdminTask objects.</returns>
		public TList<AdminTask> GetByAdminTaskStatusId(System.Int32 _adminTaskStatusId)
		{
			int count = -1;
			return GetByAdminTaskStatusId(_adminTaskStatusId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_AdminTask_AdminTaskStatusId key.
		///		FK_AdminTask_AdminTaskStatusId Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_adminTaskStatusId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.AdminTask objects.</returns>
		/// <remarks></remarks>
		public TList<AdminTask> GetByAdminTaskStatusId(TransactionManager transactionManager, System.Int32 _adminTaskStatusId)
		{
			int count = -1;
			return GetByAdminTaskStatusId(transactionManager, _adminTaskStatusId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_AdminTask_AdminTaskStatusId key.
		///		FK_AdminTask_AdminTaskStatusId Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_adminTaskStatusId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.AdminTask objects.</returns>
		public TList<AdminTask> GetByAdminTaskStatusId(TransactionManager transactionManager, System.Int32 _adminTaskStatusId, int start, int pageLength)
		{
			int count = -1;
			return GetByAdminTaskStatusId(transactionManager, _adminTaskStatusId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_AdminTask_AdminTaskStatusId key.
		///		fkAdminTaskAdminTaskStatusId Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_adminTaskStatusId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.AdminTask objects.</returns>
		public TList<AdminTask> GetByAdminTaskStatusId(System.Int32 _adminTaskStatusId, int start, int pageLength)
		{
			int count =  -1;
			return GetByAdminTaskStatusId(null, _adminTaskStatusId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_AdminTask_AdminTaskStatusId key.
		///		fkAdminTaskAdminTaskStatusId Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_adminTaskStatusId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.AdminTask objects.</returns>
		public TList<AdminTask> GetByAdminTaskStatusId(System.Int32 _adminTaskStatusId, int start, int pageLength,out int count)
		{
			return GetByAdminTaskStatusId(null, _adminTaskStatusId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_AdminTask_AdminTaskStatusId key.
		///		FK_AdminTask_AdminTaskStatusId Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_adminTaskStatusId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.AdminTask objects.</returns>
		public abstract TList<AdminTask> GetByAdminTaskStatusId(TransactionManager transactionManager, System.Int32 _adminTaskStatusId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_AdminTask_CsmsAccessId key.
		///		FK_AdminTask_CsmsAccessId Description: 
		/// </summary>
		/// <param name="_csmsAccessId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.AdminTask objects.</returns>
		public TList<AdminTask> GetByCsmsAccessId(System.Int32 _csmsAccessId)
		{
			int count = -1;
			return GetByCsmsAccessId(_csmsAccessId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_AdminTask_CsmsAccessId key.
		///		FK_AdminTask_CsmsAccessId Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_csmsAccessId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.AdminTask objects.</returns>
		/// <remarks></remarks>
		public TList<AdminTask> GetByCsmsAccessId(TransactionManager transactionManager, System.Int32 _csmsAccessId)
		{
			int count = -1;
			return GetByCsmsAccessId(transactionManager, _csmsAccessId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_AdminTask_CsmsAccessId key.
		///		FK_AdminTask_CsmsAccessId Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_csmsAccessId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.AdminTask objects.</returns>
		public TList<AdminTask> GetByCsmsAccessId(TransactionManager transactionManager, System.Int32 _csmsAccessId, int start, int pageLength)
		{
			int count = -1;
			return GetByCsmsAccessId(transactionManager, _csmsAccessId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_AdminTask_CsmsAccessId key.
		///		fkAdminTaskCsmsAccessId Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_csmsAccessId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.AdminTask objects.</returns>
		public TList<AdminTask> GetByCsmsAccessId(System.Int32 _csmsAccessId, int start, int pageLength)
		{
			int count =  -1;
			return GetByCsmsAccessId(null, _csmsAccessId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_AdminTask_CsmsAccessId key.
		///		fkAdminTaskCsmsAccessId Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_csmsAccessId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.AdminTask objects.</returns>
		public TList<AdminTask> GetByCsmsAccessId(System.Int32 _csmsAccessId, int start, int pageLength,out int count)
		{
			return GetByCsmsAccessId(null, _csmsAccessId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_AdminTask_CsmsAccessId key.
		///		FK_AdminTask_CsmsAccessId Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_csmsAccessId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.AdminTask objects.</returns>
		public abstract TList<AdminTask> GetByCsmsAccessId(TransactionManager transactionManager, System.Int32 _csmsAccessId, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.AdminTask Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.AdminTaskKey key, int start, int pageLength)
		{
			return GetByAdminTaskId(transactionManager, key.AdminTaskId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_AdminTask index.
		/// </summary>
		/// <param name="_adminTaskId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdminTask"/> class.</returns>
		public KaiZen.CSMS.Entities.AdminTask GetByAdminTaskId(System.Int32 _adminTaskId)
		{
			int count = -1;
			return GetByAdminTaskId(null,_adminTaskId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_AdminTask index.
		/// </summary>
		/// <param name="_adminTaskId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdminTask"/> class.</returns>
		public KaiZen.CSMS.Entities.AdminTask GetByAdminTaskId(System.Int32 _adminTaskId, int start, int pageLength)
		{
			int count = -1;
			return GetByAdminTaskId(null, _adminTaskId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_AdminTask index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_adminTaskId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdminTask"/> class.</returns>
		public KaiZen.CSMS.Entities.AdminTask GetByAdminTaskId(TransactionManager transactionManager, System.Int32 _adminTaskId)
		{
			int count = -1;
			return GetByAdminTaskId(transactionManager, _adminTaskId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_AdminTask index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_adminTaskId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdminTask"/> class.</returns>
		public KaiZen.CSMS.Entities.AdminTask GetByAdminTaskId(TransactionManager transactionManager, System.Int32 _adminTaskId, int start, int pageLength)
		{
			int count = -1;
			return GetByAdminTaskId(transactionManager, _adminTaskId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_AdminTask index.
		/// </summary>
		/// <param name="_adminTaskId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdminTask"/> class.</returns>
		public KaiZen.CSMS.Entities.AdminTask GetByAdminTaskId(System.Int32 _adminTaskId, int start, int pageLength, out int count)
		{
			return GetByAdminTaskId(null, _adminTaskId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_AdminTask index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_adminTaskId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdminTask"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.AdminTask GetByAdminTaskId(TransactionManager transactionManager, System.Int32 _adminTaskId, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_AdminTaskTypeId_Login index.
		/// </summary>
		/// <param name="_adminTaskTypeId"></param>
		/// <param name="_login"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;AdminTask&gt;"/> class.</returns>
		public TList<AdminTask> GetByAdminTaskTypeIdLogin(System.Int32 _adminTaskTypeId, System.String _login)
		{
			int count = -1;
			return GetByAdminTaskTypeIdLogin(null,_adminTaskTypeId, _login, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_AdminTaskTypeId_Login index.
		/// </summary>
		/// <param name="_adminTaskTypeId"></param>
		/// <param name="_login"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;AdminTask&gt;"/> class.</returns>
		public TList<AdminTask> GetByAdminTaskTypeIdLogin(System.Int32 _adminTaskTypeId, System.String _login, int start, int pageLength)
		{
			int count = -1;
			return GetByAdminTaskTypeIdLogin(null, _adminTaskTypeId, _login, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_AdminTaskTypeId_Login index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_adminTaskTypeId"></param>
		/// <param name="_login"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;AdminTask&gt;"/> class.</returns>
		public TList<AdminTask> GetByAdminTaskTypeIdLogin(TransactionManager transactionManager, System.Int32 _adminTaskTypeId, System.String _login)
		{
			int count = -1;
			return GetByAdminTaskTypeIdLogin(transactionManager, _adminTaskTypeId, _login, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_AdminTaskTypeId_Login index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_adminTaskTypeId"></param>
		/// <param name="_login"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;AdminTask&gt;"/> class.</returns>
		public TList<AdminTask> GetByAdminTaskTypeIdLogin(TransactionManager transactionManager, System.Int32 _adminTaskTypeId, System.String _login, int start, int pageLength)
		{
			int count = -1;
			return GetByAdminTaskTypeIdLogin(transactionManager, _adminTaskTypeId, _login, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_AdminTaskTypeId_Login index.
		/// </summary>
		/// <param name="_adminTaskTypeId"></param>
		/// <param name="_login"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;AdminTask&gt;"/> class.</returns>
		public TList<AdminTask> GetByAdminTaskTypeIdLogin(System.Int32 _adminTaskTypeId, System.String _login, int start, int pageLength, out int count)
		{
			return GetByAdminTaskTypeIdLogin(null, _adminTaskTypeId, _login, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_AdminTaskTypeId_Login index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_adminTaskTypeId"></param>
		/// <param name="_login"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;AdminTask&gt;"/> class.</returns>
		public abstract TList<AdminTask> GetByAdminTaskTypeIdLogin(TransactionManager transactionManager, System.Int32 _adminTaskTypeId, System.String _login, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_AdminTaskTypeId_Login_AdminTaskStatusId index.
		/// </summary>
		/// <param name="_adminTaskTypeId"></param>
		/// <param name="_login"></param>
		/// <param name="_adminTaskStatusId"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;AdminTask&gt;"/> class.</returns>
		public TList<AdminTask> GetByAdminTaskTypeIdLoginAdminTaskStatusId(System.Int32 _adminTaskTypeId, System.String _login, System.Int32 _adminTaskStatusId)
		{
			int count = -1;
			return GetByAdminTaskTypeIdLoginAdminTaskStatusId(null,_adminTaskTypeId, _login, _adminTaskStatusId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_AdminTaskTypeId_Login_AdminTaskStatusId index.
		/// </summary>
		/// <param name="_adminTaskTypeId"></param>
		/// <param name="_login"></param>
		/// <param name="_adminTaskStatusId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;AdminTask&gt;"/> class.</returns>
		public TList<AdminTask> GetByAdminTaskTypeIdLoginAdminTaskStatusId(System.Int32 _adminTaskTypeId, System.String _login, System.Int32 _adminTaskStatusId, int start, int pageLength)
		{
			int count = -1;
			return GetByAdminTaskTypeIdLoginAdminTaskStatusId(null, _adminTaskTypeId, _login, _adminTaskStatusId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_AdminTaskTypeId_Login_AdminTaskStatusId index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_adminTaskTypeId"></param>
		/// <param name="_login"></param>
		/// <param name="_adminTaskStatusId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;AdminTask&gt;"/> class.</returns>
		public TList<AdminTask> GetByAdminTaskTypeIdLoginAdminTaskStatusId(TransactionManager transactionManager, System.Int32 _adminTaskTypeId, System.String _login, System.Int32 _adminTaskStatusId)
		{
			int count = -1;
			return GetByAdminTaskTypeIdLoginAdminTaskStatusId(transactionManager, _adminTaskTypeId, _login, _adminTaskStatusId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_AdminTaskTypeId_Login_AdminTaskStatusId index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_adminTaskTypeId"></param>
		/// <param name="_login"></param>
		/// <param name="_adminTaskStatusId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;AdminTask&gt;"/> class.</returns>
		public TList<AdminTask> GetByAdminTaskTypeIdLoginAdminTaskStatusId(TransactionManager transactionManager, System.Int32 _adminTaskTypeId, System.String _login, System.Int32 _adminTaskStatusId, int start, int pageLength)
		{
			int count = -1;
			return GetByAdminTaskTypeIdLoginAdminTaskStatusId(transactionManager, _adminTaskTypeId, _login, _adminTaskStatusId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_AdminTaskTypeId_Login_AdminTaskStatusId index.
		/// </summary>
		/// <param name="_adminTaskTypeId"></param>
		/// <param name="_login"></param>
		/// <param name="_adminTaskStatusId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;AdminTask&gt;"/> class.</returns>
		public TList<AdminTask> GetByAdminTaskTypeIdLoginAdminTaskStatusId(System.Int32 _adminTaskTypeId, System.String _login, System.Int32 _adminTaskStatusId, int start, int pageLength, out int count)
		{
			return GetByAdminTaskTypeIdLoginAdminTaskStatusId(null, _adminTaskTypeId, _login, _adminTaskStatusId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_AdminTaskTypeId_Login_AdminTaskStatusId index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_adminTaskTypeId"></param>
		/// <param name="_login"></param>
		/// <param name="_adminTaskStatusId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;AdminTask&gt;"/> class.</returns>
		public abstract TList<AdminTask> GetByAdminTaskTypeIdLoginAdminTaskStatusId(TransactionManager transactionManager, System.Int32 _adminTaskTypeId, System.String _login, System.Int32 _adminTaskStatusId, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;AdminTask&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;AdminTask&gt;"/></returns>
		public static TList<AdminTask> Fill(IDataReader reader, TList<AdminTask> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.AdminTask c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("AdminTask")
					.Append("|").Append((System.Int32)reader[((int)AdminTaskColumn.AdminTaskId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<AdminTask>(
					key.ToString(), // EntityTrackingKey
					"AdminTask",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.AdminTask();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.AdminTaskId = (System.Int32)reader[((int)AdminTaskColumn.AdminTaskId - 1)];
					c.AdminTaskSourceId = (System.Int32)reader[((int)AdminTaskColumn.AdminTaskSourceId - 1)];
					c.AdminTaskTypeId = (System.Int32)reader[((int)AdminTaskColumn.AdminTaskTypeId - 1)];
					c.AdminTaskStatusId = (System.Int32)reader[((int)AdminTaskColumn.AdminTaskStatusId - 1)];
					c.AdminTaskComments = (reader.IsDBNull(((int)AdminTaskColumn.AdminTaskComments - 1)))?null:(System.String)reader[((int)AdminTaskColumn.AdminTaskComments - 1)];
					c.DateOpened = (System.DateTime)reader[((int)AdminTaskColumn.DateOpened - 1)];
					c.OpenedByUserId = (System.Int32)reader[((int)AdminTaskColumn.OpenedByUserId - 1)];
					c.DateClosed = (reader.IsDBNull(((int)AdminTaskColumn.DateClosed - 1)))?null:(System.DateTime?)reader[((int)AdminTaskColumn.DateClosed - 1)];
					c.ClosedByUserId = (reader.IsDBNull(((int)AdminTaskColumn.ClosedByUserId - 1)))?null:(System.Int32?)reader[((int)AdminTaskColumn.ClosedByUserId - 1)];
					c.CsmsAccessId = (System.Int32)reader[((int)AdminTaskColumn.CsmsAccessId - 1)];
					c.CompanyId = (System.Int32)reader[((int)AdminTaskColumn.CompanyId - 1)];
					c.Login = (System.String)reader[((int)AdminTaskColumn.Login - 1)];
					c.EmailAddress = (System.String)reader[((int)AdminTaskColumn.EmailAddress - 1)];
					c.FirstName = (System.String)reader[((int)AdminTaskColumn.FirstName - 1)];
					c.LastName = (System.String)reader[((int)AdminTaskColumn.LastName - 1)];
					c.JobRole = (System.String)reader[((int)AdminTaskColumn.JobRole - 1)];
					c.JobTitle = (System.String)reader[((int)AdminTaskColumn.JobTitle - 1)];
					c.MobileNo = (reader.IsDBNull(((int)AdminTaskColumn.MobileNo - 1)))?null:(System.String)reader[((int)AdminTaskColumn.MobileNo - 1)];
					c.TelephoneNo = (reader.IsDBNull(((int)AdminTaskColumn.TelephoneNo - 1)))?null:(System.String)reader[((int)AdminTaskColumn.TelephoneNo - 1)];
					c.FaxNo = (reader.IsDBNull(((int)AdminTaskColumn.FaxNo - 1)))?null:(System.String)reader[((int)AdminTaskColumn.FaxNo - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.AdminTask"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.AdminTask"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.AdminTask entity)
		{
			if (!reader.Read()) return;
			
			entity.AdminTaskId = (System.Int32)reader[((int)AdminTaskColumn.AdminTaskId - 1)];
			entity.AdminTaskSourceId = (System.Int32)reader[((int)AdminTaskColumn.AdminTaskSourceId - 1)];
			entity.AdminTaskTypeId = (System.Int32)reader[((int)AdminTaskColumn.AdminTaskTypeId - 1)];
			entity.AdminTaskStatusId = (System.Int32)reader[((int)AdminTaskColumn.AdminTaskStatusId - 1)];
			entity.AdminTaskComments = (reader.IsDBNull(((int)AdminTaskColumn.AdminTaskComments - 1)))?null:(System.String)reader[((int)AdminTaskColumn.AdminTaskComments - 1)];
			entity.DateOpened = (System.DateTime)reader[((int)AdminTaskColumn.DateOpened - 1)];
			entity.OpenedByUserId = (System.Int32)reader[((int)AdminTaskColumn.OpenedByUserId - 1)];
			entity.DateClosed = (reader.IsDBNull(((int)AdminTaskColumn.DateClosed - 1)))?null:(System.DateTime?)reader[((int)AdminTaskColumn.DateClosed - 1)];
			entity.ClosedByUserId = (reader.IsDBNull(((int)AdminTaskColumn.ClosedByUserId - 1)))?null:(System.Int32?)reader[((int)AdminTaskColumn.ClosedByUserId - 1)];
			entity.CsmsAccessId = (System.Int32)reader[((int)AdminTaskColumn.CsmsAccessId - 1)];
			entity.CompanyId = (System.Int32)reader[((int)AdminTaskColumn.CompanyId - 1)];
			entity.Login = (System.String)reader[((int)AdminTaskColumn.Login - 1)];
			entity.EmailAddress = (System.String)reader[((int)AdminTaskColumn.EmailAddress - 1)];
			entity.FirstName = (System.String)reader[((int)AdminTaskColumn.FirstName - 1)];
			entity.LastName = (System.String)reader[((int)AdminTaskColumn.LastName - 1)];
			entity.JobRole = (System.String)reader[((int)AdminTaskColumn.JobRole - 1)];
			entity.JobTitle = (System.String)reader[((int)AdminTaskColumn.JobTitle - 1)];
			entity.MobileNo = (reader.IsDBNull(((int)AdminTaskColumn.MobileNo - 1)))?null:(System.String)reader[((int)AdminTaskColumn.MobileNo - 1)];
			entity.TelephoneNo = (reader.IsDBNull(((int)AdminTaskColumn.TelephoneNo - 1)))?null:(System.String)reader[((int)AdminTaskColumn.TelephoneNo - 1)];
			entity.FaxNo = (reader.IsDBNull(((int)AdminTaskColumn.FaxNo - 1)))?null:(System.String)reader[((int)AdminTaskColumn.FaxNo - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.AdminTask"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.AdminTask"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.AdminTask entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.AdminTaskId = (System.Int32)dataRow["AdminTaskId"];
			entity.AdminTaskSourceId = (System.Int32)dataRow["AdminTaskSourceId"];
			entity.AdminTaskTypeId = (System.Int32)dataRow["AdminTaskTypeId"];
			entity.AdminTaskStatusId = (System.Int32)dataRow["AdminTaskStatusId"];
			entity.AdminTaskComments = Convert.IsDBNull(dataRow["AdminTaskComments"]) ? null : (System.String)dataRow["AdminTaskComments"];
			entity.DateOpened = (System.DateTime)dataRow["DateOpened"];
			entity.OpenedByUserId = (System.Int32)dataRow["OpenedByUserId"];
			entity.DateClosed = Convert.IsDBNull(dataRow["DateClosed"]) ? null : (System.DateTime?)dataRow["DateClosed"];
			entity.ClosedByUserId = Convert.IsDBNull(dataRow["ClosedByUserId"]) ? null : (System.Int32?)dataRow["ClosedByUserId"];
			entity.CsmsAccessId = (System.Int32)dataRow["CsmsAccessId"];
			entity.CompanyId = (System.Int32)dataRow["CompanyId"];
			entity.Login = (System.String)dataRow["Login"];
			entity.EmailAddress = (System.String)dataRow["EmailAddress"];
			entity.FirstName = (System.String)dataRow["FirstName"];
			entity.LastName = (System.String)dataRow["LastName"];
			entity.JobRole = (System.String)dataRow["JobRole"];
			entity.JobTitle = (System.String)dataRow["JobTitle"];
			entity.MobileNo = Convert.IsDBNull(dataRow["MobileNo"]) ? null : (System.String)dataRow["MobileNo"];
			entity.TelephoneNo = Convert.IsDBNull(dataRow["TelephoneNo"]) ? null : (System.String)dataRow["TelephoneNo"];
			entity.FaxNo = Convert.IsDBNull(dataRow["FaxNo"]) ? null : (System.String)dataRow["FaxNo"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.AdminTask"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.AdminTask Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.AdminTask entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region AdminTaskTypeIdSource	
			if (CanDeepLoad(entity, "AdminTaskType|AdminTaskTypeIdSource", deepLoadType, innerList) 
				&& entity.AdminTaskTypeIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.AdminTaskTypeId;
				AdminTaskType tmpEntity = EntityManager.LocateEntity<AdminTaskType>(EntityLocator.ConstructKeyFromPkItems(typeof(AdminTaskType), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.AdminTaskTypeIdSource = tmpEntity;
				else
					entity.AdminTaskTypeIdSource = DataRepository.AdminTaskTypeProvider.GetByAdminTaskTypeId(transactionManager, entity.AdminTaskTypeId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'AdminTaskTypeIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.AdminTaskTypeIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.AdminTaskTypeProvider.DeepLoad(transactionManager, entity.AdminTaskTypeIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion AdminTaskTypeIdSource

			#region ClosedByUserIdSource	
			if (CanDeepLoad(entity, "Users|ClosedByUserIdSource", deepLoadType, innerList) 
				&& entity.ClosedByUserIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.ClosedByUserId ?? (int)0);
				Users tmpEntity = EntityManager.LocateEntity<Users>(EntityLocator.ConstructKeyFromPkItems(typeof(Users), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.ClosedByUserIdSource = tmpEntity;
				else
					entity.ClosedByUserIdSource = DataRepository.UsersProvider.GetByUserId(transactionManager, (entity.ClosedByUserId ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ClosedByUserIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.ClosedByUserIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.UsersProvider.DeepLoad(transactionManager, entity.ClosedByUserIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion ClosedByUserIdSource

			#region AdminTaskSourceIdSource	
			if (CanDeepLoad(entity, "AdminTaskSource|AdminTaskSourceIdSource", deepLoadType, innerList) 
				&& entity.AdminTaskSourceIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.AdminTaskSourceId;
				AdminTaskSource tmpEntity = EntityManager.LocateEntity<AdminTaskSource>(EntityLocator.ConstructKeyFromPkItems(typeof(AdminTaskSource), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.AdminTaskSourceIdSource = tmpEntity;
				else
					entity.AdminTaskSourceIdSource = DataRepository.AdminTaskSourceProvider.GetByAdminTaskSourceId(transactionManager, entity.AdminTaskSourceId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'AdminTaskSourceIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.AdminTaskSourceIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.AdminTaskSourceProvider.DeepLoad(transactionManager, entity.AdminTaskSourceIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion AdminTaskSourceIdSource

			#region AdminTaskStatusIdSource	
			if (CanDeepLoad(entity, "AdminTaskStatus|AdminTaskStatusIdSource", deepLoadType, innerList) 
				&& entity.AdminTaskStatusIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.AdminTaskStatusId;
				AdminTaskStatus tmpEntity = EntityManager.LocateEntity<AdminTaskStatus>(EntityLocator.ConstructKeyFromPkItems(typeof(AdminTaskStatus), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.AdminTaskStatusIdSource = tmpEntity;
				else
					entity.AdminTaskStatusIdSource = DataRepository.AdminTaskStatusProvider.GetByAdminTaskStatusId(transactionManager, entity.AdminTaskStatusId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'AdminTaskStatusIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.AdminTaskStatusIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.AdminTaskStatusProvider.DeepLoad(transactionManager, entity.AdminTaskStatusIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion AdminTaskStatusIdSource

			#region CsmsAccessIdSource	
			if (CanDeepLoad(entity, "CsmsAccess|CsmsAccessIdSource", deepLoadType, innerList) 
				&& entity.CsmsAccessIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.CsmsAccessId;
				CsmsAccess tmpEntity = EntityManager.LocateEntity<CsmsAccess>(EntityLocator.ConstructKeyFromPkItems(typeof(CsmsAccess), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.CsmsAccessIdSource = tmpEntity;
				else
					entity.CsmsAccessIdSource = DataRepository.CsmsAccessProvider.GetByCsmsAccessId(transactionManager, entity.CsmsAccessId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CsmsAccessIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.CsmsAccessIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.CsmsAccessProvider.DeepLoad(transactionManager, entity.CsmsAccessIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion CsmsAccessIdSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetByAdminTaskId methods when available
			
			#region AdminTaskEmailLogCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<AdminTaskEmailLog>|AdminTaskEmailLogCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'AdminTaskEmailLogCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.AdminTaskEmailLogCollection = DataRepository.AdminTaskEmailLogProvider.GetByAdminTaskId(transactionManager, entity.AdminTaskId);

				if (deep && entity.AdminTaskEmailLogCollection.Count > 0)
				{
					deepHandles.Add("AdminTaskEmailLogCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<AdminTaskEmailLog>) DataRepository.AdminTaskEmailLogProvider.DeepLoad,
						new object[] { transactionManager, entity.AdminTaskEmailLogCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.AdminTask object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.AdminTask instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.AdminTask Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.AdminTask entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region AdminTaskTypeIdSource
			if (CanDeepSave(entity, "AdminTaskType|AdminTaskTypeIdSource", deepSaveType, innerList) 
				&& entity.AdminTaskTypeIdSource != null)
			{
				DataRepository.AdminTaskTypeProvider.Save(transactionManager, entity.AdminTaskTypeIdSource);
				entity.AdminTaskTypeId = entity.AdminTaskTypeIdSource.AdminTaskTypeId;
			}
			#endregion 
			
			#region ClosedByUserIdSource
			if (CanDeepSave(entity, "Users|ClosedByUserIdSource", deepSaveType, innerList) 
				&& entity.ClosedByUserIdSource != null)
			{
				DataRepository.UsersProvider.Save(transactionManager, entity.ClosedByUserIdSource);
				entity.ClosedByUserId = entity.ClosedByUserIdSource.UserId;
			}
			#endregion 
			
			#region AdminTaskSourceIdSource
			if (CanDeepSave(entity, "AdminTaskSource|AdminTaskSourceIdSource", deepSaveType, innerList) 
				&& entity.AdminTaskSourceIdSource != null)
			{
				DataRepository.AdminTaskSourceProvider.Save(transactionManager, entity.AdminTaskSourceIdSource);
				entity.AdminTaskSourceId = entity.AdminTaskSourceIdSource.AdminTaskSourceId;
			}
			#endregion 
			
			#region AdminTaskStatusIdSource
			if (CanDeepSave(entity, "AdminTaskStatus|AdminTaskStatusIdSource", deepSaveType, innerList) 
				&& entity.AdminTaskStatusIdSource != null)
			{
				DataRepository.AdminTaskStatusProvider.Save(transactionManager, entity.AdminTaskStatusIdSource);
				entity.AdminTaskStatusId = entity.AdminTaskStatusIdSource.AdminTaskStatusId;
			}
			#endregion 
			
			#region CsmsAccessIdSource
			if (CanDeepSave(entity, "CsmsAccess|CsmsAccessIdSource", deepSaveType, innerList) 
				&& entity.CsmsAccessIdSource != null)
			{
				DataRepository.CsmsAccessProvider.Save(transactionManager, entity.CsmsAccessIdSource);
				entity.CsmsAccessId = entity.CsmsAccessIdSource.CsmsAccessId;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<AdminTaskEmailLog>
				if (CanDeepSave(entity.AdminTaskEmailLogCollection, "List<AdminTaskEmailLog>|AdminTaskEmailLogCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(AdminTaskEmailLog child in entity.AdminTaskEmailLogCollection)
					{
						if(child.AdminTaskIdSource != null)
						{
							child.AdminTaskId = child.AdminTaskIdSource.AdminTaskId;
						}
						else
						{
							child.AdminTaskId = entity.AdminTaskId;
						}

					}

					if (entity.AdminTaskEmailLogCollection.Count > 0 || entity.AdminTaskEmailLogCollection.DeletedItems.Count > 0)
					{
						//DataRepository.AdminTaskEmailLogProvider.Save(transactionManager, entity.AdminTaskEmailLogCollection);
						
						deepHandles.Add("AdminTaskEmailLogCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< AdminTaskEmailLog >) DataRepository.AdminTaskEmailLogProvider.DeepSave,
							new object[] { transactionManager, entity.AdminTaskEmailLogCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region AdminTaskChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.AdminTask</c>
	///</summary>
	public enum AdminTaskChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>AdminTaskType</c> at AdminTaskTypeIdSource
		///</summary>
		[ChildEntityType(typeof(AdminTaskType))]
		AdminTaskType,
			
		///<summary>
		/// Composite Property for <c>Users</c> at ClosedByUserIdSource
		///</summary>
		[ChildEntityType(typeof(Users))]
		Users,
			
		///<summary>
		/// Composite Property for <c>AdminTaskSource</c> at AdminTaskSourceIdSource
		///</summary>
		[ChildEntityType(typeof(AdminTaskSource))]
		AdminTaskSource,
			
		///<summary>
		/// Composite Property for <c>AdminTaskStatus</c> at AdminTaskStatusIdSource
		///</summary>
		[ChildEntityType(typeof(AdminTaskStatus))]
		AdminTaskStatus,
			
		///<summary>
		/// Composite Property for <c>CsmsAccess</c> at CsmsAccessIdSource
		///</summary>
		[ChildEntityType(typeof(CsmsAccess))]
		CsmsAccess,
	
		///<summary>
		/// Collection of <c>AdminTask</c> as OneToMany for AdminTaskEmailLogCollection
		///</summary>
		[ChildEntityType(typeof(TList<AdminTaskEmailLog>))]
		AdminTaskEmailLogCollection,
	}
	
	#endregion AdminTaskChildEntityTypes
	
	#region AdminTaskFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;AdminTaskColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AdminTask"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AdminTaskFilterBuilder : SqlFilterBuilder<AdminTaskColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AdminTaskFilterBuilder class.
		/// </summary>
		public AdminTaskFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the AdminTaskFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AdminTaskFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AdminTaskFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AdminTaskFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AdminTaskFilterBuilder
	
	#region AdminTaskParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;AdminTaskColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AdminTask"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AdminTaskParameterBuilder : ParameterizedSqlFilterBuilder<AdminTaskColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AdminTaskParameterBuilder class.
		/// </summary>
		public AdminTaskParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the AdminTaskParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AdminTaskParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AdminTaskParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AdminTaskParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AdminTaskParameterBuilder
	
	#region AdminTaskSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;AdminTaskColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AdminTask"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class AdminTaskSortBuilder : SqlSortBuilder<AdminTaskColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AdminTaskSqlSortBuilder class.
		/// </summary>
		public AdminTaskSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion AdminTaskSortBuilder
	
} // end namespace
