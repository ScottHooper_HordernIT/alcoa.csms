﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="UserControls_Main_ComplianceCharts2_ComplianceChart2_S_S"
    CodeBehind="ComplianceChart2_S_S.ascx.cs" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.XtraCharts.v14.1.Web, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.XtraCharts.Web" TagPrefix="dxchartsui" %>
<%@ Register Assembly="DevExpress.XtraCharts.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.XtraCharts" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.ASPxGauges.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGauges" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxGauges.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGauges.Gauges" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxGauges.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGauges.Gauges.Linear" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxGauges.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGauges.Gauges.Circular" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxGauges.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGauges.Gauges.State" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxGauges.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGauges.Gauges.Digital" TagPrefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1" namespace="DevExpress.Web.ASPxTabControl" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1" namespace="DevExpress.Web.ASPxClasses" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.XtraCharts.v14.1.Web" namespace="DevExpress.XtraCharts.Web" tagprefix="dxchartsui" %>
<%@ Register assembly="DevExpress.XtraCharts.v14.1" namespace="DevExpress.XtraCharts" tagprefix="cc2" %>
<%@ Register assembly="DevExpress.Web.v14.1" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.ASPxGauges.v14.1" namespace="DevExpress.Web.ASPxGauges" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.ASPxGauges.v14.1" namespace="DevExpress.Web.ASPxGauges.Gauges.State" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.ASPxGauges.v14.1" namespace="DevExpress.Web.ASPxGauges.Gauges" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.ASPxGauges.v14.1" namespace="DevExpress.Web.ASPxGauges.Gauges.Linear" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.ASPxGauges.v14.1" namespace="DevExpress.Web.ASPxGauges.Gauges.Circular" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.ASPxGauges.v14.1" namespace="DevExpress.Web.ASPxGauges.Gauges.Digital" tagprefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<style type="text/css">
    .style1
    {
        height: 13px;
        width: 156px;
    }
    .style2
    {
        height: 13px;
        width: 160px;
    }
    .style3
    {
        height: 13px;
        width: 147px;
    }
    .style4
    {
        height: 13px;
        width: 114px;
    }
    .style5
    {
        height: 13px;
        width: 104px;
    }
    .style6
    {
        height: 13px;
        width: 111px;
    }
</style>
<div style="width: 900px">
    <div style="text-align: center" align="center">
        <dx:ASPxLabel ID="lblResidential" runat="server" ForeColor="Red" Text="">
        </dx:ASPxLabel>
    </div>
    <br />
    <dx:ASPxPageControl ID="ASPxPageControl1" runat="server" ActiveTabIndex="0" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
        CssPostfix="Office2003Blue" Width="100%">
        <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
        </LoadingPanelImage>
        <ContentStyle>
            <Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px" />
<Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px"></Border>
        </ContentStyle>
        <TabPages>
            <dx:TabPage Name="tabComplianceData" Text="Compliance Data">
                <TabStyle Font-Bold="True">
                </TabStyle>
                <ContentCollection>
                    <dx:ContentControl ID="ContentControl1" runat="server">
                        <div style="float: left; text-align: left;">
                            <table>
                                <tr>
                                    <td>
                                        Select TimeFrame:
                                    </td>
                                    <td>
                                        <dx:ASPxComboBox ID="ddlMonth" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                            CssPostfix="Office2003Blue" IncrementalFilteringMode="StartsWith" OnSelectedIndexChanged="ddlMonth_SelectedIndexChanged"
                                            SelectedIndex="0" ValueType="System.Int32" AutoPostBack="true" EnableCallbackMode="true"
                                            Width="90px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                            <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                                            </LoadingPanelImage>
                                            <ButtonStyle Width="13px">
                                            </ButtonStyle>
                                        </dx:ASPxComboBox>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div style="float: right; text-align: right;">
                            <dx:ASPxLabel ID="lblLastViewedAt" runat="server" Text="" ForeColor="GrayText" Font-Size="11px">
                            </dx:ASPxLabel>
                        </div>
                        <table>
                            <tr>
                                <td style="width: 460px; text-align: center; padding-top: 15px;">
                                    <div style="width: 460px; text-align: center; padding: 1px; border: 1px solid black;">
                                            <table>
                                            <tr  style="border-bottom:1px; border-bottom-color:Black">
                                                <td class="style4">
                                                    <strong>LWDFR</strong>
                                                    <dx:ASPxLabel ID="lblLWDFR" runat="server" Text="n/a">
                                                    </dx:ASPxLabel>
                                                    <strong></strong>
                                                </td>
                                                <td class="style5">
                                                    <strong>TRIFR</strong>
                                                    <dx:ASPxLabel ID="lblTRIFR" runat="server" Text="n/a">
                                                    </dx:ASPxLabel>
                                                </td>
                                                <td class="style6">
                                                    <strong>AIFR</strong>
                                                    <dx:ASPxLabel ID="lblAIFR" runat="server" Text="n/a">
                                                    </dx:ASPxLabel>
                                                </td>
                                               
                                                <td style="width: 90px; height: 13px">
                                                    <strong>DART</strong>
                                                    <dx:ASPxLabel ID="lblDART" runat="server" Text="n/a">
                                                    </dx:ASPxLabel>
                                                </td>
                                                
                                            </tr>
                                            </table>
                                            </div>
                                             <div style="width: 460px; text-align: center; padding: 1px; border-bottom: 1px solid black;
                                             border-left: 1px solid black;border-right: 1px solid black">
                                            <table  >
                                            <tr >
                                                <td class="style3">
                                                
                                                    <strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; IFE COUNT</strong>
                                                    <dx:ASPxLabel ID="lblIFECount" runat="server" Text="n/a">
                                                    </dx:ASPxLabel>
                                                </td>
                                                <td class="style2">
                                                    <strong>&nbsp;&nbsp;&nbsp;RN COUNT</strong>
                                                    <dx:ASPxLabel ID="lblRNCount" runat="server" Text="n/a">
                                                    </dx:ASPxLabel>
                                                </td>
                                                <td class="style1">
                                                    <strong>&nbsp;&nbsp;IFE+RN COUNT</strong>
                                                    <dx:ASPxLabel ID="lblIFERNCount" runat="server" Text="n/a">
                                                    </dx:ASPxLabel>
                                                </td>
                                            
                                            </tr>
                                            </table>
                                            </div>
                                             <div style="width: 460px; text-align: center; padding: 1px; border-bottom: 1px solid black;
                                             border-left: 1px solid black;border-right: 1px solid black">
                                            <table >
                                            <tr >
                                            
                                                <td style="width: 160px; height: 13px">
                                                    <strong>IFE: Injury Ratio</strong>
                                                    <dx:ASPxLabel ID="lblIFEInjuryRatio" runat="server" Text="n/a">
                                                    </dx:ASPxLabel>
                                                </td>
                                                <td style="width: 150px; height: 13px">
                                                    <strong>RN: Injury Ratio</strong>
                                                    <dx:ASPxLabel ID="lblRNInjuryRatio" runat="server" Text="n/a">
                                                    </dx:ASPxLabel>
                                                </td>
                                                <td style="width: 190px; height: 13px">
                                                    <strong>IFE+RN: Injury Ratio</strong>
                                                    <dx:ASPxLabel ID="lblIFERNInjuryRatio" runat="server" Text="n/a">
                                                    </dx:ASPxLabel>
                                                </td>

                                            </tr>
                                        </table>
                                    </div>
                                    <br />
                                    <dxchartsui:WebChartControl ID="wcSpider" runat="server" Height="350px" Width="455px"
                                        AppearanceName="Light" PaletteBaseColorNumber="1" PaletteName="Custom Green-Red">
                                        <seriestemplate>
                                            <ViewSerializable>
<cc1:RadarAreaSeriesView HiddenSerializableString="to be serialized" Transparency="0">
                                            </cc1:RadarAreaSeriesView>
</ViewSerializable>
                                            <LabelSerializable>
<cc1:RadarPointSeriesLabel HiddenSerializableString="to be serialized" LineVisible="True">
                                                <FillStyle >
                                                    <OptionsSerializable>
<cc1:SolidFillOptions HiddenSerializableString="to be serialized" />
</OptionsSerializable>
                                                </FillStyle>
                                            </cc1:RadarPointSeriesLabel>
</LabelSerializable>
                                            <PointOptionsSerializable>
<cc1:PointOptions HiddenSerializableString="to be serialized">
                                            </cc1:PointOptions>
</PointOptionsSerializable>
                                            <LegendPointOptionsSerializable>
<cc1:PointOptions HiddenSerializableString="to be serialized">
                                            </cc1:PointOptions>
</LegendPointOptionsSerializable>
                                        </seriestemplate>
<EmptyChartText Text=""></EmptyChartText>

<SmallChartText Text="Increase the chart&#39;s size,
to view its layout.
    "></SmallChartText>

<BorderOptions Color="Black" Visible="False"></BorderOptions>
                                        <diagramserializable>
<cc1:RadarDiagram DrawingStyle="Polygon" RotationDirection="Clockwise">
<AxisX>
<Range ScrollingRange-SideMarginsEnabled="False" scrollingrange-maxvalueserializable="100" scrollingrange-minvalueserializable="0"></Range>

<VisualRange AutoSideMargins="False" SideMarginsValue="0" auto="False" maxvalueserializable="4" minvalueserializable="0"></VisualRange>

<WholeRange AutoSideMargins="False" SideMarginsValue="0" auto="False" maxvalueserializable="100" minvalueserializable="0"></WholeRange>
</AxisX>

                                            <axisy>
<range auto="False" maxvalueserializable="100" minvalueserializable="0" sidemarginsenabled="True" scrollingrange-sidemarginsenabled="False"></range>
<VisualRange Auto="False" AutoSideMargins="False" MinValueSerializable="0" MaxValueSerializable="100" sidemarginsvalue="0"></VisualRange>

<WholeRange Auto="False" AutoSideMargins="False" MinValueSerializable="0" MaxValueSerializable="100" sidemarginsvalue="0"></WholeRange>
</axisy>
                                        </cc1:RadarDiagram>
</diagramserializable>
                                        <fillstyle fillmode="Empty">
                                        </fillstyle>
                                        <borderoptions visible="False" color="Black" />
                                        <legend visible="False"></legend>
                                        <seriesserializable>
                                            <cc1:Series  Name="Series 1" 
                                                >
                                                <ViewSerializable>
<cc1:RadarAreaSeriesView ColorEach="True" HiddenSerializableString="to be serialized" Transparency="0">
                                                </cc1:RadarAreaSeriesView>
</ViewSerializable>
                                                <LabelSerializable>
<cc1:RadarPointSeriesLabel Antialiasing="True" HiddenSerializableString="to be serialized" LineVisible="True"
                                                    Visible="False" ResolveOverlappingMode="JustifyAllAroundPoint">
                                                    <FillStyle >
                                                        <OptionsSerializable>
<cc1:SolidFillOptions HiddenSerializableString="to be serialized" />
</OptionsSerializable>
                                                    </FillStyle>
                                                </cc1:RadarPointSeriesLabel>
</LabelSerializable>
                                                <PointOptionsSerializable>
<cc1:PointOptions HiddenSerializableString="to be serialized">
                                                </cc1:PointOptions>
</PointOptionsSerializable>
                                                <LegendPointOptionsSerializable>
<cc1:PointOptions HiddenSerializableString="to be serialized">
                                                </cc1:PointOptions>
</LegendPointOptionsSerializable>
                                            </cc1:Series>
                                        </seriesserializable>
                                        <palettewrappers>
                                            <dxchartsui:PaletteWrapper Name="Custom Green-Red" ScaleMode="Repeat">
                                                <Palette>
                                                    <cc1:PaletteEntry Color="Lime" Color2="Lime" />
                                                </Palette>
                                            </dxchartsui:PaletteWrapper>
                                        </palettewrappers>
                                    </dxchartsui:WebChartControl>
                                </td>
                                <td style="width: 470px; vertical-align: top">
                                    <dx:ASPxGridView ID="grid" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                        CssPostfix="Office2003Blue" Width="100%" AutoGenerateColumns="false">
                                        <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                            <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
                                            </LoadingPanelOnStatusBar>
                                            <LoadingPanel Url="~/App_Themes/Office2003Blue/GridView/Loading.gif">
                                            </LoadingPanel>
                                        </Images>
                                        <ImagesFilterControl>
                                            <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
                                            </LoadingPanel>
                                        </ImagesFilterControl>
                                        <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue">
                                            <Header ImageSpacing="5px" SortingImageSpacing="5px">
                                            </Header>
                                            <LoadingPanel ImageSpacing="10px">
                                            </LoadingPanel>
                                        </Styles>
                                        <StylesEditors>
                                            <ProgressBar Height="25px">
                                            </ProgressBar>
                                        </StylesEditors>
<SettingsBehavior AllowDragDrop="False" AllowSort="False" AllowGroup="False"></SettingsBehavior>

                                        <SettingsPager PageSize="30">
                                            <AllButton Visible="True"></AllButton>
                                        </SettingsPager>
                                        <SettingsBehavior AllowSort="false" AllowGroup="False" AllowDragDrop="False" />
                                    </dx:ASPxGridView>
                                    <div align="center" style="text-align: center; padding-top: 8px; padding-bottom: 3px;">
                                        <dx:ASPxLabel ID="lblGrid2" runat="server" ForeColor="Red" Font-Bold="true" Text="YTD Averages:">
                                        </dx:ASPxLabel>
                                    </div>
                                    <dx:ASPxGridView ID="grid2" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                        CssPostfix="Office2003Blue" Width="100%" AutoGenerateColumns="false" OnHtmlRowCreated="grid2_HtmlRowCreated">

<Settings ShowFooter="True"></Settings>

                                        <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                            <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
                                            </LoadingPanelOnStatusBar>
                                            <LoadingPanel Url="~/App_Themes/Office2003Blue/GridView/Loading.gif">
                                            </LoadingPanel>
                                        </Images>
                                        <ImagesFilterControl>
                                            <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
                                            </LoadingPanel>
                                        </ImagesFilterControl>
                                        <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue">
                                            <Header ImageSpacing="5px" SortingImageSpacing="5px">
                                            </Header>
                                            <LoadingPanel ImageSpacing="10px">
                                            </LoadingPanel>
                                        </Styles>
                                        <StylesEditors>
                                            <ProgressBar Height="25px">
                                            </ProgressBar>
                                        </StylesEditors>
                                        <Settings ShowFooter="true" />
<SettingsBehavior AllowDragDrop="False" AllowSort="False" AllowGroup="False"></SettingsBehavior>

                                        <SettingsPager PageSize="30">
                                            <AllButton Visible="True"></AllButton>
                                        </SettingsPager>
                                        <SettingsBehavior AllowSort="false" AllowGroup="False" AllowDragDrop="False" />
                                    </dx:ASPxGridView>
                                </td>
                            </tr>
                            <tr>
                                <td style="vertical-align: middle" colspan="2" style="padding-top: 8px;">
                                    <table border="0" cellpadding="0" cellspacing="0" class="dxgvControl_Office2003_Blue"
                                        style="width: 100%; border-collapse: collapse; empty-cells: show;">
                                        <tr>
                                            <td style="width: 30px; text-align: center">
                                                <dx:ASPxGaugeControl ID="gcTL_SQ" runat="server" BackColor="White" Height="25px"
                                                    ImageType="Png" Value="3" Width="25px">
                                                    <Gauges>
                                                        <dx:StateIndicatorGauge Bounds="0, 0, 20, 20" Name="Gauge0">
                                                            <indicators>
                                                                                <dx:StateIndicatorComponent Center="124, 124" Name="stateIndicatorComponent1" 
                                                                                    StateIndex="0">
                                                                                    <states>
                                                                                        <dx:IndicatorStateWeb Name="State1" ShapeType="ElectricLight1" />
                                                                                        <dx:IndicatorStateWeb Name="State2" ShapeType="ElectricLight2" />
                                                                                        <dx:IndicatorStateWeb Name="State3" ShapeType="ElectricLight3" />
                                                                                        <dx:IndicatorStateWeb Name="State4" ShapeType="ElectricLight4" />
                                                                                    </states>
                                                                                </dx:StateIndicatorComponent>
                                                                            </indicators>
                                                        </dx:StateIndicatorGauge>
                                                    </Gauges>

<LayoutPadding All="0" Left="0" Top="0" Right="0" Bottom="0"></LayoutPadding>
                                                </dx:ASPxGaugeControl>
                                            </td>
                                            <td style="text-align: left; width: 350px">
                                                <dx:ASPxLabel ID="lblTL_SQ" runat="server" Text="">
                                                </dx:ASPxLabel>
                                            </td>
                                            <td style="width: 30px; text-align: center">
                                                <dx:ASPxGaugeControl ID="gcTL_SC" runat="server" BackColor="White" Height="25px"
                                                    ImageType="Png" Value="3" Width="25px">
                                                    <Gauges>
                                                        <dx:StateIndicatorGauge Bounds="0, 0, 20, 20" Name="Gauge0">
                                                            <indicators>
                                                                                <dx:StateIndicatorComponent Center="124, 124" Name="stateIndicatorComponent1" 
                                                                                    StateIndex="0">
                                                                                    <states>
                                                                                        <dx:IndicatorStateWeb Name="State1" ShapeType="ElectricLight1" />
                                                                                        <dx:IndicatorStateWeb Name="State2" ShapeType="ElectricLight2" />
                                                                                        <dx:IndicatorStateWeb Name="State3" ShapeType="ElectricLight3" />
                                                                                        <dx:IndicatorStateWeb Name="State4" ShapeType="ElectricLight4" />
                                                                                    </states>
                                                                                </dx:StateIndicatorComponent>
                                                                            </indicators>
                                                        </dx:StateIndicatorGauge>
                                                    </Gauges>

<LayoutPadding All="0" Left="0" Top="0" Right="0" Bottom="0"></LayoutPadding>
                                                </dx:ASPxGaugeControl>
                                            </td>
                                            <td style="text-align: left">
                                                <dx:ASPxLabel ID="lblTL_SC" runat="server" Text="">
                                                </dx:ASPxLabel>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 30px; text-align: center">
                                                <dx:ASPxGaugeControl ID="gcTL_CC" runat="server" BackColor="White" Height="25px"
                                                    ImageType="Png" Value="3" Width="25px">
                                                    <Gauges>
                                                        <dx:StateIndicatorGauge Bounds="0, 0, 20, 20" Name="Gauge0">
                                                            <indicators>
                                                                                <dx:StateIndicatorComponent Center="124, 124" Name="stateIndicatorComponent1" 
                                                                                    StateIndex="0">
                                                                                    <states>
                                                                                        <dx:IndicatorStateWeb Name="State1" ShapeType="ElectricLight1" />
                                                                                        <dx:IndicatorStateWeb Name="State2" ShapeType="ElectricLight2" />
                                                                                        <dx:IndicatorStateWeb Name="State3" ShapeType="ElectricLight3" />
                                                                                        <dx:IndicatorStateWeb Name="State4" ShapeType="ElectricLight4" />
                                                                                    </states>
                                                                                </dx:StateIndicatorComponent>
                                                                            </indicators>
                                                        </dx:StateIndicatorGauge>
                                                    </Gauges>

<LayoutPadding All="0" Left="0" Top="0" Right="0" Bottom="0"></LayoutPadding>
                                                </dx:ASPxGaugeControl>
                                            </td>
                                            <td style="text-align: left">
                                                <dx:ASPxLabel ID="lblTL_CC" runat="server" Text="">
                                                </dx:ASPxLabel>
                                            </td>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <div align="center" style="text-align: center; padding-top: 8px; padding-bottom: 3px;">
                                        <dx:ASPxLabel ID="lblMonthlyE" runat="server" ForeColor="Red" Font-Bold="true" Text="Monthly Progress:">
                                        </dx:ASPxLabel>
                                    </div>
                                    <dx:ASPxGridView ID="gridMonthlyE" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                        CssPostfix="Office2003Blue" Width="100%" AutoGenerateColumns="false" OnHtmlRowCreated="gridMonthlyE_HtmlRowCreated">
                                        <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                            <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
                                            </LoadingPanelOnStatusBar>
                                            <LoadingPanel Url="~/App_Themes/Office2003Blue/GridView/Loading.gif">
                                            </LoadingPanel>
                                        </Images>
                                        <ImagesFilterControl>
                                            <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
                                            </LoadingPanel>
                                        </ImagesFilterControl>
                                        <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue">
                                            <Header ImageSpacing="5px" SortingImageSpacing="5px">
                                            </Header>
                                            <LoadingPanel ImageSpacing="10px">
                                            </LoadingPanel>
                                        </Styles>
                                        <StylesEditors>
                                            <ProgressBar Height="25px">
                                            </ProgressBar>
                                        </StylesEditors>
<SettingsBehavior AllowDragDrop="False" AllowSort="False" AllowGroup="False"></SettingsBehavior>

                                        <SettingsPager PageSize="30">
                                            <AllButton Visible="True"></AllButton>
                                        </SettingsPager>
                                        <SettingsBehavior AllowSort="false" AllowGroup="False" AllowDragDrop="False" />
                                    </dx:ASPxGridView>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <div align="center" style="text-align: center; padding-top: 8px; padding-bottom: 3px;">
                                        <dx:ASPxLabel ID="lblQtr" runat="server" ForeColor="Red" Font-Bold="true" Text="Quarterly Progress:">
                                        </dx:ASPxLabel>
                                    </div>
                                    <dx:ASPxGridView ID="gridQtrly" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                        CssPostfix="Office2003Blue" Width="100%" AutoGenerateColumns="false" OnHtmlRowCreated="gridQtrly_HtmlRowCreated">
                                        <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                            <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
                                            </LoadingPanelOnStatusBar>
                                            <LoadingPanel Url="~/App_Themes/Office2003Blue/GridView/Loading.gif">
                                            </LoadingPanel>
                                        </Images>
                                        <ImagesFilterControl>
                                            <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
                                            </LoadingPanel>
                                        </ImagesFilterControl>
                                        <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue">
                                            <Header ImageSpacing="5px" SortingImageSpacing="5px">
                                            </Header>
                                            <LoadingPanel ImageSpacing="10px">
                                            </LoadingPanel>
                                        </Styles>
                                        <StylesEditors>
                                            <ProgressBar Height="25px">
                                            </ProgressBar>
                                        </StylesEditors>
<SettingsBehavior AllowDragDrop="False" AllowSort="False" AllowGroup="False"></SettingsBehavior>

                                        <SettingsPager PageSize="30">
                                            <AllButton Visible="True"></AllButton>
                                        </SettingsPager>
                                        <SettingsBehavior AllowSort="false" AllowGroup="False" AllowDragDrop="False" />
                                    </dx:ASPxGridView>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <div id="divRawScores" runat="server" visible="false" style="padding-top: 12px">
                                        <table border="0" cellpadding="0" cellspacing="0" class="dxgvControl_Office2003_Blue"
                                            style="width: 100%; border-collapse: collapse; empty-cells: show;">
                                            <tr>
                                                <td style="width: 100%; text-align: center; padding-top: 5px;">
                                                    <div align="center" style="text-align: center; padding-bottom: 3px;">
                                                        <dx:ASPxLabel ID="lblRawScores" runat="server" ForeColor="Red" Font-Bold="true" Text="Raw Scores:">
                                                        </dx:ASPxLabel>
                                                    </div>
                                                    <dx:ASPxGridView ID="gridRawScores" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                        CssPostfix="Office2003Blue" Width="100%" AutoGenerateColumns="false" OnHtmlRowCreated="gridRawScores_HtmlRowCreated">
                                                        <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                                            <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
                                                            </LoadingPanelOnStatusBar>
                                                            <LoadingPanel Url="~/App_Themes/Office2003Blue/GridView/Loading.gif">
                                                            </LoadingPanel>
                                                        </Images>
                                                        <ImagesFilterControl>
                                                            <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
                                                            </LoadingPanel>
                                                        </ImagesFilterControl>
                                                        <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue">
                                                            <Header ImageSpacing="5px" SortingImageSpacing="5px">
                                                            </Header>
                                                            <LoadingPanel ImageSpacing="10px">
                                                            </LoadingPanel>
                                                        </Styles>
                                                        <StylesEditors>
                                                            <ProgressBar Height="25px">
                                                            </ProgressBar>
                                                        </StylesEditors>
<SettingsBehavior AllowDragDrop="False" AllowSort="False" AllowGroup="False"></SettingsBehavior>

                                                        <SettingsPager PageSize="30">
                                                            <AllButton Visible="True"></AllButton>
                                                        </SettingsPager>
                                                        <SettingsBehavior AllowSort="false" AllowGroup="False" AllowDragDrop="False" />
                                                    </dx:ASPxGridView>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </dx:ContentControl>
                </ContentCollection>
            </dx:TabPage>
        </TabPages>
        <LoadingPanelStyle ImageSpacing="6px">
        </LoadingPanelStyle>
    </dx:ASPxPageControl>

       
    
</div>
