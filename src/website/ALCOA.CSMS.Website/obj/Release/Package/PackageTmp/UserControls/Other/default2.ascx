﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="UserControls_Other_default2" Codebehind="default2.ascx.cs" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxNewsControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<%@ Register assembly="DevExpress.Web.ASPxHtmlEditor.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxHtmlEditor" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.ASPxSpellChecker.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxSpellChecker" tagprefix="dx" %>
<%@ Register Src="welcome2.ascx" TagName="welcome" TagPrefix="uc1" %>
<%@ Register Src="nonCompliance2.ascx" TagName="nonCompliance" TagPrefix="uc2" %>


<table align="center" border="0" cellpadding="0" cellspacing="0" width="900">
    <tbody>
        <tr>
            <td style="width: 1px">
                <img height="1" src="Images/spacer.gif" width="0" />
            </td>
            <td style="width: 200px; margin-left: 40px; vertical-align: top">
                <uc1:welcome ID="welcome1" runat="server" />
            </td>
            <td bgcolor="#336699" style="height: 400px" width="1">
            </td>
            <td style="width: 694px; vertical-align: top;">
                <table>
                    <tr>
                        <td>
                            <uc2:nonCompliance ID="nonCompliance1" runat="server" />
                        </td>
                    </tr>
                </table>
            </td>
            <td style="width: 1px">
                <img height="1" src="Images/spacer.gif" width="0" />
            </td>
        </tr>
    </tbody>
</table>