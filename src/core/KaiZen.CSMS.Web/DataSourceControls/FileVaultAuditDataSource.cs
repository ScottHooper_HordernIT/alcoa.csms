﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.FileVaultAuditProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(FileVaultAuditDataSourceDesigner))]
	public class FileVaultAuditDataSource : ProviderDataSource<FileVaultAudit, FileVaultAuditKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FileVaultAuditDataSource class.
		/// </summary>
		public FileVaultAuditDataSource() : base(new FileVaultAuditService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the FileVaultAuditDataSourceView used by the FileVaultAuditDataSource.
		/// </summary>
		protected FileVaultAuditDataSourceView FileVaultAuditView
		{
			get { return ( View as FileVaultAuditDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the FileVaultAuditDataSource control invokes to retrieve data.
		/// </summary>
		public FileVaultAuditSelectMethod SelectMethod
		{
			get
			{
				FileVaultAuditSelectMethod selectMethod = FileVaultAuditSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (FileVaultAuditSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the FileVaultAuditDataSourceView class that is to be
		/// used by the FileVaultAuditDataSource.
		/// </summary>
		/// <returns>An instance of the FileVaultAuditDataSourceView class.</returns>
		protected override BaseDataSourceView<FileVaultAudit, FileVaultAuditKey> GetNewDataSourceView()
		{
			return new FileVaultAuditDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the FileVaultAuditDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class FileVaultAuditDataSourceView : ProviderDataSourceView<FileVaultAudit, FileVaultAuditKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FileVaultAuditDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the FileVaultAuditDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public FileVaultAuditDataSourceView(FileVaultAuditDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal FileVaultAuditDataSource FileVaultAuditOwner
		{
			get { return Owner as FileVaultAuditDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal FileVaultAuditSelectMethod SelectMethod
		{
			get { return FileVaultAuditOwner.SelectMethod; }
			set { FileVaultAuditOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal FileVaultAuditService FileVaultAuditProvider
		{
			get { return Provider as FileVaultAuditService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<FileVaultAudit> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<FileVaultAudit> results = null;
			FileVaultAudit item;
			count = 0;
			
			System.Int32 _fileVaultAuditId;

			switch ( SelectMethod )
			{
				case FileVaultAuditSelectMethod.Get:
					FileVaultAuditKey entityKey  = new FileVaultAuditKey();
					entityKey.Load(values);
					item = FileVaultAuditProvider.Get(entityKey);
					results = new TList<FileVaultAudit>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case FileVaultAuditSelectMethod.GetAll:
                    results = FileVaultAuditProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case FileVaultAuditSelectMethod.GetPaged:
					results = FileVaultAuditProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case FileVaultAuditSelectMethod.Find:
					if ( FilterParameters != null )
						results = FileVaultAuditProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = FileVaultAuditProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case FileVaultAuditSelectMethod.GetByFileVaultAuditId:
					_fileVaultAuditId = ( values["FileVaultAuditId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["FileVaultAuditId"], typeof(System.Int32)) : (int)0;
					item = FileVaultAuditProvider.GetByFileVaultAuditId(_fileVaultAuditId);
					results = new TList<FileVaultAudit>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				// FK
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == FileVaultAuditSelectMethod.Get || SelectMethod == FileVaultAuditSelectMethod.GetByFileVaultAuditId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				FileVaultAudit entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					FileVaultAuditProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<FileVaultAudit> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			FileVaultAuditProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region FileVaultAuditDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the FileVaultAuditDataSource class.
	/// </summary>
	public class FileVaultAuditDataSourceDesigner : ProviderDataSourceDesigner<FileVaultAudit, FileVaultAuditKey>
	{
		/// <summary>
		/// Initializes a new instance of the FileVaultAuditDataSourceDesigner class.
		/// </summary>
		public FileVaultAuditDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public FileVaultAuditSelectMethod SelectMethod
		{
			get { return ((FileVaultAuditDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new FileVaultAuditDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region FileVaultAuditDataSourceActionList

	/// <summary>
	/// Supports the FileVaultAuditDataSourceDesigner class.
	/// </summary>
	internal class FileVaultAuditDataSourceActionList : DesignerActionList
	{
		private FileVaultAuditDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the FileVaultAuditDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public FileVaultAuditDataSourceActionList(FileVaultAuditDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public FileVaultAuditSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion FileVaultAuditDataSourceActionList
	
	#endregion FileVaultAuditDataSourceDesigner
	
	#region FileVaultAuditSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the FileVaultAuditDataSource.SelectMethod property.
	/// </summary>
	public enum FileVaultAuditSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByFileVaultAuditId method.
		/// </summary>
		GetByFileVaultAuditId
	}
	
	#endregion FileVaultAuditSelectMethod

	#region FileVaultAuditFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="FileVaultAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FileVaultAuditFilter : SqlFilter<FileVaultAuditColumn>
	{
	}
	
	#endregion FileVaultAuditFilter

	#region FileVaultAuditExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="FileVaultAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FileVaultAuditExpressionBuilder : SqlExpressionBuilder<FileVaultAuditColumn>
	{
	}
	
	#endregion FileVaultAuditExpressionBuilder	

	#region FileVaultAuditProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;FileVaultAuditChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="FileVaultAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FileVaultAuditProperty : ChildEntityProperty<FileVaultAuditChildEntityTypes>
	{
	}
	
	#endregion FileVaultAuditProperty
}

