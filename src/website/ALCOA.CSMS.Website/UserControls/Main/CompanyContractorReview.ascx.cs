﻿using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;
using DevExpress.XtraPrinting;
using KaiZen.CSMS.Entities;
using Repo.CSMS.DAL.EntityModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using dal = Repo.CSMS.DAL.EntityModels;
using repo = Repo.CSMS.Service.Database;


namespace ALCOA.CSMS.Website.UserControls.Main
{
    public partial class CompanyContractorReview : System.Web.UI.UserControl
    {
        // enums for the values in the database table [dbo].[UpdateReasonType]
        private enum UpdateReasonType
        {
            Insert = 1,
            Refresh = 2,
            Update = 3
        }

        Auth auth = new Auth();
        bool isAdmin;
        repo.ICompanyService companySvc = ALCOA.CSMS.Website.Global.GetInstance<repo.ICompanyService>();
        repo.ILabourClassTypeService labourClassTypeSvc = ALCOA.CSMS.Website.Global.GetInstance<repo.ILabourClassTypeService>();
        repo.View.IvwCompanyContractorReviewDetailService companyContractorReviewDetailSvc = ALCOA.CSMS.Website.Global.GetInstance<repo.View.IvwCompanyContractorReviewDetailService>();
        repo.ICompanyContractorReviewService companyContractorReviewSvc = ALCOA.CSMS.Website.Global.GetInstance<repo.ICompanyContractorReviewService>();

        protected void Page_Load(object sender, EventArgs e)
        {
            Helper._Auth.PageLoad(auth);
            moduleInit(Page.IsPostBack);
        }

        private void moduleInit(bool postBack)
        {
            isAdmin = auth.RoleId == (int)RoleList.Administrator;

            if(!postBack)
            {
                ASPxCompany.Text = auth.CompanyName;

                switch(auth.RoleId)
                {
                case (int)RoleList.Contractor:
                case (int)RoleList.CWKMaintainer:
                    ASPxlblInfo1.Text = String.Format("The following is a list of contractors working for {0}", auth.CompanyName);
                    break;
                case (int)RoleList.Administrator:
                    ASPxlblInfo1.Text = "The following is a list of contractors working for all Companies.";
                    break;

                case (int)RoleList.Reader:
                case (int)RoleList.PreQual:
                    Response.Redirect(String.Format("AccessDenied.aspx?error={0}", Server.UrlEncode("Unauthorized user")));
                    break;

                default:
                    // RoleList.Disabled or No Role
                    Response.Redirect(String.Format("AccessDenied.aspx?error={0}", Server.UrlEncode("Unrecognised user")));
                    break;
                }

                GridViewDataComboBoxColumn comboBoxCol;

                comboBoxCol = grid.Columns["CompanyId"] as GridViewDataComboBoxColumn;
                comboBoxCol.PropertiesComboBox.DataSource = companySvc.GetMany(null, null, o => o.OrderBy(c => c.CompanyName), null);

                comboBoxCol = grid.Columns["LabourClassTypeId"] as GridViewDataComboBoxColumn;
                List<LabourClassType> lctLst = labourClassTypeSvc.GetMany(null, null, o => o.OrderBy(l => l.LabourClassTypeId), null);
                foreach(LabourClassType lct in lctLst)
                {
                    if(!lct.LabourClassTypeIsActive)
                    {
                        lct.LabourClassTypeDesc = "--[" + lct.LabourClassTypeDesc + "]--";
                    }
                }
                comboBoxCol.PropertiesComboBox.DataSource = lctLst;

                grid.Columns["CompanyId"].Visible = isAdmin;
                RetrieveRefreshBtn.Enabled = RetrieveRefreshBtn.Visible = !isAdmin;
                UpdateProcessedBtn.Enabled = UpdateProcessedBtn.Visible = isAdmin;

                // Export
                String exportFileName = @"ALCOA CSMS - Company Contractor Review";
                String userAgent = Request.Headers.Get("User-Agent");                                                       //Chrome & IE is fine.
                if ((userAgent.Contains("Mozilla") && !userAgent.Contains("AppleWebKit") && !userAgent.Contains("MSIE")) || //Mozilla
                    (userAgent.Contains("AppleWebKit") && !userAgent.Contains("Chrome")))                                   //Safari
                {
                    exportFileName = exportFileName.Replace(" ", "_");
                }
                Helper.ExportGrid.Settings(ucExportButtons, exportFileName);
            }
            else
            {
            }
            LoadGridData();
        }

        // TODO !####! Improve performance as per below:
        // 1. Look at implementing my own paging logic to optimise on loading amount and time
        //     - during paging we need to detect if it is a paging page load
        //     - what other non-postback loads require databind
        // 2. See if we can avoid the double binding when a edit update is done
        //     - look at option to store data in session variable until it changes
        //     - look at DevExpress EnableRowCache option
        //     - look at DevExpress OnDataBinding handler
        private void LoadGridData()
        {
            Expression<Func<vwCompanyContractorReviewDetail, bool>> where = null;
            Func<IQueryable<vwCompanyContractorReviewDetail>, IOrderedQueryable<vwCompanyContractorReviewDetail>> orderBy = null;

            if(isAdmin)
            {
                orderBy = o => o.OrderBy(c => c.CompanyName).ThenBy(n => n.FullName);
            }
            else
            {
                // NOTE there should not really be a case where Changed and Processed are both true but if it does happen
                // then it will be treated as processed with no pending changes
                int? changedCnt = companyContractorReviewSvc.GetCount(c => c.CompanyId == auth.CompanyId && c.Changed == true && c.Processed == false, null);
                if(changedCnt.HasValue && changedCnt > 0)
                {
                    RetrieveRefreshBtn.ClientSideEvents.Click = "function(s, e) { e.processOnServer = confirm('Existing data may be overwritten, are you sure you want to proceed?'); }";
                }

                where = c => c.CompanyId == auth.CompanyId;
                orderBy = o => o.OrderBy(n => n.FullName);
            }
            grid.DataSource = companyContractorReviewDetailSvc.GetMany(null, where, orderBy, null);
            grid.DataBind();
        }

        protected void grid_AutoFilterCellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
        {
            if(e.Column.FieldName == "CompanyId" || e.Column.FieldName == "LabourClassTypeId")
            {
                ASPxComboBox comboBox = e.Editor as ASPxComboBox;
                comboBox.ClientSideEvents.Init = "function(s, e) { s.InsertItem(0, '(ALL)', ''); }";
            }
        }

        protected void grid_CellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
        {
            if(!grid.IsNewRowEditing)
            {
                if(e.Column.FieldName == "LabourClassTypeId")
                {
                    ASPxComboBox comboBox = e.Editor as ASPxComboBox;
                    int curVal = (int)e.Editor.Value;
                    bool addCurInactive = false;
                    
                    // remove "Unknown" and InActive classifications
                    List<LabourClassType> lctLst = labourClassTypeSvc.GetMany(null, null, o => o.OrderBy(l => l.LabourClassTypeId), null);
                    for(int idx = comboBox.Items.Count - 1; idx >= 0; idx--)
                    {
                        ListEditItem cbItem = comboBox.Items[idx];
                        int val = (int)cbItem.Value;
                        LabourClassType lct = lctLst.FirstOrDefault(lc => lc.LabourClassTypeId == val);
                        if(val == 0)
                        {
                            comboBox.Items.Remove(cbItem);
                        }
                        else if(!lct.LabourClassTypeIsActive)
                        {
                            if(val == curVal)
                                addCurInactive = true;
                            comboBox.Items.Remove(cbItem);
                        }
                    }
                    
                    // add this inactive classification with no text
                    if(addCurInactive)
                    {
                        ListEditItem cbItem = new ListEditItem("", curVal);
                        comboBox.Items.Insert(0, cbItem);
                    }
                }
            }
        }

        protected void grid_CommandButtonInitialize(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewCommandButtonEventArgs e)
        {
            // dont show check box for non-admin
            if(!isAdmin)
            {
                switch(e.ButtonType)
                {
                case ColumnCommandButtonType.SelectCheckbox:
                    e.Visible = false;
                    break;
                }
            }
        }

        protected void HdrCheckBox_Init(object sender, EventArgs e)
        {
            // dont show header check box for non-admin
            if(!isAdmin)
            {
                HtmlInputCheckBox cbx = (HtmlInputCheckBox)sender;
                cbx.Visible = false;
                cbx.Disabled = true;
            }
        }

        private bool GetLabourClassTypeIsActive(int labourClassTypeId)
        {
            LabourClassType lct = labourClassTypeSvc.Get(lc => lc.LabourClassTypeId == labourClassTypeId, null);
            // this should never happen, but...
            if(lct == null)
                throw new Exception("CompanyContractorReview.GetLabourClassTypeIsActive NULL LabourClassType returned");
            return lct.LabourClassTypeIsActive;
        }

        protected void grid_CustomColumnDisplayText(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewColumnDisplayTextEventArgs e)
        {
            if(e.Column.FieldName == "LabourClassTypeId")
            {
                if(!GetLabourClassTypeIsActive((int)e.GetFieldValue("LabourClassTypeId")))
                {
                    e.DisplayText = "";
                }
            }
        }

        protected void grid_RowValidating(object sender, DevExpress.Web.Data.ASPxDataValidationEventArgs e)
        {
            if((bool)e.NewValues["ActiveAtAlcoa"] == true)
            {
                bool alcoaRecordActive = (bool)grid.GetRowValues(e.VisibleIndex, "AlcoaRecordActive");
                bool activeInduction = (bool)grid.GetRowValues(e.VisibleIndex, "ActiveInduction");

                if(!alcoaRecordActive && !activeInduction)
                {
                    e.Errors[grid.Columns["ActiveAtAlcoa"]] = "Cannot be active at Alcoa without an active induction";
                }
            }
            
            if(!GetLabourClassTypeIsActive((int)e.NewValues["LabourClassTypeId"]))
            {
                e.Errors[grid.Columns["LabourClassTypeId"]] = "Must set Labour Classification to an active classification";
            }
        }

        protected void grid_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
        {
            string companyAbn = e.Keys["CompanyAbn"].ToString();
            decimal cWKNumber = (decimal)e.Keys["CWKNumber"];

            dal.CompanyContractorReview ccr = companyContractorReviewSvc.Get(r => r.CompanyAbn == companyAbn && r.CWKNumber == cWKNumber, null);
            // this should never happen, but...
            if(ccr == null)
                throw new Exception("CompanyContractorReview.grid_RowUpdating NULL CompanyContractorReview returned");

            ccr.LabourClassTypeId = (int)e.NewValues["LabourClassTypeId"];
            ccr.StillAtCompany = (bool)e.NewValues["StillAtCompany"];
            ccr.ActiveAtAlcoa = (bool)e.NewValues["ActiveAtAlcoa"];
            ccr.Comments = e.NewValues["Comments"] == null ? null : e.NewValues["Comments"].ToString();

            ccr.Changed = true;
            ccr.Processed = false;
            ccr.ChangedByUserId = ccr.LastUpdateByUserId = auth.UserId;
            ccr.ChangedDate = ccr.LastUpdateDate = DateTime.Now;
            ccr.LastUpdateReasonTypeId = (int)UpdateReasonType.Update;

            companyContractorReviewSvc.Update(ccr, true);

            e.Cancel = true;
            grid.CancelEdit();
            LoadGridData();
        }

        protected void RetrieveRefreshBtn_Click(object sender, EventArgs e)
        {
            try
            {
                ConnectionStringSettings conString = ConfigurationManager.ConnectionStrings["ALCOA_WAO_CSMWP_devConnectionString"];
                string sqldb = conString.ConnectionString;
                using(SqlConnection cn = new SqlConnection(sqldb))
                {
                    cn.Open();
                    SqlCommand cmd = new SqlCommand("CompanyContractorReviewRetreiveRefresh", cn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@UserId", auth.UserId);
                    cmd.Parameters.AddWithValue("@CompanyId", auth.CompanyId);
                    cmd.CommandTimeout = 300;
                    cmd.ExecuteNonQuery();
                }
                LoadGridData();
            }
            catch(Exception ex)
            {
                Response.Redirect("Error.aspx" + QueryStringModule.Encrypt("error=" + ex.Message));
            }
        }

        protected void UpdateProcessedBtn_Click(object sender, EventArgs e)
        {
            List<object> keyValues = grid.GetSelectedFieldValues("CompanyAbn;CWKNumber");

            foreach(object obj in keyValues)
            {
                string[] keys = obj.ToString().Split('|');
                string companyAbn = keys[0];
                decimal cWKNumber = Convert.ToDecimal(keys[1]);
                dal.CompanyContractorReview ccr = companyContractorReviewSvc.Get(r => r.CompanyAbn == companyAbn && r.CWKNumber == cWKNumber, null);
                // NOTE even if they select a non-changed record we will still set it to processed
                ccr.Changed = false;
                ccr.Processed = true;
                ccr.ProcessedByUserId = ccr.LastUpdateByUserId = auth.UserId;
                ccr.ProcessedDate = ccr.LastUpdateDate = DateTime.Now;
                ccr.LastUpdateReasonTypeId = (int)UpdateReasonType.Update;
                companyContractorReviewSvc.Update(ccr, false);
            }
            companyContractorReviewSvc.SaveChanges();
            grid.Selection.UnselectAll();
            LoadGridData();
        }
    }
}
