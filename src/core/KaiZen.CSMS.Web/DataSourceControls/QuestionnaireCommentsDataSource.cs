﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.QuestionnaireCommentsProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(QuestionnaireCommentsDataSourceDesigner))]
	public class QuestionnaireCommentsDataSource : ProviderDataSource<QuestionnaireComments, QuestionnaireCommentsKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireCommentsDataSource class.
		/// </summary>
		public QuestionnaireCommentsDataSource() : base(new QuestionnaireCommentsService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the QuestionnaireCommentsDataSourceView used by the QuestionnaireCommentsDataSource.
		/// </summary>
		protected QuestionnaireCommentsDataSourceView QuestionnaireCommentsView
		{
			get { return ( View as QuestionnaireCommentsDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the QuestionnaireCommentsDataSource control invokes to retrieve data.
		/// </summary>
		public QuestionnaireCommentsSelectMethod SelectMethod
		{
			get
			{
				QuestionnaireCommentsSelectMethod selectMethod = QuestionnaireCommentsSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (QuestionnaireCommentsSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the QuestionnaireCommentsDataSourceView class that is to be
		/// used by the QuestionnaireCommentsDataSource.
		/// </summary>
		/// <returns>An instance of the QuestionnaireCommentsDataSourceView class.</returns>
		protected override BaseDataSourceView<QuestionnaireComments, QuestionnaireCommentsKey> GetNewDataSourceView()
		{
			return new QuestionnaireCommentsDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the QuestionnaireCommentsDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class QuestionnaireCommentsDataSourceView : ProviderDataSourceView<QuestionnaireComments, QuestionnaireCommentsKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireCommentsDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the QuestionnaireCommentsDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public QuestionnaireCommentsDataSourceView(QuestionnaireCommentsDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal QuestionnaireCommentsDataSource QuestionnaireCommentsOwner
		{
			get { return Owner as QuestionnaireCommentsDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal QuestionnaireCommentsSelectMethod SelectMethod
		{
			get { return QuestionnaireCommentsOwner.SelectMethod; }
			set { QuestionnaireCommentsOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal QuestionnaireCommentsService QuestionnaireCommentsProvider
		{
			get { return Provider as QuestionnaireCommentsService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<QuestionnaireComments> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<QuestionnaireComments> results = null;
			QuestionnaireComments item;
			count = 0;
			
			System.Int32 _questionnaireCommentId;
			System.Int32 _questionnaireId;
			System.Int32 _createdByUserId;
			System.Int32? _modifiedByUserId_nullable;

			switch ( SelectMethod )
			{
				case QuestionnaireCommentsSelectMethod.Get:
					QuestionnaireCommentsKey entityKey  = new QuestionnaireCommentsKey();
					entityKey.Load(values);
					item = QuestionnaireCommentsProvider.Get(entityKey);
					results = new TList<QuestionnaireComments>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case QuestionnaireCommentsSelectMethod.GetAll:
                    results = QuestionnaireCommentsProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case QuestionnaireCommentsSelectMethod.GetPaged:
					results = QuestionnaireCommentsProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case QuestionnaireCommentsSelectMethod.Find:
					if ( FilterParameters != null )
						results = QuestionnaireCommentsProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = QuestionnaireCommentsProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case QuestionnaireCommentsSelectMethod.GetByQuestionnaireCommentId:
					_questionnaireCommentId = ( values["QuestionnaireCommentId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["QuestionnaireCommentId"], typeof(System.Int32)) : (int)0;
					item = QuestionnaireCommentsProvider.GetByQuestionnaireCommentId(_questionnaireCommentId);
					results = new TList<QuestionnaireComments>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				case QuestionnaireCommentsSelectMethod.GetByQuestionnaireId:
					_questionnaireId = ( values["QuestionnaireId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["QuestionnaireId"], typeof(System.Int32)) : (int)0;
					results = QuestionnaireCommentsProvider.GetByQuestionnaireId(_questionnaireId, this.StartIndex, this.PageSize, out count);
					break;
				// FK
				case QuestionnaireCommentsSelectMethod.GetByCreatedByUserId:
					_createdByUserId = ( values["CreatedByUserId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["CreatedByUserId"], typeof(System.Int32)) : (int)0;
					results = QuestionnaireCommentsProvider.GetByCreatedByUserId(_createdByUserId, this.StartIndex, this.PageSize, out count);
					break;
				case QuestionnaireCommentsSelectMethod.GetByModifiedByUserId:
					_modifiedByUserId_nullable = (System.Int32?) EntityUtil.ChangeType(values["ModifiedByUserId"], typeof(System.Int32?));
					results = QuestionnaireCommentsProvider.GetByModifiedByUserId(_modifiedByUserId_nullable, this.StartIndex, this.PageSize, out count);
					break;
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == QuestionnaireCommentsSelectMethod.Get || SelectMethod == QuestionnaireCommentsSelectMethod.GetByQuestionnaireCommentId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				QuestionnaireComments entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					QuestionnaireCommentsProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<QuestionnaireComments> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			QuestionnaireCommentsProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region QuestionnaireCommentsDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the QuestionnaireCommentsDataSource class.
	/// </summary>
	public class QuestionnaireCommentsDataSourceDesigner : ProviderDataSourceDesigner<QuestionnaireComments, QuestionnaireCommentsKey>
	{
		/// <summary>
		/// Initializes a new instance of the QuestionnaireCommentsDataSourceDesigner class.
		/// </summary>
		public QuestionnaireCommentsDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public QuestionnaireCommentsSelectMethod SelectMethod
		{
			get { return ((QuestionnaireCommentsDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new QuestionnaireCommentsDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region QuestionnaireCommentsDataSourceActionList

	/// <summary>
	/// Supports the QuestionnaireCommentsDataSourceDesigner class.
	/// </summary>
	internal class QuestionnaireCommentsDataSourceActionList : DesignerActionList
	{
		private QuestionnaireCommentsDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the QuestionnaireCommentsDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public QuestionnaireCommentsDataSourceActionList(QuestionnaireCommentsDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public QuestionnaireCommentsSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion QuestionnaireCommentsDataSourceActionList
	
	#endregion QuestionnaireCommentsDataSourceDesigner
	
	#region QuestionnaireCommentsSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the QuestionnaireCommentsDataSource.SelectMethod property.
	/// </summary>
	public enum QuestionnaireCommentsSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByQuestionnaireCommentId method.
		/// </summary>
		GetByQuestionnaireCommentId,
		/// <summary>
		/// Represents the GetByQuestionnaireId method.
		/// </summary>
		GetByQuestionnaireId,
		/// <summary>
		/// Represents the GetByCreatedByUserId method.
		/// </summary>
		GetByCreatedByUserId,
		/// <summary>
		/// Represents the GetByModifiedByUserId method.
		/// </summary>
		GetByModifiedByUserId
	}
	
	#endregion QuestionnaireCommentsSelectMethod

	#region QuestionnaireCommentsFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireComments"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireCommentsFilter : SqlFilter<QuestionnaireCommentsColumn>
	{
	}
	
	#endregion QuestionnaireCommentsFilter

	#region QuestionnaireCommentsExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireComments"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireCommentsExpressionBuilder : SqlExpressionBuilder<QuestionnaireCommentsColumn>
	{
	}
	
	#endregion QuestionnaireCommentsExpressionBuilder	

	#region QuestionnaireCommentsProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;QuestionnaireCommentsChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireComments"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireCommentsProperty : ChildEntityProperty<QuestionnaireCommentsChildEntityTypes>
	{
	}
	
	#endregion QuestionnaireCommentsProperty
}

