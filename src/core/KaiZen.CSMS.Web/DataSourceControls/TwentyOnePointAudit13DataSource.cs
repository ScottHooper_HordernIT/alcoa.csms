﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.TwentyOnePointAudit13Provider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(TwentyOnePointAudit13DataSourceDesigner))]
	public class TwentyOnePointAudit13DataSource : ProviderDataSource<TwentyOnePointAudit13, TwentyOnePointAudit13Key>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit13DataSource class.
		/// </summary>
		public TwentyOnePointAudit13DataSource() : base(new TwentyOnePointAudit13Service())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the TwentyOnePointAudit13DataSourceView used by the TwentyOnePointAudit13DataSource.
		/// </summary>
		protected TwentyOnePointAudit13DataSourceView TwentyOnePointAudit13View
		{
			get { return ( View as TwentyOnePointAudit13DataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the TwentyOnePointAudit13DataSource control invokes to retrieve data.
		/// </summary>
		public TwentyOnePointAudit13SelectMethod SelectMethod
		{
			get
			{
				TwentyOnePointAudit13SelectMethod selectMethod = TwentyOnePointAudit13SelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (TwentyOnePointAudit13SelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the TwentyOnePointAudit13DataSourceView class that is to be
		/// used by the TwentyOnePointAudit13DataSource.
		/// </summary>
		/// <returns>An instance of the TwentyOnePointAudit13DataSourceView class.</returns>
		protected override BaseDataSourceView<TwentyOnePointAudit13, TwentyOnePointAudit13Key> GetNewDataSourceView()
		{
			return new TwentyOnePointAudit13DataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the TwentyOnePointAudit13DataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class TwentyOnePointAudit13DataSourceView : ProviderDataSourceView<TwentyOnePointAudit13, TwentyOnePointAudit13Key>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit13DataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the TwentyOnePointAudit13DataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public TwentyOnePointAudit13DataSourceView(TwentyOnePointAudit13DataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal TwentyOnePointAudit13DataSource TwentyOnePointAudit13Owner
		{
			get { return Owner as TwentyOnePointAudit13DataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal TwentyOnePointAudit13SelectMethod SelectMethod
		{
			get { return TwentyOnePointAudit13Owner.SelectMethod; }
			set { TwentyOnePointAudit13Owner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal TwentyOnePointAudit13Service TwentyOnePointAudit13Provider
		{
			get { return Provider as TwentyOnePointAudit13Service; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<TwentyOnePointAudit13> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<TwentyOnePointAudit13> results = null;
			TwentyOnePointAudit13 item;
			count = 0;
			
			System.Int32 _id;

			switch ( SelectMethod )
			{
				case TwentyOnePointAudit13SelectMethod.Get:
					TwentyOnePointAudit13Key entityKey  = new TwentyOnePointAudit13Key();
					entityKey.Load(values);
					item = TwentyOnePointAudit13Provider.Get(entityKey);
					results = new TList<TwentyOnePointAudit13>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case TwentyOnePointAudit13SelectMethod.GetAll:
                    results = TwentyOnePointAudit13Provider.GetAll(StartIndex, PageSize, out count);
                    break;
				case TwentyOnePointAudit13SelectMethod.GetPaged:
					results = TwentyOnePointAudit13Provider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case TwentyOnePointAudit13SelectMethod.Find:
					if ( FilterParameters != null )
						results = TwentyOnePointAudit13Provider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = TwentyOnePointAudit13Provider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case TwentyOnePointAudit13SelectMethod.GetById:
					_id = ( values["Id"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["Id"], typeof(System.Int32)) : (int)0;
					item = TwentyOnePointAudit13Provider.GetById(_id);
					results = new TList<TwentyOnePointAudit13>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				// FK
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == TwentyOnePointAudit13SelectMethod.Get || SelectMethod == TwentyOnePointAudit13SelectMethod.GetById )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				TwentyOnePointAudit13 entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					TwentyOnePointAudit13Provider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<TwentyOnePointAudit13> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			TwentyOnePointAudit13Provider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region TwentyOnePointAudit13DataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the TwentyOnePointAudit13DataSource class.
	/// </summary>
	public class TwentyOnePointAudit13DataSourceDesigner : ProviderDataSourceDesigner<TwentyOnePointAudit13, TwentyOnePointAudit13Key>
	{
		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit13DataSourceDesigner class.
		/// </summary>
		public TwentyOnePointAudit13DataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public TwentyOnePointAudit13SelectMethod SelectMethod
		{
			get { return ((TwentyOnePointAudit13DataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new TwentyOnePointAudit13DataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region TwentyOnePointAudit13DataSourceActionList

	/// <summary>
	/// Supports the TwentyOnePointAudit13DataSourceDesigner class.
	/// </summary>
	internal class TwentyOnePointAudit13DataSourceActionList : DesignerActionList
	{
		private TwentyOnePointAudit13DataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit13DataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public TwentyOnePointAudit13DataSourceActionList(TwentyOnePointAudit13DataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public TwentyOnePointAudit13SelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion TwentyOnePointAudit13DataSourceActionList
	
	#endregion TwentyOnePointAudit13DataSourceDesigner
	
	#region TwentyOnePointAudit13SelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the TwentyOnePointAudit13DataSource.SelectMethod property.
	/// </summary>
	public enum TwentyOnePointAudit13SelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetById method.
		/// </summary>
		GetById
	}
	
	#endregion TwentyOnePointAudit13SelectMethod

	#region TwentyOnePointAudit13Filter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit13"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAudit13Filter : SqlFilter<TwentyOnePointAudit13Column>
	{
	}
	
	#endregion TwentyOnePointAudit13Filter

	#region TwentyOnePointAudit13ExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit13"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAudit13ExpressionBuilder : SqlExpressionBuilder<TwentyOnePointAudit13Column>
	{
	}
	
	#endregion TwentyOnePointAudit13ExpressionBuilder	

	#region TwentyOnePointAudit13Property
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;TwentyOnePointAudit13ChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit13"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAudit13Property : ChildEntityProperty<TwentyOnePointAudit13ChildEntityTypes>
	{
	}
	
	#endregion TwentyOnePointAudit13Property
}

