<%@ Page Language="C#" AutoEventWireup="true" Inherits="logout" Codebehind="logout.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Logged Out</title>
    <link rel="shortcut icon" href="~/Images/favicon.ico" type="image/x-icon" />
    <script language="javascript" type="text/javascript">
            function prevent_previous_page_return()
            {
               window.history.forward();
            }
    </script>
    <asp:PlaceHolder ID="phBundling" runat="server">
        <%: System.Web.Optimization.Styles.Render("~/bundles/user_master_styles") %>
    </asp:PlaceHolder>

</head>


<body onload="prevent_previous_page_return(); self.close();">
    <form id="form1" runat="server">
    <div style="text-align: center">
        <img border="0" height="33" src="Images/top_alcoa_logo_wide.gif" width="153" /><br />
        <table id="Table1" align="center" border="0" cellpadding="0" cellspacing="0" width="900">
            <tr>
                <td align="right" style="text-align: center" valign="top">
                    <table border="0" cellpadding="0" cellspacing="0">
                        <tbody>
                            <tr>
                                <td>
                                    <img height="3" src="Images/spacer.gif" /></td>
                            </tr>
                        </tbody>
                    </table>
                    <span style="color: #003399">Australian Operations</span></td>
            </tr>
            <tr>
                <td align="right" style="text-align: center" valign="bottom">
                    <span class="sitetitleblue"><a class="sitetitleblue" href="default.aspx" onfocus="blur(this)">
                        Contractor Services Management System</a></span>
                </td>
            </tr>
        </table>
        <br />
        <strong>You have been logged out.</strong></div>
    </form>

    <%: System.Web.Optimization.Scripts.Render("~/bundles/jquery_scripts") %>
    <%: System.Web.Optimization.Scripts.Render("~/bundles/user_master_scripts") %>
</body>
</html>
