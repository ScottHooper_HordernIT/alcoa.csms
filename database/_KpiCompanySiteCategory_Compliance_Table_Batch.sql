USE [AUA_CSMS_Dev]
GO

/****** Object:  StoredProcedure [dbo].[_KpiCompanySiteCategory_Compliance_Table_Batch]    Script Date: 17/01/2016 10:48:05 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

















  
/*              
----------------------------------------------------------------------------------------------------              
-- Date Created: Friday, 9 October 2015             
              
-- Created By: R.O'Brien @ HordernIT   
-- Purpose: Used to run UpdateAdHocRadar batch job

-- Modified:
-- 25/11/2015 DT397	Cindi Thornton	- added KpiScoreYTD back in to insert otherwise you get a null on insert error.     
----------------------------------------------------------------------------------------------------              
              
              
*/              
CREATE PROCEDURE [dbo].[_KpiCompanySiteCategory_Compliance_Table_Batch]              
(                          
	@Year nvarchar(7) --eg. All, Current, 2015          
)              
AS              

print 'Start: ' + cast(getdate() as varchar(max));

print 'Declarations';
declare @avgNoEmployees_Divisor decimal(18,2) = 1;
declare @lastMonth int = month(dateadd(mm, -1, getdate()));
if @lastMonth <> 12
begin
	set @avgNoEmployees_Divisor = cast(@lastMonth as decimal(18,2));
end

print 'Declare for current year';
declare @fromYear int = year(getdate());
declare @toYear int = year(getdate());
             
if @Year = 'All'
begin
	print 'Select all years (from ' + cast(@fromYear as varchar(max)) + ' to ' + cast(@toYear as varchar(max)) + ')';
	set @fromYear = 2007;
end
else
begin 
	--print 'Truncate items2 if not all year recalculation'
	--DELETE FROM [dbo].[AdHoc_Radar_Items2];

	if @Year = 'Current'
	begin
		print 'Select current year (from ' + cast(@fromYear as varchar(max)) + ' to ' + cast(@toYear as varchar(max)) + ')';
	end
	else
	begin
		print 'Select specific year (from ' + cast(@fromYear as varchar(max)) + ' to ' + cast(@toYear as varchar(max)) + ')';
		set @fromYear = cast(@Year as int);
		set @toYear = cast(@Year as int);
	end
end

print 'Create adhoc radar records where not alread exists';
;With MissingAdhocRadar
As
(
	select	[KpiMeasureId],
			[Description],
			[CompanySiteCategoryId],
			Rank() over (Partition by [CompanySiteCategoryId] Order by [Description]) as [RowNum],
			(select isnull(max([KpiOrdinal]) + 1, 1) from [dbo].[AdHoc_Radar] where [dbo].[AdHoc_Radar].[CompanySiteCategoryId] = kpi.[CompanySiteCategoryId]) as [MaxOrdinal]
	from	[dbo].[KpiMeasure] kpi
	where not exists (
		select	'x' 
		from	[dbo].[AdHoc_Radar] ahr
		where	ahr.[CompanySiteCategoryId] = kpi.[CompanySiteCategoryId]
		  and	ahr.[KpiName] = kpi.[Description]
	)
)
insert into [dbo].[AdHoc_Radar] ([CompanySiteCategoryId],[KpiOrdinal],[KpiName],[ModifiedDate])
select	[CompanySiteCategoryId],
		([RowNum] + [MaxOrdinal]),
		[Description],
		getdate()
from	[MissingAdhocRadar]

print 'Loop through selected years';
;while @fromYear <= @toYear
begin
	declare @dtNowMinusOneMonth datetime = dateadd(mm, -1, getdate());
	if @fromYear < year(getdate())
	begin
		set @dtNowMinusOneMonth = DATEFROMPARTS(@fromYear, 12,1);
	end;
	print 'dtNowMinusOneMonth: ' + cast(@dtNowMinusOneMonth as varchar(max));

	print 'Year: ' + cast(@fromYear as varchar(max));
	With AdhocRadarKpis
	As
	(
		--Get all kpi measures and cross join with sites to get a kpi per site (these lines then become adhoc_radar_items
		select	@fromYear as [Year],
				kpi.[KpiMeasureId],
				kpi.[Description] as [KpiDescription],
				kpi.[CompanySiteCategoryId],
				csc.[CategoryName],
				csc.[CategoryDesc],
				s.[SiteId],
				s.[SiteName],
				s.[SiteAbbrev],
				s.[SiteNameEbi],
				s.[RegionId],
				ar.[AdHoc_RadarId]
		from	[dbo].[KpiMeasure] kpi
				inner join [dbo].[CompanySiteCategory] csc on kpi.[CompanySiteCategoryId] = csc.[CompanySiteCategoryId]
				left join [dbo].[AdHoc_Radar] ar on ar.[CompanySiteCategoryId] = kpi.[CompanySiteCategoryId] and ar.[KpiName]= kpi.[Description]
				cross apply (
					--Note: some sites are actually regions (start with '-').  for these sites, get their corresponding RegionId
					select	[dbo].[Sites].*,
							[dbo].[Regions].[RegionId]
					from	[dbo].[Sites]
							left join [dbo].[Regions] on [dbo].[Sites].[SiteId] = [dbo].[Regions].[RegionSiteId]
					where	(LEFT([SiteAbbrev], 1) <> '-' or (LEFT([SiteAbbrev], 1) = '-' and [RegionSiteId] is not null))
				) as s
	), AdhocRadarItems
	As
	(
		--Perform calculation on each site kpi to create ytd score
		select	ar.[Year],
				ar.[KpiMeasureId],
				ar.[KpiDescription],
				ar.[CompanySiteCategoryId],
				ar.[CategoryName],
				ar.[CategoryDesc],
				ar.[SiteId],
				ar.[SiteName],
				ar.[SiteAbbrev],
				ar.[SiteNameEbi],
				ar.[RegionId],
				ar.[AdHoc_RadarId],
				ari.[AdHoc_Radar_ItemId],
				(SELECT COUNT(DISTINCT f.CompanyId) FROM FileDb f INNER JOIN CompanySiteCategoryStandard cscs ON f.CompanyId = cscs.CompanyId WHERE cscs.SiteId = ar.SiteId AND f.Description LIKE CAST(ar.Year As varchar(4)) + '%' AND dbo.GetCompanySiteCategoryExceptionId(SiteId,f.CompanyId,DateFromParts(ar.Year,MONTH(dateadd("m",0, GETDATE())),1),cscs.CompanySiteCategoryId) LIKE ar.CompanySiteCategoryId) As [SmpNo],          
				(SELECT COUNT(DISTINCT(CAST(CompanyId as varchar(5)) + '&' + CAST(SiteId As varchar(5)))) FROM CompanySiteCategoryStandard cscs WHERE cscs.SiteId = ar.SiteId AND dbo.GetCompanySiteCategoryExceptionId(SiteId,CompanyId,DateFromParts(ar.Year,MONTH(dateadd("m",0, GETDATE())),1),cscs.CompanySiteCategoryId) LIKE ar.CompanySiteCategoryId) As [SmpExpected],
				case 
					--Site kpi
					when ar.[KpiDescription] = 'Peak No Employees (Previous Month)' and LEFT(ar.[SiteAbbrev], 1) <> '-' 
						then ISNULL((SELECT SUM(aheaPeakNopplSiteWeek) FROM KpiCompanySiteCategory WHERE YEAR(kpiDateTime) = ar.[Year] AND Month(kpiDateTime) = MONTH(dateadd("m",-1, GETDATE())) AND CompanySiteCategoryId LIKE ar.[CompanySiteCategoryId] AND SiteId = ar.[SiteId]),0)
					when ar.[KpiDescription] = 'Avg No Employees (YTD)' and LEFT(ar.[SiteAbbrev], 1) <> '-' 
						then (select ISNULL(SUM(aheaAvgNopplSiteMonth),0) from KpiCompanySiteCategory WHERE YEAR(kpiDateTime) = ar.[Year] AND dbo.GetCompanySiteCategoryExceptionId(SiteId,CompanyId,kpiDateTime,CompanySiteCategoryId) LIKE ar.[CompanySiteCategoryId] AND SiteId = ar.[SiteId])
					when ar.[KpiDescription] = 'Number of Companies' and LEFT(ar.[SiteAbbrev], 1) <> '-' 
						then (SELECT COUNT(DISTINCT CompanyId) FROM CompanySiteCategoryStandard WHERE SiteId = ar.[SiteId] AND dbo.GetCompanySiteCategoryExceptionId(SiteId,CompanyId,DateFromParts(ar.[Year],MONTH(dateadd("m",0, GETDATE())),1),CompanySiteCategoryId) like ar.[CompanySiteCategoryId])
					when ar.[KpiDescription] = 'Number of Companies On Site' and LEFT(ar.[SiteAbbrev], 1) <> '-' 
						then (SELECT COUNT(DISTINCT CompanyId) FROM KpiCompanySiteCategory WHERE YEAR(kpiDateTime) = ar.[Year] AND dbo.GetCompanySiteCategoryExceptionId(SiteId,CompanyId,DateFromParts(ar.[Year],MONTH(dateadd("m",0, GETDATE())),1),CompanySiteCategoryId) LIKE ar.[CompanySiteCategoryId] AND SiteId = ar.[SiteId] AND (EbiOnSite = 1 OR AheaTotalManHours > 0))
					when ar.[KpiDescription] = 'Total Man Hours' and LEFT(ar.[SiteAbbrev], 1) <> '-' 
						then (select ISNULL(CAST(SUM(COALESCE(aheaTotalManHours,0)) as decimal(18,2)),0) from KpiCompanySiteCategory WHERE YEAR(kpiDateTime) = ar.[Year] AND dbo.GetCompanySiteCategoryExceptionId(SiteId,CompanyId,kpiDateTime,CompanySiteCategoryId) LIKE ar.[CompanySiteCategoryId] AND SiteId = ar.[SiteId])
					when ar.[KpiDescription] = 'LWDFR' and LEFT(ar.[SiteAbbrev], 1) <> '-' 
						then (select ISNULL(CAST((ISNULL((SUM(COALESCE(ipLTI,0)) * 200000),0) / NULLIF(ISNULL(COALESCE((SUM(COALESCE(aheaTotalManHours,0))),0),0),0)) AS Decimal(18,2)),0) from KpiCompanySiteCategory WHERE YEAR(kpiDateTime) = ar.[Year] AND dbo.GetCompanySiteCategoryExceptionId(SiteId,CompanyId,kpiDateTime,CompanySiteCategoryId) LIKE ar.[CompanySiteCategoryId] AND SiteId = ar.[SiteId])
					when ar.[KpiDescription] = 'TRIFR' and LEFT(ar.[SiteAbbrev], 1) <> '-' 
						then (select ISNULL(((CAST((CASE WHEN (SUM(COALESCE(aheaTotalManHours,0)) = 0.0) THEN 0.0 ELSE (((SUM(COALESCE(ipMTI,0)) + SUM(COALESCE(ipRDI,0)) + SUM(COALESCE(ipLTI,0))) * 200000) / (SUM(COALESCE(aheaTotalManHours,0)))) END) AS DECIMAL(18,2)))),0) from KpiCompanySiteCategory WHERE YEAR(kpiDateTime) = ar.[Year] AND dbo.GetCompanySiteCategoryExceptionId(SiteId,CompanyId,kpiDateTime,CompanySiteCategoryId) LIKE ar.[CompanySiteCategoryId] AND SiteId = ar.[SiteId])
					when ar.[KpiDescription] = 'AIFR' and LEFT(ar.[SiteAbbrev], 1) <> '-' 
						then (select ISNULL(((CAST((CASE WHEN (SUM(COALESCE(aheaTotalManHours,0)) = 0.0) THEN 0.0 ELSE (((SUM(COALESCE(ipFATI,0)) + SUM(COALESCE(ipMTI,0)) + SUM(COALESCE(ipRDI,0)) + SUM(COALESCE(ipLTI,0))) * 200000) / (SUM(COALESCE(aheaTotalManHours,0)))) END) AS DECIMAL(18,2)))),0) from KpiCompanySiteCategory WHERE YEAR(kpiDateTime) = ar.[Year] AND dbo.GetCompanySiteCategoryExceptionId(SiteId,CompanyId,kpiDateTime,CompanySiteCategoryId) LIKE ar.[CompanySiteCategoryId] AND SiteId = ar.[SiteId])
					when ar.[KpiDescription] = 'IFE : Injury Ratio' and LEFT(ar.[SiteAbbrev], 1) <> '-' 
						then (select ISNULL(CAST((CASE WHEN ((SUM(COALESCE(ipLTI,0)) + SUM(COALESCE(ipRDI,0)) + SUM(COALESCE(ipMTI,0)) + SUM(COALESCE(ipFATI,0)) ) = 0.0) THEN CAST(SUM(COALESCE(ipIFE,0)) AS DECIMAL(18,2)) ELSE (CAST(SUM(COALESCE(ipIFE,0)) AS DECIMAL(18,2)) / CAST((SUM(COALESCE(ipLTI,0)) + SUM(COALESCE(ipRDI,0)) + SUM(COALESCE(ipMTI,0)) + SUM(COALESCE(ipFATI,0))) AS DECIMAL(18,2))) END) AS DECIMAL(18,2)),0) from KpiCompanySiteCategory WHERE YEAR(kpiDateTime) = ar.[Year] AND dbo.GetCompanySiteCategoryExceptionId(SiteId,CompanyId,kpiDateTime,CompanySiteCategoryId) LIKE ar.[CompanySiteCategoryId] AND SiteId = ar.[SiteId])
					when ar.[KpiDescription] = 'DART' and LEFT(ar.[SiteAbbrev], 1) <> '-' 
						then (select ISNULL(CAST(CASE WHEN (COALESCE(ISNULL(SUM(aheaTotalManHours),0),0) = 0) THEN (0) ELSE ((((ISNULL(SUM(ipLTI),0) + (ISNULL(SUM(ipRDI),0)))) * 200000) / (SUM(aheaTotalManHours))) END As Decimal(18,2)),0) from KpiCompanySiteCategory WHERE YEAR(kpiDateTime) = ar.[Year] AND dbo.GetCompanySiteCategoryExceptionId(SiteId,CompanyId,kpiDateTime,CompanySiteCategoryId) LIKE ar.[CompanySiteCategoryId] AND SiteId = ar.[SiteId])
					when ar.[KpiDescription] = 'Risk Notifications' and LEFT(ar.[SiteAbbrev], 1) <> '-' 
						then (select ISNULL(((CAST((CASE  WHEN (SUM(COALESCE(ipRN,0)) = 0) THEN 0 ELSE (SUM(COALESCE(ipRN,0))) END) AS int))),0) from KpiCompanySiteCategory WHERE YEAR(kpiDateTime) = ar.[Year] AND dbo.GetCompanySiteCategoryExceptionId(SiteId,CompanyId,kpiDateTime,CompanySiteCategoryId) LIKE ar.[CompanySiteCategoryId] AND SiteId = ar.[SiteId])
					when ar.[KpiDescription] = 'Safety Plan(s) Submitted' and LEFT(ar.[SiteAbbrev], 1) <> '-' 
						then 0.0

					--Region kpi
					when ar.[KpiDescription] = 'Peak No Employees (Previous Month)' and LEFT(ar.[SiteAbbrev], 1) = '-' 
						then ISNULL((SELECT	SUM(aheaPeakNopplSiteWeek) FROM	KpiCompanySiteCategory WHERE YEAR(kpiDateTime) = ar.[Year] AND Month(kpiDateTime) = MONTH(dateadd("m",-1, GETDATE())) AND CompanySiteCategoryId LIKE ar.CompanySiteCategoryId AND	((SELECT COUNT(*) FROM RegionsSites WHERE RegionsSites.Regionid = ar.RegionId AND RegionsSites.SiteId = KpiCompanySiteCategory.SiteId) > 0)),0)
					when ar.[KpiDescription] = 'Avg No Employees (YTD)' and LEFT(ar.[SiteAbbrev], 1) = '-' 
						then (select ISNULL(SUM(aheaAvgNopplSiteMonth),0) FROM KpiCompanySiteCategory WHERE YEAR(kpiDateTime) = ar.[Year] AND KpiCompanySiteCategory.CompanySiteCategoryId LIKE ar.CompanySiteCategoryId AND ((SELECT COUNT(*) FROM RegionsSites WHERE RegionsSites.Regionid = ar.RegionId AND RegionsSites.SiteId = KpiCompanySiteCategory.SiteId) > 0))
					when ar.[KpiDescription] = 'Number of Companies' and LEFT(ar.[SiteAbbrev], 1) = '-' 
						then (SELECT COUNT(DISTINCT(CAST(CompanyId as varchar(5)) + '&' + CAST(SiteId As varchar(5)))) FROM	CompanySiteCategoryStandard WHERE dbo.GetCompanySiteCategoryExceptionId(SiteId,CompanyId,DateFromParts(Year(GetDate()),MONTH(dateadd("m",0, GETDATE())),1),CompanySiteCategoryId) LIKE ar.CompanySiteCategoryId AND	((SELECT COUNT(*) FROM RegionsSites WHERE RegionsSites.Regionid = ar.RegionId AND RegionsSites.SiteId = CompanySiteCategoryStandard.SiteId) > 0))
					when ar.[KpiDescription] = 'Number of Companies On Site' and LEFT(ar.[SiteAbbrev], 1) = '-' 
						then (SELECT COUNT(DISTINCT CAST(CompanyId as varchar) + '-' + CAST(SiteId as varchar)) FROM KpiCompanySiteCategory WHERE YEAR(kpiDateTime) = ar.[Year] AND	dbo.GetCompanySiteCategoryExceptionId(SiteId,CompanyId,DateFromParts(Year(GetDate()),MONTH(dateadd("m",0, GETDATE())),1),CompanySiteCategoryId) LIKE ar.CompanySiteCategoryId AND ((SELECT COUNT(*) FROM RegionsSites WHERE RegionsSites.Regionid = ar.RegionId AND RegionsSites.SiteId = KpiCompanySiteCategory.SiteId) > 0) AND (EbiOnSite = 1 OR AheaTotalManHours > 0))
					when ar.[KpiDescription] = 'Total Man Hours' and LEFT(ar.[SiteAbbrev], 1) = '-' 
						then (select ISNULL(CAST(SUM(COALESCE(aheaTotalManHours,0)) as decimal(18,2)),0) FROM KpiCompanySiteCategory WHERE YEAR(kpiDateTime) = ar.[Year] AND KpiCompanySiteCategory.CompanySiteCategoryId LIKE ar.CompanySiteCategoryId AND ((SELECT COUNT(*) FROM RegionsSites WHERE RegionsSites.Regionid = ar.RegionId AND RegionsSites.SiteId = KpiCompanySiteCategory.SiteId) > 0))
					when ar.[KpiDescription] = 'LWDFR' and LEFT(ar.[SiteAbbrev], 1) = '-' 
						then (select ISNULL(CAST((ISNULL((SUM(COALESCE(ipLTI,0)) * 200000),0) / NULLIF(ISNULL(COALESCE((SUM(COALESCE(aheaTotalManHours,0))),0),0),0)) AS Decimal(18,2)),0) FROM KpiCompanySiteCategory WHERE YEAR(kpiDateTime) = ar.[Year] AND KpiCompanySiteCategory.CompanySiteCategoryId LIKE ar.CompanySiteCategoryId AND ((SELECT COUNT(*) FROM RegionsSites WHERE RegionsSites.Regionid = ar.RegionId AND RegionsSites.SiteId = KpiCompanySiteCategory.SiteId) > 0))
					when ar.[KpiDescription] = 'TRIFR' and LEFT(ar.[SiteAbbrev], 1) = '-' 
						then (select ISNULL(((CAST((CASE WHEN (SUM(COALESCE(aheaTotalManHours,0)) = 0.0) THEN 0.0 ELSE (((SUM(COALESCE(ipMTI,0)) + SUM(COALESCE(ipRDI,0)) + SUM(COALESCE(ipLTI,0))) * 200000) / (SUM(COALESCE(aheaTotalManHours,0)))) END) AS DECIMAL(18,2)))),0) FROM KpiCompanySiteCategory WHERE YEAR(kpiDateTime) = ar.[Year] AND KpiCompanySiteCategory.CompanySiteCategoryId LIKE ar.CompanySiteCategoryId AND ((SELECT COUNT(*) FROM RegionsSites WHERE RegionsSites.Regionid = ar.RegionId AND RegionsSites.SiteId = KpiCompanySiteCategory.SiteId) > 0))
					when ar.[KpiDescription] = 'AIFR' and LEFT(ar.[SiteAbbrev], 1) = '-' 
						then (select ISNULL(((CAST((CASE WHEN (SUM(COALESCE(aheaTotalManHours,0)) = 0.0) THEN 0.0 ELSE (((SUM(COALESCE(ipFATI,0)) + SUM(COALESCE(ipMTI,0)) + SUM(COALESCE(ipRDI,0)) + SUM(COALESCE(ipLTI,0))) * 200000) / (SUM(COALESCE(aheaTotalManHours,0)))) END) AS DECIMAL(18,2)))),0) FROM KpiCompanySiteCategory WHERE YEAR(kpiDateTime) = ar.[Year] AND KpiCompanySiteCategory.CompanySiteCategoryId LIKE ar.CompanySiteCategoryId AND ((SELECT COUNT(*) FROM RegionsSites WHERE RegionsSites.Regionid = ar.RegionId AND RegionsSites.SiteId = KpiCompanySiteCategory.SiteId) > 0))
					when ar.[KpiDescription] = 'IFE : Injury Ratio' and LEFT(ar.[SiteAbbrev], 1) = '-' 
						then (SELECT (ISNULL(CAST((CASE WHEN ((SUM(COALESCE(ipLTI,0)) + SUM(COALESCE(ipRDI,0)) + SUM(COALESCE(ipMTI,0)) + SUM(COALESCE(ipFATI,0)) ) = 0.0) THEN CAST(SUM(COALESCE(ipIFE,0)) AS DECIMAL(18,2)) ELSE (CAST(SUM(COALESCE(ipIFE,0)) AS DECIMAL(18,2)) / CAST((SUM(COALESCE(ipLTI,0)) + SUM(COALESCE(ipRDI,0)) + SUM(COALESCE(ipMTI,0)) + SUM(COALESCE(ipFATI,0))) AS DECIMAL(18,2))) END) AS DECIMAL(18,2)),0)) FROM KpiCompanySiteCategory WHERE YEAR(kpiDateTime) = ar.[Year] AND KpiCompanySiteCategory.CompanySiteCategoryId LIKE ar.CompanySiteCategoryId AND ((SELECT COUNT(*) FROM RegionsSites WHERE RegionsSites.Regionid = ar.RegionId AND RegionsSites.SiteId = KpiCompanySiteCategory.SiteId) > 0))
					when ar.[KpiDescription] = 'DART' and LEFT(ar.[SiteAbbrev], 1) = '-' 
						then (SELECT ISNULL(CAST(CASE WHEN (COALESCE(ISNULL(SUM(aheaTotalManHours),0),0) = 0) THEN (0) ELSE ((((ISNULL(SUM(ipLTI),0) + (ISNULL(SUM(ipRDI),0)))) * 200000) / (SUM(aheaTotalManHours))) END As Decimal(18,2)),0) FROM KpiCompanySiteCategory WHERE YEAR(kpiDateTime) = ar.[Year] AND KpiCompanySiteCategory.CompanySiteCategoryId LIKE ar.CompanySiteCategoryId AND ((SELECT COUNT(*) FROM RegionsSites WHERE RegionsSites.Regionid = ar.RegionId AND RegionsSites.SiteId = KpiCompanySiteCategory.SiteId) > 0))
					when ar.[KpiDescription] = 'Risk Notifications' and LEFT(ar.[SiteAbbrev], 1) = '-' 
						then (SELECT ISNULL(((CAST((CASE WHEN (SUM(COALESCE(ipRN,0)) = 0) THEN 0 ELSE (SUM(COALESCE(ipRN,0))) END) AS int))),0) FROM KpiCompanySiteCategory WHERE YEAR(kpiDateTime) = ar.[Year] AND CompanySiteCategoryId LIKE ar.CompanySiteCategoryId AND ((SELECT COUNT(*) FROM	dbo.RegionsSites WHERE	dbo.RegionsSites.SiteId = dbo.KpiCompanySiteCategory.SiteId AND	dbo.RegionsSites.RegionId = ar.RegionId) > 0))
					when ar.[KpiDescription] = 'Safety Plan(s) Submitted' and LEFT(ar.[SiteAbbrev], 1) = '-' 
						then 0.0

					else null --n/a
				end as KpiScoreYtd
		from	AdhocRadarKpis ar
				left join [dbo].[AdHoc_Radar_Items] ari on ari.[Year] = @fromYear and ari.[AdHoc_RadarId] = ar.[AdHoc_RadarId] and ari.[SiteId] = ar.[SiteId]
	), AdhocRadarItemsMonth
	As
	(
		--Perform calculation on each site/region kpi to create monthly score
		Select	ari.[Year],
				ari.[KpiMeasureId],
				ari.[KpiDescription],
				ari.[CompanySiteCategoryId],
				ari.[CategoryName],
				ari.[CategoryDesc],
				ari.[SiteId],
				ari.[SiteName],
				ari.[SiteAbbrev],
				ari.[SiteNameEbi],
				ari.[RegionId],
				ari.[AdHoc_RadarId],
				ari.[AdHoc_Radar_ItemId],
				ari.[SmpNo],
				ari.[SmpExpected],
				ari.[KpiScoreYtd],
				case when arim.[Year] is not null then arim.[Jan] else null end as [KpiScoreJan],
				case when arim.[Year] is not null then arim.[Feb] else null end as [KpiScoreFeb],
				case when arim.[Year] is not null then arim.[Mar] else null end as [KpiScoreMar],
				case when arim.[Year] is not null then arim.[Apr] else null end as [KpiScoreApr],
				case when arim.[Year] is not null then arim.[May] else null end as [KpiScoreMay],
				case when arim.[Year] is not null then arim.[Jun] else null end as [KpiScoreJun],
				case when arim.[Year] is not null then arim.[Jul] else null end as [KpiScoreJul],
				case when arim.[Year] is not null then arim.[Aug] else null end as [KpiScoreAug],
				case when arim.[Year] is not null then arim.[Sep] else null end as [KpiScoreSep],
				case when arim.[Year] is not null then arim.[Oct] else null end as [KpiScoreOct],
				case when arim.[Year] is not null then arim.[Nov] else null end as [KpiScoreNov],
				case when arim.[Year] is not null then arim.[Dec] else null end as [KpiScoreDec]
		FROM	AdhocRadarItems ari
				left join
				(
					Select [Year], [SiteId], [CompanySiteCategoryId], KpiDescription, sum([Jan]) as [Jan], sum([Feb]) as [Feb], sum([Mar]) as [Mar], sum([Apr]) as [Apr], sum([May]) as [May], sum([Jun]) as [Jun], sum([Jul]) as [Jul], sum([Aug]) as [Aug], sum([Sep]) as [Sep], sum([Oct]) as [Oct], sum([Nov]) as [Nov], sum([Dec]) as [Dec]
					FROM
					(
						Select [Year], [SiteId], [CompanySiteCategoryId], MonthAbbrev, attribute as KpiDescription, value
						FROM
						(
							SELECT	--Sites
								ari.SiteId,
								ari.CompanySiteCategoryId,
								ari.[Year],
								m.MonthId,
								m.MonthAbbrev,        
								cast(0 as decimal(18,2)) As [Peak No Employees (Previous Month)],  --Previous month calculation only, no breakdown required??    
								cast(ISNULL(AVG(aheaPeakNopplSiteWeek),0) as decimal(18,2)) As [Peak No. Of People On Site],          
								cast(ISNULL(SUM(aheaAvgNopplSiteMonth),0) as decimal(18,2)) As [Average No. Of People On Site],          
								cast(COUNT(distinct(CompanyId)) as decimal(18,2)) As [Number Of Companies],  
								cast((SELECT COUNT(DISTINCT CompanyId) FROM KpiCompanySiteCategory WHERE YEAR(kpiDateTime) = ari.Year AND MONTH(kpiDateTime) = m.MonthId AND dbo.GetCompanySiteCategoryExceptionId(SiteId,CompanyId,DateFromParts(ari.Year,MONTH(dateadd("m",0, GETDATE())),1),CompanySiteCategoryId) LIKE ari.CompanySiteCategoryId AND SiteId = ari.SiteId AND (EbiOnSite = 1 OR AheaTotalManHours > 0)) as decimal(18,2)) As [Number of Companies On Site],
								cast(ISNULL(SUM(COALESCE(aheaTotalManHours,0)),0) as decimal(18,2)) As [Total Man Hours],                
								cast(0 as decimal(18,2)) As [SmpNo], --Searching year on part of the description???  I can't filter this down by month as it's not in the description
								cast(0 as decimal(18,2)) As [SmpExpected],  --Again, not searching by year so how can I search by month??        
								cast(ISNULL(CAST((ISNULL((SUM(COALESCE(ipLTI,0)) * 200000),0) / NULLIF(ISNULL(COALESCE((SUM(COALESCE(aheaTotalManHours,0))),0),0),0)) AS Decimal(18,2)),0) as decimal(18,2)) As [LWDFR],          
								cast(ISNULL(((CAST((CASE WHEN (SUM(COALESCE(aheaTotalManHours,0)) = 0.0) THEN 0.0 ELSE (((SUM(COALESCE(ipMTI,0)) + SUM(COALESCE(ipRDI,0)) + SUM(COALESCE(ipLTI,0))) * 200000) / (SUM(COALESCE(aheaTotalManHours,0)))) END) AS DECIMAL(18,2)))),0) as decimal(18,2)) As [TRIFR],          
								cast(ISNULL(((CAST((CASE WHEN (SUM(COALESCE(aheaTotalManHours,0)) = 0.0) THEN 0.0 ELSE (((SUM(COALESCE(ipFATI,0)) + SUM(COALESCE(ipMTI,0)) + SUM(COALESCE(ipRDI,0)) + SUM(COALESCE(ipLTI,0))) * 200000) / (SUM(COALESCE(aheaTotalManHours,0)))) END) AS DECIMAL(18,2)))),0) as decimal(18,2)) As [AIFR],          
								cast((SELECT ISNULL(CAST((CASE WHEN ((SUM(COALESCE(k.ipLTI,0)) + SUM(COALESCE(k.ipRDI,0)) + SUM(COALESCE(k.ipMTI,0)) + SUM(COALESCE(k.ipFATI,0)) ) = 0.0) THEN CAST(SUM(COALESCE(k.ipIFE,0)) AS DECIMAL(18,2)) ELSE (CAST(SUM(COALESCE(k.ipIFE,0)) AS DECIMAL(18,2)) / CAST((SUM(COALESCE(k.ipLTI,0)) + SUM(COALESCE(k.ipRDI,0)) + SUM(COALESCE(k.ipMTI,0)) + SUM(COALESCE(k.ipFATI,0))) AS DECIMAL(18,2))) END) AS DECIMAL(18,2)),0)) as decimal(18,2)) As [IFEInjuryRatio],
								cast((SELECT ISNULL(CAST(CASE WHEN (COALESCE(ISNULL(SUM(k.aheaTotalManHours),0),0) = 0) THEN (0) ELSE ((((ISNULL(SUM(k.ipLTI),0) + (ISNULL(SUM(k.ipRDI),0)))) * 200000) / (SUM(k.aheaTotalManHours))) END As Decimal(18,2)),0)) as decimal(18,2)) As [DART],        
								cast((SELECT ISNULL(((CAST((CASE  WHEN (SUM(COALESCE(k.ipRN,0)) = 0) THEN 0 ELSE (SUM(COALESCE(k.ipRN,0))) END) AS int))),0)) as decimal(18,2)) as [Risk Notifications],            
								cast(0 as decimal(18,2)) As [NoCsa] --No month field in CSA table?? 
							FROM	KpiCompanySiteCategory k 
									inner join (
										select	distinct SiteId, CompanySiteCategoryId, [Year] 
										from	AdhocRadarItems
										where	LEFT([SiteAbbrev], 1) <> '-' 
									) ari on ari.[Year] = YEAR(k.kpiDateTime) and ari.[SiteId] = k.SiteId and dbo.GetCompanySiteCategoryExceptionId(k.SiteId,k.CompanyId,k.kpiDateTime,k.CompanySiteCategoryId) LIKE ari.CompanySiteCategoryId 
									left join Months m on m.MonthId = MONTH(k.kpiDateTime) 
							GROUP BY ari.SiteId, ari.CompanySiteCategoryId, ari.[Year], m.MonthId, m.MonthAbbrev
							UNION
							SELECT  --Regions
								ari.SiteId,
								ari.CompanySiteCategoryId,
								ari.[Year],
								m.MonthId,
								m.MonthAbbrev,
								cast(0 as decimal(18,2)) As [Peak No Employees (Previous Month)],  --Previous month calculation only, no breakdown required??
								cast(ISNULL(SUM(aheaPeakNopplSiteWeek),0) as decimal(18,2)) As [Peak No. Of People On Site],              
								cast(ISNULL(SUM(aheaAvgNopplSiteMonth),0) as decimal(18,2)) As [Average No. Of People On Site],
								cast(COUNT(distinct(CompanyId)) as decimal(18,2)) As [Number Of Companies],
								cast((SELECT COUNT(DISTINCT CAST(CompanyId as varchar) + '-' + CAST(SiteId as varchar)) FROM	KpiCompanySiteCategory WHERE YEAR(kpiDateTime) = @Year AND MONTH(kpiDateTime) = m.MonthId AND dbo.GetCompanySiteCategoryExceptionId(SiteId,CompanyId,DateFromParts(Year(GetDate()),MONTH(dateadd("m",0, GETDATE())),1),CompanySiteCategoryId) LIKE ari.CompanySiteCategoryId AND ((SELECT COUNT(*) FROM RegionsSites WHERE RegionsSites.Regionid = ari.RegionId AND RegionsSites.SiteId = KpiCompanySiteCategory.SiteId) > 0) AND	(EbiOnSite = 1 OR AheaTotalManHours > 0)) as decimal(18,2)) As [Number of Companies On Site],
								cast(ISNULL(SUM(COALESCE(aheaTotalManHours,0)),0) as decimal(18,2)) As [Total Man Hours],
								cast(0 as decimal(18,2)) As [SmpNo], --Searching year on part of the description???  I can't filter this down by month as it's not in the description
								cast(0 as decimal(18,2)) As [SmpExpected],  --Again, not searching by year so how can I search by month??     
								cast(ISNULL(CAST((ISNULL((SUM(COALESCE(ipLTI,0)) * 200000),0) / NULLIF(ISNULL(COALESCE((SUM(COALESCE(aheaTotalManHours,0))),0),0),0)) AS Decimal(18,2)),0) as decimal(18,2)) As [LWDFR], 
								cast(ISNULL(((CAST((CASE WHEN (SUM(COALESCE(aheaTotalManHours,0)) = 0.0) THEN 0.0 ELSE (((SUM(COALESCE(ipMTI,0)) + SUM(COALESCE(ipRDI,0)) + SUM(COALESCE(ipLTI,0))) * 200000) / (SUM(COALESCE(aheaTotalManHours,0)))) END) AS DECIMAL(18,2)))),0) as decimal(18,2)) As [TRIFR],  
								cast(ISNULL(((CAST((CASE WHEN (SUM(COALESCE(aheaTotalManHours,0)) = 0.0) THEN 0.0 ELSE (((SUM(COALESCE(ipFATI,0)) + SUM(COALESCE(ipMTI,0)) + SUM(COALESCE(ipRDI,0)) + SUM(COALESCE(ipLTI,0))) * 200000) / (SUM(COALESCE(aheaTotalManHours,0)))) END) AS DECIMAL(18,2)))),0) as decimal(18,2)) As [AIFR],
								cast((SELECT	ISNULL(CAST((CASE WHEN ((SUM(COALESCE(k.ipLTI,0)) + SUM(COALESCE(k.ipRDI,0)) + SUM(COALESCE(k.ipMTI,0)) + SUM(COALESCE(k.ipFATI,0)) ) = 0.0) THEN CAST(SUM(COALESCE(k.ipIFE,0)) AS DECIMAL(18,2)) ELSE (CAST(SUM(COALESCE(k.ipIFE,0)) AS DECIMAL(18,2)) / CAST((SUM(COALESCE(k.ipLTI,0)) + SUM(COALESCE(k.ipRDI,0)) + SUM(COALESCE(k.ipMTI,0)) + SUM(COALESCE(k.ipFATI,0))) AS DECIMAL(18,2))) END) AS DECIMAL(18,2)),0)) as decimal(18,2)) As [IFEInjuryRatio],
								cast((SELECT ISNULL(CAST(CASE WHEN (COALESCE(ISNULL(SUM(k.aheaTotalManHours),0),0) = 0) THEN (0) ELSE ((((ISNULL(SUM(k.ipLTI),0) + (ISNULL(SUM(k.ipRDI),0)))) * 200000) / (SUM(k.aheaTotalManHours))) END As Decimal(18,2)),0)) as decimal(18,2)) As [DART],
								cast((SELECT ISNULL(((CAST((CASE WHEN (SUM(COALESCE(dbo.KpiCompanySiteCategory.ipRN,0)) = 0) THEN 0 ELSE (SUM(COALESCE(dbo.KpiCompanySiteCategory.ipRN,0))) END) AS int))),0) FROM dbo.KpiCompanySiteCategory WHERE YEAR(dbo.KpiCompanySiteCategory.kpiDateTime) = ari.Year AND	MONTH(dbo.KpiCompanySiteCategory.kpiDateTime) = m.MonthId AND KpiCompanySiteCategory.CompanySiteCategoryId LIKE ari.CompanySiteCategoryId AND ((SELECT COUNT(*) FROM dbo.RegionsSites WHERE dbo.RegionsSites.SiteId = dbo.KpiCompanySiteCategory.SiteId AND dbo.RegionsSites.RegionId = ari.RegionId) > 0)) as decimal(18,2)) as [Risk Notifications],                 
								cast(0 as decimal(18,2)) As [NoCsa] --No month field in CSA table??  
							FROM	KpiCompanySiteCategory k
									inner join (
										select	distinct SiteId, RegionId, CompanySiteCategoryId, [Year] 
										from	AdhocRadarItems
										where	LEFT([SiteAbbrev], 1) = '-' 
									) ari on ari.[Year] = YEAR(k.kpiDateTime) AND k.CompanySiteCategoryId LIKE ari.CompanySiteCategoryId AND ((SELECT COUNT(*) FROM RegionsSites WHERE RegionsSites.Regionid = ari.RegionId AND RegionsSites.SiteId = k.SiteId) > 0) 
									left join Months m on m.MonthId = MONTH(k.kpiDateTime)
							GROUP BY ari.RegionId, ari.SiteId, ari.CompanySiteCategoryId, ari.[Year], m.MonthId, m.MonthAbbrev
						) u
						unpivot (  --unpivot kpi columns as we want to put them as rows
							value
							for attribute in (
								[Peak No Employees (Previous Month)],
								[Peak No. Of People On Site],
								[Average No. Of People On Site],
								[Number Of Companies],
								[Number of Companies On Site],
								[Total Man Hours],
								[SmpNo],
								[SmpExpected],
								[LWDFR],
								[TRIFR],
								[AIFR],
								[IFEInjuryRatio],
								[DART],
								[Risk Notifications],
								[NoCsa]
							)
						) u
					) unpvt
					PIVOT  --pivot month as we want these rows as columns
					(
						sum([value])
						for MonthAbbrev in ([Jan], [Feb], [Mar], [Apr], [May], [Jun], [Jul], [Aug], [Sep], [Oct], [Nov], [Dec])
					) as pvt1
					group by [Year], [SiteId], [CompanySiteCategoryId], [KpiDescription]
				) arim on ari.[Year] = arim.[Year] and ari.[SiteId] = arim.[SiteId] and ari.[CompanySiteCategoryId] = arim.[CompanySiteCategoryId] and ari.[KpiDescription] = arim.[KpiDescription]
	), AdhocRadarItemsBatch
	As
	(
		--Perform further calculations on the returned score (for certain kpis)
		select	ari.[Year],
				ari.[KpiMeasureId],
				ari.[KpiDescription],
				ari.[CompanySiteCategoryId],
				ari.[CategoryName],
				ari.[CategoryDesc],
				ari.[SiteId],
				ari.[SiteName],
				ari.[SiteAbbrev],
				ari.[SiteNameEbi],
				ari.[RegionId],
				ari.[AdHoc_RadarId],
				ari.[AdHoc_Radar_ItemId],
				ari.[SmpNo],
				ari.[SmpExpected],
				case 
					when ari.[KpiDescription] = 'Safety Plan(s) Submitted' and ari.[SmpExpected] = 0
						then null --n/a
					when ari.[KpiDescription] = 'Safety Plan(s) Submitted' and ari.[SmpNo] <> 0 and ari.[SmpExpected] > 0
						then (cast(ari.[SmpNo] as DECIMAL(18,2)) / cast(ari.[SmpExpected] as DECIMAL(18,2))) * 100.0 
					when ari.[KpiDescription] = 'Avg No Employees (YTD)' and ari.[KpiScoreYtd] > 0
						then cast(ari.[KpiScoreYtd] as DECIMAL(18,2)) / @avgNoEmployees_Divisor
					else KpiScoreYtd
				end as KpiScoreYtd,
				ari.[KpiScoreJan],
				ari.[KpiScoreFeb],
				ari.[KpiScoreMar],
				ari.[KpiScoreApr],
				ari.[KpiScoreMay],
				ari.[KpiScoreJun],
				ari.[KpiScoreJul],
				ari.[KpiScoreAug],
				ari.[KpiScoreSep],
				ari.[KpiScoreOct],
				ari.[KpiScoreNov],
				ari.[KpiScoreDec]
		from	AdhocRadarItemsMonth ari
	)
	--, CscsrTlist
	--As
	--(
	--	SELECT	cscsr.*
	--	FROM	[dbo].[CompanySiteCategoryStandardRegion] cscsr
	--			inner join AdhocRadarItemsBatch ari on cscsr.[CompanySiteCategoryId] = ari.[CompanySiteCategoryId] 
	--												and ((ari.[RegionId] is not null and cscsr.[RegionId] = ari.[RegionId]) or (ari.[RegionId] is null and cscsr.[RegionId] = 2 and cscsr.[SiteId] = ari.[SiteId]))
	--			inner join [dbo].[KpiCompanySiteCategory] kcsc on cscsr.CompanyId = kcsc.CompanyId and cscsr.SiteId = kcsc.SiteId and ari.[Year] = YEAR(kcsc.kpiDateTime)
	--			inner join dbo.[CSA] c on cscsr.CompanyId = c.CompanyId and cscsr.SiteId = c.SiteId and c.[Year] = @Year
	--	WHERE	ari.[KpiDescription] = 'CSA - Self Audit (Conducted)'							
	--)
	--Insert adhoc_radar_item if not already exist, else update the score
	merge	dbo.[AdHoc_Radar_Items] as t
	using	AdhocRadarItemsBatch as s
	on		(t.AdHoc_Radar_ItemId = s.AdHoc_Radar_ItemId)
	when not matched by target and s.[KpiScoreYtd] is not null
		then insert([AdHoc_RadarId],[Year],[SiteId],[ModifiedDate],[CompaniesNotCompliant],
						[KpiScoreYtd], -- DT397 CT 25/11/15 added this back in
						[KpiScoreJan],[KpiScoreFeb],[KpiScoreMar],[KpiScoreApr],[KpiScoreMay],
						[KpiScoreJun],[KpiScoreJul],[KpiScoreAug],[KpiScoreSep],[KpiScoreOct],[KpiScoreNov],[KpiScoreDec])
		     values(s.[AdHoc_RadarId],s.[Year],s.[SiteId],getdate(),null,
						'n/a', -- DT397 CT 25/11/15 added this back in, was --s.[KpiScoreYtd],
						s.[KpiScoreJan],s.[KpiScoreFeb],s.[KpiScoreMar],s.[KpiScoreApr],s.[KpiScoreMay],
						s.[KpiScoreJun],s.[KpiScoreJul],s.[KpiScoreAug],s.[KpiScoreSep],s.[KpiScoreOct],s.[KpiScoreNov],s.[KpiScoreDec])
	when matched and s.[KpiScoreYtd] is not null
		then update set t.[ModifiedDate]=getdate(), 
						--t.[KpiScoreYtd]=s.[KpiScoreYtd], 
						t.[KpiScoreJan]=s.[KpiScoreJan], t.[KpiScoreFeb]=s.[KpiScoreFeb],
						t.[KpiScoreMar]=s.[KpiScoreMar], t.[KpiScoreApr]=s.[KpiScoreApr], t.[KpiScoreMay]=s.[KpiScoreMay], t.[KpiScoreJun]=s.[KpiScoreJun],
						t.[KpiScoreJul]=s.[KpiScoreJul], t.[KpiScoreAug]=s.[KpiScoreAug], t.[KpiScoreSep]=s.[KpiScoreSep], t.[KpiScoreOct]=s.[KpiScoreOct],
						t.[KpiScoreNov]=s.[KpiScoreNov], t.[KpiScoreDec]=s.[KpiScoreDec];
	--select	*
	--from	CscsrTlist;

	set @fromYear = @fromYear + 1;
end

print 'End: ' + cast(getdate() as varchar(max));





GO


