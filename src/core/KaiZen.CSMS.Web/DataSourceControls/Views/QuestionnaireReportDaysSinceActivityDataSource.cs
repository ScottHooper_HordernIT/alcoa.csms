﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.QuestionnaireReportDaysSinceActivityProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[CLSCompliant(true)]
	[Designer(typeof(QuestionnaireReportDaysSinceActivityDataSourceDesigner))]
	public class QuestionnaireReportDaysSinceActivityDataSource : ReadOnlyDataSource<QuestionnaireReportDaysSinceActivity>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportDaysSinceActivityDataSource class.
		/// </summary>
		public QuestionnaireReportDaysSinceActivityDataSource() : base(new QuestionnaireReportDaysSinceActivityService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the QuestionnaireReportDaysSinceActivityDataSourceView used by the QuestionnaireReportDaysSinceActivityDataSource.
		/// </summary>
		protected QuestionnaireReportDaysSinceActivityDataSourceView QuestionnaireReportDaysSinceActivityView
		{
			get { return ( View as QuestionnaireReportDaysSinceActivityDataSourceView ); }
		}
		
		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the QuestionnaireReportDaysSinceActivityDataSourceView class that is to be
		/// used by the QuestionnaireReportDaysSinceActivityDataSource.
		/// </summary>
		/// <returns>An instance of the QuestionnaireReportDaysSinceActivityDataSourceView class.</returns>
		protected override BaseDataSourceView<QuestionnaireReportDaysSinceActivity, Object> GetNewDataSourceView()
		{
			return new QuestionnaireReportDaysSinceActivityDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the QuestionnaireReportDaysSinceActivityDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class QuestionnaireReportDaysSinceActivityDataSourceView : ReadOnlyDataSourceView<QuestionnaireReportDaysSinceActivity>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportDaysSinceActivityDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the QuestionnaireReportDaysSinceActivityDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public QuestionnaireReportDaysSinceActivityDataSourceView(QuestionnaireReportDaysSinceActivityDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal QuestionnaireReportDaysSinceActivityDataSource QuestionnaireReportDaysSinceActivityOwner
		{
			get { return Owner as QuestionnaireReportDaysSinceActivityDataSource; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal QuestionnaireReportDaysSinceActivityService QuestionnaireReportDaysSinceActivityProvider
		{
			get { return Provider as QuestionnaireReportDaysSinceActivityService; }
		}

		#endregion Properties
		
		#region Methods
		
		#endregion Methods
	}

	#region QuestionnaireReportDaysSinceActivityDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the QuestionnaireReportDaysSinceActivityDataSource class.
	/// </summary>
	public class QuestionnaireReportDaysSinceActivityDataSourceDesigner : ReadOnlyDataSourceDesigner<QuestionnaireReportDaysSinceActivity>
	{
	}

	#endregion QuestionnaireReportDaysSinceActivityDataSourceDesigner

	#region QuestionnaireReportDaysSinceActivityFilter

	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireReportDaysSinceActivity"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireReportDaysSinceActivityFilter : SqlFilter<QuestionnaireReportDaysSinceActivityColumn>
	{
	}

	#endregion QuestionnaireReportDaysSinceActivityFilter

	#region QuestionnaireReportDaysSinceActivityExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireReportDaysSinceActivity"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireReportDaysSinceActivityExpressionBuilder : SqlExpressionBuilder<QuestionnaireReportDaysSinceActivityColumn>
	{
	}
	
	#endregion QuestionnaireReportDaysSinceActivityExpressionBuilder		
}

