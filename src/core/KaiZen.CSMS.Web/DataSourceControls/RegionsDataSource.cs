﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.RegionsProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(RegionsDataSourceDesigner))]
	public class RegionsDataSource : ProviderDataSource<Regions, RegionsKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the RegionsDataSource class.
		/// </summary>
		public RegionsDataSource() : base(new RegionsService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the RegionsDataSourceView used by the RegionsDataSource.
		/// </summary>
		protected RegionsDataSourceView RegionsView
		{
			get { return ( View as RegionsDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the RegionsDataSource control invokes to retrieve data.
		/// </summary>
		public RegionsSelectMethod SelectMethod
		{
			get
			{
				RegionsSelectMethod selectMethod = RegionsSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (RegionsSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the RegionsDataSourceView class that is to be
		/// used by the RegionsDataSource.
		/// </summary>
		/// <returns>An instance of the RegionsDataSourceView class.</returns>
		protected override BaseDataSourceView<Regions, RegionsKey> GetNewDataSourceView()
		{
			return new RegionsDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the RegionsDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class RegionsDataSourceView : ProviderDataSourceView<Regions, RegionsKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the RegionsDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the RegionsDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public RegionsDataSourceView(RegionsDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal RegionsDataSource RegionsOwner
		{
			get { return Owner as RegionsDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal RegionsSelectMethod SelectMethod
		{
			get { return RegionsOwner.SelectMethod; }
			set { RegionsOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal RegionsService RegionsProvider
		{
			get { return Provider as RegionsService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<Regions> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<Regions> results = null;
			Regions item;
			count = 0;
			
			System.Int32 _regionId;
			System.String _regionInternalName_nullable;
			System.String _regionName;
			System.String _regionNameAbbrev_nullable;
			System.Int32 _regionValue;

			switch ( SelectMethod )
			{
				case RegionsSelectMethod.Get:
					RegionsKey entityKey  = new RegionsKey();
					entityKey.Load(values);
					item = RegionsProvider.Get(entityKey);
					results = new TList<Regions>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case RegionsSelectMethod.GetAll:
                    results = RegionsProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case RegionsSelectMethod.GetPaged:
					results = RegionsProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case RegionsSelectMethod.Find:
					if ( FilterParameters != null )
						results = RegionsProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = RegionsProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case RegionsSelectMethod.GetByRegionId:
					_regionId = ( values["RegionId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["RegionId"], typeof(System.Int32)) : (int)0;
					item = RegionsProvider.GetByRegionId(_regionId);
					results = new TList<Regions>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				case RegionsSelectMethod.GetByRegionInternalName:
					_regionInternalName_nullable = (System.String) EntityUtil.ChangeType(values["RegionInternalName"], typeof(System.String));
					item = RegionsProvider.GetByRegionInternalName(_regionInternalName_nullable);
					results = new TList<Regions>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case RegionsSelectMethod.GetByRegionName:
					_regionName = ( values["RegionName"] != null ) ? (System.String) EntityUtil.ChangeType(values["RegionName"], typeof(System.String)) : string.Empty;
					item = RegionsProvider.GetByRegionName(_regionName);
					results = new TList<Regions>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case RegionsSelectMethod.GetByRegionNameAbbrev:
					_regionNameAbbrev_nullable = (System.String) EntityUtil.ChangeType(values["RegionNameAbbrev"], typeof(System.String));
					results = RegionsProvider.GetByRegionNameAbbrev(_regionNameAbbrev_nullable, this.StartIndex, this.PageSize, out count);
					break;
				case RegionsSelectMethod.GetByRegionValue:
					_regionValue = ( values["RegionValue"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["RegionValue"], typeof(System.Int32)) : (int)0;
					item = RegionsProvider.GetByRegionValue(_regionValue);
					results = new TList<Regions>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// FK
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == RegionsSelectMethod.Get || SelectMethod == RegionsSelectMethod.GetByRegionId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				Regions entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					RegionsProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<Regions> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			RegionsProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region RegionsDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the RegionsDataSource class.
	/// </summary>
	public class RegionsDataSourceDesigner : ProviderDataSourceDesigner<Regions, RegionsKey>
	{
		/// <summary>
		/// Initializes a new instance of the RegionsDataSourceDesigner class.
		/// </summary>
		public RegionsDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public RegionsSelectMethod SelectMethod
		{
			get { return ((RegionsDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new RegionsDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region RegionsDataSourceActionList

	/// <summary>
	/// Supports the RegionsDataSourceDesigner class.
	/// </summary>
	internal class RegionsDataSourceActionList : DesignerActionList
	{
		private RegionsDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the RegionsDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public RegionsDataSourceActionList(RegionsDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public RegionsSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion RegionsDataSourceActionList
	
	#endregion RegionsDataSourceDesigner
	
	#region RegionsSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the RegionsDataSource.SelectMethod property.
	/// </summary>
	public enum RegionsSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByRegionId method.
		/// </summary>
		GetByRegionId,
		/// <summary>
		/// Represents the GetByRegionInternalName method.
		/// </summary>
		GetByRegionInternalName,
		/// <summary>
		/// Represents the GetByRegionName method.
		/// </summary>
		GetByRegionName,
		/// <summary>
		/// Represents the GetByRegionNameAbbrev method.
		/// </summary>
		GetByRegionNameAbbrev,
		/// <summary>
		/// Represents the GetByRegionValue method.
		/// </summary>
		GetByRegionValue
	}
	
	#endregion RegionsSelectMethod

	#region RegionsFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Regions"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class RegionsFilter : SqlFilter<RegionsColumn>
	{
	}
	
	#endregion RegionsFilter

	#region RegionsExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Regions"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class RegionsExpressionBuilder : SqlExpressionBuilder<RegionsColumn>
	{
	}
	
	#endregion RegionsExpressionBuilder	

	#region RegionsProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;RegionsChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="Regions"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class RegionsProperty : ChildEntityProperty<RegionsChildEntityTypes>
	{
	}
	
	#endregion RegionsProperty
}

