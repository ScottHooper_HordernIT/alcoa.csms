﻿#region Using directives

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="QuestionnaireReportServicesOtherProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract class QuestionnaireReportServicesOtherProviderBaseCore : EntityViewProviderBase<QuestionnaireReportServicesOther>
	{
		#region Custom Methods
		
		
		#region _QuestionnaireReportServicesOther_GetAllWithEbiActiveSitesThisYear
		
		/// <summary>
		///	This method wrap the '_QuestionnaireReportServicesOther_GetAllWithEbiActiveSitesThisYear' stored procedure. 
		/// </summary>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetAllWithEbiActiveSitesThisYear()
		{
			return GetAllWithEbiActiveSitesThisYear(null, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_QuestionnaireReportServicesOther_GetAllWithEbiActiveSitesThisYear' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetAllWithEbiActiveSitesThisYear(int start, int pageLength)
		{
			return GetAllWithEbiActiveSitesThisYear(null, start, pageLength );
		}
				
		/// <summary>
		///	This method wrap the '_QuestionnaireReportServicesOther_GetAllWithEbiActiveSitesThisYear' stored procedure. 
		/// </summary>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetAllWithEbiActiveSitesThisYear(TransactionManager transactionManager)
		{
			return GetAllWithEbiActiveSitesThisYear(transactionManager, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_QuestionnaireReportServicesOther_GetAllWithEbiActiveSitesThisYear' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet GetAllWithEbiActiveSitesThisYear(TransactionManager transactionManager, int start, int pageLength);
		
		#endregion

		
		#region _QuestionnaireReportServicesOther_GetAllWithEbiActiveSitesThisYear_ByCompanyId
		
		/// <summary>
		///	This method wrap the '_QuestionnaireReportServicesOther_GetAllWithEbiActiveSitesThisYear_ByCompanyId' stored procedure. 
		/// </summary>
		/// <param name="sCompanyId"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetAllWithEbiActiveSitesThisYear_ByCompanyId(System.Int32? sCompanyId)
		{
			return GetAllWithEbiActiveSitesThisYear_ByCompanyId(null, 0, int.MaxValue , sCompanyId);
		}
		
		/// <summary>
		///	This method wrap the '_QuestionnaireReportServicesOther_GetAllWithEbiActiveSitesThisYear_ByCompanyId' stored procedure. 
		/// </summary>
		/// <param name="sCompanyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetAllWithEbiActiveSitesThisYear_ByCompanyId(int start, int pageLength, System.Int32? sCompanyId)
		{
			return GetAllWithEbiActiveSitesThisYear_ByCompanyId(null, start, pageLength , sCompanyId);
		}
				
		/// <summary>
		///	This method wrap the '_QuestionnaireReportServicesOther_GetAllWithEbiActiveSitesThisYear_ByCompanyId' stored procedure. 
		/// </summary>
		/// <param name="sCompanyId"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetAllWithEbiActiveSitesThisYear_ByCompanyId(TransactionManager transactionManager, System.Int32? sCompanyId)
		{
			return GetAllWithEbiActiveSitesThisYear_ByCompanyId(transactionManager, 0, int.MaxValue , sCompanyId);
		}
		
		/// <summary>
		///	This method wrap the '_QuestionnaireReportServicesOther_GetAllWithEbiActiveSitesThisYear_ByCompanyId' stored procedure. 
		/// </summary>
		/// <param name="sCompanyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet GetAllWithEbiActiveSitesThisYear_ByCompanyId(TransactionManager transactionManager, int start, int pageLength, System.Int32? sCompanyId);
		
		#endregion

		
		#endregion

		#region Helper Functions
		
		/*
		///<summary>
		/// Fill an VList&lt;QuestionnaireReportServicesOther&gt; From a DataSet
		///</summary>
		/// <param name="dataSet">the DataSet</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList&lt;QuestionnaireReportServicesOther&gt;"/></returns>
		protected static VList&lt;QuestionnaireReportServicesOther&gt; Fill(DataSet dataSet, VList<QuestionnaireReportServicesOther> rows, int start, int pagelen)
		{
			if (dataSet.Tables.Count == 1)
			{
				return Fill(dataSet.Tables[0], rows, start, pagelen);
			}
			else
			{
				return new VList<QuestionnaireReportServicesOther>();
			}	
		}
		
		
		///<summary>
		/// Fill an VList&lt;QuestionnaireReportServicesOther&gt; From a DataTable
		///</summary>
		/// <param name="dataTable">the DataTable that hold the data.</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList<QuestionnaireReportServicesOther>"/></returns>
		protected static VList&lt;QuestionnaireReportServicesOther&gt; Fill(DataTable dataTable, VList<QuestionnaireReportServicesOther> rows, int start, int pagelen)
		{
			int recordnum = 0;
			
			System.Collections.IEnumerator dataRows =  dataTable.Rows.GetEnumerator();
			
			while (dataRows.MoveNext() && (pagelen != 0))
			{
				if(recordnum >= start)
				{
					DataRow row = (DataRow)dataRows.Current;
				
					QuestionnaireReportServicesOther c = new QuestionnaireReportServicesOther();
					c.CompanyName = (Convert.IsDBNull(row["CompanyName"]))?string.Empty:(System.String)row["CompanyName"];
					c.QuestionnaireId = (Convert.IsDBNull(row["QuestionnaireId"]))?(int)0:(System.Int32)row["QuestionnaireId"];
					c.CompanyId = (Convert.IsDBNull(row["CompanyId"]))?(int)0:(System.Int32)row["CompanyId"];
					c.Status = (Convert.IsDBNull(row["Status"]))?(int)0:(System.Int32)row["Status"];
					c.Recommended = (Convert.IsDBNull(row["Recommended"]))?false:(System.Boolean?)row["Recommended"];
					c.CreatedByUserId = (Convert.IsDBNull(row["CreatedByUserId"]))?(int)0:(System.Int32)row["CreatedByUserId"];
					c.CreatedDate = (Convert.IsDBNull(row["CreatedDate"]))?DateTime.MinValue:(System.DateTime)row["CreatedDate"];
					c.ModifiedByUserId = (Convert.IsDBNull(row["ModifiedByUserId"]))?(int)0:(System.Int32)row["ModifiedByUserId"];
					c.ModifiedDate = (Convert.IsDBNull(row["ModifiedDate"]))?DateTime.MinValue:(System.DateTime)row["ModifiedDate"];
					c.ApprovedByUserId = (Convert.IsDBNull(row["ApprovedByUserId"]))?(int)0:(System.Int32?)row["ApprovedByUserId"];
					c.ApprovedDate = (Convert.IsDBNull(row["ApprovedDate"]))?DateTime.MinValue:(System.DateTime?)row["ApprovedDate"];
					c.LevelOfSupervision = (Convert.IsDBNull(row["LevelOfSupervision"]))?string.Empty:(System.String)row["LevelOfSupervision"];
					c.CompanyStatusDesc = (Convert.IsDBNull(row["CompanyStatusDesc"]))?string.Empty:(System.String)row["CompanyStatusDesc"];
					c.AnswerId = (Convert.IsDBNull(row["AnswerId"]))?(int)0:(System.Int32)row["AnswerId"];
					c.AnswerText = (Convert.IsDBNull(row["AnswerText"]))?string.Empty:(System.String)row["AnswerText"];
					c.Type = (Convert.IsDBNull(row["Type"]))?string.Empty:(System.String)row["Type"];
					c.AcceptChanges();
					rows.Add(c);
					pagelen -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		*/	
						
		///<summary>
		/// Fill an <see cref="VList&lt;QuestionnaireReportServicesOther&gt;"/> From a DataReader.
		///</summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pageLength">number of row.</param>
		///<returns>a <see cref="VList&lt;QuestionnaireReportServicesOther&gt;"/></returns>
		protected VList<QuestionnaireReportServicesOther> Fill(IDataReader reader, VList<QuestionnaireReportServicesOther> rows, int start, int pageLength)
		{
			int recordnum = 0;
			while (reader.Read() && (pageLength != 0))
			{
				if(recordnum >= start)
				{
					QuestionnaireReportServicesOther entity = null;
					if (DataRepository.Provider.UseEntityFactory)
					{
						entity = EntityManager.CreateViewEntity<QuestionnaireReportServicesOther>("QuestionnaireReportServicesOther",  DataRepository.Provider.EntityCreationalFactoryType); 
					}
					else
					{
						entity = new QuestionnaireReportServicesOther();
					}
					
					entity.SuppressEntityEvents = true;

					entity.CompanyName = (System.String)reader[((int)QuestionnaireReportServicesOtherColumn.CompanyName)];
					//entity.CompanyName = (Convert.IsDBNull(reader["CompanyName"]))?string.Empty:(System.String)reader["CompanyName"];
					entity.QuestionnaireId = (System.Int32)reader[((int)QuestionnaireReportServicesOtherColumn.QuestionnaireId)];
					//entity.QuestionnaireId = (Convert.IsDBNull(reader["QuestionnaireId"]))?(int)0:(System.Int32)reader["QuestionnaireId"];
					entity.CompanyId = (System.Int32)reader[((int)QuestionnaireReportServicesOtherColumn.CompanyId)];
					//entity.CompanyId = (Convert.IsDBNull(reader["CompanyId"]))?(int)0:(System.Int32)reader["CompanyId"];
					entity.Status = (System.Int32)reader[((int)QuestionnaireReportServicesOtherColumn.Status)];
					//entity.Status = (Convert.IsDBNull(reader["Status"]))?(int)0:(System.Int32)reader["Status"];
					entity.Recommended = (reader.IsDBNull(((int)QuestionnaireReportServicesOtherColumn.Recommended)))?null:(System.Boolean?)reader[((int)QuestionnaireReportServicesOtherColumn.Recommended)];
					//entity.Recommended = (Convert.IsDBNull(reader["Recommended"]))?false:(System.Boolean?)reader["Recommended"];
					entity.CreatedByUserId = (System.Int32)reader[((int)QuestionnaireReportServicesOtherColumn.CreatedByUserId)];
					//entity.CreatedByUserId = (Convert.IsDBNull(reader["CreatedByUserId"]))?(int)0:(System.Int32)reader["CreatedByUserId"];
					entity.CreatedDate = (System.DateTime)reader[((int)QuestionnaireReportServicesOtherColumn.CreatedDate)];
					//entity.CreatedDate = (Convert.IsDBNull(reader["CreatedDate"]))?DateTime.MinValue:(System.DateTime)reader["CreatedDate"];
					entity.ModifiedByUserId = (System.Int32)reader[((int)QuestionnaireReportServicesOtherColumn.ModifiedByUserId)];
					//entity.ModifiedByUserId = (Convert.IsDBNull(reader["ModifiedByUserId"]))?(int)0:(System.Int32)reader["ModifiedByUserId"];
					entity.ModifiedDate = (System.DateTime)reader[((int)QuestionnaireReportServicesOtherColumn.ModifiedDate)];
					//entity.ModifiedDate = (Convert.IsDBNull(reader["ModifiedDate"]))?DateTime.MinValue:(System.DateTime)reader["ModifiedDate"];
					entity.ApprovedByUserId = (reader.IsDBNull(((int)QuestionnaireReportServicesOtherColumn.ApprovedByUserId)))?null:(System.Int32?)reader[((int)QuestionnaireReportServicesOtherColumn.ApprovedByUserId)];
					//entity.ApprovedByUserId = (Convert.IsDBNull(reader["ApprovedByUserId"]))?(int)0:(System.Int32?)reader["ApprovedByUserId"];
					entity.ApprovedDate = (reader.IsDBNull(((int)QuestionnaireReportServicesOtherColumn.ApprovedDate)))?null:(System.DateTime?)reader[((int)QuestionnaireReportServicesOtherColumn.ApprovedDate)];
					//entity.ApprovedDate = (Convert.IsDBNull(reader["ApprovedDate"]))?DateTime.MinValue:(System.DateTime?)reader["ApprovedDate"];
					entity.LevelOfSupervision = (reader.IsDBNull(((int)QuestionnaireReportServicesOtherColumn.LevelOfSupervision)))?null:(System.String)reader[((int)QuestionnaireReportServicesOtherColumn.LevelOfSupervision)];
					//entity.LevelOfSupervision = (Convert.IsDBNull(reader["LevelOfSupervision"]))?string.Empty:(System.String)reader["LevelOfSupervision"];
					entity.CompanyStatusDesc = (reader.IsDBNull(((int)QuestionnaireReportServicesOtherColumn.CompanyStatusDesc)))?null:(System.String)reader[((int)QuestionnaireReportServicesOtherColumn.CompanyStatusDesc)];
					//entity.CompanyStatusDesc = (Convert.IsDBNull(reader["CompanyStatusDesc"]))?string.Empty:(System.String)reader["CompanyStatusDesc"];
					entity.AnswerId = (System.Int32)reader[((int)QuestionnaireReportServicesOtherColumn.AnswerId)];
					//entity.AnswerId = (Convert.IsDBNull(reader["AnswerId"]))?(int)0:(System.Int32)reader["AnswerId"];
					entity.AnswerText = (reader.IsDBNull(((int)QuestionnaireReportServicesOtherColumn.AnswerText)))?null:(System.String)reader[((int)QuestionnaireReportServicesOtherColumn.AnswerText)];
					//entity.AnswerText = (Convert.IsDBNull(reader["AnswerText"]))?string.Empty:(System.String)reader["AnswerText"];
					entity.Type = (System.String)reader[((int)QuestionnaireReportServicesOtherColumn.Type)];
					//entity.Type = (Convert.IsDBNull(reader["Type"]))?string.Empty:(System.String)reader["Type"];
					entity.AcceptChanges();
					entity.SuppressEntityEvents = false;
					
					rows.Add(entity);
					pageLength -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		
		
		/// <summary>
		/// Refreshes the <see cref="QuestionnaireReportServicesOther"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="QuestionnaireReportServicesOther"/> object to refresh.</param>
		protected void RefreshEntity(IDataReader reader, QuestionnaireReportServicesOther entity)
		{
			reader.Read();
			entity.CompanyName = (System.String)reader[((int)QuestionnaireReportServicesOtherColumn.CompanyName)];
			//entity.CompanyName = (Convert.IsDBNull(reader["CompanyName"]))?string.Empty:(System.String)reader["CompanyName"];
			entity.QuestionnaireId = (System.Int32)reader[((int)QuestionnaireReportServicesOtherColumn.QuestionnaireId)];
			//entity.QuestionnaireId = (Convert.IsDBNull(reader["QuestionnaireId"]))?(int)0:(System.Int32)reader["QuestionnaireId"];
			entity.CompanyId = (System.Int32)reader[((int)QuestionnaireReportServicesOtherColumn.CompanyId)];
			//entity.CompanyId = (Convert.IsDBNull(reader["CompanyId"]))?(int)0:(System.Int32)reader["CompanyId"];
			entity.Status = (System.Int32)reader[((int)QuestionnaireReportServicesOtherColumn.Status)];
			//entity.Status = (Convert.IsDBNull(reader["Status"]))?(int)0:(System.Int32)reader["Status"];
			entity.Recommended = (reader.IsDBNull(((int)QuestionnaireReportServicesOtherColumn.Recommended)))?null:(System.Boolean?)reader[((int)QuestionnaireReportServicesOtherColumn.Recommended)];
			//entity.Recommended = (Convert.IsDBNull(reader["Recommended"]))?false:(System.Boolean?)reader["Recommended"];
			entity.CreatedByUserId = (System.Int32)reader[((int)QuestionnaireReportServicesOtherColumn.CreatedByUserId)];
			//entity.CreatedByUserId = (Convert.IsDBNull(reader["CreatedByUserId"]))?(int)0:(System.Int32)reader["CreatedByUserId"];
			entity.CreatedDate = (System.DateTime)reader[((int)QuestionnaireReportServicesOtherColumn.CreatedDate)];
			//entity.CreatedDate = (Convert.IsDBNull(reader["CreatedDate"]))?DateTime.MinValue:(System.DateTime)reader["CreatedDate"];
			entity.ModifiedByUserId = (System.Int32)reader[((int)QuestionnaireReportServicesOtherColumn.ModifiedByUserId)];
			//entity.ModifiedByUserId = (Convert.IsDBNull(reader["ModifiedByUserId"]))?(int)0:(System.Int32)reader["ModifiedByUserId"];
			entity.ModifiedDate = (System.DateTime)reader[((int)QuestionnaireReportServicesOtherColumn.ModifiedDate)];
			//entity.ModifiedDate = (Convert.IsDBNull(reader["ModifiedDate"]))?DateTime.MinValue:(System.DateTime)reader["ModifiedDate"];
			entity.ApprovedByUserId = (reader.IsDBNull(((int)QuestionnaireReportServicesOtherColumn.ApprovedByUserId)))?null:(System.Int32?)reader[((int)QuestionnaireReportServicesOtherColumn.ApprovedByUserId)];
			//entity.ApprovedByUserId = (Convert.IsDBNull(reader["ApprovedByUserId"]))?(int)0:(System.Int32?)reader["ApprovedByUserId"];
			entity.ApprovedDate = (reader.IsDBNull(((int)QuestionnaireReportServicesOtherColumn.ApprovedDate)))?null:(System.DateTime?)reader[((int)QuestionnaireReportServicesOtherColumn.ApprovedDate)];
			//entity.ApprovedDate = (Convert.IsDBNull(reader["ApprovedDate"]))?DateTime.MinValue:(System.DateTime?)reader["ApprovedDate"];
			entity.LevelOfSupervision = (reader.IsDBNull(((int)QuestionnaireReportServicesOtherColumn.LevelOfSupervision)))?null:(System.String)reader[((int)QuestionnaireReportServicesOtherColumn.LevelOfSupervision)];
			//entity.LevelOfSupervision = (Convert.IsDBNull(reader["LevelOfSupervision"]))?string.Empty:(System.String)reader["LevelOfSupervision"];
			entity.CompanyStatusDesc = (reader.IsDBNull(((int)QuestionnaireReportServicesOtherColumn.CompanyStatusDesc)))?null:(System.String)reader[((int)QuestionnaireReportServicesOtherColumn.CompanyStatusDesc)];
			//entity.CompanyStatusDesc = (Convert.IsDBNull(reader["CompanyStatusDesc"]))?string.Empty:(System.String)reader["CompanyStatusDesc"];
			entity.AnswerId = (System.Int32)reader[((int)QuestionnaireReportServicesOtherColumn.AnswerId)];
			//entity.AnswerId = (Convert.IsDBNull(reader["AnswerId"]))?(int)0:(System.Int32)reader["AnswerId"];
			entity.AnswerText = (reader.IsDBNull(((int)QuestionnaireReportServicesOtherColumn.AnswerText)))?null:(System.String)reader[((int)QuestionnaireReportServicesOtherColumn.AnswerText)];
			//entity.AnswerText = (Convert.IsDBNull(reader["AnswerText"]))?string.Empty:(System.String)reader["AnswerText"];
			entity.Type = (System.String)reader[((int)QuestionnaireReportServicesOtherColumn.Type)];
			//entity.Type = (Convert.IsDBNull(reader["Type"]))?string.Empty:(System.String)reader["Type"];
			reader.Close();
	
			entity.AcceptChanges();
		}
		
		/*
		/// <summary>
		/// Refreshes the <see cref="QuestionnaireReportServicesOther"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="QuestionnaireReportServicesOther"/> object.</param>
		protected static void RefreshEntity(DataSet dataSet, QuestionnaireReportServicesOther entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.CompanyName = (Convert.IsDBNull(dataRow["CompanyName"]))?string.Empty:(System.String)dataRow["CompanyName"];
			entity.QuestionnaireId = (Convert.IsDBNull(dataRow["QuestionnaireId"]))?(int)0:(System.Int32)dataRow["QuestionnaireId"];
			entity.CompanyId = (Convert.IsDBNull(dataRow["CompanyId"]))?(int)0:(System.Int32)dataRow["CompanyId"];
			entity.Status = (Convert.IsDBNull(dataRow["Status"]))?(int)0:(System.Int32)dataRow["Status"];
			entity.Recommended = (Convert.IsDBNull(dataRow["Recommended"]))?false:(System.Boolean?)dataRow["Recommended"];
			entity.CreatedByUserId = (Convert.IsDBNull(dataRow["CreatedByUserId"]))?(int)0:(System.Int32)dataRow["CreatedByUserId"];
			entity.CreatedDate = (Convert.IsDBNull(dataRow["CreatedDate"]))?DateTime.MinValue:(System.DateTime)dataRow["CreatedDate"];
			entity.ModifiedByUserId = (Convert.IsDBNull(dataRow["ModifiedByUserId"]))?(int)0:(System.Int32)dataRow["ModifiedByUserId"];
			entity.ModifiedDate = (Convert.IsDBNull(dataRow["ModifiedDate"]))?DateTime.MinValue:(System.DateTime)dataRow["ModifiedDate"];
			entity.ApprovedByUserId = (Convert.IsDBNull(dataRow["ApprovedByUserId"]))?(int)0:(System.Int32?)dataRow["ApprovedByUserId"];
			entity.ApprovedDate = (Convert.IsDBNull(dataRow["ApprovedDate"]))?DateTime.MinValue:(System.DateTime?)dataRow["ApprovedDate"];
			entity.LevelOfSupervision = (Convert.IsDBNull(dataRow["LevelOfSupervision"]))?string.Empty:(System.String)dataRow["LevelOfSupervision"];
			entity.CompanyStatusDesc = (Convert.IsDBNull(dataRow["CompanyStatusDesc"]))?string.Empty:(System.String)dataRow["CompanyStatusDesc"];
			entity.AnswerId = (Convert.IsDBNull(dataRow["AnswerId"]))?(int)0:(System.Int32)dataRow["AnswerId"];
			entity.AnswerText = (Convert.IsDBNull(dataRow["AnswerText"]))?string.Empty:(System.String)dataRow["AnswerText"];
			entity.Type = (Convert.IsDBNull(dataRow["Type"]))?string.Empty:(System.String)dataRow["Type"];
			entity.AcceptChanges();
		}
		*/
			
		#endregion Helper Functions
	}//end class

	#region QuestionnaireReportServicesOtherFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireReportServicesOther"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireReportServicesOtherFilterBuilder : SqlFilterBuilder<QuestionnaireReportServicesOtherColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportServicesOtherFilterBuilder class.
		/// </summary>
		public QuestionnaireReportServicesOtherFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportServicesOtherFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireReportServicesOtherFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportServicesOtherFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireReportServicesOtherFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireReportServicesOtherFilterBuilder

	#region QuestionnaireReportServicesOtherParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireReportServicesOther"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireReportServicesOtherParameterBuilder : ParameterizedSqlFilterBuilder<QuestionnaireReportServicesOtherColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportServicesOtherParameterBuilder class.
		/// </summary>
		public QuestionnaireReportServicesOtherParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportServicesOtherParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireReportServicesOtherParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportServicesOtherParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireReportServicesOtherParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireReportServicesOtherParameterBuilder
	
	#region QuestionnaireReportServicesOtherSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireReportServicesOther"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class QuestionnaireReportServicesOtherSortBuilder : SqlSortBuilder<QuestionnaireReportServicesOtherColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportServicesOtherSqlSortBuilder class.
		/// </summary>
		public QuestionnaireReportServicesOtherSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion QuestionnaireReportServicesOtherSortBuilder

} // end namespace
