﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="KpiProjectsProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class KpiProjectsProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.KpiProjects, KaiZen.CSMS.Entities.KpiProjectsKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.KpiProjectsKey key)
		{
			return Delete(transactionManager, key.KpiProjectId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_kpiProjectId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _kpiProjectId)
		{
			return Delete(null, _kpiProjectId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_kpiProjectId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _kpiProjectId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_KpiProjects_Kpi_KpiId key.
		///		FK_KpiProjects_Kpi_KpiId Description: 
		/// </summary>
		/// <param name="_kpiId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.KpiProjects objects.</returns>
		public TList<KpiProjects> GetByKpiId(System.Int32 _kpiId)
		{
			int count = -1;
			return GetByKpiId(_kpiId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_KpiProjects_Kpi_KpiId key.
		///		FK_KpiProjects_Kpi_KpiId Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_kpiId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.KpiProjects objects.</returns>
		/// <remarks></remarks>
		public TList<KpiProjects> GetByKpiId(TransactionManager transactionManager, System.Int32 _kpiId)
		{
			int count = -1;
			return GetByKpiId(transactionManager, _kpiId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_KpiProjects_Kpi_KpiId key.
		///		FK_KpiProjects_Kpi_KpiId Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_kpiId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.KpiProjects objects.</returns>
		public TList<KpiProjects> GetByKpiId(TransactionManager transactionManager, System.Int32 _kpiId, int start, int pageLength)
		{
			int count = -1;
			return GetByKpiId(transactionManager, _kpiId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_KpiProjects_Kpi_KpiId key.
		///		fkKpiProjectsKpiKpiId Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_kpiId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.KpiProjects objects.</returns>
		public TList<KpiProjects> GetByKpiId(System.Int32 _kpiId, int start, int pageLength)
		{
			int count =  -1;
			return GetByKpiId(null, _kpiId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_KpiProjects_Kpi_KpiId key.
		///		fkKpiProjectsKpiKpiId Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_kpiId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.KpiProjects objects.</returns>
		public TList<KpiProjects> GetByKpiId(System.Int32 _kpiId, int start, int pageLength,out int count)
		{
			return GetByKpiId(null, _kpiId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_KpiProjects_Kpi_KpiId key.
		///		FK_KpiProjects_Kpi_KpiId Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_kpiId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.KpiProjects objects.</returns>
		public abstract TList<KpiProjects> GetByKpiId(TransactionManager transactionManager, System.Int32 _kpiId, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.KpiProjects Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.KpiProjectsKey key, int start, int pageLength)
		{
			return GetByKpiProjectId(transactionManager, key.KpiProjectId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_KpiProjects index.
		/// </summary>
		/// <param name="_kpiProjectId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.KpiProjects"/> class.</returns>
		public KaiZen.CSMS.Entities.KpiProjects GetByKpiProjectId(System.Int32 _kpiProjectId)
		{
			int count = -1;
			return GetByKpiProjectId(null,_kpiProjectId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_KpiProjects index.
		/// </summary>
		/// <param name="_kpiProjectId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.KpiProjects"/> class.</returns>
		public KaiZen.CSMS.Entities.KpiProjects GetByKpiProjectId(System.Int32 _kpiProjectId, int start, int pageLength)
		{
			int count = -1;
			return GetByKpiProjectId(null, _kpiProjectId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_KpiProjects index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_kpiProjectId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.KpiProjects"/> class.</returns>
		public KaiZen.CSMS.Entities.KpiProjects GetByKpiProjectId(TransactionManager transactionManager, System.Int32 _kpiProjectId)
		{
			int count = -1;
			return GetByKpiProjectId(transactionManager, _kpiProjectId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_KpiProjects index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_kpiProjectId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.KpiProjects"/> class.</returns>
		public KaiZen.CSMS.Entities.KpiProjects GetByKpiProjectId(TransactionManager transactionManager, System.Int32 _kpiProjectId, int start, int pageLength)
		{
			int count = -1;
			return GetByKpiProjectId(transactionManager, _kpiProjectId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_KpiProjects index.
		/// </summary>
		/// <param name="_kpiProjectId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.KpiProjects"/> class.</returns>
		public KaiZen.CSMS.Entities.KpiProjects GetByKpiProjectId(System.Int32 _kpiProjectId, int start, int pageLength, out int count)
		{
			return GetByKpiProjectId(null, _kpiProjectId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_KpiProjects index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_kpiProjectId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.KpiProjects"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.KpiProjects GetByKpiProjectId(TransactionManager transactionManager, System.Int32 _kpiProjectId, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_KpiProjectsTitle index.
		/// </summary>
		/// <param name="_kpiId"></param>
		/// <param name="_projectTitle"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;KpiProjects&gt;"/> class.</returns>
		public TList<KpiProjects> GetByKpiIdProjectTitle(System.Int32 _kpiId, System.String _projectTitle)
		{
			int count = -1;
			return GetByKpiIdProjectTitle(null,_kpiId, _projectTitle, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_KpiProjectsTitle index.
		/// </summary>
		/// <param name="_kpiId"></param>
		/// <param name="_projectTitle"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;KpiProjects&gt;"/> class.</returns>
		public TList<KpiProjects> GetByKpiIdProjectTitle(System.Int32 _kpiId, System.String _projectTitle, int start, int pageLength)
		{
			int count = -1;
			return GetByKpiIdProjectTitle(null, _kpiId, _projectTitle, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_KpiProjectsTitle index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_kpiId"></param>
		/// <param name="_projectTitle"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;KpiProjects&gt;"/> class.</returns>
		public TList<KpiProjects> GetByKpiIdProjectTitle(TransactionManager transactionManager, System.Int32 _kpiId, System.String _projectTitle)
		{
			int count = -1;
			return GetByKpiIdProjectTitle(transactionManager, _kpiId, _projectTitle, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_KpiProjectsTitle index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_kpiId"></param>
		/// <param name="_projectTitle"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;KpiProjects&gt;"/> class.</returns>
		public TList<KpiProjects> GetByKpiIdProjectTitle(TransactionManager transactionManager, System.Int32 _kpiId, System.String _projectTitle, int start, int pageLength)
		{
			int count = -1;
			return GetByKpiIdProjectTitle(transactionManager, _kpiId, _projectTitle, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_KpiProjectsTitle index.
		/// </summary>
		/// <param name="_kpiId"></param>
		/// <param name="_projectTitle"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;KpiProjects&gt;"/> class.</returns>
		public TList<KpiProjects> GetByKpiIdProjectTitle(System.Int32 _kpiId, System.String _projectTitle, int start, int pageLength, out int count)
		{
			return GetByKpiIdProjectTitle(null, _kpiId, _projectTitle, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_KpiProjectsTitle index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_kpiId"></param>
		/// <param name="_projectTitle"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;KpiProjects&gt;"/> class.</returns>
		public abstract TList<KpiProjects> GetByKpiIdProjectTitle(TransactionManager transactionManager, System.Int32 _kpiId, System.String _projectTitle, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;KpiProjects&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;KpiProjects&gt;"/></returns>
		public static TList<KpiProjects> Fill(IDataReader reader, TList<KpiProjects> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.KpiProjects c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("KpiProjects")
					.Append("|").Append((System.Int32)reader[((int)KpiProjectsColumn.KpiProjectId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<KpiProjects>(
					key.ToString(), // EntityTrackingKey
					"KpiProjects",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.KpiProjects();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.KpiProjectId = (System.Int32)reader[((int)KpiProjectsColumn.KpiProjectId - 1)];
					c.KpiId = (System.Int32)reader[((int)KpiProjectsColumn.KpiId - 1)];
					c.ProjectTitle = (System.String)reader[((int)KpiProjectsColumn.ProjectTitle - 1)];
					c.ProjectHours = (System.Int32)reader[((int)KpiProjectsColumn.ProjectHours - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.KpiProjects"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.KpiProjects"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.KpiProjects entity)
		{
			if (!reader.Read()) return;
			
			entity.KpiProjectId = (System.Int32)reader[((int)KpiProjectsColumn.KpiProjectId - 1)];
			entity.KpiId = (System.Int32)reader[((int)KpiProjectsColumn.KpiId - 1)];
			entity.ProjectTitle = (System.String)reader[((int)KpiProjectsColumn.ProjectTitle - 1)];
			entity.ProjectHours = (System.Int32)reader[((int)KpiProjectsColumn.ProjectHours - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.KpiProjects"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.KpiProjects"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.KpiProjects entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.KpiProjectId = (System.Int32)dataRow["KpiProjectId"];
			entity.KpiId = (System.Int32)dataRow["KpiId"];
			entity.ProjectTitle = (System.String)dataRow["ProjectTitle"];
			entity.ProjectHours = (System.Int32)dataRow["ProjectHours"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.KpiProjects"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.KpiProjects Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.KpiProjects entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region KpiIdSource	
			if (CanDeepLoad(entity, "Kpi|KpiIdSource", deepLoadType, innerList) 
				&& entity.KpiIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.KpiId;
				Kpi tmpEntity = EntityManager.LocateEntity<Kpi>(EntityLocator.ConstructKeyFromPkItems(typeof(Kpi), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.KpiIdSource = tmpEntity;
				else
					entity.KpiIdSource = DataRepository.KpiProvider.GetByKpiId(transactionManager, entity.KpiId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'KpiIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.KpiIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.KpiProvider.DeepLoad(transactionManager, entity.KpiIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion KpiIdSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.KpiProjects object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.KpiProjects instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.KpiProjects Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.KpiProjects entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region KpiIdSource
			if (CanDeepSave(entity, "Kpi|KpiIdSource", deepSaveType, innerList) 
				&& entity.KpiIdSource != null)
			{
				DataRepository.KpiProvider.Save(transactionManager, entity.KpiIdSource);
				entity.KpiId = entity.KpiIdSource.KpiId;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region KpiProjectsChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.KpiProjects</c>
	///</summary>
	public enum KpiProjectsChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>Kpi</c> at KpiIdSource
		///</summary>
		[ChildEntityType(typeof(Kpi))]
		Kpi,
		}
	
	#endregion KpiProjectsChildEntityTypes
	
	#region KpiProjectsFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;KpiProjectsColumn&gt;"/> class
	/// that is used exclusively with a <see cref="KpiProjects"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class KpiProjectsFilterBuilder : SqlFilterBuilder<KpiProjectsColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the KpiProjectsFilterBuilder class.
		/// </summary>
		public KpiProjectsFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the KpiProjectsFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public KpiProjectsFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the KpiProjectsFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public KpiProjectsFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion KpiProjectsFilterBuilder
	
	#region KpiProjectsParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;KpiProjectsColumn&gt;"/> class
	/// that is used exclusively with a <see cref="KpiProjects"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class KpiProjectsParameterBuilder : ParameterizedSqlFilterBuilder<KpiProjectsColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the KpiProjectsParameterBuilder class.
		/// </summary>
		public KpiProjectsParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the KpiProjectsParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public KpiProjectsParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the KpiProjectsParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public KpiProjectsParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion KpiProjectsParameterBuilder
	
	#region KpiProjectsSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;KpiProjectsColumn&gt;"/> class
	/// that is used exclusively with a <see cref="KpiProjects"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class KpiProjectsSortBuilder : SqlSortBuilder<KpiProjectsColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the KpiProjectsSqlSortBuilder class.
		/// </summary>
		public KpiProjectsSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion KpiProjectsSortBuilder
	
} // end namespace
