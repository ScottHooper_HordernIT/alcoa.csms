﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using KaiZen.CSMS.Data;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Services;
using KaiZen.CSMS.Web;

using System.Text;
using System.Collections;

using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxEditors;

using model = Repo.CSMS.DAL.EntityModels;
using repo = Repo.CSMS.Service.Database;

namespace ALCOA.CSMS.Website.UserControls.Admin
{
    public partial class AdminTaskEmailTemplates : System.Web.UI.UserControl
    {
        Auth auth = new Auth();
        //Repo.CSMS.Service.Database.ICompanyService companyService = ALCOA.CSMS.Website.Global.GetInstance<Repo.CSMS.Service.Database.ICompanyService>();
        repo.IAdminTaskEmailTemplateService adminTaskEmailTemplateService = ALCOA.CSMS.Website.Global.GetInstance<repo.IAdminTaskEmailTemplateService>();
        repo.IAdminTaskEmailTemplateAttachmentService adminTaskEmailTemplateAttachmentService = ALCOA.CSMS.Website.Global.GetInstance<repo.IAdminTaskEmailTemplateAttachmentService>();
        protected void Page_Load(object sender, EventArgs e) {  Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); }

        

        private void moduleInit(bool postBack)
        {
            SessionHandler.spVar_Page = "EmailTemplates"; // DT 3265, Added By Subhro
            if (!postBack)
            {
                Hide(true,null);
            }
            BindRecipients();
        }

        protected void grid_DataBound(object sender, EventArgs e)
        {
            ASPxGridView _grid = sender as ASPxGridView;
            _grid.Selection.UnselectAll();
            AdminTaskEmailTemplateAttachmentService atetaService = new AdminTaskEmailTemplateAttachmentService();
            TList<AdminTaskEmailTemplateAttachment> atetaTlist = atetaService.GetByAdminTaskEmailTemplateId((int)cbTemplate.SelectedItem.Value);
            foreach (AdminTaskEmailTemplateAttachment ateta in atetaTlist)
            {
                if (ateta.CsmsAccessId == (int)CsmsAccessList.AlcoaLan)
                {
                    //object key2 = _grid.GetRowValues(ateta.CsmsFileId, "CsmsFileId");
                    _grid.Selection.SelectRowByKey(ateta.CsmsFileId);
                }
            }
        }

        protected void grid2_DataBound(object sender, EventArgs e)
        {
            ASPxGridView _grid = sender as ASPxGridView;
            _grid.Selection.UnselectAll();
            AdminTaskEmailTemplateAttachmentService atetaService = new AdminTaskEmailTemplateAttachmentService();
            TList<AdminTaskEmailTemplateAttachment> atetaTlist = atetaService.GetByAdminTaskEmailTemplateId((int)cbTemplate.SelectedItem.Value);
            foreach (AdminTaskEmailTemplateAttachment ateta in atetaTlist)
            {
                if (ateta.CsmsAccessId == (int)CsmsAccessList.AlcoaDirect)
                {
                    //object key2 = _grid.GetRowValues(ateta.CsmsFileId, "CsmsFileId");
                    _grid.Selection.SelectRowByKey(ateta.CsmsFileId);
                }
            }
        } 

        protected void Load()
        {

            int AdminTaskEmailTemplateId = (int)cbTemplate.SelectedItem.Value;

           // AdminTaskEmailTemplateService atetService = new AdminTaskEmailTemplateService();
           // KaiZen.CSMS.Entities.AdminTaskEmailTemplate atet = atetService.GetByAdminTaskEmailTemplateId(AdminTaskEmailTemplateId);

            model.AdminTaskEmailTemplate atet = adminTaskEmailTemplateService.Get(i => i.AdminTaskEmailTemplateId == AdminTaskEmailTemplateId, null);

            if (atet == null) throw new Exception("Could not load Email Template");

            AdminTaskEmailTemplateRecipientService atetrService = new AdminTaskEmailTemplateRecipientService();
            TList<AdminTaskEmailTemplateRecipient> atetrToList = atetrService.GetByAdminTaskEmailTemplateIdCsmsEmailRecipientTypeId(
                                                                    AdminTaskEmailTemplateId, (int)CsmsEmailRecipientTypeList.To);
            TList<AdminTaskEmailTemplateRecipient> atetrCcList = atetrService.GetByAdminTaskEmailTemplateIdCsmsEmailRecipientTypeId(
                                                                    AdminTaskEmailTemplateId, (int)CsmsEmailRecipientTypeList.Cc);
            TList<AdminTaskEmailTemplateRecipient> atetrBccList = atetrService.GetByAdminTaskEmailTemplateIdCsmsEmailRecipientTypeId(
                                                                    AdminTaskEmailTemplateId, (int)CsmsEmailRecipientTypeList.Bcc);

            AdminTaskEmailRecipientService aterService = new AdminTaskEmailRecipientService();
            TList<AdminTaskEmailRecipient> aterTlist = aterService.GetAll();

            ddeTo.Text = "";
            ddeCc.Text = "";
            ddeBcc.Text = "";

            int count = 1;
            foreach (AdminTaskEmailTemplateRecipient atetr in atetrToList)
            {
                AdminTaskEmailRecipient ater = aterTlist.Find(AdminTaskEmailRecipientColumn.AdminTaskEmailRecipientId, atetr.AdminTaskEmailRecipientId);
                
                if (count < atetrToList.Count)
                {
                    ddeTo.Text += ater.RecipientDesc + ";";
                }
                else
                {
                    ddeTo.Text += ater.RecipientDesc;
                }
                count++;

                ASPxListBox lbTo = (ASPxListBox)ddeTo.FindControl("lbTo");
            }

            count = 1;
            foreach (AdminTaskEmailTemplateRecipient atetr in atetrCcList)
            {
                AdminTaskEmailRecipient ater = aterTlist.Find(AdminTaskEmailRecipientColumn.AdminTaskEmailRecipientId, atetr.AdminTaskEmailRecipientId);

                if (count < atetrCcList.Count)
                {
                    ddeCc.Text += ater.RecipientDesc + ";";
                }
                else
                {
                    ddeCc.Text += ater.RecipientDesc;
                }
                count++;
            }

            count = 1;
            foreach (AdminTaskEmailTemplateRecipient atetr in atetrBccList)
            {
                AdminTaskEmailRecipient ater = aterTlist.Find(AdminTaskEmailRecipientColumn.AdminTaskEmailRecipientId, atetr.AdminTaskEmailRecipientId);

                if (count < atetrBccList.Count)
                {
                    ddeBcc.Text += ater.RecipientDesc + ";";
                }
                else
                {
                    ddeBcc.Text += ater.RecipientDesc;
                }
                count++;
            }

            tbSubject.Text = atet.EmailSubject;

            heBodyAlcoaLan.Html = "";
            if (atet.EmailBodyAlcoaDirectExistingUser != null)
            {
                heBodyAlcoaLan.Html = Encoding.ASCII.GetString(atet.EmailBodyAlcoaLan);
            }

            heBodyAlcoaDirectExistingUser.Html = "";
            if (atet.EmailBodyAlcoaDirectExistingUser != null)
            {
                heBodyAlcoaDirectExistingUser.Html = Encoding.ASCII.GetString(atet.EmailBodyAlcoaDirectExistingUser);
            }

            heBodyAlcoaDirectNewUser.Html = "";
            if (atet.EmailBodyAlcoaDirectNewUser != null)
            {
                heBodyAlcoaDirectNewUser.Html = Encoding.ASCII.GetString(atet.EmailBodyAlcoaDirectNewUser);
            }
           

            //Added by Ashley Goldstraw September 2015.
            //shows the available variables the user can use in the subject or body
           // List<model.AdminTaskEmailTemplateVariable> variables = adminTaskEmailTemplateVariableService.GetMany(null,id=>id.AdminTaskEmailTemplateId == AdminTaskEmailTemplateId,null,null);
           // if (variables.Count > 0)
           // {
           //     grdAvailableVariables.DataSource = variables;
           //     grdAvailableVariables.DataBind();
           //     pnlAvailableVariables.Visible = true;
           // }
           // else
           //     pnlAvailableVariables.Visible = false;



            //SelectGridRows();

            //CsmsFileDataSource1.DataBind();
            //grid.DataBind();
            //grid2.DataBind();

            Hide(false,atet);
        }
        protected void SelectGridRows()
        {
            grid.DataBind();
            grid2.DataBind();
            AdminTaskEmailTemplateAttachmentService atetaService = new AdminTaskEmailTemplateAttachmentService();
            TList<AdminTaskEmailTemplateAttachment> atetaTlist = atetaService.GetByAdminTaskEmailTemplateId((int)cbTemplate.SelectedItem.Value);
            foreach (AdminTaskEmailTemplateAttachment ateta in atetaTlist)
            {
                switch (ateta.CsmsAccessId)
                {
                    case (int)CsmsAccessList.AlcoaLan:
                        grid.Selection.SelectRowByKey(ateta.CsmsFileId);
                        break;
                    case (int)CsmsAccessList.AlcoaDirect:
                        grid2.Selection.SelectRowByKey(ateta.CsmsFileId);
                        break;
                    default:
                        break;
                }

            }
        }

        protected void Save(bool close)
        {
            
            int AdminTaskEmailTemplateId = (int)cbTemplate.SelectedItem.Value;
            model.AdminTaskEmailTemplate atet = adminTaskEmailTemplateService.Get(x => x.AdminTaskEmailTemplateId == AdminTaskEmailTemplateId, null);
            if (atet == null)
                throw new Exception("Could not load Email Template");

            atet.EmailSubject = tbSubject.Text;
            ASCIIEncoding encoding = new ASCIIEncoding();
            atet.EmailBodyAlcoaLan = (byte[])encoding.GetBytes(heBodyAlcoaLan.Html);
            atet.EmailBodyAlcoaDirectExistingUser = (byte[])encoding.GetBytes(heBodyAlcoaDirectExistingUser.Html);
            atet.EmailBodyAlcoaDirectNewUser = (byte[])encoding.GetBytes(heBodyAlcoaDirectNewUser.Html);
           
            adminTaskEmailTemplateService.Update(atet);

            //Attachments
            List<model.AdminTaskEmailTemplateAttachment> atetaList = adminTaskEmailTemplateAttachmentService.GetMany(null, i => i.AdminTaskEmailTemplateId == atet.AdminTaskEmailTemplateId,null,null);
            foreach (model.AdminTaskEmailTemplateAttachment atetaOld in atetaList)
            {
                adminTaskEmailTemplateAttachmentService.Delete(atetaOld);
            }

            List<object> keyValues = grid.GetSelectedFieldValues("CsmsFileId");
            foreach (int CsmsFileId in keyValues)
            {
                model.AdminTaskEmailTemplateAttachment ateta = new model.AdminTaskEmailTemplateAttachment();
                ateta.AdminTaskEmailTemplateId = atet.AdminTaskEmailTemplateId;
                ateta.CsmsFileId = CsmsFileId;
                ateta.CsmsAccessId = (int)CsmsAccessList.AlcoaLan;
                adminTaskEmailTemplateAttachmentService.Insert(ateta);
            }
            List<object> keyValues2 = grid2.GetSelectedFieldValues("CsmsFileId");
            foreach (int CsmsFileId in keyValues2)
            {
                model.AdminTaskEmailTemplateAttachment ateta = new model.AdminTaskEmailTemplateAttachment();
                ateta.AdminTaskEmailTemplateId = atet.AdminTaskEmailTemplateId;
                ateta.CsmsFileId = CsmsFileId;
                ateta.CsmsAccessId = (int)CsmsAccessList.AlcoaDirect;
                adminTaskEmailTemplateAttachmentService.Insert(ateta);
            }

            //recipients

            AdminTaskEmailTemplateRecipientService atetrService = new AdminTaskEmailTemplateRecipientService();
            TList<AdminTaskEmailTemplateRecipient> atetrTlist = atetrService.GetByAdminTaskEmailTemplateId(atet.AdminTaskEmailTemplateId);
            atetrService.Delete(atetrTlist);

            AdminTaskEmailRecipientService aterService = new AdminTaskEmailRecipientService();
            TList<AdminTaskEmailRecipient> aterTlist = aterService.GetAll();

            string[] recipientsTo = ddeTo.Text.Split(';');
            foreach (string recipientTo in recipientsTo)
            {
                AdminTaskEmailRecipient ater = aterTlist.Find(AdminTaskEmailRecipientColumn.RecipientDesc, recipientTo);
                if (ater != null)
                {
                    AdminTaskEmailTemplateRecipient atetr = new AdminTaskEmailTemplateRecipient();
                    atetr.CsmsEmailRecipientTypeId = (int)CsmsEmailRecipientTypeList.To;
                    atetr.AdminTaskEmailTemplateId = atet.AdminTaskEmailTemplateId;
                    atetr.AdminTaskEmailRecipientId = ater.AdminTaskEmailRecipientId;
                    atetrService.Save(atetr);
                }
            }

            string[] recipientsCc = ddeCc.Text.Split(';');
            foreach (string recipientCc in recipientsCc)
            {
                AdminTaskEmailRecipient ater = aterTlist.Find(AdminTaskEmailRecipientColumn.RecipientDesc, recipientCc);
                if (ater != null)
                {
                    AdminTaskEmailTemplateRecipient atetr = new AdminTaskEmailTemplateRecipient();
                    atetr.CsmsEmailRecipientTypeId = (int)CsmsEmailRecipientTypeList.Cc;
                    atetr.AdminTaskEmailTemplateId = atet.AdminTaskEmailTemplateId;
                    atetr.AdminTaskEmailRecipientId = ater.AdminTaskEmailRecipientId;
                    atetrService.Save(atetr);
                }
            }

            string[] recipientsBcc = ddeBcc.Text.Split(';');
            foreach (string recipientBcc in recipientsBcc)
            {
                AdminTaskEmailRecipient ater = aterTlist.Find(AdminTaskEmailRecipientColumn.RecipientDesc, recipientBcc);
                if (ater != null)
                {
                    AdminTaskEmailTemplateRecipient atetr = new AdminTaskEmailTemplateRecipient();
                    atetr.CsmsEmailRecipientTypeId = (int)CsmsEmailRecipientTypeList.Bcc;
                    atetr.AdminTaskEmailTemplateId = atet.AdminTaskEmailTemplateId;
                    atetr.AdminTaskEmailRecipientId = ater.AdminTaskEmailRecipientId;
                    atetrService.Save(atetr);
                }
            }


            if (close) Close();

        }

        protected void Save2(bool close) //been replaced with method above
        {
            int AdminTaskEmailTemplateId = (int)cbTemplate.SelectedItem.Value;
            AdminTaskEmailTemplateService atetService = new AdminTaskEmailTemplateService();
            KaiZen.CSMS.Entities.AdminTaskEmailTemplate atet = atetService.GetByAdminTaskEmailTemplateId(AdminTaskEmailTemplateId);

            if (atet == null) throw new Exception("Could not load Email Template");

            atet.EmailSubject = tbSubject.Text;

            ASCIIEncoding encoding = new ASCIIEncoding();
            atet.EmailBodyAlcoaLan = (byte[])encoding.GetBytes(heBodyAlcoaLan.Html);
            atet.EmailBodyAlcoaDirectExistingUser = (byte[])encoding.GetBytes(heBodyAlcoaDirectExistingUser.Html);
            atet.EmailBodyAlcoaDirectNewUser = (byte[])encoding.GetBytes(heBodyAlcoaDirectNewUser.Html);

            atetService.Save(atet);

            //attachments
            AdminTaskEmailTemplateAttachmentService atetaService = new AdminTaskEmailTemplateAttachmentService();
            TList<AdminTaskEmailTemplateAttachment> atetaTlist = atetaService.GetByAdminTaskEmailTemplateId(atet.AdminTaskEmailTemplateId);
            atetaService.Delete(atetaTlist);

            List<object> keyValues = grid.GetSelectedFieldValues("CsmsFileId");
            foreach (int CsmsFileId in keyValues)
            {
                AdminTaskEmailTemplateAttachment ateta = new AdminTaskEmailTemplateAttachment();
                ateta.AdminTaskEmailTemplateId = atet.AdminTaskEmailTemplateId;
                ateta.CsmsFileId = CsmsFileId;
                ateta.CsmsAccessId = (int)CsmsAccessList.AlcoaLan;
                atetaService.Save(ateta);
            }

            List<object> keyValues2 = grid2.GetSelectedFieldValues("CsmsFileId");
            foreach (int CsmsFileId in keyValues2)
            {
                AdminTaskEmailTemplateAttachment ateta = new AdminTaskEmailTemplateAttachment();
                ateta.AdminTaskEmailTemplateId = atet.AdminTaskEmailTemplateId;
                ateta.CsmsFileId = CsmsFileId;
                ateta.CsmsAccessId = (int)CsmsAccessList.AlcoaDirect;
                atetaService.Save(ateta);
            }

            //recipients

            AdminTaskEmailTemplateRecipientService atetrService = new AdminTaskEmailTemplateRecipientService();
            TList<AdminTaskEmailTemplateRecipient> atetrTlist = atetrService.GetByAdminTaskEmailTemplateId(atet.AdminTaskEmailTemplateId);
            atetrService.Delete(atetrTlist);

            AdminTaskEmailRecipientService aterService = new AdminTaskEmailRecipientService();
            TList<AdminTaskEmailRecipient> aterTlist = aterService.GetAll();

            string[] recipientsTo = ddeTo.Text.Split(';');
            foreach (string recipientTo in recipientsTo)
            {
                AdminTaskEmailRecipient ater = aterTlist.Find(AdminTaskEmailRecipientColumn.RecipientDesc, recipientTo);
                if (ater != null)
                {
                    AdminTaskEmailTemplateRecipient atetr = new AdminTaskEmailTemplateRecipient();
                    atetr.CsmsEmailRecipientTypeId = (int)CsmsEmailRecipientTypeList.To;
                    atetr.AdminTaskEmailTemplateId = atet.AdminTaskEmailTemplateId;
                    atetr.AdminTaskEmailRecipientId = ater.AdminTaskEmailRecipientId;
                    atetrService.Save(atetr);
                }
            }

            string[] recipientsCc = ddeCc.Text.Split(';');
            foreach (string recipientCc in recipientsCc)
            {
                AdminTaskEmailRecipient ater = aterTlist.Find(AdminTaskEmailRecipientColumn.RecipientDesc, recipientCc);
                if (ater != null)
                {
                    AdminTaskEmailTemplateRecipient atetr = new AdminTaskEmailTemplateRecipient();
                    atetr.CsmsEmailRecipientTypeId = (int)CsmsEmailRecipientTypeList.Cc;
                    atetr.AdminTaskEmailTemplateId = atet.AdminTaskEmailTemplateId;
                    atetr.AdminTaskEmailRecipientId = ater.AdminTaskEmailRecipientId;
                    atetrService.Save(atetr);
                }
            }

            string[] recipientsBcc = ddeBcc.Text.Split(';');
            foreach (string recipientBcc in recipientsBcc)
            {
                AdminTaskEmailRecipient ater = aterTlist.Find(AdminTaskEmailRecipientColumn.RecipientDesc, recipientBcc);
                if (ater != null)
                {
                    AdminTaskEmailTemplateRecipient atetr = new AdminTaskEmailTemplateRecipient();
                    atetr.CsmsEmailRecipientTypeId = (int)CsmsEmailRecipientTypeList.Bcc;
                    atetr.AdminTaskEmailTemplateId = atet.AdminTaskEmailTemplateId;
                    atetr.AdminTaskEmailRecipientId = ater.AdminTaskEmailRecipientId;
                    atetrService.Save(atetr);
                }
            }

            if (close) Close();
        }
        protected void Close()
        {
            btnLoad.Enabled = true;
            cbTemplate.Enabled = true;

            grid.Selection.CancelSelection();
            Hide(true,null);

            Response.Redirect("Admin.aspx?s=" + SessionHandler.spVar_Page, false); //DT 3265, Added By Subhro
        }
        protected void Hide(bool hide,model.AdminTaskEmailTemplate atet)
        {
            if (hide)
            {
                btnSave.Enabled = false;
                btnSaveClose.Enabled = false;
                //ASPxPageControl1.Enabled = false;
                //tbSubject.Enabled = false;
                //ddeTo.Enabled = false;
                //cbCc.Enabled = false;
                //cbBcc.Enabled = false;
                //ASPxGridView1.Enabled = false;

                heBodyAlcoaLan.Visible = false;
                //heBodyAlcoaDirectExistingUser.Visible = false;
               
                heBodyAlcoaDirectNewUser.Visible = false;
                cbTemplate.Enabled = true;
                btnLoad.Enabled = true;

                //heBodyAlcoaDirectExistingUser.Height = Unit.Pixel(500);
                //heBodyAlcoaDirectNewUser.Height = Unit.Pixel(500);
                //heBodyAlcoaLan.Height = Unit.Pixel(500);
                Panel1.Visible = false;
            }
            else
            {
                

                btnSave.Enabled = true;
                btnSaveClose.Enabled = true;
                //ASPxPageControl1.Enabled = true;
                //tbSubject.Enabled = true;
                //ddeTo.Enabled = true;
                //cbCc.Enabled = true;
                //cbBcc.Enabled = true;
                //ASPxGridView1.Enabled = true;

                heBodyAlcoaLan.Visible = true;
                heBodyAlcoaDirectExistingUser.Visible = true;
                heBodyAlcoaDirectNewUser.Visible = true;
                
                cbTemplate.Enabled = false;
                btnLoad.Enabled = false;

                //heBodyAlcoaDirectExistingUser.Height = Unit.Pixel(500);
                //heBodyAlcoaDirectNewUser.Height = Unit.Pixel(500);
                //heBodyAlcoaLan.Height = Unit.Pixel(500);

                Panel1.Visible = true;
            }
        }

        protected void btnLoad_Click(object sender, EventArgs e)
        {
            Load();
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            Save(false);
        }
        protected void btnSaveAndClose_Click(object sender, EventArgs e)
        {
            Save(true);
        }
        protected void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }


        protected void BindRecipients()
        {
            ASPxListBox lbTo = (ASPxListBox)ddeTo.FindControl("lbTo");
            ASPxListBox lbCc = (ASPxListBox)ddeCc.FindControl("lbCc");
            ASPxListBox lbBcc = (ASPxListBox)ddeBcc.FindControl("lbBcc");
            lbTo.Items.Clear();
            lbTo.Items.Add("(Select all)");
            lbCc.Items.Clear();
            lbCc.Items.Add("(Select all)");
            lbBcc.Items.Clear();
            lbBcc.Items.Add("(Select all)");
            AdminTaskEmailRecipientService aterService = new AdminTaskEmailRecipientService();

            int totalCount = 0;
            TList<AdminTaskEmailRecipient> aterTlist = aterService.GetPaged(null, "RecipientDesc ASC", 0, 100, out totalCount);

            if (totalCount > 0)
            {
                foreach (AdminTaskEmailRecipient ater in aterTlist)
                {
                    lbTo.Items.Add(ater.RecipientDesc, ater.AdminTaskEmailRecipientId);
                    lbCc.Items.Add(ater.RecipientDesc, ater.AdminTaskEmailRecipientId);
                    lbBcc.Items.Add(ater.RecipientDesc, ater.AdminTaskEmailRecipientId);
                }
            }
            else
            {
                lbTo.Items.Clear();
                lbCc.Items.Clear();
                lbBcc.Items.Clear();
            }

        }
    }
}