﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="UserControls_Tiny_Workload_Sqq"
    CodeBehind="Workload_Sqq_Procurement.ascx.cs" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dxw" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dxtc" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Src="~/UserControls/Other/ExportButtons.ascx" TagName="ExportButtons"
    TagPrefix="uc1" %>
<table style="width: 100%" cellspacing="0" cellpadding="0" border="0">
    <tbody>
        <tr>
            <td style="height: 28px; text-align: left" colspan="6">
                <table cellspacing="0" cellpadding="0" border="0">
                    <tbody>
                        <tr>
                            <td style="width: 272px; text-align: right; height: 36px;">
                                &nbsp;<strong>Functional Procurement Manager:</strong>
                            </td>
                            <td style="width: 157px; height: 36px;">
                                <dxe:ASPxComboBox runat="server" IncrementalFilteringMode="StartsWith" ValueType="System.String"
                                    Width="246px" AutoPostBack="True" CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                    ClientInstanceName="cmbFunctionalProcurementManager" ID="cmbFunctionalProcurementManager"
                                    OnSelectedIndexChanged="cmbFunctionalProcurementManager_SelectedIndexChanged"
                                    SelectedIndex="0" DataSourceID="dsQuestionnaire_GetAssignedFunctionalProcurementManagers"
                                    TextField="UserFullName" ValueField="UserId" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                    <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                                    </LoadingPanelImage>
                                    <ButtonStyle Width="13px">
                                    </ButtonStyle>
                                    <Items>
                                        <dxe:ListEditItem Selected="True" Text="All" Value=" " />
                                    </Items>
                                </dxe:ASPxComboBox>
                            </td>
                            <td style="width: 440px; text-align: right">
                                <div align="right">
                                    <a style="color: #0000ff; text-align: right; text-decoration: none" href="javascript:ShowHideCustomizationWindow2()">
                                        Customize</a>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td style="height: 28px; text-align: left" colspan="6">
                <dxwgv:ASPxGridView runat="server" ClientInstanceName="grid2" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                    CssPostfix="Office2003Blue" AutoGenerateColumns="False" DataSourceID="dsQuestionnaire_GetProcurementWorkLoad"
                    Width="870px" ID="grid2">
                    <SettingsCustomizationWindow Enabled="True"></SettingsCustomizationWindow>
                    <TotalSummary>
                        <dxwgv:ASPxSummaryItem SummaryType="Sum" FieldName="CurrentlyAllocated" DisplayFormat="{0}"
                            ShowInColumn="Current Work Allocated" ShowInGroupFooterColumn="Current Work Allocated">
                        </dxwgv:ASPxSummaryItem>
                        <dxwgv:ASPxSummaryItem SummaryType="Sum" FieldName="ProcurementIncomplete" DisplayFormat="{0}"
                            ShowInColumn="Incomplete Procurement Questionnaire" ShowInGroupFooterColumn="Incomplete Procurement Questionnairee">
                        </dxwgv:ASPxSummaryItem>
                        <dxwgv:ASPxSummaryItem SummaryType="Sum" FieldName="SupplierIncomplete" DisplayFormat="{0}"
                            ShowInColumn="Incomplete Supplier Questionnaire (33.055 + VQ)" ShowInGroupFooterColumn="Incomplete Supplier Questionnaire (33.055 + VQ)">
                        </dxwgv:ASPxSummaryItem>
                        <dxwgv:ASPxSummaryItem SummaryType="Sum" FieldName="Being Assessed" DisplayFormat="{0}"
                            ShowInColumn="Being Assessed by HS Assessor" ShowInGroupFooterColumn="Being Assessed by HS Assessor">
                        </dxwgv:ASPxSummaryItem>
                        <dxwgv:ASPxSummaryItem SummaryType="Sum" FieldName="AwaitingContractAssignment" DisplayFormat="{0}"
                            ShowInColumn="Awaiting Contract Assignment" ShowInGroupFooterColumn="# Awaiting Contract Assignment">
                        </dxwgv:ASPxSummaryItem>
                        <dxwgv:ASPxSummaryItem SummaryType="Sum" FieldName="PresentlyWithProcurement" DisplayFormat="{0}"
                            ShowInColumn="Presently with Procurement" ShowInGroupFooterColumn="Presently with Procurement">
                        </dxwgv:ASPxSummaryItem>
                        <dxwgv:ASPxSummaryItem SummaryType="Sum" FieldName="SupplierIncomplete" DisplayFormat="{0}"
                            ShowInColumn="Presently with Supplier" ShowInGroupFooterColumn="Presently with Supplier">
                        </dxwgv:ASPxSummaryItem>
                        <dxwgv:ASPxSummaryItem SummaryType="Sum" FieldName="ProcessComplete" DisplayFormat="{0}"
                            ShowInColumn="SQ Process Complete" ShowInGroupFooterColumn="SQ Process Complete">
                        </dxwgv:ASPxSummaryItem>
                    </TotalSummary>
                    <Columns>
                        <dxwgv:GridViewDataTextColumn Caption="Site" FieldName="SiteName" Visible="false"
                            Width="75px">
                            <HeaderStyle HorizontalAlign="Center" Wrap="True"></HeaderStyle>
                            <CellStyle HorizontalAlign="Center">
                            </CellStyle>
                            <FooterCellStyle HorizontalAlign="Center">
                            </FooterCellStyle>
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Region" FieldName="RegionNameAbbrev" Visible="false"
                            Width="75px">
                            <HeaderStyle HorizontalAlign="Center" Wrap="True"></HeaderStyle>
                            <CellStyle HorizontalAlign="Center">
                            </CellStyle>
                            <FooterCellStyle HorizontalAlign="Center">
                            </FooterCellStyle>
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Company Status" FieldName="CompanyStatusName" Visible="false"
                            Settings-HeaderFilterMode="CheckedList" Width="100px">
                            <HeaderStyle HorizontalAlign="Center" Wrap="True"></HeaderStyle>
                            <CellStyle HorizontalAlign="Center">
                            </CellStyle>
                            <FooterCellStyle HorizontalAlign="Center">
                            </FooterCellStyle>
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataComboBoxColumn FieldName="UserId" VisibleIndex="0" Visible="false" ShowInCustomizationForm="false">
                            <PropertiesComboBox DataSourceID="UsersProcurementListDataSource1" TextField="UserFullName"
                                ValueField="UserId" ValueType="System.String">
                            </PropertiesComboBox>
                            <Settings FilterMode="DisplayText" />
                            <Settings FilterMode="DisplayText"></Settings>
                            <HeaderStyle HorizontalAlign="Center" Wrap="True"></HeaderStyle>
                            <CellStyle HorizontalAlign="Left">
                            </CellStyle>
                            <FooterCellStyle HorizontalAlign="Center">
                            </FooterCellStyle>
                        </dxwgv:GridViewDataComboBoxColumn>
                        <dxwgv:GridViewDataComboBoxColumn Caption="Procurement User" FieldName="UserFullName"
                            VisibleIndex="1">
                            <PropertiesComboBox DataSourceID="UsersProcurementListDataSource1" TextField="UserFullName"
                                ValueField="UserFullName" ValueType="System.String">
                            </PropertiesComboBox>
                            <Settings FilterMode="DisplayText" />
                            <Settings FilterMode="DisplayText"></Settings>
                            <HeaderStyle HorizontalAlign="Center" Wrap="True"></HeaderStyle>
                            <CellStyle HorizontalAlign="Left">
                            </CellStyle>
                            <FooterCellStyle HorizontalAlign="Right">
                            </FooterCellStyle>
                            <FooterTemplate>
                                Total:</FooterTemplate>
                        </dxwgv:GridViewDataComboBoxColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Current Work Allocated" FieldName="CurrentlyAllocated"
                            VisibleIndex="2" Width="75px">
                            <HeaderStyle HorizontalAlign="Center" Wrap="True"></HeaderStyle>
                            <CellStyle HorizontalAlign="Center">
                            </CellStyle>
                            <FooterCellStyle HorizontalAlign="Center">
                            </FooterCellStyle>
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Incomplete Procurement Questionnaire" FieldName="ProcurementIncomplete"
                            VisibleIndex="3" Width="102px">
                            <HeaderStyle HorizontalAlign="Center" Wrap="True"></HeaderStyle>
                            <CellStyle HorizontalAlign="Center">
                            </CellStyle>
                            <FooterCellStyle HorizontalAlign="Center">
                            </FooterCellStyle>
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Incomplete Supplier Questionnaire (33.055 + VQ)"
                            FieldName="SupplierIncomplete" VisibleIndex="4" Width="107px">
                            <HeaderStyle HorizontalAlign="Center" Wrap="True"></HeaderStyle>
                            <CellStyle HorizontalAlign="Center">
                            </CellStyle>
                            <FooterCellStyle HorizontalAlign="Center">
                            </FooterCellStyle>
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Being Assessed by HS Assessor" FieldName="Being Assessed"
                            VisibleIndex="5" Width="81px">
                            <HeaderStyle HorizontalAlign="Center" Wrap="True"></HeaderStyle>
                            <CellStyle HorizontalAlign="Center">
                            </CellStyle>
                            <FooterCellStyle HorizontalAlign="Center">
                            </FooterCellStyle>
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Awaiting Contract Assignment" FieldName="AwaitingContractAssignment"
                            VisibleIndex="6" Width="90px">
                            <HeaderStyle HorizontalAlign="Center" Wrap="True"></HeaderStyle>
                            <CellStyle HorizontalAlign="Center">
                            </CellStyle>
                            <FooterCellStyle HorizontalAlign="Center">
                            </FooterCellStyle>
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Presently with Procurement" FieldName="PresentlyWithProcurement"
                            VisibleIndex="7" Width="99px">
                            <HeaderStyle HorizontalAlign="Center" Wrap="True"></HeaderStyle>
                            <CellStyle HorizontalAlign="Center">
                            </CellStyle>
                            <FooterCellStyle HorizontalAlign="Center">
                            </FooterCellStyle>
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Presently with Supplier" FieldName="SupplierIncomplete"
                            VisibleIndex="8" Width="82px">
                            <HeaderStyle HorizontalAlign="Center" Wrap="True"></HeaderStyle>
                            <CellStyle HorizontalAlign="Center">
                            </CellStyle>
                            <FooterCellStyle HorizontalAlign="Center">
                            </FooterCellStyle>
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="SQ Process Complete" FieldName="ProcessComplete"
                            VisibleIndex="9" Width="100px">
                            <HeaderStyle HorizontalAlign="Center" Wrap="True"></HeaderStyle>
                            <CellStyle HorizontalAlign="Center">
                            </CellStyle>
                            <FooterCellStyle HorizontalAlign="Center">
                            </FooterCellStyle>
                        </dxwgv:GridViewDataTextColumn>
                    </Columns>
                    <SettingsPager>
                        <AllButton Visible="True">
                        </AllButton>
                    </SettingsPager>
                    <Settings ShowFilterRow="True" ShowHeaderFilterButton="True" ShowFilterBar="Auto" ShowGroupPanel="True"
                        ShowFooter="True" ShowGroupFooter="VisibleIfExpanded" ShowVerticalScrollBar="True"
                        VerticalScrollableHeight="215"></Settings>
                    <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                        <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
                        </LoadingPanelOnStatusBar>
                        <CollapsedButton Width="11px">
                        </CollapsedButton>
                        <ExpandedButton Width="11px">
                        </ExpandedButton>
                        <DetailCollapsedButton Width="11px">
                        </DetailCollapsedButton>
                        <DetailExpandedButton Width="11px">
                        </DetailExpandedButton>
                        <LoadingPanel Url="~/App_Themes/Office2003BlueOld/GridView/Loading.gif"> <%--chnage by Debashis--%>
                        </LoadingPanel>
                    </Images>
                    <ImagesFilterControl>
                        <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
                        </LoadingPanel>
                    </ImagesFilterControl>
                    <SettingsBehavior ColumnResizeMode="NextColumn" />
                    <Styles CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css">
                        <Header SortingImageSpacing="5px" ImageSpacing="5px">
                        </Header>
                        <LoadingPanel ImageSpacing="10px">
                        </LoadingPanel>
                    </Styles>
                    <StylesEditors>
                        <ProgressBar Height="25px">
                        </ProgressBar>
                    </StylesEditors>
                </dxwgv:ASPxGridView>
            </td>
        </tr>
        <tr>
            <td style="text-align: right; padding-top: 6px;" colspan="6">
                <div align="right">
                    <uc1:ExportButtons runat="server" ID="ucExportButtons"></uc1:ExportButtons>
                </div>
            </td>
        </tr>
    </tbody>
</table>
<asp:ObjectDataSource ID="dsQuestionnaire_GetEhsConsultantsWorkLoad" runat="server"
    TypeName="KaiZen.CSMS.Services.QuestionnaireService" SelectMethod="GetEhsConsultantsWorkload">
    <SelectParameters>
        <asp:SessionParameter DefaultValue="%" Name="CurrentYear" SessionField="spVar_Year"
            Type="String" />
    </SelectParameters>
</asp:ObjectDataSource>
<data:CompaniesEhsConsultantsDataSource ID="CompaniesEhsConsultantsDataSource1" runat="server">
</data:CompaniesEhsConsultantsDataSource>
<asp:ObjectDataSource ID="dsQuestionnaire_CountEhsConsultantsUnAllocated" runat="server"
    TypeName="KaiZen.CSMS.Services.QuestionnaireService" SelectMethod="CountEhsConsultantsUnAllocated">
    <SelectParameters>
        <asp:SessionParameter DefaultValue="%" Name="CurrentYear" SessionField="spVar_Year"
            Type="String" />
    </SelectParameters>
</asp:ObjectDataSource>
<asp:ObjectDataSource ID="dsQuestionnaire_GetDistinctYears" runat="server" TypeName="KaiZen.CSMS.Services.QuestionnaireService"
    SelectMethod="GetDistinctYears"></asp:ObjectDataSource>
<asp:SqlDataSource ID="dsQuestionnaire_GetProcurementWorkLoad" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="_Questionnaire_GetProcurementWorkLoad" SelectCommandType="StoredProcedure">
    <SelectParameters>
        <asp:SessionParameter DefaultValue="-1" Name="FunctionalProcurementManagerUserId"
            SessionField="FunctionalProcurementManagerUserId" Type="Int32" />
    </SelectParameters>
</asp:SqlDataSource>
<data:UsersProcurementListDataSource ID="UsersProcurementListDataSource1" runat="server">
</data:UsersProcurementListDataSource>
<asp:SqlDataSource ID="dsQuestionnaire_GetAssignedFunctionalProcurementManagers"
    runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="_Questionnaire_GetAssignedFunctionalProcurementManagers" SelectCommandType="StoredProcedure">
</asp:SqlDataSource>
