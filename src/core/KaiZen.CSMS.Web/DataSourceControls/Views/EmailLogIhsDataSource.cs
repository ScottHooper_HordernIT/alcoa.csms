﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.EmailLogIhsProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[CLSCompliant(true)]
	[Designer(typeof(EmailLogIhsDataSourceDesigner))]
	public class EmailLogIhsDataSource : ReadOnlyDataSource<EmailLogIhs>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EmailLogIhsDataSource class.
		/// </summary>
		public EmailLogIhsDataSource() : base(new EmailLogIhsService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the EmailLogIhsDataSourceView used by the EmailLogIhsDataSource.
		/// </summary>
		protected EmailLogIhsDataSourceView EmailLogIhsView
		{
			get { return ( View as EmailLogIhsDataSourceView ); }
		}
		
		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the EmailLogIhsDataSourceView class that is to be
		/// used by the EmailLogIhsDataSource.
		/// </summary>
		/// <returns>An instance of the EmailLogIhsDataSourceView class.</returns>
		protected override BaseDataSourceView<EmailLogIhs, Object> GetNewDataSourceView()
		{
			return new EmailLogIhsDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the EmailLogIhsDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class EmailLogIhsDataSourceView : ReadOnlyDataSourceView<EmailLogIhs>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EmailLogIhsDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the EmailLogIhsDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public EmailLogIhsDataSourceView(EmailLogIhsDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal EmailLogIhsDataSource EmailLogIhsOwner
		{
			get { return Owner as EmailLogIhsDataSource; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal EmailLogIhsService EmailLogIhsProvider
		{
			get { return Provider as EmailLogIhsService; }
		}

		#endregion Properties
		
		#region Methods
		
		#endregion Methods
	}

	#region EmailLogIhsDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the EmailLogIhsDataSource class.
	/// </summary>
	public class EmailLogIhsDataSourceDesigner : ReadOnlyDataSourceDesigner<EmailLogIhs>
	{
	}

	#endregion EmailLogIhsDataSourceDesigner

	#region EmailLogIhsFilter

	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EmailLogIhs"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EmailLogIhsFilter : SqlFilter<EmailLogIhsColumn>
	{
	}

	#endregion EmailLogIhsFilter

	#region EmailLogIhsExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EmailLogIhs"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EmailLogIhsExpressionBuilder : SqlExpressionBuilder<EmailLogIhsColumn>
	{
	}
	
	#endregion EmailLogIhsExpressionBuilder		
}

