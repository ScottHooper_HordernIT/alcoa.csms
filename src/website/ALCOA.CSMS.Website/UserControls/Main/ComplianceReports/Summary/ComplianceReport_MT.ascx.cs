using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using KaiZen.CSMS.Web;
using KaiZen.CSMS.Services;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxTabControl;
using DevExpress.Web.Data;
using DevExpress.Web.ASPxGridView;
using System.Drawing;
using DevExpress.Web.ASPxCallbackPanel;
using DevExpress.Web.ASPxClasses;
using DevExpress.XtraPrinting;

public partial class UserControls_Tiny_ComplianceReports_ComplianceReport_MT : System.Web.UI.UserControl
{
    Auth auth = new Auth();

    protected void Page_Load(object sender, EventArgs e) {  Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); }

    private void moduleInit(bool postBack)
    {
        if (!postBack)
        {
            switch (auth.RoleId)
            {
                case ((int)RoleList.Reader):
                    break;
                case ((int)RoleList.Contractor):
                    break;
                case ((int)RoleList.Administrator):
                    break;
                default:
                    // do something
                    Response.Redirect(String.Format("AccessDenied.aspx?error={0}", Server.UrlEncode("Unrecognised User.")));
                    break;
            }

            if (SessionHandler.spVar_Page == "ComplianceReport")
            {
                grid.Settings.ShowFilterRow = false;
                grid.Settings.ShowGroupPanel = false;
                grid.SettingsBehavior.AllowSort = false;
                grid.SettingsBehavior.AllowGroup = false;
                //residentialstatus = SessionHandler.spVar_ResidentialStatus2;

                grid.DataSourceID = null;

                DataSet ds = new DataSet();
                if (Convert.ToInt32(SessionHandler.spVar_CompanyId) > 0) //specific company
                {
                    ds = DataRepository.KpiProvider.ComplianceReport_GetMandatedTraining_ByCompany(
                            Convert.ToInt32(SessionHandler.spVar_Year),
                            Convert.ToInt32(SessionHandler.spVar_CompanyId),
                            Convert.ToInt32(SessionHandler.spVar_SiteId),
                            SessionHandler.spVar_CategoryId2);
                    //Added By Bishwajit for Item#31
					Session["spVar_GridMT"] = ds;
					//End Added By Bishwajit for Item#31
                }

                grid.DataSource = ds.Tables[0];
                grid.DataBind();
                ds.Dispose();
            }
        }
    }
}
