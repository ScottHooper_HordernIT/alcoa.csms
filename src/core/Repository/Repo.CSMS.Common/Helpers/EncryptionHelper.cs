﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Security.Cryptography;

namespace Repo.CSMS.Common.Helpers
{
    public class EncryptionHelper
    {
        #region MD5 Encryption

        /// <summary>
        /// Convert the input string to an MD5 hash
        /// </summary>
        /// <param name="input">Unencrypted string</param>
        /// <returns>Encrypted hash string</returns>
        public static string GetMD5Hash(string input, string salt = "")
        {
            string hash = "";

            using (MD5 md5Hash = MD5.Create())
            {
                // Convert the input string to a byte array and compute the hash. 
                byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(salt + input));

                // Create a new Stringbuilder to collect the bytes 
                // and create a string.
                StringBuilder sBuilder = new StringBuilder();

                // Loop through each byte of the hashed data  
                // and format each one as a hexadecimal string. 
                for (int i = 0; i < data.Length; i++)
                {
                    sBuilder.Append(data[i].ToString("x2"));
                }

                // Return the hexadecimal string. 
                hash = sBuilder.ToString();
            }

            return hash;
        }

        /// <summary>
        /// Verify an MD5 hash against a string. 
        /// </summary>
        /// <param name="input">Unencrypted string</param>
        /// <param name="hash">Encrypted string</param>
        /// <returns>True if encrypted input is the same as the hash, else false</returns>
        public static bool VerifyMd5Hash(string input, string hash, string salt = "")
        {
            bool verified = false;

            using (MD5 md5Hash = MD5.Create())
            {
                // Hash the input. 
                string hashOfInput = GetMD5Hash(input, salt);

                // Create a StringComparer an compare the hashes.
                StringComparer comparer = StringComparer.OrdinalIgnoreCase;

                if (0 == comparer.Compare(hashOfInput, hash))
                {
                    verified = true;
                }
            }

            return verified;
        }

        /// <summary>
        /// Determines if a password is sufficiently complex.
        /// </summary>
        /// <param name="password">Password to validate</param>
        /// <param name="minLength">Minimum number of password characters.</param>
        /// <param name="numUpper">Minimum number of uppercase characters.</param>
        /// <param name="numLower">Minimum number of lowercase characters.</param>
        /// <param name="numNumbers">Minimum number of numeric characters.</param>
        /// <param name="numSpecial">Minimum number of special characters.</param>
        /// <returns>True if the password is sufficiently complex.</returns>
        public static bool VerifyComplexPassword(string password, int minLength = 8, int numUpper = 2, int numLower = 2, int numNumbers = 2, int numSpecial = 2)
        {
            //Replace [A-Z] with \p{Lu}, to allow for Unicode uppercase letters. 
            var upper = new Regex("[A-Z]");
            var lower = new Regex("[a-z]");
            var number = new Regex("[0-9]");
    
            //Special is "none of the above". 
            var special = new Regex("[^a-zA-Z0-9]");

            //Check the length. 
            if (password.Length < minLength)
                return false;

            //Check for minimum number of occurrences. 
            if (upper.Matches(password).Count < numUpper)
                return false; 
            if (lower.Matches(password).Count < numLower)
                return false; 
            if (number.Matches(password).Count < numNumbers)
                return false;
            if (special.Matches(password).Count < numSpecial)
                return false;

            //Passed all checks. 
            return true;
        }

        #endregion
    }
}
