#region Using Derivatives
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using KaiZen.CSMS.Services;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Encryption;
using System.Data.SqlClient;
#endregion

/// <summary>
/// Summary description for Auth
/// </summary>
public class Auth
{
    #region Variables
    private string AlcoaDirectHTTPParameter = "SESSION";
    private string WAOWindowsSecurityRole = "NT AUTHORITY\\NETWORK";
    private string AlcoaSecretKey = "AlcoaSecretKey";
    private string LoggedIn = "LoggedIn";
    private bool _AlcoaDirect;
    private bool _valid;
    private string _currentUserIdentity;
    private string _AlcoaDirectEmail;
    private string _errorMsg;
    private int _userId;
    private int _CompanyId;
    private int _RoleId;
    private int _CompanyStatus2Id;
    private string _UserLogon;
    private string _LastName;
    private string _FirstName;
    private string _Email;
    private string _Role;
    private string _CompanyName;
    private string _CompanyABN;
    #endregion

    public Auth()
    {
        _currentUserIdentity = HttpContext.Current.User.Identity.Name.ToString();//Uncommented by Debashis for get actual User value 

        //_currentUserIdentity = @"AUSTRALIA_ASIA\FOC05YL";//commented by Debashis for HardCode value 
        //Role : Reader
        //_currentUserIdentity = @"AUSTRALIA_ASIA\huh10oo";
        //Role : Reader, Susanty.Bradley@alcoa.com.au (Procurement Alcoa)
        //_currentUserIdentity = @"AUSTRALIA_ASIA\kwk78sb";

              
        //Using AlcoaDirect GateWay Check

        if (HttpContext.Current.User.Identity.IsAuthenticated == false) { _AlcoaDirect = true; }
        else { _AlcoaDirect = false; }
        //_AlcoaDirect = true;

    }

    public void authAlcoaDirect(string value)
    {
        if (value != "")
        {
            _AlcoaDirectEmail = value.ToLower();
            _valid = true;
        }
        else
        {
            _valid = false;
            _errorMsg = "You do not appear to be coming in from AlcoaDirect.";
        }
    }
    public void authAlcoaLan()
    {
        #region Code to Run in Local
        _valid = true; 
        #endregion
        #region Code to Run in Server
        //if (Roles.IsUserInRole(WAOWindowsSecurityRole) == true)
        //{
        //    //TODO: Also check that user exists inside of database & populate privateVars
        //    _valid = true;
        //}
        //else
        //{
        //    _valid = false;
        //    _errorMsg = "You do not appear to be within the security role designated to all WAO CSM WP users";
        //}
        #endregion
    }
    public void getUserDetails()
    {
        if (!_AlcoaDirect)
        { // external
            UsersCompaniesService usersCompaniesService = new UsersCompaniesService();
            //VList<UsersCompanies> usersCompaniesList = usersCompaniesService.GetByUserLogon(_currentUserIdentity);
            
            UsersCompaniesFilters ucFilters = new UsersCompaniesFilters();
            ucFilters.Append(UsersCompaniesColumn.UserLogon, _currentUserIdentity);
            int count = 0;
            VList<UsersCompanies> ucList = DataRepository.UsersCompaniesProvider.GetPaged(ucFilters.ToString(), null, 0, 100, out count);
            _UserLogon = _currentUserIdentity;

            foreach (UsersCompanies usersCompanies in ucList)
            {
                _userId = usersCompanies.UserId;
                _CompanyId = usersCompanies.CompanyId;
                _CompanyName = usersCompanies.CompanyName.ToString();
                _CompanyABN = usersCompanies.CompanyAbn;
                _CompanyStatus2Id = usersCompanies.CompanyStatus2Id;

                UserCompanyService ucSer = new UserCompanyService();
                TList<UserCompany> ucList1 = ucSer.GetByUserId(_userId);
                
                foreach (UserCompany uc in ucList1)
                {
                    if (uc.Active == true)
                    {
                        _CompanyId=uc.CompanyId;
                        CompaniesService compSer = new CompaniesService();
                        Companies comp = compSer.GetByCompanyId(_CompanyId);
                        _CompanyName = comp.CompanyName;
                        _CompanyABN = comp.CompanyAbn;
                        _CompanyStatus2Id = comp.CompanyStatus2Id;
                    }
                        
                }              
                _RoleId = Convert.ToInt32(usersCompanies.RoleId);
                _FirstName = usersCompanies.FirstName;
                _LastName = usersCompanies.LastName;
                _Role = usersCompanies.Role;
                _Email = usersCompanies.Email;
                
            }
        }
        else
        { // internal
            UsersCompaniesService usersCompaniesService = new UsersCompaniesService();
            //VList<UsersCompanies> usersCompaniesList = usersCompaniesService.GetByUserLogon(_AlcoaDirectEmail);
            UsersCompaniesFilters ucFilters = new UsersCompaniesFilters();
            ucFilters.Append(UsersCompaniesColumn.UserLogon, _AlcoaDirectEmail);
            int count = 0;
            VList<UsersCompanies> ucList = DataRepository.UsersCompaniesProvider.GetPaged(ucFilters.ToString(), null, 0, 100, out count);
            
            _UserLogon = _AlcoaDirectEmail;

            foreach (UsersCompanies usersCompanies in ucList)
            {
                _userId = usersCompanies.UserId;
                _CompanyId = usersCompanies.CompanyId;
                _CompanyName = usersCompanies.CompanyName.ToString();
                _RoleId = Convert.ToInt32(usersCompanies.RoleId);
                _CompanyABN = usersCompanies.CompanyAbn;
                _CompanyStatus2Id = usersCompanies.CompanyStatus2Id;

                UserCompanyService ucSer = new UserCompanyService();
                TList<UserCompany> ucList1 = ucSer.GetByUserId(_userId);
                foreach (UserCompany uc in ucList1)
                {
                    if (uc.Active == true)
                    {
                        _CompanyId = uc.CompanyId;
                        CompaniesService compSer = new CompaniesService();
                        Companies comp = compSer.GetByCompanyId(_CompanyId);
                        _CompanyName = comp.CompanyName;
                        _CompanyABN = comp.CompanyAbn;
                        _CompanyStatus2Id = comp.CompanyStatus2Id;
                    }

                }
                _FirstName = usersCompanies.FirstName;
                _LastName = usersCompanies.LastName;
                _Role = usersCompanies.Role;
                _Email = usersCompanies.Email;
               
            }
        }
    }

    //Added By Bishwajit Sahoo for Item#14
    public string getUserLogon()
    {
        string userLogon = string.Empty;
        if (!_AlcoaDirect)
        {
            userLogon = _currentUserIdentity;
        }
        else
        {
            userLogon = _AlcoaDirectEmail;
        }
        return userLogon;

    }
    //End Added By Bishwajit Sahoo for Item#14

    //Added By Bishwajit Sahoo for Item#14
    public void getUserDetails(int companyId)
    {
        if (!_AlcoaDirect)
        { // external
            UsersCompaniesService usersCompaniesService = new UsersCompaniesService();
            //VList<UsersCompanies> usersCompaniesList = usersCompaniesService.GetByUserLogon(_currentUserIdentity);

            UsersCompaniesFilters ucFilters = new UsersCompaniesFilters();
            ucFilters.Append(UsersCompaniesColumn.UserLogon, _currentUserIdentity);
            int count = 0;
            VList<UsersCompanies> ucList = DataRepository.UsersCompaniesProvider.GetPaged(ucFilters.ToString(), null, 0, 100, out count);
            _UserLogon = _currentUserIdentity;

            foreach (UsersCompanies usersCompanies in ucList)
            {
                _userId = usersCompanies.UserId;
                CompaniesService compSer = new CompaniesService();
                Companies comp = compSer.GetByCompanyId(companyId);
                if (comp != null)
                {
                    _CompanyId = companyId;
                    _CompanyName = comp.CompanyName;
                    _CompanyABN = comp.CompanyAbn;
                    _CompanyStatus2Id = comp.CompanyStatus2Id;
                }
                else
                {
                    _CompanyId = usersCompanies.CompanyId;
                    _CompanyName = usersCompanies.CompanyName.ToString();
                    _CompanyABN = usersCompanies.CompanyAbn;
                    _CompanyStatus2Id = usersCompanies.CompanyStatus2Id;
                }
                _RoleId = Convert.ToInt32(usersCompanies.RoleId);

                
                _FirstName = usersCompanies.FirstName;
                _LastName = usersCompanies.LastName;
                _Role = usersCompanies.Role;
                _Email = usersCompanies.Email;
                
            }
        }
        else
        { // internal
            UsersCompaniesService usersCompaniesService = new UsersCompaniesService();
            //VList<UsersCompanies> usersCompaniesList = usersCompaniesService.GetByUserLogon(_AlcoaDirectEmail);
            UsersCompaniesFilters ucFilters = new UsersCompaniesFilters();
            ucFilters.Append(UsersCompaniesColumn.UserLogon, _AlcoaDirectEmail);
            int count = 0;
            VList<UsersCompanies> ucList = DataRepository.UsersCompaniesProvider.GetPaged(ucFilters.ToString(), null, 0, 100, out count);

            _UserLogon = _AlcoaDirectEmail;

            foreach (UsersCompanies usersCompanies in ucList)
            {
                _userId = usersCompanies.UserId;
                CompaniesService compSer = new CompaniesService();
                Companies comp = compSer.GetByCompanyId(companyId);
                if (comp != null)
                {
                    _CompanyId = companyId;
                    _CompanyName = comp.CompanyName;
                    _CompanyABN = comp.CompanyAbn;
                    _CompanyStatus2Id = comp.CompanyStatus2Id;
                }
                else
                {
                    _CompanyId = usersCompanies.CompanyId;
                    _CompanyName = usersCompanies.CompanyName.ToString();
                    _CompanyABN = usersCompanies.CompanyAbn;
                    _CompanyStatus2Id = usersCompanies.CompanyStatus2Id;
                }
                _RoleId = Convert.ToInt32(usersCompanies.RoleId);
                
                _FirstName = usersCompanies.FirstName;
                _LastName = usersCompanies.LastName;
                _Role = usersCompanies.Role;
                _Email = usersCompanies.Email;                
            }
        }
    }
    //End Added By Bishwajit Sahoo for Item#14

    public bool isPrivledged(int UserId)
    {
        using (SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["ALCOA_WAO_CSMWP_devConnectionString"].ToString()))
        {
            cn.Open();
            SqlCommand cmd = new SqlCommand(String.Format("SELECT COUNT(*) FROM dbo.[UsersPrivileged] WHERE UserId = {0}", UserId), cn);

            SqlDataReader rdr = cmd.ExecuteReader();
            rdr.Read();
            if (rdr[0].ToString() != "0")
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
    #region Get/Set
    public string _AlcoaDirectHTTPParameter
    {
        get { return AlcoaDirectHTTPParameter; }
        set { AlcoaDirectHTTPParameter = value; }
    }
    public string _WAOWindowsSecurityRole
    {
        get { return WAOWindowsSecurityRole; }
        set { WAOWindowsSecurityRole = value; }
    }
    public string _LoggedIn
    {
        get { return LoggedIn; }
        set { LoggedIn = value; }
    }

    public bool isAlcoaDirect
    {
        get { return _AlcoaDirect; }
        set { _AlcoaDirect = value; }
    }
    public bool isValid
    {
        get { return _valid; }
        set { _valid = value; }
    }

    public string currentUserIdentity
    {
        get { return _currentUserIdentity; }
        set { _currentUserIdentity = value; }
    }
    public string AlcoaDirectEmail
    {
        get { return _AlcoaDirectEmail; }
        set { _AlcoaDirectEmail = value; }
    }
    public string errorMsg
    {
        get { return _errorMsg; }
        set { _errorMsg = value; }
    }

    public int UserId
    {
        get { return _userId; }
        set { _userId = value; }
    }
    public int CompanyId
    {
        get { return _CompanyId; }
        set { _CompanyId = value; }
    }
    public int RoleId
    {
        get { return _RoleId; }
        set { _RoleId = value; }
    }
    public int CompanyStatus2Id
    {
        get { return _CompanyStatus2Id; }
        set { _CompanyStatus2Id = value; }
    }

    public string UserLogon
    {
        get { return _UserLogon; }
        set { _UserLogon = value; }
    }
    public string LastName
    {
        get { return _LastName; }
        set { _LastName = value; }
    }
    public string FirstName
    {
        get { return _FirstName; }
        set { _FirstName = value; }
    }
    public string Email
    {
        get { return _Email; }
        set { _Email = value; }
    }
    public string Role
    {
        get { return _Role; }
        set { _Role = value; }
    }
    public string CompanyName
    {
        get { return _CompanyName; }
        set { _CompanyName = value; }
    }
    public string CompanyABN
    {
        get { return _CompanyABN; }
        set { _CompanyABN = value; }
    }



    public string Decrypt(string ciphertext)
    {
        return Crypt.DecryptTripleDES(ciphertext, AlcoaSecretKey);
    }
    public string Encrypt(string plaintext)
    {
        return Crypt.EncryptTripleDES(plaintext, AlcoaSecretKey);
    }




    #endregion




}
