﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Repo.CSMS.DAL.Infrastructure
{
    public interface IUnitOfWork : IDisposable
    {
        void Commit();
        bool LazyLoadingEnabled { set; get; }
    }
}
