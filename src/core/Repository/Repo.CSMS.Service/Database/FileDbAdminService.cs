﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface IFileDbAdminService
    {
        FileDbAdmin Get(Expression<Func<FileDbAdmin, bool>> where, List<Expression<Func<FileDbAdmin, object>>> includes);
        List<FileDbAdmin> GetMany(int? top, Expression<Func<FileDbAdmin, bool>> where, Func<IQueryable<FileDbAdmin>, IOrderedQueryable<FileDbAdmin>> orderBy, List<Expression<Func<FileDbAdmin, object>>> includes);
        List<FileDbAdmin> GetManyPaged(int pageNumber, int pageSize, Expression<Func<FileDbAdmin, bool>> where, Func<IQueryable<FileDbAdmin>, IOrderedQueryable<FileDbAdmin>> orderBy, List<Expression<Func<FileDbAdmin, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<FileDbAdmin, bool>> where, List<Expression<Func<FileDbAdmin, object>>> includes);
        void Insert(FileDbAdmin item, bool saveChanges = true);
        void Update(FileDbAdmin item, bool saveChanges = true);
        void Delete(FileDbAdmin item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class FileDbAdminService : IFileDbAdminService
    {
        private readonly IRepository<FileDbAdmin> _repository;

        public FileDbAdminService(IRepository<FileDbAdmin> repository)
        {
            this._repository = repository;
        }

        public FileDbAdmin Get(Expression<Func<FileDbAdmin, bool>> where, List<Expression<Func<FileDbAdmin, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<FileDbAdmin> GetMany(int? top, Expression<Func<FileDbAdmin, bool>> where, Func<IQueryable<FileDbAdmin>, IOrderedQueryable<FileDbAdmin>> orderBy, List<Expression<Func<FileDbAdmin, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<FileDbAdmin> GetManyPaged(int pageNumber, int pageSize, Expression<Func<FileDbAdmin, bool>> where, Func<IQueryable<FileDbAdmin>, IOrderedQueryable<FileDbAdmin>> orderBy, List<Expression<Func<FileDbAdmin, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<FileDbAdmin, bool>> where, List<Expression<Func<FileDbAdmin, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(FileDbAdmin item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("FileDbAdmin");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(FileDbAdmin item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("FileDbAdmin");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(FileDbAdmin item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("FileDbAdmin");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
