﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.Web;
using System.Web.Security;
using System.Web.Routing;

namespace Repo.CSMS.Service.Security
{
    #region Security Helper

    public class SecurityHelper
    {
        /// <summary>
        /// Check if the currently logged in user (for httpContext use only) has the required roles to access the item.
        /// </summary>
        /// <param name="roleNames">The role name to check (can be comma separated)</param>
        /// <param name="startsWith">True if we want to check the start of users roles; False if we want to perform an exact match for the users roles</param>
        /// <returns>True if the currently logged in user has permission to view the item, else False</returns>
        public static bool ShowItemForCurrentUser(string roleNames, bool startsWith = false)
        {
            bool showItem = false;

            roleNames = roleNames.Trim();

            if (roleNames != string.Empty)
            {
                try
                {
                    //Get list of current users roles
                    List<string> userRoles = new List<string>(Roles.GetRolesForUser(System.Web.HttpContext.Current.User.Identity.Name));

                    //Split roleNames if provided as comma separated
                    List<string> roleNameArray = new List<string>(roleNames.Split(','));

                    //Loop through each roleName
                    foreach (string roleName in roleNameArray)
                    {
                        //Create instance of roleName so we can dissect it
                        string roleNameInstance = roleName.Trim().ToLower();

                        //Loop while the roleNameInstance contains a "."
                        while (roleNameInstance != string.Empty)
                        {
                            //Check if the users roles startsWith (or equals) the roleNameInstance
                            if (startsWith)
                                showItem = userRoles.Any(r => r.StartsWith(roleNameInstance));
                            else
                                showItem = userRoles.Any(r => r.Equals(roleNameInstance));

                            //If contains, then user has permission.  If we run out of "." then we exit the loop, else we dissect the roleNameInstance and check again.
                            if (showItem || (roleNameInstance.IndexOf('.') < 0))
                                break;
                            else
                                roleNameInstance = roleNameInstance.Substring(0, roleNameInstance.LastIndexOf('.'));
                        }

                        //Found permission so exit
                        if (showItem)
                            break;
                    }
                }
                catch { }
            }

            return showItem;
        }
    }

    #endregion

    #region Custom Authorize Attributes

    public class CSMSAuthorizeAttribute : AuthorizeAttribute
    {
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            base.OnAuthorization(filterContext);

            if (filterContext.Result is HttpUnauthorizedResult)
            {
                filterContext.Result = new RedirectToRouteResult(
                    new RouteValueDictionary
                {
                    { "folder", filterContext.RouteData.Values["folder"] },
                    { "controller", "Account" },
                    { "action", "Login" },
                    { "ReturnUrl", filterContext.HttpContext.Request.RawUrl }
                });
            }
        }
    }

    #endregion
}
