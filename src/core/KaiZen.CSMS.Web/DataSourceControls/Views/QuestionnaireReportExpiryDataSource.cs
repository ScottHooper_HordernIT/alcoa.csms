﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.QuestionnaireReportExpiryProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[CLSCompliant(true)]
	[Designer(typeof(QuestionnaireReportExpiryDataSourceDesigner))]
	public class QuestionnaireReportExpiryDataSource : ReadOnlyDataSource<QuestionnaireReportExpiry>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportExpiryDataSource class.
		/// </summary>
		public QuestionnaireReportExpiryDataSource() : base(new QuestionnaireReportExpiryService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the QuestionnaireReportExpiryDataSourceView used by the QuestionnaireReportExpiryDataSource.
		/// </summary>
		protected QuestionnaireReportExpiryDataSourceView QuestionnaireReportExpiryView
		{
			get { return ( View as QuestionnaireReportExpiryDataSourceView ); }
		}
		
		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the QuestionnaireReportExpiryDataSourceView class that is to be
		/// used by the QuestionnaireReportExpiryDataSource.
		/// </summary>
		/// <returns>An instance of the QuestionnaireReportExpiryDataSourceView class.</returns>
		protected override BaseDataSourceView<QuestionnaireReportExpiry, Object> GetNewDataSourceView()
		{
			return new QuestionnaireReportExpiryDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the QuestionnaireReportExpiryDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class QuestionnaireReportExpiryDataSourceView : ReadOnlyDataSourceView<QuestionnaireReportExpiry>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportExpiryDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the QuestionnaireReportExpiryDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public QuestionnaireReportExpiryDataSourceView(QuestionnaireReportExpiryDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal QuestionnaireReportExpiryDataSource QuestionnaireReportExpiryOwner
		{
			get { return Owner as QuestionnaireReportExpiryDataSource; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal QuestionnaireReportExpiryService QuestionnaireReportExpiryProvider
		{
			get { return Provider as QuestionnaireReportExpiryService; }
		}

		#endregion Properties
		
		#region Methods
		
		#endregion Methods
	}

	#region QuestionnaireReportExpiryDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the QuestionnaireReportExpiryDataSource class.
	/// </summary>
	public class QuestionnaireReportExpiryDataSourceDesigner : ReadOnlyDataSourceDesigner<QuestionnaireReportExpiry>
	{
	}

	#endregion QuestionnaireReportExpiryDataSourceDesigner

	#region QuestionnaireReportExpiryFilter

	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireReportExpiry"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireReportExpiryFilter : SqlFilter<QuestionnaireReportExpiryColumn>
	{
	}

	#endregion QuestionnaireReportExpiryFilter

	#region QuestionnaireReportExpiryExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireReportExpiry"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireReportExpiryExpressionBuilder : SqlExpressionBuilder<QuestionnaireReportExpiryColumn>
	{
	}
	
	#endregion QuestionnaireReportExpiryExpressionBuilder		
}

