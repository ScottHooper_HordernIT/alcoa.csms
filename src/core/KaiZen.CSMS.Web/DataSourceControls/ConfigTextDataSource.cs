﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.ConfigTextProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(ConfigTextDataSourceDesigner))]
	public class ConfigTextDataSource : ProviderDataSource<ConfigText, ConfigTextKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ConfigTextDataSource class.
		/// </summary>
		public ConfigTextDataSource() : base(new ConfigTextService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the ConfigTextDataSourceView used by the ConfigTextDataSource.
		/// </summary>
		protected ConfigTextDataSourceView ConfigTextView
		{
			get { return ( View as ConfigTextDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the ConfigTextDataSource control invokes to retrieve data.
		/// </summary>
		public ConfigTextSelectMethod SelectMethod
		{
			get
			{
				ConfigTextSelectMethod selectMethod = ConfigTextSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (ConfigTextSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the ConfigTextDataSourceView class that is to be
		/// used by the ConfigTextDataSource.
		/// </summary>
		/// <returns>An instance of the ConfigTextDataSourceView class.</returns>
		protected override BaseDataSourceView<ConfigText, ConfigTextKey> GetNewDataSourceView()
		{
			return new ConfigTextDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the ConfigTextDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class ConfigTextDataSourceView : ProviderDataSourceView<ConfigText, ConfigTextKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ConfigTextDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the ConfigTextDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public ConfigTextDataSourceView(ConfigTextDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal ConfigTextDataSource ConfigTextOwner
		{
			get { return Owner as ConfigTextDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal ConfigTextSelectMethod SelectMethod
		{
			get { return ConfigTextOwner.SelectMethod; }
			set { ConfigTextOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal ConfigTextService ConfigTextProvider
		{
			get { return Provider as ConfigTextService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<ConfigText> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<ConfigText> results = null;
			ConfigText item;
			count = 0;
			
			System.Int32 _configTextId;
			System.Int32 _configTextTypeId;

			switch ( SelectMethod )
			{
				case ConfigTextSelectMethod.Get:
					ConfigTextKey entityKey  = new ConfigTextKey();
					entityKey.Load(values);
					item = ConfigTextProvider.Get(entityKey);
					results = new TList<ConfigText>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case ConfigTextSelectMethod.GetAll:
                    results = ConfigTextProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case ConfigTextSelectMethod.GetPaged:
					results = ConfigTextProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case ConfigTextSelectMethod.Find:
					if ( FilterParameters != null )
						results = ConfigTextProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = ConfigTextProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case ConfigTextSelectMethod.GetByConfigTextId:
					_configTextId = ( values["ConfigTextId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["ConfigTextId"], typeof(System.Int32)) : (int)0;
					item = ConfigTextProvider.GetByConfigTextId(_configTextId);
					results = new TList<ConfigText>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				// FK
				case ConfigTextSelectMethod.GetByConfigTextTypeId:
					_configTextTypeId = ( values["ConfigTextTypeId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["ConfigTextTypeId"], typeof(System.Int32)) : (int)0;
					results = ConfigTextProvider.GetByConfigTextTypeId(_configTextTypeId, this.StartIndex, this.PageSize, out count);
					break;
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == ConfigTextSelectMethod.Get || SelectMethod == ConfigTextSelectMethod.GetByConfigTextId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				ConfigText entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					ConfigTextProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<ConfigText> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			ConfigTextProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region ConfigTextDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the ConfigTextDataSource class.
	/// </summary>
	public class ConfigTextDataSourceDesigner : ProviderDataSourceDesigner<ConfigText, ConfigTextKey>
	{
		/// <summary>
		/// Initializes a new instance of the ConfigTextDataSourceDesigner class.
		/// </summary>
		public ConfigTextDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public ConfigTextSelectMethod SelectMethod
		{
			get { return ((ConfigTextDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new ConfigTextDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region ConfigTextDataSourceActionList

	/// <summary>
	/// Supports the ConfigTextDataSourceDesigner class.
	/// </summary>
	internal class ConfigTextDataSourceActionList : DesignerActionList
	{
		private ConfigTextDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the ConfigTextDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public ConfigTextDataSourceActionList(ConfigTextDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public ConfigTextSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion ConfigTextDataSourceActionList
	
	#endregion ConfigTextDataSourceDesigner
	
	#region ConfigTextSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the ConfigTextDataSource.SelectMethod property.
	/// </summary>
	public enum ConfigTextSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByConfigTextId method.
		/// </summary>
		GetByConfigTextId,
		/// <summary>
		/// Represents the GetByConfigTextTypeId method.
		/// </summary>
		GetByConfigTextTypeId
	}
	
	#endregion ConfigTextSelectMethod

	#region ConfigTextFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ConfigText"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ConfigTextFilter : SqlFilter<ConfigTextColumn>
	{
	}
	
	#endregion ConfigTextFilter

	#region ConfigTextExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ConfigText"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ConfigTextExpressionBuilder : SqlExpressionBuilder<ConfigTextColumn>
	{
	}
	
	#endregion ConfigTextExpressionBuilder	

	#region ConfigTextProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;ConfigTextChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="ConfigText"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ConfigTextProperty : ChildEntityProperty<ConfigTextChildEntityTypes>
	{
	}
	
	#endregion ConfigTextProperty
}

