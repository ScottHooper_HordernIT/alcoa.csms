﻿#region Using directives

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="QuestionnaireReportServicesListAllProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract class QuestionnaireReportServicesListAllProviderBaseCore : EntityViewProviderBase<QuestionnaireReportServicesListAll>
	{
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions
		
		/*
		///<summary>
		/// Fill an VList&lt;QuestionnaireReportServicesListAll&gt; From a DataSet
		///</summary>
		/// <param name="dataSet">the DataSet</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList&lt;QuestionnaireReportServicesListAll&gt;"/></returns>
		protected static VList&lt;QuestionnaireReportServicesListAll&gt; Fill(DataSet dataSet, VList<QuestionnaireReportServicesListAll> rows, int start, int pagelen)
		{
			if (dataSet.Tables.Count == 1)
			{
				return Fill(dataSet.Tables[0], rows, start, pagelen);
			}
			else
			{
				return new VList<QuestionnaireReportServicesListAll>();
			}	
		}
		
		
		///<summary>
		/// Fill an VList&lt;QuestionnaireReportServicesListAll&gt; From a DataTable
		///</summary>
		/// <param name="dataTable">the DataTable that hold the data.</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList<QuestionnaireReportServicesListAll>"/></returns>
		protected static VList&lt;QuestionnaireReportServicesListAll&gt; Fill(DataTable dataTable, VList<QuestionnaireReportServicesListAll> rows, int start, int pagelen)
		{
			int recordnum = 0;
			
			System.Collections.IEnumerator dataRows =  dataTable.Rows.GetEnumerator();
			
			while (dataRows.MoveNext() && (pagelen != 0))
			{
				if(recordnum >= start)
				{
					DataRow row = (DataRow)dataRows.Current;
				
					QuestionnaireReportServicesListAll c = new QuestionnaireReportServicesListAll();
					c.QuestionnaireId = (Convert.IsDBNull(row["QuestionnaireId"]))?(int)0:(System.Int32)row["QuestionnaireId"];
					c.QuestionnaireReportServices = (Convert.IsDBNull(row["QuestionnaireReportServices"]))?string.Empty:(System.String)row["QuestionnaireReportServices"];
					c.AcceptChanges();
					rows.Add(c);
					pagelen -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		*/	
						
		///<summary>
		/// Fill an <see cref="VList&lt;QuestionnaireReportServicesListAll&gt;"/> From a DataReader.
		///</summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pageLength">number of row.</param>
		///<returns>a <see cref="VList&lt;QuestionnaireReportServicesListAll&gt;"/></returns>
		protected VList<QuestionnaireReportServicesListAll> Fill(IDataReader reader, VList<QuestionnaireReportServicesListAll> rows, int start, int pageLength)
		{
			int recordnum = 0;
			while (reader.Read() && (pageLength != 0))
			{
				if(recordnum >= start)
				{
					QuestionnaireReportServicesListAll entity = null;
					if (DataRepository.Provider.UseEntityFactory)
					{
						entity = EntityManager.CreateViewEntity<QuestionnaireReportServicesListAll>("QuestionnaireReportServicesListAll",  DataRepository.Provider.EntityCreationalFactoryType); 
					}
					else
					{
						entity = new QuestionnaireReportServicesListAll();
					}
					
					entity.SuppressEntityEvents = true;

					entity.QuestionnaireId = (System.Int32)reader[((int)QuestionnaireReportServicesListAllColumn.QuestionnaireId)];
					//entity.QuestionnaireId = (Convert.IsDBNull(reader["QuestionnaireId"]))?(int)0:(System.Int32)reader["QuestionnaireId"];
					entity.QuestionnaireReportServices = (reader.IsDBNull(((int)QuestionnaireReportServicesListAllColumn.QuestionnaireReportServices)))?null:(System.String)reader[((int)QuestionnaireReportServicesListAllColumn.QuestionnaireReportServices)];
					//entity.QuestionnaireReportServices = (Convert.IsDBNull(reader["QuestionnaireReportServices"]))?string.Empty:(System.String)reader["QuestionnaireReportServices"];
					entity.AcceptChanges();
					entity.SuppressEntityEvents = false;
					
					rows.Add(entity);
					pageLength -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		
		
		/// <summary>
		/// Refreshes the <see cref="QuestionnaireReportServicesListAll"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="QuestionnaireReportServicesListAll"/> object to refresh.</param>
		protected void RefreshEntity(IDataReader reader, QuestionnaireReportServicesListAll entity)
		{
			reader.Read();
			entity.QuestionnaireId = (System.Int32)reader[((int)QuestionnaireReportServicesListAllColumn.QuestionnaireId)];
			//entity.QuestionnaireId = (Convert.IsDBNull(reader["QuestionnaireId"]))?(int)0:(System.Int32)reader["QuestionnaireId"];
			entity.QuestionnaireReportServices = (reader.IsDBNull(((int)QuestionnaireReportServicesListAllColumn.QuestionnaireReportServices)))?null:(System.String)reader[((int)QuestionnaireReportServicesListAllColumn.QuestionnaireReportServices)];
			//entity.QuestionnaireReportServices = (Convert.IsDBNull(reader["QuestionnaireReportServices"]))?string.Empty:(System.String)reader["QuestionnaireReportServices"];
			reader.Close();
	
			entity.AcceptChanges();
		}
		
		/*
		/// <summary>
		/// Refreshes the <see cref="QuestionnaireReportServicesListAll"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="QuestionnaireReportServicesListAll"/> object.</param>
		protected static void RefreshEntity(DataSet dataSet, QuestionnaireReportServicesListAll entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.QuestionnaireId = (Convert.IsDBNull(dataRow["QuestionnaireId"]))?(int)0:(System.Int32)dataRow["QuestionnaireId"];
			entity.QuestionnaireReportServices = (Convert.IsDBNull(dataRow["QuestionnaireReportServices"]))?string.Empty:(System.String)dataRow["QuestionnaireReportServices"];
			entity.AcceptChanges();
		}
		*/
			
		#endregion Helper Functions
	}//end class

	#region QuestionnaireReportServicesListAllFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireReportServicesListAll"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireReportServicesListAllFilterBuilder : SqlFilterBuilder<QuestionnaireReportServicesListAllColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportServicesListAllFilterBuilder class.
		/// </summary>
		public QuestionnaireReportServicesListAllFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportServicesListAllFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireReportServicesListAllFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportServicesListAllFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireReportServicesListAllFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireReportServicesListAllFilterBuilder

	#region QuestionnaireReportServicesListAllParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireReportServicesListAll"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireReportServicesListAllParameterBuilder : ParameterizedSqlFilterBuilder<QuestionnaireReportServicesListAllColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportServicesListAllParameterBuilder class.
		/// </summary>
		public QuestionnaireReportServicesListAllParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportServicesListAllParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireReportServicesListAllParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportServicesListAllParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireReportServicesListAllParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireReportServicesListAllParameterBuilder
	
	#region QuestionnaireReportServicesListAllSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireReportServicesListAll"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class QuestionnaireReportServicesListAllSortBuilder : SqlSortBuilder<QuestionnaireReportServicesListAllColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportServicesListAllSqlSortBuilder class.
		/// </summary>
		public QuestionnaireReportServicesListAllSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion QuestionnaireReportServicesListAllSortBuilder

} // end namespace
