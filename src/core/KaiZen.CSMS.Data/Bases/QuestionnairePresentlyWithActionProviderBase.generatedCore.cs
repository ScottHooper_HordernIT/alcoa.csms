﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="QuestionnairePresentlyWithActionProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class QuestionnairePresentlyWithActionProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.QuestionnairePresentlyWithAction, KaiZen.CSMS.Entities.QuestionnairePresentlyWithActionKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.QuestionnairePresentlyWithActionKey key)
		{
			return Delete(transactionManager, key.QuestionnairePresentlyWithActionId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_questionnairePresentlyWithActionId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _questionnairePresentlyWithActionId)
		{
			return Delete(null, _questionnairePresentlyWithActionId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnairePresentlyWithActionId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _questionnairePresentlyWithActionId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.QuestionnairePresentlyWithAction Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.QuestionnairePresentlyWithActionKey key, int start, int pageLength)
		{
			return GetByQuestionnairePresentlyWithActionId(transactionManager, key.QuestionnairePresentlyWithActionId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_QuestionnairePresentlyWithAction index.
		/// </summary>
		/// <param name="_questionnairePresentlyWithActionId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnairePresentlyWithAction"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnairePresentlyWithAction GetByQuestionnairePresentlyWithActionId(System.Int32 _questionnairePresentlyWithActionId)
		{
			int count = -1;
			return GetByQuestionnairePresentlyWithActionId(null,_questionnairePresentlyWithActionId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnairePresentlyWithAction index.
		/// </summary>
		/// <param name="_questionnairePresentlyWithActionId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnairePresentlyWithAction"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnairePresentlyWithAction GetByQuestionnairePresentlyWithActionId(System.Int32 _questionnairePresentlyWithActionId, int start, int pageLength)
		{
			int count = -1;
			return GetByQuestionnairePresentlyWithActionId(null, _questionnairePresentlyWithActionId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnairePresentlyWithAction index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnairePresentlyWithActionId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnairePresentlyWithAction"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnairePresentlyWithAction GetByQuestionnairePresentlyWithActionId(TransactionManager transactionManager, System.Int32 _questionnairePresentlyWithActionId)
		{
			int count = -1;
			return GetByQuestionnairePresentlyWithActionId(transactionManager, _questionnairePresentlyWithActionId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnairePresentlyWithAction index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnairePresentlyWithActionId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnairePresentlyWithAction"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnairePresentlyWithAction GetByQuestionnairePresentlyWithActionId(TransactionManager transactionManager, System.Int32 _questionnairePresentlyWithActionId, int start, int pageLength)
		{
			int count = -1;
			return GetByQuestionnairePresentlyWithActionId(transactionManager, _questionnairePresentlyWithActionId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnairePresentlyWithAction index.
		/// </summary>
		/// <param name="_questionnairePresentlyWithActionId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnairePresentlyWithAction"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnairePresentlyWithAction GetByQuestionnairePresentlyWithActionId(System.Int32 _questionnairePresentlyWithActionId, int start, int pageLength, out int count)
		{
			return GetByQuestionnairePresentlyWithActionId(null, _questionnairePresentlyWithActionId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnairePresentlyWithAction index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnairePresentlyWithActionId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnairePresentlyWithAction"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.QuestionnairePresentlyWithAction GetByQuestionnairePresentlyWithActionId(TransactionManager transactionManager, System.Int32 _questionnairePresentlyWithActionId, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_QuestionnairePresentlyWithAction_1 index.
		/// </summary>
		/// <param name="_questionnairePresentlyWithUserId"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;QuestionnairePresentlyWithAction&gt;"/> class.</returns>
		public TList<QuestionnairePresentlyWithAction> GetByQuestionnairePresentlyWithUserId(System.Int32 _questionnairePresentlyWithUserId)
		{
			int count = -1;
			return GetByQuestionnairePresentlyWithUserId(null,_questionnairePresentlyWithUserId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnairePresentlyWithAction_1 index.
		/// </summary>
		/// <param name="_questionnairePresentlyWithUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;QuestionnairePresentlyWithAction&gt;"/> class.</returns>
		public TList<QuestionnairePresentlyWithAction> GetByQuestionnairePresentlyWithUserId(System.Int32 _questionnairePresentlyWithUserId, int start, int pageLength)
		{
			int count = -1;
			return GetByQuestionnairePresentlyWithUserId(null, _questionnairePresentlyWithUserId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnairePresentlyWithAction_1 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnairePresentlyWithUserId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;QuestionnairePresentlyWithAction&gt;"/> class.</returns>
		public TList<QuestionnairePresentlyWithAction> GetByQuestionnairePresentlyWithUserId(TransactionManager transactionManager, System.Int32 _questionnairePresentlyWithUserId)
		{
			int count = -1;
			return GetByQuestionnairePresentlyWithUserId(transactionManager, _questionnairePresentlyWithUserId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnairePresentlyWithAction_1 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnairePresentlyWithUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;QuestionnairePresentlyWithAction&gt;"/> class.</returns>
		public TList<QuestionnairePresentlyWithAction> GetByQuestionnairePresentlyWithUserId(TransactionManager transactionManager, System.Int32 _questionnairePresentlyWithUserId, int start, int pageLength)
		{
			int count = -1;
			return GetByQuestionnairePresentlyWithUserId(transactionManager, _questionnairePresentlyWithUserId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnairePresentlyWithAction_1 index.
		/// </summary>
		/// <param name="_questionnairePresentlyWithUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;QuestionnairePresentlyWithAction&gt;"/> class.</returns>
		public TList<QuestionnairePresentlyWithAction> GetByQuestionnairePresentlyWithUserId(System.Int32 _questionnairePresentlyWithUserId, int start, int pageLength, out int count)
		{
			return GetByQuestionnairePresentlyWithUserId(null, _questionnairePresentlyWithUserId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnairePresentlyWithAction_1 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnairePresentlyWithUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;QuestionnairePresentlyWithAction&gt;"/> class.</returns>
		public abstract TList<QuestionnairePresentlyWithAction> GetByQuestionnairePresentlyWithUserId(TransactionManager transactionManager, System.Int32 _questionnairePresentlyWithUserId, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_QuestionnairePresentlyWithAction index.
		/// </summary>
		/// <param name="_actionName"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnairePresentlyWithAction"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnairePresentlyWithAction GetByActionName(System.String _actionName)
		{
			int count = -1;
			return GetByActionName(null,_actionName, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnairePresentlyWithAction index.
		/// </summary>
		/// <param name="_actionName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnairePresentlyWithAction"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnairePresentlyWithAction GetByActionName(System.String _actionName, int start, int pageLength)
		{
			int count = -1;
			return GetByActionName(null, _actionName, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnairePresentlyWithAction index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_actionName"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnairePresentlyWithAction"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnairePresentlyWithAction GetByActionName(TransactionManager transactionManager, System.String _actionName)
		{
			int count = -1;
			return GetByActionName(transactionManager, _actionName, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnairePresentlyWithAction index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_actionName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnairePresentlyWithAction"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnairePresentlyWithAction GetByActionName(TransactionManager transactionManager, System.String _actionName, int start, int pageLength)
		{
			int count = -1;
			return GetByActionName(transactionManager, _actionName, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnairePresentlyWithAction index.
		/// </summary>
		/// <param name="_actionName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnairePresentlyWithAction"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnairePresentlyWithAction GetByActionName(System.String _actionName, int start, int pageLength, out int count)
		{
			return GetByActionName(null, _actionName, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnairePresentlyWithAction index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_actionName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnairePresentlyWithAction"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.QuestionnairePresentlyWithAction GetByActionName(TransactionManager transactionManager, System.String _actionName, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_QuestionnairePresentlyWithAction_2 index.
		/// </summary>
		/// <param name="_processNo"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnairePresentlyWithAction"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnairePresentlyWithAction GetByProcessNo(System.Int32? _processNo)
		{
			int count = -1;
			return GetByProcessNo(null,_processNo, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnairePresentlyWithAction_2 index.
		/// </summary>
		/// <param name="_processNo"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnairePresentlyWithAction"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnairePresentlyWithAction GetByProcessNo(System.Int32? _processNo, int start, int pageLength)
		{
			int count = -1;
			return GetByProcessNo(null, _processNo, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnairePresentlyWithAction_2 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_processNo"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnairePresentlyWithAction"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnairePresentlyWithAction GetByProcessNo(TransactionManager transactionManager, System.Int32? _processNo)
		{
			int count = -1;
			return GetByProcessNo(transactionManager, _processNo, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnairePresentlyWithAction_2 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_processNo"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnairePresentlyWithAction"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnairePresentlyWithAction GetByProcessNo(TransactionManager transactionManager, System.Int32? _processNo, int start, int pageLength)
		{
			int count = -1;
			return GetByProcessNo(transactionManager, _processNo, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnairePresentlyWithAction_2 index.
		/// </summary>
		/// <param name="_processNo"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnairePresentlyWithAction"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnairePresentlyWithAction GetByProcessNo(System.Int32? _processNo, int start, int pageLength, out int count)
		{
			return GetByProcessNo(null, _processNo, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnairePresentlyWithAction_2 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_processNo"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnairePresentlyWithAction"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.QuestionnairePresentlyWithAction GetByProcessNo(TransactionManager transactionManager, System.Int32? _processNo, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;QuestionnairePresentlyWithAction&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;QuestionnairePresentlyWithAction&gt;"/></returns>
		public static TList<QuestionnairePresentlyWithAction> Fill(IDataReader reader, TList<QuestionnairePresentlyWithAction> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.QuestionnairePresentlyWithAction c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("QuestionnairePresentlyWithAction")
					.Append("|").Append((System.Int32)reader[((int)QuestionnairePresentlyWithActionColumn.QuestionnairePresentlyWithActionId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<QuestionnairePresentlyWithAction>(
					key.ToString(), // EntityTrackingKey
					"QuestionnairePresentlyWithAction",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.QuestionnairePresentlyWithAction();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.QuestionnairePresentlyWithActionId = (System.Int32)reader[((int)QuestionnairePresentlyWithActionColumn.QuestionnairePresentlyWithActionId - 1)];
					c.ActionName = (System.String)reader[((int)QuestionnairePresentlyWithActionColumn.ActionName - 1)];
					c.ActionDescription = (reader.IsDBNull(((int)QuestionnairePresentlyWithActionColumn.ActionDescription - 1)))?null:(System.String)reader[((int)QuestionnairePresentlyWithActionColumn.ActionDescription - 1)];
					c.QuestionnairePresentlyWithUserId = (System.Int32)reader[((int)QuestionnairePresentlyWithActionColumn.QuestionnairePresentlyWithUserId - 1)];
					c.ProcessNo = (reader.IsDBNull(((int)QuestionnairePresentlyWithActionColumn.ProcessNo - 1)))?null:(System.Int32?)reader[((int)QuestionnairePresentlyWithActionColumn.ProcessNo - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.QuestionnairePresentlyWithAction"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.QuestionnairePresentlyWithAction"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.QuestionnairePresentlyWithAction entity)
		{
			if (!reader.Read()) return;
			
			entity.QuestionnairePresentlyWithActionId = (System.Int32)reader[((int)QuestionnairePresentlyWithActionColumn.QuestionnairePresentlyWithActionId - 1)];
			entity.ActionName = (System.String)reader[((int)QuestionnairePresentlyWithActionColumn.ActionName - 1)];
			entity.ActionDescription = (reader.IsDBNull(((int)QuestionnairePresentlyWithActionColumn.ActionDescription - 1)))?null:(System.String)reader[((int)QuestionnairePresentlyWithActionColumn.ActionDescription - 1)];
			entity.QuestionnairePresentlyWithUserId = (System.Int32)reader[((int)QuestionnairePresentlyWithActionColumn.QuestionnairePresentlyWithUserId - 1)];
			entity.ProcessNo = (reader.IsDBNull(((int)QuestionnairePresentlyWithActionColumn.ProcessNo - 1)))?null:(System.Int32?)reader[((int)QuestionnairePresentlyWithActionColumn.ProcessNo - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.QuestionnairePresentlyWithAction"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.QuestionnairePresentlyWithAction"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.QuestionnairePresentlyWithAction entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.QuestionnairePresentlyWithActionId = (System.Int32)dataRow["QuestionnairePresentlyWithActionId"];
			entity.ActionName = (System.String)dataRow["ActionName"];
			entity.ActionDescription = Convert.IsDBNull(dataRow["ActionDescription"]) ? null : (System.String)dataRow["ActionDescription"];
			entity.QuestionnairePresentlyWithUserId = (System.Int32)dataRow["QuestionnairePresentlyWithUserId"];
			entity.ProcessNo = Convert.IsDBNull(dataRow["ProcessNo"]) ? null : (System.Int32?)dataRow["ProcessNo"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.QuestionnairePresentlyWithAction"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.QuestionnairePresentlyWithAction Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.QuestionnairePresentlyWithAction entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetByQuestionnairePresentlyWithActionId methods when available
			
			#region QuestionnairePresentlyWithMetricCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<QuestionnairePresentlyWithMetric>|QuestionnairePresentlyWithMetricCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'QuestionnairePresentlyWithMetricCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.QuestionnairePresentlyWithMetricCollection = DataRepository.QuestionnairePresentlyWithMetricProvider.GetByQuestionnairePresentlyWithActionId(transactionManager, entity.QuestionnairePresentlyWithActionId);

				if (deep && entity.QuestionnairePresentlyWithMetricCollection.Count > 0)
				{
					deepHandles.Add("QuestionnairePresentlyWithMetricCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<QuestionnairePresentlyWithMetric>) DataRepository.QuestionnairePresentlyWithMetricProvider.DeepLoad,
						new object[] { transactionManager, entity.QuestionnairePresentlyWithMetricCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.QuestionnairePresentlyWithAction object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.QuestionnairePresentlyWithAction instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.QuestionnairePresentlyWithAction Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.QuestionnairePresentlyWithAction entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<QuestionnairePresentlyWithMetric>
				if (CanDeepSave(entity.QuestionnairePresentlyWithMetricCollection, "List<QuestionnairePresentlyWithMetric>|QuestionnairePresentlyWithMetricCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(QuestionnairePresentlyWithMetric child in entity.QuestionnairePresentlyWithMetricCollection)
					{
						if(child.QuestionnairePresentlyWithActionIdSource != null)
						{
							child.QuestionnairePresentlyWithActionId = child.QuestionnairePresentlyWithActionIdSource.QuestionnairePresentlyWithActionId;
						}
						else
						{
							child.QuestionnairePresentlyWithActionId = entity.QuestionnairePresentlyWithActionId;
						}

					}

					if (entity.QuestionnairePresentlyWithMetricCollection.Count > 0 || entity.QuestionnairePresentlyWithMetricCollection.DeletedItems.Count > 0)
					{
						//DataRepository.QuestionnairePresentlyWithMetricProvider.Save(transactionManager, entity.QuestionnairePresentlyWithMetricCollection);
						
						deepHandles.Add("QuestionnairePresentlyWithMetricCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< QuestionnairePresentlyWithMetric >) DataRepository.QuestionnairePresentlyWithMetricProvider.DeepSave,
							new object[] { transactionManager, entity.QuestionnairePresentlyWithMetricCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region QuestionnairePresentlyWithActionChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.QuestionnairePresentlyWithAction</c>
	///</summary>
	public enum QuestionnairePresentlyWithActionChildEntityTypes
	{

		///<summary>
		/// Collection of <c>QuestionnairePresentlyWithAction</c> as OneToMany for QuestionnairePresentlyWithMetricCollection
		///</summary>
		[ChildEntityType(typeof(TList<QuestionnairePresentlyWithMetric>))]
		QuestionnairePresentlyWithMetricCollection,
	}
	
	#endregion QuestionnairePresentlyWithActionChildEntityTypes
	
	#region QuestionnairePresentlyWithActionFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;QuestionnairePresentlyWithActionColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnairePresentlyWithAction"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnairePresentlyWithActionFilterBuilder : SqlFilterBuilder<QuestionnairePresentlyWithActionColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnairePresentlyWithActionFilterBuilder class.
		/// </summary>
		public QuestionnairePresentlyWithActionFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnairePresentlyWithActionFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnairePresentlyWithActionFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnairePresentlyWithActionFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnairePresentlyWithActionFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnairePresentlyWithActionFilterBuilder
	
	#region QuestionnairePresentlyWithActionParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;QuestionnairePresentlyWithActionColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnairePresentlyWithAction"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnairePresentlyWithActionParameterBuilder : ParameterizedSqlFilterBuilder<QuestionnairePresentlyWithActionColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnairePresentlyWithActionParameterBuilder class.
		/// </summary>
		public QuestionnairePresentlyWithActionParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnairePresentlyWithActionParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnairePresentlyWithActionParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnairePresentlyWithActionParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnairePresentlyWithActionParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnairePresentlyWithActionParameterBuilder
	
	#region QuestionnairePresentlyWithActionSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;QuestionnairePresentlyWithActionColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnairePresentlyWithAction"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class QuestionnairePresentlyWithActionSortBuilder : SqlSortBuilder<QuestionnairePresentlyWithActionColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnairePresentlyWithActionSqlSortBuilder class.
		/// </summary>
		public QuestionnairePresentlyWithActionSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion QuestionnairePresentlyWithActionSortBuilder
	
} // end namespace
