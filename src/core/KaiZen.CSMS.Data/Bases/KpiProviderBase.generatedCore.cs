﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="KpiProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class KpiProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.Kpi, KaiZen.CSMS.Entities.KpiKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.KpiKey key)
		{
			return Delete(transactionManager, key.KpiId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_kpiId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _kpiId)
		{
			return Delete(null, _kpiId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_kpiId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _kpiId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Kpi_CompanyId key.
		///		FK_Kpi_CompanyId Description: 
		/// </summary>
		/// <param name="_companyId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Kpi objects.</returns>
		public TList<Kpi> GetByCompanyId(System.Int32 _companyId)
		{
			int count = -1;
			return GetByCompanyId(_companyId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Kpi_CompanyId key.
		///		FK_Kpi_CompanyId Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Kpi objects.</returns>
		/// <remarks></remarks>
		public TList<Kpi> GetByCompanyId(TransactionManager transactionManager, System.Int32 _companyId)
		{
			int count = -1;
			return GetByCompanyId(transactionManager, _companyId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_Kpi_CompanyId key.
		///		FK_Kpi_CompanyId Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Kpi objects.</returns>
		public TList<Kpi> GetByCompanyId(TransactionManager transactionManager, System.Int32 _companyId, int start, int pageLength)
		{
			int count = -1;
			return GetByCompanyId(transactionManager, _companyId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Kpi_CompanyId key.
		///		fkKpiCompanyId Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_companyId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Kpi objects.</returns>
		public TList<Kpi> GetByCompanyId(System.Int32 _companyId, int start, int pageLength)
		{
			int count =  -1;
			return GetByCompanyId(null, _companyId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Kpi_CompanyId key.
		///		fkKpiCompanyId Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_companyId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Kpi objects.</returns>
		public TList<Kpi> GetByCompanyId(System.Int32 _companyId, int start, int pageLength,out int count)
		{
			return GetByCompanyId(null, _companyId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Kpi_CompanyId key.
		///		FK_Kpi_CompanyId Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Kpi objects.</returns>
		public abstract TList<Kpi> GetByCompanyId(TransactionManager transactionManager, System.Int32 _companyId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Kpi_UserId key.
		///		FK_Kpi_UserId Description: 
		/// </summary>
		/// <param name="_modifiedbyUserId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Kpi objects.</returns>
		public TList<Kpi> GetByModifiedbyUserId(System.Int32 _modifiedbyUserId)
		{
			int count = -1;
			return GetByModifiedbyUserId(_modifiedbyUserId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Kpi_UserId key.
		///		FK_Kpi_UserId Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_modifiedbyUserId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Kpi objects.</returns>
		/// <remarks></remarks>
		public TList<Kpi> GetByModifiedbyUserId(TransactionManager transactionManager, System.Int32 _modifiedbyUserId)
		{
			int count = -1;
			return GetByModifiedbyUserId(transactionManager, _modifiedbyUserId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_Kpi_UserId key.
		///		FK_Kpi_UserId Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_modifiedbyUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Kpi objects.</returns>
		public TList<Kpi> GetByModifiedbyUserId(TransactionManager transactionManager, System.Int32 _modifiedbyUserId, int start, int pageLength)
		{
			int count = -1;
			return GetByModifiedbyUserId(transactionManager, _modifiedbyUserId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Kpi_UserId key.
		///		fkKpiUserId Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_modifiedbyUserId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Kpi objects.</returns>
		public TList<Kpi> GetByModifiedbyUserId(System.Int32 _modifiedbyUserId, int start, int pageLength)
		{
			int count =  -1;
			return GetByModifiedbyUserId(null, _modifiedbyUserId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Kpi_UserId key.
		///		fkKpiUserId Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_modifiedbyUserId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Kpi objects.</returns>
		public TList<Kpi> GetByModifiedbyUserId(System.Int32 _modifiedbyUserId, int start, int pageLength,out int count)
		{
			return GetByModifiedbyUserId(null, _modifiedbyUserId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Kpi_UserId key.
		///		FK_Kpi_UserId Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_modifiedbyUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Kpi objects.</returns>
		public abstract TList<Kpi> GetByModifiedbyUserId(TransactionManager transactionManager, System.Int32 _modifiedbyUserId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Kpi_Sites key.
		///		FK_Kpi_Sites Description: 
		/// </summary>
		/// <param name="_siteId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Kpi objects.</returns>
		public TList<Kpi> GetBySiteId(System.Int32 _siteId)
		{
			int count = -1;
			return GetBySiteId(_siteId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Kpi_Sites key.
		///		FK_Kpi_Sites Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_siteId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Kpi objects.</returns>
		/// <remarks></remarks>
		public TList<Kpi> GetBySiteId(TransactionManager transactionManager, System.Int32 _siteId)
		{
			int count = -1;
			return GetBySiteId(transactionManager, _siteId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_Kpi_Sites key.
		///		FK_Kpi_Sites Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_siteId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Kpi objects.</returns>
		public TList<Kpi> GetBySiteId(TransactionManager transactionManager, System.Int32 _siteId, int start, int pageLength)
		{
			int count = -1;
			return GetBySiteId(transactionManager, _siteId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Kpi_Sites key.
		///		fkKpiSites Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_siteId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Kpi objects.</returns>
		public TList<Kpi> GetBySiteId(System.Int32 _siteId, int start, int pageLength)
		{
			int count =  -1;
			return GetBySiteId(null, _siteId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Kpi_Sites key.
		///		fkKpiSites Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_siteId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Kpi objects.</returns>
		public TList<Kpi> GetBySiteId(System.Int32 _siteId, int start, int pageLength,out int count)
		{
			return GetBySiteId(null, _siteId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Kpi_Sites key.
		///		FK_Kpi_Sites Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_siteId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Kpi objects.</returns>
		public abstract TList<Kpi> GetBySiteId(TransactionManager transactionManager, System.Int32 _siteId, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.Kpi Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.KpiKey key, int start, int pageLength)
		{
			return GetByKpiId(transactionManager, key.KpiId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_KPI index.
		/// </summary>
		/// <param name="_kpiId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Kpi"/> class.</returns>
		public KaiZen.CSMS.Entities.Kpi GetByKpiId(System.Int32 _kpiId)
		{
			int count = -1;
			return GetByKpiId(null,_kpiId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_KPI index.
		/// </summary>
		/// <param name="_kpiId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Kpi"/> class.</returns>
		public KaiZen.CSMS.Entities.Kpi GetByKpiId(System.Int32 _kpiId, int start, int pageLength)
		{
			int count = -1;
			return GetByKpiId(null, _kpiId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_KPI index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_kpiId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Kpi"/> class.</returns>
		public KaiZen.CSMS.Entities.Kpi GetByKpiId(TransactionManager transactionManager, System.Int32 _kpiId)
		{
			int count = -1;
			return GetByKpiId(transactionManager, _kpiId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_KPI index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_kpiId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Kpi"/> class.</returns>
		public KaiZen.CSMS.Entities.Kpi GetByKpiId(TransactionManager transactionManager, System.Int32 _kpiId, int start, int pageLength)
		{
			int count = -1;
			return GetByKpiId(transactionManager, _kpiId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_KPI index.
		/// </summary>
		/// <param name="_kpiId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Kpi"/> class.</returns>
		public KaiZen.CSMS.Entities.Kpi GetByKpiId(System.Int32 _kpiId, int start, int pageLength, out int count)
		{
			return GetByKpiId(null, _kpiId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_KPI index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_kpiId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Kpi"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.Kpi GetByKpiId(TransactionManager transactionManager, System.Int32 _kpiId, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_KPI index.
		/// </summary>
		/// <param name="_companyId"></param>
		/// <param name="_siteId"></param>
		/// <param name="_kpiDateTime"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Kpi"/> class.</returns>
		public KaiZen.CSMS.Entities.Kpi GetByCompanyIdSiteIdKpiDateTime(System.Int32 _companyId, System.Int32 _siteId, System.DateTime _kpiDateTime)
		{
			int count = -1;
			return GetByCompanyIdSiteIdKpiDateTime(null,_companyId, _siteId, _kpiDateTime, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_KPI index.
		/// </summary>
		/// <param name="_companyId"></param>
		/// <param name="_siteId"></param>
		/// <param name="_kpiDateTime"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Kpi"/> class.</returns>
		public KaiZen.CSMS.Entities.Kpi GetByCompanyIdSiteIdKpiDateTime(System.Int32 _companyId, System.Int32 _siteId, System.DateTime _kpiDateTime, int start, int pageLength)
		{
			int count = -1;
			return GetByCompanyIdSiteIdKpiDateTime(null, _companyId, _siteId, _kpiDateTime, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_KPI index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyId"></param>
		/// <param name="_siteId"></param>
		/// <param name="_kpiDateTime"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Kpi"/> class.</returns>
		public KaiZen.CSMS.Entities.Kpi GetByCompanyIdSiteIdKpiDateTime(TransactionManager transactionManager, System.Int32 _companyId, System.Int32 _siteId, System.DateTime _kpiDateTime)
		{
			int count = -1;
			return GetByCompanyIdSiteIdKpiDateTime(transactionManager, _companyId, _siteId, _kpiDateTime, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_KPI index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyId"></param>
		/// <param name="_siteId"></param>
		/// <param name="_kpiDateTime"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Kpi"/> class.</returns>
		public KaiZen.CSMS.Entities.Kpi GetByCompanyIdSiteIdKpiDateTime(TransactionManager transactionManager, System.Int32 _companyId, System.Int32 _siteId, System.DateTime _kpiDateTime, int start, int pageLength)
		{
			int count = -1;
			return GetByCompanyIdSiteIdKpiDateTime(transactionManager, _companyId, _siteId, _kpiDateTime, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_KPI index.
		/// </summary>
		/// <param name="_companyId"></param>
		/// <param name="_siteId"></param>
		/// <param name="_kpiDateTime"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Kpi"/> class.</returns>
		public KaiZen.CSMS.Entities.Kpi GetByCompanyIdSiteIdKpiDateTime(System.Int32 _companyId, System.Int32 _siteId, System.DateTime _kpiDateTime, int start, int pageLength, out int count)
		{
			return GetByCompanyIdSiteIdKpiDateTime(null, _companyId, _siteId, _kpiDateTime, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_KPI index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyId"></param>
		/// <param name="_siteId"></param>
		/// <param name="_kpiDateTime"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Kpi"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.Kpi GetByCompanyIdSiteIdKpiDateTime(TransactionManager transactionManager, System.Int32 _companyId, System.Int32 _siteId, System.DateTime _kpiDateTime, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_Kpi_CompanyIdSiteId index.
		/// </summary>
		/// <param name="_companyId"></param>
		/// <param name="_siteId"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Kpi&gt;"/> class.</returns>
		public TList<Kpi> GetByCompanyIdSiteId(System.Int32 _companyId, System.Int32 _siteId)
		{
			int count = -1;
			return GetByCompanyIdSiteId(null,_companyId, _siteId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Kpi_CompanyIdSiteId index.
		/// </summary>
		/// <param name="_companyId"></param>
		/// <param name="_siteId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Kpi&gt;"/> class.</returns>
		public TList<Kpi> GetByCompanyIdSiteId(System.Int32 _companyId, System.Int32 _siteId, int start, int pageLength)
		{
			int count = -1;
			return GetByCompanyIdSiteId(null, _companyId, _siteId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Kpi_CompanyIdSiteId index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyId"></param>
		/// <param name="_siteId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Kpi&gt;"/> class.</returns>
		public TList<Kpi> GetByCompanyIdSiteId(TransactionManager transactionManager, System.Int32 _companyId, System.Int32 _siteId)
		{
			int count = -1;
			return GetByCompanyIdSiteId(transactionManager, _companyId, _siteId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Kpi_CompanyIdSiteId index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyId"></param>
		/// <param name="_siteId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Kpi&gt;"/> class.</returns>
		public TList<Kpi> GetByCompanyIdSiteId(TransactionManager transactionManager, System.Int32 _companyId, System.Int32 _siteId, int start, int pageLength)
		{
			int count = -1;
			return GetByCompanyIdSiteId(transactionManager, _companyId, _siteId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Kpi_CompanyIdSiteId index.
		/// </summary>
		/// <param name="_companyId"></param>
		/// <param name="_siteId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Kpi&gt;"/> class.</returns>
		public TList<Kpi> GetByCompanyIdSiteId(System.Int32 _companyId, System.Int32 _siteId, int start, int pageLength, out int count)
		{
			return GetByCompanyIdSiteId(null, _companyId, _siteId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Kpi_CompanyIdSiteId index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyId"></param>
		/// <param name="_siteId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Kpi&gt;"/> class.</returns>
		public abstract TList<Kpi> GetByCompanyIdSiteId(TransactionManager transactionManager, System.Int32 _companyId, System.Int32 _siteId, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#region _Kpi_GetComparisonReportDataGroupedByMonthCompanySiteFilterMonthYear 
		
		/// <summary>
		///	This method wrap the '_Kpi_GetComparisonReportDataGroupedByMonthCompanySiteFilterMonthYear' stored procedure. 
		/// </summary>
		/// <param name="month"> A <c>System.Int32?</c> instance.</param>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetComparisonReportDataGroupedByMonthCompanySiteFilterMonthYear(System.Int32? month, System.Int32? year)
		{
			return GetComparisonReportDataGroupedByMonthCompanySiteFilterMonthYear(null, 0, int.MaxValue , month, year);
		}
		
		/// <summary>
		///	This method wrap the '_Kpi_GetComparisonReportDataGroupedByMonthCompanySiteFilterMonthYear' stored procedure. 
		/// </summary>
		/// <param name="month"> A <c>System.Int32?</c> instance.</param>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetComparisonReportDataGroupedByMonthCompanySiteFilterMonthYear(int start, int pageLength, System.Int32? month, System.Int32? year)
		{
			return GetComparisonReportDataGroupedByMonthCompanySiteFilterMonthYear(null, start, pageLength , month, year);
		}
				
		/// <summary>
		///	This method wrap the '_Kpi_GetComparisonReportDataGroupedByMonthCompanySiteFilterMonthYear' stored procedure. 
		/// </summary>
		/// <param name="month"> A <c>System.Int32?</c> instance.</param>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetComparisonReportDataGroupedByMonthCompanySiteFilterMonthYear(TransactionManager transactionManager, System.Int32? month, System.Int32? year)
		{
			return GetComparisonReportDataGroupedByMonthCompanySiteFilterMonthYear(transactionManager, 0, int.MaxValue , month, year);
		}
		
		/// <summary>
		///	This method wrap the '_Kpi_GetComparisonReportDataGroupedByMonthCompanySiteFilterMonthYear' stored procedure. 
		/// </summary>
		/// <param name="month"> A <c>System.Int32?</c> instance.</param>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet GetComparisonReportDataGroupedByMonthCompanySiteFilterMonthYear(TransactionManager transactionManager, int start, int pageLength , System.Int32? month, System.Int32? year);
		
		#endregion
		
		#region _Kpi_GetComparisonReportDataGroupedByCompanySite 
		
		/// <summary>
		///	This method wrap the '_Kpi_GetComparisonReportDataGroupedByCompanySite' stored procedure. 
		/// </summary>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetComparisonReportDataGroupedByCompanySite()
		{
			return GetComparisonReportDataGroupedByCompanySite(null, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_Kpi_GetComparisonReportDataGroupedByCompanySite' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetComparisonReportDataGroupedByCompanySite(int start, int pageLength)
		{
			return GetComparisonReportDataGroupedByCompanySite(null, start, pageLength );
		}
				
		/// <summary>
		///	This method wrap the '_Kpi_GetComparisonReportDataGroupedByCompanySite' stored procedure. 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetComparisonReportDataGroupedByCompanySite(TransactionManager transactionManager)
		{
			return GetComparisonReportDataGroupedByCompanySite(transactionManager, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_Kpi_GetComparisonReportDataGroupedByCompanySite' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet GetComparisonReportDataGroupedByCompanySite(TransactionManager transactionManager, int start, int pageLength );
		
		#endregion
		
		#region _Kpi_Reports_Statistical_Contractor 
		
		/// <summary>
		///	This method wrap the '_Kpi_Reports_Statistical_Contractor' stored procedure. 
		/// </summary>
		/// <param name="fromDate"> A <c>System.String</c> instance.</param>
		/// <param name="toDate"> A <c>System.String</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Reports_Statistical_Contractor(System.String fromDate, System.String toDate)
		{
			return Reports_Statistical_Contractor(null, 0, int.MaxValue , fromDate, toDate);
		}
		
		/// <summary>
		///	This method wrap the '_Kpi_Reports_Statistical_Contractor' stored procedure. 
		/// </summary>
		/// <param name="fromDate"> A <c>System.String</c> instance.</param>
		/// <param name="toDate"> A <c>System.String</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Reports_Statistical_Contractor(int start, int pageLength, System.String fromDate, System.String toDate)
		{
			return Reports_Statistical_Contractor(null, start, pageLength , fromDate, toDate);
		}
				
		/// <summary>
		///	This method wrap the '_Kpi_Reports_Statistical_Contractor' stored procedure. 
		/// </summary>
		/// <param name="fromDate"> A <c>System.String</c> instance.</param>
		/// <param name="toDate"> A <c>System.String</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Reports_Statistical_Contractor(TransactionManager transactionManager, System.String fromDate, System.String toDate)
		{
			return Reports_Statistical_Contractor(transactionManager, 0, int.MaxValue , fromDate, toDate);
		}
		
		/// <summary>
		///	This method wrap the '_Kpi_Reports_Statistical_Contractor' stored procedure. 
		/// </summary>
		/// <param name="fromDate"> A <c>System.String</c> instance.</param>
		/// <param name="toDate"> A <c>System.String</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet Reports_Statistical_Contractor(TransactionManager transactionManager, int start, int pageLength , System.String fromDate, System.String toDate);
		
		#endregion
		
		#region _Kpi_GetAll_Brief 
		
		/// <summary>
		///	This method wrap the '_Kpi_GetAll_Brief' stored procedure. 
		/// </summary>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetAll_Brief()
		{
			return GetAll_Brief(null, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_Kpi_GetAll_Brief' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetAll_Brief(int start, int pageLength)
		{
			return GetAll_Brief(null, start, pageLength );
		}
				
		/// <summary>
		///	This method wrap the '_Kpi_GetAll_Brief' stored procedure. 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetAll_Brief(TransactionManager transactionManager)
		{
			return GetAll_Brief(transactionManager, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_Kpi_GetAll_Brief' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet GetAll_Brief(TransactionManager transactionManager, int start, int pageLength );
		
		#endregion
		
		#region _Kpi_Reports_LeadingI_AllCompanies_AllSites 
		
		/// <summary>
		///	This method wrap the '_Kpi_Reports_LeadingI_AllCompanies_AllSites' stored procedure. 
		/// </summary>
		/// <param name="fromDate"> A <c>System.String</c> instance.</param>
		/// <param name="toDate"> A <c>System.String</c> instance.</param>
		/// <param name="currentMonth"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Reports_LeadingI_AllCompanies_AllSites(System.String fromDate, System.String toDate, System.Int32? currentMonth)
		{
			return Reports_LeadingI_AllCompanies_AllSites(null, 0, int.MaxValue , fromDate, toDate, currentMonth);
		}
		
		/// <summary>
		///	This method wrap the '_Kpi_Reports_LeadingI_AllCompanies_AllSites' stored procedure. 
		/// </summary>
		/// <param name="fromDate"> A <c>System.String</c> instance.</param>
		/// <param name="toDate"> A <c>System.String</c> instance.</param>
		/// <param name="currentMonth"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Reports_LeadingI_AllCompanies_AllSites(int start, int pageLength, System.String fromDate, System.String toDate, System.Int32? currentMonth)
		{
			return Reports_LeadingI_AllCompanies_AllSites(null, start, pageLength , fromDate, toDate, currentMonth);
		}
				
		/// <summary>
		///	This method wrap the '_Kpi_Reports_LeadingI_AllCompanies_AllSites' stored procedure. 
		/// </summary>
		/// <param name="fromDate"> A <c>System.String</c> instance.</param>
		/// <param name="toDate"> A <c>System.String</c> instance.</param>
		/// <param name="currentMonth"> A <c>System.Int32?</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Reports_LeadingI_AllCompanies_AllSites(TransactionManager transactionManager, System.String fromDate, System.String toDate, System.Int32? currentMonth)
		{
			return Reports_LeadingI_AllCompanies_AllSites(transactionManager, 0, int.MaxValue , fromDate, toDate, currentMonth);
		}
		
		/// <summary>
		///	This method wrap the '_Kpi_Reports_LeadingI_AllCompanies_AllSites' stored procedure. 
		/// </summary>
		/// <param name="fromDate"> A <c>System.String</c> instance.</param>
		/// <param name="toDate"> A <c>System.String</c> instance.</param>
		/// <param name="currentMonth"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet Reports_LeadingI_AllCompanies_AllSites(TransactionManager transactionManager, int start, int pageLength , System.String fromDate, System.String toDate, System.Int32? currentMonth);
		
		#endregion
		
		#region _Kpi_GetComparisonReportDataGroupedByMonthCompanySite 
		
		/// <summary>
		///	This method wrap the '_Kpi_GetComparisonReportDataGroupedByMonthCompanySite' stored procedure. 
		/// </summary>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetComparisonReportDataGroupedByMonthCompanySite()
		{
			return GetComparisonReportDataGroupedByMonthCompanySite(null, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_Kpi_GetComparisonReportDataGroupedByMonthCompanySite' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetComparisonReportDataGroupedByMonthCompanySite(int start, int pageLength)
		{
			return GetComparisonReportDataGroupedByMonthCompanySite(null, start, pageLength );
		}
				
		/// <summary>
		///	This method wrap the '_Kpi_GetComparisonReportDataGroupedByMonthCompanySite' stored procedure. 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetComparisonReportDataGroupedByMonthCompanySite(TransactionManager transactionManager)
		{
			return GetComparisonReportDataGroupedByMonthCompanySite(transactionManager, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_Kpi_GetComparisonReportDataGroupedByMonthCompanySite' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet GetComparisonReportDataGroupedByMonthCompanySite(TransactionManager transactionManager, int start, int pageLength );
		
		#endregion
		
		#region _Kpi_GetReport_Compliance 
		
		/// <summary>
		///	This method wrap the '_Kpi_GetReport_Compliance' stored procedure. 
		/// </summary>
		/// <param name="sYear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="regionId"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetReport_Compliance(System.Int32? sYear, System.Int32? regionId)
		{
			return GetReport_Compliance(null, 0, int.MaxValue , sYear, regionId);
		}
		
		/// <summary>
		///	This method wrap the '_Kpi_GetReport_Compliance' stored procedure. 
		/// </summary>
		/// <param name="sYear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="regionId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetReport_Compliance(int start, int pageLength, System.Int32? sYear, System.Int32? regionId)
		{
			return GetReport_Compliance(null, start, pageLength , sYear, regionId);
		}
				
		/// <summary>
		///	This method wrap the '_Kpi_GetReport_Compliance' stored procedure. 
		/// </summary>
		/// <param name="sYear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="regionId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetReport_Compliance(TransactionManager transactionManager, System.Int32? sYear, System.Int32? regionId)
		{
			return GetReport_Compliance(transactionManager, 0, int.MaxValue , sYear, regionId);
		}
		
		/// <summary>
		///	This method wrap the '_Kpi_GetReport_Compliance' stored procedure. 
		/// </summary>
		/// <param name="sYear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="regionId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet GetReport_Compliance(TransactionManager transactionManager, int start, int pageLength , System.Int32? sYear, System.Int32? regionId);
		
		#endregion
		
		#region _Kpi_Reports_LeadingI_ 
		
		/// <summary>
		///	This method wrap the '_Kpi_Reports_LeadingI_' stored procedure. 
		/// </summary>
		/// <param name="fromDate"> A <c>System.String</c> instance.</param>
		/// <param name="toDate"> A <c>System.String</c> instance.</param>
		/// <param name="currentMonth"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Reports_LeadingI_(System.String fromDate, System.String toDate, System.Int32? currentMonth, System.Int32? companyId)
		{
			return Reports_LeadingI_(null, 0, int.MaxValue , fromDate, toDate, currentMonth, companyId);
		}
		
		/// <summary>
		///	This method wrap the '_Kpi_Reports_LeadingI_' stored procedure. 
		/// </summary>
		/// <param name="fromDate"> A <c>System.String</c> instance.</param>
		/// <param name="toDate"> A <c>System.String</c> instance.</param>
		/// <param name="currentMonth"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Reports_LeadingI_(int start, int pageLength, System.String fromDate, System.String toDate, System.Int32? currentMonth, System.Int32? companyId)
		{
			return Reports_LeadingI_(null, start, pageLength , fromDate, toDate, currentMonth, companyId);
		}
				
		/// <summary>
		///	This method wrap the '_Kpi_Reports_LeadingI_' stored procedure. 
		/// </summary>
		/// <param name="fromDate"> A <c>System.String</c> instance.</param>
		/// <param name="toDate"> A <c>System.String</c> instance.</param>
		/// <param name="currentMonth"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Reports_LeadingI_(TransactionManager transactionManager, System.String fromDate, System.String toDate, System.Int32? currentMonth, System.Int32? companyId)
		{
			return Reports_LeadingI_(transactionManager, 0, int.MaxValue , fromDate, toDate, currentMonth, companyId);
		}
		
		/// <summary>
		///	This method wrap the '_Kpi_Reports_LeadingI_' stored procedure. 
		/// </summary>
		/// <param name="fromDate"> A <c>System.String</c> instance.</param>
		/// <param name="toDate"> A <c>System.String</c> instance.</param>
		/// <param name="currentMonth"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet Reports_LeadingI_(TransactionManager transactionManager, int start, int pageLength , System.String fromDate, System.String toDate, System.Int32? currentMonth, System.Int32? companyId);
		
		#endregion
		
		#region _Kpi_Report 
		
		/// <summary>
		///	This method wrap the '_Kpi_Report' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="kpiDateTimeFrom"> A <c>System.DateTime?</c> instance.</param>
		/// <param name="kpiDateTimeTo"> A <c>System.DateTime?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Report(System.Int32? companyId, System.DateTime? kpiDateTimeFrom, System.DateTime? kpiDateTimeTo)
		{
			return Report(null, 0, int.MaxValue , companyId, kpiDateTimeFrom, kpiDateTimeTo);
		}
		
		/// <summary>
		///	This method wrap the '_Kpi_Report' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="kpiDateTimeFrom"> A <c>System.DateTime?</c> instance.</param>
		/// <param name="kpiDateTimeTo"> A <c>System.DateTime?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Report(int start, int pageLength, System.Int32? companyId, System.DateTime? kpiDateTimeFrom, System.DateTime? kpiDateTimeTo)
		{
			return Report(null, start, pageLength , companyId, kpiDateTimeFrom, kpiDateTimeTo);
		}
				
		/// <summary>
		///	This method wrap the '_Kpi_Report' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="kpiDateTimeFrom"> A <c>System.DateTime?</c> instance.</param>
		/// <param name="kpiDateTimeTo"> A <c>System.DateTime?</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Report(TransactionManager transactionManager, System.Int32? companyId, System.DateTime? kpiDateTimeFrom, System.DateTime? kpiDateTimeTo)
		{
			return Report(transactionManager, 0, int.MaxValue , companyId, kpiDateTimeFrom, kpiDateTimeTo);
		}
		
		/// <summary>
		///	This method wrap the '_Kpi_Report' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="kpiDateTimeFrom"> A <c>System.DateTime?</c> instance.</param>
		/// <param name="kpiDateTimeTo"> A <c>System.DateTime?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet Report(TransactionManager transactionManager, int start, int pageLength , System.Int32? companyId, System.DateTime? kpiDateTimeFrom, System.DateTime? kpiDateTimeTo);
		
		#endregion
		
		#region _Kpi_GetCountIproc 
		
		/// <summary>
		///	This method wrap the '_Kpi_GetCountIproc' stored procedure. 
		/// </summary>
		/// <param name="companyName"> A <c>System.String</c> instance.</param>
		/// <param name="siteId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="month"> A <c>System.Int32?</c> instance.</param>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetCountIproc(System.String companyName, System.Int32? siteId, System.Int32? month, System.Int32? year)
		{
			return GetCountIproc(null, 0, int.MaxValue , companyName, siteId, month, year);
		}
		
		/// <summary>
		///	This method wrap the '_Kpi_GetCountIproc' stored procedure. 
		/// </summary>
		/// <param name="companyName"> A <c>System.String</c> instance.</param>
		/// <param name="siteId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="month"> A <c>System.Int32?</c> instance.</param>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetCountIproc(int start, int pageLength, System.String companyName, System.Int32? siteId, System.Int32? month, System.Int32? year)
		{
			return GetCountIproc(null, start, pageLength , companyName, siteId, month, year);
		}
				
		/// <summary>
		///	This method wrap the '_Kpi_GetCountIproc' stored procedure. 
		/// </summary>
		/// <param name="companyName"> A <c>System.String</c> instance.</param>
		/// <param name="siteId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="month"> A <c>System.Int32?</c> instance.</param>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetCountIproc(TransactionManager transactionManager, System.String companyName, System.Int32? siteId, System.Int32? month, System.Int32? year)
		{
			return GetCountIproc(transactionManager, 0, int.MaxValue , companyName, siteId, month, year);
		}
		
		/// <summary>
		///	This method wrap the '_Kpi_GetCountIproc' stored procedure. 
		/// </summary>
		/// <param name="companyName"> A <c>System.String</c> instance.</param>
		/// <param name="siteId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="month"> A <c>System.Int32?</c> instance.</param>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet GetCountIproc(TransactionManager transactionManager, int start, int pageLength , System.String companyName, System.Int32? siteId, System.Int32? month, System.Int32? year);
		
		#endregion
		
		#region _Kpi_Reports_LeadingI_AllResidentialCompanies_AllMines 
		
		/// <summary>
		///	This method wrap the '_Kpi_Reports_LeadingI_AllResidentialCompanies_AllMines' stored procedure. 
		/// </summary>
		/// <param name="fromDate"> A <c>System.String</c> instance.</param>
		/// <param name="toDate"> A <c>System.String</c> instance.</param>
		/// <param name="currentMonth"> A <c>System.Int32?</c> instance.</param>
		/// <param name="currentYear"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Reports_LeadingI_AllResidentialCompanies_AllMines(System.String fromDate, System.String toDate, System.Int32? currentMonth, System.Int32? currentYear)
		{
			return Reports_LeadingI_AllResidentialCompanies_AllMines(null, 0, int.MaxValue , fromDate, toDate, currentMonth, currentYear);
		}
		
		/// <summary>
		///	This method wrap the '_Kpi_Reports_LeadingI_AllResidentialCompanies_AllMines' stored procedure. 
		/// </summary>
		/// <param name="fromDate"> A <c>System.String</c> instance.</param>
		/// <param name="toDate"> A <c>System.String</c> instance.</param>
		/// <param name="currentMonth"> A <c>System.Int32?</c> instance.</param>
		/// <param name="currentYear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Reports_LeadingI_AllResidentialCompanies_AllMines(int start, int pageLength, System.String fromDate, System.String toDate, System.Int32? currentMonth, System.Int32? currentYear)
		{
			return Reports_LeadingI_AllResidentialCompanies_AllMines(null, start, pageLength , fromDate, toDate, currentMonth, currentYear);
		}
				
		/// <summary>
		///	This method wrap the '_Kpi_Reports_LeadingI_AllResidentialCompanies_AllMines' stored procedure. 
		/// </summary>
		/// <param name="fromDate"> A <c>System.String</c> instance.</param>
		/// <param name="toDate"> A <c>System.String</c> instance.</param>
		/// <param name="currentMonth"> A <c>System.Int32?</c> instance.</param>
		/// <param name="currentYear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Reports_LeadingI_AllResidentialCompanies_AllMines(TransactionManager transactionManager, System.String fromDate, System.String toDate, System.Int32? currentMonth, System.Int32? currentYear)
		{
			return Reports_LeadingI_AllResidentialCompanies_AllMines(transactionManager, 0, int.MaxValue , fromDate, toDate, currentMonth, currentYear);
		}
		
		/// <summary>
		///	This method wrap the '_Kpi_Reports_LeadingI_AllResidentialCompanies_AllMines' stored procedure. 
		/// </summary>
		/// <param name="fromDate"> A <c>System.String</c> instance.</param>
		/// <param name="toDate"> A <c>System.String</c> instance.</param>
		/// <param name="currentMonth"> A <c>System.Int32?</c> instance.</param>
		/// <param name="currentYear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet Reports_LeadingI_AllResidentialCompanies_AllMines(TransactionManager transactionManager, int start, int pageLength , System.String fromDate, System.String toDate, System.Int32? currentMonth, System.Int32? currentYear);
		
		#endregion
		
		#region _Kpi_HealthCheck_NoSubmitted 
		
		/// <summary>
		///	This method wrap the '_Kpi_HealthCheck_NoSubmitted' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="currentyear"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet HealthCheck_NoSubmitted(System.Int32? companyId, System.Int32? currentyear)
		{
			return HealthCheck_NoSubmitted(null, 0, int.MaxValue , companyId, currentyear);
		}
		
		/// <summary>
		///	This method wrap the '_Kpi_HealthCheck_NoSubmitted' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="currentyear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet HealthCheck_NoSubmitted(int start, int pageLength, System.Int32? companyId, System.Int32? currentyear)
		{
			return HealthCheck_NoSubmitted(null, start, pageLength , companyId, currentyear);
		}
				
		/// <summary>
		///	This method wrap the '_Kpi_HealthCheck_NoSubmitted' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="currentyear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet HealthCheck_NoSubmitted(TransactionManager transactionManager, System.Int32? companyId, System.Int32? currentyear)
		{
			return HealthCheck_NoSubmitted(transactionManager, 0, int.MaxValue , companyId, currentyear);
		}
		
		/// <summary>
		///	This method wrap the '_Kpi_HealthCheck_NoSubmitted' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="currentyear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet HealthCheck_NoSubmitted(TransactionManager transactionManager, int start, int pageLength , System.Int32? companyId, System.Int32? currentyear);
		
		#endregion
		
		#region _Kpi_Reports_Statistical_WAO 
		
		/// <summary>
		///	This method wrap the '_Kpi_Reports_Statistical_WAO' stored procedure. 
		/// </summary>
		/// <param name="fromDate"> A <c>System.String</c> instance.</param>
		/// <param name="toDate"> A <c>System.String</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Reports_Statistical_WAO(System.String fromDate, System.String toDate)
		{
			return Reports_Statistical_WAO(null, 0, int.MaxValue , fromDate, toDate);
		}
		
		/// <summary>
		///	This method wrap the '_Kpi_Reports_Statistical_WAO' stored procedure. 
		/// </summary>
		/// <param name="fromDate"> A <c>System.String</c> instance.</param>
		/// <param name="toDate"> A <c>System.String</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Reports_Statistical_WAO(int start, int pageLength, System.String fromDate, System.String toDate)
		{
			return Reports_Statistical_WAO(null, start, pageLength , fromDate, toDate);
		}
				
		/// <summary>
		///	This method wrap the '_Kpi_Reports_Statistical_WAO' stored procedure. 
		/// </summary>
		/// <param name="fromDate"> A <c>System.String</c> instance.</param>
		/// <param name="toDate"> A <c>System.String</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Reports_Statistical_WAO(TransactionManager transactionManager, System.String fromDate, System.String toDate)
		{
			return Reports_Statistical_WAO(transactionManager, 0, int.MaxValue , fromDate, toDate);
		}
		
		/// <summary>
		///	This method wrap the '_Kpi_Reports_Statistical_WAO' stored procedure. 
		/// </summary>
		/// <param name="fromDate"> A <c>System.String</c> instance.</param>
		/// <param name="toDate"> A <c>System.String</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet Reports_Statistical_WAO(TransactionManager transactionManager, int start, int pageLength , System.String fromDate, System.String toDate);
		
		#endregion
		
		#region _Kpi_ComplianceReport 
		
		/// <summary>
		///	This method wrap the '_Kpi_ComplianceReport' stored procedure. 
		/// </summary>
		/// <param name="sYear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="siteId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companySiteCategoryId"> A <c>System.String</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet ComplianceReport(System.Int32? sYear, System.Int32? siteId, System.String companySiteCategoryId)
		{
			return ComplianceReport(null, 0, int.MaxValue , sYear, siteId, companySiteCategoryId);
		}
		
		/// <summary>
		///	This method wrap the '_Kpi_ComplianceReport' stored procedure. 
		/// </summary>
		/// <param name="sYear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="siteId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companySiteCategoryId"> A <c>System.String</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet ComplianceReport(int start, int pageLength, System.Int32? sYear, System.Int32? siteId, System.String companySiteCategoryId)
		{
			return ComplianceReport(null, start, pageLength , sYear, siteId, companySiteCategoryId);
		}
				
		/// <summary>
		///	This method wrap the '_Kpi_ComplianceReport' stored procedure. 
		/// </summary>
		/// <param name="sYear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="siteId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companySiteCategoryId"> A <c>System.String</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet ComplianceReport(TransactionManager transactionManager, System.Int32? sYear, System.Int32? siteId, System.String companySiteCategoryId)
		{
			return ComplianceReport(transactionManager, 0, int.MaxValue , sYear, siteId, companySiteCategoryId);
		}
		
		/// <summary>
		///	This method wrap the '_Kpi_ComplianceReport' stored procedure. 
		/// </summary>
		/// <param name="sYear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="siteId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companySiteCategoryId"> A <c>System.String</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet ComplianceReport(TransactionManager transactionManager, int start, int pageLength , System.Int32? sYear, System.Int32? siteId, System.String companySiteCategoryId);
		
		#endregion
		
		#region _Kpi_Reports_LeadingI_Company_AllSites 
		
		/// <summary>
		///	This method wrap the '_Kpi_Reports_LeadingI_Company_AllSites' stored procedure. 
		/// </summary>
		/// <param name="fromDate"> A <c>System.String</c> instance.</param>
		/// <param name="toDate"> A <c>System.String</c> instance.</param>
		/// <param name="currentMonth"> A <c>System.Int32?</c> instance.</param>
		/// <param name="currentYear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Reports_LeadingI_Company_AllSites(System.String fromDate, System.String toDate, System.Int32? currentMonth, System.Int32? currentYear, System.Int32? companyId)
		{
			return Reports_LeadingI_Company_AllSites(null, 0, int.MaxValue , fromDate, toDate, currentMonth, currentYear, companyId);
		}
		
		/// <summary>
		///	This method wrap the '_Kpi_Reports_LeadingI_Company_AllSites' stored procedure. 
		/// </summary>
		/// <param name="fromDate"> A <c>System.String</c> instance.</param>
		/// <param name="toDate"> A <c>System.String</c> instance.</param>
		/// <param name="currentMonth"> A <c>System.Int32?</c> instance.</param>
		/// <param name="currentYear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Reports_LeadingI_Company_AllSites(int start, int pageLength, System.String fromDate, System.String toDate, System.Int32? currentMonth, System.Int32? currentYear, System.Int32? companyId)
		{
			return Reports_LeadingI_Company_AllSites(null, start, pageLength , fromDate, toDate, currentMonth, currentYear, companyId);
		}
				
		/// <summary>
		///	This method wrap the '_Kpi_Reports_LeadingI_Company_AllSites' stored procedure. 
		/// </summary>
		/// <param name="fromDate"> A <c>System.String</c> instance.</param>
		/// <param name="toDate"> A <c>System.String</c> instance.</param>
		/// <param name="currentMonth"> A <c>System.Int32?</c> instance.</param>
		/// <param name="currentYear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Reports_LeadingI_Company_AllSites(TransactionManager transactionManager, System.String fromDate, System.String toDate, System.Int32? currentMonth, System.Int32? currentYear, System.Int32? companyId)
		{
			return Reports_LeadingI_Company_AllSites(transactionManager, 0, int.MaxValue , fromDate, toDate, currentMonth, currentYear, companyId);
		}
		
		/// <summary>
		///	This method wrap the '_Kpi_Reports_LeadingI_Company_AllSites' stored procedure. 
		/// </summary>
		/// <param name="fromDate"> A <c>System.String</c> instance.</param>
		/// <param name="toDate"> A <c>System.String</c> instance.</param>
		/// <param name="currentMonth"> A <c>System.Int32?</c> instance.</param>
		/// <param name="currentYear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet Reports_LeadingI_Company_AllSites(TransactionManager transactionManager, int start, int pageLength , System.String fromDate, System.String toDate, System.Int32? currentMonth, System.Int32? currentYear, System.Int32? companyId);
		
		#endregion
		
		#region _Kpi_ComplianceReport_GetMandatedTraining_ByCompany 
		
		/// <summary>
		///	This method wrap the '_Kpi_ComplianceReport_GetMandatedTraining_ByCompany' stored procedure. 
		/// </summary>
		/// <param name="sYear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="siteId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companySiteCategoryId2"> A <c>System.String</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet ComplianceReport_GetMandatedTraining_ByCompany(System.Int32? sYear, System.Int32? companyId, System.Int32? siteId, System.String companySiteCategoryId2)
		{
			return ComplianceReport_GetMandatedTraining_ByCompany(null, 0, int.MaxValue , sYear, companyId, siteId, companySiteCategoryId2);
		}
		
		/// <summary>
		///	This method wrap the '_Kpi_ComplianceReport_GetMandatedTraining_ByCompany' stored procedure. 
		/// </summary>
		/// <param name="sYear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="siteId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companySiteCategoryId2"> A <c>System.String</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet ComplianceReport_GetMandatedTraining_ByCompany(int start, int pageLength, System.Int32? sYear, System.Int32? companyId, System.Int32? siteId, System.String companySiteCategoryId2)
		{
			return ComplianceReport_GetMandatedTraining_ByCompany(null, start, pageLength , sYear, companyId, siteId, companySiteCategoryId2);
		}
				
		/// <summary>
		///	This method wrap the '_Kpi_ComplianceReport_GetMandatedTraining_ByCompany' stored procedure. 
		/// </summary>
		/// <param name="sYear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="siteId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companySiteCategoryId2"> A <c>System.String</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet ComplianceReport_GetMandatedTraining_ByCompany(TransactionManager transactionManager, System.Int32? sYear, System.Int32? companyId, System.Int32? siteId, System.String companySiteCategoryId2)
		{
			return ComplianceReport_GetMandatedTraining_ByCompany(transactionManager, 0, int.MaxValue , sYear, companyId, siteId, companySiteCategoryId2);
		}
		
		/// <summary>
		///	This method wrap the '_Kpi_ComplianceReport_GetMandatedTraining_ByCompany' stored procedure. 
		/// </summary>
		/// <param name="sYear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="siteId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companySiteCategoryId2"> A <c>System.String</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet ComplianceReport_GetMandatedTraining_ByCompany(TransactionManager transactionManager, int start, int pageLength , System.Int32? sYear, System.Int32? companyId, System.Int32? siteId, System.String companySiteCategoryId2);
		
		#endregion
		
		#region _Kpi_Reports_LeadingI_AllCompanies_VICOPS 
		
		/// <summary>
		///	This method wrap the '_Kpi_Reports_LeadingI_AllCompanies_VICOPS' stored procedure. 
		/// </summary>
		/// <param name="fromDate"> A <c>System.String</c> instance.</param>
		/// <param name="toDate"> A <c>System.String</c> instance.</param>
		/// <param name="currentMonth"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Reports_LeadingI_AllCompanies_VICOPS(System.String fromDate, System.String toDate, System.Int32? currentMonth)
		{
			return Reports_LeadingI_AllCompanies_VICOPS(null, 0, int.MaxValue , fromDate, toDate, currentMonth);
		}
		
		/// <summary>
		///	This method wrap the '_Kpi_Reports_LeadingI_AllCompanies_VICOPS' stored procedure. 
		/// </summary>
		/// <param name="fromDate"> A <c>System.String</c> instance.</param>
		/// <param name="toDate"> A <c>System.String</c> instance.</param>
		/// <param name="currentMonth"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Reports_LeadingI_AllCompanies_VICOPS(int start, int pageLength, System.String fromDate, System.String toDate, System.Int32? currentMonth)
		{
			return Reports_LeadingI_AllCompanies_VICOPS(null, start, pageLength , fromDate, toDate, currentMonth);
		}
				
		/// <summary>
		///	This method wrap the '_Kpi_Reports_LeadingI_AllCompanies_VICOPS' stored procedure. 
		/// </summary>
		/// <param name="fromDate"> A <c>System.String</c> instance.</param>
		/// <param name="toDate"> A <c>System.String</c> instance.</param>
		/// <param name="currentMonth"> A <c>System.Int32?</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Reports_LeadingI_AllCompanies_VICOPS(TransactionManager transactionManager, System.String fromDate, System.String toDate, System.Int32? currentMonth)
		{
			return Reports_LeadingI_AllCompanies_VICOPS(transactionManager, 0, int.MaxValue , fromDate, toDate, currentMonth);
		}
		
		/// <summary>
		///	This method wrap the '_Kpi_Reports_LeadingI_AllCompanies_VICOPS' stored procedure. 
		/// </summary>
		/// <param name="fromDate"> A <c>System.String</c> instance.</param>
		/// <param name="toDate"> A <c>System.String</c> instance.</param>
		/// <param name="currentMonth"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet Reports_LeadingI_AllCompanies_VICOPS(TransactionManager transactionManager, int start, int pageLength , System.String fromDate, System.String toDate, System.Int32? currentMonth);
		
		#endregion
		
		#region _Kpi_Reports_CustomReport_GetByCompanyId 
		
		/// <summary>
		///	This method wrap the '_Kpi_Reports_CustomReport_GetByCompanyId' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Reports_CustomReport_GetByCompanyId(System.Int32? companyId)
		{
			return Reports_CustomReport_GetByCompanyId(null, 0, int.MaxValue , companyId);
		}
		
		/// <summary>
		///	This method wrap the '_Kpi_Reports_CustomReport_GetByCompanyId' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Reports_CustomReport_GetByCompanyId(int start, int pageLength, System.Int32? companyId)
		{
			return Reports_CustomReport_GetByCompanyId(null, start, pageLength , companyId);
		}
				
		/// <summary>
		///	This method wrap the '_Kpi_Reports_CustomReport_GetByCompanyId' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Reports_CustomReport_GetByCompanyId(TransactionManager transactionManager, System.Int32? companyId)
		{
			return Reports_CustomReport_GetByCompanyId(transactionManager, 0, int.MaxValue , companyId);
		}
		
		/// <summary>
		///	This method wrap the '_Kpi_Reports_CustomReport_GetByCompanyId' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet Reports_CustomReport_GetByCompanyId(TransactionManager transactionManager, int start, int pageLength , System.Int32? companyId);
		
		#endregion
		
		#region _Kpi_mmSummary_ByRegion 
		
		/// <summary>
		///	This method wrap the '_Kpi_mmSummary_ByRegion' stored procedure. 
		/// </summary>
		/// <param name="previousmonth"> A <c>System.Int32?</c> instance.</param>
		/// <param name="currentmonth"> A <c>System.Int32?</c> instance.</param>
		/// <param name="currentyear"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet mmSummary_ByRegion(System.Int32? previousmonth, System.Int32? currentmonth, System.Int32? currentyear)
		{
			return mmSummary_ByRegion(null, 0, int.MaxValue , previousmonth, currentmonth, currentyear);
		}
		
		/// <summary>
		///	This method wrap the '_Kpi_mmSummary_ByRegion' stored procedure. 
		/// </summary>
		/// <param name="previousmonth"> A <c>System.Int32?</c> instance.</param>
		/// <param name="currentmonth"> A <c>System.Int32?</c> instance.</param>
		/// <param name="currentyear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet mmSummary_ByRegion(int start, int pageLength, System.Int32? previousmonth, System.Int32? currentmonth, System.Int32? currentyear)
		{
			return mmSummary_ByRegion(null, start, pageLength , previousmonth, currentmonth, currentyear);
		}
				
		/// <summary>
		///	This method wrap the '_Kpi_mmSummary_ByRegion' stored procedure. 
		/// </summary>
		/// <param name="previousmonth"> A <c>System.Int32?</c> instance.</param>
		/// <param name="currentmonth"> A <c>System.Int32?</c> instance.</param>
		/// <param name="currentyear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet mmSummary_ByRegion(TransactionManager transactionManager, System.Int32? previousmonth, System.Int32? currentmonth, System.Int32? currentyear)
		{
			return mmSummary_ByRegion(transactionManager, 0, int.MaxValue , previousmonth, currentmonth, currentyear);
		}
		
		/// <summary>
		///	This method wrap the '_Kpi_mmSummary_ByRegion' stored procedure. 
		/// </summary>
		/// <param name="previousmonth"> A <c>System.Int32?</c> instance.</param>
		/// <param name="currentmonth"> A <c>System.Int32?</c> instance.</param>
		/// <param name="currentyear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet mmSummary_ByRegion(TransactionManager transactionManager, int start, int pageLength , System.Int32? previousmonth, System.Int32? currentmonth, System.Int32? currentyear);
		
		#endregion
		
		#region _Kpi_ComplianceReport_BySite 
		
		/// <summary>
		///	This method wrap the '_Kpi_ComplianceReport_BySite' stored procedure. 
		/// </summary>
		/// <param name="sYear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="siteId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companySiteCategoryId"> A <c>System.String</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet ComplianceReport_BySite(System.Int32? sYear, System.Int32? siteId, System.String companySiteCategoryId)
		{
			return ComplianceReport_BySite(null, 0, int.MaxValue , sYear, siteId, companySiteCategoryId);
		}
		
		/// <summary>
		///	This method wrap the '_Kpi_ComplianceReport_BySite' stored procedure. 
		/// </summary>
		/// <param name="sYear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="siteId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companySiteCategoryId"> A <c>System.String</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet ComplianceReport_BySite(int start, int pageLength, System.Int32? sYear, System.Int32? siteId, System.String companySiteCategoryId)
		{
			return ComplianceReport_BySite(null, start, pageLength , sYear, siteId, companySiteCategoryId);
		}
				
		/// <summary>
		///	This method wrap the '_Kpi_ComplianceReport_BySite' stored procedure. 
		/// </summary>
		/// <param name="sYear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="siteId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companySiteCategoryId"> A <c>System.String</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet ComplianceReport_BySite(TransactionManager transactionManager, System.Int32? sYear, System.Int32? siteId, System.String companySiteCategoryId)
		{
			return ComplianceReport_BySite(transactionManager, 0, int.MaxValue , sYear, siteId, companySiteCategoryId);
		}
		
		/// <summary>
		///	This method wrap the '_Kpi_ComplianceReport_BySite' stored procedure. 
		/// </summary>
		/// <param name="sYear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="siteId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companySiteCategoryId"> A <c>System.String</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet ComplianceReport_BySite(TransactionManager transactionManager, int start, int pageLength , System.Int32? sYear, System.Int32? siteId, System.String companySiteCategoryId);
		
		#endregion
		
		#region _Kpi_Reports_LeadingI_Company_VICOPS 
		
		/// <summary>
		///	This method wrap the '_Kpi_Reports_LeadingI_Company_VICOPS' stored procedure. 
		/// </summary>
		/// <param name="fromDate"> A <c>System.String</c> instance.</param>
		/// <param name="toDate"> A <c>System.String</c> instance.</param>
		/// <param name="currentMonth"> A <c>System.Int32?</c> instance.</param>
		/// <param name="currentYear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Reports_LeadingI_Company_VICOPS(System.String fromDate, System.String toDate, System.Int32? currentMonth, System.Int32? currentYear, System.Int32? companyId)
		{
			return Reports_LeadingI_Company_VICOPS(null, 0, int.MaxValue , fromDate, toDate, currentMonth, currentYear, companyId);
		}
		
		/// <summary>
		///	This method wrap the '_Kpi_Reports_LeadingI_Company_VICOPS' stored procedure. 
		/// </summary>
		/// <param name="fromDate"> A <c>System.String</c> instance.</param>
		/// <param name="toDate"> A <c>System.String</c> instance.</param>
		/// <param name="currentMonth"> A <c>System.Int32?</c> instance.</param>
		/// <param name="currentYear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Reports_LeadingI_Company_VICOPS(int start, int pageLength, System.String fromDate, System.String toDate, System.Int32? currentMonth, System.Int32? currentYear, System.Int32? companyId)
		{
			return Reports_LeadingI_Company_VICOPS(null, start, pageLength , fromDate, toDate, currentMonth, currentYear, companyId);
		}
				
		/// <summary>
		///	This method wrap the '_Kpi_Reports_LeadingI_Company_VICOPS' stored procedure. 
		/// </summary>
		/// <param name="fromDate"> A <c>System.String</c> instance.</param>
		/// <param name="toDate"> A <c>System.String</c> instance.</param>
		/// <param name="currentMonth"> A <c>System.Int32?</c> instance.</param>
		/// <param name="currentYear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Reports_LeadingI_Company_VICOPS(TransactionManager transactionManager, System.String fromDate, System.String toDate, System.Int32? currentMonth, System.Int32? currentYear, System.Int32? companyId)
		{
			return Reports_LeadingI_Company_VICOPS(transactionManager, 0, int.MaxValue , fromDate, toDate, currentMonth, currentYear, companyId);
		}
		
		/// <summary>
		///	This method wrap the '_Kpi_Reports_LeadingI_Company_VICOPS' stored procedure. 
		/// </summary>
		/// <param name="fromDate"> A <c>System.String</c> instance.</param>
		/// <param name="toDate"> A <c>System.String</c> instance.</param>
		/// <param name="currentMonth"> A <c>System.Int32?</c> instance.</param>
		/// <param name="currentYear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet Reports_LeadingI_Company_VICOPS(TransactionManager transactionManager, int start, int pageLength , System.String fromDate, System.String toDate, System.Int32? currentMonth, System.Int32? currentYear, System.Int32? companyId);
		
		#endregion
		
		#region _Kpi_Reports_Statistical_NoKPI 
		
		/// <summary>
		///	This method wrap the '_Kpi_Reports_Statistical_NoKPI' stored procedure. 
		/// </summary>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Reports_Statistical_NoKPI()
		{
			return Reports_Statistical_NoKPI(null, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_Kpi_Reports_Statistical_NoKPI' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Reports_Statistical_NoKPI(int start, int pageLength)
		{
			return Reports_Statistical_NoKPI(null, start, pageLength );
		}
				
		/// <summary>
		///	This method wrap the '_Kpi_Reports_Statistical_NoKPI' stored procedure. 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Reports_Statistical_NoKPI(TransactionManager transactionManager)
		{
			return Reports_Statistical_NoKPI(transactionManager, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_Kpi_Reports_Statistical_NoKPI' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet Reports_Statistical_NoKPI(TransactionManager transactionManager, int start, int pageLength );
		
		#endregion
		
		#region _Kpi_Reports_LeadingI_AllCompanies_WAO 
		
		/// <summary>
		///	This method wrap the '_Kpi_Reports_LeadingI_AllCompanies_WAO' stored procedure. 
		/// </summary>
		/// <param name="fromDate"> A <c>System.String</c> instance.</param>
		/// <param name="toDate"> A <c>System.String</c> instance.</param>
		/// <param name="currentMonth"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Reports_LeadingI_AllCompanies_WAO(System.String fromDate, System.String toDate, System.Int32? currentMonth)
		{
			return Reports_LeadingI_AllCompanies_WAO(null, 0, int.MaxValue , fromDate, toDate, currentMonth);
		}
		
		/// <summary>
		///	This method wrap the '_Kpi_Reports_LeadingI_AllCompanies_WAO' stored procedure. 
		/// </summary>
		/// <param name="fromDate"> A <c>System.String</c> instance.</param>
		/// <param name="toDate"> A <c>System.String</c> instance.</param>
		/// <param name="currentMonth"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Reports_LeadingI_AllCompanies_WAO(int start, int pageLength, System.String fromDate, System.String toDate, System.Int32? currentMonth)
		{
			return Reports_LeadingI_AllCompanies_WAO(null, start, pageLength , fromDate, toDate, currentMonth);
		}
				
		/// <summary>
		///	This method wrap the '_Kpi_Reports_LeadingI_AllCompanies_WAO' stored procedure. 
		/// </summary>
		/// <param name="fromDate"> A <c>System.String</c> instance.</param>
		/// <param name="toDate"> A <c>System.String</c> instance.</param>
		/// <param name="currentMonth"> A <c>System.Int32?</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Reports_LeadingI_AllCompanies_WAO(TransactionManager transactionManager, System.String fromDate, System.String toDate, System.Int32? currentMonth)
		{
			return Reports_LeadingI_AllCompanies_WAO(transactionManager, 0, int.MaxValue , fromDate, toDate, currentMonth);
		}
		
		/// <summary>
		///	This method wrap the '_Kpi_Reports_LeadingI_AllCompanies_WAO' stored procedure. 
		/// </summary>
		/// <param name="fromDate"> A <c>System.String</c> instance.</param>
		/// <param name="toDate"> A <c>System.String</c> instance.</param>
		/// <param name="currentMonth"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet Reports_LeadingI_AllCompanies_WAO(TransactionManager transactionManager, int start, int pageLength , System.String fromDate, System.String toDate, System.Int32? currentMonth);
		
		#endregion
		
		#region _Kpi_mmSummary_BySite_ByCompany 
		
		/// <summary>
		///	This method wrap the '_Kpi_mmSummary_BySite_ByCompany' stored procedure. 
		/// </summary>
		/// <param name="previousmonth"> A <c>System.Int32?</c> instance.</param>
		/// <param name="currentmonth"> A <c>System.Int32?</c> instance.</param>
		/// <param name="currentyear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="regionId"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet mmSummary_BySite_ByCompany(System.Int32? previousmonth, System.Int32? currentmonth, System.Int32? currentyear, System.Int32? companyId, System.Int32? regionId)
		{
			return mmSummary_BySite_ByCompany(null, 0, int.MaxValue , previousmonth, currentmonth, currentyear, companyId, regionId);
		}
		
		/// <summary>
		///	This method wrap the '_Kpi_mmSummary_BySite_ByCompany' stored procedure. 
		/// </summary>
		/// <param name="previousmonth"> A <c>System.Int32?</c> instance.</param>
		/// <param name="currentmonth"> A <c>System.Int32?</c> instance.</param>
		/// <param name="currentyear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="regionId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet mmSummary_BySite_ByCompany(int start, int pageLength, System.Int32? previousmonth, System.Int32? currentmonth, System.Int32? currentyear, System.Int32? companyId, System.Int32? regionId)
		{
			return mmSummary_BySite_ByCompany(null, start, pageLength , previousmonth, currentmonth, currentyear, companyId, regionId);
		}
				
		/// <summary>
		///	This method wrap the '_Kpi_mmSummary_BySite_ByCompany' stored procedure. 
		/// </summary>
		/// <param name="previousmonth"> A <c>System.Int32?</c> instance.</param>
		/// <param name="currentmonth"> A <c>System.Int32?</c> instance.</param>
		/// <param name="currentyear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="regionId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet mmSummary_BySite_ByCompany(TransactionManager transactionManager, System.Int32? previousmonth, System.Int32? currentmonth, System.Int32? currentyear, System.Int32? companyId, System.Int32? regionId)
		{
			return mmSummary_BySite_ByCompany(transactionManager, 0, int.MaxValue , previousmonth, currentmonth, currentyear, companyId, regionId);
		}
		
		/// <summary>
		///	This method wrap the '_Kpi_mmSummary_BySite_ByCompany' stored procedure. 
		/// </summary>
		/// <param name="previousmonth"> A <c>System.Int32?</c> instance.</param>
		/// <param name="currentmonth"> A <c>System.Int32?</c> instance.</param>
		/// <param name="currentyear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="regionId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet mmSummary_BySite_ByCompany(TransactionManager transactionManager, int start, int pageLength , System.Int32? previousmonth, System.Int32? currentmonth, System.Int32? currentyear, System.Int32? companyId, System.Int32? regionId);
		
		#endregion
		
		#region _Kpi_Reports_LeadingI_AllResidentialCompanies_VicOps 
		
		/// <summary>
		///	This method wrap the '_Kpi_Reports_LeadingI_AllResidentialCompanies_VicOps' stored procedure. 
		/// </summary>
		/// <param name="fromDate"> A <c>System.String</c> instance.</param>
		/// <param name="toDate"> A <c>System.String</c> instance.</param>
		/// <param name="currentMonth"> A <c>System.Int32?</c> instance.</param>
		/// <param name="currentYear"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Reports_LeadingI_AllResidentialCompanies_VicOps(System.String fromDate, System.String toDate, System.Int32? currentMonth, System.Int32? currentYear)
		{
			return Reports_LeadingI_AllResidentialCompanies_VicOps(null, 0, int.MaxValue , fromDate, toDate, currentMonth, currentYear);
		}
		
		/// <summary>
		///	This method wrap the '_Kpi_Reports_LeadingI_AllResidentialCompanies_VicOps' stored procedure. 
		/// </summary>
		/// <param name="fromDate"> A <c>System.String</c> instance.</param>
		/// <param name="toDate"> A <c>System.String</c> instance.</param>
		/// <param name="currentMonth"> A <c>System.Int32?</c> instance.</param>
		/// <param name="currentYear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Reports_LeadingI_AllResidentialCompanies_VicOps(int start, int pageLength, System.String fromDate, System.String toDate, System.Int32? currentMonth, System.Int32? currentYear)
		{
			return Reports_LeadingI_AllResidentialCompanies_VicOps(null, start, pageLength , fromDate, toDate, currentMonth, currentYear);
		}
				
		/// <summary>
		///	This method wrap the '_Kpi_Reports_LeadingI_AllResidentialCompanies_VicOps' stored procedure. 
		/// </summary>
		/// <param name="fromDate"> A <c>System.String</c> instance.</param>
		/// <param name="toDate"> A <c>System.String</c> instance.</param>
		/// <param name="currentMonth"> A <c>System.Int32?</c> instance.</param>
		/// <param name="currentYear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Reports_LeadingI_AllResidentialCompanies_VicOps(TransactionManager transactionManager, System.String fromDate, System.String toDate, System.Int32? currentMonth, System.Int32? currentYear)
		{
			return Reports_LeadingI_AllResidentialCompanies_VicOps(transactionManager, 0, int.MaxValue , fromDate, toDate, currentMonth, currentYear);
		}
		
		/// <summary>
		///	This method wrap the '_Kpi_Reports_LeadingI_AllResidentialCompanies_VicOps' stored procedure. 
		/// </summary>
		/// <param name="fromDate"> A <c>System.String</c> instance.</param>
		/// <param name="toDate"> A <c>System.String</c> instance.</param>
		/// <param name="currentMonth"> A <c>System.Int32?</c> instance.</param>
		/// <param name="currentYear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet Reports_LeadingI_AllResidentialCompanies_VicOps(TransactionManager transactionManager, int start, int pageLength , System.String fromDate, System.String toDate, System.Int32? currentMonth, System.Int32? currentYear);
		
		#endregion
		
		#region _Kpi_GetReport_ComplianceChart_A_A 
		
		/// <summary>
		///	This method wrap the '_Kpi_GetReport_ComplianceChart_A_A' stored procedure. 
		/// </summary>
		/// <param name="regionId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="fromDate"> A <c>System.String</c> instance.</param>
		/// <param name="toDate"> A <c>System.String</c> instance.</param>
		/// <param name="currentMonth"> A <c>System.Int32?</c> instance.</param>
		/// <param name="currentYear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="qtrId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="residentialStatus"> A <c>System.String</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetReport_ComplianceChart_A_A(System.Int32? regionId, System.Int32? companyId, System.String fromDate, System.String toDate, System.Int32? currentMonth, System.Int32? currentYear, System.Int32? qtrId, System.String residentialStatus)
		{
			return GetReport_ComplianceChart_A_A(null, 0, int.MaxValue , regionId, companyId, fromDate, toDate, currentMonth, currentYear, qtrId, residentialStatus);
		}
		
		/// <summary>
		///	This method wrap the '_Kpi_GetReport_ComplianceChart_A_A' stored procedure. 
		/// </summary>
		/// <param name="regionId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="fromDate"> A <c>System.String</c> instance.</param>
		/// <param name="toDate"> A <c>System.String</c> instance.</param>
		/// <param name="currentMonth"> A <c>System.Int32?</c> instance.</param>
		/// <param name="currentYear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="qtrId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="residentialStatus"> A <c>System.String</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetReport_ComplianceChart_A_A(int start, int pageLength, System.Int32? regionId, System.Int32? companyId, System.String fromDate, System.String toDate, System.Int32? currentMonth, System.Int32? currentYear, System.Int32? qtrId, System.String residentialStatus)
		{
			return GetReport_ComplianceChart_A_A(null, start, pageLength , regionId, companyId, fromDate, toDate, currentMonth, currentYear, qtrId, residentialStatus);
		}
				
		/// <summary>
		///	This method wrap the '_Kpi_GetReport_ComplianceChart_A_A' stored procedure. 
		/// </summary>
		/// <param name="regionId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="fromDate"> A <c>System.String</c> instance.</param>
		/// <param name="toDate"> A <c>System.String</c> instance.</param>
		/// <param name="currentMonth"> A <c>System.Int32?</c> instance.</param>
		/// <param name="currentYear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="qtrId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="residentialStatus"> A <c>System.String</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetReport_ComplianceChart_A_A(TransactionManager transactionManager, System.Int32? regionId, System.Int32? companyId, System.String fromDate, System.String toDate, System.Int32? currentMonth, System.Int32? currentYear, System.Int32? qtrId, System.String residentialStatus)
		{
			return GetReport_ComplianceChart_A_A(transactionManager, 0, int.MaxValue , regionId, companyId, fromDate, toDate, currentMonth, currentYear, qtrId, residentialStatus);
		}
		
		/// <summary>
		///	This method wrap the '_Kpi_GetReport_ComplianceChart_A_A' stored procedure. 
		/// </summary>
		/// <param name="regionId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="fromDate"> A <c>System.String</c> instance.</param>
		/// <param name="toDate"> A <c>System.String</c> instance.</param>
		/// <param name="currentMonth"> A <c>System.Int32?</c> instance.</param>
		/// <param name="currentYear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="qtrId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="residentialStatus"> A <c>System.String</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet GetReport_ComplianceChart_A_A(TransactionManager transactionManager, int start, int pageLength , System.Int32? regionId, System.Int32? companyId, System.String fromDate, System.String toDate, System.Int32? currentMonth, System.Int32? currentYear, System.Int32? qtrId, System.String residentialStatus);
		
		#endregion
		
		#region _Kpi_GetReport_ComplianceChart_S_A_Spider 
		
		/// <summary>
		///	This method wrap the '_Kpi_GetReport_ComplianceChart_S_A_Spider' stored procedure. 
		/// </summary>
		/// <param name="fromDate"> A <c>System.String</c> instance.</param>
		/// <param name="toDate"> A <c>System.String</c> instance.</param>
		/// <param name="currentMonth"> A <c>System.Int32?</c> instance.</param>
		/// <param name="currentYear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="residentialStatus"> A <c>System.String</c> instance.</param>
		/// <param name="regionId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="qtrId"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetReport_ComplianceChart_S_A_Spider(System.String fromDate, System.String toDate, System.Int32? currentMonth, System.Int32? currentYear, System.Int32? companyId, System.String residentialStatus, System.Int32? regionId, System.Int32? qtrId)
		{
			return GetReport_ComplianceChart_S_A_Spider(null, 0, int.MaxValue , fromDate, toDate, currentMonth, currentYear, companyId, residentialStatus, regionId, qtrId);
		}
		
		/// <summary>
		///	This method wrap the '_Kpi_GetReport_ComplianceChart_S_A_Spider' stored procedure. 
		/// </summary>
		/// <param name="fromDate"> A <c>System.String</c> instance.</param>
		/// <param name="toDate"> A <c>System.String</c> instance.</param>
		/// <param name="currentMonth"> A <c>System.Int32?</c> instance.</param>
		/// <param name="currentYear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="residentialStatus"> A <c>System.String</c> instance.</param>
		/// <param name="regionId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="qtrId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetReport_ComplianceChart_S_A_Spider(int start, int pageLength, System.String fromDate, System.String toDate, System.Int32? currentMonth, System.Int32? currentYear, System.Int32? companyId, System.String residentialStatus, System.Int32? regionId, System.Int32? qtrId)
		{
			return GetReport_ComplianceChart_S_A_Spider(null, start, pageLength , fromDate, toDate, currentMonth, currentYear, companyId, residentialStatus, regionId, qtrId);
		}
				
		/// <summary>
		///	This method wrap the '_Kpi_GetReport_ComplianceChart_S_A_Spider' stored procedure. 
		/// </summary>
		/// <param name="fromDate"> A <c>System.String</c> instance.</param>
		/// <param name="toDate"> A <c>System.String</c> instance.</param>
		/// <param name="currentMonth"> A <c>System.Int32?</c> instance.</param>
		/// <param name="currentYear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="residentialStatus"> A <c>System.String</c> instance.</param>
		/// <param name="regionId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="qtrId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetReport_ComplianceChart_S_A_Spider(TransactionManager transactionManager, System.String fromDate, System.String toDate, System.Int32? currentMonth, System.Int32? currentYear, System.Int32? companyId, System.String residentialStatus, System.Int32? regionId, System.Int32? qtrId)
		{
			return GetReport_ComplianceChart_S_A_Spider(transactionManager, 0, int.MaxValue , fromDate, toDate, currentMonth, currentYear, companyId, residentialStatus, regionId, qtrId);
		}
		
		/// <summary>
		///	This method wrap the '_Kpi_GetReport_ComplianceChart_S_A_Spider' stored procedure. 
		/// </summary>
		/// <param name="fromDate"> A <c>System.String</c> instance.</param>
		/// <param name="toDate"> A <c>System.String</c> instance.</param>
		/// <param name="currentMonth"> A <c>System.Int32?</c> instance.</param>
		/// <param name="currentYear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="residentialStatus"> A <c>System.String</c> instance.</param>
		/// <param name="regionId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="qtrId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet GetReport_ComplianceChart_S_A_Spider(TransactionManager transactionManager, int start, int pageLength , System.String fromDate, System.String toDate, System.Int32? currentMonth, System.Int32? currentYear, System.Int32? companyId, System.String residentialStatus, System.Int32? regionId, System.Int32? qtrId);
		
		#endregion
		
		#region _Kpi_GetComparisonReportDataGroupedByCompanySiteByYearMonth 
		
		/// <summary>
		///	This method wrap the '_Kpi_GetComparisonReportDataGroupedByCompanySiteByYearMonth' stored procedure. 
		/// </summary>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="month"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetComparisonReportDataGroupedByCompanySiteByYearMonth(System.Int32? year, System.Int32? month)
		{
			return GetComparisonReportDataGroupedByCompanySiteByYearMonth(null, 0, int.MaxValue , year, month);
		}
		
		/// <summary>
		///	This method wrap the '_Kpi_GetComparisonReportDataGroupedByCompanySiteByYearMonth' stored procedure. 
		/// </summary>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="month"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetComparisonReportDataGroupedByCompanySiteByYearMonth(int start, int pageLength, System.Int32? year, System.Int32? month)
		{
			return GetComparisonReportDataGroupedByCompanySiteByYearMonth(null, start, pageLength , year, month);
		}
				
		/// <summary>
		///	This method wrap the '_Kpi_GetComparisonReportDataGroupedByCompanySiteByYearMonth' stored procedure. 
		/// </summary>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="month"> A <c>System.Int32?</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetComparisonReportDataGroupedByCompanySiteByYearMonth(TransactionManager transactionManager, System.Int32? year, System.Int32? month)
		{
			return GetComparisonReportDataGroupedByCompanySiteByYearMonth(transactionManager, 0, int.MaxValue , year, month);
		}
		
		/// <summary>
		///	This method wrap the '_Kpi_GetComparisonReportDataGroupedByCompanySiteByYearMonth' stored procedure. 
		/// </summary>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="month"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet GetComparisonReportDataGroupedByCompanySiteByYearMonth(TransactionManager transactionManager, int start, int pageLength , System.Int32? year, System.Int32? month);
		
		#endregion
		
		#region _Kpi_GetReport_ComplianceChart_A_A_Australia 
		
		/// <summary>
		///	This method wrap the '_Kpi_GetReport_ComplianceChart_A_A_Australia' stored procedure. 
		/// </summary>
		/// <param name="regionId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="fromDate"> A <c>System.String</c> instance.</param>
		/// <param name="toDate"> A <c>System.String</c> instance.</param>
		/// <param name="currentMonth"> A <c>System.Int32?</c> instance.</param>
		/// <param name="currentYear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="qtrId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="residentialStatus"> A <c>System.String</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetReport_ComplianceChart_A_A_Australia(System.Int32? regionId, System.Int32? companyId, System.String fromDate, System.String toDate, System.Int32? currentMonth, System.Int32? currentYear, System.Int32? qtrId, System.String residentialStatus)
		{
			return GetReport_ComplianceChart_A_A_Australia(null, 0, int.MaxValue , regionId, companyId, fromDate, toDate, currentMonth, currentYear, qtrId, residentialStatus);
		}
		
		/// <summary>
		///	This method wrap the '_Kpi_GetReport_ComplianceChart_A_A_Australia' stored procedure. 
		/// </summary>
		/// <param name="regionId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="fromDate"> A <c>System.String</c> instance.</param>
		/// <param name="toDate"> A <c>System.String</c> instance.</param>
		/// <param name="currentMonth"> A <c>System.Int32?</c> instance.</param>
		/// <param name="currentYear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="qtrId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="residentialStatus"> A <c>System.String</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetReport_ComplianceChart_A_A_Australia(int start, int pageLength, System.Int32? regionId, System.Int32? companyId, System.String fromDate, System.String toDate, System.Int32? currentMonth, System.Int32? currentYear, System.Int32? qtrId, System.String residentialStatus)
		{
			return GetReport_ComplianceChart_A_A_Australia(null, start, pageLength , regionId, companyId, fromDate, toDate, currentMonth, currentYear, qtrId, residentialStatus);
		}
				
		/// <summary>
		///	This method wrap the '_Kpi_GetReport_ComplianceChart_A_A_Australia' stored procedure. 
		/// </summary>
		/// <param name="regionId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="fromDate"> A <c>System.String</c> instance.</param>
		/// <param name="toDate"> A <c>System.String</c> instance.</param>
		/// <param name="currentMonth"> A <c>System.Int32?</c> instance.</param>
		/// <param name="currentYear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="qtrId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="residentialStatus"> A <c>System.String</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetReport_ComplianceChart_A_A_Australia(TransactionManager transactionManager, System.Int32? regionId, System.Int32? companyId, System.String fromDate, System.String toDate, System.Int32? currentMonth, System.Int32? currentYear, System.Int32? qtrId, System.String residentialStatus)
		{
			return GetReport_ComplianceChart_A_A_Australia(transactionManager, 0, int.MaxValue , regionId, companyId, fromDate, toDate, currentMonth, currentYear, qtrId, residentialStatus);
		}
		
		/// <summary>
		///	This method wrap the '_Kpi_GetReport_ComplianceChart_A_A_Australia' stored procedure. 
		/// </summary>
		/// <param name="regionId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="fromDate"> A <c>System.String</c> instance.</param>
		/// <param name="toDate"> A <c>System.String</c> instance.</param>
		/// <param name="currentMonth"> A <c>System.Int32?</c> instance.</param>
		/// <param name="currentYear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="qtrId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="residentialStatus"> A <c>System.String</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet GetReport_ComplianceChart_A_A_Australia(TransactionManager transactionManager, int start, int pageLength , System.Int32? regionId, System.Int32? companyId, System.String fromDate, System.String toDate, System.Int32? currentMonth, System.Int32? currentYear, System.Int32? qtrId, System.String residentialStatus);
		
		#endregion
		
		#region _Kpi_Reports_LeadingI_AllResidentialCompanies_Site 
		
		/// <summary>
		///	This method wrap the '_Kpi_Reports_LeadingI_AllResidentialCompanies_Site' stored procedure. 
		/// </summary>
		/// <param name="fromDate"> A <c>System.String</c> instance.</param>
		/// <param name="toDate"> A <c>System.String</c> instance.</param>
		/// <param name="currentMonth"> A <c>System.Int32?</c> instance.</param>
		/// <param name="currentYear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="siteId"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Reports_LeadingI_AllResidentialCompanies_Site(System.String fromDate, System.String toDate, System.Int32? currentMonth, System.Int32? currentYear, System.Int32? siteId)
		{
			return Reports_LeadingI_AllResidentialCompanies_Site(null, 0, int.MaxValue , fromDate, toDate, currentMonth, currentYear, siteId);
		}
		
		/// <summary>
		///	This method wrap the '_Kpi_Reports_LeadingI_AllResidentialCompanies_Site' stored procedure. 
		/// </summary>
		/// <param name="fromDate"> A <c>System.String</c> instance.</param>
		/// <param name="toDate"> A <c>System.String</c> instance.</param>
		/// <param name="currentMonth"> A <c>System.Int32?</c> instance.</param>
		/// <param name="currentYear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="siteId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Reports_LeadingI_AllResidentialCompanies_Site(int start, int pageLength, System.String fromDate, System.String toDate, System.Int32? currentMonth, System.Int32? currentYear, System.Int32? siteId)
		{
			return Reports_LeadingI_AllResidentialCompanies_Site(null, start, pageLength , fromDate, toDate, currentMonth, currentYear, siteId);
		}
				
		/// <summary>
		///	This method wrap the '_Kpi_Reports_LeadingI_AllResidentialCompanies_Site' stored procedure. 
		/// </summary>
		/// <param name="fromDate"> A <c>System.String</c> instance.</param>
		/// <param name="toDate"> A <c>System.String</c> instance.</param>
		/// <param name="currentMonth"> A <c>System.Int32?</c> instance.</param>
		/// <param name="currentYear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="siteId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Reports_LeadingI_AllResidentialCompanies_Site(TransactionManager transactionManager, System.String fromDate, System.String toDate, System.Int32? currentMonth, System.Int32? currentYear, System.Int32? siteId)
		{
			return Reports_LeadingI_AllResidentialCompanies_Site(transactionManager, 0, int.MaxValue , fromDate, toDate, currentMonth, currentYear, siteId);
		}
		
		/// <summary>
		///	This method wrap the '_Kpi_Reports_LeadingI_AllResidentialCompanies_Site' stored procedure. 
		/// </summary>
		/// <param name="fromDate"> A <c>System.String</c> instance.</param>
		/// <param name="toDate"> A <c>System.String</c> instance.</param>
		/// <param name="currentMonth"> A <c>System.Int32?</c> instance.</param>
		/// <param name="currentYear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="siteId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet Reports_LeadingI_AllResidentialCompanies_Site(TransactionManager transactionManager, int start, int pageLength , System.String fromDate, System.String toDate, System.Int32? currentMonth, System.Int32? currentYear, System.Int32? siteId);
		
		#endregion
		
		#region _Kpi_Reports_LeadingI_AllResidentialCompanies_WAO 
		
		/// <summary>
		///	This method wrap the '_Kpi_Reports_LeadingI_AllResidentialCompanies_WAO' stored procedure. 
		/// </summary>
		/// <param name="fromDate"> A <c>System.String</c> instance.</param>
		/// <param name="toDate"> A <c>System.String</c> instance.</param>
		/// <param name="currentMonth"> A <c>System.Int32?</c> instance.</param>
		/// <param name="currentYear"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Reports_LeadingI_AllResidentialCompanies_WAO(System.String fromDate, System.String toDate, System.Int32? currentMonth, System.Int32? currentYear)
		{
			return Reports_LeadingI_AllResidentialCompanies_WAO(null, 0, int.MaxValue , fromDate, toDate, currentMonth, currentYear);
		}
		
		/// <summary>
		///	This method wrap the '_Kpi_Reports_LeadingI_AllResidentialCompanies_WAO' stored procedure. 
		/// </summary>
		/// <param name="fromDate"> A <c>System.String</c> instance.</param>
		/// <param name="toDate"> A <c>System.String</c> instance.</param>
		/// <param name="currentMonth"> A <c>System.Int32?</c> instance.</param>
		/// <param name="currentYear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Reports_LeadingI_AllResidentialCompanies_WAO(int start, int pageLength, System.String fromDate, System.String toDate, System.Int32? currentMonth, System.Int32? currentYear)
		{
			return Reports_LeadingI_AllResidentialCompanies_WAO(null, start, pageLength , fromDate, toDate, currentMonth, currentYear);
		}
				
		/// <summary>
		///	This method wrap the '_Kpi_Reports_LeadingI_AllResidentialCompanies_WAO' stored procedure. 
		/// </summary>
		/// <param name="fromDate"> A <c>System.String</c> instance.</param>
		/// <param name="toDate"> A <c>System.String</c> instance.</param>
		/// <param name="currentMonth"> A <c>System.Int32?</c> instance.</param>
		/// <param name="currentYear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Reports_LeadingI_AllResidentialCompanies_WAO(TransactionManager transactionManager, System.String fromDate, System.String toDate, System.Int32? currentMonth, System.Int32? currentYear)
		{
			return Reports_LeadingI_AllResidentialCompanies_WAO(transactionManager, 0, int.MaxValue , fromDate, toDate, currentMonth, currentYear);
		}
		
		/// <summary>
		///	This method wrap the '_Kpi_Reports_LeadingI_AllResidentialCompanies_WAO' stored procedure. 
		/// </summary>
		/// <param name="fromDate"> A <c>System.String</c> instance.</param>
		/// <param name="toDate"> A <c>System.String</c> instance.</param>
		/// <param name="currentMonth"> A <c>System.Int32?</c> instance.</param>
		/// <param name="currentYear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet Reports_LeadingI_AllResidentialCompanies_WAO(TransactionManager transactionManager, int start, int pageLength , System.String fromDate, System.String toDate, System.Int32? currentMonth, System.Int32? currentYear);
		
		#endregion
		
		#region _Kpi_mmSummary_BySite 
		
		/// <summary>
		///	This method wrap the '_Kpi_mmSummary_BySite' stored procedure. 
		/// </summary>
		/// <param name="previousmonth"> A <c>System.Int32?</c> instance.</param>
		/// <param name="currentmonth"> A <c>System.Int32?</c> instance.</param>
		/// <param name="currentyear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="regionId"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet mmSummary_BySite(System.Int32? previousmonth, System.Int32? currentmonth, System.Int32? currentyear, System.Int32? regionId)
		{
			return mmSummary_BySite(null, 0, int.MaxValue , previousmonth, currentmonth, currentyear, regionId);
		}
		
		/// <summary>
		///	This method wrap the '_Kpi_mmSummary_BySite' stored procedure. 
		/// </summary>
		/// <param name="previousmonth"> A <c>System.Int32?</c> instance.</param>
		/// <param name="currentmonth"> A <c>System.Int32?</c> instance.</param>
		/// <param name="currentyear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="regionId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet mmSummary_BySite(int start, int pageLength, System.Int32? previousmonth, System.Int32? currentmonth, System.Int32? currentyear, System.Int32? regionId)
		{
			return mmSummary_BySite(null, start, pageLength , previousmonth, currentmonth, currentyear, regionId);
		}
				
		/// <summary>
		///	This method wrap the '_Kpi_mmSummary_BySite' stored procedure. 
		/// </summary>
		/// <param name="previousmonth"> A <c>System.Int32?</c> instance.</param>
		/// <param name="currentmonth"> A <c>System.Int32?</c> instance.</param>
		/// <param name="currentyear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="regionId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet mmSummary_BySite(TransactionManager transactionManager, System.Int32? previousmonth, System.Int32? currentmonth, System.Int32? currentyear, System.Int32? regionId)
		{
			return mmSummary_BySite(transactionManager, 0, int.MaxValue , previousmonth, currentmonth, currentyear, regionId);
		}
		
		/// <summary>
		///	This method wrap the '_Kpi_mmSummary_BySite' stored procedure. 
		/// </summary>
		/// <param name="previousmonth"> A <c>System.Int32?</c> instance.</param>
		/// <param name="currentmonth"> A <c>System.Int32?</c> instance.</param>
		/// <param name="currentyear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="regionId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet mmSummary_BySite(TransactionManager transactionManager, int start, int pageLength , System.Int32? previousmonth, System.Int32? currentmonth, System.Int32? currentyear, System.Int32? regionId);
		
		#endregion
		
		#region _Kpi_GetReport_Recordables 
		
		/// <summary>
		///	This method wrap the '_Kpi_GetReport_Recordables' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="regionId"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetReport_Recordables(System.Int32? companyId, System.Int32? regionId)
		{
			return GetReport_Recordables(null, 0, int.MaxValue , companyId, regionId);
		}
		
		/// <summary>
		///	This method wrap the '_Kpi_GetReport_Recordables' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="regionId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetReport_Recordables(int start, int pageLength, System.Int32? companyId, System.Int32? regionId)
		{
			return GetReport_Recordables(null, start, pageLength , companyId, regionId);
		}
				
		/// <summary>
		///	This method wrap the '_Kpi_GetReport_Recordables' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="regionId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetReport_Recordables(TransactionManager transactionManager, System.Int32? companyId, System.Int32? regionId)
		{
			return GetReport_Recordables(transactionManager, 0, int.MaxValue , companyId, regionId);
		}
		
		/// <summary>
		///	This method wrap the '_Kpi_GetReport_Recordables' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="regionId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet GetReport_Recordables(TransactionManager transactionManager, int start, int pageLength , System.Int32? companyId, System.Int32? regionId);
		
		#endregion
		
		#region _Kpi_Reports_LeadingI_AllResidentialCompanies_Australia 
		
		/// <summary>
		///	This method wrap the '_Kpi_Reports_LeadingI_AllResidentialCompanies_Australia' stored procedure. 
		/// </summary>
		/// <param name="fromDate"> A <c>System.String</c> instance.</param>
		/// <param name="toDate"> A <c>System.String</c> instance.</param>
		/// <param name="currentMonth"> A <c>System.Int32?</c> instance.</param>
		/// <param name="currentYear"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Reports_LeadingI_AllResidentialCompanies_Australia(System.String fromDate, System.String toDate, System.Int32? currentMonth, System.Int32? currentYear)
		{
			return Reports_LeadingI_AllResidentialCompanies_Australia(null, 0, int.MaxValue , fromDate, toDate, currentMonth, currentYear);
		}
		
		/// <summary>
		///	This method wrap the '_Kpi_Reports_LeadingI_AllResidentialCompanies_Australia' stored procedure. 
		/// </summary>
		/// <param name="fromDate"> A <c>System.String</c> instance.</param>
		/// <param name="toDate"> A <c>System.String</c> instance.</param>
		/// <param name="currentMonth"> A <c>System.Int32?</c> instance.</param>
		/// <param name="currentYear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Reports_LeadingI_AllResidentialCompanies_Australia(int start, int pageLength, System.String fromDate, System.String toDate, System.Int32? currentMonth, System.Int32? currentYear)
		{
			return Reports_LeadingI_AllResidentialCompanies_Australia(null, start, pageLength , fromDate, toDate, currentMonth, currentYear);
		}
				
		/// <summary>
		///	This method wrap the '_Kpi_Reports_LeadingI_AllResidentialCompanies_Australia' stored procedure. 
		/// </summary>
		/// <param name="fromDate"> A <c>System.String</c> instance.</param>
		/// <param name="toDate"> A <c>System.String</c> instance.</param>
		/// <param name="currentMonth"> A <c>System.Int32?</c> instance.</param>
		/// <param name="currentYear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Reports_LeadingI_AllResidentialCompanies_Australia(TransactionManager transactionManager, System.String fromDate, System.String toDate, System.Int32? currentMonth, System.Int32? currentYear)
		{
			return Reports_LeadingI_AllResidentialCompanies_Australia(transactionManager, 0, int.MaxValue , fromDate, toDate, currentMonth, currentYear);
		}
		
		/// <summary>
		///	This method wrap the '_Kpi_Reports_LeadingI_AllResidentialCompanies_Australia' stored procedure. 
		/// </summary>
		/// <param name="fromDate"> A <c>System.String</c> instance.</param>
		/// <param name="toDate"> A <c>System.String</c> instance.</param>
		/// <param name="currentMonth"> A <c>System.Int32?</c> instance.</param>
		/// <param name="currentYear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet Reports_LeadingI_AllResidentialCompanies_Australia(TransactionManager transactionManager, int start, int pageLength , System.String fromDate, System.String toDate, System.Int32? currentMonth, System.Int32? currentYear);
		
		#endregion
		
		#region _Kpi_GetAllYearsSubmitted 
		
		/// <summary>
		///	This method wrap the '_Kpi_GetAllYearsSubmitted' stored procedure. 
		/// </summary>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetAllYearsSubmitted()
		{
			return GetAllYearsSubmitted(null, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_Kpi_GetAllYearsSubmitted' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetAllYearsSubmitted(int start, int pageLength)
		{
			return GetAllYearsSubmitted(null, start, pageLength );
		}
				
		/// <summary>
		///	This method wrap the '_Kpi_GetAllYearsSubmitted' stored procedure. 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetAllYearsSubmitted(TransactionManager transactionManager)
		{
			return GetAllYearsSubmitted(transactionManager, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_Kpi_GetAllYearsSubmitted' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet GetAllYearsSubmitted(TransactionManager transactionManager, int start, int pageLength );
		
		#endregion
		
		#region _Kpi_Reports_EHSIMS_Kpi_Incidents 
		
		/// <summary>
		///	This method wrap the '_Kpi_Reports_EHSIMS_Kpi_Incidents' stored procedure. 
		/// </summary>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Reports_EHSIMS_Kpi_Incidents()
		{
			return Reports_EHSIMS_Kpi_Incidents(null, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_Kpi_Reports_EHSIMS_Kpi_Incidents' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Reports_EHSIMS_Kpi_Incidents(int start, int pageLength)
		{
			return Reports_EHSIMS_Kpi_Incidents(null, start, pageLength );
		}
				
		/// <summary>
		///	This method wrap the '_Kpi_Reports_EHSIMS_Kpi_Incidents' stored procedure. 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Reports_EHSIMS_Kpi_Incidents(TransactionManager transactionManager)
		{
			return Reports_EHSIMS_Kpi_Incidents(transactionManager, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_Kpi_Reports_EHSIMS_Kpi_Incidents' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet Reports_EHSIMS_Kpi_Incidents(TransactionManager transactionManager, int start, int pageLength );
		
		#endregion
		
		#region _Kpi_GetReport_Compliance_SQ 
		
		/// <summary>
		///	This method wrap the '_Kpi_GetReport_Compliance_SQ' stored procedure. 
		/// </summary>
		/// <param name="regionId"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetReport_Compliance_SQ(System.Int32? regionId)
		{
			return GetReport_Compliance_SQ(null, 0, int.MaxValue , regionId);
		}
		
		/// <summary>
		///	This method wrap the '_Kpi_GetReport_Compliance_SQ' stored procedure. 
		/// </summary>
		/// <param name="regionId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetReport_Compliance_SQ(int start, int pageLength, System.Int32? regionId)
		{
			return GetReport_Compliance_SQ(null, start, pageLength , regionId);
		}
				
		/// <summary>
		///	This method wrap the '_Kpi_GetReport_Compliance_SQ' stored procedure. 
		/// </summary>
		/// <param name="regionId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetReport_Compliance_SQ(TransactionManager transactionManager, System.Int32? regionId)
		{
			return GetReport_Compliance_SQ(transactionManager, 0, int.MaxValue , regionId);
		}
		
		/// <summary>
		///	This method wrap the '_Kpi_GetReport_Compliance_SQ' stored procedure. 
		/// </summary>
		/// <param name="regionId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet GetReport_Compliance_SQ(TransactionManager transactionManager, int start, int pageLength , System.Int32? regionId);
		
		#endregion
		
		#region _Kpi_Reports_NonCompliance 
		
		/// <summary>
		///	This method wrap the '_Kpi_Reports_NonCompliance' stored procedure. 
		/// </summary>
		/// <param name="month"> A <c>System.String</c> instance.</param>
		/// <param name="year"> A <c>System.String</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Reports_NonCompliance(System.String month, System.String year)
		{
			return Reports_NonCompliance(null, 0, int.MaxValue , month, year);
		}
		
		/// <summary>
		///	This method wrap the '_Kpi_Reports_NonCompliance' stored procedure. 
		/// </summary>
		/// <param name="month"> A <c>System.String</c> instance.</param>
		/// <param name="year"> A <c>System.String</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Reports_NonCompliance(int start, int pageLength, System.String month, System.String year)
		{
			return Reports_NonCompliance(null, start, pageLength , month, year);
		}
				
		/// <summary>
		///	This method wrap the '_Kpi_Reports_NonCompliance' stored procedure. 
		/// </summary>
		/// <param name="month"> A <c>System.String</c> instance.</param>
		/// <param name="year"> A <c>System.String</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Reports_NonCompliance(TransactionManager transactionManager, System.String month, System.String year)
		{
			return Reports_NonCompliance(transactionManager, 0, int.MaxValue , month, year);
		}
		
		/// <summary>
		///	This method wrap the '_Kpi_Reports_NonCompliance' stored procedure. 
		/// </summary>
		/// <param name="month"> A <c>System.String</c> instance.</param>
		/// <param name="year"> A <c>System.String</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet Reports_NonCompliance(TransactionManager transactionManager, int start, int pageLength , System.String month, System.String year);
		
		#endregion
		
		#region _Kpi_Reports_Statistical_Site 
		
		/// <summary>
		///	This method wrap the '_Kpi_Reports_Statistical_Site' stored procedure. 
		/// </summary>
		/// <param name="fromDate"> A <c>System.String</c> instance.</param>
		/// <param name="toDate"> A <c>System.String</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Reports_Statistical_Site(System.String fromDate, System.String toDate)
		{
			return Reports_Statistical_Site(null, 0, int.MaxValue , fromDate, toDate);
		}
		
		/// <summary>
		///	This method wrap the '_Kpi_Reports_Statistical_Site' stored procedure. 
		/// </summary>
		/// <param name="fromDate"> A <c>System.String</c> instance.</param>
		/// <param name="toDate"> A <c>System.String</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Reports_Statistical_Site(int start, int pageLength, System.String fromDate, System.String toDate)
		{
			return Reports_Statistical_Site(null, start, pageLength , fromDate, toDate);
		}
				
		/// <summary>
		///	This method wrap the '_Kpi_Reports_Statistical_Site' stored procedure. 
		/// </summary>
		/// <param name="fromDate"> A <c>System.String</c> instance.</param>
		/// <param name="toDate"> A <c>System.String</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Reports_Statistical_Site(TransactionManager transactionManager, System.String fromDate, System.String toDate)
		{
			return Reports_Statistical_Site(transactionManager, 0, int.MaxValue , fromDate, toDate);
		}
		
		/// <summary>
		///	This method wrap the '_Kpi_Reports_Statistical_Site' stored procedure. 
		/// </summary>
		/// <param name="fromDate"> A <c>System.String</c> instance.</param>
		/// <param name="toDate"> A <c>System.String</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet Reports_Statistical_Site(TransactionManager transactionManager, int start, int pageLength , System.String fromDate, System.String toDate);
		
		#endregion
		
		#region _Kpi_ComplianceReport_ByRegion 
		
		/// <summary>
		///	This method wrap the '_Kpi_ComplianceReport_ByRegion' stored procedure. 
		/// </summary>
		/// <param name="sYear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="regionId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companySiteCategoryId"> A <c>System.String</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet ComplianceReport_ByRegion(System.Int32? sYear, System.Int32? regionId, System.String companySiteCategoryId)
		{
			return ComplianceReport_ByRegion(null, 0, int.MaxValue , sYear, regionId, companySiteCategoryId);
		}
		
		/// <summary>
		///	This method wrap the '_Kpi_ComplianceReport_ByRegion' stored procedure. 
		/// </summary>
		/// <param name="sYear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="regionId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companySiteCategoryId"> A <c>System.String</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet ComplianceReport_ByRegion(int start, int pageLength, System.Int32? sYear, System.Int32? regionId, System.String companySiteCategoryId)
		{
			return ComplianceReport_ByRegion(null, start, pageLength , sYear, regionId, companySiteCategoryId);
		}
				
		/// <summary>
		///	This method wrap the '_Kpi_ComplianceReport_ByRegion' stored procedure. 
		/// </summary>
		/// <param name="sYear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="regionId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companySiteCategoryId"> A <c>System.String</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet ComplianceReport_ByRegion(TransactionManager transactionManager, System.Int32? sYear, System.Int32? regionId, System.String companySiteCategoryId)
		{
			return ComplianceReport_ByRegion(transactionManager, 0, int.MaxValue , sYear, regionId, companySiteCategoryId);
		}
		
		/// <summary>
		///	This method wrap the '_Kpi_ComplianceReport_ByRegion' stored procedure. 
		/// </summary>
		/// <param name="sYear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="regionId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companySiteCategoryId"> A <c>System.String</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet ComplianceReport_ByRegion(TransactionManager transactionManager, int start, int pageLength , System.Int32? sYear, System.Int32? regionId, System.String companySiteCategoryId);
		
		#endregion
		
		#region _Kpi_Reports_LeadingI_Company_AllMines 
		
		/// <summary>
		///	This method wrap the '_Kpi_Reports_LeadingI_Company_AllMines' stored procedure. 
		/// </summary>
		/// <param name="fromDate"> A <c>System.String</c> instance.</param>
		/// <param name="toDate"> A <c>System.String</c> instance.</param>
		/// <param name="currentMonth"> A <c>System.Int32?</c> instance.</param>
		/// <param name="currentYear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Reports_LeadingI_Company_AllMines(System.String fromDate, System.String toDate, System.Int32? currentMonth, System.Int32? currentYear, System.Int32? companyId)
		{
			return Reports_LeadingI_Company_AllMines(null, 0, int.MaxValue , fromDate, toDate, currentMonth, currentYear, companyId);
		}
		
		/// <summary>
		///	This method wrap the '_Kpi_Reports_LeadingI_Company_AllMines' stored procedure. 
		/// </summary>
		/// <param name="fromDate"> A <c>System.String</c> instance.</param>
		/// <param name="toDate"> A <c>System.String</c> instance.</param>
		/// <param name="currentMonth"> A <c>System.Int32?</c> instance.</param>
		/// <param name="currentYear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Reports_LeadingI_Company_AllMines(int start, int pageLength, System.String fromDate, System.String toDate, System.Int32? currentMonth, System.Int32? currentYear, System.Int32? companyId)
		{
			return Reports_LeadingI_Company_AllMines(null, start, pageLength , fromDate, toDate, currentMonth, currentYear, companyId);
		}
				
		/// <summary>
		///	This method wrap the '_Kpi_Reports_LeadingI_Company_AllMines' stored procedure. 
		/// </summary>
		/// <param name="fromDate"> A <c>System.String</c> instance.</param>
		/// <param name="toDate"> A <c>System.String</c> instance.</param>
		/// <param name="currentMonth"> A <c>System.Int32?</c> instance.</param>
		/// <param name="currentYear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Reports_LeadingI_Company_AllMines(TransactionManager transactionManager, System.String fromDate, System.String toDate, System.Int32? currentMonth, System.Int32? currentYear, System.Int32? companyId)
		{
			return Reports_LeadingI_Company_AllMines(transactionManager, 0, int.MaxValue , fromDate, toDate, currentMonth, currentYear, companyId);
		}
		
		/// <summary>
		///	This method wrap the '_Kpi_Reports_LeadingI_Company_AllMines' stored procedure. 
		/// </summary>
		/// <param name="fromDate"> A <c>System.String</c> instance.</param>
		/// <param name="toDate"> A <c>System.String</c> instance.</param>
		/// <param name="currentMonth"> A <c>System.Int32?</c> instance.</param>
		/// <param name="currentYear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet Reports_LeadingI_Company_AllMines(TransactionManager transactionManager, int start, int pageLength , System.String fromDate, System.String toDate, System.Int32? currentMonth, System.Int32? currentYear, System.Int32? companyId);
		
		#endregion
		
		#region _Kpi_Reports_LeadingI_Company_Site 
		
		/// <summary>
		///	This method wrap the '_Kpi_Reports_LeadingI_Company_Site' stored procedure. 
		/// </summary>
		/// <param name="fromDate"> A <c>System.String</c> instance.</param>
		/// <param name="toDate"> A <c>System.String</c> instance.</param>
		/// <param name="currentMonth"> A <c>System.Int32?</c> instance.</param>
		/// <param name="currentYear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="siteId"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Reports_LeadingI_Company_Site(System.String fromDate, System.String toDate, System.Int32? currentMonth, System.Int32? currentYear, System.Int32? companyId, System.Int32? siteId)
		{
			return Reports_LeadingI_Company_Site(null, 0, int.MaxValue , fromDate, toDate, currentMonth, currentYear, companyId, siteId);
		}
		
		/// <summary>
		///	This method wrap the '_Kpi_Reports_LeadingI_Company_Site' stored procedure. 
		/// </summary>
		/// <param name="fromDate"> A <c>System.String</c> instance.</param>
		/// <param name="toDate"> A <c>System.String</c> instance.</param>
		/// <param name="currentMonth"> A <c>System.Int32?</c> instance.</param>
		/// <param name="currentYear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="siteId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Reports_LeadingI_Company_Site(int start, int pageLength, System.String fromDate, System.String toDate, System.Int32? currentMonth, System.Int32? currentYear, System.Int32? companyId, System.Int32? siteId)
		{
			return Reports_LeadingI_Company_Site(null, start, pageLength , fromDate, toDate, currentMonth, currentYear, companyId, siteId);
		}
				
		/// <summary>
		///	This method wrap the '_Kpi_Reports_LeadingI_Company_Site' stored procedure. 
		/// </summary>
		/// <param name="fromDate"> A <c>System.String</c> instance.</param>
		/// <param name="toDate"> A <c>System.String</c> instance.</param>
		/// <param name="currentMonth"> A <c>System.Int32?</c> instance.</param>
		/// <param name="currentYear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="siteId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Reports_LeadingI_Company_Site(TransactionManager transactionManager, System.String fromDate, System.String toDate, System.Int32? currentMonth, System.Int32? currentYear, System.Int32? companyId, System.Int32? siteId)
		{
			return Reports_LeadingI_Company_Site(transactionManager, 0, int.MaxValue , fromDate, toDate, currentMonth, currentYear, companyId, siteId);
		}
		
		/// <summary>
		///	This method wrap the '_Kpi_Reports_LeadingI_Company_Site' stored procedure. 
		/// </summary>
		/// <param name="fromDate"> A <c>System.String</c> instance.</param>
		/// <param name="toDate"> A <c>System.String</c> instance.</param>
		/// <param name="currentMonth"> A <c>System.Int32?</c> instance.</param>
		/// <param name="currentYear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="siteId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet Reports_LeadingI_Company_Site(TransactionManager transactionManager, int start, int pageLength , System.String fromDate, System.String toDate, System.Int32? currentMonth, System.Int32? currentYear, System.Int32? companyId, System.Int32? siteId);
		
		#endregion
		
		#region _Kpi_mmSummary 
		
		/// <summary>
		///	This method wrap the '_Kpi_mmSummary' stored procedure. 
		/// </summary>
		/// <param name="previousmonth"> A <c>System.Int32?</c> instance.</param>
		/// <param name="currentmonth"> A <c>System.Int32?</c> instance.</param>
		/// <param name="currentyear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="regionId"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet mmSummary(System.Int32? previousmonth, System.Int32? currentmonth, System.Int32? currentyear, System.Int32? regionId)
		{
			return mmSummary(null, 0, int.MaxValue , previousmonth, currentmonth, currentyear, regionId);
		}
		
		/// <summary>
		///	This method wrap the '_Kpi_mmSummary' stored procedure. 
		/// </summary>
		/// <param name="previousmonth"> A <c>System.Int32?</c> instance.</param>
		/// <param name="currentmonth"> A <c>System.Int32?</c> instance.</param>
		/// <param name="currentyear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="regionId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet mmSummary(int start, int pageLength, System.Int32? previousmonth, System.Int32? currentmonth, System.Int32? currentyear, System.Int32? regionId)
		{
			return mmSummary(null, start, pageLength , previousmonth, currentmonth, currentyear, regionId);
		}
				
		/// <summary>
		///	This method wrap the '_Kpi_mmSummary' stored procedure. 
		/// </summary>
		/// <param name="previousmonth"> A <c>System.Int32?</c> instance.</param>
		/// <param name="currentmonth"> A <c>System.Int32?</c> instance.</param>
		/// <param name="currentyear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="regionId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet mmSummary(TransactionManager transactionManager, System.Int32? previousmonth, System.Int32? currentmonth, System.Int32? currentyear, System.Int32? regionId)
		{
			return mmSummary(transactionManager, 0, int.MaxValue , previousmonth, currentmonth, currentyear, regionId);
		}
		
		/// <summary>
		///	This method wrap the '_Kpi_mmSummary' stored procedure. 
		/// </summary>
		/// <param name="previousmonth"> A <c>System.Int32?</c> instance.</param>
		/// <param name="currentmonth"> A <c>System.Int32?</c> instance.</param>
		/// <param name="currentyear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="regionId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet mmSummary(TransactionManager transactionManager, int start, int pageLength , System.Int32? previousmonth, System.Int32? currentmonth, System.Int32? currentyear, System.Int32? regionId);
		
		#endregion
		
		#region _Kpi_Reports_LeadingI_AllCompanies_AllMines 
		
		/// <summary>
		///	This method wrap the '_Kpi_Reports_LeadingI_AllCompanies_AllMines' stored procedure. 
		/// </summary>
		/// <param name="fromDate"> A <c>System.String</c> instance.</param>
		/// <param name="toDate"> A <c>System.String</c> instance.</param>
		/// <param name="currentMonth"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Reports_LeadingI_AllCompanies_AllMines(System.String fromDate, System.String toDate, System.Int32? currentMonth)
		{
			return Reports_LeadingI_AllCompanies_AllMines(null, 0, int.MaxValue , fromDate, toDate, currentMonth);
		}
		
		/// <summary>
		///	This method wrap the '_Kpi_Reports_LeadingI_AllCompanies_AllMines' stored procedure. 
		/// </summary>
		/// <param name="fromDate"> A <c>System.String</c> instance.</param>
		/// <param name="toDate"> A <c>System.String</c> instance.</param>
		/// <param name="currentMonth"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Reports_LeadingI_AllCompanies_AllMines(int start, int pageLength, System.String fromDate, System.String toDate, System.Int32? currentMonth)
		{
			return Reports_LeadingI_AllCompanies_AllMines(null, start, pageLength , fromDate, toDate, currentMonth);
		}
				
		/// <summary>
		///	This method wrap the '_Kpi_Reports_LeadingI_AllCompanies_AllMines' stored procedure. 
		/// </summary>
		/// <param name="fromDate"> A <c>System.String</c> instance.</param>
		/// <param name="toDate"> A <c>System.String</c> instance.</param>
		/// <param name="currentMonth"> A <c>System.Int32?</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Reports_LeadingI_AllCompanies_AllMines(TransactionManager transactionManager, System.String fromDate, System.String toDate, System.Int32? currentMonth)
		{
			return Reports_LeadingI_AllCompanies_AllMines(transactionManager, 0, int.MaxValue , fromDate, toDate, currentMonth);
		}
		
		/// <summary>
		///	This method wrap the '_Kpi_Reports_LeadingI_AllCompanies_AllMines' stored procedure. 
		/// </summary>
		/// <param name="fromDate"> A <c>System.String</c> instance.</param>
		/// <param name="toDate"> A <c>System.String</c> instance.</param>
		/// <param name="currentMonth"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet Reports_LeadingI_AllCompanies_AllMines(TransactionManager transactionManager, int start, int pageLength , System.String fromDate, System.String toDate, System.Int32? currentMonth);
		
		#endregion
		
		#region _Kpi_GetByCompanyId_Custom2 
		
		/// <summary>
		///	This method wrap the '_Kpi_GetByCompanyId_Custom2' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetByCompanyId_Custom2(System.Int32? companyId)
		{
			return GetByCompanyId_Custom2(null, 0, int.MaxValue , companyId);
		}
		
		/// <summary>
		///	This method wrap the '_Kpi_GetByCompanyId_Custom2' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetByCompanyId_Custom2(int start, int pageLength, System.Int32? companyId)
		{
			return GetByCompanyId_Custom2(null, start, pageLength , companyId);
		}
				
		/// <summary>
		///	This method wrap the '_Kpi_GetByCompanyId_Custom2' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetByCompanyId_Custom2(TransactionManager transactionManager, System.Int32? companyId)
		{
			return GetByCompanyId_Custom2(transactionManager, 0, int.MaxValue , companyId);
		}
		
		/// <summary>
		///	This method wrap the '_Kpi_GetByCompanyId_Custom2' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet GetByCompanyId_Custom2(TransactionManager transactionManager, int start, int pageLength , System.Int32? companyId);
		
		#endregion
		
		#region _Kpi_Reports_LeadingI_AllCompanies_Site 
		
		/// <summary>
		///	This method wrap the '_Kpi_Reports_LeadingI_AllCompanies_Site' stored procedure. 
		/// </summary>
		/// <param name="fromDate"> A <c>System.String</c> instance.</param>
		/// <param name="toDate"> A <c>System.String</c> instance.</param>
		/// <param name="currentMonth"> A <c>System.Int32?</c> instance.</param>
		/// <param name="siteId"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Reports_LeadingI_AllCompanies_Site(System.String fromDate, System.String toDate, System.Int32? currentMonth, System.Int32? siteId)
		{
			return Reports_LeadingI_AllCompanies_Site(null, 0, int.MaxValue , fromDate, toDate, currentMonth, siteId);
		}
		
		/// <summary>
		///	This method wrap the '_Kpi_Reports_LeadingI_AllCompanies_Site' stored procedure. 
		/// </summary>
		/// <param name="fromDate"> A <c>System.String</c> instance.</param>
		/// <param name="toDate"> A <c>System.String</c> instance.</param>
		/// <param name="currentMonth"> A <c>System.Int32?</c> instance.</param>
		/// <param name="siteId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Reports_LeadingI_AllCompanies_Site(int start, int pageLength, System.String fromDate, System.String toDate, System.Int32? currentMonth, System.Int32? siteId)
		{
			return Reports_LeadingI_AllCompanies_Site(null, start, pageLength , fromDate, toDate, currentMonth, siteId);
		}
				
		/// <summary>
		///	This method wrap the '_Kpi_Reports_LeadingI_AllCompanies_Site' stored procedure. 
		/// </summary>
		/// <param name="fromDate"> A <c>System.String</c> instance.</param>
		/// <param name="toDate"> A <c>System.String</c> instance.</param>
		/// <param name="currentMonth"> A <c>System.Int32?</c> instance.</param>
		/// <param name="siteId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Reports_LeadingI_AllCompanies_Site(TransactionManager transactionManager, System.String fromDate, System.String toDate, System.Int32? currentMonth, System.Int32? siteId)
		{
			return Reports_LeadingI_AllCompanies_Site(transactionManager, 0, int.MaxValue , fromDate, toDate, currentMonth, siteId);
		}
		
		/// <summary>
		///	This method wrap the '_Kpi_Reports_LeadingI_AllCompanies_Site' stored procedure. 
		/// </summary>
		/// <param name="fromDate"> A <c>System.String</c> instance.</param>
		/// <param name="toDate"> A <c>System.String</c> instance.</param>
		/// <param name="currentMonth"> A <c>System.Int32?</c> instance.</param>
		/// <param name="siteId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet Reports_LeadingI_AllCompanies_Site(TransactionManager transactionManager, int start, int pageLength , System.String fromDate, System.String toDate, System.Int32? currentMonth, System.Int32? siteId);
		
		#endregion
		
		#region _Kpi_GetAll_Last12MonthsHoursEntered 
		
		/// <summary>
		///	This method wrap the '_Kpi_GetAll_Last12MonthsHoursEntered' stored procedure. 
		/// </summary>
		/// <param name="currentMonth"> A <c>System.Int32?</c> instance.</param>
		/// <param name="currentYear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companySiteCategoryId"> A <c>System.String</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetAll_Last12MonthsHoursEntered(System.Int32? currentMonth, System.Int32? currentYear, System.String companySiteCategoryId)
		{
			return GetAll_Last12MonthsHoursEntered(null, 0, int.MaxValue , currentMonth, currentYear, companySiteCategoryId);
		}
		
		/// <summary>
		///	This method wrap the '_Kpi_GetAll_Last12MonthsHoursEntered' stored procedure. 
		/// </summary>
		/// <param name="currentMonth"> A <c>System.Int32?</c> instance.</param>
		/// <param name="currentYear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companySiteCategoryId"> A <c>System.String</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetAll_Last12MonthsHoursEntered(int start, int pageLength, System.Int32? currentMonth, System.Int32? currentYear, System.String companySiteCategoryId)
		{
			return GetAll_Last12MonthsHoursEntered(null, start, pageLength , currentMonth, currentYear, companySiteCategoryId);
		}
				
		/// <summary>
		///	This method wrap the '_Kpi_GetAll_Last12MonthsHoursEntered' stored procedure. 
		/// </summary>
		/// <param name="currentMonth"> A <c>System.Int32?</c> instance.</param>
		/// <param name="currentYear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companySiteCategoryId"> A <c>System.String</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetAll_Last12MonthsHoursEntered(TransactionManager transactionManager, System.Int32? currentMonth, System.Int32? currentYear, System.String companySiteCategoryId)
		{
			return GetAll_Last12MonthsHoursEntered(transactionManager, 0, int.MaxValue , currentMonth, currentYear, companySiteCategoryId);
		}
		
		/// <summary>
		///	This method wrap the '_Kpi_GetAll_Last12MonthsHoursEntered' stored procedure. 
		/// </summary>
		/// <param name="currentMonth"> A <c>System.Int32?</c> instance.</param>
		/// <param name="currentYear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companySiteCategoryId"> A <c>System.String</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet GetAll_Last12MonthsHoursEntered(TransactionManager transactionManager, int start, int pageLength , System.Int32? currentMonth, System.Int32? currentYear, System.String companySiteCategoryId);
		
		#endregion
		
		#region _Kpi_GetReport_ComplianceChart_S_A 
		
		/// <summary>
		///	This method wrap the '_Kpi_GetReport_ComplianceChart_S_A' stored procedure. 
		/// </summary>
		/// <param name="regionId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="fromDate"> A <c>System.String</c> instance.</param>
		/// <param name="toDate"> A <c>System.String</c> instance.</param>
		/// <param name="currentMonth"> A <c>System.Int32?</c> instance.</param>
		/// <param name="currentYear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="qtrId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="residentialStatus"> A <c>System.String</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetReport_ComplianceChart_S_A(System.Int32? regionId, System.Int32? companyId, System.String fromDate, System.String toDate, System.Int32? currentMonth, System.Int32? currentYear, System.Int32? qtrId, System.String residentialStatus)
		{
			return GetReport_ComplianceChart_S_A(null, 0, int.MaxValue , regionId, companyId, fromDate, toDate, currentMonth, currentYear, qtrId, residentialStatus);
		}
		
		/// <summary>
		///	This method wrap the '_Kpi_GetReport_ComplianceChart_S_A' stored procedure. 
		/// </summary>
		/// <param name="regionId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="fromDate"> A <c>System.String</c> instance.</param>
		/// <param name="toDate"> A <c>System.String</c> instance.</param>
		/// <param name="currentMonth"> A <c>System.Int32?</c> instance.</param>
		/// <param name="currentYear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="qtrId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="residentialStatus"> A <c>System.String</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetReport_ComplianceChart_S_A(int start, int pageLength, System.Int32? regionId, System.Int32? companyId, System.String fromDate, System.String toDate, System.Int32? currentMonth, System.Int32? currentYear, System.Int32? qtrId, System.String residentialStatus)
		{
			return GetReport_ComplianceChart_S_A(null, start, pageLength , regionId, companyId, fromDate, toDate, currentMonth, currentYear, qtrId, residentialStatus);
		}
				
		/// <summary>
		///	This method wrap the '_Kpi_GetReport_ComplianceChart_S_A' stored procedure. 
		/// </summary>
		/// <param name="regionId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="fromDate"> A <c>System.String</c> instance.</param>
		/// <param name="toDate"> A <c>System.String</c> instance.</param>
		/// <param name="currentMonth"> A <c>System.Int32?</c> instance.</param>
		/// <param name="currentYear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="qtrId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="residentialStatus"> A <c>System.String</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetReport_ComplianceChart_S_A(TransactionManager transactionManager, System.Int32? regionId, System.Int32? companyId, System.String fromDate, System.String toDate, System.Int32? currentMonth, System.Int32? currentYear, System.Int32? qtrId, System.String residentialStatus)
		{
			return GetReport_ComplianceChart_S_A(transactionManager, 0, int.MaxValue , regionId, companyId, fromDate, toDate, currentMonth, currentYear, qtrId, residentialStatus);
		}
		
		/// <summary>
		///	This method wrap the '_Kpi_GetReport_ComplianceChart_S_A' stored procedure. 
		/// </summary>
		/// <param name="regionId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="fromDate"> A <c>System.String</c> instance.</param>
		/// <param name="toDate"> A <c>System.String</c> instance.</param>
		/// <param name="currentMonth"> A <c>System.Int32?</c> instance.</param>
		/// <param name="currentYear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="qtrId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="residentialStatus"> A <c>System.String</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet GetReport_ComplianceChart_S_A(TransactionManager transactionManager, int start, int pageLength , System.Int32? regionId, System.Int32? companyId, System.String fromDate, System.String toDate, System.Int32? currentMonth, System.Int32? currentYear, System.Int32? qtrId, System.String residentialStatus);
		
		#endregion
		
		#region _Kpi_GetDistinctYears 
		
		/// <summary>
		///	This method wrap the '_Kpi_GetDistinctYears' stored procedure. 
		/// </summary>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetDistinctYears()
		{
			return GetDistinctYears(null, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_Kpi_GetDistinctYears' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetDistinctYears(int start, int pageLength)
		{
			return GetDistinctYears(null, start, pageLength );
		}
				
		/// <summary>
		///	This method wrap the '_Kpi_GetDistinctYears' stored procedure. 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetDistinctYears(TransactionManager transactionManager)
		{
			return GetDistinctYears(transactionManager, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_Kpi_GetDistinctYears' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet GetDistinctYears(TransactionManager transactionManager, int start, int pageLength );
		
		#endregion
		
		#region _Kpi_Reports_LeadingI_Company_WAO 
		
		/// <summary>
		///	This method wrap the '_Kpi_Reports_LeadingI_Company_WAO' stored procedure. 
		/// </summary>
		/// <param name="fromDate"> A <c>System.String</c> instance.</param>
		/// <param name="toDate"> A <c>System.String</c> instance.</param>
		/// <param name="currentMonth"> A <c>System.Int32?</c> instance.</param>
		/// <param name="currentYear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Reports_LeadingI_Company_WAO(System.String fromDate, System.String toDate, System.Int32? currentMonth, System.Int32? currentYear, System.Int32? companyId)
		{
			return Reports_LeadingI_Company_WAO(null, 0, int.MaxValue , fromDate, toDate, currentMonth, currentYear, companyId);
		}
		
		/// <summary>
		///	This method wrap the '_Kpi_Reports_LeadingI_Company_WAO' stored procedure. 
		/// </summary>
		/// <param name="fromDate"> A <c>System.String</c> instance.</param>
		/// <param name="toDate"> A <c>System.String</c> instance.</param>
		/// <param name="currentMonth"> A <c>System.Int32?</c> instance.</param>
		/// <param name="currentYear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Reports_LeadingI_Company_WAO(int start, int pageLength, System.String fromDate, System.String toDate, System.Int32? currentMonth, System.Int32? currentYear, System.Int32? companyId)
		{
			return Reports_LeadingI_Company_WAO(null, start, pageLength , fromDate, toDate, currentMonth, currentYear, companyId);
		}
				
		/// <summary>
		///	This method wrap the '_Kpi_Reports_LeadingI_Company_WAO' stored procedure. 
		/// </summary>
		/// <param name="fromDate"> A <c>System.String</c> instance.</param>
		/// <param name="toDate"> A <c>System.String</c> instance.</param>
		/// <param name="currentMonth"> A <c>System.Int32?</c> instance.</param>
		/// <param name="currentYear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Reports_LeadingI_Company_WAO(TransactionManager transactionManager, System.String fromDate, System.String toDate, System.Int32? currentMonth, System.Int32? currentYear, System.Int32? companyId)
		{
			return Reports_LeadingI_Company_WAO(transactionManager, 0, int.MaxValue , fromDate, toDate, currentMonth, currentYear, companyId);
		}
		
		/// <summary>
		///	This method wrap the '_Kpi_Reports_LeadingI_Company_WAO' stored procedure. 
		/// </summary>
		/// <param name="fromDate"> A <c>System.String</c> instance.</param>
		/// <param name="toDate"> A <c>System.String</c> instance.</param>
		/// <param name="currentMonth"> A <c>System.Int32?</c> instance.</param>
		/// <param name="currentYear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet Reports_LeadingI_Company_WAO(TransactionManager transactionManager, int start, int pageLength , System.String fromDate, System.String toDate, System.Int32? currentMonth, System.Int32? currentYear, System.Int32? companyId);
		
		#endregion
		
		#region _Kpi_Reports_CustomReport 
		
		/// <summary>
		///	This method wrap the '_Kpi_Reports_CustomReport' stored procedure. 
		/// </summary>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Reports_CustomReport()
		{
			return Reports_CustomReport(null, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_Kpi_Reports_CustomReport' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Reports_CustomReport(int start, int pageLength)
		{
			return Reports_CustomReport(null, start, pageLength );
		}
				
		/// <summary>
		///	This method wrap the '_Kpi_Reports_CustomReport' stored procedure. 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Reports_CustomReport(TransactionManager transactionManager)
		{
			return Reports_CustomReport(transactionManager, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_Kpi_Reports_CustomReport' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet Reports_CustomReport(TransactionManager transactionManager, int start, int pageLength );
		
		#endregion
		
		#region _Kpi_ComplianceReport_ByCompany 
		
		/// <summary>
		///	This method wrap the '_Kpi_ComplianceReport_ByCompany' stored procedure. 
		/// </summary>
		/// <param name="sYear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companyid"> A <c>System.Int32?</c> instance.</param>
		/// <param name="siteId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companySiteCategoryId"> A <c>System.String</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet ComplianceReport_ByCompany(System.Int32? sYear, System.Int32? companyid, System.Int32? siteId, System.String companySiteCategoryId)
		{
			return ComplianceReport_ByCompany(null, 0, int.MaxValue , sYear, companyid, siteId, companySiteCategoryId);
		}
		
		/// <summary>
		///	This method wrap the '_Kpi_ComplianceReport_ByCompany' stored procedure. 
		/// </summary>
		/// <param name="sYear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companyid"> A <c>System.Int32?</c> instance.</param>
		/// <param name="siteId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companySiteCategoryId"> A <c>System.String</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet ComplianceReport_ByCompany(int start, int pageLength, System.Int32? sYear, System.Int32? companyid, System.Int32? siteId, System.String companySiteCategoryId)
		{
			return ComplianceReport_ByCompany(null, start, pageLength , sYear, companyid, siteId, companySiteCategoryId);
		}
				
		/// <summary>
		///	This method wrap the '_Kpi_ComplianceReport_ByCompany' stored procedure. 
		/// </summary>
		/// <param name="sYear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companyid"> A <c>System.Int32?</c> instance.</param>
		/// <param name="siteId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companySiteCategoryId"> A <c>System.String</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet ComplianceReport_ByCompany(TransactionManager transactionManager, System.Int32? sYear, System.Int32? companyid, System.Int32? siteId, System.String companySiteCategoryId)
		{
			return ComplianceReport_ByCompany(transactionManager, 0, int.MaxValue , sYear, companyid, siteId, companySiteCategoryId);
		}
		
		/// <summary>
		///	This method wrap the '_Kpi_ComplianceReport_ByCompany' stored procedure. 
		/// </summary>
		/// <param name="sYear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companyid"> A <c>System.Int32?</c> instance.</param>
		/// <param name="siteId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companySiteCategoryId"> A <c>System.String</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet ComplianceReport_ByCompany(TransactionManager transactionManager, int start, int pageLength , System.Int32? sYear, System.Int32? companyid, System.Int32? siteId, System.String companySiteCategoryId);
		
		#endregion
		
		#region _Kpi_GetByCompanyId_Custom 
		
		/// <summary>
		///	This method wrap the '_Kpi_GetByCompanyId_Custom' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetByCompanyId_Custom(System.Int32? companyId)
		{
			return GetByCompanyId_Custom(null, 0, int.MaxValue , companyId);
		}
		
		/// <summary>
		///	This method wrap the '_Kpi_GetByCompanyId_Custom' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetByCompanyId_Custom(int start, int pageLength, System.Int32? companyId)
		{
			return GetByCompanyId_Custom(null, start, pageLength , companyId);
		}
				
		/// <summary>
		///	This method wrap the '_Kpi_GetByCompanyId_Custom' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetByCompanyId_Custom(TransactionManager transactionManager, System.Int32? companyId)
		{
			return GetByCompanyId_Custom(transactionManager, 0, int.MaxValue , companyId);
		}
		
		/// <summary>
		///	This method wrap the '_Kpi_GetByCompanyId_Custom' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet GetByCompanyId_Custom(TransactionManager transactionManager, int start, int pageLength , System.Int32? companyId);
		
		#endregion
		
		#region _Kpi_GetComparisonReportDataGroupedByMonthCompanySiteByYearMonth 
		
		/// <summary>
		///	This method wrap the '_Kpi_GetComparisonReportDataGroupedByMonthCompanySiteByYearMonth' stored procedure. 
		/// </summary>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="month"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetComparisonReportDataGroupedByMonthCompanySiteByYearMonth(System.Int32? year, System.Int32? month)
		{
			return GetComparisonReportDataGroupedByMonthCompanySiteByYearMonth(null, 0, int.MaxValue , year, month);
		}
		
		/// <summary>
		///	This method wrap the '_Kpi_GetComparisonReportDataGroupedByMonthCompanySiteByYearMonth' stored procedure. 
		/// </summary>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="month"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetComparisonReportDataGroupedByMonthCompanySiteByYearMonth(int start, int pageLength, System.Int32? year, System.Int32? month)
		{
			return GetComparisonReportDataGroupedByMonthCompanySiteByYearMonth(null, start, pageLength , year, month);
		}
				
		/// <summary>
		///	This method wrap the '_Kpi_GetComparisonReportDataGroupedByMonthCompanySiteByYearMonth' stored procedure. 
		/// </summary>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="month"> A <c>System.Int32?</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetComparisonReportDataGroupedByMonthCompanySiteByYearMonth(TransactionManager transactionManager, System.Int32? year, System.Int32? month)
		{
			return GetComparisonReportDataGroupedByMonthCompanySiteByYearMonth(transactionManager, 0, int.MaxValue , year, month);
		}
		
		/// <summary>
		///	This method wrap the '_Kpi_GetComparisonReportDataGroupedByMonthCompanySiteByYearMonth' stored procedure. 
		/// </summary>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="month"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet GetComparisonReportDataGroupedByMonthCompanySiteByYearMonth(TransactionManager transactionManager, int start, int pageLength , System.Int32? year, System.Int32? month);
		
		#endregion
		
		#region _Kpi_SafetyFrequencyRatesReport_ByCompany 
		
		/// <summary>
		///	This method wrap the '_Kpi_SafetyFrequencyRatesReport_ByCompany' stored procedure. 
		/// </summary>
		/// <param name="sYear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="siteId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companySiteCategoryId2"> A <c>System.String</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet SafetyFrequencyRatesReport_ByCompany(System.Int32? sYear, System.Int32? companyId, System.Int32? siteId, System.String companySiteCategoryId2)
		{
			return SafetyFrequencyRatesReport_ByCompany(null, 0, int.MaxValue , sYear, companyId, siteId, companySiteCategoryId2);
		}
		
		/// <summary>
		///	This method wrap the '_Kpi_SafetyFrequencyRatesReport_ByCompany' stored procedure. 
		/// </summary>
		/// <param name="sYear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="siteId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companySiteCategoryId2"> A <c>System.String</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet SafetyFrequencyRatesReport_ByCompany(int start, int pageLength, System.Int32? sYear, System.Int32? companyId, System.Int32? siteId, System.String companySiteCategoryId2)
		{
			return SafetyFrequencyRatesReport_ByCompany(null, start, pageLength , sYear, companyId, siteId, companySiteCategoryId2);
		}
				
		/// <summary>
		///	This method wrap the '_Kpi_SafetyFrequencyRatesReport_ByCompany' stored procedure. 
		/// </summary>
		/// <param name="sYear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="siteId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companySiteCategoryId2"> A <c>System.String</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet SafetyFrequencyRatesReport_ByCompany(TransactionManager transactionManager, System.Int32? sYear, System.Int32? companyId, System.Int32? siteId, System.String companySiteCategoryId2)
		{
			return SafetyFrequencyRatesReport_ByCompany(transactionManager, 0, int.MaxValue , sYear, companyId, siteId, companySiteCategoryId2);
		}
		
		/// <summary>
		///	This method wrap the '_Kpi_SafetyFrequencyRatesReport_ByCompany' stored procedure. 
		/// </summary>
		/// <param name="sYear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="siteId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companySiteCategoryId2"> A <c>System.String</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet SafetyFrequencyRatesReport_ByCompany(TransactionManager transactionManager, int start, int pageLength , System.Int32? sYear, System.Int32? companyId, System.Int32? siteId, System.String companySiteCategoryId2);
		
		#endregion

        //Added by Sayani Sil for Item# 16
        #region SafetyRecognitionProgram

        #region Tab1

        public DataSet SafetyRecognitionProgram(System.Int32? siteId, System.Int32? sYear)
        {
            return SafetyRecognitionProgram(null, 0, int.MaxValue, siteId, sYear);
        }
        public abstract DataSet SafetyRecognitionProgram(TransactionManager transactionManager, int start, int pageLength, System.Int32? sYear, System.Int32? siteId);


        public DataSet Compliance_Table(System.Int32? siteId, System.Int32? sYear, System.String companySiteCategoryId)
        {
            return Compliance_Table(null, 0, int.MaxValue, siteId, sYear, companySiteCategoryId);
        }
        public abstract DataSet Compliance_Table(TransactionManager transactionManager, int start, int pageLength, System.Int32? siteId, System.Int32? sYear, System.String companySiteCategoryId);

        #endregion

        #region Tab2

        public DataSet SafetyRecognitionProgram_InjuryFree(System.Int32? siteId, System.Int32? sYear)
        {
            return SafetyRecognitionProgram_InjuryFree(null, 0, int.MaxValue, siteId, sYear);
        }
        public abstract DataSet SafetyRecognitionProgram_InjuryFree(TransactionManager transactionManager, int start, int pageLength, System.Int32? sYear, System.Int32? siteId);

        #endregion

        #endregion
        //End

        //Added By Sayani Sil for Item# 21
        #region Dashboard popups

        public DataSet KpiNonCompliancePopups(System.Int32? siteid, System.Int32? year, System.Int32 month)
        {
            return KpiNonCompliancePopups(null, 0, int.MaxValue, siteid, year, month);
        }
        public abstract DataSet KpiNonCompliancePopups(TransactionManager transactionManager, int start, int pageLength, System.Int32? siteid, System.Int32? year, System.Int32 month);
        //-///
        public DataSet CurrentlyWithProcurement(System.Int32? siteid, System.Int32? year, System.Int32 month)
        {
            return CurrentlyWithProcurement(null, 0, int.MaxValue, siteid, year, month);
        }
        public abstract DataSet CurrentlyWithProcurement(TransactionManager transactionManager, int start, int pageLength, System.Int32? siteid, System.Int32? year, System.Int32 month);

        //-///
        public DataSet CurrentlyWithSupplier(System.Int32? siteid, System.Int32? year, System.Int32 month)
        {
            return CurrentlyWithSupplier(null, 0, int.MaxValue, siteid, year, month);
        }
        public abstract DataSet CurrentlyWithSupplier(TransactionManager transactionManager, int start, int pageLength, System.Int32? siteid, System.Int32? year, System.Int32 month);

        //-///

        public DataSet ExpiringGT7Days(System.Int32? siteid, System.Int32? year, System.Int32 month)
        {
            return ExpiringGT7Days(null, 0, int.MaxValue, siteid, year, month);
        }
        public abstract DataSet ExpiringGT7Days(TransactionManager transactionManager, int start, int pageLength, System.Int32? siteid, System.Int32? year, System.Int32 month);

        //-///

        public DataSet QuestionnaireGT7Days(System.Int32? siteid, System.Int32? year, System.Int32 month)
        {
            return QuestionnaireGT7Days(null, 0, int.MaxValue, siteid, year, month);
        }
        public abstract DataSet QuestionnaireGT7Days(TransactionManager transactionManager, int start, int pageLength, System.Int32? siteid, System.Int32? year, System.Int32 month);

        //-///

        public DataSet AssignCompanyStatusGT7Day(System.Int32? siteid, System.Int32? year, System.Int32 month)
        {
            return AssignCompanyStatusGT7Day(null, 0, int.MaxValue, siteid, year, month);
        }
        public abstract DataSet AssignCompanyStatusGT7Day(TransactionManager transactionManager, int start, int pageLength, System.Int32? siteid, System.Int32? year, System.Int32 month);

        //-///

        public DataSet QuestionnaireGT28Days(System.Int32? siteid, System.Int32? year, System.Int32 month)
        {
            return QuestionnaireGT28Days(null, 0, int.MaxValue, siteid, year, month);
        }
        public abstract DataSet QuestionnaireGT28Days(TransactionManager transactionManager, int start, int pageLength, System.Int32? siteid, System.Int32? year, System.Int32 month);

        //-///

        public DataSet Questionnaire_Resubmitting(System.Int32? siteid, System.Int32? year, System.Int32 month)
        {
            return Questionnaire_Resubmitting(null, 0, int.MaxValue, siteid, year, month);
        }
        public abstract DataSet Questionnaire_Resubmitting(TransactionManager transactionManager, int start, int pageLength, System.Int32? siteid, System.Int32? year, System.Int32 month);


        //-///

        public DataSet AccessGT7Days(System.Int32? siteid, System.Int32? year, System.Int32 month)
        {
            return AccessGT7Days(null, 0, int.MaxValue, siteid, year, month);
        }
        public abstract DataSet AccessGT7Days(TransactionManager transactionManager, int start, int pageLength, System.Int32? siteid, System.Int32? year, System.Int32 month);


        //-///

        public DataSet CurrentlyWithHSAssessor(System.Int32? siteid, System.Int32? year, System.Int32 month)
        {
            return CurrentlyWithHSAssessor(null, 0, int.MaxValue, siteid, year, month);
        }
        public abstract DataSet CurrentlyWithHSAssessor(TransactionManager transactionManager, int start, int pageLength, System.Int32? siteid, System.Int32? year, System.Int32 month);



        //-///

        public DataSet AssessingProcurementGT7Days(System.Int32? siteid, System.Int32? year, System.Int32 month)
        {
            return AssessingProcurementGT7Days(null, 0, int.MaxValue, siteid, year, month);
        }
        public abstract DataSet AssessingProcurementGT7Days(TransactionManager transactionManager, int start, int pageLength, System.Int32? siteid, System.Int32? year, System.Int32 month);


        //-///

        public DataSet AssessingGT7Days(System.Int32? siteid, System.Int32? year, System.Int32 month)
        {
            return AssessingGT7Days(null, 0, int.MaxValue, siteid, year, month);
        }
        public abstract DataSet AssessingGT7Days(TransactionManager transactionManager, int start, int pageLength, System.Int32? siteid, System.Int32? year, System.Int32 month);

        //-///

        public DataSet SQProcessTotal(System.Int32? siteid, System.Int32? year, System.Int32 month)
        {
            return SQProcessTotal(null, 0, int.MaxValue, siteid, year, month);
        }
        public abstract DataSet SQProcessTotal(TransactionManager transactionManager, int start, int pageLength, System.Int32? siteid, System.Int32? year, System.Int32 month);

        //-///

        public DataSet Yes_sqCurrent(System.Int32? siteid, System.Int32? year, System.Int32 month)
        {
            return Yes_sqCurrent(null, 0, int.MaxValue, siteid, year, month);
        }
        public abstract DataSet Yes_sqCurrent(TransactionManager transactionManager, int start, int pageLength, System.Int32? siteid, System.Int32? year, System.Int32 month);

        //-///

        public DataSet NO_CurrentSQNotFound(System.Int32? siteid, System.Int32? year, System.Int32 month)
        {
            return NO_CurrentSQNotFound(null, 0, int.MaxValue, siteid, year, month);
        }
        public abstract DataSet NO_CurrentSQNotFound(TransactionManager transactionManager, int start, int pageLength, System.Int32? siteid, System.Int32? year, System.Int32 month);

        //-///

        public DataSet No_SqExpired(System.Int32? siteid, System.Int32? year, System.Int32 month)
        {
            return No_SqExpired(null, 0, int.MaxValue, siteid, year, month);
        }
        public abstract DataSet No_SqExpired(TransactionManager transactionManager, int start, int pageLength, System.Int32? siteid, System.Int32? year, System.Int32 month);

        //-///

        public DataSet No_AccessSiteNotGranted(System.Int32? siteid, System.Int32? year, System.Int32 month)
        {
            return No_AccessSiteNotGranted(null, 0, int.MaxValue, siteid, year, month);
        }
        public abstract DataSet No_AccessSiteNotGranted(TransactionManager transactionManager, int start, int pageLength, System.Int32? siteid, System.Int32? year, System.Int32 month);


        //-///

        public DataSet No_SafetyQualificationNotFound(System.Int32? siteid, System.Int32? year, System.Int32 month)
        {
            return No_SafetyQualificationNotFound(null, 0, int.MaxValue, siteid, year, month);
        }
        public abstract DataSet No_SafetyQualificationNotFound(TransactionManager transactionManager, int start, int pageLength, System.Int32? siteid, System.Int32? year, System.Int32 month);

        //-///

        public DataSet InformationNotFound(System.Int32? siteid, System.Int32? year, System.Int32 month)
        {
            return InformationNotFound(null, 0, int.MaxValue, siteid, year, month);
        }
        public abstract DataSet InformationNotFound(TransactionManager transactionManager, int start, int pageLength, System.Int32? siteid, System.Int32? year, System.Int32 month);

        //-///

        public DataSet ValidSQExemption(System.Int32? siteid, System.Int32? year, System.Int32 month)
        {
            return ValidSQExemption(null, 0, int.MaxValue, siteid, year, month);
        }
        public abstract DataSet ValidSQExemption(TransactionManager transactionManager, int start, int pageLength, System.Int32? siteid, System.Int32? year, System.Int32 month);

        //-///

        public DataSet HSAssessors_Required(System.Int32? siteid, System.Int32? year, System.Int32 month)
        {
            return HSAssessors_Required(null, 0, int.MaxValue, siteid, year, month);
        }
        public abstract DataSet HSAssessors_Required(TransactionManager transactionManager, int start, int pageLength, System.Int32? siteid, System.Int32? year, System.Int32 month);


        //-///

        public DataSet HSAssessors_BeingAssessed(System.Int32? siteid, System.Int32? year, System.Int32 month)
        {
            return HSAssessors_BeingAssessed(null, 0, int.MaxValue, siteid, year, month);
        }
        public abstract DataSet HSAssessors_BeingAssessed(TransactionManager transactionManager, int start, int pageLength, System.Int32? siteid, System.Int32? year, System.Int32 month);


        //-///

        public DataSet HSAssessors_AwaitingAssignment(System.Int32? siteid, System.Int32? year, System.Int32 month)
        {
            return HSAssessors_AwaitingAssignment(null, 0, int.MaxValue, siteid, year, month);
        }
        public abstract DataSet HSAssessors_AwaitingAssignment(TransactionManager transactionManager, int start, int pageLength, System.Int32? siteid, System.Int32? year, System.Int32 month);

        //-///

        public DataSet HSAssessors_Approved(System.Int32? siteid, System.Int32? year, System.Int32 month)
        {
            return HSAssessors_Approved(null, 0, int.MaxValue, siteid, year, month);
        }
        public abstract DataSet HSAssessors_Approved(TransactionManager transactionManager, int start, int pageLength, System.Int32? siteid, System.Int32? year, System.Int32 month);

        //-///

        public DataSet HSAssessors_ApprovedPercentage(System.Int32? siteid, System.Int32? year, System.Int32 month)
        {
            return HSAssessors_ApprovedPercentage(null, 0, int.MaxValue, siteid, year, month);
        }
        public abstract DataSet HSAssessors_ApprovedPercentage(TransactionManager transactionManager, int start, int pageLength, System.Int32? siteid, System.Int32? year, System.Int32 month);

        //-///

        public DataSet _CSA_Emb_ReqToDate(System.Int32? siteid, System.Int32? year, System.Int32 month)
        {
            return _CSA_Emb_ReqToDate(null, 0, int.MaxValue, siteid, year, month);
        }
        public abstract DataSet _CSA_Emb_ReqToDate(TransactionManager transactionManager, int start, int pageLength, System.Int32? siteid, System.Int32? year, System.Int32 month);

        //-///

        public DataSet _CSA_NonEmb_ReqToDate(System.Int32? siteid, System.Int32? year, System.Int32 month)
        {
            return _CSA_NonEmb_ReqToDate(null, 0, int.MaxValue, siteid, year, month);
        }
        public abstract DataSet _CSA_NonEmb_ReqToDate(TransactionManager transactionManager, int start, int pageLength, System.Int32? siteid, System.Int32? year, System.Int32 month);

        //-///

        public DataSet _CSA_Emb_Actual(System.Int32? siteid, System.Int32? year, System.Int32 month)
        {
            return _CSA_Emb_Actual(null, 0, int.MaxValue, siteid, year, month);
        }
        public abstract DataSet _CSA_Emb_Actual(TransactionManager transactionManager, int start, int pageLength, System.Int32? siteid, System.Int32? year, System.Int32 month);

        //-///

        public DataSet _CSA_NonEmb_Actual(System.Int32? siteid, System.Int32? year, System.Int32 month)
        {
            return _CSA_NonEmb_Actual(null, 0, int.MaxValue, siteid, year, month);
        }
        public abstract DataSet _CSA_NonEmb_Actual(TransactionManager transactionManager, int start, int pageLength, System.Int32? siteid, System.Int32? year, System.Int32 month);

        //-///
        //Embeded kpi
        //-///


        public DataSet Embeded_Non_Compliant(System.Int32? siteid, System.Int32? year, System.Int32 month)
        {
            return Embeded_Non_Compliant(null, 0, int.MaxValue, siteid, year, month);
        }
        public abstract DataSet Embeded_Non_Compliant(TransactionManager transactionManager, int start, int pageLength, System.Int32? siteid, System.Int32? year, System.Int32 month);

        //-///

        public DataSet Embeded_Non_Compliant_CSA(System.Int32? siteid, System.Int32? year, System.Int32 month)
        {
            return Embeded_Non_Compliant_CSA(null, 0, int.MaxValue, siteid, year, month);
        }
        public abstract DataSet Embeded_Non_Compliant_CSA(TransactionManager transactionManager, int start, int pageLength, System.Int32? siteid, System.Int32? year, System.Int32 month);

        //-///

        public DataSet Embeded_Non_Compliant_SafetyPlanSubmitted(System.Int32? siteid, System.Int32? year, System.Int32 month)
        {
            return Embeded_Non_Compliant_SafetyPlanSubmitted(null, 0, int.MaxValue, siteid, year, month);
        }
        public abstract DataSet Embeded_Non_Compliant_SafetyPlanSubmitted(TransactionManager transactionManager, int start, int pageLength, System.Int32? siteid, System.Int32? year, System.Int32 month);

        //-///
        // Non Embeded kpi
        //-///

        public DataSet NonEmbeded_Non_Compliant_CSA(System.Int32? siteid, System.Int32? year, System.Int32 month)
        {
            return NonEmbeded_Non_Compliant_CSA(null, 0, int.MaxValue, siteid, year, month);
        }
        public abstract DataSet NonEmbeded_Non_Compliant_CSA(TransactionManager transactionManager, int start, int pageLength, System.Int32? siteid, System.Int32? year, System.Int32 month);

        //-///

        public DataSet NonEmbeded_Non_Compliant(System.Int32? siteid, System.Int32? year, System.Int32 month)
        {
            return NonEmbeded_Non_Compliant(null, 0, int.MaxValue, siteid, year, month);
        }
        public abstract DataSet NonEmbeded_Non_Compliant(TransactionManager transactionManager, int start, int pageLength, System.Int32? siteid, System.Int32? year, System.Int32 month);



        #endregion
        #region New Dash Popups
        public DataSet Get_NonCompliant_Companies(System.Int32? siteid, System.Int32? year, System.Int32 month, string ItemName)
        {
            return Get_NonCompliant_Companies(null, 0, int.MaxValue, siteid, year, month, ItemName);
        }
        public abstract DataSet Get_NonCompliant_Companies(TransactionManager transactionManager, int start, int pageLength
            , System.Int32? siteid, System.Int32? year, System.Int32 month, string ItemName);

        #endregion

        #region Dashboard

        #region kpi

        public DataSet KpiComplianceDashboard(System.Int32? year, System.Int32 month)
        {
            return KpiComplianceDashboard(null, 0, int.MaxValue, year, month);
        }
        public abstract DataSet KpiComplianceDashboard(TransactionManager transactionManager, int start, int pageLength, System.Int32? year, System.Int32 month);

        #endregion

        #region Safety Qualification


        public DataSet SafetyQualificationDashboard(System.Int32? year, System.Int32 month)
        {
            return SafetyQualificationDashboard(null, 0, int.MaxValue, year, month);
        }
        public abstract DataSet SafetyQualificationDashboard(TransactionManager transactionManager, int start, int pageLength, System.Int32? year, System.Int32 month);


        #endregion


        #region Ebi Data


        public DataSet EbiDashboard(System.Int32? year, System.Int32 month)
        {
            return EbiDashboard(null, 0, int.MaxValue, year, month);
        }
        public abstract DataSet EbiDashboard(TransactionManager transactionManager, int start, int pageLength, System.Int32? year, System.Int32 month);


        #endregion

        #region SMP Data


        public DataSet SMPDashboard(System.Int32? year, System.Int32 month)
        {
            return SMPDashboard(null, 0, int.MaxValue, year, month);
        }
        public abstract DataSet SMPDashboard(TransactionManager transactionManager, int start, int pageLength, System.Int32? year, System.Int32 month);


        #endregion

        #region Embeded Compliance

        public DataSet EmbededComplianceDashboard(System.Int32? year, System.Int32 month)
        {
            return EmbededComplianceDashboard(null, 0, int.MaxValue, year, month);
        }
        public abstract DataSet EmbededComplianceDashboard(TransactionManager transactionManager, int start, int pageLength, System.Int32? year, System.Int32 month);

        #endregion

        #region NonEmbeded Compliance

        public DataSet NonEmbededComplianceDashboard(System.Int32? year, System.Int32 month)
        {
            return NonEmbededComplianceDashboard(null, 0, int.MaxValue, year, month);
        }
        public abstract DataSet NonEmbededComplianceDashboard(TransactionManager transactionManager, int start, int pageLength, System.Int32? year, System.Int32 month);

        #endregion

        #region CSA Data

        public DataSet CSADashBoard(System.Int32? year, System.Int32 month)
        {
            return CSADashBoard(null, 0, int.MaxValue, year, month);
        }
        public abstract DataSet CSADashBoard(TransactionManager transactionManager, int start, int pageLength, System.Int32? year, System.Int32 month);

        #endregion

        public DataSet AuditScoreDashBoard(System.Int32? year)
        {
            return AuditScoreDashBoard(null, 0, int.MaxValue, year);
        }
        public abstract DataSet AuditScoreDashBoard(TransactionManager transactionManager, int start, int pageLength, System.Int32? year);





        #region Text

        public DataSet Text()
        {
            return Text(null, 0, int.MaxValue);
        }
        public abstract DataSet Text(TransactionManager transactionManager, int start, int pageLength);

        #endregion


        #region Plan

        public DataSet Plan(System.Int32? year, System.Int32? month)
        {
            return Plan(null, 0, int.MaxValue, year, month);
        }
        public abstract DataSet Plan(TransactionManager transactionManager, int start, int pageLength, System.Int32? year, System.Int32? month);



        public int SetPlanValue(System.Int32? DashboardPlanId, System.Int32? DashboardId,
          string Operator, string PlanValue, string PlanText, System.Int32? year, System.Int32? month)
        {
            return SetPlanValue(null, 0, int.MaxValue,
                DashboardPlanId, DashboardId, Operator, PlanValue, PlanText, year, month);
        }
        public abstract int SetPlanValue(TransactionManager transactionManager, int start, int pageLength,
            System.Int32? DashboardPlanId, System.Int32? DashboardId,
          string Operator, string PlanValue, string PlanText, System.Int32? year, System.Int32? month);

        public DataSet getSet(System.Int32? key)
        {
            return getSet(null, 0, int.MaxValue, key);
        }
        public abstract DataSet getSet(TransactionManager transactionManager, int start, int pageLength, System.Int32? key);


        public DataSet CheckEntry(System.Int32? SiteId, System.Int32? Year)
        {
            return CheckEntry(null, 0, int.MaxValue, SiteId, Year);
        }
        public abstract DataSet CheckEntry(TransactionManager transactionManager, int start, int pageLength, System.Int32? SiteId, System.Int32? Year);

        public int UpdateGrid(System.Int32? SiteId, System.Int32? Year, System.Int32? AuditScoreId, string Description)
        {
            return UpdateGrid(null, 0, int.MaxValue, SiteId, Year, AuditScoreId, Description);
        }
        public abstract int UpdateGrid(TransactionManager transactionManager, int start, int pageLength, System.Int32? SiteId, System.Int32? Year, System.Int32? AuditScoreId, string Description);

        public int InsertGrid(System.Int32? SiteId, System.Int32? Year, string Description)
        {
            return InsertGrid(null, 0, int.MaxValue, SiteId, Year, Description);
        }
        public abstract int InsertGrid(TransactionManager transactionManager, int start, int pageLength, System.Int32? SiteId, System.Int32? Year, string Description);


        #endregion

        #endregion

        //END
        //for Item# 19
        #region ARP CRP
        public DataSet Get_CRP(System.Int32? CompanyId)
        {
            return Get_CRP(null, 0, int.MaxValue, CompanyId);
        }
        public abstract DataSet Get_CRP(TransactionManager transactionManager, int start, int pageLength, System.Int32? CompanyId);


        public DataSet Get_ARP(System.Int32? CompanyId, System.Int32? SiteId)
        {
            return Get_ARP(null, 0, int.MaxValue, CompanyId, SiteId);
        }
        public abstract DataSet Get_ARP(TransactionManager transactionManager, int start, int pageLength, System.Int32? CompanyId, System.Int32? SiteId);


        public DataSet ViewARPListing(System.Int32? SiteId)
        {
            return ViewARPListing(null, 0, int.MaxValue, SiteId);
        }
        public abstract DataSet ViewARPListing(TransactionManager transactionManager, int start, int pageLength, System.Int32? SiteId);

        #endregion


        #region _Kpi_ResetEhsimsValues

        /// <summary>
		///	This method wrap the '_Kpi_ResetEhsimsValues' stored procedure. 
		/// </summary>
		/// <remark>This method is generate from a stored procedure.</remark>
		public void ResetEhsimsValues()
		{
			 ResetEhsimsValues(null, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_Kpi_ResetEhsimsValues' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		public void ResetEhsimsValues(int start, int pageLength)
		{
			 ResetEhsimsValues(null, start, pageLength );
		}
				
		/// <summary>
		///	This method wrap the '_Kpi_ResetEhsimsValues' stored procedure. 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		public void ResetEhsimsValues(TransactionManager transactionManager)
		{
			 ResetEhsimsValues(transactionManager, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_Kpi_ResetEhsimsValues' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		public abstract void ResetEhsimsValues(TransactionManager transactionManager, int start, int pageLength );
		
		#endregion
		
#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;Kpi&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;Kpi&gt;"/></returns>
		public static TList<Kpi> Fill(IDataReader reader, TList<Kpi> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.Kpi c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("Kpi")
					.Append("|").Append((System.Int32)reader[((int)KpiColumn.KpiId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<Kpi>(
					key.ToString(), // EntityTrackingKey
					"Kpi",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.Kpi();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.KpiId = (System.Int32)reader[((int)KpiColumn.KpiId - 1)];
					c.SiteId = (System.Int32)reader[((int)KpiColumn.SiteId - 1)];
					c.CreatedbyUserId = (System.Int32)reader[((int)KpiColumn.CreatedbyUserId - 1)];
					c.ModifiedbyUserId = (System.Int32)reader[((int)KpiColumn.ModifiedbyUserId - 1)];
					c.DateAdded = (System.DateTime)reader[((int)KpiColumn.DateAdded - 1)];
					c.Datemodified = (System.DateTime)reader[((int)KpiColumn.Datemodified - 1)];
					c.CompanyId = (System.Int32)reader[((int)KpiColumn.CompanyId - 1)];
					c.KpiDateTime = (System.DateTime)reader[((int)KpiColumn.KpiDateTime - 1)];
					c.KpiGeneral = (System.Decimal)reader[((int)KpiColumn.KpiGeneral - 1)];
					c.KpiCalcinerExpense = (System.Decimal)reader[((int)KpiColumn.KpiCalcinerExpense - 1)];
					c.KpiCalcinerCapital = (System.Decimal)reader[((int)KpiColumn.KpiCalcinerCapital - 1)];
					c.KpiResidue = (System.Decimal)reader[((int)KpiColumn.KpiResidue - 1)];
					c.ProjectCapital1Title = (reader.IsDBNull(((int)KpiColumn.ProjectCapital1Title - 1)))?null:(System.String)reader[((int)KpiColumn.ProjectCapital1Title - 1)];
					c.ProjectCapital1Hours = (reader.IsDBNull(((int)KpiColumn.ProjectCapital1Hours - 1)))?null:(System.Decimal?)reader[((int)KpiColumn.ProjectCapital1Hours - 1)];
					c.ProjectCapital2Title = (reader.IsDBNull(((int)KpiColumn.ProjectCapital2Title - 1)))?null:(System.String)reader[((int)KpiColumn.ProjectCapital2Title - 1)];
					c.ProjectCapital2Hours = (reader.IsDBNull(((int)KpiColumn.ProjectCapital2Hours - 1)))?null:(System.Decimal?)reader[((int)KpiColumn.ProjectCapital2Hours - 1)];
					c.ProjectCapital3Title = (reader.IsDBNull(((int)KpiColumn.ProjectCapital3Title - 1)))?null:(System.String)reader[((int)KpiColumn.ProjectCapital3Title - 1)];
					c.ProjectCapital3Hours = (reader.IsDBNull(((int)KpiColumn.ProjectCapital3Hours - 1)))?null:(System.Decimal?)reader[((int)KpiColumn.ProjectCapital3Hours - 1)];
					c.ProjectCapital4Title = (reader.IsDBNull(((int)KpiColumn.ProjectCapital4Title - 1)))?null:(System.String)reader[((int)KpiColumn.ProjectCapital4Title - 1)];
					c.ProjectCapital4Hours = (reader.IsDBNull(((int)KpiColumn.ProjectCapital4Hours - 1)))?null:(System.Decimal?)reader[((int)KpiColumn.ProjectCapital4Hours - 1)];
					c.ProjectCapital5Title = (reader.IsDBNull(((int)KpiColumn.ProjectCapital5Title - 1)))?null:(System.String)reader[((int)KpiColumn.ProjectCapital5Title - 1)];
					c.ProjectCapital5Hours = (reader.IsDBNull(((int)KpiColumn.ProjectCapital5Hours - 1)))?null:(System.Decimal?)reader[((int)KpiColumn.ProjectCapital5Hours - 1)];
					c.AheaRefineryWork = (System.Decimal)reader[((int)KpiColumn.AheaRefineryWork - 1)];
					c.AheaResidueWork = (System.Decimal)reader[((int)KpiColumn.AheaResidueWork - 1)];
					c.AheaSmeltingWork = (reader.IsDBNull(((int)KpiColumn.AheaSmeltingWork - 1)))?null:(System.Decimal?)reader[((int)KpiColumn.AheaSmeltingWork - 1)];
					c.AheaPowerGenerationWork = (reader.IsDBNull(((int)KpiColumn.AheaPowerGenerationWork - 1)))?null:(System.Decimal?)reader[((int)KpiColumn.AheaPowerGenerationWork - 1)];
					c.AheaTotalManHours = (System.Decimal)reader[((int)KpiColumn.AheaTotalManHours - 1)];
					c.AheaPeakNopplSiteWeek = (System.Int32)reader[((int)KpiColumn.AheaPeakNopplSiteWeek - 1)];
					c.AheaAvgNopplSiteMonth = (System.Int32)reader[((int)KpiColumn.AheaAvgNopplSiteMonth - 1)];
					c.AheaNoEmpExcessMonth = (System.Int32)reader[((int)KpiColumn.AheaNoEmpExcessMonth - 1)];
					c.IpFati = (reader.IsDBNull(((int)KpiColumn.IpFati - 1)))?null:(System.Int32?)reader[((int)KpiColumn.IpFati - 1)];
					c.IpMti = (reader.IsDBNull(((int)KpiColumn.IpMti - 1)))?null:(System.Int32?)reader[((int)KpiColumn.IpMti - 1)];
					c.IpRdi = (reader.IsDBNull(((int)KpiColumn.IpRdi - 1)))?null:(System.Int32?)reader[((int)KpiColumn.IpRdi - 1)];
					c.IpLti = (reader.IsDBNull(((int)KpiColumn.IpLti - 1)))?null:(System.Int32?)reader[((int)KpiColumn.IpLti - 1)];
					c.IpIfe = (reader.IsDBNull(((int)KpiColumn.IpIfe - 1)))?null:(System.Int32?)reader[((int)KpiColumn.IpIfe - 1)];
					c.EhspNoLwd = (reader.IsDBNull(((int)KpiColumn.EhspNoLwd - 1)))?null:(System.Decimal?)reader[((int)KpiColumn.EhspNoLwd - 1)];
					c.EhspNoRd = (reader.IsDBNull(((int)KpiColumn.EhspNoRd - 1)))?null:(System.Decimal?)reader[((int)KpiColumn.EhspNoRd - 1)];
					c.EhsCorrective = (reader.IsDBNull(((int)KpiColumn.EhsCorrective - 1)))?null:(System.Int32?)reader[((int)KpiColumn.EhsCorrective - 1)];
					c.JsaAudits = (reader.IsDBNull(((int)KpiColumn.JsaAudits - 1)))?null:(System.Int32?)reader[((int)KpiColumn.JsaAudits - 1)];
					c.IWsc = (reader.IsDBNull(((int)KpiColumn.IWsc - 1)))?null:(System.Int32?)reader[((int)KpiColumn.IWsc - 1)];
					c.ONoHswc = (reader.IsDBNull(((int)KpiColumn.ONoHswc - 1)))?null:(System.Int32?)reader[((int)KpiColumn.ONoHswc - 1)];
					c.ONoBsp = (reader.IsDBNull(((int)KpiColumn.ONoBsp - 1)))?null:(System.Int32?)reader[((int)KpiColumn.ONoBsp - 1)];
					c.QQas = (reader.IsDBNull(((int)KpiColumn.QQas - 1)))?null:(System.Decimal?)reader[((int)KpiColumn.QQas - 1)];
					c.QNoNci = (reader.IsDBNull(((int)KpiColumn.QNoNci - 1)))?null:(System.Int32?)reader[((int)KpiColumn.QNoNci - 1)];
					c.MTbmpm = (reader.IsDBNull(((int)KpiColumn.MTbmpm - 1)))?null:(System.Int32?)reader[((int)KpiColumn.MTbmpm - 1)];
					c.MAwcm = (reader.IsDBNull(((int)KpiColumn.MAwcm - 1)))?null:(System.Int32?)reader[((int)KpiColumn.MAwcm - 1)];
					c.MAmcm = (reader.IsDBNull(((int)KpiColumn.MAmcm - 1)))?null:(System.Int32?)reader[((int)KpiColumn.MAmcm - 1)];
					c.MFatality = (reader.IsDBNull(((int)KpiColumn.MFatality - 1)))?null:(System.Int32?)reader[((int)KpiColumn.MFatality - 1)];
					c.Training = (reader.IsDBNull(((int)KpiColumn.Training - 1)))?null:(System.Int32?)reader[((int)KpiColumn.Training - 1)];
					c.MtTolo = (reader.IsDBNull(((int)KpiColumn.MtTolo - 1)))?null:(System.Int32?)reader[((int)KpiColumn.MtTolo - 1)];
					c.MtFp = (reader.IsDBNull(((int)KpiColumn.MtFp - 1)))?null:(System.Int32?)reader[((int)KpiColumn.MtFp - 1)];
					c.MtElec = (reader.IsDBNull(((int)KpiColumn.MtElec - 1)))?null:(System.Int32?)reader[((int)KpiColumn.MtElec - 1)];
					c.MtMe = (reader.IsDBNull(((int)KpiColumn.MtMe - 1)))?null:(System.Int32?)reader[((int)KpiColumn.MtMe - 1)];
					c.MtCs = (reader.IsDBNull(((int)KpiColumn.MtCs - 1)))?null:(System.Int32?)reader[((int)KpiColumn.MtCs - 1)];
					c.MtCb = (reader.IsDBNull(((int)KpiColumn.MtCb - 1)))?null:(System.Int32?)reader[((int)KpiColumn.MtCb - 1)];
					c.MtErgo = (reader.IsDBNull(((int)KpiColumn.MtErgo - 1)))?null:(System.Int32?)reader[((int)KpiColumn.MtErgo - 1)];
					c.MtRa = (reader.IsDBNull(((int)KpiColumn.MtRa - 1)))?null:(System.Int32?)reader[((int)KpiColumn.MtRa - 1)];
					c.MtHs = (reader.IsDBNull(((int)KpiColumn.MtHs - 1)))?null:(System.Int32?)reader[((int)KpiColumn.MtHs - 1)];
					c.MtSp = (reader.IsDBNull(((int)KpiColumn.MtSp - 1)))?null:(System.Int32?)reader[((int)KpiColumn.MtSp - 1)];
					c.MtIf = (reader.IsDBNull(((int)KpiColumn.MtIf - 1)))?null:(System.Int32?)reader[((int)KpiColumn.MtIf - 1)];
					c.MtHp = (reader.IsDBNull(((int)KpiColumn.MtHp - 1)))?null:(System.Int32?)reader[((int)KpiColumn.MtHp - 1)];
					c.MtRp = (reader.IsDBNull(((int)KpiColumn.MtRp - 1)))?null:(System.Int32?)reader[((int)KpiColumn.MtRp - 1)];
					c.MtEnginfo81t = (reader.IsDBNull(((int)KpiColumn.MtEnginfo81t - 1)))?null:(System.Int32?)reader[((int)KpiColumn.MtEnginfo81t - 1)];
					c.MtOthers = (reader.IsDBNull(((int)KpiColumn.MtOthers - 1)))?null:(System.String)reader[((int)KpiColumn.MtOthers - 1)];
					c.SafetyPlansSubmitted = (System.Boolean)reader[((int)KpiColumn.SafetyPlansSubmitted - 1)];
					c.EbiOnSite = (reader.IsDBNull(((int)KpiColumn.EbiOnSite - 1)))?null:(System.Boolean?)reader[((int)KpiColumn.EbiOnSite - 1)];
					c.ProjectCapital10Hours = (reader.IsDBNull(((int)KpiColumn.ProjectCapital10Hours - 1)))?null:(System.Decimal?)reader[((int)KpiColumn.ProjectCapital10Hours - 1)];
					c.ProjectCapital10Title = (reader.IsDBNull(((int)KpiColumn.ProjectCapital10Title - 1)))?null:(System.String)reader[((int)KpiColumn.ProjectCapital10Title - 1)];
					c.ProjectCapital11Hours = (reader.IsDBNull(((int)KpiColumn.ProjectCapital11Hours - 1)))?null:(System.Decimal?)reader[((int)KpiColumn.ProjectCapital11Hours - 1)];
					c.ProjectCapital11Title = (reader.IsDBNull(((int)KpiColumn.ProjectCapital11Title - 1)))?null:(System.String)reader[((int)KpiColumn.ProjectCapital11Title - 1)];
					c.ProjectCapital12Hours = (reader.IsDBNull(((int)KpiColumn.ProjectCapital12Hours - 1)))?null:(System.Decimal?)reader[((int)KpiColumn.ProjectCapital12Hours - 1)];
					c.ProjectCapital12Title = (reader.IsDBNull(((int)KpiColumn.ProjectCapital12Title - 1)))?null:(System.String)reader[((int)KpiColumn.ProjectCapital12Title - 1)];
					c.ProjectCapital13Hours = (reader.IsDBNull(((int)KpiColumn.ProjectCapital13Hours - 1)))?null:(System.Decimal?)reader[((int)KpiColumn.ProjectCapital13Hours - 1)];
					c.ProjectCapital13Title = (reader.IsDBNull(((int)KpiColumn.ProjectCapital13Title - 1)))?null:(System.String)reader[((int)KpiColumn.ProjectCapital13Title - 1)];
					c.ProjectCapital14Hours = (reader.IsDBNull(((int)KpiColumn.ProjectCapital14Hours - 1)))?null:(System.Decimal?)reader[((int)KpiColumn.ProjectCapital14Hours - 1)];
					c.ProjectCapital14Title = (reader.IsDBNull(((int)KpiColumn.ProjectCapital14Title - 1)))?null:(System.String)reader[((int)KpiColumn.ProjectCapital14Title - 1)];
					c.ProjectCapital15Hours = (reader.IsDBNull(((int)KpiColumn.ProjectCapital15Hours - 1)))?null:(System.Decimal?)reader[((int)KpiColumn.ProjectCapital15Hours - 1)];
					c.ProjectCapital15Title = (reader.IsDBNull(((int)KpiColumn.ProjectCapital15Title - 1)))?null:(System.String)reader[((int)KpiColumn.ProjectCapital15Title - 1)];
					c.ProjectCapital6Hours = (reader.IsDBNull(((int)KpiColumn.ProjectCapital6Hours - 1)))?null:(System.Decimal?)reader[((int)KpiColumn.ProjectCapital6Hours - 1)];
					c.ProjectCapital6Title = (reader.IsDBNull(((int)KpiColumn.ProjectCapital6Title - 1)))?null:(System.String)reader[((int)KpiColumn.ProjectCapital6Title - 1)];
					c.ProjectCapital7Hours = (reader.IsDBNull(((int)KpiColumn.ProjectCapital7Hours - 1)))?null:(System.Decimal?)reader[((int)KpiColumn.ProjectCapital7Hours - 1)];
					c.ProjectCapital7Title = (reader.IsDBNull(((int)KpiColumn.ProjectCapital7Title - 1)))?null:(System.String)reader[((int)KpiColumn.ProjectCapital7Title - 1)];
					c.ProjectCapital8Hours = (reader.IsDBNull(((int)KpiColumn.ProjectCapital8Hours - 1)))?null:(System.Decimal?)reader[((int)KpiColumn.ProjectCapital8Hours - 1)];
					c.ProjectCapital8Title = (reader.IsDBNull(((int)KpiColumn.ProjectCapital8Title - 1)))?null:(System.String)reader[((int)KpiColumn.ProjectCapital8Title - 1)];
					c.ProjectCapital9Hours = (reader.IsDBNull(((int)KpiColumn.ProjectCapital9Hours - 1)))?null:(System.Decimal?)reader[((int)KpiColumn.ProjectCapital9Hours - 1)];
					c.ProjectCapital9Title = (reader.IsDBNull(((int)KpiColumn.ProjectCapital9Title - 1)))?null:(System.String)reader[((int)KpiColumn.ProjectCapital9Title - 1)];
					c.ProjectCapital1Po = (reader.IsDBNull(((int)KpiColumn.ProjectCapital1Po - 1)))?null:(System.String)reader[((int)KpiColumn.ProjectCapital1Po - 1)];
					c.ProjectCapital1Line = (reader.IsDBNull(((int)KpiColumn.ProjectCapital1Line - 1)))?null:(System.String)reader[((int)KpiColumn.ProjectCapital1Line - 1)];
					c.ProjectCapital2Po = (reader.IsDBNull(((int)KpiColumn.ProjectCapital2Po - 1)))?null:(System.String)reader[((int)KpiColumn.ProjectCapital2Po - 1)];
					c.ProjectCapital2Line = (reader.IsDBNull(((int)KpiColumn.ProjectCapital2Line - 1)))?null:(System.String)reader[((int)KpiColumn.ProjectCapital2Line - 1)];
					c.ProjectCapital3Po = (reader.IsDBNull(((int)KpiColumn.ProjectCapital3Po - 1)))?null:(System.String)reader[((int)KpiColumn.ProjectCapital3Po - 1)];
					c.ProjectCapital3Line = (reader.IsDBNull(((int)KpiColumn.ProjectCapital3Line - 1)))?null:(System.String)reader[((int)KpiColumn.ProjectCapital3Line - 1)];
					c.ProjectCapital4Po = (reader.IsDBNull(((int)KpiColumn.ProjectCapital4Po - 1)))?null:(System.String)reader[((int)KpiColumn.ProjectCapital4Po - 1)];
					c.ProjectCapital4Line = (reader.IsDBNull(((int)KpiColumn.ProjectCapital4Line - 1)))?null:(System.String)reader[((int)KpiColumn.ProjectCapital4Line - 1)];
					c.ProjectCapital5Po = (reader.IsDBNull(((int)KpiColumn.ProjectCapital5Po - 1)))?null:(System.String)reader[((int)KpiColumn.ProjectCapital5Po - 1)];
					c.ProjectCapital5Line = (reader.IsDBNull(((int)KpiColumn.ProjectCapital5Line - 1)))?null:(System.String)reader[((int)KpiColumn.ProjectCapital5Line - 1)];
					c.ProjectCapital6Po = (reader.IsDBNull(((int)KpiColumn.ProjectCapital6Po - 1)))?null:(System.String)reader[((int)KpiColumn.ProjectCapital6Po - 1)];
					c.ProjectCapital6Line = (reader.IsDBNull(((int)KpiColumn.ProjectCapital6Line - 1)))?null:(System.String)reader[((int)KpiColumn.ProjectCapital6Line - 1)];
					c.ProjectCapital7Po = (reader.IsDBNull(((int)KpiColumn.ProjectCapital7Po - 1)))?null:(System.String)reader[((int)KpiColumn.ProjectCapital7Po - 1)];
					c.ProjectCapital7Line = (reader.IsDBNull(((int)KpiColumn.ProjectCapital7Line - 1)))?null:(System.String)reader[((int)KpiColumn.ProjectCapital7Line - 1)];
					c.ProjectCapital8Po = (reader.IsDBNull(((int)KpiColumn.ProjectCapital8Po - 1)))?null:(System.String)reader[((int)KpiColumn.ProjectCapital8Po - 1)];
					c.ProjectCapital8Line = (reader.IsDBNull(((int)KpiColumn.ProjectCapital8Line - 1)))?null:(System.String)reader[((int)KpiColumn.ProjectCapital8Line - 1)];
					c.ProjectCapital9Po = (reader.IsDBNull(((int)KpiColumn.ProjectCapital9Po - 1)))?null:(System.String)reader[((int)KpiColumn.ProjectCapital9Po - 1)];
					c.ProjectCapital9Line = (reader.IsDBNull(((int)KpiColumn.ProjectCapital9Line - 1)))?null:(System.String)reader[((int)KpiColumn.ProjectCapital9Line - 1)];
					c.ProjectCapital10Po = (reader.IsDBNull(((int)KpiColumn.ProjectCapital10Po - 1)))?null:(System.String)reader[((int)KpiColumn.ProjectCapital10Po - 1)];
					c.ProjectCapital10Line = (reader.IsDBNull(((int)KpiColumn.ProjectCapital10Line - 1)))?null:(System.String)reader[((int)KpiColumn.ProjectCapital10Line - 1)];
					c.ProjectCapital11Po = (reader.IsDBNull(((int)KpiColumn.ProjectCapital11Po - 1)))?null:(System.String)reader[((int)KpiColumn.ProjectCapital11Po - 1)];
					c.ProjectCapital11Line = (reader.IsDBNull(((int)KpiColumn.ProjectCapital11Line - 1)))?null:(System.String)reader[((int)KpiColumn.ProjectCapital11Line - 1)];
					c.ProjectCapital12Po = (reader.IsDBNull(((int)KpiColumn.ProjectCapital12Po - 1)))?null:(System.String)reader[((int)KpiColumn.ProjectCapital12Po - 1)];
					c.ProjectCapital12Line = (reader.IsDBNull(((int)KpiColumn.ProjectCapital12Line - 1)))?null:(System.String)reader[((int)KpiColumn.ProjectCapital12Line - 1)];
					c.ProjectCapital13Po = (reader.IsDBNull(((int)KpiColumn.ProjectCapital13Po - 1)))?null:(System.String)reader[((int)KpiColumn.ProjectCapital13Po - 1)];
					c.ProjectCapital13Line = (reader.IsDBNull(((int)KpiColumn.ProjectCapital13Line - 1)))?null:(System.String)reader[((int)KpiColumn.ProjectCapital13Line - 1)];
					c.ProjectCapital14Po = (reader.IsDBNull(((int)KpiColumn.ProjectCapital14Po - 1)))?null:(System.String)reader[((int)KpiColumn.ProjectCapital14Po - 1)];
					c.ProjectCapital14Line = (reader.IsDBNull(((int)KpiColumn.ProjectCapital14Line - 1)))?null:(System.String)reader[((int)KpiColumn.ProjectCapital14Line - 1)];
					c.ProjectCapital15Po = (reader.IsDBNull(((int)KpiColumn.ProjectCapital15Po - 1)))?null:(System.String)reader[((int)KpiColumn.ProjectCapital15Po - 1)];
					c.ProjectCapital15Line = (reader.IsDBNull(((int)KpiColumn.ProjectCapital15Line - 1)))?null:(System.String)reader[((int)KpiColumn.ProjectCapital15Line - 1)];
					c.IsSystemEdit = (reader.IsDBNull(((int)KpiColumn.IsSystemEdit - 1)))?null:(System.Boolean?)reader[((int)KpiColumn.IsSystemEdit - 1)];
                    c.IpRN = (reader.IsDBNull(((int)KpiColumn.IpRN - 1))) ? null : (System.Int32?)reader[((int)KpiColumn.IpRN - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.Kpi"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.Kpi"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.Kpi entity)
		{
			if (!reader.Read()) return;
			
			entity.KpiId = (System.Int32)reader[((int)KpiColumn.KpiId - 1)];
			entity.SiteId = (System.Int32)reader[((int)KpiColumn.SiteId - 1)];
			entity.CreatedbyUserId = (System.Int32)reader[((int)KpiColumn.CreatedbyUserId - 1)];
			entity.ModifiedbyUserId = (System.Int32)reader[((int)KpiColumn.ModifiedbyUserId - 1)];
			entity.DateAdded = (System.DateTime)reader[((int)KpiColumn.DateAdded - 1)];
			entity.Datemodified = (System.DateTime)reader[((int)KpiColumn.Datemodified - 1)];
			entity.CompanyId = (System.Int32)reader[((int)KpiColumn.CompanyId - 1)];
			entity.KpiDateTime = (System.DateTime)reader[((int)KpiColumn.KpiDateTime - 1)];
			entity.KpiGeneral = (System.Decimal)reader[((int)KpiColumn.KpiGeneral - 1)];
			entity.KpiCalcinerExpense = (System.Decimal)reader[((int)KpiColumn.KpiCalcinerExpense - 1)];
			entity.KpiCalcinerCapital = (System.Decimal)reader[((int)KpiColumn.KpiCalcinerCapital - 1)];
			entity.KpiResidue = (System.Decimal)reader[((int)KpiColumn.KpiResidue - 1)];
			entity.ProjectCapital1Title = (reader.IsDBNull(((int)KpiColumn.ProjectCapital1Title - 1)))?null:(System.String)reader[((int)KpiColumn.ProjectCapital1Title - 1)];
			entity.ProjectCapital1Hours = (reader.IsDBNull(((int)KpiColumn.ProjectCapital1Hours - 1)))?null:(System.Decimal?)reader[((int)KpiColumn.ProjectCapital1Hours - 1)];
			entity.ProjectCapital2Title = (reader.IsDBNull(((int)KpiColumn.ProjectCapital2Title - 1)))?null:(System.String)reader[((int)KpiColumn.ProjectCapital2Title - 1)];
			entity.ProjectCapital2Hours = (reader.IsDBNull(((int)KpiColumn.ProjectCapital2Hours - 1)))?null:(System.Decimal?)reader[((int)KpiColumn.ProjectCapital2Hours - 1)];
			entity.ProjectCapital3Title = (reader.IsDBNull(((int)KpiColumn.ProjectCapital3Title - 1)))?null:(System.String)reader[((int)KpiColumn.ProjectCapital3Title - 1)];
			entity.ProjectCapital3Hours = (reader.IsDBNull(((int)KpiColumn.ProjectCapital3Hours - 1)))?null:(System.Decimal?)reader[((int)KpiColumn.ProjectCapital3Hours - 1)];
			entity.ProjectCapital4Title = (reader.IsDBNull(((int)KpiColumn.ProjectCapital4Title - 1)))?null:(System.String)reader[((int)KpiColumn.ProjectCapital4Title - 1)];
			entity.ProjectCapital4Hours = (reader.IsDBNull(((int)KpiColumn.ProjectCapital4Hours - 1)))?null:(System.Decimal?)reader[((int)KpiColumn.ProjectCapital4Hours - 1)];
			entity.ProjectCapital5Title = (reader.IsDBNull(((int)KpiColumn.ProjectCapital5Title - 1)))?null:(System.String)reader[((int)KpiColumn.ProjectCapital5Title - 1)];
			entity.ProjectCapital5Hours = (reader.IsDBNull(((int)KpiColumn.ProjectCapital5Hours - 1)))?null:(System.Decimal?)reader[((int)KpiColumn.ProjectCapital5Hours - 1)];
			entity.AheaRefineryWork = (System.Decimal)reader[((int)KpiColumn.AheaRefineryWork - 1)];
			entity.AheaResidueWork = (System.Decimal)reader[((int)KpiColumn.AheaResidueWork - 1)];
			entity.AheaSmeltingWork = (reader.IsDBNull(((int)KpiColumn.AheaSmeltingWork - 1)))?null:(System.Decimal?)reader[((int)KpiColumn.AheaSmeltingWork - 1)];
			entity.AheaPowerGenerationWork = (reader.IsDBNull(((int)KpiColumn.AheaPowerGenerationWork - 1)))?null:(System.Decimal?)reader[((int)KpiColumn.AheaPowerGenerationWork - 1)];
			entity.AheaTotalManHours = (System.Decimal)reader[((int)KpiColumn.AheaTotalManHours - 1)];
			entity.AheaPeakNopplSiteWeek = (System.Int32)reader[((int)KpiColumn.AheaPeakNopplSiteWeek - 1)];
			entity.AheaAvgNopplSiteMonth = (System.Int32)reader[((int)KpiColumn.AheaAvgNopplSiteMonth - 1)];
			entity.AheaNoEmpExcessMonth = (System.Int32)reader[((int)KpiColumn.AheaNoEmpExcessMonth - 1)];
			entity.IpFati = (reader.IsDBNull(((int)KpiColumn.IpFati - 1)))?null:(System.Int32?)reader[((int)KpiColumn.IpFati - 1)];
			entity.IpMti = (reader.IsDBNull(((int)KpiColumn.IpMti - 1)))?null:(System.Int32?)reader[((int)KpiColumn.IpMti - 1)];
			entity.IpRdi = (reader.IsDBNull(((int)KpiColumn.IpRdi - 1)))?null:(System.Int32?)reader[((int)KpiColumn.IpRdi - 1)];
			entity.IpLti = (reader.IsDBNull(((int)KpiColumn.IpLti - 1)))?null:(System.Int32?)reader[((int)KpiColumn.IpLti - 1)];
			entity.IpIfe = (reader.IsDBNull(((int)KpiColumn.IpIfe - 1)))?null:(System.Int32?)reader[((int)KpiColumn.IpIfe - 1)];
			entity.EhspNoLwd = (reader.IsDBNull(((int)KpiColumn.EhspNoLwd - 1)))?null:(System.Decimal?)reader[((int)KpiColumn.EhspNoLwd - 1)];
			entity.EhspNoRd = (reader.IsDBNull(((int)KpiColumn.EhspNoRd - 1)))?null:(System.Decimal?)reader[((int)KpiColumn.EhspNoRd - 1)];
			entity.EhsCorrective = (reader.IsDBNull(((int)KpiColumn.EhsCorrective - 1)))?null:(System.Int32?)reader[((int)KpiColumn.EhsCorrective - 1)];
			entity.JsaAudits = (reader.IsDBNull(((int)KpiColumn.JsaAudits - 1)))?null:(System.Int32?)reader[((int)KpiColumn.JsaAudits - 1)];
			entity.IWsc = (reader.IsDBNull(((int)KpiColumn.IWsc - 1)))?null:(System.Int32?)reader[((int)KpiColumn.IWsc - 1)];
			entity.ONoHswc = (reader.IsDBNull(((int)KpiColumn.ONoHswc - 1)))?null:(System.Int32?)reader[((int)KpiColumn.ONoHswc - 1)];
			entity.ONoBsp = (reader.IsDBNull(((int)KpiColumn.ONoBsp - 1)))?null:(System.Int32?)reader[((int)KpiColumn.ONoBsp - 1)];
			entity.QQas = (reader.IsDBNull(((int)KpiColumn.QQas - 1)))?null:(System.Decimal?)reader[((int)KpiColumn.QQas - 1)];
			entity.QNoNci = (reader.IsDBNull(((int)KpiColumn.QNoNci - 1)))?null:(System.Int32?)reader[((int)KpiColumn.QNoNci - 1)];
			entity.MTbmpm = (reader.IsDBNull(((int)KpiColumn.MTbmpm - 1)))?null:(System.Int32?)reader[((int)KpiColumn.MTbmpm - 1)];
			entity.MAwcm = (reader.IsDBNull(((int)KpiColumn.MAwcm - 1)))?null:(System.Int32?)reader[((int)KpiColumn.MAwcm - 1)];
			entity.MAmcm = (reader.IsDBNull(((int)KpiColumn.MAmcm - 1)))?null:(System.Int32?)reader[((int)KpiColumn.MAmcm - 1)];
			entity.MFatality = (reader.IsDBNull(((int)KpiColumn.MFatality - 1)))?null:(System.Int32?)reader[((int)KpiColumn.MFatality - 1)];
			entity.Training = (reader.IsDBNull(((int)KpiColumn.Training - 1)))?null:(System.Int32?)reader[((int)KpiColumn.Training - 1)];
			entity.MtTolo = (reader.IsDBNull(((int)KpiColumn.MtTolo - 1)))?null:(System.Int32?)reader[((int)KpiColumn.MtTolo - 1)];
			entity.MtFp = (reader.IsDBNull(((int)KpiColumn.MtFp - 1)))?null:(System.Int32?)reader[((int)KpiColumn.MtFp - 1)];
			entity.MtElec = (reader.IsDBNull(((int)KpiColumn.MtElec - 1)))?null:(System.Int32?)reader[((int)KpiColumn.MtElec - 1)];
			entity.MtMe = (reader.IsDBNull(((int)KpiColumn.MtMe - 1)))?null:(System.Int32?)reader[((int)KpiColumn.MtMe - 1)];
			entity.MtCs = (reader.IsDBNull(((int)KpiColumn.MtCs - 1)))?null:(System.Int32?)reader[((int)KpiColumn.MtCs - 1)];
			entity.MtCb = (reader.IsDBNull(((int)KpiColumn.MtCb - 1)))?null:(System.Int32?)reader[((int)KpiColumn.MtCb - 1)];
			entity.MtErgo = (reader.IsDBNull(((int)KpiColumn.MtErgo - 1)))?null:(System.Int32?)reader[((int)KpiColumn.MtErgo - 1)];
			entity.MtRa = (reader.IsDBNull(((int)KpiColumn.MtRa - 1)))?null:(System.Int32?)reader[((int)KpiColumn.MtRa - 1)];
			entity.MtHs = (reader.IsDBNull(((int)KpiColumn.MtHs - 1)))?null:(System.Int32?)reader[((int)KpiColumn.MtHs - 1)];
			entity.MtSp = (reader.IsDBNull(((int)KpiColumn.MtSp - 1)))?null:(System.Int32?)reader[((int)KpiColumn.MtSp - 1)];
			entity.MtIf = (reader.IsDBNull(((int)KpiColumn.MtIf - 1)))?null:(System.Int32?)reader[((int)KpiColumn.MtIf - 1)];
			entity.MtHp = (reader.IsDBNull(((int)KpiColumn.MtHp - 1)))?null:(System.Int32?)reader[((int)KpiColumn.MtHp - 1)];
			entity.MtRp = (reader.IsDBNull(((int)KpiColumn.MtRp - 1)))?null:(System.Int32?)reader[((int)KpiColumn.MtRp - 1)];
			entity.MtEnginfo81t = (reader.IsDBNull(((int)KpiColumn.MtEnginfo81t - 1)))?null:(System.Int32?)reader[((int)KpiColumn.MtEnginfo81t - 1)];
			entity.MtOthers = (reader.IsDBNull(((int)KpiColumn.MtOthers - 1)))?null:(System.String)reader[((int)KpiColumn.MtOthers - 1)];
			entity.SafetyPlansSubmitted = (System.Boolean)reader[((int)KpiColumn.SafetyPlansSubmitted - 1)];
			entity.EbiOnSite = (reader.IsDBNull(((int)KpiColumn.EbiOnSite - 1)))?null:(System.Boolean?)reader[((int)KpiColumn.EbiOnSite - 1)];
			entity.ProjectCapital10Hours = (reader.IsDBNull(((int)KpiColumn.ProjectCapital10Hours - 1)))?null:(System.Decimal?)reader[((int)KpiColumn.ProjectCapital10Hours - 1)];
			entity.ProjectCapital10Title = (reader.IsDBNull(((int)KpiColumn.ProjectCapital10Title - 1)))?null:(System.String)reader[((int)KpiColumn.ProjectCapital10Title - 1)];
			entity.ProjectCapital11Hours = (reader.IsDBNull(((int)KpiColumn.ProjectCapital11Hours - 1)))?null:(System.Decimal?)reader[((int)KpiColumn.ProjectCapital11Hours - 1)];
			entity.ProjectCapital11Title = (reader.IsDBNull(((int)KpiColumn.ProjectCapital11Title - 1)))?null:(System.String)reader[((int)KpiColumn.ProjectCapital11Title - 1)];
			entity.ProjectCapital12Hours = (reader.IsDBNull(((int)KpiColumn.ProjectCapital12Hours - 1)))?null:(System.Decimal?)reader[((int)KpiColumn.ProjectCapital12Hours - 1)];
			entity.ProjectCapital12Title = (reader.IsDBNull(((int)KpiColumn.ProjectCapital12Title - 1)))?null:(System.String)reader[((int)KpiColumn.ProjectCapital12Title - 1)];
			entity.ProjectCapital13Hours = (reader.IsDBNull(((int)KpiColumn.ProjectCapital13Hours - 1)))?null:(System.Decimal?)reader[((int)KpiColumn.ProjectCapital13Hours - 1)];
			entity.ProjectCapital13Title = (reader.IsDBNull(((int)KpiColumn.ProjectCapital13Title - 1)))?null:(System.String)reader[((int)KpiColumn.ProjectCapital13Title - 1)];
			entity.ProjectCapital14Hours = (reader.IsDBNull(((int)KpiColumn.ProjectCapital14Hours - 1)))?null:(System.Decimal?)reader[((int)KpiColumn.ProjectCapital14Hours - 1)];
			entity.ProjectCapital14Title = (reader.IsDBNull(((int)KpiColumn.ProjectCapital14Title - 1)))?null:(System.String)reader[((int)KpiColumn.ProjectCapital14Title - 1)];
			entity.ProjectCapital15Hours = (reader.IsDBNull(((int)KpiColumn.ProjectCapital15Hours - 1)))?null:(System.Decimal?)reader[((int)KpiColumn.ProjectCapital15Hours - 1)];
			entity.ProjectCapital15Title = (reader.IsDBNull(((int)KpiColumn.ProjectCapital15Title - 1)))?null:(System.String)reader[((int)KpiColumn.ProjectCapital15Title - 1)];
			entity.ProjectCapital6Hours = (reader.IsDBNull(((int)KpiColumn.ProjectCapital6Hours - 1)))?null:(System.Decimal?)reader[((int)KpiColumn.ProjectCapital6Hours - 1)];
			entity.ProjectCapital6Title = (reader.IsDBNull(((int)KpiColumn.ProjectCapital6Title - 1)))?null:(System.String)reader[((int)KpiColumn.ProjectCapital6Title - 1)];
			entity.ProjectCapital7Hours = (reader.IsDBNull(((int)KpiColumn.ProjectCapital7Hours - 1)))?null:(System.Decimal?)reader[((int)KpiColumn.ProjectCapital7Hours - 1)];
			entity.ProjectCapital7Title = (reader.IsDBNull(((int)KpiColumn.ProjectCapital7Title - 1)))?null:(System.String)reader[((int)KpiColumn.ProjectCapital7Title - 1)];
			entity.ProjectCapital8Hours = (reader.IsDBNull(((int)KpiColumn.ProjectCapital8Hours - 1)))?null:(System.Decimal?)reader[((int)KpiColumn.ProjectCapital8Hours - 1)];
			entity.ProjectCapital8Title = (reader.IsDBNull(((int)KpiColumn.ProjectCapital8Title - 1)))?null:(System.String)reader[((int)KpiColumn.ProjectCapital8Title - 1)];
			entity.ProjectCapital9Hours = (reader.IsDBNull(((int)KpiColumn.ProjectCapital9Hours - 1)))?null:(System.Decimal?)reader[((int)KpiColumn.ProjectCapital9Hours - 1)];
			entity.ProjectCapital9Title = (reader.IsDBNull(((int)KpiColumn.ProjectCapital9Title - 1)))?null:(System.String)reader[((int)KpiColumn.ProjectCapital9Title - 1)];
			entity.ProjectCapital1Po = (reader.IsDBNull(((int)KpiColumn.ProjectCapital1Po - 1)))?null:(System.String)reader[((int)KpiColumn.ProjectCapital1Po - 1)];
			entity.ProjectCapital1Line = (reader.IsDBNull(((int)KpiColumn.ProjectCapital1Line - 1)))?null:(System.String)reader[((int)KpiColumn.ProjectCapital1Line - 1)];
			entity.ProjectCapital2Po = (reader.IsDBNull(((int)KpiColumn.ProjectCapital2Po - 1)))?null:(System.String)reader[((int)KpiColumn.ProjectCapital2Po - 1)];
			entity.ProjectCapital2Line = (reader.IsDBNull(((int)KpiColumn.ProjectCapital2Line - 1)))?null:(System.String)reader[((int)KpiColumn.ProjectCapital2Line - 1)];
			entity.ProjectCapital3Po = (reader.IsDBNull(((int)KpiColumn.ProjectCapital3Po - 1)))?null:(System.String)reader[((int)KpiColumn.ProjectCapital3Po - 1)];
			entity.ProjectCapital3Line = (reader.IsDBNull(((int)KpiColumn.ProjectCapital3Line - 1)))?null:(System.String)reader[((int)KpiColumn.ProjectCapital3Line - 1)];
			entity.ProjectCapital4Po = (reader.IsDBNull(((int)KpiColumn.ProjectCapital4Po - 1)))?null:(System.String)reader[((int)KpiColumn.ProjectCapital4Po - 1)];
			entity.ProjectCapital4Line = (reader.IsDBNull(((int)KpiColumn.ProjectCapital4Line - 1)))?null:(System.String)reader[((int)KpiColumn.ProjectCapital4Line - 1)];
			entity.ProjectCapital5Po = (reader.IsDBNull(((int)KpiColumn.ProjectCapital5Po - 1)))?null:(System.String)reader[((int)KpiColumn.ProjectCapital5Po - 1)];
			entity.ProjectCapital5Line = (reader.IsDBNull(((int)KpiColumn.ProjectCapital5Line - 1)))?null:(System.String)reader[((int)KpiColumn.ProjectCapital5Line - 1)];
			entity.ProjectCapital6Po = (reader.IsDBNull(((int)KpiColumn.ProjectCapital6Po - 1)))?null:(System.String)reader[((int)KpiColumn.ProjectCapital6Po - 1)];
			entity.ProjectCapital6Line = (reader.IsDBNull(((int)KpiColumn.ProjectCapital6Line - 1)))?null:(System.String)reader[((int)KpiColumn.ProjectCapital6Line - 1)];
			entity.ProjectCapital7Po = (reader.IsDBNull(((int)KpiColumn.ProjectCapital7Po - 1)))?null:(System.String)reader[((int)KpiColumn.ProjectCapital7Po - 1)];
			entity.ProjectCapital7Line = (reader.IsDBNull(((int)KpiColumn.ProjectCapital7Line - 1)))?null:(System.String)reader[((int)KpiColumn.ProjectCapital7Line - 1)];
			entity.ProjectCapital8Po = (reader.IsDBNull(((int)KpiColumn.ProjectCapital8Po - 1)))?null:(System.String)reader[((int)KpiColumn.ProjectCapital8Po - 1)];
			entity.ProjectCapital8Line = (reader.IsDBNull(((int)KpiColumn.ProjectCapital8Line - 1)))?null:(System.String)reader[((int)KpiColumn.ProjectCapital8Line - 1)];
			entity.ProjectCapital9Po = (reader.IsDBNull(((int)KpiColumn.ProjectCapital9Po - 1)))?null:(System.String)reader[((int)KpiColumn.ProjectCapital9Po - 1)];
			entity.ProjectCapital9Line = (reader.IsDBNull(((int)KpiColumn.ProjectCapital9Line - 1)))?null:(System.String)reader[((int)KpiColumn.ProjectCapital9Line - 1)];
			entity.ProjectCapital10Po = (reader.IsDBNull(((int)KpiColumn.ProjectCapital10Po - 1)))?null:(System.String)reader[((int)KpiColumn.ProjectCapital10Po - 1)];
			entity.ProjectCapital10Line = (reader.IsDBNull(((int)KpiColumn.ProjectCapital10Line - 1)))?null:(System.String)reader[((int)KpiColumn.ProjectCapital10Line - 1)];
			entity.ProjectCapital11Po = (reader.IsDBNull(((int)KpiColumn.ProjectCapital11Po - 1)))?null:(System.String)reader[((int)KpiColumn.ProjectCapital11Po - 1)];
			entity.ProjectCapital11Line = (reader.IsDBNull(((int)KpiColumn.ProjectCapital11Line - 1)))?null:(System.String)reader[((int)KpiColumn.ProjectCapital11Line - 1)];
			entity.ProjectCapital12Po = (reader.IsDBNull(((int)KpiColumn.ProjectCapital12Po - 1)))?null:(System.String)reader[((int)KpiColumn.ProjectCapital12Po - 1)];
			entity.ProjectCapital12Line = (reader.IsDBNull(((int)KpiColumn.ProjectCapital12Line - 1)))?null:(System.String)reader[((int)KpiColumn.ProjectCapital12Line - 1)];
			entity.ProjectCapital13Po = (reader.IsDBNull(((int)KpiColumn.ProjectCapital13Po - 1)))?null:(System.String)reader[((int)KpiColumn.ProjectCapital13Po - 1)];
			entity.ProjectCapital13Line = (reader.IsDBNull(((int)KpiColumn.ProjectCapital13Line - 1)))?null:(System.String)reader[((int)KpiColumn.ProjectCapital13Line - 1)];
			entity.ProjectCapital14Po = (reader.IsDBNull(((int)KpiColumn.ProjectCapital14Po - 1)))?null:(System.String)reader[((int)KpiColumn.ProjectCapital14Po - 1)];
			entity.ProjectCapital14Line = (reader.IsDBNull(((int)KpiColumn.ProjectCapital14Line - 1)))?null:(System.String)reader[((int)KpiColumn.ProjectCapital14Line - 1)];
			entity.ProjectCapital15Po = (reader.IsDBNull(((int)KpiColumn.ProjectCapital15Po - 1)))?null:(System.String)reader[((int)KpiColumn.ProjectCapital15Po - 1)];
			entity.ProjectCapital15Line = (reader.IsDBNull(((int)KpiColumn.ProjectCapital15Line - 1)))?null:(System.String)reader[((int)KpiColumn.ProjectCapital15Line - 1)];
			entity.IsSystemEdit = (reader.IsDBNull(((int)KpiColumn.IsSystemEdit - 1)))?null:(System.Boolean?)reader[((int)KpiColumn.IsSystemEdit - 1)];
            entity.IpRN = (reader.IsDBNull(((int)KpiColumn.IpRN - 1))) ? null : (System.Int32?)reader[((int)KpiColumn.IpRN - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.Kpi"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.Kpi"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.Kpi entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.KpiId = (System.Int32)dataRow["KpiId"];
			entity.SiteId = (System.Int32)dataRow["SiteId"];
			entity.CreatedbyUserId = (System.Int32)dataRow["CreatedbyUserId"];
			entity.ModifiedbyUserId = (System.Int32)dataRow["ModifiedbyUserId"];
			entity.DateAdded = (System.DateTime)dataRow["DateAdded"];
			entity.Datemodified = (System.DateTime)dataRow["Datemodified"];
			entity.CompanyId = (System.Int32)dataRow["CompanyId"];
			entity.KpiDateTime = (System.DateTime)dataRow["kpiDateTime"];
			entity.KpiGeneral = (System.Decimal)dataRow["kpiGeneral"];
			entity.KpiCalcinerExpense = (System.Decimal)dataRow["kpiCalcinerExpense"];
			entity.KpiCalcinerCapital = (System.Decimal)dataRow["kpiCalcinerCapital"];
			entity.KpiResidue = (System.Decimal)dataRow["kpiResidue"];
			entity.ProjectCapital1Title = Convert.IsDBNull(dataRow["projectCapital1Title"]) ? null : (System.String)dataRow["projectCapital1Title"];
			entity.ProjectCapital1Hours = Convert.IsDBNull(dataRow["projectCapital1Hours"]) ? null : (System.Decimal?)dataRow["projectCapital1Hours"];
			entity.ProjectCapital2Title = Convert.IsDBNull(dataRow["projectCapital2Title"]) ? null : (System.String)dataRow["projectCapital2Title"];
			entity.ProjectCapital2Hours = Convert.IsDBNull(dataRow["projectCapital2Hours"]) ? null : (System.Decimal?)dataRow["projectCapital2Hours"];
			entity.ProjectCapital3Title = Convert.IsDBNull(dataRow["projectCapital3Title"]) ? null : (System.String)dataRow["projectCapital3Title"];
			entity.ProjectCapital3Hours = Convert.IsDBNull(dataRow["projectCapital3Hours"]) ? null : (System.Decimal?)dataRow["projectCapital3Hours"];
			entity.ProjectCapital4Title = Convert.IsDBNull(dataRow["projectCapital4Title"]) ? null : (System.String)dataRow["projectCapital4Title"];
			entity.ProjectCapital4Hours = Convert.IsDBNull(dataRow["projectCapital4Hours"]) ? null : (System.Decimal?)dataRow["projectCapital4Hours"];
			entity.ProjectCapital5Title = Convert.IsDBNull(dataRow["projectCapital5Title"]) ? null : (System.String)dataRow["projectCapital5Title"];
			entity.ProjectCapital5Hours = Convert.IsDBNull(dataRow["projectCapital5Hours"]) ? null : (System.Decimal?)dataRow["projectCapital5Hours"];
			entity.AheaRefineryWork = (System.Decimal)dataRow["aheaRefineryWork"];
			entity.AheaResidueWork = (System.Decimal)dataRow["aheaResidueWork"];
			entity.AheaSmeltingWork = Convert.IsDBNull(dataRow["aheaSmeltingWork"]) ? null : (System.Decimal?)dataRow["aheaSmeltingWork"];
			entity.AheaPowerGenerationWork = Convert.IsDBNull(dataRow["aheaPowerGenerationWork"]) ? null : (System.Decimal?)dataRow["aheaPowerGenerationWork"];
			entity.AheaTotalManHours = (System.Decimal)dataRow["aheaTotalManHours"];
			entity.AheaPeakNopplSiteWeek = (System.Int32)dataRow["aheaPeakNopplSiteWeek"];
			entity.AheaAvgNopplSiteMonth = (System.Int32)dataRow["aheaAvgNopplSiteMonth"];
			entity.AheaNoEmpExcessMonth = (System.Int32)dataRow["aheaNoEmpExcessMonth"];
			entity.IpFati = Convert.IsDBNull(dataRow["ipFATI"]) ? null : (System.Int32?)dataRow["ipFATI"];
			entity.IpMti = Convert.IsDBNull(dataRow["ipMTI"]) ? null : (System.Int32?)dataRow["ipMTI"];
			entity.IpRdi = Convert.IsDBNull(dataRow["ipRDI"]) ? null : (System.Int32?)dataRow["ipRDI"];
			entity.IpLti = Convert.IsDBNull(dataRow["ipLTI"]) ? null : (System.Int32?)dataRow["ipLTI"];
			entity.IpIfe = Convert.IsDBNull(dataRow["ipIFE"]) ? null : (System.Int32?)dataRow["ipIFE"];
			entity.EhspNoLwd = Convert.IsDBNull(dataRow["ehspNoLWD"]) ? null : (System.Decimal?)dataRow["ehspNoLWD"];
			entity.EhspNoRd = Convert.IsDBNull(dataRow["ehspNoRD"]) ? null : (System.Decimal?)dataRow["ehspNoRD"];
			entity.EhsCorrective = Convert.IsDBNull(dataRow["ehsCorrective"]) ? null : (System.Int32?)dataRow["ehsCorrective"];
			entity.JsaAudits = Convert.IsDBNull(dataRow["JSAAudits"]) ? null : (System.Int32?)dataRow["JSAAudits"];
			entity.IWsc = Convert.IsDBNull(dataRow["iWSC"]) ? null : (System.Int32?)dataRow["iWSC"];
			entity.ONoHswc = Convert.IsDBNull(dataRow["oNoHSWC"]) ? null : (System.Int32?)dataRow["oNoHSWC"];
			entity.ONoBsp = Convert.IsDBNull(dataRow["oNoBSP"]) ? null : (System.Int32?)dataRow["oNoBSP"];
			entity.QQas = Convert.IsDBNull(dataRow["qQAS"]) ? null : (System.Decimal?)dataRow["qQAS"];
			entity.QNoNci = Convert.IsDBNull(dataRow["qNoNCI"]) ? null : (System.Int32?)dataRow["qNoNCI"];
			entity.MTbmpm = Convert.IsDBNull(dataRow["mTbmpm"]) ? null : (System.Int32?)dataRow["mTbmpm"];
			entity.MAwcm = Convert.IsDBNull(dataRow["mAwcm"]) ? null : (System.Int32?)dataRow["mAwcm"];
			entity.MAmcm = Convert.IsDBNull(dataRow["mAmcm"]) ? null : (System.Int32?)dataRow["mAmcm"];
			entity.MFatality = Convert.IsDBNull(dataRow["mFatality"]) ? null : (System.Int32?)dataRow["mFatality"];
			entity.Training = Convert.IsDBNull(dataRow["Training"]) ? null : (System.Int32?)dataRow["Training"];
			entity.MtTolo = Convert.IsDBNull(dataRow["mtTolo"]) ? null : (System.Int32?)dataRow["mtTolo"];
			entity.MtFp = Convert.IsDBNull(dataRow["mtFp"]) ? null : (System.Int32?)dataRow["mtFp"];
			entity.MtElec = Convert.IsDBNull(dataRow["mtElec"]) ? null : (System.Int32?)dataRow["mtElec"];
			entity.MtMe = Convert.IsDBNull(dataRow["mtMe"]) ? null : (System.Int32?)dataRow["mtMe"];
			entity.MtCs = Convert.IsDBNull(dataRow["mtCs"]) ? null : (System.Int32?)dataRow["mtCs"];
			entity.MtCb = Convert.IsDBNull(dataRow["mtCb"]) ? null : (System.Int32?)dataRow["mtCb"];
			entity.MtErgo = Convert.IsDBNull(dataRow["mtErgo"]) ? null : (System.Int32?)dataRow["mtErgo"];
			entity.MtRa = Convert.IsDBNull(dataRow["mtRa"]) ? null : (System.Int32?)dataRow["mtRa"];
			entity.MtHs = Convert.IsDBNull(dataRow["mtHs"]) ? null : (System.Int32?)dataRow["mtHs"];
			entity.MtSp = Convert.IsDBNull(dataRow["mtSp"]) ? null : (System.Int32?)dataRow["mtSp"];
			entity.MtIf = Convert.IsDBNull(dataRow["mtIf"]) ? null : (System.Int32?)dataRow["mtIf"];
			entity.MtHp = Convert.IsDBNull(dataRow["mtHp"]) ? null : (System.Int32?)dataRow["mtHp"];
			entity.MtRp = Convert.IsDBNull(dataRow["mtRp"]) ? null : (System.Int32?)dataRow["mtRp"];
			entity.MtEnginfo81t = Convert.IsDBNull(dataRow["mtENGINFO81t"]) ? null : (System.Int32?)dataRow["mtENGINFO81t"];
			entity.MtOthers = Convert.IsDBNull(dataRow["mtOthers"]) ? null : (System.String)dataRow["mtOthers"];
			entity.SafetyPlansSubmitted = (System.Boolean)dataRow["SafetyPlansSubmitted"];
			entity.EbiOnSite = Convert.IsDBNull(dataRow["EbiOnSite"]) ? null : (System.Boolean?)dataRow["EbiOnSite"];
			entity.ProjectCapital10Hours = Convert.IsDBNull(dataRow["projectCapital10Hours"]) ? null : (System.Decimal?)dataRow["projectCapital10Hours"];
			entity.ProjectCapital10Title = Convert.IsDBNull(dataRow["projectCapital10Title"]) ? null : (System.String)dataRow["projectCapital10Title"];
			entity.ProjectCapital11Hours = Convert.IsDBNull(dataRow["projectCapital11Hours"]) ? null : (System.Decimal?)dataRow["projectCapital11Hours"];
			entity.ProjectCapital11Title = Convert.IsDBNull(dataRow["projectCapital11Title"]) ? null : (System.String)dataRow["projectCapital11Title"];
			entity.ProjectCapital12Hours = Convert.IsDBNull(dataRow["projectCapital12Hours"]) ? null : (System.Decimal?)dataRow["projectCapital12Hours"];
			entity.ProjectCapital12Title = Convert.IsDBNull(dataRow["projectCapital12Title"]) ? null : (System.String)dataRow["projectCapital12Title"];
			entity.ProjectCapital13Hours = Convert.IsDBNull(dataRow["projectCapital13Hours"]) ? null : (System.Decimal?)dataRow["projectCapital13Hours"];
			entity.ProjectCapital13Title = Convert.IsDBNull(dataRow["projectCapital13Title"]) ? null : (System.String)dataRow["projectCapital13Title"];
			entity.ProjectCapital14Hours = Convert.IsDBNull(dataRow["projectCapital14Hours"]) ? null : (System.Decimal?)dataRow["projectCapital14Hours"];
			entity.ProjectCapital14Title = Convert.IsDBNull(dataRow["projectCapital14Title"]) ? null : (System.String)dataRow["projectCapital14Title"];
			entity.ProjectCapital15Hours = Convert.IsDBNull(dataRow["projectCapital15Hours"]) ? null : (System.Decimal?)dataRow["projectCapital15Hours"];
			entity.ProjectCapital15Title = Convert.IsDBNull(dataRow["projectCapital15Title"]) ? null : (System.String)dataRow["projectCapital15Title"];
			entity.ProjectCapital6Hours = Convert.IsDBNull(dataRow["projectCapital6Hours"]) ? null : (System.Decimal?)dataRow["projectCapital6Hours"];
			entity.ProjectCapital6Title = Convert.IsDBNull(dataRow["projectCapital6Title"]) ? null : (System.String)dataRow["projectCapital6Title"];
			entity.ProjectCapital7Hours = Convert.IsDBNull(dataRow["projectCapital7Hours"]) ? null : (System.Decimal?)dataRow["projectCapital7Hours"];
			entity.ProjectCapital7Title = Convert.IsDBNull(dataRow["projectCapital7Title"]) ? null : (System.String)dataRow["projectCapital7Title"];
			entity.ProjectCapital8Hours = Convert.IsDBNull(dataRow["projectCapital8Hours"]) ? null : (System.Decimal?)dataRow["projectCapital8Hours"];
			entity.ProjectCapital8Title = Convert.IsDBNull(dataRow["projectCapital8Title"]) ? null : (System.String)dataRow["projectCapital8Title"];
			entity.ProjectCapital9Hours = Convert.IsDBNull(dataRow["projectCapital9Hours"]) ? null : (System.Decimal?)dataRow["projectCapital9Hours"];
			entity.ProjectCapital9Title = Convert.IsDBNull(dataRow["projectCapital9Title"]) ? null : (System.String)dataRow["projectCapital9Title"];
			entity.ProjectCapital1Po = Convert.IsDBNull(dataRow["projectCapital1Po"]) ? null : (System.String)dataRow["projectCapital1Po"];
			entity.ProjectCapital1Line = Convert.IsDBNull(dataRow["projectCapital1Line"]) ? null : (System.String)dataRow["projectCapital1Line"];
			entity.ProjectCapital2Po = Convert.IsDBNull(dataRow["projectCapital2Po"]) ? null : (System.String)dataRow["projectCapital2Po"];
			entity.ProjectCapital2Line = Convert.IsDBNull(dataRow["projectCapital2Line"]) ? null : (System.String)dataRow["projectCapital2Line"];
			entity.ProjectCapital3Po = Convert.IsDBNull(dataRow["projectCapital3Po"]) ? null : (System.String)dataRow["projectCapital3Po"];
			entity.ProjectCapital3Line = Convert.IsDBNull(dataRow["projectCapital3Line"]) ? null : (System.String)dataRow["projectCapital3Line"];
			entity.ProjectCapital4Po = Convert.IsDBNull(dataRow["projectCapital4Po"]) ? null : (System.String)dataRow["projectCapital4Po"];
			entity.ProjectCapital4Line = Convert.IsDBNull(dataRow["projectCapital4Line"]) ? null : (System.String)dataRow["projectCapital4Line"];
			entity.ProjectCapital5Po = Convert.IsDBNull(dataRow["projectCapital5Po"]) ? null : (System.String)dataRow["projectCapital5Po"];
			entity.ProjectCapital5Line = Convert.IsDBNull(dataRow["projectCapital5Line"]) ? null : (System.String)dataRow["projectCapital5Line"];
			entity.ProjectCapital6Po = Convert.IsDBNull(dataRow["projectCapital6Po"]) ? null : (System.String)dataRow["projectCapital6Po"];
			entity.ProjectCapital6Line = Convert.IsDBNull(dataRow["projectCapital6Line"]) ? null : (System.String)dataRow["projectCapital6Line"];
			entity.ProjectCapital7Po = Convert.IsDBNull(dataRow["projectCapital7Po"]) ? null : (System.String)dataRow["projectCapital7Po"];
			entity.ProjectCapital7Line = Convert.IsDBNull(dataRow["projectCapital7Line"]) ? null : (System.String)dataRow["projectCapital7Line"];
			entity.ProjectCapital8Po = Convert.IsDBNull(dataRow["projectCapital8Po"]) ? null : (System.String)dataRow["projectCapital8Po"];
			entity.ProjectCapital8Line = Convert.IsDBNull(dataRow["projectCapital8Line"]) ? null : (System.String)dataRow["projectCapital8Line"];
			entity.ProjectCapital9Po = Convert.IsDBNull(dataRow["projectCapital9Po"]) ? null : (System.String)dataRow["projectCapital9Po"];
			entity.ProjectCapital9Line = Convert.IsDBNull(dataRow["projectCapital9Line"]) ? null : (System.String)dataRow["projectCapital9Line"];
			entity.ProjectCapital10Po = Convert.IsDBNull(dataRow["projectCapital10Po"]) ? null : (System.String)dataRow["projectCapital10Po"];
			entity.ProjectCapital10Line = Convert.IsDBNull(dataRow["projectCapital10Line"]) ? null : (System.String)dataRow["projectCapital10Line"];
			entity.ProjectCapital11Po = Convert.IsDBNull(dataRow["projectCapital11Po"]) ? null : (System.String)dataRow["projectCapital11Po"];
			entity.ProjectCapital11Line = Convert.IsDBNull(dataRow["projectCapital11Line"]) ? null : (System.String)dataRow["projectCapital11Line"];
			entity.ProjectCapital12Po = Convert.IsDBNull(dataRow["projectCapital12Po"]) ? null : (System.String)dataRow["projectCapital12Po"];
			entity.ProjectCapital12Line = Convert.IsDBNull(dataRow["projectCapital12Line"]) ? null : (System.String)dataRow["projectCapital12Line"];
			entity.ProjectCapital13Po = Convert.IsDBNull(dataRow["projectCapital13Po"]) ? null : (System.String)dataRow["projectCapital13Po"];
			entity.ProjectCapital13Line = Convert.IsDBNull(dataRow["projectCapital13Line"]) ? null : (System.String)dataRow["projectCapital13Line"];
			entity.ProjectCapital14Po = Convert.IsDBNull(dataRow["projectCapital14Po"]) ? null : (System.String)dataRow["projectCapital14Po"];
			entity.ProjectCapital14Line = Convert.IsDBNull(dataRow["projectCapital14Line"]) ? null : (System.String)dataRow["projectCapital14Line"];
			entity.ProjectCapital15Po = Convert.IsDBNull(dataRow["projectCapital15Po"]) ? null : (System.String)dataRow["projectCapital15Po"];
			entity.ProjectCapital15Line = Convert.IsDBNull(dataRow["projectCapital15Line"]) ? null : (System.String)dataRow["projectCapital15Line"];
			entity.IsSystemEdit = Convert.IsDBNull(dataRow["IsSystemEdit"]) ? null : (System.Boolean?)dataRow["IsSystemEdit"];
            entity.IpRN = Convert.IsDBNull(dataRow["IpRN"]) ? null : (System.Int32?)dataRow["IpRN"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.Kpi"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.Kpi Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.Kpi entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region CompanyIdSource	
			if (CanDeepLoad(entity, "Companies|CompanyIdSource", deepLoadType, innerList) 
				&& entity.CompanyIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.CompanyId;
				Companies tmpEntity = EntityManager.LocateEntity<Companies>(EntityLocator.ConstructKeyFromPkItems(typeof(Companies), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.CompanyIdSource = tmpEntity;
				else
					entity.CompanyIdSource = DataRepository.CompaniesProvider.GetByCompanyId(transactionManager, entity.CompanyId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CompanyIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.CompanyIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.CompaniesProvider.DeepLoad(transactionManager, entity.CompanyIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion CompanyIdSource

			#region ModifiedbyUserIdSource	
			if (CanDeepLoad(entity, "Users|ModifiedbyUserIdSource", deepLoadType, innerList) 
				&& entity.ModifiedbyUserIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.ModifiedbyUserId;
				Users tmpEntity = EntityManager.LocateEntity<Users>(EntityLocator.ConstructKeyFromPkItems(typeof(Users), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.ModifiedbyUserIdSource = tmpEntity;
				else
					entity.ModifiedbyUserIdSource = DataRepository.UsersProvider.GetByUserId(transactionManager, entity.ModifiedbyUserId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ModifiedbyUserIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.ModifiedbyUserIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.UsersProvider.DeepLoad(transactionManager, entity.ModifiedbyUserIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion ModifiedbyUserIdSource

			#region SiteIdSource	
			if (CanDeepLoad(entity, "Sites|SiteIdSource", deepLoadType, innerList) 
				&& entity.SiteIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.SiteId;
				Sites tmpEntity = EntityManager.LocateEntity<Sites>(EntityLocator.ConstructKeyFromPkItems(typeof(Sites), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.SiteIdSource = tmpEntity;
				else
					entity.SiteIdSource = DataRepository.SitesProvider.GetBySiteId(transactionManager, entity.SiteId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'SiteIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.SiteIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.SitesProvider.DeepLoad(transactionManager, entity.SiteIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion SiteIdSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetByKpiId methods when available
			
			#region KpiProjectsCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<KpiProjects>|KpiProjectsCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'KpiProjectsCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.KpiProjectsCollection = DataRepository.KpiProjectsProvider.GetByKpiId(transactionManager, entity.KpiId);

				if (deep && entity.KpiProjectsCollection.Count > 0)
				{
					deepHandles.Add("KpiProjectsCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<KpiProjects>) DataRepository.KpiProjectsProvider.DeepLoad,
						new object[] { transactionManager, entity.KpiProjectsCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.Kpi object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.Kpi instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.Kpi Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.Kpi entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region CompanyIdSource
			if (CanDeepSave(entity, "Companies|CompanyIdSource", deepSaveType, innerList) 
				&& entity.CompanyIdSource != null)
			{
				DataRepository.CompaniesProvider.Save(transactionManager, entity.CompanyIdSource);
				entity.CompanyId = entity.CompanyIdSource.CompanyId;
			}
			#endregion 
			
			#region ModifiedbyUserIdSource
			if (CanDeepSave(entity, "Users|ModifiedbyUserIdSource", deepSaveType, innerList) 
				&& entity.ModifiedbyUserIdSource != null)
			{
				DataRepository.UsersProvider.Save(transactionManager, entity.ModifiedbyUserIdSource);
				entity.ModifiedbyUserId = entity.ModifiedbyUserIdSource.UserId;
			}
			#endregion 
			
			#region SiteIdSource
			if (CanDeepSave(entity, "Sites|SiteIdSource", deepSaveType, innerList) 
				&& entity.SiteIdSource != null)
			{
				DataRepository.SitesProvider.Save(transactionManager, entity.SiteIdSource);
				entity.SiteId = entity.SiteIdSource.SiteId;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<KpiProjects>
				if (CanDeepSave(entity.KpiProjectsCollection, "List<KpiProjects>|KpiProjectsCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(KpiProjects child in entity.KpiProjectsCollection)
					{
						if(child.KpiIdSource != null)
						{
							child.KpiId = child.KpiIdSource.KpiId;
						}
						else
						{
							child.KpiId = entity.KpiId;
						}

					}

					if (entity.KpiProjectsCollection.Count > 0 || entity.KpiProjectsCollection.DeletedItems.Count > 0)
					{
						//DataRepository.KpiProjectsProvider.Save(transactionManager, entity.KpiProjectsCollection);
						
						deepHandles.Add("KpiProjectsCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< KpiProjects >) DataRepository.KpiProjectsProvider.DeepSave,
							new object[] { transactionManager, entity.KpiProjectsCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region KpiChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.Kpi</c>
	///</summary>
	public enum KpiChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>Companies</c> at CompanyIdSource
		///</summary>
		[ChildEntityType(typeof(Companies))]
		Companies,
			
		///<summary>
		/// Composite Property for <c>Users</c> at ModifiedbyUserIdSource
		///</summary>
		[ChildEntityType(typeof(Users))]
		Users,
			
		///<summary>
		/// Composite Property for <c>Sites</c> at SiteIdSource
		///</summary>
		[ChildEntityType(typeof(Sites))]
		Sites,
	
		///<summary>
		/// Collection of <c>Kpi</c> as OneToMany for KpiProjectsCollection
		///</summary>
		[ChildEntityType(typeof(TList<KpiProjects>))]
		KpiProjectsCollection,
	}
	
	#endregion KpiChildEntityTypes
	
	#region KpiFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;KpiColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Kpi"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class KpiFilterBuilder : SqlFilterBuilder<KpiColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the KpiFilterBuilder class.
		/// </summary>
		public KpiFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the KpiFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public KpiFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the KpiFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public KpiFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion KpiFilterBuilder
	
	#region KpiParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;KpiColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Kpi"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class KpiParameterBuilder : ParameterizedSqlFilterBuilder<KpiColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the KpiParameterBuilder class.
		/// </summary>
		public KpiParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the KpiParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public KpiParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the KpiParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public KpiParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion KpiParameterBuilder
	
	#region KpiSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;KpiColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Kpi"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class KpiSortBuilder : SqlSortBuilder<KpiColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the KpiSqlSortBuilder class.
		/// </summary>
		public KpiSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion KpiSortBuilder
	
} // end namespace
