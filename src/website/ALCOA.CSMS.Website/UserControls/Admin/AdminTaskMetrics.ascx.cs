﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Text;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using KaiZen.CSMS.Web.UI;
using KaiZen.CSMS.Services;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxTabControl;
using DevExpress.Web.Data;
using DevExpress.Web.ASPxGridView;
using System.Drawing;
using DevExpress.XtraPrinting;
using System.Data.SqlClient;

using DevExpress.Web.ASPxCallbackPanel;
using DevExpress.Web.ASPxClasses;
using DevExpress.XtraCharts.Native;
using DevExpress.XtraPrintingLinks;
using DevExpress.Web.ASPxPivotGrid;
using System.IO;

namespace ALCOA.CSMS.Website.UserControls.Admin
{
    public partial class AdminTaskMetrics : System.Web.UI.UserControl
    {
        Auth auth = new Auth();
        const string fileName = "ALCOA CSMS - Admin Tasks - Metrics";

        protected void Page_Load(object sender, EventArgs e) { Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack, Page.IsCallback); }

        private void moduleInit(bool postBack, bool callBack)
        {
            if (!postBack)
            {

                switch (auth.RoleId)
                {
                    case ((int)RoleList.Administrator):
                        //full access
                        break;
                    default:
                        // do something
                        Response.Redirect("AccessDenied.aspx?error=" + Server.UrlEncode("Unrecognised User."));
                        break;
                }
            }
            else
            {
                gridPivot.DataBind();
            }
        }

        protected void imgBtnWord_Click(object sender, ImageClickEventArgs e) //export to word document format 
        {
            using (MemoryStream stream = new MemoryStream())
            {
                gridPivotExporter.ExportToRtf(stream);
                byte[] buffer = stream.GetBuffer();
                Response.Clear();
                Response.Buffer = false;
                Response.AppendHeader("Content-Type", "application/ms-excel");
                Response.AppendHeader("Content-Transfer-Encoding", "binary");
                Response.AppendHeader("Content-Disposition", String.Format("attachment; filename={0}.doc", fileName));
                Response.BinaryWrite(buffer);
                Response.End();
            }
        }
        protected void imgBtnPdf_Click(object sender, ImageClickEventArgs e) //export to adobe acrobat pdf format 
        {
            PrintingSystem ps = new PrintingSystem();

            using (CompositeLink compositeLink = new CompositeLink())
            {
                using (PrintableComponentLink link1 = new PrintableComponentLink())
                {
                    link1.Component = gridPivotExporter;
                    link1.Landscape = true;
                    link1.PrintingSystem = ps;
                    using (PrintableComponentLink link2 = new PrintableComponentLink())
                    {
                        WebChartControl1.DataBind();
                        link2.Component = ((IChartContainer)WebChartControl1).Chart;
                        link2.Landscape = true;
                        link2.PrintingSystem = ps;

                        compositeLink.Links.AddRange(new object[] { link1, link2 });


                        compositeLink.Landscape = true;
                        compositeLink.PrintingSystem = ps;
                        compositeLink.PageHeaderFooter = new PageHeaderFooter(
                            new PageHeaderArea(new string[] { 
                            String.Format("Exported by: {0}, {1}", auth.LastName, auth.FirstName), 
                            String.Format("at: {0}", DateTime.Now), "[Page # of Pages #]" },
                                SystemFonts.DefaultFont, BrickAlignment.Center),
                            new PageFooterArea(new string[] { 
                            String.Format("Exported by: {0}, {1}", auth.LastName, auth.FirstName), 
                            String.Format("at: {0}", DateTime.Now), "[Page # of Pages #]" },
                                SystemFonts.DefaultFont, BrickAlignment.Center));
                        compositeLink.CreateDocument();
                        compositeLink.Landscape = true;
                        compositeLink.PrintingSystem.ExportOptions.Pdf.DocumentOptions.Author = "ALCOA CSMS";

                        using (MemoryStream stream = new MemoryStream())
                        {
                            compositeLink.PrintingSystem.ExportToPdf(stream);
                            Response.Clear();
                            Response.Buffer = false;
                            Response.AppendHeader("Content-Type", "application/pdf");
                            Response.AppendHeader("Content-Transfer-Encoding", "binary");
                            Response.AppendHeader("Content-Disposition", String.Format("attachment; filename={0}.pdf", fileName));
                            Response.BinaryWrite(stream.GetBuffer());
                            Response.End();
                        }
                    }
                }
            }
            ps.Dispose();
        }
        protected void imgBtnExcel_Click(object sender, ImageClickEventArgs e) //export to excel spreadsheet format 
        {
            using (MemoryStream stream = new MemoryStream())
            {
                gridPivotExporter.ExportToXls(stream);
                byte[] buffer = stream.GetBuffer();
                Response.Clear();
                Response.Buffer = false;
                Response.AppendHeader("Content-Type", "application/ms-excel");
                Response.AppendHeader("Content-Transfer-Encoding", "binary");
                Response.AppendHeader("Content-Disposition", String.Format("attachment; filename={0}.xls", fileName));
                Response.BinaryWrite(buffer);
                Response.End();
            }
        }

    }
}