﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------



public partial class UserControls_Reports_EhsWorkload_Smp {
    
    /// <summary>
    /// cmbYear control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::DevExpress.Web.ASPxEditors.ASPxComboBox cmbYear;
    
    /// <summary>
    /// grid control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::DevExpress.Web.ASPxGridView.ASPxGridView grid;
    
    /// <summary>
    /// lblUnallocated control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::DevExpress.Web.ASPxEditors.ASPxLabel lblUnallocated;
    
    /// <summary>
    /// ucExportButtons control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::UserControls_Tiny_ExportButtons ucExportButtons;
    
    /// <summary>
    /// dsFileDb_GetDistinctYears control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::System.Web.UI.WebControls.ObjectDataSource dsFileDb_GetDistinctYears;
    
    /// <summary>
    /// sqldsUnallocated control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::System.Web.UI.WebControls.SqlDataSource sqldsUnallocated;
    
    /// <summary>
    /// dsFileDb_GetEhsConsultantsWorkLoad control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::System.Web.UI.WebControls.ObjectDataSource dsFileDb_GetEhsConsultantsWorkLoad;
}
