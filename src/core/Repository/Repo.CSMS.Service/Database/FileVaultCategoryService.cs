﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface IFileVaultCategoryService
    {
        FileVaultCategory Get(Expression<Func<FileVaultCategory, bool>> where, List<Expression<Func<FileVaultCategory, object>>> includes);
        List<FileVaultCategory> GetMany(int? top, Expression<Func<FileVaultCategory, bool>> where, Func<IQueryable<FileVaultCategory>, IOrderedQueryable<FileVaultCategory>> orderBy, List<Expression<Func<FileVaultCategory, object>>> includes);
        List<FileVaultCategory> GetManyPaged(int pageNumber, int pageSize, Expression<Func<FileVaultCategory, bool>> where, Func<IQueryable<FileVaultCategory>, IOrderedQueryable<FileVaultCategory>> orderBy, List<Expression<Func<FileVaultCategory, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<FileVaultCategory, bool>> where, List<Expression<Func<FileVaultCategory, object>>> includes);
        void Insert(FileVaultCategory item, bool saveChanges = true);
        void Update(FileVaultCategory item, bool saveChanges = true);
        void Delete(FileVaultCategory item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class FileVaultCategoryService : IFileVaultCategoryService
    {
        private readonly IRepository<FileVaultCategory> _repository;

        public FileVaultCategoryService(IRepository<FileVaultCategory> repository)
        {
            this._repository = repository;
        }

        public FileVaultCategory Get(Expression<Func<FileVaultCategory, bool>> where, List<Expression<Func<FileVaultCategory, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<FileVaultCategory> GetMany(int? top, Expression<Func<FileVaultCategory, bool>> where, Func<IQueryable<FileVaultCategory>, IOrderedQueryable<FileVaultCategory>> orderBy, List<Expression<Func<FileVaultCategory, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<FileVaultCategory> GetManyPaged(int pageNumber, int pageSize, Expression<Func<FileVaultCategory, bool>> where, Func<IQueryable<FileVaultCategory>, IOrderedQueryable<FileVaultCategory>> orderBy, List<Expression<Func<FileVaultCategory, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<FileVaultCategory, bool>> where, List<Expression<Func<FileVaultCategory, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(FileVaultCategory item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("FileVaultCategory");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(FileVaultCategory item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("FileVaultCategory");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(FileVaultCategory item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("FileVaultCategory");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
