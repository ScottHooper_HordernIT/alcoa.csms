﻿using System;
using System.ComponentModel;

namespace KaiZen.CSMS.Entities
{
	/// <summary>
	///		The data structure representation of the 'QuestionnairePresentlyWithMetric' table via interface.
	/// </summary>
	/// <remarks>
	/// 	This struct is generated by a tool and should never be modified.
	/// </remarks>
	public interface IQuestionnairePresentlyWithMetric 
	{
		/// <summary>			
		/// QuestionnairePresentlyWithMetricId : 
		/// </summary>
		/// <remarks>Member of the primary key of the underlying table "QuestionnairePresentlyWithMetric"</remarks>
		System.Int32 QuestionnairePresentlyWithMetricId { get; set; }
				
		
		
		/// <summary>
		/// SiteId : 
		/// </summary>
		System.Int32?  SiteId  { get; set; }
		
		/// <summary>
		/// QuestionnairePresentlyWithActionId : 
		/// </summary>
		System.Int32  QuestionnairePresentlyWithActionId  { get; set; }
		
		/// <summary>
		/// QuestionnairePresentlyWithUserId : 
		/// </summary>
		System.Int32  QuestionnairePresentlyWithUserId  { get; set; }
		
		/// <summary>
		/// Date : 
		/// </summary>
		System.DateTime  Date  { get; set; }
		
		/// <summary>
		/// Year : 
		/// </summary>
		System.Int32  Year  { get; set; }
		
		/// <summary>
		/// WeekNo : 
		/// </summary>
		System.Int32  WeekNo  { get; set; }
		
		/// <summary>
		/// Metric : 
		/// </summary>
		System.Int32  Metric  { get; set; }
			
		/// <summary>
		/// Creates a new object that is a copy of the current instance.
		/// </summary>
		/// <returns>A new object that is a copy of this instance.</returns>
		System.Object Clone();
		
		#region Data Properties

		#endregion Data Properties

	}
}


