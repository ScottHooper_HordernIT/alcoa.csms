﻿<%@ Page Title="Safety Qualification - Compare" Language="C#" MasterPageFile="~/MasterPages/popup.master" AutoEventWireup="true" Inherits="PopUps_SafetyPQ_Compare" Codebehind="SafetyPQ_Compare.aspx.cs" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
    
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .ci {COLOR:black; BACKGROUND-COLOR:#80FF80}
        .cd {COLOR:black; BACKGROUND-COLOR:#FF8080}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div style="text-align: left">
        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
            <tr>
                <td style="height: 40px; text-align: left">
                    
                </td>
                <td style="width: 8px; height: 40px">
                    <dx:ASPxButton ID="ASPxButton2" runat="server" AutoPostBack="False" CausesValidation="False"
                        CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
                        CssPostfix="Office2003Blue" 
                        SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                        Text="Print" UseSubmitBehavior="False">
                        <ClientSideEvents Click="function(s, e) {
	javascript:window.print();
}" />
                    </dx:ASPxButton>
                </td>
                <td>
                    &nbsp;
                    <dx:ASPxButton ID="btnApprove" runat="server" CausesValidation="False"
                        CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
                        CssPostfix="Office2003Blue" AutoPostBack="true"
                        SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                        Text="Approve All Unchanged Supplier/Verification Answers" 
                        onclick="btnApprove_Click" Width="323px" Enabled="false">
                    </dx:ASPxButton>
                </td>
                <td style="width: 8px; height: 40px;">
                    <dx:ASPxButton ID="ASPxButton1" runat="server" AutoPostBack="False" CausesValidation="False"
                        CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
                        CssPostfix="Office2003Blue" 
                        SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                        Text="Close" UseSubmitBehavior="False">
                        <ClientSideEvents Click="function(s, e) {
	javascript:window.close();
}" />
                    </dx:ASPxButton>
                </td>
            </tr>
            <tr>
                <td colspan="4" style="height: 13px; text-align: center">
                    <asp:Label ID="lblCompanyName" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label></td>
            </tr>
            <tr>
                <td colspan="4" style="text-align: center; height: 7px;">
                    &nbsp;<asp:Label runat="server" ForeColor="Red" ID="lblError"></asp:Label></td>
            </tr>
        </table>
        &nbsp;&nbsp;&nbsp;
        <dx:ASPxPageControl ID="ASPxPageControl1" runat="server" ActiveTabIndex="0" 
            CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
            CssPostfix="Office2003Blue" Width="100%">
            <ContentStyle>
                <Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px" />
            </ContentStyle>
            <TabPages>
                <dx:TabPage Name="Questionnaire" Text="Questionnaire">
                    <ContentCollection>
                        <dx:contentcontrol runat="server"><asp:PlaceHolder runat="server" ID="phQuestionnaire"></asp:PlaceHolder>
 </dx:contentcontrol>
                    </ContentCollection>
                </dx:TabPage>
                <dx:TabPage Name="Procurement" Text="Procurement">
                    <ContentCollection>
                        <dx:contentcontrol runat="server"><asp:PlaceHolder runat="server" ID="phProcurement"></asp:PlaceHolder>
</dx:contentcontrol>
                    </ContentCollection>
                </dx:TabPage>
                <dx:TabPage Name="Supplier" Text="Supplier">
                    <ContentCollection>
                        <dx:contentcontrol runat="server"><asp:PlaceHolder runat="server" ID="phSupplier"></asp:PlaceHolder>
                        </dx:contentcontrol>
                    </ContentCollection>
                </dx:TabPage>
                <dx:TabPage Name="Verification" Text="Verification">
                    <ContentCollection>
                        <dx:contentcontrol runat="server"><asp:PlaceHolder runat="server" ID="phVerification"></asp:PlaceHolder>
                        </dx:contentcontrol>
                    </ContentCollection>
                </dx:TabPage>
            </TabPages>
            <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
            </LoadingPanelImage>
            <LoadingPanelStyle ImageSpacing="6px">
            </LoadingPanelStyle>
        </dx:ASPxPageControl>
    
    </div>
</asp:Content>

