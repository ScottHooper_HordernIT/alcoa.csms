﻿#region Using directives

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="EmailLogAutoProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract class EmailLogAutoProviderBaseCore : EntityViewProviderBase<EmailLogAuto>
	{
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions
		
		/*
		///<summary>
		/// Fill an VList&lt;EmailLogAuto&gt; From a DataSet
		///</summary>
		/// <param name="dataSet">the DataSet</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList&lt;EmailLogAuto&gt;"/></returns>
		protected static VList&lt;EmailLogAuto&gt; Fill(DataSet dataSet, VList<EmailLogAuto> rows, int start, int pagelen)
		{
			if (dataSet.Tables.Count == 1)
			{
				return Fill(dataSet.Tables[0], rows, start, pagelen);
			}
			else
			{
				return new VList<EmailLogAuto>();
			}	
		}
		
		
		///<summary>
		/// Fill an VList&lt;EmailLogAuto&gt; From a DataTable
		///</summary>
		/// <param name="dataTable">the DataTable that hold the data.</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList<EmailLogAuto>"/></returns>
		protected static VList&lt;EmailLogAuto&gt; Fill(DataTable dataTable, VList<EmailLogAuto> rows, int start, int pagelen)
		{
			int recordnum = 0;
			
			System.Collections.IEnumerator dataRows =  dataTable.Rows.GetEnumerator();
			
			while (dataRows.MoveNext() && (pagelen != 0))
			{
				if(recordnum >= start)
				{
					DataRow row = (DataRow)dataRows.Current;
				
					EmailLogAuto c = new EmailLogAuto();
					c.EmailLogId = (Convert.IsDBNull(row["EmailLogId"]))?(int)0:(System.Int32)row["EmailLogId"];
					c.EmailLogTypeId = (Convert.IsDBNull(row["EmailLogTypeId"]))?(int)0:(System.Int32)row["EmailLogTypeId"];
					c.EmailLogTypeDesc = (Convert.IsDBNull(row["EmailLogTypeDesc"]))?string.Empty:(System.String)row["EmailLogTypeDesc"];
					c.EmailDateTime = (Convert.IsDBNull(row["EmailDateTime"]))?DateTime.MinValue:(System.DateTime)row["EmailDateTime"];
					c.EmailFrom = (Convert.IsDBNull(row["EmailFrom"]))?string.Empty:(System.String)row["EmailFrom"];
					c.EmailTo = (Convert.IsDBNull(row["EmailTo"]))?string.Empty:(System.String)row["EmailTo"];
					c.EmailCc = (Convert.IsDBNull(row["EmailCc"]))?string.Empty:(System.String)row["EmailCc"];
					c.EmailBcc = (Convert.IsDBNull(row["EmailBcc"]))?string.Empty:(System.String)row["EmailBcc"];
					c.EmailLogMessageSubject = (Convert.IsDBNull(row["EmailLogMessageSubject"]))?string.Empty:(System.String)row["EmailLogMessageSubject"];
					c.EmailLogMessageBody = (Convert.IsDBNull(row["EmailLogMessageBody"]))?new byte[] {}:(System.Byte[])row["EmailLogMessageBody"];
					c.SentByUserId = (Convert.IsDBNull(row["SentByUserId"]))?(int)0:(System.Int32?)row["SentByUserId"];
					c.SentByUserFullName = (Convert.IsDBNull(row["SentByUserFullName"]))?string.Empty:(System.String)row["SentByUserFullName"];
					c.AcceptChanges();
					rows.Add(c);
					pagelen -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		*/	
						
		///<summary>
		/// Fill an <see cref="VList&lt;EmailLogAuto&gt;"/> From a DataReader.
		///</summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pageLength">number of row.</param>
		///<returns>a <see cref="VList&lt;EmailLogAuto&gt;"/></returns>
		protected VList<EmailLogAuto> Fill(IDataReader reader, VList<EmailLogAuto> rows, int start, int pageLength)
		{
			int recordnum = 0;
			while (reader.Read() && (pageLength != 0))
			{
				if(recordnum >= start)
				{
					EmailLogAuto entity = null;
					if (DataRepository.Provider.UseEntityFactory)
					{
						entity = EntityManager.CreateViewEntity<EmailLogAuto>("EmailLogAuto",  DataRepository.Provider.EntityCreationalFactoryType); 
					}
					else
					{
						entity = new EmailLogAuto();
					}
					
					entity.SuppressEntityEvents = true;

					entity.EmailLogId = (System.Int32)reader[((int)EmailLogAutoColumn.EmailLogId)];
					//entity.EmailLogId = (Convert.IsDBNull(reader["EmailLogId"]))?(int)0:(System.Int32)reader["EmailLogId"];
					entity.EmailLogTypeId = (System.Int32)reader[((int)EmailLogAutoColumn.EmailLogTypeId)];
					//entity.EmailLogTypeId = (Convert.IsDBNull(reader["EmailLogTypeId"]))?(int)0:(System.Int32)reader["EmailLogTypeId"];
					entity.EmailLogTypeDesc = (reader.IsDBNull(((int)EmailLogAutoColumn.EmailLogTypeDesc)))?null:(System.String)reader[((int)EmailLogAutoColumn.EmailLogTypeDesc)];
					//entity.EmailLogTypeDesc = (Convert.IsDBNull(reader["EmailLogTypeDesc"]))?string.Empty:(System.String)reader["EmailLogTypeDesc"];
					entity.EmailDateTime = (System.DateTime)reader[((int)EmailLogAutoColumn.EmailDateTime)];
					//entity.EmailDateTime = (Convert.IsDBNull(reader["EmailDateTime"]))?DateTime.MinValue:(System.DateTime)reader["EmailDateTime"];
					entity.EmailFrom = (reader.IsDBNull(((int)EmailLogAutoColumn.EmailFrom)))?null:(System.String)reader[((int)EmailLogAutoColumn.EmailFrom)];
					//entity.EmailFrom = (Convert.IsDBNull(reader["EmailFrom"]))?string.Empty:(System.String)reader["EmailFrom"];
					entity.EmailTo = (System.String)reader[((int)EmailLogAutoColumn.EmailTo)];
					//entity.EmailTo = (Convert.IsDBNull(reader["EmailTo"]))?string.Empty:(System.String)reader["EmailTo"];
					entity.EmailCc = (reader.IsDBNull(((int)EmailLogAutoColumn.EmailCc)))?null:(System.String)reader[((int)EmailLogAutoColumn.EmailCc)];
					//entity.EmailCc = (Convert.IsDBNull(reader["EmailCc"]))?string.Empty:(System.String)reader["EmailCc"];
					entity.EmailBcc = (reader.IsDBNull(((int)EmailLogAutoColumn.EmailBcc)))?null:(System.String)reader[((int)EmailLogAutoColumn.EmailBcc)];
					//entity.EmailBcc = (Convert.IsDBNull(reader["EmailBcc"]))?string.Empty:(System.String)reader["EmailBcc"];
					entity.EmailLogMessageSubject = (System.String)reader[((int)EmailLogAutoColumn.EmailLogMessageSubject)];
					//entity.EmailLogMessageSubject = (Convert.IsDBNull(reader["EmailLogMessageSubject"]))?string.Empty:(System.String)reader["EmailLogMessageSubject"];
					entity.EmailLogMessageBody = (reader.IsDBNull(((int)EmailLogAutoColumn.EmailLogMessageBody)))?null:(System.Byte[])reader[((int)EmailLogAutoColumn.EmailLogMessageBody)];
					//entity.EmailLogMessageBody = (Convert.IsDBNull(reader["EmailLogMessageBody"]))?new byte[] {}:(System.Byte[])reader["EmailLogMessageBody"];
					entity.SentByUserId = (reader.IsDBNull(((int)EmailLogAutoColumn.SentByUserId)))?null:(System.Int32?)reader[((int)EmailLogAutoColumn.SentByUserId)];
					//entity.SentByUserId = (Convert.IsDBNull(reader["SentByUserId"]))?(int)0:(System.Int32?)reader["SentByUserId"];
					entity.SentByUserFullName = (reader.IsDBNull(((int)EmailLogAutoColumn.SentByUserFullName)))?null:(System.String)reader[((int)EmailLogAutoColumn.SentByUserFullName)];
					//entity.SentByUserFullName = (Convert.IsDBNull(reader["SentByUserFullName"]))?string.Empty:(System.String)reader["SentByUserFullName"];
					entity.AcceptChanges();
					entity.SuppressEntityEvents = false;
					
					rows.Add(entity);
					pageLength -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		
		
		/// <summary>
		/// Refreshes the <see cref="EmailLogAuto"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="EmailLogAuto"/> object to refresh.</param>
		protected void RefreshEntity(IDataReader reader, EmailLogAuto entity)
		{
			reader.Read();
			entity.EmailLogId = (System.Int32)reader[((int)EmailLogAutoColumn.EmailLogId)];
			//entity.EmailLogId = (Convert.IsDBNull(reader["EmailLogId"]))?(int)0:(System.Int32)reader["EmailLogId"];
			entity.EmailLogTypeId = (System.Int32)reader[((int)EmailLogAutoColumn.EmailLogTypeId)];
			//entity.EmailLogTypeId = (Convert.IsDBNull(reader["EmailLogTypeId"]))?(int)0:(System.Int32)reader["EmailLogTypeId"];
			entity.EmailLogTypeDesc = (reader.IsDBNull(((int)EmailLogAutoColumn.EmailLogTypeDesc)))?null:(System.String)reader[((int)EmailLogAutoColumn.EmailLogTypeDesc)];
			//entity.EmailLogTypeDesc = (Convert.IsDBNull(reader["EmailLogTypeDesc"]))?string.Empty:(System.String)reader["EmailLogTypeDesc"];
			entity.EmailDateTime = (System.DateTime)reader[((int)EmailLogAutoColumn.EmailDateTime)];
			//entity.EmailDateTime = (Convert.IsDBNull(reader["EmailDateTime"]))?DateTime.MinValue:(System.DateTime)reader["EmailDateTime"];
			entity.EmailFrom = (reader.IsDBNull(((int)EmailLogAutoColumn.EmailFrom)))?null:(System.String)reader[((int)EmailLogAutoColumn.EmailFrom)];
			//entity.EmailFrom = (Convert.IsDBNull(reader["EmailFrom"]))?string.Empty:(System.String)reader["EmailFrom"];
			entity.EmailTo = (System.String)reader[((int)EmailLogAutoColumn.EmailTo)];
			//entity.EmailTo = (Convert.IsDBNull(reader["EmailTo"]))?string.Empty:(System.String)reader["EmailTo"];
			entity.EmailCc = (reader.IsDBNull(((int)EmailLogAutoColumn.EmailCc)))?null:(System.String)reader[((int)EmailLogAutoColumn.EmailCc)];
			//entity.EmailCc = (Convert.IsDBNull(reader["EmailCc"]))?string.Empty:(System.String)reader["EmailCc"];
			entity.EmailBcc = (reader.IsDBNull(((int)EmailLogAutoColumn.EmailBcc)))?null:(System.String)reader[((int)EmailLogAutoColumn.EmailBcc)];
			//entity.EmailBcc = (Convert.IsDBNull(reader["EmailBcc"]))?string.Empty:(System.String)reader["EmailBcc"];
			entity.EmailLogMessageSubject = (System.String)reader[((int)EmailLogAutoColumn.EmailLogMessageSubject)];
			//entity.EmailLogMessageSubject = (Convert.IsDBNull(reader["EmailLogMessageSubject"]))?string.Empty:(System.String)reader["EmailLogMessageSubject"];
			entity.EmailLogMessageBody = (reader.IsDBNull(((int)EmailLogAutoColumn.EmailLogMessageBody)))?null:(System.Byte[])reader[((int)EmailLogAutoColumn.EmailLogMessageBody)];
			//entity.EmailLogMessageBody = (Convert.IsDBNull(reader["EmailLogMessageBody"]))?new byte[] {}:(System.Byte[])reader["EmailLogMessageBody"];
			entity.SentByUserId = (reader.IsDBNull(((int)EmailLogAutoColumn.SentByUserId)))?null:(System.Int32?)reader[((int)EmailLogAutoColumn.SentByUserId)];
			//entity.SentByUserId = (Convert.IsDBNull(reader["SentByUserId"]))?(int)0:(System.Int32?)reader["SentByUserId"];
			entity.SentByUserFullName = (reader.IsDBNull(((int)EmailLogAutoColumn.SentByUserFullName)))?null:(System.String)reader[((int)EmailLogAutoColumn.SentByUserFullName)];
			//entity.SentByUserFullName = (Convert.IsDBNull(reader["SentByUserFullName"]))?string.Empty:(System.String)reader["SentByUserFullName"];
			reader.Close();
	
			entity.AcceptChanges();
		}
		
		/*
		/// <summary>
		/// Refreshes the <see cref="EmailLogAuto"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="EmailLogAuto"/> object.</param>
		protected static void RefreshEntity(DataSet dataSet, EmailLogAuto entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.EmailLogId = (Convert.IsDBNull(dataRow["EmailLogId"]))?(int)0:(System.Int32)dataRow["EmailLogId"];
			entity.EmailLogTypeId = (Convert.IsDBNull(dataRow["EmailLogTypeId"]))?(int)0:(System.Int32)dataRow["EmailLogTypeId"];
			entity.EmailLogTypeDesc = (Convert.IsDBNull(dataRow["EmailLogTypeDesc"]))?string.Empty:(System.String)dataRow["EmailLogTypeDesc"];
			entity.EmailDateTime = (Convert.IsDBNull(dataRow["EmailDateTime"]))?DateTime.MinValue:(System.DateTime)dataRow["EmailDateTime"];
			entity.EmailFrom = (Convert.IsDBNull(dataRow["EmailFrom"]))?string.Empty:(System.String)dataRow["EmailFrom"];
			entity.EmailTo = (Convert.IsDBNull(dataRow["EmailTo"]))?string.Empty:(System.String)dataRow["EmailTo"];
			entity.EmailCc = (Convert.IsDBNull(dataRow["EmailCc"]))?string.Empty:(System.String)dataRow["EmailCc"];
			entity.EmailBcc = (Convert.IsDBNull(dataRow["EmailBcc"]))?string.Empty:(System.String)dataRow["EmailBcc"];
			entity.EmailLogMessageSubject = (Convert.IsDBNull(dataRow["EmailLogMessageSubject"]))?string.Empty:(System.String)dataRow["EmailLogMessageSubject"];
			entity.EmailLogMessageBody = (Convert.IsDBNull(dataRow["EmailLogMessageBody"]))?new byte[] {}:(System.Byte[])dataRow["EmailLogMessageBody"];
			entity.SentByUserId = (Convert.IsDBNull(dataRow["SentByUserId"]))?(int)0:(System.Int32?)dataRow["SentByUserId"];
			entity.SentByUserFullName = (Convert.IsDBNull(dataRow["SentByUserFullName"]))?string.Empty:(System.String)dataRow["SentByUserFullName"];
			entity.AcceptChanges();
		}
		*/
			
		#endregion Helper Functions
	}//end class

	#region EmailLogAutoFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EmailLogAuto"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EmailLogAutoFilterBuilder : SqlFilterBuilder<EmailLogAutoColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EmailLogAutoFilterBuilder class.
		/// </summary>
		public EmailLogAutoFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the EmailLogAutoFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public EmailLogAutoFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the EmailLogAutoFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public EmailLogAutoFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion EmailLogAutoFilterBuilder

	#region EmailLogAutoParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EmailLogAuto"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EmailLogAutoParameterBuilder : ParameterizedSqlFilterBuilder<EmailLogAutoColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EmailLogAutoParameterBuilder class.
		/// </summary>
		public EmailLogAutoParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the EmailLogAutoParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public EmailLogAutoParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the EmailLogAutoParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public EmailLogAutoParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion EmailLogAutoParameterBuilder
	
	#region EmailLogAutoSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EmailLogAuto"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class EmailLogAutoSortBuilder : SqlSortBuilder<EmailLogAutoColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EmailLogAutoSqlSortBuilder class.
		/// </summary>
		public EmailLogAutoSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion EmailLogAutoSortBuilder

} // end namespace
