﻿<%@ Page Title="Health Check" Language="C#" MasterPageFile="~/MasterPages/popup.master" AutoEventWireup="true" Inherits="ALCOA.CSMS.Website.PopUps.HealthCheckPopup" Codebehind="HealthCheckPopup.aspx.cs" %>

<%@ Register src="~/UserControls/Other/nonCompliance2.ascx" tagname="nonCompliance2" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <%: System.Web.Optimization.Styles.Render("~/bundles/Office2003BlueOld_styles") %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <uc1:nonCompliance2 ID="nonCompliance2Popup" runat="server"/>
</asp:Content>

