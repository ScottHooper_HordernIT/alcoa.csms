﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="UserControls_NonComplianceMT" Codebehind="NonComplianceMT.ascx.cs" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
    <%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"  Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Src="../../Other/ExportButtons.ascx" TagName="ExportButtons" TagPrefix="uc1" %>

<table border="0" cellpadding="2" cellspacing="0" width="100%">
    <tr>
        <td class="pageName" colspan="3" style="height: 16px">
        <span class="bodycopy"><span class="title">Compliance Report</span><br />
                <span class="date"><asp:Label ID="lblMS" runat="server" Text="Medical Schedules"></asp:Label></span><br />
            <img src="images/grfc_dottedline.gif" width="24" height="1"></span></td>
    </tr>
        <tr>
            <td colspan="3" style="height: 28px; text-align: left">
                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                    <tr>
                        <td style="width: 44px; height: 28px; text-align: right">
                            &nbsp;<strong>Year:</strong>&nbsp;</td>
                        <td style="width: 100px; height: 28px">
                            <dxe:ASPxComboBox ID="cmbYear" runat="server" AutoPostBack="True" ClientInstanceName="cmbYear"
                                CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue"
                                IncrementalFilteringMode="StartsWith"
                                OnSelectedIndexChanged="cmbYear_SelectedIndexChanged" ValueType="System.String"
                                Width="55px" 
                                SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                                </LoadingPanelImage>
                                <ButtonStyle Width="13px">
                                </ButtonStyle>
                            </dxe:ASPxComboBox>
                        </td>
                        <td colspan="4" style="height: 28px" align="right">
                            <button name="btnPrint" onclick="javascript:print();" style="width:80px; height:25px; font-weight:bold">Print</button>
                        </td>
                    </tr>
                </table>

            <dxwgv:ASPxGridView ID="grid"
                ClientInstanceName="grid"
                runat="server"
                AutoGenerateColumns="False"
                CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                CssPostfix="Office2003Blue"
                DataSourceID="sqldsNonComplianceReportYearMedical"
                Width="900px"
                OnHtmlRowCreated="grid_RowCreated"
                OnAutoFilterCellEditorInitialize="grid_AutoFilterCellEditorInitialize"
             >
                <Columns>
                    <dxwgv:GridViewDataComboBoxColumn Caption="Company Name" FieldName="CompanyId" Name="CompanyBox"
                        VisibleIndex="0" SortIndex="1" Settings-SortMode="DisplayText" SortOrder="Ascending">
                        <PropertiesComboBox DataSourceID="sqldsCompaniesList" DropDownHeight="150px" TextField="CompanyName"
                            ValueField="CompanyId" ValueType="System.Int32" IncrementalFilteringMode="StartsWith">
                        </PropertiesComboBox>
                        <EditFormSettings Visible="True" />
                        <Settings SortMode="DisplayText" />
                    </dxwgv:GridViewDataComboBoxColumn>
                    
                    <dxwgv:GridViewDataComboBoxColumn Name="gcbResidentialCategory" 
                    Caption="Residential Category" FieldName="CompanySiteCategoryId"
                    SortIndex="1" SortOrder="Ascending" UnboundType="String" VisibleIndex="1" 
                    FixedStyle="Left" Width="145px">
                    <PropertiesComboBox DataSourceID="CompanySiteCategoryDataSource" DropDownHeight="150px" TextField="CategoryDesc"
                        ValueField="CompanySiteCategoryId" ValueType="System.Int32" IncrementalFilteringMode="StartsWith">
                    </PropertiesComboBox>
                    <Settings SortMode="DisplayText" />
                </dxwgv:GridViewDataComboBoxColumn>
                    
                    <dxwgv:GridViewDataComboBoxColumn FieldName="RegionName" ReadOnly="True" 
                        Caption="Region" Name="Region" VisibleIndex="2">
                        <PropertiesComboBox DataSourceID="SqlDsRegions" DropDownHeight="100px" TextField="Region"
                            ValueField="Region" ValueType="System.String" IncrementalFilteringMode="StartsWith">
                        </PropertiesComboBox>
                        <EditFormSettings Visible="True" />
                        <Settings SortMode="DisplayText" />
                    </dxwgv:GridViewDataComboBoxColumn>  
                    
                    <dxwgv:GridViewDataTextColumn FieldName="SiteName" ReadOnly="True" 
                        Caption="Site" Name="SiteName" VisibleIndex="2" SortIndex="2">
                    <Settings AutoFilterCondition="BeginsWith" />
                    </dxwgv:GridViewDataTextColumn> 
                                                    
                    <dxwgv:GridViewDataTextColumn FieldName="Qtr1" ReadOnly="True" Name="Qtr1" Caption="Qtr 1" VisibleIndex="3"><DataItemTemplate>
                     <table cellpadding="0" cellspacing="0"><tr>
                     <td><asp:Image ID="changeImage1" runat="server" ImageUrl="~/Images/spacer.gif" Visible="true" GenerateEmptyAlternateText="True" /></td>
                     <td style="width:30px"><asp:Label ID="noKPISubmitted1" runat="server" Text="" ></asp:Label></td>
                     </tr></table>
                        
</DataItemTemplate>

<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<CellStyle HorizontalAlign="Center"></CellStyle>
</dxwgv:GridViewDataTextColumn>
<dxwgv:GridViewDataTextColumn FieldName="Qtr2" ReadOnly="True" Name="Qtr2" Caption="Qtr 2" VisibleIndex="4"><DataItemTemplate>
                     <table cellpadding="0" cellspacing="0"><tr>
                     <td><asp:Image ID="changeImage2" runat="server" ImageUrl="~/Images/spacer.gif" Visible="true" GenerateEmptyAlternateText="True" /></td>
                     <td style="width:20px"><asp:Label ID="noKPISubmitted2" runat="server" Text="" ></asp:Label></td>
                     </tr></table>
                        
</DataItemTemplate>

<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<CellStyle HorizontalAlign="Center"></CellStyle>
</dxwgv:GridViewDataTextColumn>
<dxwgv:GridViewDataTextColumn FieldName="Qtr3" ReadOnly="True" Name="Qtr3" Caption="Qtr 3" VisibleIndex="5"><DataItemTemplate>
                     <table cellpadding="0" cellspacing="0"><tr>
                     <td><asp:Image ID="changeImage3" runat="server" ImageUrl="~/Images/spacer.gif" Visible="true" GenerateEmptyAlternateText="True" /></td>
                     <td style="width:20px"><asp:Label ID="noKPISubmitted3" runat="server" Text="" ></asp:Label></td>
                     </tr></table>
                        
</DataItemTemplate>

<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<CellStyle HorizontalAlign="Center"></CellStyle>
</dxwgv:GridViewDataTextColumn>

<dxwgv:GridViewDataTextColumn FieldName="Qtr4" ReadOnly="True" Name="Qtr4" Caption="Qtr 4" VisibleIndex="6"><DataItemTemplate>
                     <table cellpadding="0" cellspacing="0"><tr>
                     <td><asp:Image ID="changeImage4" runat="server" ImageUrl="~/Images/spacer.gif" Visible="true" GenerateEmptyAlternateText="True" /></td>
                     <td style="width:20px"><asp:Label ID="noKPISubmitted4" runat="server" Text="" ></asp:Label></td>
                     </tr></table>
                        
</DataItemTemplate>

<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<CellStyle HorizontalAlign="Center"></CellStyle>
</dxwgv:GridViewDataTextColumn>
<dxwgv:GridViewDataTextColumn FieldName="Year" ReadOnly="True" Name="Year" VisibleIndex="15"><DataItemTemplate>
                     <table cellpadding="0" cellspacing="0"><tr>
                     <td style="width:20px"><asp:Label ID="noKPISubmitted13" runat="server" Text="" ></asp:Label></td>
                     </tr></table>
                        
</DataItemTemplate>

<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<CellStyle HorizontalAlign="Center"></CellStyle>
</dxwgv:GridViewDataTextColumn>

                </Columns>
                 <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                    <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
                    </LoadingPanelOnStatusBar>
                    <LoadingPanel Url="~/App_Themes/Office2003BlueOld/GridView/Loading.gif"><%--Enhancement_023:change by debashis for testing platform upgradation--%>
                    </LoadingPanel>
                </Images>
                <Settings ShowGroupPanel="True" ShowFilterRow="True" />
                <SettingsPager PageSize="20" AlwaysShowPager="True">
                    <AllButton Visible="True">
                    </AllButton>
                    <NextPageButton>
                    </NextPageButton>
                    <PrevPageButton>
                    </PrevPageButton>
                </SettingsPager>
                <SettingsCustomizationWindow Enabled="True" />
                <ImagesFilterControl>
                    <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
                    </LoadingPanel>
                </ImagesFilterControl>
                <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
                    CssPostfix="Office2003Blue">
                    <Header ImageSpacing="5px" SortingImageSpacing="5px">
                    </Header>
                    <AlternatingRow Enabled="True">
                    </AlternatingRow>
                    <LoadingPanel ImageSpacing="10px">
                    </LoadingPanel>
                </Styles>
                <SettingsBehavior ConfirmDelete="True"/>
                <SettingsCookies CookiesID="NonComplianceMedical" Version="0.2"/>
                <StylesEditors>
                    <ProgressBar Height="25px">
                    </ProgressBar>
                </StylesEditors>
            </dxwgv:ASPxGridView>
                <a href="javascript:ShowHideCustomizationWindow()">
                        <span style="color: #0000ff; text-decoration: underline"></span>
                </a>
            </td>
        </tr>
    </table>
    <table width="900px">
<tr align="right">
        <td align="right" class="pageName" style="height: 30px; text-align: right; text-align:-moz-right; width: 900px;">
            <uc1:ExportButtons ID="ucExportButtons" runat="server" />
        </td>
</tr>
</table> 
<asp:SqlDataSource ID="sqldsNonComplianceReportYearMedical" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="_FileDbMedicalTraining_ComplianceReport" SelectCommandType="StoredProcedure">
    <SelectParameters>
        <asp:SessionParameter DefaultValue="2008" Name="YEAR" SessionField="spVar_Year" Type="Int32" />
        <asp:SessionParameter DefaultValue="MS" Name="Type" SessionField="MS" Type="String" />
    </SelectParameters>
</asp:SqlDataSource>

<asp:SqlDataSource ID="sqldsKPIYear" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    ProviderName="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString.ProviderName %>"
    SelectCommand="SELECT DISTINCT(YEAR(kpiDateTime)) As [Year] FROM dbo.KPI ORDER BY [Year] ASC">
</asp:SqlDataSource>

<asp:SqlDataSource ID="SqlDsRegions" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    ProviderName="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString.ProviderName %>"
    SelectCommand="Select 'Victorian Operations' Region union Select 'Western Australian Operations' Region ">
</asp:SqlDataSource>

<asp:SqlDataSource ID="sqldsCompaniesList" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="_Companies_GetCompanyNameList" SelectCommandType="StoredProcedure">
</asp:SqlDataSource>

        <data:CompanySiteCategoryDataSource ID="CompanySiteCategoryDataSource" runat="server" SelectMethod="GetPaged"
            EnablePaging="True" EnableSorting="True">
        </data:CompanySiteCategoryDataSource>
        
        <data:SitesDataSource ID="dsSitesFilter" runat="server" Filter="IsVisible = True" Sort="SiteName ASC"></data:SitesDataSource>     