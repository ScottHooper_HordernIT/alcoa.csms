﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface IQuestionnaireMainResponseService
    {
        QuestionnaireMainResponse Get(Expression<Func<QuestionnaireMainResponse, bool>> where, List<Expression<Func<QuestionnaireMainResponse, object>>> includes);
        List<QuestionnaireMainResponse> GetMany(int? top, Expression<Func<QuestionnaireMainResponse, bool>> where, Func<IQueryable<QuestionnaireMainResponse>, IOrderedQueryable<QuestionnaireMainResponse>> orderBy, List<Expression<Func<QuestionnaireMainResponse, object>>> includes);
        List<QuestionnaireMainResponse> GetManyPaged(int pageNumber, int pageSize, Expression<Func<QuestionnaireMainResponse, bool>> where, Func<IQueryable<QuestionnaireMainResponse>, IOrderedQueryable<QuestionnaireMainResponse>> orderBy, List<Expression<Func<QuestionnaireMainResponse, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<QuestionnaireMainResponse, bool>> where, List<Expression<Func<QuestionnaireMainResponse, object>>> includes);
        void Insert(QuestionnaireMainResponse item, bool saveChanges = true);
        void Update(QuestionnaireMainResponse item, bool saveChanges = true);
        void Delete(QuestionnaireMainResponse item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class QuestionnaireMainResponseService : IQuestionnaireMainResponseService
    {
        private readonly IRepository<QuestionnaireMainResponse> _repository;

        public QuestionnaireMainResponseService(IRepository<QuestionnaireMainResponse> repository)
        {
            this._repository = repository;
        }

        public QuestionnaireMainResponse Get(Expression<Func<QuestionnaireMainResponse, bool>> where, List<Expression<Func<QuestionnaireMainResponse, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<QuestionnaireMainResponse> GetMany(int? top, Expression<Func<QuestionnaireMainResponse, bool>> where, Func<IQueryable<QuestionnaireMainResponse>, IOrderedQueryable<QuestionnaireMainResponse>> orderBy, List<Expression<Func<QuestionnaireMainResponse, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<QuestionnaireMainResponse> GetManyPaged(int pageNumber, int pageSize, Expression<Func<QuestionnaireMainResponse, bool>> where, Func<IQueryable<QuestionnaireMainResponse>, IOrderedQueryable<QuestionnaireMainResponse>> orderBy, List<Expression<Func<QuestionnaireMainResponse, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<QuestionnaireMainResponse, bool>> where, List<Expression<Func<QuestionnaireMainResponse, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(QuestionnaireMainResponse item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("QuestionnaireMainResponse");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(QuestionnaireMainResponse item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("QuestionnaireMainResponse");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(QuestionnaireMainResponse item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("QuestionnaireMainResponse");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
