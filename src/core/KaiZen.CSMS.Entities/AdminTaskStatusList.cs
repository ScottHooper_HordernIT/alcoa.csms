﻿
using System;

namespace KaiZen.CSMS.Entities
{
	/// <summary>
	/// An enum representation of the 'AdminTaskStatus' table. [No description found in the database]
	/// </summary>
	/// <remark>this enumeration contains the items contained in the table AdminTaskStatus</remark>
	[Serializable, Flags]
	public enum AdminTaskStatusList
	{
		/// <summary> 
		/// Open
		/// </summary>
		[EnumTextValue(@"Open")]
		Open = 1, 

		/// <summary> 
		/// Closed
		/// </summary>
		[EnumTextValue(@"Closed")]
		Closed = 2

	}
}
