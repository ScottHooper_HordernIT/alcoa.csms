﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Adminv2_UserControls_News" Codebehind="News.ascx.cs" %>

<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
    
<%@ Register assembly="DevExpress.Web.ASPxHtmlEditor.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxHtmlEditor" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.ASPxSpellChecker.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxSpellChecker" tagprefix="dx" %>
<p>
    <dxe:ASPxButton ID="ASPxButton1" runat="server" 
        CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
        CssPostfix="Office2003Blue" onclick="ASPxButton1_Click" 
        SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" Text="Save">
    </dxe:ASPxButton>
</p>
<p>
    <dxe:ASPxLabel ID="lblNewsSaveStatus" runat="server" ForeColor="Red"  ClientEnabled="true" ClientInstanceName="lbl"
        Text="Not Saved Yet.">
    </dxe:ASPxLabel>
    <br />
</p>
<dx:ASPxHtmlEditor ID="ASPxHtmlEditor1" runat="server" 
    CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
    CssPostfix="Office2003Blue" Height="580px" Width="689px">
    <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
        CssPostfix="Office2003Blue">
        <ViewArea>
            <Border BorderColor="#002D96" />
        </ViewArea>
    </Styles>
    <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
        <LoadingPanel Url="~/App_Themes/Office2003Blue/HtmlEditor/Loading.gif">
        </LoadingPanel>
    </Images>
    <ImagesFileManager>
        <FolderContainerNodeLoadingPanel Url="~/App_Themes/Office2003Blue/Web/tvNodeLoading.gif">
        </FolderContainerNodeLoadingPanel>
        <LoadingPanel Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
        </LoadingPanel>
    </ImagesFileManager>
<SettingsImageSelector>
<CommonSettings AllowedFileExtensions=".jpe, .jpeg, .jpg, .gif, .png"></CommonSettings>
</SettingsImageSelector>

    <ClientSideEvents HtmlChanged="function(s, e) {
    var test = document.getElementById('ctl00_body_ctl00_lblNewsSaveStatus');
    test.innerHtml = 'Text Changed. Not Saved Yet';
}" />

<SettingsImageUpload>
<ValidationSettings AllowedFileExtensions=".jpe, .jpeg, .jpg, .gif, .png"></ValidationSettings>
</SettingsImageUpload>

<SettingsDocumentSelector>
<CommonSettings AllowedFileExtensions=".rtf, .pdf, .doc, .docx, .odt, .txt, .xls, .xlsx, .ods, .ppt, .pptx, .odp"></CommonSettings>
</SettingsDocumentSelector>
</dx:ASPxHtmlEditor>
        
<data:News2DataSource
    ID="News2DataSource"
    runat="server"
	SelectMethod="GetPaged"
	EnablePaging="True"
	EnableSorting="True">
</data:News2DataSource>