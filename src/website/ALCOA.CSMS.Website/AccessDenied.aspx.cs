﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using KaiZen.CSMS.Entities;

public partial class AccessDenied : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //CheckIfOffline();

        Configuration configuration = new Configuration();
        ASPxHyperLinkContact.NavigateUrl = "mailto:" + configuration.GetValue(ConfigList.ContactEmail);

        string error = Request.QueryString["error"];
        if (!String.IsNullOrEmpty(error))
        {
            Label1.Text = error;
            //Log Error?!
            if (error == "Not Logged In.")
            {
                Label1.Text = "You are not logged in.<br />" +
                            "If you have logged in and been inactive for over 30 minutes you will have to re-login for security reasons.";
            }
        }
        else
        {
            Label1.Text = "No Error Specified.";
        }
    }

    protected void CheckIfOffline()
    {
        bool isOffline = false;

        Configuration configuration = new Configuration();
        if (configuration.GetValue(ConfigList.SiteOfflineStatus) == "1")
        {
            isOffline = true;
        }

        if (isOffline == true)
        {
            Response.Redirect("~/SiteDown.aspx");
        }
    }
}
