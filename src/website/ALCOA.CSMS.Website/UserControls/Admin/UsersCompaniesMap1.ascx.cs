﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;
using DevExpress.Data.Filtering;
using KaiZen.CSMS.Services;
using KaiZen.CSMS.Entities;
using DevExpress.Web.ASPxClasses;

namespace ALCOA.CSMS.Website.UserControls.Admin
{
    public partial class UsersCompaniesMap1 : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                
            }
        }

        protected void ASPxGridView1_RowValidating(object sender, DevExpress.Web.Data.ASPxDataValidationEventArgs e)
        {
            foreach (GridViewColumn column in ASPxGridView1.Columns)
            {
                GridViewDataColumn dataColumn = column as GridViewDataColumn;
                if (dataColumn == null) continue;
                if (dataColumn.FieldName == "UserLogon")
                    dataColumn.EditFormSettings.Visible = DevExpress.Utils.DefaultBoolean.False;
            }

        }

        protected void ASPxGridView1_StartRowEditing(object sender, DevExpress.Web.Data.ASPxStartRowEditingEventArgs e)
        {
            // Validates the edited row if it isn't a new row,.
            if (!ASPxGridView1.IsNewRowEditing)
                ASPxGridView1.DoRowValidation();
        }


        protected void grid_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
        {
            try
            {
                int _userCompanyId = Convert.ToInt32(e.Keys["UserCompanyId"]);
                int _userId = Convert.ToInt32(e.OldValues["UserId"]);
                int _companyId = Convert.ToInt32(e.NewValues["CompanyId"]);
                bool _active = Convert.ToBoolean(e.OldValues["Active"]);
                UserCompanyService ucser = new UserCompanyService();
                KaiZen.CSMS.Entities.UserCompany uc1 = ucser.GetByUserCompanyId(_userCompanyId);

                TList<KaiZen.CSMS.Entities.UserCompany> ucList = ucser.GetByUserId(uc1.UserId);
                int isInsert = 0;

                foreach (UserCompany uc in ucList)
                {
                    if (uc.CompanyId == _companyId)
                    {
                        isInsert++;
                    }
                }
                if (isInsert == 0)
                {
                    ods.UpdateParameters.Add("UserCompanyId", TypeCode.Int32, _userCompanyId.ToString());
                    ods.UpdateParameters.Add("UserId", TypeCode.Int32, _userId.ToString());
                    ods.UpdateParameters.Add("CompanyId", TypeCode.Int32, _companyId.ToString());
                    ods.UpdateParameters.Add("Active", TypeCode.Boolean, _active.ToString());
                    ods.Update();                   
                }
                else
                {                    
                    e.Cancel = true;
                    //ASPxGridView1.CancelEdit();
                    throw new Exception("This User/Company Mapping is already exists!");
                    //ASPxGridView1.DataBind();

                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);

            }

        }
        protected void grid_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
        {
            try
            {
                GridViewDataTextColumn col = ((ASPxGridView)sender).Columns[1] as GridViewDataTextColumn;
                ASPxComboBox ddl1 = (ASPxComboBox)((ASPxGridView)sender).FindEditRowCellTemplateControl(col, "cmbUsers");

                

                ASPxComboBox ddl = ((ASPxComboBox)ASPxGridView1.FindEditRowCellTemplateControl(ASPxGridView1.Columns["UserLogon"] as GridViewDataTextColumn, "cmbUsers"));


                UsersService uSer = new UsersService();
                Users u = uSer.GetByUserLogon(ddl.Value.ToString());
                if (u != null)
                {

                    int _userId = u.UserId ;
                    int _companyId = Convert.ToInt32(e.NewValues["CompanyId"]);
                    bool _active = Convert.ToBoolean(e.NewValues["Active"]);
                    ods.InsertParameters.Add("UserId", TypeCode.Int32, _userId.ToString());
                    ods.InsertParameters.Add("CompanyId", TypeCode.Int32, _companyId.ToString());
                    ods.InsertParameters.Add("Active", TypeCode.Boolean, _active.ToString());
                    ods.Insert();
                }

            }
            catch (Exception ex)
            {

            }

        }

        protected void grid_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
        {
            try
            {
                int _userCompanyId = Convert.ToInt32(e.Keys["UserCompanyId"]);

                ods.DeleteParameters.Add("UserCompanyId", TypeCode.Int32, _userCompanyId.ToString());
               
                ods.Delete();

            }
            catch (Exception ex)
            {

            }

        }

        protected void grid_ProcessColumnAutoFilter(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewAutoFilterEventArgs e)
        {

            int filterCount = 0;
            string whereCondition = string.Empty;

            if (e.Column.FieldName == "UserId")
            {
                if (e.Kind == DevExpress.Web.ASPxGridView.GridViewAutoFilterEventKind.CreateCriteria)
                {
                    e.Criteria = new OperandProperty("UserId") == e.Value.ToString();
                    Session["UserIdVal"] = e.Value.ToString();
                    filterCount++;
                    Session["filterCount"] = filterCount;
                    ASPxGridView1.PageIndex = 0; 
                }
            }

            if (e.Column.FieldName == "CompanyId")
            {
                if (e.Kind == DevExpress.Web.ASPxGridView.GridViewAutoFilterEventKind.CreateCriteria)
                {
                    e.Criteria = new OperandProperty("CompanyId") == e.Value.ToString();
                    Session["CompanyIdVal"] = e.Value.ToString();
                    filterCount++;
                    Session["filterCount"] = filterCount;
                    ASPxGridView1.PageIndex = 0;
                }
            }

            

        }

        protected void grid_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
        {
            if (e.CallbackName == "APPLYFILTER")
            {
                Session["UserIdVal"] = null;
                Session["CompanyIdVal"] = null;

                ASPxGridView1.DataBind();
            }
        }


        protected void btnSave_Click(object sender, EventArgs e)
        {
            UserCompanyService ucser = new UserCompanyService();
            //KaiZen.CSMS.Entities.ContractReviewsRegister cont = conser.GetByCompanyIdSiteIdYear(_companyId, _siteId, _year);

            TList<KaiZen.CSMS.Entities.UserCompany> ucList = ucser.GetByUserId(Convert.ToInt32(cmbUsers.Value));
            int isInsert = 0;

            foreach (UserCompany uc in ucList)
            {
                if (uc.CompanyId == Convert.ToInt32(cmbCompanies.Value))
                {
                    isInsert++;
                }
            }
            if (isInsert == 0)
            {

                int _userId = Convert.ToInt32(cmbUsers.Value);
                int _companyId = Convert.ToInt32(cmbCompanies.Value);
                bool _active = false;
                ods.InsertParameters.Add("UserId", TypeCode.Int32, _userId.ToString());
                ods.InsertParameters.Add("CompanyId", TypeCode.Int32, _companyId.ToString());
                ods.InsertParameters.Add("Active", TypeCode.Boolean, _active.ToString());
                ods.Insert();

                lblMessage.Visible = false;
                lblMessage.Text = "";

                Response.Redirect("Admin.aspx?s=UsersCompaniesMap1", true);

            }
            else
            {
                lblMessage.Visible = true;
                lblMessage.Text = "The selected company is already mapped against the selected user.<br/> Please select a different company (or user).";
            }
            //cmbCompanies.SelectedIndex = 0;
            //cmbUsers.SelectedIndex = 0;
        }


        protected void ASPxComboBox_OnItemsRequestedByFilterCondition_SQL(object source, ListEditItemsRequestedByFilterConditionEventArgs e)
        {
            ASPxComboBox comboBox = (ASPxComboBox)source;
            string filter = "";
            
            if (e.Filter == "") { filter = "'%'"; }
            else { filter = "'%" + e.Filter + "%'"; }

            sqldaUsersList.SelectCommand =
                   @"SELECT top 25 UserId, UserLogon from [Users]  where UserLogon LIKE " + filter;

            comboBox.DataSource = sqldaUsersList;
            comboBox.DataBind();
        }

        protected void ASPxComboBox_OnItemRequestedByValue_SQL(object source, ListEditItemRequestedByValueEventArgs e)
        {
            long value = 0;
            //if (!Int64.TryParse(e.Value.ToString(), out value))
            //    return;
            ASPxComboBox comboBox = (ASPxComboBox)source;


            sqldaUsersList.SelectCommand = @"SELECT top 25 UserId, UserLogon FROM Users ORDER BY UserLogon";

            sqldaUsersList.SelectParameters.Clear();
           // sqldaUsersList.SelectParameters.Add("UserLogon", TypeCode.String, e.Value.ToString());
            comboBox.DataSource = sqldaUsersList;
            comboBox.DataBind();
        }

        protected void FilterMinLengthSpinEdit_ValueChanged(object sender, EventArgs e)
        {
            cmbUsers.FilterMinLength = (int)((ASPxSpinEdit)sender).Number;
        }

        string SectionParameter
        {
            get
            {
                return Page.Request.QueryString["Section"];
            }
        }




        
    }
}