﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface IEbiMetricService
    {
        EbiMetric Get(Expression<Func<EbiMetric, bool>> where, List<Expression<Func<EbiMetric, object>>> includes);
        List<EbiMetric> GetMany(int? top, Expression<Func<EbiMetric, bool>> where, Func<IQueryable<EbiMetric>, IOrderedQueryable<EbiMetric>> orderBy, List<Expression<Func<EbiMetric, object>>> includes);
        List<EbiMetric> GetManyPaged(int pageNumber, int pageSize, Expression<Func<EbiMetric, bool>> where, Func<IQueryable<EbiMetric>, IOrderedQueryable<EbiMetric>> orderBy, List<Expression<Func<EbiMetric, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<EbiMetric, bool>> where, List<Expression<Func<EbiMetric, object>>> includes);
        void Insert(EbiMetric item, bool saveChanges = true);
        void Update(EbiMetric item, bool saveChanges = true);
        void Delete(EbiMetric item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class EbiMetricService : IEbiMetricService
    {
        private readonly IRepository<EbiMetric> _repository;

        public EbiMetricService(IRepository<EbiMetric> repository)
        {
            this._repository = repository;
        }

        public EbiMetric Get(Expression<Func<EbiMetric, bool>> where, List<Expression<Func<EbiMetric, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<EbiMetric> GetMany(int? top, Expression<Func<EbiMetric, bool>> where, Func<IQueryable<EbiMetric>, IOrderedQueryable<EbiMetric>> orderBy, List<Expression<Func<EbiMetric, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<EbiMetric> GetManyPaged(int pageNumber, int pageSize, Expression<Func<EbiMetric, bool>> where, Func<IQueryable<EbiMetric>, IOrderedQueryable<EbiMetric>> orderBy, List<Expression<Func<EbiMetric, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<EbiMetric, bool>> where, List<Expression<Func<EbiMetric, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(EbiMetric item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("EbiMetric");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(EbiMetric item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("EbiMetric");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(EbiMetric item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("EbiMetric");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
