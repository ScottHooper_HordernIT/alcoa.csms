<%@ Control Language="C#" AutoEventWireup="true"
    Inherits="UserControls_Tiny_QuestionnaireReports_QuestionnaireReport_Expire" Codebehind="QuestionnaireReport_Expire.ascx.cs" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Src="../../Other/ExportButtons.ascx" TagName="ExportButtons" TagPrefix="uc1" %>
<table border="0" cellpadding="2" cellspacing="0" width="100%">
    <tr>
        <td class="pageName" colspan="3" style="height: 17px">
            <a name="save"></a><span class="bodycopy"><span class="title">Reports</span><br />
                <span class="date">Safety Qualification &gt; Overdue or Within 30/60 days of Expiry</span><br />
                <img height="1" src="images/grfc_dottedline.gif" width="24" />&nbsp;</span>
        </td>
    </tr>
</table>
<asp:UpdatePanel ID="UpdatePanel1" runat="server" ChildrenAsTriggers="False" RenderMode="Inline"
    UpdateMode="Conditional">
    <ContentTemplate>
        <table style="width: 100%" cellspacing="0" cellpadding="0" border="0">
            <tbody>
                <tr>
                    <td style="width: 100%; padding-bottom: 3px">
                        <table cellspacing="0" cellpadding="0" border="0">
                            <tbody>
                                <tr>
                                    <td style="width: 60px; height: 13px">
                                        Filter By:
                                    </td>
                                    <td style="width: 100px; height: 13px">
                                        <dxe:ASPxComboBox ID="ASPxComboBox1" runat="server" AutoPostBack="True" ValueType="System.Int32"
                                            SelectedIndex="1" OnSelectedIndexChanged="ASPxComboBox1_SelectedIndexChanged"
                                            CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                            SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                            <Items>
                                                <dxe:ListEditItem Text="Within 30 Days of Expiry" Value="1"></dxe:ListEditItem>
                                                <dxe:ListEditItem Text="Within 60 Days of Expiry" Value="2"></dxe:ListEditItem>
                                                <dxe:ListEditItem Text="Expired" Value="3"></dxe:ListEditItem>
                                                <dxe:ListEditItem Text="(No Filter)" Value="0"></dxe:ListEditItem>
                                            </Items>
                                            <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                                            </LoadingPanelImage>
                                            <ButtonStyle Width="13px">
                                            </ButtonStyle>
                                        </dxe:ASPxComboBox>
                                    </td>
                                    <td style="width: 740px">
                                    <div align="right">
                                        <a style="color: #0000ff; text-align: right; text-decoration: none" href="javascript:ShowHideCustomizationWindow()">
                                            Customize</a>
                                    </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="width: 100%; height: 70px">
                        <dxwgv:ASPxGridView ID="grid" runat="server" CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                            OnHtmlRowCreated="grid_RowCreated" Width="900px" KeyFieldName="QuestionnaireId"
                            DataSourceID="QuestionnaireReportExpiryDataSource" AutoGenerateColumns="False"
                            ClientInstanceName="grid" OnAutoFilterCellEditorInitialize="grid_AutoFilterCellEditorInitialize">
                            <SettingsCustomizationWindow Enabled="True"></SettingsCustomizationWindow>
                            <ImagesFilterControl>
                                <LoadingPanel Url="~/App_Themes/Office2003BlueOld/Editors/Loading.gif">
                                </LoadingPanel>
                            </ImagesFilterControl>
                            <Styles CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css">
                                <Header SortingImageSpacing="5px" ImageSpacing="5px">
                                </Header>
                                <AlternatingRow Enabled="True">
                                </AlternatingRow>
                                <LoadingPanel ImageSpacing="10px">
                                </LoadingPanel>
                            </Styles>
                            <SettingsPager AlwaysShowPager="True">
                                <AllButton Visible="True">
                                </AllButton>
                            </SettingsPager>
                            <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif"> 
                                </LoadingPanelOnStatusBar>
                                <CollapsedButton Width="11px">
                                </CollapsedButton>
                                <ExpandedButton Width="11px">
                                </ExpandedButton>
                                <DetailCollapsedButton Width="11px">
                                </DetailCollapsedButton>
                                <DetailExpandedButton Width="11px">
                                </DetailExpandedButton>
                                <FilterRowButton Width="13px">
                                </FilterRowButton>
                                <LoadingPanel Url="~/App_Themes/Office2003BlueOld/GridView/Loading.gif"> <%--Enhancement_023:change by debashis for testing platform upgradation--%>
                                </LoadingPanel>
                            </Images>
                            <Columns>
                                <dxwgv:GridViewDataTextColumn FieldName="QuestionnaireId" ReadOnly="True" Caption=" "
                                    VisibleIndex="0" Width="50px">
                                    <Settings AllowHeaderFilter="False"></Settings>
                                    <EditFormSettings Visible="False"></EditFormSettings>
                                    <DataItemTemplate>
                                        <table cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td align="center">
                                                    <dxe:ASPxHyperLink ID="hlReQualify" runat="server" Text="Review" Visible="False"
                                                        EncodeHtml="false" ClientEnabled="true" ClientInstanceName="hlrq" ClientSideEvents-Click="function(s,e) {
           
            }" />
                                                    <%--  if (confirm(&quot;Are you sure? &quot;)) {
                location.href =  hlrq.GetNavigateUrl();
            } else {
                if (!e.htmlEvent)
                    if (window.event)
                        e.htmlEvent = window.event;
                else
                    return;

                if (e.htmlEvent.cancelBubble != null)
                    e.htmlEvent.cancelBubble = true;
                if (e.htmlEvent.stopPropagation)
                    e.htmlEvent.stopPropagation();
                if (e.htmlEvent.preventDefault)
                    e.htmlEvent.preventDefault();
                if (window.event)
                    e.htmlEvent.returnValue = false;
                if (e.htmlEvent.cancel != null)
                    e.htmlEvent.cancel = true;
            }--%>
                                                    <asp:Label ID="lblNa" runat="server" Text="-" Visible="True" />
                                                </td>
                                            </tr>
                                        </table>
                                    </DataItemTemplate>
                                </dxwgv:GridViewDataTextColumn>
                                <dxwgv:GridViewDataComboBoxColumn FieldName="CompanyName"
                                    Caption="Company" VisibleIndex="1">
                                    <Settings SortMode="DisplayText" />
                                </dxwgv:GridViewDataComboBoxColumn>
                                <dxwgv:GridViewDataComboBoxColumn Caption="Procurement Contact" FieldName="ProcurementContactUser"
                                    VisibleIndex="2" Width="155px">
                                </dxwgv:GridViewDataComboBoxColumn>
                                <dxwgv:GridViewDataComboBoxColumn FieldName="SupplierContact" Caption="Supplier Contact"
                                    VisibleIndex="3">
                                </dxwgv:GridViewDataComboBoxColumn>
                                 <dxwgv:GridViewDataTextColumn FieldName="SupplierContactPhone" Visible="False" Width="110px">
                                        <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                                </dxwgv:GridViewDataTextColumn>
                                 <dxwgv:GridViewDataTextColumn FieldName="SupplierContactEmail" Visible="False" Width="110px">
                                        <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                                </dxwgv:GridViewDataTextColumn>
                                <dxwgv:GridViewDataComboBoxColumn Caption="H&amp;S Assessor" FieldName="SafetyAssessor"
                                    VisibleIndex="4" Width="110px">
                                </dxwgv:GridViewDataComboBoxColumn>
                                <dxwgv:GridViewDataDateColumn FieldName="MainAssessmentValidTo" SortIndex="0" SortOrder="Ascending"
                                    Caption="Valid To" VisibleIndex="5" Width="88px">
                                </dxwgv:GridViewDataDateColumn>
                                <dxwgv:GridViewDataTextColumn FieldName="ExpiresIn" Caption="Expires in" ToolTip="Days Left Till Expiry"
                                    VisibleIndex="6" Width="90px">
                                    <Settings SortMode="DisplayText"></Settings>
                                    <DataItemTemplate>
                                        <table cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblExpiresIn" runat="server" Text=""></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </DataItemTemplate>
                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    <CellStyle HorizontalAlign="Center">
                                    </CellStyle>
                                </dxwgv:GridViewDataTextColumn>
                                <dxwgv:GridViewDataComboBoxColumn FieldName="CreatedByUser" Caption="Created By"
                                    VisibleIndex="2" Visible="False">
                                </dxwgv:GridViewDataComboBoxColumn>
                                <dxwgv:GridViewDataDateColumn FieldName="CreatedDate" VisibleIndex="4" Visible="False">
                                </dxwgv:GridViewDataDateColumn>
                                <dxwgv:GridViewDataDateColumn FieldName="ModifiedDate" Caption="Submitted Date" VisibleIndex="4"
                                    Visible="False">
                                </dxwgv:GridViewDataDateColumn>
                                <dxwgv:GridViewDataComboBoxColumn FieldName="Status" VisibleIndex="4" Visible="False">
                                    <PropertiesComboBox DataSourceID="QuestionnaireStatusDataSource" TextField="QuestionnaireStatusDesc"
                                        ValueField="QuestionnaireStatusId" ValueType="System.Int32">
                                    </PropertiesComboBox>
                                </dxwgv:GridViewDataComboBoxColumn>
                                <dxwgv:GridViewDataTextColumn FieldName="CompanyStatusDesc" Caption="Process Status"
                                    Visible="False">
                                    <HeaderStyle Wrap="True"></HeaderStyle>
                                </dxwgv:GridViewDataTextColumn>
                            </Columns>
                            <Settings ShowTitlePanel="True" ShowHeaderFilterButton="True" ShowGroupPanel="True"
                                ShowVerticalScrollBar="True" VerticalScrollableHeight="255"></Settings>
                            <StylesEditors>
                                <ProgressBar Height="25px">
                                </ProgressBar>
                            </StylesEditors>
                            <SettingsBehavior ColumnResizeMode="Control" />
                        </dxwgv:ASPxGridView>
                        <data:QuestionnaireReportExpiryDataSource ID="QuestionnaireReportExpiryDataSource"
                            runat="server" SelectMethod="GetPaged" Filter="">
                        </data:QuestionnaireReportExpiryDataSource>
                        <data:UsersFullNameDataSource ID="UsersFullNameDataSource" runat="server">
                        </data:UsersFullNameDataSource>
                        <data:QuestionnaireStatusDataSource ID="QuestionnaireStatusDataSource" runat="server">
                            <DeepLoadProperties Method="IncludeChildren" Recursive="False">
                            </DeepLoadProperties>
                        </data:QuestionnaireStatusDataSource>
                    </td>
                </tr>
            </tbody>
        </table>
    </ContentTemplate>
    <Triggers>
        <asp:PostBackTrigger ControlID="ASPxComboBox1"></asp:PostBackTrigger>
    </Triggers>
</asp:UpdatePanel>
<table>
    <tr align="right">
        <td align="right" class="pageName" colspan="7" style="height: 30px; text-align: right;
            text-align: -moz-right; width: 900px;">
            <div align="right">
                <uc1:ExportButtons ID="ucExportButtons" runat="server" />
            </div>
        </td>
    </tr>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
        SelectCommand="SELECT DISTINCT C.CompanyId, C.CompanyName FROM QuestionnaireReportExpiry As Q INNER JOIN Companies As C ON Q.CompanyId = C.CompanyId ORDER BY C.CompanyName ASC">
    </asp:SqlDataSource>
    <data:UsersFullNameDataSource ID="UsersFullNameDataSource1" runat="server" Sort="UserFullName ASC">
    </data:UsersFullNameDataSource>
    <data:UsersEhsConsultantsDataSource ID="UsersEhsConsultants1" runat="server" Sort="UserFullName ASC">
    </data:UsersEhsConsultantsDataSource>
