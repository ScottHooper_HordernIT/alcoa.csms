﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.EnumQuestionnaireMainAnswerProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[CLSCompliant(true)]
	[Designer(typeof(EnumQuestionnaireMainAnswerDataSourceDesigner))]
	public class EnumQuestionnaireMainAnswerDataSource : ReadOnlyDataSource<EnumQuestionnaireMainAnswer>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EnumQuestionnaireMainAnswerDataSource class.
		/// </summary>
		public EnumQuestionnaireMainAnswerDataSource() : base(new EnumQuestionnaireMainAnswerService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the EnumQuestionnaireMainAnswerDataSourceView used by the EnumQuestionnaireMainAnswerDataSource.
		/// </summary>
		protected EnumQuestionnaireMainAnswerDataSourceView EnumQuestionnaireMainAnswerView
		{
			get { return ( View as EnumQuestionnaireMainAnswerDataSourceView ); }
		}
		
		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the EnumQuestionnaireMainAnswerDataSourceView class that is to be
		/// used by the EnumQuestionnaireMainAnswerDataSource.
		/// </summary>
		/// <returns>An instance of the EnumQuestionnaireMainAnswerDataSourceView class.</returns>
		protected override BaseDataSourceView<EnumQuestionnaireMainAnswer, Object> GetNewDataSourceView()
		{
			return new EnumQuestionnaireMainAnswerDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the EnumQuestionnaireMainAnswerDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class EnumQuestionnaireMainAnswerDataSourceView : ReadOnlyDataSourceView<EnumQuestionnaireMainAnswer>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EnumQuestionnaireMainAnswerDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the EnumQuestionnaireMainAnswerDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public EnumQuestionnaireMainAnswerDataSourceView(EnumQuestionnaireMainAnswerDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal EnumQuestionnaireMainAnswerDataSource EnumQuestionnaireMainAnswerOwner
		{
			get { return Owner as EnumQuestionnaireMainAnswerDataSource; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal EnumQuestionnaireMainAnswerService EnumQuestionnaireMainAnswerProvider
		{
			get { return Provider as EnumQuestionnaireMainAnswerService; }
		}

		#endregion Properties
		
		#region Methods
		
		#endregion Methods
	}

	#region EnumQuestionnaireMainAnswerDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the EnumQuestionnaireMainAnswerDataSource class.
	/// </summary>
	public class EnumQuestionnaireMainAnswerDataSourceDesigner : ReadOnlyDataSourceDesigner<EnumQuestionnaireMainAnswer>
	{
	}

	#endregion EnumQuestionnaireMainAnswerDataSourceDesigner

	#region EnumQuestionnaireMainAnswerFilter

	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EnumQuestionnaireMainAnswer"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EnumQuestionnaireMainAnswerFilter : SqlFilter<EnumQuestionnaireMainAnswerColumn>
	{
	}

	#endregion EnumQuestionnaireMainAnswerFilter

	#region EnumQuestionnaireMainAnswerExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EnumQuestionnaireMainAnswer"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EnumQuestionnaireMainAnswerExpressionBuilder : SqlExpressionBuilder<EnumQuestionnaireMainAnswerColumn>
	{
	}
	
	#endregion EnumQuestionnaireMainAnswerExpressionBuilder		
}

