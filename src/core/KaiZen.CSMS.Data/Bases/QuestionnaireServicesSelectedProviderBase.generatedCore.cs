﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="QuestionnaireServicesSelectedProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class QuestionnaireServicesSelectedProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.QuestionnaireServicesSelected, KaiZen.CSMS.Entities.QuestionnaireServicesSelectedKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.QuestionnaireServicesSelectedKey key)
		{
			return Delete(transactionManager, key.QuestionnaireServiceId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_questionnaireServiceId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _questionnaireServiceId)
		{
			return Delete(null, _questionnaireServiceId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireServiceId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _questionnaireServiceId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireServicesSelected_Questionnaire key.
		///		FK_QuestionnaireServicesSelected_Questionnaire Description: 
		/// </summary>
		/// <param name="_questionnaireId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireServicesSelected objects.</returns>
		public TList<QuestionnaireServicesSelected> GetByQuestionnaireId(System.Int32 _questionnaireId)
		{
			int count = -1;
			return GetByQuestionnaireId(_questionnaireId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireServicesSelected_Questionnaire key.
		///		FK_QuestionnaireServicesSelected_Questionnaire Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireServicesSelected objects.</returns>
		/// <remarks></remarks>
		public TList<QuestionnaireServicesSelected> GetByQuestionnaireId(TransactionManager transactionManager, System.Int32 _questionnaireId)
		{
			int count = -1;
			return GetByQuestionnaireId(transactionManager, _questionnaireId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireServicesSelected_Questionnaire key.
		///		FK_QuestionnaireServicesSelected_Questionnaire Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireServicesSelected objects.</returns>
		public TList<QuestionnaireServicesSelected> GetByQuestionnaireId(TransactionManager transactionManager, System.Int32 _questionnaireId, int start, int pageLength)
		{
			int count = -1;
			return GetByQuestionnaireId(transactionManager, _questionnaireId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireServicesSelected_Questionnaire key.
		///		fkQuestionnaireServicesSelectedQuestionnaire Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_questionnaireId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireServicesSelected objects.</returns>
		public TList<QuestionnaireServicesSelected> GetByQuestionnaireId(System.Int32 _questionnaireId, int start, int pageLength)
		{
			int count =  -1;
			return GetByQuestionnaireId(null, _questionnaireId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireServicesSelected_Questionnaire key.
		///		fkQuestionnaireServicesSelectedQuestionnaire Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_questionnaireId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireServicesSelected objects.</returns>
		public TList<QuestionnaireServicesSelected> GetByQuestionnaireId(System.Int32 _questionnaireId, int start, int pageLength,out int count)
		{
			return GetByQuestionnaireId(null, _questionnaireId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireServicesSelected_Questionnaire key.
		///		FK_QuestionnaireServicesSelected_Questionnaire Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireServicesSelected objects.</returns>
		public abstract TList<QuestionnaireServicesSelected> GetByQuestionnaireId(TransactionManager transactionManager, System.Int32 _questionnaireId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireServicesSelected_QuestionnaireServicesCategory key.
		///		FK_QuestionnaireServicesSelected_QuestionnaireServicesCategory Description: 
		/// </summary>
		/// <param name="_categoryId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireServicesSelected objects.</returns>
		public TList<QuestionnaireServicesSelected> GetByCategoryId(System.Int32 _categoryId)
		{
			int count = -1;
			return GetByCategoryId(_categoryId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireServicesSelected_QuestionnaireServicesCategory key.
		///		FK_QuestionnaireServicesSelected_QuestionnaireServicesCategory Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_categoryId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireServicesSelected objects.</returns>
		/// <remarks></remarks>
		public TList<QuestionnaireServicesSelected> GetByCategoryId(TransactionManager transactionManager, System.Int32 _categoryId)
		{
			int count = -1;
			return GetByCategoryId(transactionManager, _categoryId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireServicesSelected_QuestionnaireServicesCategory key.
		///		FK_QuestionnaireServicesSelected_QuestionnaireServicesCategory Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_categoryId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireServicesSelected objects.</returns>
		public TList<QuestionnaireServicesSelected> GetByCategoryId(TransactionManager transactionManager, System.Int32 _categoryId, int start, int pageLength)
		{
			int count = -1;
			return GetByCategoryId(transactionManager, _categoryId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireServicesSelected_QuestionnaireServicesCategory key.
		///		fkQuestionnaireServicesSelectedQuestionnaireServicesCategory Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_categoryId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireServicesSelected objects.</returns>
		public TList<QuestionnaireServicesSelected> GetByCategoryId(System.Int32 _categoryId, int start, int pageLength)
		{
			int count =  -1;
			return GetByCategoryId(null, _categoryId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireServicesSelected_QuestionnaireServicesCategory key.
		///		fkQuestionnaireServicesSelectedQuestionnaireServicesCategory Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_categoryId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireServicesSelected objects.</returns>
		public TList<QuestionnaireServicesSelected> GetByCategoryId(System.Int32 _categoryId, int start, int pageLength,out int count)
		{
			return GetByCategoryId(null, _categoryId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireServicesSelected_QuestionnaireServicesCategory key.
		///		FK_QuestionnaireServicesSelected_QuestionnaireServicesCategory Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_categoryId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireServicesSelected objects.</returns>
		public abstract TList<QuestionnaireServicesSelected> GetByCategoryId(TransactionManager transactionManager, System.Int32 _categoryId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireServicesSelected_QuestionnaireType key.
		///		FK_QuestionnaireServicesSelected_QuestionnaireType Description: 
		/// </summary>
		/// <param name="_questionnaireTypeId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireServicesSelected objects.</returns>
		public TList<QuestionnaireServicesSelected> GetByQuestionnaireTypeId(System.Int32 _questionnaireTypeId)
		{
			int count = -1;
			return GetByQuestionnaireTypeId(_questionnaireTypeId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireServicesSelected_QuestionnaireType key.
		///		FK_QuestionnaireServicesSelected_QuestionnaireType Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireTypeId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireServicesSelected objects.</returns>
		/// <remarks></remarks>
		public TList<QuestionnaireServicesSelected> GetByQuestionnaireTypeId(TransactionManager transactionManager, System.Int32 _questionnaireTypeId)
		{
			int count = -1;
			return GetByQuestionnaireTypeId(transactionManager, _questionnaireTypeId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireServicesSelected_QuestionnaireType key.
		///		FK_QuestionnaireServicesSelected_QuestionnaireType Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireTypeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireServicesSelected objects.</returns>
		public TList<QuestionnaireServicesSelected> GetByQuestionnaireTypeId(TransactionManager transactionManager, System.Int32 _questionnaireTypeId, int start, int pageLength)
		{
			int count = -1;
			return GetByQuestionnaireTypeId(transactionManager, _questionnaireTypeId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireServicesSelected_QuestionnaireType key.
		///		fkQuestionnaireServicesSelectedQuestionnaireType Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_questionnaireTypeId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireServicesSelected objects.</returns>
		public TList<QuestionnaireServicesSelected> GetByQuestionnaireTypeId(System.Int32 _questionnaireTypeId, int start, int pageLength)
		{
			int count =  -1;
			return GetByQuestionnaireTypeId(null, _questionnaireTypeId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireServicesSelected_QuestionnaireType key.
		///		fkQuestionnaireServicesSelectedQuestionnaireType Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_questionnaireTypeId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireServicesSelected objects.</returns>
		public TList<QuestionnaireServicesSelected> GetByQuestionnaireTypeId(System.Int32 _questionnaireTypeId, int start, int pageLength,out int count)
		{
			return GetByQuestionnaireTypeId(null, _questionnaireTypeId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireServicesSelected_QuestionnaireType key.
		///		FK_QuestionnaireServicesSelected_QuestionnaireType Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireTypeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireServicesSelected objects.</returns>
		public abstract TList<QuestionnaireServicesSelected> GetByQuestionnaireTypeId(TransactionManager transactionManager, System.Int32 _questionnaireTypeId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireServicesSelected_Users key.
		///		FK_QuestionnaireServicesSelected_Users Description: 
		/// </summary>
		/// <param name="_modifiedByUserId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireServicesSelected objects.</returns>
		public TList<QuestionnaireServicesSelected> GetByModifiedByUserId(System.Int32 _modifiedByUserId)
		{
			int count = -1;
			return GetByModifiedByUserId(_modifiedByUserId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireServicesSelected_Users key.
		///		FK_QuestionnaireServicesSelected_Users Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_modifiedByUserId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireServicesSelected objects.</returns>
		/// <remarks></remarks>
		public TList<QuestionnaireServicesSelected> GetByModifiedByUserId(TransactionManager transactionManager, System.Int32 _modifiedByUserId)
		{
			int count = -1;
			return GetByModifiedByUserId(transactionManager, _modifiedByUserId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireServicesSelected_Users key.
		///		FK_QuestionnaireServicesSelected_Users Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_modifiedByUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireServicesSelected objects.</returns>
		public TList<QuestionnaireServicesSelected> GetByModifiedByUserId(TransactionManager transactionManager, System.Int32 _modifiedByUserId, int start, int pageLength)
		{
			int count = -1;
			return GetByModifiedByUserId(transactionManager, _modifiedByUserId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireServicesSelected_Users key.
		///		fkQuestionnaireServicesSelectedUsers Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_modifiedByUserId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireServicesSelected objects.</returns>
		public TList<QuestionnaireServicesSelected> GetByModifiedByUserId(System.Int32 _modifiedByUserId, int start, int pageLength)
		{
			int count =  -1;
			return GetByModifiedByUserId(null, _modifiedByUserId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireServicesSelected_Users key.
		///		fkQuestionnaireServicesSelectedUsers Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_modifiedByUserId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireServicesSelected objects.</returns>
		public TList<QuestionnaireServicesSelected> GetByModifiedByUserId(System.Int32 _modifiedByUserId, int start, int pageLength,out int count)
		{
			return GetByModifiedByUserId(null, _modifiedByUserId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireServicesSelected_Users key.
		///		FK_QuestionnaireServicesSelected_Users Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_modifiedByUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireServicesSelected objects.</returns>
		public abstract TList<QuestionnaireServicesSelected> GetByModifiedByUserId(TransactionManager transactionManager, System.Int32 _modifiedByUserId, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.QuestionnaireServicesSelected Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.QuestionnaireServicesSelectedKey key, int start, int pageLength)
		{
			return GetByQuestionnaireServiceId(transactionManager, key.QuestionnaireServiceId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_QuestionnaireServicesSelected index.
		/// </summary>
		/// <param name="_questionnaireServiceId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireServicesSelected"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireServicesSelected GetByQuestionnaireServiceId(System.Int32 _questionnaireServiceId)
		{
			int count = -1;
			return GetByQuestionnaireServiceId(null,_questionnaireServiceId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireServicesSelected index.
		/// </summary>
		/// <param name="_questionnaireServiceId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireServicesSelected"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireServicesSelected GetByQuestionnaireServiceId(System.Int32 _questionnaireServiceId, int start, int pageLength)
		{
			int count = -1;
			return GetByQuestionnaireServiceId(null, _questionnaireServiceId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireServicesSelected index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireServiceId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireServicesSelected"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireServicesSelected GetByQuestionnaireServiceId(TransactionManager transactionManager, System.Int32 _questionnaireServiceId)
		{
			int count = -1;
			return GetByQuestionnaireServiceId(transactionManager, _questionnaireServiceId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireServicesSelected index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireServiceId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireServicesSelected"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireServicesSelected GetByQuestionnaireServiceId(TransactionManager transactionManager, System.Int32 _questionnaireServiceId, int start, int pageLength)
		{
			int count = -1;
			return GetByQuestionnaireServiceId(transactionManager, _questionnaireServiceId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireServicesSelected index.
		/// </summary>
		/// <param name="_questionnaireServiceId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireServicesSelected"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireServicesSelected GetByQuestionnaireServiceId(System.Int32 _questionnaireServiceId, int start, int pageLength, out int count)
		{
			return GetByQuestionnaireServiceId(null, _questionnaireServiceId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireServicesSelected index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireServiceId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireServicesSelected"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.QuestionnaireServicesSelected GetByQuestionnaireServiceId(TransactionManager transactionManager, System.Int32 _questionnaireServiceId, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_QuestionnaireServicesSelected index.
		/// </summary>
		/// <param name="_questionnaireId"></param>
		/// <param name="_questionnaireTypeId"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;QuestionnaireServicesSelected&gt;"/> class.</returns>
		public TList<QuestionnaireServicesSelected> GetByQuestionnaireIdQuestionnaireTypeId(System.Int32 _questionnaireId, System.Int32 _questionnaireTypeId)
		{
			int count = -1;
			return GetByQuestionnaireIdQuestionnaireTypeId(null,_questionnaireId, _questionnaireTypeId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireServicesSelected index.
		/// </summary>
		/// <param name="_questionnaireId"></param>
		/// <param name="_questionnaireTypeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;QuestionnaireServicesSelected&gt;"/> class.</returns>
		public TList<QuestionnaireServicesSelected> GetByQuestionnaireIdQuestionnaireTypeId(System.Int32 _questionnaireId, System.Int32 _questionnaireTypeId, int start, int pageLength)
		{
			int count = -1;
			return GetByQuestionnaireIdQuestionnaireTypeId(null, _questionnaireId, _questionnaireTypeId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireServicesSelected index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireId"></param>
		/// <param name="_questionnaireTypeId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;QuestionnaireServicesSelected&gt;"/> class.</returns>
		public TList<QuestionnaireServicesSelected> GetByQuestionnaireIdQuestionnaireTypeId(TransactionManager transactionManager, System.Int32 _questionnaireId, System.Int32 _questionnaireTypeId)
		{
			int count = -1;
			return GetByQuestionnaireIdQuestionnaireTypeId(transactionManager, _questionnaireId, _questionnaireTypeId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireServicesSelected index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireId"></param>
		/// <param name="_questionnaireTypeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;QuestionnaireServicesSelected&gt;"/> class.</returns>
		public TList<QuestionnaireServicesSelected> GetByQuestionnaireIdQuestionnaireTypeId(TransactionManager transactionManager, System.Int32 _questionnaireId, System.Int32 _questionnaireTypeId, int start, int pageLength)
		{
			int count = -1;
			return GetByQuestionnaireIdQuestionnaireTypeId(transactionManager, _questionnaireId, _questionnaireTypeId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireServicesSelected index.
		/// </summary>
		/// <param name="_questionnaireId"></param>
		/// <param name="_questionnaireTypeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;QuestionnaireServicesSelected&gt;"/> class.</returns>
		public TList<QuestionnaireServicesSelected> GetByQuestionnaireIdQuestionnaireTypeId(System.Int32 _questionnaireId, System.Int32 _questionnaireTypeId, int start, int pageLength, out int count)
		{
			return GetByQuestionnaireIdQuestionnaireTypeId(null, _questionnaireId, _questionnaireTypeId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireServicesSelected index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireId"></param>
		/// <param name="_questionnaireTypeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;QuestionnaireServicesSelected&gt;"/> class.</returns>
		public abstract TList<QuestionnaireServicesSelected> GetByQuestionnaireIdQuestionnaireTypeId(TransactionManager transactionManager, System.Int32 _questionnaireId, System.Int32 _questionnaireTypeId, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;QuestionnaireServicesSelected&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;QuestionnaireServicesSelected&gt;"/></returns>
		public static TList<QuestionnaireServicesSelected> Fill(IDataReader reader, TList<QuestionnaireServicesSelected> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.QuestionnaireServicesSelected c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("QuestionnaireServicesSelected")
					.Append("|").Append((System.Int32)reader[((int)QuestionnaireServicesSelectedColumn.QuestionnaireServiceId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<QuestionnaireServicesSelected>(
					key.ToString(), // EntityTrackingKey
					"QuestionnaireServicesSelected",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.QuestionnaireServicesSelected();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.QuestionnaireServiceId = (System.Int32)reader[((int)QuestionnaireServicesSelectedColumn.QuestionnaireServiceId - 1)];
					c.QuestionnaireId = (System.Int32)reader[((int)QuestionnaireServicesSelectedColumn.QuestionnaireId - 1)];
					c.CategoryId = (System.Int32)reader[((int)QuestionnaireServicesSelectedColumn.CategoryId - 1)];
					c.QuestionnaireTypeId = (System.Int32)reader[((int)QuestionnaireServicesSelectedColumn.QuestionnaireTypeId - 1)];
					c.ModifiedByUserId = (System.Int32)reader[((int)QuestionnaireServicesSelectedColumn.ModifiedByUserId - 1)];
					c.ModifiedDate = (System.DateTime)reader[((int)QuestionnaireServicesSelectedColumn.ModifiedDate - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.QuestionnaireServicesSelected"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.QuestionnaireServicesSelected"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.QuestionnaireServicesSelected entity)
		{
			if (!reader.Read()) return;
			
			entity.QuestionnaireServiceId = (System.Int32)reader[((int)QuestionnaireServicesSelectedColumn.QuestionnaireServiceId - 1)];
			entity.QuestionnaireId = (System.Int32)reader[((int)QuestionnaireServicesSelectedColumn.QuestionnaireId - 1)];
			entity.CategoryId = (System.Int32)reader[((int)QuestionnaireServicesSelectedColumn.CategoryId - 1)];
			entity.QuestionnaireTypeId = (System.Int32)reader[((int)QuestionnaireServicesSelectedColumn.QuestionnaireTypeId - 1)];
			entity.ModifiedByUserId = (System.Int32)reader[((int)QuestionnaireServicesSelectedColumn.ModifiedByUserId - 1)];
			entity.ModifiedDate = (System.DateTime)reader[((int)QuestionnaireServicesSelectedColumn.ModifiedDate - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.QuestionnaireServicesSelected"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.QuestionnaireServicesSelected"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.QuestionnaireServicesSelected entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.QuestionnaireServiceId = (System.Int32)dataRow["QuestionnaireServiceId"];
			entity.QuestionnaireId = (System.Int32)dataRow["QuestionnaireId"];
			entity.CategoryId = (System.Int32)dataRow["CategoryId"];
			entity.QuestionnaireTypeId = (System.Int32)dataRow["QuestionnaireTypeId"];
			entity.ModifiedByUserId = (System.Int32)dataRow["ModifiedByUserId"];
			entity.ModifiedDate = (System.DateTime)dataRow["ModifiedDate"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.QuestionnaireServicesSelected"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.QuestionnaireServicesSelected Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.QuestionnaireServicesSelected entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region QuestionnaireIdSource	
			if (CanDeepLoad(entity, "Questionnaire|QuestionnaireIdSource", deepLoadType, innerList) 
				&& entity.QuestionnaireIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.QuestionnaireId;
				Questionnaire tmpEntity = EntityManager.LocateEntity<Questionnaire>(EntityLocator.ConstructKeyFromPkItems(typeof(Questionnaire), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.QuestionnaireIdSource = tmpEntity;
				else
					entity.QuestionnaireIdSource = DataRepository.QuestionnaireProvider.GetByQuestionnaireId(transactionManager, entity.QuestionnaireId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'QuestionnaireIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.QuestionnaireIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.QuestionnaireProvider.DeepLoad(transactionManager, entity.QuestionnaireIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion QuestionnaireIdSource

			#region CategoryIdSource	
			if (CanDeepLoad(entity, "QuestionnaireServicesCategory|CategoryIdSource", deepLoadType, innerList) 
				&& entity.CategoryIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.CategoryId;
				QuestionnaireServicesCategory tmpEntity = EntityManager.LocateEntity<QuestionnaireServicesCategory>(EntityLocator.ConstructKeyFromPkItems(typeof(QuestionnaireServicesCategory), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.CategoryIdSource = tmpEntity;
				else
					entity.CategoryIdSource = DataRepository.QuestionnaireServicesCategoryProvider.GetByCategoryId(transactionManager, entity.CategoryId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CategoryIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.CategoryIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.QuestionnaireServicesCategoryProvider.DeepLoad(transactionManager, entity.CategoryIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion CategoryIdSource

			#region QuestionnaireTypeIdSource	
			if (CanDeepLoad(entity, "QuestionnaireType|QuestionnaireTypeIdSource", deepLoadType, innerList) 
				&& entity.QuestionnaireTypeIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.QuestionnaireTypeId;
				QuestionnaireType tmpEntity = EntityManager.LocateEntity<QuestionnaireType>(EntityLocator.ConstructKeyFromPkItems(typeof(QuestionnaireType), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.QuestionnaireTypeIdSource = tmpEntity;
				else
					entity.QuestionnaireTypeIdSource = DataRepository.QuestionnaireTypeProvider.GetByQuestionnaireTypeId(transactionManager, entity.QuestionnaireTypeId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'QuestionnaireTypeIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.QuestionnaireTypeIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.QuestionnaireTypeProvider.DeepLoad(transactionManager, entity.QuestionnaireTypeIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion QuestionnaireTypeIdSource

			#region ModifiedByUserIdSource	
			if (CanDeepLoad(entity, "Users|ModifiedByUserIdSource", deepLoadType, innerList) 
				&& entity.ModifiedByUserIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.ModifiedByUserId;
				Users tmpEntity = EntityManager.LocateEntity<Users>(EntityLocator.ConstructKeyFromPkItems(typeof(Users), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.ModifiedByUserIdSource = tmpEntity;
				else
					entity.ModifiedByUserIdSource = DataRepository.UsersProvider.GetByUserId(transactionManager, entity.ModifiedByUserId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ModifiedByUserIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.ModifiedByUserIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.UsersProvider.DeepLoad(transactionManager, entity.ModifiedByUserIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion ModifiedByUserIdSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.QuestionnaireServicesSelected object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.QuestionnaireServicesSelected instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.QuestionnaireServicesSelected Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.QuestionnaireServicesSelected entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region QuestionnaireIdSource
			if (CanDeepSave(entity, "Questionnaire|QuestionnaireIdSource", deepSaveType, innerList) 
				&& entity.QuestionnaireIdSource != null)
			{
				DataRepository.QuestionnaireProvider.Save(transactionManager, entity.QuestionnaireIdSource);
				entity.QuestionnaireId = entity.QuestionnaireIdSource.QuestionnaireId;
			}
			#endregion 
			
			#region CategoryIdSource
			if (CanDeepSave(entity, "QuestionnaireServicesCategory|CategoryIdSource", deepSaveType, innerList) 
				&& entity.CategoryIdSource != null)
			{
				DataRepository.QuestionnaireServicesCategoryProvider.Save(transactionManager, entity.CategoryIdSource);
				entity.CategoryId = entity.CategoryIdSource.CategoryId;
			}
			#endregion 
			
			#region QuestionnaireTypeIdSource
			if (CanDeepSave(entity, "QuestionnaireType|QuestionnaireTypeIdSource", deepSaveType, innerList) 
				&& entity.QuestionnaireTypeIdSource != null)
			{
				DataRepository.QuestionnaireTypeProvider.Save(transactionManager, entity.QuestionnaireTypeIdSource);
				entity.QuestionnaireTypeId = entity.QuestionnaireTypeIdSource.QuestionnaireTypeId;
			}
			#endregion 
			
			#region ModifiedByUserIdSource
			if (CanDeepSave(entity, "Users|ModifiedByUserIdSource", deepSaveType, innerList) 
				&& entity.ModifiedByUserIdSource != null)
			{
				DataRepository.UsersProvider.Save(transactionManager, entity.ModifiedByUserIdSource);
				entity.ModifiedByUserId = entity.ModifiedByUserIdSource.UserId;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region QuestionnaireServicesSelectedChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.QuestionnaireServicesSelected</c>
	///</summary>
	public enum QuestionnaireServicesSelectedChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>Questionnaire</c> at QuestionnaireIdSource
		///</summary>
		[ChildEntityType(typeof(Questionnaire))]
		Questionnaire,
			
		///<summary>
		/// Composite Property for <c>QuestionnaireServicesCategory</c> at CategoryIdSource
		///</summary>
		[ChildEntityType(typeof(QuestionnaireServicesCategory))]
		QuestionnaireServicesCategory,
			
		///<summary>
		/// Composite Property for <c>QuestionnaireType</c> at QuestionnaireTypeIdSource
		///</summary>
		[ChildEntityType(typeof(QuestionnaireType))]
		QuestionnaireType,
			
		///<summary>
		/// Composite Property for <c>Users</c> at ModifiedByUserIdSource
		///</summary>
		[ChildEntityType(typeof(Users))]
		Users,
		}
	
	#endregion QuestionnaireServicesSelectedChildEntityTypes
	
	#region QuestionnaireServicesSelectedFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;QuestionnaireServicesSelectedColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireServicesSelected"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireServicesSelectedFilterBuilder : SqlFilterBuilder<QuestionnaireServicesSelectedColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireServicesSelectedFilterBuilder class.
		/// </summary>
		public QuestionnaireServicesSelectedFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireServicesSelectedFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireServicesSelectedFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireServicesSelectedFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireServicesSelectedFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireServicesSelectedFilterBuilder
	
	#region QuestionnaireServicesSelectedParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;QuestionnaireServicesSelectedColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireServicesSelected"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireServicesSelectedParameterBuilder : ParameterizedSqlFilterBuilder<QuestionnaireServicesSelectedColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireServicesSelectedParameterBuilder class.
		/// </summary>
		public QuestionnaireServicesSelectedParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireServicesSelectedParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireServicesSelectedParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireServicesSelectedParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireServicesSelectedParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireServicesSelectedParameterBuilder
	
	#region QuestionnaireServicesSelectedSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;QuestionnaireServicesSelectedColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireServicesSelected"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class QuestionnaireServicesSelectedSortBuilder : SqlSortBuilder<QuestionnaireServicesSelectedColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireServicesSelectedSqlSortBuilder class.
		/// </summary>
		public QuestionnaireServicesSelectedSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion QuestionnaireServicesSelectedSortBuilder
	
} // end namespace
