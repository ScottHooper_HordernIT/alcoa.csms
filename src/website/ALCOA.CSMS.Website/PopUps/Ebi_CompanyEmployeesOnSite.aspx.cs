﻿using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Services;
using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Security.Principal;


public partial class PopUps_Ebi_CompanyEmployeesOnSite : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string EbiCompanyName = "";
        string EbiSiteName = "";
        string sYear = "";
        string sMonth = "";
        string sDay = "";

        EbiCompanyName = (Server.UrlDecode(Request.QueryString["EbiCompanyName"])).Replace("[ampersand]", "&");
        EbiSiteName = Request.QueryString["EbiSiteName"];
        sYear = Request.QueryString["Year"];
        sMonth = Request.QueryString["Month"];
        sDay = Request.QueryString["Day"];

        if (String.IsNullOrEmpty(sYear) && String.IsNullOrEmpty(sMonth) && String.IsNullOrEmpty(sDay))
        {
            //yesterday
            DateTime dt = DateTime.Now.AddDays(-1);
            sYear = dt.Year.ToString();
            sMonth = dt.Month.ToString();
            sDay = dt.Day.ToString();
        }

        if (!String.IsNullOrEmpty(EbiCompanyName) && !String.IsNullOrEmpty(EbiSiteName) && !String.IsNullOrEmpty(sYear) &&
            !String.IsNullOrEmpty(sMonth) && !String.IsNullOrEmpty(sDay))
        {
            //Connect to EBI Database.
            SitesService sService = new SitesService();
            TList<Sites> s = sService.GetBySiteNameEbi(EbiSiteName.ToUpper());
            if((s.Count == 1) && !string.IsNullOrEmpty(s[0].EbiServerName) && !string.IsNullOrEmpty(s[0].EbiTodayViewName))
            {
                WindowsImpersonationContext impContext = null;
                try
                {
                    impContext = Helper.ImpersonateWAOCSMUser();
                }
                catch (ApplicationException ex)
                {
                    Response.Write(ex.Message);
                }

                try
                {
                    if (null != impContext)
                    {
                        using (impContext)
                        {
                            string strConnString = ConfigurationManager.ConnectionStrings[
                                                            String.Format("Alcoa.Ebi.{0}.ConnectionString", s[0].EbiServerName)].ConnectionString;

                            using (SqlConnection cn = new SqlConnection(strConnString))
                            {
                                cn.Open();
                                SqlCommand cmd = new SqlCommand("SELECT DISTINCT(FullName), Company, ClockID, FullName, CardNumber, Site FROM " + s[0].EbiTodayViewName + " WHERE Company = '" + EbiCompanyName + "' AND Site = '" + EbiSiteName + "'" , cn);
                                SqlDataReader r = cmd.ExecuteReader();

                                DataTable dt = new DataTable();
                                dt.Columns.Add("Company", typeof(string));
                                dt.Columns.Add("ClockID", typeof(string));
                                dt.Columns.Add("FullName", typeof(string));
                                dt.Columns.Add("CardNumber", typeof(string));
                                dt.Columns.Add("Site", typeof(string));

                                while (r.Read())
                                {
                                    DataRow dr = dt.NewRow();
                                    dr["Company"] = (string)r["Company"];
                                    dr["ClockId"] = (string)r["ClockID"];
                                    dr["FullName"] = (string)r["FullName"];
                                    dr["CardNumber"] = (string)r["CardNumber"];
                                    dr["Site"] = (string)r["Site"];

                                    dt.Rows.Add(dr);
                                }

                                ASPxGridView1.DataSource = dt;
                                ASPxGridView1.DataBind();
                                cn.Close();
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Response.Write(ex.Message.ToString() + "\nServer Configuration Account Error. Contact Administrator.");
                }
                finally
                {
                    if (impContext != null)
                        impContext.Undo();
                }
            }
            else
            {
                Response.Write("Invalid Site or Site Does not Have EBI Data");
            }
        }
        else
        {
            ASPxGridView1.Visible = false;
            Response.Write("Invalid Parameters");
        }
    }
}
