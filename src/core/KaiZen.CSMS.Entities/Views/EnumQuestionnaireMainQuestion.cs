﻿#region Using directives

using System;

#endregion

namespace KaiZen.CSMS.Entities
{	
	///<summary>
	/// An object representation of the 'enumQuestionnaireMainQuestion' view. [No description found in the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class EnumQuestionnaireMainQuestion : EnumQuestionnaireMainQuestionBase
	{
		#region Constructors

		///<summary>
		/// Creates a new <see cref="EnumQuestionnaireMainQuestion"/> instance.
		///</summary>
		public EnumQuestionnaireMainQuestion():base(){}	
		
		#endregion
	}
}
