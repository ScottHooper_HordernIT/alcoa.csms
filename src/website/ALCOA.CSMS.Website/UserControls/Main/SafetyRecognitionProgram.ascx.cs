﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Text;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using KaiZen.CSMS.Web;
using KaiZen.CSMS.Services;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxTabControl;
using DevExpress.Web.Data;
using DevExpress.Web.ASPxGridView;
using DevExpress.XtraPrinting;

using System.Collections.Generic;
using System.Drawing;

public partial class UserControls_SafetyRecognitionProgram : System.Web.UI.UserControl
    {
    System.Web.UI.HtmlControls.HtmlTableRow rowIFE;
               
        Auth auth = new Auth();
        protected void Page_Load(object sender, EventArgs e) {  Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack);}

        private void moduleInit(bool postBack)
        {
            if (!postBack)
            {
                int _i = 0;
                cbRegionSite.Items.Clear();
                SitesFilters sitesFilters = new SitesFilters();
                sitesFilters.Append(SitesColumn.IsVisible, "1");
                TList<Sites> sitesList = DataRepository.SitesProvider.GetPaged(sitesFilters.ToString(), "SiteName ASC", 0, 100, out _i);
                foreach (Sites s in sitesList)
                {
                    cbRegionSite.Items.Add(s.SiteName, s.SiteId);
                }
                cbRegionSite.Items.Add("----------------", 0);

                RegionsFilters regionsFilters = new RegionsFilters();
                regionsFilters.Append(RegionsColumn.IsVisible, "1");
                TList<Regions> regionsList = DataRepository.RegionsProvider.GetPaged(regionsFilters.ToString(), "Ordinal ASC", 0, 100, out _i);
                foreach (Regions r in regionsList)
                {
                    int NegRegionId = -1 * r.RegionId;
                    //if (r.RegionInternalName != "AU")
                    cbRegionSite.Items.Add(r.RegionName, NegRegionId);
                    
                }
                int indexAus = cbRegionSite.Items.IndexOfText("Australia");
                //cbRegionSite.Text = "Anglesea";
                //cbRegionSite.SelectedItem.Value = 13;
                cbRegionSite.SelectedIndex = indexAus;

                //int currentYear = DateTime.Today.Year;
                //ddlYear.Items.Add(new ListEditItem((Convert.ToInt32(DateTime.Now.Year) - 5).ToString(), (Convert.ToInt32(DateTime.Now.Year) - 5)));
                //ddlYear.Items.Add(new ListEditItem((Convert.ToInt32(DateTime.Now.Year) - 4).ToString(), (Convert.ToInt32(DateTime.Now.Year) - 4)));
               
                //ddlYear.Items.Add(new ListEditItem((Convert.ToInt32(DateTime.Now.Year) - 3).ToString(), (Convert.ToInt32(DateTime.Now.Year) - 3)));
                //ddlYear.Items.Add(new ListEditItem((Convert.ToInt32(DateTime.Now.Year) - 2).ToString(), (Convert.ToInt32(DateTime.Now.Year) - 2)));
                //ddlYear.Items.Add(new ListEditItem((Convert.ToInt32(DateTime.Now.Year) - 1).ToString(), (Convert.ToInt32(DateTime.Now.Year) - 1)));
                //ddlYear.Items.Add(new ListEditItem(DateTime.Now.Year.ToString(), DateTime.Now.Year.ToString()));
                //ddlYear.Value = currentYear;



                int Current_year = DateTime.Now.Year;
                while (Current_year >= 2007)
                {
                    ddlYear.Items.Add(Current_year.ToString(), Convert.ToInt32(Current_year));
                    Current_year--;
                } //End
                ddlYear.Value = DateTime.Today.Year;


                #region Export

                    String exportFileName = @"ALCOA CSMS - Safety Recognition Program"; //Chrome & IE is fine.
                    String userAgent = Request.Headers.Get("User-Agent");
                    if (
                        (userAgent.Contains("Mozilla") && !userAgent.Contains("AppleWebKit") && !userAgent.Contains("MSIE")) || //Mozilla
                        (userAgent.Contains("AppleWebKit") && !userAgent.Contains("Chrome"))      //Safari
                        )
                    {
                        exportFileName = exportFileName.Replace(" ", "_");
                    }
                    Helper.ExportGrid.Settings(ucExportButtons, exportFileName, "grid");
               

                #endregion


                Session["spVar_YearN"] = DateTime.Now.Year;
                Session["spVar_SiteIdN"] = -2;


                #region Tab 2

                DataSet dsInjuryFree = DataRepository.KpiProvider.SafetyRecognitionProgram_InjuryFree(
                    Convert.ToInt32(Session["spVar_SiteIdN"].ToString()),
                Convert.ToInt32(Session["spVar_YearN"].ToString()));

                DataTable dtIFE = dsInjuryFree.Tables[0];
                int Check = dtIFE.Rows.Count / 3;
                int rem = dtIFE.Rows.Count % 3;

                if (rem > 0)
                {
                    Check = Check + 1;
                }


                for (int i = 0; i < Check; i++)
                {
                    rowIFE = new System.Web.UI.HtmlControls.HtmlTableRow();
                    Tlb_InjuryFree.Rows.Add(rowIFE);
                    for (int j = 0; j < 5; j++)
                    {
                        System.Web.UI.HtmlControls.HtmlTableCell cell = new System.Web.UI.HtmlControls.HtmlTableCell();
                        cell.BorderColor = "#DDECFE";
                        rowIFE.Cells.Add(cell);
                    }

                }

                int xx = 0;
                for (int i = 0; i < 5; i = i + 2)
                    for (int j = 0; j < Tlb_InjuryFree.Rows.Count; j++)
                    {
                        if (xx < dtIFE.Rows.Count)
                        {
                            string cmpName = dtIFE.Rows[xx][1].ToString();
                            Tlb_InjuryFree.Rows[j].Cells[i].InnerText = cmpName;
                            //Tlb_InjuryFree.Rows[j].Cells[i].BgColor = "#9ACD32";
                            Tlb_InjuryFree.Rows[j].Cells[i].BgColor = "#D6FF6B";
                            Tlb_InjuryFree.Rows[j].Cells[i].BorderColor = "#76923A";
                            Tlb_InjuryFree.Rows[j].Cells[i].Width = "32%";
                            xx++;
                        }

                    }

                #endregion
            }

            grid.Settings.ShowFilterRow = false;
            grid.Settings.ShowGroupPanel = false;
            grid.SettingsBehavior.AllowSort = true;
            grid.SettingsBehavior.AllowGroup = false;

            DataSet ds = null;

           
            ds = DataRepository.KpiProvider.SafetyRecognitionProgram(
                Convert.ToInt32(Session["spVar_SiteIdN"].ToString()), 
                Convert.ToInt32(Session["spVar_YearN"].ToString()));
            

            ////////

            DataTable dt_Temp = new DataTable();
            dt_Temp.Columns.Add("CompanyId");
            dt_Temp.Columns.Add("CompanyName");
            dt_Temp.Columns.Add("LWDFR",typeof(decimal));
            dt_Temp.Columns.Add("TRIFR", typeof(decimal));
            dt_Temp.Columns.Add("AIFR", typeof(decimal));
            dt_Temp.Columns.Add("Color");
            DataRow dr_Temp;
            int item = 0;
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                string trifr = dr["TRIFR"].ToString();

                if (trifr == "0.00" && item == 0)
                {
                    string selected_Site = cbRegionSite.SelectedItem.Text;
                    selected_Site = selected_Site + " – Contractor Services";

                    DataSet dsCompReport = DataRepository.KpiProvider.Compliance_Table(Convert.ToInt32(Session["spVar_SiteIdN"].ToString()),
                        Convert.ToInt32(Session["spVar_YearN"].ToString()), "%");

                    dr_Temp = dt_Temp.NewRow();
                    dr_Temp["CompanyId"] = "34";
                    dr_Temp["CompanyName"] = selected_Site;
                    dr_Temp["LWDFR"] = dsCompReport.Tables[0].Rows[0]["LWDFR"].ToString();
                    dr_Temp["TRIFR"] = dsCompReport.Tables[0].Rows[0]["TRIFR"].ToString();
                    dr_Temp["AIFR"] = dsCompReport.Tables[0].Rows[0]["AIFR"].ToString();
                    dr_Temp["Color"] = "Y";

                    dt_Temp.Rows.Add(dr_Temp);
                    item++;
                }

                dr_Temp = dt_Temp.NewRow();
                dr_Temp["CompanyId"] = dr["CompanyId"].ToString();
                dr_Temp["CompanyName"] = dr["CompanyName"].ToString();
                dr_Temp["LWDFR"] = dr["LWDFR"].ToString();
                dr_Temp["TRIFR"] = dr["TRIFR"].ToString();
                dr_Temp["AIFR"] = dr["AIFR"].ToString();
                dr_Temp["Color"] = "Z";

                dt_Temp.Rows.Add(dr_Temp);

            }

            grid.DataSource = dt_Temp;

            ///////
            //grid.DataSource = ds.Tables[0];
            grid.DataBind();

            ds.Dispose();
           
        }

        protected void ASPxButton1_Click(object sender, EventArgs e)
        {
            #region Tab 1

            int siteId = Convert.ToInt32(cbRegionSite.SelectedItem.Value.ToString());
           
            int year = Convert.ToInt32(ddlYear.SelectedItem.Value.ToString());

            Session["spVar_YearN"] = Convert.ToInt32(ddlYear.SelectedItem.Value.ToString());
            Session["spVar_SiteIdN"] = Convert.ToInt32(cbRegionSite.SelectedItem.Value.ToString());

            DataSet ds = DataRepository.KpiProvider.SafetyRecognitionProgram(siteId, year);

            DataTable dt_Temp = new DataTable();
            dt_Temp.Columns.Add("CompanyId");
            dt_Temp.Columns.Add("CompanyName");
            dt_Temp.Columns.Add("LWDFR", typeof(decimal));
            dt_Temp.Columns.Add("TRIFR", typeof(decimal));
            dt_Temp.Columns.Add("AIFR", typeof(decimal));
            dt_Temp.Columns.Add("Color");
            DataRow dr_Temp;
            int item = 0;
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                string trifr = dr["TRIFR"].ToString();

                if (trifr == "0.00" && item == 0)
                {
                    string selected_Site = cbRegionSite.SelectedItem.Text;
                    selected_Site = selected_Site + " – Contractor Services";

                    DataSet dsCompReport = DataRepository.KpiProvider.Compliance_Table(siteId, year, "%");

                    dr_Temp = dt_Temp.NewRow();
                    dr_Temp["CompanyId"] = "34";
                    dr_Temp["CompanyName"] = selected_Site;
                    dr_Temp["LWDFR"] = dsCompReport.Tables[0].Rows[0]["LWDFR"].ToString();
                    dr_Temp["TRIFR"] = dsCompReport.Tables[0].Rows[0]["TRIFR"].ToString();
                    dr_Temp["AIFR"] = dsCompReport.Tables[0].Rows[0]["AIFR"].ToString();
                    dr_Temp["Color"] = "Y";

                    dt_Temp.Rows.Add(dr_Temp);
                    item++;
                }

                dr_Temp = dt_Temp.NewRow();
                dr_Temp["CompanyId"] = dr["CompanyId"].ToString();
                dr_Temp["CompanyName"] = dr["CompanyName"].ToString();
                dr_Temp["LWDFR"] = dr["LWDFR"].ToString();
                dr_Temp["TRIFR"] = dr["TRIFR"].ToString();
                dr_Temp["AIFR"] = dr["AIFR"].ToString();
                dr_Temp["Color"] = "Z";

                dt_Temp.Rows.Add(dr_Temp);

            }

            grid.DataSource = dt_Temp;
            grid.DataBind();

            ds.Dispose();

            #endregion

            #region Tab 2

            DataSet dsInjuryFree = DataRepository.KpiProvider.SafetyRecognitionProgram_InjuryFree(siteId, year);

            DataTable dtIFE = dsInjuryFree.Tables[0];
            int Check = dtIFE.Rows.Count/3; 
            int rem    = dtIFE.Rows.Count%3;

            if (rem > 0)
            {
                Check = Check + 1;
            }
           

            for (int i = 0; i < Check; i++)
            {
                rowIFE = new System.Web.UI.HtmlControls.HtmlTableRow();
                Tlb_InjuryFree.Rows.Add(rowIFE);
                for (int j = 0; j < 5; j++)
                {
                    System.Web.UI.HtmlControls.HtmlTableCell cell = new System.Web.UI.HtmlControls.HtmlTableCell();
                    cell.BorderColor = "#DDECFE";
                    rowIFE.Cells.Add(cell);
                }

            }
            
            int xx = 0;
            for (int i = 0; i < 5; i = i + 2)
                for (int j = 0; j < Tlb_InjuryFree.Rows.Count; j++)
                {
                    if (xx < dtIFE.Rows.Count)
                    {
                        string cmpName = dtIFE.Rows[xx][1].ToString();
                        Tlb_InjuryFree.Rows[j].Cells[i].InnerText = cmpName;
                        //Tlb_InjuryFree.Rows[j].Cells[i].BgColor = "#9ACD32";
                        Tlb_InjuryFree.Rows[j].Cells[i].BgColor = "#D6FF6B";
                        Tlb_InjuryFree.Rows[j].Cells[i].BorderColor = "#76923A";
                        Tlb_InjuryFree.Rows[j].Cells[i].Width = "32%";
                        xx++;
                    }

                }

            #endregion
        }

        protected void grid_AutoFilterCellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
        {
        }
        //protected void grid_RowCreated(object sender, ASPxGridViewTableRowEventArgs e)
        //{
        //    //if (e.RowType != GridViewRowType.Data) return;
            
        //  //  decimal TRIFR_Var = (decimal)grid.GetRowValues(e.VisibleIndex, "TRIFR");
        //}

        protected void grid_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
        {
            if (e.RowType != GridViewRowType.Data) return;

            decimal TRIFR_Var = Convert.ToDecimal(grid.GetRowValues(e.VisibleIndex, "TRIFR").ToString());
            string color = (string)grid.GetRowValues(e.VisibleIndex, "Color");


            if (TRIFR_Var > 0 && color == "Z")
            {
                e.Row.BackColor = Color.Red;
            }

            else if (color == "Y")
            {
                e.Row.BackColor = Color.Yellow;
            }

            else
            {
                e.Row.BackColor = Color.Lime;
            }
        }


   
    }
