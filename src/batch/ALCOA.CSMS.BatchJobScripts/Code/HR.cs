﻿using System;
using System.Collections.Generic;
using System.Text;

using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Security.Principal;

using KaiZen.CSMS.Data;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Services;
using KaiZen.Library;
using ALCOA.CSMS.Batch.Common.Log;

namespace ALCOA.CSMS.BatchJobScripts
{
    public class HR
    {
        //public static void UpdateCWKHistory()
        //{
        //    WindowsImpersonationContext impContext = null;
        //    try
        //    {
        //        impContext = NetworkSecurity.Impersonation.ImpersonateUser(
        //                                System.Configuration.ConfigurationManager.AppSettings["WAOCSM_domain"].ToString(),
        //                                System.Configuration.ConfigurationManager.AppSettings["WAOCSM_user"].ToString(),
        //                                System.Configuration.ConfigurationManager.AppSettings["WAOCSM_pass"].ToString());
        //    }
        //    catch (ApplicationException ex)
        //    {
        //        Log4NetService.LogInfo(ex.Message);
        //    }
        //    try
        //    {
        //        if (null != impContext)
        //        {
        //            using (impContext)
        //            {
        //                DateTime pollingdate=DateTime.Now;
        //                try
        //                {
        //                    ConnectionStringSettings conString = ConfigurationManager.ConnectionStrings["Alcoa.HR.Data.ConnectionString"];
        //                    using (SqlConnection conn = new SqlConnection(conString.ConnectionString))
        //                    {

        //                        using (SqlCommand cmd = new SqlCommand("Select Polling_Date from HR..POLLING_DATA WHERE Description='XXHR_CWK_HISTORY'", conn))
        //                        {
        //                            Log4NetService.LogInfo("Fetching information from HR Polling Data Table...");


        //                            conn.Open();
        //                            object objpollingDate = cmd.ExecuteScalar();
        //                            pollingdate = Convert.ToDateTime(objpollingDate);
        //                            conn.Close();

        //                            Log4NetService.LogInfo("Polling datetime for XXHR_CWK_HISTORY=" + pollingdate);
        //                        }
        //                    }


        //                }
        //                catch (Exception ex)
        //                {
        //                    Log4NetService.LogInfo("Error occurred while fetching information from HR table: " + ex.Message);
        //                }
        //                try
        //                {
        //                    ConnectionStringSettings conString = ConfigurationManager.ConnectionStrings["Alcoa.HR.Data.ConnectionString"];
        //                    using (SqlConnection conn = new SqlConnection(conString.ConnectionString))
        //                    {
        //                        // using (SqlCommand cmd = new SqlCommand("Select * from EBS_HR..APPS.XXHR_SQL_CWK_V WHERE LAST_UPDATE_DATE_PER > Convert(datetime,'" + pollingdate + "',103) or LAST_UPDATE_DATE_ASS> Convert(datetime,'" + pollingdate + "',103)", conn))
        //                        using (SqlCommand cmd = new SqlCommand("Select * from EBS_HR..APPS.XXHR_SQL_CWK_V WHERE LAST_UPDATE_DATE_PER > '" + pollingdate + "' or LAST_UPDATE_DATE_ASS> '" + pollingdate + "'", conn))
        //                        {
        //                            Log4NetService.LogInfo("Reading information from HR DB Link...");
        //                            string cmdString = cmd.CommandText;
        //                            conn.Open();
        //                            cmd.CommandTimeout = 60000;
                                    
        //                            SqlDataReader r = cmd.ExecuteReader();
        //                            while (r.Read())

        //                            {

        //                                try
        //                                {   
                                          

        //                                    try
        //                                    {
        //                                        using (SqlConnection conn1 = new SqlConnection(conString.ConnectionString))
        //                                        {

        //                                            using (SqlCommand cmd1 = new SqlCommand("XXHR_CWK_History_Update", conn1))
        //                                            {

        //                                                cmd1.CommandType = CommandType.StoredProcedure;
        //                                                cmd1.Parameters.Add(new SqlParameter("@PERSON_ID", r["PERSON_ID"]));
        //                                                cmd1.Parameters.Add(new SqlParameter("@CWK_NBR", r["CWK_NBR"]));
        //                                                cmd1.Parameters.Add(new SqlParameter("@FULL_NAME", r["FULL_NAME"]));
        //                                                cmd1.Parameters.Add(new SqlParameter("@LAST_NAME", r["LAST_NAME"]));
        //                                                cmd1.Parameters.Add(new SqlParameter("@FIRST_NAME", r["FIRST_NAME"]));
        //                                                cmd1.Parameters.Add(new SqlParameter("@MIDDLE_NAMES", r["MIDDLE_NAMES"]));
        //                                                cmd1.Parameters.Add(new SqlParameter("@AOA_PREF_NAME", r["AOA_PREF_NAME"]));
        //                                                cmd1.Parameters.Add(new SqlParameter("@Gender", r["Gender"]));
        //                                                cmd1.Parameters.Add(new SqlParameter("@LOGINID", r["LOGINID"]));
        //                                                cmd1.Parameters.Add(new SqlParameter("@PER_TYPE_ID", r["PER_TYPE_ID"]));
        //                                                cmd1.Parameters.Add(new SqlParameter("@SERVICE_DATE", r["SERVICE_DATE"]));
        //                                                cmd1.Parameters.Add(new SqlParameter("@ORIGINAL_DATE_OF_HIRE", r["ORIGINAL_DATE_OF_HIRE"]));
        //                                                cmd1.Parameters.Add(new SqlParameter("@TERMINATION_DATE", r["TERMINATION_DATE"]));
        //                                                cmd1.Parameters.Add(new SqlParameter("@Pos_Descr", r["Pos_Descr"]));
        //                                                cmd1.Parameters.Add(new SqlParameter("@Supervisor_Id", r["Supervisor_Id"]));
        //                                                cmd1.Parameters.Add(new SqlParameter("@EFFECTIVE_START_DATE", r["EFFECTIVE_START_DATE"]));
        //                                                cmd1.Parameters.Add(new SqlParameter("@EFFECTIVE_END_DATE", r["EFFECTIVE_END_DATE"]));
        //                                                cmd1.Parameters.Add(new SqlParameter("@ALCOA_PRIV", r["ALCOA_PRIV"]));
        //                                                cmd1.Parameters.Add(new SqlParameter("@LABOUR_CLASS", r["LABOUR_CLASS"]));
        //                                                cmd1.Parameters.Add(new SqlParameter("@NON_PREFF_SUPP", r["NON_PREFF_SUPP"]));
        //                                                cmd1.Parameters.Add(new SqlParameter("@SPONSOR_ID", r["SPONSOR_ID"]));
        //                                                cmd1.Parameters.Add(new SqlParameter("@SUPPLY_SUP", r["SUPPLY_SUP"]));
        //                                                cmd1.Parameters.Add(new SqlParameter("@LOCATION_CODE", r["LOCATION_CODE"]));
        //                                                cmd1.Parameters.Add(new SqlParameter("@ORGANIZATION_ID", r["ORGANIZATION_ID"]));
        //                                                cmd1.Parameters.Add(new SqlParameter("@ORG_CODE", r["ORG_CODE"]));
        //                                                cmd1.Parameters.Add(new SqlParameter("@DEPT_ORG_CODE", r["DEPT_ORG_CODE"]));
        //                                                cmd1.Parameters.Add(new SqlParameter("@OP_CENTRE", r["OP_CENTRE"]));
        //                                                cmd1.Parameters.Add(new SqlParameter("@NAME", r["NAME"]));
        //                                                cmd1.Parameters.Add(new SqlParameter("@LBC", r["LBC"]));
        //                                                cmd1.Parameters.Add(new SqlParameter("@DEPT", r["DEPT"]));
        //                                                cmd1.Parameters.Add(new SqlParameter("@ACCOUNT", r["ACCOUNT"]));
        //                                                cmd1.Parameters.Add(new SqlParameter("@CWK_VENDOR_ID", r["CWK_VENDOR_ID"]));
        //                                                cmd1.Parameters.Add(new SqlParameter("@CWK_VENDOR_NO", r["CWK_VENDOR_NO"]));
        //                                                cmd1.Parameters.Add(new SqlParameter("@CWK_VENDOR_NAME", r["CWK_VENDOR_NAME"]));
        //                                                cmd1.Parameters.Add(new SqlParameter("@CWK_NON_PREFERRED_SUPP_NAME", r["CWK_NON_PREFERRED_SUPP_NAME"]));
        //                                                cmd1.Parameters.Add(new SqlParameter("@CWK_SPONSOR_ID", r["CWK_SPONSOR_ID"]));
        //                                                cmd1.Parameters.Add(new SqlParameter("@CWK_SUPP_SUPERVISOR_ID", r["CWK_SUPP_SUPERVISOR_ID"]));
        //                                                cmd1.Parameters.Add(new SqlParameter("@CWK_PRIVILEGE_FLAG", r["CWK_PRIVILEGE_FLAG"]));
        //                                                cmd1.Parameters.Add(new SqlParameter("@LAST_UPDATE_DATE_PER", r["LAST_UPDATE_DATE_PER"]));
        //                                                cmd1.Parameters.Add(new SqlParameter("@LAST_UPDATE_DATE_ASS", r["LAST_UPDATE_DATE_ASS"]));




        //                                                conn1.Open();
        //                                                cmd1.ExecuteNonQuery();
        //                                                conn1.Close();
        //                                            }

        //                                        }

        //                                    }


        //                                    catch (Exception ex)
        //                                    {
        //                                        Log4NetService.LogInfo("Error occurred while updating XXHR_CWK_HISTORY table for Person_Id" + r["person_id"] + " " + ex.Message);
        //                                    }

        //                                }
        //                                catch (Exception ex)
        //                                {
        //                                    Log4NetService.LogInfo("Error In Row " + ex.Message);
        //                                }
        //                            }
        //                            conn.Close();

        //                            Log4NetService.LogInfo("Finished reading from DB Link");
        //                        }
        //                    }


        //                }
        //                catch (Exception ex)
        //                {
        //                    Log4NetService.LogInfo("Error occurred while fetching information from HR table: " + ex.Message);
        //                }
        //                try
        //                {
        //                    ConnectionStringSettings conString = ConfigurationManager.ConnectionStrings["Alcoa.HR.Data.ConnectionString"];
        //                    using (SqlConnection conn = new SqlConnection(conString.ConnectionString))
        //                    {

        //                        using (SqlCommand cmd = new SqlCommand("Update HR..POLLING_DATA SET Polling_Date=getDate() where Description='XXHR_CWK_HISTORY'", conn))
        //                        {
        //                            Log4NetService.LogInfo("Updating HR Polling Data Table...");


        //                            conn.Open();
        //                            cmd.ExecuteNonQuery();
        //                            conn.Close();

        //                            Log4NetService.LogInfo("Updated Polling Date Table");
        //                        }
        //                    }


        //                }
        //                catch (Exception ex)
        //                {
        //                    Log4NetService.LogInfo("Error occurred while updating Polling Table for XXHR_CWK_HISTORY");
        //                }

        //            }

        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Log4NetService.LogInfo(ex.Message.ToString() + "\nServer Configuration Account Error. Contact Administrator.");
        //    }
        //    finally
        //    {
        //        if (impContext != null) impContext.Undo();

        //    }

        //}  //old logic

        //public static void UpdateCWKHistory()
        //{
        //    WindowsImpersonationContext impContext = null;
        //    try
        //    {
        //        impContext = NetworkSecurity.Impersonation.ImpersonateUser(
        //                                System.Configuration.ConfigurationManager.AppSettings["WAOCSM_domain"].ToString(),
        //                                System.Configuration.ConfigurationManager.AppSettings["WAOCSM_user"].ToString(),
        //                                System.Configuration.ConfigurationManager.AppSettings["WAOCSM_pass"].ToString());
        //    }
        //    catch (ApplicationException ex)
        //    {
        //        Log4NetService.LogInfo(ex.Message);
        //    }
        //    try
        //    {
        //        if (null != impContext)
        //        {
        //            using (impContext)
        //            {
        //                DateTime pollingdate = DateTime.Now;
        //                try
        //                {
        //                    ConnectionStringSettings conString = ConfigurationManager.ConnectionStrings["Alcoa.HR.Data.ConnectionString"];
        //                    using (SqlConnection conn = new SqlConnection(conString.ConnectionString))
        //                    {

        //                        using (SqlCommand cmd = new SqlCommand("Select Polling_Date from HR..POLLING_DATA WHERE Description='XXHR_CWK_HISTORY'", conn))
        //                        {
        //                            Log4NetService.LogInfo("Fetching information from HR Polling Data Table...");


        //                            conn.Open();
        //                            object objpollingDate = cmd.ExecuteScalar();
        //                            pollingdate = Convert.ToDateTime(objpollingDate);
        //                            conn.Close();

        //                            Log4NetService.LogInfo("Polling datetime for XXHR_CWK_HISTORY=" + pollingdate);
        //                        }
        //                    }


        //                }
        //                catch (Exception ex)
        //                {
        //                    Log4NetService.LogInfo("Error occurred while fetching information from HR table: " + ex.Message);
        //                }
        //                try
        //                {
        //                    ConnectionStringSettings conString = ConfigurationManager.ConnectionStrings["Alcoa.HR.Data.ConnectionString"];
        //                    using (SqlConnection conn = new SqlConnection(conString.ConnectionString))
        //                    {

        //                        using (SqlCommand cmd = new SqlCommand("Select Distinct Person_Id from EBS_HR..APPS.XXHR_SQL_CWK_V WHERE LAST_UPDATE_DATE_PER > '" + pollingdate + "' or LAST_UPDATE_DATE_ASS> '" + pollingdate + "'", conn))
        //                        {
        //                            Log4NetService.LogInfo("Reading information from HR DB Link...");
        //                            string cmdString = cmd.CommandText;
        //                            conn.Open();
        //                            cmd.CommandTimeout = 60000;

        //                            SqlDataReader r = cmd.ExecuteReader();
        //                            while (r.Read())
        //                            {

        //                                try
        //                                {


        //                                    try
        //                                    {
        //                                        UpdateCWKByPersonId(Convert.ToDecimal(r["Person_Id"]));

        //                                    }


        //                                    catch (Exception ex)
        //                                    {
        //                                        Log4NetService.LogInfo("Error occurred while updating XXHR_CWK_HISTORY table for Person_Id" + r["person_id"] + " " + ex.Message);
        //                                    }

        //                                }
        //                                catch (Exception ex)
        //                                {
        //                                    Log4NetService.LogInfo("Error In Row " + ex.Message);
        //                                }
        //                            }
        //                            conn.Close();

        //                            Log4NetService.LogInfo("Finished reading from DB Link");
        //                        }
        //                    }


        //                }
        //                catch (Exception ex)
        //                {
        //                    Log4NetService.LogInfo("Error occurred while fetching information from HR table: " + ex.Message);
        //                }
        //                try
        //                {
        //                    ConnectionStringSettings conString = ConfigurationManager.ConnectionStrings["Alcoa.HR.Data.ConnectionString"];
        //                    using (SqlConnection conn = new SqlConnection(conString.ConnectionString))
        //                    {

        //                        using (SqlCommand cmd = new SqlCommand("Update HR..POLLING_DATA SET Polling_Date=getDate() where Description='XXHR_CWK_HISTORY'", conn))
        //                        {
        //                            Log4NetService.LogInfo("Updating HR Polling Data Table...");


        //                            conn.Open();
        //                            cmd.ExecuteNonQuery();
        //                            conn.Close();

        //                            Log4NetService.LogInfo("Updated Polling Date Table");
        //                        }
        //                    }


        //                }
        //                catch (Exception ex)
        //                {
        //                    Log4NetService.LogInfo("Error occurred while updating Polling Table for XXHR_CWK_HISTORY");
        //                }

        //            }

        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Log4NetService.LogInfo(ex.Message.ToString() + "\nServer Configuration Account Error. Contact Administrator.");
        //    }
        //    finally
        //    {
        //        if (impContext != null) impContext.Undo();

        //    }

        //}

        //public static void UpdateCWKByPersonId(decimal Person_Id)
        //{
        //    try
        //    {
        //        ConnectionStringSettings conString = ConfigurationManager.ConnectionStrings["Alcoa.HR.Data.ConnectionString"];
        //        using (SqlConnection conn = new SqlConnection(conString.ConnectionString))
        //        {

        //            using (SqlCommand cmd = new SqlCommand("Delete from HR..XXHR_CWK_HISTORY where Person_id=" + Person_Id, conn))
        //            {
        //                Log4NetService.LogInfo("Updating Rows for person_id=" + Person_Id);


        //                conn.Open();
        //                cmd.ExecuteNonQuery();
        //                conn.Close();

        //               // Log4NetService.LogInfo("Deleted..");
        //            }
        //        }


        //    }
        //    catch (Exception ex)
        //    {
        //        Log4NetService.LogInfo("Error occurred while updating Polling Table for XXHR_CWK_HISTORY");
        //    }
        //    try
        //    {
        //        ConnectionStringSettings conString = ConfigurationManager.ConnectionStrings["Alcoa.HR.Data.ConnectionString"];
        //        using (SqlConnection conn = new SqlConnection(conString.ConnectionString))
        //        {
        //            using (SqlCommand cmd = new SqlCommand("Select * from EBS_HR..APPS.XXHR_SQL_CWK_V WHERE Person_Id=" + Person_Id, conn))
        //            {
        //                Log4NetService.LogInfo("Reading information from HR DB Link...");
        //                string cmdString = cmd.CommandText;
        //                conn.Open();
        //                cmd.CommandTimeout = 60000;

        //                SqlDataReader r = cmd.ExecuteReader();
        //                while (r.Read())
        //                {

        //                    try
        //                    {


        //                        try
        //                        {
        //                            using (SqlConnection conn1 = new SqlConnection(conString.ConnectionString))
        //                            {

        //                                using (SqlCommand cmd1 = new SqlCommand("XXHR_CWK_History_Insert", conn1))
        //                                {

        //                                    cmd1.CommandType = CommandType.StoredProcedure;
        //                                    cmd1.Parameters.Add(new SqlParameter("@AssignmentId", r["ASSIGNMENT_ID"]));
        //                                    cmd1.Parameters.Add(new SqlParameter("@PERSON_ID", r["PERSON_ID"]));
        //                                    cmd1.Parameters.Add(new SqlParameter("@CWK_NBR", r["CWK_NBR"]));
        //                                    cmd1.Parameters.Add(new SqlParameter("@FULL_NAME", r["FULL_NAME"]));
        //                                    cmd1.Parameters.Add(new SqlParameter("@LAST_NAME", r["LAST_NAME"]));
        //                                    cmd1.Parameters.Add(new SqlParameter("@FIRST_NAME", r["FIRST_NAME"]));
        //                                    cmd1.Parameters.Add(new SqlParameter("@MIDDLE_NAMES", r["MIDDLE_NAMES"]));
        //                                    cmd1.Parameters.Add(new SqlParameter("@AOA_PREF_NAME", r["AOA_PREF_NAME"]));
        //                                    cmd1.Parameters.Add(new SqlParameter("@Gender", r["Gender"]));
        //                                    cmd1.Parameters.Add(new SqlParameter("@LOGINID", r["LOGINID"]));
        //                                    cmd1.Parameters.Add(new SqlParameter("@PER_TYPE_ID", r["PER_TYPE_ID"]));
        //                                    cmd1.Parameters.Add(new SqlParameter("@SERVICE_DATE", r["SERVICE_DATE"]));
        //                                    cmd1.Parameters.Add(new SqlParameter("@ORIGINAL_DATE_OF_HIRE", r["ORIGINAL_DATE_OF_HIRE"]));
        //                                    cmd1.Parameters.Add(new SqlParameter("@TERMINATION_DATE", r["TERMINATION_DATE"]));
        //                                    cmd1.Parameters.Add(new SqlParameter("@Pos_Descr", r["Pos_Descr"]));
        //                                    cmd1.Parameters.Add(new SqlParameter("@Supervisor_Id", r["Supervisor_Id"]));
        //                                    cmd1.Parameters.Add(new SqlParameter("@EFFECTIVE_START_DATE", r["EFFECTIVE_START_DATE"]));
        //                                    cmd1.Parameters.Add(new SqlParameter("@EFFECTIVE_END_DATE", r["EFFECTIVE_END_DATE"]));
        //                                    cmd1.Parameters.Add(new SqlParameter("@ALCOA_PRIV", r["ALCOA_PRIV"]));
        //                                    cmd1.Parameters.Add(new SqlParameter("@LABOUR_CLASS", r["LABOUR_CLASS"]));
        //                                    cmd1.Parameters.Add(new SqlParameter("@NON_PREFF_SUPP", r["NON_PREFF_SUPP"]));
        //                                    cmd1.Parameters.Add(new SqlParameter("@SPONSOR_ID", r["SPONSOR_ID"]));
        //                                    cmd1.Parameters.Add(new SqlParameter("@SUPPLY_SUP", r["SUPPLY_SUP"]));
        //                                    cmd1.Parameters.Add(new SqlParameter("@LOCATION_CODE", r["LOCATION_CODE"]));
        //                                    cmd1.Parameters.Add(new SqlParameter("@ORGANIZATION_ID", r["ORGANIZATION_ID"]));
        //                                    cmd1.Parameters.Add(new SqlParameter("@ORG_CODE", r["ORG_CODE"]));
        //                                    cmd1.Parameters.Add(new SqlParameter("@DEPT_ORG_CODE", r["DEPT_ORG_CODE"]));
        //                                    cmd1.Parameters.Add(new SqlParameter("@OP_CENTRE", r["OP_CENTRE"]));
        //                                    cmd1.Parameters.Add(new SqlParameter("@NAME", r["NAME"]));
        //                                    cmd1.Parameters.Add(new SqlParameter("@LBC", r["LBC"]));
        //                                    cmd1.Parameters.Add(new SqlParameter("@DEPT", r["DEPT"]));
        //                                    cmd1.Parameters.Add(new SqlParameter("@ACCOUNT", r["ACCOUNT"]));
        //                                    cmd1.Parameters.Add(new SqlParameter("@CWK_VENDOR_ID", r["CWK_VENDOR_ID"]));
        //                                    cmd1.Parameters.Add(new SqlParameter("@CWK_VENDOR_NO", r["CWK_VENDOR_NO"]));
        //                                    cmd1.Parameters.Add(new SqlParameter("@CWK_VENDOR_NAME", r["CWK_VENDOR_NAME"]));
        //                                    cmd1.Parameters.Add(new SqlParameter("@CWK_NON_PREFERRED_SUPP_NAME", r["CWK_NON_PREFERRED_SUPP_NAME"]));
        //                                    cmd1.Parameters.Add(new SqlParameter("@CWK_SPONSOR_ID", r["CWK_SPONSOR_ID"]));
        //                                    cmd1.Parameters.Add(new SqlParameter("@CWK_SUPP_SUPERVISOR_ID", r["CWK_SUPP_SUPERVISOR_ID"]));
        //                                    cmd1.Parameters.Add(new SqlParameter("@CWK_PRIVILEGE_FLAG", r["CWK_PRIVILEGE_FLAG"]));
        //                                    cmd1.Parameters.Add(new SqlParameter("@LAST_UPDATE_DATE_PER", r["LAST_UPDATE_DATE_PER"]));
        //                                    cmd1.Parameters.Add(new SqlParameter("@LAST_UPDATE_DATE_ASS", r["LAST_UPDATE_DATE_ASS"]));




        //                                    conn1.Open();
        //                                    cmd1.ExecuteNonQuery();
        //                                    conn1.Close();
        //                                    Log4NetService.LogInfo("Updated..");
        //                                }

        //                            }

        //                        }


        //                        catch (Exception ex)
        //                        {
        //                            Log4NetService.LogInfo("Error occurred while updating XXHR_CWK_HISTORY table for Person_Id" + r["person_id"] + " " + ex.Message);
        //                        }

        //                    }
        //                    catch (Exception ex)
        //                    {
        //                        Log4NetService.LogInfo("Error In Row " + ex.Message);
        //                    }
        //                }
        //                conn.Close();

        //                Log4NetService.LogInfo("Finished reading from DB Link");
        //            }

        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        Log4NetService.LogInfo("Error :" + ex.Message);
        //    }


        //}



        public static void UpdateCWKHistory()
        {
            // Cindi Thornton 17/9/15 - scheduled tasks now run as service account, this isn't needed. Remove all impersonation related code
            //WindowsImpersonationContext impContext = null;
            //try
            //{
            //    impContext = NetworkSecurity.Impersonation.ImpersonateUser(
            //                            System.Configuration.ConfigurationManager.AppSettings["WAOCSM_domain"].ToString(),
            //                            System.Configuration.ConfigurationManager.AppSettings["WAOCSM_user"].ToString(),
            //                            System.Configuration.ConfigurationManager.AppSettings["WAOCSM_pass"].ToString());
            //}
            //catch (ApplicationException ex)
            //{
            //    Log4NetService.LogInfo(ex.Message);
            //}
            //DT Batch Failing
            bool PollingdateUpdate = true;
            try
            {
                //if (null != impContext)
                //{
                //    using (impContext)
                //    {
                        string pollingdate = string.Empty;
                        //DateTime pollingdate = DateTime.Now;
                        try
                        {
                            ConnectionStringSettings conString = ConfigurationManager.ConnectionStrings["Alcoa.HR.Data.ConnectionString"];
                            using (SqlConnection conn = new SqlConnection(conString.ConnectionString))
                            {

                                using (SqlCommand cmd = new SqlCommand("Select Convert(varchar(30),Polling_Date,120) from HR..POLLING_DATA WHERE Description='XXHR_CWK_HISTORY'", conn))
                                {
                                    Log4NetService.LogInfo("Fetching information from HR Polling Data Table...");


                                    conn.Open();
                                    object objpollingDate = cmd.ExecuteScalar();
                                    pollingdate = Convert.ToString(objpollingDate);
                                    conn.Close();

                                    Log4NetService.LogInfo("Polling datetime for XXHR_CWK_HISTORY=" + pollingdate);
                                }
                            }


                        }
                        catch (Exception ex)
                        {
                            //DT Batch Failing
                            PollingdateUpdate = false;
                            Log4NetService.LogInfo("Error occurred while fetching information from HR table: " + ex.Message);
                            throw;
                        }

                        //To Get DataTable containg 
                        DataSet ds = new DataSet();

                        try
                        {
                            ConnectionStringSettings conString = ConfigurationManager.ConnectionStrings["Alcoa.HR.Data.ConnectionString"];
                            using (SqlConnection conn = new SqlConnection(conString.ConnectionString))
                            {
                                //SP 1509 changes
                                //string strQuery = "Select * from EBS_HR..APPS.XXHR_SQL_CWK_V  WHERE Person_Id in (Select Distinct Person_Id from EBS_HR..APPS.XXHR_SQL_CWK_V WHERE LAST_UPDATE_DATE_PER > CAST('" + pollingdate + "' as datetime) or LAST_UPDATE_DATE_ASS> CAST('" + pollingdate + "' AS DATETIME))";
                                string strQuery = "Select * from EBS_HR..APPS.XXHR_GSI_INTF_HR_TO_CSMS_CWK_V  WHERE Person_Id in (Select Distinct Person_Id from EBS_HR..APPS.XXHR_GSI_INTF_HR_TO_CSMS_CWK_V WHERE LAST_UPDATE_DATE_PER > CAST('" + pollingdate + "' as datetime) or LAST_UPDATE_DATE_ASS> CAST('" + pollingdate + "' AS DATETIME))";
                                //Not used the below query. So keep it commented.
                                //string strQuery = "Select * from EBS_HR..APPS.XXHR_SQL_CWK_V  WHERE Person_Id in (Select Distinct Person_Id from EBS_HR..APPS.XXHR_SQL_CWK_V WHERE LAST_UPDATE_DATE_PER > Convert(datetime,'" + pollingdate + "',103) or LAST_UPDATE_DATE_ASS> Convert(datetime,'" + pollingdate + "',103))";

                                using (SqlCommand cmd = new SqlCommand(strQuery, conn))
                                {
                                    cmd.CommandTimeout = 60000;
                                    //SP 1509 changes
                                    //Log4NetService.LogInfo("Getting all Rows from XXHR_SQL_CWK_V");
                                    Log4NetService.LogInfo("Getting all Rows from XXHR_GSI_INTF_HR_TO_CSMS_CWK_V");

                                    SqlDataAdapter sqlda = new SqlDataAdapter(cmd);
                                    sqlda.Fill(ds);

                                    //conn.Open();
                                    //cmd.ExecuteNonQuery();
                                    //conn.Close();                                    
                                    // Log4NetService.LogInfo("Deleted..");
                                }
                            }


                        }
                        catch (Exception ex)
                        {
                            //DT Batch Failing
                            //Log4NetService.LogInfo("Error occurred while updating Polling Table for XXHR_CWK_HISTORY");
                            PollingdateUpdate = false;
                            //SP 1509 changes
                            //Log4NetService.LogInfo("Error occurred while Getting all Rows from XXHR_SQL_CWK_V: " + ex.Message);
                            Log4NetService.LogInfo("Error occurred while Getting all Rows from XXHR_GSI_INTF_HR_TO_CSMS_CWK_V: " + ex.Message);
                            throw;
                        }

                        //To get DataTable
                        //DT Batch Failing
                        DataSet FulDs = new DataSet();
                        DataTable FulDt = new DataTable();

                        try
                        {
                            ConnectionStringSettings conString = ConfigurationManager.ConnectionStrings["Alcoa.HR.Data.ConnectionString"];
                            using (SqlConnection conn = new SqlConnection(conString.ConnectionString))
                            {
                                //SP 1509 changes
                                //using (SqlCommand cmd = new SqlCommand("Select Distinct Person_Id from EBS_HR..APPS.XXHR_SQL_CWK_V WHERE LAST_UPDATE_DATE_PER > CAST('" + pollingdate + "' AS DATETIME) or LAST_UPDATE_DATE_ASS> CAST('" + pollingdate + "' AS DATETIME)", conn))
                                using (SqlCommand cmd = new SqlCommand("Select Distinct Person_Id from EBS_HR..APPS.XXHR_GSI_INTF_HR_TO_CSMS_CWK_V WHERE LAST_UPDATE_DATE_PER > CAST('" + pollingdate + "' AS DATETIME) or LAST_UPDATE_DATE_ASS> CAST('" + pollingdate + "' AS DATETIME)", conn))
                                //The below query not used so keep it commented.
                                //using (SqlCommand cmd = new SqlCommand("Select Distinct Person_Id from EBS_HR..APPS.XXHR_SQL_CWK_V WHERE LAST_UPDATE_DATE_PER > Convert(datetime,'" + pollingdate + "',103) or LAST_UPDATE_DATE_ASS> Convert(datetime,'" + pollingdate + "',103)", conn))
                                {
                                    Log4NetService.LogInfo("Reading information from HR DB Link...");
                                    string cmdString = cmd.CommandText;
                                    conn.Open();
                                    cmd.CommandTimeout = 60000;
                                    //DT Batch Failing
                                    SqlDataAdapter sqlfulda = new SqlDataAdapter(cmd);
                                    sqlfulda.Fill(FulDs);
                                    conn.Close();
                                    Log4NetService.LogInfo("Finished reading from DB Link");                                    
                                    Log4NetService.LogInfo("Start updating HR table...");                                    
                                    FulDt = FulDs.Tables[0];

                                    //DT Batch Failing
                                    //SqlDataReader r = cmd.ExecuteReader();
                                    //while (r.Read())
                                    foreach (DataRow dr in FulDt.Rows)
                                    {

                                        try
                                        {


                                            try
                                            {
                                                UpdateCWKByPersonId(Convert.ToDecimal(dr["Person_Id"]), ds);

                                            }


                                            catch (Exception ex)
                                            {
                                                PollingdateUpdate = false;
                                                Log4NetService.LogInfo("Error occurred while updating XXHR_CWK_HISTORY table for Person_Id" + dr["person_id"] + " " + ex.Message);
                                            }

                                        }
                                        catch (Exception ex)
                                        {
                                            PollingdateUpdate = false;
                                            Log4NetService.LogInfo("Error In Row " + ex.Message);
                                        }
                                    }
                                    //DT Batch Failing
                                    Log4NetService.LogInfo("Finished updating HR table");
                                    //conn.Close();

                                    //Log4NetService.LogInfo("Finished reading from DB Link");
                                }
                            }


                        }
                        catch (Exception ex)
                        {
                            //DT Batch Failing  
                            PollingdateUpdate = false;
                            Log4NetService.LogInfo("Error occurred while fetching information from HR DB Link: " + ex.Message);
                            throw;
                        }
                        try
                        {
                            ConnectionStringSettings conString = ConfigurationManager.ConnectionStrings["Alcoa.HR.Data.ConnectionString"];
                            using (SqlConnection conn = new SqlConnection(conString.ConnectionString))
                            {
                                //DT Batch Failing
                                if (PollingdateUpdate == true)
                                {
                                    using (SqlCommand cmd = new SqlCommand("Update HR..POLLING_DATA SET Polling_Date=getDate() where Description='XXHR_CWK_HISTORY'", conn))
                                    {
                                        Log4NetService.LogInfo("Updating HR Polling Data Table...");


                                        conn.Open();
                                        cmd.ExecuteNonQuery();
                                        conn.Close();

                                        Log4NetService.LogInfo("Updated Polling Date Table");
                                    }
                                }
                                else 
                                {
                                    Log4NetService.LogInfo("HR Polling Data Table Not Updated");
                                }
                            }


                        }
                        catch (Exception ex)
                        {
                            //DT Batch Failing :  Added + ex.Message
                            Log4NetService.LogInfo("Error occurred while updating Polling Table for XXHR_CWK_HISTORY :" + ex.Message);
                            throw;
                        }

                    //}

                //}
            }
            catch (Exception ex)
            {
                Log4NetService.LogInfo(ex.Message.ToString() + "\nServer Configuration Account Error. Contact Administrator.");
                throw;
            }
            finally
            {
                //if (impContext != null) impContext.Undo();

            }

        }

        public static void UpdateCWKByPersonId(decimal Person_Id, DataSet _ds)
        {
            try
            {
                ConnectionStringSettings conString = ConfigurationManager.ConnectionStrings["Alcoa.HR.Data.ConnectionString"];
                using (SqlConnection conn = new SqlConnection(conString.ConnectionString))
                {

                    using (SqlCommand cmd = new SqlCommand("Delete from HR..XXHR_CWK_HISTORY where Person_id=" + Person_Id, conn))
                    {
                        Log4NetService.LogInfo("Updating Rows for person_id=" + Person_Id);


                        conn.Open();
                        cmd.ExecuteNonQuery();
                        conn.Close();

                        // Log4NetService.LogInfo("Deleted..");
                    }
                }


            }
            catch (Exception ex)
            {
                //DT Batch Failing : added + ex.Message
                Log4NetService.LogInfo("Error occurred while updating Polling Table for XXHR_CWK_HISTORY :" + ex.Message);
                throw;
            }

            try
            {
                ConnectionStringSettings conString = ConfigurationManager.ConnectionStrings["Alcoa.HR.Data.ConnectionString"];


                DataTable dtData = _ds.Tables[0];
                DataView dv = dtData.DefaultView;
                dv.RowFilter = "Person_Id=" + Person_Id;
                DataTable dtPerson = dv.ToTable();

                //using (SqlConnection conn = new SqlConnection(conString.ConnectionString))
                //{
                //    using (SqlCommand cmd = new SqlCommand("Select * from EBS_HR..APPS.XXHR_SQL_CWK_V WHERE Person_Id=" + Person_Id, conn))
                //    {
                //        Log4NetService.LogInfo("Reading information from HR DB Link...");
                //        string cmdString = cmd.CommandText;
                //        conn.Open();
                //        cmd.CommandTimeout = 60000;

                //        SqlDataReader r = cmd.ExecuteReader();
                //        while (r.Read())
                //        {

                //            try
                //            {
                foreach (DataRow r in dtPerson.Rows)
                {

                    try
                    {
                        using (SqlConnection conn1 = new SqlConnection(conString.ConnectionString))
                        {

                            using (SqlCommand cmd1 = new SqlCommand("XXHR_CWK_History_Insert", conn1))
                            {

                                cmd1.CommandType = CommandType.StoredProcedure;
                                cmd1.Parameters.Add(new SqlParameter("@AssignmentId", r["ASSIGNMENT_ID"]));
                                cmd1.Parameters.Add(new SqlParameter("@PERSON_ID", r["PERSON_ID"]));
                                cmd1.Parameters.Add(new SqlParameter("@CWK_NBR", r["CWK_NBR"]));
                                cmd1.Parameters.Add(new SqlParameter("@FULL_NAME", r["FULL_NAME"]));
                                cmd1.Parameters.Add(new SqlParameter("@LAST_NAME", r["LAST_NAME"]));
                                cmd1.Parameters.Add(new SqlParameter("@FIRST_NAME", r["FIRST_NAME"]));
                                cmd1.Parameters.Add(new SqlParameter("@MIDDLE_NAMES", r["MIDDLE_NAMES"]));
                                cmd1.Parameters.Add(new SqlParameter("@AOA_PREF_NAME", r["AOA_PREF_NAME"]));
                                cmd1.Parameters.Add(new SqlParameter("@Gender", r["Gender"]));
                                cmd1.Parameters.Add(new SqlParameter("@LOGINID", r["LOGINID"]));
                                cmd1.Parameters.Add(new SqlParameter("@PER_TYPE_ID", r["PER_TYPE_ID"]));
                                cmd1.Parameters.Add(new SqlParameter("@SERVICE_DATE", r["SERVICE_DATE"]));
                                cmd1.Parameters.Add(new SqlParameter("@ORIGINAL_DATE_OF_HIRE", r["ORIGINAL_DATE_OF_HIRE"]));
                                cmd1.Parameters.Add(new SqlParameter("@TERMINATION_DATE", r["TERMINATION_DATE"]));
                                cmd1.Parameters.Add(new SqlParameter("@Pos_Descr", r["Pos_Descr"]));
                                cmd1.Parameters.Add(new SqlParameter("@Supervisor_Id", r["Supervisor_Id"]));
                                cmd1.Parameters.Add(new SqlParameter("@EFFECTIVE_START_DATE", r["EFFECTIVE_START_DATE"]));
                                cmd1.Parameters.Add(new SqlParameter("@EFFECTIVE_END_DATE", r["EFFECTIVE_END_DATE"]));
                                cmd1.Parameters.Add(new SqlParameter("@ALCOA_PRIV", r["ALCOA_PRIV"]));
                                cmd1.Parameters.Add(new SqlParameter("@LABOUR_CLASS", r["LABOUR_CLASS"]));
                                cmd1.Parameters.Add(new SqlParameter("@NON_PREFF_SUPP", r["NON_PREFF_SUPP"]));
                                cmd1.Parameters.Add(new SqlParameter("@SPONSOR_ID", r["SPONSOR_ID"]));
                                cmd1.Parameters.Add(new SqlParameter("@SUPPLY_SUP", r["SUPPLY_SUP"]));
                                cmd1.Parameters.Add(new SqlParameter("@LOCATION_CODE", r["LOCATION_CODE"]));
                                cmd1.Parameters.Add(new SqlParameter("@ORGANIZATION_ID", r["ORGANIZATION_ID"]));
                                cmd1.Parameters.Add(new SqlParameter("@ORG_CODE", r["ORG_CODE"]));
                                cmd1.Parameters.Add(new SqlParameter("@DEPT_ORG_CODE", r["DEPT_ORG_CODE"]));
                                cmd1.Parameters.Add(new SqlParameter("@OP_CENTRE", r["OP_CENTRE"]));
                                cmd1.Parameters.Add(new SqlParameter("@NAME", r["NAME"]));
                                cmd1.Parameters.Add(new SqlParameter("@LBC", r["LBC"]));
                                cmd1.Parameters.Add(new SqlParameter("@DEPT", r["DEPT"]));
                                cmd1.Parameters.Add(new SqlParameter("@ACCOUNT", r["ACCOUNT"]));
                                cmd1.Parameters.Add(new SqlParameter("@CWK_VENDOR_ID", r["CWK_VENDOR_ID"]));
                                cmd1.Parameters.Add(new SqlParameter("@CWK_VENDOR_NO", r["CWK_VENDOR_NO"]));
                                cmd1.Parameters.Add(new SqlParameter("@CWK_VENDOR_NAME", r["CWK_VENDOR_NAME"]));
                                cmd1.Parameters.Add(new SqlParameter("@CWK_NON_PREFERRED_SUPP_NAME", r["CWK_NON_PREFERRED_SUPP_NAME"]));
                                cmd1.Parameters.Add(new SqlParameter("@CWK_SPONSOR_ID", r["CWK_SPONSOR_ID"]));
                                cmd1.Parameters.Add(new SqlParameter("@CWK_SUPP_SUPERVISOR_ID", r["CWK_SUPP_SUPERVISOR_ID"]));
                                cmd1.Parameters.Add(new SqlParameter("@CWK_PRIVILEGE_FLAG", r["CWK_PRIVILEGE_FLAG"]));
                                cmd1.Parameters.Add(new SqlParameter("@LAST_UPDATE_DATE_PER", r["LAST_UPDATE_DATE_PER"]));
                                cmd1.Parameters.Add(new SqlParameter("@LAST_UPDATE_DATE_ASS", r["LAST_UPDATE_DATE_ASS"]));


                                conn1.Open();
                                cmd1.ExecuteNonQuery();
                                conn1.Close();
                                Log4NetService.LogInfo("Updated..");
                            }

                        }

                    }


                    catch (Exception ex)
                    {                       
                        Log4NetService.LogInfo("Error occurred while updating XXHR_CWK_HISTORY table for Person_Id" + r["person_id"] + " " + ex.Message);
                        throw;
                    }
                }

                //        }
                //        catch (Exception ex)
                //        {
                //            Log4NetService.LogInfo("Error In Row " + ex.Message);
                //        }
                //    }
                //    conn.Close();

                //    Log4NetService.LogInfo("Finished reading from DB Link");
                //}

                // }

            }
            catch (Exception ex)
            {
                Log4NetService.LogInfo("Error :" + ex.Message);
                throw;
            }


        }
                
        public static void UpdateOLMTrainingCourse()
        {
            // Cindi Thornton 17/9/15 - scheduled tasks now run as service account, this isn't needed. Remove all impersonation related code
            //WindowsImpersonationContext impContext = null;
            //try
            //{
            //    impContext = NetworkSecurity.Impersonation.ImpersonateUser(
            //                            System.Configuration.ConfigurationManager.AppSettings["WAOCSM_domain"].ToString(),
            //                            System.Configuration.ConfigurationManager.AppSettings["WAOCSM_user"].ToString(),
            //                            System.Configuration.ConfigurationManager.AppSettings["WAOCSM_pass"].ToString());
            //}
            //catch (ApplicationException ex)
            //{
            //    Log4NetService.LogInfo(ex.Message);
            //}

            //DT Batch Failing
            bool PollingdateUpdate = true;

            try
            {
                //if (null != impContext)
                //{
                //    using (impContext)
                //    {
                        //DateTime pollingdate = DateTime.Now;
                        string pollingdate = string.Empty;
                        DataTable dt=new DataTable();
                        try
                        {
                            ConnectionStringSettings conString = ConfigurationManager.ConnectionStrings["Alcoa.HR.Data.ConnectionString"];
                            using (SqlConnection conn = new SqlConnection(conString.ConnectionString))
                            {
                                using (SqlCommand cmd = new SqlCommand("Select Convert(varchar(30),Polling_Date,120) from HR..POLLING_DATA WHERE Description='XXHR_OLM_TRNG_COURSE'", conn))
                                //using (SqlCommand cmd = new SqlCommand("Select Day(Polling_Date), Month(Polling_Date), Year(Polling_Date), Polling_Date from HR..POLLING_DATA WHERE Description='XXHR_OLM_TRNG_COURSE'", conn))
                                {
                                    //DT 3127 Changes
                                    Log4NetService.LogInfo("Fetching information from HR POLLING_DATA Table...");


                                    conn.Open();
                                    //SqlDataAdapter sqlda = new SqlDataAdapter(cmd);
                                        //sqlda.Fill(dt);
                                    object objpollingDate = cmd.ExecuteScalar();
                                    pollingdate = Convert.ToString(objpollingDate);
                                    conn.Close();

                                    Log4NetService.LogInfo("Polling datetime for XXHR_OLM_TRNG_COURSE=" + pollingdate);
                                }
                            }


                        }
                        catch (Exception ex)
                        {
                            //DT Batch Failing
                            PollingdateUpdate = false;
                            Log4NetService.LogInfo("Error occurred while fetching information from HR table: " + ex.Message);
                            throw;
                        }

                        //DT Batch Failing
                        DataSet FulDs = new DataSet();
                        DataTable FulDt = new DataTable();

                        try
                        {
                            ConnectionStringSettings conString = ConfigurationManager.ConnectionStrings["Alcoa.HR.Data.ConnectionString"];
                            using (SqlConnection conn = new SqlConnection(conString.ConnectionString))
                            {
                                //using (SqlCommand cmd = new SqlCommand("Select * from EBS_HR..APPS.XXHR_SQL_TRNG_CRSE_V WHERE LAST_UPDATE_DATE > Convert(datetime,'" + pollingdate + "',103)", conn))
                                //string strTime = dt.Rows[0][3].ToString().Split(" ")
                                //string strDate = dt.Rows[0][2].ToString() + "-" + dt.Rows[0][1].ToString() + "-" + dt.Rows[0][0].ToString();

                                //SP 1509 changes
                                //using (SqlCommand cmd = new SqlCommand("Select * from EBS_HR..APPS.XXHR_SQL_TRNG_CRSE_V WHERE LAST_UPDATE_DATE > CAST('" + pollingdate + "' AS DATETIME)", conn))
                                using (SqlCommand cmd = new SqlCommand("Select * from EBS_HR..APPS.XXHR_GSI_INTF_CSMS_TRNG_CRSE_V WHERE LAST_UPDATE_DATE > CAST('" + pollingdate + "' AS DATETIME)", conn))
                                {                                    
                                    Log4NetService.LogInfo("Reading information from HR DB Link...");
                                    string cmdString = cmd.CommandText;
                                    conn.Open();
                                    cmd.CommandTimeout = 60000;

                                    //DT Batch Failing
                                    SqlDataAdapter sqlfulda = new SqlDataAdapter(cmd);
                                    sqlfulda.Fill(FulDs);
                                    conn.Close();
                                    Log4NetService.LogInfo("Finished reading from HR DB Link");
                                    Log4NetService.LogInfo("Start updating HR table...");
                                    FulDt = FulDs.Tables[0];
                                    
                                    //DT Batch Failing
                                    //SqlDataReader r = cmd.ExecuteReader();
                                    //while (r.Read())
                                    foreach (DataRow dr in FulDt.Rows)
                                    {

                                        try
                                        {
                                           


                                            try
                                            {
                                                using (SqlConnection conn1 = new SqlConnection(conString.ConnectionString))
                                                {

                                                    using (SqlCommand cmd1 = new SqlCommand("XXHR_OLM_TRNG_COURSE_Update", conn1))
                                                    {

                                                        cmd1.CommandType = CommandType.StoredProcedure;
                                                        cmd1.Parameters.Add(new SqlParameter("@COURSE_NAME", dr["COURSE_NAME"]));
                                                        cmd1.Parameters.Add(new SqlParameter("@VERSION_NAME", dr["VERSION_NAME"]));
                                                        cmd1.Parameters.Add(new SqlParameter("@DESCRIPTION", dr["DESCRIPTION"]));
                                                        cmd1.Parameters.Add(new SqlParameter("@DURATION", dr["DURATION"]));
                                                        cmd1.Parameters.Add(new SqlParameter("@DURATION_UNITS", dr["DURATION_UNITS"]));
                                                        cmd1.Parameters.Add(new SqlParameter("@START_DATE", dr["START_DATE"]));
                                                        cmd1.Parameters.Add(new SqlParameter("@END_DATE", dr["END_DATE"]));
                                                        cmd1.Parameters.Add(new SqlParameter("@TAV_INFORMATION2", dr["TAV_INFORMATION2"]));
                                                        cmd1.Parameters.Add(new SqlParameter("@BUSINESS_GROUP_ID", dr["BUSINESS_GROUP_ID"]));
                                                        //cmd1.Parameters.Add(new SqlParameter("@LAST_UPDATE_DATE", r["LAST_UPDATE_DATE"]));

                                                        
                                                        conn1.Open();
                                                        cmd1.ExecuteNonQuery();
                                                        conn1.Close();
                                                    }

                                                }

                                            }


                                            catch (Exception ex)
                                            {
                                                //DT Batch Failing
                                                PollingdateUpdate = false;
                                                Log4NetService.LogInfo("Error occurred while updating XXHR_OLM_TRNG_COURSE table for COURSE_NAME" + dr["COURSE_NAME"] + " " + ex.Message);
                                            }

                                        }
                                        catch (Exception ex)
                                        {
                                            //DT Batch Failing
                                            PollingdateUpdate = false;
                                            Log4NetService.LogInfo("Error In Row " + ex.Message);
                                        }
                                    }
                                    //DT 3127 Changes
                                    if (PollingdateUpdate == true)
                                    { Log4NetService.LogInfo("HR.XXHR_OLM_TRNG_COURSE table is being read and updated."); }
                                    //DT Batch Failing
                                    Log4NetService.LogInfo("Finished updating HR table");
                                    //conn.Close();

                                    //Log4NetService.LogInfo("Finished reading from DB Link");
                                }
                            }


                        }
                        catch (Exception ex)
                        {
                            //DT Batch Failing
                            PollingdateUpdate = false;
                            Log4NetService.LogInfo("Error occurred while fetching information from HR table: " + ex.Message);
                            throw;
                        }
                        try
                        {
                            ConnectionStringSettings conString = ConfigurationManager.ConnectionStrings["Alcoa.HR.Data.ConnectionString"];
                            using (SqlConnection conn = new SqlConnection(conString.ConnectionString))
                            {
                                //DT Batch Failing
                                if (PollingdateUpdate == true)
                                {
                                    using (SqlCommand cmd = new SqlCommand("Update HR..POLLING_DATA SET Polling_Date=getDate() where Description='XXHR_OLM_TRNG_COURSE'", conn))
                                    {
                                        //DT 3127 Changes
                                        Log4NetService.LogInfo("Updating HR POLLING_DATA Table...");


                                        conn.Open();
                                        cmd.ExecuteNonQuery();
                                        //DT 3127 Changes
                                        cmd.CommandText = "Select Convert(varchar(30),Polling_Date,120) from HR..POLLING_DATA WHERE Description='XXHR_OLM_TRNG_COURSE'";
                                        object NewOLMpollingDate = cmd.ExecuteScalar();
                                        conn.Close();
                                        Log4NetService.LogInfo("New Polling datetime will be set as :" + Convert.ToString(NewOLMpollingDate));
                                        Log4NetService.LogInfo("Updated POLLING_DATA Table");
                                    }
                                }
                                else
                                {
                                    Log4NetService.LogInfo("HR POLLING_DATA Table Not Updated");
                                }
                            }


                        }
                        catch (Exception ex)
                        {
                            //DT Batch Failing : added ex.message
                            Log4NetService.LogInfo("Error occurred while updating Polling Table for XXHR_OLM_TRNG_COURSE: " + ex.Message);
                            throw;
                        }

                    //}

                //}
            }
            catch (Exception ex)
            {
                Log4NetService.LogInfo(ex.Message.ToString() + "\nServer Configuration Account Error. Contact Administrator.");
            }
            finally
            {
                //if (impContext != null) impContext.Undo();

            }

        }

        public static void UpdateAddTraining()
        {
            // Cindi Thornton 17/9/15 - scheduled tasks now run as service account, this isn't needed. Remove all impersonation related code
            //WindowsImpersonationContext impContext = null;
            //try
            //{
            //    impContext = NetworkSecurity.Impersonation.ImpersonateUser(
            //                            System.Configuration.ConfigurationManager.AppSettings["WAOCSM_domain"].ToString(),
            //                            System.Configuration.ConfigurationManager.AppSettings["WAOCSM_user"].ToString(),
            //                            System.Configuration.ConfigurationManager.AppSettings["WAOCSM_pass"].ToString());
            //}
            //catch (ApplicationException ex)
            //{
            //    Log4NetService.LogInfo(ex.Message);
            //}

            //DT Batch Failing
            bool PollingdateUpdate = true;

            try
            {
                //if (null != impContext)
                //{
                //    using (impContext)
                //    {
                        string pollingdate = string.Empty;
                        //DateTime pollingdate = DateTime.Now;
                        try
                        {
                            ConnectionStringSettings conString = ConfigurationManager.ConnectionStrings["Alcoa.HR.Data.ConnectionString"];
                            using (SqlConnection conn = new SqlConnection(conString.ConnectionString))
                            {

                                using (SqlCommand cmd = new SqlCommand("Select Convert(varchar(30),Polling_Date,120) from HR..POLLING_DATA WHERE Description='XXHR_ADD_TRNG'", conn))
                                {
                                    //DT 3127 Changes
                                    Log4NetService.LogInfo("Fetching information from HR POLLING_DATA Table...");
                                    
                                    conn.Open();
                                    object objpollingDate = cmd.ExecuteScalar();
                                    pollingdate = Convert.ToString(objpollingDate);
                                    conn.Close();

                                    Log4NetService.LogInfo("Polling datetime for XXHR_ADD_TRNG=" + pollingdate);
                                }
                            }


                        }
                        catch (Exception ex)
                        {
                            //DT Batch Failing
                            PollingdateUpdate = false;
                            Log4NetService.LogInfo("Error occurred while fetching information from HR table: " + ex.Message);
                            throw;
                        }
                        
                        //DT Batch Failing
                        DataSet FulDs = new DataSet();
                        DataTable FulDt = new DataTable();

                        try
                        {
                            ConnectionStringSettings conString = ConfigurationManager.ConnectionStrings["Alcoa.HR.Data.ConnectionString"];
                            using (SqlConnection conn = new SqlConnection(conString.ConnectionString))
                            {

                                //using (SqlCommand cmd = new SqlCommand("(Select * from EBS_HR..APPS.XXHR_SQL_EMP_ARP_ADD_V WHERE LAST_UPDATE_DATE > Convert(datetime,'" + pollingdate + "',103)) UNION ALL (Select * from EBS_HR..APPS.XXHR_SQL_CWK_ADD_TRNG_V WHERE LAST_UPDATE_DATE > Convert(datetime,'" + pollingdate + "',103))", conn))

                                //SP 1509 changes //Waiting for HCM view
                                //using (SqlCommand cmd = new SqlCommand("(Select * from EBS_HR..APPS.XXHR_SQL_EMP_ARP_ADD_V WHERE LAST_UPDATE_DATE > CAST('" + pollingdate + "' AS DATETIME)) UNION ALL (Select * from EBS_HR..APPS.XXHR_SQL_CWK_ADD_TRNG_V WHERE LAST_UPDATE_DATE > CAST('" + pollingdate + "' AS DATETIME))", conn))
                                using (SqlCommand cmd = new SqlCommand("(Select * from EBS_HR..APPS.XXHR_GSI_INTF_CSMS_EMPARPADD_V WHERE LAST_UPDATE_DATE > CAST('" + pollingdate + "' AS DATETIME)) UNION ALL (Select * from EBS_HR..APPS.XXHR_GSI_INTF_CSMS_ADD_TRNG_V WHERE LAST_UPDATE_DATE > CAST('" + pollingdate + "' AS DATETIME))", conn))
                                {
                                    Log4NetService.LogInfo("Reading information from HR DB Link...");
                                    string cmdString = cmd.CommandText;
                                    conn.Open();
                                    cmd.CommandTimeout = 60000;

                                    //DT Batch Failing
                                    SqlDataAdapter sqlfulda = new SqlDataAdapter(cmd);
                                    sqlfulda.Fill(FulDs);
                                    conn.Close();
                                    Log4NetService.LogInfo("Finished reading from HR DB Link");
                                    Log4NetService.LogInfo("Start updating HR table...");
                                    FulDt = FulDs.Tables[0];

                                    //DT Batch Failing
                                    //SqlDataReader r = cmd.ExecuteReader();
                                    //while (r.Read())
                                    foreach (DataRow dr in FulDt.Rows)
                                    {

                                        try
                                        {                                            

                                            try
                                            {
                                                using (SqlConnection conn1 = new SqlConnection(conString.ConnectionString))
                                                {

                                                    using (SqlCommand cmd1 = new SqlCommand("XXHR_ADD_TRNG_Update", conn1))
                                                    {

                                                        cmd1.CommandType = CommandType.StoredProcedure;
                                                        cmd1.Parameters.Add(new SqlParameter("@PERSON_ID", dr["PERSON_ID"]));
                                                        cmd1.Parameters.Add(new SqlParameter("@FULL_NAME", dr["FULL_NAME"]));
                                                        cmd1.Parameters.Add(new SqlParameter("@EMPL_ID", dr["EMP_NBR"]));
                                                        cmd1.Parameters.Add(new SqlParameter("@COURSE_NAME", dr["COURSE_NAME"]));
                                                        cmd1.Parameters.Add(new SqlParameter("@TRAINING_TITLE", dr["TRAINING_TITLE"]));
                                                        cmd1.Parameters.Add(new SqlParameter("@COURSE_END_DATE", dr["COURSE_END_DATE"]));
                                                        cmd1.Parameters.Add(new SqlParameter("@EXPIRATION_DATE", dr["EXPIRATION_DATE"]));
                                                        cmd1.Parameters.Add(new SqlParameter("@DURATION", dr["DURATION"]));
                                                        cmd1.Parameters.Add(new SqlParameter("@DURATION_UNITS", dr["DURATION_UNITS"]));
                                                        cmd1.Parameters.Add(new SqlParameter("@PER_TYPE_ID", dr["PER_TYPE_ID"]));
                                                        cmd1.Parameters.Add(new SqlParameter("@RECORD_TYPE", dr["RECORD_TYPE"]));
                                                        //cmd1.Parameters.Add(new SqlParameter("@LAST_UPDATE_DATE", r["LAST_UPDATE_DATE"]));

                                                                                                                
                                                        conn1.Open();
                                                        cmd1.ExecuteNonQuery();
                                                        conn1.Close();
                                                    }

                                                }

                                            }


                                            catch (Exception ex)
                                            {
                                                //DT Batch Failing
                                                PollingdateUpdate = false;
                                                Log4NetService.LogInfo("Error occurred while updating XXHR_ADD_TRNG table for PERSON_ID" + dr["PERSON_ID"] + " " + ex.Message);
                                            }

                                        }
                                        catch (Exception ex)
                                        {
                                            //DT Batch Failing
                                            PollingdateUpdate = false;
                                            Log4NetService.LogInfo("Error In Row " + ex.Message);
                                        }
                                    }
                                    //DT 3127 Changes
                                    if (PollingdateUpdate == true)
                                    { Log4NetService.LogInfo("HR.XXHR_ADD_TRNG table is being read and updated."); }
                                    //DT Batch Failing
                                    Log4NetService.LogInfo("Finished updating HR table");
                                    //conn.Close();

                                    //Log4NetService.LogInfo("Finished reading from DB Link");
                                }
                            }


                        }
                        catch (Exception ex)
                        {
                            //DT Batch Failing
                            PollingdateUpdate = false;
                            Log4NetService.LogInfo("Error occurred while fetching information from HR table: " + ex.Message);
                            throw;
                        }
                        try
                        {
                            ConnectionStringSettings conString = ConfigurationManager.ConnectionStrings["Alcoa.HR.Data.ConnectionString"];
                            using (SqlConnection conn = new SqlConnection(conString.ConnectionString))
                            {
                                if (PollingdateUpdate == true)
                                {
                                    using (SqlCommand cmd = new SqlCommand("Update HR..POLLING_DATA SET Polling_Date=getDate() where Description='XXHR_ADD_TRNG'", conn))
                                    {
                                        Log4NetService.LogInfo("Updating HR POLLING_DATA Table...");
                                        
                                        conn.Open();
                                        cmd.ExecuteNonQuery();
                                        //DT 3127 Changes
                                        cmd.CommandText = "Select Convert(varchar(30),Polling_Date,120) from HR..POLLING_DATA WHERE Description='XXHR_ADD_TRNG'";
                                        object NewobjpollingDate = cmd.ExecuteScalar();                                        
                                        conn.Close();
                                        Log4NetService.LogInfo("New Polling datetime will be set as :" + Convert.ToString(NewobjpollingDate));                                        

                                        Log4NetService.LogInfo("Updated HR POLLING_DATA Table");
                                    }
                                }
                                else
                                {
                                    Log4NetService.LogInfo("HR POLLING_DATA Table Not Updated");
                                }
                            }


                        }
                        catch (Exception ex)
                        {
                            //DT Batch Failing : added ex.Message
                            Log4NetService.LogInfo("Error occurred while updating POLLING_DATA Table for XXHR_ADD_TRNG" + ex.Message);
                            throw;
                        }

                    //}

                //}
            }
            catch (Exception ex)
            {
                Log4NetService.LogInfo(ex.Message.ToString() + "\nServer Configuration Account Error. Contact Administrator.");
                throw;
            }
            finally
            {
                //if (impContext != null) impContext.Undo();

            }

        }

        public static void UpdateENRTraining()
        {
            // Cindi Thornton 17/9/15 - scheduled tasks now run as service account, this isn't needed. Remove all impersonation related code
            //WindowsImpersonationContext impContext = null;
            //try
            //{
            //    impContext = NetworkSecurity.Impersonation.ImpersonateUser(
            //                            System.Configuration.ConfigurationManager.AppSettings["WAOCSM_domain"].ToString(),
            //                            System.Configuration.ConfigurationManager.AppSettings["WAOCSM_user"].ToString(),
            //                            System.Configuration.ConfigurationManager.AppSettings["WAOCSM_pass"].ToString());
            //}
            //catch (ApplicationException ex)
            //{
            //    Log4NetService.LogInfo(ex.Message);
            //}

            //DT Batch Failing
            bool PollingdateUpdate = true;

            try
            {
                //if (null != impContext)
                //{
                //    using (impContext)
                //    {
                        string pollingdate = string.Empty;
                        //DateTime pollingdate = DateTime.Now;
                        try
                        {
                            ConnectionStringSettings conString = ConfigurationManager.ConnectionStrings["Alcoa.HR.Data.ConnectionString"];
                            using (SqlConnection conn = new SqlConnection(conString.ConnectionString))
                            {

                                using (SqlCommand cmd = new SqlCommand("Select Convert(varchar(30),Polling_Date,120) from HR..POLLING_DATA WHERE Description='XXHR_ENR_TRNG'", conn))
                                {
                                    //DT 3127 Changes
                                    Log4NetService.LogInfo("Fetching information from HR POLLING_DATA Table...");


                                    conn.Open();
                                    object objpollingDate = cmd.ExecuteScalar();
                                    pollingdate = Convert.ToString(objpollingDate);
                                    //pollingdate = Convert.ToDateTime(pollingdate.ToString("yyyy-MM-dd HH:mm:ss"));
                                    
                                    conn.Close();

                                    Log4NetService.LogInfo("Polling datetime for XXHR_ENR_TRNG=" + pollingdate);
                                }
                            }


                        }
                        catch (Exception ex)
                        {
                            //DT Batch Failing
                            PollingdateUpdate = false;
                            //DT 3127 Changes
                            Log4NetService.LogInfo("Error occurred while fetching information from HR POLLING_DATA table: " + ex.Message);
                            throw;
                        }
                        
                        //DT Batch Failing
                        DataSet FulDs = new DataSet();
                        DataTable FulDt = new DataTable();
                        try
                        {
                            //SP 1509 changes
                            ConnectionStringSettings conString = ConfigurationManager.ConnectionStrings["Alcoa.HR.Data.ConnectionString"];                            
                            using (SqlConnection conn = new SqlConnection(conString.ConnectionString))
                            {

                               
                                //using (SqlCommand cmd = new SqlCommand("(Select * from EBS_HR..APPS.XXHR_SQL_EMP_ARP_ENR_V WHERE LAST_UPDATE_DATE > Convert(datetime,'" + pollingdate + "',103)) UNION ALL (Select * from EBS_HR..APPS.XXHR_SQL_CWK_ENR_TRNG_V WHERE LAST_UPDATE_DATE > Convert(datetime,'" + pollingdate + "',103))", conn))

                                //SP 1509 changes
                                //using (SqlCommand cmd = new SqlCommand("(Select * from EBS_HR..APPS.XXHR_SQL_EMP_ARP_ENR_V WHERE LAST_UPDATE_DATE > CAST('" + pollingdate + "' AS DATETIME)) UNION ALL (Select * from EBS_HR..APPS.XXHR_SQL_CWK_ENR_TRNG_V WHERE LAST_UPDATE_DATE > CAST('" + pollingdate + "' AS DATETIME))", conn))                                
                                using (SqlCommand cmd = new SqlCommand("(Select * from EBS_HR..APPS.XXHR_GSI_INTF_CSMS_EMPARPENR_V WHERE LAST_UPDATE_DATE > CAST('" + pollingdate + "' AS DATETIME)) UNION ALL (Select * from EBS_HR..APPS.XXHR_GSI_INTF_CSMS_ENR_TRNG_V WHERE LAST_UPDATE_DATE > CAST('" + pollingdate + "' AS DATETIME))", conn))                                                                
                                {
                                    Log4NetService.LogInfo("Reading information from HR DB Link...");
                                    string cmdString = cmd.CommandText;
                                    conn.Open();
                                    //DT Batch Failing : Changed the CommandTimeout to 180000(3m)
                                    //cmd.CommandTimeout = 60000;
                                    cmd.CommandTimeout = 180000;
                                    //DT Batch Failing
                                    SqlDataAdapter sqlfulda = new SqlDataAdapter(cmd);
                                    sqlfulda.Fill(FulDs);
                                    conn.Close();
                                    Log4NetService.LogInfo("Finished reading from HR DB Link");
                                    Log4NetService.LogInfo("Start updating HR table...");
                                    FulDt = FulDs.Tables[0];

                                    //DT Batch Failing
                                    //SqlDataReader r = cmd.ExecuteReader();
                                    //while (r.Read())
                                    foreach (DataRow dr in FulDt.Rows)
                                    {

                                        try
                                        {
                                            

                                            try
                                            {
                                                using (SqlConnection conn1 = new SqlConnection(conString.ConnectionString))
                                                {

                                                    using (SqlCommand cmd1 = new SqlCommand("XXHR_ENR_TRNG_Update", conn1))
                                                    {

                                                        cmd1.CommandType = CommandType.StoredProcedure;
                                                        cmd1.Parameters.Add(new SqlParameter("@PERSON_ID", dr["PERSON_ID"]));
                                                        cmd1.Parameters.Add(new SqlParameter("@FULL_NAME", dr["FULL_NAME"]));
                                                        cmd1.Parameters.Add(new SqlParameter("@EMPL_ID", dr["EMP_NBR"]));
                                                        cmd1.Parameters.Add(new SqlParameter("@COURSE_NAME", dr["COURSE_NAME"]));
                                                        cmd1.Parameters.Add(new SqlParameter("@TRAINING_TITLE", dr["TRAINING_TITLE"]));
                                                        cmd1.Parameters.Add(new SqlParameter("@COURSE_END_DATE", dr["COURSE_END_DATE"]));
                                                        cmd1.Parameters.Add(new SqlParameter("@EXPIRATION_DATE", dr["EXPIRATION_DATE"]));
                                                        cmd1.Parameters.Add(new SqlParameter("@PER_TYPE_ID", dr["PER_TYPE_ID"]));
                                                        cmd1.Parameters.Add(new SqlParameter("@RECORD_TYPE", dr["RECORD_TYPE"]));
                                                        //cmd1.Parameters.Add(new SqlParameter("@LAST_UPDATE_DATE", r["LAST_UPDATE_DATE"]));
                                                        
                                                        conn1.Open();
                                                        cmd1.ExecuteNonQuery();
                                                        conn1.Close();
                                                    }

                                                }

                                            }


                                            catch (Exception ex)
                                            {
                                                //DT Batch Failing
                                                PollingdateUpdate = false;
                                                Log4NetService.LogInfo("Error occurred while updating HR XXHR_ENR_TRNG table for PERSON_ID" + dr["PERSON_ID"] + " " + ex.Message);
                                            }

                                        }
                                        catch (Exception ex)
                                        {
                                            //DT Batch Failing
                                            PollingdateUpdate = false;
                                            Log4NetService.LogInfo("Error In Row " + ex.Message);
                                        }
                                    }
                                    //DT 3127 Changes
                                    if (PollingdateUpdate == true)
                                    { Log4NetService.LogInfo("HR.XXHR_ENR_TRNG table is being read and updated."); }
                                    //DT Batch Failing
                                    Log4NetService.LogInfo("Finished updating HR table");
                                    //conn.Close();

                                    //Log4NetService.LogInfo("Finished reading from DB Link");
                                }
                            }


                        }
                        catch (Exception ex)
                        {
                            //DT Batch Failing
                            PollingdateUpdate = false;
                            //DT 3127 Changes
                            Log4NetService.LogInfo("Error occurred while fetching information from HR XXHR_ENR_TRNG table: " + ex.Message);
                            throw;
                        }
                        try
                        {
                            ConnectionStringSettings conString = ConfigurationManager.ConnectionStrings["Alcoa.HR.Data.ConnectionString"];
                            using (SqlConnection conn = new SqlConnection(conString.ConnectionString))
                            {
                                //DT Batch Failing
                                if (PollingdateUpdate == true)
                                {
                                    using (SqlCommand cmd = new SqlCommand("Update HR..POLLING_DATA SET Polling_Date=getDate() where Description='XXHR_ENR_TRNG'", conn))
                                    {
                                        Log4NetService.LogInfo("Updating HR POLLING_DATA Table...");


                                        conn.Open();
                                        cmd.ExecuteNonQuery();
                                        //DT 3127 Changes
                                        cmd.CommandText = "Select Convert(varchar(30),Polling_Date,120) from HR..POLLING_DATA WHERE Description='XXHR_ENR_TRNG'";
                                        object NewENRpollingDate = cmd.ExecuteScalar();                                        
                                        conn.Close();
                                        Log4NetService.LogInfo("New Polling datetime will be set as :" + Convert.ToString(NewENRpollingDate));

                                        Log4NetService.LogInfo("Updated HR POLLING_DATA Table");
                                    }
                                }
                                else
                                {
                                    Log4NetService.LogInfo("HR POLLING_DATA Table Not Updated");
                                }
                            }


                        }
                        catch (Exception ex)
                        {
                            //DT Batch Failing : added the ex.Message                            
                            Log4NetService.LogInfo("Error occurred while updating POLLING_DATA Table for XXHR_ENR_TRNG" + ex.Message);
                            throw;
                        }

                    //}

                //}
            }
            catch (Exception ex)
            {
                Log4NetService.LogInfo(ex.Message.ToString() + "\nServer Configuration Account Error. Contact Administrator.");
                throw;
            }
            finally
            {
                //if (impContext != null) impContext.Undo();

            }

        }

        public static void UpdatePeopleDetails()
        {
            // Cindi Thornton 17/9/15 - scheduled tasks now run as service account, this isn't needed. Remove all impersonation related code
            //WindowsImpersonationContext impContext = null;
            //try
            //{
            //    impContext = NetworkSecurity.Impersonation.ImpersonateUser(
            //                            System.Configuration.ConfigurationManager.AppSettings["WAOCSM_domain"].ToString(),
            //                            System.Configuration.ConfigurationManager.AppSettings["WAOCSM_user"].ToString(),
            //                            System.Configuration.ConfigurationManager.AppSettings["WAOCSM_pass"].ToString());
            //}
            //catch (ApplicationException ex)
            //{
            //    Log4NetService.LogInfo(ex.Message);
            //}

            //DT Batch Failing
            bool PollingdateUpdate = true;

            try
            {
                //if (null != impContext)
                //{
                //    using (impContext)
                //    {
                        string pollingdate = string.Empty;
                        //DateTime pollingdate = DateTime.Now;
                        try
                        {
                            ConnectionStringSettings conString = ConfigurationManager.ConnectionStrings["Alcoa.HR.Data.ConnectionString"];
                            using (SqlConnection conn = new SqlConnection(conString.ConnectionString))
                            {

                                using (SqlCommand cmd = new SqlCommand("Select Convert(varchar(30),Polling_Date,120) from HR..POLLING_DATA WHERE Description='XXHR_PEOPLE_DETAILS'", conn))
                                {
                                    //DT 3127 Changes
                                    Log4NetService.LogInfo("Fetching information from HR POLLING_DATA Table...");


                                    conn.Open();
                                    object objpollingDate = cmd.ExecuteScalar();
                                    pollingdate = Convert.ToString(objpollingDate);
                                    conn.Close();

                                    Log4NetService.LogInfo("Polling datetime for XXHR_PEOPLE_DETAILS=" + pollingdate);
                                }
                            }


                        }
                        catch (Exception ex)
                        {
                            //DT Batch Failing
                            PollingdateUpdate = false;
                            //DT 3127 Changes
                            Log4NetService.LogInfo("Error occurred while fetching information from HR POLLING_DATA table: " + ex.Message);
                            throw;
                        }

                        //DT Batch Failing
                        DataSet FulDs = new DataSet();
                        DataTable FulDt = new DataTable();
                        try
                        {
                            ConnectionStringSettings conString = ConfigurationManager.ConnectionStrings["Alcoa.HR.Data.ConnectionString"];
                            using (SqlConnection conn = new SqlConnection(conString.ConnectionString))
                            {
                                //using (SqlCommand cmd = new SqlCommand("Select * from EBS_HR..APPS.XXHR_SQL_PEOPLE_V WHERE LAST_UPDATE_DATE_PER > Convert(datetime,'" + pollingdate + "',103) or LAST_UPDATE_DATE_ASS> Convert(datetime,'" + pollingdate + "',103)", conn))
                                //SP 1509 changes                                
                                //using (SqlCommand cmd = new SqlCommand("Select * from EBS_HR..APPS.XXHR_SQL_PEOPLE_V WHERE LAST_UPDATE_DATE_PER > CAST('" + pollingdate + "' AS DATETIME) or LAST_UPDATE_DATE_ASS> CAST('" + pollingdate + "' AS DATETIME)", conn))
                                using (SqlCommand cmd = new SqlCommand("Select * from EBS_HR..APPS.XXHR_GSI_INTF_CSMS_SQL_PPL_V WHERE LAST_UPDATE_DATE_PER > CAST('" + pollingdate + "' AS DATETIME) or LAST_UPDATE_DATE_ASS> CAST('" + pollingdate + "' AS DATETIME)", conn))
                                {
                                    Log4NetService.LogInfo("Reading information from HR DB Link...");
                                    string cmdString = cmd.CommandText;
                                    conn.Open();
                                    cmd.CommandTimeout = 60000;

                                    //DT Batch Failing
                                    SqlDataAdapter sqlfulda = new SqlDataAdapter(cmd);
                                    sqlfulda.Fill(FulDs);
                                    conn.Close();
                                    Log4NetService.LogInfo("Finished reading from HR DB Link");
                                    Log4NetService.LogInfo("Start updating HR table...");
                                    FulDt = FulDs.Tables[0];

                                    //DT Batch Failing
                                    //SqlDataReader r = cmd.ExecuteReader();
                                    //while (r.Read())
                                    foreach (DataRow dr in FulDt.Rows)
                                    {

                                        try
                                        {
                                           
                                            try
                                            {
                                                using (SqlConnection conn1 = new SqlConnection(conString.ConnectionString))
                                                {

                                                    using (SqlCommand cmd1 = new SqlCommand("XXHR_SQL_PEOPLE_Update", conn1))
                                                    {

                                                        cmd1.CommandType = CommandType.StoredProcedure;
                                                        cmd1.Parameters.Add(new SqlParameter("@PERSON_ID", dr["PERSON_ID"]));
                                                        cmd1.Parameters.Add(new SqlParameter("@EMPLID", dr["EMPLOYEE_NUMBER"]));
                                                        cmd1.Parameters.Add(new SqlParameter("@PER_TYPE_ID", dr["PER_TYPE_ID"]));
                                                        cmd1.Parameters.Add(new SqlParameter("@FULL_NAME", dr["FULL_NAME"]));
                                                        cmd1.Parameters.Add(new SqlParameter("@SGNAME", dr["MIDDLE_NAMES"]));
                                                        cmd1.Parameters.Add(new SqlParameter("@LAST_NAME_SRCH", dr["LAST_NAME"]));
                                                        cmd1.Parameters.Add(new SqlParameter("@FIRST_NAME_SRCH", dr["FIRST_NAME"]));
                                                        cmd1.Parameters.Add(new SqlParameter("@SUPERVISOR_ID", dr["SUPERVISOR_ID"]));
                                                        cmd1.Parameters.Add(new SqlParameter("@POS_DESCR", dr["POS_DESCR"]));
                                                        cmd1.Parameters.Add(new SqlParameter("@PAYGROUP", dr["PAYGROUP"]));
                                                        cmd1.Parameters.Add(new SqlParameter("@LOCATION", dr["LOCATION_CODE"]));
                                                        cmd1.Parameters.Add(new SqlParameter("@COMPANY", dr["COMPANY"]));
                                                        cmd1.Parameters.Add(new SqlParameter("@SEX", dr["GENDER"]));
                                                        cmd1.Parameters.Add(new SqlParameter("@LBC", dr["LBC"]));
                                                        cmd1.Parameters.Add(new SqlParameter("@DEPT_CD", dr["DEPT"]));//Doubt
                                                        cmd1.Parameters.Add(new SqlParameter("@ACCOUNT", dr["ACCOUNT"]));
                                                        cmd1.Parameters.Add(new SqlParameter("@SERVICE_DT", dr["SERVICE_DATE"]));
                                                        cmd1.Parameters.Add(new SqlParameter("@TERMINATION_DT", dr["TERMINATION_DATE"]));
                                                        cmd1.Parameters.Add(new SqlParameter("@AOA_PREF_NAME", dr["AOA_PREF_NAME"]));
                                                        cmd1.Parameters.Add(new SqlParameter("@LOGINID", dr["LOGINID"]));
                                                        cmd1.Parameters.Add(new SqlParameter("@ORG_CODE", dr["ORG_CODE"]));
                                                        cmd1.Parameters.Add(new SqlParameter("@DEPT_ORG_CODE", dr["DEPT_ORG_CODE"]));
                                                        cmd1.Parameters.Add(new SqlParameter("@OP_CENTRE", dr["OP_CENTRE"]));
                                                                                                                
                                                        conn1.Open();
                                                        cmd1.ExecuteNonQuery();
                                                        conn1.Close();
                                                    }

                                                }

                                            }


                                            catch (Exception ex)
                                            {
                                                //DT Batch Failing
                                                PollingdateUpdate = false;
                                                Log4NetService.LogInfo("Error occurred while updating XXHR_PEOPLE_DETAILS table for PERSON_ID" + dr["PERSON_ID"] + " " + ex.Message);
                                            }

                                        }
                                        catch (Exception ex)
                                        {
                                            //DT Batch Failing
                                            PollingdateUpdate = false;
                                            Log4NetService.LogInfo("Error In Row " + ex.Message);
                                        }
                                    }

                                    //DT 3127 Changes
                                    if (PollingdateUpdate == true)
                                    { Log4NetService.LogInfo("HR.XXHR_PEOPLE_DETAILS table is being read and updated."); }
                                    //DT Batch Failing
                                    Log4NetService.LogInfo("Finished updating HR table");
                                    //conn.Close();

                                    //Log4NetService.LogInfo("Finished reading from DB Link");
                                }
                            }


                        }
                        catch (Exception ex)
                        {
                            //DT Batch Failing
                            PollingdateUpdate = false;
                            //DT 3127 Changes
                            Log4NetService.LogInfo("Error occurred while fetching information : " + ex.Message);
                            throw;
                        }

                        // Begin part 2

                        //DT Batch Failing
                        DataSet PartDs = new DataSet();
                        DataTable PartDt = new DataTable();

                        try
                        {
                            ConnectionStringSettings conString = ConfigurationManager.ConnectionStrings["Alcoa.HR.Data.ConnectionString"];
                            using (SqlConnection conn = new SqlConnection(conString.ConnectionString))
                            {
                                //using (SqlCommand cmd = new SqlCommand("Select * from EBS_HR..APPS.XXHR_SQL_CWK_V WHERE (LAST_UPDATE_DATE_PER > Convert(datetime,'" + pollingdate + "',103) or LAST_UPDATE_DATE_ASS> Convert(datetime,'" + pollingdate + "',103)) AND GETDATE() between EFFECTIVE_START_DATE and EFFECTIVE_END_DATE", conn))
                                //SP 1509 changes                                
                                //using (SqlCommand cmd = new SqlCommand("Select * from EBS_HR..APPS.XXHR_SQL_CWK_V WHERE (LAST_UPDATE_DATE_PER > CAST('" + pollingdate + "' AS DATETIME) or LAST_UPDATE_DATE_ASS> CAST('" + pollingdate + "' AS DATETIME)) AND GETDATE() between EFFECTIVE_START_DATE and EFFECTIVE_END_DATE", conn))
                                using (SqlCommand cmd = new SqlCommand("Select * from EBS_HR..APPS.XXHR_GSI_INTF_HR_TO_CSMS_CWK_V WHERE (LAST_UPDATE_DATE_PER > CAST('" + pollingdate + "' AS DATETIME) or LAST_UPDATE_DATE_ASS> CAST('" + pollingdate + "' AS DATETIME)) AND GETDATE() between EFFECTIVE_START_DATE and EFFECTIVE_END_DATE", conn))
                                {
                                    Log4NetService.LogInfo("Reading information from HR DB Link...");
                                    string cmdString = cmd.CommandText;
                                    conn.Open();
                                    cmd.CommandTimeout = 60000;

                                    //DT Batch Failing
                                    SqlDataAdapter sqlpartda = new SqlDataAdapter(cmd);
                                    sqlpartda.Fill(PartDs);
                                    conn.Close();
                                    Log4NetService.LogInfo("Finished reading from HR DB Link");
                                    Log4NetService.LogInfo("Start updating HR table...");
                                    PartDt = PartDs.Tables[0];

                                    //DT Batch Failing
                                    //SqlDataReader r = cmd.ExecuteReader();
                                    //while (r.Read())
                                    foreach (DataRow dr in PartDt.Rows)
                                    {

                                        try
                                        {
                                            

                                            try
                                            {
                                                using (SqlConnection conn1 = new SqlConnection(conString.ConnectionString))
                                                {

                                                    using (SqlCommand cmd1 = new SqlCommand("XXHR_SQL_CWK_Update", conn1))
                                                    {

                                                        cmd1.CommandType = CommandType.StoredProcedure;
                                                        cmd1.Parameters.Add(new SqlParameter("@PERSON_ID", dr["PERSON_ID"]));
                                                        cmd1.Parameters.Add(new SqlParameter("@EMPLID", dr["CWK_NBR"]));
                                                        cmd1.Parameters.Add(new SqlParameter("@PER_TYPE_ID", dr["PER_TYPE_ID"]));
                                                        cmd1.Parameters.Add(new SqlParameter("@FULL_NAME", dr["FULL_NAME"]));
                                                        cmd1.Parameters.Add(new SqlParameter("@SGNAME", dr["MIDDLE_NAMES"]));
                                                        cmd1.Parameters.Add(new SqlParameter("@LAST_NAME_SRCH", dr["LAST_NAME"]));
                                                        cmd1.Parameters.Add(new SqlParameter("@FIRST_NAME_SRCH", dr["FIRST_NAME"]));
                                                        cmd1.Parameters.Add(new SqlParameter("@SUPERVISOR_ID", dr["SUPERVISOR_ID"]));
                                                        cmd1.Parameters.Add(new SqlParameter("@POS_DESCR", dr["POS_DESCR"]));
                                                        //cmd1.Parameters.Add(new SqlParameter("@PAYGROUP", r["PAYGROUP"]));
                                                        cmd1.Parameters.Add(new SqlParameter("@LOCATION", dr["LOCATION_CODE"]));
                                                        //cmd1.Parameters.Add(new SqlParameter("@COMPANY", r["COMPANY"]));
                                                        cmd1.Parameters.Add(new SqlParameter("@SEX", dr["GENDER"]));
                                                        cmd1.Parameters.Add(new SqlParameter("@LBC", dr["LBC"]));
                                                        cmd1.Parameters.Add(new SqlParameter("@DEPT_CD", dr["DEPT"]));
                                                        cmd1.Parameters.Add(new SqlParameter("@ACCOUNT", dr["ACCOUNT"]));
                                                        cmd1.Parameters.Add(new SqlParameter("@SERVICE_DT", dr["SERVICE_DATE"]));
                                                        cmd1.Parameters.Add(new SqlParameter("@TERMINATION_DT", dr["TERMINATION_DATE"]));
                                                        cmd1.Parameters.Add(new SqlParameter("@AOA_PREF_NAME", dr["AOA_PREF_NAME"]));
                                                        cmd1.Parameters.Add(new SqlParameter("@LOGINID", dr["LOGINID"]));
                                                        cmd1.Parameters.Add(new SqlParameter("@ORG_CODE", dr["ORG_CODE"]));
                                                        cmd1.Parameters.Add(new SqlParameter("@DEPT_ORG_CODE", dr["DEPT_ORG_CODE"]));
                                                        cmd1.Parameters.Add(new SqlParameter("@OP_CENTRE", dr["OP_CENTRE"]));
                                                        cmd1.Parameters.Add(new SqlParameter("@CWK_VENDOR_NO", dr["CWK_VENDOR_NO"]));
                                                        cmd1.Parameters.Add(new SqlParameter("@CWK_VENDOR_NAME", dr["CWK_VENDOR_NAME"]));
                                                        cmd1.Parameters.Add(new SqlParameter("@CWK_NON_PREFERRED_SUPP_NAME", dr["CWK_NON_PREFERRED_SUPP_NAME"]));
                                                        cmd1.Parameters.Add(new SqlParameter("@CWK_SPONSOR_ID", dr["CWK_SPONSOR_ID"]));
                                                        cmd1.Parameters.Add(new SqlParameter("@CWK_SUPP_SUPERVISOR_ID", dr["CWK_SUPP_SUPERVISOR_ID"]));
                                                        cmd1.Parameters.Add(new SqlParameter("@CWL_PRIVILEGE_FLAG", dr["CWK_PRIVILEGE_FLAG"]));


                                                        conn1.Open();
                                                        cmd1.ExecuteNonQuery();
                                                        conn1.Close();
                                                    }

                                                }

                                            }


                                            catch (Exception ex)
                                            {
                                                //DT Batch Failing
                                                PollingdateUpdate = false;
                                                Log4NetService.LogInfo("Error occurred while updating XXHR_PEOPLE_DETAILS table for PERSON_ID" + dr["PERSON_ID"] + " " + ex.Message);
                                            }

                                        }
                                        catch (Exception ex)
                                        {
                                            //DT Batch Failing
                                            PollingdateUpdate = false;
                                            Log4NetService.LogInfo("Error In Row " + ex.Message);
                                        }
                                    }
                                    //DT 3127 Changes
                                    if (PollingdateUpdate == true)
                                    { Log4NetService.LogInfo("HR.XXHR_PEOPLE_DETAILS table is being read and updated."); }
                                    //DT Batch Failing
                                    Log4NetService.LogInfo("Finished updating HR table");
                                    //conn.Close();

                                    //Log4NetService.LogInfo("Finished reading from DB Link");
                                }
                            }


                        }
                        catch (Exception ex)
                        {
                            //DT Batch Failing
                            PollingdateUpdate = false;
                            //DT 3127 Changes
                            Log4NetService.LogInfo("Error occurred while fetching information : " + ex.Message);
                            throw;
                        }

                        // End of part 2
                        try
                        {
                            ConnectionStringSettings conString = ConfigurationManager.ConnectionStrings["Alcoa.HR.Data.ConnectionString"];
                            using (SqlConnection conn = new SqlConnection(conString.ConnectionString))
                            {
                                //DT Batch Failing
                                if (PollingdateUpdate == true)
                                {
                                    using (SqlCommand cmd = new SqlCommand("Update HR..POLLING_DATA SET Polling_Date=getDate() where Description='XXHR_PEOPLE_DETAILS'", conn))
                                    {
                                        Log4NetService.LogInfo("Updating HR POLLING_DATA Table...");


                                        conn.Open();
                                        cmd.ExecuteNonQuery();
                                        //DT 3127 Changes
                                        cmd.CommandText = "Select Convert(varchar(30),Polling_Date,120) from HR..POLLING_DATA WHERE Description='XXHR_PEOPLE_DETAILS'";
                                        object NewPplpollingDate = cmd.ExecuteScalar();
                                        conn.Close();
                                        Log4NetService.LogInfo("New Polling datetime will be set as :" + Convert.ToString(NewPplpollingDate));
                                        
                                        Log4NetService.LogInfo("Updated POLLING_DATA Table");
                                    }
                                }
                                else
                                {
                                    Log4NetService.LogInfo("HR POLLING_DATA Table Not Updated");
                                }
                            }


                        }
                        catch (Exception ex)
                        {
                            //DT Batch Failing : added ex.Message
                            Log4NetService.LogInfo("Error occurred while updating POLLING_DATA Table for XXHR_PEOPLE_DETAILS" + ex.Message);
                            throw;
                        }

                    //}

                //}
            }
            catch (Exception ex)
            {
                Log4NetService.LogInfo(ex.Message.ToString() + "\nServer Configuration Account Error. Contact Administrator.");
                throw;
            }
            finally
            {
                //if (impContext != null) impContext.Undo();

            }

        }
    }
}
