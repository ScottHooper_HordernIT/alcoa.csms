﻿<%@ Control Language="C#" AutoEventWireup="true"
    Inherits="Adminv2_UserControls_AnnualTargets" Codebehind="AnnualTargets.ascx.cs" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
                    <dxwgv:ASPxGridView ID="gridAnnualTargets" runat="server" AutoGenerateColumns="False"
                        CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue"
                        DataSourceID="AnnualTargetsDataSource" KeyFieldName="AnnualTargetsId" Width="880px">
                        <Columns>
                            <dxwgv:GridViewCommandColumn Caption="Action" FixedStyle="Left" VisibleIndex="0"
                                Width="80px">
                                <EditButton Visible="True">
                                </EditButton>
                                <NewButton Visible="True">
                                </NewButton>
                                <ClearFilterButton Visible="True">
                                </ClearFilterButton>
                            </dxwgv:GridViewCommandColumn>
                            <dxwgv:GridViewDataTextColumn FieldName="AnnualTargetsId" ReadOnly="True" Visible="False" VisibleIndex="0">
                            </dxwgv:GridViewDataTextColumn>
                            <dxwgv:GridViewDataComboBoxColumn Caption="Residential Category" FieldName="CompanySiteCategoryId"
                                VisibleIndex="0" FixedStyle="Left">
                                <PropertiesComboBox DataSourceID="CompanySiteCategoryDataSource" TextField="CategoryDesc"
                                    ValueField="CompanySiteCategoryId" ValueType="System.Int32">
                                </PropertiesComboBox>
                            </dxwgv:GridViewDataComboBoxColumn>
                            <dxwgv:GridViewDataTextColumn FieldName="Year" FixedStyle="Left" VisibleIndex="1"
                                Width="50px">
                            </dxwgv:GridViewDataTextColumn>
                            <dxwgv:GridViewDataTextColumn FieldName="EhsAudits" VisibleIndex="2" Width="50px"
                                Settings-AllowHeaderFilter="False">
                            </dxwgv:GridViewDataTextColumn>
                            <dxwgv:GridViewDataTextColumn FieldName="WorkplaceSafetyCompliance" VisibleIndex="3"
                                Settings-AllowHeaderFilter="False">
                            </dxwgv:GridViewDataTextColumn>
                            <dxwgv:GridViewDataTextColumn Caption="Management Health &amp; Safety Contacts" FieldName="HealthSafetyWorkContacts"
                                VisibleIndex="4" Settings-AllowHeaderFilter="False">
                            </dxwgv:GridViewDataTextColumn>
                            <dxwgv:GridViewDataTextColumn FieldName="BehaviouralSafetyProgram" VisibleIndex="5"
                                Settings-AllowHeaderFilter="False">
                            </dxwgv:GridViewDataTextColumn>
                            <dxwgv:GridViewDataTextColumn FieldName="QuarterlyAuditScore" VisibleIndex="6" Settings-AllowHeaderFilter="False">
                            </dxwgv:GridViewDataTextColumn>
                            <dxwgv:GridViewDataTextColumn Caption="Tool box meetings" FieldName="ToolboxMeetingsPerMonth"
                                VisibleIndex="7" Settings-AllowHeaderFilter="False">
                            </dxwgv:GridViewDataTextColumn>
                            <dxwgv:GridViewDataTextColumn FieldName="AlcoaWeeklyContractorsMeeting" VisibleIndex="8"
                                Settings-AllowHeaderFilter="False">
                            </dxwgv:GridViewDataTextColumn>
                            <dxwgv:GridViewDataTextColumn FieldName="AlcoaMonthlyContractorsMeeting" VisibleIndex="9"
                                Settings-AllowHeaderFilter="False">
                            </dxwgv:GridViewDataTextColumn>
                            <dxwgv:GridViewDataTextColumn FieldName="SafetyPlan" VisibleIndex="10" Settings-AllowHeaderFilter="False">
                            </dxwgv:GridViewDataTextColumn>
                            <dxwgv:GridViewDataTextColumn Caption="JSA Audits" FieldName="JsaScore" VisibleIndex="11"
                                Settings-AllowHeaderFilter="False">
                            </dxwgv:GridViewDataTextColumn>
                            <dxwgv:GridViewDataTextColumn FieldName="FatalityPrevention" VisibleIndex="12" Settings-AllowHeaderFilter="False">
                            </dxwgv:GridViewDataTextColumn>
                            <dxwgv:GridViewDataTextColumn Caption="AIFR" FieldName="Aifr" VisibleIndex="13" Settings-AllowHeaderFilter="False">
                            </dxwgv:GridViewDataTextColumn>
                            <dxwgv:GridViewDataTextColumn Caption="TRIFR" FieldName="Trifr" VisibleIndex="14"
                                Settings-AllowHeaderFilter="False">
                            </dxwgv:GridViewDataTextColumn>
                            <dxwgv:GridViewDataTextColumn Caption="Training" FieldName="Training" VisibleIndex="15"
                                Settings-AllowHeaderFilter="False">
                            </dxwgv:GridViewDataTextColumn>

                            <%--Added By SAyani For Task# 1--%>
                            <dxwgv:GridViewDataTextColumn Caption="IFE: Injury Ratio Target (YTD)" FieldName="IFEInjuryRatioTargetYTD" VisibleIndex="16"
                                Settings-AllowHeaderFilter="False">
                            </dxwgv:GridViewDataTextColumn>
                            <dxwgv:GridViewDataTextColumn Caption="IFE: Injury Ratio Target (Month)" FieldName="IFEInjuryRatioTargetMon" VisibleIndex="17"
                                Settings-AllowHeaderFilter="False">
                            </dxwgv:GridViewDataTextColumn>
                             <dxwgv:GridViewDataTextColumn Caption="RN: Injury Target (YTD)" FieldName="RNInjuryTargetYTD" VisibleIndex="18"
                                Settings-AllowHeaderFilter="False">
                            </dxwgv:GridViewDataTextColumn>
                             <dxwgv:GridViewDataTextColumn Caption="RN: Injury Ratio Target (Month)" FieldName="RNInjuryRatioTargetMon" VisibleIndex="19"
                                Settings-AllowHeaderFilter="False">
                            </dxwgv:GridViewDataTextColumn>

                            <%--End--%>

                        </Columns>
                        <SettingsBehavior ColumnResizeMode="NextColumn" ConfirmDelete="True" />
                        <SettingsEditing Mode="Inline" />
                        <Settings ShowHorizontalScrollBar="True" ShowHeaderFilterButton="true" />
                        <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                            <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
                            </LoadingPanelOnStatusBar>
                            <CollapsedButton Height="12px" Width="11px">
                            </CollapsedButton>
                            <ExpandedButton Height="12px" Width="11px">
                            </ExpandedButton>
                            <DetailCollapsedButton Height="12px" Width="11px">
                            </DetailCollapsedButton>
                            <DetailExpandedButton Height="12px" Width="11px">
                            </DetailExpandedButton>
                            <LoadingPanel Url="~/App_Themes/Office2003BlueOld/GridView/Loading.gif"><%--Enhancement_023:change by debashis for testing platform upgradation--%>
                            </LoadingPanel>
                        </Images>
                        <ImagesFilterControl>
                            <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
                            </LoadingPanel>
                        </ImagesFilterControl>
                        <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue">
                            <Header HorizontalAlign="Center" ImageSpacing="5px" SortingImageSpacing="5px" Wrap="True">
                            </Header>
                            <Cell HorizontalAlign="Center">
                            </Cell>
                            <LoadingPanel ImageSpacing="10px">
                            </LoadingPanel>
                        </Styles>
                    </dxwgv:ASPxGridView>
               
<data:AnnualTargetsDataSource ID="AnnualTargetsDataSource" runat="server" SelectMethod="GetPaged"
    EnablePaging="True" EnableSorting="True">
</data:AnnualTargetsDataSource>
<data:CompanySiteCategoryDataSource ID="CompanySiteCategoryDataSource" runat="server"
    SelectMethod="GetPaged" EnablePaging="True" EnableSorting="True">
</data:CompanySiteCategoryDataSource>
