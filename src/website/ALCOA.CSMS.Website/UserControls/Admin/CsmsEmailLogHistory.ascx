﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CsmsEmailLogHistory.ascx.cs"
    Inherits="ALCOA.CSMS.Website.UserControls.Admin.CsmsEmailLogHistory" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<dx:ASPxPageControl ID="ASPxPageControl1" runat="server" ActiveTabIndex="0" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
    CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
    Width="900px">
    <TabPages>
        <dx:TabPage Text="Admin Task Emails">
            <ContentCollection>
                <dx:ContentControl runat="server" SupportsDisabledAttribute="True">
                    <table style="width: 870px" cellspacing="0" cellpadding="0" border="0">
                        <tbody>
                            <tr>
                                <td style="width: 870px; padding-top: 2px; text-align: center" class="pageName" align="right">
                                    <div align="right">
                                        <a style="color: #0000ff; text-align: right; text-decoration: none" href="javascript:ShowHideCustomizationWindow()">
                                            Customize</a>
                                    </div>
                                </td>
                            </tr>
                            <tr style="padding-top: 2px">
                                <td style="height: 13px; text-align: left">
                                    <dx:ASPxGridView ID="grid" ClientInstanceName="grid" runat="server" AutoGenerateColumns="False"
                                        CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue"
                                        DataSourceID="CsmsEmailLogListDataSource1" Width="870px">
                                        <SettingsCustomizationWindow Enabled="True"></SettingsCustomizationWindow>
                                        <Columns>
                                            <dx:GridViewDataTextColumn FieldName="CsmsEmailLogId" ShowInCustomizationForm="True"
                                                Visible="False" VisibleIndex="1">
                                            </dx:GridViewDataTextColumn>
                                            <%--<dx:GridViewDataTextColumn Caption="Sent By" FieldName="SentByUserFullName" ShowInCustomizationForm="True"
                                                VisibleIndex="3">
                                                <Settings AutoFilterCondition="BeginsWith" />
                                            </dx:GridViewDataTextColumn>--%>
                                            <dx:GridViewDataComboBoxColumn FieldName="SentByUserId" VisibleIndex="3"
                                                Caption="Sent By" ShowInCustomizationForm="True">
                                                <PropertiesComboBox DataSourceID="UsersSentByDS" TextField="UserFullName"
                                                    ValueField="SentByUserId" ValueType="System.Int32" IncrementalFilteringMode="StartsWith">
                                                </PropertiesComboBox>
                                                 <%--<Settings SortMode="DisplayText"/>--%>
                                            </dx:GridViewDataComboBoxColumn>
                                            
                                            <dx:GridViewDataDateColumn Caption="Sent At" FieldName="EmailSentDateTime" ShowInCustomizationForm="True"
                                                SortIndex="0" SortOrder="Descending" VisibleIndex="2">
                                                <PropertiesDateEdit EditFormat="DateTime">
                                                </PropertiesDateEdit>
                                            </dx:GridViewDataDateColumn>
                                            <dx:GridViewDataTextColumn FieldName="Subject" ShowInCustomizationForm="True" VisibleIndex="4">
                                                <Settings AutoFilterCondition="Contains" />
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn Caption="To" FieldName="EmailTo" ShowInCustomizationForm="True"
                                                VisibleIndex="5">
                                                <Settings AutoFilterCondition="Contains" />
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn Caption="CC" FieldName="EmailCc" ShowInCustomizationForm="True"
                                                Visible="false">
                                                <Settings AutoFilterCondition="Contains" />
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn Caption="Bcc" FieldName="EmailBcc" ShowInCustomizationForm="True"
                                                Visible="false">
                                                <Settings AutoFilterCondition="Contains" />
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn FieldName="AdminTaskId" ShowInCustomizationForm="True"
                                                Visible="False" VisibleIndex="8">
                                            </dx:GridViewDataTextColumn>
                                           <%-- <dx:GridViewDataTextColumn Caption="Source" FieldName="AdminTaskSourceDesc" ShowInCustomizationForm="True"
                                                Visible="False" VisibleIndex="9">
                                            </dx:GridViewDataTextColumn>--%>
                                            <dx:GridViewDataComboBoxColumn FieldName="AdminTaskSourceDesc" VisibleIndex="9"
                                                 ShowInCustomizationForm="True" Visible="False" Caption="Source">
                                                <PropertiesComboBox DataSourceID="SourceDS" TextField="AdminTaskSourceDesc"
                                                    IncrementalFilteringMode="StartsWith">
                                                </PropertiesComboBox>
                                                 <Settings SortMode="DisplayText"/>
                                            </dx:GridViewDataComboBoxColumn>
                                            <%--<dx:GridViewDataTextColumn Caption="Task Type" FieldName="AdminTaskTypeName" ShowInCustomizationForm="True"
                                                Visible="False" VisibleIndex="10">
                                            </dx:GridViewDataTextColumn>--%>
                                            <dx:GridViewDataComboBoxColumn FieldName="AdminTaskTypeName" VisibleIndex="10"
                                                 ShowInCustomizationForm="True" Visible="False" Caption="Task Type">
                                                <PropertiesComboBox DataSourceID="AdminTaskTypeDataSource1" TextField="AdminTaskTypeName"
                                                    IncrementalFilteringMode="StartsWith">
                                                </PropertiesComboBox>
                                                 <Settings SortMode="DisplayText"/>
                                            </dx:GridViewDataComboBoxColumn>
                                            <%--<dx:GridViewDataTextColumn FieldName="StatusDesc" ShowInCustomizationForm="True"
                                                Visible="False" VisibleIndex="11">
                                            </dx:GridViewDataTextColumn>--%>
                                            <dx:GridViewDataComboBoxColumn FieldName="StatusDesc" VisibleIndex="11"
                                                 ShowInCustomizationForm="True" Visible="False">
                                                <PropertiesComboBox DataSourceID="AdminTaskStatusDataSource1" TextField="StatusDesc"
                                                    IncrementalFilteringMode="StartsWith">
                                                </PropertiesComboBox>
                                                 <Settings SortMode="DisplayText"/>
                                            </dx:GridViewDataComboBoxColumn>
                                            <dx:GridViewDataTextColumn FieldName="AdminTaskComments" ShowInCustomizationForm="True"
                                                Visible="False" VisibleIndex="12">
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataDateColumn FieldName="DateOpened" ShowInCustomizationForm="True"
                                                Visible="False" VisibleIndex="13">
                                            </dx:GridViewDataDateColumn>
                                            <dx:GridViewDataDateColumn FieldName="DateClosed" ShowInCustomizationForm="True"
                                                Visible="False" VisibleIndex="14">
                                            </dx:GridViewDataDateColumn>
                                            <%--<dx:GridViewDataTextColumn FieldName="OpenedByUser" ShowInCustomizationForm="True"
                                                Visible="False" VisibleIndex="15">
                                            </dx:GridViewDataTextColumn>--%>
                                            <dx:GridViewDataComboBoxColumn FieldName="OpenedByUser" VisibleIndex="15"
                                                 ShowInCustomizationForm="True" Visible="False">
                                                <PropertiesComboBox DataSourceID="OpenedByUserDS" TextField="OpenedByUser"
                                                    IncrementalFilteringMode="StartsWith">
                                                </PropertiesComboBox>
                                                 <Settings SortMode="DisplayText"/>
                                            </dx:GridViewDataComboBoxColumn>
                                               <dx:GridViewDataComboBoxColumn FieldName="ClosedByUser" VisibleIndex="16"
                                                 ShowInCustomizationForm="True" Visible="False">
                                                <PropertiesComboBox DataSourceID="ClosedByUserDS" TextField="ClosedByUser"
                                                    IncrementalFilteringMode="StartsWith">
                                                </PropertiesComboBox>
                                                <Settings SortMode="DisplayText"></Settings>
                                                 </dx:GridViewDataComboBoxColumn>
                                            <dx:GridViewDataTextColumn FieldName="DaysWith" ShowInCustomizationForm="True" Visible="False"
                                                VisibleIndex="17">
                                            </dx:GridViewDataTextColumn>
                                            
                                            <dx:GridViewDataComboBoxColumn FieldName="AccessDesc" VisibleIndex="18"
                                                 ShowInCustomizationForm="True" Visible="False">
                                                <PropertiesComboBox DataSourceID="CsmsAccessDataSource1" TextField="AccessDesc"
                                                     IncrementalFilteringMode="StartsWith">
                                                </PropertiesComboBox>
                                                 </dx:GridViewDataComboBoxColumn>
                                           
                                            <dx:GridViewDataComboBoxColumn  FieldName="CompanyName"  ShowInCustomizationForm="True"
                                             VisibleIndex="19" Visible="true">
                                            <PropertiesComboBox DataSourceID="CompaniesDataSource1"  
                                                TextField="CompanyName" IncrementalFilteringMode="StartsWith">
                                            </PropertiesComboBox>
                                            <Settings SortMode="DisplayText" />
                                           
                                        </dx:GridViewDataComboBoxColumn>
                                            <%--<dx:GridViewDataTextColumn FieldName="Login" ShowInCustomizationForm="True" Visible="False"
                                                VisibleIndex="20">
                                            </dx:GridViewDataTextColumn>--%>
                                             <dx:GridViewDataComboBoxColumn FieldName="Login" VisibleIndex="20"
                                                 ShowInCustomizationForm="True" Visible="False">
                                                <PropertiesComboBox DataSourceID="UsersDataSource1" TextField="UserLogon"
                                                     IncrementalFilteringMode="StartsWith">
                                                </PropertiesComboBox>
                                                 </dx:GridViewDataComboBoxColumn>
                                            <dx:GridViewDataTextColumn FieldName="EmailAddress" ShowInCustomizationForm="True"
                                                Visible="False" VisibleIndex="21">
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn FieldName="FirstName" ShowInCustomizationForm="True" Visible="False"
                                                VisibleIndex="22">
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn FieldName="LastName" ShowInCustomizationForm="True" Visible="False"
                                                VisibleIndex="23">
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn FieldName="JobRole" ShowInCustomizationForm="True" Visible="False"
                                                VisibleIndex="24">
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn FieldName="JobTitle" ShowInCustomizationForm="True" Visible="False"
                                                VisibleIndex="25">
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn FieldName="MobileNo" ShowInCustomizationForm="True" Visible="False"
                                                VisibleIndex="26">
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn FieldName="TelephoneNo" ShowInCustomizationForm="True"
                                                Visible="False" VisibleIndex="27">
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn FieldName="FaxNo" ShowInCustomizationForm="True" Visible="False"
                                                VisibleIndex="28">
                                            </dx:GridViewDataTextColumn>
                                        </Columns>
                                        <Settings ShowFilterRow="True" ShowGroupPanel="True" ShowFilterBar="Visible"/>
                                        <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                            <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
                                            </LoadingPanelOnStatusBar>
                                            <LoadingPanel Url="~/App_Themes/Office2003BlueOld/GridView/Loading.gif"><%--Enhancement_023:change by debashis for testing platform upgradation--%>
                                            </LoadingPanel>
                                        </Images>
                                        <ImagesFilterControl>
                                            <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
                                            </LoadingPanel>
                                        </ImagesFilterControl>
                                        <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue">
                                            <Header ImageSpacing="5px" SortingImageSpacing="5px">
                                            </Header>
                                            <LoadingPanel ImageSpacing="10px">
                                            </LoadingPanel>
                                        </Styles>
                                        <StylesEditors>
                                            <ProgressBar Height="25px">
                                            </ProgressBar>
                                        </StylesEditors>
                                    </dx:ASPxGridView>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </dx:ContentControl>
            </ContentCollection>
        </dx:TabPage>
        <dx:TabPage Text="Automatic System Emails">
            <ContentCollection>
                <dx:ContentControl runat="server" SupportsDisabledAttribute="True">
                    <table style="width: 870px" cellspacing="0" cellpadding="0" border="0">
                        <tbody>
                            <tr>
                                <td style="width: 870px; padding-top: 2px; text-align: center" class="pageName" align="right">
                                    <div align="right">
                                        <a style="color: #0000ff; text-align: right; text-decoration: none" href="javascript:ShowHideCustomizationWindow3()">
                                            Customize</a>
                                    </div>
                                </td>
                            </tr>
                            <tr style="padding-top: 2px">
                                <td style="height: 13px; text-align: left">
                                    <dx:ASPxGridView ID="ASPxGridView3" ClientInstanceName="grid3" runat="server" AutoGenerateColumns="False"
                                        CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue"
                                        DataSourceID="EmailLogAutoDataSource1" Width="870px">
                                        <SettingsCustomizationWindow Enabled="True"></SettingsCustomizationWindow>
                                        <Columns>
                                            <dx:GridViewDataTextColumn FieldName="EmailLogId" ShowInCustomizationForm="True"
                                                Visible="False" VisibleIndex="1">
                                            </dx:GridViewDataTextColumn>
<%--<dx:GridViewDataTextColumn Caption="Type" FieldName="EmailLogTypeDesc" ShowInCustomizationForm="True"
                                                VisibleIndex="0">
                                                <Settings AutoFilterCondition="BeginsWith" />
                                            </dx:GridViewDataTextColumn>--%>
                                            <dx:GridViewDataComboBoxColumn FieldName="EmailLogTypeId"  Caption="Type" ShowInCustomizationForm="True" VisibleIndex="0">
                                                <PropertiesComboBox DataSourceID="EmailLogTypeDataSource1" TextField="EmailLogTypeDesc"
                                                    ValueField="EmailLogTypeId" ValueType="System.Int32" IncrementalFilteringMode="StartsWith">
                                                </PropertiesComboBox>
                                            </dx:GridViewDataComboBoxColumn>
                                            <dx:GridViewDataDateColumn Caption="Sent At" FieldName="EmailDateTime" ShowInCustomizationForm="True"
                                                SortIndex="0" SortOrder="Descending" VisibleIndex="1">
                                                <PropertiesDateEdit EditFormat="DateTime">
                                                </PropertiesDateEdit>
                                            </dx:GridViewDataDateColumn>
                                            <dx:GridViewDataTextColumn FieldName="EmailLogMessageSubject" ShowInCustomizationForm="True" VisibleIndex="4">
                                                <Settings AutoFilterCondition="Contains" />
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn Caption="From" FieldName="EmailFrom" ShowInCustomizationForm="True"
                                                Visible="false">
                                                <Settings AutoFilterCondition="Contains" />
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn Caption="To" FieldName="EmailTo" ShowInCustomizationForm="True"
                                                VisibleIndex="6">
                                                <Settings AutoFilterCondition="Contains" />
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn Caption="CC" FieldName="EmailCc" ShowInCustomizationForm="True"
                                                Visible="false">
                                                <Settings AutoFilterCondition="Contains" />
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn Caption="Bcc" FieldName="EmailBcc" ShowInCustomizationForm="True"
                                                Visible="false">
                                                <Settings AutoFilterCondition="Contains" />
                                            </dx:GridViewDataTextColumn>
                                        </Columns>
                                        <Settings ShowFilterRow="True" ShowGroupPanel="True" ShowFilterBar="Visible" />
                                        <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                            <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
                                            </LoadingPanelOnStatusBar>
                                            <LoadingPanel Url="~/App_Themes/Office2003BlueOld/GridView/Loading.gif"><%--Enhancement_023:change by debashis for testing platform upgradation--%>
                                            </LoadingPanel>
                                        </Images>
                                        <ImagesFilterControl>
                                            <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
                                            </LoadingPanel>
                                        </ImagesFilterControl>
                                        <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue">
                                            <Header ImageSpacing="5px" SortingImageSpacing="5px">
                                            </Header>
                                            <LoadingPanel ImageSpacing="10px">
                                            </LoadingPanel>
                                        </Styles>
                                        <StylesEditors>
                                            <ProgressBar Height="25px">
                                            </ProgressBar>
                                        </StylesEditors>
                                    </dx:ASPxGridView>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </dx:ContentControl>
            </ContentCollection>
        </dx:TabPage>
        <dx:TabPage Text="IHS Emails">
            <ContentCollection>
                <dx:ContentControl runat="server" SupportsDisabledAttribute="True">
                    <table style="width: 870px" cellspacing="0" cellpadding="0" border="0">
                        <tbody>
                            <tr>
                                <td style="width: 870px; padding-top: 2px; text-align: center" class="pageName" align="right">
                                    <div align="right">
                                        <a style="color: #0000ff; text-align: right; text-decoration: none" href="javascript:ShowHideCustomizationWindow2()">
                                            Customize</a>
                                    </div>
                                </td>
                            </tr>
                            <tr style="padding-top: 2px">
                                <td style="height: 13px; text-align: left">
                                    <dx:ASPxGridView ID="ASPxGridView1" ClientInstanceName="grid2" runat="server" AutoGenerateColumns="False"
                                        CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue"
                                        DataSourceID="EmailLogIhsDataSource1" Width="870px">
                                        <SettingsCustomizationWindow Enabled="True"></SettingsCustomizationWindow>
                                        <Columns>
                                            <dx:GridViewDataTextColumn FieldName="EmailLogId" ShowInCustomizationForm="True"
                                                Visible="False" VisibleIndex="1">
                                            </dx:GridViewDataTextColumn>
                                            <%--<dx:GridViewDataTextColumn Caption="Sent By" FieldName="SentByUserFullName" ShowInCustomizationForm="True"
                                                VisibleIndex="3">
                                                <Settings AutoFilterCondition="BeginsWith" />
                                            </dx:GridViewDataTextColumn>--%>
                                            <dx:GridViewDataComboBoxColumn FieldName="SentByUserId"  Caption="Sent By" ShowInCustomizationForm="True" VisibleIndex="3">
                                                <PropertiesComboBox DataSourceID="SentByDS" TextField="UserFullName"
                                                    ValueField="SentByUserId" ValueType="System.Int32" IncrementalFilteringMode="StartsWith">
                                                </PropertiesComboBox>
                                            </dx:GridViewDataComboBoxColumn>
                                            <dx:GridViewDataDateColumn Caption="Sent At" FieldName="EmailDateTime" ShowInCustomizationForm="True"
                                                SortIndex="0" SortOrder="Descending" VisibleIndex="2">
                                                <PropertiesDateEdit EditFormat="DateTime">
                                                </PropertiesDateEdit>
                                            </dx:GridViewDataDateColumn>
                                            <dx:GridViewDataTextColumn FieldName="EmailLogMessageSubject" ShowInCustomizationForm="True" VisibleIndex="4">
                                                <Settings AutoFilterCondition="Contains" />
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn Caption="From" FieldName="EmailFrom" ShowInCustomizationForm="True"
                                                Visible="false">
                                                <Settings AutoFilterCondition="Contains" />
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn Caption="To" FieldName="EmailTo" ShowInCustomizationForm="True"
                                                VisibleIndex="6">
                                                <Settings AutoFilterCondition="Contains" />
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn Caption="CC" FieldName="EmailCc" ShowInCustomizationForm="True"
                                                Visible="false">
                                                <Settings AutoFilterCondition="Contains" />
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn Caption="Bcc" FieldName="EmailBcc" ShowInCustomizationForm="True"
                                                Visible="false">
                                                <Settings AutoFilterCondition="Contains" />
                                            </dx:GridViewDataTextColumn>
                                        </Columns>
                                        <Settings ShowFilterRow="True" ShowGroupPanel="True" ShowFilterBar="Visible"/>
                                        <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                            <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
                                            </LoadingPanelOnStatusBar>
                                            <LoadingPanel Url="~/App_Themes/Office2003Blue/GridView/Loading.gif">
                                            </LoadingPanel>
                                        </Images>
                                        <ImagesFilterControl>
                                            <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
                                            </LoadingPanel>
                                        </ImagesFilterControl>
                                        <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue">
                                            <Header ImageSpacing="5px" SortingImageSpacing="5px">
                                            </Header>
                                            <LoadingPanel ImageSpacing="10px">
                                            </LoadingPanel>
                                        </Styles>
                                        <StylesEditors>
                                            <ProgressBar Height="25px">
                                            </ProgressBar>
                                        </StylesEditors>
                                    </dx:ASPxGridView>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </dx:ContentControl>
            </ContentCollection>
        </dx:TabPage>
    </TabPages>
    <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
    </LoadingPanelImage>
    <LoadingPanelStyle ImageSpacing="6px">
    </LoadingPanelStyle>
    <ContentStyle>
        <Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px" />
    </ContentStyle>
</dx:ASPxPageControl>
<data:CsmsEmailLogListDataSource ID="CsmsEmailLogListDataSource1" runat="server">
</data:CsmsEmailLogListDataSource>
<data:EmailLogIhsDataSource ID="EmailLogIhsDataSource1" runat="server">
</data:EmailLogIhsDataSource>
<data:EmailLogAutoDataSource ID="EmailLogAutoDataSource1" runat="server">
</data:EmailLogAutoDataSource>
<data:UsersFullNameDataSource ID="UsersFullNameDataSource1" runat="server" Sort="UserFullName ASC">
</data:UsersFullNameDataSource>
<data:CompaniesDataSource ID="CompaniesDataSource1" runat="server" EnableCaching="False"
                        Sort="CompanyName ASC">
</data:CompaniesDataSource>
<data:CsmsAccessDataSource ID="CsmsAccessDataSource1" runat="server" EnableCaching="false">
 </data:CsmsAccessDataSource>
 <data:EmailLogTypeDataSource ID="EmailLogTypeDataSource1" runat="server" EnableCaching="false" Sort="EmailLogTypeDesc ASC"></data:EmailLogTypeDataSource>
 <data:UsersDataSource ID="UsersDataSource1" runat="server" EnableCaching="false" Sort="UserLogon ASC"></data:UsersDataSource>
 <data:AdminTaskStatusDataSource ID="AdminTaskStatusDataSource1" runat="server" EnableCaching="false" Sort="StatusDesc ASC"></data:AdminTaskStatusDataSource>
 <data:AdminTaskTypeDataSource ID="AdminTaskTypeDataSource1" runat="server" EnableCaching="false" Sort="AdminTaskTypeName ASC"></data:AdminTaskTypeDataSource>
 <asp:SqlDataSource ID="UsersSentByDS" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="select Distinct A.SentByUserId,B.UserFullName from 
dbo.CsmsEmailLog A LEFT OUTER JOIN dbo.UsersFullName B ON
A.SentByUserId=B.UserId Order By UserFullName">
</asp:SqlDataSource>
<asp:SqlDataSource ID="OpenedByUserDS" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="select Distinct A.OpenedByUser from dbo.CsmsEmailLogList A  Order by OpenedByUser">
</asp:SqlDataSource>
<asp:SqlDataSource ID="ClosedByUserDS" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="select Distinct A.ClosedByUser from dbo.CsmsEmailLogList A  Order by ClosedByUser">
</asp:SqlDataSource>
<asp:SqlDataSource ID="SourceDS" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="select Distinct A.AdminTaskSourceDesc from dbo.CsmsEmailLogList A  Order by AdminTaskSourceDesc">
</asp:SqlDataSource>
<asp:SqlDataSource ID="SentByDS" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="select Distinct A.SentByUserId,B.UserFullName from 
dbo.EmailLog_Ihs A LEFT OUTER JOIN dbo.UsersFullName B ON
A.SentByUserId=B.UserId Order By UserFullName">
</asp:SqlDataSource>
