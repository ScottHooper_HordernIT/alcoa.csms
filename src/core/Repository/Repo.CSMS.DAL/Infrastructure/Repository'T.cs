﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.Common.Mvc.OrderByHelper;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Core.Common;
using System.Collections;
using System.Data.Entity.Core.Objects.DataClasses;
using System.Data.Entity.Core;

namespace Repo.CSMS.DAL.Infrastructure
{
    public class Repository<T> : IRepository<T>, IQueryable where T : class
    {
        #region Variables

        private readonly IObjectSet<T> _objectSet;
        private readonly IObjectSetFactory _objectSetFactory;
        private readonly IUnitOfWork _unitOfWork;

        #endregion

        #region Constructor

        public Repository(IObjectSetFactory objectSetFactory, IUnitOfWork unitOfWork)
        {
            _objectSet = objectSetFactory.CreateObjectSet<T>();
            _objectSetFactory = objectSetFactory;
            _unitOfWork = unitOfWork;

            _objectSetFactory.Context.CommandTimeout = 600;
        }

        #endregion

        #region Insert Functions

        public virtual void Insert(T entity)
        {
            _objectSet.Attach(entity);
            _objectSetFactory.ChangeObjectState(entity, EntityState.Added);
        }

        #endregion

        #region Update Functions

        public virtual void Update(T entity)
        {
            var entityKey = _objectSetFactory.Context.GetEntityKey(entity);

            if (entityKey == null || (entityKey != null && !_objectSetFactory.IsObjectAttached(entityKey)))
            {
                _objectSet.Attach(entity);
            }
            _objectSetFactory.ChangeObjectState(entity, EntityState.Modified);
        }

        //public virtual void Update(T entity)
        //{
        //    var entry = dataContext.Entry<T>(entity);

        //    if (entry.State == EntityState.Detached)
        //    {
        //        var set = dataContext.Set<T>();
        //        T attachedEntity = set.Find(entity.Id);

        //        if (attachedEntity != null)
        //        {
        //            var attachedEntry = dataContext.Entry(attachedEntity);
        //            attachedEntry.CurrentValues.SetValues(entity);
        //        }
        //        else
        //        {
        //            entry.State = EntityState.Modified;
        //        }
        //    }
        //}

        #endregion

        #region Delete Functions

        public virtual void Delete(T entity)
        {
            _objectSet.DeleteObject(entity);
        }

        public virtual void Delete(Expression<Func<T, bool>> where)
        {
            IEnumerable<T> objects = _objectSet.Where<T>(where).AsEnumerable();
            foreach (T obj in objects)
                _objectSet.DeleteObject(obj);
        }

        #endregion

        #region Get Functions

        public virtual T Get(Expression<Func<T, bool>> where)
        {
            return Get(where, new List<Expression<Func<T,object>>>(), true);
        }

        public virtual T Get(Expression<Func<T, bool>> where, List<Expression<Func<T, object>>> includes, bool isEntityTracking = true)
        {
            IQueryable<T> query = AsQueryable();

            if (!isEntityTracking)
                query = query.AsNoTracking();

            if (includes != null)
                PerformInclusions(includes, query);

            return query.Where(where).FirstOrDefault<T>();
        }

        #endregion

        #region GetAll Functions

        public virtual IEnumerable<T> GetAll()
        {
            return GetAll(null);
        }

        public virtual IEnumerable<T> GetAll(int? top)
        {
            return GetAll(top, new List<Expression<Func<T,object>>>(), true);
        }

        public virtual IEnumerable<T> GetAll(int? top, List<Expression<Func<T, object>>> includes, bool isEntityTracking = true)
        {
            IQueryable<T> query = AsQueryable();

            if (!isEntityTracking)
                query = query.AsNoTracking();

            if (includes != null)
                PerformInclusions(includes, query);

            if (top != null)
                return query.Take(top.Value).ToList();
            else
                return query.ToList();
        }

        public virtual IEnumerable<T> GetAll(int? top, Func<IQueryable<T>, IOrderedQueryable<T>> orderBy, List<Expression<Func<T, object>>> includes, bool isEntityTracking = true)
        {
            IQueryable<T> query = AsQueryable();

            if (!isEntityTracking)
                query = query.AsNoTracking();

            if (includes != null)
                PerformInclusions(includes, query);

            if (orderBy != null)
                query = orderBy(query);

            if (top != null)
                return query.Take(top.Value).ToList();
            else
                return query.ToList();
        }
        #endregion

        #region GetMany Functions

        public virtual IEnumerable<T> GetMany(Expression<Func<T, bool>> where)
        {
            return GetMany(where, null);
        }

        public virtual IEnumerable<T> GetMany(Expression<Func<T, bool>> where, int? top)
        {
            return GetMany(where, top, null, new List<Expression<Func<T,object>>>(), true);
        }

        public virtual IEnumerable<T> GetMany(Expression<Func<T, bool>> where, int? top, Func<IQueryable<T>, IOrderedQueryable<T>> orderBy)
        {
            return GetMany(where, top, orderBy, new List<Expression<Func<T, object>>>(), true);
        }

        public virtual IEnumerable<T> GetMany(Expression<Func<T, bool>> where, int? top, Func<IQueryable<T>, IOrderedQueryable<T>> orderBy, List<Expression<Func<T, object>>> includes, bool isEntityTracking = true)
        {
            IQueryable<T> query = AsQueryable();

            if (!isEntityTracking)
                query = query.AsNoTracking();

            if (includes != null)
                PerformInclusions(includes, query);

            if (orderBy != null)
                query = orderBy(query);

            if (top != null)
                return query.Where(where).Take(top.Value).ToList();
            else
                return query.Where(where).ToList();
        }

        #endregion

        #region GetSum Functions

        public virtual decimal? GetSum(Expression<Func<T, bool>> where, Expression<Func<T, decimal>> selector, List<Expression<Func<T, object>>> includes)
        {
            try
            {
                IQueryable<T> query = AsQueryable();

                if (includes != null)
                    PerformInclusions(includes, query);

                if (where != null)
                    return query.Where(where).Sum(selector);
                else
                    return query.Sum(selector);
            }
            catch
            {
                return null;
            }
        }

        public virtual float? GetSum(Expression<Func<T, bool>> where, Expression<Func<T, float>> selector, List<Expression<Func<T, object>>> includes)
        {
            try
            {
                IQueryable<T> query = AsQueryable();

                if (includes != null)
                    PerformInclusions(includes, query);

                if (where != null)
                    return query.Where(where).Sum(selector);
                else
                    return query.Sum(selector);
            }
            catch
            {
                return null;
            }
        }

        public virtual double? GetSum(Expression<Func<T, bool>> where, Expression<Func<T, double>> selector, List<Expression<Func<T, object>>> includes)
        {
            try
            {
                IQueryable<T> query = AsQueryable();

                if (includes != null)
                    PerformInclusions(includes, query);

                if (where != null)
                    return query.Where(where).Sum(selector);
                else
                    return query.Sum(selector);
            }
            catch
            {
                return null;
            }
        }

        public virtual int? GetSum(Expression<Func<T, bool>> where, Expression<Func<T, int>> selector, List<Expression<Func<T, object>>> includes)
        {
            try
            {
                IQueryable<T> query = AsQueryable();

                if (includes != null)
                    PerformInclusions(includes, query);

                if (where != null)
                    return query.Where(where).Sum(selector);
                else
                    return query.Sum(selector);
            }
            catch
            {
                return null;
            }
        }

        #endregion

        #region GetMax Functions

        public virtual int? GetMax(Expression<Func<T, bool>> where, Expression<Func<T, int>> selector, List<Expression<Func<T, object>>> includes)
        {
            try
            {
                IQueryable<T> query = AsQueryable();

                if (includes != null)
                    PerformInclusions(includes, query);

                if (where != null)
                    return query.Where(where).Max(selector);
                else
                    return query.Max(selector);
            }
            catch
            {
                return null;
            }
        }

        public virtual DateTime? GetMax(Expression<Func<T, bool>> where, Expression<Func<T, DateTime?>> selector, List<Expression<Func<T, object>>> includes)
        {
            try
            {
                IQueryable<T> query = AsQueryable();

                if (includes != null)
                    PerformInclusions(includes, query);

                if (where != null)
                    return query.Where(where).Max(selector);
                else
                    return query.Max(selector);
            }
            catch
            {
                return null;
            }
        }

        #endregion

        #region GetCount Functions

        public virtual int? GetCount(Expression<Func<T, bool>> where, List<Expression<Func<T, object>>> includes)
        {
            try
            {
                IQueryable<T> query = AsQueryable();

                if (includes != null)
                    PerformInclusions(includes, query);

                if (where != null)
                    return query.Where(where).Count();
                else
                    return query.Count();
            }
            catch
            {
                return null;
            }
        }

        #endregion

        #region GetManyPaged

        public virtual IEnumerable<T> GetManyPaged(int pageNumber, int pageSize, Expression<Func<T, bool>> where, Func<IQueryable<T>, IOrderedQueryable<T>> orderBy, out int pageCount, out int totalRecords)
        {
            return GetManyPaged(pageNumber, pageSize, where, orderBy, new List<Expression<Func<T,object>>>(), out pageCount, out totalRecords);
        }

        public virtual IEnumerable<T> GetManyPaged(int pageNumber, int pageSize, Expression<Func<T, bool>> where, Func<IQueryable<T>, IOrderedQueryable<T>> orderBy, List<Expression<Func<T, object>>> includes, out int pageCount, out int totalRecords)
        {
            IQueryable<T> query = AsQueryable();

            if (includes != null)
                PerformInclusions(includes, query);

            if (where != null)
                query = query.Where(where);

            totalRecords = query.Count();
            pageCount = ((totalRecords - 1) / pageSize) + 1;

            if (orderBy != null)
                query = orderBy(query).Skip(pageSize * (pageNumber - 1)).Take(pageSize);
            
            return query.ToList();
        }

        #endregion

        #region SaveChanges Functions

        public void SaveChanges()
        {
            _unitOfWork.Commit();
        }

        #endregion

        #region ExecuteCustomFunction (Stored Procedures)

        public int ExecuteCustomFunction(string commandName, params ObjectParameter[] parameters)
        {
            return _objectSetFactory.Context.ExecuteFunction(commandName, parameters);
        }

        public IEnumerable<T> ExecuteCustomFunction<T>(string commandName, params ObjectParameter[] parameters)
        {
            return _objectSetFactory.Context.ExecuteFunction<T>(commandName, MergeOption.NoTracking, parameters);
        }

        #endregion

        #region Miscellaneous Functions

        //Return IQueryable (ie. table to query)
        public virtual IQueryable<T> AsQueryable()
        {
            return _objectSet;
        }

        private static IQueryable<T> PerformInclusions(IEnumerable<Expression<Func<T, object>>> includeProperties, IQueryable<T> query)
        {
            return includeProperties.Aggregate(query, (current, includeProperty) => current.Include(includeProperty));
        }

        public IEnumerator GetEnumerator()
        {
            return _objectSet.GetEnumerator();
        }

        public Expression Expression
        {
            get { return _objectSet.Expression; }
        }
        public Type ElementType
        {
            get { return _objectSet.ElementType; }
        }
        public IQueryProvider Provider
        {
            get { return _objectSet.Provider; }
        }

        #endregion
    }

    public static class RepoHelper
    {
        public static EntityKey GetEntityKey<T>(this ObjectContext context, T entity) where T : class
        {
            ObjectStateEntry ose;
            if (null != entity && context.ObjectStateManager
                                    .TryGetObjectStateEntry(entity, out ose))
            {
                return ose.EntityKey;
            }
            return null;
        }
    }
}
