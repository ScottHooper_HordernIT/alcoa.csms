﻿	

#region Using Directives
using System;
using System.ComponentModel;
using System.Collections;
using System.Xml.Serialization;
using System.Data;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Entities.Validation;

using KaiZen.CSMS.Data;
using Microsoft.Practices.EnterpriseLibrary.Logging;

#endregion

namespace KaiZen.CSMS.Services
{		
	/// <summary>
	/// An component type implementation of the 'TwentyOnePointAudit_Qtr' table.
	/// </summary>
	/// <remarks>
	/// All custom implementations should be done here.
	/// </remarks>
	[CLSCompliant(true)]
	public partial class TwentyOnePointAuditQtrService : KaiZen.CSMS.Services.TwentyOnePointAuditQtrServiceBase
	{
		#region Constructors
		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAuditQtrService class.
		/// </summary>
		public TwentyOnePointAuditQtrService() : base()
		{
		}
		#endregion Constructors
		
	}//End Class

} // end namespace
