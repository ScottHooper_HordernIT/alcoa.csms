﻿using System;
using System.ComponentModel;

namespace KaiZen.CSMS.Entities
{
	/// <summary>
	///		The data structure representation of the 'QuestionnaireInitialContactAudit' table via interface.
	/// </summary>
	/// <remarks>
	/// 	This struct is generated by a tool and should never be modified.
	/// </remarks>
	public interface IQuestionnaireInitialContactAudit 
	{
		/// <summary>			
		/// AuditId : 
		/// </summary>
		/// <remarks>Member of the primary key of the underlying table "QuestionnaireInitialContactAudit"</remarks>
		System.Int32 AuditId { get; set; }
				
		
		
		/// <summary>
		/// AuditedOn : 
		/// </summary>
		System.DateTime  AuditedOn  { get; set; }
		
		/// <summary>
		/// AuditEventId : 
		/// </summary>
		System.String  AuditEventId  { get; set; }
		
		/// <summary>
		/// QuestionnaireInitialContactId : 
		/// </summary>
		System.Int32?  QuestionnaireInitialContactId  { get; set; }
		
		/// <summary>
		/// QuestionnaireId : 
		/// </summary>
		System.Int32?  QuestionnaireId  { get; set; }
		
		/// <summary>
		/// ContactFirstName : 
		/// </summary>
		System.String  ContactFirstName  { get; set; }
		
		/// <summary>
		/// ContactLastName : 
		/// </summary>
		System.String  ContactLastName  { get; set; }
		
		/// <summary>
		/// ContactEmail : 
		/// </summary>
		System.String  ContactEmail  { get; set; }
		
		/// <summary>
		/// ContactTitle : 
		/// </summary>
		System.String  ContactTitle  { get; set; }
		
		/// <summary>
		/// ContactPhone : 
		/// </summary>
		System.String  ContactPhone  { get; set; }
		
		/// <summary>
		/// ContactStatusId : 
		/// </summary>
		System.Int32?  ContactStatusId  { get; set; }
		
		/// <summary>
		/// ModifiedDate : 
		/// </summary>
		System.DateTime?  ModifiedDate  { get; set; }
		
		/// <summary>
		/// ModifiedByUserId : 
		/// </summary>
		System.Int32?  ModifiedByUserId  { get; set; }
		
		/// <summary>
		/// CreatedByUserId : 
		/// </summary>
		System.Int32?  CreatedByUserId  { get; set; }
		
		/// <summary>
		/// VerifiedDate : 
		/// </summary>
		System.DateTime?  VerifiedDate  { get; set; }
			
		/// <summary>
		/// Creates a new object that is a copy of the current instance.
		/// </summary>
		/// <returns>A new object that is a copy of this instance.</returns>
		System.Object Clone();
		
		#region Data Properties

		#endregion Data Properties

	}
}


