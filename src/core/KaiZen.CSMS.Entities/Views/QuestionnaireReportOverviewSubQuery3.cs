﻿#region Using directives

using System;

#endregion

namespace KaiZen.CSMS.Entities
{	
	///<summary>
	/// An object representation of the 'QuestionnaireReportOverview_SubQuery3' view. [No description found in the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class QuestionnaireReportOverviewSubQuery3 : QuestionnaireReportOverviewSubQuery3Base
	{
		#region Constructors

		///<summary>
		/// Creates a new <see cref="QuestionnaireReportOverviewSubQuery3"/> instance.
		///</summary>
		public QuestionnaireReportOverviewSubQuery3():base(){}	
		
		#endregion
	}
}
