//-----------------------------------------------------------------------
// <copyright file="Program.cs" company="KAI-ZEN">
//     Copyright (c) Yujia Li / KAI-ZEN. All Rights Reserved.
// </copyright>
//-----------------------------------------------------------------------
using ALCOA.CSMS.Batch.Common;
using ALCOA.CSMS.Batch.Common.Log;
using Autofac;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Services;
using LumenWorks.Framework.IO.Csv;
using Repo.CSMS.Service.Database;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Text;
using System.Web.Mvc;
using common = ALCOA.CSMS.Batch.Common.HealthCheck;

namespace ALCOA.CSMS.BatchJobScripts
{

    class Program
    {
        static string sqldb = "";
        /// <summary>
        /// Mains the specified args.
        /// </summary>
        /// <param name="args">The args.</param>
        /// <returns></returns>
        static int Main(string[] args)
        {
            //Setup dependencies
            DependencyConfig.RegisterDependencies(new ContainerBuilder());

            ConnectionStringSettings conString = ConfigurationManager.ConnectionStrings["ALCOA_WAO_CSMWP_devConnectionString"];
            sqldb = conString.ConnectionString;

            DateTime startTime1 = DateTime.Now;
            //DT Batch Failing
            Log4NetService.LogInfo("Program Started at: " + startTime1.ToString());
            header();
            try
            {
                if (args.Length == 0)
                {
                    //DT259 2/10/15 Cindi Thornton - add try/catch and HealthCheck.Update calls, also call to common.HealthCheck.ResetJobSchedule
                    UpdateIhs(); //Safety.All.UpdateDb("Ihs");
                }
                else
                {
                    try
                    {
                        switch (args[0].ToString())
                        {
                            case "--UpdateDbFromEhsims":
                                //DT259 2/10/15 Cindi Thornton - add try/catch and HealthCheck.Update calls, also call to common.HealthCheck.ResetJobSchedule
                                try
                                {
                                    common.HealthCheck.ResetJobSchedule("@EHSIMSDataImport_@EHSIMSDataImport"); //DT259 2/10/15 Cindi Thornton - used to send emails if this hasn't run on schedule
                                    Safety.All.UpdateDb("Ehsims");
                                    HealthCheck.Update(true, "UpdateDbFromEhsims");
                                }
                                catch (Exception ex)
                                {
                                    HealthCheck.Update(false, "UpdateDbFromEhsims", null, ex);
                                }
                                break;
                            case "--UpdateDbFromIhs":
                                //DT259 2/10/15 Cindi Thornton - add try/catch and HealthCheck.Update calls, also call to common.HealthCheck.ResetJobSchedule
                                UpdateIhs();
                                break;
                            case "--UpdateRNCActionOpenClosed":
                                try
                                {
                                    Safety.IHS.UpdateRNCActionOpenCLosed();
                                    HealthCheck.Update(true, "UpdateRNCActionOpenClosed");
                                }
                                catch (Exception ex)
                                {
                                    HealthCheck.Update(false, "UpdateRNCActionOpenClosed", null, ex);
                                }
                                break;
                            case "--UpdateRs":
                                //UpdateRs();
                                break;
                            case "--SendSqExpiryReminderEmails":
                                //DT259 2/10/15 Cindi Thornton - add try/catch and HealthCheck.Update calls
                                try
                                {
                                    Sq.Email.SqExpiryReminderEmails();
                                    HealthCheck.Update(true, "SendSqExpiryReminderEmails");
                                }
                                catch (Exception ex)
                                {
                                    HealthCheck.Update(false, "SendSqExpiryReminderEmails", null, ex);
                                }
                                break;
                            case "--TestEhsimsCodes":
                                //DT259 2/10/15 Cindi Thornton - add try/catch and HealthCheck.Update calls
                                try
                                {
                                    Safety.EHSIMS.TestEhsimsCodes();
                                    HealthCheck.Update(true, "TestEhsimsCodes");
                                }
                                catch (Exception ex)
                                {
                                    HealthCheck.Update(false, "TestEhsimsCodes", null, ex);
                                }
                                break;
                            case "--TestGetCompaniesStatus":
                                //DT259 2/10/15 Cindi Thornton - add try/catch and HealthCheck.Update calls
                                try
                                {
                                    TestGetCompaniesStatus();
                                    HealthCheck.Update(true, "TestGetCompaniesStatus");
                                }
                                catch (Exception ex)
                                {
                                    HealthCheck.Update(false, "TestGetCompaniesStatus", null, ex);
                                }
                                break;
                            case "--Add":
                                //DT259 2/10/15 Cindi Thornton - add try/catch and HealthCheck.Update calls
                                try
                                {
                                    if (args[1].ToString() == "Companies")
                                        Scripts.Insert.Companies(args[2].ToString());
                                    HealthCheck.Update(true, "Add");
                                }
                                catch (Exception ex)
                                {
                                    HealthCheck.Update(false, "Add", null, ex);
                                }
                                break;

                            case "--SendEmail":
                                //DT259 2/10/15 Cindi Thornton - add try/catch and HealthCheck.Update calls
                                try
                                {
                                    if (!String.IsNullOrEmpty(args[1].ToString()))
                                    {
                                        string[] to = { args[1].ToString() };

                                        var emailLogService = DependencyResolver.Current.GetService<IEmailLogService>();
                                        emailLogService.Insert(to, null, null, "Test", "...", "General", false, null, null);

                                    }
                                    HealthCheck.Update(true, "SendEmail");
                                }
                                catch (Exception ex)
                                {
                                    HealthCheck.Update(false, "SendEmail", null, ex);
                                }
                                break;
                            case "--ListSqExpiryReminders":
                                //DT259 2/10/15 Cindi Thornton - add try/catch and HealthCheck.Update calls
                                try
                                {
                                    Sq.List.SqExpiryReminders();
                                    HealthCheck.Update(true, "ListSqExpiryReminders");
                                }
                                catch (Exception ex)
                                {
                                    HealthCheck.Update(false, "ListSqExpiryReminders", null, ex);
                                }
                                break;
                            case "--UpdateDbFromEbi":
                                //DT259 2/10/15 Cindi Thornton - add try/catch and HealthCheck.Update calls
                                try
                                {
                                    if (args.Length < 2) throw new Exception("--UpdateDbFromEbi [AllArchive|AllArchive2|AllRecent|MostRecent]: All or MostRecent option must be selected.");

                                    string opt;
                                    if (args.Length == 3)
                                        opt = args[2].ToString();
                                    else
                                        opt = "";
                                    EBI.Statistics.UpdateCsmsEbi(args[1].ToString(), opt);
                                    HealthCheck.Update(true, "UpdateDbFromEbi");
                                }
                                catch (Exception ex)
                                {
                                    HealthCheck.Update(false, "UpdateDbFromEbi", null, ex);
                                }
                                break;
                            case "--UpdateEbiAllVendorNumber"://Vikas
                                try
                                {
                                    EBI.Statistics.UpdateEbiAllVendorNumber();
                                    HealthCheck.Update(true, "UpdateEbiAllVendorNumber");
                                }
                                catch (Exception ex)
                                {
                                    HealthCheck.Update(false, "UpdateEbiAllVendorNumber", null, ex);
                                }
                                break;
                            case "--UpdateEbiVendorNumberByYear"://Vikas
                                try
                                {
                                    if (args.Length == 2)
                                    {
                                        EBI.Statistics.UpdateEbiVendorNumberByYear(Convert.ToInt32(args[1]));
                                    }
                                    else
                                        EBI.Statistics.UpdateEbiVendorNumberByYear(2013);
                                    HealthCheck.Update(true, "UpdateEbiAllVendorNumber");
                                }
                                catch (Exception ex)
                                {
                                    HealthCheck.Update(false, "UpdateEbiAllVendorNumber", null, ex);
                                }
                                break;
                            case "--UpdateKpiFromEbiDb":
                                //DT259 2/10/15 Cindi Thornton - add try/catch and HealthCheck.Update calls
                                try
                                {
                                    if (args.Length >= 1)
                                    {
                                        if (args.Length == 2)
                                            EBI.Statistics.UpdateCsmsKpi(args[1].ToString());
                                        else
                                            EBI.Statistics.UpdateCsmsKpi("");
                                    }
                                    else
                                    {
                                        throw new Exception("--UpdateDbFromEbi : Has no additional arguments/parameters.");
                                    }
                                    HealthCheck.Update(true, "UpdateKpiFromEbiDb");
                                }
                                catch (Exception ex)
                                {
                                    HealthCheck.Update(false, "UpdateKpiFromEbiDb", null, ex);
                                }
                                break;
                            case "--UpdateContractManager":
                                //DT259 2/10/15 Cindi Thornton - add try/catch and HealthCheck.Update calls
                                try
                                {
                                    UpdateContractManager(args[1].ToString(), args[2].ToString());
                                    HealthCheck.Update(true, "UpdateContractManager");
                                }
                                catch (Exception ex)
                                {
                                    HealthCheck.Update(false, "UpdateContractManager", null, ex);
                                }
                                break;
                            case "--UpdateProcContact":
                                //DT259 2/10/15 Cindi Thornton - add try/catch and HealthCheck.Update calls
                                try
                                {
                                    Scripts.Update.ProcurementContacts();
                                    HealthCheck.Update(true, "UpdateProcContact");
                                }
                                catch (Exception ex)
                                {
                                    HealthCheck.Update(false, "UpdateProcContact", null, ex);
                                }
                                break;
                            case "--ResetApssReadFlagIfNewVersion":
                                //DT259 2/10/15 Cindi Thornton - add try/catch and HealthCheck.Update calls
                                try
                                {
                                    Scripts.Misc.ResetApssReadFlagIfNewVersion();
                                    HealthCheck.Update(true, "ResetApssReadFlagIfNewVersion");
                                }
                                catch (Exception ex)
                                {
                                    HealthCheck.Update(false, "ResetApssReadFlagIfNewVersion", null, ex);
                                }
                                break;
                            case "--ProcurementWorkReminderEmail":
                                try
                                {
                                    common.HealthCheck.ResetJobSchedule("@EmailProcurement_@ProcurementWorkReminderEmail"); //DT259 2/10/15 Cindi Thornton - used to send emails if this hasn't run on schedule
                                    Sq.Email.ProcurementWorkReminderEmail();
                                    //escalation emails
                                    Sq.Email.ProcurementEscalationEmail();
                                    Sq.Email.SupplierEscalationEmail();
                                    HealthCheck.Update(true, "ProcurementWorkReminderEmail");
                                }
                                catch (Exception ex)
                                {
                                    HealthCheck.Update(false, "ProcurementWorkReminderEmail", null, ex);
                                }
                                break;
                            case "--UpdateFatigueManagement"://Vikas
                                try
                                {
                                    EBI.Statistics.UpdateFatigueManagementTable(false);
                                    HealthCheck.Update(true, "UpdateFatigueManagementHistory");
                                }
                                catch (Exception ex)
                                {
                                    HealthCheck.Update(false, "UpdateFatigueManagementHistory", null, ex);
                                }
                                break;
                            case "--HSAssessorWorkReminderEmail":
                                try
                                {
                                    common.HealthCheck.ResetJobSchedule("@EmailHS_@HSAssessorWorkReminderEmail"); //DT259 2/10/15 Cindi Thornton - used to send emails if this hasn't run on schedule
                                    Sq.Email.HSAssessorWorkReminderEmail();
                                    //escalation emails
                                    Sq.Email.HsAssessorEscalationEmail();
                                    HealthCheck.Update(true, "HSAssessorWorkReminderEmail");
                                }
                                catch (Exception ex)
                                {
                                    HealthCheck.Update(false, "HSAssessorWorkReminderEmail", null, ex);
                                }
                                break;
                            case "--UpdateKpiProjectsToNewTable":
                                //DT259 2/10/15 Cindi Thornton - add try/catch and HealthCheck.Update calls
                                try
                                {
                                    UpdateKpiProjectsToNewTable();
                                    HealthCheck.Update(true, "UpdateKpiProjectsToNewTable");
                                }
                                catch (Exception ex)
                                {
                                    HealthCheck.Update(false, "UpdateKpiProjectsToNewTable", null, ex);
                                }
                                break;
                            case "--UpdateCompanyStatusIfQuestionnaireExpired":
                                //DT259 2/10/15 Cindi Thornton - add try/catch and HealthCheck.Update calls, also call to common.HealthCheck.ResetJobSchedule
                                try
                                {
                                    common.HealthCheck.ResetJobSchedule("@QuestionnaireExpiryCheck_@UpdateCompanyStatusIfQuestionnaireExpired"); //DT259 2/10/15 Cindi Thornton - used to send emails if this hasn't run on schedule
                                    Sq.Update.CompanyStatusIfQuestionnaireExpired();
                                    HealthCheck.Update(true, "UpdateCompanyStatusIfQuestionnaireExpired");
                                }
                                catch (Exception ex)
                                {
                                    HealthCheck.Update(false, "UpdateCompanyStatusIfQuestionnaireExpired", null, ex);
                                }
                                break;
                            case "--UpdateQuestionnairePresentlyWith":
                                //DT259 2/10/15 Cindi Thornton - add try/catch and HealthCheck.Update calls
                                try
                                {
                                    Sq.Update.QuestionnairePresentlyWith();
                                    HealthCheck.Update(true, "UpdateQuestionnairePresentlyWith");
                                }
                                catch (Exception ex)
                                {
                                    HealthCheck.Update(false, "UpdateQuestionnairePresentlyWith", null, ex);
                                }
                                break;
                            case "--UpdatePastQuestionnairePresentlyWith":
                                //DT259 2/10/15 Cindi Thornton - add try/catch and HealthCheck.Update calls
                                try
                                {
                                    Sq.Update.PastQuestionnairePresentlyWith(sqldb);
                                    HealthCheck.Update(true, "UpdatePastQuestionnairePresentlyWith");
                                }
                                catch (Exception ex)
                                {
                                    HealthCheck.Update(false, "UpdatePastQuestionnairePresentlyWith", null, ex);
                                }
                                break;
                            case "--GenerateContractorHoursInputIaFile":
                                string activity = "GenerateContractorHoursInputIaFile";

                                try
                                {
                                    if (args.Length == 2)
                                    {
                                        if (args[1].ToString() == "lastmonth")
                                        {
                                            common.HealthCheck.ResetJobSchedule("@IhsLastMonth_@GenerateContractorHoursInputIaFile-lastmonth"); //DT259 2/10/15 Cindi Thornton - used to send emails if this hasn't run on schedule
                                            GenerateContractorHoursInputIaFile(true);
                                            activity += " lastmonth";
                                        }
                                        else
                                        {
                                            throw new Exception("invalid argument. try 'lastnmonth'");
                                        }
                                    }
                                    else
                                    {
                                        common.HealthCheck.ResetJobSchedule("@IhsAll_@GenerateContractorHoursInputIaFile"); //DT259 2/10/15 Cindi Thornton - used to send emails if this hasn't run on schedule
                                        GenerateContractorHoursInputIaFile(false);
                                    }

                                    HealthCheck.Update(true, activity);
                                }
                                catch (Exception ex)
                                {
                                    HealthCheck.Update(false, activity, "Exception: " + ex.Message);
                                }
                                break;
                            case "--PopulateResidentialCategoryFromResidencyTable":
                                //DT259 2/10/15 Cindi Thornton - add try/catch and HealthCheck.Update calls
                                try
                                {
                                    PopulateResidentialCategoryFromResidencyTable();
                                    HealthCheck.Update(true, "PopulateResidentialCategoryFromResidencyTable");
                                }
                                catch (Exception ex)
                                {
                                    HealthCheck.Update(false, "PopulateResidentialCategoryFromResidencyTable", null, ex);
                                }
                                break;
                            case "--PopulateResidentialCategoryFromSqLocationApproval":
                                //DT259 2/10/15 Cindi Thornton - add try/catch and HealthCheck.Update calls
                                try
                                {
                                    PopulateResidentialCategoryFromSqLocationApproval();
                                    HealthCheck.Update(true, "PopulateResidentialCategoryFromSqLocationApproval");
                                }
                                catch (Exception ex)
                                {
                                    HealthCheck.Update(false, "PopulateResidentialCategoryFromSqLocationApproval", null, ex);
                                }
                                break;
                            case "--UpdateAdHocRadarNoCompaniesAndCSAPreviousMonth":
                                ALCOA.CSMS.BatchJobScripts.Code.RadarHelper rh = new ALCOA.CSMS.BatchJobScripts.Code.RadarHelper();
                                try
                                {
                                    rh.RecordCompanySiteCategoryStandardHistory();
                                    Log4NetService.LogInfo("Recorded the Company Site Categories for previous months");
                                    rh.UpdateNumberOfCompaniesAllSites();
                                    Log4NetService.LogInfo("Updated Number Of Companies for Sites");
                                    rh.UpdateNumberOfCompaniesAllRegions();
                                    Log4NetService.LogInfo("Updated Number Of Companies for Regions");
                                    rh.UpdateNumberOfCompaniesOnSiteAllSites();
                                    Log4NetService.LogInfo("Updated Number Of Companies On Site for Sites");
                                    rh.UpdateNumberOfCompaniesOnSiteAllRegions();
                                    Log4NetService.LogInfo("Updated Number Of Companies On Site for Regions");
                                    rh.UpdatePercentageCSACompliantAllSites();
                                    Log4NetService.LogInfo("Updated Number Of Companies CSA Compliant for Sites");
                                    rh.UpdatePercentageCSACompliantAllRegions();
                                    Log4NetService.LogInfo("Updated Number Of Companies CSA Compliant for Regions");
                                    rh.UpdateCompaniesNotCSACompliantAllSites();
                                    Log4NetService.LogInfo("Updated Companies Not CSA Compliant for Sites");
                                    rh.UpdateNonCompliantCompaniesAllSites("iWSC", "Workplace Safety & Compliance","WorkplaceSafetyCompliance");
                                    Log4NetService.LogInfo("Updated Companies Not Workplace Safety & Compliance Compliant for Sites");
                                    rh.UpdateNonCompliantCompaniesAllSites("oNoHSWC", "Management Health Safety Work Contacts","HealthSafetyWorkContacts");
                                    Log4NetService.LogInfo("Updated Companies Not Management Health Safety Work Contacts Compliant for Sites");
                                    rh.UpdateNonCompliantCompaniesAllSites("oNoBSP", "Behavioural Observations (%)", "BehaviouralSafetyProgram");
                                    Log4NetService.LogInfo("Updated Companies Not Compliant for Behavioural Observations (%) for Sites");
                                    rh.UpdateNonCompliantCompaniesAllSites("mFatality", "Fatality Prevention Program","FatalityPrevention");
                                    Log4NetService.LogInfo("Updated Companies Not Fatality Prevention Program Compliant for Sites");
                                    rh.UpdateNonCompliantCompaniesAllSites("JSAAudits", "JSA Field Audit Verifications","JSAScore");
                                    Log4NetService.LogInfo("Updated Companies Not JSA Audits Compliant for Sites");
                                    rh.UpdateNonCompliantCompaniesAllSites("mTbmpm", "Toolbox Meetings","ToolboxMeetingsPerMonth");
                                    Log4NetService.LogInfo("Updated Companies Not Toolbox Meetings Compliant for Sites");
                                    rh.UpdateNonCompliantCompaniesAllSites("mAwcm", "Alcoa Weekly Contractors Meeting","AlcoaWeeklyContractorsMeeting");
                                    Log4NetService.LogInfo("Updated Companies Not Alcoa Weekly Contractors Meeting Compliant for Sites");
                                    rh.UpdateNonCompliantCompaniesAllSites("mAmcm", "Alcoa Monthly Contractors Meeting","AlcoaMonthlyContractorsMeeting");
                                    Log4NetService.LogInfo("Updated Companies Not Compliant for Alcoa Monthly Contractors Meeting for Sites");
                                    rh.UpdateNonCompliantCompaniesAllSites("Training", "% Mandated Training","Training");
                                    Log4NetService.LogInfo("Updated Companies Not Compliant for % Mandated Training for Sites");
                                    rh.UpdateNonCompliantCompaniesSafetyPlansAllSites();
                                    Log4NetService.LogInfo("Updated Companies Not Compliant for Safety Plan(s) Submitted for Sites");
                                    rh.UpdatePercentageSafetyPlansCompliantAllSites();
                                    Log4NetService.LogInfo("Updated Percentage of Companies Compliant for Safety Plan(s) Submitted for Sites");
                                    rh.UpdatePercentageSafetyPlansCompliantAllRegions();
                                    Log4NetService.LogInfo("Updated Percentage of Companies Compliant for Safety Plan(s) Submitted for Regions");
                                }
                                catch (Exception ex)
                                {
                                    HealthCheck.Update(false, "UpdateAdHocRadarNoCompaniesAndCSAPreviousMonth", null, ex);
                                }
                                break;
                            case "--UpdateAdHocRadar":

                                StringBuilder errorlist = new StringBuilder();
                                switch (args.Length)
                                {
                                    case 2:
                                        if (args[1].ToString() == "All")
                                        {
                                            Update_AdHoc_Radar(true, false, errorlist); //DT419 - added isBackdatedMonth = false
                                        }
                                        else
                                        {
                                            throw new Exception("--UpdateAdHocRadar : Unexpected Argument");
                                        }
                                        break;
                                    case 3:
                                        if (args[1].ToString() == "Year")
                                        {
                                            try
                                            {
                                                int i;
                                                bool isInt = Int32.TryParse(args[2].ToString(), out i);
                                                if (isInt)
                                                {
                                                    if (i <= DateTime.Now.Year)
                                                    {
                                                        Update_AdHoc_Radar(i, 0, false, errorlist); //DT418 - added month param == 0 for YTD, DT419 added isBackdatedMonth param
                                                    }
                                                    else if (i == 9999)
                                                    {
                                                        try
                                                        {
                                                            common.HealthCheck.ResetJobSchedule("@UpdateAdHocRadarYear9999_@UpdateAdHocRadarYear"); //DT259 2/10/15 Cindi Thornton - used to send emails if this hasn't run on schedule
                                                            //Yesterday's stuff
                                                            DateTime dtYesterday = DateTime.Now.AddDays(-1);
                                                            Update_AdHoc_Radar(dtYesterday.Year, 0, false, errorlist);  //DT418 - added month param == 0 for YTD, DT419 added isBackdatedMonth param
                                                            Log4NetService.LogInfo("Updating " + dtYesterday.Year);

                                                            if (errorlist.Length == 0) //if something failed, fail the job
                                                                HealthCheck.Update(true, "UpdateAdHocRadar year 9999");
                                                            else
                                                                HealthCheck.Update(false, "UpdateAdHocRadar year 9999", errorlist.ToString());
                                                        }
                                                        catch (Exception ex)
                                                        {
                                                            HealthCheck.Update(false, "UpdateAdHocRadar year 9999", errorlist.ToString(), ex);
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    throw new Exception("Not a Valid Year.");
                                                }
                                            }
                                            catch (Exception ex)
                                            {
                                                throw new Exception("--UpdateAdHocRadar :", ex); //CT 4/12/15 - throw full exception as inner exception, was just ex.Message
                                            }
                                        }
                                        else
                                        {
                                            throw new Exception("--UpdateAdHocRadar : Unexpected Argument");
                                        }
                                        break;
                                    case 4:
                                        //DT418 4/12/15 Cindi Thornton - change code to allow a single month to be run. Parameter 3 = year, param 4 = month, 9999 == current
                                        AdHocRadar_Month(args, errorlist);
                                        break;
                                    //end DT418 change
                                    default:
                                        Update_AdHoc_Radar(false, false, errorlist); //DT419 - added isBackdatedMonth = false
                                        break;
                                }
                                break;
                            case "--Update_AdHoc_Radar_test":
                                //DT259 2/10/15 Cindi Thornton - add try/catch and HealthCheck.Update calls
                                try
                                {
                                    Update_AdHoc_Radar_test();
                                    HealthCheck.Update(true, "Update_AdHoc_Radar_test");
                                }
                                catch (Exception ex)
                                {
                                    HealthCheck.Update(false, "Update_AdHoc_Radar_test", null, ex);
                                }
                                break;
                            case "--Update_KpiProjectsList":
                                //DT259 2/10/15 Cindi Thornton - add try/catch and HealthCheck.Update calls
                                try
                                {
                                    Update_KpiProjectsList();
                                    HealthCheck.Update(true, "Update_KpiProjectsList");
                                }
                                catch (Exception ex)
                                {
                                    HealthCheck.Update(false, "Update_KpiProjectsList", null, ex);
                                }
                                break;
                            case "--Update_KpiPurchaseOrderList":
                                //DT259 2/10/15 Cindi Thornton - add try/catch and HealthCheck.Update calls, also call to common.HealthCheck.ResetJobSchedule
                                try
                                {
                                    common.HealthCheck.ResetJobSchedule("@Update_KpiPurchaseOrderList"); //DT259 2/10/15 Cindi Thornton - used to send emails if this hasn't run on schedule
                                    Update_KpiPurchaseOrderList();
                                    HealthCheck.Update(true, "Update_KpiPurchaseOrderList");
                                }
                                catch (Exception ex)
                                {
                                    HealthCheck.Update(false, "Update_KpiPurchaseOrderList", null, ex);
                                }
                                break;
                            case "--UpdateContractors":
                                //DT259 2/10/15 Cindi Thornton - add try/catch and HealthCheck.Update calls
                                try
                                {
                                    UpdateContractors();
                                    HealthCheck.Update(true, "UpdateContractors");
                                }
                                catch (Exception ex)
                                {
                                    HealthCheck.Update(false, "UpdateContractors", null, ex);
                                }
                                break;
                            case "--GenerateKpiProjectHoursReport":
                                //DT259 2/10/15 Cindi Thornton - add try/catch and HealthCheck.Update calls
                                try
                                {
                                    Scripts.Misc.GenerateKpiProjectHoursReport();
                                    HealthCheck.Update(true, "GenerateKpiProjectHoursReport");
                                }
                                catch (Exception ex)
                                {
                                    HealthCheck.Update(false, "GenerateKpiProjectHoursReport", null, ex);
                                }
                                break;
                            case "--UpdateSafetyCategoriesFromFile":
                                //DT259 2/10/15 Cindi Thornton - add try/catch and HealthCheck.Update calls
                                try
                                {
                                    UpdateSafetyCategoriesFromFile(args[1].ToString());
                                    HealthCheck.Update(true, "UpdateSafetyCategoriesFromFile");
                                }
                                catch (Exception ex)
                                {
                                    HealthCheck.Update(false, "UpdateSafetyCategoriesFromFile", null, ex);
                                }
                                break;
                            case "--UpdateSafetyCategoriesFromFileTest":
                                //DT259 2/10/15 Cindi Thornton - add try/catch and HealthCheck.Update calls
                                try
                                {
                                    UpdateSafetyCategoriesFromFile(@"c:\temp\arpph.csv");
                                    HealthCheck.Update(true, "UpdateSafetyCategoriesFromFileTest");
                                }
                                catch (Exception ex)
                                {
                                    HealthCheck.Update(false, "UpdateSafetyCategoriesFromFileTest", null, ex);
                                }
                                break;
                            case "--EbiCsvDump":
                                //DT259 2/10/15 Cindi Thornton - add try/catch and HealthCheck.Update calls
                                try
                                {
                                    if (args.Length >= 3)
                                    {
                                        int Year = 0;
                                        Int32.TryParse(args[2].ToString(), out Year);
                                        if (Year >= 1990)
                                        {
                                            if (args.Length >= 4)
                                            {
                                                int Month = 0;
                                                Int32.TryParse(args[3].ToString(), out Month);
                                                if (Month >= 1 && Month <= 12)
                                                {
                                                    if (args.Length == 5)
                                                    {
                                                        int Day = 0;
                                                        Int32.TryParse(args[4].ToString(), out Day);
                                                        if (Day >= 1 && Day <= 31)
                                                        {
                                                            EBI.Statistics.ExportCsv(args[1].ToString(), Year, Month, Day);
                                                        }
                                                        else
                                                        {
                                                            throw new Exception("Invalid Day! Must be between 1 and 31");
                                                        }
                                                    }
                                                    else
                                                    {
                                                        EBI.Statistics.ExportCsv(args[1].ToString(), Year, Month, null);
                                                    }
                                                }
                                                else
                                                {
                                                    throw new Exception("Invalid Month! Must be between 1 and 12");
                                                }
                                            }
                                            else
                                            {
                                                EBI.Statistics.ExportCsv(args[1].ToString(), Year, null, null);
                                            }
                                        }
                                        else
                                        {
                                            throw new Exception("Invalid Year! I don't think there's any data for <1990");
                                        }
                                    }
                                    else if (args.Length == 2)
                                    {
                                        EBI.Statistics.ExportCsv(args[1].ToString(), null, null, null);
                                        EBI.Metrics.Update(false);

                                        DateTime dtNow = DateTime.Now;
                                        DateTime dtFirst = new DateTime(dtNow.Year, 1, 1);
                                        int i = 0;
                                        while (i < 52)
                                        {
                                            if (i != 0)
                                                dtFirst = dtFirst.AddDays(7);

                                            if (dtFirst.Year > dtNow.Year) dtFirst = new DateTime(dtNow.Year, 12, 31);

                                            if (dtFirst.Day == dtNow.Day && dtFirst.Month == dtNow.Month && dtFirst.Year == dtNow.Year)
                                                Sq.Update.QuestionnairePresentlyWithMetrics();

                                            i++;
                                        }
                                    }
                                    HealthCheck.Update(true, "EbiCsvDump");
                                }
                                catch (Exception ex)
                                {
                                    HealthCheck.Update(false, "EbiCsvDump", null, ex);
                                }
                                break;
                            case "--CorrectInitialRiskRating":
                                //DT259 2/10/15 Cindi Thornton - add try/catch and HealthCheck.Update calls
                                try
                                {
                                    CorrectInitialRiskRating();
                                    HealthCheck.Update(true, "CorrectInitialRiskRating");
                                }
                                catch (Exception ex)
                                {
                                    HealthCheck.Update(false, "CorrectInitialRiskRating", null, ex);
                                }
                                break;
                            case "--UpdateCompanyOnSitePrevious":
                                //DT259 2/10/15 Cindi Thornton - add try/catch and HealthCheck.Update calls
                                try
                                {
                                    UpdateCompanyOnSitePrevious();
                                    HealthCheck.Update(true, "UpdateCompanyOnSitePrevious");
                                }
                                catch (Exception ex)
                                {
                                    HealthCheck.Update(false, "UpdateCompanyOnSitePrevious", null, ex);
                                }
                                break;
                            case "--UpdateEbiMetrics":
                                //DT259 2/10/15 Cindi Thornton - add try/catch and HealthCheck.Update calls
                                try
                                {
                                    CorrectInitialRiskRating();

                                    if (args.Length == 2)
                                    {
                                        if (args[1].ToString() == "live")
                                        {
                                            EBI.Metrics.Update(true);
                                        }
                                    }
                                    else
                                    {
                                        EBI.Metrics.Update(false);
                                    }
                                    HealthCheck.Update(true, "UpdateEbiMetrics");
                                }
                                catch (Exception ex)
                                {
                                    HealthCheck.Update(false, "UpdateEbiMetrics", null, ex);
                                }
                                break;
                                //DT2325 14/01/16 Cindi Thornton - new section to update EBIMetrics table that allows a date range.
                                //Didn't use existing as it also does CorrectInitialRiskRating which perhaps shouldn't be run???
                                //It also didn't allow for multiple dates
                            case "--EBIMetric":
                                try
                                {
                                    Log4NetService.LogInfo(String.Format("--EBIMetric with {0} arguments", args.Length));
                                    if(args.Length < 2 || args.Length > 4)
                                    {
                                        throw new Exception("Expecting --EBIMetric Today, --EBI Metric Yesterday --EBIMetric Other <start date> or --EBIMetric Other <start date> <end date>");
                                    }
                                    else
                                    {
                                        string type = args[1].ToString();

                                        if (type.ToLower() != "today" && type.ToLower() != "yesterday" && type.ToLower() != "other")
                                        {
                                            throw new Exception("Expecting second argument Today, Yesterday or Other");
                                        }

                                        if (args.Length == 2)
                                        {
                                            if(type.ToLower() == "today")
                                            {
                                                EBI.Metrics.Update(false, DateTime.Today);
                                            }
                                            else if(type.ToLower() == "yesterday")
                                            {
                                                EBI.Metrics.Update(false, DateTime.Today.AddDays(-1));
                                            }
                                            else if(type.ToLower() == "other")
                                            {
                                                throw new Exception("If using 'Other' you need to provide at least one date in the format yyyy-mm-dd");
                                            }
                                        }
                                        else // args.Length >= 3
                                        {
                                            var startDateAsString = args[2].ToString(); //check for a valid start date
                                            DateTime startDate;

                                            if (!DateTime.TryParse(startDateAsString, out startDate))
                                            {
                                                throw new Exception("Expecting third argument to be a valid date, use yyyy-mm-dd");
                                            }

                                            if (args.Length == 3)
                                            {
                                                EBI.Metrics.Update(false, startDate);
                                            }
                                            else if (args.Length == 4)  //do everyday between the 2 dates
                                            {
                                                var endDateAsString = args[3].ToString();
                                                DateTime endDate;

                                                if (!DateTime.TryParse(endDateAsString, out endDate))
                                                {
                                                    throw new Exception("Expecting fourth argument to be a valid date, use yyyy-mm-dd");
                                                }

                                                EBI.Metrics.Update(false, startDate, endDate);
                                            }
                                        }
                                    }
                                    HealthCheck.Update(true, "EbiMetric");
                                }
                                catch (Exception ex)
                                {
                                    HealthCheck.Update(false, "EbiMetric", null, ex);
                                }
                                break; //end DT325
                            case "--ProcurementEscalationEmail":
                                //DT259 2/10/15 Cindi Thornton - add try/catch and HealthCheck.Update calls
                                try
                                {
                                    Sq.Email.ProcurementEscalationEmail();
                                    HealthCheck.Update(true, "ProcurementEscalationEmail");
                                }
                                catch (Exception ex)
                                {
                                    HealthCheck.Update(false, "ProcurementEscalationEmail", null, ex);
                                }
                                break;
                            case "--HsAssessorEscalationEmail":
                                //DT259 2/10/15 Cindi Thornton - add try/catch and HealthCheck.Update calls
                                try
                                {
                                    Sq.Email.HsAssessorEscalationEmail();
                                    HealthCheck.Update(true, "HsAssessorEscalationEmail");
                                }
                                catch (Exception ex)
                                {
                                    HealthCheck.Update(false, "HsAssessorEscalationEmail", null, ex);
                                }
                                break;
                            case "--SupplierEscalationEmail":
                                //DT259 2/10/15 Cindi Thornton - add try/catch and HealthCheck.Update calls
                                try
                                {
                                    Sq.Email.SupplierEscalationEmail();
                                    HealthCheck.Update(true, "SupplierEscalationEmail");
                                }
                                catch (Exception ex)
                                {
                                    HealthCheck.Update(false, "SupplierEscalationEmail", null, ex);
                                }
                                break;
                            case "--EscalationEmails":
                                //DT259 2/10/15 Cindi Thornton - add try/catch and HealthCheck.Update calls
                                try
                                {
                                    Sq.Email.ProcurementEscalationEmail();
                                    Sq.Email.SupplierEscalationEmail();
                                    Sq.Email.HsAssessorEscalationEmail();
                                    HealthCheck.Update(true, "EscalationEmails");
                                }
                                catch (Exception ex)
                                {
                                    HealthCheck.Update(false, "EscalationEmails", null, ex);
                                }
                                break;
                            case "--CrpContacts":
                                //DT259 2/10/15 Cindi Thornton - add try/catch and HealthCheck.Update calls
                                try
                                {
                                    Scripts.Update.CrpContacts();
                                    HealthCheck.Update(true, "CrpContacts");
                                }
                                catch (Exception ex)
                                {
                                    HealthCheck.Update(false, "CrpContacts", null, ex);
                                }
                                break;
                            case "--QuestionnairePresentlyWithMetrics":
                                //DT259 2/10/15 Cindi Thornton - add try/catch and HealthCheck.Update calls
                                try
                                {
                                    Sq.Update.QuestionnairePresentlyWithMetrics();
                                    HealthCheck.Update(true, "QuestionnairePresentlyWithMetrics");
                                }
                                catch (Exception ex)
                                {
                                    HealthCheck.Update(false, "QuestionnairePresentlyWithMetrics", null, ex);
                                }
                                break;
                            case "--UpdateCompanyVendorTable": //Vikas
                                try
                                {
                                    UpdateCompanyVendor();
                                    HealthCheck.Update(true, "UpdateCompanyVendorTable");
                                }
                                catch (Exception ex)
                                {
                                    HealthCheck.Update(false, "UpdateCompanyVendorTable", null, ex);
                                }
                                break;
                            case "--UpdateIHSMappingTable"://Vikas
                                try
                                {
                                    UpdateIHSMappingTable();
                                    HealthCheck.Update(true, "UpdateIHSMappingTable");
                                }
                                catch (Exception ex)
                                {
                                    HealthCheck.Update(false, "UpdateIHSMappingTable", null, ex);
                                }
                                break;
                            case "--UpdateCRP"://Vikas
                                try
                                {
                                    UpdateCRP();
                                    HealthCheck.Update(true, "UpdateCRP");
                                }
                                catch (Exception ex)
                                {
                                    HealthCheck.Update(false, "UpdateCRP", null, ex);
                                }
                                break;
                            case "--ExportIHSData"://Vikas
                                try
                                {
                                    ExportIHSDataToCSV();
                                    HealthCheck.Update(true, "ExportIHSData");
                                }
                                catch (Exception ex)
                                {
                                    HealthCheck.Update(false, "ExportIHSData", null, ex);
                                }
                                break;
                            case "--UpdateCWKHistory"://Vikas
                                try
                                {
                                    HR.UpdateCWKHistory();
                                    HealthCheck.Update(true, "UpdateCWKHistory");
                                }
                                catch (Exception ex)
                                {
                                    HealthCheck.Update(false, "UpdateCWKHistory", null, ex);
                                }
                                break;
                            case "--UpdateOLMTrainingCourse"://Vikas
                                try
                                {
                                    HR.UpdateOLMTrainingCourse();
                                    HealthCheck.Update(true, "UpdateOLMTrainingCourse");
                                }
                                catch (Exception ex)
                                {
                                    HealthCheck.Update(false, "UpdateOLMTrainingCourse", null, ex);
                                }
                                break;
                            case "--UpdateAddTraining"://Vikas
                                try
                                {
                                    HR.UpdateAddTraining();
                                    HealthCheck.Update(true, "UpdateAddTraining");
                                }
                                catch (Exception ex)
                                {
                                    HealthCheck.Update(false, "UpdateAddTraining", null, ex);
                                }
                                break;
                            case "--UpdatePeopleDetails"://Vikas
                                try
                                {
                                    HR.UpdatePeopleDetails();
                                    HealthCheck.Update(true, "UpdatePeopleDetails");
                                }
                                catch (Exception ex)
                                {
                                    HealthCheck.Update(false, "UpdatePeopleDetails", null, ex);
                                }
                                break;
                            case "--UpdateENRTraining"://Vikas
                                try
                                {
                                    HR.UpdateENRTraining();
                                    HealthCheck.Update(true, "UpdateENRTraining");
                                }
                                catch (Exception ex)
                                {
                                    HealthCheck.Update(false, "UpdateENRTraining", null, ex);
                                }
                                break;
                            case "--UpdateCSMSdbFromEbi": //Vikas
                                if (args.Length == 2)
                                {
                                    if (args[1].ToString() == "live")
                                    {
                                        try
                                        {
                                            int EbiId = GetLastEbiID();
                                            EBI.Statistics.UpdateCSMSdbFromEbi();
                                            EBI.Statistics.UpdateCSMSEBICompaniesMap(EbiId);
                                            EBIHoursWorked.UpdateHoursWorkedTable(EbiId, true);
                                            EBI.Statistics.UpdateCSMSEbiHoursWorkedDump();
                                            EBI.Statistics.UpdateFatigueManagementTable(true);
                                            HealthCheck.Update(true, "UpdateCSMSdbFromEbi live");
                                        }
                                        catch (Exception ex)
                                        {
                                            HealthCheck.Update(false, "UpdateCSMSdbFromEbi live", null, ex);
                                        }
                                    }
                                }
                                else
                                {
                                    try
                                    {
                                        EBI.Statistics.UpdateCSMSdbFromEbi();
                                        HealthCheck.Update(true, "UpdateCSMSdbFromEbi");
                                    }
                                    catch (Exception ex)
                                    {
                                        HealthCheck.Update(false, "UpdateCSMSdbFromEbi", null, ex);
                                    }
                                }
                                break;
                            case "--UpdateCSMSEBICompaniesMap":
                                //DT259 2/10/15 Cindi Thornton - add try/catch and HealthCheck.Update calls
                                try
                                {
                                    EBI.Statistics.UpdateCSMSEBICompaniesMap(0);
                                    HealthCheck.Update(true, "UpdateCSMSEBICompaniesMap");
                                }
                                catch (Exception ex)
                                {
                                    HealthCheck.Update(false, "UpdateCSMSEBICompaniesMap", null, ex);
                                }
                                break;
                            case "--UpdateCSMSEbiHoursWorkedDump":
                                //DT259 2/10/15 Cindi Thornton - add try/catch and HealthCheck.Update calls
                                try
                                {
                                    EBI.Statistics.UpdateCSMSEbiHoursWorkedDump();
                                    HealthCheck.Update(true, "UpdateCSMSEbiHoursWorkedDump");
                                }
                                catch (Exception ex)
                                {
                                    HealthCheck.Update(false, "UpdateCSMSEbiHoursWorkedDump", null, ex);
                                }
                                break;
                            case "--UpdateTrainingManagementValidity":
                                try
                                {
                                    Safety.IHS.UpdateTraining_Management_Validity();
                                    HealthCheck.Update(true, "UpdateTrainingManagementValidity");
                                }
                                catch (Exception ex)
                                {
                                    HealthCheck.Update(false, "UpdateTrainingManagementValidity", null, ex);
                                }
                                break;
                            case "--UpdateItem21DashBoardTables":
                                try
                                {
                                    if (args.Length == 2)
                                    {
                                        if (args[1].ToString() == "live")
                                        {
                                            Dashboard.UpdateItem21DashBoardTables("live");
                                        }
                                        else if (args[1].ToString() == "all")
                                        {
                                            Dashboard.UpdateItem21DashBoardTables("all");
                                        }
                                        HealthCheck.Update(true, "UpdateItem21DashBoardTables");
                                    }
                                }
                                catch (Exception ex)
                                {
                                    HealthCheck.Update(false, "UpdateItem21DashBoardTables", null, ex);
                                }
                                break;
                            case "--UpdateEbiSource": //Vikas
                                try
                                {
                                    if (args.Length < 2) throw new Exception("Please provide an year");
                                    if (args.Length == 2)
                                        EBI.Statistics.UpdateEbiSource(Convert.ToInt32(args[1].ToString()));
                                    HealthCheck.Update(true, "UpdateEbiSource");
                                }
                                catch (Exception ex)
                                {
                                    HealthCheck.Update(false, "UpdateEbiSource", null, ex);
                                }
                                break;
                            case "--UpdateEbiSource1"://Vikas
                                try
                                {
                                    if (args.Length < 3) throw new Exception("Please provide an year and month");
                                    if (args.Length == 3)
                                    {
                                        EBI.Statistics.UpdateEbiSource1(Convert.ToInt32(args[1].ToString()), Convert.ToInt32(args[2].ToString()));
                                        HealthCheck.Update(true, "UpdateEbiSourceHistory");
                                    }
                                }
                                catch (Exception ex)
                                {
                                    HealthCheck.Update(false, "UpdateEbiSourceHistory", null, ex);
                                }
                                break;
                            case "--UpdateEbiHoursWorked": //Vikas
                                try
                                {
                                    EBIHoursWorked.UpdateHoursWorkedTable(0, false);
                                    HealthCheck.Update(true, "UpdateEbiHoursWorked");
                                }
                                catch (Exception ex)
                                {
                                    HealthCheck.Update(false, "UpdateEbiHoursWorked", null, ex);
                                }
                                break;
                            case "--TestEbiMM":
                                //DT259 2/10/15 Cindi Thornton - add try/catch and HealthCheck.Update calls
                                try
                                {
                                    EBIHoursWorked.CalculateData(96949);
                                    HealthCheck.Update(true, "TestEbiMM");
                                }
                                catch (Exception ex)
                                {
                                    HealthCheck.Update(false, "TestEbiMM", null, ex);
                                }
                                break;
                            case "--Test":
                                Test();
                                break;
                            case "--help":
                                help();
                                break;
                            case "/?":
                                help();
                                break;
                            default:
                                break;
                        }
                    }
                    catch (FormatException)
                    {
                        invalidArguments();
                    }
                }
            }
            catch (Exception ex)
            {
                Log4NetService.LogInfo("[ERROR] " + ex.Message.ToString());
            }
            DateTime stopTime1 = DateTime.Now;
            TimeSpan duration1 = stopTime1 - startTime1;
            Log4NetService.LogInfo("\nProgram Finished at: " + stopTime1.ToLongTimeString());
            if (duration1.TotalSeconds > 60)
            {
                if (duration1.TotalMinutes > 60)
                {
                    Log4NetService.LogInfo(String.Format("\nExecution Time: {0}h {1}m {2}s", duration1.Hours, duration1.Minutes, duration1.Seconds));
                }
                else
                {
                    Log4NetService.LogInfo(String.Format("\nExecution Time: {0}m {1}s", duration1.Minutes, duration1.Seconds));
                }
            }
            else
            {
                if (duration1.Seconds > 0)
                {
                    Log4NetService.LogInfo(String.Format("\nExecution Time: {0}s", duration1.TotalSeconds));
                }
                else
                {
                    Log4NetService.LogInfo(String.Format("\nExecution Time: {0}ms", duration1.TotalMilliseconds));
                }
            }
            Log4NetService.LogInfo("\n");

            //Cindi Thornton 09/10/2015 - change return 1 to use exit code. 1 = fail in windows scheduled tasks
            return Environment.ExitCode;

        }

        //DT418 4/12/15 Cindi Thornton - new function to handle calculating radar for a single month
        private static void AdHocRadar_Month(string[] args, StringBuilder errorlist)
        {
            if (args[1].ToString() == "Month")
            {
                int year, month;
                bool isYearInt = Int32.TryParse(args[2].ToString(), out year);
                bool isMonthInt = Int32.TryParse(args[3].ToString(), out month);

                //DT419 - pass done to Adhoc Radar methods whether this is for the current data or we're refreshing older data
                bool isBackDatedMonth = true;
                if (year == 9999 && month == 9999)
                    isBackDatedMonth = false;

                if (isYearInt && isMonthInt && (month == 9999 || month >= 1 && month <= 12))
                {
                    //if we've been passed in 9999, set to previous month's month & year
                    if (month == 9999)
                        month = DateTime.Now.AddMonths(-1).Month;

                    if (year == 9999)
                        if (month == 12) //if we've gone back a month, we also need to go back to last year.
                            year = DateTime.Now.Year - 1;
                        else
                            year = DateTime.Now.Year;

                    try
                    {
                        common.HealthCheck.ResetJobSchedule("@UpdateAdHocRadarMonth");
                        Update_AdHoc_Radar(year, month, isBackDatedMonth, errorlist);

                        if (errorlist.Length == 0) //if something failed, fail the job
                            HealthCheck.Update(true, "UpdateAdHocRadar month");
                        else
                            HealthCheck.Update(false, "UpdateAdHocRadar month", errorlist.ToString());
                    }
                    catch (Exception ex)
                    {
                        HealthCheck.Update(false, "UpdateAdHocRadar month", errorlist.ToString(), ex);
                    }
                }
                else
                {
                    throw new Exception("--UpdateAdHocRadar Month : Unexpected Argument. Not a Valid Year or Month.");
                }
            }
            else
            {
                throw new Exception("--UpdateAdHocRadar Month : Unexpected Argument. Expected Month <year> <month>");
            }
        }

        private static void UpdateIhs()
        {
            try
            {
                common.HealthCheck.ResetJobSchedule("@EHSIMSDataImport_@IHSDataImport"); //DT259 2/10/15 Cindi Thornton - used to send emails if this hasn't run on schedule
                Safety.All.UpdateDb("Ihs");
                HealthCheck.Update(true, "UpdateDbFromIhs");
            }
            catch (Exception ex)
            {
                HealthCheck.Update(false, "UpdateDbFromIhs", null, ex);
            }
        }

        static void UpdateCRP()
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(sqldb))
                {
                    using (SqlCommand cmd = new SqlCommand("CompanyCRP_Insert", conn))
                    {
                        Log4NetService.LogInfo("Updating company CRP table...");
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandTimeout = 120000;
                        conn.Open();
                        cmd.ExecuteNonQuery();
                        conn.Close();
                        Log4NetService.LogInfo("Successfully updated company CRP table");
                    }
                }
            }
            catch (Exception ex)
            {
                Log4NetService.LogInfo("Error occurred while updating company CRP table: " + ex.Message);
            }
        }

        static void FromIhsToCSV(string myFile)
        {
            string saveFolder = "";
            string fileName = "";
            var cfService = new KaiZen.CSMS.Services.ConfigService();
            Config cng = cfService.GetByKey("IHSMappingDir");
            saveFolder = cng.Value;
            fileName = String.Format("IHS Entity Change Request Template_NEW.csv");

            if (String.IsNullOrEmpty(saveFolder) || String.IsNullOrEmpty(fileName))
            {
                throw new Exception("Expected folder name or filename");
            }

            string csvHeaders = "Action,Current Entity,Target Entity,Entity Type,Current Parent Entity,Target Parent Entity";

            TextWriter tw = new StreamWriter(saveFolder + fileName, false);
            tw.WriteLine(csvHeaders);

            using (SqlConnection cn = new SqlConnection(sqldb))
            {
                cn.Open();
                Log4NetService.LogInfo("Reading IHS Mapping Table");
                SqlCommand cmd = new SqlCommand("Select A.ContCompanyCode,A.ABN,(Select Top 1 SiteNameIhs from Sites B where A.SiteId=B.SiteId) As SiteNameIhs from CompaniesEhsimsMap A", cn);
                //DT Batch Failing : Changed the CommandTimeout to 60000(1m)
                cmd.CommandTimeout = 60000;
                SqlDataReader r = cmd.ExecuteReader();
                Log4NetService.LogInfo("Read...");
                int rowCnt = 0;
                bool found;
                while (r.Read())
                {
                    rowCnt++;
                    try
                    {
                        string CompanyCode = (string)r["ContCompanyCode"];
                        string SiteNameIhs = (string)r["SiteNameIhs"];
                        string ABN = (string)r["ABN"];
                        string itemDisplay = string.Empty;
                        found = false;

                        if (CompanyCode.Length < 3)
                        {
                            if (CompanyCode.Length == 1)
                                CompanyCode = "00" + CompanyCode;
                            if (CompanyCode.Length == 2)
                                CompanyCode = "0" + CompanyCode;
                        }

                        //get ihs company entity value from csv file
                        using (CsvReader csv = new CsvReader(new StringReader(myFile), true))
                        {
                            //int i = 0;
                            while (csv.ReadNextRecord())
                            {
                                string SiteNameIhsCSV = csv["Site (Lookup Only)"].ToString();
                                string ContCompanyCodeCSV = csv["Cont Company Code"].ToString();

                                if (SiteNameIhs == SiteNameIhsCSV && CompanyCode == ContCompanyCodeCSV)
                                {
                                    itemDisplay = csv["Item Display"].ToString();
                                    found = true;

                                }
                            }
                        }

                        string Action = "Rename";
                        string Current_Entity = itemDisplay;
                        string fromReplace = "(" + CompanyCode + ")";
                        //string toReplace = "(" + ABN + ")";
                        string toReplace = "(" + CompanyCode + " " + ABN + ")";
                        string Target_Entity = itemDisplay.Replace(fromReplace, toReplace);
                        string Entity_Type = "SRU";
                        string Current_Parent_Entity = "";
                        string Target_Parent_Entity = "";
                        if (found)
                        {
                            tw.WriteLine("\"" + Action + "\",\"" + Current_Entity + "\",\"" + Target_Entity + "\",\"" + Entity_Type + "\",\"" + Current_Parent_Entity + "\",\"" + Target_Parent_Entity + "\"");
                        }
                        if (!found)
                        {
                            Log4NetService.LogInfo("The data for SiteNameIhs=" + SiteNameIhs + " CompanyCode=" + CompanyCode + " was not found in AI Contractor's List");
                        }
                    }
                    catch (Exception ex)
                    {
                        Log4NetService.LogInfo("Error In Row " + rowCnt + ": " + ex.Message);
                    }
                }
                Log4NetService.LogInfo("Finished.\n\n");

                cn.Close();
            }
            tw.Close();
        }

        static void ExportIHSDataToCSV()
        {
            try
            {
                Configuration configuration = new Configuration();
                string dir = configuration.GetValue(ConfigList.AIContractorListFilePath);
                string myFile = "";
                FileStream logFileStream = new FileStream(dir + "AI-Contractor List.csv", FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                StreamReader logFileReader = new StreamReader(logFileStream);

                bool cont = false;
                while (!logFileReader.EndOfStream)
                {
                    string line = logFileReader.ReadLine();
                    if (!cont)
                    {
                        if (line.Contains(@"Site (Lookup Only)")) cont = true;
                    }

                    if (cont)
                    {
                        myFile += line + "\n";
                    }
                }

                FromIhsToCSV(myFile);
                logFileReader.Close();
                logFileStream.Close();
            }
            catch (Exception ex)
            {
                Log4NetService.LogInfo(ex.Message.ToString() + "\nServer Configuration Account Error. Contact Administrator.");
            }
            finally
            {
            }
        }

        //Added by Vikas for item15
        /// <summary>
        /// Added for updating ABN column in IHS Mapping Table
        /// </summary>
        /// <param name=""></param>
        static void UpdateIHSMappingTable()
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(sqldb))
                {
                    using (SqlCommand cmd = new SqlCommand("IHS_UpdateABN", conn))
                    {
                        Log4NetService.LogInfo("Updating IHS Mapping Table...");
                        cmd.CommandType = CommandType.StoredProcedure;
                        SqlParameter message = new SqlParameter("@Message", SqlDbType.VarChar);
                        message.Size = 50;
                        message.Direction = ParameterDirection.Output; // This is important!
                        cmd.Parameters.Add(message);
                        conn.Open();
                        cmd.ExecuteNonQuery();
                        conn.Close();
                        Log4NetService.LogInfo("Successfully updated IHS Mapping Table");
                        if (((string)cmd.Parameters["@Message"].Value) != "")
                            Log4NetService.LogInfo("Successfully updated IHS Mapping Table but the following CompanyId(s) were not found in Companies table:" + cmd.Parameters["@Message"].Value);
                    }
                }
            }
            catch (Exception ex)
            {
                Log4NetService.LogInfo("Error occurred while updating IHS Mapping table: " + ex.Message);
            }
        }

        //Added by Vikas on 17/11 for item#15
        static void UpdateCompanyVendorTable()
        {
            int rows;
            string activity = "UpdateCompanyVendorInformation";

            try
            {
                using (SqlConnection conn = new SqlConnection(sqldb))
                {
                    using (SqlCommand cmd = new SqlCommand("GetLatestCompanyVendorRows", conn))
                    {
                        Log4NetService.LogInfo("Updating CompanyVendor Table...");
                        cmd.CommandType = CommandType.StoredProcedure;
                        conn.Open();
                        rows = cmd.ExecuteNonQuery();
                        conn.Close();
                    }
                }
                if (rows > 0)
                {
                    Log4NetService.LogInfo("Successfully updated CompanyVendor Table");
                    HealthCheck.Update(true, activity);
                }
            }
            catch (Exception ex)
            {
                Log4NetService.LogInfo("Error occurred while updating CompanyVendor table: " + ex.Message);
                HealthCheck.Update(false, activity, "Exception: " + ex.Message);
            }
        }

        public static void UpdateCompanyVendor()
        {
            bool PollingdateUpdate = true;
            try
            {
                string pollingdate = string.Empty;
                try
                {
                    using (SqlConnection conn = new SqlConnection(sqldb))
                    {
                        using (SqlCommand cmd = new SqlCommand("Select Convert(varchar(30),PollingDateTime,120) from Pollingtable WHERE Description='CompanyVendor'", conn))
                        {
                            //DT 3127 Changes
                            Log4NetService.LogInfo("Fetching information from Polling Table...");
                            conn.Open();
                            object objpollingDate = cmd.ExecuteScalar();
                            pollingdate = Convert.ToString(objpollingDate);
                            conn.Close();
                            Log4NetService.LogInfo("Polling datetime for CompanyVendor=" + pollingdate);
                        }
                    }
                }
                catch (Exception ex)
                {
                    //DT 3127 Changes
                    PollingdateUpdate = false;
                    Log4NetService.LogInfo("Error occurred while fetching information from Polling table(POLLINGTABLE) for CompanyVendor: " + ex.Message);
                    throw;
                }
                try
                {
                    using (SqlConnection conn = new SqlConnection(sqldb))
                    {
                        using (SqlCommand cmd = new SqlCommand("Delete from CompanyVendor where Vendor_ID NOT IN (Select Vendor_Id from EBS_COMM..APPS.XXPO_SQL_VEND_V)", conn))
                        {
                            //DT 3127 Changes
                            Log4NetService.LogInfo("Deleting unwanted rows from COMPANYVENDOR,those are not present in EBS Commercial link");

                            cmd.CommandTimeout = 60000;
                            conn.Open();
                            cmd.ExecuteNonQuery();
                            conn.Close();
                        }
                    }
                }
                catch (Exception ex)
                {
                    //DT 3127 Changes
                    PollingdateUpdate = false;
                    Log4NetService.LogInfo("Error occurred while Deleting unwanted rows from COMPANYVENDOR,those are not present in EBS Commercial link: " + ex.Message);
                    throw;
                }

                //DT 3127 Changes
                DataSet FulDs = new DataSet();
                DataTable FulDt = new DataTable();
                try
                {
                    using (SqlConnection conn = new SqlConnection(sqldb))
                    {
                        //DT 3127 Changes
                        // Last 7 days record should be picked ie (pollingdate-7 days)
                        using (SqlCommand cmd = new SqlCommand("Select * from EBS_COMM..APPS.XXPO_SQL_VEND_V WHERE LAST_UPDATE_DATE > (CAST('" + pollingdate + "' AS DATETIME)-7)", conn))
                        {
                            Log4NetService.LogInfo("Reading information(last 7 days from Polling Date) from Vendor Information Link...");
                            string cmdString = cmd.CommandText;
                            conn.Open();
                            cmd.CommandTimeout = 60000;

                            //DT 3127 Changes
                            SqlDataAdapter sqlfulda = new SqlDataAdapter(cmd);
                            sqlfulda.Fill(FulDs);
                            conn.Close();
                            Log4NetService.LogInfo("Finished reading from DB Link");
                            Log4NetService.LogInfo("Start updating CompanyVendor table...");
                            FulDt = FulDs.Tables[0];

                            foreach (DataRow dr in FulDt.Rows)
                            {
                                try
                                {
                                    try
                                    {
                                        using (SqlConnection conn1 = new SqlConnection(sqldb))
                                        {
                                            using (SqlCommand cmd1 = new SqlCommand("UpdateCompanyVendor", conn1))
                                            {
                                                cmd1.CommandType = CommandType.StoredProcedure;
                                                cmd1.Parameters.Add(new SqlParameter("@VENDOR_ID", dr["VENDOR_ID"]));
                                                cmd1.Parameters.Add(new SqlParameter("@VENDOR_NUMBER", dr["VENDOR_NUMBER"]));
                                                cmd1.Parameters.Add(new SqlParameter("@VENDOR_NAME", dr["VENDOR_NAME"]));
                                                cmd1.Parameters.Add(new SqlParameter("@VENDOR_NAME_ALT", dr["VENDOR_NAME_ALT"]));
                                                cmd1.Parameters.Add(new SqlParameter("@LAST_UPDATE_DATE", dr["LAST_UPDATE_DATE"]));
                                                cmd1.Parameters.Add(new SqlParameter("@START_DATE_ACTIVE", dr["START_DATE_ACTIVE"]));
                                                cmd1.Parameters.Add(new SqlParameter("@END_DATE_ACTIVE", dr["END_DATE_ACTIVE"]));
                                                cmd1.Parameters.Add(new SqlParameter("@TAX_REGISTRATION_NUMBER", dr["TAX_REGISTRATION_NUMBER"]));

                                                conn1.Open();
                                                cmd1.ExecuteNonQuery();
                                                conn1.Close();
                                            }
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        //DT 3127 Changes
                                        PollingdateUpdate = false;
                                        Log4NetService.LogInfo("Error occurred while updating COMPANYVENDOR table for Vendor_ID" + dr["Vendor_ID"] + " " + ex.Message);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    //DT 3127 Changes
                                    PollingdateUpdate = false;
                                    Log4NetService.LogInfo("Error In Row " + ex.Message);
                                }
                            }
                            Log4NetService.LogInfo("Finished updating CompanyVendor table");
                        }
                    }
                }
                catch (Exception ex)
                {
                    //DT 3127 Changes
                    PollingdateUpdate = false;
                    Log4NetService.LogInfo("Error occurred : " + ex.Message);
                }
                try
                {
                    using (SqlConnection conn = new SqlConnection(sqldb))
                    {
                        //DT 3127 Changes
                        if (PollingdateUpdate == true)
                        {
                            using (SqlCommand cmd = new SqlCommand("Update Pollingtable SET PollingDateTime=getDate() where Description='CompanyVendor'", conn))
                            {
                                Log4NetService.LogInfo("Updating Polling Table...");

                                conn.Open();
                                cmd.ExecuteNonQuery();
                                //DT 3127 Changes
                                cmd.CommandText = "Select Convert(varchar(30),PollingDateTime,120) from Pollingtable WHERE Description='CompanyVendor'";
                                object NewVndpollingDate = cmd.ExecuteScalar();
                                conn.Close();
                                Log4NetService.LogInfo("New Polling datetime will be set as :" + Convert.ToString(NewVndpollingDate));

                                Log4NetService.LogInfo("Updated Polling Table");
                            }
                        }
                        else
                        {
                            Log4NetService.LogInfo("Polling Table Not Updated");
                        }
                    }
                }
                catch (Exception ex)
                {
                    //DT 3127 Changes
                    Log4NetService.LogInfo("Error occurred while updating Polling Table for CompanyVendor" + ex.Message.ToString());
                    throw;
                }
            }
            catch (Exception ex)
            {
                Log4NetService.LogInfo(ex.Message.ToString() + "\nServer Configuration Account Error. Contact Administrator.");
            }
            finally
            {
            }
        }

        /// <summary>
        /// Displays information about the application version, build time and author.
        /// </summary>
        static void header()
        {
            Version MyVersion = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version;
            DateTime MyTime = new DateTime(2000, 1, 1).AddDays(MyVersion.Build).AddSeconds(MyVersion.Revision * 2);

            Log4NetService.LogInfo("");
            Log4NetService.LogInfo("---------------------------------------------------------------");
            Log4NetService.LogInfo("Alcoa Contractor Services Management System - Batch Job Scripts");
            Log4NetService.LogInfo(String.Format("v0.96 - Build Date: {0:dd/MM/yyyy} {1}", MyTime.Date, MyTime.TimeOfDay));
            Log4NetService.LogInfo("---------------------------------------------------------------\n");
        }

        /// <summary>
        /// Displays the possible usage paramaters
        /// </summary>
        static void help()
        {
            Log4NetService.LogInfo("Usage: ALCOA.CSMS.BatchJobScripts <Argument>");
            Log4NetService.LogInfo("         --UpdateDbFromEhsims");
            Log4NetService.LogInfo("         --UpdateRs");
            Log4NetService.LogInfo("         --SendSqExpiryReminderEmails");
            Log4NetService.LogInfo("         --TestEhsimsCodes");
            Log4NetService.LogInfo("         --TestGetCompaniesStatus");
            Log4NetService.LogInfo("         --Add Companies <CompanyName>");
            Log4NetService.LogInfo("         --SendEmail <Email Address>");
            Log4NetService.LogInfo("         --ListSqExpiryReminders");
            Log4NetService.LogInfo("         --UpdateDbFromEbi <All|MostRecent>");
            Log4NetService.LogInfo("               All - All Files");
            Log4NetService.LogInfo("               MostRecent - Yesterday's file");
            Log4NetService.LogInfo("");
            Log4NetService.LogInfo("    Example:  ALCOA.CSMS.BatchJobScripts --UpdateDbFromEhsims");
        }

        /// <summary>
        /// Executes when invalid argument sent to application
        /// </summary>
        static void invalidArguments()
        {
            Log4NetService.LogInfo("Invalid Command");
        }

        /// <summary>
        /// Tests the get companies status.
        /// </summary>
        static void TestGetCompaniesStatus()
        {
            try
            {
                using (FileStream logFileStream = new FileStream(@"C:\temp\check.txt", FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    StreamReader logFileReader = new StreamReader(logFileStream);
                    while (!logFileReader.EndOfStream)
                        try
                        {
                            string line = logFileReader.ReadLine();
                            using (SqlConnection cn = new SqlConnection("Data Source=AUABGN-SQLPROD;Initial Catalog=ALCOA_WAO_CSMWP;Persist Security Info=True;User ID=WAO_CSMWP;Password=secure"))
                            {
                                cn.Open();
                                SqlCommand cmd = new SqlCommand(String.Format("SELECT CompanyStatusName From Companies INNER JOIN CompanyStatus on Companies.CompanyStatusId = CompanyStatus.CompanyStatusId WHERE CompanyName = '{0}'", line), cn);
                                SqlDataReader rdr = cmd.ExecuteReader();
                                rdr.Read();
                                try
                                {
                                    if (!String.IsNullOrEmpty(rdr[0].ToString()))
                                        Log4NetService.LogInfo(rdr[0].ToString());
                                    else
                                        Log4NetService.LogInfo("-2");
                                }
                                catch (Exception)
                                {
                                    Log4NetService.LogInfo("-1");
                                }
                                cn.Close();
                            }
                        }
                        catch (Exception ex)
                        {
                            Log4NetService.LogInfo("Error: " + ex.Message.ToString());
                        }
                    logFileReader.Close();
                    logFileStream.Close();
                }
            }
            catch (Exception ex)
            {
                Log4NetService.LogInfo(ex.Message);
            }
        }

        /// <summary>
        /// Updates the contract manager.
        /// </summary>
        /// <param name="ProcurementManager">The procurement manager.</param>
        /// <param name="ContractManager">The contract manager.</param>
        static void UpdateContractManager(string ProcurementManager, string ContractManager)
        {
            int qCnt = 0;
            try
            {
                Log4NetService.LogInfo("Checking...");

                UsersService uService = new UsersService();
                TList<Users> uList = uService.GetAll();

                Users uProcurement = null;
                Users uContractManager = null;

                foreach (Users u in uList)
                {
                    string fullname = String.Format("{0}, {1}", u.LastName, u.FirstName);
                    if (fullname.ToLower() == ProcurementManager.ToLower())
                    {
                        uProcurement = u;
                    }
                    if (fullname.ToLower() == ContractManager.ToLower())
                    {
                        uContractManager = u;
                    }
                }

                string uProcurementManagerFound = "(Not Found)";
                string uContractManagerFound = "(Not Found)";
                if (uProcurement != null) uProcurementManagerFound = String.Format("{0} - {1}, {2}", uProcurement.UserId, uProcurement.LastName, uProcurement.FirstName);
                if (uContractManager != null) uContractManagerFound = String.Format("{0} - {1}, {2}", uContractManager.UserId, uContractManager.LastName, uContractManager.FirstName);

                Log4NetService.LogInfo("Procurement Contact: " + uProcurementManagerFound);
                Log4NetService.LogInfo(String.Format("Procurement Functional Supervisor: {0}\n", uContractManagerFound));

                if (uProcurement != null & uContractManager != null)
                {
                    var qirService = new KaiZen.CSMS.Services.QuestionnaireInitialResponseService();
                    TList<QuestionnaireInitialResponse> qirList = qirService.GetAll();

                    foreach (QuestionnaireInitialResponse qir in qirList)
                    {
                        if (qir.QuestionId == "2" & qir.AnswerText == uProcurement.UserId.ToString())
                        {
                            qCnt++;
                            QuestionnaireInitialResponse qirContractManager = qirService.GetByQuestionnaireIdQuestionId(qir.QuestionnaireId, "2_1");
                            qirContractManager.AnswerText = uContractManager.UserId.ToString();
                            Log4NetService.LogInfo("Updating QuestionnaireId: " + qir.QuestionnaireId);
                            qirService.Update(qirContractManager);
                        }
                    }
                    Log4NetService.LogInfo(String.Format("\nUpdated ({0}) Questionnaires.", qCnt));
                }
                else
                {
                    Log4NetService.LogInfo("\nAborted.");
                }
            }
            catch (Exception ex)
            {
                Log4NetService.LogInfo(ex.Message.ToString());
            }
        }

        static void UpdateKpiProjectsToNewTable()
        {
            var kpiService = new KaiZen.CSMS.Services.KpiService();
            TList<Kpi> kpiList = kpiService.GetAll();
            KpiProjectsService kpiProjectsService = new KpiProjectsService();

            foreach (Kpi kpi in kpiList)
            {
                //Project 1
                if (!String.IsNullOrEmpty(kpi.ProjectCapital1Title) && kpi.ProjectCapital1Hours != null)
                {
                    using (KpiProjects kpiProjects = new KpiProjects())
                    {
                        kpiProjects.KpiId = kpi.KpiId;
                        kpiProjects.ProjectTitle = kpi.ProjectCapital1Title;
                        kpiProjects.ProjectHours = Convert.ToInt32(kpi.ProjectCapital1Hours);
                        kpiProjectsService.Insert(kpiProjects);
                        Log4NetService.LogInfo("Wrote: [" + kpi.KpiId + "] " + kpiProjects.ProjectTitle + " - " + kpiProjects.ProjectHours.ToString());
                    }
                }
                //Project 2
                if (!String.IsNullOrEmpty(kpi.ProjectCapital2Title) && kpi.ProjectCapital2Hours != null)
                {
                    using (KpiProjects kpiProjects = new KpiProjects())
                    {
                        kpiProjects.KpiId = kpi.KpiId;
                        kpiProjects.ProjectTitle = kpi.ProjectCapital2Title;
                        kpiProjects.ProjectHours = Convert.ToInt32(kpi.ProjectCapital2Hours);
                        kpiProjectsService.Insert(kpiProjects);
                        Log4NetService.LogInfo("Wrote: [" + kpi.KpiId + "] " + kpiProjects.ProjectTitle + " - " + kpiProjects.ProjectHours.ToString());
                    }
                }
                //Project 3
                if (!String.IsNullOrEmpty(kpi.ProjectCapital3Title) && kpi.ProjectCapital3Hours != null)
                {
                    using (KpiProjects kpiProjects = new KpiProjects())
                    {
                        kpiProjects.KpiId = kpi.KpiId;
                        kpiProjects.ProjectTitle = kpi.ProjectCapital3Title;
                        kpiProjects.ProjectHours = Convert.ToInt32(kpi.ProjectCapital3Hours);
                        kpiProjectsService.Insert(kpiProjects);
                        Log4NetService.LogInfo("Wrote: [" + kpi.KpiId + "] " + kpiProjects.ProjectTitle + " - " + kpiProjects.ProjectHours.ToString());
                    }
                }
                //Project 4
                if (!String.IsNullOrEmpty(kpi.ProjectCapital4Title) && kpi.ProjectCapital4Hours != null)
                {
                    using (KpiProjects kpiProjects = new KpiProjects())
                    {
                        kpiProjects.KpiId = kpi.KpiId;
                        kpiProjects.ProjectTitle = kpi.ProjectCapital4Title;
                        kpiProjects.ProjectHours = Convert.ToInt32(kpi.ProjectCapital4Hours);
                        kpiProjectsService.Insert(kpiProjects);
                        Log4NetService.LogInfo("Wrote: [" + kpi.KpiId + "] " + kpiProjects.ProjectTitle + " - " + kpiProjects.ProjectHours.ToString());
                    }
                }
                //Project 5
                if (!String.IsNullOrEmpty(kpi.ProjectCapital5Title) && kpi.ProjectCapital5Hours != null)
                {
                    using (KpiProjects kpiProjects = new KpiProjects())
                    {
                        kpiProjects.KpiId = kpi.KpiId;
                        kpiProjects.ProjectTitle = kpi.ProjectCapital5Title;
                        kpiProjects.ProjectHours = Convert.ToInt32(kpi.ProjectCapital5Hours);
                        kpiProjectsService.Insert(kpiProjects);
                        Log4NetService.LogInfo("Wrote: [" + kpi.KpiId + "] " + kpiProjects.ProjectTitle + " - " + kpiProjects.ProjectHours.ToString());
                    }
                }
            }
        }

        static void GenerateContractorHoursInputIaFile(bool lastmonth)
        {
            try
            {
                Configuration configuration = new Configuration();
                string dir = configuration.GetValue(ConfigList.AIContractorListFilePath);
                string dir2 = dir + @"AI Import Files\";

                if (lastmonth)
                {
                    DateTime dtLastMonth = DateTime.Now.AddMonths(-1);
                    dir2 += String.Format(@"month\{0}-{1}\", dtLastMonth.Year, dtLastMonth.Month);

                    //check if directory exists. if not, create
                    if (!Directory.Exists(dir2))
                    {
                        Directory.CreateDirectory(dir2);
                    }
                }

                string myFile = "";
                DataTable dsAiClist = new DataTable();
                dsAiClist.Columns.Add("Site", typeof(string));
                dsAiClist.Columns.Add("ABN", typeof(string)); //Cont Company Code
                dsAiClist.Columns.Add("CompanyName", typeof(string));
                dsAiClist.Columns.Add("Entity", typeof(string)); //Item Display

                try
                {
                    FileStream logFileStream = new FileStream(dir + "AI-Contractor List.csv", FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                    StreamReader logFileReader = new StreamReader(logFileStream);

                    bool cont = false;
                    while (!logFileReader.EndOfStream)
                    {
                        string line = logFileReader.ReadLine();

                        if (!cont)
                        {
                            if (line.Contains(@"Site (Lookup Only)")) cont = true;
                        }

                        if (cont)
                        {
                            myFile += line + "\n";
                        }
                    }
                    using (CsvReader csv = new CsvReader(new StringReader(myFile), true))
                    {
                        while (csv.ReadNextRecord())
                        {
                            DataRow drNew = dsAiClist.NewRow();
                            drNew["Site"] = csv["Site (Lookup Only)"].ToString();
                            drNew["ABN"] = csv["Cont Company Code"].ToString();
                            drNew["CompanyName"] = csv["Cont Company Name"].ToString();
                            drNew["Entity"] = csv["Item Display"].ToString();

                            dsAiClist.Rows.Add(drNew);
                        }
                    }
                    logFileReader.Close();
                    logFileStream.Close();
                }
                catch (Exception ex)
                {
                    Log4NetService.LogInfo(ex.Message);
                }

                File.Delete(dir2 + "Errors.txt");
                TextWriter tw_Errors = new StreamWriter(dir2 + "Errors.txt");

                var kpiService = new KaiZen.CSMS.Services.KpiService();
                SitesService sService = new SitesService();
                TList<Sites> sTlist = sService.GetAll();
                foreach (Sites s in sTlist)
                {
                    if (!String.IsNullOrEmpty(s.SiteNameIhs))
                    {
                        ConnectionStringSettings conString = ConfigurationManager.ConnectionStrings["ALCOA_WAO_CSMWP_devConnectionString"];
                        string strConnString = conString.ConnectionString;

                        Log4NetService.LogInfo("Connecting to CSMS");
                        using (SqlConnection cn = new SqlConnection(strConnString))
                        {
                            cn.Open();

                            SqlCommand cmd = new SqlCommand("Select CompanyId,KpiDateTime,aheaTotalManHours,aheaAvgNopplSiteMonth from Kpi where year(kpidatetime)>=2011 and SiteId=" + s.SiteId, cn);
                            //DT Batch Failing : Changed the CommandTimeout to 60000(1m)
                            cmd.CommandTimeout = 60000;

                            SqlDataReader r = cmd.ExecuteReader();

                            string filename = @s.SiteNameIhs + ".csv";
                            File.Delete(dir2 + filename);
                            TextWriter tw = new StreamWriter(dir2 + filename);
                            tw.WriteLine("Entity, Month, Year, Hours Worked, Number of Employees");

                            while (r.Read())
                            {
                                int CompanyId = Convert.ToInt32(r["CompanyId"]);
                                DateTime KpiDateTime = Convert.ToDateTime(r["KpiDateTime"]);
                                Decimal aheaTotalManHours = Convert.ToDecimal(r["aheaTotalManHours"]);
                                int aheaAvgNopplSiteMonth = Convert.ToInt32(r["aheaAvgNopplSiteMonth"]);

                                string entity = "";
                                string CompanyName = string.Empty;
                                string KPI = KpiDateTime.Month.ToString() + "/" + KpiDateTime.Year.ToString();
                                //get ihs company entity value from csv file

                                //string ABN = csv["ABN"].ToString();
                                string ABN = string.Empty;
                                string searchSite = s.SiteNameIhs;
                                CompaniesService cService = new CompaniesService();
                                if (CompanyId != null)
                                {
                                    Companies c = cService.GetByCompanyId(CompanyId);
                                    if (c != null)
                                    {
                                        ABN = c.CompanyAbn;
                                        CompanyName += c.CompanyName + " (" + c.CompanyAbn + ")";
                                    }
                                }

                                DataRow[] dr = dsAiClist.Select("Site='" + searchSite + "' and ABN='" + ABN + "'");
                                if (dr.Length > 0)
                                    entity = Convert.ToString(dr[0]["Entity"]);

                                if (!String.IsNullOrEmpty(entity))
                                {
                                    int SiteId = s.SiteId;
                                    bool cont = true;

                                    if (lastmonth)
                                    {
                                        DateTime dtLastMonth = DateTime.Now.AddMonths(-1);
                                        if (KpiDateTime.Year != dtLastMonth.Year) cont = false;
                                        if (KpiDateTime.Month != dtLastMonth.Month) cont = false;
                                    }

                                    if (cont)
                                    {
                                        decimal hoursWorked = aheaTotalManHours;
                                        int noEmployees = aheaAvgNopplSiteMonth;
                                        int Month = KpiDateTime.Month;
                                        string MonthName = Month.ToString();

                                        MonthsService mService = new MonthsService();
                                        Months m = mService.GetByMonthId(Month);
                                        if (m != null) MonthName = m.MonthName;

                                        tw.WriteLine(String.Format("{0}, {1}, {2}, {3}, {4}", entity, MonthName, KpiDateTime.Year, hoursWorked, noEmployees));
                                    }
                                }
                                else
                                {
                                    DateTime dtLastMonth = DateTime.Now.AddMonths(-1);
                                    if (lastmonth)
                                    {
                                        if ((KpiDateTime.Year == dtLastMonth.Year) && (KpiDateTime.Month == dtLastMonth.Month))
                                        {
                                            Log4NetService.LogInfo(String.Format("Error: Could not Find Company Entity Name in Contractors List - {0} - {1} - {2} for KPI - {3}", s.SiteNameIhs, s.SiteName, CompanyName, KPI));
                                            tw_Errors.WriteLine(String.Format("Error: Could not Find Company Entity Name in Contractors List - {0} - {1} - {2} for KPI - {3}", s.SiteNameIhs, s.SiteName, CompanyName, KPI));
                                        }
                                    }
                                    else
                                    {
                                        Log4NetService.LogInfo(String.Format("Error: Could not Find Company Entity Name in Contractors List - {0} - {1} - {2} for KPI - {3}", s.SiteNameIhs, s.SiteName, CompanyName, KPI));
                                        tw_Errors.WriteLine(String.Format("Error: Could not Find Company Entity Name in Contractors List - {0} - {1} - {2} for KPI - {3}", s.SiteNameIhs, s.SiteName, CompanyName, KPI));
                                    }
                                }
                            }
                            tw.Close();
                        }
                    }
                }
                tw_Errors.Close();
            }
            catch (Exception ex)
            {
                Log4NetService.LogInfo(ex.Message.ToString() + "\nServer Configuration Account Error. Contact Administrator.");
            }
            finally
            {
            }
        }

        static int GetLastEbiID()
        {
            try
            {
                int EbiId;
                using (SqlConnection cn = new SqlConnection(sqldb))
                {
                    cn.Open();
                    SqlCommand cmd = new SqlCommand("Select Max(EbiId) from Ebi", cn);
                    object objEbiId = cmd.ExecuteScalar();
                    EbiId = Convert.ToInt32(objEbiId);
                    cn.Close();
                }
                return EbiId;
            }
            catch (Exception)
            {
                throw;
            }
        }

        static void PopulateResidentialCategoryFromResidencyTable()
        {
            var rService = new KaiZen.CSMS.Services.ResidentialService();
            TList<Residential> rTlist = rService.GetAll();

            var cscsService = new KaiZen.CSMS.Services.CompanySiteCategoryStandardService();

            foreach (Residential r in rTlist)
            {
                CompanySiteCategoryStandard cscs = new CompanySiteCategoryStandard();
                cscs.CompanyId = r.CompanyId;
                cscs.SiteId = r.SiteId;
                if (r.ResidentialStatus == true)
                {
                    cscs.CompanySiteCategoryId = (int)CompanySiteCategoryList.E;
                }
                else
                {
                    cscs.CompanySiteCategoryId = (int)CompanySiteCategoryList.NE1;
                }

                cscs.ModifiedByUserId = 0;
                cscs.ModifiedDate = DateTime.Now;

                try
                {
                    cscsService.Save(cscs);
                }
                catch (Exception ex)
                {
                    CompaniesService cService = new CompaniesService();
                    Companies c = cService.GetByCompanyId(r.CompanyId);
                    Log4NetService.LogInfo(c.CompanyName + ": " + ex.Message.ToString());
                }
            }
        }
        
        static void PopulateResidentialCategoryFromSqLocationApproval()
        {
            QuestionnaireWithLocationApprovalViewLatestService qService = new QuestionnaireWithLocationApprovalViewLatestService();
            var qilService = new KaiZen.CSMS.Services.QuestionnaireInitialLocationService();
            var cscsService = new KaiZen.CSMS.Services.CompanySiteCategoryStandardService();

            SitesService sService = new SitesService();
            CompaniesService cService = new CompaniesService();
            UsersService uService = new UsersService();

            VList<QuestionnaireWithLocationApprovalViewLatest> qTlist = qService.GetAll();
            foreach (QuestionnaireWithLocationApprovalViewLatest q in qTlist)
            {
                TList<QuestionnaireInitialLocation> qilTlist = qilService.GetByQuestionnaireId(q.QuestionnaireId);

                foreach (QuestionnaireInitialLocation qil in qilTlist)
                {
                    try
                    {
                        string type = "Updating";
                        CompanySiteCategoryStandard cscs = cscsService.GetByCompanyIdSiteId(q.CompanyId, qil.SiteId);
                        if (cscs == null)
                        {
                            cscs = new CompanySiteCategoryStandard();
                            cscs.CompanyId = q.CompanyId;
                            cscs.SiteId = qil.SiteId;
                            type = "Adding";
                        }

                        if (qil.ModifiedDate != null)
                        {
                            cscs.ModifiedDate = qil.ModifiedDate;
                        }
                        else
                        {
                            cscs.ModifiedDate = DateTime.Now;
                        }

                        cscs.ModifiedByUserId = qil.ModifiedByUserId;
                        cscs.LocationSponsorUserId = qil.Spa;
                        cscs.ApprovedByUserId = qil.ApprovedByUserId;
                        cscs.Approved = qil.Approved;

                        string CompanyName = "-";
                        string SiteName = "-";
                        string userModified = "-";
                        string userApproved = "-";
                        string Approved = "-";

                        Companies c = cService.GetByCompanyId(q.CompanyId);
                        CompanyName = c.CompanyName;

                        Sites s = sService.GetBySiteId(qil.SiteId);
                        SiteName = s.SiteName;

                        Users uModified = uService.GetByUserId(qil.ModifiedByUserId);
                        userModified = uModified.FirstName + " " + uModified.LastName;

                        if (qil.ApprovedByUserId != null)
                        {
                            Users uApproved = uService.GetByUserId(Convert.ToInt32(qil.ApprovedByUserId));
                            userApproved = uApproved.FirstName + " " + uApproved.LastName;
                        }

                        if (qil.Approved == true)
                        {
                            Approved = "Yes";
                        }
                        else if (qil.Approved == false)
                        {
                            Approved = "No";
                        }

                        TList<Users> uContractorList = uService.GetByCompanyId(q.CompanyId);
                        int noContractors = 0;
                        foreach (Users uc in uContractorList)
                        {
                            if (uc.RoleId == (int)RoleList.Contractor) noContractors++;
                        }

                        try
                        {
                            cscsService.Save(cscs);
                        }
                        catch (Exception ex)
                        {
                            throw new Exception(CompanyName + " - " + SiteName + ": " + ex.Message);
                        }
                        Log4NetService.LogInfo(String.Format(type + " Entry :: Company [{0}] - Site [{1}] - Approved [{2}] - Approved By [{3}] :: Modified By [{4}] ::  {5}",
                                            CompanyName, SiteName, Approved, userApproved, userModified, noContractors));
                    }
                    catch (Exception ex)
                    {
                        Log4NetService.LogInfo("Error: " + ex.Message);
                    }
                }
            }
        }

        static void Update_AdHoc_Radar(int Year, int Month, bool isBackDatedMonth, StringBuilder errorlist)
        {
            /* DT418 4/12/15 Cindi Thornton - added month parameter. 0 == YTD. Changes are:
            - Only use AdHoc_Radar_Items2 for YTD
             * DT419 15/1/16 Cindi Thornton - added isBackDatedMonth parameter, added Month to SP_Compliance_Table_Batch call
            */
            if (Month != 0) // DT419 - this stored proc is only used to calcualte month data
            {
                var storedProcedureService = DependencyResolver.Current.GetService<Repo.CSMS.Service.Database.IStoredProcedureService>();
                storedProcedureService.SP_Compliance_Table_Batch(Year.ToString(), Month);
            }

            if (Month == 0) //DT418
            {
                using (SqlConnection cn = new SqlConnection(sqldb))
                {
                    cn.Open();
                    SqlCommand cmd = new SqlCommand("DELETE FROM AdHoc_Radar_Items2", cn);
                    cmd.ExecuteNonQuery();
                    cn.Close();
                }
            }

            // Updates AdHoc_Radar_Items (and AdHoc_Radar if applicable) for All Companies - Australia, i.e. WA / VICOPS
            try
            {
                //DT418 4/12/15 Cindi Thornton - added month parameter
                Update_AdHoc_Radar(Year, (int)CompanySiteCategoryList.E, true, Month, isBackDatedMonth, errorlist);
                Update_AdHoc_Radar(Year, (int)CompanySiteCategoryList.NE1, true, Month, isBackDatedMonth, errorlist);
                Update_AdHoc_Radar(Year, (int)CompanySiteCategoryList.NE2, true, Month, isBackDatedMonth, errorlist);
                Update_AdHoc_Radar(Year, (int)CompanySiteCategoryList.NE3, true, Month, isBackDatedMonth, errorlist);

            }
            catch (Exception ex)
            {
                Log4NetService.LogInfo("Update_AdHoc_Radar_AA failed: ", ex);
            }

            // Updates AdHoc_Radar_Items (and AdHoc_Radar if applicable) for All Companies - Sites, i.e. HUN, WDL, etc.
            try
            {
                //DT418 4/12/15 Cindi Thornton - added month parameter
                Update_AdHoc_Radar(Year, (int)CompanySiteCategoryList.E, false, Month, isBackDatedMonth, errorlist);
                Update_AdHoc_Radar(Year, (int)CompanySiteCategoryList.NE1, false, Month, isBackDatedMonth, errorlist);
                Update_AdHoc_Radar(Year, (int)CompanySiteCategoryList.NE2, false, Month, isBackDatedMonth, errorlist);
                Update_AdHoc_Radar(Year, (int)CompanySiteCategoryList.NE3, false, Month, isBackDatedMonth, errorlist);
            }
            catch (Exception ex)
            {
                Log4NetService.LogInfo("Update_AdHoc_Radar_AS failed: ", ex);
            }
        }
        
        static void Update_AdHoc_Radar(bool All, bool isBackDatedMonth, StringBuilder errorlist)
        {
            //DT419 - don't do this for months, just years. The sp is only for months and is only for a single year/month
            //var storedProcedureService = DependencyResolver.Current.GetService<Repo.CSMS.Service.Database.IStoredProcedureService>();
            //storedProcedureService.SP_Compliance_Table_Batch("All");

            // Updates AdHoc_Radar_Items (and AdHoc_Radar if applicable) for All Companies - Australia, i.e. WA / VICOPS
            try
            {
                int CurrentYear = DateTime.Now.Year;
                int StartYear = DateTime.Now.Year;
                if (All)
                {
                    StartYear = 2007;
                }
                while (StartYear <= CurrentYear)
                {
                    //DT418 4/12/15 Cindi Thornton - added month parameter = 0 (== YTD)
                    Update_AdHoc_Radar(StartYear, (int)CompanySiteCategoryList.E, true, 0, isBackDatedMonth, errorlist);
                    Update_AdHoc_Radar(StartYear, (int)CompanySiteCategoryList.NE1, true, 0, isBackDatedMonth, errorlist);
                    Update_AdHoc_Radar(StartYear, (int)CompanySiteCategoryList.NE2, true, 0, isBackDatedMonth, errorlist);
                    Update_AdHoc_Radar(StartYear, (int)CompanySiteCategoryList.NE3, true, 0, isBackDatedMonth, errorlist);
                    StartYear++;
                }
            }
            catch (Exception ex)
            {
                Log4NetService.LogInfo("Update_AdHoc_Radar_AA failed: ", ex);
            }

            // Updates AdHoc_Radar_Items (and AdHoc_Radar if applicable) for All Companies - Sites, i.e. HUN, WDL, etc.
            try
            {
                int CurrentYear = DateTime.Now.Year;
                int StartYear = DateTime.Now.Year;
                if (All)
                {
                    StartYear = 2007;
                }
                while (StartYear <= CurrentYear)
                {
                    //DT418 4/12/15 Cindi Thornton - added month parameter = 0 (== YTD)
                    Update_AdHoc_Radar(StartYear, (int)CompanySiteCategoryList.E, false, 0, isBackDatedMonth, errorlist);
                    Update_AdHoc_Radar(StartYear, (int)CompanySiteCategoryList.NE1, false, 0, isBackDatedMonth, errorlist);
                    Update_AdHoc_Radar(StartYear, (int)CompanySiteCategoryList.NE2, false, 0, isBackDatedMonth, errorlist);
                    Update_AdHoc_Radar(StartYear, (int)CompanySiteCategoryList.NE3, false, 0, isBackDatedMonth, errorlist);
                    StartYear++;
                }
            }
            catch (Exception ex)
            {
                Log4NetService.LogInfo("Update_AdHoc_Radar_AS failed: ", ex);
            }
        }

        static void Update_AdHoc_Radar_test()
        {
            string KpiMeasure = "CSA - Self Audit (Conducted)";
            int Year = 2007;
            int CategoryId = (int)CompanySiteCategoryList.E;

            int CurrentYear = DateTime.Now.Year;
            while (Year <= CurrentYear)
            {
                try
                {
                    AdHocRadarService ahrService = new AdHocRadarService();
                    AdHocRadar ahr = ahrService.GetByCompanySiteCategoryIdKpiName(CategoryId, KpiMeasure);

                    SitesService sService = new SitesService();
                    TList<Sites> sTlist = sService.GetAll();
                    foreach (Sites s in sTlist)
                    {
                        if (!s.SiteAbbrev.StartsWith("-"))
                        {
                            Update_AdHoc_Radar(Year, ahr.AdHocRadarId, s.SiteId, CategoryId, KpiMeasure, 0, false);  //DT418 - added month param == 0 for YTD, DT419 - added isBackDatedMonth = false;
                        }
                    }
                    //DT418 4/12/15 Cindi Thornton - added month parameter to all calls to Update_AdHoc_Radar
                    Update_AdHoc_Radar(Year, ahr.AdHocRadarId, 20, CategoryId, KpiMeasure, 0, false); //WAO
                    Update_AdHoc_Radar(Year, ahr.AdHocRadarId, 22, CategoryId, KpiMeasure, 0, false); //VIC OPS
                    Year++;
                    Log4NetService.LogInfo("Update - " + Year + " - " + KpiMeasure + " [OK].");
                }
                catch (Exception ex)
                {
                    Log4NetService.LogInfo("Update - " + Year + " - " + KpiMeasure + " [Failed] : " + ex.Message);
                }
            }
        }
        
        private static void Update_AdHoc_Radar(int Year, int CategoryId, bool AA, int Month, bool isBackDatedMonth, StringBuilder errorlist)
        {
            //DT418 4/12/15 Cindi Thornton - added month parameter (0 == YTD)
            AdHocRadarService ahrService = new AdHocRadarService();
            int kpiOrd = 0;
            Log4NetService.LogInfo(String.Format("Update_AdHoc_Radar - Year: {0}, Month: {1}, CategoryId: {2}, AA: {3}", Year, Month, CategoryId, AA)); //Cindi Thornton 25/11/15 add more logging

            foreach (string KpiMeasure in GetKpiMeasurements(CategoryId))
            {
                kpiOrd++;
                try
                {
                    AdHocRadar ahr = ahrService.GetByCompanySiteCategoryIdKpiName(CategoryId, KpiMeasure);
                    if (ahr == null)
                    {
                        ahr = new AdHocRadar();
                        ahr.KpiName = KpiMeasure;
                        ahr.CompanySiteCategoryId = CategoryId;

                        ahr.KpiOrdinal = kpiOrd;
                        ahr.ModifiedDate = DateTime.Now;
                        ahrService.Save(ahr);
                    }

                    Log4NetService.LogInfo("Update_AdHoc_Radar - AA value = " + AA); //Cindi Thornton 25/11/15 more logging info

                    if (AA)
                    {
                        //DT418 4/12/15 Cindi Thornton - added month parameter
                        Update_AdHoc_Radar(Year, ahr.AdHocRadarId, 20, CategoryId, KpiMeasure, Month, isBackDatedMonth); //WAO
                        Update_AdHoc_Radar(Year, ahr.AdHocRadarId, 22, CategoryId, KpiMeasure, Month, isBackDatedMonth); //VIC OPS
                        Update_AdHoc_Radar(Year, ahr.AdHocRadarId, 21, CategoryId, KpiMeasure, Month, isBackDatedMonth);//WAM
                        Update_AdHoc_Radar(Year, ahr.AdHocRadarId, 26, CategoryId, KpiMeasure, Month, isBackDatedMonth);//Australia
                        Update_AdHoc_Radar(Year, ahr.AdHocRadarId, 27, CategoryId, KpiMeasure, Month, isBackDatedMonth);//ARP
                    }
                    else
                    {
                        SitesService sService = new SitesService();
                        TList<Sites> sTlist = sService.GetAll();
                        foreach (Sites s in sTlist)
                        {
                            if (!s.SiteAbbrev.StartsWith("-"))
                            {
                                //DT418 4/12/15 Cindi Thornton - added month parameter, DT419 15/1/16 Cindi Thornton added isBackDatedMonth
                                Update_AdHoc_Radar(Year, ahr.AdHocRadarId, s.SiteId, CategoryId, KpiMeasure, Month, isBackDatedMonth);
                            }
                        }
                    }
                    Log4NetService.LogInfo("Update - " + Year + " - " + KpiMeasure + " [OK].");
                }
                catch (Exception ex)
                {
                    errorlist.AppendLine("Update - " + Year + " - " + KpiMeasure + " [Failed] See log for more information. : " + ex.Message); //add this error to the list, at end of processing, fail job
                    Log4NetService.LogInfo("Update - " + Year + " - " + KpiMeasure + " [Failed] : ", ex);
                }
            }
        }

        private static void Update_AdHoc_Radar(int Year, int AdHocRadarId, int SiteId, int CategoryId, string KpiMeasure, int Month, bool isBackDatedMonth)
        {
            //DT418 4/12/15 Cindi Thornton - added month parameter
            Log4NetService.LogInfo(String.Format("Update_AdHoc_Radar - Year: {0}, Month: {1}, AdHocRadarId: {2}, SiteId: {3}, CategoryId: {4}, KpiMeasure: {5}",
                Year, Month, AdHocRadarId, SiteId, CategoryId, KpiMeasure)); //Cindi Thornton 25/11/15 add more logging

            var ahriService = DependencyResolver.Current.GetService<Repo.CSMS.Service.Database.IAdhocRadarItemService>();
            AdHocRadarItems2Service ahri2Service = new AdHocRadarItems2Service();
            CompaniesService cService = new CompaniesService();

            var ahri = ahriService.Get(e => e.Year == Year && e.AdHoc_RadarId == AdHocRadarId && e.SiteId == SiteId, null);
            if (ahri == null)
            {
                ahri = new Repo.CSMS.DAL.EntityModels.AdHoc_Radar_Items();
                ahri.Year = Year;
                ahri.AdHoc_RadarId = AdHocRadarId;
                ahri.SiteId = SiteId;
            }

            if (Month == 0) //DT481 4/12/15 CT - if we're only doing a single month, this won't be updated unless month = 0;
                ahri.KpiScoreYtd = "n/a";

            int NoCompaniesOnSite = -1;
            KpiCompanySiteCategoryService kcscService = new KpiCompanySiteCategoryService();

            DataSet ds = kcscService.Compliance_Table_AS(SiteId, Year, CategoryId.ToString());
            if (SiteId == 20 || SiteId == 22 || SiteId == 21 || SiteId == 26 || SiteId == 27)
            {
                ds = null;
                if (SiteId == 20)
                {
                    ds = kcscService.Compliance_Table_AA(1, Year, CategoryId.ToString()); //WAO
                }
                else if (SiteId == 22)
                {
                    ds = kcscService.Compliance_Table_AA(4, Year, CategoryId.ToString()); //VICOPS
                }
                else if (SiteId == 21)
                {
                    ds = kcscService.Compliance_Table_AA(3, Year, CategoryId.ToString());//WAM
                }
                else if (SiteId == 26)
                {
                    ds = kcscService.Compliance_Table_AA(2, Year, CategoryId.ToString());//Australia
                }
                else if (SiteId == 27)
                {
                    ds = kcscService.Compliance_Table_AA(7, Year, CategoryId.ToString());//ARP
                }
            }

            if (ds.Tables[0] != null)
            {
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    Int32.TryParse(dr["Number of Companies On Site"].ToString(), out NoCompaniesOnSite);
                }

            }
            //Added by Pankaj Dikshit for <DT 2819, 21-Feb-2013>
            CompanySiteCategoryStandardRegionService cscsrService = new CompanySiteCategoryStandardRegionService();
            CompanySiteCategoryStandardRegionFilters cscsrFilters = new CompanySiteCategoryStandardRegionFilters();
            bool isSafetyPlanSubmittedCompliant = true;
            //Get Score
            if (KpiMeasure == "Peak No Employees (Previous Month)" ||
                KpiMeasure == "Avg No Employees (YTD)" ||
                KpiMeasure == "Average No. Of People On Site" ||
                KpiMeasure == "Total Man Hours" ||
                KpiMeasure == "LWDFR" || KpiMeasure == "TRIFR" || KpiMeasure == "AIFR" ||
                KpiMeasure == "IFE : Injury Ratio" || //KpiMeasure == "CSA - Self Audit (Conducted)" ||
                KpiMeasure == "Number of Companies:" || KpiMeasure == "Number of Companies On Site:" ||
                KpiMeasure == "Safety Plan(s) Submitted" ||
                KpiMeasure == "DART" || KpiMeasure == "Risk Notifications")
            {
                if (ds.Tables[0] != null)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        //Added by Pankaj Dikshit for <DT 2819, 21-Feb-2013>
                        isSafetyPlanSubmittedCompliant = true;
                        switch (KpiMeasure)
                        {
                            case "Peak No Employees (Previous Month)":
                                ahri.KpiScoreYtd = dr["Peak No Employees (Previous Month)"].ToString();
                                break;
                            case "Avg No Employees (YTD)":
                                ahri.KpiScoreYtd = dr["Average No. Of People On Site"].ToString();

                                int avgNoEmployees = 0;
                                Int32.TryParse(ahri.KpiScoreYtd, out avgNoEmployees);
                                if (avgNoEmployees > 0)
                                {
                                    Double _avgNoEmployees_Divisor = 1;

                                    DateTime dtNowMinusOneMonth = DateTime.Now.AddMonths(-1);
                                    if (dtNowMinusOneMonth.Month != 12)
                                    {
                                        _avgNoEmployees_Divisor = Convert.ToDouble(dtNowMinusOneMonth.Month);
                                    }

                                    Double _avgNoEmployees = Convert.ToDouble(avgNoEmployees);
                                    _avgNoEmployees = _avgNoEmployees / _avgNoEmployees_Divisor;
                                    avgNoEmployees = Convert.ToInt32(_avgNoEmployees);

                                    ahri.KpiScoreYtd = avgNoEmployees.ToString();
                                }
                                break;
                            case "Number of Companies:":
                                ahri.KpiScoreYtd = dr["Number of Companies"].ToString();
                                break;
                            case "Number of Companies On Site:":
                                ahri.KpiScoreYtd = dr["Number of Companies On Site"].ToString();
                                break;
                            case "Total Man Hours":
                                ahri.KpiScoreYtd = dr["Total Man Hours"].ToString();
                                break;
                            case "LWDFR":
                                ahri.KpiScoreYtd = dr["LWDFR"].ToString();
                                break;
                            case "TRIFR":
                                ahri.KpiScoreYtd = dr["TRIFR"].ToString();
                                break;
                            case "AIFR":
                                ahri.KpiScoreYtd = dr["AIFR"].ToString();
                                break;
                            case "IFE : Injury Ratio":
                                ahri.KpiScoreYtd = dr["IFEInjuryRatio"].ToString();
                                break;
                            case "DART": //PANKAJ
                                ahri.KpiScoreYtd = dr["DART"].ToString();
                                break;
                            case "Risk Notifications": //Added by Sayani Sil for Task# 1
                                ahri.KpiScoreYtd = dr["Risk Notifications"].ToString();
                                break;
                            case "Safety Plan(s) Submitted":
                                Decimal SmpNo = 0;
                                Decimal SmpExpected = 0;
                                Decimal.TryParse(dr["SmpNo"].ToString(), out SmpNo);
                                Decimal.TryParse(dr["SmpExpected"].ToString(), out SmpExpected);

                                if (SmpNo == 0)
                                {
                                    ahri.KpiScoreYtd = "0";
                                }
                                else
                                {
                                    if (SmpExpected > 0)
                                    {
                                        Decimal Total = (SmpNo / SmpExpected) * 100.0m;
                                        Total = Decimal.Round(Total, 2);
                                        ahri.KpiScoreYtd = Convert.ToInt32(Total).ToString();
                                    }
                                    else
                                    {
                                        ahri.KpiScoreYtd = "0";
                                    }
                                }
                                //Adding by Pankaj Dikshit for <DT 2819, 21-Feb-2013> - Start
                                //Below if-section added by Pankaj Dikshit for dashboard issue# 19
                                if (SmpExpected == 0)
                                {
                                    ahri.KpiScoreYtd = "n/a";
                                }
                                if (!(ahri.KpiScoreYtd.ToString() == "100" || ahri.KpiScoreYtd.ToString() == "-"))
                                {
                                    isSafetyPlanSubmittedCompliant = false;
                                }
                                //Adding by Pankaj Dikshit for <DT 2819, 21-Feb-2013> - End
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
            else
            {
                //DT375 Cindi Thornton 2/12/15 - get the kpi once here to improve performance. Code was hitting the database multiple times.
                var kpiService = DependencyResolver.Current.GetService<Repo.CSMS.Service.Database.IKpiService>();
                DateTime kpiStartYearMonth = new DateTime(Year, 1, 1);
                List<Repo.CSMS.DAL.EntityModels.Kpi> kpi = kpiService.GetMany(null, x => x.kpiDateTime >= kpiStartYearMonth, null, null);

                //DT418 4/12/15 Cindi Thornton - don't loop through all months, this was added for DT236 but isn't correct.
                int month = Month;

                int Compliant = 0;
                int Total = 0;
                string v = "n/a";
                cscsrFilters.Append(CompanySiteCategoryStandardRegionColumn.CompanySiteCategoryId, CategoryId.ToString());
                if (SiteId == 20)
                {
                    cscsrFilters.Append(CompanySiteCategoryStandardRegionColumn.RegionId, "1"); //wao
                }
                else if (SiteId == 22)
                {
                    cscsrFilters.Append(CompanySiteCategoryStandardRegionColumn.RegionId, "4"); //vicops
                }
                else if (SiteId == 21)
                {
                    cscsrFilters.Append(CompanySiteCategoryStandardRegionColumn.RegionId, "3");//WAM
                }
                else if (SiteId == 26)
                {
                    cscsrFilters.Append(CompanySiteCategoryStandardRegionColumn.RegionId, "2");//Australia
                }
                else if (SiteId == 27)
                {
                    cscsrFilters.Append(CompanySiteCategoryStandardRegionColumn.RegionId, "7");//ARP
                }
                else
                {
                    cscsrFilters.Append(CompanySiteCategoryStandardRegionColumn.SiteId, SiteId.ToString());
                    cscsrFilters.Append(CompanySiteCategoryStandardRegionColumn.RegionId, "2"); //australia
                }

                int _count = 0;
                VList<CompanySiteCategoryStandardRegion> cscsrTlist = DataRepository.CompanySiteCategoryStandardRegionProvider.GetPaged(cscsrFilters.ToString(), null, 0, 99999, out _count);

                DateTime dtNowMinusOneMonth = DateTime.Now.AddMonths(-1);
                if (Year < DateTime.Now.Year)
                {
                    dtNowMinusOneMonth = new DateTime(Year, 12, 1);
                }
                foreach (CompanySiteCategoryStandardRegion cscsr in cscsrTlist)
                {
                    bool isCompliant = true;

                    if (KpiMeasure != "CSA - Self Audit (Conducted)")
                    {
                        //DT375 - Cindi Thornton 2/12/15 - filter the kpi list now we have more details.
                        DateTime dtStartMonthYear = new DateTime(Year, 1, 1);
                        List<Repo.CSMS.DAL.EntityModels.Kpi> kpiForCompanySite = kpi.FindAll(x => x.CompanyId == cscsr.CompanyId && x.SiteId == cscsr.SiteId);

                        DataSet DataSet2_Ytd = new DataSet();
                        DataSet2_Ytd.Tables.Add(Helper.Radar.CreateDataTable(cscsr.CompanyId, cscsr.SiteId, 2, null));

                        DataRow dr = Helper.Radar.CreateDataRow_DataSet2_Ytd(DataSet2_Ytd.Tables[0], KpiMeasure,
                                                                                    cscsr.CompanyId, cscsr.SiteId, month, Year, CategoryId, kpiForCompanySite);
                        if (dr["KpiScoreYtdPercentage"].ToString() == "100" || dr["KpiScoreYtdPercentage"].ToString() == "-")
                        {
                            Compliant++;
                        }
                        else
                        {
                            isCompliant = false;
                        }
                        Total++;
                    }

                    //if company less than target, add to list of non compliance (month = 0 = YTD only).
                    if (month == 0 && !isCompliant)
                    {
                        AdHocRadarItems2 ahri2 = ahri2Service.GetByYearAdHocRadarIdCompanyIdSiteId(Year, AdHocRadarId, cscsr.CompanyId, SiteId);
                        if (ahri2 == null)
                        {
                            ahri2 = new AdHocRadarItems2();
                            ahri2.Year = Year;
                            ahri2.AdHocRadarId = AdHocRadarId;
                            ahri2.CompanyId = cscsr.CompanyId;
                            ahri2.SiteId = SiteId;
                            ahri2.ModifiedDate = DateTime.Now;
                        }
                        ahri2Service.Save(ahri2);
                    }

                }
                
                if (KpiMeasure != "CSA - Self Audit (Conducted)")
                {
                    if (Total > 0)
                    {
                        Decimal _v = (Convert.ToDecimal(Compliant) / Convert.ToDecimal(Total)) * 100.0m;
                        v = Convert.ToInt32(_v).ToString();
                    }
                    else
                    {
                        v = "n/a";
                    }
                }

                if (NoCompaniesOnSite == 0 && v == "100") v = "n/a";
                ahri.SetKpiScore(v, month);
            }

            if (KpiMeasure == "Safety Plan(s) Submitted")
            {
                if (!isSafetyPlanSubmittedCompliant)
                {
                    cscsrFilters.Append(CompanySiteCategoryStandardRegionColumn.CompanySiteCategoryId, CategoryId.ToString());
                    if (SiteId == 20)
                    {
                        cscsrFilters.Append(CompanySiteCategoryStandardRegionColumn.RegionId, "1"); //wao
                    }
                    else if (SiteId == 22)
                    {
                        cscsrFilters.Append(CompanySiteCategoryStandardRegionColumn.RegionId, "4"); //vicops
                    }
                    else if (SiteId == 21)
                    {
                        cscsrFilters.Append(CompanySiteCategoryStandardRegionColumn.RegionId, "3");//WAM
                    }
                    else if (SiteId == 26)
                    {
                        cscsrFilters.Append(CompanySiteCategoryStandardRegionColumn.RegionId, "2");//Australia
                    }
                    else if (SiteId == 27)
                    {
                        cscsrFilters.Append(CompanySiteCategoryStandardRegionColumn.RegionId, "7");//ARP
                    }
                    else
                    {
                        cscsrFilters.Append(CompanySiteCategoryStandardRegionColumn.SiteId, SiteId.ToString());
                        cscsrFilters.Append(CompanySiteCategoryStandardRegionColumn.RegionId, "2"); //australia
                    }
                    int _cnt = 0;
                    VList<CompanySiteCategoryStandardRegion> cscsrTlist1 = DataRepository.CompanySiteCategoryStandardRegionProvider.GetPaged(cscsrFilters.ToString(), null, 0, 99999, out _cnt);

                    foreach (CompanySiteCategoryStandardRegion cscsr in cscsrTlist1)
                    {
                        bool flag = false;
                        var fdbService = new KaiZen.CSMS.Services.FileDbService();
                        TList<FileDb> filedb = fdbService.GetByCompanyId(cscsr.CompanyId);
                        foreach (FileDb fdb in filedb)
                        {
                            if (Convert.ToString(fdb.Description).Substring(0, 4) == Year.ToString())
                            {
                                flag = true;
                            }
                        }
                        if (!flag && Month == 0) //DT418 - added Month == 0. Only needed for YTD
                        {
                            AdHocRadarItems2 ahri2 = ahri2Service.GetByYearAdHocRadarIdCompanyIdSiteId(Year, AdHocRadarId, cscsr.CompanyId, SiteId);
                            if (ahri2 == null)
                            {
                                ahri2 = new AdHocRadarItems2();
                                ahri2.Year = Year;
                                ahri2.AdHocRadarId = AdHocRadarId;
                                ahri2.CompanyId = cscsr.CompanyId;
                                ahri2.SiteId = SiteId;
                                ahri2.ModifiedDate = DateTime.Now;
                            }
                            ahri2Service.Save(ahri2);
                        }
                    }
                }
            }

            ahri.ModifiedDate = DateTime.Now;

            if (Month == 0) //DT418 - Companies not compliant is only for YTD.
            {
                List<string> companiesList = new List<string>();

                TList<AdHocRadarItems2> ahri2List = ahri2Service.GetByYearAdHocRadarIdSiteId(Year, AdHocRadarId, SiteId);
                if (ahri2List != null)
                {
                    foreach (AdHocRadarItems2 ahri2 in ahri2List)
                    {
                        Companies c = cService.GetByCompanyId(ahri2.CompanyId);
                        companiesList.Add(c.CompanyName);
                    }
                }
                companiesList.Sort();

                string CompaniesNotCompliant = "";
                foreach (string c in companiesList)
                {
                    if (String.IsNullOrEmpty(CompaniesNotCompliant))
                    {
                        CompaniesNotCompliant = c;
                    }
                    else
                    {
                        CompaniesNotCompliant += ", " + c;
                    }
                }
                ahri.CompaniesNotCompliant = CompaniesNotCompliant;
            }

            if (ahri.AdHoc_Radar_ItemId <= 0)
            {
                if (ahri.KpiScoreYtd == null) ahri.KpiScoreYtd = "n/a"; //DT418 - can't insert null, n/a is being used as null substitute.

                ahriService.Insert(ahri);
            }
            else
            {
                ahriService.Update(ahri);
            }
        }

        public static string[] GetKpiMeasurements(int CategoryId)
        {
            string[] KpiArray2_Embedded = {
                                    "Peak No Employees (Previous Month)",
                                    "Avg No Employees (YTD)",
                                    "Total Man Hours",
                                    "LWDFR",
                                    "TRIFR",
                                    "AIFR",
                                    "Number of Companies:",
                                    "Number of Companies On Site:",
                                    "CSA - Self Audit (Conducted)",
                                    "IFE : Injury Ratio",
                                    "DART", //Added by Pankaj Dikshit for <DT 2459, 14-Aug-2012>
                                    "Risk Notifications", //Added by Sayani Sil for Task# 1
                                    "Workplace Safety & Compliance",
                                    "Management Health Safety Work Contacts",
                                    "Behavioural Observations (%)",
                                    "Fatality Prevention Program",
                                    "JSA Field Audit Verifications",
                                    "Toolbox Meetings",
                                    "Alcoa Weekly Contractors Meeting",
                                    "Alcoa Monthly Contractors Meeting",
                                    "Safety Plan(s) Submitted",
                                    "% Mandated Training"
                                 };
            string[] KpiArray2_NonEmbedded_1 = {
                                    "Peak No Employees (Previous Month)",
                                    "Avg No Employees (YTD)",
                                    "Total Man Hours",
                                    "LWDFR",
                                    "TRIFR",
                                    "AIFR",
                                    "Number of Companies:",
                                    "Number of Companies On Site:",
                                    "CSA - Self Audit (Conducted)",
                                    "IFE : Injury Ratio",
                                    "DART", //Added by Pankaj Dikshit for <DT 2459, 14-Aug-2012>
                                    "Risk Notifications", //Added by Sayani Sil for Task# 1
                                    "Workplace Safety & Compliance",
                                    "Management Health Safety Work Contacts",
                                    "Behavioural Observations (%)",
                                    "JSA Field Audit Verifications",
                                    "Toolbox Meetings",
                                    "Alcoa Weekly Contractors Meeting",
                                    "Alcoa Monthly Contractors Meeting",
                                    "% Mandated Training"
                                 };
            string[] KpiArray2_NonEmbedded_2 = {
                                    "Peak No Employees (Previous Month)",
                                    "Avg No Employees (YTD)",
                                    "Total Man Hours",
                                    "LWDFR",
                                    "TRIFR",
                                    "AIFR",
                                    "Number of Companies:",
                                    "Number of Companies On Site:",
                                    "IFE : Injury Ratio",
                                    "DART", //Added by Pankaj Dikshit for <DT 2459, 14-Aug-2012>
                                    "Risk Notifications", //Added by Sayani Sil for Task# 1
                                    "Workplace Safety & Compliance",
                                    "Management Health Safety Work Contacts",
                                    "Behavioural Observations (%)",
                                    "JSA Field Audit Verifications",
                                    "Toolbox Meetings",
                                    "Alcoa Monthly Contractors Meeting",
                                    "% Mandated Training"
                                 };
            string[] KpiArray2_NonEmbedded_3 = {
                                    "Peak No Employees (Previous Month)",
                                    "Avg No Employees (YTD)",
                                    "Total Man Hours",
                                    "LWDFR",
                                    "TRIFR",
                                    "AIFR",
                                    "Number of Companies:",
                                    "Number of Companies On Site:",
                                    "IFE : Injury Ratio",
                                    "DART", //Added by Pankaj Dikshit for <DT 2459, 14-Aug-2012>
                                    "Risk Notifications", //Added by Sayani Sil for Task# 1
                                    //"JSA Field Audit Verifications", -- Removed 13.Oct.2011 and replaced with 'Toolbox Meetings'
                                    "Toolbox Meetings",
                                    "% Mandated Training"
                                 };
            switch (CategoryId)
            {
                case (int)CompanySiteCategoryList.E:
                    return KpiArray2_Embedded;
                case (int)CompanySiteCategoryList.NE1:
                    return KpiArray2_NonEmbedded_1;
                case (int)CompanySiteCategoryList.NE2:
                    return KpiArray2_NonEmbedded_2;
                case (int)CompanySiteCategoryList.NE3:
                    return KpiArray2_NonEmbedded_3;
                default:
                    throw new Exception("Unexpected value of CategoryId received.");
            }
        }

        static void Update_KpiProjectsList()
        {
            try
            {
                Configuration configuration = new Configuration();
                Update_KpiProjectsList(configuration.GetValue(ConfigList.KpiProjectsListFilePath));
            }
            catch (Exception ex)
            {
                Log4NetService.LogInfo(ex.Message.ToString() + "\nServer Configuration Account Error. Contact Administrator.");
            }
            finally
            {
            }
        }
        static void Update_KpiProjectsList(string csvFilePath)
        {
            int cnt = 0;
            string startHeader = "\"Project Number\",\"Project Description\",\"Project Code\",\"Area\",\"Project Status\",\"Current Project Manager\",\"Phase\",\"Original Completion Date\",\"Authorised Date\",\"Concept Approval Number\"";
            //2.1 Read from the actual Header of the CSV file. (Ignore junk at start of CSV file)
            string myFile = "";
            bool start = false;

            try
            {
                FileStream logFileStream = new FileStream(csvFilePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                StreamReader logFileReader = new StreamReader(logFileStream);

                while (!logFileReader.EndOfStream)
                {
                    string line = logFileReader.ReadLine();
                    if (line.Contains(startHeader)) { start = true; };
                    if (start == true)
                    {
                        myFile += line + "\n";
                        cnt++;
                    }
                }
                logFileReader.Close();
                logFileStream.Close();
            }
            catch (Exception ex)
            {
                Log4NetService.LogInfo(ex.Message);
            }

            if (!String.IsNullOrEmpty(myFile))
            {
                Log4NetService.LogInfo("CSV File Ok.");
                try
                {
                    using (CsvReader csv = new CsvReader(new StringReader(myFile), true))
                    {
                        int fieldCount = csv.FieldCount;
                        string[] headers = csv.GetFieldHeaders();
                        Log4NetService.LogInfo("Found Headers: " + String.Join(",", headers));

                        if (cnt > 0)
                        {
                            var kplService = new KaiZen.CSMS.Services.KpiProjectListService();
                            TList<KpiProjectList> kplTlist = kplService.GetAll();
                            TList<KpiProjectList> kplTlist_new = new TList<KpiProjectList>();

                            string datetimeFormat = "dd/MM/yyyy";
                            CultureInfo provider = CultureInfo.InvariantCulture;

                            while (csv.ReadNextRecord())
                            {
                                try
                                {
                                    KpiProjectList kpl = new KpiProjectList();

                                    //Mandatory
                                    kpl.ProjectNumber = csv["Project Number"].ToString();
                                    kpl.ProjectDescription = csv["Project Description"].ToString();
                                    kpl.ProjectCode = csv["Project Code"].ToString();
                                    kpl.Area = csv["Area"].ToString();
                                    kpl.ProjectStatus = csv["Project Status"].ToString();
                                    kpl.CurrentProjectManager = csv["Current Project Manager"].ToString();
                                    kpl.Phase = csv["Phase"].ToString();

                                    //Optional
                                    if (!String.IsNullOrEmpty(csv["Original Completion Date"].ToString()))
                                    {
                                        try
                                        {
                                            DateTime dt = DateTime.ParseExact(csv["Original Completion Date"].ToString(), datetimeFormat, provider);
                                            kpl.OriginalCompletionDate = dt;
                                        }
                                        catch (Exception ex)
                                        {
                                            Log4NetService.LogInfo(kpl.ProjectNumber + ": " + ex.Message);
                                        }
                                    }
                                    if (!String.IsNullOrEmpty(csv["Authorised Date"].ToString()))
                                    {
                                        try
                                        {
                                            DateTime dt = DateTime.ParseExact(csv["Authorised Date"].ToString(), datetimeFormat, provider);
                                            kpl.AuthorisedDate = dt;
                                        }
                                        catch (Exception ex)
                                        {
                                            Log4NetService.LogInfo(kpl.ProjectNumber + ": " + ex.Message);
                                        }
                                    }

                                    kpl.ConceptApprovalNumber = csv["Concept Approval Number"].ToString();
                                    kplTlist_new.Add(kpl);
                                }
                                catch (Exception ex)
                                {
                                    Log4NetService.LogInfo(ex.Message);
                                }
                            }

                            if (kplTlist.Count > 0 && kplTlist_new.Count > 0)
                            {
                                try
                                {
                                    kplService.Delete(kplTlist);
                                    kplService.Reseed();
                                    Log4NetService.LogInfo("Deleting Current Data: SUCCESS");
                                }
                                catch (Exception ex)
                                {
                                    Log4NetService.LogInfo("Deleting Current Data: FAIL.. " + ex.Message);
                                }
                            }
                            try
                            {
                                kplService.Insert(kplTlist_new);
                                Log4NetService.LogInfo("Adding New Data: SUCCESS");
                            }
                            catch (Exception ex)
                            {
                                Log4NetService.LogInfo("Adding New Data: FAIL.. " + ex.Message);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Log4NetService.LogInfo(ex.Message);
                }
            }
        }

        static void UpdateContractors()
        {
            UsersService uService = new UsersService();
            CompaniesService cService = new CompaniesService();
            QuestionnaireWithLocationApprovalViewLatestService qwlavlService = new QuestionnaireWithLocationApprovalViewLatestService();
            VList<QuestionnaireWithLocationApprovalViewLatest> qwlavlVlist = qwlavlService.GetAll();
            foreach (QuestionnaireWithLocationApprovalViewLatest qwlavl in qwlavlVlist)
            {
                Companies c = cService.GetByCompanyId(qwlavl.CompanyId);
                if (c.Deactivated != false)
                {
                    if (qwlavl.CompanyStatusId == (int)CompanyStatusList.Active && qwlavl.MainAssessmentValidTo != null)
                    {
                        if (qwlavl.Status == (int)QuestionnaireStatusList.AssessmentComplete && qwlavl.MainAssessmentValidTo > DateTime.Now)
                        {
                            UsersFilters uFilters = new UsersFilters();
                            uFilters.Append(UsersColumn.CompanyId, qwlavl.CompanyId.ToString());
                            uFilters.Append(UsersColumn.RoleId, ((int)RoleList.PreQual).ToString());
                            int count;
                            TList<Users> uTlist = uService.GetPaged(uFilters.ToString(), "FirstName ASC", 0, 9999, out count);

                            if (count > 0)
                            {
                                foreach (Users u in uTlist)
                                {
                                    Users uUpdate = uService.GetByUserId(u.UserId);
                                    uUpdate.RoleId = (int)RoleList.Contractor;
                                    uService.Save(uUpdate);
                                    Log4NetService.LogInfo(qwlavl.CompanyName + ", " + qwlavl.CompanyStatusDesc + ", " + qwlavl.MainAssessmentValidTo + ", " + qwlavl.Status.ToString() + ", " + u.UserId + ", " + uUpdate.FirstName + ", " + uUpdate.LastName);
                                }
                            }
                        }
                    }
                }
            }
        }

        static void UpdateSafetyCategoriesFromFile(string FileFullPath)
        {
            if (!String.IsNullOrEmpty(FileFullPath))
            {
                Log4NetService.LogInfo("CSV File Ok. Reading: " + FileFullPath);
                try
                {
                    using (CsvReader csv = new CsvReader(new StreamReader(FileFullPath), true))
                    {
                        int lineCnt = 0;
                        int updateCnt = 0;
                        int fieldCount = csv.FieldCount;
                        string[] headers = csv.GetFieldHeaders();
                        Log4NetService.LogInfo("Found Headers: " + String.Join(",", headers));

                        CompaniesService cService = new CompaniesService();
                        TList<Companies> cTlist = cService.GetAll();

                        SitesService sService = new SitesService();
                        TList<Sites> sTlist = sService.GetAll();

                        var cscsService = new KaiZen.CSMS.Services.CompanySiteCategoryStandardService();

                        while (csv.ReadNextRecord())
                        {
                            lineCnt++;
                            string Company = "";
                            string Site = "";

                            try
                            {
                                Company = csv["Company"].ToString();
                                Site = csv["Site"].ToString();
                                string ResidentialCategory = csv["Residential Category"].ToString();

                                Companies c = cTlist.Find(CompaniesColumn.CompanyName, Company, false);
                                if (c == null) throw new Exception("Could not find Company");

                                Sites s = sTlist.Find(SitesColumn.SiteName, Site, false);
                                if (s == null) throw new Exception("Could not find Site");

                                int? _ResidentialCategory = null;
                                if (ResidentialCategory == "Embedded") _ResidentialCategory = (int)CompanySiteCategoryList.E;
                                if (ResidentialCategory == "Non-Embedded 1") _ResidentialCategory = (int)CompanySiteCategoryList.NE1;
                                if (ResidentialCategory == "Non-Embedded 2") _ResidentialCategory = (int)CompanySiteCategoryList.NE2;
                                if (ResidentialCategory == "Non-Embedded 3") _ResidentialCategory = (int)CompanySiteCategoryList.NE3;

                                CompanySiteCategoryStandard cscs = cscsService.GetByCompanyIdSiteId(c.CompanyId, s.SiteId);
                                if (cscs == null) throw new Exception("Could not find in CompanySiteCategoryStandard"); //shouldn't happen!

                                bool changed = false;

                                if (cscs.CompanySiteCategoryId != _ResidentialCategory)
                                {
                                    cscs.CompanySiteCategoryId = _ResidentialCategory;
                                    changed = true;
                                }

                                string Approved = csv["Approved"].ToString();

                                bool? _Approved = null;
                                if (Approved == "Yes") _Approved = true;
                                if (Approved == "No") _Approved = false;
                                if (_Approved == null) Log4NetService.LogInfo("Warning: Approval not selected");
                                if (cscs.Approved != _Approved)
                                {
                                    cscs.Approved = _Approved;
                                    changed = true;
                                }

                                string LocationSponsor = csv["Location Sponsor"].ToString();
                                string ARP = csv["ARP"].ToString();
                                string CRP = csv["CRP"].ToString();

                                if (!String.IsNullOrEmpty(LocationSponsor))
                                {
                                    #region Location Sponsor
                                    int LocationSponsorCount = 0;

                                    VList<UsersAlcoan> uaVlist = DataRepository.UsersAlcoanProvider.GetPaged(String.Format("UserFullName LIKE '{0}'", LocationSponsor), "", 0, 9999, out LocationSponsorCount);
                                    if (LocationSponsorCount == 0)
                                    {
                                        Log4NetService.LogInfo("Could not find Location Sponsor: " + LocationSponsor);
                                    }
                                    else
                                    {
                                        if (LocationSponsorCount == 1)
                                        {
                                            int? LocationSponsorUserId = uaVlist[0].UserId;
                                            if (cscs.LocationSponsorUserId != LocationSponsorUserId)
                                            {
                                                cscs.LocationSponsorUserId = LocationSponsorUserId;
                                                changed = true;
                                            }
                                        }
                                        else
                                        {
                                            Log4NetService.LogInfo("Found two or more Location Sponsor: " + LocationSponsor);
                                        }
                                    }
                                    #endregion
                                }

                                if (!String.IsNullOrEmpty(ARP))
                                {
                                    #region ARP
                                    int ARPCount = 0;

                                    VList<UsersAlcoan> uaVlist = DataRepository.UsersAlcoanProvider.GetPaged(String.Format("UserFullName LIKE '{0}'", ARP), "", 0, 9999, out ARPCount);
                                    if (ARPCount == 0)
                                    {
                                        Log4NetService.LogInfo("Could not find ARP: " + ARP);
                                    }
                                    else
                                    {
                                        if (ARPCount == 1)
                                        {
                                            int? ARPUserId = uaVlist[0].UserId;
                                            if (cscs.ArpUserId != ARPUserId)
                                            {
                                                cscs.ArpUserId = ARPUserId;
                                                changed = true;
                                            }
                                        }
                                        else
                                        {
                                            Log4NetService.LogInfo("Found two or more ARP: " + ARP);
                                        }
                                    }
                                    #endregion
                                }

                                if (changed)
                                {
                                    cscsService.Save(cscs);
                                    Log4NetService.LogInfo(String.Format("{0},{1},Updated", Company, Site));
                                    updateCnt++;
                                }
                                else
                                {
                                    Log4NetService.LogInfo(String.Format("{0},{1},Skipped - No Changes", Company, Site));
                                }
                            }
                            catch (Exception ex)
                            {
                                Log4NetService.LogInfo(String.Format("{0},{1},Error occured on line {2}: {3}", Company, Site, lineCnt, ex.Message));
                            }
                        }
                        Log4NetService.LogInfo(String.Format("Updated {0} Entries, Skipped", updateCnt));
                    }
                }
                catch (Exception ex)
                {
                    Log4NetService.LogInfo("[ERROR] " + ex.Message.ToString());
                }
            }
        }

        static void CorrectInitialRiskRating()
        {
            var qService = new KaiZen.CSMS.Services.QuestionnaireService();
            var qirService = new KaiZen.CSMS.Services.QuestionnaireInitialResponseService();

            TList<Questionnaire> qList = qService.GetAll();

            int changes = 0;

            foreach (Questionnaire q in qList)
            {
                if (q.InitialStatus != (int)QuestionnaireStatusList.Incomplete && q.SubContractor != true)
                {
                    try
                    {
                        string InitialRiskRating = q.InitialRiskAssessment;
                        string InitialRiskRating_Actual = "";
                        TList<QuestionnaireInitialResponse> qirList = qirService.GetByQuestionnaireId(q.QuestionnaireId);

                        if (qirList.Count > 0)
                        {
                            if (String.IsNullOrEmpty(InitialRiskRating_Actual))
                            {
                                QuestionnaireInitialResponse qir10_11 = qirList.Find(QuestionnaireInitialResponseColumn.QuestionId, "10_11");
                                QuestionnaireInitialResponse qir10_10 = qirList.Find(QuestionnaireInitialResponseColumn.QuestionId, "10_10");
                                QuestionnaireInitialResponse qir10_9 = qirList.Find(QuestionnaireInitialResponseColumn.QuestionId, "10_9");
                                QuestionnaireInitialResponse qir10_8 = qirList.Find(QuestionnaireInitialResponseColumn.QuestionId, "10_8");
                                if (qir10_11 != null) { if (qir10_11.AnswerText == "Yes") InitialRiskRating_Actual = "Forced High Risk"; };
                                if (qir10_10 != null) { if (qir10_10.AnswerText == "Yes") InitialRiskRating_Actual = "Forced High Risk"; };
                                if (qir10_9 != null) { if (qir10_9.AnswerText == "Yes") InitialRiskRating_Actual = "Forced High Risk"; };
                                if (qir10_8 != null) { if (qir10_8.AnswerText == "Yes") InitialRiskRating_Actual = "Forced High Risk"; };
                            }

                            if (String.IsNullOrEmpty(InitialRiskRating_Actual))
                            {
                                QuestionnaireInitialResponse qir10_7 = qirList.Find(QuestionnaireInitialResponseColumn.QuestionId, "10_7");
                                QuestionnaireInitialResponse qir10_6 = qirList.Find(QuestionnaireInitialResponseColumn.QuestionId, "10_6");
                                if (qir10_7 != null) { if (qir10_7.AnswerText == "Yes") InitialRiskRating_Actual = "High Risk"; };
                                if (qir10_6 != null) { if (qir10_6.AnswerText == "Yes") InitialRiskRating_Actual = "High Risk"; };
                            }

                            if (String.IsNullOrEmpty(InitialRiskRating_Actual))
                            {
                                QuestionnaireInitialResponse qir10_5 = qirList.Find(QuestionnaireInitialResponseColumn.QuestionId, "10_5");
                                QuestionnaireInitialResponse qir10_4 = qirList.Find(QuestionnaireInitialResponseColumn.QuestionId, "10_4");
                                if (qir10_5 != null) { if (qir10_5.AnswerText == "Yes") InitialRiskRating_Actual = "Medium Risk"; };
                                if (qir10_4 != null) { if (qir10_4.AnswerText == "Yes") InitialRiskRating_Actual = "Medium Risk"; };
                            }

                            if (String.IsNullOrEmpty(InitialRiskRating_Actual))
                            {
                                QuestionnaireInitialResponse qir10_3 = qirList.Find(QuestionnaireInitialResponseColumn.QuestionId, "10_3");
                                QuestionnaireInitialResponse qir10_2 = qirList.Find(QuestionnaireInitialResponseColumn.QuestionId, "10_2");
                                QuestionnaireInitialResponse qir10_1 = qirList.Find(QuestionnaireInitialResponseColumn.QuestionId, "10_1");
                                if (qir10_3 != null) { if (qir10_3.AnswerText == "Yes") InitialRiskRating_Actual = "Low Risk"; };
                                if (qir10_2 != null) { if (qir10_2.AnswerText == "Yes") InitialRiskRating_Actual = "Low Risk"; };
                                if (qir10_1 != null) { if (qir10_1.AnswerText == "Yes") InitialRiskRating_Actual = "Low Risk"; };
                            }
                        }

                        if (String.IsNullOrEmpty(InitialRiskRating_Actual))
                        {
                            throw new Exception(q.QuestionnaireId + ",Initial Risk Assessment not done.");
                        }
                        else
                        {
                            bool wantsSubContractors = false;
                            QuestionnaireInitialResponse qir12_1 = qirList.Find(QuestionnaireInitialResponseColumn.QuestionId, "12_1");
                            if (qir12_1 != null) { if (qir12_1.AnswerText == "Yes") wantsSubContractors = true; };
                            if (InitialRiskRating_Actual.Contains("High") || wantsSubContractors)
                            {
                                if (!q.IsVerificationRequired)
                                {
                                    changes++;
                                    CompaniesService cService = new CompaniesService();
                                    Companies c = cService.GetByCompanyId(q.CompanyId);
                                    Log4NetService.LogInfo("[Change]," + q.QuestionnaireId + "," + c.CompanyName + "," + q.Status + "," + c.CompanyStatusId);
                                }
                            }

                            if (InitialRiskRating == InitialRiskRating_Actual)
                            {
                                Log4NetService.LogInfo("[Match]," + q.QuestionnaireId + "," + InitialRiskRating);
                            }
                            else
                            {
                                Log4NetService.LogInfo("[Not Match]," + q.QuestionnaireId + "," + InitialRiskRating + "," + InitialRiskRating_Actual);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Log4NetService.LogInfo("[Error], " + ex.Message);
                    }
                }
            }
            Log4NetService.LogInfo(changes.ToString());
        }

        static void UpdateCompanyOnSitePrevious()
        {
            UpdateCompanyOnSitePrevious((int)CompanySiteCategoryList.E);
            UpdateCompanyOnSitePrevious((int)CompanySiteCategoryList.NE1);
            UpdateCompanyOnSitePrevious((int)CompanySiteCategoryList.NE2);
            UpdateCompanyOnSitePrevious((int)CompanySiteCategoryList.NE3);
        }

        static void UpdateCompanyOnSitePrevious(int CompanySiteCategoryId)
        {
            AdHocRadarService ahrService = new AdHocRadarService();
            AdHocRadar ahr = ahrService.GetByCompanySiteCategoryIdKpiName(CompanySiteCategoryId, "Number of Companies:");
            AdHocRadar ahr_new = ahrService.GetByCompanySiteCategoryIdKpiName(CompanySiteCategoryId, "Number of Companies On Site:");

            AdHocRadarItemsService ahriService = new AdHocRadarItemsService();
            TList<AdHocRadarItems> ahriList = ahriService.GetByAdHocRadarId(ahr.AdHocRadarId);
            foreach (AdHocRadarItems ahri in ahriList)
            {
                AdHocRadarItems ahri_new = ahriService.GetByYearAdHocRadarIdSiteId(ahri.Year, ahr_new.AdHocRadarId, ahri.SiteId);
                if (ahri_new == null)
                {
                    ahri_new = new AdHocRadarItems();
                    ahri_new.AdHocRadarId = ahr_new.AdHocRadarId;
                    ahri_new.KpiScoreYtd = "-";
                    ahri_new.SiteId = ahri.SiteId;
                    ahri_new.Year = ahri.Year;
                    ahri_new.ModifiedDate = ahri.ModifiedDate;

                    ahriService.Insert(ahri_new);
                    Log4NetService.LogInfo("Added " + ahri_new.Year + " - " + ahri_new.SiteId);
                }
            }
        }

        static void Update_KpiPurchaseOrderList()
        {
            try
            {
                Configuration configuration = new Configuration();
                Update_KpiPurchaseOrderList(configuration.GetValue(ConfigList.KpiPurchaseOrderListFilePath));
            }
            catch (Exception ex)
            {
                Log4NetService.LogInfo(ex.Message.ToString() + "\nServer Configuration Account Error. Contact Administrator.");
            }
            finally
            {
            }
        }
        
        static void Update_KpiPurchaseOrderList(string csvFilePath)
        {
            int i = 0;
            string startHeader = "\"Purchase Order Number\",\"Purchase Order Line Number\",\"Project Number\",\"Project Description\",\"Area\",\"Project Status\",\"Task Number\"";
            string startHeaderAlt = "Purchase Order Number,Purchase Order Line Number,Project Number,Project Description,Area,Project Status,Task Number";
            //2.1 Read from the actual Header of the CSV file. (Ignore junk at start of CSV file)
            string myFile = "";
            bool start = false;

            try
            {
                FileStream logFileStream = new FileStream(csvFilePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                StreamReader logFileReader = new StreamReader(logFileStream);

                while (!logFileReader.EndOfStream)
                {
                    string line = logFileReader.ReadLine();
                    if (line.Contains(startHeader) || line.Contains(startHeaderAlt)) { start = true; };
                    if (start == true)
                    {
                        myFile += line + "\n";
                        i++;
                    }
                }
                logFileReader.Close();
                logFileStream.Close();
            }
            catch (Exception ex)
            {
                Log4NetService.LogInfo(ex.Message);
            }

            if (!String.IsNullOrEmpty(myFile))
            {
                Log4NetService.LogInfo("CSV File Ok.");
                try
                {
                    using (CsvReader csv = new CsvReader(new StringReader(myFile), true))
                    {
                        int fieldCount = csv.FieldCount;
                        string[] headers = csv.GetFieldHeaders();
                        Log4NetService.LogInfo("Found Headers: " + String.Join(",", headers));

                        if (i > 0)
                        {
                            var kpolService = new KaiZen.CSMS.Services.KpiPurchaseOrderListService();
                            TList<KpiPurchaseOrderList> kpolTlist = kpolService.GetAll();
                            TList<KpiPurchaseOrderList> kpolTlist_new = new TList<KpiPurchaseOrderList>();

                            CultureInfo provider = CultureInfo.InvariantCulture;

                            while (csv.ReadNextRecord())
                            {
                                try
                                {
                                    KpiPurchaseOrderList kpol = new KpiPurchaseOrderList();
                                    //Mandatory
                                    kpol.PurchaseOrderNumber = csv["Purchase Order Number"].ToString();
                                    kpol.PurchaseOrderLineNumber = csv["Purchase Order Line Number"].ToString();
                                    kpol.ProjectNumber = csv["Project Number"].ToString();
                                    kpol.ProjectDesc = csv["Project Description"].ToString();
                                    kpol.Area = csv["Area"].ToString();
                                    kpol.ProjectStatus = csv["Project Status"].ToString();
                                    kpol.TaskNumber = csv["Task Number"].ToString();
                                    kpolTlist_new.Add(kpol);
                                }
                                catch (Exception ex)
                                {
                                    Log4NetService.LogInfo(ex.Message);
                                }
                            }

                            try
                            {
                                if (kpolTlist.Count > 0 && kpolTlist_new.Count > 0)
                                {
                                    try
                                    {
                                        kpolService.Delete(kpolTlist);
                                        kpolService.Reseed();
                                        Log4NetService.LogInfo("Deleting Current Data: SUCCESS");
                                    }
                                    catch (Exception ex)
                                    {
                                        Log4NetService.LogInfo("Deleting Current Data: FAIL.. " + ex.Message);
                                    }
                                }
                                try
                                {
                                    kpolService.Insert(kpolTlist_new);
                                    Log4NetService.LogInfo("Adding New Data: SUCCESS");
                                }
                                catch (Exception ex)
                                {
                                    Log4NetService.LogInfo("Adding New Data: FAIL.. " + ex.Message);
                                }
                            }
                            catch (Exception ex)
                            {
                                //rollback?
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Log4NetService.LogInfo(ex.Message);
                }
            }
        }

        static void Test()
        {
            bool success = false;

            Log4NetService.LogInfo("Test Mode");
            Log4NetService.LogInfo("--------------");
            Log4NetService.LogInfo("Have access to CSMS Database via Connection #1....................");

            try
            {
                var cService = new KaiZen.CSMS.Services.ConfigService();
                TList<Config> cList = cService.GetAll();
                if (cList.Count > 0)
                    success = true;
                else
                    success = false;
            }
            catch (Exception)
            {
                success = false;
            }

            if (success)
                Log4NetService.LogInfo("Yes\n");
            else
                Log4NetService.LogInfo("No\n");

            success = false;
            Log4NetService.LogInfo("Have access to CSMS Database via Connection #2....................");
            try
            {
                using (SqlConnection cn = new SqlConnection(sqldb))
                {
                    cn.Open();
                    SqlCommand cmd = new SqlCommand("SELECT COUNT(*) FROM Sites", cn);
                    cmd.ExecuteNonQuery();
                    cn.Close();
                }
                success = true;
            }
            catch (Exception)
            {
                success = false;
            }

            if (success)
                Log4NetService.LogInfo("Yes\n");
            else
                Log4NetService.LogInfo("No\n");

            success = true;
            Log4NetService.LogInfo("Have access to EBI Databases....................");
            try
            {
                SitesService sService = new SitesService();
                TList<Sites> sitesAll = sService.GetAll();
                foreach (Sites s in sitesAll)
                {
                    try
                    {
                        if (!String.IsNullOrEmpty(s.EbiServerName) && !String.IsNullOrEmpty(s.EbiTodayViewName))
                        {
                            string server = s.EbiServerName;
                            string view = s.EbiTodayViewName;

                            ConnectionStringSettings conString = ConfigurationManager.ConnectionStrings[String.Format("Alcoa.Ebi.{0}.ConnectionString", server)];
                            string strConnString = conString.ConnectionString;
                            using (SqlConnection cn = new SqlConnection(strConnString))
                            {
                                cn.Open();
                                Log4NetService.LogInfo("..Reading View: " + view + "...");
                                SqlCommand cmd = new SqlCommand("SELECT COUNT(*) FROM " + view, cn);
                                cmd.CommandTimeout = 60000;
                                SqlDataReader r = cmd.ExecuteReader();
                                cn.Close();
                            }
                        }
                    }
                    catch (Exception)
                    {
                        success = false;
                    }
                }
            }
            catch (Exception)
            {
                success = false;
            }

            if (success)
                Log4NetService.LogInfo("...............................................Yes\n");
            else
                Log4NetService.LogInfo("...............................................No\n");

            success = false;
            Log4NetService.LogInfo("Have Read/Write access to CSMS Network Directories...............");
            try
            {
                string file = @"\\aua.alcoa.com\dfs\Bgn\Teams\Contractor Management\17 CSMS Production Operational Files NEVER DELETE\test.txt";
                TextWriter tw = new StreamWriter(file);
                tw.WriteLine("test");
                tw.Close();
                File.Delete(file);
                success = true;
            }
            catch (Exception)
            {
                success = false;
            }

            if (success)
                Log4NetService.LogInfo("Yes\n");
            else
                Log4NetService.LogInfo("No\n");

            Log4NetService.LogInfo("\n\nTest Completed.");
            Console.ReadLine();
        }
    }
}
