using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using DevExpress.Web.ASPxGridView.Export;

using KaiZen.CSMS.Services;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

using System.Security.Principal;
using LumenWorks.Framework.IO.Csv;
using System.IO;

using System.Collections.Generic;

using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxTabControl;
using DevExpress.Web.Data;
using DevExpress.Web.ASPxGridView;

using System.Data.Sql;
using System.Data.SqlClient;
using System.Threading;


public partial class UserControls_ComplianceReport_EBI : System.Web.UI.UserControl
{
    Auth auth = new Auth();
    protected void Page_Load(object sender, EventArgs e) {  Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); }

    private void moduleInit(bool postBack)
    {
        SessionHandler.spVar_Questionnaire_FrontPageLink = "EbiContractorsOnSite";
        if (!postBack)
        { //first time load
            if (auth.RoleId == (int)RoleList.Administrator || Helper.General.isEbiUser(auth.UserId))
            {
                //all good.
                LoadData();
            }
            else
            {
                grid.SettingsText.EmptyDataRow = "You are not allowed to view this report. Please Contact the Contractor Services Management Team for assistance.";
                //Response.Redirect("AccessDenied.aspx?error=" + Server.UrlEncode("Unrecognised User."));
            }

            if (SessionHandler.spVar_Page == "ComplianceReportBySite")
            {
                if (Convert.ToInt32(SessionHandler.spVar_SiteId) > 0) //Site
                {
                    grid.Columns["Site Name"].Visible = false;
                }
                else
                {
                    grid.Columns["Site Name"].Visible = true;
                }
            }
        }
        else
        {
            grid.DataSource = (DataTable)Session["dtEbi"];
            grid.DataBind();
        }
    }

    private void LoadData()
    {
        WindowsImpersonationContext impContext = null;
        try { impContext = Helper.ImpersonateWAOCSMUser(); }
        catch (ApplicationException ex) { Console.Write(ex.Message); }

        try
        {
            if (null != impContext)
            {
                using (impContext)
                {
                    string myFile = "CompanyName,ClockId,FullName,AccessCardNo,Site,SwipeDateTime\n";
                    DateTime dtYesterday = DateTime.Now.AddDays(-1);
                    string FileName = String.Format("ContractorSwipes2_{0}-{1}-{2}.csv", dtYesterday.Day, dtYesterday.Month, dtYesterday.Year);
                    string FileFullPath = Helper.Config.GetValue(ConfigList.EbiTodayDir) + FileName;
                    string r = "";
                    r = ReadEbiFile(FileFullPath);
                    if (!String.IsNullOrEmpty(r))
                    {
                        myFile += r;
                        LoadData2(myFile);
                    }
                    //LoadData3(false);
                }
            }
        }
        catch (Exception ex)
        {
            Elmah.ErrorSignal.FromContext(Context).Raise(ex);
            Console.Write(ex.Message + "\nServer Configuration Account Error. Contact Administrator.");
        }
        finally
        {
            //if (impContext != null) //impContext.Undo();
        }
    }
    private void LoadData2(string myFile)
    {
        DataTable dtEbi = new DataTable();
        dtEbi.Columns.Add("CompanyName", typeof(string));
        dtEbi.Columns.Add("ClockId", typeof(string));
        dtEbi.Columns.Add("FullName", typeof(string));
        dtEbi.Columns.Add("AccessCardNo", typeof(string));
        dtEbi.Columns.Add("Site", typeof(string));
        dtEbi.Columns.Add("SwipeDateTime", typeof(string));
        dtEbi.Columns.Add("CompanyName-CSMS", typeof(string));

        CompaniesService cService = new CompaniesService();
        List<String> CompaniesSitesList = new List<String>();

        DataTable dtEbi2 = new DataTable();
        dtEbi2.Columns.Add("CompanyName", typeof(string));
        dtEbi2.Columns.Add("CompanyName-CSMS", typeof(string));
        dtEbi2.Columns.Add("Site", typeof(string));
        dtEbi2.Columns.Add("ApprovedOnSite", typeof(string));
        dtEbi2.Columns.Add("Valid", typeof(string));
        dtEbi2.Columns.Add("SqValidTill", typeof(string));
        dtEbi2.Columns.Add("CompanySQStatus", typeof(string));
        dtEbi2.Columns.Add("QuestionnaireId", typeof(string));
        dtEbi2.Columns.Add("Validity", typeof(string));
        dtEbi2.Columns.Add("Type", typeof(string));
        dtEbi2.Columns.Add("CompanySiteCategoryId", typeof(int));

        int i = 0;
        if (!String.IsNullOrEmpty(myFile))
        {
            Console.WriteLine("CSV File Ok.");
            using (CsvReader csv = new CsvReader(new StringReader(myFile), true))
            {
                string[] headers = csv.GetFieldHeaders();
                Console.WriteLine("Found Headers: " + String.Join(",", headers));
                i = 0; //justincase..

                SitesService sService = new SitesService();
                Sites sBooragoon = sService.GetBySiteAbbrev("BGN");
                Sites sMccoy = sService.GetBySiteAbbrev("HUN");
                Sites sPeel = sService.GetBySiteAbbrev("PNJ");
                Sites sPinjarra = sService.GetBySiteAbbrev("PIN");
                Sites sWagerup = sService.GetBySiteAbbrev("WGP");

                while (csv.ReadNextRecord())
                {
                    i++;
                    string CompanyName = csv["CompanyName"].ToString();
                    string ClockId = csv["ClockId"].ToString();
                    string FullName = csv["FullName"].ToString();
                    string AccessCardNo = csv["AccessCardNo"].ToString();
                    string Site = csv["Site"].ToString();
                    string SwipeDateTime = csv["SwipeDateTime"].ToString();
                    string CompanyNameCSMS = "";

                    int? CompanySiteCategoryId = null;

                    int CompanyId = -1;
                    int SiteId = -1;
                    if (Site == "BOORAGOON") SiteId = sBooragoon.SiteId;
                    if (Site == "MCCOY") SiteId = sMccoy.SiteId;
                    if (Site == "PEEL") SiteId = sPeel.SiteId;
                    if (Site == "PINJARRA") SiteId = sPinjarra.SiteId;
                    if (Site == "WAGERUP") SiteId = sWagerup.SiteId;
                    TList<Companies> cList = cService.GetByCompanyNameEbi(CompanyName);
                    if (cList != null)
                    {
                        if (cList.Count > 0)
                        {
                            if (cList.Count > 1)
                            {
                                int cListCount = 0;
                                foreach (Companies c in cList)
                                {
                                    if (cListCount != cList.Count)
                                    {
                                        CompanyNameCSMS += c.CompanyName + ", ";
                                        CompanyId = c.CompanyId;
                                    }
                                    else
                                    {
                                        CompanyNameCSMS += c.CompanyName;
                                        CompanyId = c.CompanyId;
                                    }
                                    cListCount++;
                                }
                            }
                            else
                            {
                                CompanyNameCSMS = cList[0].CompanyName;
                                CompanyId = cList[0].CompanyId;
                            }
                        }

                    }
                    dtEbi.Rows.Add(new object[] { CompanyName, ClockId, FullName, AccessCardNo, Site, SwipeDateTime, CompanyNameCSMS });
                    if (!CompaniesSitesList.Contains(String.Format("{0}|||||{1}", CompanyName, Site)))
                    {
                        CompaniesSitesList.Add(String.Format("{0}|||||{1}", CompanyName, Site));
                        string ApprovedOnSite = "";
                        string Valid = "";
                        string SqValidTill = "";
                        string CompanySQStatus = "";
                        string QuestionnaireId = "";
                        string Validity = "?";
                        string Type = "?";
                        if (CompanyId != -1)
                        {
                            CompaniesService cService2 = new CompaniesService();
                            Companies c = cService2.GetByCompanyId(CompanyId);
                            CompanyStatusService css = new CompanyStatusService();
                            CompanyStatus cs = css.GetByCompanyStatusId(c.CompanyStatusId);
                            CompanySQStatus = cs.CompanyStatusDesc;

                            int _count = 0;
                            TList<Questionnaire> qList = DataRepository.QuestionnaireProvider.GetPaged(String.Format("CompanyId = {0}", CompanyId), "CreatedDate DESC", 0, 9999, out _count);
                            if (_count > 0)
                            {
                                QuestionnaireId = qList[0].QuestionnaireId.ToString();

                                DateTime dtValidTo = DateTime.Now;
                                if (qList[0].MainAssessmentValidTo != null)
                                {
                                    SqValidTill = qList[0].MainAssessmentValidTo.ToString();
                                    dtValidTo = Convert.ToDateTime(qList[0].MainAssessmentValidTo);
                                }

                                if (String.IsNullOrEmpty(SqValidTill))
                                {
                                    if (_count > 1)
                                    {
                                        if (qList[1].MainAssessmentValidTo != null)
                                        {
                                            SqValidTill = qList[1].MainAssessmentValidTo.ToString();
                                            dtValidTo = Convert.ToDateTime(qList[1].MainAssessmentValidTo);
                                        }
                                    }
                                }
 
                                Valid = "No";

                                if (qList[0].SubContractor == true)
                                {
                                    Type = "Sub Contractor";
                                }
                                else
                                {
                                    Type = "Contractor";
                                }


                                CompanySiteCategoryStandardService cscsService = new CompanySiteCategoryStandardService();
                                
                                if (SiteId != -1)
                                {
                                    CompanySiteCategoryStandard cscs = cscsService.GetByCompanyIdSiteId(qList[0].CompanyId, SiteId);
                                    if (cscs != null)
                                    {
                                        if (cscs.Approved == true)
                                        {
                                            ApprovedOnSite = "Yes";
                                        }
                                        if (cscs.Approved == false)
                                        {
                                            ApprovedOnSite = "No";
                                        }
                                        if (cscs.Approved == null)
                                        {
                                            ApprovedOnSite = "Tentative";
                                        }
                                        CompanySiteCategoryId = cscs.CompanySiteCategoryId;
                                    }
                                }
                                Valid = "";
                                Validity = "?";

                                if (CompanySQStatus.StartsWith("Active") && qList[0].MainAssessmentValidTo > DateTime.Now && 
                                    qList[0].Status == (int)QuestionnaireStatusList.AssessmentComplete)
                                {
                                    Valid = "Yes";
                                }

                                if(!String.IsNullOrEmpty(SqValidTill))
                                {
                                    if (CompanySQStatus.StartsWith("Re-Qual") && dtValidTo > DateTime.Now)
                                    {
                                        Valid = "Yes (Previous SQ)";
                                    }
 
                                    if (DateTime.Now >= dtValidTo)
                                    {
                                        Validity = "Expired";
                                        Valid = "No";
                                    }
                                    if (dtValidTo > DateTime.Now )
                                    {
                                        TimeSpan span = dtValidTo - DateTime.Now;
                                        if (span.TotalDays <= 60)
                                        {
                                            Validity = "Expiring";
                                        }
                                        else
                                        {
                                            Validity = "Current";
                                        }
                                    }
                                }
                            }
                        }
                        
                        bool add = false;

                        if (CompanySiteCategoryId != null)
                        {
                            if (SessionHandler.spVar_CategoryId == CompanySiteCategoryId.ToString()) add = true;
                        }
                        else
                        {
                            add = true;
                        }

                        if (SessionHandler.spVar_CategoryId2 == "%") add = true;

                        if (Convert.ToInt32(SessionHandler.spVar_SiteId) > 0)
                        {
                            add = false;
                            if (SessionHandler.spVar_SiteId == SiteId.ToString()) add = true;
                        }
                        else
                        {
                            add = false;
                            RegionsSitesService rsService = new RegionsSitesService();
                            RegionsSites rs = rsService.GetBySiteIdRegionId(SiteId, (Convert.ToInt32(SessionHandler.spVar_SiteId) * -1));
                            if (rs != null) add = true;
                        }

                        if (add)
                        {
                            dtEbi2.Rows.Add(new object[] { CompanyName, CompanyNameCSMS, Site, ApprovedOnSite, Valid, SqValidTill, CompanySQStatus, QuestionnaireId, Validity, Type, CompanySiteCategoryId });
                        }
                    }
                }
            }
            Session["dtEbi"] = dtEbi2;
            grid.DataSource = (DataTable)Session["dtEbi"];
            grid.DataBind();
        }
    }

    private void LoadData3(bool live)
    {
        try
        {
            DataTable dtEbi = new DataTable();
            dtEbi.Columns.Add("CompanyName", typeof(string));
            dtEbi.Columns.Add("ClockId", typeof(string));
            dtEbi.Columns.Add("FullName", typeof(string));
            dtEbi.Columns.Add("AccessCardNo", typeof(string));
            dtEbi.Columns.Add("Site", typeof(string));
            dtEbi.Columns.Add("SwipeDateTime", typeof(string));
            dtEbi.Columns.Add("CompanyName-CSMS", typeof(string));

            CompaniesService cService = new CompaniesService();
            List<String> CompaniesSitesList = new List<String>();
            List<String> CompaniesSitesNullList = new List<String>();

            DataTable dtEbi2 = new DataTable();
            dtEbi2.Columns.Add("CompanyName", typeof(string));
            dtEbi2.Columns.Add("CompanyName-CSMS", typeof(string));
            dtEbi2.Columns.Add("Site", typeof(string));
            dtEbi2.Columns.Add("ApprovedOnSite", typeof(string));
            dtEbi2.Columns.Add("Valid", typeof(string));
            dtEbi2.Columns.Add("SqValidTill", typeof(string));
            dtEbi2.Columns.Add("CompanySQStatus", typeof(string));
            dtEbi2.Columns.Add("QuestionnaireId", typeof(string));
            dtEbi2.Columns.Add("Validity", typeof(string));
            dtEbi2.Columns.Add("Type", typeof(string));

            dtEbi2.Columns.Add("ProcurementContact", typeof(string));
            dtEbi2.Columns.Add("HSAssessor", typeof(string));

            //These values to take to compare..
            //SessionHandler.spVar_CompanyId = "";
            //SessionHandler.spVar_SiteId = sSite;
            //SessionHandler.spVar_SiteName = ddlSites.SelectedItem.Text;
            //SessionHandler.spVar_Year = sYear;
            //SessionHandler.spVar_CategoryId = sCategory;
            int i = 0;
            string strQuery;
            if (!live)
                //DT346 - changed EbiCompaniesSitesMap to EbiCompaniesMap as one was a view of the other
                strQuery = "Select * from EbiCompaniesMap where SwipeDay=" + DateTime.Now.Day + " and SwipeMonth=" + DateTime.Now.Month + " and SwipeYear=" + DateTime.Now.Year + " and DATEPART(hour, SwipeDateTime)<9 order by SwipeDateTime";                
            else
                strQuery = "Select * from EbiCompaniesMap where SwipeDay=" + DateTime.Now.Day + " and SwipeMonth=" + DateTime.Now.Month + " and SwipeYear=" + DateTime.Now.Year + " order by SwipeDateTime";

            ConnectionStringSettings conString = ConfigurationManager.ConnectionStrings["ALCOA_WAO_CSMWP_devConnectionString"];
            using (SqlConnection conn = new SqlConnection(conString.ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand(strQuery, conn))
                //using (SqlCommand cmd = new SqlCommand("Select Distinct Person_Id from EBS_HR..APPS.XXHR_SQL_CWK_V WHERE LAST_UPDATE_DATE_PER > Convert(datetime,'" + pollingdate + "',103) or LAST_UPDATE_DATE_ASS> Convert(datetime,'" + pollingdate + "',103)", conn))
                {

                    conn.Open();
                    cmd.CommandTimeout = 60000;

                    SqlDataReader csv = cmd.ExecuteReader();
                    //    }
                    //}
                    //if (!String.IsNullOrEmpty(myFile))
                    //{
                    //    Console.WriteLine("CSV File Ok.");
                    //    using (CsvReader csv = new CsvReader(new StringReader(myFile), true))
                    //    {
                    //        string[] headers = csv.GetFieldHeaders();
                    //        Console.WriteLine("Found Headers: " + String.Join(",", headers));
                    //        i = 0; //justincase..

                    SitesService sService = new SitesService();
                    TList<Sites> sList = sService.GetAll();

                    while (csv.Read())
                    {
                        i++;
                        int CompanyId = -1;
                        string CompanyNameCSMS = "";
                        string CompanyName = csv["EbiCompanyName"].ToString();
                        string ClockId = csv["ClockId"].ToString();
                        string FullName = csv["FullName"].ToString();
                        string AccessCardNo = csv["AccessCardNo"].ToString();
                        string Site = csv["SwipeSite"].ToString();
                        DateTime SwipeDateTime = DateTime.Now;
                        try
                        {
                            CompanyId = Convert.ToInt32(csv["CompanyId"]);
                        }
                        catch (Exception ex)
                        {

                        }
                        try
                        {
                            CompanyNameCSMS = Convert.ToString(csv["CompanyName"]);
                        }
                        catch (Exception ex)
                        { }
                        //string live = string.Empty;
                        //if (Request.QueryString["q"] == "live")
                        //{
                        //    live = "live";
                        //    SwipeDateTime = Convert.ToDateTime(csv["SwipeDateTime"]);
                        //}
                        //else
                        //{
                        //    live = "nonlive";
                        //    string[] dateArr = csv["SwipeDateTime"].ToString().Split('/');
                        //    string strDate = dateArr[1] + "/" + dateArr[0] + "/" + dateArr[2];
                        //    SwipeDateTime = Convert.ToDateTime(strDate);
                        //}


                        string ProcurementContact = "(n/a)";
                        string HSAssessor = "(n/a)";


                        int SiteId = -1;

                        Sites s = sList.Find(SitesColumn.SiteNameEbi, Site);
                        if (s != null) SiteId = s.SiteId;





                        //}
                        if (CompanyId != -1)
                        {
                            QuestionnaireWithLocationApprovalViewLatestService qwlavlService = new QuestionnaireWithLocationApprovalViewLatestService();
                            DataSet DSqwlavl = qwlavlService.GetProcurementContactHsAssessor_ByCompanyId(CompanyId);
                            if (DSqwlavl.Tables.Count > 0)
                            {
                                if (DSqwlavl.Tables[0].Rows.Count > 0)
                                {
                                    foreach (DataRow DRqwlavl in DSqwlavl.Tables[0].Rows)
                                    {
                                        ProcurementContact = DRqwlavl[0].ToString();
                                        HSAssessor = DRqwlavl[1].ToString();
                                    }
                                }
                            }
                        }

                        dtEbi.Rows.Add(new object[] { CompanyName, ClockId, FullName, AccessCardNo, Site, SwipeDateTime, CompanyNameCSMS });
                        if (CompaniesSitesList.Contains(String.Format("{0}|||||{1}", CompanyName, Site)) == false && CompanyId != -1)
                        {

                            CompaniesSitesList.Add(String.Format("{0}|||||{1}", CompanyName, Site));
                            string ApprovedOnSite = "";
                            string Valid = "";
                            string SqValidTill = "";
                            string CompanySQStatus = "";
                            string QuestionnaireId = "";
                            string Validity = "?";
                            string Type = "?";
                            if (CompanyId != -1)
                            {

                                CompaniesService cService2 = new CompaniesService();
                                Companies c = cService2.GetByCompanyId(CompanyId);
                                CompanyStatusService css = new CompanyStatusService();
                                CompanyStatus cs = css.GetByCompanyStatusId(c.CompanyStatusId);
                                CompanySQStatus = cs.CompanyStatusDesc;

                                int _count = 0;
                                VList<QuestionnaireWithLocationApprovalViewLatest> qwlavlList = DataRepository.QuestionnaireWithLocationApprovalViewLatestProvider.GetPaged(
                                                                            String.Format("CompanyId = {0}", CompanyId), "CreatedDate DESC", 0, 9999, out _count);

                                if (_count == 0)
                                {
                                    Valid = "6) No - Safety Qualification not found";
                                }
                                else
                                {
                                    QuestionnaireId = qwlavlList[0].QuestionnaireId.ToString();

                                    if (qwlavlList[0].SubContractor == true)
                                        Type = "Sub Contractor";
                                    else
                                        Type = "Contractor";

                                    CompanySiteCategoryStandardService cscsService = new CompanySiteCategoryStandardService();
                                    if (SiteId != -1)
                                    {
                                        CompanySiteCategoryStandard cscs = cscsService.GetByCompanyIdSiteId(qwlavlList[0].CompanyId, SiteId);
                                        if (cscs != null)
                                        {
                                            switch (cscs.Approved)
                                            {
                                                case true:
                                                    ApprovedOnSite = "Yes";
                                                    break;
                                                case false:
                                                    ApprovedOnSite = "No";
                                                    break;
                                                case null:
                                                    ApprovedOnSite = "Tentative";
                                                    break;
                                                default:
                                                    ApprovedOnSite = "";
                                                    break;
                                            }
                                        }
                                    }

                                    bool CompanyStatusAllowsOnSite = false;
                                    if (Type == "Contractor")
                                    {
                                        //Changed by Pankaj Dikshit for <Issue# 2411, 05-July-2012> - Append next 2nd condition in If
                                        if (c.CompanyStatusId == (int)CompanyStatusList.Active || c.CompanyStatusId == (int)CompanyStatusList.Active_No_PQ_Required_Restricted_Access_to_Site)
                                            CompanyStatusAllowsOnSite = true;
                                    }
                                    else
                                    {
                                        //Changed by Pankaj Dikshit for <Issue# 2411, 05-July-2012> - Append next 2nd condition in If
                                        if (c.CompanyStatusId == (int)CompanyStatusList.SubContractor || c.CompanyStatusId == (int)CompanyStatusList.Active_No_PQ_Required_Restricted_Access_to_Site)
                                            CompanyStatusAllowsOnSite = true;
                                    }


                                    if (qwlavlList[0].MainAssessmentValidTo != null)
                                    {
                                        DateTime dtValidTo = Convert.ToDateTime(qwlavlList[0].MainAssessmentValidTo.ToString());
                                        SqValidTill = dtValidTo.ToString("dd/MM/yyyy");

                                        if (DateTime.Now >= dtValidTo)
                                        {
                                            Validity = "Expired";
                                            Valid = "4) No - Safety Qualification Expired / Access to Site Not Granted";
                                        }
                                        else
                                        {
                                            TimeSpan span = dtValidTo - DateTime.Now;
                                            if (span.TotalDays <= 60)
                                            {
                                                if (c.CompanyStatusId == (int)CompanyStatusList.Requal_Incomplete) CompanyStatusAllowsOnSite = true;

                                                if (ApprovedOnSite == "Yes" && CompanyStatusAllowsOnSite)
                                                    Valid = "2) Yes - Safety Qualification Current (expiring in 60 days) / Access to Site Granted";
                                                else
                                                    Valid = "5) No - Access to Site Not Granted";
                                            }
                                            else
                                            {
                                                Validity = "Current";
                                                if (ApprovedOnSite == "Yes" && CompanyStatusAllowsOnSite)
                                                    Valid = "1) Yes - Safety Qualification Current / Access to Site Granted";
                                                else
                                                    Valid = "5) No - Access to Site Not Granted";
                                            }
                                        }
                                    }
                                    else
                                    {
                                        Valid = "3) No - Current Safety Qualification not found / Access to Site Not Granted";
                                    }
                                }

                                SqExemptionService sqeService = new SqExemptionService();
                                TList<SqExemption> sqeList = sqeService.GetByCompanyId(CompanyId);
                                sqeList = sqeList.FindAll(SqExemptionColumn.SiteId, SiteId);
                                if (sqeList.Count > 0)
                                {
                                    foreach (SqExemption sq in sqeList)
                                    {
                                        if (sq.ValidTo > DateTime.Now)
                                        {
                                            Valid = "8) Safety Qualification Exemption Exists";
                                            break;
                                        }
                                    }
                                }

                            }
                            else
                            {
                                Valid = "7) No - Information could not be found (Check that Company exists in CSMS)";
                            }
                            //if (!String.IsNullOrEmpty(CompanyName) && !String.IsNullOrEmpty(CompanyNameCSMS))
                            if (!String.IsNullOrEmpty(CompanyName))
                            {
                                dtEbi2.Rows.Add(new object[] { CompanyName, CompanyNameCSMS, Site, ApprovedOnSite, Valid, SqValidTill, CompanySQStatus, QuestionnaireId, Validity, Type, 
                            ProcurementContact,  HSAssessor});
                            }
                        }
                        else
                        {
                            if (CompanyId == -1 && CompaniesSitesNullList.Contains(String.Format("{0}|||||{1}", CompanyName, Site)) == false)
                            {

                                CompaniesSitesNullList.Add(String.Format("{0}|||||{1}", CompanyName, Site));

                            }

                        }
                    }
                }
                foreach (string str in CompaniesSitesNullList)
                {
                    if (!CompaniesSitesList.Contains(str))
                    {
                        string[] nullList = str.Split('|');
                        string CompanyName = nullList[0];
                        string Site = nullList[5];
                        dtEbi2.Rows.Add(new object[] { CompanyName, "", Site, "", "7) No - Information could not be found (Check that Company exists in CSMS)", "", "", "", "", "?", 
                            "",  ""});

                    }

                }
                Session["dtEbi"] = dtEbi2;
                grid.DataSource = (DataTable)Session["dtEbi"];
                grid.DataBind();
            }
        }
        catch (Exception ex)
        {

            throw new Exception();
        }
    }

    static string ReadEbiFile(string FileFullPath)
    {
        Console.WriteLine("Reading file: " + FileFullPath);
        string myFile = "";
        try
        {
            using (FileStream logFileStream = new FileStream(FileFullPath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            {
                StreamReader logFileReader = new StreamReader(logFileStream);
                while (!logFileReader.EndOfStream)
                {
                    string line = logFileReader.ReadLine();
                    myFile += line + "\n";
                }
                logFileReader.Close();
                logFileStream.Close();
            }
            return myFile;
        }
        catch (Exception ex)
        {
            Elmah.ErrorSignal.FromCurrentContext();
            Console.WriteLine("[ERROR] " + ex.Message);
        }
        return "";
    }

    protected void grid_RowCreated(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewTableRowEventArgs e)
    {
        if (e.RowType != DevExpress.Web.ASPxGridView.GridViewRowType.Data) return;
        Image img = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "changeImage") as Image;
        if (img != null)
        {
            img.Visible = false;

            if (grid.GetRowValues(e.VisibleIndex, "Valid") != null)
            {
                string Valid = grid.GetRowValues(e.VisibleIndex, "Valid").ToString();
                if (Valid == "Yes")
                {
                    img.Visible = true;
                    img.ImageUrl = "~/Images/greentick.gif";
                }
                if (Valid == "Yes (Previous SQ)")
                {
                    img.Visible = true;
                    img.ImageUrl = "~/Images/TrafficLights/tlYellow.gif";
                }
                if (Valid == "No")
                {
                    img.Visible = true;
                    img.ImageUrl = "~/Images/redcross.gif";
                }
            }
        }

        HyperLink hl = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "hlq") as HyperLink;
        Label lb = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "lbq") as Label;
        if (hl != null && lb != null)
        {

            if (grid.GetRowValues(e.VisibleIndex, "CompanyName-CSMS") != null)
            {
                lb.Visible = true;
                lb.Text = grid.GetRowValues(e.VisibleIndex, "CompanyName-CSMS").ToString();

                if (grid.GetRowValues(e.VisibleIndex, "QuestionnaireId") != null)
                {
                    string QuestionnaireId = grid.GetRowValues(e.VisibleIndex, "QuestionnaireId").ToString();
                    if (!String.IsNullOrEmpty(QuestionnaireId))
                    {
                        lb.Visible = false;
                        hl.Visible = true;
                        hl.Text = grid.GetRowValues(e.VisibleIndex, "CompanyName-CSMS").ToString();
                        hl.NavigateUrl = String.Format("~/SafetyPQ_QuestionnaireModify.aspx{0}", QueryStringModule.Encrypt("q=" + QuestionnaireId));

                    }
                }
            }
        }
        //if (e.RowType != DevExpress.Web.ASPxGridView.GridViewRowType.Data) return;
        //Image img = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "changeImage") as Image;
        //if (img != null)
        //{
        //    img.Visible = false;

        //    if (grid.GetRowValues(e.VisibleIndex, "Valid") != null)
        //    {
        //        string Valid = grid.GetRowValues(e.VisibleIndex, "Valid").ToString();

        //        switch (Valid)
        //        {
        //            case "1) Yes - Safety Qualification Current / Access to Site Granted":
        //                img.Visible = true;
        //                img.ImageUrl = "~/Images/greentick.gif";
        //                break;
        //            case "2) Yes - Safety Qualification Current (expiring in 60 days) / Access to Site Granted":
        //                img.Visible = true;
        //                img.ImageUrl = "~/Images/TrafficLights/tlYellow.gif";
        //                break;
        //            case "3) No - Current Safety Qualification not found / Access to Site Not Granted":
        //                img.Visible = true;
        //                img.ImageUrl = "~/Images/TrafficLights/tlRed.gif";
        //                break;
        //            case "4) No - Safety Qualification Expired / Access to Site Not Granted":
        //                img.Visible = true;
        //                img.ImageUrl = "~/Images/TrafficLights/tlRed.gif";
        //                break;
        //            case "5) No - Access to Site Not Granted":
        //                img.Visible = true;
        //                img.ImageUrl = "~/Images/TrafficLights/tlRed.gif";
        //                break;
        //            case "6) No - Safety Qualification not found":
        //                img.Visible = true;
        //                img.ImageUrl = "~/Images/redcross.gif";
        //                break;
        //            case "7) No - Contractor company information could not be found (Check that Company exists in CSMS)":
        //                img.Visible = true;
        //                img.ImageUrl = "~/Images/redcross.gif";
        //                break;
        //            case "8) No - Subcontractor company information could not be found (Check that Company exists in CSMS)":
        //                img.Visible = true;
        //                img.ImageUrl = "~/Images/redcross.gif";
        //                break;
        //            case "9) Safety Qualification Exemption Exists":
        //                img.Visible = true;
        //                img.ImageUrl = "~/Images/TrafficLights/tlPurple.gif";
        //                break;
        //            default:
        //                break;
        //        }
        //        img.ToolTip = Valid;
        //    }
        //}

        //HyperLink hl = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "hlq") as HyperLink;
        //Label lb = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "lbq") as Label;
        //if (hl != null && lb != null)
        //{

        //    if (grid.GetRowValues(e.VisibleIndex, "CompanyName-CSMS") != null)
        //    {
        //        lb.Visible = true;
        //        lb.Text = grid.GetRowValues(e.VisibleIndex, "CompanyName-CSMS").ToString();

        //        if (grid.GetRowValues(e.VisibleIndex, "QuestionnaireId") != null)
        //        {
        //            string QuestionnaireId = grid.GetRowValues(e.VisibleIndex, "QuestionnaireId").ToString();
        //            if (!String.IsNullOrEmpty(QuestionnaireId))
        //            {
        //                lb.Visible = false;
        //                hl.Visible = true;
        //                hl.Text = grid.GetRowValues(e.VisibleIndex, "CompanyName-CSMS").ToString();
        //                hl.NavigateUrl = String.Format("~/SafetyPQ_QuestionnaireModify.aspx{0}", QueryStringModule.Encrypt("q=" + QuestionnaireId));

        //            }
        //        }
        //    }
        //}

        //HyperLink hl2 = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "hlebi") as HyperLink;
        //Label lb2 = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "lblebi") as Label;
        //if (hl2 != null && lb2 != null)
        //{
        //    string CompanyNameEbi = "???";
        //    if (grid.GetRowValues(e.VisibleIndex, "CompanyName") != null)
        //    {
        //        CompanyNameEbi = grid.GetRowValues(e.VisibleIndex, "CompanyName").ToString();
        //    }

        //    lb2.Visible = true;
        //    lb2.Text = CompanyNameEbi;

        //    bool NotValid = false;

        //    if (grid.GetRowValues(e.VisibleIndex, "Valid") != null)
        //    {
        //        if (String.IsNullOrEmpty(grid.GetRowValues(e.VisibleIndex, "Valid").ToString())) NotValid = true;
        //    }
        //    else
        //    {
        //        NotValid = true;
        //    }

        //    if (NotValid)
        //    {
        //        DateTime dt = DateTime.Now.AddDays(-1);
        //        string SiteName = grid.GetRowValues(e.VisibleIndex, "Site").ToString();

        //        lb2.Visible = false;
        //        hl2.Visible = true;
        //        hl2.Text = CompanyNameEbi;
        //        hl2.NavigateUrl = String.Format("javascript:popUp('PopUps/Ebi_CompanyEmployeesOnSite.aspx{0}');",
        //            QueryStringModule.Encrypt(String.Format("EbiCompanyName={0}&EbiSiteName={1}", Server.UrlEncode((CompanyNameEbi.Replace("???", "")).Replace("&", "[ampersand]")), SiteName)));


        //    }
        //}
    }
    protected void btnReset_Click(object sender, EventArgs e)
    {
        //string a = grid.SaveClientLayout();
        //Label1.Text = a;
        grid.LoadClientLayout("version0.3|page1|sort3|a0|d4|a1|visible10|t0|t1|t2|t3|t4|t5|t6|t7|t8|f-1|width10|78px|108px|105px|70px|71px|83px|70px|150px|e|e");
    }

    protected void grid_CellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
    {
        if (e.Column.FieldName == "Validity")
        {
            ASPxComboBox combo = (ASPxComboBox)e.Editor;
            combo.DataBindItems();

            ListEditItem item = new ListEditItem(string.Empty, null);
            combo.Items.Insert(0, item);
        }
    } 
}
