﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Repo.CSMS.Common.Mvc
{
    /// <summary>
    /// This attribute lets you add "must be true" attribute to a checkbox.  It must be clicked before submission.
    /// Note: Please add the following to the pages JScript for this to work:
    ///         jQuery.validator.unobtrusive.adapters.add("checkbox", function (options) {
    ///             if (options.element.tagName.toUpperCase() == "INPUT" && options.element.type.toUpperCase() == "CHECKBOX") {
    ///                 options.rules["required"] = true;
    ///                 if (options.message) {
    ///                     options.messages["required"] = options.message;
    ///                 }
    ///             }
    ///         });
    /// </summary>
    public class MustBeTrueAttribute : ValidationAttribute, IClientValidatable // IClientValidatable for client side Validation
    {
        public override bool IsValid(object value)
        {
            return value is bool && (bool)value;
        }
        // Implement IClientValidatable for client side Validation
        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            return new ModelClientValidationRule[] { new ModelClientValidationRule { ValidationType = "checkbox", ErrorMessage = this.ErrorMessage } };
        }
    }
}
