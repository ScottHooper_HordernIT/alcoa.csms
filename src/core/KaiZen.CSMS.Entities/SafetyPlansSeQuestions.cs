﻿#region Using directives

using System;

#endregion

namespace KaiZen.CSMS.Entities
{	
	///<summary>
	/// An object representation of the 'SafetyPlans_SE_Questions' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class SafetyPlansSeQuestions : SafetyPlansSeQuestionsBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="SafetyPlansSeQuestions"/> instance.
		///</summary>
		public SafetyPlansSeQuestions():base(){}	
		
		#endregion
	}
}
