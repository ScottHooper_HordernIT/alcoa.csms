<%@ Control Language="C#" AutoEventWireup="true"
    Inherits="UserControls_SqQuestionnaire_QuestionnaireList" Codebehind="QuestionnaireList.ascx.cs" %>
<%@ Register Src="../../Other/ExportButtons.ascx" TagName="ExportButtons" TagPrefix="uc1" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="System.Web.Extensions" Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallbackPanel" TagPrefix="dxcp" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dxp" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <table style="width: 874px" cellspacing="0" cellpadding="0" border="0">
            <tbody>
                <tr>
                    <td style="width: 874px; padding-top: 2px; text-align: center" class="pageName" align="right">
                        <div align="right">
                            <a style="color: #0000ff; text-align: right; text-decoration: none" href="javascript:ShowHideCustomizationWindow()">
                                Customize</a>
                        </div>
                    </td>
                </tr>
                <tr style="padding-top: 2px">
                    <td style="height: 13px; text-align: left" colspan="2">
                        <dxwgv:ASPxGridView ID="grid" runat="server" CssPostfix="Office2010BlueNew" CssFilePath="~/App_Themes/Office2010BlueNew/{0}/styles.css"
                            Width="874px" OnHtmlRowCreated="grid_RowCreated" KeyFieldName="QuestionnaireId"
                            ClientInstanceName="grid" AutoGenerateColumns="False">
                            <SettingsCustomizationWindow Enabled="True"></SettingsCustomizationWindow>
                            <SettingsBehavior ColumnResizeMode="Control"></SettingsBehavior>
                            <ImagesFilterControl >
                                <LoadingPanel Url="~/App_Themes/Office2003BlueOld/Editors/Loading.gif"> 
                                </LoadingPanel>
                            </ImagesFilterControl>
                            <Styles CssPostfix="Office2010BlueNew" CssFilePath="~/App_Themes/Office2010BlueNew/{0}/styles.css">
                                <Header SortingImageSpacing="5px" ImageSpacing="5px">
                                </Header>
                                <AlternatingRow Enabled="True">
                                </AlternatingRow>
                                <LoadingPanel ImageSpacing="10px">
                                </LoadingPanel>
                            </Styles>
                            <SettingsPager>
                                <AllButton Visible="True">
                                </AllButton>
                            </SettingsPager>
                            <Images SpriteCssFilePath="~/App_Themes/Office2010BlueNew/{0}/sprite.css"> 
                                <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
                                </LoadingPanelOnStatusBar>
                                <LoadingPanel Url="~/App_Themes/Office2003BlueOld/GridView/Loading.gif"> <%--Enhancement_023:change by debashis for testing platform upgradation--%>
                                </LoadingPanel>
                            </Images>
                            <SettingsText EmptyDataRow="You do not have any incomplete Questionnaires."></SettingsText>
                            <Columns>
                                <dxwgv:GridViewCommandColumn VisibleIndex="2" Visible="False">
                                    <ClearFilterButton Visible="True">
                                    </ClearFilterButton>
                                </dxwgv:GridViewCommandColumn>
                                <dxwgv:GridViewDataTextColumn Caption=" " FieldName="QuestionnaireId" FixedStyle="Left"
                                    ReadOnly="True" VisibleIndex="0" Width="57px">
                                    <Settings AllowHeaderFilter="False" AllowAutoFilter="False" />
                                    <EditFormSettings Visible="False" />
                                    <Settings AllowHeaderFilter="False" AllowAutoFilter="False" /><EditFormSettings Visible="False" /><DataItemTemplate>
                                        <asp:HyperLink ID="hlAssess" runat="server" Text="Assess/Review" NavigateUrl=""></asp:HyperLink>
                                    </DataItemTemplate>
                                </dxwgv:GridViewDataTextColumn>
                                <dxwgv:GridViewDataTextColumn Caption="Company" FieldName="CompanyName" FixedStyle="Left"
                                    SortIndex="0" SortOrder="Ascending" VisibleIndex="1" Width="205px">
                                    <Settings SortMode="DisplayText" /><HeaderStyle HorizontalAlign="Center" Wrap="True" />
                                </dxwgv:GridViewDataTextColumn>
                                <dxwgv:GridViewDataTextColumn Caption="Company Status" FieldName="CompanyStatusDesc" Visible="false" Width="185px">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <CellStyle HorizontalAlign="Left">
                                    </CellStyle>
                                </dxwgv:GridViewDataTextColumn>
                                <dxwgv:GridViewDataTextColumn Caption="Type" FieldName="Type" VisibleIndex="6" Width="120px">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <CellStyle HorizontalAlign="Left">
                                    </CellStyle>
                                </dxwgv:GridViewDataTextColumn>
                                 <dxwgv:GridViewDataTextColumn FieldName="UserDescription" Caption="Presently With" Visible="false" Width="110px">
                                    <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                                </dxwgv:GridViewDataTextColumn>
                                 <dxwgv:GridViewDataTextColumn FieldName="ActionDescription" 
                                    Caption="Presently With Details"  VisibleIndex="4" Width="110px">
                                    <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                                </dxwgv:GridViewDataTextColumn>
                                 <dxwgv:GridViewDataTextColumn 
                                    FieldName="QuestionnairePresentlyWithSinceDaysCount" Caption="Days With" VisibleIndex="2" 
                                                                    Width="60px">
                                    <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                                </dxwgv:GridViewDataTextColumn>
                                <dxwgv:GridViewDataTextColumn Caption="Created By" FieldName="CreatedByUser"
                                    Name="UserBox" VisibleIndex="6" Width="150px">
                                    <Settings SortMode="DisplayText" />
                                    <EditFormSettings Visible="True" />
                                    <Settings SortMode="DisplayText" /><EditFormSettings Visible="True" /><HeaderStyle HorizontalAlign="Center" />
                                </dxwgv:GridViewDataTextColumn>
                                <dxwgv:GridViewDataTextColumn Caption="Last Modified By" FieldName="ModifiedByUser"
                                    Name="UserBox" VisibleIndex="7" Width="150px">
                                    <Settings SortMode="DisplayText" />
                                    <EditFormSettings Visible="True" />
                                    <Settings SortMode="DisplayText" /><EditFormSettings Visible="True" /><HeaderStyle HorizontalAlign="Center" />
                                </dxwgv:GridViewDataTextColumn>
                                <dxwgv:GridViewDataDateColumn Caption="Last Modified At" FieldName="ModifiedDate"
                                    SortIndex="1" SortOrder="Descending" VisibleIndex="4" Width="130px">
                                    <PropertiesDateEdit DisplayFormatString="dd/MM/yy HH:mm">
                                    </PropertiesDateEdit>
                                    <Settings AutoFilterCondition="Contains" />
                                    <Settings AutoFilterCondition="Contains" /><HeaderStyle HorizontalAlign="Center" />
                                </dxwgv:GridViewDataDateColumn>
                                
                                <dxwgv:GridViewDataTextColumn Caption="Procurement Risk Rating" FieldName="InitialRiskAssessment"
                                    VisibleIndex="6" Width="115px">
                                    <HeaderStyle HorizontalAlign="Center" Wrap="True" />
                                </dxwgv:GridViewDataTextColumn>
                                <dxwgv:GridViewDataTextColumn Caption="AssessmentRiskRating" FieldName="MainAssessmentRiskRating"
                                    ShowInCustomizationForm="False" Visible="False" Width="100px">
                                    <HeaderStyle HorizontalAlign="Center" />
                                </dxwgv:GridViewDataTextColumn>
                                <dxwgv:GridViewDataTextColumn Caption="33.055.1 Risk Rating" FieldName="MainAssessmentRiskRating"
                                    VisibleIndex="9" Width="100px">
                                    <DataItemTemplate>
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:HyperLink ID="RiskLevel" runat="server" Text="" NavigateUrl=""></asp:HyperLink>
                                                </td>
                                            </tr>
                                        </table>
                                    </DataItemTemplate>
                                    <HeaderStyle HorizontalAlign="Center" Wrap="True" />
                                </dxwgv:GridViewDataTextColumn>
                                <dxwgv:GridViewDataTextColumn Caption="Final Risk Rating" Name="FinalRiskRating"
                                    VisibleIndex="8" Width="115px">
                                    <DataItemTemplate>
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="FinalRiskLevel" runat="server" Text=""></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </DataItemTemplate>
                                    <HeaderStyle HorizontalAlign="Center" Wrap="True" />
                                </dxwgv:GridViewDataTextColumn>
                                <dxwgv:GridViewDataComboBoxColumn FieldName="Status" Name="Status" UnboundType="String"
                                    VisibleIndex="10" Width="130px">
                                    <PropertiesComboBox DataSourceID="dsQuestionnaireStatus" DropDownHeight="150px" TextField="QuestionnaireStatusDesc"
                                        ValueField="QuestionnaireStatusId" ValueType="System.String">
                                    </PropertiesComboBox>
                                </dxwgv:GridViewDataComboBoxColumn>
                                <dxwgv:GridViewDataComboBoxColumn FieldName="Recommended" VisibleIndex="11" 
                                    Width="105px">
                                    <PropertiesComboBox ValueType="System.Int32">
                                        <Items>
                                            <dxe:ListEditItem Text="Yes" Value="1">
                                            </dxe:ListEditItem>
                                            <dxe:ListEditItem Text="No" Value="0">
                                            </dxe:ListEditItem>
                                        </Items>
                                        <ItemStyle Font-Bold="True" />
                                    </PropertiesComboBox>
                                    <Settings SortMode="DisplayText" />
                                    <Settings SortMode="DisplayText" /><HeaderStyle HorizontalAlign="Center" />
                                    <CellStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                    </CellStyle>
                                </dxwgv:GridViewDataComboBoxColumn>
                                <dxwgv:GridViewDataComboBoxColumn Caption="Safety Assessor" FieldName="SafetyAssessor"
                                    Name="EHS Consultant" UnboundType="String" VisibleIndex="10" Width="125px">
                                    <PropertiesComboBox ValueType="System.String">
                                    </PropertiesComboBox>
                                </dxwgv:GridViewDataComboBoxColumn>
                                <dxwgv:GridViewDataTextColumn Caption="Procurement Contact" FieldName="ProcurementContactUser"
                                    VisibleIndex="1" Width="160px" Settings-SortMode="DisplayText">
                                    <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                                <settings sortmode="DisplayText" /></dxwgv:GridViewDataTextColumn>
                                <dxwgv:GridViewDataTextColumn Caption="Procurement Functional Supervisor" FieldName="ContractManagerUser"
                                    VisibleIndex="3" Width="160px" Settings-SortMode="DisplayText">
                                    <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                                    <Settings AllowAutoFilter="True" />
                                <Settings AllowAutoFilter="True" /></dxwgv:GridViewDataTextColumn>
                                <dxwgv:GridViewDataTextColumn Caption="KWI" FieldName="KWI" ToolTip="Approved to work at Kwinana?"
                                    VisibleIndex="12" Width="40px" Visible="false" ShowInCustomizationForm="true">
                                    <Settings AllowHeaderFilter="False" AllowSort="False" AllowAutoFilter="False" /><DataItemTemplate>
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblKWI" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </DataItemTemplate>
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <CellStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                    </CellStyle>
                                </dxwgv:GridViewDataTextColumn>
                                <dxwgv:GridViewDataTextColumn Caption="PIN" FieldName="PIN" ToolTip="Approved to work at Pinjarra?"
                                    VisibleIndex="13" Width="40px" Visible="false" ShowInCustomizationForm="true">
                                    <Settings AllowHeaderFilter="False" AllowSort="False" AllowAutoFilter="False" /><DataItemTemplate>
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblPIN" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </DataItemTemplate>
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <CellStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                    </CellStyle>
                                </dxwgv:GridViewDataTextColumn>
                                <dxwgv:GridViewDataTextColumn Caption="WGP" FieldName="WGP" ToolTip="Approved to work at Wagerup?"
                                    VisibleIndex="14" Width="40px" Visible="false" ShowInCustomizationForm="true">
                                    <Settings AllowHeaderFilter="False" AllowSort="False" AllowAutoFilter="False" /><DataItemTemplate>
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblWGP" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </DataItemTemplate>
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <CellStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                    </CellStyle>
                                </dxwgv:GridViewDataTextColumn>
                                <dxwgv:GridViewDataTextColumn Caption="HUN" FieldName="HUN" ToolTip="Approved to work at Huntly (Mine)?"
                                    VisibleIndex="15" Width="40px" Visible="false" ShowInCustomizationForm="true">
                                    <Settings AllowHeaderFilter="False" AllowSort="False" AllowAutoFilter="False" /><DataItemTemplate>
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblHUN" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </DataItemTemplate>
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <CellStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                    </CellStyle>
                                </dxwgv:GridViewDataTextColumn>
                                <dxwgv:GridViewDataTextColumn Caption="WDL" FieldName="WDL" ToolTip="Approved to work at Willowdale (Mine)?"
                                    VisibleIndex="16" Width="40px" Visible="false" ShowInCustomizationForm="true">
                                    <Settings AllowHeaderFilter="False" AllowSort="False" AllowAutoFilter="False" /><DataItemTemplate>
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblWDL" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </DataItemTemplate>
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <CellStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                    </CellStyle>
                                </dxwgv:GridViewDataTextColumn>
                                <dxwgv:GridViewDataTextColumn Caption="BUN" FieldName="BUN" ToolTip="Approved to work at Bunbury Port?"
                                    VisibleIndex="17" Width="40px" Visible="false" ShowInCustomizationForm="true">
                                    <Settings AllowHeaderFilter="False" AllowSort="False" AllowAutoFilter="False" /><DataItemTemplate>
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblBUN" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </DataItemTemplate>
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <CellStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                    </CellStyle>
                                </dxwgv:GridViewDataTextColumn>
                                <dxwgv:GridViewDataTextColumn Caption="FML" FieldName="FML" ToolTip="Approved to work at Farmlands?"
                                    VisibleIndex="18" Width="40px" Visible="false" ShowInCustomizationForm="true">
                                    <Settings AllowHeaderFilter="False" AllowSort="False" AllowAutoFilter="False" /><DataItemTemplate>
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblFML" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </DataItemTemplate>
                                    <HeaderStyle HorizontalAlign="Center" />
                                </dxwgv:GridViewDataTextColumn>
                                <dxwgv:GridViewDataTextColumn Caption="BGN" FieldName="BGN" ToolTip="Approved to work at Booragoon Office?"
                                    VisibleIndex="19" Width="40px" Visible="false" ShowInCustomizationForm="true">
                                    <Settings AllowHeaderFilter="False" AllowSort="False" AllowAutoFilter="False" /><DataItemTemplate>
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblBGN" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </DataItemTemplate>
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <CellStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                    </CellStyle>
                                </dxwgv:GridViewDataTextColumn>
                                <dxwgv:GridViewDataTextColumn Caption="ANG" FieldName="ANG" ToolTip="Approved to work at Anglesea?"
                                    VisibleIndex="20" Width="40px" Visible="false" ShowInCustomizationForm="true">
                                    <Settings AllowHeaderFilter="False" AllowSort="False" AllowAutoFilter="False" /><DataItemTemplate>
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblANG" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </DataItemTemplate>
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <CellStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                    </CellStyle>
                                </dxwgv:GridViewDataTextColumn>
                                <dxwgv:GridViewDataTextColumn Caption="PTL" FieldName="PTL" ToolTip="Approved to work at Portland?"
                                    VisibleIndex="21" Width="40px" Visible="false" ShowInCustomizationForm="true">
                                    <Settings AllowHeaderFilter="False" AllowSort="False" AllowAutoFilter="False" /><DataItemTemplate>
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblPTL" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </DataItemTemplate>
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <CellStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                    </CellStyle>
                                </dxwgv:GridViewDataTextColumn>
                                <dxwgv:GridViewDataTextColumn Caption="PTH" FieldName="PTH" ToolTip="Approved to work at Point Henry?"
                                    VisibleIndex="22" Width="40px" Visible="false" ShowInCustomizationForm="true">
  
                                    <Settings AllowHeaderFilter="False" AllowSort="False" AllowAutoFilter="False" /><DataItemTemplate>
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblPTH" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </DataItemTemplate>
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <CellStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                    </CellStyle>
                                </dxwgv:GridViewDataTextColumn>
                                <dxwgv:GridViewDataTextColumn Caption="PEEL" FieldName="PEL" ToolTip="Approved to work at Peel Office?"
                                    VisibleIndex="23" Width="50px" Visible="false" ShowInCustomizationForm="true">
                                    <Settings AllowHeaderFilter="False" AllowSort="False" AllowAutoFilter="False" /><DataItemTemplate>
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblPEEL" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </DataItemTemplate>
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <CellStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                    </CellStyle>
                                </dxwgv:GridViewDataTextColumn>
                                <dxwgv:GridViewDataTextColumn Caption="ARP" FieldName="ARP" ToolTip="Approved to work at ARP Point Henry?"
                                    VisibleIndex="24" Width="50px" Visible="false" ShowInCustomizationForm="true">
                                    <Settings AllowHeaderFilter="False" AllowSort="False" AllowAutoFilter="False" /><DataItemTemplate>
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblARP" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </DataItemTemplate>
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <CellStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                    </CellStyle>
                                </dxwgv:GridViewDataTextColumn>
                                <dxwgv:GridViewDataTextColumn Caption="YEN" FieldName="YEN" ToolTip="Approved to work at Yennora?"
                                    VisibleIndex="25" Width="50px" Visible="false" ShowInCustomizationForm="true">
                                    <Settings AllowHeaderFilter="False" AllowSort="False" AllowAutoFilter="False" /><DataItemTemplate>
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblYEN" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </DataItemTemplate>
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <CellStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                    </CellStyle>
                                </dxwgv:GridViewDataTextColumn>
                                <dxwgv:GridViewDataComboBoxColumn Caption="Initial Status" FieldName="InitialStatus"
                                    ShowInCustomizationForm="False" Visible="False" VisibleIndex="18" Width="100px">
                                    <PropertiesComboBox DataSourceID="dsQuestionnaireStatus" TextField="QuestionnaireStatusDesc"
                                        ValueField="QuestionnaireStatusId" ValueType="System.Int32">
                                    </PropertiesComboBox>
                                </dxwgv:GridViewDataComboBoxColumn>
                                <dxwgv:GridViewDataCheckColumn FieldName="ProcurementModified" Visible="False">
                                </dxwgv:GridViewDataCheckColumn>
                                <dxwgv:GridViewDataCheckColumn FieldName="SubContractor" Visible="False" VisibleIndex="21">
                                </dxwgv:GridViewDataCheckColumn>
                                <%--<dxwgv:GridViewDataTextColumn Caption="# Expiry E-Mails Sent" FieldName="NoSqExpiryEmailsSent"
                                    VisibleIndex="999" Width="160px" Visible="true" ShowInCustomizationForm="true">
                                    <Settings AllowHeaderFilter="True" AllowSort="True" AllowAutoFilter="True" />
                                </dxwgv:GridViewDataTextColumn>--%>
                                <dxwgv:GridViewDataComboBoxColumn Caption="Safety Assessor Region" FieldName="SafetyAssessorRegion"
                                    UnboundType="String" VisibleIndex="10" Visible="false">
                                    <PropertiesComboBox ValueType="System.String">
                                    </PropertiesComboBox>
                                </dxwgv:GridViewDataComboBoxColumn>
                                <dxwgv:GridViewDataComboBoxColumn Caption="Procurement Region" FieldName="ProcurementRegion"
                                    UnboundType="String" VisibleIndex="10" Visible="false">
                                    <PropertiesComboBox ValueType="System.String">
                                    </PropertiesComboBox>
                                </dxwgv:GridViewDataComboBoxColumn>
                            </Columns>
                            <Settings ShowHeaderFilterButton="True" ShowGroupPanel="True" ShowHorizontalScrollBar="True"
                                ShowVerticalScrollBar="True" VerticalScrollableHeight="225"></Settings>
                            <StylesEditors>
                                <ProgressBar Height="25px">
                                </ProgressBar>
                            </StylesEditors>
                        </dxwgv:ASPxGridView>
                    </td>
                </tr>
                <tr>
                    <td style="height: 13px; text-align: left" colspan="2">
                        <table style="width: 100%" cellspacing="0" cellpadding="0" border="0">
                            <tbody>
                                <tr>
                                    <td style="width: 50%; height: 38px">
                                        <dxe:ASPxCheckBox ID="cbShowAll" runat="server" AutoPostBack="true" OnCheckedChanged="cbShowAll_CheckedChanged"
                                            Text="Show All" Checked="False">
                                        </dxe:ASPxCheckBox>
                                    </td>
                                    <td style="width: 50%; height: 38px; text-align: right">
                                        <div align="right">
                                            <uc1:ExportButtons ID="ucExportButtons" runat="server"></uc1:ExportButtons>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
    </ContentTemplate>
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="cbShowAll" EventName="CheckedChanged"></asp:AsyncPostBackTrigger>
    </Triggers>
</asp:UpdatePanel>
<data:CompaniesEhsConsultantsDataSource ID="CompaniesEhsConsultantsDataSource1" runat="server">
</data:CompaniesEhsConsultantsDataSource>
<data:QuestionnaireStatusDataSource ID="dsQuestionnaireStatus" runat="server">
</data:QuestionnaireStatusDataSource>
<data:SitesDataSource ID="dsSites" runat="server">
    <DeepLoadProperties Method="IncludeChildren" Recursive="False">
    </DeepLoadProperties>
</data:SitesDataSource>

<%--<data:UsersFullNameDataSource ID="UsersFullNameDataSource1" runat="server" Sort="UserFullName ASC" SelectMethod="GetPaged">
</data:UsersFullNameDataSource>--%>

<asp:SqlDataSource ID="UsersFullNameDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="_Users_FullName" SelectCommandType="StoredProcedure">
</asp:SqlDataSource>

<asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="SELECT DISTINCT C.CompanyId, C.CompanyName FROM QuestionnaireWithLocationApprovalView As Q INNER JOIN Companies As C ON Q.CompanyId = C.CompanyId ORDER BY C.CompanyName ASC">
</asp:SqlDataSource>