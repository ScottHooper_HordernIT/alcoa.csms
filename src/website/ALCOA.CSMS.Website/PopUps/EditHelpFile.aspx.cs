﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxUploadControl;
using DevExpress.Web.ASPxEditors;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Services;
using KaiZen.CSMS.Web;
using KaiZen.Library;

using System.Collections;
using System.IO;

namespace ALCOA.CSMS.Website.PopUps
{
    public partial class EditHelpFile : System.Web.UI.Page
    {
        bool EditMode = false;
        int FileVaultId = 0;
        Auth auth = new Auth();

        protected void Page_Load(object sender, EventArgs e) { Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); }

        private void moduleInit(bool postBack)
        {
            try
            {
                if (Request.QueryString["q"] == null)
                {
                    //new
                    btnUpload.Text = "Add / Upload";
                }
                else
                {
                    Int32.TryParse(Request.QueryString["q"], out FileVaultId);

                    if (FileVaultId == 0) throw new Exception("Invalid Url");
                    btnUpload.Text = "Edit / Upload";
                    EditMode = true;
                }

                if (!postBack)
                { //first time load
                    if (auth.RoleId == (int)RoleList.Administrator)
                    {
                       if (FileVaultId > 0)
                        Load(FileVaultId);
                    }
                    else
                    {
                        throw new Exception("Not Allowed");
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromContext(Context).Raise(ex);
                throw new Exception(ex.Message);
            }
        }

        protected void Load(int FileVaultId)
        {
            lblCurrentFile.Visible = true;
            hlCurrentFile.Visible = true;
            hlCurrentFile.NavigateUrl = String.Format("javascript:popUp2alt('../PopUps/GetFile.aspx{0}');", QueryStringModule.Encrypt("q=" + FileVaultId));
            btnDelete.Visible = true;

            int count = 0;
            FileVaultTableListHelpFilesFilters fvtlhfFilters = new FileVaultTableListHelpFilesFilters();
            fvtlhfFilters.Append(FileVaultTableListHelpFilesColumn.FileVaultId, FileVaultId.ToString());
            VList<FileVaultTableListHelpFiles> fvtlhfList = DataRepository.FileVaultTableListHelpFilesProvider.GetPaged(fvtlhfFilters.ToString(), "", 0, 99, out count);
            if(count == 0) throw new Exception("Could Not Find File.");
            hlCurrentFile.Text = fvtlhfList[0].FileName;

            foreach (FileVaultTableListHelpFiles fvtlhf in fvtlhfList)
            {
                switch (fvtlhf.FileVaultSubCategoryId)
                {
                    case (int)FileVaultSubCategoryList.HelpFiles_Everyone:
                        cbHelpFiles_Everyone.Checked = true;
                        break;
                    case (int)FileVaultSubCategoryList.HelpFiles_Administrator:
                        cbHelpFiles_Administrator.Checked = true;
                        break;
                    case (int)FileVaultSubCategoryList.HelpFiles_Contractor_Prequal:
                        cbHelpFiles_Contractor_Prequal.Checked = true;
                        break;
                    case (int)FileVaultSubCategoryList.HelpFiles_Contractor_Requal:
                        cbHelpFiles_Contractor_Requal.Checked = true;
                        break;
                    case (int)FileVaultSubCategoryList.HelpFiles_Contractor_ViewEngineeringHours:
                        cbHelpFiles_Contractor_ViewEngineeringHours.Checked = true;
                        break;
                    case (int)FileVaultSubCategoryList.HelpFiles_HsAssessor:
                        cbHelpFiles_HsAssessor.Checked = true;
                        break;
                    case (int)FileVaultSubCategoryList.HelpFiles_HsAssessor_Lead:
                        cbHelpFiles_HsAssessor_Lead.Checked = true;
                        break;
                    case (int)FileVaultSubCategoryList.HelpFiles_Procurement:
                        cbHelpFiles_Procurement.Checked = true;
                        break;
                    case (int)FileVaultSubCategoryList.HelpFiles_Reader:
                        cbHelpFiles_Reader.Checked = true;
                        break;
                    case (int)FileVaultSubCategoryList.HelpFiles_Reader_Ebi:
                        cbHelpFiles_Reader_Ebi.Checked = true;
                        break;
                    case (int)FileVaultSubCategoryList.HelpFiles_Reader_SafetyQualificationReadAccess:
                        cbHelpFiles_Reader_SafetyQualificationReadAccess.Checked = true;
                        break;
                    case (int)FileVaultSubCategoryList.HelpFiles_Reader_FinancialReportingAccess:
                        cbHelpFiles_Reader_FinancialReportingAccess.Checked = true;
                        break;
                    case (int)FileVaultSubCategoryList.HelpFiles_Reader_TrainingPackageTrainers:
                        cbHelpFiles_Reader_TrainingPackageTrainers.Checked = true;
                        break;
                    default:
                        break;
                }
            }

        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            if (Page.IsPostBack)
            {

                bool success = false;
                string errorMsg = "No File Selected. Please click Browse and select a file for upload.";
                try
                {
                    if (Request.QueryString["q"] != null)
                        Int32.TryParse(Request.QueryString["q"], out FileVaultId);

                    if (ucNewFile.UploadedFiles != null && ucNewFile.UploadedFiles.Length > 0)
                    {
                        if (!String.IsNullOrEmpty(ucNewFile.UploadedFiles[0].FileName) && ucNewFile.UploadedFiles[0].IsValid)
                            FileVaultId = UploadFile(ucNewFile, FileVaultId);
                    }
                    EditFileVaultTable(FileVaultId);
                    success = true;
                }
                catch (Exception ex)
                {
                    Elmah.ErrorSignal.FromContext(this.Context).Raise(ex);
                    errorMsg = ex.Message;
                }
                finally
                {
                    if (success)
                    {
                        ClientScriptManager clientScript = Page.ClientScript;
                        clientScript.RegisterStartupScript(this.GetType(), "CloseWindowScript", "<script>javascript:window.close();</script>");

                    }
                    else
                    {
                        lblFileUploadMsg.Text = String.Format("Error Occured: {0}", errorMsg);
                    }
                }
            }
        }

        protected int UploadFile(ASPxUploadControl ucNewFile, int fileVaultId)
        {
            TransactionManager transactionManager = null;
            try
            {
                transactionManager = ConnectionScope.ValidateOrCreateTransaction(true);
                FileVault fv = new FileVault() { FileVaultCategoryId = (int)FileVaultCategoryList.HelpFiles, ModifiedByUserId = auth.UserId, ModifiedDate = DateTime.Now };
                if (fileVaultId > 0)
                {
                    FileVaultService fvService = new FileVaultService();
                    fv = fvService.GetByFileVaultId(fileVaultId);
                }

                Configuration configuration = new Configuration();
                string tempPath = @configuration.GetValue(ConfigList.TempDir);
                string fileName = ucNewFile.UploadedFiles[0].FileName;
                string fullPath = tempPath + fileName;
                ucNewFile.UploadedFiles[0].SaveAs(fullPath);

                FileStream fs = new FileStream(fullPath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                fv.FileName = fileName;
                fv.Content = FileUtilities.Read.ReadFully(fs, 51200);
                fv.FileHash = FileUtilities.Calculate.HashFile(fv.Content);
                fv.ContentLength = Convert.ToInt32(fs.Length);

                fs.Close();
                fs.Dispose();
                System.Threading.Thread.Sleep(1000);
                File.Delete(fullPath);

                DataRepository.FileVaultProvider.Insert(transactionManager, fv);
                transactionManager.Commit();

                return fv.FileVaultId;
            }
            catch (Exception ex)
            {
                if ((transactionManager != null) && (transactionManager.IsOpen))
                    transactionManager.Rollback();
                throw ex;
            }
        }

        protected void InsertUpdateDelete(TList<FileVaultTable> fvtList_old, TList<FileVaultTable> fvtList_delete, TList<FileVaultTable> fvtList_insert, TList<FileVaultTable> fvtList_update,
                                            int fileVaultId, int FileVaultSubCategoryId, ASPxTextBox tb, ASPxCheckBox cb,
                                         out TList<FileVaultTable> fvtList_delete_out, out TList<FileVaultTable> fvtList_insert_out, out TList<FileVaultTable> fvtList_update_out)
        {
            fvtList_delete_out = fvtList_delete;
            fvtList_insert_out = fvtList_insert;
            fvtList_update_out = fvtList_update;

            DateTime dtNow = DateTime.Now;
            //string Search = "";

            //switch (FileVaultSubCategoryId)
            //{
            //    case (int)FileVaultSubCategoryList.HelpFiles_Everyone:
            //        Search = "HelpFiles_Everyone";
            //        break;
            //    default:
            //        break;
            //}

            //ASPxTextBox tb = (ASPxTextBox)FindControl("tb" + Search);
            //ASPxCheckBox cb = (ASPxCheckBox)FindControl("cb" + Search);

            FileVaultTable fvt = fvtList_old.Find(FileVaultTableColumn.FileVaultSubCategoryId, FileVaultSubCategoryId);
            if (cb.Checked == true)
            {
                if (fvt == null)
                {
                    fvt = new FileVaultTable();
                    fvt.FileVaultId = fileVaultId;
                    fvt.FileVaultSubCategoryId = FileVaultSubCategoryId;
                    fvt.FileNameCustom = tbHelpFiles_Everyone.Text;
                    fvt.ModifiedByUserId = auth.UserId;
                    fvt.ModifiedDate = dtNow;
                    fvtList_insert_out.Add(fvt);
                }
                else
                {
                    if (fvt.FileNameCustom != tbHelpFiles_Everyone.Text)
                    {
                        fvt.FileNameCustom = tbHelpFiles_Everyone.Text;
                        fvt.ModifiedByUserId = auth.UserId;
                        fvt.ModifiedDate = dtNow;
                        fvtList_update_out.Add(fvt);
                    }
                }
            }
            else
            {
                if (fvt != null)
                    fvtList_delete_out.Add(fvt);
            }
        }

        protected void EditFileVaultTable(int fileVaultId)
        {
            

            TransactionManager transactionManager = null;
            try
            {
                transactionManager = ConnectionScope.ValidateOrCreateTransaction(true);

                FileVaultTableService fvService = new FileVaultTableService();
                TList<FileVaultTable> fvtList_old = fvService.GetByFileVaultId(fileVaultId);
                TList<FileVaultTable> fvtList_delete = new TList<FileVaultTable>();
                TList<FileVaultTable> fvtList_insert = new TList<FileVaultTable>();
                TList<FileVaultTable> fvtList_update = new TList<FileVaultTable>();

                InsertUpdateDelete(fvtList_old, fvtList_delete, fvtList_insert, fvtList_update, 
                                    fileVaultId, (int)FileVaultSubCategoryList.HelpFiles_Everyone, tbHelpFiles_Everyone, cbHelpFiles_Everyone,
                                    out fvtList_delete, out fvtList_insert, out fvtList_update);
                InsertUpdateDelete(fvtList_old, fvtList_delete, fvtList_insert, fvtList_update,
                                    fileVaultId, (int)FileVaultSubCategoryList.HelpFiles_Administrator, tbHelpFiles_Administrator, cbHelpFiles_Administrator,
                                    out fvtList_delete, out fvtList_insert, out fvtList_update);
                InsertUpdateDelete(fvtList_old, fvtList_delete, fvtList_insert, fvtList_update,
                                    fileVaultId, (int)FileVaultSubCategoryList.HelpFiles_Contractor_Prequal, tbHelpFiles_Contractor_Prequal, cbHelpFiles_Contractor_Prequal,
                                    out fvtList_delete, out fvtList_insert, out fvtList_update);
                InsertUpdateDelete(fvtList_old, fvtList_delete, fvtList_insert, fvtList_update,
                                    fileVaultId, (int)FileVaultSubCategoryList.HelpFiles_Contractor_Requal, tbHelpFiles_Contractor_Requal, cbHelpFiles_Contractor_Requal,
                                    out fvtList_delete, out fvtList_insert, out fvtList_update);
                InsertUpdateDelete(fvtList_old, fvtList_delete, fvtList_insert, fvtList_update,
                                    fileVaultId, (int)FileVaultSubCategoryList.HelpFiles_Contractor_ViewEngineeringHours, tbHelpFiles_Contractor_ViewEngineeringHours, cbHelpFiles_Contractor_ViewEngineeringHours,
                                    out fvtList_delete, out fvtList_insert, out fvtList_update);
                InsertUpdateDelete(fvtList_old, fvtList_delete, fvtList_insert, fvtList_update,
                                    fileVaultId, (int)FileVaultSubCategoryList.HelpFiles_HsAssessor, tbHelpFiles_HsAssessor, cbHelpFiles_HsAssessor,
                                    out fvtList_delete, out fvtList_insert, out fvtList_update);
                InsertUpdateDelete(fvtList_old, fvtList_delete, fvtList_insert, fvtList_update,
                                   fileVaultId, (int)FileVaultSubCategoryList.HelpFiles_HsAssessor_Lead, tbHelpFiles_HsAssessor_Lead, cbHelpFiles_HsAssessor_Lead,
                                   out fvtList_delete, out fvtList_insert, out fvtList_update);
                InsertUpdateDelete(fvtList_old, fvtList_delete, fvtList_insert, fvtList_update,
                                    fileVaultId, (int)FileVaultSubCategoryList.HelpFiles_Procurement, tbHelpFiles_Procurement, cbHelpFiles_Procurement,
                                    out fvtList_delete, out fvtList_insert, out fvtList_update);
                InsertUpdateDelete(fvtList_old, fvtList_delete, fvtList_insert, fvtList_update,
                                    fileVaultId, (int)FileVaultSubCategoryList.HelpFiles_Reader, tbHelpFiles_Reader, cbHelpFiles_Reader,
                                    out fvtList_delete, out fvtList_insert, out fvtList_update);
                InsertUpdateDelete(fvtList_old, fvtList_delete, fvtList_insert, fvtList_update,
                                    fileVaultId, (int)FileVaultSubCategoryList.HelpFiles_Reader_Ebi, tbHelpFiles_Reader_Ebi, cbHelpFiles_Reader_Ebi,
                                    out fvtList_delete, out fvtList_insert, out fvtList_update);
                InsertUpdateDelete(fvtList_old, fvtList_delete, fvtList_insert, fvtList_update,
                                    fileVaultId, (int)FileVaultSubCategoryList.HelpFiles_Reader_SafetyQualificationReadAccess, tbHelpFiles_Reader_SafetyQualificationReadAccess, cbHelpFiles_Reader_SafetyQualificationReadAccess,
                                    out fvtList_delete, out fvtList_insert, out fvtList_update);
                InsertUpdateDelete(fvtList_old, fvtList_delete, fvtList_insert, fvtList_update,
                                    fileVaultId, (int)FileVaultSubCategoryList.HelpFiles_Reader_FinancialReportingAccess, tbHelpFiles_Reader_FinancialReportingAccess, cbHelpFiles_Reader_FinancialReportingAccess,
                                    out fvtList_delete, out fvtList_insert, out fvtList_update);
                InsertUpdateDelete(fvtList_old, fvtList_delete, fvtList_insert, fvtList_update,
                                    fileVaultId, (int)FileVaultSubCategoryList.HelpFiles_Reader_TrainingPackageTrainers, tbHelpFiles_Reader_TrainingPackageTrainers, cbHelpFiles_Reader_TrainingPackageTrainers,
                                    out fvtList_delete, out fvtList_insert, out fvtList_update);


                //FileVaultTable fvtHelpFiles_Everyone = fvtList_old.Find(FileVaultTableColumn.FileVaultSubCategoryId, (int)FileVaultSubCategoryList.HelpFiles_Everyone);
                //if (cbHelpFiles_Everyone.Checked == true)
                //{
                //    if (fvtHelpFiles_Everyone == null)
                //    {
                //        fvtHelpFiles_Everyone = new FileVaultTable();
                //        fvtHelpFiles_Everyone.FileVaultId = fileVaultId;
                //        fvtHelpFiles_Everyone.FileVaultSubCategoryId = (int)FileVaultSubCategoryList.HelpFiles_Everyone;
                //        fvtHelpFiles_Everyone.FileNameCustom = tbHelpFiles_Everyone.Text;
                //        fvtHelpFiles_Everyone.ModifiedByUserId = auth.UserId;
                //        fvtHelpFiles_Everyone.ModifiedDate = dtNow;
                //        fvtList_insert.Add(fvtHelpFiles_Everyone);
                //    }
                //    else
                //    {
                //        if (fvtHelpFiles_Everyone.FileNameCustom != tbHelpFiles_Everyone.Text)
                //        {
                //            fvtHelpFiles_Everyone.FileNameCustom = tbHelpFiles_Everyone.Text;
                //            fvtHelpFiles_Everyone.ModifiedByUserId = auth.UserId;
                //            fvtHelpFiles_Everyone.ModifiedDate = dtNow;
                //            fvtList_update.Add(fvtHelpFiles_Everyone);
                //        }
                //    }
                //}
                //else
                //{
                //    if (fvtHelpFiles_Everyone != null)
                //        fvtList_delete.Add(fvtHelpFiles_Everyone);
                //}



                //foreach (FileVaultTable fvt in fvtList_old)
                //{
                //    switch (fvt.FileVaultSubCategoryId)
                //    {
                //        case (int)FileVaultSubCategoryList.HelpFiles_Everyone:
                            
                //            break;
                //        case (int)FileVaultSubCategoryList.HelpFiles_Administrator:
                //            cbHelpFiles_Administrator.Checked = true;
                //            break;
                //        case (int)FileVaultSubCategoryList.HelpFiles_Contractor_Prequal:
                //            cbHelpFiles_Contractor_Prequal.Checked = true;
                //            break;
                //        case (int)FileVaultSubCategoryList.HelpFiles_Contractor_Requal:
                //            cbHelpFiles_Contractor_Requal.Checked = true;
                //            break;
                //        case (int)FileVaultSubCategoryList.HelpFiles_Contractor_ViewEngineeringHours:
                //            cbHelpFiles_Contractor_ViewEngineeringHours.Checked = true;
                //            break;
                //        case (int)FileVaultSubCategoryList.HelpFiles_HsAssessor:
                //            cbHelpFiles_HsAssessor.Checked = true;
                //            break;
                //        case (int)FileVaultSubCategoryList.HelpFiles_HsAssessor_Lead:
                //            cbHelpFiles_HsAssessor_Lead.Checked = true;
                //            break;
                //        case (int)FileVaultSubCategoryList.HelpFiles_Procurement:
                //            cbHelpFiles_Procurement.Checked = true;
                //            break;
                //        case (int)FileVaultSubCategoryList.HelpFiles_Reader:
                //            cbHelpFiles_Reader.Checked = true;
                //            break;
                //        case (int)FileVaultSubCategoryList.HelpFiles_Reader_Ebi:
                //            cbHelpFiles_Reader_Ebi.Checked = true;
                //            break;
                //        case (int)FileVaultSubCategoryList.HelpFiles_Reader_SafetyQualificationReadAccess:
                //            cbHelpFiles_Reader_SafetyQualificationReadAccess.Checked = true;
                //            break;
                //        case (int)FileVaultSubCategoryList.HelpFiles_Reader_FinancialReportingAccess:
                //            cbHelpFiles_Reader_FinancialReportingAccess.Checked = true;
                //            break;
                //        case (int)FileVaultSubCategoryList.HelpFiles_Reader_TrainingPackageTrainers:
                //            cbHelpFiles_Reader_TrainingPackageTrainers.Checked = true;
                //            break;
                //    }
                //}

                if (fvtList_delete.Count > 0)
                    DataRepository.FileVaultTableProvider.Delete(fvtList_delete);

                if (fvtList_update.Count > 0)
                    DataRepository.FileVaultTableProvider.Update(fvtList_update);

                if (fvtList_insert.Count > 0)
                    DataRepository.FileVaultTableProvider.Insert(fvtList_insert);

                transactionManager.Commit();

                
            }
            catch (Exception ex)
            {
                if ((transactionManager != null) && (transactionManager.IsOpen))
                    transactionManager.Rollback();
                throw ex;
            }
        }


        protected void btnDelete_Click(object sender, EventArgs e)
        {
            if (Page.IsPostBack)
            {

                bool success = false;
                string errorMsg = "Error Deleting File. Contact your Administrator.";
                try
                {
                    if (Request.QueryString["q"] != null)
                        Int32.TryParse(Request.QueryString["q"], out FileVaultId);

                    if (ucNewFile.UploadedFiles != null && ucNewFile.UploadedFiles.Length > 0)
                    {
                        if (!String.IsNullOrEmpty(ucNewFile.UploadedFiles[0].FileName) && ucNewFile.UploadedFiles[0].IsValid)
                            FileVaultId = UploadFile(ucNewFile, FileVaultId);
                    }
                    Delete(FileVaultId);
                    success = true;
                }
                catch (Exception ex)
                {
                    Elmah.ErrorSignal.FromContext(this.Context).Raise(ex);
                    errorMsg = ex.Message;
                }
                finally
                {
                    if (success)
                    {
                        //Close and refresh main page.
                    }
                    else
                    {
                        lblFileUploadMsg.Text = String.Format("Error Occured: {0}", errorMsg);
                    }
                }
            }
        }

        protected void Delete(int fileVaultId)
        {
            TransactionManager transactionManager = null;
            try
            {
                transactionManager = ConnectionScope.ValidateOrCreateTransaction(true);

                FileVaultService fvService = new FileVaultService();
                FileVault fv = fvService.GetByFileVaultId(fileVaultId);
                if (fv == null) throw new Exception("File Not Found.");

                FileVaultTableService fvtService = new FileVaultTableService();
                TList<FileVaultTable> fvt = fvtService.GetByFileVaultId(fileVaultId);
                if (fvt.Count > 0) DataRepository.FileVaultTableProvider.Delete(transactionManager, fvt);

                DataRepository.FileVaultProvider.Delete(transactionManager, fv);

                transactionManager.Commit();

            }
            catch (Exception ex)
            {
                if ((transactionManager != null) && (transactionManager.IsOpen))
                    transactionManager.Rollback();
                throw ex;
            }
        }

        protected void ASPxButton1_Click(object sender, EventArgs e)
        {
            ListControlCollections();
        }

        protected void ListControlCollections()
        {
            ArrayList controlList = new ArrayList();
            AddControls(Page.Controls, controlList);

            foreach (string str in controlList)
                Response.Write(str + "<br />");
            Response.Write("Total Controls:" + controlList.Count);
        }

        private void AddControls(ControlCollection page, ArrayList controlList)
        {
            foreach (Control c in page)
            {
                if (c.ID != null)
                    controlList.Add(c.ID);

                if(c.HasControls())
                    AddControls(c.Controls, controlList);
            }
        }
    }
}