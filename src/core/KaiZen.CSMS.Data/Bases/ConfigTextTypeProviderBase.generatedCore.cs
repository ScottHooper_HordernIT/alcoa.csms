﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="ConfigTextTypeProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class ConfigTextTypeProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.ConfigTextType, KaiZen.CSMS.Entities.ConfigTextTypeKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.ConfigTextTypeKey key)
		{
			return Delete(transactionManager, key.ConfigTextTypeId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_configTextTypeId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _configTextTypeId)
		{
			return Delete(null, _configTextTypeId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_configTextTypeId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _configTextTypeId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.ConfigTextType Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.ConfigTextTypeKey key, int start, int pageLength)
		{
			return GetByConfigTextTypeId(transactionManager, key.ConfigTextTypeId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_ConfigTextType index.
		/// </summary>
		/// <param name="_configTextTypeId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.ConfigTextType"/> class.</returns>
		public KaiZen.CSMS.Entities.ConfigTextType GetByConfigTextTypeId(System.Int32 _configTextTypeId)
		{
			int count = -1;
			return GetByConfigTextTypeId(null,_configTextTypeId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ConfigTextType index.
		/// </summary>
		/// <param name="_configTextTypeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.ConfigTextType"/> class.</returns>
		public KaiZen.CSMS.Entities.ConfigTextType GetByConfigTextTypeId(System.Int32 _configTextTypeId, int start, int pageLength)
		{
			int count = -1;
			return GetByConfigTextTypeId(null, _configTextTypeId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ConfigTextType index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_configTextTypeId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.ConfigTextType"/> class.</returns>
		public KaiZen.CSMS.Entities.ConfigTextType GetByConfigTextTypeId(TransactionManager transactionManager, System.Int32 _configTextTypeId)
		{
			int count = -1;
			return GetByConfigTextTypeId(transactionManager, _configTextTypeId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ConfigTextType index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_configTextTypeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.ConfigTextType"/> class.</returns>
		public KaiZen.CSMS.Entities.ConfigTextType GetByConfigTextTypeId(TransactionManager transactionManager, System.Int32 _configTextTypeId, int start, int pageLength)
		{
			int count = -1;
			return GetByConfigTextTypeId(transactionManager, _configTextTypeId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ConfigTextType index.
		/// </summary>
		/// <param name="_configTextTypeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.ConfigTextType"/> class.</returns>
		public KaiZen.CSMS.Entities.ConfigTextType GetByConfigTextTypeId(System.Int32 _configTextTypeId, int start, int pageLength, out int count)
		{
			return GetByConfigTextTypeId(null, _configTextTypeId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ConfigTextType index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_configTextTypeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.ConfigTextType"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.ConfigTextType GetByConfigTextTypeId(TransactionManager transactionManager, System.Int32 _configTextTypeId, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;ConfigTextType&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;ConfigTextType&gt;"/></returns>
		public static TList<ConfigTextType> Fill(IDataReader reader, TList<ConfigTextType> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.ConfigTextType c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("ConfigTextType")
					.Append("|").Append((System.Int32)reader[((int)ConfigTextTypeColumn.ConfigTextTypeId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<ConfigTextType>(
					key.ToString(), // EntityTrackingKey
					"ConfigTextType",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.ConfigTextType();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.ConfigTextTypeId = (System.Int32)reader[((int)ConfigTextTypeColumn.ConfigTextTypeId - 1)];
					c.ConfigTextType = (System.String)reader[((int)ConfigTextTypeColumn.ConfigTextType - 1)];
					c.ConfigTextTypeDesc = (reader.IsDBNull(((int)ConfigTextTypeColumn.ConfigTextTypeDesc - 1)))?null:(System.String)reader[((int)ConfigTextTypeColumn.ConfigTextTypeDesc - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.ConfigTextType"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.ConfigTextType"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.ConfigTextType entity)
		{
			if (!reader.Read()) return;
			
			entity.ConfigTextTypeId = (System.Int32)reader[((int)ConfigTextTypeColumn.ConfigTextTypeId - 1)];
			entity.ConfigTextType = (System.String)reader[((int)ConfigTextTypeColumn.ConfigTextType - 1)];
			entity.ConfigTextTypeDesc = (reader.IsDBNull(((int)ConfigTextTypeColumn.ConfigTextTypeDesc - 1)))?null:(System.String)reader[((int)ConfigTextTypeColumn.ConfigTextTypeDesc - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.ConfigTextType"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.ConfigTextType"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.ConfigTextType entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.ConfigTextTypeId = (System.Int32)dataRow["ConfigTextTypeId"];
			entity.ConfigTextType = (System.String)dataRow["ConfigTextType"];
			entity.ConfigTextTypeDesc = Convert.IsDBNull(dataRow["ConfigTextTypeDesc"]) ? null : (System.String)dataRow["ConfigTextTypeDesc"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.ConfigTextType"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.ConfigTextType Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.ConfigTextType entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetByConfigTextTypeId methods when available
			
			#region ConfigText2Collection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<ConfigText2>|ConfigText2Collection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ConfigText2Collection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.ConfigText2Collection = DataRepository.ConfigText2Provider.GetByConfigTextTypeId(transactionManager, entity.ConfigTextTypeId);

				if (deep && entity.ConfigText2Collection.Count > 0)
				{
					deepHandles.Add("ConfigText2Collection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<ConfigText2>) DataRepository.ConfigText2Provider.DeepLoad,
						new object[] { transactionManager, entity.ConfigText2Collection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region ConfigTextCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<ConfigText>|ConfigTextCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ConfigTextCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.ConfigTextCollection = DataRepository.ConfigTextProvider.GetByConfigTextTypeId(transactionManager, entity.ConfigTextTypeId);

				if (deep && entity.ConfigTextCollection.Count > 0)
				{
					deepHandles.Add("ConfigTextCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<ConfigText>) DataRepository.ConfigTextProvider.DeepLoad,
						new object[] { transactionManager, entity.ConfigTextCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.ConfigTextType object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.ConfigTextType instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.ConfigTextType Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.ConfigTextType entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<ConfigText2>
				if (CanDeepSave(entity.ConfigText2Collection, "List<ConfigText2>|ConfigText2Collection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(ConfigText2 child in entity.ConfigText2Collection)
					{
						if(child.ConfigTextTypeIdSource != null)
						{
							child.ConfigTextTypeId = child.ConfigTextTypeIdSource.ConfigTextTypeId;
						}
						else
						{
							child.ConfigTextTypeId = entity.ConfigTextTypeId;
						}

					}

					if (entity.ConfigText2Collection.Count > 0 || entity.ConfigText2Collection.DeletedItems.Count > 0)
					{
						//DataRepository.ConfigText2Provider.Save(transactionManager, entity.ConfigText2Collection);
						
						deepHandles.Add("ConfigText2Collection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< ConfigText2 >) DataRepository.ConfigText2Provider.DeepSave,
							new object[] { transactionManager, entity.ConfigText2Collection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<ConfigText>
				if (CanDeepSave(entity.ConfigTextCollection, "List<ConfigText>|ConfigTextCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(ConfigText child in entity.ConfigTextCollection)
					{
						if(child.ConfigTextTypeIdSource != null)
						{
							child.ConfigTextTypeId = child.ConfigTextTypeIdSource.ConfigTextTypeId;
						}
						else
						{
							child.ConfigTextTypeId = entity.ConfigTextTypeId;
						}

					}

					if (entity.ConfigTextCollection.Count > 0 || entity.ConfigTextCollection.DeletedItems.Count > 0)
					{
						//DataRepository.ConfigTextProvider.Save(transactionManager, entity.ConfigTextCollection);
						
						deepHandles.Add("ConfigTextCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< ConfigText >) DataRepository.ConfigTextProvider.DeepSave,
							new object[] { transactionManager, entity.ConfigTextCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region ConfigTextTypeChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.ConfigTextType</c>
	///</summary>
	public enum ConfigTextTypeChildEntityTypes
	{

		///<summary>
		/// Collection of <c>ConfigTextType</c> as OneToMany for ConfigText2Collection
		///</summary>
		[ChildEntityType(typeof(TList<ConfigText2>))]
		ConfigText2Collection,

		///<summary>
		/// Collection of <c>ConfigTextType</c> as OneToMany for ConfigTextCollection
		///</summary>
		[ChildEntityType(typeof(TList<ConfigText>))]
		ConfigTextCollection,
	}
	
	#endregion ConfigTextTypeChildEntityTypes
	
	#region ConfigTextTypeFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;ConfigTextTypeColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ConfigTextType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ConfigTextTypeFilterBuilder : SqlFilterBuilder<ConfigTextTypeColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ConfigTextTypeFilterBuilder class.
		/// </summary>
		public ConfigTextTypeFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ConfigTextTypeFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ConfigTextTypeFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ConfigTextTypeFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ConfigTextTypeFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ConfigTextTypeFilterBuilder
	
	#region ConfigTextTypeParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;ConfigTextTypeColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ConfigTextType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ConfigTextTypeParameterBuilder : ParameterizedSqlFilterBuilder<ConfigTextTypeColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ConfigTextTypeParameterBuilder class.
		/// </summary>
		public ConfigTextTypeParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ConfigTextTypeParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ConfigTextTypeParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ConfigTextTypeParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ConfigTextTypeParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ConfigTextTypeParameterBuilder
	
	#region ConfigTextTypeSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;ConfigTextTypeColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ConfigTextType"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class ConfigTextTypeSortBuilder : SqlSortBuilder<ConfigTextTypeColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ConfigTextTypeSqlSortBuilder class.
		/// </summary>
		public ConfigTextTypeSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion ConfigTextTypeSortBuilder
	
} // end namespace
