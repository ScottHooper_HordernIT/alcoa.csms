﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.QuestionnaireReportTimeDelayAssessmentCompleteProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[CLSCompliant(true)]
	[Designer(typeof(QuestionnaireReportTimeDelayAssessmentCompleteDataSourceDesigner))]
	public class QuestionnaireReportTimeDelayAssessmentCompleteDataSource : ReadOnlyDataSource<QuestionnaireReportTimeDelayAssessmentComplete>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportTimeDelayAssessmentCompleteDataSource class.
		/// </summary>
		public QuestionnaireReportTimeDelayAssessmentCompleteDataSource() : base(new QuestionnaireReportTimeDelayAssessmentCompleteService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the QuestionnaireReportTimeDelayAssessmentCompleteDataSourceView used by the QuestionnaireReportTimeDelayAssessmentCompleteDataSource.
		/// </summary>
		protected QuestionnaireReportTimeDelayAssessmentCompleteDataSourceView QuestionnaireReportTimeDelayAssessmentCompleteView
		{
			get { return ( View as QuestionnaireReportTimeDelayAssessmentCompleteDataSourceView ); }
		}
		
		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the QuestionnaireReportTimeDelayAssessmentCompleteDataSourceView class that is to be
		/// used by the QuestionnaireReportTimeDelayAssessmentCompleteDataSource.
		/// </summary>
		/// <returns>An instance of the QuestionnaireReportTimeDelayAssessmentCompleteDataSourceView class.</returns>
		protected override BaseDataSourceView<QuestionnaireReportTimeDelayAssessmentComplete, Object> GetNewDataSourceView()
		{
			return new QuestionnaireReportTimeDelayAssessmentCompleteDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the QuestionnaireReportTimeDelayAssessmentCompleteDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class QuestionnaireReportTimeDelayAssessmentCompleteDataSourceView : ReadOnlyDataSourceView<QuestionnaireReportTimeDelayAssessmentComplete>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportTimeDelayAssessmentCompleteDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the QuestionnaireReportTimeDelayAssessmentCompleteDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public QuestionnaireReportTimeDelayAssessmentCompleteDataSourceView(QuestionnaireReportTimeDelayAssessmentCompleteDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal QuestionnaireReportTimeDelayAssessmentCompleteDataSource QuestionnaireReportTimeDelayAssessmentCompleteOwner
		{
			get { return Owner as QuestionnaireReportTimeDelayAssessmentCompleteDataSource; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal QuestionnaireReportTimeDelayAssessmentCompleteService QuestionnaireReportTimeDelayAssessmentCompleteProvider
		{
			get { return Provider as QuestionnaireReportTimeDelayAssessmentCompleteService; }
		}

		#endregion Properties
		
		#region Methods
		
		#endregion Methods
	}

	#region QuestionnaireReportTimeDelayAssessmentCompleteDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the QuestionnaireReportTimeDelayAssessmentCompleteDataSource class.
	/// </summary>
	public class QuestionnaireReportTimeDelayAssessmentCompleteDataSourceDesigner : ReadOnlyDataSourceDesigner<QuestionnaireReportTimeDelayAssessmentComplete>
	{
	}

	#endregion QuestionnaireReportTimeDelayAssessmentCompleteDataSourceDesigner

	#region QuestionnaireReportTimeDelayAssessmentCompleteFilter

	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireReportTimeDelayAssessmentComplete"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireReportTimeDelayAssessmentCompleteFilter : SqlFilter<QuestionnaireReportTimeDelayAssessmentCompleteColumn>
	{
	}

	#endregion QuestionnaireReportTimeDelayAssessmentCompleteFilter

	#region QuestionnaireReportTimeDelayAssessmentCompleteExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireReportTimeDelayAssessmentComplete"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireReportTimeDelayAssessmentCompleteExpressionBuilder : SqlExpressionBuilder<QuestionnaireReportTimeDelayAssessmentCompleteColumn>
	{
	}
	
	#endregion QuestionnaireReportTimeDelayAssessmentCompleteExpressionBuilder		
}

