﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="EhsimsExceptionsProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class EhsimsExceptionsProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.EhsimsExceptions, KaiZen.CSMS.Entities.EhsimsExceptionsKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.EhsimsExceptionsKey key)
		{
			return Delete(transactionManager, key.EhsimsExceptionId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_ehsimsExceptionId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _ehsimsExceptionId)
		{
			return Delete(null, _ehsimsExceptionId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_ehsimsExceptionId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _ehsimsExceptionId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.EhsimsExceptions Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.EhsimsExceptionsKey key, int start, int pageLength)
		{
			return GetByEhsimsExceptionId(transactionManager, key.EhsimsExceptionId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_EhsimsExceptions index.
		/// </summary>
		/// <param name="_ehsimsExceptionId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.EhsimsExceptions"/> class.</returns>
		public KaiZen.CSMS.Entities.EhsimsExceptions GetByEhsimsExceptionId(System.Int32 _ehsimsExceptionId)
		{
			int count = -1;
			return GetByEhsimsExceptionId(null,_ehsimsExceptionId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_EhsimsExceptions index.
		/// </summary>
		/// <param name="_ehsimsExceptionId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.EhsimsExceptions"/> class.</returns>
		public KaiZen.CSMS.Entities.EhsimsExceptions GetByEhsimsExceptionId(System.Int32 _ehsimsExceptionId, int start, int pageLength)
		{
			int count = -1;
			return GetByEhsimsExceptionId(null, _ehsimsExceptionId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_EhsimsExceptions index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_ehsimsExceptionId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.EhsimsExceptions"/> class.</returns>
		public KaiZen.CSMS.Entities.EhsimsExceptions GetByEhsimsExceptionId(TransactionManager transactionManager, System.Int32 _ehsimsExceptionId)
		{
			int count = -1;
			return GetByEhsimsExceptionId(transactionManager, _ehsimsExceptionId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_EhsimsExceptions index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_ehsimsExceptionId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.EhsimsExceptions"/> class.</returns>
		public KaiZen.CSMS.Entities.EhsimsExceptions GetByEhsimsExceptionId(TransactionManager transactionManager, System.Int32 _ehsimsExceptionId, int start, int pageLength)
		{
			int count = -1;
			return GetByEhsimsExceptionId(transactionManager, _ehsimsExceptionId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_EhsimsExceptions index.
		/// </summary>
		/// <param name="_ehsimsExceptionId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.EhsimsExceptions"/> class.</returns>
		public KaiZen.CSMS.Entities.EhsimsExceptions GetByEhsimsExceptionId(System.Int32 _ehsimsExceptionId, int start, int pageLength, out int count)
		{
			return GetByEhsimsExceptionId(null, _ehsimsExceptionId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_EhsimsExceptions index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_ehsimsExceptionId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.EhsimsExceptions"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.EhsimsExceptions GetByEhsimsExceptionId(TransactionManager transactionManager, System.Int32 _ehsimsExceptionId, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#region _EhsimsExceptions_Summary_GetByDate 
		
		/// <summary>
		///	This method wrap the '_EhsimsExceptions_Summary_GetByDate' stored procedure. 
		/// </summary>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="month"> A <c>System.Int32?</c> instance.</param>
		/// <param name="day"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Summary_GetByDate(System.Int32? year, System.Int32? month, System.Int32? day)
		{
			return Summary_GetByDate(null, 0, int.MaxValue , year, month, day);
		}
		
		/// <summary>
		///	This method wrap the '_EhsimsExceptions_Summary_GetByDate' stored procedure. 
		/// </summary>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="month"> A <c>System.Int32?</c> instance.</param>
		/// <param name="day"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Summary_GetByDate(int start, int pageLength, System.Int32? year, System.Int32? month, System.Int32? day)
		{
			return Summary_GetByDate(null, start, pageLength , year, month, day);
		}
				
		/// <summary>
		///	This method wrap the '_EhsimsExceptions_Summary_GetByDate' stored procedure. 
		/// </summary>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="month"> A <c>System.Int32?</c> instance.</param>
		/// <param name="day"> A <c>System.Int32?</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Summary_GetByDate(TransactionManager transactionManager, System.Int32? year, System.Int32? month, System.Int32? day)
		{
			return Summary_GetByDate(transactionManager, 0, int.MaxValue , year, month, day);
		}
		
		/// <summary>
		///	This method wrap the '_EhsimsExceptions_Summary_GetByDate' stored procedure. 
		/// </summary>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="month"> A <c>System.Int32?</c> instance.</param>
		/// <param name="day"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet Summary_GetByDate(TransactionManager transactionManager, int start, int pageLength , System.Int32? year, System.Int32? month, System.Int32? day);
		
		#endregion
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;EhsimsExceptions&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;EhsimsExceptions&gt;"/></returns>
		public static TList<EhsimsExceptions> Fill(IDataReader reader, TList<EhsimsExceptions> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.EhsimsExceptions c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("EhsimsExceptions")
					.Append("|").Append((System.Int32)reader[((int)EhsimsExceptionsColumn.EhsimsExceptionId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<EhsimsExceptions>(
					key.ToString(), // EntityTrackingKey
					"EhsimsExceptions",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.EhsimsExceptions();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.EhsimsExceptionId = (System.Int32)reader[((int)EhsimsExceptionsColumn.EhsimsExceptionId - 1)];
					c.ReportDateTime = (System.DateTime)reader[((int)EhsimsExceptionsColumn.ReportDateTime - 1)];
					c.CompanyId = (reader.IsDBNull(((int)EhsimsExceptionsColumn.CompanyId - 1)))?null:(System.Int32?)reader[((int)EhsimsExceptionsColumn.CompanyId - 1)];
					c.SiteId = (reader.IsDBNull(((int)EhsimsExceptionsColumn.SiteId - 1)))?null:(System.Int32?)reader[((int)EhsimsExceptionsColumn.SiteId - 1)];
					c.KpiDateTime = (reader.IsDBNull(((int)EhsimsExceptionsColumn.KpiDateTime - 1)))?null:(System.DateTime?)reader[((int)EhsimsExceptionsColumn.KpiDateTime - 1)];
					c.ErrorMsg = (reader.IsDBNull(((int)EhsimsExceptionsColumn.ErrorMsg - 1)))?null:(System.String)reader[((int)EhsimsExceptionsColumn.ErrorMsg - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.EhsimsExceptions"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.EhsimsExceptions"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.EhsimsExceptions entity)
		{
			if (!reader.Read()) return;
			
			entity.EhsimsExceptionId = (System.Int32)reader[((int)EhsimsExceptionsColumn.EhsimsExceptionId - 1)];
			entity.ReportDateTime = (System.DateTime)reader[((int)EhsimsExceptionsColumn.ReportDateTime - 1)];
			entity.CompanyId = (reader.IsDBNull(((int)EhsimsExceptionsColumn.CompanyId - 1)))?null:(System.Int32?)reader[((int)EhsimsExceptionsColumn.CompanyId - 1)];
			entity.SiteId = (reader.IsDBNull(((int)EhsimsExceptionsColumn.SiteId - 1)))?null:(System.Int32?)reader[((int)EhsimsExceptionsColumn.SiteId - 1)];
			entity.KpiDateTime = (reader.IsDBNull(((int)EhsimsExceptionsColumn.KpiDateTime - 1)))?null:(System.DateTime?)reader[((int)EhsimsExceptionsColumn.KpiDateTime - 1)];
			entity.ErrorMsg = (reader.IsDBNull(((int)EhsimsExceptionsColumn.ErrorMsg - 1)))?null:(System.String)reader[((int)EhsimsExceptionsColumn.ErrorMsg - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.EhsimsExceptions"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.EhsimsExceptions"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.EhsimsExceptions entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.EhsimsExceptionId = (System.Int32)dataRow["EhsimsExceptionId"];
			entity.ReportDateTime = (System.DateTime)dataRow["ReportDateTime"];
			entity.CompanyId = Convert.IsDBNull(dataRow["CompanyId"]) ? null : (System.Int32?)dataRow["CompanyId"];
			entity.SiteId = Convert.IsDBNull(dataRow["SiteId"]) ? null : (System.Int32?)dataRow["SiteId"];
			entity.KpiDateTime = Convert.IsDBNull(dataRow["kpiDateTime"]) ? null : (System.DateTime?)dataRow["kpiDateTime"];
			entity.ErrorMsg = Convert.IsDBNull(dataRow["ErrorMsg"]) ? null : (System.String)dataRow["ErrorMsg"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.EhsimsExceptions"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.EhsimsExceptions Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.EhsimsExceptions entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.EhsimsExceptions object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.EhsimsExceptions instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.EhsimsExceptions Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.EhsimsExceptions entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region EhsimsExceptionsChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.EhsimsExceptions</c>
	///</summary>
	public enum EhsimsExceptionsChildEntityTypes
	{
	}
	
	#endregion EhsimsExceptionsChildEntityTypes
	
	#region EhsimsExceptionsFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EhsimsExceptionsColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EhsimsExceptions"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EhsimsExceptionsFilterBuilder : SqlFilterBuilder<EhsimsExceptionsColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EhsimsExceptionsFilterBuilder class.
		/// </summary>
		public EhsimsExceptionsFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the EhsimsExceptionsFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public EhsimsExceptionsFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the EhsimsExceptionsFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public EhsimsExceptionsFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion EhsimsExceptionsFilterBuilder
	
	#region EhsimsExceptionsParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EhsimsExceptionsColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EhsimsExceptions"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EhsimsExceptionsParameterBuilder : ParameterizedSqlFilterBuilder<EhsimsExceptionsColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EhsimsExceptionsParameterBuilder class.
		/// </summary>
		public EhsimsExceptionsParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the EhsimsExceptionsParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public EhsimsExceptionsParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the EhsimsExceptionsParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public EhsimsExceptionsParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion EhsimsExceptionsParameterBuilder
	
	#region EhsimsExceptionsSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EhsimsExceptionsColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EhsimsExceptions"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class EhsimsExceptionsSortBuilder : SqlSortBuilder<EhsimsExceptionsColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EhsimsExceptionsSqlSortBuilder class.
		/// </summary>
		public EhsimsExceptionsSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion EhsimsExceptionsSortBuilder
	
} // end namespace
