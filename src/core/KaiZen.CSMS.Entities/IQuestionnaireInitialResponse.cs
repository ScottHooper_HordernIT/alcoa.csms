﻿using System;
using System.ComponentModel;

namespace KaiZen.CSMS.Entities
{
	/// <summary>
	///		The data structure representation of the 'QuestionnaireInitialResponse' table via interface.
	/// </summary>
	/// <remarks>
	/// 	This struct is generated by a tool and should never be modified.
	/// </remarks>
	public interface IQuestionnaireInitialResponse 
	{
		/// <summary>			
		/// ResponseId : 
		/// </summary>
		/// <remarks>Member of the primary key of the underlying table "QuestionnaireInitialResponse"</remarks>
		System.Int32 ResponseId { get; set; }
				
		
		
		/// <summary>
		/// QuestionId : 
		/// </summary>
		System.String  QuestionId  { get; set; }
		
		/// <summary>
		/// AnswerText : 
		/// </summary>
		System.String  AnswerText  { get; set; }
		
		/// <summary>
		/// QuestionnaireId : 
		/// </summary>
		System.Int32  QuestionnaireId  { get; set; }
		
		/// <summary>
		/// ModifiedByUserId : 
		/// </summary>
		System.Int32  ModifiedByUserId  { get; set; }
		
		/// <summary>
		/// ModifiedDate : 
		/// </summary>
		System.DateTime  ModifiedDate  { get; set; }
			
		/// <summary>
		/// Creates a new object that is a copy of the current instance.
		/// </summary>
		/// <returns>A new object that is a copy of this instance.</returns>
		System.Object Clone();
		
		#region Data Properties

		#endregion Data Properties

	}
}


