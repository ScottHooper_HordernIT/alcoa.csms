﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.QuestionnaireVerificationResponseAuditProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(QuestionnaireVerificationResponseAuditDataSourceDesigner))]
	public class QuestionnaireVerificationResponseAuditDataSource : ProviderDataSource<QuestionnaireVerificationResponseAudit, QuestionnaireVerificationResponseAuditKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireVerificationResponseAuditDataSource class.
		/// </summary>
		public QuestionnaireVerificationResponseAuditDataSource() : base(new QuestionnaireVerificationResponseAuditService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the QuestionnaireVerificationResponseAuditDataSourceView used by the QuestionnaireVerificationResponseAuditDataSource.
		/// </summary>
		protected QuestionnaireVerificationResponseAuditDataSourceView QuestionnaireVerificationResponseAuditView
		{
			get { return ( View as QuestionnaireVerificationResponseAuditDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the QuestionnaireVerificationResponseAuditDataSource control invokes to retrieve data.
		/// </summary>
		public QuestionnaireVerificationResponseAuditSelectMethod SelectMethod
		{
			get
			{
				QuestionnaireVerificationResponseAuditSelectMethod selectMethod = QuestionnaireVerificationResponseAuditSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (QuestionnaireVerificationResponseAuditSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the QuestionnaireVerificationResponseAuditDataSourceView class that is to be
		/// used by the QuestionnaireVerificationResponseAuditDataSource.
		/// </summary>
		/// <returns>An instance of the QuestionnaireVerificationResponseAuditDataSourceView class.</returns>
		protected override BaseDataSourceView<QuestionnaireVerificationResponseAudit, QuestionnaireVerificationResponseAuditKey> GetNewDataSourceView()
		{
			return new QuestionnaireVerificationResponseAuditDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the QuestionnaireVerificationResponseAuditDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class QuestionnaireVerificationResponseAuditDataSourceView : ProviderDataSourceView<QuestionnaireVerificationResponseAudit, QuestionnaireVerificationResponseAuditKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireVerificationResponseAuditDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the QuestionnaireVerificationResponseAuditDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public QuestionnaireVerificationResponseAuditDataSourceView(QuestionnaireVerificationResponseAuditDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal QuestionnaireVerificationResponseAuditDataSource QuestionnaireVerificationResponseAuditOwner
		{
			get { return Owner as QuestionnaireVerificationResponseAuditDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal QuestionnaireVerificationResponseAuditSelectMethod SelectMethod
		{
			get { return QuestionnaireVerificationResponseAuditOwner.SelectMethod; }
			set { QuestionnaireVerificationResponseAuditOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal QuestionnaireVerificationResponseAuditService QuestionnaireVerificationResponseAuditProvider
		{
			get { return Provider as QuestionnaireVerificationResponseAuditService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<QuestionnaireVerificationResponseAudit> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<QuestionnaireVerificationResponseAudit> results = null;
			QuestionnaireVerificationResponseAudit item;
			count = 0;
			
			System.Int32 _auditId;
			System.Int32? _questionnaireId_nullable;
			System.Int32? _sectionId_nullable;
			System.Int32? _questionId_nullable;
			System.Int32? _modifiedByUserId_nullable;
			System.Int32? _responseId_nullable;

			switch ( SelectMethod )
			{
				case QuestionnaireVerificationResponseAuditSelectMethod.Get:
					QuestionnaireVerificationResponseAuditKey entityKey  = new QuestionnaireVerificationResponseAuditKey();
					entityKey.Load(values);
					item = QuestionnaireVerificationResponseAuditProvider.Get(entityKey);
					results = new TList<QuestionnaireVerificationResponseAudit>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case QuestionnaireVerificationResponseAuditSelectMethod.GetAll:
                    results = QuestionnaireVerificationResponseAuditProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case QuestionnaireVerificationResponseAuditSelectMethod.GetPaged:
					results = QuestionnaireVerificationResponseAuditProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case QuestionnaireVerificationResponseAuditSelectMethod.Find:
					if ( FilterParameters != null )
						results = QuestionnaireVerificationResponseAuditProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = QuestionnaireVerificationResponseAuditProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case QuestionnaireVerificationResponseAuditSelectMethod.GetByAuditId:
					_auditId = ( values["AuditId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["AuditId"], typeof(System.Int32)) : (int)0;
					item = QuestionnaireVerificationResponseAuditProvider.GetByAuditId(_auditId);
					results = new TList<QuestionnaireVerificationResponseAudit>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				case QuestionnaireVerificationResponseAuditSelectMethod.GetByQuestionnaireIdSectionIdQuestionId:
					_questionnaireId_nullable = (System.Int32?) EntityUtil.ChangeType(values["QuestionnaireId"], typeof(System.Int32?));
					_sectionId_nullable = (System.Int32?) EntityUtil.ChangeType(values["SectionId"], typeof(System.Int32?));
					_questionId_nullable = (System.Int32?) EntityUtil.ChangeType(values["QuestionId"], typeof(System.Int32?));
					results = QuestionnaireVerificationResponseAuditProvider.GetByQuestionnaireIdSectionIdQuestionId(_questionnaireId_nullable, _sectionId_nullable, _questionId_nullable, this.StartIndex, this.PageSize, out count);
					break;
				case QuestionnaireVerificationResponseAuditSelectMethod.GetByModifiedByUserId:
					_modifiedByUserId_nullable = (System.Int32?) EntityUtil.ChangeType(values["ModifiedByUserId"], typeof(System.Int32?));
					results = QuestionnaireVerificationResponseAuditProvider.GetByModifiedByUserId(_modifiedByUserId_nullable, this.StartIndex, this.PageSize, out count);
					break;
				case QuestionnaireVerificationResponseAuditSelectMethod.GetByResponseId:
					_responseId_nullable = (System.Int32?) EntityUtil.ChangeType(values["ResponseId"], typeof(System.Int32?));
					results = QuestionnaireVerificationResponseAuditProvider.GetByResponseId(_responseId_nullable, this.StartIndex, this.PageSize, out count);
					break;
				// FK
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == QuestionnaireVerificationResponseAuditSelectMethod.Get || SelectMethod == QuestionnaireVerificationResponseAuditSelectMethod.GetByAuditId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				QuestionnaireVerificationResponseAudit entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					QuestionnaireVerificationResponseAuditProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<QuestionnaireVerificationResponseAudit> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			QuestionnaireVerificationResponseAuditProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region QuestionnaireVerificationResponseAuditDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the QuestionnaireVerificationResponseAuditDataSource class.
	/// </summary>
	public class QuestionnaireVerificationResponseAuditDataSourceDesigner : ProviderDataSourceDesigner<QuestionnaireVerificationResponseAudit, QuestionnaireVerificationResponseAuditKey>
	{
		/// <summary>
		/// Initializes a new instance of the QuestionnaireVerificationResponseAuditDataSourceDesigner class.
		/// </summary>
		public QuestionnaireVerificationResponseAuditDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public QuestionnaireVerificationResponseAuditSelectMethod SelectMethod
		{
			get { return ((QuestionnaireVerificationResponseAuditDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new QuestionnaireVerificationResponseAuditDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region QuestionnaireVerificationResponseAuditDataSourceActionList

	/// <summary>
	/// Supports the QuestionnaireVerificationResponseAuditDataSourceDesigner class.
	/// </summary>
	internal class QuestionnaireVerificationResponseAuditDataSourceActionList : DesignerActionList
	{
		private QuestionnaireVerificationResponseAuditDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the QuestionnaireVerificationResponseAuditDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public QuestionnaireVerificationResponseAuditDataSourceActionList(QuestionnaireVerificationResponseAuditDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public QuestionnaireVerificationResponseAuditSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion QuestionnaireVerificationResponseAuditDataSourceActionList
	
	#endregion QuestionnaireVerificationResponseAuditDataSourceDesigner
	
	#region QuestionnaireVerificationResponseAuditSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the QuestionnaireVerificationResponseAuditDataSource.SelectMethod property.
	/// </summary>
	public enum QuestionnaireVerificationResponseAuditSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByAuditId method.
		/// </summary>
		GetByAuditId,
		/// <summary>
		/// Represents the GetByQuestionnaireIdSectionIdQuestionId method.
		/// </summary>
		GetByQuestionnaireIdSectionIdQuestionId,
		/// <summary>
		/// Represents the GetByModifiedByUserId method.
		/// </summary>
		GetByModifiedByUserId,
		/// <summary>
		/// Represents the GetByResponseId method.
		/// </summary>
		GetByResponseId
	}
	
	#endregion QuestionnaireVerificationResponseAuditSelectMethod

	#region QuestionnaireVerificationResponseAuditFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireVerificationResponseAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireVerificationResponseAuditFilter : SqlFilter<QuestionnaireVerificationResponseAuditColumn>
	{
	}
	
	#endregion QuestionnaireVerificationResponseAuditFilter

	#region QuestionnaireVerificationResponseAuditExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireVerificationResponseAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireVerificationResponseAuditExpressionBuilder : SqlExpressionBuilder<QuestionnaireVerificationResponseAuditColumn>
	{
	}
	
	#endregion QuestionnaireVerificationResponseAuditExpressionBuilder	

	#region QuestionnaireVerificationResponseAuditProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;QuestionnaireVerificationResponseAuditChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireVerificationResponseAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireVerificationResponseAuditProperty : ChildEntityProperty<QuestionnaireVerificationResponseAuditChildEntityTypes>
	{
	}
	
	#endregion QuestionnaireVerificationResponseAuditProperty
}

