<%@ Control Language="C#" AutoEventWireup="true" Inherits="UserControls_Admin_RegionSites" Codebehind="RegionsSites.ascx.cs" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
    
<dxwgv:aspxgridview id="grid"
    runat="server" autogeneratecolumns="False"
    cssfilepath="~/App_Themes/Office2003Blue/{0}/styles.css" csspostfix="Office2003Blue"
    datasourceid="dsRegionsSites" keyfieldname="RegionSiteId"
    OnRowUpdating="grid_RowUpdating" OnRowInserting="grid_RowInserting"
    >
    <ImagesFilterControl>
        <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
        </LoadingPanel>
    </ImagesFilterControl>
<Styles CssPostfix="Office2003Blue" 
        CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css">
<Header SortingImageSpacing="5px" ImageSpacing="5px"></Header>

<LoadingPanel ImageSpacing="10px"></LoadingPanel>
</Styles>

<Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
    <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
    </LoadingPanelOnStatusBar>
<CollapsedButton Height="12px" Width="11px"></CollapsedButton>

<ExpandedButton Height="12px" Width="11px"></ExpandedButton>

<DetailCollapsedButton Height="12px" Width="11px"></DetailCollapsedButton>

<DetailExpandedButton Height="12px" Width="11px"></DetailExpandedButton>

<FilterRowButton Height="13px" Width="13px"></FilterRowButton>
    <LoadingPanel Url="~/App_Themes/Office2003BlueOld/GridView/Loading.gif"><%--Enhancement_023:change by debashis for testing platform upgradation--%>
    </LoadingPanel>
</Images>

<SettingsEditing Mode="Inline"></SettingsEditing>
<Columns>
    <dxwgv:GridViewCommandColumn VisibleIndex="0">
        <EditButton Visible="True">
        </EditButton>
        <NewButton Visible="True">
        </NewButton>
        <DeleteButton Visible="True">
        </DeleteButton>
    </dxwgv:GridViewCommandColumn>
    <dxwgv:GridViewDataTextColumn FieldName="RegionSiteId" ReadOnly="True" Visible="False"
        VisibleIndex="0">
        <EditFormSettings Visible="False" />
    </dxwgv:GridViewDataTextColumn>
   
    <dxwgv:GridViewDataComboBoxColumn Caption="Region" FieldName="RegionId"
            unboundType="String" VisibleIndex="1" SortIndex="0" SortOrder="Ascending">
            <PropertiesComboBox DataSourceID="dsRegionsFilter" DropDownHeight="150px" TextField="RegionName"
                ValueField="RegionId" ValueType="System.Int32" IncrementalFilteringMode="StartsWith">
            </PropertiesComboBox>
        <Settings SortMode="DisplayText" />
        <EditFormSettings Visible="true" />
       
    </dxwgv:GridViewDataComboBoxColumn>
    
    <dxwgv:GridViewDataComboBoxColumn Caption="Site" FieldName="SiteId"
            unboundType="String" VisibleIndex="2" SortIndex="1" SortOrder="Ascending">
            <PropertiesComboBox DataSourceID="dsSitesFilter" DropDownHeight="150px" TextField="SiteName"
                ValueField="SiteId" ValueType="System.Int32" IncrementalFilteringMode="StartsWith">
            </PropertiesComboBox>
        <Settings SortMode="DisplayText" />
        <EditFormSettings Visible="true" />
    </dxwgv:GridViewDataComboBoxColumn>
    
</Columns>
<Settings ShowFilterBar="Visible" ShowFilterRow="true"/>
    <SettingsPager NumericButtonCount="25" PageSize="25">
        <AllButton Visible="True"></AllButton>
    </SettingsPager>
    <SettingsBehavior ConfirmDelete="True"></SettingsBehavior>
</dxwgv:aspxgridview>

<data:RegionsSitesDataSource ID="dsRegionsSites" runat="server" Filter="RegionId != 8"></data:RegionsSitesDataSource>
<data:SitesDataSource ID="dsSitesFilter" runat="server" Filter="IsVisible = True AND Editable = True" Sort="SiteName ASC"></data:SitesDataSource> 
<data:RegionsDataSource ID="dsRegionsFilter" runat="server" Filter="IsVisible = True" Sort="RegionName ASC"></data:RegionsDataSource> 