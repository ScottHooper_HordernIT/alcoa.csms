﻿#region Using directives

using System;

#endregion

namespace KaiZen.CSMS.Entities
{	
	///<summary>
	/// An object representation of the 'ConfigSa812Sites' view. [No description found in the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class ConfigSa812Sites : ConfigSa812SitesBase
	{
		#region Constructors

		///<summary>
		/// Creates a new <see cref="ConfigSa812Sites"/> instance.
		///</summary>
		public ConfigSa812Sites():base(){}	
		
		#endregion
	}
}
