﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="AdminTaskSourceProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class AdminTaskSourceProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.AdminTaskSource, KaiZen.CSMS.Entities.AdminTaskSourceKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.AdminTaskSourceKey key)
		{
			return Delete(transactionManager, key.AdminTaskSourceId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_adminTaskSourceId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _adminTaskSourceId)
		{
			return Delete(null, _adminTaskSourceId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_adminTaskSourceId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _adminTaskSourceId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.AdminTaskSource Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.AdminTaskSourceKey key, int start, int pageLength)
		{
			return GetByAdminTaskSourceId(transactionManager, key.AdminTaskSourceId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_AdminTaskSource index.
		/// </summary>
		/// <param name="_adminTaskSourceId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdminTaskSource"/> class.</returns>
		public KaiZen.CSMS.Entities.AdminTaskSource GetByAdminTaskSourceId(System.Int32 _adminTaskSourceId)
		{
			int count = -1;
			return GetByAdminTaskSourceId(null,_adminTaskSourceId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_AdminTaskSource index.
		/// </summary>
		/// <param name="_adminTaskSourceId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdminTaskSource"/> class.</returns>
		public KaiZen.CSMS.Entities.AdminTaskSource GetByAdminTaskSourceId(System.Int32 _adminTaskSourceId, int start, int pageLength)
		{
			int count = -1;
			return GetByAdminTaskSourceId(null, _adminTaskSourceId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_AdminTaskSource index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_adminTaskSourceId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdminTaskSource"/> class.</returns>
		public KaiZen.CSMS.Entities.AdminTaskSource GetByAdminTaskSourceId(TransactionManager transactionManager, System.Int32 _adminTaskSourceId)
		{
			int count = -1;
			return GetByAdminTaskSourceId(transactionManager, _adminTaskSourceId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_AdminTaskSource index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_adminTaskSourceId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdminTaskSource"/> class.</returns>
		public KaiZen.CSMS.Entities.AdminTaskSource GetByAdminTaskSourceId(TransactionManager transactionManager, System.Int32 _adminTaskSourceId, int start, int pageLength)
		{
			int count = -1;
			return GetByAdminTaskSourceId(transactionManager, _adminTaskSourceId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_AdminTaskSource index.
		/// </summary>
		/// <param name="_adminTaskSourceId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdminTaskSource"/> class.</returns>
		public KaiZen.CSMS.Entities.AdminTaskSource GetByAdminTaskSourceId(System.Int32 _adminTaskSourceId, int start, int pageLength, out int count)
		{
			return GetByAdminTaskSourceId(null, _adminTaskSourceId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_AdminTaskSource index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_adminTaskSourceId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdminTaskSource"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.AdminTaskSource GetByAdminTaskSourceId(TransactionManager transactionManager, System.Int32 _adminTaskSourceId, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key UK_AdminTaskSourceName index.
		/// </summary>
		/// <param name="_adminTaskSourceName"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdminTaskSource"/> class.</returns>
		public KaiZen.CSMS.Entities.AdminTaskSource GetByAdminTaskSourceName(System.String _adminTaskSourceName)
		{
			int count = -1;
			return GetByAdminTaskSourceName(null,_adminTaskSourceName, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the UK_AdminTaskSourceName index.
		/// </summary>
		/// <param name="_adminTaskSourceName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdminTaskSource"/> class.</returns>
		public KaiZen.CSMS.Entities.AdminTaskSource GetByAdminTaskSourceName(System.String _adminTaskSourceName, int start, int pageLength)
		{
			int count = -1;
			return GetByAdminTaskSourceName(null, _adminTaskSourceName, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the UK_AdminTaskSourceName index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_adminTaskSourceName"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdminTaskSource"/> class.</returns>
		public KaiZen.CSMS.Entities.AdminTaskSource GetByAdminTaskSourceName(TransactionManager transactionManager, System.String _adminTaskSourceName)
		{
			int count = -1;
			return GetByAdminTaskSourceName(transactionManager, _adminTaskSourceName, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the UK_AdminTaskSourceName index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_adminTaskSourceName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdminTaskSource"/> class.</returns>
		public KaiZen.CSMS.Entities.AdminTaskSource GetByAdminTaskSourceName(TransactionManager transactionManager, System.String _adminTaskSourceName, int start, int pageLength)
		{
			int count = -1;
			return GetByAdminTaskSourceName(transactionManager, _adminTaskSourceName, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the UK_AdminTaskSourceName index.
		/// </summary>
		/// <param name="_adminTaskSourceName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdminTaskSource"/> class.</returns>
		public KaiZen.CSMS.Entities.AdminTaskSource GetByAdminTaskSourceName(System.String _adminTaskSourceName, int start, int pageLength, out int count)
		{
			return GetByAdminTaskSourceName(null, _adminTaskSourceName, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the UK_AdminTaskSourceName index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_adminTaskSourceName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdminTaskSource"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.AdminTaskSource GetByAdminTaskSourceName(TransactionManager transactionManager, System.String _adminTaskSourceName, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;AdminTaskSource&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;AdminTaskSource&gt;"/></returns>
		public static TList<AdminTaskSource> Fill(IDataReader reader, TList<AdminTaskSource> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.AdminTaskSource c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("AdminTaskSource")
					.Append("|").Append((System.Int32)reader[((int)AdminTaskSourceColumn.AdminTaskSourceId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<AdminTaskSource>(
					key.ToString(), // EntityTrackingKey
					"AdminTaskSource",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.AdminTaskSource();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.AdminTaskSourceId = (System.Int32)reader[((int)AdminTaskSourceColumn.AdminTaskSourceId - 1)];
					c.AdminTaskSourceName = (System.String)reader[((int)AdminTaskSourceColumn.AdminTaskSourceName - 1)];
					c.AdminTaskSourceDesc = (System.String)reader[((int)AdminTaskSourceColumn.AdminTaskSourceDesc - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.AdminTaskSource"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.AdminTaskSource"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.AdminTaskSource entity)
		{
			if (!reader.Read()) return;
			
			entity.AdminTaskSourceId = (System.Int32)reader[((int)AdminTaskSourceColumn.AdminTaskSourceId - 1)];
			entity.AdminTaskSourceName = (System.String)reader[((int)AdminTaskSourceColumn.AdminTaskSourceName - 1)];
			entity.AdminTaskSourceDesc = (System.String)reader[((int)AdminTaskSourceColumn.AdminTaskSourceDesc - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.AdminTaskSource"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.AdminTaskSource"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.AdminTaskSource entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.AdminTaskSourceId = (System.Int32)dataRow["AdminTaskSourceId"];
			entity.AdminTaskSourceName = (System.String)dataRow["AdminTaskSourceName"];
			entity.AdminTaskSourceDesc = (System.String)dataRow["AdminTaskSourceDesc"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.AdminTaskSource"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.AdminTaskSource Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.AdminTaskSource entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetByAdminTaskSourceId methods when available
			
			#region AdminTaskCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<AdminTask>|AdminTaskCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'AdminTaskCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.AdminTaskCollection = DataRepository.AdminTaskProvider.GetByAdminTaskSourceId(transactionManager, entity.AdminTaskSourceId);

				if (deep && entity.AdminTaskCollection.Count > 0)
				{
					deepHandles.Add("AdminTaskCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<AdminTask>) DataRepository.AdminTaskProvider.DeepLoad,
						new object[] { transactionManager, entity.AdminTaskCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.AdminTaskSource object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.AdminTaskSource instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.AdminTaskSource Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.AdminTaskSource entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<AdminTask>
				if (CanDeepSave(entity.AdminTaskCollection, "List<AdminTask>|AdminTaskCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(AdminTask child in entity.AdminTaskCollection)
					{
						if(child.AdminTaskSourceIdSource != null)
						{
							child.AdminTaskSourceId = child.AdminTaskSourceIdSource.AdminTaskSourceId;
						}
						else
						{
							child.AdminTaskSourceId = entity.AdminTaskSourceId;
						}

					}

					if (entity.AdminTaskCollection.Count > 0 || entity.AdminTaskCollection.DeletedItems.Count > 0)
					{
						//DataRepository.AdminTaskProvider.Save(transactionManager, entity.AdminTaskCollection);
						
						deepHandles.Add("AdminTaskCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< AdminTask >) DataRepository.AdminTaskProvider.DeepSave,
							new object[] { transactionManager, entity.AdminTaskCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region AdminTaskSourceChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.AdminTaskSource</c>
	///</summary>
	public enum AdminTaskSourceChildEntityTypes
	{

		///<summary>
		/// Collection of <c>AdminTaskSource</c> as OneToMany for AdminTaskCollection
		///</summary>
		[ChildEntityType(typeof(TList<AdminTask>))]
		AdminTaskCollection,
	}
	
	#endregion AdminTaskSourceChildEntityTypes
	
	#region AdminTaskSourceFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;AdminTaskSourceColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AdminTaskSource"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AdminTaskSourceFilterBuilder : SqlFilterBuilder<AdminTaskSourceColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AdminTaskSourceFilterBuilder class.
		/// </summary>
		public AdminTaskSourceFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the AdminTaskSourceFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AdminTaskSourceFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AdminTaskSourceFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AdminTaskSourceFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AdminTaskSourceFilterBuilder
	
	#region AdminTaskSourceParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;AdminTaskSourceColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AdminTaskSource"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AdminTaskSourceParameterBuilder : ParameterizedSqlFilterBuilder<AdminTaskSourceColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AdminTaskSourceParameterBuilder class.
		/// </summary>
		public AdminTaskSourceParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the AdminTaskSourceParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AdminTaskSourceParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AdminTaskSourceParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AdminTaskSourceParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AdminTaskSourceParameterBuilder
	
	#region AdminTaskSourceSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;AdminTaskSourceColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AdminTaskSource"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class AdminTaskSourceSortBuilder : SqlSortBuilder<AdminTaskSourceColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AdminTaskSourceSqlSortBuilder class.
		/// </summary>
		public AdminTaskSourceSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion AdminTaskSourceSortBuilder
	
} // end namespace
