
/****** Object:  StoredProcedure [dbo].[_Embeded_CmpCompliant_DashBoard]    Script Date: 3/08/2015 1:39:03 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[_Embeded_CmpCompliant_DashBoard]                              
(                              
 @SiteId int,                              
 @Month int,                              
 @Year int                              
)                              
AS                    
        
DECLARE @SiteIdN int         
        
--IF (@SiteId < 0)                               
-- BEGIN          
--  SELECT @SiteIdN = [dbo].[GetSiteIdByComboRegionId](@SiteId)          
-- END          
--ELSE          
-- BEGIN          
--  SET @SiteIdN = @SiteId          
-- END    
  
SELECT @SiteIdN = [dbo].[GetSiteIdByComboRegionId](@SiteId)          
                              
 BEGIN                  
  SELECT DISTINCT                  
  ----                  
  (SELECT A.KpiScoreYtd FROM dbo.AdHoc_Radar_Items A LEFT OUTER JOIN dbo.AdHoc_Radar B                  
  ON A.AdHoc_RadarId = B.AdHoc_RadarId WHERE A.SiteId = C.SiteId                  
  AND  A.[Year] = C.[Year]                  
  --AND MONTH(A.ModifiedDate) = MONTH(C.ModifiedDate)                  
  AND B.CompanySiteCategoryId = 1                  
  AND B.AdHoc_RadarId = 7 )[Number of Companies:],                  
  ------                  
  (SELECT A.KpiScoreYtd FROM dbo.AdHoc_Radar_Items A LEFT OUTER JOIN dbo.AdHoc_Radar B                  
  ON A.AdHoc_RadarId = B.AdHoc_RadarId WHERE A.SiteId = C.SiteId                  
  AND  A.[Year] = C.[Year]                  
  --AND MONTH(A.ModifiedDate) = MONTH(C.ModifiedDate)                  
  AND B.CompanySiteCategoryId = 1                 
  AND B.AdHoc_RadarId = 8)[Contractor Services Audit],              
  ------                  
  (SELECT A.KpiScoreYtd FROM dbo.AdHoc_Radar_Items A LEFT OUTER JOIN dbo.AdHoc_Radar B                  
  ON A.AdHoc_RadarId = B.AdHoc_RadarId WHERE A.SiteId = C.SiteId                  
  AND  A.[Year] = C.[Year]                  
  --AND MONTH(A.ModifiedDate) = MONTH(C.ModifiedDate)                  
  AND B.CompanySiteCategoryId = 1                  
  AND B.AdHoc_RadarId = 10)[Workplace Safety Compliance],           
  -----                  
  (SELECT A.KpiScoreYtd FROM dbo.AdHoc_Radar_Items A LEFT OUTER JOIN dbo.AdHoc_Radar B                  
  ON A.AdHoc_RadarId = B.AdHoc_RadarId WHERE A.SiteId = C.SiteId                  
  AND  A.[Year] = C.[Year]                  
  --AND MONTH(A.ModifiedDate) = MONTH(C.ModifiedDate)                  
  AND B.CompanySiteCategoryId = 1                 
  AND B.AdHoc_RadarId = 11)[Management Health Safety Work Contacts],          
  -----                  
  (SELECT A.KpiScoreYtd FROM dbo.AdHoc_Radar_Items A LEFT OUTER JOIN dbo.AdHoc_Radar B                  
  ON A.AdHoc_RadarId = B.AdHoc_RadarId WHERE A.SiteId = C.SiteId                  
  AND  A.[Year] = C.[Year]                  
  --AND MONTH(A.ModifiedDate) = MONTH(C.ModifiedDate)                  
  AND B.CompanySiteCategoryId = 1                 
  AND B.AdHoc_RadarId = 12)[Behavioural Observations],                
  -----                  
  (SELECT A.KpiScoreYtd FROM dbo.AdHoc_Radar_Items A LEFT OUTER JOIN dbo.AdHoc_Radar B                  
  ON A.AdHoc_RadarId = B.AdHoc_RadarId WHERE A.SiteId = C.SiteId                  
  AND  A.[Year] = C.[Year]                  
  --AND MONTH(A.ModifiedDate) = MONTH(C.ModifiedDate)                  
  AND B.CompanySiteCategoryId = 1                  
  AND B.AdHoc_RadarId = 13)[Fatality Prevention Program],             
  ------                  
  (SELECT A.KpiScoreYtd FROM dbo.AdHoc_Radar_Items A LEFT OUTER JOIN dbo.AdHoc_Radar B                  
  ON A.AdHoc_RadarId = B.AdHoc_RadarId WHERE A.SiteId = C.SiteId                  
  AND  A.[Year] = C.[Year]                  
  --AND MONTH(A.ModifiedDate) = MONTH(C.ModifiedDate)                  
  AND B.CompanySiteCategoryId = 1                 
  AND B.AdHoc_RadarId = 14)[JSA Field Audit Verifications],               
  -----                  
  (SELECT A.KpiScoreYtd FROM dbo.AdHoc_Radar_Items A LEFT OUTER JOIN dbo.AdHoc_Radar B                  
ON A.AdHoc_RadarId = B.AdHoc_RadarId WHERE A.SiteId = C.SiteId                  
  AND  A.[Year] = C.[Year]                  
  --AND MONTH(A.ModifiedDate) = MONTH(C.ModifiedDate)                  
  AND B.CompanySiteCategoryId = 1                
  AND B.AdHoc_RadarId = 15)[Toolbox Meetings],           
  -----                  
  (SELECT A.KpiScoreYtd FROM dbo.AdHoc_Radar_Items A LEFT OUTER JOIN dbo.AdHoc_Radar B                  
  ON A.AdHoc_RadarId = B.AdHoc_RadarId WHERE A.SiteId = C.SiteId                  
  AND  A.[Year] = C.[Year]                  
  --AND MONTH(A.ModifiedDate) = MONTH(C.ModifiedDate)                  
  AND B.CompanySiteCategoryId = 1                 
  AND B.AdHoc_RadarId = 16)[Alcoa Weekly Contractors Meeting],              
  -----         
  (SELECT A.KpiScoreYtd FROM dbo.AdHoc_Radar_Items A LEFT OUTER JOIN dbo.AdHoc_Radar B                  
  ON A.AdHoc_RadarId = B.AdHoc_RadarId WHERE A.SiteId = C.SiteId                  
  AND  A.[Year] = C.[Year]                  
  --AND MONTH(A.ModifiedDate) = MONTH(C.ModifiedDate)                  
  AND B.CompanySiteCategoryId = 1                 
  AND B.AdHoc_RadarId = 17)[Alcoa Monthly Contractors Meeting],               
  -----                  
  (SELECT A.KpiScoreYtd FROM dbo.AdHoc_Radar_Items A LEFT OUTER JOIN dbo.AdHoc_Radar B                  
  ON A.AdHoc_RadarId = B.AdHoc_RadarId WHERE A.SiteId = C.SiteId                  
  AND  A.[Year] = C.[Year]                  
  --AND MONTH(A.ModifiedDate) = MONTH(C.ModifiedDate)                  
  AND B.CompanySiteCategoryId = 1                  
  AND B.AdHoc_RadarId = 18)[Safety Plan(s) Submitted],                
  ----                  
  (SELECT A.KpiScoreYtd FROM dbo.AdHoc_Radar_Items A LEFT OUTER JOIN dbo.AdHoc_Radar B                  
  ON A.AdHoc_RadarId = B.AdHoc_RadarId WHERE A.SiteId = C.SiteId                  
  AND  A.[Year] = C.[Year]                  
  --AND MONTH(A.ModifiedDate) = MONTH(C.ModifiedDate)                  
  AND B.CompanySiteCategoryId = 1                  
  AND B.AdHoc_RadarId = 19)[Mandated Training]              
  ----                  
  FROM AdHoc_Radar_Items C WHERE C.SiteId = @SiteIdN           
  AND  C.[Year] = @Year                  
  --AND MONTH(C.ModifiedDate) = @Month                  
 END       
----------------------------------------

GO


