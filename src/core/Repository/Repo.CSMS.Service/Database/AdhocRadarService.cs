﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface IAdhocRadarService
    {
        AdHoc_Radar Get(Expression<Func<AdHoc_Radar, bool>> where, List<Expression<Func<AdHoc_Radar, object>>> includes);
        List<AdHoc_Radar> GetMany(int? top, Expression<Func<AdHoc_Radar, bool>> where, Func<IQueryable<AdHoc_Radar>, IOrderedQueryable<AdHoc_Radar>> orderBy, List<Expression<Func<AdHoc_Radar, object>>> includes);
        List<AdHoc_Radar> GetManyPaged(int pageNumber, int pageSize, Expression<Func<AdHoc_Radar, bool>> where, Func<IQueryable<AdHoc_Radar>, IOrderedQueryable<AdHoc_Radar>> orderBy, List<Expression<Func<AdHoc_Radar, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<AdHoc_Radar, bool>> where, List<Expression<Func<AdHoc_Radar, object>>> includes);
        void Insert(AdHoc_Radar item, bool saveChanges = true);
        void Update(AdHoc_Radar item, bool saveChanges = true);
        void Delete(AdHoc_Radar item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class AdhocRadarService : IAdhocRadarService
    {
        private readonly IRepository<AdHoc_Radar> _repository;

        public AdhocRadarService(IRepository<AdHoc_Radar> repository)
        {
            this._repository = repository;
        }

        public AdHoc_Radar Get(Expression<Func<AdHoc_Radar, bool>> where, List<Expression<Func<AdHoc_Radar, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<AdHoc_Radar> GetMany(int? top, Expression<Func<AdHoc_Radar, bool>> where, Func<IQueryable<AdHoc_Radar>, IOrderedQueryable<AdHoc_Radar>> orderBy, List<Expression<Func<AdHoc_Radar, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<AdHoc_Radar> GetManyPaged(int pageNumber, int pageSize, Expression<Func<AdHoc_Radar, bool>> where, Func<IQueryable<AdHoc_Radar>, IOrderedQueryable<AdHoc_Radar>> orderBy, List<Expression<Func<AdHoc_Radar, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<AdHoc_Radar, bool>> where, List<Expression<Func<AdHoc_Radar, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(AdHoc_Radar item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("AdHoc_Radar");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(AdHoc_Radar item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("AdHoc_Radar");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(AdHoc_Radar item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("AdHoc_Radar");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
