﻿using System;
using System.ComponentModel;

namespace KaiZen.CSMS.Entities
{
	/// <summary>
	///		The data structure representation of the 'TwentyOnePointAudit_18' table via interface.
	/// </summary>
	/// <remarks>
	/// 	This struct is generated by a tool and should never be modified.
	/// </remarks>
	public interface ITwentyOnePointAudit18 
	{
		/// <summary>			
		/// ID : 
		/// </summary>
		/// <remarks>Member of the primary key of the underlying table "TwentyOnePointAudit_18"</remarks>
		System.Int32 Id { get; set; }
				
		
		
		/// <summary>
		/// Achieved18a : 
		/// </summary>
		System.Int32?  Achieved18a  { get; set; }
		
		/// <summary>
		/// Achieved18b : 
		/// </summary>
		System.Int32?  Achieved18b  { get; set; }
		
		/// <summary>
		/// Achieved18c : 
		/// </summary>
		System.Int32?  Achieved18c  { get; set; }
		
		/// <summary>
		/// Achieved18d : 
		/// </summary>
		System.Int32?  Achieved18d  { get; set; }
		
		/// <summary>
		/// Achieved18e : 
		/// </summary>
		System.Int32?  Achieved18e  { get; set; }
		
		/// <summary>
		/// Achieved18f : 
		/// </summary>
		System.Int32?  Achieved18f  { get; set; }
		
		/// <summary>
		/// Achieved18g : 
		/// </summary>
		System.Int32?  Achieved18g  { get; set; }
		
		/// <summary>
		/// Achieved18h : 
		/// </summary>
		System.Int32?  Achieved18h  { get; set; }
		
		/// <summary>
		/// Achieved18i : 
		/// </summary>
		System.Int32?  Achieved18i  { get; set; }
		
		/// <summary>
		/// Achieved18j : 
		/// </summary>
		System.Int32?  Achieved18j  { get; set; }
		
		/// <summary>
		/// Achieved18k : 
		/// </summary>
		System.Int32?  Achieved18k  { get; set; }
		
		/// <summary>
		/// Achieved18l : 
		/// </summary>
		System.Int32?  Achieved18l  { get; set; }
		
		/// <summary>
		/// Achieved18m : 
		/// </summary>
		System.Int32?  Achieved18m  { get; set; }
		
		/// <summary>
		/// Achieved18n : 
		/// </summary>
		System.Int32?  Achieved18n  { get; set; }
		
		/// <summary>
		/// Achieved18o : 
		/// </summary>
		System.Int32?  Achieved18o  { get; set; }
		
		/// <summary>
		/// Achieved18p : 
		/// </summary>
		System.Int32?  Achieved18p  { get; set; }
		
		/// <summary>
		/// Observation18a : 
		/// </summary>
		System.String  Observation18a  { get; set; }
		
		/// <summary>
		/// Observation18b : 
		/// </summary>
		System.String  Observation18b  { get; set; }
		
		/// <summary>
		/// Observation18c : 
		/// </summary>
		System.String  Observation18c  { get; set; }
		
		/// <summary>
		/// Observation18d : 
		/// </summary>
		System.String  Observation18d  { get; set; }
		
		/// <summary>
		/// Observation18e : 
		/// </summary>
		System.String  Observation18e  { get; set; }
		
		/// <summary>
		/// Observation18f : 
		/// </summary>
		System.String  Observation18f  { get; set; }
		
		/// <summary>
		/// Observation18g : 
		/// </summary>
		System.String  Observation18g  { get; set; }
		
		/// <summary>
		/// Observation18h : 
		/// </summary>
		System.String  Observation18h  { get; set; }
		
		/// <summary>
		/// Observation18i : 
		/// </summary>
		System.String  Observation18i  { get; set; }
		
		/// <summary>
		/// Observation18j : 
		/// </summary>
		System.String  Observation18j  { get; set; }
		
		/// <summary>
		/// Observation18k : 
		/// </summary>
		System.String  Observation18k  { get; set; }
		
		/// <summary>
		/// Observation18l : 
		/// </summary>
		System.String  Observation18l  { get; set; }
		
		/// <summary>
		/// Observation18m : 
		/// </summary>
		System.String  Observation18m  { get; set; }
		
		/// <summary>
		/// Observation18n : 
		/// </summary>
		System.String  Observation18n  { get; set; }
		
		/// <summary>
		/// Observation18o : 
		/// </summary>
		System.String  Observation18o  { get; set; }
		
		/// <summary>
		/// Observation18p : 
		/// </summary>
		System.String  Observation18p  { get; set; }
		
		/// <summary>
		/// TotalScore : 
		/// </summary>
		System.Int32?  TotalScore  { get; set; }
			
		/// <summary>
		/// Creates a new object that is a copy of the current instance.
		/// </summary>
		/// <returns>A new object that is a copy of this instance.</returns>
		System.Object Clone();
		
		#region Data Properties


		/// <summary>
		///	Holds a collection of entity objects
		///	which are related to this object through the relation _twentyOnePointAuditPoint18Id
		/// </summary>	
		TList<TwentyOnePointAudit> TwentyOnePointAuditCollection {  get;  set;}	

		#endregion Data Properties

	}
}


