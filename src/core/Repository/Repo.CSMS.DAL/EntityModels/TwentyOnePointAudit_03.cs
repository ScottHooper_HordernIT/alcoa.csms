//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Repo.CSMS.DAL.EntityModels
{
    using System;
    using System.Collections.Generic;
    
    public partial class TwentyOnePointAudit_03
    {
        public TwentyOnePointAudit_03()
        {
            this.TwentyOnePointAudits = new HashSet<TwentyOnePointAudit>();
        }
    
        public int ID { get; set; }
        public Nullable<int> Achieved3a { get; set; }
        public Nullable<int> Achieved3b { get; set; }
        public Nullable<int> Achieved3c { get; set; }
        public Nullable<int> Achieved3d { get; set; }
        public Nullable<int> Achieved3e { get; set; }
        public Nullable<int> Achieved3f { get; set; }
        public Nullable<int> Achieved3g { get; set; }
        public Nullable<int> Achieved3h { get; set; }
        public Nullable<int> Achieved3i { get; set; }
        public string Observation3a { get; set; }
        public string Observation3b { get; set; }
        public string Observation3c { get; set; }
        public string Observation3d { get; set; }
        public string Observation3e { get; set; }
        public string Observation3f { get; set; }
        public string Observation3g { get; set; }
        public string Observation3h { get; set; }
        public string Observation3i { get; set; }
        public Nullable<int> TotalScore { get; set; }
    
        public virtual ICollection<TwentyOnePointAudit> TwentyOnePointAudits { get; set; }
    }
}
