﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/user.master" AutoEventWireup="true" Inherits="SafetyPQ_QuestionnaireVerification" Codebehind="SafetyPQ_QuestionnaireVerification.aspx.cs" %>

<%@ Register src="UserControls/Main/SqQuestionnaire/Questionnaire3Verification.ascx" tagname="Questionnaire3Verification" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <%: System.Web.Optimization.Styles.Render("~/bundles/Office2003BlueOld_styles") %>
    <style type="text/css">
        A
        {
            font-weight: normal;
            font-size: 11px;
            color: Navy;
            font-family: Verdana, Arial, Helvetica, sans-serif;
            text-decoration: none;
        }
        A:hover
        {
            color: Navy;
            text-decoration: underline;
        }
        A:link
        {
            color: Navy;
            text-decoration: none;
        }
        A:visited
        {
            color: Navy;
        }
        A:active
        {
            color: Navy;
            text-decoration: none;
        }
        NoHighlightLink
        {
            color: Navy;
            text-decoration: none;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" Runat="Server">
    <uc1:Questionnaire3Verification ID="Questionnaire3Verification1" 
        runat="server" />
</asp:Content>

