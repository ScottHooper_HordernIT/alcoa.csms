//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Repo.CSMS.DAL.EntityModels
{
    using System;
    using System.Collections.Generic;
    
    public partial class FileVault
    {
        public FileVault()
        {
            this.FileVaultTables = new HashSet<FileVaultTable>();
            this.SqExemptions = new HashSet<SqExemption>();
        }
    
        public int FileVaultId { get; set; }
        public int FileVaultCategoryId { get; set; }
        public string FileName { get; set; }
        public byte[] FileHash { get; set; }
        public int ContentLength { get; set; }
        public byte[] Content { get; set; }
        public System.DateTime ModifiedDate { get; set; }
        public int ModifiedByUserId { get; set; }
    
        public virtual FileVaultCategory FileVaultCategory { get; set; }
        public virtual User User { get; set; }
        public virtual ICollection<FileVaultTable> FileVaultTables { get; set; }
        public virtual ICollection<SqExemption> SqExemptions { get; set; }
    }
}
