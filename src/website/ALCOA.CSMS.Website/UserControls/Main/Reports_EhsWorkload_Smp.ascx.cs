using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Text;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using KaiZen.CSMS.Web;
using KaiZen.CSMS.Services;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxTabControl;
using DevExpress.Web.Data;
using DevExpress.Web.ASPxGridView;
using System.Drawing;
using DevExpress.XtraPrinting;
using System.Data.SqlClient;

using KaiZen.CSMS.Web.Data;

public partial class UserControls_Reports_EhsWorkload_Smp : System.Web.UI.UserControl
{
    Auth auth = new Auth();
    protected void Page_Load(object sender, EventArgs e) {  Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); }

    private void moduleInit(bool postBack)
    {
        if (!postBack)
        { //first time load
            cmbYear.DataSourceID = "dsFileDb_GetDistinctYears";
            cmbYear.TextField = "Year";
            cmbYear.ValueField = "Year";
            cmbYear.Value = "-1";
            cmbYear.DataBind();
            cmbYear.Items.Add("All", -1);

            SessionHandler.spVar_Year = "%";

            LoadData();

            switch (auth.RoleId)
            {
                case ((int)RoleList.Reader):
                    //full access
                    break;
                case ((int)RoleList.Contractor):
                    //full access
                    break;
                case ((int)RoleList.Administrator):
                    //full access
                    break;
                default:
                    // do something
                    Response.Redirect("AccessDenied.aspx?error=" + Server.UrlEncode("Unrecognised User."));
                    break;
            }

            // Export
            String exportFileName = @"ALCOA CSMS - Work Levelling Tool - Safety Management Plans"; //Chrome & IE is fine.
            String userAgent = Request.Headers.Get("User-Agent");
            if (
                (userAgent.Contains("Mozilla") && !userAgent.Contains("AppleWebKit") && !userAgent.Contains("MSIE")) || //Mozilla
                (userAgent.Contains("AppleWebKit") && !userAgent.Contains("Chrome"))      //Safari
                )
            {
                exportFileName = exportFileName.Replace(" ", "_");
            }
            Helper.ExportGrid.Settings(ucExportButtons, exportFileName);

        }
    }
    protected void cmbYear_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (cmbYear.SelectedItem.Value.ToString() == "-1")
        {
            SessionHandler.spVar_Year = "%";
        }
        else
        {
            SessionHandler.spVar_Year = cmbYear.SelectedItem.Value.ToString();
        }
        LoadData();
    }

    protected void LoadData()
    {
        dsFileDb_GetEhsConsultantsWorkLoad.DataBind();
        grid.DataBind();

        using (SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["ALCOA_WAO_CSMWP_devConnectionString"].ToString()))
        {
            cn.Open();
            string year = SessionHandler.spVar_Year;
            SqlCommand cmd = new SqlCommand(String.Format("SELECT COUNT(ISNULL(EHSConsultantId,0)) As [Unallocated] FROM dbo.FileDB WHERE Description LIKE '%{0}%' AND (EHSConsultantId = 0 OR EHSConsultantId IS NULL)", year), cn);
            SqlDataReader rdr = cmd.ExecuteReader();
            rdr.Read();

            lblUnallocated.Text = String.Format("There are currently ({0}) unallocated Safety Plans.", rdr[0]);

            cn.Close();
        }
    }
}
