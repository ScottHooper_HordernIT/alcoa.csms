﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="QuestionnaireInitialLocationProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class QuestionnaireInitialLocationProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.QuestionnaireInitialLocation, KaiZen.CSMS.Entities.QuestionnaireInitialLocationKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.QuestionnaireInitialLocationKey key)
		{
			return Delete(transactionManager, key.QuestionnaireLocationId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_questionnaireLocationId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _questionnaireLocationId)
		{
			return Delete(null, _questionnaireLocationId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireLocationId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _questionnaireLocationId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireInitialLocation_Sites key.
		///		FK_QuestionnaireInitialLocation_Sites Description: 
		/// </summary>
		/// <param name="_siteId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireInitialLocation objects.</returns>
		public TList<QuestionnaireInitialLocation> GetBySiteId(System.Int32 _siteId)
		{
			int count = -1;
			return GetBySiteId(_siteId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireInitialLocation_Sites key.
		///		FK_QuestionnaireInitialLocation_Sites Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_siteId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireInitialLocation objects.</returns>
		/// <remarks></remarks>
		public TList<QuestionnaireInitialLocation> GetBySiteId(TransactionManager transactionManager, System.Int32 _siteId)
		{
			int count = -1;
			return GetBySiteId(transactionManager, _siteId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireInitialLocation_Sites key.
		///		FK_QuestionnaireInitialLocation_Sites Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_siteId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireInitialLocation objects.</returns>
		public TList<QuestionnaireInitialLocation> GetBySiteId(TransactionManager transactionManager, System.Int32 _siteId, int start, int pageLength)
		{
			int count = -1;
			return GetBySiteId(transactionManager, _siteId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireInitialLocation_Sites key.
		///		fkQuestionnaireInitialLocationSites Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_siteId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireInitialLocation objects.</returns>
		public TList<QuestionnaireInitialLocation> GetBySiteId(System.Int32 _siteId, int start, int pageLength)
		{
			int count =  -1;
			return GetBySiteId(null, _siteId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireInitialLocation_Sites key.
		///		fkQuestionnaireInitialLocationSites Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_siteId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireInitialLocation objects.</returns>
		public TList<QuestionnaireInitialLocation> GetBySiteId(System.Int32 _siteId, int start, int pageLength,out int count)
		{
			return GetBySiteId(null, _siteId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireInitialLocation_Sites key.
		///		FK_QuestionnaireInitialLocation_Sites Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_siteId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireInitialLocation objects.</returns>
		public abstract TList<QuestionnaireInitialLocation> GetBySiteId(TransactionManager transactionManager, System.Int32 _siteId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireInitialLocation_Users_ApprovedBy key.
		///		FK_QuestionnaireInitialLocation_Users_ApprovedBy Description: 
		/// </summary>
		/// <param name="_approvedByUserId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireInitialLocation objects.</returns>
		public TList<QuestionnaireInitialLocation> GetByApprovedByUserId(System.Int32? _approvedByUserId)
		{
			int count = -1;
			return GetByApprovedByUserId(_approvedByUserId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireInitialLocation_Users_ApprovedBy key.
		///		FK_QuestionnaireInitialLocation_Users_ApprovedBy Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_approvedByUserId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireInitialLocation objects.</returns>
		/// <remarks></remarks>
		public TList<QuestionnaireInitialLocation> GetByApprovedByUserId(TransactionManager transactionManager, System.Int32? _approvedByUserId)
		{
			int count = -1;
			return GetByApprovedByUserId(transactionManager, _approvedByUserId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireInitialLocation_Users_ApprovedBy key.
		///		FK_QuestionnaireInitialLocation_Users_ApprovedBy Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_approvedByUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireInitialLocation objects.</returns>
		public TList<QuestionnaireInitialLocation> GetByApprovedByUserId(TransactionManager transactionManager, System.Int32? _approvedByUserId, int start, int pageLength)
		{
			int count = -1;
			return GetByApprovedByUserId(transactionManager, _approvedByUserId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireInitialLocation_Users_ApprovedBy key.
		///		fkQuestionnaireInitialLocationUsersApprovedBy Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_approvedByUserId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireInitialLocation objects.</returns>
		public TList<QuestionnaireInitialLocation> GetByApprovedByUserId(System.Int32? _approvedByUserId, int start, int pageLength)
		{
			int count =  -1;
			return GetByApprovedByUserId(null, _approvedByUserId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireInitialLocation_Users_ApprovedBy key.
		///		fkQuestionnaireInitialLocationUsersApprovedBy Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_approvedByUserId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireInitialLocation objects.</returns>
		public TList<QuestionnaireInitialLocation> GetByApprovedByUserId(System.Int32? _approvedByUserId, int start, int pageLength,out int count)
		{
			return GetByApprovedByUserId(null, _approvedByUserId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireInitialLocation_Users_ApprovedBy key.
		///		FK_QuestionnaireInitialLocation_Users_ApprovedBy Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_approvedByUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireInitialLocation objects.</returns>
		public abstract TList<QuestionnaireInitialLocation> GetByApprovedByUserId(TransactionManager transactionManager, System.Int32? _approvedByUserId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireInitialLocation_Users_ModifiedBy key.
		///		FK_QuestionnaireInitialLocation_Users_ModifiedBy Description: 
		/// </summary>
		/// <param name="_modifiedByUserId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireInitialLocation objects.</returns>
		public TList<QuestionnaireInitialLocation> GetByModifiedByUserId(System.Int32 _modifiedByUserId)
		{
			int count = -1;
			return GetByModifiedByUserId(_modifiedByUserId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireInitialLocation_Users_ModifiedBy key.
		///		FK_QuestionnaireInitialLocation_Users_ModifiedBy Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_modifiedByUserId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireInitialLocation objects.</returns>
		/// <remarks></remarks>
		public TList<QuestionnaireInitialLocation> GetByModifiedByUserId(TransactionManager transactionManager, System.Int32 _modifiedByUserId)
		{
			int count = -1;
			return GetByModifiedByUserId(transactionManager, _modifiedByUserId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireInitialLocation_Users_ModifiedBy key.
		///		FK_QuestionnaireInitialLocation_Users_ModifiedBy Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_modifiedByUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireInitialLocation objects.</returns>
		public TList<QuestionnaireInitialLocation> GetByModifiedByUserId(TransactionManager transactionManager, System.Int32 _modifiedByUserId, int start, int pageLength)
		{
			int count = -1;
			return GetByModifiedByUserId(transactionManager, _modifiedByUserId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireInitialLocation_Users_ModifiedBy key.
		///		fkQuestionnaireInitialLocationUsersModifiedBy Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_modifiedByUserId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireInitialLocation objects.</returns>
		public TList<QuestionnaireInitialLocation> GetByModifiedByUserId(System.Int32 _modifiedByUserId, int start, int pageLength)
		{
			int count =  -1;
			return GetByModifiedByUserId(null, _modifiedByUserId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireInitialLocation_Users_ModifiedBy key.
		///		fkQuestionnaireInitialLocationUsersModifiedBy Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_modifiedByUserId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireInitialLocation objects.</returns>
		public TList<QuestionnaireInitialLocation> GetByModifiedByUserId(System.Int32 _modifiedByUserId, int start, int pageLength,out int count)
		{
			return GetByModifiedByUserId(null, _modifiedByUserId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireInitialLocation_Users_ModifiedBy key.
		///		FK_QuestionnaireInitialLocation_Users_ModifiedBy Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_modifiedByUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireInitialLocation objects.</returns>
		public abstract TList<QuestionnaireInitialLocation> GetByModifiedByUserId(TransactionManager transactionManager, System.Int32 _modifiedByUserId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireInitialLocation_Users_SPA key.
		///		FK_QuestionnaireInitialLocation_Users_SPA Description: 
		/// </summary>
		/// <param name="_spa"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireInitialLocation objects.</returns>
		public TList<QuestionnaireInitialLocation> GetBySpa(System.Int32? _spa)
		{
			int count = -1;
			return GetBySpa(_spa, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireInitialLocation_Users_SPA key.
		///		FK_QuestionnaireInitialLocation_Users_SPA Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_spa"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireInitialLocation objects.</returns>
		/// <remarks></remarks>
		public TList<QuestionnaireInitialLocation> GetBySpa(TransactionManager transactionManager, System.Int32? _spa)
		{
			int count = -1;
			return GetBySpa(transactionManager, _spa, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireInitialLocation_Users_SPA key.
		///		FK_QuestionnaireInitialLocation_Users_SPA Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_spa"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireInitialLocation objects.</returns>
		public TList<QuestionnaireInitialLocation> GetBySpa(TransactionManager transactionManager, System.Int32? _spa, int start, int pageLength)
		{
			int count = -1;
			return GetBySpa(transactionManager, _spa, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireInitialLocation_Users_SPA key.
		///		fkQuestionnaireInitialLocationUsersSpa Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_spa"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireInitialLocation objects.</returns>
		public TList<QuestionnaireInitialLocation> GetBySpa(System.Int32? _spa, int start, int pageLength)
		{
			int count =  -1;
			return GetBySpa(null, _spa, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireInitialLocation_Users_SPA key.
		///		fkQuestionnaireInitialLocationUsersSpa Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_spa"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireInitialLocation objects.</returns>
		public TList<QuestionnaireInitialLocation> GetBySpa(System.Int32? _spa, int start, int pageLength,out int count)
		{
			return GetBySpa(null, _spa, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireInitialLocation_Users_SPA key.
		///		FK_QuestionnaireInitialLocation_Users_SPA Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_spa"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireInitialLocation objects.</returns>
		public abstract TList<QuestionnaireInitialLocation> GetBySpa(TransactionManager transactionManager, System.Int32? _spa, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.QuestionnaireInitialLocation Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.QuestionnaireInitialLocationKey key, int start, int pageLength)
		{
			return GetByQuestionnaireLocationId(transactionManager, key.QuestionnaireLocationId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_QuestionnaireInitialLocation index.
		/// </summary>
		/// <param name="_questionnaireLocationId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireInitialLocation"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireInitialLocation GetByQuestionnaireLocationId(System.Int32 _questionnaireLocationId)
		{
			int count = -1;
			return GetByQuestionnaireLocationId(null,_questionnaireLocationId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireInitialLocation index.
		/// </summary>
		/// <param name="_questionnaireLocationId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireInitialLocation"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireInitialLocation GetByQuestionnaireLocationId(System.Int32 _questionnaireLocationId, int start, int pageLength)
		{
			int count = -1;
			return GetByQuestionnaireLocationId(null, _questionnaireLocationId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireInitialLocation index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireLocationId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireInitialLocation"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireInitialLocation GetByQuestionnaireLocationId(TransactionManager transactionManager, System.Int32 _questionnaireLocationId)
		{
			int count = -1;
			return GetByQuestionnaireLocationId(transactionManager, _questionnaireLocationId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireInitialLocation index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireLocationId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireInitialLocation"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireInitialLocation GetByQuestionnaireLocationId(TransactionManager transactionManager, System.Int32 _questionnaireLocationId, int start, int pageLength)
		{
			int count = -1;
			return GetByQuestionnaireLocationId(transactionManager, _questionnaireLocationId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireInitialLocation index.
		/// </summary>
		/// <param name="_questionnaireLocationId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireInitialLocation"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireInitialLocation GetByQuestionnaireLocationId(System.Int32 _questionnaireLocationId, int start, int pageLength, out int count)
		{
			return GetByQuestionnaireLocationId(null, _questionnaireLocationId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireInitialLocation index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireLocationId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireInitialLocation"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.QuestionnaireInitialLocation GetByQuestionnaireLocationId(TransactionManager transactionManager, System.Int32 _questionnaireLocationId, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_QuestionnaireInitialLocation index.
		/// </summary>
		/// <param name="_questionnaireId"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;QuestionnaireInitialLocation&gt;"/> class.</returns>
		public TList<QuestionnaireInitialLocation> GetByQuestionnaireId(System.Int32 _questionnaireId)
		{
			int count = -1;
			return GetByQuestionnaireId(null,_questionnaireId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireInitialLocation index.
		/// </summary>
		/// <param name="_questionnaireId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;QuestionnaireInitialLocation&gt;"/> class.</returns>
		public TList<QuestionnaireInitialLocation> GetByQuestionnaireId(System.Int32 _questionnaireId, int start, int pageLength)
		{
			int count = -1;
			return GetByQuestionnaireId(null, _questionnaireId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireInitialLocation index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;QuestionnaireInitialLocation&gt;"/> class.</returns>
		public TList<QuestionnaireInitialLocation> GetByQuestionnaireId(TransactionManager transactionManager, System.Int32 _questionnaireId)
		{
			int count = -1;
			return GetByQuestionnaireId(transactionManager, _questionnaireId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireInitialLocation index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;QuestionnaireInitialLocation&gt;"/> class.</returns>
		public TList<QuestionnaireInitialLocation> GetByQuestionnaireId(TransactionManager transactionManager, System.Int32 _questionnaireId, int start, int pageLength)
		{
			int count = -1;
			return GetByQuestionnaireId(transactionManager, _questionnaireId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireInitialLocation index.
		/// </summary>
		/// <param name="_questionnaireId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;QuestionnaireInitialLocation&gt;"/> class.</returns>
		public TList<QuestionnaireInitialLocation> GetByQuestionnaireId(System.Int32 _questionnaireId, int start, int pageLength, out int count)
		{
			return GetByQuestionnaireId(null, _questionnaireId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireInitialLocation index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;QuestionnaireInitialLocation&gt;"/> class.</returns>
		public abstract TList<QuestionnaireInitialLocation> GetByQuestionnaireId(TransactionManager transactionManager, System.Int32 _questionnaireId, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_QuestionnaireInitialLocation_1 index.
		/// </summary>
		/// <param name="_questionnaireId"></param>
		/// <param name="_siteId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireInitialLocation"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireInitialLocation GetByQuestionnaireIdSiteId(System.Int32 _questionnaireId, System.Int32 _siteId)
		{
			int count = -1;
			return GetByQuestionnaireIdSiteId(null,_questionnaireId, _siteId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireInitialLocation_1 index.
		/// </summary>
		/// <param name="_questionnaireId"></param>
		/// <param name="_siteId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireInitialLocation"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireInitialLocation GetByQuestionnaireIdSiteId(System.Int32 _questionnaireId, System.Int32 _siteId, int start, int pageLength)
		{
			int count = -1;
			return GetByQuestionnaireIdSiteId(null, _questionnaireId, _siteId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireInitialLocation_1 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireId"></param>
		/// <param name="_siteId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireInitialLocation"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireInitialLocation GetByQuestionnaireIdSiteId(TransactionManager transactionManager, System.Int32 _questionnaireId, System.Int32 _siteId)
		{
			int count = -1;
			return GetByQuestionnaireIdSiteId(transactionManager, _questionnaireId, _siteId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireInitialLocation_1 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireId"></param>
		/// <param name="_siteId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireInitialLocation"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireInitialLocation GetByQuestionnaireIdSiteId(TransactionManager transactionManager, System.Int32 _questionnaireId, System.Int32 _siteId, int start, int pageLength)
		{
			int count = -1;
			return GetByQuestionnaireIdSiteId(transactionManager, _questionnaireId, _siteId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireInitialLocation_1 index.
		/// </summary>
		/// <param name="_questionnaireId"></param>
		/// <param name="_siteId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireInitialLocation"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireInitialLocation GetByQuestionnaireIdSiteId(System.Int32 _questionnaireId, System.Int32 _siteId, int start, int pageLength, out int count)
		{
			return GetByQuestionnaireIdSiteId(null, _questionnaireId, _siteId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireInitialLocation_1 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireId"></param>
		/// <param name="_siteId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireInitialLocation"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.QuestionnaireInitialLocation GetByQuestionnaireIdSiteId(TransactionManager transactionManager, System.Int32 _questionnaireId, System.Int32 _siteId, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;QuestionnaireInitialLocation&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;QuestionnaireInitialLocation&gt;"/></returns>
		public static TList<QuestionnaireInitialLocation> Fill(IDataReader reader, TList<QuestionnaireInitialLocation> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.QuestionnaireInitialLocation c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("QuestionnaireInitialLocation")
					.Append("|").Append((System.Int32)reader[((int)QuestionnaireInitialLocationColumn.QuestionnaireLocationId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<QuestionnaireInitialLocation>(
					key.ToString(), // EntityTrackingKey
					"QuestionnaireInitialLocation",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.QuestionnaireInitialLocation();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.QuestionnaireLocationId = (System.Int32)reader[((int)QuestionnaireInitialLocationColumn.QuestionnaireLocationId - 1)];
					c.QuestionnaireId = (System.Int32)reader[((int)QuestionnaireInitialLocationColumn.QuestionnaireId - 1)];
					c.SiteId = (System.Int32)reader[((int)QuestionnaireInitialLocationColumn.SiteId - 1)];
					c.Spa = (reader.IsDBNull(((int)QuestionnaireInitialLocationColumn.Spa - 1)))?null:(System.Int32?)reader[((int)QuestionnaireInitialLocationColumn.Spa - 1)];
					c.Approved = (reader.IsDBNull(((int)QuestionnaireInitialLocationColumn.Approved - 1)))?null:(System.Boolean?)reader[((int)QuestionnaireInitialLocationColumn.Approved - 1)];
					c.ApprovedByUserId = (reader.IsDBNull(((int)QuestionnaireInitialLocationColumn.ApprovedByUserId - 1)))?null:(System.Int32?)reader[((int)QuestionnaireInitialLocationColumn.ApprovedByUserId - 1)];
					c.ApprovedDate = (reader.IsDBNull(((int)QuestionnaireInitialLocationColumn.ApprovedDate - 1)))?null:(System.DateTime?)reader[((int)QuestionnaireInitialLocationColumn.ApprovedDate - 1)];
					c.ModifiedByUserId = (System.Int32)reader[((int)QuestionnaireInitialLocationColumn.ModifiedByUserId - 1)];
					c.ModifiedDate = (System.DateTime)reader[((int)QuestionnaireInitialLocationColumn.ModifiedDate - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.QuestionnaireInitialLocation"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.QuestionnaireInitialLocation"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.QuestionnaireInitialLocation entity)
		{
			if (!reader.Read()) return;
			
			entity.QuestionnaireLocationId = (System.Int32)reader[((int)QuestionnaireInitialLocationColumn.QuestionnaireLocationId - 1)];
			entity.QuestionnaireId = (System.Int32)reader[((int)QuestionnaireInitialLocationColumn.QuestionnaireId - 1)];
			entity.SiteId = (System.Int32)reader[((int)QuestionnaireInitialLocationColumn.SiteId - 1)];
			entity.Spa = (reader.IsDBNull(((int)QuestionnaireInitialLocationColumn.Spa - 1)))?null:(System.Int32?)reader[((int)QuestionnaireInitialLocationColumn.Spa - 1)];
			entity.Approved = (reader.IsDBNull(((int)QuestionnaireInitialLocationColumn.Approved - 1)))?null:(System.Boolean?)reader[((int)QuestionnaireInitialLocationColumn.Approved - 1)];
			entity.ApprovedByUserId = (reader.IsDBNull(((int)QuestionnaireInitialLocationColumn.ApprovedByUserId - 1)))?null:(System.Int32?)reader[((int)QuestionnaireInitialLocationColumn.ApprovedByUserId - 1)];
			entity.ApprovedDate = (reader.IsDBNull(((int)QuestionnaireInitialLocationColumn.ApprovedDate - 1)))?null:(System.DateTime?)reader[((int)QuestionnaireInitialLocationColumn.ApprovedDate - 1)];
			entity.ModifiedByUserId = (System.Int32)reader[((int)QuestionnaireInitialLocationColumn.ModifiedByUserId - 1)];
			entity.ModifiedDate = (System.DateTime)reader[((int)QuestionnaireInitialLocationColumn.ModifiedDate - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.QuestionnaireInitialLocation"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.QuestionnaireInitialLocation"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.QuestionnaireInitialLocation entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.QuestionnaireLocationId = (System.Int32)dataRow["QuestionnaireLocationId"];
			entity.QuestionnaireId = (System.Int32)dataRow["QuestionnaireId"];
			entity.SiteId = (System.Int32)dataRow["SiteId"];
			entity.Spa = Convert.IsDBNull(dataRow["SPA"]) ? null : (System.Int32?)dataRow["SPA"];
			entity.Approved = Convert.IsDBNull(dataRow["Approved"]) ? null : (System.Boolean?)dataRow["Approved"];
			entity.ApprovedByUserId = Convert.IsDBNull(dataRow["ApprovedByUserId"]) ? null : (System.Int32?)dataRow["ApprovedByUserId"];
			entity.ApprovedDate = Convert.IsDBNull(dataRow["ApprovedDate"]) ? null : (System.DateTime?)dataRow["ApprovedDate"];
			entity.ModifiedByUserId = (System.Int32)dataRow["ModifiedByUserId"];
			entity.ModifiedDate = (System.DateTime)dataRow["ModifiedDate"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.QuestionnaireInitialLocation"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.QuestionnaireInitialLocation Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.QuestionnaireInitialLocation entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region SiteIdSource	
			if (CanDeepLoad(entity, "Sites|SiteIdSource", deepLoadType, innerList) 
				&& entity.SiteIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.SiteId;
				Sites tmpEntity = EntityManager.LocateEntity<Sites>(EntityLocator.ConstructKeyFromPkItems(typeof(Sites), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.SiteIdSource = tmpEntity;
				else
					entity.SiteIdSource = DataRepository.SitesProvider.GetBySiteId(transactionManager, entity.SiteId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'SiteIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.SiteIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.SitesProvider.DeepLoad(transactionManager, entity.SiteIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion SiteIdSource

			#region ApprovedByUserIdSource	
			if (CanDeepLoad(entity, "Users|ApprovedByUserIdSource", deepLoadType, innerList) 
				&& entity.ApprovedByUserIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.ApprovedByUserId ?? (int)0);
				Users tmpEntity = EntityManager.LocateEntity<Users>(EntityLocator.ConstructKeyFromPkItems(typeof(Users), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.ApprovedByUserIdSource = tmpEntity;
				else
					entity.ApprovedByUserIdSource = DataRepository.UsersProvider.GetByUserId(transactionManager, (entity.ApprovedByUserId ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ApprovedByUserIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.ApprovedByUserIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.UsersProvider.DeepLoad(transactionManager, entity.ApprovedByUserIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion ApprovedByUserIdSource

			#region ModifiedByUserIdSource	
			if (CanDeepLoad(entity, "Users|ModifiedByUserIdSource", deepLoadType, innerList) 
				&& entity.ModifiedByUserIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.ModifiedByUserId;
				Users tmpEntity = EntityManager.LocateEntity<Users>(EntityLocator.ConstructKeyFromPkItems(typeof(Users), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.ModifiedByUserIdSource = tmpEntity;
				else
					entity.ModifiedByUserIdSource = DataRepository.UsersProvider.GetByUserId(transactionManager, entity.ModifiedByUserId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ModifiedByUserIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.ModifiedByUserIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.UsersProvider.DeepLoad(transactionManager, entity.ModifiedByUserIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion ModifiedByUserIdSource

			#region QuestionnaireIdSource	
			if (CanDeepLoad(entity, "Questionnaire|QuestionnaireIdSource", deepLoadType, innerList) 
				&& entity.QuestionnaireIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.QuestionnaireId;
				Questionnaire tmpEntity = EntityManager.LocateEntity<Questionnaire>(EntityLocator.ConstructKeyFromPkItems(typeof(Questionnaire), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.QuestionnaireIdSource = tmpEntity;
				else
					entity.QuestionnaireIdSource = DataRepository.QuestionnaireProvider.GetByQuestionnaireId(transactionManager, entity.QuestionnaireId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'QuestionnaireIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.QuestionnaireIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.QuestionnaireProvider.DeepLoad(transactionManager, entity.QuestionnaireIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion QuestionnaireIdSource

			#region SpaSource	
			if (CanDeepLoad(entity, "Users|SpaSource", deepLoadType, innerList) 
				&& entity.SpaSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.Spa ?? (int)0);
				Users tmpEntity = EntityManager.LocateEntity<Users>(EntityLocator.ConstructKeyFromPkItems(typeof(Users), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.SpaSource = tmpEntity;
				else
					entity.SpaSource = DataRepository.UsersProvider.GetByUserId(transactionManager, (entity.Spa ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'SpaSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.SpaSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.UsersProvider.DeepLoad(transactionManager, entity.SpaSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion SpaSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.QuestionnaireInitialLocation object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.QuestionnaireInitialLocation instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.QuestionnaireInitialLocation Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.QuestionnaireInitialLocation entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region SiteIdSource
			if (CanDeepSave(entity, "Sites|SiteIdSource", deepSaveType, innerList) 
				&& entity.SiteIdSource != null)
			{
				DataRepository.SitesProvider.Save(transactionManager, entity.SiteIdSource);
				entity.SiteId = entity.SiteIdSource.SiteId;
			}
			#endregion 
			
			#region ApprovedByUserIdSource
			if (CanDeepSave(entity, "Users|ApprovedByUserIdSource", deepSaveType, innerList) 
				&& entity.ApprovedByUserIdSource != null)
			{
				DataRepository.UsersProvider.Save(transactionManager, entity.ApprovedByUserIdSource);
				entity.ApprovedByUserId = entity.ApprovedByUserIdSource.UserId;
			}
			#endregion 
			
			#region ModifiedByUserIdSource
			if (CanDeepSave(entity, "Users|ModifiedByUserIdSource", deepSaveType, innerList) 
				&& entity.ModifiedByUserIdSource != null)
			{
				DataRepository.UsersProvider.Save(transactionManager, entity.ModifiedByUserIdSource);
				entity.ModifiedByUserId = entity.ModifiedByUserIdSource.UserId;
			}
			#endregion 
			
			#region QuestionnaireIdSource
			if (CanDeepSave(entity, "Questionnaire|QuestionnaireIdSource", deepSaveType, innerList) 
				&& entity.QuestionnaireIdSource != null)
			{
				DataRepository.QuestionnaireProvider.Save(transactionManager, entity.QuestionnaireIdSource);
				entity.QuestionnaireId = entity.QuestionnaireIdSource.QuestionnaireId;
			}
			#endregion 
			
			#region SpaSource
			if (CanDeepSave(entity, "Users|SpaSource", deepSaveType, innerList) 
				&& entity.SpaSource != null)
			{
				DataRepository.UsersProvider.Save(transactionManager, entity.SpaSource);
				entity.Spa = entity.SpaSource.UserId;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region QuestionnaireInitialLocationChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.QuestionnaireInitialLocation</c>
	///</summary>
	public enum QuestionnaireInitialLocationChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>Sites</c> at SiteIdSource
		///</summary>
		[ChildEntityType(typeof(Sites))]
		Sites,
			
		///<summary>
		/// Composite Property for <c>Users</c> at ApprovedByUserIdSource
		///</summary>
		[ChildEntityType(typeof(Users))]
		Users,
			
		///<summary>
		/// Composite Property for <c>Questionnaire</c> at QuestionnaireIdSource
		///</summary>
		[ChildEntityType(typeof(Questionnaire))]
		Questionnaire,
		}
	
	#endregion QuestionnaireInitialLocationChildEntityTypes
	
	#region QuestionnaireInitialLocationFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;QuestionnaireInitialLocationColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireInitialLocation"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireInitialLocationFilterBuilder : SqlFilterBuilder<QuestionnaireInitialLocationColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialLocationFilterBuilder class.
		/// </summary>
		public QuestionnaireInitialLocationFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialLocationFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireInitialLocationFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialLocationFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireInitialLocationFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireInitialLocationFilterBuilder
	
	#region QuestionnaireInitialLocationParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;QuestionnaireInitialLocationColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireInitialLocation"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireInitialLocationParameterBuilder : ParameterizedSqlFilterBuilder<QuestionnaireInitialLocationColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialLocationParameterBuilder class.
		/// </summary>
		public QuestionnaireInitialLocationParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialLocationParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireInitialLocationParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialLocationParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireInitialLocationParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireInitialLocationParameterBuilder
	
	#region QuestionnaireInitialLocationSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;QuestionnaireInitialLocationColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireInitialLocation"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class QuestionnaireInitialLocationSortBuilder : SqlSortBuilder<QuestionnaireInitialLocationColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialLocationSqlSortBuilder class.
		/// </summary>
		public QuestionnaireInitialLocationSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion QuestionnaireInitialLocationSortBuilder
	
} // end namespace
