﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface IQuestionnaireActionService
    {
        QuestionnaireAction Get(Expression<Func<QuestionnaireAction, bool>> where, List<Expression<Func<QuestionnaireAction, object>>> includes);
        List<QuestionnaireAction> GetMany(int? top, Expression<Func<QuestionnaireAction, bool>> where, Func<IQueryable<QuestionnaireAction>, IOrderedQueryable<QuestionnaireAction>> orderBy, List<Expression<Func<QuestionnaireAction, object>>> includes);
        List<QuestionnaireAction> GetManyPaged(int pageNumber, int pageSize, Expression<Func<QuestionnaireAction, bool>> where, Func<IQueryable<QuestionnaireAction>, IOrderedQueryable<QuestionnaireAction>> orderBy, List<Expression<Func<QuestionnaireAction, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<QuestionnaireAction, bool>> where, List<Expression<Func<QuestionnaireAction, object>>> includes);
        void Insert(QuestionnaireAction item, bool saveChanges = true);
        void Update(QuestionnaireAction item, bool saveChanges = true);
        void Delete(QuestionnaireAction item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class QuestionnaireActionService : IQuestionnaireActionService
    {
        private readonly IRepository<QuestionnaireAction> _repository;

        public QuestionnaireActionService(IRepository<QuestionnaireAction> repository)
        {
            this._repository = repository;
        }

        public QuestionnaireAction Get(Expression<Func<QuestionnaireAction, bool>> where, List<Expression<Func<QuestionnaireAction, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<QuestionnaireAction> GetMany(int? top, Expression<Func<QuestionnaireAction, bool>> where, Func<IQueryable<QuestionnaireAction>, IOrderedQueryable<QuestionnaireAction>> orderBy, List<Expression<Func<QuestionnaireAction, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<QuestionnaireAction> GetManyPaged(int pageNumber, int pageSize, Expression<Func<QuestionnaireAction, bool>> where, Func<IQueryable<QuestionnaireAction>, IOrderedQueryable<QuestionnaireAction>> orderBy, List<Expression<Func<QuestionnaireAction, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<QuestionnaireAction, bool>> where, List<Expression<Func<QuestionnaireAction, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(QuestionnaireAction item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("QuestionnaireAction");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(QuestionnaireAction item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("QuestionnaireAction");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(QuestionnaireAction item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("QuestionnaireAction");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
