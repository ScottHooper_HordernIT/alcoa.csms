﻿#region Using directives

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="QuestionnaireEscalationHsAssessorListProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract class QuestionnaireEscalationHsAssessorListProviderBaseCore : EntityViewProviderBase<QuestionnaireEscalationHsAssessorList>
	{
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions
		
		/*
		///<summary>
		/// Fill an VList&lt;QuestionnaireEscalationHsAssessorList&gt; From a DataSet
		///</summary>
		/// <param name="dataSet">the DataSet</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList&lt;QuestionnaireEscalationHsAssessorList&gt;"/></returns>
		protected static VList&lt;QuestionnaireEscalationHsAssessorList&gt; Fill(DataSet dataSet, VList<QuestionnaireEscalationHsAssessorList> rows, int start, int pagelen)
		{
			if (dataSet.Tables.Count == 1)
			{
				return Fill(dataSet.Tables[0], rows, start, pagelen);
			}
			else
			{
				return new VList<QuestionnaireEscalationHsAssessorList>();
			}	
		}
		
		
		///<summary>
		/// Fill an VList&lt;QuestionnaireEscalationHsAssessorList&gt; From a DataTable
		///</summary>
		/// <param name="dataTable">the DataTable that hold the data.</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList<QuestionnaireEscalationHsAssessorList>"/></returns>
		protected static VList&lt;QuestionnaireEscalationHsAssessorList&gt; Fill(DataTable dataTable, VList<QuestionnaireEscalationHsAssessorList> rows, int start, int pagelen)
		{
			int recordnum = 0;
			
			System.Collections.IEnumerator dataRows =  dataTable.Rows.GetEnumerator();
			
			while (dataRows.MoveNext() && (pagelen != 0))
			{
				if(recordnum >= start)
				{
					DataRow row = (DataRow)dataRows.Current;
				
					QuestionnaireEscalationHsAssessorList c = new QuestionnaireEscalationHsAssessorList();
					c.QuestionnaireId = (Convert.IsDBNull(row["QuestionnaireId"]))?(int)0:(System.Int32)row["QuestionnaireId"];
					c.HsAssessorRegionId = (Convert.IsDBNull(row["HsAssessorRegionId"]))?(int)0:(System.Int32?)row["HsAssessorRegionId"];
					c.HsAssessorSiteId = (Convert.IsDBNull(row["HsAssessorSiteId"]))?(int)0:(System.Int32?)row["HsAssessorSiteId"];
					c.UserId = (Convert.IsDBNull(row["UserId"]))?(int)0:(System.Int32?)row["UserId"];
					c.SupervisorUserId = (Convert.IsDBNull(row["SupervisorUserId"]))?(int)0:(System.Int32?)row["SupervisorUserId"];
					c.Level3UserId = (Convert.IsDBNull(row["Level3UserId"]))?(int)0:(System.Int32?)row["Level3UserId"];
					c.Level4UserId = (Convert.IsDBNull(row["Level4UserId"]))?(int)0:(System.Int32?)row["Level4UserId"];
					c.Level5UserId = (Convert.IsDBNull(row["Level5UserId"]))?(int)0:(System.Int32?)row["Level5UserId"];
					c.Level6UserId = (Convert.IsDBNull(row["Level6UserId"]))?(int)0:(System.Int32?)row["Level6UserId"];
					c.Level7UserId = (Convert.IsDBNull(row["Level7UserId"]))?(int)0:(System.Int32?)row["Level7UserId"];
					c.Level8UserId = (Convert.IsDBNull(row["Level8UserId"]))?(int)0:(System.Int32?)row["Level8UserId"];
					c.Level9UserId = (Convert.IsDBNull(row["Level9UserId"]))?(int)0:(System.Int32?)row["Level9UserId"];
					c.HsAssessorFirstLastName = (Convert.IsDBNull(row["HsAssessorFirstLastName"]))?string.Empty:(System.String)row["HsAssessorFirstLastName"];
					c.HsAssessorLastFirstName = (Convert.IsDBNull(row["HsAssessorLastFirstName"]))?string.Empty:(System.String)row["HsAssessorLastFirstName"];
					c.HsAssessorContactEmail = (Convert.IsDBNull(row["HsAssessorContactEmail"]))?string.Empty:(System.String)row["HsAssessorContactEmail"];
					c.CompanyName = (Convert.IsDBNull(row["CompanyName"]))?string.Empty:(System.String)row["CompanyName"];
					c.ActionDescription = (Convert.IsDBNull(row["ActionDescription"]))?string.Empty:(System.String)row["ActionDescription"];
					c.UserDescription = (Convert.IsDBNull(row["UserDescription"]))?string.Empty:(System.String)row["UserDescription"];
					c.QuestionnairePresentlyWithSince = (Convert.IsDBNull(row["QuestionnairePresentlyWithSince"]))?DateTime.MinValue:(System.DateTime?)row["QuestionnairePresentlyWithSince"];
					c.DaysWith = (Convert.IsDBNull(row["DaysWith"]))?(int)0:(System.Int32?)row["DaysWith"];
					c.AcceptChanges();
					rows.Add(c);
					pagelen -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		*/	
						
		///<summary>
		/// Fill an <see cref="VList&lt;QuestionnaireEscalationHsAssessorList&gt;"/> From a DataReader.
		///</summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pageLength">number of row.</param>
		///<returns>a <see cref="VList&lt;QuestionnaireEscalationHsAssessorList&gt;"/></returns>
		protected VList<QuestionnaireEscalationHsAssessorList> Fill(IDataReader reader, VList<QuestionnaireEscalationHsAssessorList> rows, int start, int pageLength)
		{
			int recordnum = 0;
			while (reader.Read() && (pageLength != 0))
			{
				if(recordnum >= start)
				{
					QuestionnaireEscalationHsAssessorList entity = null;
					if (DataRepository.Provider.UseEntityFactory)
					{
						entity = EntityManager.CreateViewEntity<QuestionnaireEscalationHsAssessorList>("QuestionnaireEscalationHsAssessorList",  DataRepository.Provider.EntityCreationalFactoryType); 
					}
					else
					{
						entity = new QuestionnaireEscalationHsAssessorList();
					}
					
					entity.SuppressEntityEvents = true;

					entity.QuestionnaireId = (System.Int32)reader[((int)QuestionnaireEscalationHsAssessorListColumn.QuestionnaireId)];
					//entity.QuestionnaireId = (Convert.IsDBNull(reader["QuestionnaireId"]))?(int)0:(System.Int32)reader["QuestionnaireId"];
					entity.HsAssessorRegionId = (reader.IsDBNull(((int)QuestionnaireEscalationHsAssessorListColumn.HsAssessorRegionId)))?null:(System.Int32?)reader[((int)QuestionnaireEscalationHsAssessorListColumn.HsAssessorRegionId)];
					//entity.HsAssessorRegionId = (Convert.IsDBNull(reader["HsAssessorRegionId"]))?(int)0:(System.Int32?)reader["HsAssessorRegionId"];
					entity.HsAssessorSiteId = (reader.IsDBNull(((int)QuestionnaireEscalationHsAssessorListColumn.HsAssessorSiteId)))?null:(System.Int32?)reader[((int)QuestionnaireEscalationHsAssessorListColumn.HsAssessorSiteId)];
					//entity.HsAssessorSiteId = (Convert.IsDBNull(reader["HsAssessorSiteId"]))?(int)0:(System.Int32?)reader["HsAssessorSiteId"];
					entity.UserId = (reader.IsDBNull(((int)QuestionnaireEscalationHsAssessorListColumn.UserId)))?null:(System.Int32?)reader[((int)QuestionnaireEscalationHsAssessorListColumn.UserId)];
					//entity.UserId = (Convert.IsDBNull(reader["UserId"]))?(int)0:(System.Int32?)reader["UserId"];
					entity.SupervisorUserId = (reader.IsDBNull(((int)QuestionnaireEscalationHsAssessorListColumn.SupervisorUserId)))?null:(System.Int32?)reader[((int)QuestionnaireEscalationHsAssessorListColumn.SupervisorUserId)];
					//entity.SupervisorUserId = (Convert.IsDBNull(reader["SupervisorUserId"]))?(int)0:(System.Int32?)reader["SupervisorUserId"];
					entity.Level3UserId = (reader.IsDBNull(((int)QuestionnaireEscalationHsAssessorListColumn.Level3UserId)))?null:(System.Int32?)reader[((int)QuestionnaireEscalationHsAssessorListColumn.Level3UserId)];
					//entity.Level3UserId = (Convert.IsDBNull(reader["Level3UserId"]))?(int)0:(System.Int32?)reader["Level3UserId"];
					entity.Level4UserId = (reader.IsDBNull(((int)QuestionnaireEscalationHsAssessorListColumn.Level4UserId)))?null:(System.Int32?)reader[((int)QuestionnaireEscalationHsAssessorListColumn.Level4UserId)];
					//entity.Level4UserId = (Convert.IsDBNull(reader["Level4UserId"]))?(int)0:(System.Int32?)reader["Level4UserId"];
					entity.Level5UserId = (reader.IsDBNull(((int)QuestionnaireEscalationHsAssessorListColumn.Level5UserId)))?null:(System.Int32?)reader[((int)QuestionnaireEscalationHsAssessorListColumn.Level5UserId)];
					//entity.Level5UserId = (Convert.IsDBNull(reader["Level5UserId"]))?(int)0:(System.Int32?)reader["Level5UserId"];
					entity.Level6UserId = (reader.IsDBNull(((int)QuestionnaireEscalationHsAssessorListColumn.Level6UserId)))?null:(System.Int32?)reader[((int)QuestionnaireEscalationHsAssessorListColumn.Level6UserId)];
					//entity.Level6UserId = (Convert.IsDBNull(reader["Level6UserId"]))?(int)0:(System.Int32?)reader["Level6UserId"];
					entity.Level7UserId = (reader.IsDBNull(((int)QuestionnaireEscalationHsAssessorListColumn.Level7UserId)))?null:(System.Int32?)reader[((int)QuestionnaireEscalationHsAssessorListColumn.Level7UserId)];
					//entity.Level7UserId = (Convert.IsDBNull(reader["Level7UserId"]))?(int)0:(System.Int32?)reader["Level7UserId"];
					entity.Level8UserId = (reader.IsDBNull(((int)QuestionnaireEscalationHsAssessorListColumn.Level8UserId)))?null:(System.Int32?)reader[((int)QuestionnaireEscalationHsAssessorListColumn.Level8UserId)];
					//entity.Level8UserId = (Convert.IsDBNull(reader["Level8UserId"]))?(int)0:(System.Int32?)reader["Level8UserId"];
					entity.Level9UserId = (reader.IsDBNull(((int)QuestionnaireEscalationHsAssessorListColumn.Level9UserId)))?null:(System.Int32?)reader[((int)QuestionnaireEscalationHsAssessorListColumn.Level9UserId)];
					//entity.Level9UserId = (Convert.IsDBNull(reader["Level9UserId"]))?(int)0:(System.Int32?)reader["Level9UserId"];
					entity.HsAssessorFirstLastName = (reader.IsDBNull(((int)QuestionnaireEscalationHsAssessorListColumn.HsAssessorFirstLastName)))?null:(System.String)reader[((int)QuestionnaireEscalationHsAssessorListColumn.HsAssessorFirstLastName)];
					//entity.HsAssessorFirstLastName = (Convert.IsDBNull(reader["HsAssessorFirstLastName"]))?string.Empty:(System.String)reader["HsAssessorFirstLastName"];
					entity.HsAssessorLastFirstName = (reader.IsDBNull(((int)QuestionnaireEscalationHsAssessorListColumn.HsAssessorLastFirstName)))?null:(System.String)reader[((int)QuestionnaireEscalationHsAssessorListColumn.HsAssessorLastFirstName)];
					//entity.HsAssessorLastFirstName = (Convert.IsDBNull(reader["HsAssessorLastFirstName"]))?string.Empty:(System.String)reader["HsAssessorLastFirstName"];
					entity.HsAssessorContactEmail = (reader.IsDBNull(((int)QuestionnaireEscalationHsAssessorListColumn.HsAssessorContactEmail)))?null:(System.String)reader[((int)QuestionnaireEscalationHsAssessorListColumn.HsAssessorContactEmail)];
					//entity.HsAssessorContactEmail = (Convert.IsDBNull(reader["HsAssessorContactEmail"]))?string.Empty:(System.String)reader["HsAssessorContactEmail"];
					entity.CompanyName = (reader.IsDBNull(((int)QuestionnaireEscalationHsAssessorListColumn.CompanyName)))?null:(System.String)reader[((int)QuestionnaireEscalationHsAssessorListColumn.CompanyName)];
					//entity.CompanyName = (Convert.IsDBNull(reader["CompanyName"]))?string.Empty:(System.String)reader["CompanyName"];
					entity.ActionDescription = (reader.IsDBNull(((int)QuestionnaireEscalationHsAssessorListColumn.ActionDescription)))?null:(System.String)reader[((int)QuestionnaireEscalationHsAssessorListColumn.ActionDescription)];
					//entity.ActionDescription = (Convert.IsDBNull(reader["ActionDescription"]))?string.Empty:(System.String)reader["ActionDescription"];
					entity.UserDescription = (reader.IsDBNull(((int)QuestionnaireEscalationHsAssessorListColumn.UserDescription)))?null:(System.String)reader[((int)QuestionnaireEscalationHsAssessorListColumn.UserDescription)];
					//entity.UserDescription = (Convert.IsDBNull(reader["UserDescription"]))?string.Empty:(System.String)reader["UserDescription"];
					entity.QuestionnairePresentlyWithSince = (reader.IsDBNull(((int)QuestionnaireEscalationHsAssessorListColumn.QuestionnairePresentlyWithSince)))?null:(System.DateTime?)reader[((int)QuestionnaireEscalationHsAssessorListColumn.QuestionnairePresentlyWithSince)];
					//entity.QuestionnairePresentlyWithSince = (Convert.IsDBNull(reader["QuestionnairePresentlyWithSince"]))?DateTime.MinValue:(System.DateTime?)reader["QuestionnairePresentlyWithSince"];
					entity.DaysWith = (reader.IsDBNull(((int)QuestionnaireEscalationHsAssessorListColumn.DaysWith)))?null:(System.Int32?)reader[((int)QuestionnaireEscalationHsAssessorListColumn.DaysWith)];
					//entity.DaysWith = (Convert.IsDBNull(reader["DaysWith"]))?(int)0:(System.Int32?)reader["DaysWith"];
					entity.AcceptChanges();
					entity.SuppressEntityEvents = false;
					
					rows.Add(entity);
					pageLength -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		
		
		/// <summary>
		/// Refreshes the <see cref="QuestionnaireEscalationHsAssessorList"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="QuestionnaireEscalationHsAssessorList"/> object to refresh.</param>
		protected void RefreshEntity(IDataReader reader, QuestionnaireEscalationHsAssessorList entity)
		{
			reader.Read();
			entity.QuestionnaireId = (System.Int32)reader[((int)QuestionnaireEscalationHsAssessorListColumn.QuestionnaireId)];
			//entity.QuestionnaireId = (Convert.IsDBNull(reader["QuestionnaireId"]))?(int)0:(System.Int32)reader["QuestionnaireId"];
			entity.HsAssessorRegionId = (reader.IsDBNull(((int)QuestionnaireEscalationHsAssessorListColumn.HsAssessorRegionId)))?null:(System.Int32?)reader[((int)QuestionnaireEscalationHsAssessorListColumn.HsAssessorRegionId)];
			//entity.HsAssessorRegionId = (Convert.IsDBNull(reader["HsAssessorRegionId"]))?(int)0:(System.Int32?)reader["HsAssessorRegionId"];
			entity.HsAssessorSiteId = (reader.IsDBNull(((int)QuestionnaireEscalationHsAssessorListColumn.HsAssessorSiteId)))?null:(System.Int32?)reader[((int)QuestionnaireEscalationHsAssessorListColumn.HsAssessorSiteId)];
			//entity.HsAssessorSiteId = (Convert.IsDBNull(reader["HsAssessorSiteId"]))?(int)0:(System.Int32?)reader["HsAssessorSiteId"];
			entity.UserId = (reader.IsDBNull(((int)QuestionnaireEscalationHsAssessorListColumn.UserId)))?null:(System.Int32?)reader[((int)QuestionnaireEscalationHsAssessorListColumn.UserId)];
			//entity.UserId = (Convert.IsDBNull(reader["UserId"]))?(int)0:(System.Int32?)reader["UserId"];
			entity.SupervisorUserId = (reader.IsDBNull(((int)QuestionnaireEscalationHsAssessorListColumn.SupervisorUserId)))?null:(System.Int32?)reader[((int)QuestionnaireEscalationHsAssessorListColumn.SupervisorUserId)];
			//entity.SupervisorUserId = (Convert.IsDBNull(reader["SupervisorUserId"]))?(int)0:(System.Int32?)reader["SupervisorUserId"];
			entity.Level3UserId = (reader.IsDBNull(((int)QuestionnaireEscalationHsAssessorListColumn.Level3UserId)))?null:(System.Int32?)reader[((int)QuestionnaireEscalationHsAssessorListColumn.Level3UserId)];
			//entity.Level3UserId = (Convert.IsDBNull(reader["Level3UserId"]))?(int)0:(System.Int32?)reader["Level3UserId"];
			entity.Level4UserId = (reader.IsDBNull(((int)QuestionnaireEscalationHsAssessorListColumn.Level4UserId)))?null:(System.Int32?)reader[((int)QuestionnaireEscalationHsAssessorListColumn.Level4UserId)];
			//entity.Level4UserId = (Convert.IsDBNull(reader["Level4UserId"]))?(int)0:(System.Int32?)reader["Level4UserId"];
			entity.Level5UserId = (reader.IsDBNull(((int)QuestionnaireEscalationHsAssessorListColumn.Level5UserId)))?null:(System.Int32?)reader[((int)QuestionnaireEscalationHsAssessorListColumn.Level5UserId)];
			//entity.Level5UserId = (Convert.IsDBNull(reader["Level5UserId"]))?(int)0:(System.Int32?)reader["Level5UserId"];
			entity.Level6UserId = (reader.IsDBNull(((int)QuestionnaireEscalationHsAssessorListColumn.Level6UserId)))?null:(System.Int32?)reader[((int)QuestionnaireEscalationHsAssessorListColumn.Level6UserId)];
			//entity.Level6UserId = (Convert.IsDBNull(reader["Level6UserId"]))?(int)0:(System.Int32?)reader["Level6UserId"];
			entity.Level7UserId = (reader.IsDBNull(((int)QuestionnaireEscalationHsAssessorListColumn.Level7UserId)))?null:(System.Int32?)reader[((int)QuestionnaireEscalationHsAssessorListColumn.Level7UserId)];
			//entity.Level7UserId = (Convert.IsDBNull(reader["Level7UserId"]))?(int)0:(System.Int32?)reader["Level7UserId"];
			entity.Level8UserId = (reader.IsDBNull(((int)QuestionnaireEscalationHsAssessorListColumn.Level8UserId)))?null:(System.Int32?)reader[((int)QuestionnaireEscalationHsAssessorListColumn.Level8UserId)];
			//entity.Level8UserId = (Convert.IsDBNull(reader["Level8UserId"]))?(int)0:(System.Int32?)reader["Level8UserId"];
			entity.Level9UserId = (reader.IsDBNull(((int)QuestionnaireEscalationHsAssessorListColumn.Level9UserId)))?null:(System.Int32?)reader[((int)QuestionnaireEscalationHsAssessorListColumn.Level9UserId)];
			//entity.Level9UserId = (Convert.IsDBNull(reader["Level9UserId"]))?(int)0:(System.Int32?)reader["Level9UserId"];
			entity.HsAssessorFirstLastName = (reader.IsDBNull(((int)QuestionnaireEscalationHsAssessorListColumn.HsAssessorFirstLastName)))?null:(System.String)reader[((int)QuestionnaireEscalationHsAssessorListColumn.HsAssessorFirstLastName)];
			//entity.HsAssessorFirstLastName = (Convert.IsDBNull(reader["HsAssessorFirstLastName"]))?string.Empty:(System.String)reader["HsAssessorFirstLastName"];
			entity.HsAssessorLastFirstName = (reader.IsDBNull(((int)QuestionnaireEscalationHsAssessorListColumn.HsAssessorLastFirstName)))?null:(System.String)reader[((int)QuestionnaireEscalationHsAssessorListColumn.HsAssessorLastFirstName)];
			//entity.HsAssessorLastFirstName = (Convert.IsDBNull(reader["HsAssessorLastFirstName"]))?string.Empty:(System.String)reader["HsAssessorLastFirstName"];
			entity.HsAssessorContactEmail = (reader.IsDBNull(((int)QuestionnaireEscalationHsAssessorListColumn.HsAssessorContactEmail)))?null:(System.String)reader[((int)QuestionnaireEscalationHsAssessorListColumn.HsAssessorContactEmail)];
			//entity.HsAssessorContactEmail = (Convert.IsDBNull(reader["HsAssessorContactEmail"]))?string.Empty:(System.String)reader["HsAssessorContactEmail"];
			entity.CompanyName = (reader.IsDBNull(((int)QuestionnaireEscalationHsAssessorListColumn.CompanyName)))?null:(System.String)reader[((int)QuestionnaireEscalationHsAssessorListColumn.CompanyName)];
			//entity.CompanyName = (Convert.IsDBNull(reader["CompanyName"]))?string.Empty:(System.String)reader["CompanyName"];
			entity.ActionDescription = (reader.IsDBNull(((int)QuestionnaireEscalationHsAssessorListColumn.ActionDescription)))?null:(System.String)reader[((int)QuestionnaireEscalationHsAssessorListColumn.ActionDescription)];
			//entity.ActionDescription = (Convert.IsDBNull(reader["ActionDescription"]))?string.Empty:(System.String)reader["ActionDescription"];
			entity.UserDescription = (reader.IsDBNull(((int)QuestionnaireEscalationHsAssessorListColumn.UserDescription)))?null:(System.String)reader[((int)QuestionnaireEscalationHsAssessorListColumn.UserDescription)];
			//entity.UserDescription = (Convert.IsDBNull(reader["UserDescription"]))?string.Empty:(System.String)reader["UserDescription"];
			entity.QuestionnairePresentlyWithSince = (reader.IsDBNull(((int)QuestionnaireEscalationHsAssessorListColumn.QuestionnairePresentlyWithSince)))?null:(System.DateTime?)reader[((int)QuestionnaireEscalationHsAssessorListColumn.QuestionnairePresentlyWithSince)];
			//entity.QuestionnairePresentlyWithSince = (Convert.IsDBNull(reader["QuestionnairePresentlyWithSince"]))?DateTime.MinValue:(System.DateTime?)reader["QuestionnairePresentlyWithSince"];
			entity.DaysWith = (reader.IsDBNull(((int)QuestionnaireEscalationHsAssessorListColumn.DaysWith)))?null:(System.Int32?)reader[((int)QuestionnaireEscalationHsAssessorListColumn.DaysWith)];
			//entity.DaysWith = (Convert.IsDBNull(reader["DaysWith"]))?(int)0:(System.Int32?)reader["DaysWith"];
			reader.Close();
	
			entity.AcceptChanges();
		}
		
		/*
		/// <summary>
		/// Refreshes the <see cref="QuestionnaireEscalationHsAssessorList"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="QuestionnaireEscalationHsAssessorList"/> object.</param>
		protected static void RefreshEntity(DataSet dataSet, QuestionnaireEscalationHsAssessorList entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.QuestionnaireId = (Convert.IsDBNull(dataRow["QuestionnaireId"]))?(int)0:(System.Int32)dataRow["QuestionnaireId"];
			entity.HsAssessorRegionId = (Convert.IsDBNull(dataRow["HsAssessorRegionId"]))?(int)0:(System.Int32?)dataRow["HsAssessorRegionId"];
			entity.HsAssessorSiteId = (Convert.IsDBNull(dataRow["HsAssessorSiteId"]))?(int)0:(System.Int32?)dataRow["HsAssessorSiteId"];
			entity.UserId = (Convert.IsDBNull(dataRow["UserId"]))?(int)0:(System.Int32?)dataRow["UserId"];
			entity.SupervisorUserId = (Convert.IsDBNull(dataRow["SupervisorUserId"]))?(int)0:(System.Int32?)dataRow["SupervisorUserId"];
			entity.Level3UserId = (Convert.IsDBNull(dataRow["Level3UserId"]))?(int)0:(System.Int32?)dataRow["Level3UserId"];
			entity.Level4UserId = (Convert.IsDBNull(dataRow["Level4UserId"]))?(int)0:(System.Int32?)dataRow["Level4UserId"];
			entity.Level5UserId = (Convert.IsDBNull(dataRow["Level5UserId"]))?(int)0:(System.Int32?)dataRow["Level5UserId"];
			entity.Level6UserId = (Convert.IsDBNull(dataRow["Level6UserId"]))?(int)0:(System.Int32?)dataRow["Level6UserId"];
			entity.Level7UserId = (Convert.IsDBNull(dataRow["Level7UserId"]))?(int)0:(System.Int32?)dataRow["Level7UserId"];
			entity.Level8UserId = (Convert.IsDBNull(dataRow["Level8UserId"]))?(int)0:(System.Int32?)dataRow["Level8UserId"];
			entity.Level9UserId = (Convert.IsDBNull(dataRow["Level9UserId"]))?(int)0:(System.Int32?)dataRow["Level9UserId"];
			entity.HsAssessorFirstLastName = (Convert.IsDBNull(dataRow["HsAssessorFirstLastName"]))?string.Empty:(System.String)dataRow["HsAssessorFirstLastName"];
			entity.HsAssessorLastFirstName = (Convert.IsDBNull(dataRow["HsAssessorLastFirstName"]))?string.Empty:(System.String)dataRow["HsAssessorLastFirstName"];
			entity.HsAssessorContactEmail = (Convert.IsDBNull(dataRow["HsAssessorContactEmail"]))?string.Empty:(System.String)dataRow["HsAssessorContactEmail"];
			entity.CompanyName = (Convert.IsDBNull(dataRow["CompanyName"]))?string.Empty:(System.String)dataRow["CompanyName"];
			entity.ActionDescription = (Convert.IsDBNull(dataRow["ActionDescription"]))?string.Empty:(System.String)dataRow["ActionDescription"];
			entity.UserDescription = (Convert.IsDBNull(dataRow["UserDescription"]))?string.Empty:(System.String)dataRow["UserDescription"];
			entity.QuestionnairePresentlyWithSince = (Convert.IsDBNull(dataRow["QuestionnairePresentlyWithSince"]))?DateTime.MinValue:(System.DateTime?)dataRow["QuestionnairePresentlyWithSince"];
			entity.DaysWith = (Convert.IsDBNull(dataRow["DaysWith"]))?(int)0:(System.Int32?)dataRow["DaysWith"];
			entity.AcceptChanges();
		}
		*/
			
		#endregion Helper Functions
	}//end class

	#region QuestionnaireEscalationHsAssessorListFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireEscalationHsAssessorList"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireEscalationHsAssessorListFilterBuilder : SqlFilterBuilder<QuestionnaireEscalationHsAssessorListColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireEscalationHsAssessorListFilterBuilder class.
		/// </summary>
		public QuestionnaireEscalationHsAssessorListFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireEscalationHsAssessorListFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireEscalationHsAssessorListFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireEscalationHsAssessorListFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireEscalationHsAssessorListFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireEscalationHsAssessorListFilterBuilder

	#region QuestionnaireEscalationHsAssessorListParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireEscalationHsAssessorList"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireEscalationHsAssessorListParameterBuilder : ParameterizedSqlFilterBuilder<QuestionnaireEscalationHsAssessorListColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireEscalationHsAssessorListParameterBuilder class.
		/// </summary>
		public QuestionnaireEscalationHsAssessorListParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireEscalationHsAssessorListParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireEscalationHsAssessorListParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireEscalationHsAssessorListParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireEscalationHsAssessorListParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireEscalationHsAssessorListParameterBuilder
	
	#region QuestionnaireEscalationHsAssessorListSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireEscalationHsAssessorList"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class QuestionnaireEscalationHsAssessorListSortBuilder : SqlSortBuilder<QuestionnaireEscalationHsAssessorListColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireEscalationHsAssessorListSqlSortBuilder class.
		/// </summary>
		public QuestionnaireEscalationHsAssessorListSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion QuestionnaireEscalationHsAssessorListSortBuilder

} // end namespace
