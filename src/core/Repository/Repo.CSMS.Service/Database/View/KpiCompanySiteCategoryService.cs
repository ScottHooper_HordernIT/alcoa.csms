﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database.View
{
    #region Interface

    public interface IKpiCompanySiteCategoryService
    {
        KpiCompanySiteCategory Get(Expression<Func<KpiCompanySiteCategory, bool>> where, List<Expression<Func<KpiCompanySiteCategory, object>>> includes);
        List<KpiCompanySiteCategory> GetMany(int? top, Expression<Func<KpiCompanySiteCategory, bool>> where, Func<IQueryable<KpiCompanySiteCategory>, IOrderedQueryable<KpiCompanySiteCategory>> orderBy, List<Expression<Func<KpiCompanySiteCategory, object>>> includes);
        List<KpiCompanySiteCategory> GetManyPaged(int pageNumber, int pageSize, Expression<Func<KpiCompanySiteCategory, bool>> where, Func<IQueryable<KpiCompanySiteCategory>, IOrderedQueryable<KpiCompanySiteCategory>> orderBy, List<Expression<Func<KpiCompanySiteCategory, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<KpiCompanySiteCategory, bool>> where, List<Expression<Func<KpiCompanySiteCategory, object>>> includes);
    }
    #endregion

    #region Interface Implementation

    public class KpiCompanySiteCategoryService : IKpiCompanySiteCategoryService
    {
        private readonly IRepository<KpiCompanySiteCategory> _repository;

        public KpiCompanySiteCategoryService(IRepository<KpiCompanySiteCategory> repository)
        {
            this._repository = repository;
        }

        public KpiCompanySiteCategory Get(Expression<Func<KpiCompanySiteCategory, bool>> where, List<Expression<Func<KpiCompanySiteCategory, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<KpiCompanySiteCategory> GetMany(int? top, Expression<Func<KpiCompanySiteCategory, bool>> where, Func<IQueryable<KpiCompanySiteCategory>, IOrderedQueryable<KpiCompanySiteCategory>> orderBy, List<Expression<Func<KpiCompanySiteCategory, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<KpiCompanySiteCategory> GetManyPaged(int pageNumber, int pageSize, Expression<Func<KpiCompanySiteCategory, bool>> where, Func<IQueryable<KpiCompanySiteCategory>, IOrderedQueryable<KpiCompanySiteCategory>> orderBy, List<Expression<Func<KpiCompanySiteCategory, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<KpiCompanySiteCategory, bool>> where, List<Expression<Func<KpiCompanySiteCategory, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }
    }

    #endregion
}
