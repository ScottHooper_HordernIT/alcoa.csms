<%@ Control Language="C#" AutoEventWireup="true" Inherits="UserControls_Admin_ebi" CodeBehind="EBI.ascx.cs" %>

<%@ Register Src="../Other/ExportButtons.ascx" TagName="ExportButtons" TagPrefix="uc1" %>

<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallbackPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxScheduler.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxScheduler" TagPrefix="dxwschs" %>


<%@ Register Assembly="DevExpress.Web.v14.1" Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1" Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1" Namespace="DevExpress.Web.ASPxCallbackPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1" Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>


<dx:ASPxPageControl ID="ASPxPageControl1" runat="server" ActiveTabIndex="1" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
    CssPostfix="Office2003Blue" Width="900px">
    <loadingpanelimage url="~/App_Themes/Office2003Blue/Web/Loading.gif"></loadingpanelimage>

    <contentstyle>
<Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px"></Border>
</contentstyle>
    <tabpages>
    <dx:TabPage Text="Configuration">
        <ContentCollection>
            <dx:ContentControl runat="server">
                <div>
                <table>
                    <tr>
                        <td>
                        <dx:ASPxCheckBox ID="cbAutoEmail"  
                       runat="server" EnableSynchronization="False" 
                       CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
                        CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                        ValueType="System.Int32" >
                       
                        </dx:ASPxCheckBox> 
                        </td>
                        <td> Automatic Email Sent out to Site Supervisors when Alcoa 16/64 Violation occurs. </td>
                    <td>
                    <dx:ASPxButton ID="btnAutoEmail" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                    CssPostfix="Office2003Blue" OnClick="btnAutoEmail_Click" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                    Text="Save">                    
                    </dx:ASPxButton>
                    </td>
                    </tr>
                </table>
                </div>
                
                <strong>Sites</strong>&nbsp;<dx:ASPxGridView ID="gridSites" runat="server" AutoGenerateColumns="False" DataSourceID="dsSitesFilter"
        KeyFieldName="SiteId" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue" Width="100%">
        <columns>
            <dx:GridViewCommandColumn Caption="Action" VisibleIndex="0" Width="80px">
                <EditButton Visible="True">
                </EditButton>
                <ClearFilterButton Visible="true"></ClearFilterButton>
            </dx:GridViewCommandColumn>
            <dx:GridViewDataTextColumn FieldName="SiteId" ReadOnly="True" Visible="False" VisibleIndex="0">
                <EditFormSettings Visible="False" />
            </dx:GridViewDataTextColumn>
            
            <dx:GridViewDataComboBoxColumn FieldName="SiteName" VisibleIndex="1" 
                                                 Caption="Site Name (In CSMS)"  ReadOnly="True"  SortIndex="0" SortOrder="Ascending">
                                                 <HeaderStyle HorizontalAlign="Center" />
                                                <PropertiesComboBox DataSourceID="SitesDataSource2" TextField="SiteName"
                                                    IncrementalFilteringMode="StartsWith">
                                                </PropertiesComboBox>
                                                <Settings SortMode="DisplayText"></Settings>
                                                </dx:GridViewDataComboBoxColumn>
            
            
            <dx:GridViewDataComboBoxColumn FieldName="SiteNameEbi" VisibleIndex="2" 
                                                 Caption="Site Name (In EBI)">
                                                 <HeaderStyle HorizontalAlign="Center" />
                                                <PropertiesComboBox DataSourceID="SitesDtSource" TextField="SiteNameEbi"
                                                    IncrementalFilteringMode="StartsWith">
                                                </PropertiesComboBox>
                                                <Settings SortMode="DisplayText"></Settings>
                                                </dx:GridViewDataComboBoxColumn>
</columns>
                <Settings ShowFilterRow="true"></Settings>
                    <SettingsPager PageSize="100">
                        <AllButton Visible="True"></AllButton>
                    </SettingsPager>
                    <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                        <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
                        </LoadingPanelOnStatusBar>
                        <LoadingPanel Url="~/App_Themes/Office2003BlueOld/GridView/Loading.gif"><%--Enhancement_023:change by debashis for testing platform upgradation--%>
                        </LoadingPanel>
                    </Images>
                    <ImagesFilterControl>
                        <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
                        </LoadingPanel>
                    </ImagesFilterControl>
                    <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue">
                        <Header ImageSpacing="5px" SortingImageSpacing="5px">
                        </Header>
                        <LoadingPanel ImageSpacing="10px">
                        </LoadingPanel>
                    </Styles>
                    <StylesEditors>
                        <ProgressBar Height="25px">
                        </ProgressBar>
                    </StylesEditors>
                    <SettingsBehavior ColumnResizeMode="NextColumn" ConfirmDelete="True"></SettingsBehavior>
    </dx:ASPxGridView>
            </dx:ContentControl>
        </ContentCollection>
    </dx:TabPage>
    <dx:TabPage Text="Raw Data">
        <ContentCollection>
            <dx:ContentControl runat="server">
                    <table style="width:100%; text-align:left; vertical-align:top">
                <tr>
                    <td style="width:30%; text-align:left; vertical-align:top">
                      <div style="float:left; padding-top:4px"><b>Company:&nbsp;</b></div>
                      <div style="float:left"><dx:ASPxComboBox ID="CmbRawDataCompany"
                       ClientInstanceName="cmbCompanies" runat="server"
                        CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                        CssPostfix="Office2003Blue" EnableSynchronization="False" 
                         SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                          OnSelectedIndexChanged="CmbRawDataCompany_SelectedIndexChanged"
                        ValueType="System.Int32" IncrementalFilteringMode="StartsWith">
                        <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                        </LoadingPanelImage>
                        <ButtonStyle Width="13px">
                        </ButtonStyle>
                        <ClientSideEvents SelectedIndexChanged="function(s, e) {
	                                                                    cmbSites.PerformCallback(s.GetValue());
                                                                    }" />
                    </dx:ASPxComboBox></div>
                    </td>
                    <td style="width:25%; text-align:left; vertical-align:top">
                        <div style="float:left; padding-top:4px"><b>Site:&nbsp;</b></div> 
                      <div style="float:left"><dx:ASPxComboBox ID="CmbRawDataSite"  ClientInstanceName="cmbSites"
                       runat="server" EnableSynchronization="False" OnCallback="cmbSites_Callback"
                       CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" IncrementalFilteringMode="StartsWith"
                        CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                        ValueType="System.Int32" >
                        <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                        </LoadingPanelImage>
                        <ButtonStyle Width="13px">
                        </ButtonStyle>
                    </dx:ASPxComboBox></div>
                    </td>
                    <td style="width:30%; text-align:left; vertical-align:top">
                         <div style="float:left; padding-top:4px"><b>Month/Year:&nbsp;</b></div>
                      <div style="float:left"><dx:ASPxComboBox ID="cmbRawDataMonth" Width="70px" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                        CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                        ValueType="System.Int32">
                        <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                        </LoadingPanelImage>
                        <ButtonStyle Width="13px">
                        </ButtonStyle>
                    </dx:ASPxComboBox></div>
                    <div style="float:left; padding-left:5px"><dx:ASPxComboBox ID="cbRawData_Year" Width="55px" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                        CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                        ValueType="System.Int32">
                        <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                        </LoadingPanelImage>
                        <ButtonStyle Width="13px">
                        </ButtonStyle>
                    </dx:ASPxComboBox></div>
                    </td>
                    <td  style="width:7%; text-align:left; vertical-align:top">
                         <a href="javascript:ShowHideCustomizationWindow2()"><span style="color: #0000ff; text-decoration: none">
                Customize</span></a>
                    </td>
                    <td style="width:8%; text-align:left; vertical-align:top">
                         <dx:ASPxButton ID="ASPxButton1" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                    CssPostfix="Office2003Blue" OnClick="ASPxButton1_Click" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                    Text="Go/Refresh">
                </dx:ASPxButton>
                    </td>
                </tr>
            </table>

                    <br />
                    <dx:ASPxGridView width="100%" ID="grid2" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue" AutoGenerateColumns="False" Settings-ShowGroupPanel="true" DataSourceId="dsEbi_RawData_ByYearMonthCompanySite" ClientInstanceName="grid2">
                    <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                        <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
                        </LoadingPanelOnStatusBar>
                        <LoadingPanel Url="~/App_Themes/Office2003BlueOld/GridView/Loading.gif"><%--Enhancement_023:change by debashis for testing platform upgradation--%>
                        </LoadingPanel>
                    </Images>
                    <ImagesFilterControl>
                        <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
                        </LoadingPanel>
                    </ImagesFilterControl>
                    <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue">
                        <Header ImageSpacing="5px" SortingImageSpacing="5px">
                        </Header>
                        <AlternatingRow Enabled="True">
                    </AlternatingRow>
                        <LoadingPanel ImageSpacing="10px">
                        </LoadingPanel>
                    </Styles>
                    <StylesEditors>
                        <ProgressBar Height="25px">
                        </ProgressBar>
                    </StylesEditors>
                    <Settings ShowFilterRow="true" ShowFilterBar="Visible" ShowGroupPanel="True" />
                    <SettingsPager>
                            <AllButton Visible="True">
                            </AllButton>
                        </SettingsPager>
                    <Columns>
                        <dx:GridViewDataComboBoxColumn FieldName="SwipeMonth" VisibleIndex="0"
                                                Caption="Month" UnboundType="Integer">
                                                <PropertiesComboBox>
                                                   <Items>
<dx:ListEditItem Text="1" Value="1" />
<dx:ListEditItem Text="2" Value="2" />
<dx:ListEditItem Text="3" Value="3" />
<dx:ListEditItem Text="4" Value="4" />
<dx:ListEditItem Text="5" Value="5" />
<dx:ListEditItem Text="6" Value="6" />
<dx:ListEditItem Text="7" Value="7" />
<dx:ListEditItem Text="8" Value="8" />
<dx:ListEditItem Text="9" Value="9" />
<dx:ListEditItem Text="10" Value="10" />
<dx:ListEditItem Text="11" Value="11" />
<dx:ListEditItem Text="12" Value="12" />
</Items>
</PropertiesComboBox>
                                                <settings allowheaderfilter="True" />
                       </dx:GridViewDataComboBoxColumn>
                       <dx:GridViewDataTextColumn FieldName="SwipeDay" VisibleIndex="1"
                                                Caption="Day" >
                           <settings allowheaderfilter="True" />
                       </dx:GridViewDataTextColumn>
                       <dx:GridViewDataDateColumn Caption="Swipe Date/Time" FieldName="SwipeDateTime" ReadOnly="True" VisibleIndex="2" SortOrder="Descending">
                                <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy HH:mm:ss">
                                </PropertiesDateEdit>
                        </dx:GridViewDataDateColumn>
                         
                          <dx:GridViewDataTextColumn FieldName="Description" Caption="Swipe Location" VisibleIndex="3">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="Source" Caption="Source" VisibleIndex="4">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="ClockId" Caption="Clock Id" VisibleIndex="5">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn  FieldName="FullName" Caption="Full Name" VisibleIndex="6">
                        </dx:GridViewDataTextColumn>
                        <%--<dx:GridViewDataTextColumn  FieldName="CompanyId" Visible="false">
                        </dx:GridViewDataTextColumn>--%>
                        <dx:GridViewDataComboBoxColumn FieldName="CompanyName" VisibleIndex="7" Caption="Company Name" >
                                                <PropertiesComboBox DataSourceID="CompanyNameDS" TextField="CompanyName"
                                                     IncrementalFilteringMode="StartsWith" >
                                                </PropertiesComboBox>
                                                <Settings SortMode="DisplayText"></Settings>
                                                 </dx:GridViewDataComboBoxColumn>

                        
                        <dx:GridViewDataComboBoxColumn FieldName="SwipeSite" VisibleIndex="8" Caption="Site">
                                                <PropertiesComboBox DataSourceID="SiteNameEbiDS" TextField="SiteNameEbi" 
                                                     IncrementalFilteringMode="StartsWith"  NullDisplayText="-">
                                                </PropertiesComboBox>
                                                <Settings SortMode="DisplayText"></Settings>
                        </dx:GridViewDataComboBoxColumn>
                        <dx:GridViewDataTextColumn Caption="EBI Card #" FieldName="AccessCardNo" ShowInCustomizationForm="True" VisibleIndex="9" Visible="False">
                            <settings sortmode="DisplayText" />
                        </dx:GridViewDataTextColumn>
                    </Columns>
                        <SettingsCustomizationWindow Enabled="True" />
                        </dx:ASPxGridView>

                    
                
                <asp:SqlDataSource ID="dsEbi_RawData_ByYearMonthCompanySite" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
                        SelectCommand="_EbiCompaniesMap_CompaniesRawData_ByYearMonthCompanyIdSiteId" SelectCommandType="StoredProcedure">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="cbRawData_Year" Name="Year" PropertyName="Value" Type="Int32" />
                            <asp:ControlParameter ControlID="cmbRawDataMonth" Name="Month" PropertyName="Value" Type="Int32" />
                            <asp:ControlParameter ControlID="CmbRawDataCompany" Name="CompanyId" PropertyName="Value" Type="Int32" />
                            <asp:ControlParameter ControlID="CmbRawDataSite" Name="SiteId" PropertyName="Value" Type="Int32" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                    <div align="right">
                <uc1:ExportButtons ID="ExportButtons3" runat="server" />
                </div>
                
                    
            </dx:ContentControl>
        </ContentCollection>
    </dx:TabPage>
    
    <dx:TabPage Text="Company By Day">
            <ContentCollection>
                <dx:ContentControl runat="server">
                    Select Date:&nbsp;
                    <dx:ASPxDateEdit runat="server" AllowNull="False" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" ClientInstanceName="deByDay" ID="deByDay">
<ClientSideEvents DateChanged="function(s, e) {
     cbpByDay.PerformCallback(s.GetValue());
}"></ClientSideEvents>

<ButtonStyle Width="13px"></ButtonStyle>
</dx:ASPxDateEdit>
                    <dx:ASPxCallbackPanel ID="cbpByDay" runat="server" ClientInstanceName="cbpByDay"
                        Width="200px" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue">
                        <panelcollection>
<dx:PanelContent runat="server">
                    <dx:ASPxGridView ID="gridByDay" runat="server" ClientInstanceName="gridByDay" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue" Width="880px" AutoGenerateColumns="False"  Settings-ShowGroupPanel="true" DataSourceId="Ebi_YearMonthDayCompanies_ByDay">
                        <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                            <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
                            </LoadingPanelOnStatusBar>
                            <LoadingPanel Url="~/App_Themes/Office2003BlueOld/GridView/Loading.gif"><%--Enhancement_023:change by debashis for testing platform upgradation--%>
                            </LoadingPanel>
                        </Images>
                        <ImagesFilterControl>
                            <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
                            </LoadingPanel>
                        </ImagesFilterControl>
                        <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue">
                            <Header ImageSpacing="5px" SortingImageSpacing="5px">
                            </Header>
                            <LoadingPanel ImageSpacing="10px">
                            </LoadingPanel>
                        </Styles>
                        <StylesEditors>
                            <ProgressBar Height="25px">
                            </ProgressBar>
                        </StylesEditors>
                        <Columns>
                            <dx:GridViewDataDateColumn FieldName="ClockInFirst" ReadOnly="True" VisibleIndex="0" SortIndex="0">
                                <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy HH:mm:ss">
                                </PropertiesDateEdit>
                            </dx:GridViewDataDateColumn>
                            <dx:GridViewDataDateColumn FieldName="ClockInLast" ReadOnly="True" VisibleIndex="1">
                                <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy HH:mm:ss">
                                </PropertiesDateEdit>
                            </dx:GridViewDataDateColumn>
                            <dx:GridViewDataDateColumn FieldName="Time" ReadOnly="True" VisibleIndex="2">
                                <PropertiesDateEdit DisplayFormatString="HH:mm:ss">
                                </PropertiesDateEdit>
                            </dx:GridViewDataDateColumn>
                            
                            <dx:GridViewDataTextColumn Caption="Company (EBI)" FieldName="Ebi_Company" 
                                                    VisibleIndex="3">
                                
                            </dx:GridViewDataTextColumn>
                            
                            <dx:GridViewDataComboBoxColumn FieldName="Ebi_Site" VisibleIndex="4" Caption="Site (EBI)">
                                                <PropertiesComboBox DataSourceID="SitesDtSource" TextField="SiteNameEbi"
                                                     IncrementalFilteringMode="StartsWith">
                                                </PropertiesComboBox>
                                                <Settings SortMode="DisplayText"></Settings>
                                                 </dx:GridViewDataComboBoxColumn>
                            <dx:GridViewDataTextColumn Caption="Company Name (CSMS)" FieldName="CompanyId" ReadOnly="True"
                                Visible="False" VisibleIndex="5">
                                <EditFormSettings Visible="False" />
                            </dx:GridViewDataTextColumn>
                           
                            
                            <dx:GridViewDataComboBoxColumn FieldName="CompanyName" VisibleIndex="5" Caption="Company Name (CSMS)">
                                                <PropertiesComboBox DataSourceID="CompanyNameDS" TextField="CompanyName"
                                                     IncrementalFilteringMode="StartsWith">
                                                </PropertiesComboBox>
                                                <Settings SortMode="DisplayText"></Settings>
                                                 </dx:GridViewDataComboBoxColumn>
                            <dx:GridViewDataTextColumn Caption="Site (CSMS)" FieldName="SiteId" ReadOnly="True"
                                Visible="False" VisibleIndex="7">
                                <EditFormSettings Visible="False" />
                            </dx:GridViewDataTextColumn>
                            
                            <dx:GridViewDataComboBoxColumn FieldName="SiteName" VisibleIndex="6" Caption="Site (CSMS)">
                                                <PropertiesComboBox DataSourceID="SitesDataSource2" TextField="SiteName"
                                                     IncrementalFilteringMode="StartsWith">
                                                </PropertiesComboBox>
                                                <Settings SortMode="DisplayText"></Settings>
                                                 </dx:GridViewDataComboBoxColumn>
                            <dx:GridViewDataTextColumn FieldName="SwipeYear" Visible="False" VisibleIndex="9">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="SwipeMonth" Visible="False" VisibleIndex="10">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="SwipeDay" Visible="False" VisibleIndex="11">
                            </dx:GridViewDataTextColumn>
                        </Columns>
                        <Settings ShowFilterbar="Visible" ShowFilterRow="true"/>
                        <SettingsPager>
                            <AllButton Visible="True">
                            </AllButton>
                        </SettingsPager>
                        <SettingsBehavior ColumnResizeMode="NextColumn" ConfirmDelete="True"></SettingsBehavior>
                    </dx:ASPxGridView>

 </dx:PanelContent>
</panelcollection>
                        <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                        </LoadingPanelImage>
                    </dx:ASPxCallbackPanel>
                    <div align="right">
                        <uc1:ExportButtons ID="ExportButtons1" runat="server" />
                    </div>
                    <asp:SqlDataSource ID="Ebi_YearMonthDayCompanies_ByDay" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
                        SelectCommand="_EbiCompaniesMap_YearMonthDayCompanies_ByDay" SelectCommandType="StoredProcedure">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="deByDay" DefaultValue="1/1/2010" Name="Day" PropertyName="Value"
                                Type="DateTime" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                </dx:ContentControl>
            </ContentCollection>
        </dx:TabPage>
        <dx:TabPage Text="Company By Month">
            <ContentCollection>
                <dx:ContentControl runat="server">
                    Select Date:<br />Year:<br />
                    <dx:ASPxComboBox ID="cbByMonth_Year" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                        CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                        ValueType="System.String">
                        <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                        </LoadingPanelImage>
                        <ButtonStyle Width="13px">
                        </ButtonStyle>
                    </dx:ASPxComboBox>
                    <br />
                    Month:
                    <dx:ASPxComboBox ID="cbByMonth_Month" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                        CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                        ValueType="System.String">
                        <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                        </LoadingPanelImage>
                        <ButtonStyle Width="13px">
                        </ButtonStyle>
                    </dx:ASPxComboBox>
                    <br />
                    <dx:ASPxButton ID="btnByMonth_GoFilter" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                        CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                        Text="Go / Filter" OnClick="btnByMonth_GoFilter_Click">
                        
                    </dx:ASPxButton>
                    <dx:ASPxGridView ID="gridByMonth" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue" Width="880px" AutoGenerateColumns="False"  Settings-ShowGroupPanel="true" >
                        <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" >
                            <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
                            </LoadingPanelOnStatusBar>
                            <LoadingPanel Url="~/App_Themes/Office2003BlueOld/GridView/Loading.gif"><%--Enhancement_023:change by debashis for testing platform upgradation--%>
                            </LoadingPanel>
                        </Images>
                        <ImagesFilterControl>
                            <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
                            </LoadingPanel>
                        </ImagesFilterControl>
                        <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue">
                            <Header ImageSpacing="5px" SortingImageSpacing="5px">
                            </Header>
                            <LoadingPanel ImageSpacing="10px">
                            </LoadingPanel>
                        </Styles>
                        <StylesEditors>
                            <ProgressBar Height="25px">
                            </ProgressBar>
                        </StylesEditors>
                        <Columns>
                            <dx:GridViewDataTextColumn FieldName="CompanyId" VisibleIndex="0" Visible="False">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="SiteId" VisibleIndex="1" Visible="False">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="CompanyStatusId" VisibleIndex="2" Visible="False">
                            </dx:GridViewDataTextColumn>
                            
                            <dx:GridViewDataComboBoxColumn FieldName="CompanyName" VisibleIndex="0" Caption="Company Name (CSMS)" SortIndex="0">
                                                <PropertiesComboBox DataSourceID="CompaniesDataSource3" TextField="CompanyName"
                                                     IncrementalFilteringMode="StartsWith">
                                                </PropertiesComboBox>
                                                <Settings SortMode="DisplayText"></Settings>
                            </dx:GridViewDataComboBoxColumn>
                            
                            <dx:GridViewDataComboBoxColumn FieldName="SiteName" VisibleIndex="2" Caption="Site (CSMS)" SortIndex="1">
                                                <PropertiesComboBox DataSourceID="SitesDataSource2" TextField="SiteName"
                                                     IncrementalFilteringMode="StartsWith">
                                                </PropertiesComboBox>
                                                <Settings SortMode="DisplayText"></Settings>
                                                 </dx:GridViewDataComboBoxColumn>
                            
                            <dx:GridViewDataComboBoxColumn FieldName="SiteNameEbi" VisibleIndex="3" Caption="Site (EBI)">
                                                <PropertiesComboBox DataSourceID="SitesDtSource" TextField="SiteNameEbi"
                                                     IncrementalFilteringMode="StartsWith">
                                                </PropertiesComboBox>
                                                <Settings SortMode="DisplayText"></Settings>
                                                 </dx:GridViewDataComboBoxColumn>
                            
                            <dx:GridViewDataComboBoxColumn FieldName="OnSite" VisibleIndex="4"
                                                 UnboundType="String" SortIndex="2" ReadOnly="True">
                                                <PropertiesComboBox  ValueType="System.String">
                                                   <items>
<dx:ListEditItem Text="Yes" Value="Yes" />
<dx:ListEditItem Text="No" Value="No" />
<dx:ListEditItem Text="(No Ebi Link)" Value="(No Ebi Link)" />
</items>
</PropertiesComboBox>
                                            </dx:GridViewDataComboBoxColumn>
                            
                            <dx:GridViewDataComboBoxColumn FieldName="InQualifiedDatabaseReport" VisibleIndex="5"
                                                 UnboundType="String" ReadOnly="True">
                                                <PropertiesComboBox  ValueType="System.String">
                                                   <items>
<dx:ListEditItem Text="Yes" Value="Yes" />
<dx:ListEditItem Text="No" Value="No" />
</items>
</PropertiesComboBox>
                                            </dx:GridViewDataComboBoxColumn>
                            
                            <dx:GridViewDataComboBoxColumn FieldName="InStatusReport" VisibleIndex="6"
                                                 UnboundType="String" ReadOnly="True">
                                                <PropertiesComboBox  ValueType="System.String">
                                                   <items>
<dx:ListEditItem Text="Yes" Value="Yes" />
<dx:ListEditItem Text="No" Value="No" />
</items>
</PropertiesComboBox>
                                            </dx:GridViewDataComboBoxColumn>
                            <dx:GridViewDataComboBoxColumn FieldName="CompanyStatus" VisibleIndex="7" ReadOnly="True">
                                                <PropertiesComboBox DataSourceID="CompanyStatusDataSource1" TextField="CompanyStatusDesc"
                                                     IncrementalFilteringMode="StartsWith">
                                                </PropertiesComboBox>
                                                <Settings SortMode="DisplayText"></Settings>
                                                 </dx:GridViewDataComboBoxColumn>
                        </Columns>
                        <Settings ShowFilterBar="Visible" ShowFilterRow="true" />
                        <SettingsPager >
                            <AllButton Visible="True">
                            </AllButton>
                        </SettingsPager>
                        <SettingsBehavior ColumnResizeMode="NextColumn" ConfirmDelete="True"></SettingsBehavior>
                    </dx:ASPxGridView>
                    <asp:SqlDataSource ID="Ebi_YearMonthDayCompanies_ByYearMonth" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
                        SelectCommand="_Ebi_CompaniesResidential_OnSite_ByYearMonth" SelectCommandType="StoredProcedure">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="cbByMonth_Year" Name="Year" PropertyName="Value"
                                Type="Int32" />
                            <asp:ControlParameter ControlID="cbByMonth_Month" Name="Month" PropertyName="Value"
                                Type="Int32" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                    <div align="right">
                        <uc1:ExportButtons ID="ExportButtons2" runat="server" />
                    </div>
                </dx:ContentControl>
            </ContentCollection>
        </dx:TabPage>
        <dx:TabPage Text="Hours Worked">
            <ContentCollection>
                <dx:ContentControl runat="server">

                <table style="width:100%; text-align:left; vertical-align:top">
                <tr>
                    <td style="width:30%; text-align:left; vertical-align:top">
                      <div style="float:left; padding-top:4px"><b>Company:&nbsp;</b></div>
                      <div style="float:left"><dx:ASPxComboBox ID="cmbHourWorkedCompany"
                       ClientInstanceName="cmbCompanies1" runat="server"
                        CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                        CssPostfix="Office2003Blue" EnableSynchronization="False" 
                         SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                          OnSelectedIndexChanged="cmbHourWorkedCompany_SelectedIndexChanged"
                        ValueType="System.Int32" IncrementalFilteringMode="StartsWith">
                        <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                        </LoadingPanelImage>
                        <ButtonStyle Width="13px">
                        </ButtonStyle>
                        <ClientSideEvents SelectedIndexChanged="function(s, e) {
	                                                                    cmbSites1.PerformCallback(s.GetValue());
                                                                    }" />
                    </dx:ASPxComboBox></div>
                    </td>
                    <td style="width:25%; text-align:left; vertical-align:top">
                        <div style="float:left; padding-top:4px"><b>Site:&nbsp;</b></div> 
                      <div style="float:left"><dx:ASPxComboBox ID="cmbHourWorkedSite"  ClientInstanceName="cmbSites1"
                       runat="server" EnableSynchronization="False" OnCallback="cmbSites1_Callback"
                       CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" IncrementalFilteringMode="StartsWith"
                        CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                        ValueType="System.Int32">
                        <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                        </LoadingPanelImage>
                        <ButtonStyle Width="13px">
                        </ButtonStyle>
                    </dx:ASPxComboBox></div>
                    </td>
                    <td style="width:32%; text-align:left; vertical-align:top">
                         <div style="float:left; padding-top:4px"><b>Month/Year:&nbsp;</b></div>
                      <div style="float:left"><dx:ASPxComboBox ID="cmbHourWorkedMonth" Width="70px" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                        CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                        ValueType="System.String">
                        <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                        </LoadingPanelImage>
                        <ButtonStyle Width="13px">
                        </ButtonStyle>
                    </dx:ASPxComboBox></div>
                    <div style="float:left; padding-left:5px"><dx:ASPxComboBox ID="cmbHourWorkedYear" Width="55px" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                        CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                        ValueType="System.String">
                        <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                        </LoadingPanelImage>
                        <ButtonStyle Width="13px">
                        </ButtonStyle>
                    </dx:ASPxComboBox></div>
                    </td>
                    <td style="width:6%; text-align:left; vertical-align:top">
                     <a href="javascript:ShowHideCustomizationWindow()"><span style="color: #0000ff; text-decoration: none">
                Customize</span></a>
                        </td>
                    <td style="width:7%; text-align:left; vertical-align:top">
                         <dx:ASPxButton ID="btnHoursWorked" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                    CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                    Text="Go/Refresh" OnClick="btnHoursWorked_Click">
                </dx:ASPxButton>
                    </td>
                </tr>
            </table>

                <br />
                    <dx:ASPxGridView width="100%" ID="gridHourWorked_ByMonthYear" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue" AutoGenerateColumns="False" Settings-ShowGroupPanel="true" DataSourceId="EbiHoursWorkedDS" EnableRowsCache="true" ClientInstanceName="grid" >
                    <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                        <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
                        </LoadingPanelOnStatusBar>
                        <LoadingPanel Url="~/App_Themes/Office2003BlueOld/GridView/Loading.gif"><%--Enhancement_023:change by debashis for testing platform upgradation--%>
                        </LoadingPanel>
                    </Images>
                    <ImagesFilterControl>
                        <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
                        </LoadingPanel>
                    </ImagesFilterControl>
                    <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue">
                        <Header ImageSpacing="5px" SortingImageSpacing="5px">
                        </Header>
                        <AlternatingRow Enabled="True">
                    </AlternatingRow>
                        <LoadingPanel ImageSpacing="10px">
                        </LoadingPanel>
                    </Styles>
                    <StylesEditors>
                        <ProgressBar Height="25px">
                        </ProgressBar>
                    </StylesEditors>
                    <Settings ShowFilterRow="true" ShowFilterBar="Visible" ShowGroupPanel="True" />
                    <Columns>
                        
                       <dx:GridViewDataDateColumn Caption="Date/Time In" FieldName="EntryDateTime" ReadOnly="True" VisibleIndex="0" >
                                <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy HH:mm:ss">
                                </PropertiesDateEdit>
                        </dx:GridViewDataDateColumn>
                         <dx:GridViewDataDateColumn Caption="Date/Time Out" FieldName="ExitDateTime" ReadOnly="True" VisibleIndex="1" SortOrder="Descending">
                                <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy HH:mm:ss">
                                </PropertiesDateEdit>
                        </dx:GridViewDataDateColumn>
                        <dx:GridViewDataTextColumn FieldName="TotalTime" Caption="Total Time" VisibleIndex="2">
                        </dx:GridViewDataTextColumn>
                          <dx:GridViewDataTextColumn FieldName="ClockId" Caption="ClockID" VisibleIndex="3">
                        </dx:GridViewDataTextColumn>
                          
                        <dx:GridViewDataTextColumn  FieldName="FullName" Caption="Full Name" VisibleIndex="6">
                        </dx:GridViewDataTextColumn>
                        <%-- <dx:GridViewDataTextColumn  FieldName="Vendor_Name" Caption="Vendor Name" VisibleIndex="7">
                        </dx:GridViewDataTextColumn> --%>                      
                        <%--EBI_DT_2958--%>
                        <%--<dx:GridViewDataComboBoxColumn FieldName="CompanyName" VisibleIndex="7" Caption="Company Name">
                                                <PropertiesComboBox DataSourceID="CompanyNameDS" TextField="CompanyName"
                                                  IncrementalFilteringMode="StartsWith">
                                                </PropertiesComboBox>
                                                <Settings SortMode="DisplayText"></Settings>
                        </dx:GridViewDataComboBoxColumn>--%>
                        <dx:GridViewDataTextColumn Caption="Company Name" name="CompanyName" FieldName="CompanyName" VisibleIndex="7"
                                ReadOnly="false" Visible="True" >                                
                                <Settings SortMode="DisplayText" AllowHeaderFilter="True"></Settings>
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataComboBoxColumn FieldName="Site" VisibleIndex="8" Caption="Site">
                                                <PropertiesComboBox DataSourceID="SitesDtSource" TextField="SiteNameEbi"
                                                     IncrementalFilteringMode="StartsWith">
                                                </PropertiesComboBox>
                                                <Settings SortMode="DisplayText"></Settings>
                        </dx:GridViewDataComboBoxColumn>

                        <dx:GridViewDataCheckColumn FieldName="Doubt" Caption="Doubt" VisibleIndex="9" >
                        </dx:GridViewDataCheckColumn>
                        <dx:GridViewDataTextColumn Caption="EBI Card #" FieldName="AccessCardNo" ShowInCustomizationForm="True" VisibleIndex="10" Visible="False">
                            <settings sortmode="DisplayText" />
                        </dx:GridViewDataTextColumn>
                    </Columns>
                    <SettingsPager>
                            <AllButton Visible="True">
                            </AllButton>
                        </SettingsPager>
                        <SettingsCustomizationWindow Enabled="True" />
                        </dx:ASPxGridView>

                        <div align="right">
                        <uc1:ExportButtons ID="ExportButtons4" runat="server" />
                        </div>
                        <asp:SqlDataSource ID="EbiHoursWorkedDS" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
                        SelectCommand="_EbiHoursWorked_ByYearMonthCompanyIdSiteId_2" SelectCommandType="StoredProcedure">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="cmbHourWorkedYear" Name="Year" PropertyName="Value" Type="Int32" />
                            <asp:ControlParameter ControlID="cmbHourWorkedMonth" Name="Month" PropertyName="Value" Type="Int32" />
                            <asp:ControlParameter ControlID="cmbHourWorkedCompany" Name="CompanyId" PropertyName="Value" Type="Int32" />
                            <asp:ControlParameter ControlID="cmbHourWorkedSite" Name="SiteId" PropertyName="Value" Type="Int32" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                   


                </dx:ContentControl>
            </ContentCollection>
          </dx:TabPage>

         
          <dx:TabPage Text="Company KPI vs EBI hours">
            <ContentCollection>
                <dx:ContentControl runat="server">

                <table style="width:100%; text-align:left; vertical-align:top">
                <tr>
                    <td style="width:30%; text-align:left; vertical-align:top">
                      <div style="float:left; padding-top:4px"><b>Company:&nbsp;</b></div>
                      <div style="float:left"><dx:ASPxComboBox ID="cmbKPECompany"
                       ClientInstanceName="cmbCompanies2" runat="server"
                        CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                        CssPostfix="Office2003Blue" EnableSynchronization="False" 
                         SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                          OnSelectedIndexChanged="cmbKPECompany_SelectedIndexChanged"
                        ValueType="System.Int32" IncrementalFilteringMode="StartsWith">
                        <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                        </LoadingPanelImage>
                        <ButtonStyle Width="13px">
                        </ButtonStyle>
                        <ClientSideEvents SelectedIndexChanged="function(s, e) {
	                                                                    cmbSites2.PerformCallback(s.GetValue());
                                                                    }" />
                    </dx:ASPxComboBox></div>
                    </td>
                    <td style="width:30%; text-align:left; vertical-align:top">
                        <div style="float:left; padding-top:4px"><b>Site/Region:&nbsp;</b></div> 
                      <div style="float:left"><dx:ASPxComboBox ID="cmbKPESite"  ClientInstanceName="cmbSites2"
                       runat="server" EnableSynchronization="False" OnCallback="cmbSites2_Callback"
                       CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" IncrementalFilteringMode="StartsWith"
                        CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                        ValueType="System.Int32">
                        <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                        </LoadingPanelImage>
                        <ButtonStyle Width="13px">
                        </ButtonStyle>
                    </dx:ASPxComboBox></div>
                    </td>
                    <td style="width:30%; text-align:left; vertical-align:top">
                         <div style="float:left; padding-top:4px"><b>Month/Year:&nbsp;</b></div>
                      <div style="float:left"><dx:ASPxComboBox ID="cmbKPEMonth" Width="70px" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                        CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                        ValueType="System.String">
                        <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                        </LoadingPanelImage>
                        <ButtonStyle Width="13px">
                        </ButtonStyle>
                    </dx:ASPxComboBox></div>
                    <div style="float:left; padding-left:5px"><dx:ASPxComboBox ID="cmbKPEYear" Width="55px" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                        CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                        ValueType="System.String">
                        <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                        </LoadingPanelImage>
                        <ButtonStyle Width="13px">
                        </ButtonStyle>
                    </dx:ASPxComboBox></div>
                    </td>
                    <td style="width:10%; text-align:left; vertical-align:top">
                         <dx:ASPxButton ID="btnKPE" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                    CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                    Text="Go/Refresh" OnClick="btnKPE_Click">
                </dx:ASPxButton>
                    </td>
                </tr>
            </table>

                <br />
                    <dx:ASPxGridView width="100%" ID="gridKPE_ByMonthYear" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue" AutoGenerateColumns="False" Settings-ShowGroupPanel="true"  OnCustomUnboundColumnData="grid_CustomUnboundColumnData" ClientInstanceName="grid5"  DataSourceId="KpiVsEbiDS">
                    <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                        <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
                        </LoadingPanelOnStatusBar>
                        <LoadingPanel Url="~/App_Themes/Office2003BlueOld/GridView/Loading.gif"><%--Enhancement_023:change by debashis for testing platform upgradation--%>
                        </LoadingPanel>
                    </Images>
                    <ImagesFilterControl>
                        <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
                        </LoadingPanel>
                    </ImagesFilterControl>
                    <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue">
                        <Header ImageSpacing="5px" SortingImageSpacing="5px">
                        </Header>
                        <AlternatingRow Enabled="True">
                    </AlternatingRow>
                        <LoadingPanel ImageSpacing="10px">
                        </LoadingPanel>
                    </Styles>
                    <StylesEditors>
                        <ProgressBar Height="25px">
                        </ProgressBar>
                    </StylesEditors>
                    <Settings ShowFilterRow="true" ShowFooter="True" ShowFilterBar="Visible" ShowGroupPanel="True" />
                    
                    <Columns>
                        
                       <%--<dx:GridViewDataTextColumn FieldName="Vendor_Name" VisibleIndex="0" Caption="Vendor Name">
                                                
                                                <Settings SortMode="DisplayText"></Settings>
                          </dx:GridViewDataTextColumn>--%>
                          <dx:GridViewDataComboBoxColumn FieldName="CompanyId" VisibleIndex="0" Caption="Company Name">
                                                <PropertiesComboBox DataSourceID="CompaniesDataSource2" TextField="CompanyName"
                                                 ValueField="CompanyId" IncrementalFilteringMode="StartsWith" ValueType="System.Int32">
                                                </PropertiesComboBox>
                                                <Settings SortMode="DisplayText"></Settings>
                        </dx:GridViewDataComboBoxColumn>
                          <dx:GridViewDataComboBoxColumn FieldName="SiteId" VisibleIndex="1" Caption="Site">
                                                <PropertiesComboBox DataSourceID="SitesDt" TextField="SiteName"
                                                    ValueField="SiteId" ValueType="System.Int32"
                                                     IncrementalFilteringMode="StartsWith">
                                                </PropertiesComboBox>
                                               
                        </dx:GridViewDataComboBoxColumn>

                         <dx:GridViewDataTextColumn FieldName="KpiHours" VisibleIndex="2" Caption="KPI Hours">
                                     
                        </dx:GridViewDataTextColumn>

                        <dx:GridViewDataTextColumn FieldName="EbiHours" VisibleIndex="3" Caption="EBI Hours">
                                     
                        </dx:GridViewDataTextColumn>

                        <dx:GridViewDataTextColumn FieldName="Doubts" VisibleIndex="4" Caption="Doubt(#)"  UnboundType="Decimal">
                                     
                        </dx:GridViewDataTextColumn>

                        <dx:GridViewDataTextColumn FieldName="TotalSwipes" Caption="Total Swipes" Visible="false">
                                     
                        </dx:GridViewDataTextColumn>
                        
                        <dx:GridViewDataColumn FieldName="DoubtPer" Caption="Doubt(%)" VisibleIndex="5" UnboundType="Decimal">
                               
                            
                        </dx:GridViewDataColumn>
                        
                        <dx:GridViewDataTextColumn FieldName="Variance" Caption="Variance (Hours)" VisibleIndex="6" UnboundType="Decimal">
                                     
                        </dx:GridViewDataTextColumn>

                         <dx:GridViewDataColumn FieldName="VariancePer" VisibleIndex="7" Caption="Variance (%)" UnboundType="Decimal">
                                   
                        
                        </dx:GridViewDataColumn>
                    </Columns>
                    <TotalSummary>
                        <dx:ASPxSummaryItem FieldName="KpiHours" SummaryType="Sum" />
                        <dx:ASPxSummaryItem FieldName="EbiHours" SummaryType="Sum" />
                        <dx:ASPxSummaryItem FieldName="Doubts" SummaryType="Sum" />                        
                    </TotalSummary>
                    <SettingsPager>
                            <AllButton Visible="True">
                            </AllButton>
                        </SettingsPager>
                        </dx:ASPxGridView>
                    <div align="right">
                        <uc1:ExportButtons ID="ExportButtons5" runat="server" />
                        </div>                    
                    <asp:SqlDataSource ID="KpiVsEbiDS" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
                        SelectCommand="_kpiHoursvsEbiHours_ByYearMonthCompanyIdSiteId" SelectCommandType="StoredProcedure">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="cmbKPEYear" Name="Year" PropertyName="Value" Type="Int32" />
                            <asp:ControlParameter ControlID="cmbKPEMonth" Name="Month" PropertyName="Value" Type="Int32" />
                            <asp:ControlParameter ControlID="cmbKPECompany" Name="CompanyId" PropertyName="Value" Type="Int32" />
                            <asp:ControlParameter ControlID="cmbKPESite" Name="SiteId" PropertyName="Value" Type="Int32" />
                        </SelectParameters>
                    </asp:SqlDataSource>


                </dx:ContentControl>
            </ContentCollection>
          </dx:TabPage>
    
</tabpages>

    <loadingpanelstyle imagespacing="6px"></loadingpanelstyle>
</dx:ASPxPageControl>

<data:CompaniesDataSource ID="CompaniesDataSource1" runat="server"
    SelectMethod="GetPaged"
    EnablePaging="True"
    EnableDeepLoad="True"
    Sort="CompanyName ASC">
    <DeepLoadProperties Method="IncludeChildren" Recursive="False">
        <Types>
            <data:CompaniesProperty Name="Users" />
        </Types>
    </DeepLoadProperties>
</data:CompaniesDataSource>
<data:SitesDataSource ID="dsSitesFilter" runat="server" Filter="Editable = True" Sort="SiteName ASC"></data:SitesDataSource>
<data:CompaniesDataSource ID="CompaniesDataSource2" runat="server" Sort="CompanyName Asc"></data:CompaniesDataSource>
<data:UsersFullNameDataSource ID="UsersFullNameDataSource1" runat="server" Sort="UserFullName ASC"></data:UsersFullNameDataSource>
<data:CompanyStatusDataSource ID="CompanyStatusDataSource1" runat="server" Sort="CompanyStatusDesc ASC"></data:CompanyStatusDataSource>
<data:SitesDataSource ID="SitesDataSource1" runat="server" Sort="SiteName ASC"></data:SitesDataSource>
<data:SitesDataSource ID="SitesDataSource2" runat="server" Sort="SiteName ASC" Filter="SiteName != 'Australia GPP'"></data:SitesDataSource>
<%--Enhancement_023:change by debashis for testing platform upgradation--%>
<data:CompaniesDataSource ID="CompaniesDataSource3" runat="server" Sort="CompanyName ASC"></data:CompaniesDataSource>
<%--Enhancement_023:change by debashis for testing platform upgradation--%>
<asp:SqlDataSource ID="CompaniesDataSource4" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand=" SELECT TOP 1 '' AS CompanyNameEbi,'0' AS CompanyId FROM dbo.Companies
UNION
SELECT CompanyNameEbi, CompanyId FROM dbo.Companies
WHERE CompanyNameEbi IS NOT NULL
ORDER BY CompanyNameEbi"></asp:SqlDataSource>
<asp:SqlDataSource ID="SitesDtSource" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="SELECT TOP 1 '' AS SiteNameEbi,'0' AS SiteId FROM dbo.Sites
UNION
SELECT SiteNameEbi, SiteId FROM dbo.Sites
WHERE SiteNameEbi IS NOT NULL
ORDER BY SiteNameEbi"></asp:SqlDataSource>
<asp:SqlDataSource ID="SiteNameEbiDS" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="Select '-' as SiteNameEbi Union Select SiteNameEbi from Sites where SiteNameEbi is Not Null order by SiteNameEbi"></asp:SqlDataSource>
<asp:SqlDataSource ID="CompanyNameDS" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="select ' ' as CompanyName
Union
Select CompanyName from Companies order by CompanyName"></asp:SqlDataSource>
<asp:SqlDataSource ID="SitesDt" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="SELECT SiteName, SiteId FROM dbo.Sites
WHERE IsVisible='true'
ORDER BY SiteName"></asp:SqlDataSource>
