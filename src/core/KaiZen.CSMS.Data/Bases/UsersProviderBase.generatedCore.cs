﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="UsersProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class UsersProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.Users, KaiZen.CSMS.Entities.UsersKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.UsersKey key)
		{
			return Delete(transactionManager, key.UserId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_userId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _userId)
		{
			return Delete(null, _userId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_userId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _userId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_User_User_ModifiedByUserId key.
		///		FK_User_User_ModifiedByUserId Description: 
		/// </summary>
		/// <param name="_modifiedByUserId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Users objects.</returns>
		public TList<Users> GetByModifiedByUserId(System.Int32 _modifiedByUserId)
		{
			int count = -1;
			return GetByModifiedByUserId(_modifiedByUserId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_User_User_ModifiedByUserId key.
		///		FK_User_User_ModifiedByUserId Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_modifiedByUserId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Users objects.</returns>
		/// <remarks></remarks>
		public TList<Users> GetByModifiedByUserId(TransactionManager transactionManager, System.Int32 _modifiedByUserId)
		{
			int count = -1;
			return GetByModifiedByUserId(transactionManager, _modifiedByUserId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_User_User_ModifiedByUserId key.
		///		FK_User_User_ModifiedByUserId Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_modifiedByUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Users objects.</returns>
		public TList<Users> GetByModifiedByUserId(TransactionManager transactionManager, System.Int32 _modifiedByUserId, int start, int pageLength)
		{
			int count = -1;
			return GetByModifiedByUserId(transactionManager, _modifiedByUserId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_User_User_ModifiedByUserId key.
		///		fkUserUserModifiedByUserId Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_modifiedByUserId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Users objects.</returns>
		public TList<Users> GetByModifiedByUserId(System.Int32 _modifiedByUserId, int start, int pageLength)
		{
			int count =  -1;
			return GetByModifiedByUserId(null, _modifiedByUserId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_User_User_ModifiedByUserId key.
		///		fkUserUserModifiedByUserId Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_modifiedByUserId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Users objects.</returns>
		public TList<Users> GetByModifiedByUserId(System.Int32 _modifiedByUserId, int start, int pageLength,out int count)
		{
			return GetByModifiedByUserId(null, _modifiedByUserId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_User_User_ModifiedByUserId key.
		///		FK_User_User_ModifiedByUserId Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_modifiedByUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Users objects.</returns>
		public abstract TList<Users> GetByModifiedByUserId(TransactionManager transactionManager, System.Int32 _modifiedByUserId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Users_Role key.
		///		FK_Users_Role Description: 
		/// </summary>
		/// <param name="_roleId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Users objects.</returns>
		public TList<Users> GetByRoleId(System.Int32 _roleId)
		{
			int count = -1;
			return GetByRoleId(_roleId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Users_Role key.
		///		FK_Users_Role Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_roleId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Users objects.</returns>
		/// <remarks></remarks>
		public TList<Users> GetByRoleId(TransactionManager transactionManager, System.Int32 _roleId)
		{
			int count = -1;
			return GetByRoleId(transactionManager, _roleId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_Users_Role key.
		///		FK_Users_Role Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_roleId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Users objects.</returns>
		public TList<Users> GetByRoleId(TransactionManager transactionManager, System.Int32 _roleId, int start, int pageLength)
		{
			int count = -1;
			return GetByRoleId(transactionManager, _roleId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Users_Role key.
		///		fkUsersRole Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_roleId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Users objects.</returns>
		public TList<Users> GetByRoleId(System.Int32 _roleId, int start, int pageLength)
		{
			int count =  -1;
			return GetByRoleId(null, _roleId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Users_Role key.
		///		fkUsersRole Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_roleId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Users objects.</returns>
		public TList<Users> GetByRoleId(System.Int32 _roleId, int start, int pageLength,out int count)
		{
			return GetByRoleId(null, _roleId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Users_Role key.
		///		FK_Users_Role Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_roleId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Users objects.</returns>
		public abstract TList<Users> GetByRoleId(TransactionManager transactionManager, System.Int32 _roleId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Users_Companies key.
		///		FK_Users_Companies Description: 
		/// </summary>
		/// <param name="_companyId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Users objects.</returns>
		public TList<Users> GetByCompanyId(System.Int32 _companyId)
		{
			int count = -1;
			return GetByCompanyId(_companyId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Users_Companies key.
		///		FK_Users_Companies Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Users objects.</returns>
		/// <remarks></remarks>
		public TList<Users> GetByCompanyId(TransactionManager transactionManager, System.Int32 _companyId)
		{
			int count = -1;
			return GetByCompanyId(transactionManager, _companyId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_Users_Companies key.
		///		FK_Users_Companies Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Users objects.</returns>
		public TList<Users> GetByCompanyId(TransactionManager transactionManager, System.Int32 _companyId, int start, int pageLength)
		{
			int count = -1;
			return GetByCompanyId(transactionManager, _companyId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Users_Companies key.
		///		fkUsersCompanies Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_companyId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Users objects.</returns>
		public TList<Users> GetByCompanyId(System.Int32 _companyId, int start, int pageLength)
		{
			int count =  -1;
			return GetByCompanyId(null, _companyId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Users_Companies key.
		///		fkUsersCompanies Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_companyId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Users objects.</returns>
		public TList<Users> GetByCompanyId(System.Int32 _companyId, int start, int pageLength,out int count)
		{
			return GetByCompanyId(null, _companyId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Users_Companies key.
		///		FK_Users_Companies Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Users objects.</returns>
		public abstract TList<Users> GetByCompanyId(TransactionManager transactionManager, System.Int32 _companyId, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.Users Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.UsersKey key, int start, int pageLength)
		{
			return GetByUserId(transactionManager, key.UserId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_Users index.
		/// </summary>
		/// <param name="_userId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Users"/> class.</returns>
		public KaiZen.CSMS.Entities.Users GetByUserId(System.Int32 _userId)
		{
			int count = -1;
			return GetByUserId(null,_userId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Users index.
		/// </summary>
		/// <param name="_userId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Users"/> class.</returns>
		public KaiZen.CSMS.Entities.Users GetByUserId(System.Int32 _userId, int start, int pageLength)
		{
			int count = -1;
			return GetByUserId(null, _userId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Users index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_userId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Users"/> class.</returns>
		public KaiZen.CSMS.Entities.Users GetByUserId(TransactionManager transactionManager, System.Int32 _userId)
		{
			int count = -1;
			return GetByUserId(transactionManager, _userId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Users index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_userId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Users"/> class.</returns>
		public KaiZen.CSMS.Entities.Users GetByUserId(TransactionManager transactionManager, System.Int32 _userId, int start, int pageLength)
		{
			int count = -1;
			return GetByUserId(transactionManager, _userId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Users index.
		/// </summary>
		/// <param name="_userId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Users"/> class.</returns>
		public KaiZen.CSMS.Entities.Users GetByUserId(System.Int32 _userId, int start, int pageLength, out int count)
		{
			return GetByUserId(null, _userId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Users index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_userId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Users"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.Users GetByUserId(TransactionManager transactionManager, System.Int32 _userId, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_Email index.
		/// </summary>
		/// <param name="_email"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Users&gt;"/> class.</returns>
		public TList<Users> GetByEmail(System.String _email)
		{
			int count = -1;
			return GetByEmail(null,_email, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Email index.
		/// </summary>
		/// <param name="_email"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Users&gt;"/> class.</returns>
		public TList<Users> GetByEmail(System.String _email, int start, int pageLength)
		{
			int count = -1;
			return GetByEmail(null, _email, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Email index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_email"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Users&gt;"/> class.</returns>
		public TList<Users> GetByEmail(TransactionManager transactionManager, System.String _email)
		{
			int count = -1;
			return GetByEmail(transactionManager, _email, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Email index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_email"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Users&gt;"/> class.</returns>
		public TList<Users> GetByEmail(TransactionManager transactionManager, System.String _email, int start, int pageLength)
		{
			int count = -1;
			return GetByEmail(transactionManager, _email, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Email index.
		/// </summary>
		/// <param name="_email"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Users&gt;"/> class.</returns>
		public TList<Users> GetByEmail(System.String _email, int start, int pageLength, out int count)
		{
			return GetByEmail(null, _email, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Email index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_email"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Users&gt;"/> class.</returns>
		public abstract TList<Users> GetByEmail(TransactionManager transactionManager, System.String _email, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_UserLogon index.
		/// </summary>
		/// <param name="_userLogon"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Users"/> class.</returns>
		public KaiZen.CSMS.Entities.Users GetByUserLogon(System.String _userLogon)
		{
			int count = -1;
			return GetByUserLogon(null,_userLogon, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_UserLogon index.
		/// </summary>
		/// <param name="_userLogon"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Users"/> class.</returns>
		public KaiZen.CSMS.Entities.Users GetByUserLogon(System.String _userLogon, int start, int pageLength)
		{
			int count = -1;
			return GetByUserLogon(null, _userLogon, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_UserLogon index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_userLogon"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Users"/> class.</returns>
		public KaiZen.CSMS.Entities.Users GetByUserLogon(TransactionManager transactionManager, System.String _userLogon)
		{
			int count = -1;
			return GetByUserLogon(transactionManager, _userLogon, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_UserLogon index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_userLogon"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Users"/> class.</returns>
		public KaiZen.CSMS.Entities.Users GetByUserLogon(TransactionManager transactionManager, System.String _userLogon, int start, int pageLength)
		{
			int count = -1;
			return GetByUserLogon(transactionManager, _userLogon, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_UserLogon index.
		/// </summary>
		/// <param name="_userLogon"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Users"/> class.</returns>
		public KaiZen.CSMS.Entities.Users GetByUserLogon(System.String _userLogon, int start, int pageLength, out int count)
		{
			return GetByUserLogon(null, _userLogon, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_UserLogon index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_userLogon"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Users"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.Users GetByUserLogon(TransactionManager transactionManager, System.String _userLogon, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#region _Users_FullName 
		
		/// <summary>
		///	This method wrap the '_Users_FullName' stored procedure. 
		/// </summary>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet FullName()
		{
			return FullName(null, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_Users_FullName' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet FullName(int start, int pageLength)
		{
			return FullName(null, start, pageLength );
		}
				
		/// <summary>
		///	This method wrap the '_Users_FullName' stored procedure. 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet FullName(TransactionManager transactionManager)
		{
			return FullName(transactionManager, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_Users_FullName' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet FullName(TransactionManager transactionManager, int start, int pageLength );
		
		#endregion
		
		#region _Users_WHA_GetByCompanyId 
		
		/// <summary>
		///	This method wrap the '_Users_WHA_GetByCompanyId' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet WHA_GetByCompanyId(System.Int32? companyId)
		{
			return WHA_GetByCompanyId(null, 0, int.MaxValue , companyId);
		}
		
		/// <summary>
		///	This method wrap the '_Users_WHA_GetByCompanyId' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet WHA_GetByCompanyId(int start, int pageLength, System.Int32? companyId)
		{
			return WHA_GetByCompanyId(null, start, pageLength , companyId);
		}
				
		/// <summary>
		///	This method wrap the '_Users_WHA_GetByCompanyId' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet WHA_GetByCompanyId(TransactionManager transactionManager, System.Int32? companyId)
		{
			return WHA_GetByCompanyId(transactionManager, 0, int.MaxValue , companyId);
		}
		
		/// <summary>
		///	This method wrap the '_Users_WHA_GetByCompanyId' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet WHA_GetByCompanyId(TransactionManager transactionManager, int start, int pageLength , System.Int32? companyId);
		
		#endregion
		
		#region _Users_GetCustom 
		
		/// <summary>
		///	This method wrap the '_Users_GetCustom' stored procedure. 
		/// </summary>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetCustom()
		{
			return GetCustom(null, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_Users_GetCustom' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetCustom(int start, int pageLength)
		{
			return GetCustom(null, start, pageLength );
		}
				
		/// <summary>
		///	This method wrap the '_Users_GetCustom' stored procedure. 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetCustom(TransactionManager transactionManager)
		{
			return GetCustom(transactionManager, 0, int.MaxValue );
		}


		
		/// <summary>
		///	This method wrap the '_Users_GetCustom' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet GetCustom(TransactionManager transactionManager, int start, int pageLength );
		
		#endregion      

		
		#region _Users_WHA_GetAll 
		
		/// <summary>
		///	This method wrap the '_Users_WHA_GetAll' stored procedure. 
		/// </summary>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet WHA_GetAll()
		{
			return WHA_GetAll(null, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_Users_WHA_GetAll' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet WHA_GetAll(int start, int pageLength)
		{
			return WHA_GetAll(null, start, pageLength );
		}
				
		/// <summary>
		///	This method wrap the '_Users_WHA_GetAll' stored procedure. 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet WHA_GetAll(TransactionManager transactionManager)
		{
			return WHA_GetAll(transactionManager, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_Users_WHA_GetAll' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet WHA_GetAll(TransactionManager transactionManager, int start, int pageLength );
		
		#endregion
		
		#region _Users_GetUserLogon_Asc 
		
		/// <summary>
		///	This method wrap the '_Users_GetUserLogon_Asc' stored procedure. 
		/// </summary>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetUserLogon_Asc()
		{
			return GetUserLogon_Asc(null, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_Users_GetUserLogon_Asc' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetUserLogon_Asc(int start, int pageLength)
		{
			return GetUserLogon_Asc(null, start, pageLength );
		}
				
		/// <summary>
		///	This method wrap the '_Users_GetUserLogon_Asc' stored procedure. 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetUserLogon_Asc(TransactionManager transactionManager)
		{
			return GetUserLogon_Asc(transactionManager, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_Users_GetUserLogon_Asc' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet GetUserLogon_Asc(TransactionManager transactionManager, int start, int pageLength );
		
		#endregion
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;Users&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;Users&gt;"/></returns>
		public static TList<Users> Fill(IDataReader reader, TList<Users> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.Users c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("Users")
					.Append("|").Append((System.Int32)reader[((int)UsersColumn.UserId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<Users>(
					key.ToString(), // EntityTrackingKey
					"Users",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.Users();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.UserId = (System.Int32)reader[((int)UsersColumn.UserId - 1)];
					c.UserLogon = (System.String)reader[((int)UsersColumn.UserLogon - 1)];
					c.LastName = (System.String)reader[((int)UsersColumn.LastName - 1)];
					c.FirstName = (System.String)reader[((int)UsersColumn.FirstName - 1)];
					c.CompanyId = (System.Int32)reader[((int)UsersColumn.CompanyId - 1)];
					c.Title = (reader.IsDBNull(((int)UsersColumn.Title - 1)))?null:(System.String)reader[((int)UsersColumn.Title - 1)];
					c.PhoneNo = (reader.IsDBNull(((int)UsersColumn.PhoneNo - 1)))?null:(System.String)reader[((int)UsersColumn.PhoneNo - 1)];
					c.FaxNo = (reader.IsDBNull(((int)UsersColumn.FaxNo - 1)))?null:(System.String)reader[((int)UsersColumn.FaxNo - 1)];
					c.MobNo = (reader.IsDBNull(((int)UsersColumn.MobNo - 1)))?null:(System.String)reader[((int)UsersColumn.MobNo - 1)];
					c.Email = (System.String)reader[((int)UsersColumn.Email - 1)];
					c.RoleId = (System.Int32)reader[((int)UsersColumn.RoleId - 1)];
					c.ModifiedByUserId = (System.Int32)reader[((int)UsersColumn.ModifiedByUserId - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.Users"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.Users"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.Users entity)
		{
			if (!reader.Read()) return;
			
			entity.UserId = (System.Int32)reader[((int)UsersColumn.UserId - 1)];
			entity.UserLogon = (System.String)reader[((int)UsersColumn.UserLogon - 1)];
			entity.LastName = (System.String)reader[((int)UsersColumn.LastName - 1)];
			entity.FirstName = (System.String)reader[((int)UsersColumn.FirstName - 1)];
			entity.CompanyId = (System.Int32)reader[((int)UsersColumn.CompanyId - 1)];
			entity.Title = (reader.IsDBNull(((int)UsersColumn.Title - 1)))?null:(System.String)reader[((int)UsersColumn.Title - 1)];
			entity.PhoneNo = (reader.IsDBNull(((int)UsersColumn.PhoneNo - 1)))?null:(System.String)reader[((int)UsersColumn.PhoneNo - 1)];
			entity.FaxNo = (reader.IsDBNull(((int)UsersColumn.FaxNo - 1)))?null:(System.String)reader[((int)UsersColumn.FaxNo - 1)];
			entity.MobNo = (reader.IsDBNull(((int)UsersColumn.MobNo - 1)))?null:(System.String)reader[((int)UsersColumn.MobNo - 1)];
			entity.Email = (System.String)reader[((int)UsersColumn.Email - 1)];
			entity.RoleId = (System.Int32)reader[((int)UsersColumn.RoleId - 1)];
			entity.ModifiedByUserId = (System.Int32)reader[((int)UsersColumn.ModifiedByUserId - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.Users"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.Users"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.Users entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.UserId = (System.Int32)dataRow["UserId"];
			entity.UserLogon = (System.String)dataRow["UserLogon"];
			entity.LastName = (System.String)dataRow["LastName"];
			entity.FirstName = (System.String)dataRow["FirstName"];
			entity.CompanyId = (System.Int32)dataRow["CompanyId"];
			entity.Title = Convert.IsDBNull(dataRow["Title"]) ? null : (System.String)dataRow["Title"];
			entity.PhoneNo = Convert.IsDBNull(dataRow["PhoneNo"]) ? null : (System.String)dataRow["PhoneNo"];
			entity.FaxNo = Convert.IsDBNull(dataRow["FaxNo"]) ? null : (System.String)dataRow["FaxNo"];
			entity.MobNo = Convert.IsDBNull(dataRow["MobNo"]) ? null : (System.String)dataRow["MobNo"];
			entity.Email = (System.String)dataRow["Email"];
			entity.RoleId = (System.Int32)dataRow["RoleId"];
			entity.ModifiedByUserId = (System.Int32)dataRow["ModifiedByUserId"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.Users"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.Users Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.Users entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region ModifiedByUserIdSource	
			if (CanDeepLoad(entity, "Users|ModifiedByUserIdSource", deepLoadType, innerList) 
				&& entity.ModifiedByUserIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.ModifiedByUserId;
				Users tmpEntity = EntityManager.LocateEntity<Users>(EntityLocator.ConstructKeyFromPkItems(typeof(Users), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.ModifiedByUserIdSource = tmpEntity;
				else
					entity.ModifiedByUserIdSource = DataRepository.UsersProvider.GetByUserId(transactionManager, entity.ModifiedByUserId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ModifiedByUserIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.ModifiedByUserIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.UsersProvider.DeepLoad(transactionManager, entity.ModifiedByUserIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion ModifiedByUserIdSource

			#region RoleIdSource	
			if (CanDeepLoad(entity, "Role|RoleIdSource", deepLoadType, innerList) 
				&& entity.RoleIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.RoleId;
				Role tmpEntity = EntityManager.LocateEntity<Role>(EntityLocator.ConstructKeyFromPkItems(typeof(Role), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.RoleIdSource = tmpEntity;
				else
					entity.RoleIdSource = DataRepository.RoleProvider.GetByRoleId(transactionManager, entity.RoleId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'RoleIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.RoleIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.RoleProvider.DeepLoad(transactionManager, entity.RoleIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion RoleIdSource

			#region CompanyIdSource	
			if (CanDeepLoad(entity, "Companies|CompanyIdSource", deepLoadType, innerList) 
				&& entity.CompanyIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.CompanyId;
				Companies tmpEntity = EntityManager.LocateEntity<Companies>(EntityLocator.ConstructKeyFromPkItems(typeof(Companies), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.CompanyIdSource = tmpEntity;
				else
					entity.CompanyIdSource = DataRepository.CompaniesProvider.GetByCompanyId(transactionManager, entity.CompanyId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CompanyIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.CompanyIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.CompaniesProvider.DeepLoad(transactionManager, entity.CompanyIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion CompanyIdSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetByUserId methods when available
			
			#region CompanySiteCategoryStandardCollectionGetByApprovedByUserId
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<CompanySiteCategoryStandard>|CompanySiteCategoryStandardCollectionGetByApprovedByUserId", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CompanySiteCategoryStandardCollectionGetByApprovedByUserId' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.CompanySiteCategoryStandardCollectionGetByApprovedByUserId = DataRepository.CompanySiteCategoryStandardProvider.GetByApprovedByUserId(transactionManager, entity.UserId);

				if (deep && entity.CompanySiteCategoryStandardCollectionGetByApprovedByUserId.Count > 0)
				{
					deepHandles.Add("CompanySiteCategoryStandardCollectionGetByApprovedByUserId",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<CompanySiteCategoryStandard>) DataRepository.CompanySiteCategoryStandardProvider.DeepLoad,
						new object[] { transactionManager, entity.CompanySiteCategoryStandardCollectionGetByApprovedByUserId, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region QuestionnaireServicesSelectedCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<QuestionnaireServicesSelected>|QuestionnaireServicesSelectedCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'QuestionnaireServicesSelectedCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.QuestionnaireServicesSelectedCollection = DataRepository.QuestionnaireServicesSelectedProvider.GetByModifiedByUserId(transactionManager, entity.UserId);

				if (deep && entity.QuestionnaireServicesSelectedCollection.Count > 0)
				{
					deepHandles.Add("QuestionnaireServicesSelectedCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<QuestionnaireServicesSelected>) DataRepository.QuestionnaireServicesSelectedProvider.DeepLoad,
						new object[] { transactionManager, entity.QuestionnaireServicesSelectedCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region QuestionnaireMainAssessmentCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<QuestionnaireMainAssessment>|QuestionnaireMainAssessmentCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'QuestionnaireMainAssessmentCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.QuestionnaireMainAssessmentCollection = DataRepository.QuestionnaireMainAssessmentProvider.GetByModifiedByUserId(transactionManager, entity.UserId);

				if (deep && entity.QuestionnaireMainAssessmentCollection.Count > 0)
				{
					deepHandles.Add("QuestionnaireMainAssessmentCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<QuestionnaireMainAssessment>) DataRepository.QuestionnaireMainAssessmentProvider.DeepLoad,
						new object[] { transactionManager, entity.QuestionnaireMainAssessmentCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region ConfigSa812Collection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<ConfigSa812>|ConfigSa812Collection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ConfigSa812Collection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.ConfigSa812Collection = DataRepository.ConfigSa812Provider.GetBySupervisorUserId(transactionManager, entity.UserId);

				if (deep && entity.ConfigSa812Collection.Count > 0)
				{
					deepHandles.Add("ConfigSa812Collection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<ConfigSa812>) DataRepository.ConfigSa812Provider.DeepLoad,
						new object[] { transactionManager, entity.ConfigSa812Collection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region QuestionnaireCollectionGetByInitialModifiedByUserId
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<Questionnaire>|QuestionnaireCollectionGetByInitialModifiedByUserId", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'QuestionnaireCollectionGetByInitialModifiedByUserId' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.QuestionnaireCollectionGetByInitialModifiedByUserId = DataRepository.QuestionnaireProvider.GetByInitialModifiedByUserId(transactionManager, entity.UserId);

				if (deep && entity.QuestionnaireCollectionGetByInitialModifiedByUserId.Count > 0)
				{
					deepHandles.Add("QuestionnaireCollectionGetByInitialModifiedByUserId",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<Questionnaire>) DataRepository.QuestionnaireProvider.DeepLoad,
						new object[] { transactionManager, entity.QuestionnaireCollectionGetByInitialModifiedByUserId, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region CompanyStatusChangeApprovalCollectionGetByRequestedByUserId
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<CompanyStatusChangeApproval>|CompanyStatusChangeApprovalCollectionGetByRequestedByUserId", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CompanyStatusChangeApprovalCollectionGetByRequestedByUserId' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.CompanyStatusChangeApprovalCollectionGetByRequestedByUserId = DataRepository.CompanyStatusChangeApprovalProvider.GetByRequestedByUserId(transactionManager, entity.UserId);

				if (deep && entity.CompanyStatusChangeApprovalCollectionGetByRequestedByUserId.Count > 0)
				{
					deepHandles.Add("CompanyStatusChangeApprovalCollectionGetByRequestedByUserId",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<CompanyStatusChangeApproval>) DataRepository.CompanyStatusChangeApprovalProvider.DeepLoad,
						new object[] { transactionManager, entity.CompanyStatusChangeApprovalCollectionGetByRequestedByUserId, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region QuestionnaireActionLogCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<QuestionnaireActionLog>|QuestionnaireActionLogCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'QuestionnaireActionLogCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.QuestionnaireActionLogCollection = DataRepository.QuestionnaireActionLogProvider.GetByCreatedByUserId(transactionManager, entity.UserId);

				if (deep && entity.QuestionnaireActionLogCollection.Count > 0)
				{
					deepHandles.Add("QuestionnaireActionLogCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<QuestionnaireActionLog>) DataRepository.QuestionnaireActionLogProvider.DeepLoad,
						new object[] { transactionManager, entity.QuestionnaireActionLogCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region QuestionnaireInitialContactCollectionGetByCreatedByUserId
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<QuestionnaireInitialContact>|QuestionnaireInitialContactCollectionGetByCreatedByUserId", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'QuestionnaireInitialContactCollectionGetByCreatedByUserId' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.QuestionnaireInitialContactCollectionGetByCreatedByUserId = DataRepository.QuestionnaireInitialContactProvider.GetByCreatedByUserId(transactionManager, entity.UserId);

				if (deep && entity.QuestionnaireInitialContactCollectionGetByCreatedByUserId.Count > 0)
				{
					deepHandles.Add("QuestionnaireInitialContactCollectionGetByCreatedByUserId",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<QuestionnaireInitialContact>) DataRepository.QuestionnaireInitialContactProvider.DeepLoad,
						new object[] { transactionManager, entity.QuestionnaireInitialContactCollectionGetByCreatedByUserId, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region QuestionnaireCollectionGetByVerificationModifiedByUserId
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<Questionnaire>|QuestionnaireCollectionGetByVerificationModifiedByUserId", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'QuestionnaireCollectionGetByVerificationModifiedByUserId' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.QuestionnaireCollectionGetByVerificationModifiedByUserId = DataRepository.QuestionnaireProvider.GetByVerificationModifiedByUserId(transactionManager, entity.UserId);

				if (deep && entity.QuestionnaireCollectionGetByVerificationModifiedByUserId.Count > 0)
				{
					deepHandles.Add("QuestionnaireCollectionGetByVerificationModifiedByUserId",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<Questionnaire>) DataRepository.QuestionnaireProvider.DeepLoad,
						new object[] { transactionManager, entity.QuestionnaireCollectionGetByVerificationModifiedByUserId, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region CsaCollectionGetByCreatedByUserId
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<Csa>|CsaCollectionGetByCreatedByUserId", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CsaCollectionGetByCreatedByUserId' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.CsaCollectionGetByCreatedByUserId = DataRepository.CsaProvider.GetByCreatedByUserId(transactionManager, entity.UserId);

				if (deep && entity.CsaCollectionGetByCreatedByUserId.Count > 0)
				{
					deepHandles.Add("CsaCollectionGetByCreatedByUserId",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<Csa>) DataRepository.CsaProvider.DeepLoad,
						new object[] { transactionManager, entity.CsaCollectionGetByCreatedByUserId, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region QuestionnaireVerificationAssessmentCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<QuestionnaireVerificationAssessment>|QuestionnaireVerificationAssessmentCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'QuestionnaireVerificationAssessmentCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.QuestionnaireVerificationAssessmentCollection = DataRepository.QuestionnaireVerificationAssessmentProvider.GetByModifiedByUserId(transactionManager, entity.UserId);

				if (deep && entity.QuestionnaireVerificationAssessmentCollection.Count > 0)
				{
					deepHandles.Add("QuestionnaireVerificationAssessmentCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<QuestionnaireVerificationAssessment>) DataRepository.QuestionnaireVerificationAssessmentProvider.DeepLoad,
						new object[] { transactionManager, entity.QuestionnaireVerificationAssessmentCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region CsmsFileCollectionGetByModifiedByUserId
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<CsmsFile>|CsmsFileCollectionGetByModifiedByUserId", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CsmsFileCollectionGetByModifiedByUserId' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.CsmsFileCollectionGetByModifiedByUserId = DataRepository.CsmsFileProvider.GetByModifiedByUserId(transactionManager, entity.UserId);

				if (deep && entity.CsmsFileCollectionGetByModifiedByUserId.Count > 0)
				{
					deepHandles.Add("CsmsFileCollectionGetByModifiedByUserId",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<CsmsFile>) DataRepository.CsmsFileProvider.DeepLoad,
						new object[] { transactionManager, entity.CsmsFileCollectionGetByModifiedByUserId, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region SitesCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<Sites>|SitesCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'SitesCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.SitesCollection = DataRepository.SitesProvider.GetByModifiedbyUserId(transactionManager, entity.UserId);

				if (deep && entity.SitesCollection.Count > 0)
				{
					deepHandles.Add("SitesCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<Sites>) DataRepository.SitesProvider.DeepLoad,
						new object[] { transactionManager, entity.SitesCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region DocumentsDownloadLogCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<DocumentsDownloadLog>|DocumentsDownloadLogCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'DocumentsDownloadLogCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.DocumentsDownloadLogCollection = DataRepository.DocumentsDownloadLogProvider.GetByDownloadedByUserId(transactionManager, entity.UserId);

				if (deep && entity.DocumentsDownloadLogCollection.Count > 0)
				{
					deepHandles.Add("DocumentsDownloadLogCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<DocumentsDownloadLog>) DataRepository.DocumentsDownloadLogProvider.DeepLoad,
						new object[] { transactionManager, entity.DocumentsDownloadLogCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region QuestionnaireCommentsCollectionGetByCreatedByUserId
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<QuestionnaireComments>|QuestionnaireCommentsCollectionGetByCreatedByUserId", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'QuestionnaireCommentsCollectionGetByCreatedByUserId' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.QuestionnaireCommentsCollectionGetByCreatedByUserId = DataRepository.QuestionnaireCommentsProvider.GetByCreatedByUserId(transactionManager, entity.UserId);

				if (deep && entity.QuestionnaireCommentsCollectionGetByCreatedByUserId.Count > 0)
				{
					deepHandles.Add("QuestionnaireCommentsCollectionGetByCreatedByUserId",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<QuestionnaireComments>) DataRepository.QuestionnaireCommentsProvider.DeepLoad,
						new object[] { transactionManager, entity.QuestionnaireCommentsCollectionGetByCreatedByUserId, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region CompanySiteCategoryStandardCollectionGetByModifiedByUserId
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<CompanySiteCategoryStandard>|CompanySiteCategoryStandardCollectionGetByModifiedByUserId", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CompanySiteCategoryStandardCollectionGetByModifiedByUserId' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.CompanySiteCategoryStandardCollectionGetByModifiedByUserId = DataRepository.CompanySiteCategoryStandardProvider.GetByModifiedByUserId(transactionManager, entity.UserId);

				if (deep && entity.CompanySiteCategoryStandardCollectionGetByModifiedByUserId.Count > 0)
				{
					deepHandles.Add("CompanySiteCategoryStandardCollectionGetByModifiedByUserId",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<CompanySiteCategoryStandard>) DataRepository.CompanySiteCategoryStandardProvider.DeepLoad,
						new object[] { transactionManager, entity.CompanySiteCategoryStandardCollectionGetByModifiedByUserId, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region CustomReportingLayoutsCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<CustomReportingLayouts>|CustomReportingLayoutsCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CustomReportingLayoutsCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.CustomReportingLayoutsCollection = DataRepository.CustomReportingLayoutsProvider.GetByModifiedByUserId(transactionManager, entity.UserId);

				if (deep && entity.CustomReportingLayoutsCollection.Count > 0)
				{
					deepHandles.Add("CustomReportingLayoutsCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<CustomReportingLayouts>) DataRepository.CustomReportingLayoutsProvider.DeepLoad,
						new object[] { transactionManager, entity.CustomReportingLayoutsCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region QuestionnaireCommentsCollectionGetByModifiedByUserId
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<QuestionnaireComments>|QuestionnaireCommentsCollectionGetByModifiedByUserId", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'QuestionnaireCommentsCollectionGetByModifiedByUserId' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.QuestionnaireCommentsCollectionGetByModifiedByUserId = DataRepository.QuestionnaireCommentsProvider.GetByModifiedByUserId(transactionManager, entity.UserId);

				if (deep && entity.QuestionnaireCommentsCollectionGetByModifiedByUserId.Count > 0)
				{
					deepHandles.Add("QuestionnaireCommentsCollectionGetByModifiedByUserId",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<QuestionnaireComments>) DataRepository.QuestionnaireCommentsProvider.DeepLoad,
						new object[] { transactionManager, entity.QuestionnaireCommentsCollectionGetByModifiedByUserId, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region TwentyOnePointAuditCollectionGetByCreatedbyUserId
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<TwentyOnePointAudit>|TwentyOnePointAuditCollectionGetByCreatedbyUserId", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'TwentyOnePointAuditCollectionGetByCreatedbyUserId' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.TwentyOnePointAuditCollectionGetByCreatedbyUserId = DataRepository.TwentyOnePointAuditProvider.GetByCreatedbyUserId(transactionManager, entity.UserId);

				if (deep && entity.TwentyOnePointAuditCollectionGetByCreatedbyUserId.Count > 0)
				{
					deepHandles.Add("TwentyOnePointAuditCollectionGetByCreatedbyUserId",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<TwentyOnePointAudit>) DataRepository.TwentyOnePointAuditProvider.DeepLoad,
						new object[] { transactionManager, entity.TwentyOnePointAuditCollectionGetByCreatedbyUserId, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region SqExemptionCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<SqExemption>|SqExemptionCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'SqExemptionCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.SqExemptionCollection = DataRepository.SqExemptionProvider.GetByModifiedByUserId(transactionManager, entity.UserId);

				if (deep && entity.SqExemptionCollection.Count > 0)
				{
					deepHandles.Add("SqExemptionCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<SqExemption>) DataRepository.SqExemptionProvider.DeepLoad,
						new object[] { transactionManager, entity.SqExemptionCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region QuestionnaireInitialLocationCollectionGetByApprovedByUserId
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<QuestionnaireInitialLocation>|QuestionnaireInitialLocationCollectionGetByApprovedByUserId", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'QuestionnaireInitialLocationCollectionGetByApprovedByUserId' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.QuestionnaireInitialLocationCollectionGetByApprovedByUserId = DataRepository.QuestionnaireInitialLocationProvider.GetByApprovedByUserId(transactionManager, entity.UserId);

				if (deep && entity.QuestionnaireInitialLocationCollectionGetByApprovedByUserId.Count > 0)
				{
					deepHandles.Add("QuestionnaireInitialLocationCollectionGetByApprovedByUserId",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<QuestionnaireInitialLocation>) DataRepository.QuestionnaireInitialLocationProvider.DeepLoad,
						new object[] { transactionManager, entity.QuestionnaireInitialLocationCollectionGetByApprovedByUserId, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region QuestionnaireVerificationAttachmentCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<QuestionnaireVerificationAttachment>|QuestionnaireVerificationAttachmentCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'QuestionnaireVerificationAttachmentCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.QuestionnaireVerificationAttachmentCollection = DataRepository.QuestionnaireVerificationAttachmentProvider.GetByModifiedByUserId(transactionManager, entity.UserId);

				if (deep && entity.QuestionnaireVerificationAttachmentCollection.Count > 0)
				{
					deepHandles.Add("QuestionnaireVerificationAttachmentCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<QuestionnaireVerificationAttachment>) DataRepository.QuestionnaireVerificationAttachmentProvider.DeepLoad,
						new object[] { transactionManager, entity.QuestionnaireVerificationAttachmentCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region KpiCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<Kpi>|KpiCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'KpiCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.KpiCollection = DataRepository.KpiProvider.GetByModifiedbyUserId(transactionManager, entity.UserId);

				if (deep && entity.KpiCollection.Count > 0)
				{
					deepHandles.Add("KpiCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<Kpi>) DataRepository.KpiProvider.DeepLoad,
						new object[] { transactionManager, entity.KpiCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region QuestionnaireInitialLocationCollectionGetByModifiedByUserId
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<QuestionnaireInitialLocation>|QuestionnaireInitialLocationCollectionGetByModifiedByUserId", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'QuestionnaireInitialLocationCollectionGetByModifiedByUserId' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.QuestionnaireInitialLocationCollectionGetByModifiedByUserId = DataRepository.QuestionnaireInitialLocationProvider.GetByModifiedByUserId(transactionManager, entity.UserId);

				if (deep && entity.QuestionnaireInitialLocationCollectionGetByModifiedByUserId.Count > 0)
				{
					deepHandles.Add("QuestionnaireInitialLocationCollectionGetByModifiedByUserId",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<QuestionnaireInitialLocation>) DataRepository.QuestionnaireInitialLocationProvider.DeepLoad,
						new object[] { transactionManager, entity.QuestionnaireInitialLocationCollectionGetByModifiedByUserId, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region QuestionnaireCollectionGetByMainModifiedByUserId
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<Questionnaire>|QuestionnaireCollectionGetByMainModifiedByUserId", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'QuestionnaireCollectionGetByMainModifiedByUserId' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.QuestionnaireCollectionGetByMainModifiedByUserId = DataRepository.QuestionnaireProvider.GetByMainModifiedByUserId(transactionManager, entity.UserId);

				if (deep && entity.QuestionnaireCollectionGetByMainModifiedByUserId.Count > 0)
				{
					deepHandles.Add("QuestionnaireCollectionGetByMainModifiedByUserId",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<Questionnaire>) DataRepository.QuestionnaireProvider.DeepLoad,
						new object[] { transactionManager, entity.QuestionnaireCollectionGetByMainModifiedByUserId, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region UsersPrivileged2
			// RelationshipType.OneToOne
			if (CanDeepLoad(entity, "UsersPrivileged2|UsersPrivileged2", deepLoadType, innerList))
			{
				entity.UsersPrivileged2 = DataRepository.UsersPrivileged2Provider.GetByUserId(transactionManager, entity.UserId);
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'UsersPrivileged2' loaded. key " + entity.EntityTrackingKey);
				#endif 

				if (deep && entity.UsersPrivileged2 != null)
				{
					deepHandles.Add("UsersPrivileged2",
						new KeyValuePair<Delegate, object>((DeepLoadSingleHandle< UsersPrivileged2 >) DataRepository.UsersPrivileged2Provider.DeepLoad,
						new object[] { transactionManager, entity.UsersPrivileged2, deep, deepLoadType, childTypes, innerList }
					));
				}
			}
			#endregion 
			
			
			
			#region FileVaultTableCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<FileVaultTable>|FileVaultTableCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'FileVaultTableCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.FileVaultTableCollection = DataRepository.FileVaultTableProvider.GetByModifiedByUserId(transactionManager, entity.UserId);

				if (deep && entity.FileVaultTableCollection.Count > 0)
				{
					deepHandles.Add("FileVaultTableCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<FileVaultTable>) DataRepository.FileVaultTableProvider.DeepLoad,
						new object[] { transactionManager, entity.FileVaultTableCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region QuestionnaireInitialContactCollectionGetByModifiedByUserId
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<QuestionnaireInitialContact>|QuestionnaireInitialContactCollectionGetByModifiedByUserId", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'QuestionnaireInitialContactCollectionGetByModifiedByUserId' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.QuestionnaireInitialContactCollectionGetByModifiedByUserId = DataRepository.QuestionnaireInitialContactProvider.GetByModifiedByUserId(transactionManager, entity.UserId);

				if (deep && entity.QuestionnaireInitialContactCollectionGetByModifiedByUserId.Count > 0)
				{
					deepHandles.Add("QuestionnaireInitialContactCollectionGetByModifiedByUserId",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<QuestionnaireInitialContact>) DataRepository.QuestionnaireInitialContactProvider.DeepLoad,
						new object[] { transactionManager, entity.QuestionnaireInitialContactCollectionGetByModifiedByUserId, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region UserPrivilegeCollectionGetByModifiedByUserId
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<UserPrivilege>|UserPrivilegeCollectionGetByModifiedByUserId", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'UserPrivilegeCollectionGetByModifiedByUserId' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.UserPrivilegeCollectionGetByModifiedByUserId = DataRepository.UserPrivilegeProvider.GetByModifiedByUserId(transactionManager, entity.UserId);

				if (deep && entity.UserPrivilegeCollectionGetByModifiedByUserId.Count > 0)
				{
					deepHandles.Add("UserPrivilegeCollectionGetByModifiedByUserId",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<UserPrivilege>) DataRepository.UserPrivilegeProvider.DeepLoad,
						new object[] { transactionManager, entity.UserPrivilegeCollectionGetByModifiedByUserId, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region CompanyStatusChangeApprovalCollectionGetByAssessedByUserId
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<CompanyStatusChangeApproval>|CompanyStatusChangeApprovalCollectionGetByAssessedByUserId", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CompanyStatusChangeApprovalCollectionGetByAssessedByUserId' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.CompanyStatusChangeApprovalCollectionGetByAssessedByUserId = DataRepository.CompanyStatusChangeApprovalProvider.GetByAssessedByUserId(transactionManager, entity.UserId);

				if (deep && entity.CompanyStatusChangeApprovalCollectionGetByAssessedByUserId.Count > 0)
				{
					deepHandles.Add("CompanyStatusChangeApprovalCollectionGetByAssessedByUserId",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<CompanyStatusChangeApproval>) DataRepository.CompanyStatusChangeApprovalProvider.DeepLoad,
						new object[] { transactionManager, entity.CompanyStatusChangeApprovalCollectionGetByAssessedByUserId, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region UsersPrivileged
			// RelationshipType.OneToOne
			if (CanDeepLoad(entity, "UsersPrivileged|UsersPrivileged", deepLoadType, innerList))
			{
				entity.UsersPrivileged = DataRepository.UsersPrivilegedProvider.GetByUserId(transactionManager, entity.UserId);
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'UsersPrivileged' loaded. key " + entity.EntityTrackingKey);
				#endif 

				if (deep && entity.UsersPrivileged != null)
				{
					deepHandles.Add("UsersPrivileged",
						new KeyValuePair<Delegate, object>((DeepLoadSingleHandle< UsersPrivileged >) DataRepository.UsersPrivilegedProvider.DeepLoad,
						new object[] { transactionManager, entity.UsersPrivileged, deep, deepLoadType, childTypes, innerList }
					));
				}
			}
			#endregion 
			
			
			
			#region FileDbMedicalTrainingCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<FileDbMedicalTraining>|FileDbMedicalTrainingCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'FileDbMedicalTrainingCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.FileDbMedicalTrainingCollection = DataRepository.FileDbMedicalTrainingProvider.GetByModifiedByUserId(transactionManager, entity.UserId);

				if (deep && entity.FileDbMedicalTrainingCollection.Count > 0)
				{
					deepHandles.Add("FileDbMedicalTrainingCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<FileDbMedicalTraining>) DataRepository.FileDbMedicalTrainingProvider.DeepLoad,
						new object[] { transactionManager, entity.FileDbMedicalTrainingCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region QuestionnaireInitialContactEmailCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<QuestionnaireInitialContactEmail>|QuestionnaireInitialContactEmailCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'QuestionnaireInitialContactEmailCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.QuestionnaireInitialContactEmailCollection = DataRepository.QuestionnaireInitialContactEmailProvider.GetBySentByUserId(transactionManager, entity.UserId);

				if (deep && entity.QuestionnaireInitialContactEmailCollection.Count > 0)
				{
					deepHandles.Add("QuestionnaireInitialContactEmailCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<QuestionnaireInitialContactEmail>) DataRepository.QuestionnaireInitialContactEmailProvider.DeepLoad,
						new object[] { transactionManager, entity.QuestionnaireInitialContactEmailCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region QuestionnaireMainResponseCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<QuestionnaireMainResponse>|QuestionnaireMainResponseCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'QuestionnaireMainResponseCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.QuestionnaireMainResponseCollection = DataRepository.QuestionnaireMainResponseProvider.GetByModifiedByUserId(transactionManager, entity.UserId);

				if (deep && entity.QuestionnaireMainResponseCollection.Count > 0)
				{
					deepHandles.Add("QuestionnaireMainResponseCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<QuestionnaireMainResponse>) DataRepository.QuestionnaireMainResponseProvider.DeepLoad,
						new object[] { transactionManager, entity.QuestionnaireMainResponseCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region QuestionnaireCollectionGetByModifiedByUserId
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<Questionnaire>|QuestionnaireCollectionGetByModifiedByUserId", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'QuestionnaireCollectionGetByModifiedByUserId' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.QuestionnaireCollectionGetByModifiedByUserId = DataRepository.QuestionnaireProvider.GetByModifiedByUserId(transactionManager, entity.UserId);

				if (deep && entity.QuestionnaireCollectionGetByModifiedByUserId.Count > 0)
				{
					deepHandles.Add("QuestionnaireCollectionGetByModifiedByUserId",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<Questionnaire>) DataRepository.QuestionnaireProvider.DeepLoad,
						new object[] { transactionManager, entity.QuestionnaireCollectionGetByModifiedByUserId, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region ContactsContractorsCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<ContactsContractors>|ContactsContractorsCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ContactsContractorsCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.ContactsContractorsCollection = DataRepository.ContactsContractorsProvider.GetByModifiedByUserId(transactionManager, entity.UserId);

				if (deep && entity.ContactsContractorsCollection.Count > 0)
				{
					deepHandles.Add("ContactsContractorsCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<ContactsContractors>) DataRepository.ContactsContractorsProvider.DeepLoad,
						new object[] { transactionManager, entity.ContactsContractorsCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region CsaCollectionGetByModifiedByUserId
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<Csa>|CsaCollectionGetByModifiedByUserId", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CsaCollectionGetByModifiedByUserId' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.CsaCollectionGetByModifiedByUserId = DataRepository.CsaProvider.GetByModifiedByUserId(transactionManager, entity.UserId);

				if (deep && entity.CsaCollectionGetByModifiedByUserId.Count > 0)
				{
					deepHandles.Add("CsaCollectionGetByModifiedByUserId",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<Csa>) DataRepository.CsaProvider.DeepLoad,
						new object[] { transactionManager, entity.CsaCollectionGetByModifiedByUserId, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region QuestionnaireInitialLocationCollectionGetBySpa
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<QuestionnaireInitialLocation>|QuestionnaireInitialLocationCollectionGetBySpa", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'QuestionnaireInitialLocationCollectionGetBySpa' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.QuestionnaireInitialLocationCollectionGetBySpa = DataRepository.QuestionnaireInitialLocationProvider.GetBySpa(transactionManager, entity.UserId);

				if (deep && entity.QuestionnaireInitialLocationCollectionGetBySpa.Count > 0)
				{
					deepHandles.Add("QuestionnaireInitialLocationCollectionGetBySpa",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<QuestionnaireInitialLocation>) DataRepository.QuestionnaireInitialLocationProvider.DeepLoad,
						new object[] { transactionManager, entity.QuestionnaireInitialLocationCollectionGetBySpa, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region AdminTaskCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<AdminTask>|AdminTaskCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'AdminTaskCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.AdminTaskCollection = DataRepository.AdminTaskProvider.GetByClosedByUserId(transactionManager, entity.UserId);

				if (deep && entity.AdminTaskCollection.Count > 0)
				{
					deepHandles.Add("AdminTaskCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<AdminTask>) DataRepository.AdminTaskProvider.DeepLoad,
						new object[] { transactionManager, entity.AdminTaskCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region AccessLogsCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<AccessLogs>|AccessLogsCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'AccessLogsCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.AccessLogsCollection = DataRepository.AccessLogsProvider.GetByUserId(transactionManager, entity.UserId);

				if (deep && entity.AccessLogsCollection.Count > 0)
				{
					deepHandles.Add("AccessLogsCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<AccessLogs>) DataRepository.AccessLogsProvider.DeepLoad,
						new object[] { transactionManager, entity.AccessLogsCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region UserPrivilegeCollectionGetByUserId
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<UserPrivilege>|UserPrivilegeCollectionGetByUserId", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'UserPrivilegeCollectionGetByUserId' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.UserPrivilegeCollectionGetByUserId = DataRepository.UserPrivilegeProvider.GetByUserId(transactionManager, entity.UserId);

				if (deep && entity.UserPrivilegeCollectionGetByUserId.Count > 0)
				{
					deepHandles.Add("UserPrivilegeCollectionGetByUserId",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<UserPrivilege>) DataRepository.UserPrivilegeProvider.DeepLoad,
						new object[] { transactionManager, entity.UserPrivilegeCollectionGetByUserId, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region UsersProcurementCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<UsersProcurement>|UsersProcurementCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'UsersProcurementCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.UsersProcurementCollection = DataRepository.UsersProcurementProvider.GetBySupervisorUserId(transactionManager, entity.UserId);

				if (deep && entity.UsersProcurementCollection.Count > 0)
				{
					deepHandles.Add("UsersProcurementCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<UsersProcurement>) DataRepository.UsersProcurementProvider.DeepLoad,
						new object[] { transactionManager, entity.UsersProcurementCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region QuestionnaireCollectionGetByCreatedByUserId
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<Questionnaire>|QuestionnaireCollectionGetByCreatedByUserId", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'QuestionnaireCollectionGetByCreatedByUserId' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.QuestionnaireCollectionGetByCreatedByUserId = DataRepository.QuestionnaireProvider.GetByCreatedByUserId(transactionManager, entity.UserId);

				if (deep && entity.QuestionnaireCollectionGetByCreatedByUserId.Count > 0)
				{
					deepHandles.Add("QuestionnaireCollectionGetByCreatedByUserId",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<Questionnaire>) DataRepository.QuestionnaireProvider.DeepLoad,
						new object[] { transactionManager, entity.QuestionnaireCollectionGetByCreatedByUserId, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region UsersCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<Users>|UsersCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'UsersCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.UsersCollection = DataRepository.UsersProvider.GetByModifiedByUserId(transactionManager, entity.UserId);

				if (deep && entity.UsersCollection.Count > 0)
				{
					deepHandles.Add("UsersCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<Users>) DataRepository.UsersProvider.DeepLoad,
						new object[] { transactionManager, entity.UsersCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region EhsConsultantCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<EhsConsultant>|EhsConsultantCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'EhsConsultantCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.EhsConsultantCollection = DataRepository.EhsConsultantProvider.GetBySupervisorUserId(transactionManager, entity.UserId);

				if (deep && entity.EhsConsultantCollection.Count > 0)
				{
					deepHandles.Add("EhsConsultantCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<EhsConsultant>) DataRepository.EhsConsultantProvider.DeepLoad,
						new object[] { transactionManager, entity.EhsConsultantCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region QuestionnaireCollectionGetByApprovedByUserId
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<Questionnaire>|QuestionnaireCollectionGetByApprovedByUserId", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'QuestionnaireCollectionGetByApprovedByUserId' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.QuestionnaireCollectionGetByApprovedByUserId = DataRepository.QuestionnaireProvider.GetByApprovedByUserId(transactionManager, entity.UserId);

				if (deep && entity.QuestionnaireCollectionGetByApprovedByUserId.Count > 0)
				{
					deepHandles.Add("QuestionnaireCollectionGetByApprovedByUserId",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<Questionnaire>) DataRepository.QuestionnaireProvider.DeepLoad,
						new object[] { transactionManager, entity.QuestionnaireCollectionGetByApprovedByUserId, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region QuestionnaireVerificationResponseCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<QuestionnaireVerificationResponse>|QuestionnaireVerificationResponseCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'QuestionnaireVerificationResponseCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.QuestionnaireVerificationResponseCollection = DataRepository.QuestionnaireVerificationResponseProvider.GetByModifiedByUserId(transactionManager, entity.UserId);

				if (deep && entity.QuestionnaireVerificationResponseCollection.Count > 0)
				{
					deepHandles.Add("QuestionnaireVerificationResponseCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<QuestionnaireVerificationResponse>) DataRepository.QuestionnaireVerificationResponseProvider.DeepLoad,
						new object[] { transactionManager, entity.QuestionnaireVerificationResponseCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region QuestionnaireCollectionGetByMainAssessmentByUserId
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<Questionnaire>|QuestionnaireCollectionGetByMainAssessmentByUserId", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'QuestionnaireCollectionGetByMainAssessmentByUserId' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.QuestionnaireCollectionGetByMainAssessmentByUserId = DataRepository.QuestionnaireProvider.GetByMainAssessmentByUserId(transactionManager, entity.UserId);

				if (deep && entity.QuestionnaireCollectionGetByMainAssessmentByUserId.Count > 0)
				{
					deepHandles.Add("QuestionnaireCollectionGetByMainAssessmentByUserId",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<Questionnaire>) DataRepository.QuestionnaireProvider.DeepLoad,
						new object[] { transactionManager, entity.QuestionnaireCollectionGetByMainAssessmentByUserId, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region CompaniesCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<Companies>|CompaniesCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CompaniesCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.CompaniesCollection = DataRepository.CompaniesProvider.GetByModifiedbyUserId(transactionManager, entity.UserId);

				if (deep && entity.CompaniesCollection.Count > 0)
				{
					deepHandles.Add("CompaniesCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<Companies>) DataRepository.CompaniesProvider.DeepLoad,
						new object[] { transactionManager, entity.CompaniesCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region CompanySiteCategoryStandardCollectionGetByArpUserId
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<CompanySiteCategoryStandard>|CompanySiteCategoryStandardCollectionGetByArpUserId", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CompanySiteCategoryStandardCollectionGetByArpUserId' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.CompanySiteCategoryStandardCollectionGetByArpUserId = DataRepository.CompanySiteCategoryStandardProvider.GetByArpUserId(transactionManager, entity.UserId);

				if (deep && entity.CompanySiteCategoryStandardCollectionGetByArpUserId.Count > 0)
				{
					deepHandles.Add("CompanySiteCategoryStandardCollectionGetByArpUserId",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<CompanySiteCategoryStandard>) DataRepository.CompanySiteCategoryStandardProvider.DeepLoad,
						new object[] { transactionManager, entity.CompanySiteCategoryStandardCollectionGetByArpUserId, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region FileDbCollectionGetByStatusModifiedByUserId
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<FileDb>|FileDbCollectionGetByStatusModifiedByUserId", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'FileDbCollectionGetByStatusModifiedByUserId' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.FileDbCollectionGetByStatusModifiedByUserId = DataRepository.FileDbProvider.GetByStatusModifiedByUserId(transactionManager, entity.UserId);

				if (deep && entity.FileDbCollectionGetByStatusModifiedByUserId.Count > 0)
				{
					deepHandles.Add("FileDbCollectionGetByStatusModifiedByUserId",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<FileDb>) DataRepository.FileDbProvider.DeepLoad,
						new object[] { transactionManager, entity.FileDbCollectionGetByStatusModifiedByUserId, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region ContactsAlcoaCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<ContactsAlcoa>|ContactsAlcoaCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ContactsAlcoaCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.ContactsAlcoaCollection = DataRepository.ContactsAlcoaProvider.GetByModifiedByUserId(transactionManager, entity.UserId);

				if (deep && entity.ContactsAlcoaCollection.Count > 0)
				{
					deepHandles.Add("ContactsAlcoaCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<ContactsAlcoa>) DataRepository.ContactsAlcoaProvider.DeepLoad,
						new object[] { transactionManager, entity.ContactsAlcoaCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region UserPrivilegeCollectionGetByCreatedByUserId
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<UserPrivilege>|UserPrivilegeCollectionGetByCreatedByUserId", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'UserPrivilegeCollectionGetByCreatedByUserId' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.UserPrivilegeCollectionGetByCreatedByUserId = DataRepository.UserPrivilegeProvider.GetByCreatedByUserId(transactionManager, entity.UserId);

				if (deep && entity.UserPrivilegeCollectionGetByCreatedByUserId.Count > 0)
				{
					deepHandles.Add("UserPrivilegeCollectionGetByCreatedByUserId",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<UserPrivilege>) DataRepository.UserPrivilegeProvider.DeepLoad,
						new object[] { transactionManager, entity.UserPrivilegeCollectionGetByCreatedByUserId, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region TwentyOnePointAuditCollectionGetByModifiedbyUserId
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<TwentyOnePointAudit>|TwentyOnePointAuditCollectionGetByModifiedbyUserId", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'TwentyOnePointAuditCollectionGetByModifiedbyUserId' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.TwentyOnePointAuditCollectionGetByModifiedbyUserId = DataRepository.TwentyOnePointAuditProvider.GetByModifiedbyUserId(transactionManager, entity.UserId);

				if (deep && entity.TwentyOnePointAuditCollectionGetByModifiedbyUserId.Count > 0)
				{
					deepHandles.Add("TwentyOnePointAuditCollectionGetByModifiedbyUserId",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<TwentyOnePointAudit>) DataRepository.TwentyOnePointAuditProvider.DeepLoad,
						new object[] { transactionManager, entity.TwentyOnePointAuditCollectionGetByModifiedbyUserId, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region FileDbCollectionGetByModifiedByUserId
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<FileDb>|FileDbCollectionGetByModifiedByUserId", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'FileDbCollectionGetByModifiedByUserId' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.FileDbCollectionGetByModifiedByUserId = DataRepository.FileDbProvider.GetByModifiedByUserId(transactionManager, entity.UserId);

				if (deep && entity.FileDbCollectionGetByModifiedByUserId.Count > 0)
				{
					deepHandles.Add("FileDbCollectionGetByModifiedByUserId",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<FileDb>) DataRepository.FileDbProvider.DeepLoad,
						new object[] { transactionManager, entity.FileDbCollectionGetByModifiedByUserId, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region QuestionnaireMainAttachmentCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<QuestionnaireMainAttachment>|QuestionnaireMainAttachmentCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'QuestionnaireMainAttachmentCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.QuestionnaireMainAttachmentCollection = DataRepository.QuestionnaireMainAttachmentProvider.GetByModifiedByUserId(transactionManager, entity.UserId);

				if (deep && entity.QuestionnaireMainAttachmentCollection.Count > 0)
				{
					deepHandles.Add("QuestionnaireMainAttachmentCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<QuestionnaireMainAttachment>) DataRepository.QuestionnaireMainAttachmentProvider.DeepLoad,
						new object[] { transactionManager, entity.QuestionnaireMainAttachmentCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region CompanySiteCategoryStandardCollectionGetByLocationSponsorUserId
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<CompanySiteCategoryStandard>|CompanySiteCategoryStandardCollectionGetByLocationSponsorUserId", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CompanySiteCategoryStandardCollectionGetByLocationSponsorUserId' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.CompanySiteCategoryStandardCollectionGetByLocationSponsorUserId = DataRepository.CompanySiteCategoryStandardProvider.GetByLocationSponsorUserId(transactionManager, entity.UserId);

				if (deep && entity.CompanySiteCategoryStandardCollectionGetByLocationSponsorUserId.Count > 0)
				{
					deepHandles.Add("CompanySiteCategoryStandardCollectionGetByLocationSponsorUserId",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<CompanySiteCategoryStandard>) DataRepository.CompanySiteCategoryStandardProvider.DeepLoad,
						new object[] { transactionManager, entity.CompanySiteCategoryStandardCollectionGetByLocationSponsorUserId, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region FileVaultCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<FileVault>|FileVaultCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'FileVaultCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.FileVaultCollection = DataRepository.FileVaultProvider.GetByModifiedByUserId(transactionManager, entity.UserId);

				if (deep && entity.FileVaultCollection.Count > 0)
				{
					deepHandles.Add("FileVaultCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<FileVault>) DataRepository.FileVaultProvider.DeepLoad,
						new object[] { transactionManager, entity.FileVaultCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region CsmsFileCollectionGetByCreatedByUserId
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<CsmsFile>|CsmsFileCollectionGetByCreatedByUserId", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CsmsFileCollectionGetByCreatedByUserId' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.CsmsFileCollectionGetByCreatedByUserId = DataRepository.CsmsFileProvider.GetByCreatedByUserId(transactionManager, entity.UserId);

				if (deep && entity.CsmsFileCollectionGetByCreatedByUserId.Count > 0)
				{
					deepHandles.Add("CsmsFileCollectionGetByCreatedByUserId",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<CsmsFile>) DataRepository.CsmsFileProvider.DeepLoad,
						new object[] { transactionManager, entity.CsmsFileCollectionGetByCreatedByUserId, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region UsersPrivileged3
			// RelationshipType.OneToOne
			if (CanDeepLoad(entity, "UsersPrivileged3|UsersPrivileged3", deepLoadType, innerList))
			{
				entity.UsersPrivileged3 = DataRepository.UsersPrivileged3Provider.GetByUserId(transactionManager, entity.UserId);
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'UsersPrivileged3' loaded. key " + entity.EntityTrackingKey);
				#endif 

				if (deep && entity.UsersPrivileged3 != null)
				{
					deepHandles.Add("UsersPrivileged3",
						new KeyValuePair<Delegate, object>((DeepLoadSingleHandle< UsersPrivileged3 >) DataRepository.UsersPrivileged3Provider.DeepLoad,
						new object[] { transactionManager, entity.UsersPrivileged3, deep, deepLoadType, childTypes, innerList }
					));
				}
			}
			#endregion 
			
			
			
			#region CompaniesRelationshipCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<CompaniesRelationship>|CompaniesRelationshipCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CompaniesRelationshipCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.CompaniesRelationshipCollection = DataRepository.CompaniesRelationshipProvider.GetByModifiedByUserId(transactionManager, entity.UserId);

				if (deep && entity.CompaniesRelationshipCollection.Count > 0)
				{
					deepHandles.Add("CompaniesRelationshipCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<CompaniesRelationship>) DataRepository.CompaniesRelationshipProvider.DeepLoad,
						new object[] { transactionManager, entity.CompaniesRelationshipCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region EscalationChainSafetyCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<EscalationChainSafety>|EscalationChainSafetyCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'EscalationChainSafetyCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.EscalationChainSafetyCollection = DataRepository.EscalationChainSafetyProvider.GetByUserId(transactionManager, entity.UserId);

				if (deep && entity.EscalationChainSafetyCollection.Count > 0)
				{
					deepHandles.Add("EscalationChainSafetyCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<EscalationChainSafety>) DataRepository.EscalationChainSafetyProvider.DeepLoad,
						new object[] { transactionManager, entity.EscalationChainSafetyCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region EscalationChainProcurementCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<EscalationChainProcurement>|EscalationChainProcurementCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'EscalationChainProcurementCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.EscalationChainProcurementCollection = DataRepository.EscalationChainProcurementProvider.GetByUserId(transactionManager, entity.UserId);

				if (deep && entity.EscalationChainProcurementCollection.Count > 0)
				{
					deepHandles.Add("EscalationChainProcurementCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<EscalationChainProcurement>) DataRepository.EscalationChainProcurementProvider.DeepLoad,
						new object[] { transactionManager, entity.EscalationChainProcurementCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region QuestionnaireInitialResponseCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<QuestionnaireInitialResponse>|QuestionnaireInitialResponseCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'QuestionnaireInitialResponseCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.QuestionnaireInitialResponseCollection = DataRepository.QuestionnaireInitialResponseProvider.GetByModifiedByUserId(transactionManager, entity.UserId);

				if (deep && entity.QuestionnaireInitialResponseCollection.Count > 0)
				{
					deepHandles.Add("QuestionnaireInitialResponseCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<QuestionnaireInitialResponse>) DataRepository.QuestionnaireInitialResponseProvider.DeepLoad,
						new object[] { transactionManager, entity.QuestionnaireInitialResponseCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.Users object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.Users instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.Users Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.Users entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region ModifiedByUserIdSource
			if (CanDeepSave(entity, "Users|ModifiedByUserIdSource", deepSaveType, innerList) 
				&& entity.ModifiedByUserIdSource != null)
			{
				DataRepository.UsersProvider.Save(transactionManager, entity.ModifiedByUserIdSource);
				entity.ModifiedByUserId = entity.ModifiedByUserIdSource.UserId;
			}
			#endregion 
			
			#region RoleIdSource
			if (CanDeepSave(entity, "Role|RoleIdSource", deepSaveType, innerList) 
				&& entity.RoleIdSource != null)
			{
				DataRepository.RoleProvider.Save(transactionManager, entity.RoleIdSource);
				entity.RoleId = entity.RoleIdSource.RoleId;
			}
			#endregion 
			
			#region CompanyIdSource
			if (CanDeepSave(entity, "Companies|CompanyIdSource", deepSaveType, innerList) 
				&& entity.CompanyIdSource != null)
			{
				DataRepository.CompaniesProvider.Save(transactionManager, entity.CompanyIdSource);
				entity.CompanyId = entity.CompanyIdSource.CompanyId;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();

			#region UsersPrivileged2
			if (CanDeepSave(entity.UsersPrivileged2, "UsersPrivileged2|UsersPrivileged2", deepSaveType, innerList))
			{

				if (entity.UsersPrivileged2 != null)
				{
					// update each child parent id with the real parent id (mostly used on insert)

					entity.UsersPrivileged2.UserId = entity.UserId;
					//DataRepository.UsersPrivileged2Provider.Save(transactionManager, entity.UsersPrivileged2);
					deepHandles.Add("UsersPrivileged2",
						new KeyValuePair<Delegate, object>((DeepSaveSingleHandle< UsersPrivileged2 >) DataRepository.UsersPrivileged2Provider.DeepSave,
						new object[] { transactionManager, entity.UsersPrivileged2, deepSaveType, childTypes, innerList }
					));
				}
			} 
			#endregion 

			#region UsersPrivileged
			if (CanDeepSave(entity.UsersPrivileged, "UsersPrivileged|UsersPrivileged", deepSaveType, innerList))
			{

				if (entity.UsersPrivileged != null)
				{
					// update each child parent id with the real parent id (mostly used on insert)

					entity.UsersPrivileged.UserId = entity.UserId;
					//DataRepository.UsersPrivilegedProvider.Save(transactionManager, entity.UsersPrivileged);
					deepHandles.Add("UsersPrivileged",
						new KeyValuePair<Delegate, object>((DeepSaveSingleHandle< UsersPrivileged >) DataRepository.UsersPrivilegedProvider.DeepSave,
						new object[] { transactionManager, entity.UsersPrivileged, deepSaveType, childTypes, innerList }
					));
				}
			} 
			#endregion 

			#region UsersPrivileged3
			if (CanDeepSave(entity.UsersPrivileged3, "UsersPrivileged3|UsersPrivileged3", deepSaveType, innerList))
			{

				if (entity.UsersPrivileged3 != null)
				{
					// update each child parent id with the real parent id (mostly used on insert)

					entity.UsersPrivileged3.UserId = entity.UserId;
					//DataRepository.UsersPrivileged3Provider.Save(transactionManager, entity.UsersPrivileged3);
					deepHandles.Add("UsersPrivileged3",
						new KeyValuePair<Delegate, object>((DeepSaveSingleHandle< UsersPrivileged3 >) DataRepository.UsersPrivileged3Provider.DeepSave,
						new object[] { transactionManager, entity.UsersPrivileged3, deepSaveType, childTypes, innerList }
					));
				}
			} 
			#endregion 
	
			#region List<CompanySiteCategoryStandard>
				if (CanDeepSave(entity.CompanySiteCategoryStandardCollectionGetByApprovedByUserId, "List<CompanySiteCategoryStandard>|CompanySiteCategoryStandardCollectionGetByApprovedByUserId", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(CompanySiteCategoryStandard child in entity.CompanySiteCategoryStandardCollectionGetByApprovedByUserId)
					{
						if(child.ApprovedByUserIdSource != null)
						{
							child.ApprovedByUserId = child.ApprovedByUserIdSource.UserId;
						}
						else
						{
							child.ApprovedByUserId = entity.UserId;
						}

					}

					if (entity.CompanySiteCategoryStandardCollectionGetByApprovedByUserId.Count > 0 || entity.CompanySiteCategoryStandardCollectionGetByApprovedByUserId.DeletedItems.Count > 0)
					{
						//DataRepository.CompanySiteCategoryStandardProvider.Save(transactionManager, entity.CompanySiteCategoryStandardCollectionGetByApprovedByUserId);
						
						deepHandles.Add("CompanySiteCategoryStandardCollectionGetByApprovedByUserId",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< CompanySiteCategoryStandard >) DataRepository.CompanySiteCategoryStandardProvider.DeepSave,
							new object[] { transactionManager, entity.CompanySiteCategoryStandardCollectionGetByApprovedByUserId, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<QuestionnaireServicesSelected>
				if (CanDeepSave(entity.QuestionnaireServicesSelectedCollection, "List<QuestionnaireServicesSelected>|QuestionnaireServicesSelectedCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(QuestionnaireServicesSelected child in entity.QuestionnaireServicesSelectedCollection)
					{
						if(child.ModifiedByUserIdSource != null)
						{
							child.ModifiedByUserId = child.ModifiedByUserIdSource.UserId;
						}
						else
						{
							child.ModifiedByUserId = entity.UserId;
						}

					}

					if (entity.QuestionnaireServicesSelectedCollection.Count > 0 || entity.QuestionnaireServicesSelectedCollection.DeletedItems.Count > 0)
					{
						//DataRepository.QuestionnaireServicesSelectedProvider.Save(transactionManager, entity.QuestionnaireServicesSelectedCollection);
						
						deepHandles.Add("QuestionnaireServicesSelectedCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< QuestionnaireServicesSelected >) DataRepository.QuestionnaireServicesSelectedProvider.DeepSave,
							new object[] { transactionManager, entity.QuestionnaireServicesSelectedCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<QuestionnaireMainAssessment>
				if (CanDeepSave(entity.QuestionnaireMainAssessmentCollection, "List<QuestionnaireMainAssessment>|QuestionnaireMainAssessmentCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(QuestionnaireMainAssessment child in entity.QuestionnaireMainAssessmentCollection)
					{
						if(child.ModifiedByUserIdSource != null)
						{
							child.ModifiedByUserId = child.ModifiedByUserIdSource.UserId;
						}
						else
						{
							child.ModifiedByUserId = entity.UserId;
						}

					}

					if (entity.QuestionnaireMainAssessmentCollection.Count > 0 || entity.QuestionnaireMainAssessmentCollection.DeletedItems.Count > 0)
					{
						//DataRepository.QuestionnaireMainAssessmentProvider.Save(transactionManager, entity.QuestionnaireMainAssessmentCollection);
						
						deepHandles.Add("QuestionnaireMainAssessmentCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< QuestionnaireMainAssessment >) DataRepository.QuestionnaireMainAssessmentProvider.DeepSave,
							new object[] { transactionManager, entity.QuestionnaireMainAssessmentCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<ConfigSa812>
				if (CanDeepSave(entity.ConfigSa812Collection, "List<ConfigSa812>|ConfigSa812Collection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(ConfigSa812 child in entity.ConfigSa812Collection)
					{
						if(child.SupervisorUserIdSource != null)
						{
							child.SupervisorUserId = child.SupervisorUserIdSource.UserId;
						}
						else
						{
							child.SupervisorUserId = entity.UserId;
						}

					}

					if (entity.ConfigSa812Collection.Count > 0 || entity.ConfigSa812Collection.DeletedItems.Count > 0)
					{
						//DataRepository.ConfigSa812Provider.Save(transactionManager, entity.ConfigSa812Collection);
						
						deepHandles.Add("ConfigSa812Collection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< ConfigSa812 >) DataRepository.ConfigSa812Provider.DeepSave,
							new object[] { transactionManager, entity.ConfigSa812Collection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<Questionnaire>
				if (CanDeepSave(entity.QuestionnaireCollectionGetByInitialModifiedByUserId, "List<Questionnaire>|QuestionnaireCollectionGetByInitialModifiedByUserId", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(Questionnaire child in entity.QuestionnaireCollectionGetByInitialModifiedByUserId)
					{
						if(child.InitialModifiedByUserIdSource != null)
						{
							child.InitialModifiedByUserId = child.InitialModifiedByUserIdSource.UserId;
						}
						else
						{
							child.InitialModifiedByUserId = entity.UserId;
						}

					}

					if (entity.QuestionnaireCollectionGetByInitialModifiedByUserId.Count > 0 || entity.QuestionnaireCollectionGetByInitialModifiedByUserId.DeletedItems.Count > 0)
					{
						//DataRepository.QuestionnaireProvider.Save(transactionManager, entity.QuestionnaireCollectionGetByInitialModifiedByUserId);
						
						deepHandles.Add("QuestionnaireCollectionGetByInitialModifiedByUserId",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< Questionnaire >) DataRepository.QuestionnaireProvider.DeepSave,
							new object[] { transactionManager, entity.QuestionnaireCollectionGetByInitialModifiedByUserId, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<CompanyStatusChangeApproval>
				if (CanDeepSave(entity.CompanyStatusChangeApprovalCollectionGetByRequestedByUserId, "List<CompanyStatusChangeApproval>|CompanyStatusChangeApprovalCollectionGetByRequestedByUserId", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(CompanyStatusChangeApproval child in entity.CompanyStatusChangeApprovalCollectionGetByRequestedByUserId)
					{
						if(child.RequestedByUserIdSource != null)
						{
							child.RequestedByUserId = child.RequestedByUserIdSource.UserId;
						}
						else
						{
							child.RequestedByUserId = entity.UserId;
						}

					}

					if (entity.CompanyStatusChangeApprovalCollectionGetByRequestedByUserId.Count > 0 || entity.CompanyStatusChangeApprovalCollectionGetByRequestedByUserId.DeletedItems.Count > 0)
					{
						//DataRepository.CompanyStatusChangeApprovalProvider.Save(transactionManager, entity.CompanyStatusChangeApprovalCollectionGetByRequestedByUserId);
						
						deepHandles.Add("CompanyStatusChangeApprovalCollectionGetByRequestedByUserId",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< CompanyStatusChangeApproval >) DataRepository.CompanyStatusChangeApprovalProvider.DeepSave,
							new object[] { transactionManager, entity.CompanyStatusChangeApprovalCollectionGetByRequestedByUserId, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<QuestionnaireActionLog>
				if (CanDeepSave(entity.QuestionnaireActionLogCollection, "List<QuestionnaireActionLog>|QuestionnaireActionLogCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(QuestionnaireActionLog child in entity.QuestionnaireActionLogCollection)
					{
						if(child.CreatedByUserIdSource != null)
						{
							child.CreatedByUserId = child.CreatedByUserIdSource.UserId;
						}
						else
						{
							child.CreatedByUserId = entity.UserId;
						}

					}

					if (entity.QuestionnaireActionLogCollection.Count > 0 || entity.QuestionnaireActionLogCollection.DeletedItems.Count > 0)
					{
						//DataRepository.QuestionnaireActionLogProvider.Save(transactionManager, entity.QuestionnaireActionLogCollection);
						
						deepHandles.Add("QuestionnaireActionLogCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< QuestionnaireActionLog >) DataRepository.QuestionnaireActionLogProvider.DeepSave,
							new object[] { transactionManager, entity.QuestionnaireActionLogCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<QuestionnaireInitialContact>
				if (CanDeepSave(entity.QuestionnaireInitialContactCollectionGetByCreatedByUserId, "List<QuestionnaireInitialContact>|QuestionnaireInitialContactCollectionGetByCreatedByUserId", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(QuestionnaireInitialContact child in entity.QuestionnaireInitialContactCollectionGetByCreatedByUserId)
					{
						if(child.CreatedByUserIdSource != null)
						{
							child.CreatedByUserId = child.CreatedByUserIdSource.UserId;
						}
						else
						{
							child.CreatedByUserId = entity.UserId;
						}

					}

					if (entity.QuestionnaireInitialContactCollectionGetByCreatedByUserId.Count > 0 || entity.QuestionnaireInitialContactCollectionGetByCreatedByUserId.DeletedItems.Count > 0)
					{
						//DataRepository.QuestionnaireInitialContactProvider.Save(transactionManager, entity.QuestionnaireInitialContactCollectionGetByCreatedByUserId);
						
						deepHandles.Add("QuestionnaireInitialContactCollectionGetByCreatedByUserId",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< QuestionnaireInitialContact >) DataRepository.QuestionnaireInitialContactProvider.DeepSave,
							new object[] { transactionManager, entity.QuestionnaireInitialContactCollectionGetByCreatedByUserId, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<Questionnaire>
				if (CanDeepSave(entity.QuestionnaireCollectionGetByVerificationModifiedByUserId, "List<Questionnaire>|QuestionnaireCollectionGetByVerificationModifiedByUserId", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(Questionnaire child in entity.QuestionnaireCollectionGetByVerificationModifiedByUserId)
					{
						if(child.VerificationModifiedByUserIdSource != null)
						{
							child.VerificationModifiedByUserId = child.VerificationModifiedByUserIdSource.UserId;
						}
						else
						{
							child.VerificationModifiedByUserId = entity.UserId;
						}

					}

					if (entity.QuestionnaireCollectionGetByVerificationModifiedByUserId.Count > 0 || entity.QuestionnaireCollectionGetByVerificationModifiedByUserId.DeletedItems.Count > 0)
					{
						//DataRepository.QuestionnaireProvider.Save(transactionManager, entity.QuestionnaireCollectionGetByVerificationModifiedByUserId);
						
						deepHandles.Add("QuestionnaireCollectionGetByVerificationModifiedByUserId",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< Questionnaire >) DataRepository.QuestionnaireProvider.DeepSave,
							new object[] { transactionManager, entity.QuestionnaireCollectionGetByVerificationModifiedByUserId, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<Csa>
				if (CanDeepSave(entity.CsaCollectionGetByCreatedByUserId, "List<Csa>|CsaCollectionGetByCreatedByUserId", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(Csa child in entity.CsaCollectionGetByCreatedByUserId)
					{
						if(child.CreatedByUserIdSource != null)
						{
							child.CreatedByUserId = child.CreatedByUserIdSource.UserId;
						}
						else
						{
							child.CreatedByUserId = entity.UserId;
						}

					}

					if (entity.CsaCollectionGetByCreatedByUserId.Count > 0 || entity.CsaCollectionGetByCreatedByUserId.DeletedItems.Count > 0)
					{
						//DataRepository.CsaProvider.Save(transactionManager, entity.CsaCollectionGetByCreatedByUserId);
						
						deepHandles.Add("CsaCollectionGetByCreatedByUserId",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< Csa >) DataRepository.CsaProvider.DeepSave,
							new object[] { transactionManager, entity.CsaCollectionGetByCreatedByUserId, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<QuestionnaireVerificationAssessment>
				if (CanDeepSave(entity.QuestionnaireVerificationAssessmentCollection, "List<QuestionnaireVerificationAssessment>|QuestionnaireVerificationAssessmentCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(QuestionnaireVerificationAssessment child in entity.QuestionnaireVerificationAssessmentCollection)
					{
						if(child.ModifiedByUserIdSource != null)
						{
							child.ModifiedByUserId = child.ModifiedByUserIdSource.UserId;
						}
						else
						{
							child.ModifiedByUserId = entity.UserId;
						}

					}

					if (entity.QuestionnaireVerificationAssessmentCollection.Count > 0 || entity.QuestionnaireVerificationAssessmentCollection.DeletedItems.Count > 0)
					{
						//DataRepository.QuestionnaireVerificationAssessmentProvider.Save(transactionManager, entity.QuestionnaireVerificationAssessmentCollection);
						
						deepHandles.Add("QuestionnaireVerificationAssessmentCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< QuestionnaireVerificationAssessment >) DataRepository.QuestionnaireVerificationAssessmentProvider.DeepSave,
							new object[] { transactionManager, entity.QuestionnaireVerificationAssessmentCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<CsmsFile>
				if (CanDeepSave(entity.CsmsFileCollectionGetByModifiedByUserId, "List<CsmsFile>|CsmsFileCollectionGetByModifiedByUserId", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(CsmsFile child in entity.CsmsFileCollectionGetByModifiedByUserId)
					{
						if(child.ModifiedByUserIdSource != null)
						{
							child.ModifiedByUserId = child.ModifiedByUserIdSource.UserId;
						}
						else
						{
							child.ModifiedByUserId = entity.UserId;
						}

					}

					if (entity.CsmsFileCollectionGetByModifiedByUserId.Count > 0 || entity.CsmsFileCollectionGetByModifiedByUserId.DeletedItems.Count > 0)
					{
						//DataRepository.CsmsFileProvider.Save(transactionManager, entity.CsmsFileCollectionGetByModifiedByUserId);
						
						deepHandles.Add("CsmsFileCollectionGetByModifiedByUserId",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< CsmsFile >) DataRepository.CsmsFileProvider.DeepSave,
							new object[] { transactionManager, entity.CsmsFileCollectionGetByModifiedByUserId, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<Sites>
				if (CanDeepSave(entity.SitesCollection, "List<Sites>|SitesCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(Sites child in entity.SitesCollection)
					{
						if(child.ModifiedbyUserIdSource != null)
						{
							child.ModifiedbyUserId = child.ModifiedbyUserIdSource.UserId;
						}
						else
						{
							child.ModifiedbyUserId = entity.UserId;
						}

					}

					if (entity.SitesCollection.Count > 0 || entity.SitesCollection.DeletedItems.Count > 0)
					{
						//DataRepository.SitesProvider.Save(transactionManager, entity.SitesCollection);
						
						deepHandles.Add("SitesCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< Sites >) DataRepository.SitesProvider.DeepSave,
							new object[] { transactionManager, entity.SitesCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<DocumentsDownloadLog>
				if (CanDeepSave(entity.DocumentsDownloadLogCollection, "List<DocumentsDownloadLog>|DocumentsDownloadLogCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(DocumentsDownloadLog child in entity.DocumentsDownloadLogCollection)
					{
						if(child.DownloadedByUserIdSource != null)
						{
							child.DownloadedByUserId = child.DownloadedByUserIdSource.UserId;
						}
						else
						{
							child.DownloadedByUserId = entity.UserId;
						}

					}

					if (entity.DocumentsDownloadLogCollection.Count > 0 || entity.DocumentsDownloadLogCollection.DeletedItems.Count > 0)
					{
						//DataRepository.DocumentsDownloadLogProvider.Save(transactionManager, entity.DocumentsDownloadLogCollection);
						
						deepHandles.Add("DocumentsDownloadLogCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< DocumentsDownloadLog >) DataRepository.DocumentsDownloadLogProvider.DeepSave,
							new object[] { transactionManager, entity.DocumentsDownloadLogCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<QuestionnaireComments>
				if (CanDeepSave(entity.QuestionnaireCommentsCollectionGetByCreatedByUserId, "List<QuestionnaireComments>|QuestionnaireCommentsCollectionGetByCreatedByUserId", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(QuestionnaireComments child in entity.QuestionnaireCommentsCollectionGetByCreatedByUserId)
					{
						if(child.CreatedByUserIdSource != null)
						{
							child.CreatedByUserId = child.CreatedByUserIdSource.UserId;
						}
						else
						{
							child.CreatedByUserId = entity.UserId;
						}

					}

					if (entity.QuestionnaireCommentsCollectionGetByCreatedByUserId.Count > 0 || entity.QuestionnaireCommentsCollectionGetByCreatedByUserId.DeletedItems.Count > 0)
					{
						//DataRepository.QuestionnaireCommentsProvider.Save(transactionManager, entity.QuestionnaireCommentsCollectionGetByCreatedByUserId);
						
						deepHandles.Add("QuestionnaireCommentsCollectionGetByCreatedByUserId",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< QuestionnaireComments >) DataRepository.QuestionnaireCommentsProvider.DeepSave,
							new object[] { transactionManager, entity.QuestionnaireCommentsCollectionGetByCreatedByUserId, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<CompanySiteCategoryStandard>
				if (CanDeepSave(entity.CompanySiteCategoryStandardCollectionGetByModifiedByUserId, "List<CompanySiteCategoryStandard>|CompanySiteCategoryStandardCollectionGetByModifiedByUserId", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(CompanySiteCategoryStandard child in entity.CompanySiteCategoryStandardCollectionGetByModifiedByUserId)
					{
						if(child.ModifiedByUserIdSource != null)
						{
							child.ModifiedByUserId = child.ModifiedByUserIdSource.UserId;
						}
						else
						{
							child.ModifiedByUserId = entity.UserId;
						}

					}

					if (entity.CompanySiteCategoryStandardCollectionGetByModifiedByUserId.Count > 0 || entity.CompanySiteCategoryStandardCollectionGetByModifiedByUserId.DeletedItems.Count > 0)
					{
						//DataRepository.CompanySiteCategoryStandardProvider.Save(transactionManager, entity.CompanySiteCategoryStandardCollectionGetByModifiedByUserId);
						
						deepHandles.Add("CompanySiteCategoryStandardCollectionGetByModifiedByUserId",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< CompanySiteCategoryStandard >) DataRepository.CompanySiteCategoryStandardProvider.DeepSave,
							new object[] { transactionManager, entity.CompanySiteCategoryStandardCollectionGetByModifiedByUserId, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<CustomReportingLayouts>
				if (CanDeepSave(entity.CustomReportingLayoutsCollection, "List<CustomReportingLayouts>|CustomReportingLayoutsCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(CustomReportingLayouts child in entity.CustomReportingLayoutsCollection)
					{
						if(child.ModifiedByUserIdSource != null)
						{
							child.ModifiedByUserId = child.ModifiedByUserIdSource.UserId;
						}
						else
						{
							child.ModifiedByUserId = entity.UserId;
						}

					}

					if (entity.CustomReportingLayoutsCollection.Count > 0 || entity.CustomReportingLayoutsCollection.DeletedItems.Count > 0)
					{
						//DataRepository.CustomReportingLayoutsProvider.Save(transactionManager, entity.CustomReportingLayoutsCollection);
						
						deepHandles.Add("CustomReportingLayoutsCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< CustomReportingLayouts >) DataRepository.CustomReportingLayoutsProvider.DeepSave,
							new object[] { transactionManager, entity.CustomReportingLayoutsCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<QuestionnaireComments>
				if (CanDeepSave(entity.QuestionnaireCommentsCollectionGetByModifiedByUserId, "List<QuestionnaireComments>|QuestionnaireCommentsCollectionGetByModifiedByUserId", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(QuestionnaireComments child in entity.QuestionnaireCommentsCollectionGetByModifiedByUserId)
					{
						if(child.ModifiedByUserIdSource != null)
						{
							child.ModifiedByUserId = child.ModifiedByUserIdSource.UserId;
						}
						else
						{
							child.ModifiedByUserId = entity.UserId;
						}

					}

					if (entity.QuestionnaireCommentsCollectionGetByModifiedByUserId.Count > 0 || entity.QuestionnaireCommentsCollectionGetByModifiedByUserId.DeletedItems.Count > 0)
					{
						//DataRepository.QuestionnaireCommentsProvider.Save(transactionManager, entity.QuestionnaireCommentsCollectionGetByModifiedByUserId);
						
						deepHandles.Add("QuestionnaireCommentsCollectionGetByModifiedByUserId",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< QuestionnaireComments >) DataRepository.QuestionnaireCommentsProvider.DeepSave,
							new object[] { transactionManager, entity.QuestionnaireCommentsCollectionGetByModifiedByUserId, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<TwentyOnePointAudit>
				if (CanDeepSave(entity.TwentyOnePointAuditCollectionGetByCreatedbyUserId, "List<TwentyOnePointAudit>|TwentyOnePointAuditCollectionGetByCreatedbyUserId", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(TwentyOnePointAudit child in entity.TwentyOnePointAuditCollectionGetByCreatedbyUserId)
					{
						if(child.CreatedbyUserIdSource != null)
						{
							child.CreatedbyUserId = child.CreatedbyUserIdSource.UserId;
						}
						else
						{
							child.CreatedbyUserId = entity.UserId;
						}

					}

					if (entity.TwentyOnePointAuditCollectionGetByCreatedbyUserId.Count > 0 || entity.TwentyOnePointAuditCollectionGetByCreatedbyUserId.DeletedItems.Count > 0)
					{
						//DataRepository.TwentyOnePointAuditProvider.Save(transactionManager, entity.TwentyOnePointAuditCollectionGetByCreatedbyUserId);
						
						deepHandles.Add("TwentyOnePointAuditCollectionGetByCreatedbyUserId",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< TwentyOnePointAudit >) DataRepository.TwentyOnePointAuditProvider.DeepSave,
							new object[] { transactionManager, entity.TwentyOnePointAuditCollectionGetByCreatedbyUserId, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<SqExemption>
				if (CanDeepSave(entity.SqExemptionCollection, "List<SqExemption>|SqExemptionCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(SqExemption child in entity.SqExemptionCollection)
					{
						if(child.ModifiedByUserIdSource != null)
						{
							child.ModifiedByUserId = child.ModifiedByUserIdSource.UserId;
						}
						else
						{
							child.ModifiedByUserId = entity.UserId;
						}

					}

					if (entity.SqExemptionCollection.Count > 0 || entity.SqExemptionCollection.DeletedItems.Count > 0)
					{
						//DataRepository.SqExemptionProvider.Save(transactionManager, entity.SqExemptionCollection);
						
						deepHandles.Add("SqExemptionCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< SqExemption >) DataRepository.SqExemptionProvider.DeepSave,
							new object[] { transactionManager, entity.SqExemptionCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<QuestionnaireInitialLocation>
				if (CanDeepSave(entity.QuestionnaireInitialLocationCollectionGetByApprovedByUserId, "List<QuestionnaireInitialLocation>|QuestionnaireInitialLocationCollectionGetByApprovedByUserId", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(QuestionnaireInitialLocation child in entity.QuestionnaireInitialLocationCollectionGetByApprovedByUserId)
					{
						if(child.ApprovedByUserIdSource != null)
						{
							child.ApprovedByUserId = child.ApprovedByUserIdSource.UserId;
						}
						else
						{
							child.ApprovedByUserId = entity.UserId;
						}

					}

					if (entity.QuestionnaireInitialLocationCollectionGetByApprovedByUserId.Count > 0 || entity.QuestionnaireInitialLocationCollectionGetByApprovedByUserId.DeletedItems.Count > 0)
					{
						//DataRepository.QuestionnaireInitialLocationProvider.Save(transactionManager, entity.QuestionnaireInitialLocationCollectionGetByApprovedByUserId);
						
						deepHandles.Add("QuestionnaireInitialLocationCollectionGetByApprovedByUserId",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< QuestionnaireInitialLocation >) DataRepository.QuestionnaireInitialLocationProvider.DeepSave,
							new object[] { transactionManager, entity.QuestionnaireInitialLocationCollectionGetByApprovedByUserId, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<QuestionnaireVerificationAttachment>
				if (CanDeepSave(entity.QuestionnaireVerificationAttachmentCollection, "List<QuestionnaireVerificationAttachment>|QuestionnaireVerificationAttachmentCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(QuestionnaireVerificationAttachment child in entity.QuestionnaireVerificationAttachmentCollection)
					{
						if(child.ModifiedByUserIdSource != null)
						{
							child.ModifiedByUserId = child.ModifiedByUserIdSource.UserId;
						}
						else
						{
							child.ModifiedByUserId = entity.UserId;
						}

					}

					if (entity.QuestionnaireVerificationAttachmentCollection.Count > 0 || entity.QuestionnaireVerificationAttachmentCollection.DeletedItems.Count > 0)
					{
						//DataRepository.QuestionnaireVerificationAttachmentProvider.Save(transactionManager, entity.QuestionnaireVerificationAttachmentCollection);
						
						deepHandles.Add("QuestionnaireVerificationAttachmentCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< QuestionnaireVerificationAttachment >) DataRepository.QuestionnaireVerificationAttachmentProvider.DeepSave,
							new object[] { transactionManager, entity.QuestionnaireVerificationAttachmentCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<Kpi>
				if (CanDeepSave(entity.KpiCollection, "List<Kpi>|KpiCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(Kpi child in entity.KpiCollection)
					{
						if(child.ModifiedbyUserIdSource != null)
						{
							child.ModifiedbyUserId = child.ModifiedbyUserIdSource.UserId;
						}
						else
						{
							child.ModifiedbyUserId = entity.UserId;
						}

					}

					if (entity.KpiCollection.Count > 0 || entity.KpiCollection.DeletedItems.Count > 0)
					{
						//DataRepository.KpiProvider.Save(transactionManager, entity.KpiCollection);
						
						deepHandles.Add("KpiCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< Kpi >) DataRepository.KpiProvider.DeepSave,
							new object[] { transactionManager, entity.KpiCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<QuestionnaireInitialLocation>
				if (CanDeepSave(entity.QuestionnaireInitialLocationCollectionGetByModifiedByUserId, "List<QuestionnaireInitialLocation>|QuestionnaireInitialLocationCollectionGetByModifiedByUserId", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(QuestionnaireInitialLocation child in entity.QuestionnaireInitialLocationCollectionGetByModifiedByUserId)
					{
						if(child.ModifiedByUserIdSource != null)
						{
							child.ModifiedByUserId = child.ModifiedByUserIdSource.UserId;
						}
						else
						{
							child.ModifiedByUserId = entity.UserId;
						}

					}

					if (entity.QuestionnaireInitialLocationCollectionGetByModifiedByUserId.Count > 0 || entity.QuestionnaireInitialLocationCollectionGetByModifiedByUserId.DeletedItems.Count > 0)
					{
						//DataRepository.QuestionnaireInitialLocationProvider.Save(transactionManager, entity.QuestionnaireInitialLocationCollectionGetByModifiedByUserId);
						
						deepHandles.Add("QuestionnaireInitialLocationCollectionGetByModifiedByUserId",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< QuestionnaireInitialLocation >) DataRepository.QuestionnaireInitialLocationProvider.DeepSave,
							new object[] { transactionManager, entity.QuestionnaireInitialLocationCollectionGetByModifiedByUserId, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<Questionnaire>
				if (CanDeepSave(entity.QuestionnaireCollectionGetByMainModifiedByUserId, "List<Questionnaire>|QuestionnaireCollectionGetByMainModifiedByUserId", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(Questionnaire child in entity.QuestionnaireCollectionGetByMainModifiedByUserId)
					{
						if(child.MainModifiedByUserIdSource != null)
						{
							child.MainModifiedByUserId = child.MainModifiedByUserIdSource.UserId;
						}
						else
						{
							child.MainModifiedByUserId = entity.UserId;
						}

					}

					if (entity.QuestionnaireCollectionGetByMainModifiedByUserId.Count > 0 || entity.QuestionnaireCollectionGetByMainModifiedByUserId.DeletedItems.Count > 0)
					{
						//DataRepository.QuestionnaireProvider.Save(transactionManager, entity.QuestionnaireCollectionGetByMainModifiedByUserId);
						
						deepHandles.Add("QuestionnaireCollectionGetByMainModifiedByUserId",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< Questionnaire >) DataRepository.QuestionnaireProvider.DeepSave,
							new object[] { transactionManager, entity.QuestionnaireCollectionGetByMainModifiedByUserId, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<FileVaultTable>
				if (CanDeepSave(entity.FileVaultTableCollection, "List<FileVaultTable>|FileVaultTableCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(FileVaultTable child in entity.FileVaultTableCollection)
					{
						if(child.ModifiedByUserIdSource != null)
						{
							child.ModifiedByUserId = child.ModifiedByUserIdSource.UserId;
						}
						else
						{
							child.ModifiedByUserId = entity.UserId;
						}

					}

					if (entity.FileVaultTableCollection.Count > 0 || entity.FileVaultTableCollection.DeletedItems.Count > 0)
					{
						//DataRepository.FileVaultTableProvider.Save(transactionManager, entity.FileVaultTableCollection);
						
						deepHandles.Add("FileVaultTableCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< FileVaultTable >) DataRepository.FileVaultTableProvider.DeepSave,
							new object[] { transactionManager, entity.FileVaultTableCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<QuestionnaireInitialContact>
				if (CanDeepSave(entity.QuestionnaireInitialContactCollectionGetByModifiedByUserId, "List<QuestionnaireInitialContact>|QuestionnaireInitialContactCollectionGetByModifiedByUserId", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(QuestionnaireInitialContact child in entity.QuestionnaireInitialContactCollectionGetByModifiedByUserId)
					{
						if(child.ModifiedByUserIdSource != null)
						{
							child.ModifiedByUserId = child.ModifiedByUserIdSource.UserId;
						}
						else
						{
							child.ModifiedByUserId = entity.UserId;
						}

					}

					if (entity.QuestionnaireInitialContactCollectionGetByModifiedByUserId.Count > 0 || entity.QuestionnaireInitialContactCollectionGetByModifiedByUserId.DeletedItems.Count > 0)
					{
						//DataRepository.QuestionnaireInitialContactProvider.Save(transactionManager, entity.QuestionnaireInitialContactCollectionGetByModifiedByUserId);
						
						deepHandles.Add("QuestionnaireInitialContactCollectionGetByModifiedByUserId",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< QuestionnaireInitialContact >) DataRepository.QuestionnaireInitialContactProvider.DeepSave,
							new object[] { transactionManager, entity.QuestionnaireInitialContactCollectionGetByModifiedByUserId, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<UserPrivilege>
				if (CanDeepSave(entity.UserPrivilegeCollectionGetByModifiedByUserId, "List<UserPrivilege>|UserPrivilegeCollectionGetByModifiedByUserId", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(UserPrivilege child in entity.UserPrivilegeCollectionGetByModifiedByUserId)
					{
						if(child.ModifiedByUserIdSource != null)
						{
							child.ModifiedByUserId = child.ModifiedByUserIdSource.UserId;
						}
						else
						{
							child.ModifiedByUserId = entity.UserId;
						}

					}

					if (entity.UserPrivilegeCollectionGetByModifiedByUserId.Count > 0 || entity.UserPrivilegeCollectionGetByModifiedByUserId.DeletedItems.Count > 0)
					{
						//DataRepository.UserPrivilegeProvider.Save(transactionManager, entity.UserPrivilegeCollectionGetByModifiedByUserId);
						
						deepHandles.Add("UserPrivilegeCollectionGetByModifiedByUserId",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< UserPrivilege >) DataRepository.UserPrivilegeProvider.DeepSave,
							new object[] { transactionManager, entity.UserPrivilegeCollectionGetByModifiedByUserId, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<CompanyStatusChangeApproval>
				if (CanDeepSave(entity.CompanyStatusChangeApprovalCollectionGetByAssessedByUserId, "List<CompanyStatusChangeApproval>|CompanyStatusChangeApprovalCollectionGetByAssessedByUserId", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(CompanyStatusChangeApproval child in entity.CompanyStatusChangeApprovalCollectionGetByAssessedByUserId)
					{
						if(child.AssessedByUserIdSource != null)
						{
							child.AssessedByUserId = child.AssessedByUserIdSource.UserId;
						}
						else
						{
							child.AssessedByUserId = entity.UserId;
						}

					}

					if (entity.CompanyStatusChangeApprovalCollectionGetByAssessedByUserId.Count > 0 || entity.CompanyStatusChangeApprovalCollectionGetByAssessedByUserId.DeletedItems.Count > 0)
					{
						//DataRepository.CompanyStatusChangeApprovalProvider.Save(transactionManager, entity.CompanyStatusChangeApprovalCollectionGetByAssessedByUserId);
						
						deepHandles.Add("CompanyStatusChangeApprovalCollectionGetByAssessedByUserId",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< CompanyStatusChangeApproval >) DataRepository.CompanyStatusChangeApprovalProvider.DeepSave,
							new object[] { transactionManager, entity.CompanyStatusChangeApprovalCollectionGetByAssessedByUserId, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<FileDbMedicalTraining>
				if (CanDeepSave(entity.FileDbMedicalTrainingCollection, "List<FileDbMedicalTraining>|FileDbMedicalTrainingCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(FileDbMedicalTraining child in entity.FileDbMedicalTrainingCollection)
					{
						if(child.ModifiedByUserIdSource != null)
						{
							child.ModifiedByUserId = child.ModifiedByUserIdSource.UserId;
						}
						else
						{
							child.ModifiedByUserId = entity.UserId;
						}

					}

					if (entity.FileDbMedicalTrainingCollection.Count > 0 || entity.FileDbMedicalTrainingCollection.DeletedItems.Count > 0)
					{
						//DataRepository.FileDbMedicalTrainingProvider.Save(transactionManager, entity.FileDbMedicalTrainingCollection);
						
						deepHandles.Add("FileDbMedicalTrainingCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< FileDbMedicalTraining >) DataRepository.FileDbMedicalTrainingProvider.DeepSave,
							new object[] { transactionManager, entity.FileDbMedicalTrainingCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<QuestionnaireInitialContactEmail>
				if (CanDeepSave(entity.QuestionnaireInitialContactEmailCollection, "List<QuestionnaireInitialContactEmail>|QuestionnaireInitialContactEmailCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(QuestionnaireInitialContactEmail child in entity.QuestionnaireInitialContactEmailCollection)
					{
						if(child.SentByUserIdSource != null)
						{
							child.SentByUserId = child.SentByUserIdSource.UserId;
						}
						else
						{
							child.SentByUserId = entity.UserId;
						}

					}

					if (entity.QuestionnaireInitialContactEmailCollection.Count > 0 || entity.QuestionnaireInitialContactEmailCollection.DeletedItems.Count > 0)
					{
						//DataRepository.QuestionnaireInitialContactEmailProvider.Save(transactionManager, entity.QuestionnaireInitialContactEmailCollection);
						
						deepHandles.Add("QuestionnaireInitialContactEmailCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< QuestionnaireInitialContactEmail >) DataRepository.QuestionnaireInitialContactEmailProvider.DeepSave,
							new object[] { transactionManager, entity.QuestionnaireInitialContactEmailCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<QuestionnaireMainResponse>
				if (CanDeepSave(entity.QuestionnaireMainResponseCollection, "List<QuestionnaireMainResponse>|QuestionnaireMainResponseCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(QuestionnaireMainResponse child in entity.QuestionnaireMainResponseCollection)
					{
						if(child.ModifiedByUserIdSource != null)
						{
							child.ModifiedByUserId = child.ModifiedByUserIdSource.UserId;
						}
						else
						{
							child.ModifiedByUserId = entity.UserId;
						}

					}

					if (entity.QuestionnaireMainResponseCollection.Count > 0 || entity.QuestionnaireMainResponseCollection.DeletedItems.Count > 0)
					{
						//DataRepository.QuestionnaireMainResponseProvider.Save(transactionManager, entity.QuestionnaireMainResponseCollection);
						
						deepHandles.Add("QuestionnaireMainResponseCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< QuestionnaireMainResponse >) DataRepository.QuestionnaireMainResponseProvider.DeepSave,
							new object[] { transactionManager, entity.QuestionnaireMainResponseCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<Questionnaire>
				if (CanDeepSave(entity.QuestionnaireCollectionGetByModifiedByUserId, "List<Questionnaire>|QuestionnaireCollectionGetByModifiedByUserId", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(Questionnaire child in entity.QuestionnaireCollectionGetByModifiedByUserId)
					{
						if(child.ModifiedByUserIdSource != null)
						{
							child.ModifiedByUserId = child.ModifiedByUserIdSource.UserId;
						}
						else
						{
							child.ModifiedByUserId = entity.UserId;
						}

					}

					if (entity.QuestionnaireCollectionGetByModifiedByUserId.Count > 0 || entity.QuestionnaireCollectionGetByModifiedByUserId.DeletedItems.Count > 0)
					{
						//DataRepository.QuestionnaireProvider.Save(transactionManager, entity.QuestionnaireCollectionGetByModifiedByUserId);
						
						deepHandles.Add("QuestionnaireCollectionGetByModifiedByUserId",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< Questionnaire >) DataRepository.QuestionnaireProvider.DeepSave,
							new object[] { transactionManager, entity.QuestionnaireCollectionGetByModifiedByUserId, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<ContactsContractors>
				if (CanDeepSave(entity.ContactsContractorsCollection, "List<ContactsContractors>|ContactsContractorsCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(ContactsContractors child in entity.ContactsContractorsCollection)
					{
						if(child.ModifiedByUserIdSource != null)
						{
							child.ModifiedByUserId = child.ModifiedByUserIdSource.UserId;
						}
						else
						{
							child.ModifiedByUserId = entity.UserId;
						}

					}

					if (entity.ContactsContractorsCollection.Count > 0 || entity.ContactsContractorsCollection.DeletedItems.Count > 0)
					{
						//DataRepository.ContactsContractorsProvider.Save(transactionManager, entity.ContactsContractorsCollection);
						
						deepHandles.Add("ContactsContractorsCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< ContactsContractors >) DataRepository.ContactsContractorsProvider.DeepSave,
							new object[] { transactionManager, entity.ContactsContractorsCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<Csa>
				if (CanDeepSave(entity.CsaCollectionGetByModifiedByUserId, "List<Csa>|CsaCollectionGetByModifiedByUserId", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(Csa child in entity.CsaCollectionGetByModifiedByUserId)
					{
						if(child.ModifiedByUserIdSource != null)
						{
							child.ModifiedByUserId = child.ModifiedByUserIdSource.UserId;
						}
						else
						{
							child.ModifiedByUserId = entity.UserId;
						}

					}

					if (entity.CsaCollectionGetByModifiedByUserId.Count > 0 || entity.CsaCollectionGetByModifiedByUserId.DeletedItems.Count > 0)
					{
						//DataRepository.CsaProvider.Save(transactionManager, entity.CsaCollectionGetByModifiedByUserId);
						
						deepHandles.Add("CsaCollectionGetByModifiedByUserId",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< Csa >) DataRepository.CsaProvider.DeepSave,
							new object[] { transactionManager, entity.CsaCollectionGetByModifiedByUserId, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<QuestionnaireInitialLocation>
				if (CanDeepSave(entity.QuestionnaireInitialLocationCollectionGetBySpa, "List<QuestionnaireInitialLocation>|QuestionnaireInitialLocationCollectionGetBySpa", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(QuestionnaireInitialLocation child in entity.QuestionnaireInitialLocationCollectionGetBySpa)
					{
						if(child.SpaSource != null)
						{
							child.Spa = child.SpaSource.UserId;
						}
						else
						{
							child.Spa = entity.UserId;
						}

					}

					if (entity.QuestionnaireInitialLocationCollectionGetBySpa.Count > 0 || entity.QuestionnaireInitialLocationCollectionGetBySpa.DeletedItems.Count > 0)
					{
						//DataRepository.QuestionnaireInitialLocationProvider.Save(transactionManager, entity.QuestionnaireInitialLocationCollectionGetBySpa);
						
						deepHandles.Add("QuestionnaireInitialLocationCollectionGetBySpa",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< QuestionnaireInitialLocation >) DataRepository.QuestionnaireInitialLocationProvider.DeepSave,
							new object[] { transactionManager, entity.QuestionnaireInitialLocationCollectionGetBySpa, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<AdminTask>
				if (CanDeepSave(entity.AdminTaskCollection, "List<AdminTask>|AdminTaskCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(AdminTask child in entity.AdminTaskCollection)
					{
						if(child.ClosedByUserIdSource != null)
						{
							child.ClosedByUserId = child.ClosedByUserIdSource.UserId;
						}
						else
						{
							child.ClosedByUserId = entity.UserId;
						}

					}

					if (entity.AdminTaskCollection.Count > 0 || entity.AdminTaskCollection.DeletedItems.Count > 0)
					{
						//DataRepository.AdminTaskProvider.Save(transactionManager, entity.AdminTaskCollection);
						
						deepHandles.Add("AdminTaskCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< AdminTask >) DataRepository.AdminTaskProvider.DeepSave,
							new object[] { transactionManager, entity.AdminTaskCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<AccessLogs>
				if (CanDeepSave(entity.AccessLogsCollection, "List<AccessLogs>|AccessLogsCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(AccessLogs child in entity.AccessLogsCollection)
					{
						if(child.UserIdSource != null)
						{
							child.UserId = child.UserIdSource.UserId;
						}
						else
						{
							child.UserId = entity.UserId;
						}

					}

					if (entity.AccessLogsCollection.Count > 0 || entity.AccessLogsCollection.DeletedItems.Count > 0)
					{
						//DataRepository.AccessLogsProvider.Save(transactionManager, entity.AccessLogsCollection);
						
						deepHandles.Add("AccessLogsCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< AccessLogs >) DataRepository.AccessLogsProvider.DeepSave,
							new object[] { transactionManager, entity.AccessLogsCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<UserPrivilege>
				if (CanDeepSave(entity.UserPrivilegeCollectionGetByUserId, "List<UserPrivilege>|UserPrivilegeCollectionGetByUserId", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(UserPrivilege child in entity.UserPrivilegeCollectionGetByUserId)
					{
						if(child.UserIdSource != null)
						{
							child.UserId = child.UserIdSource.UserId;
						}
						else
						{
							child.UserId = entity.UserId;
						}

					}

					if (entity.UserPrivilegeCollectionGetByUserId.Count > 0 || entity.UserPrivilegeCollectionGetByUserId.DeletedItems.Count > 0)
					{
						//DataRepository.UserPrivilegeProvider.Save(transactionManager, entity.UserPrivilegeCollectionGetByUserId);
						
						deepHandles.Add("UserPrivilegeCollectionGetByUserId",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< UserPrivilege >) DataRepository.UserPrivilegeProvider.DeepSave,
							new object[] { transactionManager, entity.UserPrivilegeCollectionGetByUserId, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<UsersProcurement>
				if (CanDeepSave(entity.UsersProcurementCollection, "List<UsersProcurement>|UsersProcurementCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(UsersProcurement child in entity.UsersProcurementCollection)
					{
						if(child.SupervisorUserIdSource != null)
						{
							child.SupervisorUserId = child.SupervisorUserIdSource.UserId;
						}
						else
						{
							child.SupervisorUserId = entity.UserId;
						}

					}

					if (entity.UsersProcurementCollection.Count > 0 || entity.UsersProcurementCollection.DeletedItems.Count > 0)
					{
						//DataRepository.UsersProcurementProvider.Save(transactionManager, entity.UsersProcurementCollection);
						
						deepHandles.Add("UsersProcurementCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< UsersProcurement >) DataRepository.UsersProcurementProvider.DeepSave,
							new object[] { transactionManager, entity.UsersProcurementCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<Questionnaire>
				if (CanDeepSave(entity.QuestionnaireCollectionGetByCreatedByUserId, "List<Questionnaire>|QuestionnaireCollectionGetByCreatedByUserId", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(Questionnaire child in entity.QuestionnaireCollectionGetByCreatedByUserId)
					{
						if(child.CreatedByUserIdSource != null)
						{
							child.CreatedByUserId = child.CreatedByUserIdSource.UserId;
						}
						else
						{
							child.CreatedByUserId = entity.UserId;
						}

					}

					if (entity.QuestionnaireCollectionGetByCreatedByUserId.Count > 0 || entity.QuestionnaireCollectionGetByCreatedByUserId.DeletedItems.Count > 0)
					{
						//DataRepository.QuestionnaireProvider.Save(transactionManager, entity.QuestionnaireCollectionGetByCreatedByUserId);
						
						deepHandles.Add("QuestionnaireCollectionGetByCreatedByUserId",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< Questionnaire >) DataRepository.QuestionnaireProvider.DeepSave,
							new object[] { transactionManager, entity.QuestionnaireCollectionGetByCreatedByUserId, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<Users>
				if (CanDeepSave(entity.UsersCollection, "List<Users>|UsersCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(Users child in entity.UsersCollection)
					{
						if(child.ModifiedByUserIdSource != null)
						{
							child.ModifiedByUserId = child.ModifiedByUserIdSource.UserId;
						}
						else
						{
							child.ModifiedByUserId = entity.UserId;
						}

					}

					if (entity.UsersCollection.Count > 0 || entity.UsersCollection.DeletedItems.Count > 0)
					{
						//DataRepository.UsersProvider.Save(transactionManager, entity.UsersCollection);
						
						deepHandles.Add("UsersCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< Users >) DataRepository.UsersProvider.DeepSave,
							new object[] { transactionManager, entity.UsersCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<EhsConsultant>
				if (CanDeepSave(entity.EhsConsultantCollection, "List<EhsConsultant>|EhsConsultantCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(EhsConsultant child in entity.EhsConsultantCollection)
					{
						if(child.SupervisorUserIdSource != null)
						{
							child.SupervisorUserId = child.SupervisorUserIdSource.UserId;
						}
						else
						{
							child.SupervisorUserId = entity.UserId;
						}

					}

					if (entity.EhsConsultantCollection.Count > 0 || entity.EhsConsultantCollection.DeletedItems.Count > 0)
					{
						//DataRepository.EhsConsultantProvider.Save(transactionManager, entity.EhsConsultantCollection);
						
						deepHandles.Add("EhsConsultantCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< EhsConsultant >) DataRepository.EhsConsultantProvider.DeepSave,
							new object[] { transactionManager, entity.EhsConsultantCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<Questionnaire>
				if (CanDeepSave(entity.QuestionnaireCollectionGetByApprovedByUserId, "List<Questionnaire>|QuestionnaireCollectionGetByApprovedByUserId", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(Questionnaire child in entity.QuestionnaireCollectionGetByApprovedByUserId)
					{
						if(child.ApprovedByUserIdSource != null)
						{
							child.ApprovedByUserId = child.ApprovedByUserIdSource.UserId;
						}
						else
						{
							child.ApprovedByUserId = entity.UserId;
						}

					}

					if (entity.QuestionnaireCollectionGetByApprovedByUserId.Count > 0 || entity.QuestionnaireCollectionGetByApprovedByUserId.DeletedItems.Count > 0)
					{
						//DataRepository.QuestionnaireProvider.Save(transactionManager, entity.QuestionnaireCollectionGetByApprovedByUserId);
						
						deepHandles.Add("QuestionnaireCollectionGetByApprovedByUserId",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< Questionnaire >) DataRepository.QuestionnaireProvider.DeepSave,
							new object[] { transactionManager, entity.QuestionnaireCollectionGetByApprovedByUserId, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<QuestionnaireVerificationResponse>
				if (CanDeepSave(entity.QuestionnaireVerificationResponseCollection, "List<QuestionnaireVerificationResponse>|QuestionnaireVerificationResponseCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(QuestionnaireVerificationResponse child in entity.QuestionnaireVerificationResponseCollection)
					{
						if(child.ModifiedByUserIdSource != null)
						{
							child.ModifiedByUserId = child.ModifiedByUserIdSource.UserId;
						}
						else
						{
							child.ModifiedByUserId = entity.UserId;
						}

					}

					if (entity.QuestionnaireVerificationResponseCollection.Count > 0 || entity.QuestionnaireVerificationResponseCollection.DeletedItems.Count > 0)
					{
						//DataRepository.QuestionnaireVerificationResponseProvider.Save(transactionManager, entity.QuestionnaireVerificationResponseCollection);
						
						deepHandles.Add("QuestionnaireVerificationResponseCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< QuestionnaireVerificationResponse >) DataRepository.QuestionnaireVerificationResponseProvider.DeepSave,
							new object[] { transactionManager, entity.QuestionnaireVerificationResponseCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<Questionnaire>
				if (CanDeepSave(entity.QuestionnaireCollectionGetByMainAssessmentByUserId, "List<Questionnaire>|QuestionnaireCollectionGetByMainAssessmentByUserId", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(Questionnaire child in entity.QuestionnaireCollectionGetByMainAssessmentByUserId)
					{
						if(child.MainAssessmentByUserIdSource != null)
						{
							child.MainAssessmentByUserId = child.MainAssessmentByUserIdSource.UserId;
						}
						else
						{
							child.MainAssessmentByUserId = entity.UserId;
						}

					}

					if (entity.QuestionnaireCollectionGetByMainAssessmentByUserId.Count > 0 || entity.QuestionnaireCollectionGetByMainAssessmentByUserId.DeletedItems.Count > 0)
					{
						//DataRepository.QuestionnaireProvider.Save(transactionManager, entity.QuestionnaireCollectionGetByMainAssessmentByUserId);
						
						deepHandles.Add("QuestionnaireCollectionGetByMainAssessmentByUserId",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< Questionnaire >) DataRepository.QuestionnaireProvider.DeepSave,
							new object[] { transactionManager, entity.QuestionnaireCollectionGetByMainAssessmentByUserId, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<Companies>
				if (CanDeepSave(entity.CompaniesCollection, "List<Companies>|CompaniesCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(Companies child in entity.CompaniesCollection)
					{
						if(child.ModifiedbyUserIdSource != null)
						{
							child.ModifiedbyUserId = child.ModifiedbyUserIdSource.UserId;
						}
						else
						{
							child.ModifiedbyUserId = entity.UserId;
						}

					}

					if (entity.CompaniesCollection.Count > 0 || entity.CompaniesCollection.DeletedItems.Count > 0)
					{
						//DataRepository.CompaniesProvider.Save(transactionManager, entity.CompaniesCollection);
						
						deepHandles.Add("CompaniesCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< Companies >) DataRepository.CompaniesProvider.DeepSave,
							new object[] { transactionManager, entity.CompaniesCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<CompanySiteCategoryStandard>
				if (CanDeepSave(entity.CompanySiteCategoryStandardCollectionGetByArpUserId, "List<CompanySiteCategoryStandard>|CompanySiteCategoryStandardCollectionGetByArpUserId", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(CompanySiteCategoryStandard child in entity.CompanySiteCategoryStandardCollectionGetByArpUserId)
					{
						if(child.ArpUserIdSource != null)
						{
							child.ArpUserId = child.ArpUserIdSource.UserId;
						}
						else
						{
							child.ArpUserId = entity.UserId;
						}

					}

					if (entity.CompanySiteCategoryStandardCollectionGetByArpUserId.Count > 0 || entity.CompanySiteCategoryStandardCollectionGetByArpUserId.DeletedItems.Count > 0)
					{
						//DataRepository.CompanySiteCategoryStandardProvider.Save(transactionManager, entity.CompanySiteCategoryStandardCollectionGetByArpUserId);
						
						deepHandles.Add("CompanySiteCategoryStandardCollectionGetByArpUserId",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< CompanySiteCategoryStandard >) DataRepository.CompanySiteCategoryStandardProvider.DeepSave,
							new object[] { transactionManager, entity.CompanySiteCategoryStandardCollectionGetByArpUserId, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<FileDb>
				if (CanDeepSave(entity.FileDbCollectionGetByStatusModifiedByUserId, "List<FileDb>|FileDbCollectionGetByStatusModifiedByUserId", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(FileDb child in entity.FileDbCollectionGetByStatusModifiedByUserId)
					{
						if(child.StatusModifiedByUserIdSource != null)
						{
							child.StatusModifiedByUserId = child.StatusModifiedByUserIdSource.UserId;
						}
						else
						{
							child.StatusModifiedByUserId = entity.UserId;
						}

					}

					if (entity.FileDbCollectionGetByStatusModifiedByUserId.Count > 0 || entity.FileDbCollectionGetByStatusModifiedByUserId.DeletedItems.Count > 0)
					{
						//DataRepository.FileDbProvider.Save(transactionManager, entity.FileDbCollectionGetByStatusModifiedByUserId);
						
						deepHandles.Add("FileDbCollectionGetByStatusModifiedByUserId",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< FileDb >) DataRepository.FileDbProvider.DeepSave,
							new object[] { transactionManager, entity.FileDbCollectionGetByStatusModifiedByUserId, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<ContactsAlcoa>
				if (CanDeepSave(entity.ContactsAlcoaCollection, "List<ContactsAlcoa>|ContactsAlcoaCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(ContactsAlcoa child in entity.ContactsAlcoaCollection)
					{
						if(child.ModifiedByUserIdSource != null)
						{
							child.ModifiedByUserId = child.ModifiedByUserIdSource.UserId;
						}
						else
						{
							child.ModifiedByUserId = entity.UserId;
						}

					}

					if (entity.ContactsAlcoaCollection.Count > 0 || entity.ContactsAlcoaCollection.DeletedItems.Count > 0)
					{
						//DataRepository.ContactsAlcoaProvider.Save(transactionManager, entity.ContactsAlcoaCollection);
						
						deepHandles.Add("ContactsAlcoaCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< ContactsAlcoa >) DataRepository.ContactsAlcoaProvider.DeepSave,
							new object[] { transactionManager, entity.ContactsAlcoaCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<UserPrivilege>
				if (CanDeepSave(entity.UserPrivilegeCollectionGetByCreatedByUserId, "List<UserPrivilege>|UserPrivilegeCollectionGetByCreatedByUserId", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(UserPrivilege child in entity.UserPrivilegeCollectionGetByCreatedByUserId)
					{
						if(child.CreatedByUserIdSource != null)
						{
							child.CreatedByUserId = child.CreatedByUserIdSource.UserId;
						}
						else
						{
							child.CreatedByUserId = entity.UserId;
						}

					}

					if (entity.UserPrivilegeCollectionGetByCreatedByUserId.Count > 0 || entity.UserPrivilegeCollectionGetByCreatedByUserId.DeletedItems.Count > 0)
					{
						//DataRepository.UserPrivilegeProvider.Save(transactionManager, entity.UserPrivilegeCollectionGetByCreatedByUserId);
						
						deepHandles.Add("UserPrivilegeCollectionGetByCreatedByUserId",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< UserPrivilege >) DataRepository.UserPrivilegeProvider.DeepSave,
							new object[] { transactionManager, entity.UserPrivilegeCollectionGetByCreatedByUserId, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<TwentyOnePointAudit>
				if (CanDeepSave(entity.TwentyOnePointAuditCollectionGetByModifiedbyUserId, "List<TwentyOnePointAudit>|TwentyOnePointAuditCollectionGetByModifiedbyUserId", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(TwentyOnePointAudit child in entity.TwentyOnePointAuditCollectionGetByModifiedbyUserId)
					{
						if(child.ModifiedbyUserIdSource != null)
						{
							child.ModifiedbyUserId = child.ModifiedbyUserIdSource.UserId;
						}
						else
						{
							child.ModifiedbyUserId = entity.UserId;
						}

					}

					if (entity.TwentyOnePointAuditCollectionGetByModifiedbyUserId.Count > 0 || entity.TwentyOnePointAuditCollectionGetByModifiedbyUserId.DeletedItems.Count > 0)
					{
						//DataRepository.TwentyOnePointAuditProvider.Save(transactionManager, entity.TwentyOnePointAuditCollectionGetByModifiedbyUserId);
						
						deepHandles.Add("TwentyOnePointAuditCollectionGetByModifiedbyUserId",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< TwentyOnePointAudit >) DataRepository.TwentyOnePointAuditProvider.DeepSave,
							new object[] { transactionManager, entity.TwentyOnePointAuditCollectionGetByModifiedbyUserId, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<FileDb>
				if (CanDeepSave(entity.FileDbCollectionGetByModifiedByUserId, "List<FileDb>|FileDbCollectionGetByModifiedByUserId", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(FileDb child in entity.FileDbCollectionGetByModifiedByUserId)
					{
						if(child.ModifiedByUserIdSource != null)
						{
							child.ModifiedByUserId = child.ModifiedByUserIdSource.UserId;
						}
						else
						{
							child.ModifiedByUserId = entity.UserId;
						}

					}

					if (entity.FileDbCollectionGetByModifiedByUserId.Count > 0 || entity.FileDbCollectionGetByModifiedByUserId.DeletedItems.Count > 0)
					{
						//DataRepository.FileDbProvider.Save(transactionManager, entity.FileDbCollectionGetByModifiedByUserId);
						
						deepHandles.Add("FileDbCollectionGetByModifiedByUserId",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< FileDb >) DataRepository.FileDbProvider.DeepSave,
							new object[] { transactionManager, entity.FileDbCollectionGetByModifiedByUserId, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<QuestionnaireMainAttachment>
				if (CanDeepSave(entity.QuestionnaireMainAttachmentCollection, "List<QuestionnaireMainAttachment>|QuestionnaireMainAttachmentCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(QuestionnaireMainAttachment child in entity.QuestionnaireMainAttachmentCollection)
					{
						if(child.ModifiedByUserIdSource != null)
						{
							child.ModifiedByUserId = child.ModifiedByUserIdSource.UserId;
						}
						else
						{
							child.ModifiedByUserId = entity.UserId;
						}

					}

					if (entity.QuestionnaireMainAttachmentCollection.Count > 0 || entity.QuestionnaireMainAttachmentCollection.DeletedItems.Count > 0)
					{
						//DataRepository.QuestionnaireMainAttachmentProvider.Save(transactionManager, entity.QuestionnaireMainAttachmentCollection);
						
						deepHandles.Add("QuestionnaireMainAttachmentCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< QuestionnaireMainAttachment >) DataRepository.QuestionnaireMainAttachmentProvider.DeepSave,
							new object[] { transactionManager, entity.QuestionnaireMainAttachmentCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<CompanySiteCategoryStandard>
				if (CanDeepSave(entity.CompanySiteCategoryStandardCollectionGetByLocationSponsorUserId, "List<CompanySiteCategoryStandard>|CompanySiteCategoryStandardCollectionGetByLocationSponsorUserId", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(CompanySiteCategoryStandard child in entity.CompanySiteCategoryStandardCollectionGetByLocationSponsorUserId)
					{
						if(child.LocationSponsorUserIdSource != null)
						{
							child.LocationSponsorUserId = child.LocationSponsorUserIdSource.UserId;
						}
						else
						{
							child.LocationSponsorUserId = entity.UserId;
						}

					}

					if (entity.CompanySiteCategoryStandardCollectionGetByLocationSponsorUserId.Count > 0 || entity.CompanySiteCategoryStandardCollectionGetByLocationSponsorUserId.DeletedItems.Count > 0)
					{
						//DataRepository.CompanySiteCategoryStandardProvider.Save(transactionManager, entity.CompanySiteCategoryStandardCollectionGetByLocationSponsorUserId);
						
						deepHandles.Add("CompanySiteCategoryStandardCollectionGetByLocationSponsorUserId",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< CompanySiteCategoryStandard >) DataRepository.CompanySiteCategoryStandardProvider.DeepSave,
							new object[] { transactionManager, entity.CompanySiteCategoryStandardCollectionGetByLocationSponsorUserId, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<FileVault>
				if (CanDeepSave(entity.FileVaultCollection, "List<FileVault>|FileVaultCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(FileVault child in entity.FileVaultCollection)
					{
						if(child.ModifiedByUserIdSource != null)
						{
							child.ModifiedByUserId = child.ModifiedByUserIdSource.UserId;
						}
						else
						{
							child.ModifiedByUserId = entity.UserId;
						}

					}

					if (entity.FileVaultCollection.Count > 0 || entity.FileVaultCollection.DeletedItems.Count > 0)
					{
						//DataRepository.FileVaultProvider.Save(transactionManager, entity.FileVaultCollection);
						
						deepHandles.Add("FileVaultCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< FileVault >) DataRepository.FileVaultProvider.DeepSave,
							new object[] { transactionManager, entity.FileVaultCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<CsmsFile>
				if (CanDeepSave(entity.CsmsFileCollectionGetByCreatedByUserId, "List<CsmsFile>|CsmsFileCollectionGetByCreatedByUserId", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(CsmsFile child in entity.CsmsFileCollectionGetByCreatedByUserId)
					{
						if(child.CreatedByUserIdSource != null)
						{
							child.CreatedByUserId = child.CreatedByUserIdSource.UserId;
						}
						else
						{
							child.CreatedByUserId = entity.UserId;
						}

					}

					if (entity.CsmsFileCollectionGetByCreatedByUserId.Count > 0 || entity.CsmsFileCollectionGetByCreatedByUserId.DeletedItems.Count > 0)
					{
						//DataRepository.CsmsFileProvider.Save(transactionManager, entity.CsmsFileCollectionGetByCreatedByUserId);
						
						deepHandles.Add("CsmsFileCollectionGetByCreatedByUserId",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< CsmsFile >) DataRepository.CsmsFileProvider.DeepSave,
							new object[] { transactionManager, entity.CsmsFileCollectionGetByCreatedByUserId, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<CompaniesRelationship>
				if (CanDeepSave(entity.CompaniesRelationshipCollection, "List<CompaniesRelationship>|CompaniesRelationshipCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(CompaniesRelationship child in entity.CompaniesRelationshipCollection)
					{
					}

					if (entity.CompaniesRelationshipCollection.Count > 0 || entity.CompaniesRelationshipCollection.DeletedItems.Count > 0)
					{
						//DataRepository.CompaniesRelationshipProvider.Save(transactionManager, entity.CompaniesRelationshipCollection);
						
						deepHandles.Add("CompaniesRelationshipCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< CompaniesRelationship >) DataRepository.CompaniesRelationshipProvider.DeepSave,
							new object[] { transactionManager, entity.CompaniesRelationshipCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<EscalationChainSafety>
				if (CanDeepSave(entity.EscalationChainSafetyCollection, "List<EscalationChainSafety>|EscalationChainSafetyCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(EscalationChainSafety child in entity.EscalationChainSafetyCollection)
					{
						if(child.UserIdSource != null)
						{
							child.UserId = child.UserIdSource.UserId;
						}
						else
						{
							child.UserId = entity.UserId;
						}

					}

					if (entity.EscalationChainSafetyCollection.Count > 0 || entity.EscalationChainSafetyCollection.DeletedItems.Count > 0)
					{
						//DataRepository.EscalationChainSafetyProvider.Save(transactionManager, entity.EscalationChainSafetyCollection);
						
						deepHandles.Add("EscalationChainSafetyCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< EscalationChainSafety >) DataRepository.EscalationChainSafetyProvider.DeepSave,
							new object[] { transactionManager, entity.EscalationChainSafetyCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<EscalationChainProcurement>
				if (CanDeepSave(entity.EscalationChainProcurementCollection, "List<EscalationChainProcurement>|EscalationChainProcurementCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(EscalationChainProcurement child in entity.EscalationChainProcurementCollection)
					{
						if(child.UserIdSource != null)
						{
							child.UserId = child.UserIdSource.UserId;
						}
						else
						{
							child.UserId = entity.UserId;
						}

					}

					if (entity.EscalationChainProcurementCollection.Count > 0 || entity.EscalationChainProcurementCollection.DeletedItems.Count > 0)
					{
						//DataRepository.EscalationChainProcurementProvider.Save(transactionManager, entity.EscalationChainProcurementCollection);
						
						deepHandles.Add("EscalationChainProcurementCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< EscalationChainProcurement >) DataRepository.EscalationChainProcurementProvider.DeepSave,
							new object[] { transactionManager, entity.EscalationChainProcurementCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<QuestionnaireInitialResponse>
				if (CanDeepSave(entity.QuestionnaireInitialResponseCollection, "List<QuestionnaireInitialResponse>|QuestionnaireInitialResponseCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(QuestionnaireInitialResponse child in entity.QuestionnaireInitialResponseCollection)
					{
						if(child.ModifiedByUserIdSource != null)
						{
							child.ModifiedByUserId = child.ModifiedByUserIdSource.UserId;
						}
						else
						{
							child.ModifiedByUserId = entity.UserId;
						}

					}

					if (entity.QuestionnaireInitialResponseCollection.Count > 0 || entity.QuestionnaireInitialResponseCollection.DeletedItems.Count > 0)
					{
						//DataRepository.QuestionnaireInitialResponseProvider.Save(transactionManager, entity.QuestionnaireInitialResponseCollection);
						
						deepHandles.Add("QuestionnaireInitialResponseCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< QuestionnaireInitialResponse >) DataRepository.QuestionnaireInitialResponseProvider.DeepSave,
							new object[] { transactionManager, entity.QuestionnaireInitialResponseCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region UsersChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.Users</c>
	///</summary>
	public enum UsersChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>Users</c> at ModifiedByUserIdSource
		///</summary>
		[ChildEntityType(typeof(Users))]
		Users,
			
		///<summary>
		/// Composite Property for <c>Role</c> at RoleIdSource
		///</summary>
		[ChildEntityType(typeof(Role))]
		Role,
			
		///<summary>
		/// Composite Property for <c>Companies</c> at CompanyIdSource
		///</summary>
		[ChildEntityType(typeof(Companies))]
		Companies,
	
		///<summary>
		/// Collection of <c>Users</c> as OneToMany for CompanySiteCategoryStandardCollection
		///</summary>
		[ChildEntityType(typeof(TList<CompanySiteCategoryStandard>))]
		CompanySiteCategoryStandardCollectionGetByApprovedByUserId,

		///<summary>
		/// Collection of <c>Users</c> as OneToMany for QuestionnaireServicesSelectedCollection
		///</summary>
		[ChildEntityType(typeof(TList<QuestionnaireServicesSelected>))]
		QuestionnaireServicesSelectedCollection,

		///<summary>
		/// Collection of <c>Users</c> as OneToMany for QuestionnaireMainAssessmentCollection
		///</summary>
		[ChildEntityType(typeof(TList<QuestionnaireMainAssessment>))]
		QuestionnaireMainAssessmentCollection,

		///<summary>
		/// Collection of <c>Users</c> as OneToMany for ConfigSa812Collection
		///</summary>
		[ChildEntityType(typeof(TList<ConfigSa812>))]
		ConfigSa812Collection,

		///<summary>
		/// Collection of <c>Users</c> as OneToMany for QuestionnaireCollection
		///</summary>
		[ChildEntityType(typeof(TList<Questionnaire>))]
		QuestionnaireCollectionGetByInitialModifiedByUserId,

		///<summary>
		/// Collection of <c>Users</c> as OneToMany for CompanyStatusChangeApprovalCollection
		///</summary>
		[ChildEntityType(typeof(TList<CompanyStatusChangeApproval>))]
		CompanyStatusChangeApprovalCollectionGetByRequestedByUserId,

		///<summary>
		/// Collection of <c>Users</c> as OneToMany for QuestionnaireActionLogCollection
		///</summary>
		[ChildEntityType(typeof(TList<QuestionnaireActionLog>))]
		QuestionnaireActionLogCollection,

		///<summary>
		/// Collection of <c>Users</c> as OneToMany for QuestionnaireInitialContactCollection
		///</summary>
		[ChildEntityType(typeof(TList<QuestionnaireInitialContact>))]
		QuestionnaireInitialContactCollectionGetByCreatedByUserId,

		///<summary>
		/// Collection of <c>Users</c> as OneToMany for QuestionnaireCollection
		///</summary>
		[ChildEntityType(typeof(TList<Questionnaire>))]
		QuestionnaireCollectionGetByVerificationModifiedByUserId,

		///<summary>
		/// Collection of <c>Users</c> as OneToMany for CsaCollection
		///</summary>
		[ChildEntityType(typeof(TList<Csa>))]
		CsaCollectionGetByCreatedByUserId,

		///<summary>
		/// Collection of <c>Users</c> as OneToMany for QuestionnaireVerificationAssessmentCollection
		///</summary>
		[ChildEntityType(typeof(TList<QuestionnaireVerificationAssessment>))]
		QuestionnaireVerificationAssessmentCollection,

		///<summary>
		/// Collection of <c>Users</c> as OneToMany for CsmsFileCollection
		///</summary>
		[ChildEntityType(typeof(TList<CsmsFile>))]
		CsmsFileCollectionGetByModifiedByUserId,

		///<summary>
		/// Collection of <c>Users</c> as OneToMany for SitesCollection
		///</summary>
		[ChildEntityType(typeof(TList<Sites>))]
		SitesCollection,

		///<summary>
		/// Collection of <c>Users</c> as OneToMany for DocumentsDownloadLogCollection
		///</summary>
		[ChildEntityType(typeof(TList<DocumentsDownloadLog>))]
		DocumentsDownloadLogCollection,

		///<summary>
		/// Collection of <c>Users</c> as OneToMany for QuestionnaireCommentsCollection
		///</summary>
		[ChildEntityType(typeof(TList<QuestionnaireComments>))]
		QuestionnaireCommentsCollectionGetByCreatedByUserId,

		///<summary>
		/// Collection of <c>Users</c> as OneToMany for CompanySiteCategoryStandardCollection
		///</summary>
		[ChildEntityType(typeof(TList<CompanySiteCategoryStandard>))]
		CompanySiteCategoryStandardCollectionGetByModifiedByUserId,

		///<summary>
		/// Collection of <c>Users</c> as OneToMany for CustomReportingLayoutsCollection
		///</summary>
		[ChildEntityType(typeof(TList<CustomReportingLayouts>))]
		CustomReportingLayoutsCollection,

		///<summary>
		/// Collection of <c>Users</c> as OneToMany for QuestionnaireCommentsCollection
		///</summary>
		[ChildEntityType(typeof(TList<QuestionnaireComments>))]
		QuestionnaireCommentsCollectionGetByModifiedByUserId,

		///<summary>
		/// Collection of <c>Users</c> as OneToMany for TwentyOnePointAuditCollection
		///</summary>
		[ChildEntityType(typeof(TList<TwentyOnePointAudit>))]
		TwentyOnePointAuditCollectionGetByCreatedbyUserId,

		///<summary>
		/// Collection of <c>Users</c> as OneToMany for SqExemptionCollection
		///</summary>
		[ChildEntityType(typeof(TList<SqExemption>))]
		SqExemptionCollection,

		///<summary>
		/// Collection of <c>Users</c> as OneToMany for QuestionnaireInitialLocationCollection
		///</summary>
		[ChildEntityType(typeof(TList<QuestionnaireInitialLocation>))]
		QuestionnaireInitialLocationCollectionGetByApprovedByUserId,

		///<summary>
		/// Collection of <c>Users</c> as OneToMany for QuestionnaireVerificationAttachmentCollection
		///</summary>
		[ChildEntityType(typeof(TList<QuestionnaireVerificationAttachment>))]
		QuestionnaireVerificationAttachmentCollection,

		///<summary>
		/// Collection of <c>Users</c> as OneToMany for KpiCollection
		///</summary>
		[ChildEntityType(typeof(TList<Kpi>))]
		KpiCollection,

		///<summary>
		/// Collection of <c>Users</c> as OneToMany for QuestionnaireInitialLocationCollection
		///</summary>
		[ChildEntityType(typeof(TList<QuestionnaireInitialLocation>))]
		QuestionnaireInitialLocationCollectionGetByModifiedByUserId,

		///<summary>
		/// Collection of <c>Users</c> as OneToMany for QuestionnaireCollection
		///</summary>
		[ChildEntityType(typeof(TList<Questionnaire>))]
		QuestionnaireCollectionGetByMainModifiedByUserId,
		///<summary>
		/// Entity <c>UsersPrivileged2</c> as OneToOne for UsersPrivileged2
		///</summary>
		[ChildEntityType(typeof(UsersPrivileged2))]
		UsersPrivileged2,

		///<summary>
		/// Collection of <c>Users</c> as OneToMany for FileVaultTableCollection
		///</summary>
		[ChildEntityType(typeof(TList<FileVaultTable>))]
		FileVaultTableCollection,

		///<summary>
		/// Collection of <c>Users</c> as OneToMany for QuestionnaireInitialContactCollection
		///</summary>
		[ChildEntityType(typeof(TList<QuestionnaireInitialContact>))]
		QuestionnaireInitialContactCollectionGetByModifiedByUserId,

		///<summary>
		/// Collection of <c>Users</c> as OneToMany for UserPrivilegeCollection
		///</summary>
		[ChildEntityType(typeof(TList<UserPrivilege>))]
		UserPrivilegeCollectionGetByModifiedByUserId,

		///<summary>
		/// Collection of <c>Users</c> as OneToMany for CompanyStatusChangeApprovalCollection
		///</summary>
		[ChildEntityType(typeof(TList<CompanyStatusChangeApproval>))]
		CompanyStatusChangeApprovalCollectionGetByAssessedByUserId,
		///<summary>
		/// Entity <c>UsersPrivileged</c> as OneToOne for UsersPrivileged
		///</summary>
		[ChildEntityType(typeof(UsersPrivileged))]
		UsersPrivileged,

		///<summary>
		/// Collection of <c>Users</c> as OneToMany for FileDbMedicalTrainingCollection
		///</summary>
		[ChildEntityType(typeof(TList<FileDbMedicalTraining>))]
		FileDbMedicalTrainingCollection,

		///<summary>
		/// Collection of <c>Users</c> as OneToMany for QuestionnaireInitialContactEmailCollection
		///</summary>
		[ChildEntityType(typeof(TList<QuestionnaireInitialContactEmail>))]
		QuestionnaireInitialContactEmailCollection,

		///<summary>
		/// Collection of <c>Users</c> as OneToMany for QuestionnaireMainResponseCollection
		///</summary>
		[ChildEntityType(typeof(TList<QuestionnaireMainResponse>))]
		QuestionnaireMainResponseCollection,

		///<summary>
		/// Collection of <c>Users</c> as OneToMany for QuestionnaireCollection
		///</summary>
		[ChildEntityType(typeof(TList<Questionnaire>))]
		QuestionnaireCollectionGetByModifiedByUserId,

		///<summary>
		/// Collection of <c>Users</c> as OneToMany for ContactsContractorsCollection
		///</summary>
		[ChildEntityType(typeof(TList<ContactsContractors>))]
		ContactsContractorsCollection,

		///<summary>
		/// Collection of <c>Users</c> as OneToMany for CsaCollection
		///</summary>
		[ChildEntityType(typeof(TList<Csa>))]
		CsaCollectionGetByModifiedByUserId,

		///<summary>
		/// Collection of <c>Users</c> as OneToMany for QuestionnaireInitialLocationCollection
		///</summary>
		[ChildEntityType(typeof(TList<QuestionnaireInitialLocation>))]
		QuestionnaireInitialLocationCollectionGetBySpa,

		///<summary>
		/// Collection of <c>Users</c> as OneToMany for AdminTaskCollection
		///</summary>
		[ChildEntityType(typeof(TList<AdminTask>))]
		AdminTaskCollection,

		///<summary>
		/// Collection of <c>Users</c> as OneToMany for AccessLogsCollection
		///</summary>
		[ChildEntityType(typeof(TList<AccessLogs>))]
		AccessLogsCollection,

		///<summary>
		/// Collection of <c>Users</c> as OneToMany for UserPrivilegeCollection
		///</summary>
		[ChildEntityType(typeof(TList<UserPrivilege>))]
		UserPrivilegeCollectionGetByUserId,

		///<summary>
		/// Collection of <c>Users</c> as OneToMany for UsersProcurementCollection
		///</summary>
		[ChildEntityType(typeof(TList<UsersProcurement>))]
		UsersProcurementCollection,

		///<summary>
		/// Collection of <c>Users</c> as OneToMany for QuestionnaireCollection
		///</summary>
		[ChildEntityType(typeof(TList<Questionnaire>))]
		QuestionnaireCollectionGetByCreatedByUserId,

		///<summary>
		/// Collection of <c>Users</c> as OneToMany for UsersCollection
		///</summary>
		[ChildEntityType(typeof(TList<Users>))]
		UsersCollection,

		///<summary>
		/// Collection of <c>Users</c> as OneToMany for EhsConsultantCollection
		///</summary>
		[ChildEntityType(typeof(TList<EhsConsultant>))]
		EhsConsultantCollection,

		///<summary>
		/// Collection of <c>Users</c> as OneToMany for QuestionnaireCollection
		///</summary>
		[ChildEntityType(typeof(TList<Questionnaire>))]
		QuestionnaireCollectionGetByApprovedByUserId,

		///<summary>
		/// Collection of <c>Users</c> as OneToMany for QuestionnaireVerificationResponseCollection
		///</summary>
		[ChildEntityType(typeof(TList<QuestionnaireVerificationResponse>))]
		QuestionnaireVerificationResponseCollection,

		///<summary>
		/// Collection of <c>Users</c> as OneToMany for QuestionnaireCollection
		///</summary>
		[ChildEntityType(typeof(TList<Questionnaire>))]
		QuestionnaireCollectionGetByMainAssessmentByUserId,

		///<summary>
		/// Collection of <c>Users</c> as OneToMany for CompaniesCollection
		///</summary>
		[ChildEntityType(typeof(TList<Companies>))]
		CompaniesCollection,

		///<summary>
		/// Collection of <c>Users</c> as OneToMany for CompanySiteCategoryStandardCollection
		///</summary>
		[ChildEntityType(typeof(TList<CompanySiteCategoryStandard>))]
		CompanySiteCategoryStandardCollectionGetByArpUserId,

		///<summary>
		/// Collection of <c>Users</c> as OneToMany for FileDbCollection
		///</summary>
		[ChildEntityType(typeof(TList<FileDb>))]
		FileDbCollectionGetByStatusModifiedByUserId,

		///<summary>
		/// Collection of <c>Users</c> as OneToMany for ContactsAlcoaCollection
		///</summary>
		[ChildEntityType(typeof(TList<ContactsAlcoa>))]
		ContactsAlcoaCollection,

		///<summary>
		/// Collection of <c>Users</c> as OneToMany for UserPrivilegeCollection
		///</summary>
		[ChildEntityType(typeof(TList<UserPrivilege>))]
		UserPrivilegeCollectionGetByCreatedByUserId,

		///<summary>
		/// Collection of <c>Users</c> as OneToMany for TwentyOnePointAuditCollection
		///</summary>
		[ChildEntityType(typeof(TList<TwentyOnePointAudit>))]
		TwentyOnePointAuditCollectionGetByModifiedbyUserId,

		///<summary>
		/// Collection of <c>Users</c> as OneToMany for FileDbCollection
		///</summary>
		[ChildEntityType(typeof(TList<FileDb>))]
		FileDbCollectionGetByModifiedByUserId,

		///<summary>
		/// Collection of <c>Users</c> as OneToMany for QuestionnaireMainAttachmentCollection
		///</summary>
		[ChildEntityType(typeof(TList<QuestionnaireMainAttachment>))]
		QuestionnaireMainAttachmentCollection,

		///<summary>
		/// Collection of <c>Users</c> as OneToMany for CompanySiteCategoryStandardCollection
		///</summary>
		[ChildEntityType(typeof(TList<CompanySiteCategoryStandard>))]
		CompanySiteCategoryStandardCollectionGetByLocationSponsorUserId,

		///<summary>
		/// Collection of <c>Users</c> as OneToMany for FileVaultCollection
		///</summary>
		[ChildEntityType(typeof(TList<FileVault>))]
		FileVaultCollection,

		///<summary>
		/// Collection of <c>Users</c> as OneToMany for CsmsFileCollection
		///</summary>
		[ChildEntityType(typeof(TList<CsmsFile>))]
		CsmsFileCollectionGetByCreatedByUserId,
		///<summary>
		/// Entity <c>UsersPrivileged3</c> as OneToOne for UsersPrivileged3
		///</summary>
		[ChildEntityType(typeof(UsersPrivileged3))]
		UsersPrivileged3,

		///<summary>
		/// Collection of <c>Users</c> as OneToMany for CompaniesRelationshipCollection
		///</summary>
		[ChildEntityType(typeof(TList<CompaniesRelationship>))]
		CompaniesRelationshipCollection,

		///<summary>
		/// Collection of <c>Users</c> as OneToMany for EscalationChainSafetyCollection
		///</summary>
		[ChildEntityType(typeof(TList<EscalationChainSafety>))]
		EscalationChainSafetyCollection,

		///<summary>
		/// Collection of <c>Users</c> as OneToMany for EscalationChainProcurementCollection
		///</summary>
		[ChildEntityType(typeof(TList<EscalationChainProcurement>))]
		EscalationChainProcurementCollection,

		///<summary>
		/// Collection of <c>Users</c> as OneToMany for QuestionnaireInitialResponseCollection
		///</summary>
		[ChildEntityType(typeof(TList<QuestionnaireInitialResponse>))]
		QuestionnaireInitialResponseCollection,
	}
	
	#endregion UsersChildEntityTypes
	
	#region UsersFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;UsersColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Users"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UsersFilterBuilder : SqlFilterBuilder<UsersColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UsersFilterBuilder class.
		/// </summary>
		public UsersFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the UsersFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public UsersFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the UsersFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public UsersFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion UsersFilterBuilder
	
	#region UsersParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;UsersColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Users"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UsersParameterBuilder : ParameterizedSqlFilterBuilder<UsersColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UsersParameterBuilder class.
		/// </summary>
		public UsersParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the UsersParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public UsersParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the UsersParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public UsersParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion UsersParameterBuilder
	
	#region UsersSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;UsersColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Users"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class UsersSortBuilder : SqlSortBuilder<UsersColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UsersSqlSortBuilder class.
		/// </summary>
		public UsersSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion UsersSortBuilder
	
} // end namespace
