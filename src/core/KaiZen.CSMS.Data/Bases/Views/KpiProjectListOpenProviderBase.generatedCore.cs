﻿#region Using directives

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="KpiProjectListOpenProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract class KpiProjectListOpenProviderBaseCore : EntityViewProviderBase<KpiProjectListOpen>
	{
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions
		
		/*
		///<summary>
		/// Fill an VList&lt;KpiProjectListOpen&gt; From a DataSet
		///</summary>
		/// <param name="dataSet">the DataSet</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList&lt;KpiProjectListOpen&gt;"/></returns>
		protected static VList&lt;KpiProjectListOpen&gt; Fill(DataSet dataSet, VList<KpiProjectListOpen> rows, int start, int pagelen)
		{
			if (dataSet.Tables.Count == 1)
			{
				return Fill(dataSet.Tables[0], rows, start, pagelen);
			}
			else
			{
				return new VList<KpiProjectListOpen>();
			}	
		}
		
		
		///<summary>
		/// Fill an VList&lt;KpiProjectListOpen&gt; From a DataTable
		///</summary>
		/// <param name="dataTable">the DataTable that hold the data.</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList<KpiProjectListOpen>"/></returns>
		protected static VList&lt;KpiProjectListOpen&gt; Fill(DataTable dataTable, VList<KpiProjectListOpen> rows, int start, int pagelen)
		{
			int recordnum = 0;
			
			System.Collections.IEnumerator dataRows =  dataTable.Rows.GetEnumerator();
			
			while (dataRows.MoveNext() && (pagelen != 0))
			{
				if(recordnum >= start)
				{
					DataRow row = (DataRow)dataRows.Current;
				
					KpiProjectListOpen c = new KpiProjectListOpen();
					c.ProjectDescription = (Convert.IsDBNull(row["ProjectDescription"]))?string.Empty:(System.String)row["ProjectDescription"];
					c.AcceptChanges();
					rows.Add(c);
					pagelen -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		*/	
						
		///<summary>
		/// Fill an <see cref="VList&lt;KpiProjectListOpen&gt;"/> From a DataReader.
		///</summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pageLength">number of row.</param>
		///<returns>a <see cref="VList&lt;KpiProjectListOpen&gt;"/></returns>
		protected VList<KpiProjectListOpen> Fill(IDataReader reader, VList<KpiProjectListOpen> rows, int start, int pageLength)
		{
			int recordnum = 0;
			while (reader.Read() && (pageLength != 0))
			{
				if(recordnum >= start)
				{
					KpiProjectListOpen entity = null;
					if (DataRepository.Provider.UseEntityFactory)
					{
						entity = EntityManager.CreateViewEntity<KpiProjectListOpen>("KpiProjectListOpen",  DataRepository.Provider.EntityCreationalFactoryType); 
					}
					else
					{
						entity = new KpiProjectListOpen();
					}
					
					entity.SuppressEntityEvents = true;

					entity.ProjectDescription = (System.String)reader[((int)KpiProjectListOpenColumn.ProjectDescription)];
					//entity.ProjectDescription = (Convert.IsDBNull(reader["ProjectDescription"]))?string.Empty:(System.String)reader["ProjectDescription"];
					entity.AcceptChanges();
					entity.SuppressEntityEvents = false;
					
					rows.Add(entity);
					pageLength -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		
		
		/// <summary>
		/// Refreshes the <see cref="KpiProjectListOpen"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KpiProjectListOpen"/> object to refresh.</param>
		protected void RefreshEntity(IDataReader reader, KpiProjectListOpen entity)
		{
			reader.Read();
			entity.ProjectDescription = (System.String)reader[((int)KpiProjectListOpenColumn.ProjectDescription)];
			//entity.ProjectDescription = (Convert.IsDBNull(reader["ProjectDescription"]))?string.Empty:(System.String)reader["ProjectDescription"];
			reader.Close();
	
			entity.AcceptChanges();
		}
		
		/*
		/// <summary>
		/// Refreshes the <see cref="KpiProjectListOpen"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KpiProjectListOpen"/> object.</param>
		protected static void RefreshEntity(DataSet dataSet, KpiProjectListOpen entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.ProjectDescription = (Convert.IsDBNull(dataRow["ProjectDescription"]))?string.Empty:(System.String)dataRow["ProjectDescription"];
			entity.AcceptChanges();
		}
		*/
			
		#endregion Helper Functions
	}//end class

	#region KpiProjectListOpenFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="KpiProjectListOpen"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class KpiProjectListOpenFilterBuilder : SqlFilterBuilder<KpiProjectListOpenColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the KpiProjectListOpenFilterBuilder class.
		/// </summary>
		public KpiProjectListOpenFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the KpiProjectListOpenFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public KpiProjectListOpenFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the KpiProjectListOpenFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public KpiProjectListOpenFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion KpiProjectListOpenFilterBuilder

	#region KpiProjectListOpenParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="KpiProjectListOpen"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class KpiProjectListOpenParameterBuilder : ParameterizedSqlFilterBuilder<KpiProjectListOpenColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the KpiProjectListOpenParameterBuilder class.
		/// </summary>
		public KpiProjectListOpenParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the KpiProjectListOpenParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public KpiProjectListOpenParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the KpiProjectListOpenParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public KpiProjectListOpenParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion KpiProjectListOpenParameterBuilder
	
	#region KpiProjectListOpenSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="KpiProjectListOpen"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class KpiProjectListOpenSortBuilder : SqlSortBuilder<KpiProjectListOpenColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the KpiProjectListOpenSqlSortBuilder class.
		/// </summary>
		public KpiProjectListOpenSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion KpiProjectListOpenSortBuilder

} // end namespace
