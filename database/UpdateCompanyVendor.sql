USE [ALCOA_WAO_CSMWP]
GO

/****** Object:  StoredProcedure [dbo].[UpdateCompanyVendor]    Script Date: 6/07/2015 11:56:19 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[UpdateCompanyVendor]   
(  
@VENDOR_ID int,  
@VENDOR_NUMBER varchar(max),  
@VENDOR_NAME varchar(max),  
@VENDOR_NAME_ALT varchar(max)  
,@LAST_UPDATE_DATE  datetime          
,@START_DATE_ACTIVE  datetime            
,@END_DATE_ACTIVE  datetime           
,@TAX_REGISTRATION_NUMBER varchar(max)  
)             
AS             
IF EXISTS(SELECT  [VENDOR_ID]              
           ,[VENDOR_NUMBER]              
           ,[VENDOR_NAME]              
           ,[VENDOR_NAME_ALT]              
           ,[LAST_UPDATE_DATE]              
           ,[START_DATE_ACTIVE]              
           ,[END_DATE_ACTIVE]              
           ,[TAX_REGISTRATION_NUMBER]              
FROM EBS_COMM..APPS.XXPO_SQL_VEND_V)        
BEGIN          
 BEGIN TRY          
  BEGIN TRAN          
     
   If Exists(Select * from CompanyVendor where Vendor_id=@VENDOR_ID)  
 update CompanyVendor set   
   [VENDOR_NUMBER] = @VENDOR_NUMBER             
     ,[VENDOR_NAME] = @VENDOR_NAME             
     ,[VENDOR_NAME_ALT]  = @VENDOR_NAME_ALT             
     ,[LAST_UPDATE_DATE] = @LAST_UPDATE_DATE             
     ,[START_DATE_ACTIVE]=  @START_DATE_ACTIVE           
     ,[END_DATE_ACTIVE] = @END_DATE_ACTIVE             
     ,[TAX_REGISTRATION_NUMBER]=@TAX_REGISTRATION_NUMBER  
 where [VENDOR_ID]=@VENDOR_ID  
 else  
   INSERT INTO [dbo].[CompanyVendor]              
     ([VENDOR_ID]              
     ,[VENDOR_NUMBER]              
     ,[VENDOR_NAME]              
     ,[VENDOR_NAME_ALT]              
     ,[LAST_UPDATE_DATE]              
     ,[START_DATE_ACTIVE]              
     ,[END_DATE_ACTIVE]              
     ,[TAX_REGISTRATION_NUMBER])              
   values( @VENDOR_ID ,  
@VENDOR_NUMBER ,  
@VENDOR_NAME ,  
@VENDOR_NAME_ALT   
,@LAST_UPDATE_DATE           
,@START_DATE_ACTIVE          
,@END_DATE_ACTIVE             
,@TAX_REGISTRATION_NUMBER )         
  COMMIT          
           
 END TRY            
 BEGIN CATCH          
  ROLLBACK          
 END CATCH        
END      
-------------------------------------------------------------------------------------------

GO



