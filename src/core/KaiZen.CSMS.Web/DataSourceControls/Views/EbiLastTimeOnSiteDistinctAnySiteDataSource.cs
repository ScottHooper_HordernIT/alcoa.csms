﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.EbiLastTimeOnSiteDistinctAnySiteProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[CLSCompliant(true)]
	[Designer(typeof(EbiLastTimeOnSiteDistinctAnySiteDataSourceDesigner))]
	public class EbiLastTimeOnSiteDistinctAnySiteDataSource : ReadOnlyDataSource<EbiLastTimeOnSiteDistinctAnySite>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EbiLastTimeOnSiteDistinctAnySiteDataSource class.
		/// </summary>
		public EbiLastTimeOnSiteDistinctAnySiteDataSource() : base(new EbiLastTimeOnSiteDistinctAnySiteService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the EbiLastTimeOnSiteDistinctAnySiteDataSourceView used by the EbiLastTimeOnSiteDistinctAnySiteDataSource.
		/// </summary>
		protected EbiLastTimeOnSiteDistinctAnySiteDataSourceView EbiLastTimeOnSiteDistinctAnySiteView
		{
			get { return ( View as EbiLastTimeOnSiteDistinctAnySiteDataSourceView ); }
		}
		
		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the EbiLastTimeOnSiteDistinctAnySiteDataSourceView class that is to be
		/// used by the EbiLastTimeOnSiteDistinctAnySiteDataSource.
		/// </summary>
		/// <returns>An instance of the EbiLastTimeOnSiteDistinctAnySiteDataSourceView class.</returns>
		protected override BaseDataSourceView<EbiLastTimeOnSiteDistinctAnySite, Object> GetNewDataSourceView()
		{
			return new EbiLastTimeOnSiteDistinctAnySiteDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the EbiLastTimeOnSiteDistinctAnySiteDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class EbiLastTimeOnSiteDistinctAnySiteDataSourceView : ReadOnlyDataSourceView<EbiLastTimeOnSiteDistinctAnySite>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EbiLastTimeOnSiteDistinctAnySiteDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the EbiLastTimeOnSiteDistinctAnySiteDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public EbiLastTimeOnSiteDistinctAnySiteDataSourceView(EbiLastTimeOnSiteDistinctAnySiteDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal EbiLastTimeOnSiteDistinctAnySiteDataSource EbiLastTimeOnSiteDistinctAnySiteOwner
		{
			get { return Owner as EbiLastTimeOnSiteDistinctAnySiteDataSource; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal EbiLastTimeOnSiteDistinctAnySiteService EbiLastTimeOnSiteDistinctAnySiteProvider
		{
			get { return Provider as EbiLastTimeOnSiteDistinctAnySiteService; }
		}

		#endregion Properties
		
		#region Methods
		
		#endregion Methods
	}

	#region EbiLastTimeOnSiteDistinctAnySiteDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the EbiLastTimeOnSiteDistinctAnySiteDataSource class.
	/// </summary>
	public class EbiLastTimeOnSiteDistinctAnySiteDataSourceDesigner : ReadOnlyDataSourceDesigner<EbiLastTimeOnSiteDistinctAnySite>
	{
	}

	#endregion EbiLastTimeOnSiteDistinctAnySiteDataSourceDesigner

	#region EbiLastTimeOnSiteDistinctAnySiteFilter

	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EbiLastTimeOnSiteDistinctAnySite"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EbiLastTimeOnSiteDistinctAnySiteFilter : SqlFilter<EbiLastTimeOnSiteDistinctAnySiteColumn>
	{
	}

	#endregion EbiLastTimeOnSiteDistinctAnySiteFilter

	#region EbiLastTimeOnSiteDistinctAnySiteExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EbiLastTimeOnSiteDistinctAnySite"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EbiLastTimeOnSiteDistinctAnySiteExpressionBuilder : SqlExpressionBuilder<EbiLastTimeOnSiteDistinctAnySiteColumn>
	{
	}
	
	#endregion EbiLastTimeOnSiteDistinctAnySiteExpressionBuilder		
}

