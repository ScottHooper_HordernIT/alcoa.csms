﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="UserControls_SummaryComplianceBySite" Codebehind="SummaryComplianceBySite.ascx.cs" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
    <%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dxpc" %>
<%@ Register Src="~/UserControls/Other/Reports_DropDownListPicker2b.ascx" TagName="Reports_DropDownListPicker2b"
    TagPrefix="uc1" %>
    
    <%--<%@ Reference Control="~/UserControls/Main/ComplianceReports/Summary/ComplianceReport_EBI.ascx" %> --%>
    <%@ Reference Control="~/UserControls/Main/ComplianceReports/Summary/ComplianceReport_KPISQ.ascx" %> 
    <%@ Reference Control="~/UserControls/Main/ComplianceReports/Summary/ComplianceReport_MT.ascx" %> 
    <%@ Reference Control="~/UserControls/Main/ComplianceReports/Summary/ComplianceReport_SQ.ascx" %> 
    <%@ Reference Control="~/UserControls/Main/ComplianceReports/Summary/ComplianceReport_SFR.ascx" %> 
    <%@ Reference Control="~/UserControls/Main/ComplianceReports/Summary/ComplianceReport_CSA.ascx" %> 
    <%@ Reference Control="~/UserControls/Main/ComplianceReports/Summary/ComplianceReport_KPI.ascx" %> 
    <%@ Reference Control="~/UserControls/Main/ComplianceReports/Summary/ComplianceReport_MR.ascx" %> 
    <%@ Reference Control="~/UserControls/Main/ComplianceReports/Summary/ComplianceReport_MS.ascx" %> 
    <%@ Reference Control="~/UserControls/Main/ComplianceReports/Summary/ComplianceReport_SP.ascx" %> 
    <%@ Reference Control="~/UserControls/Main/ComplianceReports/Summary/ComplianceReport_TS.ascx" %> 
    <%@ Reference Control="~/UserControls/Main/ComplianceReports/Summary/ComplianceReport_CC.ascx" %> 

<%@ Register assembly="DevExpress.Web.v14.1" namespace="DevExpress.Web.ASPxPopupControl" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>

<table border="0" cellpadding="2" cellspacing="0" width="900px">
    <tr>
        <td class="pageName" colspan="8" style="text-align: left; width: 900px; height: 30px;">
            <span class="bodycopy"><span class="title">Alcoa Contractor Services Management</span><br />
                <span class="date">Summary Compliance Report By Site</span><br />
                <img height="1" src="images/grfc_dottedline.gif" width="24" />
            </span>
        </td>
    </tr>
    <tr>
        <td align="center" style="height: 23px; width: 715px;">
        <span class="title">Quantitative Comparison of Target vs Actual&nbsp; </span>
            <asp:Label ID="Label1" runat="server" Font-Size="Small" ForeColor="Navy" Text="As at: dd/mm/yyyy"></asp:Label>&nbsp;
            </td>
        <td align="right" style="padding-right: 12px; height: 23px; text-align: right">
            <asp:Button ID="Button1" runat="server" OnClientClick="window:print();" Text="Print" Width="100px" /></td>
    </tr>
    <tr>
        <td class="pageName" colspan="8" style="width: 900px; height: 17px; text-align: left">
            <table border="0" cellpadding="0" cellspacing="0" style="width: 900px">
                <tr>
                    <td style="width: 700px">
            <uc1:Reports_DropDownListPicker2b ID="Reports_DropDownListPicker1" runat="server" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    </table>
            <asp:Panel ID="Panel1" runat="server" Visible="false">
        
    <table>
    <tr>
        <td class="pageName" style="width: 900px; height: 17px; text-align: left">
        <br />
            <span style="color: #000099"><span style="font-size: 10pt">
        <strong>KPI</strong><br />
            </span>
            </span>
            <asp:PlaceHolder ID="ph_KPI" runat="server"></asp:PlaceHolder>
        </td>
    </tr>

    <%--ADDED by Sayani for DT2416--%>
    <tr id="SafetyPlan" runat="server" visible="true">
        <td  class="pageName" style="width: 900px; height: 17px; text-align: left">
            <span style="color: #000099">

            <%--Remove By Sayani For task 8--%>
       <%-- <strong>
            <br />
            <span style="font-size: 10pt">
            Safety Management Plans</span></strong><br />
            </span>--%>
            <asp:PlaceHolder ID="ph_SP" runat="server"></asp:PlaceHolder>
        </td>
    </tr>

    <tr>
        <td class="pageName" style="width: 900px; height: 17px; text-align: left">
        <br />
            <span style="color: #000099">
          <strong>
              <span style="font-size: 10pt">
              <asp:Label ID="lblMedicalSchedules" runat="server" Text="Medical Schedules"></asp:Label></span></strong><br />
            </span>
            <asp:PlaceHolder ID="ph_MS" runat="server"></asp:PlaceHolder>
        </td>
    </tr>
    <tr>
        <td class="pageName" style="width: 900px; height: 17px; text-align: left">
            <span style="color: #000099">
                
        <strong>
            <br />
            <span style="font-size: 10pt">
            <asp:Label ID="lblTrainingSchedules" runat="server" Text="Training Schedules"></asp:Label></span></strong><br />
            </span>
            <asp:PlaceHolder ID="ph_TS" runat="server"></asp:PlaceHolder>
        </td>
    </tr>
    <tr>
        <td class="pageName" style="width: 900px; height: 17px; text-align: left">
            <span style="color: #000099">
        <strong>
            <br />
            <span style="font-size: 10pt">
            Contractor Services Audit - Shows Embedded and Non Embedded 1 Sites Only</span></strong><br />
            </span>
            <asp:PlaceHolder ID="ph_CSA" runat="server"></asp:PlaceHolder>
        </td>
    </tr>

    <%--DT 3190 changes to remove the EBI subreport, on the top th reference has also been commented--%>

    <%--<tr>
        <td class="pageName" style="width: 900px; height: 17px; text-align: left">
            <span style="color: #000099">
                
        <strong>
            <br />
            <span style="font-size: 10pt">
            <asp:Label ID="lblContractorsOnSiteEBI" runat="server" Text="Contractors On Site (EBI)"></asp:Label></span></strong><br />
            </span>
            <asp:PlaceHolder ID="ph_EBI" runat="server"></asp:PlaceHolder>
        </td>
    </tr>--%>
    <tr>
        <td class="pageName" style="width: 900px; height: 17px; text-align: left">
            <span style="color: #000099">
                
        <strong>
            <br />
            <span style="font-size: 10pt">
            <asp:Label ID="lblContractorsOnSiteKpi" runat="server" Text="Contractors On Site (KPI)"></asp:Label></span></strong><br />
            </span>
            <asp:PlaceHolder ID="ph_KPISQ" runat="server"></asp:PlaceHolder>
        </td>
    </tr>
    <tr>
        <td align="right" style="padding:4px 0px 4px 0px">
            <asp:UpdatePanel ID="UpdatePanel7" runat="server">
                <ContentTemplate>
                    <dxe:ASPxButton ID="btnPdf" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                        CssPostfix="Office2003Blue" OnClick="btnPdf_Click" Text="Save to PDF" ValidationGroup="site"
                        Width="99px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                    </dxe:ASPxButton>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnPdf" />
                </Triggers>
            </asp:UpdatePanel>                       
        </td>
    </tr>
</table>
</asp:Panel>

<dxpc:aspxpopupcontrol id="ASPxPopupControl1" runat="server" cssfilepath="~/App_Themes/Office2003Blue/{0}/styles.css"
    csspostfix="Office2003Blue" enablehottrack="False" headertext="Warning" 
    height="111px" modal="True" popuphorizontalalign="WindowCenter"
    popupverticalalign="WindowCenter" width="439px" 
    SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
<SizeGripImage Width="16px"></SizeGripImage><HeaderStyle>
<Paddings PaddingRight="6px"></Paddings>
</HeaderStyle>

<CloseButtonImage Width="13px"></CloseButtonImage>

<ContentCollection>
<dxpc:PopupControlContentControl runat="server" SupportsDisabledAttribute="True"><dxe:ASPxLabel runat="server" Height="30px" Font-Size="14px" ID="ASPxLabel1">
<Border BorderColor="White" BorderWidth="10px"></Border>
</dxe:ASPxLabel>
</dxpc:PopupControlContentControl>
</ContentCollection>
</dxpc:aspxpopupcontrol>
