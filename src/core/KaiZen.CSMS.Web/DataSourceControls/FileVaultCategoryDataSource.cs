﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.FileVaultCategoryProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(FileVaultCategoryDataSourceDesigner))]
	public class FileVaultCategoryDataSource : ProviderDataSource<FileVaultCategory, FileVaultCategoryKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FileVaultCategoryDataSource class.
		/// </summary>
		public FileVaultCategoryDataSource() : base(new FileVaultCategoryService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the FileVaultCategoryDataSourceView used by the FileVaultCategoryDataSource.
		/// </summary>
		protected FileVaultCategoryDataSourceView FileVaultCategoryView
		{
			get { return ( View as FileVaultCategoryDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the FileVaultCategoryDataSource control invokes to retrieve data.
		/// </summary>
		public FileVaultCategorySelectMethod SelectMethod
		{
			get
			{
				FileVaultCategorySelectMethod selectMethod = FileVaultCategorySelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (FileVaultCategorySelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the FileVaultCategoryDataSourceView class that is to be
		/// used by the FileVaultCategoryDataSource.
		/// </summary>
		/// <returns>An instance of the FileVaultCategoryDataSourceView class.</returns>
		protected override BaseDataSourceView<FileVaultCategory, FileVaultCategoryKey> GetNewDataSourceView()
		{
			return new FileVaultCategoryDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the FileVaultCategoryDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class FileVaultCategoryDataSourceView : ProviderDataSourceView<FileVaultCategory, FileVaultCategoryKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FileVaultCategoryDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the FileVaultCategoryDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public FileVaultCategoryDataSourceView(FileVaultCategoryDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal FileVaultCategoryDataSource FileVaultCategoryOwner
		{
			get { return Owner as FileVaultCategoryDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal FileVaultCategorySelectMethod SelectMethod
		{
			get { return FileVaultCategoryOwner.SelectMethod; }
			set { FileVaultCategoryOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal FileVaultCategoryService FileVaultCategoryProvider
		{
			get { return Provider as FileVaultCategoryService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<FileVaultCategory> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<FileVaultCategory> results = null;
			FileVaultCategory item;
			count = 0;
			
			System.Int32 _fileVaultCategoryId;
			System.String _categoryName;

			switch ( SelectMethod )
			{
				case FileVaultCategorySelectMethod.Get:
					FileVaultCategoryKey entityKey  = new FileVaultCategoryKey();
					entityKey.Load(values);
					item = FileVaultCategoryProvider.Get(entityKey);
					results = new TList<FileVaultCategory>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case FileVaultCategorySelectMethod.GetAll:
                    results = FileVaultCategoryProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case FileVaultCategorySelectMethod.GetPaged:
					results = FileVaultCategoryProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case FileVaultCategorySelectMethod.Find:
					if ( FilterParameters != null )
						results = FileVaultCategoryProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = FileVaultCategoryProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case FileVaultCategorySelectMethod.GetByFileVaultCategoryId:
					_fileVaultCategoryId = ( values["FileVaultCategoryId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["FileVaultCategoryId"], typeof(System.Int32)) : (int)0;
					item = FileVaultCategoryProvider.GetByFileVaultCategoryId(_fileVaultCategoryId);
					results = new TList<FileVaultCategory>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				case FileVaultCategorySelectMethod.GetByCategoryName:
					_categoryName = ( values["CategoryName"] != null ) ? (System.String) EntityUtil.ChangeType(values["CategoryName"], typeof(System.String)) : string.Empty;
					item = FileVaultCategoryProvider.GetByCategoryName(_categoryName);
					results = new TList<FileVaultCategory>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// FK
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == FileVaultCategorySelectMethod.Get || SelectMethod == FileVaultCategorySelectMethod.GetByFileVaultCategoryId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				FileVaultCategory entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					FileVaultCategoryProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<FileVaultCategory> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			FileVaultCategoryProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region FileVaultCategoryDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the FileVaultCategoryDataSource class.
	/// </summary>
	public class FileVaultCategoryDataSourceDesigner : ProviderDataSourceDesigner<FileVaultCategory, FileVaultCategoryKey>
	{
		/// <summary>
		/// Initializes a new instance of the FileVaultCategoryDataSourceDesigner class.
		/// </summary>
		public FileVaultCategoryDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public FileVaultCategorySelectMethod SelectMethod
		{
			get { return ((FileVaultCategoryDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new FileVaultCategoryDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region FileVaultCategoryDataSourceActionList

	/// <summary>
	/// Supports the FileVaultCategoryDataSourceDesigner class.
	/// </summary>
	internal class FileVaultCategoryDataSourceActionList : DesignerActionList
	{
		private FileVaultCategoryDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the FileVaultCategoryDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public FileVaultCategoryDataSourceActionList(FileVaultCategoryDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public FileVaultCategorySelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion FileVaultCategoryDataSourceActionList
	
	#endregion FileVaultCategoryDataSourceDesigner
	
	#region FileVaultCategorySelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the FileVaultCategoryDataSource.SelectMethod property.
	/// </summary>
	public enum FileVaultCategorySelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByFileVaultCategoryId method.
		/// </summary>
		GetByFileVaultCategoryId,
		/// <summary>
		/// Represents the GetByCategoryName method.
		/// </summary>
		GetByCategoryName
	}
	
	#endregion FileVaultCategorySelectMethod

	#region FileVaultCategoryFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="FileVaultCategory"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FileVaultCategoryFilter : SqlFilter<FileVaultCategoryColumn>
	{
	}
	
	#endregion FileVaultCategoryFilter

	#region FileVaultCategoryExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="FileVaultCategory"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FileVaultCategoryExpressionBuilder : SqlExpressionBuilder<FileVaultCategoryColumn>
	{
	}
	
	#endregion FileVaultCategoryExpressionBuilder	

	#region FileVaultCategoryProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;FileVaultCategoryChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="FileVaultCategory"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FileVaultCategoryProperty : ChildEntityProperty<FileVaultCategoryChildEntityTypes>
	{
	}
	
	#endregion FileVaultCategoryProperty
}

