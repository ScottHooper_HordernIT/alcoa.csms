using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Entities;
[System.ComponentModel.DataObject]

/// <summary>
/// Summary description for ODSWrapper
/// </summary>

public class ODSWrapper
{

    public static DataSet CompaniesNoKPI()
    {
        return DataRepository.KpiProvider.Reports_Statistical_NoKPI();
    }
    //public DataSet Reporting_Statistics_WAO_Month(string Month)
    //{
    //    return DataRepository.KpiProvider.Reports_Statistical_WAO_Month(Month);
    //}

    //public DataSet Reporting_Statistics_Site_Month(string Month)
    //{
    //    return DataRepository.KpiProvider.Reports_Statistical_Site_Month(Month);
    //}

    //public DataSet Reporting_Statistics_Contractor_Month(string Month)
    //{
    //    return DataRepository.KpiProvider.Reports_Statistical_Contractor_Month(Month);
    //}

    //public DataSet Reporting_Statistics_WAO_YTD()
    //{
    //    return DataRepository.KpiProvider.Reports_Statistical_WAO_YTD();
    //}

    //public DataSet Reporting_Statistics_Site_YTD(string FromDate, string ToDate)
    //{
    //    return DataRepository.KpiProvider.Reports_Statistical_Site_YTD(FromDate, ToDate);
    //}

    //public DataSet Reporting_Statistics_Contractor_YTD(string FromDate, string ToDate)
    //{
    //    return DataRepository.KpiProvider.Reports_Statistical_Contractor_YTD(FromDate, ToDate);
    //}

    public static DataSet FileDB_GetByCompanyName(string CompanyId)
    {
        return DataRepository.FileDbProvider.GetByCompanyId_Custom(Convert.ToInt32(CompanyId));
    }

    public static DataSet FileDB_GetByCompanyName2(string CompanyId)
    {
        return DataRepository.FileDbProvider.GetByCompanyId_Custom2(Convert.ToInt32(CompanyId));
    }

    public static TList<ContactsContractors> ContactsContractors_GetByCompanyName(string CompanyId)
    {
        TList<ContactsContractors> cList = DataRepository.ContactsContractorsProvider.GetByCompanyId(Convert.ToInt32(CompanyId));
        //return cList.ToDataSet(false);
        return cList;
    }

    public static DataSet ContactsContractors_GetAll()
    {
        TList<ContactsContractors> cList = DataRepository.ContactsContractorsProvider.GetAll();
        return cList.ToDataSet(false);
    }

    public static DataSet KPIGetByCompanyId(int CompanyId)
    {
        if (CompanyId == 0) { return DataRepository.KpiProvider.GetByCompanyId_Custom2(CompanyId); }
        else { return DataRepository.KpiProvider.GetByCompanyId_Custom(CompanyId); }
    }

    //public DataSet KPIReport(int CompanyId, int kpiMonthFrom, int kpiMonthTo, int kpiYearFrom, int kpiYearTo)
    //{
    //    if (kpiMonthFrom.ToString() != "")
    //    {
    //        DateTime kpiDateTimeFrom = new DateTime(kpiYearFrom, kpiMonthFrom, 1);
    //        DateTime kpiDateTimeTo = new DateTime(kpiYearTo, kpiMonthTo, 1);
    //        return (DataSet)DataRepository.KpiProvider.Report(CompanyId, kpiDateTimeFrom, kpiDateTimeTo);
    //    }
    //    else
    //    {
    //        return null;
    //    }
    //}


}
