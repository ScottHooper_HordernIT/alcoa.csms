﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface IQuestionnaireInitialContactEmailService
    {
        QuestionnaireInitialContactEmail Get(Expression<Func<QuestionnaireInitialContactEmail, bool>> where, List<Expression<Func<QuestionnaireInitialContactEmail, object>>> includes);
        List<QuestionnaireInitialContactEmail> GetMany(int? top, Expression<Func<QuestionnaireInitialContactEmail, bool>> where, Func<IQueryable<QuestionnaireInitialContactEmail>, IOrderedQueryable<QuestionnaireInitialContactEmail>> orderBy, List<Expression<Func<QuestionnaireInitialContactEmail, object>>> includes);
        List<QuestionnaireInitialContactEmail> GetManyPaged(int pageNumber, int pageSize, Expression<Func<QuestionnaireInitialContactEmail, bool>> where, Func<IQueryable<QuestionnaireInitialContactEmail>, IOrderedQueryable<QuestionnaireInitialContactEmail>> orderBy, List<Expression<Func<QuestionnaireInitialContactEmail, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<QuestionnaireInitialContactEmail, bool>> where, List<Expression<Func<QuestionnaireInitialContactEmail, object>>> includes);
        void Insert(QuestionnaireInitialContactEmail item, bool saveChanges = true);
        void Update(QuestionnaireInitialContactEmail item, bool saveChanges = true);
        void Delete(QuestionnaireInitialContactEmail item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class QuestionnaireInitialContactEmailService : IQuestionnaireInitialContactEmailService
    {
        private readonly IRepository<QuestionnaireInitialContactEmail> _repository;

        public QuestionnaireInitialContactEmailService(IRepository<QuestionnaireInitialContactEmail> repository)
        {
            this._repository = repository;
        }

        public QuestionnaireInitialContactEmail Get(Expression<Func<QuestionnaireInitialContactEmail, bool>> where, List<Expression<Func<QuestionnaireInitialContactEmail, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<QuestionnaireInitialContactEmail> GetMany(int? top, Expression<Func<QuestionnaireInitialContactEmail, bool>> where, Func<IQueryable<QuestionnaireInitialContactEmail>, IOrderedQueryable<QuestionnaireInitialContactEmail>> orderBy, List<Expression<Func<QuestionnaireInitialContactEmail, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<QuestionnaireInitialContactEmail> GetManyPaged(int pageNumber, int pageSize, Expression<Func<QuestionnaireInitialContactEmail, bool>> where, Func<IQueryable<QuestionnaireInitialContactEmail>, IOrderedQueryable<QuestionnaireInitialContactEmail>> orderBy, List<Expression<Func<QuestionnaireInitialContactEmail, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<QuestionnaireInitialContactEmail, bool>> where, List<Expression<Func<QuestionnaireInitialContactEmail, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(QuestionnaireInitialContactEmail item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("QuestionnaireInitialContactEmail");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(QuestionnaireInitialContactEmail item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("QuestionnaireInitialContactEmail");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(QuestionnaireInitialContactEmail item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("QuestionnaireInitialContactEmail");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
