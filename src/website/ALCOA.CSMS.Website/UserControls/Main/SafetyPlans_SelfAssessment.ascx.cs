using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Services;
using KaiZen.Library;
using System.IO;
using System.Drawing;
using System.Text;
using System.Net.Mail;
using DevExpress.Web.ASPxUploadControl;


public partial class UserControls_SafetyPlans_SelfAssessment : System.Web.UI.UserControl
{
    int argCompanyId;
    Auth auth = new Auth();
    bool viewOnly = false;
    string isNew=string.Empty;

    protected void Page_Load(object sender, EventArgs e) {  Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); }

    private void moduleInit(bool postBack)
    {
        if (!postBack)
        {
            bool isEhsAdmin = false;
           
            //Check if User is an EHS Consultant
            int count;
            EhsConsultantFilters queryEhsConsultant = new EhsConsultantFilters();
            queryEhsConsultant.Append(EhsConsultantColumn.UserId, auth.UserId.ToString());
            TList<EhsConsultant> EhsConsultantList = DataRepository.EhsConsultantProvider.GetPaged(queryEhsConsultant.ToString(), null, 0, 100, out count);
            if (count > 0) { isEhsAdmin = true; };

            if (isEhsAdmin || auth.RoleId == (int)RoleList.Administrator)
            {
                btnCompare.Visible = true;
            }

            int previousYear = DateTime.Now.Year - 1;

            ddlFileDesc.Items.Clear();
            ddlFileDesc.Items.Add(DateTime.Now.Year.ToString() + " Safety Management Plan");
            ddlFileDesc.Items.Add((DateTime.Now.Year + 1).ToString() + " Safety Management Plan");
            ddlFileDesc.SelectedIndex = -1;
            //ddlFileDesc.Items.Add(previousYear + " Safety Management Plan");

            //cbFileDesc.Items.Clear();
            //cbFileDesc.Items.Add(DateTime.Now.Year.ToString() + " Safety Management Plan");
            //cbFileDesc.Items.Add((DateTime.Now.Year + 1).ToString() + " Safety Management Plan");
            //cbFileDesc.SelectedItem = null;

            switch (auth.RoleId)
            {
                case (int)RoleList.Reader: //not meant to be here....
                    //if (!isEhsAdmin) { Response.Redirect("Javascript:window.close();"); };
                    if (!isEhsAdmin)
                    {
                        btnAssess.Visible = false;
                    }
                    else
                    { ddlCompanies.Enabled = true; }
                    break;
                case (int)RoleList.Contractor:
                    ddlCompanies.SelectedValue = auth.CompanyId.ToString();
                    SessionHandler.spVar_CompanyId = auth.CompanyId.ToString();
                    break;
                case (int)RoleList.Administrator:
                    ddlCompanies.Enabled = true;
                    break;
                default:
                    break;
            }

            LoadQ();

            string r = "";
            string i = "";
            try
            {
                r = Request.QueryString["R"].ToString();
                viewOnly = true;
                i = Request.QueryString["i"].ToString();
                isNew = i;
                if (i == "1") SessionHandler.spVar_SafetyPlans_ResponseId = null;
            }
            catch (Exception)
            {
                //Todo: handle exception
            }

            bool r_valid = true;
            if (String.IsNullOrEmpty(r))
            {
                LoadR();
            }
            else
            {
                SessionHandler.spVar_SafetyPlans_ResponseId = r;

                SafetyPlansSeResponsesService sers = new SafetyPlansSeResponsesService();
                int count2 = 0;
                SafetyPlansSeResponses ser = sers.GetByResponseId(Convert.ToInt32(r), 0, 1, out count2);
                if (count2 == 0)
                {
                    r_valid = false;
                }
                else
                {
                    if (ser.IsCopy == true)
                    {
                        //enable validation for checkbox.
                    }
                    Panel1.Visible = false;

                    if (Helper.General.isEhsConsultant(auth.UserId) || auth.RoleId == (int)RoleList.Administrator)
                    {
                        ASPxRoundPanel1.Visible = true;

                        FileDbService fileDbService = new FileDbService();
                        TList<FileDb> fileDb = fileDbService.GetBySelfAssessmentResponseId(Convert.ToInt32(r));
                        if (fileDb[0] != null)
                        {
                            hlUploadedFile.Text = fileDb[0].FileName;
                            hlUploadedFile.NavigateUrl = "~/Common/GetFile.aspx" + QueryStringModule.Encrypt("Type=CSMS&SubType=SafetyPlans&File=" + fileDb[0].FileId.ToString());
                            
                            //hlUploadedFile.

                            if (fileDb[0].FileName != "SMP Not Uploaded.pdf")
                            {
                                if (auth.RoleId != (int)RoleList.Administrator)
                                {
                                    fUpload_Modify.Enabled = false;
                                    btnUpload_Modify.Enabled = false;
                                    panelUploadModify.Visible = false;
                                }
                            }


                            if (fileDb[0].StatusId == (int)SafetyPlanStatusList.Being_Assessed) //being assessed
                            {
                                fUpload_Modify.Enabled = true;
                                btnUpload_Modify.Enabled = true;
                            }
                        }
                    }
                    //ASPxRoundPanel1.Visible = false;
                    //ASPxRoundPanel1.Enabled = false;
                }
            }

            if (!String.IsNullOrEmpty(SessionHandler.spVar_SafetyPlans_ResponseId))
            {

                if (r_valid == true)
                {
                    LoadA();

                    if (auth.RoleId == (int)RoleList.Contractor)
                    {
                        if (SessionHandler.spVar_CompanyId != auth.CompanyId.ToString())
                        {
                            Response.Redirect("AccessDenied.aspx?error=Invalid Self-Assessment File", false);
                        }
                    }
                    btnPrint.OnClientClick = "popUp('SafetyPlans_SelfAssessment_print.aspx?R=" +
                        SessionHandler.spVar_SafetyPlans_ResponseId + "');";

                }
                else
                {
                    Response.Redirect("AccessDenied.aspx?error=Invalid Self-Assessment File", false);
                }
            }
            else
            {
                btnPrint.OnClientClick = "javascript:alert('Nothing to print! No Questions answered.');";
            }
            if (viewOnly == true)
            {
                ASPxButton1.Text = "Close";
                btnAssess.OnClientClick = "location.href='SafetyPlans_Assess.aspx?R=" + Request.QueryString["R"] + "';";
                //hlAssess.NavigateUrl = hlAssess.NavigateUrl + "?R=" + Request.QueryString["R"];

                FileDbService fdbs = new FileDbService();
                int r_ = Convert.ToInt32(r);
                DataSet ds_ = fdbs.GetCompanyIdByQuestionnaireResponseId(r_);
                string CompanyName = "";
                foreach (DataRow dr in ds_.Tables[0].Rows)
                {
                    CompanyName = dr[0].ToString();
                }
                CompaniesService cs = new CompaniesService();
                Companies c = cs.GetByCompanyName(CompanyName);

                ddlCompanies.SelectedValue = c.CompanyId.ToString();

                bool go = false;
                if (isEhsAdmin) go = true;
                if (auth.RoleId == (int)RoleList.Administrator) go = true;

                if (go == true)
                {
                    btnAssess.Text = "Review";
                }
                else
                {
                    btnAssess.Text = "View Feedback";
                }

                if (auth.RoleId == (int)RoleList.Reader)
                {
                    int i1 = 1;
                    while (i1 <= 10)
                    {
                        LinkButton lbutton = (LinkButton)this.FindControl("lb" + i1);
                        LinkButton lbutton2 = (LinkButton)this.FindControl("lb" + i1 + "_");
                        //lbutton.PostBackUrl = lbutton.PostBackUrl + "&R=" + r;
                        //lbutton.Enabled = false;
                        //lbutton2.Enabled = false;
                        lbutton.PostBackUrl = "javascript:void(0);";
                        lbutton2.PostBackUrl = "javascript:void(0);";
                        //lbutton.PostBackUrl = "javascript:alert('You can not edit a submitted Self-Assessment.\n Press the print button to view the submitted Self-Assessment sheet');";

                        i1++;
                    }
                }
                Label3.Visible = false;
            }
            else
            {
                btnAssess.Visible = false;
            }
        }
        else
        {
            //up();
        }
    }
    protected void LoadA()
    {
        int i = 0;
        SafetyPlansSeAnswersService sea = new SafetyPlansSeAnswersService();
        TList<SafetyPlansSeAnswers> seaList = sea.GetByResponseId(Convert.ToInt32(SessionHandler.spVar_SafetyPlans_ResponseId));
        foreach (SafetyPlansSeAnswers s in seaList)
        {
            if(s.Answer == true)
            {
                Label lbl = (Label)this.FindControl("lbl" + s.QuestionId.ToString() + "a");
                lbl.Text = "Yes"; lbl.Visible = true; lbl.ForeColor = Color.Green;
                i++;
            }
            if (s.Answer == false)
            {
                Label lbl = (Label)this.FindControl("lbl" + s.QuestionId.ToString() + "a");
                lbl.Text = "No"; lbl.Visible = true; lbl.ForeColor = Color.Red;
                i++;
            }
            string sComment = Encoding.ASCII.GetString(s.Comment);

            this.FindControl("img" + s.QuestionId.ToString() + "m").Visible = true;

            if (String.IsNullOrEmpty(sComment))
            {
                this.FindControl("img" + s.QuestionId.ToString() + "m").Visible = false;
            }

            if (sComment == "<br />")
            {
                this.FindControl("img" + s.QuestionId.ToString() + "m").Visible = false;
            }

            if(viewOnly)
            {
                if (s.AssessorApproval == true)
                {
                    Control _st = this.FindControl("lbl" + s.QuestionId.ToString() + "s");
                    Label st = (Label)_st;
                    st.Text = "Approved";
                    st.ForeColor = System.Drawing.Color.Green;

                    if (viewOnly)
                    {
                        Control _lb = this.FindControl("lb" + s.QuestionId.ToString());
                        LinkButton lb = (LinkButton)_lb;
                        Control _lb2 = this.FindControl("lb" + s.QuestionId.ToString() + "_");
                        LinkButton lb2 = (LinkButton)_lb2;
                        //lb2.PostBackUrl = "";
                        //lb2.
                        //lb.Enabled = false;
                        //lb2.Enabled = false;
                        lb.ForeColor = System.Drawing.Color.Black;
                        lb2.ForeColor = System.Drawing.Color.Black;
                        lb.PostBackUrl = "javascript:void(0);";
                        lb2.PostBackUrl = "javascript:void(0);";
                    }
                }
                else
                {
                    if (s.AssessorApproval != null)
                    {
                        Control _s2 = this.FindControl("lbl" + s.QuestionId.ToString() + "s");
                        Label s2 = (Label)_s2;
                        s2.Text = "Not Approved";
                        s2.ForeColor = System.Drawing.Color.Red;
                    }
                    Control _lb = this.FindControl("lb" + s.QuestionId.ToString());
                    LinkButton lb = (LinkButton)_lb;
                    Control _lb2 = this.FindControl("lb" + s.QuestionId.ToString() + "_");
                    LinkButton lb2 = (LinkButton)_lb2;
                    lb.PostBackUrl = lb.PostBackUrl + "&R=" + SessionHandler.spVar_SafetyPlans_ResponseId;
                    lb2.PostBackUrl = lb2.PostBackUrl + "R=" + SessionHandler.spVar_SafetyPlans_ResponseId;
                    //lb.Font.Bold = true;
                    //lb2.Font.Bold = true;
                }
            }

        }
        if (i == 10)
        {
            Label2.Visible = false;
            Panel1.Enabled = true;
        }
        else
        {
            Label2.Visible = true;
            Panel1.Enabled = false;
        }
    }
    protected void LoadQ()
    {
        if (Request.QueryString["i"] != null)
        {
            isNew = Convert.ToString(Request.QueryString["i"]);
        }

        SafetyPlansSeQuestionsService seq = new SafetyPlansSeQuestionsService();
        TList<SafetyPlansSeQuestions> seqList = seq.GetAll();
        foreach (SafetyPlansSeQuestions s in seqList)
        {
            switch (s.QuestionId)
            {
                case 1:
                    lb1.Text = s.Question;
                    //lb1.PostBackUrl = "~/SafetyPlans_Question.aspx?Q=" + s.QuestionId;
                    //Code modified by Bishwajit Sahoo
                    if (isNew == "1")
                    {
                        lb1.PostBackUrl = "~/SafetyPlans_Question.aspx?Q=" + s.QuestionId + "&i=" + isNew;
                    }
                    else
                    {
                        lb1.PostBackUrl = "~/SafetyPlans_Question.aspx?Q=" + s.QuestionId;
                    }
                    lb1_.PostBackUrl = lb1.PostBackUrl;
                    break;
                case 2:
                    lb2.Text = s.Question;
                    if (isNew == "1")
                    {
                        lb2.PostBackUrl = "~/SafetyPlans_Question.aspx?Q=" + s.QuestionId + "&i=" + isNew;
                    }
                    else
                    {
                        lb2.PostBackUrl = "~/SafetyPlans_Question.aspx?Q=" + s.QuestionId;
                    }
                    lb2_.PostBackUrl = lb2.PostBackUrl;
                    break;
                case 3:
                    lb3.Text = s.Question;
                    //lb3.PostBackUrl = "~/SafetyPlans_Question.aspx?Q=" + s.QuestionId;
                    if (isNew == "1")
                    {
                        lb3.PostBackUrl = "~/SafetyPlans_Question.aspx?Q=" + s.QuestionId + "&i=" + isNew;
                    }
                    else
                    {
                        lb3.PostBackUrl = "~/SafetyPlans_Question.aspx?Q=" + s.QuestionId;
                    }
                    lb3_.PostBackUrl = lb3.PostBackUrl;
                    break;
                case 4:
                    lb4.Text = s.Question;
                    //lb4.PostBackUrl = "~/SafetyPlans_Question.aspx?Q=" + s.QuestionId;
                    if (isNew == "1")
                    {
                        lb4.PostBackUrl = "~/SafetyPlans_Question.aspx?Q=" + s.QuestionId + "&i=" + isNew;
                    }
                    else
                    {
                        lb4.PostBackUrl = "~/SafetyPlans_Question.aspx?Q=" + s.QuestionId;
                    }
                    lb4_.PostBackUrl = lb4.PostBackUrl;
                    break;
                case 5:
                    lb5.Text = s.Question;
                    //lb5.PostBackUrl = "~/SafetyPlans_Question.aspx?Q=" + s.QuestionId;
                    if (isNew == "1")
                    {
                        lb5.PostBackUrl = "~/SafetyPlans_Question.aspx?Q=" + s.QuestionId + "&i=" + isNew;
                    }
                    else
                    {
                        lb5.PostBackUrl = "~/SafetyPlans_Question.aspx?Q=" + s.QuestionId;
                    }
                    lb5_.PostBackUrl = lb5.PostBackUrl;
                    break;
                case 6:
                    lb6.Text = s.Question;
                    //lb6.PostBackUrl = "~/SafetyPlans_Question.aspx?Q=" + s.QuestionId;
                    if (isNew == "1")
                    {
                        lb6.PostBackUrl = "~/SafetyPlans_Question.aspx?Q=" + s.QuestionId + "&i=" + isNew;
                    }
                    else
                    {
                        lb6.PostBackUrl = "~/SafetyPlans_Question.aspx?Q=" + s.QuestionId;
                    }
                    lb6_.PostBackUrl = lb6.PostBackUrl;
                    break;
                case 7:
                    lb7.Text = s.Question;
                    //lb7.PostBackUrl = "~/SafetyPlans_Question.aspx?Q=" + s.QuestionId;
                    if (isNew == "1")
                    {
                        lb7.PostBackUrl = "~/SafetyPlans_Question.aspx?Q=" + s.QuestionId + "&i=" + isNew;
                    }
                    else
                    {
                        lb7.PostBackUrl = "~/SafetyPlans_Question.aspx?Q=" + s.QuestionId;
                    }
                    lb7_.PostBackUrl = lb7.PostBackUrl;
                    break;
                case 8:
                    lb8.Text = s.Question;
                    //lb8.PostBackUrl = "~/SafetyPlans_Question.aspx?Q=" + s.QuestionId;
                    if (isNew == "1")
                    {
                        lb8.PostBackUrl = "~/SafetyPlans_Question.aspx?Q=" + s.QuestionId + "&i=" + isNew;
                    }
                    else
                    {
                        lb8.PostBackUrl = "~/SafetyPlans_Question.aspx?Q=" + s.QuestionId;
                    }
                    lb8_.PostBackUrl = lb8.PostBackUrl;
                    break;
                case 9:
                    lb9.Text = s.Question;
                   // lb9.PostBackUrl = "~/SafetyPlans_Question.aspx?Q=" + s.QuestionId;
                    if (isNew == "1")
                    {
                        lb9.PostBackUrl = "~/SafetyPlans_Question.aspx?Q=" + s.QuestionId + "&i=" + isNew;
                    }
                    else
                    {
                        lb9.PostBackUrl = "~/SafetyPlans_Question.aspx?Q=" + s.QuestionId;
                    }
                    lb9_.PostBackUrl = lb9.PostBackUrl;
                    break;
                case 10:
                    lb10.Text = s.Question;
                    //lb10.PostBackUrl = "~/SafetyPlans_Question.aspx?Q=" + s.QuestionId;
                    if (isNew == "1")
                    {
                        lb10.PostBackUrl = "~/SafetyPlans_Question.aspx?Q=" + s.QuestionId + "&i=" + isNew;
                    }
                    else
                    {
                        lb10.PostBackUrl = "~/SafetyPlans_Question.aspx?Q=" + s.QuestionId;
                    }
                    lb10_.PostBackUrl = lb10.PostBackUrl;
                    break;
                default:
                    break;
            }
        }
    }
    protected void LoadR()
    {
        SafetyPlansSeResponsesService sers = new SafetyPlansSeResponsesService();

       
        try
        {
            DataSet dsSer = sers.GetByCompanyId_Latest_NotSubmitted(Convert.ToInt32(SessionHandler.spVar_CompanyId));
            if (dsSer.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dr in dsSer.Tables[0].Rows)
                {
                    SessionHandler.spVar_SafetyPlans_ResponseId = dr[0].ToString();
                    SessionHandler.spVar_CompanyId = dr[1].ToString();
                     btnAssess.Visible = false;
                }
            }
            else
            {
                //no existing 'incomplete' smp...
                //ok. check if a last 'complete' smp exists. if so, copy it to a new responseid.
                 //DataSet dsSer2 = sers.GetByCompanyId_Latest_Submitted(Convert.ToInt32(SessionHandler.spVar_CompanyId));
                 //if (dsSer2.Tables[0].Rows.Count > 0 && auth.RoleId != (int)RoleList.Administrator)
                 //{
                 //    int? ResponseId_Old = null;
                 //    foreach (DataRow dr2 in dsSer2.Tables[0].Rows)
                 //    {
                 //        ResponseId_Old = Convert.ToInt32(dr2[0].ToString());
                 //        btnAssess.Visible = false;
                 //    }
                 //    CopySmp(ResponseId_Old);
                 //}
                 //else
                 //{
                    SessionHandler.spVar_SafetyPlans_ResponseId = null;
                 //}
            }
        }
        catch (Exception)
        {
            //Todo: Handle exception
        }

        
        //TList<SafetyPlansSeResponses> serList = sers.GetByCompanyId_Latest_NotSubmitted(Convert.ToInt32(SessionHandler.spVar_CompanyId));

        //if (serList.Count != 0)
        //{
        //    foreach (SafetyPlansSeResponses r1 in serList)
        //    {
        //        SessionHandler.spVar_SafetyPlans_ResponseId = r1.ResponseId.ToString();
        //        SessionHandler.spVar_CompanyId = r1.CompanyId.ToString();

        //        if (r1.Submitted == false)
        //        {
        //            btnAssess.Visible = false;
        //            btnAssess.Visible = false;
        //        }
        //    }
        //}
        //else
        //{
        //    SessionHandler.spVar_SafetyPlans_ResponseId = null;
        //}
    }

    protected void up()
    {
        argCompanyId = Convert.ToInt32(ddlCompanies.SelectedItem.Value);
        Label1.Text = "";
        if (Page.IsPostBack)
        {
            Configuration configuration = new Configuration();
            //string tempPath = "C:\\Temp\\";
            string tempPath = @configuration.GetValue(ConfigList.TempDir).ToString();
            string fileName = "";
            try
            {
                bool fileUploaded = false;
                try
                {

                    if (fUpload.UploadedFiles != null && fUpload.UploadedFiles.Length > 0)
                    {
                        if (!String.IsNullOrEmpty(fUpload.UploadedFiles[0].FileName) && fUpload.UploadedFiles[0].IsValid)
                        {
                            fileUploaded = true;
                        }
                    }
                }
                catch (Exception)
                {
                    //Todo: Handle Exception
                }

                //DT290: Assign a H&S Assessor to a company who hasn't uploaded a Safety Management Plan, removed fileUploaded check to let all cases fall through
                //if (cbNoUpload.Checked == true) fileUploaded = true;
                //if (fileUploaded)
                //{
                    FileStream fs;
                    if (!fileUploaded || cbNoUpload.Checked == true)
                    {
                        //FileStream fs = File.OpenRead();
                        fs = new FileStream(this.Server.MapPath("~/Common/SMP Not Uploaded.pdf"), FileMode.Open,
                                                                                   FileAccess.Read,
                                                                                   FileShare.ReadWrite);
                    }
                    else
                    {
                        //1. Dump file into dropbox for virus scanning...
                        fileName = tempPath + fUpload.UploadedFiles[0].FileName;
                        fUpload.UploadedFiles[0].SaveAs(tempPath + fUpload.UploadedFiles[0].FileName);
                        //Label1.Text = "File Uploaded: " + FileUpload.FileName;

                        //FileUpload1.Dispose();
                        //2. Upload into database
                        fileName = tempPath + fUpload.UploadedFiles[0].FileName;
                        fs = new FileStream(fileName, FileMode.Open,
                                                       FileAccess.Read,
                                                       FileShare.ReadWrite);

                    }
                    FileDbService fileDbService = new FileDbService();
                    FileDb fileDb = new FileDb();

                    //todo - check if hashfile of new file exists in db! i.e. prevent duplicates

                    argCompanyId = Convert.ToInt32(ddlCompanies.SelectedItem.Value);
                    fileDb.CompanyId = argCompanyId;

                    fileDb.ModifiedByUserId = auth.UserId;
                    fileDb.ModifiedDate = DateTime.Now;

                    if (!fileUploaded || cbNoUpload.Checked == true)
                    {
                        fileDb.FileName = "SMP Not Uploaded.pdf";
                    }
                    else
                    {
                        fileDb.FileName = fUpload.UploadedFiles[0].FileName;
                    }

                    fileDb.Description = ddlFileDesc.SelectedItem.Text.ToString();

                    fileDb.Content = FileUtilities.Read.ReadFully(fs, 51200);
                    fileDb.ContentLength = Convert.ToInt32(fs.Length);
                    fileDb.FileHash = FileUtilities.Calculate.HashFile(fileDb.Content);
                    fileDb.StatusId = (int)SafetyPlanStatusList.Submitted;

                    int? ehsConsultantId = getEHSConsultantDetails_int(argCompanyId);
                    if (ehsConsultantId != null)
                    {
                        fileDb.EhsConsultantId = ehsConsultantId;
                        fileDb.StatusId = (int)SafetyPlanStatusList.Being_Assessed;
                    }

                    //enum SafetyPlanStatus = Enum.GetValues(ALCOA.WAO.CSMWP.Entities.SafetyPlanStatus);

                    fileDb.StatusModifiedByUserId = auth.UserId; //Initial Submitted by user etc.
                    fileDb.StatusComments = null;

                    fileDb.SelfAssessmentResponseId = Convert.ToInt32(SessionHandler.spVar_SafetyPlans_ResponseId);

                    try
                    {
                        fileDbService.Insert(fileDb);
                    }
                    catch (Exception)
                    {
                        //Todo: Handle Exception
                    }

                    fileDb.Dispose();

                    //3. Delete file from dropbox
                    fs.Close();
                    fUpload.Dispose();
                    if (fileUploaded) File.Delete(fUpload.UploadedFiles[0].FileName);

                    SafetyPlansSeResponsesService sers = new SafetyPlansSeResponsesService();
                    SafetyPlansSeResponses ser = new SafetyPlansSeResponses();
                    ser = sers.GetByResponseId(Convert.ToInt32(SessionHandler.spVar_SafetyPlans_ResponseId));
                    ser.Submitted = true;
                    sers.Update(ser);

                    //Email the company contact...
                    try
                    {
                        MailMessage mail = new MailMessage();
                        mail.From = new MailAddress(configuration.GetValue(ConfigList.ContactEmail),
                                                    "AUA Alcoa Contractor Services Team");
                        mail.To.Add(getEHSConsultantEmail(argCompanyId));
                        mail.Subject = "CSM - New Safety Plan submitted by " + ddlCompanies.SelectedItem.Text;
                        mail.Body = mail.Subject;
                        SmtpClient smtp = new SmtpClient();
                        smtp.Send(mail);
                    }
                    catch (Exception)
                    {
                        //Todo: Handle exception
                    }
                    //StringBuilder sb = new StringBuilder();
                    ////sb.Append("window.opener.RefreshPage();");
                    //sb.Append("window.close();");

                    //Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "CloseWindowScript",
                    //    sb.ToString(), true);

                    Response.Redirect("~/SafetyPlans.aspx", true);
                    //Response.Redirect("javascript:window.close();", true);
                //}
                //else
                //{
                //    Label1.Text = "Upload Unsuccessful (No File or Incorrect File Type). Please check below";
                //}

            }
            catch (Exception ex)
            {
                Label1.Text = "Error Occured: " + ex.Message.ToString();
                if (File.Exists(fileName))
                {
                    File.Delete(fileName);
                }
            }
        }
    }
    protected void up_modify()
    {
        argCompanyId = Convert.ToInt32(ddlCompanies.SelectedItem.Value);
        Label1.Text = "";
        if (Page.IsPostBack)
        {
            argCompanyId = Convert.ToInt32(ddlCompanies.SelectedItem.Value);
            Configuration configuration = new Configuration();
            //string tempPath = "C:\\Temp\\";
            string tempPath = @configuration.GetValue(ConfigList.TempDir).ToString();
            string fileName = "";
            try
            {
                bool ok = false;
                try
                {

                    if (fUpload_Modify.UploadedFiles != null && fUpload_Modify.UploadedFiles.Length > 0)
                    {
                        if (!String.IsNullOrEmpty(fUpload_Modify.UploadedFiles[0].FileName) && fUpload_Modify.UploadedFiles[0].IsValid)
                        {
                            ok = true;
                        }
                    }
                }
                catch (Exception)
                {
                    //Todo: Handle exception
                }

                if (ok)
                {
                    FileStream fs;
                    FileDbService fileDbService = new FileDbService();
                    TList<FileDb> fileDb = fileDbService.GetBySelfAssessmentResponseId(Convert.ToInt32(SessionHandler.spVar_SafetyPlans_ResponseId));

                    //1. Dump file into dropbox for virus scanning...
                    fileName = tempPath + fUpload_Modify.UploadedFiles[0].FileName;
                    fUpload_Modify.UploadedFiles[0].SaveAs(tempPath + fUpload_Modify.UploadedFiles[0].FileName);
                    //Label1.Text = "File Uploaded: " + FileUpload.FileName;

                    //FileUpload1.Dispose();
                    //2. Upload into database
                    fileName = tempPath + fUpload_Modify.UploadedFiles[0].FileName;
                    fs = new FileStream(fileName, FileMode.Open,
                                                   FileAccess.Read,
                                                   FileShare.ReadWrite);

                    //todo - check if hashfile of new file exists in db! i.e. prevent duplicates

                    argCompanyId = Convert.ToInt32(ddlCompanies.SelectedItem.Value);
                    fileDb[0].CompanyId = argCompanyId;

                    fileDb[0].ModifiedByUserId = auth.UserId;
                    fileDb[0].ModifiedDate = DateTime.Now;
                    fileDb[0].FileName = fUpload_Modify.UploadedFiles[0].FileName;

                    fileDb[0].Description = ddlFileDesc.SelectedItem.Text.ToString();

                    fileDb[0].Content = FileUtilities.Read.ReadFully(fs, 51200);
                    fileDb[0].ContentLength = Convert.ToInt32(fs.Length);
                    fileDb[0].FileHash = FileUtilities.Calculate.HashFile(fileDb[0].Content);
                    fileDb[0].StatusId = (int)SafetyPlanStatusList.Submitted;
                    if (getEHSConsultantDetails_int(argCompanyId) != null)
                    {
                        fileDb[0].EhsConsultantId = getEHSConsultantDetails_int(argCompanyId);
                        fileDb[0].StatusId = (int)SafetyPlanStatusList.Being_Assessed;
                    }

                    //enum SafetyPlanStatus = Enum.GetValues(ALCOA.WAO.CSMWP.Entities.SafetyPlanStatus);

                    fileDb[0].StatusModifiedByUserId = auth.UserId; //Initial Submitted by user etc.
                    fileDb[0].StatusComments = null;

                    //fileDb[0].SelfAssessmentResponseId = Convert.ToInt32(SessionHandler.spVar_SafetyPlans_ResponseId);

                    try
                    {
                        fileDbService.Save(fileDb[0]);
                    }
                    catch (Exception)
                    {
                        //Todo: Handle exception
                    }

                    fileDb[0].Dispose();

                    //3. Delete file from dropbox
                    fs.Close();
                    fUpload_Modify.Dispose();
                    File.Delete(fUpload_Modify.UploadedFiles[0].FileName);

                    //SafetyPlansSeResponsesService sers = new SafetyPlansSeResponsesService();
                    //SafetyPlansSeResponses ser = new SafetyPlansSeResponses();
                    //ser = sers.GetByResponseId(Convert.ToInt32(SessionHandler.spVar_SafetyPlans_ResponseId));
                    //ser.Submitted = true;
                    //sers.Update(ser);

                    //Email the company contact...
                    try
                    {
                        MailMessage mail = new MailMessage();
                        mail.From = new MailAddress(configuration.GetValue(ConfigList.ContactEmail),
                                                    "AUA Alcoa Contractor Services Team");
                        mail.To.Add(getEHSConsultantEmail(argCompanyId));
                        mail.Subject = "CSM - Safety Plan uploaded for " + ddlCompanies.SelectedItem.Text;
                        mail.Body = mail.Subject;
                        SmtpClient smtp = new SmtpClient();
                        smtp.Send(mail);
                    }
                    catch (Exception)
                    {
                        //Todo: Handle exception
                    }
                    //StringBuilder sb = new StringBuilder();
                    ////sb.Append("window.opener.RefreshPage();");
                    //sb.Append("window.close();");

                    //Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "CloseWindowScript",
                    //    sb.ToString(), true);

                    Response.Redirect("~/SafetyPlans.aspx", true);
                    //Response.Redirect("javascript:window.close();", true);
                }
                else
                {
                    Label1.Text = "Upload Unsuccessful (No File or Incorrect File Type). Please check below";
                }

            }
            catch (Exception ex)
            {
                Label1.Text = "Error Occured: " + ex.Message.ToString();
                if (File.Exists(fileName))
                {
                    File.Delete(fileName);
                }
            }
        }
    }
    protected int? getEHSConsultantDetails_int(int CompanyId)
    {
        try
        {
            CompaniesService companiesService = new CompaniesService();
            Companies c = companiesService.GetByCompanyId(CompanyId);

            EhsConsultantService ehsconsultantService = new EhsConsultantService();
            int ehsConsultantId = -1;
            ehsConsultantId = Convert.ToInt32(c.EhsConsultantId);

            if (ehsConsultantId > 0)
            {
                return ehsConsultantId;
            }
            else
            {
                return null;
            }
        }
        catch (Exception)
        {
            return null;
        }

    }
    protected string getEHSConsultantEmail(int CompanyId)
    {
        Configuration configuration = new Configuration();
        string defaultEmail = configuration.GetValue(ConfigList.ContactEmail);
        try
        {
            CompaniesService companiesService = new CompaniesService();
            Companies c = companiesService.GetByCompanyId(CompanyId);

            EhsConsultantService ehsconsultantService = new EhsConsultantService();
            int ehsConsultantId = -1;
            ehsConsultantId = Convert.ToInt32(c.EhsConsultantId);
            EhsConsultant ehsconsultant = ehsconsultantService.GetByEhsConsultantId(ehsConsultantId);

            UsersService usersService = new UsersService();
            Users u = usersService.GetByUserId(ehsconsultant.UserId);
            if (!String.IsNullOrEmpty(u.Email))
            {
                return u.Email;
            }
            else
            {
                return defaultEmail;
            }
        }
        catch (Exception)
        {
            return defaultEmail;
        }
        //try
        //{
        //    int? EHSConsultantId = 0;
        //    CompaniesService companiesService = new CompaniesService();
        //    TList<Companies> companiesList = companiesService.GetAll();
        //    TList<Companies> companiesList2 = companiesList.FindAll(
        //        delegate(Companies c2)
        //        {
        //            return
        //                c2.CompanyId == CompanyId;
        //        }
        //        );


        //    if (companiesList2.Count != 0)
        //    {
        //        foreach (Companies c1 in companiesList2)
        //        {
        //            try
        //            {
        //                EHSConsultantId = (int?)c1.EhsConsultantId.Value;
        //            }
        //            catch (Exception ex)
        //            {
        //            }
        //        }
        //    }

        //    if (EHSConsultantId > 0)
        //    {
        //        UsersService usersService = new UsersService();
        //        TList<Users> usersList = usersService.GetAll();
        //        TList<Users> usersList2 = usersList.FindAll(
        //            delegate(Users u2)
        //            {
        //                return
        //                    u2.UserId == (int)EHSConsultantId;
        //            }
        //            );

        //        string eMail = "";
        //        if (usersList2.Count != 0)
        //        {
        //            foreach (Users u1 in usersList2)
        //            {
        //                eMail = u1.Email;
        //            }
        //        }
        //        return eMail;
        //    }
        //    else
        //    {
        //        return defaultEmail;
        //    }
        //}
        //catch (Exception ex)
        //{
        //    return (defaultEmail);
        //}

    }
    public void Start()
    {
        int i = 1;
        while (i <= 10)
        {
            this.FindControl("lbl" + i + "a").Visible = false;
            this.FindControl("img" + i + "m").Visible = false;
            i++;
        }
    }

    public void ddlCompanies_SelectedIndexChanged(object sender, EventArgs e)
    {
        SessionHandler.spVar_CompanyId = ddlCompanies.SelectedItem.Value;
        Start();
        LoadR();
        if (!String.IsNullOrEmpty(SessionHandler.spVar_SafetyPlans_ResponseId)) LoadA();
        //System.Threading.Thread.Sleep(1000);
        argCompanyId = Convert.ToInt32(ddlCompanies.SelectedItem.Value);
    }

    protected void btnUpload_Click(object sender, EventArgs e)
    {
        //System.Threading.Thread.Sleep(1000);
        up();
    }
    protected void btnUploadModify_Click(object sender, EventArgs e)
    {
        //System.Threading.Thread.Sleep(1000);
        up_modify();
    }

    protected void btnCompare_Click(object sender, EventArgs e)
    {
        //System.Threading.Thread.Sleep(1000);
        string R = Request.QueryString["R"].ToString();
        Response.Redirect(String.Format("javascript:popUp('PopUps/SafetyPlans_Compare.aspx{0}'); window.location='SafetyPlans_SelfAssessment.aspx{0}';", QueryStringModule.Encrypt("R=" + R)));
    }
}