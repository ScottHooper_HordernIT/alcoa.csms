﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface IAdminDashboardService
    {
        AdminDashboard Get(Expression<Func<AdminDashboard, bool>> where, List<Expression<Func<AdminDashboard, object>>> includes);
        List<AdminDashboard> GetMany(int? top, Expression<Func<AdminDashboard, bool>> where, Func<IQueryable<AdminDashboard>, IOrderedQueryable<AdminDashboard>> orderBy, List<Expression<Func<AdminDashboard, object>>> includes);
        List<AdminDashboard> GetManyPaged(int pageNumber, int pageSize, Expression<Func<AdminDashboard, bool>> where, Func<IQueryable<AdminDashboard>, IOrderedQueryable<AdminDashboard>> orderBy, List<Expression<Func<AdminDashboard, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<AdminDashboard, bool>> where, List<Expression<Func<AdminDashboard, object>>> includes);
        void Insert(AdminDashboard item, bool saveChanges = true);
        void Update(AdminDashboard item, bool saveChanges = true);
        void Delete(AdminDashboard item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class AdminDashboardService : IAdminDashboardService
    {
        private readonly IRepository<AdminDashboard> _repository;

        public AdminDashboardService(IRepository<AdminDashboard> repository)
        {
            this._repository = repository;
        }

        public AdminDashboard Get(Expression<Func<AdminDashboard, bool>> where, List<Expression<Func<AdminDashboard, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<AdminDashboard> GetMany(int? top, Expression<Func<AdminDashboard, bool>> where, Func<IQueryable<AdminDashboard>, IOrderedQueryable<AdminDashboard>> orderBy, List<Expression<Func<AdminDashboard, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<AdminDashboard> GetManyPaged(int pageNumber, int pageSize, Expression<Func<AdminDashboard, bool>> where, Func<IQueryable<AdminDashboard>, IOrderedQueryable<AdminDashboard>> orderBy, List<Expression<Func<AdminDashboard, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<AdminDashboard, bool>> where, List<Expression<Func<AdminDashboard, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(AdminDashboard item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("AdminDashboard");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(AdminDashboard item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("AdminDashboard");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(AdminDashboard item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("AdminDashboard");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
