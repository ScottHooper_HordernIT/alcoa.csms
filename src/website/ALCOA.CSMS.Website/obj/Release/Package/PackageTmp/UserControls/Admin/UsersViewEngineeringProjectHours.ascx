﻿<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
    
<%@ Control Language="C#" AutoEventWireup="true" Inherits="UserControls_Admin_UsersViewEngineeringProjectHours" Codebehind="UsersViewEngineeringProjectHours.ascx.cs" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<strong>View Engineering Project Hours</strong>
<br />
- Users (Contractors) in this table are able to see the Engineering Project Hours Report<br />
    
<dxwgv:ASPxGridView ID="grid" runat="server" AutoGenerateColumns="False"
    CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue"
    DataSourceID="UserPrivilegeDataSource" KeyFieldName="UserPrivilegeId" 
    Width="900px"
    OnRowUpdating="grid_RowUpdating" OnInitNewRow="grid_InitNewRow" OnRowInserting="grid_RowInserting">
    <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
        <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
        </LoadingPanelOnStatusBar>
        <CollapsedButton Height="12px"
            Width="11px" />
        <ExpandedButton Height="12px"
            Width="11px" />
        <DetailCollapsedButton Height="12px"
            Width="11px" />
        <DetailExpandedButton Height="12px"
            Width="11px" />
        <LoadingPanel Url="~/App_Themes/Office2003Blue/GridView/Loading.gif">
        </LoadingPanel>
    </Images>
    <ImagesFilterControl>
        <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
        </LoadingPanel>
    </ImagesFilterControl>
    <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
        CssPostfix="Office2003Blue">
        <Header ImageSpacing="5px" SortingImageSpacing="5px">
        </Header>
        <LoadingPanel ImageSpacing="10px">
        </LoadingPanel>
    </Styles>
    <Columns>
        <dxwgv:GridViewCommandColumn Caption="Action" VisibleIndex="0" Width="120px">
            <EditButton Visible="True">
            </EditButton>
            <NewButton Visible="True">
            </NewButton>
            <DeleteButton Visible="True">
            </DeleteButton>
            <ClearFilterButton Visible="True">
            </ClearFilterButton>
        </dxwgv:GridViewCommandColumn>
        <dxwgv:GridViewDataTextColumn FieldName="UserPrivilegeId" ReadOnly="True"
            VisibleIndex="0" Visible="False">
            <EditFormSettings Visible="False" />
        </dxwgv:GridViewDataTextColumn>
        
         <dxwgv:GridViewDataComboBoxColumn
                    Caption="Name"
                    FieldName="UserId"
                    unboundType="String"
                    VisibleIndex="1" SortIndex="0" SortOrder="Ascending">
            <PropertiesComboBox
                        DataSourceID="UsersDataSource"
                        DropDownHeight="150px"
                        TextField="UserDetails"
                        ValueField="UserId" IncrementalFilteringMode="StartsWith"
                        ValueType="System.Int32">
                    </PropertiesComboBox>
            <Settings SortMode="DisplayText" />
        </dxwgv:GridViewDataComboBoxColumn>
        
        <dxwgv:GridViewDataTextColumn FieldName="PrivilegeId" VisibleIndex="2" 
            Visible="False">
        </dxwgv:GridViewDataTextColumn>
        <dxwgv:GridViewDataCheckColumn FieldName="Active" VisibleIndex="3">
        </dxwgv:GridViewDataCheckColumn>
        <dxwgv:GridViewDataTextColumn FieldName="RegionId" VisibleIndex="4" 
            Visible="False">
        </dxwgv:GridViewDataTextColumn>
        <dxwgv:GridViewDataTextColumn FieldName="SiteId" VisibleIndex="5" 
            Visible="False">
        </dxwgv:GridViewDataTextColumn>
        <dxwgv:GridViewDataTextColumn FieldName="CreatedByUserId" VisibleIndex="6" 
            Visible="False">
        </dxwgv:GridViewDataTextColumn>
        <dxwgv:GridViewDataDateColumn FieldName="CreatedDate" VisibleIndex="7" 
            Visible="False">
        </dxwgv:GridViewDataDateColumn>
        <dxwgv:GridViewDataTextColumn FieldName="ModifiedByUserId" VisibleIndex="8" 
            Visible="False">
        </dxwgv:GridViewDataTextColumn>
        <dxwgv:GridViewDataDateColumn FieldName="ModifiedDate" VisibleIndex="9" 
            Visible="False">
        </dxwgv:GridViewDataDateColumn>
    </Columns>
    <SettingsPager NumericButtonCount="5" PageSize="5" AlwaysShowPager="True">
        <AllButton Visible="True">
        </AllButton>
    </SettingsPager>
    <Settings ShowFilterRow="true" />
    <SettingsBehavior ColumnResizeMode="NextColumn" ConfirmDelete="True"></SettingsBehavior>
</dxwgv:ASPxGridView>

<%--<asp:SqlDataSource ID="UserPrivilege_DataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="_UserPrivilege_GetAll_ProjectHoursVisibleForAllCompanies" SelectCommandType="StoredProcedure"></asp:SqlDataSource>--%>

<asp:SqlDataSource
	ID="UsersDataSource"
	runat="server"
	ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="SELECT dbo.Users.UserId, dbo.Users.LastName + ', ' + dbo.Users.FirstName + ' (' + dbo.Users.UserLogon + ') (' + dbo.Companies.CompanyName + ')' As UserDetails FROM dbo.Users INNER JOIN dbo.Companies ON dbo.Users.CompanyId = dbo.Companies.CompanyID AND (dbo.Users.RoleId = 2) ORDER BY dbo.Users.LastName">
</asp:SqlDataSource>


<%--<data:UserPrivilegeDataSource ID="UserPrivilege_DataSource" runat="server">
</data:UserPrivilegeDataSource>--%>

<asp:SqlDataSource
	ID="UserPrivilegeDataSource"
	runat="server"
	ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="SELECT [UserPrivilegeId],[UserId],[PrivilegeId],[Active],[RegionId],[SiteId],[CreatedByUserId],[CreatedDate],[ModifiedByUserId],[ModifiedDate] FROM [dbo].[UserPrivilege] where [UserId] in (select [UserId] from [Users] where [RoleId] = 2)">
</asp:SqlDataSource>