﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.ConfigSa812Provider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(ConfigSa812DataSourceDesigner))]
	public class ConfigSa812DataSource : ProviderDataSource<ConfigSa812, ConfigSa812Key>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ConfigSa812DataSource class.
		/// </summary>
		public ConfigSa812DataSource() : base(new ConfigSa812Service())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the ConfigSa812DataSourceView used by the ConfigSa812DataSource.
		/// </summary>
		protected ConfigSa812DataSourceView ConfigSa812View
		{
			get { return ( View as ConfigSa812DataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the ConfigSa812DataSource control invokes to retrieve data.
		/// </summary>
		public ConfigSa812SelectMethod SelectMethod
		{
			get
			{
				ConfigSa812SelectMethod selectMethod = ConfigSa812SelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (ConfigSa812SelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the ConfigSa812DataSourceView class that is to be
		/// used by the ConfigSa812DataSource.
		/// </summary>
		/// <returns>An instance of the ConfigSa812DataSourceView class.</returns>
		protected override BaseDataSourceView<ConfigSa812, ConfigSa812Key> GetNewDataSourceView()
		{
			return new ConfigSa812DataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the ConfigSa812DataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class ConfigSa812DataSourceView : ProviderDataSourceView<ConfigSa812, ConfigSa812Key>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ConfigSa812DataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the ConfigSa812DataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public ConfigSa812DataSourceView(ConfigSa812DataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal ConfigSa812DataSource ConfigSa812Owner
		{
			get { return Owner as ConfigSa812DataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal ConfigSa812SelectMethod SelectMethod
		{
			get { return ConfigSa812Owner.SelectMethod; }
			set { ConfigSa812Owner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal ConfigSa812Service ConfigSa812Provider
		{
			get { return Provider as ConfigSa812Service; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<ConfigSa812> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<ConfigSa812> results = null;
			ConfigSa812 item;
			count = 0;
			
			System.Int32 _sa812Id;
			System.Int32 _siteId;
			System.Int32? _supervisorUserId_nullable;
			System.Int32? _ehsConsultantId_nullable;

			switch ( SelectMethod )
			{
				case ConfigSa812SelectMethod.Get:
					ConfigSa812Key entityKey  = new ConfigSa812Key();
					entityKey.Load(values);
					item = ConfigSa812Provider.Get(entityKey);
					results = new TList<ConfigSa812>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case ConfigSa812SelectMethod.GetAll:
                    results = ConfigSa812Provider.GetAll(StartIndex, PageSize, out count);
                    break;
				case ConfigSa812SelectMethod.GetPaged:
					results = ConfigSa812Provider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case ConfigSa812SelectMethod.Find:
					if ( FilterParameters != null )
						results = ConfigSa812Provider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = ConfigSa812Provider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case ConfigSa812SelectMethod.GetBySa812Id:
					_sa812Id = ( values["Sa812Id"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["Sa812Id"], typeof(System.Int32)) : (int)0;
					item = ConfigSa812Provider.GetBySa812Id(_sa812Id);
					results = new TList<ConfigSa812>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				case ConfigSa812SelectMethod.GetBySiteId:
					_siteId = ( values["SiteId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["SiteId"], typeof(System.Int32)) : (int)0;
					item = ConfigSa812Provider.GetBySiteId(_siteId);
					results = new TList<ConfigSa812>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// FK
				case ConfigSa812SelectMethod.GetBySupervisorUserId:
					_supervisorUserId_nullable = (System.Int32?) EntityUtil.ChangeType(values["SupervisorUserId"], typeof(System.Int32?));
					results = ConfigSa812Provider.GetBySupervisorUserId(_supervisorUserId_nullable, this.StartIndex, this.PageSize, out count);
					break;
				case ConfigSa812SelectMethod.GetByEhsConsultantId:
					_ehsConsultantId_nullable = (System.Int32?) EntityUtil.ChangeType(values["EhsConsultantId"], typeof(System.Int32?));
					results = ConfigSa812Provider.GetByEhsConsultantId(_ehsConsultantId_nullable, this.StartIndex, this.PageSize, out count);
					break;
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == ConfigSa812SelectMethod.Get || SelectMethod == ConfigSa812SelectMethod.GetBySa812Id )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				ConfigSa812 entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					ConfigSa812Provider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<ConfigSa812> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			ConfigSa812Provider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region ConfigSa812DataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the ConfigSa812DataSource class.
	/// </summary>
	public class ConfigSa812DataSourceDesigner : ProviderDataSourceDesigner<ConfigSa812, ConfigSa812Key>
	{
		/// <summary>
		/// Initializes a new instance of the ConfigSa812DataSourceDesigner class.
		/// </summary>
		public ConfigSa812DataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public ConfigSa812SelectMethod SelectMethod
		{
			get { return ((ConfigSa812DataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new ConfigSa812DataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region ConfigSa812DataSourceActionList

	/// <summary>
	/// Supports the ConfigSa812DataSourceDesigner class.
	/// </summary>
	internal class ConfigSa812DataSourceActionList : DesignerActionList
	{
		private ConfigSa812DataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the ConfigSa812DataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public ConfigSa812DataSourceActionList(ConfigSa812DataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public ConfigSa812SelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion ConfigSa812DataSourceActionList
	
	#endregion ConfigSa812DataSourceDesigner
	
	#region ConfigSa812SelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the ConfigSa812DataSource.SelectMethod property.
	/// </summary>
	public enum ConfigSa812SelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetBySa812Id method.
		/// </summary>
		GetBySa812Id,
		/// <summary>
		/// Represents the GetBySiteId method.
		/// </summary>
		GetBySiteId,
		/// <summary>
		/// Represents the GetBySupervisorUserId method.
		/// </summary>
		GetBySupervisorUserId,
		/// <summary>
		/// Represents the GetByEhsConsultantId method.
		/// </summary>
		GetByEhsConsultantId
	}
	
	#endregion ConfigSa812SelectMethod

	#region ConfigSa812Filter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ConfigSa812"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ConfigSa812Filter : SqlFilter<ConfigSa812Column>
	{
	}
	
	#endregion ConfigSa812Filter

	#region ConfigSa812ExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ConfigSa812"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ConfigSa812ExpressionBuilder : SqlExpressionBuilder<ConfigSa812Column>
	{
	}
	
	#endregion ConfigSa812ExpressionBuilder	

	#region ConfigSa812Property
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;ConfigSa812ChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="ConfigSa812"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ConfigSa812Property : ChildEntityProperty<ConfigSa812ChildEntityTypes>
	{
	}
	
	#endregion ConfigSa812Property
}

