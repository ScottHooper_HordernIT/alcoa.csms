﻿#region Using directives

using System;

#endregion

namespace KaiZen.CSMS.Entities
{	
	///<summary>
	/// An object representation of the 'QuestionnairePresentlyWithUsersActions' view. [No description found in the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class QuestionnairePresentlyWithUsersActions : QuestionnairePresentlyWithUsersActionsBase
	{
		#region Constructors

		///<summary>
		/// Creates a new <see cref="QuestionnairePresentlyWithUsersActions"/> instance.
		///</summary>
		public QuestionnairePresentlyWithUsersActions():base(){}	
		
		#endregion
	}
}
