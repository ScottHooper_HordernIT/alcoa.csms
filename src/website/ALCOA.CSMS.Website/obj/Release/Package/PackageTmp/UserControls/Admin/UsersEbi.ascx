<%@ Control Language="C#" AutoEventWireup="true" Inherits="Adminv2_UserControls_Ebi" Codebehind="UsersEbi.ascx.cs" %>

<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
    
<dxwgv:ASPxGridView
    ID="ASPxGridView1"
    runat="server" AutoGenerateColumns="False"
    CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
    CssPostfix="Office2003Blue"
    DataSourceID="UsersEbiDataSource2"
    KeyFieldName="UsersEbiId"
    Width="900px" OnInitNewRow="grid_InitNewRow" OnRowInserting="grid_RowInserting" OnRowUpdating="grid_RowUpdating"
    >
    <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
        <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
        </LoadingPanelOnStatusBar>
        <CollapsedButton Height="12px" Width="11px" />
        <ExpandedButton Height="12px" Width="11px" />
        <DetailCollapsedButton Height="12px" Width="11px" />
        <DetailExpandedButton Height="12px" Width="11px" />
        <LoadingPanel Url="~/App_Themes/Office2003Blue/GridView/Loading.gif">
        </LoadingPanel>
    </Images>
    <ImagesFilterControl>
        <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
        </LoadingPanel>
    </ImagesFilterControl>
    <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
        CssPostfix="Office2003Blue">
        <Header ImageSpacing="5px" SortingImageSpacing="5px" />
        <LoadingPanel ImageSpacing="10px" />
    </Styles>
    <Columns>
        <dxwgv:GridViewCommandColumn Caption="Action" VisibleIndex="0" Width="120px">
            <EditButton Visible="True">
            </EditButton>
            <NewButton Visible="True">
            </NewButton>
            <DeleteButton Visible="True">
            </DeleteButton>
            <ClearFilterButton Visible="True">
            </ClearFilterButton>
        </dxwgv:GridViewCommandColumn>
        <dxwgv:GridViewDataTextColumn FieldName="UsersEbiId" ReadOnly="True" Visible="False"
            VisibleIndex="0" Width="0px">
            <EditFormSettings Visible="False" />
        </dxwgv:GridViewDataTextColumn>
        
        <dxwgv:GridViewDataComboBoxColumn
                    Caption="Name"
                    FieldName="UserId"
                    unboundType="String"
                    VisibleIndex="1" SortIndex="0" SortOrder="Ascending">
                    <PropertiesComboBox
                        DataSourceID="UsersDataSource"
                        DropDownHeight="150px"
                        TextField="UserDetails"
                        ValueField="UserId" IncrementalFilteringMode="StartsWith"
                        ValueType="System.Int32">
                    </PropertiesComboBox>
                    <EditItemTemplate>
            <dxe:ASPxComboBox ID="usersp1" runat="server" DataSourceID="SqlDataSource1"  TextField="UserDetails" ValueField="UserId" ValueType="System.Int32" Value='<%# Eval("UserId")%>' IncrementalFilteringMode="StartsWith" Width="400px"
            CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"></dxe:ASPxComboBox>
            </EditItemTemplate>
            
                </dxwgv:GridViewDataComboBoxColumn>
                
    </Columns>
    <SettingsPager NumericButtonCount="5" PageSize="5" AlwaysShowPager="True" >
        <AllButton Visible="True">
        </AllButton>
    </SettingsPager>
    <SettingsEditing Mode="Inline" />
    <Settings ShowFilterRow="true" />
    <SettingsBehavior ColumnResizeMode="NextColumn" ConfirmDelete="True"></SettingsBehavior>
</dxwgv:ASPxGridView>


<%--<data:UsersEbiDataSource ID="UsersEbiDataSource1" runat="server" EnableDeepLoad="False"
          EnableSorting="true" SelectMethod="GetAll">
</data:UsersEbiDataSource>--%>
<asp:SqlDataSource ID="UsersEbiDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    DeleteCommand="DELETE FROM [UsersEbi] WHERE [UsersEbiId] = @UsersEbiId"
    InsertCommand="INSERT INTO [UsersEbi] ([UserId],[ModifiedByUserId],[ModifiedDate]) VALUES (@UserId,@ModifiedByUserId,@ModifiedDate)"
    SelectCommand="SELECT * FROM [UsersEbi] where [UserId] in (select [UserId] from [Users] where [RoleId] < 3)"
    UpdateCommand="UPDATE [UsersEbi] SET [UserId] = @UserId,[ModifiedByUserId]=@ModifiedByUserId,[ModifiedDate]=@ModifiedDate WHERE [UsersEbiId] = @UsersEbiId">
    <DeleteParameters>
        <asp:Parameter Name="UsersEbiId" Type="Int32" />
    </DeleteParameters>
    <UpdateParameters>
        <asp:Parameter Name="UserId" Type="Int32" />
        <asp:Parameter Name="ModifiedByUserId" Type="Int32" />
        <asp:Parameter Name="ModifiedDate" Type="DateTime" />
        <asp:Parameter Name="UsersEbiId" Type="Int32" />
    </UpdateParameters>
    <InsertParameters>
        <asp:Parameter Name="UserId" Type="Int32" />
        <asp:Parameter Name="ModifiedByUserId" Type="Int32" />
        <asp:Parameter Name="ModifiedDate" Type="DateTime" />
    </InsertParameters>
</asp:SqlDataSource>

<asp:SqlDataSource
	ID="UsersDataSource"
	runat="server"
	ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="SELECT dbo.Users.UserId, dbo.Users.LastName + ', ' + dbo.Users.FirstName + ' (' + dbo.Users.UserLogon + ') (' + dbo.Companies.CompanyName + ')' As UserDetails FROM dbo.Users INNER JOIN dbo.Companies ON dbo.Users.CompanyId = dbo.Companies.CompanyID AND (dbo.Users.RoleId < 3) AND dbo.Users.UserId in (select Distinct dbo.UsersEbi.UserId from dbo.UsersEbi) ORDER BY dbo.Users.LastName">
</asp:SqlDataSource>
<asp:SqlDataSource
	ID="SqlDataSource1"
	runat="server"
	ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="SELECT dbo.Users.UserId, dbo.Users.LastName + ', ' + dbo.Users.FirstName + ' (' + dbo.Users.UserLogon + ') (' + dbo.Companies.CompanyName + ')' As UserDetails FROM dbo.Users INNER JOIN dbo.Companies ON dbo.Users.CompanyId = dbo.Companies.CompanyID AND (dbo.Users.RoleId < 3)  ORDER BY dbo.Users.LastName">
</asp:SqlDataSource>