﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Repo.CSMS.DAL.EntityModels;

namespace Repo.CSMS.DAL.Infrastructure
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly IObjectContext _objectContext;

        public UnitOfWork(IObjectContext objectContext)
        {
            _objectContext = objectContext;
        }

        public void Commit()
        {
            _objectContext.SaveChanges();

            //if (CSMS.Common.Config.AppSettings.Caching == CSMS.Common.Config.Caching.Enabled)
            //{
            //    DatabaseFactory.Cache.Clear();
            //}
        }

        public bool LazyLoadingEnabled
        {
            set { _objectContext.ContextOptions.LazyLoadingEnabled = value; }
            get { return _objectContext.ContextOptions.LazyLoadingEnabled; }
        }

        private bool _disposed;
        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing && _objectContext != null)
                {
                    _objectContext.Dispose();
                }
            }
            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
