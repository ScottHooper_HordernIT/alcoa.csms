﻿<%@ Control Language="C#" AutoEventWireup="true"
    Inherits="UserControls_ComplianceChart2p" Codebehind="ComplianceChart2p.ascx.cs" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dxpc" %>
<%@ Register Src="../Other/Reports_DropDownListPicker2p.ascx" TagName="Reports_DropDownListPicker2p"
    TagPrefix="uc1" %>
<table border="0" cellpadding="2" cellspacing="0" width="900px">
    <tr>
        <td class="pageName" colspan="7" style="text-align: left; width: 900px; height: 30px;">
            <span class="bodycopy"><span class="title">Safety KPI Compliance Chart (Radar)</span><br />
                <span class="date"><dxe:ASPxLabel ID="lblInternal" runat="server"
                    Text="External">
                </dxe:ASPxLabel> Presentation
                </span><br />
                <img height="1" src="images/grfc_dottedline.gif" width="24" />
            </span>
        </td>
    </tr>
    <tr>
        <td class="pageName" colspan="7" style="width: 900px; height: 17px; text-align: left">
            <table border="0" cellpadding="0" cellspacing="0" style="width: 900px">
                <tr>
                    <td style="width: 700px">
                        <uc1:Reports_DropDownListPicker2p ID="Reports_DropDownListPicker" runat="server" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="pageName" colspan="7" style="width: 900px; height: 17px; text-align: left">
            <asp:PlaceHolder ID="PlaceHolder1" runat="server"></asp:PlaceHolder>
        </td>
    </tr>
    <tr>
    </tr>
</table>
<dxpc:ASPxPopupControl ID="ASPxPopupControl1" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
    CssPostfix="Office2003Blue" EnableHotTrack="False" HeaderText="Warning" Height="111px"
    Modal="True" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter"
    Width="439px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
    <SizeGripImage Height="16px" Width="16px">
    </SizeGripImage>
    <HeaderStyle>
        <Paddings PaddingRight="6px"></Paddings>
    </HeaderStyle>
    <CloseButtonImage Height="12px" Width="13px">
    </CloseButtonImage>
    <ContentCollection>
        <dxpc:PopupControlContentControl runat="server" SupportsDisabledAttribute="True">
            <dxe:ASPxLabel runat="server" Height="30px" Font-Size="14px" ID="ASPxLabel1">
                <Border BorderColor="White" BorderWidth="10px"></Border>
            </dxe:ASPxLabel>
        </dxpc:PopupControlContentControl>
    </ContentCollection>
</dxpc:ASPxPopupControl>

<asp:SqlDataSource ID="sqldsKpi_GetAllYearsSubmitted" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="_Kpi_GetAllYearsSubmitted" SelectCommandType="StoredProcedure">
</asp:SqlDataSource>
