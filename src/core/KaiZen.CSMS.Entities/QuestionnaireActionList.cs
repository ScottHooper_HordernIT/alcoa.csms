﻿
using System;

namespace KaiZen.CSMS.Entities
{
	/// <summary>
	/// An enum representation of the 'QuestionnaireAction' table. [No description found in the database]
	/// </summary>
	/// <remark>this enumeration contains the items contained in the table QuestionnaireAction</remark>
	[Serializable]
	public enum QuestionnaireActionList
	{
		/// <summary> 
		/// Internal Comment Added
		/// </summary>
		[EnumTextValue(@"Internal Comment Added")]
		InternalCommentAdded = 0, 

		/// <summary> 
		/// Procurement Questionnaire Created
		/// </summary>
		[EnumTextValue(@"Procurement Questionnaire Created")]
		ProcurementQuestionnaireCreated = 1, 

		/// <summary> 
		/// Safety Questionnaire Modified
		/// </summary>
		[EnumTextValue(@"Safety Questionnaire Modified")]
		SafetyQuestionnaireModify = 2, 

		/// <summary> 
		/// Supplier Contact Verification Email Sent
		/// </summary>
		[EnumTextValue(@"Supplier Contact Verification Email Sent")]
		SupplierContactVerificationEmailSent = 3, 

		/// <summary> 
		/// Supplier Contact Verification Email Received Confirmation
		/// </summary>
		[EnumTextValue(@"Supplier Contact Verification Email Received Confirmation")]
		SupplierContactVerificationEmailReceiptConfirmed = 4, 

		/// <summary> 
		/// Procurement Questionnaire Submitted
		/// </summary>
		[EnumTextValue(@"Procurement Questionnaire Submitted")]
		ProcurementQuestionnaireSubmitted = 5, 

		/// <summary> 
		/// Procurement Questionnaire Approved
		/// </summary>
		[EnumTextValue(@"Procurement Questionnaire Approved")]
		ProcurementQuestionnaireApproved = 6, 

		/// <summary> 
		/// Supplier Questionnaire Completed
		/// </summary>
		[EnumTextValue(@"Supplier Questionnaire Completed")]
		SupplierQuestionnaireCompleted = 7, 

		/// <summary> 
		/// Verification Questionnaire Completed
		/// </summary>
		[EnumTextValue(@"Verification Questionnaire Completed")]
		VerificationQuestionnaireCompleted = 8, 

		/// <summary> 
		/// Safety Questionnaire Submitted
		/// </summary>
		[EnumTextValue(@"Safety Questionnaire Submitted")]
		SafetyQuestionnaireSubmitted = 9, 

		/// <summary> 
		/// Safety Questionnaire Not Approved
		/// </summary>
		[EnumTextValue(@"Safety Questionnaire Not Approved")]
		SafetyQuestionnaireNotApproved = 10, 

		/// <summary> 
		/// Safety Questionnaire Re-Submitted
		/// </summary>
		[EnumTextValue(@"Safety Questionnaire Re-Submitted")]
		SafetyQuestionnaireReSubmitted = 11, 

		/// <summary> 
		/// Safety Questionnaire Approved and Assessment Complete
		/// </summary>
		[EnumTextValue(@"Safety Questionnaire Approved and Assessment Complete")]
		SafetyQuestionnaireApprovedAssessmentComplete = 12, 

		/// <summary> 
		/// Safety Questionnaire Approved and Assessment Complete
		/// </summary>
        [EnumTextValue(@"Verification Questionnaire Forcibly Required")]
		VerificationQuestionnaireForciblyRequired = 13, 

        //This enum added by Ashley Goldstraw 11/1/2016 DT222
         /// <summary> 
        /// Procurement Questionnaire Rejected
        /// </summary>
        [EnumTextValue(@"Procurement Questionnaire Rejected")]
        ProcurementQuestionnaireRejected = 14,

        /// <summary> 
		/// Company Location Approval Added
		/// </summary>
		[EnumTextValue(@"Company Location Approval Added")]
		LocationApprovalAdded = 100, 

		/// <summary> 
		/// Company Location Approval Modified
		/// </summary>
		[EnumTextValue(@"Company Location Approval Modified")]
		LocationApprovalModified = 101, 

		/// <summary> 
		/// Company Location Approval Deleted
		/// </summary>
		[EnumTextValue(@"Company Location Approval Deleted")]
		LocationApprovalDeleted = 102, 

		/// <summary> 
		/// Procurement Contact Assigned
		/// </summary>
		[EnumTextValue(@"Procurement Contact Assigned")]
		ProcurementContactAssigned = 103, 

		/// <summary> 
		/// Procurement Functional Manager Assigned
		/// </summary>
		[EnumTextValue(@"Procurement Functional Manager Assigned")]
		ProcurementFunctionalManagerAssigned = 104, 

		/// <summary> 
		/// Safety Assessor Assigned
		/// </summary>
		[EnumTextValue(@"Safety Assessor Assigned")]
		SafetyAssessorAssigned = 105, 

		/// <summary> 
		/// Company Status Changed
		/// </summary>
		[EnumTextValue(@"Company Status Changed")]
		CompanyStatusChanged = 106, 

		/// <summary> 
		/// Level of Supervision Changed
		/// </summary>
		[EnumTextValue(@"Level of Supervision Changed")]
		LevelOfSupervisionChanged = 107, 

		/// <summary> 
		/// Recommendation Comments Changed
		/// </summary>
		[EnumTextValue(@"Recommendation Comments Changed")]
		RecommendationCommentsChanged = 108, 

		/// <summary> 
		/// Company Status Change Requested - Approval Pending
		/// </summary>
		[EnumTextValue(@"Company Status Change Requested - Approval Pending")]
		CompanyStatusChangeApprovalPending = 200, 

		/// <summary> 
		/// Company Status Change Requested - Approved
		/// </summary>
		[EnumTextValue(@"Company Status Change Requested - Approved")]
		CompanyStatusChangeApproved = 201, 

		/// <summary> 
		/// Company Status Change Requested - Not Approved
		/// </summary>
		[EnumTextValue(@"Company Status Change Requested - Not Approved")]
		CompanyStatusChangeNotApproved = 202,

        /// <summary> 
        /// Company Note Added
        /// </summary>
        [EnumTextValue(@"Company Note Added")]
        CompanyNoteAdded = 300

        
    }
}
