﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;
using System.Globalization;
using System.Text;
using System.Security.Cryptography;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Services;
using KaiZen.CSMS.Data;
using KaiZen.Library;

public partial class PopUps_SafetyPlans_Compare : System.Web.UI.Page
{
    string lightGreen = "#80FF80";
    string lightRed = "#FF8080";
    string lightYellow = "#FFFFCC";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["R"] != null)
        {
            ASPxPageControl1.TabPages[0].Enabled = false;
            ASPxPageControl1.TabPages[1].Enabled = false;
            ASPxPageControl1.TabPages[2].Enabled = false;
            int s = 0;
            if (Request.QueryString["S"] != null) s = Convert.ToInt32(Request.QueryString["S"]);
            compare(Convert.ToInt32(Request.QueryString["R"].ToString()), s); //http://www.mathertel.de/Diff/default.aspx
        }
        else
        {
            errorOccured("Error: No Safety Plan Specified");
        }
    }
    protected void errorOccured(string error)
    {
        cbDifference.Visible = false;
        //ASPxButton1.Visible = false;
        ASPxButton2.Visible = false;
        ASPxPageControl1.Visible = false;
        lblError.Text = error;
    }

    protected void compare(int rNo, int sNo)
    {
        SafetyPlansSeResponsesService sService = new SafetyPlansSeResponsesService();
        FileDbService fService = new FileDbService();
        CompaniesService cService = new CompaniesService();
        UsersService uService = new UsersService();

        SafetyPlansSeResponses sNew = sService.GetByResponseId(rNo);

        TList<FileDb> fList = fService.GetBySelfAssessmentResponseId(rNo);
        FileDb fNew = (FileDb)fList[0];

        //find previous questionnaire to compare against
        int count = 0;
        SafetyPlansSeResponsesFilters sFilters;
        SafetyPlansSeResponses sOld = new SafetyPlansSeResponses();
        if (sNo == 0)
        {
            sFilters = new SafetyPlansSeResponsesFilters();
            sFilters.Append(SafetyPlansSeResponsesColumn.CompanyId, sNew.CompanyId.ToString());
            sFilters.Append(SafetyPlansSeResponsesColumn.Submitted, "1");
            sFilters.AppendLessThan(SafetyPlansSeResponsesColumn.CreatedDate, sNew.CreatedDate.ToString("yyyy-MM-dd hh:mm:ss"));
            TList<SafetyPlansSeResponses> sList = DataRepository.SafetyPlansSeResponsesProvider.GetPaged(sFilters.ToString(), "CreatedDate DESC", 0, 100, out count);
            if (count > 0)
            {
                sOld = (SafetyPlansSeResponses)sList[0];
            }
        }
        else
        {
            sOld = sService.GetByResponseId(sNo);
            if (sOld != null)
            {
                count++;
                //errorOccured("No second safety plan exists to compare against.");
                //throw new Exception("No second safety plan exists to compare against.");
            }

        }

        if (count > 0)
        {

            TList<FileDb> fOldList = fService.GetBySelfAssessmentResponseId(sOld.ResponseId);
            FileDb fOld = (FileDb)fOldList[0];
            /*
             * Safety Plan
             */
            HtmlTable table = new HtmlTable();
            table.ID = "tbSafetyPlan";
            table.Width = "100%";
            if (!cbDifference.Checked) table.Rows.Add(rowTitle_Combined("Previous Safety Plan", "Current Safety Plan", true, Color.Red, "", ""));

            Companies c = cService.GetByCompanyId(sNew.CompanyId);
            Companies c2 = cService.GetByCompanyId(sOld.CompanyId);
            compare("Company", table, c2.CompanyName, c.CompanyName, cbDifference.Checked, true);
            lblCompanyName.Text = c.CompanyName;
            //-Created By
            Users uOld = uService.GetByUserId(sOld.CreatedByUserId);
            Users uNew = uService.GetByUserId(sNew.CreatedByUserId);
            Companies cOld = cService.GetByCompanyId(uOld.CompanyId);
            Companies cNew = cService.GetByCompanyId(uNew.CompanyId);
            compare("Created By", table, uOld.LastName + ", " + uOld.FirstName + " (" + cOld.CompanyName + ") at " + sOld.CreatedDate.ToString(),
                                         uNew.LastName + ", " + uNew.FirstName + " (" + cNew.CompanyName + ") at " + sNew.CreatedDate.ToString(), cbDifference.Checked, true);
            //-Modified By
            uOld = uService.GetByUserId(sOld.ModifiedByUserId);
            uNew = uService.GetByUserId(sNew.ModifiedByUserId);
            cOld = cService.GetByCompanyId(uOld.CompanyId);
            cNew = cService.GetByCompanyId(uNew.CompanyId);
            compare("Modified By", table, uOld.LastName + ", " + uOld.FirstName + " (" + cOld.CompanyName + ") at " + sOld.ModifiedDate.ToString(),
                                         uNew.LastName + ", " + uNew.FirstName + " (" + cNew.CompanyName + ") at " + sNew.ModifiedDate.ToString(),
                        cbDifference.Checked, true);
            //Uploaded File Name
            compare("File Name", table, fOld.FileName, fNew.FileName, cbDifference.Checked, true);
            compare("File Description", table, fOld.Description, fNew.Description, cbDifference.Checked, true);
            ASPxPageControl1.TabPages[0].Enabled = true;
            phSafetyPlan.Controls.Add(table);

            /*
             * Questionnaire
             */
            HtmlTable tableQuestionnaire = new HtmlTable();
            tableQuestionnaire.ID = "tbQuestionnaire";
            tableQuestionnaire.Width = "100%";

            SafetyPlansSeQuestionsService qService = new SafetyPlansSeQuestionsService();

            int i = 1;
            while (i <= 10)
            {
                string question = "";
                SafetyPlansSeQuestions q = qService.GetByQuestionId(i);
                if (q != null) question = q.Question;
                compare(i.ToString() + " - " + question, tableQuestionnaire, getQuestionnaireValue(sOld.ResponseId, i),
                                                        getQuestionnaireValue(sNew.ResponseId, i), cbDifference.Checked, true);
                compare(i.ToString() + " - Comments", tableQuestionnaire, getQuestionnaireValue_Comments(sOld.ResponseId, i),
                                                        getQuestionnaireValue_Comments(sNew.ResponseId, i), cbDifference.Checked, true);
                i++;
            }

            ASPxPageControl1.TabPages[1].Enabled = true;
            phQuestionnaire.Controls.Add(tableQuestionnaire);

            /*
             * Reviewal
             */
            HtmlTable tableReviewal = new HtmlTable();
            tableReviewal.ID = "tbReviewal";
            tableReviewal.Width = "100%";

            i = 1;
            while (i <= 10)
            {
                compare(i.ToString() + " - Assessor Approval", tableReviewal, getQuestionnaireValue_AssessorApproval(sOld.ResponseId, i),
                                                        getQuestionnaireValue_AssessorApproval(sNew.ResponseId, i), cbDifference.Checked, true);
                compare(i.ToString() + " - Assessor Comments", tableReviewal, getQuestionnaireValue_Comments_AssessorApproval(sOld.ResponseId, i),
                                                        getQuestionnaireValue_Comments_AssessorApproval(sNew.ResponseId, i), cbDifference.Checked, true);
                i++;
            }

            ASPxPageControl1.TabPages[2].Enabled = true;
            phReviewal.Controls.Add(tableReviewal);
        }
        else
        {
            errorOccured("No previously submitted safety plan exists to compare against.");
        }
    }

    protected string getQuestionnaireValue(int responseId, int questionId)
    {
        string value = "";
        SafetyPlansSeAnswersService sService = new SafetyPlansSeAnswersService();
        SafetyPlansSeAnswers s = sService.GetByQuestionIdResponseId(questionId, responseId);
        if (s != null) value = s.Answer.ToString();
        if (value == "True") value = "Yes";
        if (value == "False") value = "No";
        return value;
    }
    protected string getQuestionnaireValue_Comments(int responseId, int questionId)
    {
        string value = "";
        SafetyPlansSeAnswersService sService = new SafetyPlansSeAnswersService();
        SafetyPlansSeAnswers s = sService.GetByQuestionIdResponseId(questionId, responseId);
        if (s != null)
        {
            if (s.Comment != null)
            {
                value = Encoding.ASCII.GetString(s.Comment);
            }
        }
        return value;
    }
    protected string getQuestionnaireValue_AssessorApproval(int responseId, int questionId)
    {
        string value = "";
        SafetyPlansSeAnswersService sService = new SafetyPlansSeAnswersService();
        SafetyPlansSeAnswers s = sService.GetByQuestionIdResponseId(questionId, responseId);
        if (s != null) value = s.AssessorApproval.ToString();
        if (value == "True") value = "Yes";
        if (value == "False") value = "No";
        return value;
    }
    protected string getQuestionnaireValue_Comments_AssessorApproval(int responseId, int questionId)
    {
        string value = "";
        SafetyPlansSeAnswersService sService = new SafetyPlansSeAnswersService();
        SafetyPlansSeAnswers s = sService.GetByQuestionIdResponseId(questionId, responseId);
        try
        {
            if (s != null)
            {
                if (s.AssessorComment != null) value = Encoding.ASCII.GetString(s.AssessorComment);
            }
        }
        catch (Exception ex) { Elmah.ErrorSignal.FromContext(Context).Raise(ex); }
        return value;
    }

    protected HtmlTableRow rowTitle(string title, bool bold, Color color, string bgcolor)
    {
        HtmlTableRow row = new HtmlTableRow();
        HtmlTableCell cell = new HtmlTableCell();
        Label lbl = new Label();
        lbl.Text = title;
        lbl.Font.Bold = bold;
        lbl.ForeColor = color;
        cell.Style.Add("Padding-Top", "3px");
        cell.ColSpan = 2;
        cell.Controls.Add(lbl);
        cell.BgColor = bgcolor;
        row.Cells.Add(cell);
        return row;
    }
    public HtmlTableRow rowTitle_Combined(string title1, string title2, bool bold, Color color, string bgcolor1, string bgcolor2)
    {
        HtmlTableRow row = new HtmlTableRow();

        HtmlTableCell cell1 = new HtmlTableCell();
        Label lbl1 = new Label();
        lbl1.Text = title1;
        lbl1.Font.Bold = bold;
        lbl1.ForeColor = color;
        cell1.Style.Add("Padding-Top", "3px");
        cell1.Controls.Add(lbl1);
        cell1.BgColor = bgcolor1;
        cell1.Width = "50%";
        if (title1 == "Previous Safety Plan") cell1.Align = "Center";
        row.Cells.Add(cell1);

        HtmlTableCell cell2 = new HtmlTableCell();
        Label lbl2 = new Label();
        lbl2.Text = title2;
        lbl2.Font.Bold = bold;
        lbl2.ForeColor = color;
        cell2.Style.Add("Padding-Top", "3px");
        cell2.Controls.Add(lbl2);
        cell2.BgColor = bgcolor2;
        cell2.Width = "50%";
        if (title2 == "Current Safety Plan") cell2.Align = "Center";
        row.Cells.Add(cell2);

        return row;
    }
    protected HtmlTableRow rowCompare(string cellContents)
    {
        HtmlTableRow row = new HtmlTableRow();
        HtmlTableCell cell = new HtmlTableCell();
        cell.Controls.Add(new LiteralControl(cellContents));
        row.Cells.Add(cell);
        return row;
    }
    private static int[] DiffCharCodes(string aText, bool ignoreCase)
    {
        int[] Codes;

        if (ignoreCase)
            aText = aText.ToUpperInvariant();

        Codes = new int[aText.Length];

        for (int n = 0; n < aText.Length; n++)
            Codes[n] = (int)aText[n];

        return (Codes);
    } // DiffCharCodes
    protected void compare(string Title, HtmlTable table, string one, string two, bool diff, bool combined)
    {
        table.Rows.Add(rowTitle(Title, true, Color.Black, ""));
        if (diff == true)
        {
            int[] a_codes = DiffCharCodes(one, false);
            int[] b_codes = DiffCharCodes(two, false);

            CompareUtilities.Diff.Item[] diffs = CompareUtilities.Diff.DiffInt(a_codes, b_codes);

            String cell = "";

            int pos = 0;
            for (int n = 0; n < diffs.Length; n++)
            {
                CompareUtilities.Diff.Item it = diffs[n];

                // write unchanged chars
                while ((pos < it.StartB) && (pos < two.Length))
                {
                    cell += two[pos];
                    pos++;
                } // while

                // write deleted chars
                if (it.deletedA > 0)
                {
                    cell += "<span class='cd'>";
                    for (int m = 0; m < it.deletedA; m++)
                    {
                        cell += one[it.StartA + m];
                    } // for
                    cell += "</span>";
                }

                // write inserted chars
                if (pos < it.StartB + it.insertedB)
                {
                    cell += "<span class='ci'>";
                    while (pos < it.StartB + it.insertedB)
                    {
                        cell += two[pos];
                        pos++;
                    } // while
                    cell += "</span>";
                } // if
            } // while

            // write rest of unchanged chars
            while (pos < two.Length)
            {
                cell += two[pos];
                pos++;
            } // while

            table.Rows.Add(rowCompare(cell));
        }
        else
        {
            string color1 = "";
            string color2 = "";
            if (String.Compare(one, two) == 0)
            {
                color1 = lightYellow;
                color2 = lightYellow;
            }
            else
            {
                color1 = lightRed;
                color2 = lightGreen;
            }
            if (combined)
            {
                table.Rows.Add(rowTitle_Combined(one, two, false, Color.Black, color1, color2));
            }
            else
            {
                table.Rows.Add(rowTitle(one, false, Color.Black, color1));
                table.Rows.Add(rowTitle(two, false, Color.Black, color2));
            }
        }
    }
    protected void cbDifference_CheckedChanged(object sender, EventArgs e)
    {
    }
}
