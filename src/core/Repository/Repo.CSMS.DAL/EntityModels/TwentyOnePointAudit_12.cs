//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Repo.CSMS.DAL.EntityModels
{
    using System;
    using System.Collections.Generic;
    
    public partial class TwentyOnePointAudit_12
    {
        public TwentyOnePointAudit_12()
        {
            this.TwentyOnePointAudits = new HashSet<TwentyOnePointAudit>();
        }
    
        public int ID { get; set; }
        public Nullable<int> Achieved12a { get; set; }
        public Nullable<int> Achieved12b { get; set; }
        public Nullable<int> Achieved12c { get; set; }
        public Nullable<int> Achieved12d { get; set; }
        public Nullable<int> Achieved12e { get; set; }
        public string Observation12a { get; set; }
        public string Observation12b { get; set; }
        public string Observation12c { get; set; }
        public string Observation12d { get; set; }
        public string Observation12e { get; set; }
        public Nullable<int> TotalScore { get; set; }
    
        public virtual ICollection<TwentyOnePointAudit> TwentyOnePointAudits { get; set; }
    }
}
