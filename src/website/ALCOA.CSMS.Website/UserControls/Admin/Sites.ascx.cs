using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class UserControls_Admin_Sites : System.Web.UI.UserControl
{
    Auth auth = new Auth();

    protected void Page_Load(object sender, EventArgs e) {  Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); }

    private void moduleInit(bool postBack)
    {
        Control ucRegions = LoadControl("~/UserControls/Admin/Regions.ascx");
        phRegions.Controls.Add(ucRegions);

        Control ucRegionsSites = LoadControl("~/UserControls/Admin/RegionsSites.ascx");
        phRegionsSites.Controls.Add(ucRegionsSites);
    }

    protected void grid_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        e.NewValues["ModifiedByUserId"] = auth.UserId;
        e.NewValues["Editable"] = 1;

    }
    protected void grid_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        e.NewValues["ModifiedByUserId"] = auth.UserId;
        e.NewValues["Editable"] = 1;
    }
}
