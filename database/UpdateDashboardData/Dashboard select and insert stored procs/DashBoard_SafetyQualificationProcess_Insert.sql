
/****** Object:  StoredProcedure [dbo].[DashBoard_SafetyQualificationProcess_Insert]    Script Date: 3/08/2015 1:31:45 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[DashBoard_SafetyQualificationProcess_Insert]        
(        
 @Site int        
 ,@Month int        
 ,@Year int        
         
 ,@ExpiredCurrentlyWithProcurement decimal(18,2)        
 ,@ExpiredCurrentlyWithSupplier decimal(18,2)        
 ,@Expiring7days decimal(18,2)        
 ,@Questionnaire7days decimal(18,2)        
 ,@AssignCompanyStatus7days decimal(18,2)        
 ,@Questionnaire28Days decimal(18,2)        
 ,@QuestionnaireResubmitting decimal(18,2)        
 ,@Access7Days decimal(18,2)        
 ,@ExpiredCurrentlyWithHandSAssessor decimal(18,2)        
 ,@AssessingProcurementQuestionnaire7days decimal(18,2)        
 ,@AssessingQuestionnaire7days decimal(18,2)        
 ,@SQprocesstotal decimal(18,2)     
        
)        
AS   
IF EXISTS(select * from DashBoard_SafetyQualificationProcess where SiteId=@Site and [Month]=@Month and [Year]=@Year)       
   
UPDATE  DashBoard_SafetyQualificationProcess  SET  
                   
     [Expired - Currently With Procurement] = @ExpiredCurrentlyWithProcurement       
     ,[Expired - Currently With Supplier] = @ExpiredCurrentlyWithSupplier      
     ,[Expiring > 7 days] = @Expiring7days   
     ,[Questionnaire  > 7 days] = @Questionnaire7days       
     ,[Assign Company Status > 7 days] = @AssignCompanyStatus7days       
     ,[Questionnaire > 28 Days] = @Questionnaire28Days       
     ,[Questionnaire - Resubmitting]  = @QuestionnaireResubmitting    
     ,[Access > 7 Days] = @Access7Days    
     ,[Expired - Currently With H&S Assessor] = @ExpiredCurrentlyWithHandSAssessor       
     ,[Assessing Procurement Questionnaire > 7 days] = @AssessingProcurementQuestionnaire7days     
     ,[Assessing Questionnaire > 7 days] = @AssessingQuestionnaire7days    
     ,[SQ process total] = @SQprocesstotal 
     ,[LastModifiedDate] = GETDATE() 
 WHERE [SiteId] = @Site                  
   AND [Month] = @Month                  
   AND [Year] = @Year   
  
ELSE   
INSERT INTO [dbo].[DashBoard_SafetyQualificationProcess]        
     (        
     [SiteId]        
     ,[Month]        
     ,[Year]                 
     ,[Expired - Currently With Procurement]        
     ,[Expired - Currently With Supplier]        
     ,[Expiring > 7 days]        
     ,[Questionnaire  > 7 days]        
     ,[Assign Company Status > 7 days]        
     ,[Questionnaire > 28 Days]        
     ,[Questionnaire - Resubmitting]        
     ,[Access > 7 Days]        
     ,[Expired - Currently With H&S Assessor]        
     ,[Assessing Procurement Questionnaire > 7 days]        
     ,[Assessing Questionnaire > 7 days]        
     ,[SQ process total]        
     ,[LastModifiedDate]
     )        
    VALUES        
     (        
     @Site        
     ,@Month        
     ,@Year        
     ,@ExpiredCurrentlyWithProcurement        
     ,@ExpiredCurrentlyWithSupplier        
     ,@Expiring7days        
     ,@Questionnaire7days        
     ,@AssignCompanyStatus7days        
     ,@Questionnaire28Days        
     ,@QuestionnaireResubmitting        
     ,@Access7Days        
     ,@ExpiredCurrentlyWithHandSAssessor        
     ,@AssessingProcurementQuestionnaire7days        
     ,@AssessingQuestionnaire7days        
     ,@SQprocesstotal
     ,GETDATE()        
     )        
----------------------------------------

GO


