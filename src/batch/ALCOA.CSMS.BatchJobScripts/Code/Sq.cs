﻿using System;
using System.Collections.Generic;
using System.Text;

using System.Data;
using System.Data.SqlClient;

using KaiZen.CSMS.Data;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Services;
using KaiZen.CSMS.Web;
using KaiZen.Library;

using System.Collections;

using System.IO;
using ALCOA.CSMS.Batch.Common.Log;
using System.Web.Mvc;
using repo = Repo.CSMS.Service.Database;
using model = Repo.CSMS.DAL.EntityModels;

namespace ALCOA.CSMS.BatchJobScripts
{
    class Sq
    {
        public class Email
        {
            public static void ProcurementWorkReminderEmail()
            {
                QuestionnaireReportProgressService qrpService = new QuestionnaireReportProgressService();
                //VList<QuestionnaireReportProgress> qrpList = qrpService.GetAll();
                VList<QuestionnaireReportProgress> qrpList = qrpService.Get("", "CompanyId ASC");

                int _count = 0;
                QuestionnaireReportExpiryService qreService = new QuestionnaireReportExpiryService();
                QuestionnaireReportExpiryFilters qreFilters = new QuestionnaireReportExpiryFilters();
                qreFilters.AppendLessThanOrEqual(QuestionnaireReportExpiryColumn.ExpiresIn, "60");
                VList<QuestionnaireReportExpiry> qreList = qreService.GetPaged(qreFilters.ToString(), "", 0, 99999, out _count);

                CompaniesService cService = new CompaniesService();
                TList<Companies> cList = cService.GetAll();

                UsersProcurementService upService = new UsersProcurementService();
                UsersService uService = new UsersService();
                TList<UsersProcurement> uProcurementList = upService.GetAll();

                foreach (UsersProcurement uProcurement in uProcurementList)
                {
                    Users u = uService.GetByUserId(uProcurement.UserId);

                    string emailToName = String.Format("{0} {1}", u.FirstName, u.LastName);
                    string[] to = { u.Email };
                    string[] cc = { Helper.Configuration.GetValue(ConfigList.ContactEmail).ToString() };
                    string emailSubject = "Safety Qualification Work and Assistance Request";

                    string emailBody = BuildEmailBody(u.UserId, emailToName, qrpList, qreList, cList);

                    if (!String.IsNullOrEmpty(emailBody))
                    {
                        //Cindi Thornton 4/9/2015 - removed call to sendEmail, change to insert into logemail for new columns IsBodyHtml & attachmentPath. EmailLog used to queue & log emails.
                        //Helper.EmailAgent.sendEmail(to, null, null, emailSubject, emailBody, true, null); //Send E-Mail
                        //Helper.EmailAgent.logEmail(to, null, null, emailSubject, emailBody, EmailLogTypeList.ProcurementDailyWorkReminderEmail, true, null); //Log

                        var emailLogService = DependencyResolver.Current.GetService<repo.IEmailLogService>();
                        emailLogService.Insert(to, null, null, emailSubject, emailBody, "ProcurementDailyWorkReminderEmail", true, null, null);

                    }
                }
            }
            private static string BuildEmailBody(int UserId, string FullName, VList<QuestionnaireReportProgress> qrpList, VList<QuestionnaireReportExpiry> qreList, TList<Companies> cList)
            {
                bool sendEmail = false;

                string emailToName = FullName;
                string emailBodyProcurementQuestionnaire = "<font color='#FF0000'>(Thanks! On behalf of the Contractor Management Team, thank you for having no outstanding tasks in this area.)</font>";
                string emailBodyAwaitingContractAssignment = "<font color='#FF0000'>(Thanks! On behalf of the Contractor Management Team, thank you for having no outstanding tasks in this area.)</font>";
                string emailBodyAssistanceRequired = "<font color='#FF0000'>(Thanks! On behalf of the Contractor Management Team, thank you for having no outstanding tasks in this area.)</font>";
                string emailBodySQExpired = "<font color='#FF0000'>(Thanks! On behalf of the Contractor Management Team, thank you for having no outstanding tasks in this area.)</font>";
                string emailBodySQNearlyExpired = "<font color='#FF0000'>(Thanks! On behalf of the Contractor Management Team, thank you for having no outstanding tasks in this area.)</font>";
                string emailBody = "";

                DataTable emailBodyProcurementQuestionnaireDataSet = new DataTable();
                emailBodyProcurementQuestionnaireDataSet.Columns.Add("CompanyName", typeof(string));
                emailBodyProcurementQuestionnaireDataSet.Columns.Add("DaysOnTheList", typeof(int));

                DataTable emailBodyAwaitingContractAssignmentDataSet = new DataTable();
                emailBodyAwaitingContractAssignmentDataSet.Columns.Add("CompanyName", typeof(string));
                emailBodyAwaitingContractAssignmentDataSet.Columns.Add("DaysOnTheList", typeof(int));

                DataTable emailBodyAssistanceRequiredDataSet = new DataTable();
                emailBodyAssistanceRequiredDataSet.Columns.Add("CompanyName", typeof(string));
                emailBodyAssistanceRequiredDataSet.Columns.Add("DaysOnTheList", typeof(int));
                emailBodyAssistanceRequiredDataSet.Columns.Add("SafetyAssessor", typeof(string));
                emailBodyAssistanceRequiredDataSet.Columns.Add("SafetyAssessorPhone", typeof(string));
                emailBodyAssistanceRequiredDataSet.Columns.Add("SupplierContact", typeof(string));
                emailBodyAssistanceRequiredDataSet.Columns.Add("SupplierContactEmail", typeof(string));
                emailBodyAssistanceRequiredDataSet.Columns.Add("SupplierContactPhone", typeof(string));
                emailBodyAssistanceRequiredDataSet.Columns.Add("ExpiryDate", typeof(string)); // 8/9/2015 DT257 - Ashley Goldstraw added
                emailBodyAssistanceRequiredDataSet.Columns.Add("ExpiryDateColor", typeof(string)); // 8/9/2015 DT257 - Ashley Goldstraw added

                DataTable emailBodySQExpiredDataSet = new DataTable();
                emailBodySQExpiredDataSet.Columns.Add("CompanyName", typeof(string));
                emailBodySQExpiredDataSet.Columns.Add("DaysOnTheList", typeof(int));
                emailBodySQExpiredDataSet.Columns.Add("DaysExpired", typeof(int));

                DataTable emailBodySQNearlyExpiredDataSet = new DataTable();
                emailBodySQNearlyExpiredDataSet.Columns.Add("CompanyName", typeof(string));
                emailBodySQNearlyExpiredDataSet.Columns.Add("DaysOnTheList", typeof(int));
                emailBodySQNearlyExpiredDataSet.Columns.Add("DaysUntilExpiry", typeof(int));

                int emailBodyAssistanceRequiredCount = 0;

                foreach (QuestionnaireReportProgress qrp in qrpList)
                {
                    if (qrp.ProcurementContact != null)
                    {
                        if (qrp.QuestionnairePresentlyWithActionId == (int)QuestionnairePresentlyWithActionList.ProcurementAssignCompanyStatus &&
                            qrp.ProcurementContact.ToString() == UserId.ToString())
                            emailBodyAwaitingContractAssignmentDataSet.Rows.Add(qrp.CompanyName, qrp.QuestionnairePresentlyWithSinceDaysCount);
                    }

                    Companies c = cList.Find(CompaniesColumn.CompanyId, qrp.CompanyId, false);
                    if (c.CompanyStatusId != (int)CompanyStatusList.InActive_SQ_Incomplete &&
                        c.CompanyStatusId != (int)CompanyStatusList.InActive_No_Requal_Required &&
                        c.Deactivated != true)
                    {
                        if (qrp.ProcurementContact != null)
                        {

                            if (qrp.QuestionnairePresentlyWithActionId == (int)QuestionnairePresentlyWithActionList.ProcurementQuestionnaire &&
                                qrp.ProcurementContact.ToString() == UserId.ToString())
                                emailBodyProcurementQuestionnaireDataSet.Rows.Add(qrp.CompanyName, qrp.QuestionnairePresentlyWithSinceDaysCount);

                            //if (qrp.ProcurementContact != null)
                            //{
                            if (qrp.QuestionnairePresentlyWithActionId == (int)QuestionnairePresentlyWithActionList.SupplierQuestionnaireSupplier || qrp.QuestionnairePresentlyWithActionId == (int)QuestionnairePresentlyWithActionList.SupplierQuestionnaireIncomplete)
                            {
                                if (qrp.ProcurementContact.ToString() == UserId.ToString())
                                {
                                    //Modified by Ashley Goldstraw 15/9/15
                                    string expDate;
                                    string expDateColor;
                                    if (qrp.MainAssessmentValidTo == null)
                                        expDate = "N/A";
                                    else
                                        expDate = qrp.MainAssessmentValidTo.Value.ToString("dd/MM/yyyy");
                                    if (qrp.MainAssessmentValidTo < DateTime.Now)
                                        expDateColor = "red";
                                    else
                                        expDateColor = "black";
                                    emailBodyAssistanceRequiredDataSet.Rows.Add(qrp.CompanyName, qrp.QuestionnairePresentlyWithSinceDaysCount, qrp.SafetyAssessor, qrp.SafetyAssessorPhone, qrp.SupplierContact, qrp.SupplierContactEmail, qrp.SupplierContactPhone, expDate, expDateColor);
                                    //Modified by Ashley Goldstraw to add in expiry date
                                    // emailBodyAssistanceRequiredDataSet.Rows.Add(qrp.CompanyName, qrp.QuestionnairePresentlyWithSinceDaysCount, qrp.SafetyAssessor, qrp.SafetyAssessorPhone, qrp.SupplierContact, qrp.SupplierContactEmail, qrp.SupplierContactPhone);
                                }
                            }
                        }
                    }
                }

                try
                {
                    if (qreList.Count > 0)
                    {
                        foreach (QuestionnaireReportExpiry qre in qreList)
                        {
                            if (qre.ExpiresIn <= 60 && qre.ProcurementContact != null)
                            {
                                if (qre.ProcurementContact == UserId.ToString())
                                {
                                    if (qre.ExpiresIn <= 0)
                                    {
                                        emailBodySQExpiredDataSet.Rows.Add(qre.CompanyName, 60 - qre.ExpiresIn, 60 - qre.ExpiresIn);
                                    }
                                    else
                                    {
                                        emailBodySQNearlyExpiredDataSet.Rows.Add(qre.CompanyName, 60 - qre.ExpiresIn, qre.ExpiresIn);
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Log4NetService.LogInfo("Exception: " + ex.Message);
                    Console.ReadLine();
                }

                if (emailBodyProcurementQuestionnaireDataSet.Rows.Count > 0)
                {

                    DataRow[] drList = emailBodyProcurementQuestionnaireDataSet.Select(null, "DaysOnTheList DESC");
                    emailBodyProcurementQuestionnaire = "<table border='1' bordercolor='#C0C0C0' style='border-collapse: collapse' cellspacing='1'><tr><td style='padding-right: 10px;'><strong>Company Name</strong></td><td width='150px' style='text-align: center'><strong>Days On the List</strong></td></tr>";

                    foreach (DataRow dr in drList)
                    {
                        emailBodyProcurementQuestionnaire += "<tr><td style='padding-right: 10px;'>" + dr["CompanyName"].ToString() + "</td><td width='150px' style='text-align: center'>" + dr["DaysOnTheList"].ToString() + "</td></tr>";
                    }

                    emailBodyProcurementQuestionnaire += "</table>";
                }

                if (emailBodyAwaitingContractAssignmentDataSet.Rows.Count > 0)
                {
                    DataRow[] drList = emailBodyAwaitingContractAssignmentDataSet.Select(null, "DaysOnTheList DESC");
                    emailBodyAwaitingContractAssignment = "<table border='1' bordercolor='#C0C0C0' style='border-collapse: collapse' cellspacing='1'><tr><td style='padding-right: 10px;'><strong>Company Name</strong></td><td width='150px' style='text-align: center'><strong>Days On the List</strong></td></tr>";

                    foreach (DataRow dr in drList)
                    {
                        emailBodyAwaitingContractAssignment += "<tr><td style='padding-right: 10px;'>" + dr["CompanyName"].ToString() + "</td><td width='150px' style='text-align: center'>" + dr["DaysOnTheList"].ToString() + "</td></tr>";
                    }

                    emailBodyAwaitingContractAssignment += "</table>";
                }

                if (emailBodyAssistanceRequiredDataSet.Rows.Count > 0)
                {
                    DataRow[] drList = emailBodyAssistanceRequiredDataSet.Select(null, "DaysOnTheList DESC");
                    emailBodyAssistanceRequired = "<table border='1' bordercolor='#C0C0C0' style='border-collapse: collapse' cellspacing='1'><tr><td style='padding-right: 10px;'><strong>Company Name</strong></td><td style='padding-right: 10px;'><strong>H&S Assessor</strong></td><td width='250px' style='padding-right: 10px;'><strong>H&S Assessor Phone #</strong></td><td><strong>Supplier Contact</strong></td><td style='padding-right: 10px;'><strong>Supplier Contact E-Mail</strong></td><td width='200px'><strong>Supplier Contact Phone</strong></td><td style='text-align: center; padding-right: 10px;'><strong>Days On the List</strong></td><td style='text-align: center; padding-right: 10px;'><strong>Expiry Date</strong></td></tr>";

                    foreach (DataRow dr in drList)
                    {
                        bool cont = true;

                        try
                        {
                            int i = Convert.ToInt32(dr["DaysOnTheList"]);
                            if (i <= 5) cont = false;
                        }
                        catch (Exception)
                        {
                            //lol =/
                        }

                        if (cont)
                        {
                            emailBodyAssistanceRequiredCount++;
                            emailBodyAssistanceRequired += "<tr><td style='padding-right: 10px;'>" + dr["CompanyName"].ToString() + "</td><td>" + dr["SafetyAssessor"].ToString() + "</td><td width='250px' style='padding-right: 10px;'>" + dr["SafetyAssessorPhone"].ToString() + "</td><td style='padding-right: 10px;'>" + dr["SupplierContact"].ToString() + "</td><td style='padding-right: 10px;'>" + dr["SupplierContactEmail"].ToString() + "</td><td width='200px' style='padding-right: 10px;'>" + dr["SupplierContactPhone"].ToString() + "</td>" + "<td style='text-align: center; padding-right: 10px;'>" + dr["DaysOnTheList"].ToString() + "</td>" + "<td style='text-align: center; padding-right: 10px; color:" + dr["ExpiryDateColor"] + ";'>" + dr["ExpiryDate"].ToString() + "</td></tr>";
                        }
                    }

                    emailBodyAssistanceRequired += "</table>";
                }
                if (emailBodyAssistanceRequiredCount == 0)
                {
                    emailBodyAssistanceRequired = "<font color='#FF0000'>(Thanks! On behalf of the Contractor Management Team, thank you for having no outstanding tasks in this area.)</font>";
                }

                if (emailBodySQExpiredDataSet.Rows.Count > 0)
                {
                    DataRow[] drList = emailBodySQExpiredDataSet.Select(null, "DaysOnTheList DESC");
                    emailBodySQExpired = "<table border='1' bordercolor='#C0C0C0' style='border-collapse: collapse' cellspacing='1'><tr><td style='padding-right: 10px;'><strong>Company Name</strong></td><td width='150px' style='text-align: center'><strong>Days On the List</strong></td><td width='150px' style='text-align: center'><strong>Days Expired</strong></td></tr>";

                    foreach (DataRow dr in drList)
                    {
                        emailBodySQExpired += "<tr><td style='padding-right: 10px;'>" + dr["CompanyName"].ToString() + "</td><td width='150px' style='text-align: center'>" + dr["DaysOnTheList"].ToString() + "</td><td width='150px' style='text-align: center'>" + dr["DaysExpired"].ToString() + "</td></tr>";
                    }

                    emailBodySQExpired += "</table>";
                }

                if (emailBodySQNearlyExpiredDataSet.Rows.Count > 0)
                {
                    DataRow[] drList = emailBodySQNearlyExpiredDataSet.Select(null, "DaysOnTheList DESC");
                    emailBodySQNearlyExpired = "<table border='1' bordercolor='#C0C0C0' style='border-collapse: collapse' cellspacing='1'><tr><td style='padding-right: 10px;'><strong>Company Name</strong></td><td width='150px' style='text-align: center'><strong>Days On the List</strong></td><td width='150px' style='text-align: center'><strong>Days Until Expiry</strong></td></tr>";

                    foreach (DataRow dr in drList)
                    {
                        emailBodySQNearlyExpired += "<tr><td style='padding-right: 10px;'>" + dr["CompanyName"].ToString() + "</td><td width='150px' style='text-align: center'>" + dr["DaysOnTheList"].ToString() + "</td><td width='150px' style='text-align: center'>" + dr["DaysUntilExpiry"].ToString() + "</td></tr>";
                    }

                    emailBodySQNearlyExpired += "</table>";
                }


                if ((emailBodyProcurementQuestionnaireDataSet.Rows.Count + emailBodyAwaitingContractAssignmentDataSet.Rows.Count +
                    emailBodyAssistanceRequiredCount + emailBodySQExpiredDataSet.Rows.Count + emailBodySQNearlyExpiredDataSet.Rows.Count) > 0) sendEmail = true;

                string summary_emailBodySQExpiredCount = "-";
                string summary_emailBodySQNearlyExpiredCount = "-";
                string summary_emailBodyProcurementQuestionnaireCount = "-";
                string summary_emailBodyAwaitingContractAssignmentCount = "-";
                string summary_emailBodyAssistanceRequiredCount = "-";

                if (emailBodySQExpiredDataSet.Rows.Count > 0)
                    summary_emailBodySQExpiredCount = emailBodySQExpiredDataSet.Rows.Count.ToString();
                if (emailBodySQNearlyExpiredDataSet.Rows.Count > 0)
                    summary_emailBodySQNearlyExpiredCount = emailBodySQNearlyExpiredDataSet.Rows.Count.ToString();
                if (emailBodyProcurementQuestionnaireDataSet.Rows.Count > 0)
                    summary_emailBodyProcurementQuestionnaireCount = emailBodyProcurementQuestionnaireDataSet.Rows.Count.ToString();
                if (emailBodyAwaitingContractAssignmentDataSet.Rows.Count > 0)
                    summary_emailBodyAwaitingContractAssignmentCount = emailBodyAwaitingContractAssignmentDataSet.Rows.Count.ToString();
                if (emailBodyAssistanceRequiredCount > 0) summary_emailBodyAssistanceRequiredCount = emailBodyAssistanceRequiredCount.ToString();

                if (sendEmail)
                {
                    emailBody = "<html><body>" +
                                    "<span style='font-size:12.0pt;font-family:'Verdana'>" +
                                    "Hi " + emailToName + "," + StringUtilities.Html.Add.LineBreak(1) +
                                    StringUtilities.Html.Add.LineBreak(1) +
                                    "You have received this e-mail as you either have tasks allocated to you for the Safety Qualification process to engage Supplier's on Alcoa sites and/or the HS Assessors are requesting your assistance." + StringUtilities.Html.Add.LineBreak(2) + "It is mandatory that all Supplier's <b><font style='text-decoration:underline'>must</font></b> successfully complete the Safety Qualification process prior to entering an Alcoa site." + StringUtilities.Html.Add.LineBreak(2) +
                                    "Access to the Contractor Services Management System via : <a href='http://csm.aua.alcoa.com'>http://csm.aua.alcoa.com</a> work list." + "</span>" + StringUtilities.Html.Add.LineBreak(3) +

                                    "<div align='center'><h2><b>Summary</b></h2></div>" +
                                    "<div align='center'><table border='1' bordercolor='#C0C0C0' style='border-collapse: collapse' cellspacing='1'>" +
                                    "<tr><td style='padding-right: 10px ; text-align: center'>" +
                                        "<strong>Work Task</strong>" +
                                    "</td><td width='150px' style='text-align: center; padding-right: 10px; padding-left: 10px'>" +
                                        "<strong># Companies</strong>" +
                                     "</strong></td></tr>" +
                                    "<tr><td style='padding-right: 10px ; text-align: right'>" +
                                        "Expired Safety Qualification (Requalification required):" +
                                    "</td><td width='150px' style='text-align: center; padding-right: 10px; padding-left: 10px;'>" +
                                        "<font color='Red'><strong>" + summary_emailBodySQExpiredCount + "</strong></font>" +
                                     "</td></tr>" +
                                     "<tr><td style='padding-right: 10px; text-align: right'>" +
                                        "Safety Qualification Expiring within 60 Days:" +
                                    "</td><td width='150px' style='text-align: center; padding-right: 10px; padding-left: 10px'>" +
                                        "<font color='Red'><strong>" + summary_emailBodySQNearlyExpiredCount + "</strong></font>" +
                                     "</td></tr>" +
                                     "<tr><td style='padding-right: 10px; text-align: right'>" +
                                        "Procurement Questionnaire:" +
                                    "</td><td width='150px' style='text-align: center; padding-right: 10px; padding-left: 10px'>" +
                                        "<font color='Red'><strong>" + summary_emailBodyProcurementQuestionnaireCount + "</strong></font>" +
                                     "</td></tr>" +
                                     "<tr><td style='padding-right: 10px; text-align: right'>" +
                                        "Supplier’s Awaiting Contract Assignment:" +
                                    "</td><td width='150px' style='text-align: center; padding-right: 10px; padding-left: 10px'>" +
                                        "<font color='Red'><strong>" + summary_emailBodyAwaitingContractAssignmentCount + "</strong></font>" +
                                     "</td></tr>" +
                                     "<tr><td style='padding-right: 10px; text-align: right'>" +
                                        "Assistance Required on Overdue Supplier Actions:" +
                                    "</td><td width='150px' style='text-align: center; padding-right: 10px; padding-left: 10px'>" +
                                        "<font color='Red'><strong>" + summary_emailBodyAssistanceRequiredCount + "</strong></font>" +
                                     "</td></tr>" +
                                     "</table></div>" + StringUtilities.Html.Add.LineBreak(1) +
                                     "<div align='center'><font color='Red'><b>Specific details shown below</b></font></div>" +
                                    "<div align='center'><h2><b>Current Safety Qualification work tasks and help request</b></h2></div>" +
                        //StringUtilities.Html.Add.LineBreak(1) +
                                    "<table border='0' width='100%' height='25' cellspacing='0' cellpadding='0'><tr><td height='25' bgcolor='#000080' style='padding-left:10px'><font color='#FFFFFF'><b><font size='4'>Expired Safety Qualification (Requalification required)</font></b></font></td></tr><tr><td height='25' style='padding-left:5px; padding-bottom: 10px;'>Supplier Safety Qualification has expired and the Procurement SPA needs to initiate a Safety Re-Qualification and ensure it is completed & approved “immediately”.</td></tr></table>" +
                        //StringUtilities.Html.Add.LineBreak(1) +
                                    emailBodySQExpired + StringUtilities.Html.Add.LineBreak(1) +
                                    StringUtilities.Html.Add.LineBreak(1) +
                        //"<b><font style='text-decoration:underline'>Companies that are due to expire</font></b>" + StringUtilities.Html.Add.LineBreak(1) +
                        //"Please determine if you wish to continue with this supplier and thus request them to complete the requalification process or select “No requalification required”. If you select “No requalification require” the supplier can be reactivated at a later stage, however, please be aware that the supplier  <b><font style='text-decoration:underline'>will not</font></b> be allowed on any Alcoa site.." + StringUtilities.Html.Add.LineBreak(1) +
                                    "<table border='0' width='100%' height='25' cellspacing='0' cellpadding='0'><tr><td height='25' bgcolor='#000080' style='padding-left:10px'><font color='#FFFFFF'><b><font size='4'>Safety Qualification Expiring within 60 Days</font></b></font></td></tr><tr><td height='25' style='padding-left:5px; padding-bottom: 10px;'>Please determine if you wish to continue with this Supplier:" + StringUtilities.Html.Add.LineBreak(2) + "&nbsp;&nbsp;&nbsp;&nbsp;1.	Request Supplier to complete the Safety Re-Qualification process; <strong>OR</strong><br />&nbsp;&nbsp;&nbsp;&nbsp;2. or select “No Requalification Required”." + StringUtilities.Html.Add.LineBreak(2) + "If you select “No Requalification Required” the supplier can be reactivated at a later stage, however, please be aware that the Supplier <b><font style='text-decoration:underline'>will not</font></b> be allowed on any Alcoa site until they have successfully passed the Safety Qualification Assessment</td></tr></table>" +
                        //StringUtilities.Html.Add.LineBreak(1) +
                                    emailBodySQNearlyExpired + StringUtilities.Html.Add.LineBreak(1) +
                                    StringUtilities.Html.Add.LineBreak(1) +
                                    "<table border='0' width='100%' height='25' cellspacing='0' cellpadding='0'><tr><td height='25' bgcolor='#000080' style='padding-left:10px'><font color='#FFFFFF'><b><font size='4'>Procurement Questionnaire</font></b></font></td></tr><tr><td height='25' style='padding-left:5px; padding-bottom: 10px;'>The Procurement SPA is required to complete the Procurement Questionnaire and submit it to enable the Safety Qualification process to continue.</td></tr></table>" +
                        //"<b><font style='text-decoration:underline'>Procurement questionnaire</font></b>" + StringUtilities.Html.Add.LineBreak(1) +
                        //"You are required to complete the procurement questionnaire and submit it for assessment." + StringUtilities.Html.Add.LineBreak(1) +
                        //StringUtilities.Html.Add.LineBreak(1) + 
                                    emailBodyProcurementQuestionnaire + StringUtilities.Html.Add.LineBreak(1) +
                                    StringUtilities.Html.Add.LineBreak(1) +

                                    "<table border='0' width='100%' height='25' cellspacing='0' cellpadding='0'><tr><td height='25' bgcolor='#000080' style='padding-left:10px'><font color='#FFFFFF'><b><font size='4'>Supplier’s Awaiting Contract Assignment</font></b></font></td></tr><tr><td height='25' style='padding-left:5px; padding-bottom: 10px;'>The Procurement SPA is  required to assign the correct Supplier contract status in the Safety Qualification system to complete the Safety Qualification process. Until this task is completed the Supplier is not classified as Safety Qualified.</td></tr></table>" +
                        //"<b><font style='text-decoration:underline'>Awaiting contract assignment</font></b>" + StringUtilities.Html.Add.LineBreak(1) +
                        //"You are required to assign a supplier status to the safety qualification system." + StringUtilities.Html.Add.LineBreak(1) +
                        //StringUtilities.Html.Add.LineBreak(1) +
                                    emailBodyAwaitingContractAssignment + StringUtilities.Html.Add.LineBreak(1) +
                                    StringUtilities.Html.Add.LineBreak(1) +
                        //"<b><font style='text-decoration:underline'>Assistance Required</font></b>" + StringUtilities.Html.Add.LineBreak(1) +
                        //"Your assistance is requested as the procurement representative to communicate with the relevant suppliers that have not completed and/or resubmitted their safety Qualification Questionnaire in a reasonable time as listed below. We are asking that you contact the HS Assessor listed below to determine if the performance of the supplier can be improved by letting them know that if they do not successfully complete the Safety Qualification process they <b><font style='text-decoration:underline'>will not</font></b> be granted access to site. In fact there is no point in having a contract with them." + StringUtilities.Html.Add.LineBreak(1) +
                                     "<table border='0' width='100%' height='25' cellspacing='0' cellpadding='0'><tr><td height='25' bgcolor='#000080' style='padding-left:10px'><font color='#FFFFFF'><b><font size='4'>Assistance Required on Overdue Supplier Actions</font></b></font></td></tr><tr><td height='25' style='padding-left:5px; padding-bottom: 10px;'>Assistance is required from the Procurement SPA to inform the relevant Supplier(s) below that they have not completed and/or resubmitted their Safety Qualification Questionnaire for assessment in a reasonable time.<br />The Supplier is to be reminded that they cannot enter an Alcoa site until they have successfully passed the Safety Qualification Assessment.</td></tr></table>" +
                        //StringUtilities.Html.Add.LineBreak(1) +
                                    emailBodyAssistanceRequired + StringUtilities.Html.Add.LineBreak(1) +
                                    StringUtilities.Html.Add.LineBreak(1) +
                        //"<b><font style='text-decoration:underline'>Companies that Safety Qualification has expired and need to be requalified</font></b>" + StringUtilities.Html.Add.LineBreak(1) +
                        //"You need to have this supplier requalified “immediately” as the contractor will be denied access to any Alcoa site." + StringUtilities.Html.Add.LineBreak(1) +
                                    StringUtilities.Html.Add.LineBreak(3) +
                                    "If you have any concerns or questions, please e-mail them to the <a href='mailto:AUAAlcoaContractorServices@alcoa.com.au'>AUA Alcoa Contractor Services Team</a>." + StringUtilities.Html.Add.LineBreak(1) +
                                    "Thank You." +
                                    StringUtilities.Html.Add.LineBreak(2) +
                                    "<div align='center'><font color='#C0C0C0'>Contractor Services Management Team Daily Procurement Worklist E-Mail Update: " + DateTime.Now.ToString() + "</font></div>" +
                                    "</body></html>";

                }

                return emailBody;
            }

            public static void HSAssessorWorkReminderEmail()
            {
                QuestionnaireReportProgressService qrpService = new QuestionnaireReportProgressService();
                VList<QuestionnaireReportProgress> qrpList = qrpService.GetAll();

                UsersService uService = new UsersService();
                TList<Users> uList = uService.GetAll();

                Configuration configuration = new Configuration();
                string defaultehs_vic = configuration.GetValue(ConfigList.DefaultEhsConsultant_VICOPS);
                string defaultehs_wao = configuration.GetValue(ConfigList.DefaultEhsConsultant_WAO);
                int ehsVics = 0;
                int ehsWao = 0;
                UsersEhsConsultantsService ueService = new UsersEhsConsultantsService();
                UsersEhsConsultantsFilters ueFilters = new UsersEhsConsultantsFilters();
                ueFilters.Append(UsersEhsConsultantsColumn.Email, defaultehs_vic);
                VList<UsersEhsConsultants> ueVicList = ueService.GetPaged(ueFilters.ToString(), "", 0, 999, out ehsVics);
                UsersEhsConsultants ueVic = ueVicList[0];
                ueFilters.Clear();
                ueFilters.Append(UsersEhsConsultantsColumn.Email, defaultehs_wao);
                VList<UsersEhsConsultants> ueWaoList = ueService.GetPaged(ueFilters.ToString(), "", 0, 999, out ehsWao);
                UsersEhsConsultants ueWao = ueWaoList[0];

                CompanySiteCategoryStandardService cscsService = new CompanySiteCategoryStandardService();
                TList<CompanySiteCategoryStandard> cscsList = cscsService.GetAll();

                SitesService sService = new SitesService();
                TList<Sites> sList = sService.GetAll();

                RegionsSitesService rsService = new RegionsSitesService();
                TList<RegionsSites> rsList = rsService.GetAll();

                EhsConsultantService ehscService = new EhsConsultantService();
                TList<EhsConsultant> ehscList = ehscService.GetAll();

                foreach (EhsConsultant ehsc in ehscList)
                {
                    Users u = uService.GetByUserId(ehsc.UserId);

                    string emailToName = String.Format("{0} {1}", u.FirstName, u.LastName);
                    string[] to = { u.Email };
                    string[] cc = { Helper.Configuration.GetValue(ConfigList.ContactEmail).ToString() };
                    string emailSubject = "Safety Qualification H&S Assessor Work and Assistance Request";
                    string emailBody = BuildEmailBodyHSAssessor(ehsc.EhsConsultantId, emailToName, qrpList, uList, ueVic, ueWao, cscsList, sList, rsList);

                    if (!String.IsNullOrEmpty(emailBody))
                    {
                        //Cindi Thornton 4/9/2015 - removed call to sendEmail, modified call to logEmail for new columns IsBodyHtml & attachmentPath. EmailLog used to queue & log emails.
                        //Helper.EmailAgent.sendEmail(to, null, null, emailSubject, emailBody, true, null); //Send
                        //Helper.EmailAgent.logEmail(to, null, null, emailSubject, emailBody, EmailLogTypeList.HSAssessorDailyWorkReminderEmail, true, null); //Log

                        var emailLogService = DependencyResolver.Current.GetService<repo.IEmailLogService>();
                        emailLogService.Insert(to, null, null, emailSubject, emailBody, "HSAssessorDailyWorkReminderEmail", true, null, null);
                    }
                }
            }
            private static string BuildEmailBodyHSAssessor(int EhsConsultantId, string FullName, VList<QuestionnaireReportProgress> qrpList, TList<Users> uList,
                                                        UsersEhsConsultants ueVic, UsersEhsConsultants ueWao,
                                                       TList<CompanySiteCategoryStandard> cscsList, TList<Sites> sList, TList<RegionsSites> rsList)
            {
                bool sendEmail = false;

                string emailToName = FullName;
                string emailBodyAwaitingAssessment = "<font color='#FF0000'>(Thanks! On behalf of the Contractor Management Team, thank you for having no outstanding tasks in this area.)</font>";
                string emailBodyCurrentlyWithSupplier = "<font color='#FF0000'>(Thanks! On behalf of the Contractor Management Team, thank you for having no outstanding tasks in this area.)</font>";
                string emailBody = "";

                DataTable emailBodyAwaitingAssessmentDataSet = new DataTable();
                emailBodyAwaitingAssessmentDataSet.Columns.Add("CompanyName", typeof(string));
                emailBodyAwaitingAssessmentDataSet.Columns.Add("DaysOnTheList", typeof(int));
                emailBodyAwaitingAssessmentDataSet.Columns.Add("SupplierContact", typeof(string));
                emailBodyAwaitingAssessmentDataSet.Columns.Add("SupplierContactEmail", typeof(string));
                emailBodyAwaitingAssessmentDataSet.Columns.Add("SupplierContactPhone", typeof(string));
                emailBodyAwaitingAssessmentDataSet.Columns.Add("ProcurementContact", typeof(string));
                emailBodyAwaitingAssessmentDataSet.Columns.Add("ExpiryDate", typeof(string));// 20/10/2015 DT257 - Ashley Goldstraw added
                emailBodyAwaitingAssessmentDataSet.Columns.Add("ExpiryDateColor", typeof(string)); // 20/10/2015 DT257 - Ashley Goldstraw added

                DataTable emailBodyCurrentlyWithSupplierDataSet = new DataTable();
                emailBodyCurrentlyWithSupplierDataSet.Columns.Add("CompanyName", typeof(string));
                emailBodyCurrentlyWithSupplierDataSet.Columns.Add("DaysOnTheList", typeof(int));
                emailBodyCurrentlyWithSupplierDataSet.Columns.Add("SupplierContact", typeof(string));
                emailBodyCurrentlyWithSupplierDataSet.Columns.Add("SupplierContactEmail", typeof(string));
                emailBodyCurrentlyWithSupplierDataSet.Columns.Add("SupplierContactPhone", typeof(string));
                emailBodyCurrentlyWithSupplierDataSet.Columns.Add("ProcurementContact", typeof(string));
                emailBodyCurrentlyWithSupplierDataSet.Columns.Add("ExpiryDate", typeof(string));// 20/10/2015 DT257 - Ashley Goldstraw added
                emailBodyCurrentlyWithSupplierDataSet.Columns.Add("ExpiryDateColor", typeof(string)); // 20/10/2015 DT257 - Ashley Goldstraw added

                foreach (QuestionnaireReportProgress qrp in qrpList)
                {
                    string ProcurementContact = "n/a";

                    int ProcurementContactUserId = -1;
                    Int32.TryParse(qrp.ProcurementContact, out ProcurementContactUserId);

                    if (ProcurementContactUserId != -1)
                    {
                        Users uProcurement = uList.Find(UsersColumn.UserId, ProcurementContactUserId, false);
                        if (uProcurement != null)
                            ProcurementContact = String.Format("{0} {1}", uProcurement.FirstName, uProcurement.LastName);
                    }


                    if (qrp.EhsConsultantId != null)
                    {
                        int _ehsConsultantId = Convert.ToInt32(qrp.EhsConsultantId);
                        if ((qrp.QuestionnairePresentlyWithActionId == (int)QuestionnairePresentlyWithActionList.HSAssessorQuestionnaire) && _ehsConsultantId == EhsConsultantId)
                        {
                            string expDate;
                            string expDateColor;
                            if (qrp.MainAssessmentValidTo == null)
                                expDate = "N/A";
                            else
                                expDate = qrp.MainAssessmentValidTo.Value.ToString("dd/MM/yyyy");
                            if (qrp.MainAssessmentValidTo < DateTime.Now)
                                expDateColor = "red";
                            else
                                expDateColor = "black";
                            emailBodyAwaitingAssessmentDataSet.Rows.Add(qrp.CompanyName, qrp.QuestionnairePresentlyWithSinceDaysCount, qrp.SupplierContact, qrp.SupplierContactEmail, qrp.SupplierContactPhone, ProcurementContact, expDate, expDateColor);
                        }
                        if (qrp.QuestionnairePresentlyWithActionId == (int)QuestionnairePresentlyWithActionList.HSAssessorQuestionnaireProcurement)
                        {
                            bool sendit = false;

                            bool Vic = false;
                            bool Wao = false;

                            if (ueVic.EhsConsultantId == EhsConsultantId)
                                Vic = true;

                            if (ueWao.EhsConsultantId == EhsConsultantId)
                                Wao = true;


                            if (Wao || Vic)
                            {
                                TList<CompanySiteCategoryStandard> cscsTlist = cscsList.FindAll(CompanySiteCategoryStandardColumn.CompanyId, qrp.CompanyId, false);

                                SitesService sService = new SitesService();

                                if (cscsTlist.Count > 0)
                                {
                                    bool vic = false;
                                    bool wao = false;
                                    foreach (CompanySiteCategoryStandard cscs in cscsTlist)
                                    {
                                        TList<RegionsSites> rList = rsList.FindAll(RegionsSitesColumn.SiteId, cscs.SiteId, false);

                                        foreach (RegionsSites _r in rList)
                                        {
                                            if (_r.RegionId == 1) wao = true;
                                            if (_r.RegionId == 4) vic = true;
                                        }
                                    }

                                    if (wao && Wao)
                                    {
                                        sendit = true;
                                    }
                                    else
                                    {
                                        if (vic && Vic)
                                        {
                                            sendit = true;
                                        }
                                    }
                                }

                                if (sendit)
                                {
                                    emailBodyAwaitingAssessmentDataSet.Rows.Add(qrp.CompanyName, qrp.QuestionnairePresentlyWithSinceDaysCount, qrp.SupplierContact, qrp.SupplierContactEmail, qrp.SupplierContactPhone, ProcurementContact);
                                }
                            }
                        }

                        if ((qrp.QuestionnairePresentlyWithActionId == (int)QuestionnairePresentlyWithActionList.SupplierQuestionnaireSupplier ||
                            qrp.QuestionnairePresentlyWithActionId == (int)QuestionnairePresentlyWithActionList.SupplierQuestionnaireIncomplete) &&
                        _ehsConsultantId == EhsConsultantId)
                        {
                            string expDate;
                            string expDateColor;
                            if (qrp.MainAssessmentValidTo == null)
                                expDate = "N/A";
                            else
                                expDate = qrp.MainAssessmentValidTo.Value.ToString("dd/MM/yyyy");
                            if (qrp.MainAssessmentValidTo < DateTime.Now)
                                expDateColor = "red";
                            else
                                expDateColor = "black";
                            emailBodyCurrentlyWithSupplierDataSet.Rows.Add(qrp.CompanyName, qrp.QuestionnairePresentlyWithSinceDaysCount, qrp.SupplierContact, qrp.SupplierContactEmail, qrp.SupplierContactPhone, ProcurementContact, expDate, expDateColor);
                        }
                    }
                }

                int emailBodyCurrentlyWithSupplierCount = 0;

                if (emailBodyAwaitingAssessmentDataSet.Rows.Count > 0)
                {
                    DataRow[] drList = emailBodyAwaitingAssessmentDataSet.Select(null, "DaysOnTheList DESC");
                    emailBodyAwaitingAssessment = "<table border='1' bordercolor='#C0C0C0' style='border-collapse: collapse' cellspacing='1'><tr><td style='padding-right: 10px;'><strong>Company Name</strong></td><td style='padding-right: 10px;'><strong>Procurement Contact</strong></td><td><strong>Supplier Contact</strong></td><td style='padding-right: 10px;'><strong>Supplier Contact E-Mail</strong></td><td width='200px'><strong>Supplier Contact Phone</strong></td><td style='text-align: center; padding-right: 10px;'><strong>Days On the List</strong></td><td><strong>Expiry Date</strong></td></tr>";

                    foreach (DataRow dr in drList)
                    {
                        emailBodyAwaitingAssessment += "<tr><td style='padding-right: 10px;'>" + dr["CompanyName"].ToString() + "</td><td>" + dr["ProcurementContact"].ToString() + "</td><td width='250px' style='padding-right: 10px;'>" + dr["SupplierContact"].ToString() + "</td><td style='padding-right: 10px;'>" + dr["SupplierContactEmail"].ToString() + "</td><td width='200px' style='padding-right: 10px;'>" + dr["SupplierContactPhone"].ToString() + "</td>" + "<td style='text-align: center; padding-right: 10px;'>" + dr["DaysOnTheList"].ToString() + "</td>" + "<td style='text-align: center; padding-right: 10px; color:" + dr["ExpiryDateColor"] + ";'>" + dr["ExpiryDate"].ToString() + "</td></tr>";
                    }

                    emailBodyAwaitingAssessment += "</table>";
                }

                if (emailBodyCurrentlyWithSupplierDataSet.Rows.Count > 0)
                {
                    DataRow[] drList = emailBodyCurrentlyWithSupplierDataSet.Select(null, "DaysOnTheList DESC");
                    emailBodyCurrentlyWithSupplier = "<table border='1' bordercolor='#C0C0C0' style='border-collapse: collapse' cellspacing='1'><tr><td style='padding-right: 10px;'><strong>Company Name</strong></td><td style='padding-right: 10px;'><strong>Procurement Contact</strong></td><td><strong>Supplier Contact</strong></td><td style='padding-right: 10px;'><strong>Supplier Contact E-Mail</strong></td><td width='200px'><strong>Supplier Contact Phone</strong></td><td style='text-align: center; padding-right: 10px;'><strong>Days On the List</strong></td><td><strong>Expiry Date</strong></td></tr>";

                    foreach (DataRow dr in drList)
                    {
                        bool cont = true;

                        try
                        {
                            int i = Convert.ToInt32(dr["DaysOnTheList"]);
                            if (i <= 5) cont = false;
                        }
                        catch (Exception)
                        {
                            //lol =/
                        }

                        if (cont)
                        {
                            emailBodyCurrentlyWithSupplierCount++;
                            emailBodyCurrentlyWithSupplier += "<tr><td style='padding-right: 10px;'>" + dr["CompanyName"].ToString() + "</td><td>" + dr["ProcurementContact"].ToString() + "</td><td width='250px' style='padding-right: 10px;'>" + dr["SupplierContact"].ToString() + "</td><td style='padding-right: 10px;'>" + dr["SupplierContactEmail"].ToString() + "</td><td width='200px' style='padding-right: 10px;'>" + dr["SupplierContactPhone"].ToString() + "</td>" + "<td style='text-align: center; padding-right: 10px;'>" + dr["DaysOnTheList"].ToString() + "</td>" + "<td style='text-align: center; padding-right: 10px; color:" + dr["ExpiryDateColor"] + ";'>" + dr["ExpiryDate"].ToString() + "</td></tr>";
                        }
                    }

                    emailBodyCurrentlyWithSupplier += "</table>";
                }
                if (emailBodyCurrentlyWithSupplierCount == 0)
                {
                    emailBodyCurrentlyWithSupplier = "<font color='#FF0000'>(Thanks! On behalf of the Contractor Management Team, thank you for having no outstanding tasks in this area.)</font>";
                }

                if (emailBodyAwaitingAssessmentDataSet.Rows.Count + emailBodyCurrentlyWithSupplierCount > 0) sendEmail = true;

                if (sendEmail)
                {
                    emailBody = "<html><body>" +
                                    "<span style='font-size:12.0pt;font-family:'Verdana'>" +
                                    "Hi " + emailToName + "," + StringUtilities.Html.Add.LineBreak(1) +
                                    StringUtilities.Html.Add.LineBreak(1) +
                                    "You have received this email as you have been assigned as the H&S Assessor for the Safety Qualification process to engage contactors on Alcoa Sites" + StringUtilities.Html.Add.LineBreak(1) +
                                    StringUtilities.Html.Add.LineBreak(1) +
                                    "Access to the Contractor Services Management System via : <a href='http://csm.aua.alcoa.com'>http://csm.aua.alcoa.com</a> work list." + "</span>" + StringUtilities.Html.Add.LineBreak(1) +
                                    StringUtilities.Html.Add.LineBreak(3) +
                                    "<div align='center'><h2><b>Current Safety Qualification work tasks and help request</b></h2></div>" +
                                    "<table border='0' width='100%' height='25' cellspacing='0' cellpadding='0'><tr><td height='25' bgcolor='#000080' style='padding-left:10px'><font color='#FFFFFF'><b><font size='4'>Awaiting Assessment</font></b></font></td></tr><tr><td height='25' style='padding-left:5px; padding-bottom: 10px;'>The following questionnaires have been completed and submitted by the Supplier. As the HS Assessor you now requested to complete the HS Assessment process for each of the listed companies.</td></tr></table>" +
                                    emailBodyAwaitingAssessment + StringUtilities.Html.Add.LineBreak(1) +
                                    StringUtilities.Html.Add.LineBreak(1) +

                                    "<table border='0' width='100%' height='25' cellspacing='0' cellpadding='0'><tr><td height='25' bgcolor='#000080' style='padding-left:10px'><font color='#FFFFFF'><b><font size='4'>Currently With Supplier</font></b></font></td></tr><tr><td height='25' style='padding-left:5px; padding-bottom: 10px;'>The following companies have had their questionnaires for over 5 days. A separate e-mail has been sent to the relevant Procurement Representative to assist you in informing the supplier that it is a Procurement protocol and an Operational requirement that the supplier must successfully complete the Safety Qualification process prior to being granted access to site.</td></tr></table>" +
                                    emailBodyCurrentlyWithSupplier + StringUtilities.Html.Add.LineBreak(1) +
                                    StringUtilities.Html.Add.LineBreak(1) +
                                    StringUtilities.Html.Add.LineBreak(3) +
                                    "If you have any concerns or questions, please e-mail them to the <a href='mailto:AUAAlcoaContractorServices@alcoa.com.au'>AUA Alcoa Contractor Services Team</a>." + StringUtilities.Html.Add.LineBreak(1) +
                                    "Thank You." +
                                    StringUtilities.Html.Add.LineBreak(2) +
                                    "<div align='center'><font color='#C0C0C0'>Contractor Services Management Team Daily H&S Assessor Worklist E-Mail Update: " + DateTime.Now.ToString() + "</font></div>" +
                                    "</body></html>";
                }

                return emailBody;
            }
            public static void SqExpiryReminderEmails()
            {

                QuestionnaireReportExpiryService qreService = new QuestionnaireReportExpiryService();
                VList<QuestionnaireReportExpiry> qreVList = qreService.GetAll();

                //this isn't used anymore, so forget about this half coded optimised code..
                //foreach (QuestionnaireReportExpiry qre in qreVList)
                //{
                //    if (qre.ExpiresIn != null)
                //    {
                //        int ExpiresIn = Convert.ToInt32(qre.ExpiresIn);

                //        if (qre.QuestionnairePresentlyWithUserDesc != "-")
                //        {
                //            if (ExpiresIn == 60 || ExpiresIn == 40 || ExpiresIn == 20 ||
                //                ExpiresIn == 10 || ExpiresIn == 7 || ExpiresIn <= 0)
                //            {

                //            }
                //        }
                //    }
                //}

                DataSet qreDs = qreVList.ToDataSet(false);

                foreach (DataRow dr in qreDs.Tables[0].Rows)
                {
                    bool cont = false;
                    int ExpiresIn = 0;
                    try
                    {
                        ExpiresIn = Convert.ToInt32(dr["ExpiresIn"].ToString());
                        cont = true;
                    }
                    catch (Exception)
                    {
                        cont = false;
                    }

                    if (cont)
                    {
                        if (ExpiresIn == 60 || ExpiresIn == 40 || ExpiresIn == 20 ||
                            ExpiresIn == 10 || ExpiresIn == 7 || ExpiresIn <= 0)
                        //if(ExpiresIn == -12)
                        {
                            int QuestionnaireId = Convert.ToInt32(dr["QuestionnaireId"].ToString());

                            int count = -1;
                            QuestionnaireReportOverviewFilters qroFilters = new QuestionnaireReportOverviewFilters();
                            qroFilters.Append(QuestionnaireReportOverviewColumn.QuestionnaireId, QuestionnaireId.ToString());
                            VList<QuestionnaireReportOverview> qroVList = DataRepository.QuestionnaireReportOverviewProvider.GetPaged(qroFilters.ToString(), null, 0, 100, out count);

                            if (count <= 0)
                            {
                                Log4NetService.LogInfo(String.Format("questionnaireid ({0}) not found", QuestionnaireId));
                            }
                            else
                            {
                                if (count > 1)
                                {
                                    Log4NetService.LogInfo(String.Format("questionnaireid ({0}) duplicate found. db fail.", QuestionnaireId));
                                }
                                else
                                {
                                    UsersService usersService = new UsersService();
                                    Users userEntity;
                                    userEntity = usersService.GetByUserId(Convert.ToInt32(dr["CreatedByUserId"].ToString()));
                                    string ProcurementContact = "n/a";
                                    string ProcurementContactEmail = "n/a";
                                    if (qroVList[0].ProcurementContactUserId != null)
                                    {
                                        Users userEntityProcurementContact = usersService.GetByUserId(Convert.ToInt32(qroVList[0].ProcurementContactUserId));
                                        ProcurementContact = String.Format("{0}, {1}", userEntityProcurementContact.LastName, userEntityProcurementContact.FirstName);
                                        ProcurementContactEmail = userEntityProcurementContact.Email;
                                    }

                                    string DayExpires = dr["MainAssessmentValidTo"].ToString();
                                    string ModifiedDate = dr["ModifiedDate"].ToString();

                                    CompaniesService companiesService = new CompaniesService();
                                    Companies companiesEntity = companiesService.GetByCompanyId(qroVList[0].CompanyId);
                                    Companies companiesEntity2 = companiesService.GetByCompanyId(userEntity.CompanyId);

                                    string SitesTicked = "";
                                    bool VicOps = false;
                                    QuestionnaireInitialLocationService qilService = new QuestionnaireInitialLocationService();
                                    TList<QuestionnaireInitialLocation> qilTList = qilService.GetByQuestionnaireId(qroVList[0].QuestionnaireId);
                                    foreach (QuestionnaireInitialLocation qil in qilTList)
                                    {
                                        SitesService sService = new SitesService();
                                        Sites s = sService.GetBySiteId(qil.SiteId);
                                        if (String.IsNullOrEmpty(SitesTicked))
                                        {
                                            SitesTicked = s.SiteName;
                                        }
                                        else
                                        {
                                            SitesTicked += ", " + s.SiteName;
                                        }

                                        RegionsSitesService rsService = new RegionsSitesService();
                                        TList<RegionsSites> rsTList = rsService.GetBySiteId(qil.SiteId);
                                        RegionsService rService = new RegionsService();
                                        Regions r = rService.GetByRegionInternalName("VICOPS");
                                        foreach (RegionsSites rs in rsTList)
                                        {
                                            if (rs.RegionId == r.RegionId) VicOps = true;
                                        }
                                    }
                                    if (String.IsNullOrEmpty(SitesTicked)) SitesTicked = "n/a";


                                    string subject = String.Format("Safety Qualification Expires in {0} Days - {1}", ExpiresIn, companiesEntity.CompanyName);

                                    string body = String.Format("A Questionnaire has been found to expire in {0} Days\n\nCompany Name: {1}\nExpires On: {2}\nSites: {3}\nProcurement Contact: {4}\n\nQuestionnaire Created by: {5}, {6} ({7}) at {8}\n\n\nYOU ARE BEING CONTACTED AS YOU ARE THE LISTED PROCUREMENT CONTACT.", ExpiresIn, companiesEntity.CompanyName, DayExpires, SitesTicked, ProcurementContact, userEntity.LastName, userEntity.FirstName, companiesEntity2.CompanyName, ModifiedDate);

                                    if (ExpiresIn <= 0)
                                    {
                                        subject = "Safety Qualification Expired - " + companiesEntity.CompanyName;
                                        body = body.Replace("Expires", "Expired");
                                        if (ExpiresIn == 0)
                                        {
                                            body = body.Replace(String.Format("A Questionnaire has been found to expire in {0} Days", ExpiresIn),
                                                                "A Questionnaire has been found to have expired.");
                                        }
                                        if (ExpiresIn < 0)
                                        {
                                            body = body.Replace(String.Format("A Questionnaire has been found to expire in {0} Days", ExpiresIn),
                                                                String.Format("A Questionnaire has been found to have expired {0} Days Ago", ExpiresIn.ToString().Replace("-", "")));
                                            body = body.Replace("Expired", String.Format("Expired {0} Days Ago", ExpiresIn.ToString().Replace("-", "")));
                                        }
                                    }

                                    string[] to = { ProcurementContactEmail };
                                    string[] cc = { Helper.Configuration.GetValue(ConfigList.ContactEmail).ToString() };

                                    var emailLogService = DependencyResolver.Current.GetService<repo.IEmailLogService>(); //Cindi Thornton 8/9/2015 - used to insert into the emailLog table

                                    //check if vicops site ticked..
                                    if (companiesEntity.Deactivated != true)
                                    {
                                        if (ProcurementContactEmail == "n/a")
                                        {
                                            if (VicOps)
                                            {
                                                string[] cc2 = { Helper.Configuration.GetValue(ConfigList.ContactEmail).ToString(), Helper.Configuration.GetValue(ConfigList.DefaultEhsConsultant_VICOPS).ToString() };

                                                //Cindi Thornton 8/9/2015 - changed to write to EmailLog. Batch job sends these.
                                                //Helper.EmailAgent.sendEmail(cc2, null, null, subject, body, false, null);
                                                emailLogService.Insert(cc2, null, null, subject, body, "General", false, null, null);
                                            }
                                            else
                                            {
                                                //Cindi Thornton 8/9/2015 - changed to write to EmailLog. Batch job sends these.
                                                //Helper.EmailAgent.sendEmail(cc, null, null, subject, body, false, null);
                                                emailLogService.Insert(cc, null, null, subject, body, "General", false, null, null);
                                            }
                                        }
                                        else
                                        {
                                            if (VicOps)
                                            {
                                                string[] cc2 = { Helper.Configuration.GetValue(ConfigList.ContactEmail).ToString(), Helper.Configuration.GetValue(ConfigList.DefaultEhsConsultant_VICOPS).ToString() };

                                                //Cindi Thornton 8/9/2015 - changed to write to EmailLog. Batch job sends these.
                                                //Helper.EmailAgent.sendEmail(to, cc2, null, subject, body, false, null);
                                                emailLogService.Insert(to, cc2, null, subject, body, "General", false, null, null);
                                            }
                                            else
                                            {
                                                //Cindi Thornton 8/9/2015 - changed to write to EmailLog. Batch job sends these.
                                                //Helper.EmailAgent.sendEmail(to, cc, null, subject, body, false, null);
                                                emailLogService.Insert(to, cc, null, subject, body, "General", false, null, null);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

            }


            public static string GetEmail(int userId)
            {
                string email = "";

                UsersService uService = new UsersService();
                Users u = uService.GetByUserId(userId);
                email = u.Email;

                return email;
            }
            public static string GetFirstLastName(int userId)
            {
                string firstLastName = "";
                if (userId == -1)
                {
                    firstLastName = "CSMS Team";
                }
                else
                {
                    UsersService uService = new UsersService();
                    Users u = uService.GetByUserId(userId);
                    firstLastName = u.FirstName + " " + u.LastName;
                }

                return firstLastName;
            }
            public static int GetContactUserId(int? UserId)
            {
                if (UserId == null)
                    return 0;
                else
                    return Convert.ToInt32(UserId);
            }

            //Added by P. Dikshit for <DT 2579, 21-Aug-2012>
            public static bool IsMatchingEmailDaysFrequency(QuestionnaireEscalationProcurementList qepl, int EscalationChainDaysWith, int EscalationChainDaysWithEmailDaysFrequency)
            {
                int Dividend = 0;
                int Remainder = 0;
                int remMailSent = 0;
                bool flag = false;
                if (qepl.DaysWith >= EscalationChainDaysWith)
                {
                    if (qepl.DaysWith != null)
                    {
                        if (EscalationChainDaysWith >= EscalationChainDaysWithEmailDaysFrequency)
                        {
                            Dividend = (Int32)qepl.DaysWith;
                            remMailSent = (Dividend % EscalationChainDaysWith);
                            Remainder = (remMailSent % EscalationChainDaysWithEmailDaysFrequency);
                            if (Remainder == 0) flag = true;
                        }
                    }
                }
                return flag;
            }

            //Added by P. Dikshit for <DT 2579, 21-Aug-2012>
            public static bool IsMatchingEmailDaysFrequency(QuestionnaireEscalationHsAssessorList qepl, int EscalationChainDaysWithEmailDaysFrequency)
            {
                int Dividend = 0;
                int Remainder = 0;
                bool flag = false;
                if (qepl.DaysWith != null)
                {
                    Dividend = (Int32)qepl.DaysWith;
                    Remainder = (Dividend % EscalationChainDaysWithEmailDaysFrequency);
                    if (Remainder == 0) flag = true;
                }

                return flag;
            }

            public static int? GetContactUserId(QuestionnaireEscalationProcurementList qepl, int EscalationChainDaysWith)
            {
                int? ContactUserId = null;
                if (qepl.DaysWith != null)
                {
                    if (qepl.DaysWith >= EscalationChainDaysWith)
                    {
                        ContactUserId = GetContactUserId(qepl.SupervisorUserId);
                        if (qepl.DaysWith >= (EscalationChainDaysWith * 2))
                        {
                            ContactUserId = GetContactUserId(qepl.Level3UserId);
                            if (qepl.DaysWith >= (EscalationChainDaysWith * 3))
                            {
                                ContactUserId = GetContactUserId(qepl.Level4UserId);
                                if (qepl.DaysWith >= (EscalationChainDaysWith * 4))
                                {
                                    ContactUserId = GetContactUserId(qepl.Level5UserId);
                                    if (qepl.DaysWith >= (EscalationChainDaysWith * 5))
                                    {
                                        ContactUserId = GetContactUserId(qepl.Level6UserId);
                                        if (qepl.DaysWith >= (EscalationChainDaysWith * 6))
                                        {
                                            ContactUserId = GetContactUserId(qepl.Level7UserId);
                                            if (qepl.DaysWith >= (EscalationChainDaysWith * 7))
                                            {
                                                ContactUserId = GetContactUserId(qepl.Level8UserId);
                                                if (qepl.DaysWith >= (EscalationChainDaysWith * 8))
                                                {
                                                    ContactUserId = GetContactUserId(qepl.Level9UserId);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                return ContactUserId;
            }
            public static string GetListPeopleContactedAlready(QuestionnaireEscalationProcurementList qepl, int EscalationChainDaysWith)
            {
                string list = "";

                int? ContactUserId = null;

                ArrayList contacted = new ArrayList();

                if (qepl.DaysWith != null)
                {
                    if (qepl.DaysWith >= EscalationChainDaysWith)
                    {
                        ContactUserId = GetContactUserId(qepl.SupervisorUserId);
                        if (!contacted.Contains(GetFirstLastName(Convert.ToInt32(ContactUserId)))) contacted.Add(GetFirstLastName(Convert.ToInt32(ContactUserId)));
                        if (qepl.DaysWith >= (EscalationChainDaysWith * 2))
                        {
                            ContactUserId = GetContactUserId(qepl.Level3UserId);
                            if (!contacted.Contains(GetFirstLastName(Convert.ToInt32(ContactUserId)))) contacted.Add(GetFirstLastName(Convert.ToInt32(ContactUserId)));
                            if (qepl.DaysWith >= (EscalationChainDaysWith * 3))
                            {
                                ContactUserId = GetContactUserId(qepl.Level4UserId);
                                if (!contacted.Contains(GetFirstLastName(Convert.ToInt32(ContactUserId)))) contacted.Add(GetFirstLastName(Convert.ToInt32(ContactUserId)));
                                if (qepl.DaysWith >= (EscalationChainDaysWith * 4))
                                {
                                    ContactUserId = GetContactUserId(qepl.Level5UserId);
                                    if (!contacted.Contains(GetFirstLastName(Convert.ToInt32(ContactUserId)))) contacted.Add(GetFirstLastName(Convert.ToInt32(ContactUserId)));
                                    if (qepl.DaysWith >= (EscalationChainDaysWith * 5))
                                    {
                                        ContactUserId = GetContactUserId(qepl.Level6UserId);
                                        if (!contacted.Contains(GetFirstLastName(Convert.ToInt32(ContactUserId)))) contacted.Add(GetFirstLastName(Convert.ToInt32(ContactUserId)));
                                        if (qepl.DaysWith >= (EscalationChainDaysWith * 6))
                                        {
                                            ContactUserId = GetContactUserId(qepl.Level7UserId);
                                            if (!contacted.Contains(GetFirstLastName(Convert.ToInt32(ContactUserId)))) contacted.Add(GetFirstLastName(Convert.ToInt32(ContactUserId)));
                                            if (qepl.DaysWith >= (EscalationChainDaysWith * 7))
                                            {
                                                ContactUserId = GetContactUserId(qepl.Level8UserId);
                                                if (!contacted.Contains(GetFirstLastName(Convert.ToInt32(ContactUserId)))) contacted.Add(GetFirstLastName(Convert.ToInt32(ContactUserId)));
                                                if (qepl.DaysWith >= (EscalationChainDaysWith * 8))
                                                {
                                                    ContactUserId = GetContactUserId(qepl.Level9UserId);
                                                    if (!contacted.Contains(GetFirstLastName(Convert.ToInt32(ContactUserId)))) contacted.Add(GetFirstLastName(Convert.ToInt32(ContactUserId)));
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }

                    }
                }

                if (contacted.Count > 0)
                {
                    contacted.Sort();
                    foreach (string contact in contacted)
                    {
                        list += contact + "; ";
                    }
                    if (list.Length > 2) list = list.Remove((list.Length - 2), 2);
                }
                return list;
            }
            public static int? GetContactUserId(QuestionnaireEscalationHsAssessorList qehl, int EscalationChainDaysWith)
            {
                int? ContactUserId = null;
                if (qehl.DaysWith != null)
                {
                    if (qehl.DaysWith >= EscalationChainDaysWith)
                    {
                        ContactUserId = GetContactUserId(qehl.SupervisorUserId);
                        if (qehl.DaysWith >= (EscalationChainDaysWith * 2))
                        {
                            ContactUserId = GetContactUserId(qehl.Level3UserId);
                            if (qehl.DaysWith >= (EscalationChainDaysWith * 3))
                            {
                                ContactUserId = GetContactUserId(qehl.Level4UserId);
                                if (qehl.DaysWith >= (EscalationChainDaysWith * 4))
                                {
                                    ContactUserId = GetContactUserId(qehl.Level5UserId);
                                    if (qehl.DaysWith >= (EscalationChainDaysWith * 5))
                                    {
                                        ContactUserId = GetContactUserId(qehl.Level6UserId);
                                        if (qehl.DaysWith >= (EscalationChainDaysWith * 6))
                                        {
                                            ContactUserId = GetContactUserId(qehl.Level7UserId);
                                            if (qehl.DaysWith >= (EscalationChainDaysWith * 7))
                                            {
                                                ContactUserId = GetContactUserId(qehl.Level8UserId);
                                                if (qehl.DaysWith >= (EscalationChainDaysWith * 8))
                                                {
                                                    ContactUserId = GetContactUserId(qehl.Level9UserId);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                return ContactUserId;
            }
            public static string GetListPeopleContactedAlready(QuestionnaireEscalationHsAssessorList qehl, int EscalationChainDaysWith)
            {
                string list = "";

                int? ContactUserId = null;

                ArrayList contacted = new ArrayList();

                if (qehl.DaysWith != null)
                {
                    if (qehl.DaysWith >= EscalationChainDaysWith)
                    {
                        ContactUserId = GetContactUserId(qehl.SupervisorUserId);
                        if (!contacted.Contains(GetFirstLastName(Convert.ToInt32(ContactUserId)))) contacted.Add(GetFirstLastName(Convert.ToInt32(ContactUserId)));
                        if (qehl.DaysWith >= (EscalationChainDaysWith * 2))
                        {
                            ContactUserId = GetContactUserId(qehl.Level3UserId);
                            if (!contacted.Contains(GetFirstLastName(Convert.ToInt32(ContactUserId)))) contacted.Add(GetFirstLastName(Convert.ToInt32(ContactUserId)));
                            if (qehl.DaysWith >= (EscalationChainDaysWith * 3))
                            {
                                ContactUserId = GetContactUserId(qehl.Level4UserId);
                                if (!contacted.Contains(GetFirstLastName(Convert.ToInt32(ContactUserId)))) contacted.Add(GetFirstLastName(Convert.ToInt32(ContactUserId)));
                                if (qehl.DaysWith >= (EscalationChainDaysWith * 4))
                                {
                                    ContactUserId = GetContactUserId(qehl.Level5UserId);
                                    if (!contacted.Contains(GetFirstLastName(Convert.ToInt32(ContactUserId)))) contacted.Add(GetFirstLastName(Convert.ToInt32(ContactUserId)));
                                    if (qehl.DaysWith >= (EscalationChainDaysWith * 5))
                                    {
                                        ContactUserId = GetContactUserId(qehl.Level6UserId);
                                        if (!contacted.Contains(GetFirstLastName(Convert.ToInt32(ContactUserId)))) contacted.Add(GetFirstLastName(Convert.ToInt32(ContactUserId)));
                                        if (qehl.DaysWith >= (EscalationChainDaysWith * 6))
                                        {
                                            ContactUserId = GetContactUserId(qehl.Level7UserId);
                                            if (!contacted.Contains(GetFirstLastName(Convert.ToInt32(ContactUserId)))) contacted.Add(GetFirstLastName(Convert.ToInt32(ContactUserId)));
                                            if (qehl.DaysWith >= (EscalationChainDaysWith * 7))
                                            {
                                                ContactUserId = GetContactUserId(qehl.Level8UserId);
                                                if (!contacted.Contains(GetFirstLastName(Convert.ToInt32(ContactUserId)))) contacted.Add(GetFirstLastName(Convert.ToInt32(ContactUserId)));
                                                if (qehl.DaysWith >= (EscalationChainDaysWith * 8))
                                                {
                                                    ContactUserId = GetContactUserId(qehl.Level9UserId);
                                                    if (!contacted.Contains(GetFirstLastName(Convert.ToInt32(ContactUserId)))) contacted.Add(GetFirstLastName(Convert.ToInt32(ContactUserId)));
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }

                    }
                }

                if (contacted.Count > 0)
                {
                    contacted.Sort();
                    foreach (string contact in contacted)
                    {
                        list += contact + "; ";
                    }
                    if (list.Length > 2) list = list.Remove((list.Length - 2), 2);
                }
                return list;
            }
            public static EscalationEmailTask Task(string personResponsibleFirstLastName, string companyName, string role, string presentlyWithDetails, int daysWith, string alreadyContacted)
            {
                EscalationEmailTask task = new EscalationEmailTask();
                task.PersonResponsible = personResponsibleFirstLastName;
                task.Company = companyName;
                task.Role = role;
                task.PresentlyWithDetails = presentlyWithDetails;
                task.DaysWith = daysWith;
                task.AlreadyContacted = alreadyContacted;
                return task;
            }

            public static void HsAssessorEscalationEmail()
            {
                try
                {
                    int EscalationChainDaysWith = -1;
                    int EscalationChainDaysWithEmailDaysFrequency = -1; //Added by P. Dikshit from <DT 2579, 21-Aug-2012>
                    Configuration config = new Configuration();
                    Int32.TryParse(config.GetValue(ConfigList.EscalationChainDaysWithHsAssessor), out EscalationChainDaysWith);

                    //Added by P. Dikshit from <DT 2579, 21-Aug-2012>
                    Int32.TryParse(config.GetValue(ConfigList.EscalationChainDaysWithHsAssessorEmailDaysFrequency), out EscalationChainDaysWithEmailDaysFrequency);
                    if (EscalationChainDaysWith != -1 && EscalationChainDaysWith != 0)
                    {
                        EscalationEmailCollection emailList = new EscalationEmailCollection();

                        QuestionnaireEscalationHsAssessorListService qehlService = new QuestionnaireEscalationHsAssessorListService();
                        VList<QuestionnaireEscalationHsAssessorList> qehlList = qehlService.GetAll();

                        foreach (QuestionnaireEscalationHsAssessorList qehl in qehlList)
                        {
                            try
                            {
                                if (qehl.UserId != null)
                                {
                                    //Added by P. Dikshit from <DT 2579, 21-Aug-2012>
                                    if (IsMatchingEmailDaysFrequency(qehl, EscalationChainDaysWithEmailDaysFrequency))
                                    {
                                        int? contactUserId = GetContactUserId(qehl, EscalationChainDaysWith);
                                        string alreadyContacted = (GetListPeopleContactedAlready(qehl, EscalationChainDaysWith)).Replace("ALCOA CSMS", "Alcoa CSMS Team");

                                        if (contactUserId != null)
                                        {
                                            UsersService uService = new UsersService();
                                            Users u = uService.GetByUserId(Convert.ToInt32(contactUserId));
                                            string contactUserEmail = u.Email;
                                            string personBeingContacted = String.Format("{0} {1}", u.FirstName, u.LastName);
                                            if (personBeingContacted == "ALCOA CSMS") personBeingContacted = "Alcoa CSMS Team";
                                            string personResponsibleFirstLastName = qehl.HsAssessorFirstLastName;
                                            string companyName = qehl.CompanyName;
                                            string role = "Procurement";
                                            string presentlyWithDetails = qehl.ActionDescription;
                                            int daysWith = -1;
                                            if (qehl.DaysWith != null) daysWith = Convert.ToInt32(qehl.DaysWith);

                                            EscalationEmail email = emailList.Find(contactUserEmail);
                                            if (email == null)
                                            {
                                                email = new EscalationEmail();
                                                email.RecipientTo = contactUserEmail;
                                                email.PersonBeingContacted = personBeingContacted;
                                                email.Tasks = new EscalationEmailTasks();
                                                email.Tasks.Add(Task(personResponsibleFirstLastName, companyName, role, presentlyWithDetails, daysWith, alreadyContacted));
                                                emailList.Add(email);
                                            }
                                            else
                                            {
                                                EscalationEmailTask task = emailList[emailList.IndexOf(email)].Tasks.Find(companyName);
                                                if (task == null)
                                                    emailList[emailList.IndexOf(email)].Tasks.Add(Task(personResponsibleFirstLastName, companyName, role, presentlyWithDetails, daysWith, alreadyContacted));
                                                else
                                                {
                                                    Log4NetService.LogInfo("shouldn't hit this...");
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                Log4NetService.LogInfo(qehl.QuestionnaireId.ToString());
                            }
                        }

                        foreach (EscalationEmail email in emailList)
                        {
                            string emailToName = email.PersonBeingContacted;
                            string[] to = { email.RecipientTo };
                            string[] cc = { email.RecipientCc };
                            string emailSubject = "Alcoa CSMS - Overdue H&S Assessor Safety Qualification Actions";

                            //string emailBody = BuildEmailBody(u.UserId, emailToName, qrpList, qreList, cList);
                            string emailBody = BuildEmailBody(email.PersonBeingContacted, email.Tasks, "H&S Assessor");

                            if (!String.IsNullOrEmpty(emailBody))
                            {
                                //Cindi Thornton 4/9/2015 - removed call to sendEmail, modified call to logEmail for new columns IsBodyHtml & attachmentPath. EmailLog used to queue & log emails.
                                //Helper.EmailAgent.sendEmail(to, cc, null, emailSubject, emailBody, true, null); //Send E-Mail
                                //Helper.EmailAgent.logEmail(to, cc, null, emailSubject, emailBody, EmailLogTypeList.General, true, null); //Log

                                var emailLogService = DependencyResolver.Current.GetService<repo.IEmailLogService>();
                                emailLogService.Insert(to, cc, null, emailSubject, emailBody, "General", true, null, null);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Log4NetService.LogInfo(ex.Message);
                }
            }
            public static void ProcurementEscalationEmail()
            {
                try
                {
                    int EscalationChainDaysWith = -1;
                    int EscalationChainDaysWithEmailDaysFrequency = -1; //Added by P. Dikshit from <DT 2579, 21-Aug-2012>
                    Configuration config = new Configuration();
                    Int32.TryParse(config.GetValue(ConfigList.EscalationChainDaysWithProcurement), out EscalationChainDaysWith);
                    //Added by P. Dikshit from <DT 2579, 21-Aug-2012>
                    Int32.TryParse(config.GetValue(ConfigList.EscalationChainDaysWithProcurementEmailDaysFrequency), out EscalationChainDaysWithEmailDaysFrequency);
                    if (EscalationChainDaysWith != -1 && EscalationChainDaysWith != 0)
                    {
                        EscalationEmailCollection emailList = new EscalationEmailCollection();

                        QuestionnaireEscalationProcurementListService qeplService = new QuestionnaireEscalationProcurementListService();
                        VList<QuestionnaireEscalationProcurementList> qeplList = qeplService.GetAll();

                        foreach (QuestionnaireEscalationProcurementList qepl in qeplList)
                        {
                            try
                            {
                                if (qepl.ProcurementUserId != null && qepl.UserDescription == "Procurement")
                                {
                                    //Added by P. Dikshit from <DT 2579, 21-Aug-2012>
                                    if (IsMatchingEmailDaysFrequency(qepl, EscalationChainDaysWith, EscalationChainDaysWithEmailDaysFrequency))
                                    {
                                        int? contactUserId = GetContactUserId(qepl, EscalationChainDaysWith);
                                        string alreadyContacted = (GetListPeopleContactedAlready(qepl, EscalationChainDaysWith)).Replace("ALCOA CSMS", "Alcoa CSMS Team");

                                        if (contactUserId != null)
                                        {
                                            UsersService uService = new UsersService();
                                            Users u = uService.GetByUserId(Convert.ToInt32(contactUserId));
                                            string contactUserEmail = u.Email;
                                            string personBeingContacted = String.Format("{0} {1}", u.FirstName, u.LastName);
                                            if (personBeingContacted == "ALCOA CSMS") personBeingContacted = "Alcoa CSMS Team";
                                            string personResponsibleFirstLastName = qepl.ProcurementContactFirstLastName;
                                            string companyName = qepl.CompanyName;
                                            string role = "Procurement";
                                            string presentlyWithDetails = qepl.ActionDescription;
                                            int daysWith = -1;
                                            if (qepl.DaysWith != null) daysWith = Convert.ToInt32(qepl.DaysWith);

                                            EscalationEmail email = emailList.Find(contactUserEmail);
                                            if (email == null)
                                            {
                                                email = new EscalationEmail();
                                                email.RecipientTo = contactUserEmail;
                                                email.PersonBeingContacted = personBeingContacted;
                                                email.Tasks = new EscalationEmailTasks();
                                                email.Tasks.Add(Task(personResponsibleFirstLastName, companyName, role, presentlyWithDetails, daysWith, alreadyContacted));
                                                emailList.Add(email);
                                            }
                                            else
                                            {
                                                EscalationEmailTask task = emailList[emailList.IndexOf(email)].Tasks.Find(companyName);
                                                if (task == null)
                                                    emailList[emailList.IndexOf(email)].Tasks.Add(Task(personResponsibleFirstLastName, companyName, role, presentlyWithDetails, daysWith, alreadyContacted));
                                                else
                                                {
                                                    Log4NetService.LogInfo("shouldn't hit this...");
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                Log4NetService.LogInfo(qepl.QuestionnaireId.ToString());
                            }
                        }

                        foreach (EscalationEmail email in emailList)
                        {
                            string emailToName = email.PersonBeingContacted;
                            string[] to = { email.RecipientTo };
                            string[] cc = { email.RecipientCc };
                            string emailSubject = "Alcoa CSMS - Overdue Procurement Safety Qualification Actions";

                            //string emailBody = BuildEmailBody(u.UserId, emailToName, qrpList, qreList, cList);
                            string emailBody = BuildEmailBody(email.PersonBeingContacted, email.Tasks, "Procurement");

                            if (!String.IsNullOrEmpty(emailBody))
                            {
                                //Cindi Thornton 4/9/2015 - removed call to sendEmail, modified call to logEmail for new columns IsBodyHtml & attachmentPath. EmailLog used to queue & log emails.
                                //Helper.EmailAgent.sendEmail(to, cc, null, emailSubject, emailBody, true, null); //Send E-Mail
                                //Helper.EmailAgent.logEmail(to, cc, null, emailSubject, emailBody, EmailLogTypeList.General, true, null); //Log

                                var emailLogService = DependencyResolver.Current.GetService<repo.IEmailLogService>();
                                emailLogService.Insert(to, cc, null, emailSubject, emailBody, "General", true, null, null);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Log4NetService.LogInfo(ex.Message);
                }
            }
            public static void SupplierEscalationEmail()
            {
                try
                {
                    int EscalationChainDaysWith = -1;
                    int EscalationChainDaysWithEmailDaysFrequency = -1; //Added by P. Dikshit from <DT 2579, 21-Aug-2012>
                    Configuration config = new Configuration();
                    Int32.TryParse(config.GetValue(ConfigList.EscalationChainDaysWithSupplier), out EscalationChainDaysWith);

                    //Added by P. Dikshit from <DT 2579, 21-Aug-2012>
                    Int32.TryParse(config.GetValue(ConfigList.EscalationChainDaysWithSupplierEmailDaysFrequency), out EscalationChainDaysWithEmailDaysFrequency);
                    if (EscalationChainDaysWith != -1 && EscalationChainDaysWith != 0)
                    {
                        EscalationEmailCollection emailList = new EscalationEmailCollection();

                        QuestionnaireEscalationProcurementListService qeplService = new QuestionnaireEscalationProcurementListService();
                        VList<QuestionnaireEscalationProcurementList> qeplList = qeplService.GetAll();

                        foreach (QuestionnaireEscalationProcurementList qepl in qeplList)
                        {
                            try
                            {
                                if (qepl.ProcurementUserId != null && qepl.UserDescription == "Supplier")
                                {
                                    //Added by P. Dikshit from <DT 2579, 21-Aug-2012>
                                    if (IsMatchingEmailDaysFrequency(qepl, EscalationChainDaysWith, EscalationChainDaysWithEmailDaysFrequency))
                                    {
                                        int? contactUserId = GetContactUserId(qepl, EscalationChainDaysWith);
                                        string alreadyContacted = (GetListPeopleContactedAlready(qepl, EscalationChainDaysWith)).Replace("ALCOA CSMS", "Alcoa CSMS Team");

                                        if (contactUserId != null)
                                        {
                                            UsersService uService = new UsersService();
                                            Users u = uService.GetByUserId(Convert.ToInt32(contactUserId));
                                            string contactUserEmail = u.Email;
                                            string personBeingContacted = String.Format("{0} {1}", u.FirstName, u.LastName);
                                            if (personBeingContacted == "ALCOA CSMS") personBeingContacted = "Alcoa CSMS Team";
                                            string personResponsibleFirstLastName = qepl.ProcurementContactFirstLastName;
                                            string companyName = qepl.CompanyName;
                                            string role = "Procurement";
                                            string presentlyWithDetails = qepl.ActionDescription;
                                            int daysWith = -1;
                                            if (qepl.DaysWith != null) daysWith = Convert.ToInt32(qepl.DaysWith);

                                            EscalationEmail email = emailList.Find(contactUserEmail);
                                            if (email == null)
                                            {
                                                email = new EscalationEmail();
                                                email.RecipientTo = contactUserEmail;
                                                email.PersonBeingContacted = personBeingContacted;
                                                email.Tasks = new EscalationEmailTasks();
                                                email.Tasks.Add(Task(personResponsibleFirstLastName, companyName, role, presentlyWithDetails, daysWith, alreadyContacted));
                                                emailList.Add(email);
                                            }
                                            else
                                            {
                                                EscalationEmailTask task = emailList[emailList.IndexOf(email)].Tasks.Find(companyName);
                                                if (task == null)
                                                    emailList[emailList.IndexOf(email)].Tasks.Add(Task(personResponsibleFirstLastName, companyName, role, presentlyWithDetails, daysWith, alreadyContacted));
                                                else
                                                {
                                                    Log4NetService.LogInfo("shouldn't hit this...");
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                Log4NetService.LogInfo(qepl.QuestionnaireId.ToString());
                            }
                        }

                        foreach (EscalationEmail email in emailList)
                        {
                            string emailToName = email.PersonBeingContacted;
                            string[] to = { email.RecipientTo };
                            string[] cc = { email.RecipientCc };
                            string emailSubject = "Alcoa CSMS - Overdue Supplier Safety Qualification Actions";

                            //string emailBody = BuildEmailBody(u.UserId, emailToName, qrpList, qreList, cList);
                            string emailBody = BuildEmailBody(email.PersonBeingContacted, email.Tasks, "Supplier");

                            if (!String.IsNullOrEmpty(emailBody))
                            {
                                //Cindi Thornton 4/9/2015 - removed call to sendEmail, modified call to logEmail for new columns IsBodyHtml & attachmentPath. EmailLog used to queue & log emails.
                                //Helper.EmailAgent.sendEmail(to, cc, null, emailSubject, emailBody, true, null); //Send E-Mail
                                //Helper.EmailAgent.logEmail(to, cc, null, emailSubject, emailBody, EmailLogTypeList.General, true, null); //Log

                                var emailLogService = DependencyResolver.Current.GetService<repo.IEmailLogService>();
                                emailLogService.Insert(to, cc, null, emailSubject, emailBody, "General", true, null, null);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Log4NetService.LogInfo(ex.Message);
                }
            }

            public static string BuildEmailBody(string personBeingContacted, EscalationEmailTasks tasks, string type)
            {
                string emailBody = "";
                ASCIIEncoding encoding = new ASCIIEncoding();

                TemplateService tService = new TemplateService();
                Template t = null;

                Configuration config = new Configuration();
                string EscalationChainDaysWith = "";
                if (type == "Procurement")
                    EscalationChainDaysWith = config.GetValue(ConfigList.EscalationChainDaysWithProcurement);
                else
                    EscalationChainDaysWith = config.GetValue(ConfigList.EscalationChainDaysWithHsAssessor);

                switch (type)
                {
                    case "H&S Assessor":
                        t = tService.GetByTemplateId((int)TemplateTypeList.BatchJobEmailSqEscalationHsAssessor);
                        break;
                    case "Procurement":
                        t = tService.GetByTemplateId((int)TemplateTypeList.BatchJobEmailSqEscalationProcurement);
                        break;
                    case "Supplier":
                        t = tService.GetByTemplateId((int)TemplateTypeList.BatchJobEmailSqEscalationSupplier);
                        break;
                    default:
                        break;
                }

                if (t != null)
                {
                    emailBody = Encoding.ASCII.GetString(t.TemplateContent);



                    emailBody = emailBody.Replace("{FirstLastName}", personBeingContacted);
                    emailBody = emailBody.Replace("{EscalationChainDaysWith}", EscalationChainDaysWith);


                    DataTable dt = new DataTable();
                    dt.Columns.Add("PersonResponsible", typeof(string));
                    dt.Columns.Add("Company", typeof(string));
                    dt.Columns.Add("PresentlyWithDetails", typeof(string));
                    dt.Columns.Add("DaysWith", typeof(int));
                    dt.Columns.Add("AlreadyContacted", typeof(string));

                    foreach (EscalationEmailTask task in tasks)
                    {
                        DataRow dr = dt.NewRow();
                        dr["PersonResponsible"] = task.PersonResponsible;
                        dr["Company"] = task.Company;
                        dr["PresentlyWithDetails"] = task.PresentlyWithDetails;
                        dr["DaysWith"] = task.DaysWith;
                        dr["AlreadyContacted"] = task.AlreadyContacted;

                        dt.Rows.Add(dr);
                    }

                    DataRow[] drList = dt.Select(null, "DaysWith DESC, Company DESC");

                    string table = "<table border='1' cellpadding='0' cellspacing='0' class='style1'" +
                    "style='mso-border-alt: outset silver .75pt; mso-yfti-tbllook: 1184'>" +
                        "<tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes; padding-right: 3px; padding-left: 3px;'>" +
                            "<td>" +
                                "<p class='MsoNormal'>" +
                                    "<strong><span style='mso-fareast-font-family:&quot;Times New Roman&quot;'>Person " +
                                    "Responsible for the task</span></strong><span " +
                                        "style='mso-fareast-font-family:&quot;Times New Roman&quot;'><o:p></o:p></span></p>" +
                            "</td>" +
                            "<td>" +
                                "<p class='MsoNormal'>" +
                                    "<strong><span style='mso-fareast-font-family:&quot;Times New Roman&quot;'>" +
                                    "Company</span></strong><span " +
                                        "style='mso-fareast-font-family:&quot;Times New Roman&quot;'><o:p></o:p></span></p>" +
                            "</td>" +
                            "<td>" +
                                "<p class='MsoNormal'>" +
                                    "<strong><span style='mso-fareast-font-family:&quot;Times New Roman&quot;'>" +
                                    "Presently With Details</span></strong><span " +
                                        "style='mso-fareast-font-family:&quot;Times New Roman&quot;'><o:p></o:p></span></p>" +
                            "</td>" +
                            "<td>" +
                                "<p class='MsoNormal'>" +
                                    "<strong><span style='mso-fareast-font-family:&quot;Times New Roman&quot;'>Days " +
                                    "With</span></strong><span " +
                                        "style='mso-fareast-font-family:&quot;Times New Roman&quot;'><o:p></o:p></span></p>" +
                            "</td>" +
                            "<td>" +
                                "<p class='MsoNormal'>" +
                                    "<strong><span style='mso-fareast-font-family:&quot;Times New Roman&quot;'>" +
                                    "Following People have been informed about this issue</span></strong></p>" +
                            "</td>" +
                        "</tr>";

                    foreach (DataRow dr in drList)
                    {
                        string tableRow = String.Format("<tr style='mso-yfti-irow:1;mso-yfti-lastrow:yes'><td><p class='MsoNormal'>{0}</p></td><td><p class='MsoNormal'>{1}</p></td><td><p class='MsoNormal'>{2}</p></td><td><p class='MsoNormal'>{3}</p></td><td><p class='MsoNormal'>{4}</p></td>",
                                                        dr["PersonResponsible"].ToString(),
                                                        dr["Company"].ToString(),
                                                        dr["PresentlyWithDetails"].ToString(),
                                                        dr["DaysWith"].ToString(),
                                                        dr["AlreadyContacted"].ToString());
                        table += tableRow;
                    }

                    table += "</table>";

                    emailBody = emailBody.Replace("{Table}", table);

                }

                return emailBody;
            }

            public class EscalationEmailTask
            {
                private string personResponsible;
                private string company;
                private string role;
                private string presentlyWithDetails;
                private string alreadyContacted;
                private int daysWith = 0;

                public string PersonResponsible
                {
                    get { return personResponsible; }
                    set { personResponsible = value; }
                }
                public string Company
                {
                    get { return company; }
                    set { company = value; }
                }
                public string PresentlyWithDetails
                {
                    get { return presentlyWithDetails; }
                    set { presentlyWithDetails = value; }
                }
                public string Role
                {
                    get { return role; }
                    set { role = value; }
                }
                public string AlreadyContacted
                {
                    get { return alreadyContacted; }
                    set { alreadyContacted = value; }
                }
                public int DaysWith
                {
                    get { return daysWith; }
                    set { daysWith = value; }
                }

                public EscalationEmailTask()
                {
                }

            }
            public class EscalationEmail
            {
                private string personBeingContacted;
                private string recipientTo;
                private string recipientCc;


                public string PersonBeingContacted
                {
                    get { return personBeingContacted; }
                    set { personBeingContacted = value; }
                }
                public string RecipientTo
                {
                    get { return recipientTo; }
                    set { recipientTo = value; }
                }
                public string RecipientCc
                {
                    get { return recipientCc; }
                    set { recipientCc = value; }
                }
                public EscalationEmailTasks Tasks { get; set; }

                public EscalationEmail()
                {
                }
                //public EscalationEmail(string strItemName)
                //{
                //    m_strEscalationEmailName = strItemName;
                //}
            }
            public class EscalationEmailCollection : CollectionBase
            {
                public int Add(EscalationEmail item)
                {
                    return List.Add(item);
                }
                public void Insert(int index, EscalationEmail item)
                {
                    List.Add(item);
                }
                public void Remove(EscalationEmail item)
                {
                    List.Remove(item);
                }
                public bool Contains(EscalationEmail item)
                {
                    return List.Contains(item);
                }
                public int IndexOf(EscalationEmail item)
                {
                    return List.IndexOf(item);
                }
                public void CopyTo(EscalationEmail[] array, int index)
                {
                    List.CopyTo(array, index);
                }
                public EscalationEmail this[int index]
                {
                    get { return (EscalationEmail)List[index]; }
                    set { List[index] = value; }
                }
                public EscalationEmail Find(string to)
                {
                    foreach (EscalationEmail e in List)
                    {
                        if (e.RecipientTo == to)
                            return e;
                    }
                    return null;
                }
            }
            public class EscalationEmailTasks : CollectionBase
            {
                public int Add(EscalationEmailTask item)
                {
                    return List.Add(item);
                }
                public void Insert(int index, EscalationEmailTask item)
                {
                    List.Add(item);
                }
                public void Remove(EscalationEmailTask item)
                {
                    List.Remove(item);
                }
                public bool Contains(EscalationEmailTask item)
                {
                    return List.Contains(item);
                }
                public int IndexOf(EscalationEmailTask item)
                {
                    return List.IndexOf(item);
                }
                public void CopyTo(EscalationEmailTask[] array, int index)
                {
                    List.CopyTo(array, index);
                }
                public EscalationEmailTask this[int index]
                {
                    get { return (EscalationEmailTask)List[index]; }
                    set { List[index] = value; }
                }
                public EscalationEmailTask Find(string personResponsible, string company)
                {
                    foreach (EscalationEmailTask e in List)
                    {
                        if (e.PersonResponsible == personResponsible && e.Company == company)
                            return e;
                    }
                    return null;
                }
                public EscalationEmailTask Find(string company)
                {
                    foreach (EscalationEmailTask e in List)
                    {
                        if (e.Company == company)
                            return e;
                    }
                    return null;
                }
            }
        }

        public class List
        {
            /// <summary>
            /// Lists the sq expiry reminders.
            /// </summary>
            public static void SqExpiryReminders()
            {
                QuestionnaireReportExpiryService qreService = new QuestionnaireReportExpiryService();
                VList<QuestionnaireReportExpiry> qreVList = qreService.GetAll();
                DataSet qreDs = qreVList.ToDataSet(false);
                foreach (DataRow dr in qreDs.Tables[0].Rows)
                {
                    bool cont = false;
                    int ExpiresIn = 0;
                    try
                    {
                        ExpiresIn = Convert.ToInt32(dr["ExpiresIn"].ToString());
                        cont = true;
                    }
                    catch (Exception)
                    {
                        cont = false;
                    }
                    if (cont)
                    {
                        if (ExpiresIn <= 60)
                        {
                            int QuestionnaireId = Convert.ToInt32(dr["QuestionnaireId"].ToString());

                            int count = -1;
                            QuestionnaireReportOverviewFilters qroFilters = new QuestionnaireReportOverviewFilters();
                            qroFilters.Append(QuestionnaireReportOverviewColumn.QuestionnaireId, QuestionnaireId.ToString());
                            VList<QuestionnaireReportOverview> qroVList = DataRepository.QuestionnaireReportOverviewProvider.GetPaged(qroFilters.ToString(), null, 0, 100, out count);

                            if (count <= 0)
                            {
                                Log4NetService.LogInfo(String.Format("questionnaireid ({0}) not found", QuestionnaireId));
                            }
                            else
                            {
                                if (count > 1)
                                {
                                    Log4NetService.LogInfo(String.Format("questionnaireid ({0}) duplicate found. db fail.", QuestionnaireId));
                                }
                                else
                                {
                                    UsersService usersService = new UsersService();
                                    Users userEntity;
                                    userEntity = usersService.GetByUserId(Convert.ToInt32(dr["CreatedByUserId"].ToString()));
                                    string ProcurementContact = "n/a";
                                    string ProcurementContactEmail = "n/a";
                                    if (qroVList[0].ProcurementContactUserId != null)
                                    {
                                        Users userEntityProcurementContact = usersService.GetByUserId(Convert.ToInt32(qroVList[0].ProcurementContactUserId));
                                        ProcurementContact = String.Format("{0}, {1}", userEntityProcurementContact.LastName, userEntityProcurementContact.FirstName);
                                        ProcurementContactEmail = userEntityProcurementContact.Email;
                                    }

                                    string DayExpires = dr["MainAssessmentValidTo"].ToString();
                                    string ModifiedDate = dr["ModifiedDate"].ToString();

                                    CompaniesService companiesService = new CompaniesService();
                                    Companies companiesEntity = companiesService.GetByCompanyId(qroVList[0].CompanyId);
                                    Companies companiesEntity2 = companiesService.GetByCompanyId(userEntity.CompanyId);

                                    string SitesTicked = "";
                                    bool VicOps = false;
                                    QuestionnaireInitialLocationService qilService = new QuestionnaireInitialLocationService();
                                    TList<QuestionnaireInitialLocation> qilTList = qilService.GetByQuestionnaireId(qroVList[0].QuestionnaireId);
                                    foreach (QuestionnaireInitialLocation qil in qilTList)
                                    {
                                        SitesService sService = new SitesService();
                                        Sites s = sService.GetBySiteId(qil.SiteId);
                                        if (String.IsNullOrEmpty(SitesTicked))
                                        {
                                            SitesTicked = s.SiteName;
                                        }
                                        else
                                        {
                                            SitesTicked += ", " + s.SiteName;
                                        }

                                        RegionsSitesService rsService = new RegionsSitesService();
                                        TList<RegionsSites> rsTList = rsService.GetBySiteId(qil.SiteId);
                                        RegionsService rService = new RegionsService();
                                        Regions r = rService.GetByRegionInternalName("VICOPS");
                                        foreach (RegionsSites rs in rsTList)
                                        {
                                            if (rs.RegionId == r.RegionId) VicOps = true;
                                        }
                                    }
                                    if (String.IsNullOrEmpty(SitesTicked)) SitesTicked = "n/a";

                                    if (ProcurementContactEmail == "n/a")
                                    {
                                        if (VicOps)
                                        {
                                            Log4NetService.LogInfo(String.Format("{0}, {1}, {2}, VICOPS", QuestionnaireId, ExpiresIn, companiesEntity.CompanyName));
                                        }
                                        else
                                        {
                                            Log4NetService.LogInfo(String.Format("{0}, {1}, {2}, CSMS Team", QuestionnaireId, ExpiresIn, companiesEntity.CompanyName));
                                        }
                                    }
                                    else
                                    {
                                        if (VicOps)
                                        {
                                            Log4NetService.LogInfo(String.Format("{0}, {1}, {2}, VICOPS + {3}", QuestionnaireId, ExpiresIn, companiesEntity.CompanyName, ProcurementContactEmail));
                                        }
                                        else
                                        {
                                            Log4NetService.LogInfo(String.Format("{0}, {1}, {2}, {3}", QuestionnaireId, ExpiresIn, companiesEntity.CompanyName, ProcurementContactEmail));
                                        }
                                    }
                                }

                            }
                        }
                    }
                }
            }
        }

        public class Update
        {
            /// <summary>
            /// Sends the sq expiry reminder emails.
            /// </summary>

            /// <summary>
            /// Updates the company status if questionnaire expired.
            /// </summary>
            public static void CompanyStatusIfQuestionnaireExpired()
            {
                try
                {
                    bool errorsEncountered = false;
                    CompaniesService cService = new CompaniesService();
                    Repo.CSMS.Service.Database.ICompanySiteCategoryStandardService companySiteCategoryStandardService = DependencyResolver.Current.GetService<Repo.CSMS.Service.Database.ICompanySiteCategoryStandardService>();
                    TList<Companies> cTList = cService.GetAll();

                    StringBuilder companiesInError = new StringBuilder(); //DT 375 26/11/15 Cindi Thornton - improve logging

                    foreach (Companies c in cTList)
                    {
                        if (c.CompanyId != 0) //!= Alcoa
                        {
                            bool expired = false;
                            bool DeActivated = false;

                            QuestionnaireFilters qFilters = new QuestionnaireFilters();
                            qFilters.Append(QuestionnaireColumn.CompanyId, c.CompanyId.ToString());
                            int count = 0;
                            TList<Questionnaire> qList =
                                DataRepository.QuestionnaireProvider.GetPaged(qFilters.ToString(), "CreatedDate DESC", 0, 100, out count);

                            if (qList != null && count > 0)
                            {
                                if (c.Deactivated == true)
                                {
                                    DeActivated = true;
                                }
                                else
                                {
                                    if (qList[0].MainAssessmentValidTo != null && qList[0].Status == (int)QuestionnaireStatusList.AssessmentComplete)
                                    {
                                        DateTime dtExpiry = Convert.ToDateTime(qList[0].MainAssessmentValidTo);
                                        TimeSpan ts = Convert.ToDateTime(dtExpiry) - DateTime.Now;
                                        QuestionnaireService qService = new QuestionnaireService();
                                        if (ts.Days <= 60)
                                        {
                                            if (ts.Days > 0)
                                            {
                                                //expiring
                                                if (c.CompanyStatusId != (int)CompanyStatusList.InActive && //inactive
                                                    c.CompanyStatusId != (int)CompanyStatusList.InActive_No_Requal_Required && //inactive
                                                    c.CompanyStatusId != (int)CompanyStatusList.InActive_SQ_Incomplete && //inactive
                                                    c.CompanyStatusId != (int)CompanyStatusList.Requal_Incomplete && //being worked on
                                                    c.CompanyStatusId != (int)CompanyStatusList.Being_Assessed) //being worked on
                                                {

                                                    Questionnaire q = qService.GetByQuestionnaireId(qList[0].QuestionnaireId);
                                                    if (q.QuestionnairePresentlyWithActionId != (int)QuestionnairePresentlyWithActionList.ProcurementExpiring)
                                                    {
                                                        q.QuestionnairePresentlyWithActionId = (int)QuestionnairePresentlyWithActionList.ProcurementExpiring;

                                                        if (qList[0].MainAssessmentValidTo != null)
                                                        {
                                                            DateTime dt = Convert.ToDateTime(qList[0].MainAssessmentValidTo);
                                                            dt = dt.AddDays(-60);
                                                            q.QuestionnairePresentlyWithSince = dt;
                                                        }
                                                        else
                                                        {
                                                            q.QuestionnairePresentlyWithSince = null;
                                                        }

                                                        qService.Save(q);
                                                        Log4NetService.LogInfo(c.CompanyName + ": Expiring, Presently With Updated.");
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                expired = true;
                                                if (c.CompanyStatusId != (int)CompanyStatusList.InActive &&
                                                    c.CompanyStatusId != (int)CompanyStatusList.InActive_No_Requal_Required &&
                                                    c.CompanyStatusId != (int)CompanyStatusList.InActive_SQ_Incomplete &&
                                                    c.CompanyStatusId != (int)CompanyStatusList.Requal_Incomplete &&
                                                    c.CompanyStatusId != (int)CompanyStatusList.Being_Assessed)
                                                {

                                                    Questionnaire q = qService.GetByQuestionnaireId(qList[0].QuestionnaireId);
                                                    if (q.QuestionnairePresentlyWithActionId != (int)QuestionnairePresentlyWithActionList.ProcurementExpired)
                                                    {
                                                        q.QuestionnairePresentlyWithActionId = (int)QuestionnairePresentlyWithActionList.ProcurementExpired;
                                                        if (qList[0].MainAssessmentValidTo != null)
                                                        {
                                                            DateTime dt = Convert.ToDateTime(qList[0].MainAssessmentValidTo);
                                                            dt = dt.AddDays(-60);
                                                            q.QuestionnairePresentlyWithSince = dt;
                                                        }
                                                        else
                                                        {
                                                            q.QuestionnairePresentlyWithSince = null;
                                                        }
                                                        qService.Save(q);
                                                        Log4NetService.LogInfo(c.CompanyName + ": Expired, Presently With Updated.");
                                                    }
                                                }
                                            }
                                        }
                                        if (ts.Days > 60)
                                        {
                                            Questionnaire q = qService.GetByQuestionnaireId(qList[0].QuestionnaireId);
                                            if (c.CompanyStatusId != (int)CompanyStatusList.InActive &&
                                                c.CompanyStatusId != (int)CompanyStatusList.InActive_No_Requal_Required &&
                                                    c.CompanyStatusId != (int)CompanyStatusList.InActive_SQ_Incomplete &&
                                                    c.CompanyStatusId != (int)CompanyStatusList.Requal_Incomplete &&
                                                    c.CompanyStatusId != (int)CompanyStatusList.Being_Assessed)
                                            {
                                                if (q.QuestionnairePresentlyWithActionId != (int)QuestionnairePresentlyWithActionList.NoActionRequired)
                                                {
                                                    q.QuestionnairePresentlyWithActionId = (int)QuestionnairePresentlyWithActionList.NoActionRequired;
                                                    q.QuestionnairePresentlyWithSince = null;
                                                    qService.Save(q);
                                                    Log4NetService.LogInfo(c.CompanyName + ": No Action Required");
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (count > 1)
                                        {
                                            if (qList[1] != null)
                                            {
                                                if (qList[1].MainAssessmentValidTo != null && qList[1].Status == (int)QuestionnaireStatusList.AssessmentComplete)
                                                {
                                                    if (qList[1].MainAssessmentValidTo <= DateTime.Now)
                                                        expired = true;
                                                }
                                                else
                                                {
                                                    if (count > 2)
                                                    {
                                                        if (qList[2].MainAssessmentValidTo != null && qList[2].Status == (int)QuestionnaireStatusList.AssessmentComplete)
                                                        {
                                                            if (qList[2].MainAssessmentValidTo <= DateTime.Now)
                                                                expired = true;
                                                        }
                                                        else
                                                        {
                                                            //DT 375 26/11/15 Cindi Thornton - improve logging
                                                            //Log4NetService.LogInfo("Error: " + c.CompanyName);
                                                            AddQuestionnaireExpiredErrorText(companiesInError, c.CompanyName, qList[2].MainAssessmentValidTo, qList[2].QuestionnaireId, qList[2].Status, "3rd");

                                                            errorsEncountered = true;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        //DT 375 26/11/15 Cindi Thornton - improve logging
                                                        //Log4NetService.LogInfo("Error: " + c.CompanyName);
                                                        AddQuestionnaireExpiredErrorText(companiesInError, c.CompanyName, qList[1].MainAssessmentValidTo, qList[1].QuestionnaireId, qList[1].Status, "2nd");

                                                        errorsEncountered = true;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            if (DeActivated)
                            {
                                foreach (Questionnaire q in qList)
                                {
                                    if (q.QuestionnairePresentlyWithActionId != (int)QuestionnairePresentlyWithActionList.Deactivated)
                                    {
                                        QuestionnaireService qService = new QuestionnaireService();
                                        Questionnaire _q = qService.GetByQuestionnaireId(q.QuestionnaireId);
                                        _q.QuestionnairePresentlyWithActionId = (int)QuestionnairePresentlyWithActionList.Deactivated;
                                        _q.QuestionnairePresentlyWithSince = null;
                                        qService.Save(_q);
                                        Log4NetService.LogInfo(c.CompanyName + ": De-Activated, All SQ's Presently With Updated.");
                                    }
                                }
                            }
                            else
                            {
                                if (expired)
                                {
                                    //Added by Ashley Goldstraw to disallow subcontractors from having site approval when their qualification is expired DT225
                                    List<model.CompanySiteCategoryStandard> cscsList = companySiteCategoryStandardService.GetMany(null, i => i.CompanyId == c.CompanyId, null, null);
                                    foreach (model.CompanySiteCategoryStandard cscs in cscsList)
                                    {
                                        cscs.Approved = false;
                                        cscs.ApprovedByUserId = 0;
                                        cscs.ApprovedDate = null;
                                        companySiteCategoryStandardService.Update(cscs);
                                    }

                                    //End added by Ashley Goldstraw
                                    if (c.CompanyStatusId != (int)CompanyStatusList.InActive &&
                                                 c.CompanyStatusId != (int)CompanyStatusList.InActive_No_Requal_Required &&
                                                     c.CompanyStatusId != (int)CompanyStatusList.InActive_SQ_Incomplete &&
                                                     c.CompanyStatusId != (int)CompanyStatusList.Requal_Incomplete &&
                                                     c.CompanyStatusId != (int)CompanyStatusList.Being_Assessed)
                                    {
                                        c.CompanyStatusId = (int)CompanyStatusList.Requal_Incomplete;
                                        c.ModifiedbyUserId = 0;
                                        Log4NetService.LogInfo(c.CompanyName + ": Expired, Company Status Updated.");
                                        cService.Save(c);

                                        //Change Users from 'Contractor' to 'Prequal' if users of 'contractor' exist.
                                        UsersService uService = new UsersService();
                                        TList<Users> uCompany = uService.GetByCompanyId(c.CompanyId);
                                        bool change = false;
                                        int iCount = 0;
                                        while (iCount <= (uCompany.Count - 1))
                                        {
                                            if (uCompany[iCount].RoleId == (int)RoleList.Contractor)
                                            {
                                                uCompany[iCount].RoleId = (int)RoleList.PreQual;
                                                change = true;
                                            }
                                            iCount++;
                                        }
                                        if (change)
                                        {
                                            uCompany.AcceptChanges();
                                            uService.Save(uCompany);
                                        }
                                    }
                                }
                            }
                            //Log4NetService.LogInfo(String.Format("{0}",c.CompanyName)); //DT 375 26/11/15 Cindi Thornton - added by Ashley Goldstraw for testing purposes.
                        }
                    }
                    if (!errorsEncountered)
                    {
                        HealthCheck.Update(true, "UpdateCompanyStatusIfQuestionnaireExpired");
                    }
                    else
                    {
                        throw new Exception("Error Occurred" + Environment.NewLine + companiesInError.ToString()); //DT 375 26/11/15 Cindi Thornton - improve logging
                    }

                }
                catch (Exception ex)
                {
                    HealthCheck.Update(false, "UpdateCompanyStatusIfQuestionnaireExpired", null, ex); //DT 375 20/11/15 Cindi Thornton - added exception
                    //HealthCheck.Update(false, "UpdateCompanyStatusIfQuestionnaireExpired", "Exception: " + ex.Message);
                }
            }

            //DT375 26/11/15 Cindi Thornton - improve logging
            private static void AddQuestionnaireExpiredErrorText(StringBuilder CompaniesInError, string CompanyName, DateTime? MainAssessmentValidTo, int QuestionnaireId, int Status, string ErrorType)
            {
                CompaniesInError.AppendLine(String.Format(
                    "{0} {1} last questionnaire ({2}). Expecting MainAssessmentValidTo not null and Status=Complete. Questionnaire has MainAssessmentValidTo={3}, Status={4}.",
                    CompanyName, 
                    ErrorType,
                    QuestionnaireId,
                    MainAssessmentValidTo.HasValue ? MainAssessmentValidTo.ToString() : "<null>",
                    Enum.GetName(typeof(QuestionnaireStatusList), Status)));
            }

            public static void QuestionnairePresentlyWith()
            {
                QuestionnaireWithLocationApprovalViewLatestService qwlavlService = new QuestionnaireWithLocationApprovalViewLatestService();
                VList<QuestionnaireWithLocationApprovalViewLatest> qwlavlVlist = qwlavlService.GetAll();
                foreach (QuestionnaireWithLocationApprovalViewLatest qwlavl in qwlavlVlist)
                {
                    QuestionnaireService qService = new QuestionnaireService();
                    string status = "";

                    switch (qwlavl.CompanyStatusId)
                    {
                        //In Progress
                        case (int)CompanyStatusList.Being_Assessed:
                            status = "inprogress";
                            break;
                        case (int)CompanyStatusList.Requal_Incomplete:
                            status = "inprogress";
                            break;

                        //Status (Inactive)
                        case (int)CompanyStatusList.InActive_No_Requal_Required:
                            status = "inactive";
                            break;
                        case (int)CompanyStatusList.InActive_SQ_Incomplete:
                            status = "inactive";
                            break;
                        case (int)CompanyStatusList.InActive: //sq complete & not recommended
                            //expirycheck() -elsewhere
                            status = "sqcomplete";
                            break;

                        //Status
                        case (int)CompanyStatusList.Acceptable:
                            //expirycheck() -elsewhere
                            status = "sqcomplete";
                            break;
                        case (int)CompanyStatusList.SubContractor_Not_Recommended:
                            //expirycheck() -elsewhere
                            status = "sqcomplete";
                            break;


                        //Database
                        case (int)CompanyStatusList.Active:
                            //expirycheck() -elsewhere
                            status = "sqcomplete";
                            break;
                        case (int)CompanyStatusList.SubContractor:
                            //expirycheck() -elsewhere
                            status = "sqcomplete";
                            break;
                        default:
                            break;
                    }

                    switch (status)
                    {
                        #region "In Progress"
                        case "inprogress":
                            if (qwlavl.Status == (int)QuestionnaireStatusList.Incomplete) //Incomplete With Procurement, H&S Assessor or Supplier
                            {
                                if (qwlavl.InitialStatus == (int)QuestionnaireStatusList.Incomplete) // With Procurement
                                {
                                    Questionnaire q = qService.GetByQuestionnaireId(qwlavl.QuestionnaireId);
                                    if (q.QuestionnairePresentlyWithActionId != (int)QuestionnairePresentlyWithActionList.ProcurementQuestionnaire)
                                    {
                                        q.QuestionnairePresentlyWithActionId = (int)QuestionnairePresentlyWithActionList.ProcurementQuestionnaire;
                                        q.QuestionnairePresentlyWithSince = q.ModifiedDate;

                                        qService.Save(q);
                                        Log4NetService.LogInfo(qwlavl.CompanyName + ": Procurement Questionnaire Incomplete with Procurement, Presently With Updated.");
                                    }
                                }
                                if (qwlavl.InitialStatus == (int)QuestionnaireStatusList.BeingAssessed) // With H&S Assessors
                                {
                                    Questionnaire q = qService.GetByQuestionnaireId(qwlavl.QuestionnaireId);
                                    if (q.QuestionnairePresentlyWithActionId != (int)QuestionnairePresentlyWithActionList.HSAssessorQuestionnaireProcurement)
                                    {
                                        q.QuestionnairePresentlyWithActionId = (int)QuestionnairePresentlyWithActionList.HSAssessorQuestionnaireProcurement;
                                        q.QuestionnairePresentlyWithSince = q.ModifiedDate;
                                        qService.Save(q);
                                        Log4NetService.LogInfo(qwlavl.CompanyName + ": Procurement Questionnaire Incomplete with H&S Assessor, Presently With Updated.");
                                    }
                                }

                                if (qwlavl.InitialStatus == (int)QuestionnaireStatusList.AssessmentComplete)
                                {
                                    if (qwlavl.MainStatus == (int)QuestionnaireStatusList.Incomplete || //With Supplier
                                        qwlavl.VerificationStatus == (int)QuestionnaireStatusList.Incomplete)
                                    {
                                        QuestionnaireMainAssessmentService qmaService = new QuestionnaireMainAssessmentService();
                                        TList<QuestionnaireMainAssessment> qmaTlist_all;
                                        qmaTlist_all = qmaService.GetByQuestionnaireId(qwlavl.QuestionnaireId);
                                        if (qmaTlist_all != null)
                                        {
                                            if (qmaTlist_all.Count == 22)
                                            {
                                                if (qwlavl.QuestionnairePresentlyWithActionId !=
                                                    (int)QuestionnairePresentlyWithActionList.SupplierQuestionnaireIncomplete)
                                                {
                                                    Questionnaire q = qService.GetByQuestionnaireId(qwlavl.QuestionnaireId);
                                                    q.QuestionnairePresentlyWithActionId = (int)QuestionnairePresentlyWithActionList.SupplierQuestionnaireIncomplete;
                                                    if (q.AssessedDate != null)
                                                    {
                                                        q.QuestionnairePresentlyWithSince = q.AssessedDate;
                                                    }
                                                    else
                                                    {
                                                        if (q.MainAssessmentDate != null)
                                                        {
                                                            q.QuestionnairePresentlyWithSince = q.MainAssessmentDate;
                                                        }
                                                        else
                                                        {
                                                            q.QuestionnairePresentlyWithSince = q.ModifiedDate; //old stuff.. hmm
                                                        }
                                                    }
                                                    qService.Save(q);
                                                    Log4NetService.LogInfo(qwlavl.CompanyName + ": Supplier Questionnaire Incomplete with Supplier (Re-Submitting), Presently With Updated.");
                                                }
                                            }
                                            else
                                            {
                                                if (qwlavl.QuestionnairePresentlyWithActionId !=
                                                    (int)QuestionnairePresentlyWithActionList.SupplierQuestionnaireSupplier)
                                                {
                                                    Questionnaire q = qService.GetByQuestionnaireId(qwlavl.QuestionnaireId);
                                                    q.QuestionnairePresentlyWithActionId = (int)QuestionnairePresentlyWithActionList.SupplierQuestionnaireSupplier;

                                                    if (q.InitialSubmittedDate != null)
                                                    {
                                                        q.QuestionnairePresentlyWithSince = q.InitialSubmittedDate;
                                                    }
                                                    else
                                                    {
                                                        q.QuestionnairePresentlyWithSince = q.InitialModifiedDate;
                                                    }
                                                    qService.Save(q);
                                                    Log4NetService.LogInfo(qwlavl.CompanyName + ": Supplier Questionnaire Incomplete with Supplier, Presently With Updated.");
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            else if (qwlavl.Status == (int)QuestionnaireStatusList.BeingAssessed) //With H&S Assessor
                            {
                                if (qwlavl.QuestionnairePresentlyWithActionId != (int)QuestionnairePresentlyWithActionList.HSAssessorQuestionnaire)
                                {
                                    Questionnaire q = qService.GetByQuestionnaireId(qwlavl.QuestionnaireId);
                                    q.QuestionnairePresentlyWithActionId = (int)QuestionnairePresentlyWithActionList.HSAssessorQuestionnaire;
                                    q.QuestionnairePresentlyWithSince = q.ModifiedDate;
                                    qService.Save(q);
                                    Log4NetService.LogInfo(qwlavl.CompanyName + ": Safety Questionnaire Being Assessed with H&S Assessor, Presently With Updated.");
                                }
                            }
                            else if (qwlavl.Status == (int)QuestionnaireStatusList.AssessmentComplete) //With Procurement - Assign Company Status
                            {
                                if (qwlavl.QuestionnairePresentlyWithActionId != (int)QuestionnairePresentlyWithActionList.ProcurementAssignCompanyStatus)
                                {
                                    Questionnaire q = qService.GetByQuestionnaireId(qwlavl.QuestionnaireId);
                                    q.QuestionnairePresentlyWithActionId = (int)QuestionnairePresentlyWithActionList.ProcurementAssignCompanyStatus;
                                    if (q.ApprovedDate != null)
                                    {
                                        q.QuestionnairePresentlyWithSince = q.ApprovedDate;
                                    }
                                    else
                                    {
                                        q.QuestionnairePresentlyWithSince = q.MainAssessmentDate;
                                    }
                                    qService.Save(q);
                                    Log4NetService.LogInfo(qwlavl.CompanyName + ": Company Status Awaiting Assignment with Procurement, Presently With Updated.");
                                }
                            }
                            break;
                        #endregion
                        #region "In Active"
                        case "inactive":
                            if (qwlavl.QuestionnairePresentlyWithActionId != (int)QuestionnairePresentlyWithActionList.NoActionRequired)
                            {
                                Questionnaire q = qService.GetByQuestionnaireId(qwlavl.QuestionnaireId);
                                q.QuestionnairePresentlyWithActionId = (int)QuestionnairePresentlyWithActionList.NoActionRequired;
                                if (q.QuestionnairePresentlyWithSince != null)
                                {
                                    q.QuestionnairePresentlyWithSince = null;
                                }
                                qService.Save(q);
                                Log4NetService.LogInfo(qwlavl.CompanyName + ": In-Active (SQ Incomplete / No Requal Required) - No Action Required, Presently With Updated.");
                            }
                            break;
                        #endregion
                        case "sqcomplete":
                            //if (qwlavl.QuestionnairePresentlyWithActionId != (int)QuestionnairePresentlyWithActionList.NoActionRequired)
                            //{
                            //    Questionnaire q = qService.GetByQuestionnaireId(qwlavl.QuestionnaireId);
                            //    q.QuestionnairePresentlyWithActionId = (int)QuestionnairePresentlyWithActionList.NoActionRequired;
                            //    qwlavl.QuestionnairePresentlyWithSince = null;
                            //    qService.Save(q);
                            //}
                            //if (qwlavl.QuestionnairePresentlyWithActionId == (int)QuestionnairePresentlyWithActionList.ProcurementExpired ||
                            //    qwlavl.QuestionnairePresentlyWithActionId == (int)QuestionnairePresentlyWithActionList.ProcurementExpiring)
                            //{
                            //    Questionnaire q = qService.GetByQuestionnaireId(qwlavl.QuestionnaireId);
                            //    if (qwlavl.MainAssessmentValidTo != null)
                            //    {
                            //        DateTime dt = Convert.ToDateTime(qwlavl.MainAssessmentValidTo);
                            //        dt = dt.AddDays(-60);
                            //        q.QuestionnairePresentlyWithSince = dt;
                            //        qService.Save(q);
                            //    }
                            //    else
                            //    {
                            //        Log4NetService.LogInfo(qwlavl.CompanyName + ": Error. Assessment Valid To = null");
                            //    }
                            //}
                            break;
                        default:
                            break;
                    }

                    //With CSMS - No Users!
                    UsersService uService = new UsersService();
                    TList<Users> uTlist = uService.GetByCompanyId(qwlavl.CompanyId);
                    if (uTlist.Count == 0 && qwlavl.InitialStatus == (int)QuestionnaireStatusList.AssessmentComplete)
                    {
                        CompaniesService cService = new CompaniesService();
                        Companies c = cService.GetByCompanyId(qwlavl.CompanyId);
                        if (c.Deactivated != true)
                        {
                            if (status != "inactive")
                            {
                                if (qwlavl.QuestionnairePresentlyWithActionId != (int)QuestionnairePresentlyWithActionList.CsmsCreateCompanyLogins)
                                {
                                    Questionnaire q = qService.GetByQuestionnaireId(qwlavl.QuestionnaireId);
                                    q.QuestionnairePresentlyWithActionId = (int)QuestionnairePresentlyWithActionList.CsmsCreateCompanyLogins;
                                    DateTime? lastModified = q.InitialSubmittedDate;
                                    if (q.ModifiedDate > lastModified) lastModified = q.ModifiedDate;
                                    if (q.MainAssessmentDate > lastModified) lastModified = q.MainAssessmentDate;

                                    qwlavl.QuestionnairePresentlyWithSince = lastModified;
                                    qService.Save(q);
                                    Log4NetService.LogInfo(qwlavl.CompanyName + ": No Users for Company (Not In-Active/De-Activated), Presently With Updated.");
                                }
                            }
                        }
                    }

                    //expiry
                    if (status != "inactive" && qwlavl.CompanyStatusId != (int)CompanyStatusList.Being_Assessed)
                    {
                        if (qwlavl.MainAssessmentValidTo != null)
                        {
                            DateTime dtExpiry = Convert.ToDateTime(qwlavl.MainAssessmentValidTo);
                            TimeSpan ts = Convert.ToDateTime(dtExpiry) - DateTime.Now;
                            if (ts.Days <= 60)
                            {
                                if (ts.Days > 0)
                                {
                                    //expiring
                                    Questionnaire q = qService.GetByQuestionnaireId(qwlavl.QuestionnaireId);
                                    if (q.QuestionnairePresentlyWithActionId != (int)QuestionnairePresentlyWithActionList.ProcurementExpiring)
                                    {
                                        q.QuestionnairePresentlyWithActionId = (int)QuestionnairePresentlyWithActionList.ProcurementExpiring;

                                        if (qwlavl.MainAssessmentValidTo != null)
                                        {
                                            DateTime dt = Convert.ToDateTime(qwlavl.MainAssessmentValidTo);
                                            dt = dt.AddDays(-60);
                                            q.QuestionnairePresentlyWithSince = dt;
                                        }
                                        else
                                        {
                                            q.QuestionnairePresentlyWithSince = null;
                                        }

                                        qService.Save(q);
                                        Log4NetService.LogInfo(qwlavl.CompanyName + ": Expiring, Presently With Updated.");
                                    }
                                }
                                else
                                {
                                    Questionnaire q = qService.GetByQuestionnaireId(qwlavl.QuestionnaireId);
                                    if (q.QuestionnairePresentlyWithActionId != (int)QuestionnairePresentlyWithActionList.ProcurementExpired)
                                    {
                                        q.QuestionnairePresentlyWithActionId = (int)QuestionnairePresentlyWithActionList.ProcurementExpired;
                                        if (qwlavl.MainAssessmentValidTo != null)
                                        {
                                            DateTime dt = Convert.ToDateTime(qwlavl.MainAssessmentValidTo);
                                            dt = dt.AddDays(-60);
                                            q.QuestionnairePresentlyWithSince = dt;
                                        }
                                        else
                                        {
                                            q.QuestionnairePresentlyWithSince = null;
                                        }
                                        qService.Save(q);
                                        Log4NetService.LogInfo(qwlavl.CompanyName + ": Expired, Presently With Updated.");
                                    }
                                }
                            }
                        }
                    }

                }
            }
            public static void PastQuestionnairePresentlyWith(string sqldb) //this should only need to be run once. website should handle this aftewards internally.
            {
                DataTable dt = new DataTable();

                using (SqlConnection cn = new SqlConnection(sqldb))
                {
                    cn.Open();
                    SqlDataAdapter da = new SqlDataAdapter("SELECT Q.QuestionnaireId FROM Questionnaire As Q WHERE (SELECT COUNT(*) FROM Questionnaire WHERE Questionnaire.CompanyId = Q.CompanyId) > 1 AND QuestionnairePresentlyWithActionId IS NULL", cn);
                    da.Fill(dt);
                    cn.Close();
                }

                foreach (DataRow dr in dt.Rows)
                {
                    Log4NetService.LogInfo(dr[0].ToString());
                    int QuestionnaireId = Convert.ToInt32(dr[0].ToString());
                    QuestionnaireService qService = new QuestionnaireService();
                    Questionnaire q = qService.GetByQuestionnaireId(QuestionnaireId);
                    if (q.QuestionnairePresentlyWithActionId == null)
                    {
                        CompaniesService cService = new CompaniesService();
                        Companies c = cService.GetByCompanyId(q.CompanyId);
                        q.QuestionnairePresentlyWithActionId = (int)QuestionnairePresentlyWithActionList.NoActionRequired;
                        q.QuestionnairePresentlyWithSince = null;
                        qService.Save(q);
                        Log4NetService.LogInfo(c.CompanyName + ": Past Questionnaire - No Action Required, Presently With Updated.");
                    }
                }
            }

            public static void QuestionnairePresentlyWithMetrics()
            {
                QuestionnaireReportProgressService qrpService = new QuestionnaireReportProgressService();
                VList<QuestionnaireReportProgress> qrpList = qrpService.GetAll();

                SitesService sService = new SitesService();
                TList<Sites> sList = sService.GetAll();
                sList = sList.FindAll(SitesColumn.IsVisible, true);

                QuestionnairePresentlyWithActionService qpwaService = new QuestionnairePresentlyWithActionService();
                TList<QuestionnairePresentlyWithAction> qpwaList = qpwaService.GetAll();

                DateTime dtNow = DateTime.Now;
                int Year = dtNow.Year;
                int WeekNo = DateTimeUtilities.GetWeekNumber(dtNow);

                QuestionnairePresentlyWithMetricService qpwmService = new QuestionnairePresentlyWithMetricService();
                TList<QuestionnairePresentlyWithMetric> qpwmList_insert = new TList<QuestionnairePresentlyWithMetric>();
                TList<QuestionnairePresentlyWithMetric> qpwmList_update = new TList<QuestionnairePresentlyWithMetric>();

                foreach (QuestionnairePresentlyWithAction qpwa in qpwaList)
                {
                    TList<QuestionnairePresentlyWithMetric> qpwmList = qpwmService.GetByYearWeekNoQuestionnairePresentlyWithActionId(Year, WeekNo, qpwa.QuestionnairePresentlyWithActionId);

                    if (qpwa.QuestionnairePresentlyWithUserId == (int)QuestionnairePresentlyWithUsersList.HSAssessor ||
                        qpwa.QuestionnairePresentlyWithUserId == (int)QuestionnairePresentlyWithUsersList.Procurement ||
                        qpwa.QuestionnairePresentlyWithUserId == (int)QuestionnairePresentlyWithUsersList.Supplier)
                    {
                        foreach (Sites s in sList)
                        {

                            QuestionnairePresentlyWithMetric qpwm = qpwmList.Find(QuestionnairePresentlyWithMetricColumn.SiteId, s.SiteId);
                            bool insert = false;
                            if (qpwm == null)
                            {
                                qpwm = new QuestionnairePresentlyWithMetric();
                                insert = true;
                            }

                            VList<QuestionnaireReportProgress> qrpList_filtered = qrpList.FindAll(QuestionnaireReportProgressColumn.QuestionnairePresentlyWithActionId,
                                                                                    qpwa.QuestionnairePresentlyWithActionId);

                            //if (qpwa.QuestionnairePresentlyWithUserId == (int)QuestionnairePresentlyWithUsersList.HSAssessor)
                            //{
                            //    qrpList_filtered = qrpList_filtered.FindAll(QuestionnaireReportProgressColumn.SafetyAssessorSite,
                            //                                                        s.SiteName);
                            //}
                            //else if (qpwa.QuestionnairePresentlyWithUserId == (int)QuestionnairePresentlyWithUsersList.Procurement ||
                            //        qpwa.QuestionnairePresentlyWithUserId == (int)QuestionnairePresentlyWithUsersList.Supplier)
                            //{
                            //    qrpList_filtered = qrpList_filtered.FindAll(QuestionnaireReportProgressColumn.ProcurementSite,
                            //                                                        s.SiteName);
                            //}

                            qrpList_filtered = qrpList_filtered.FindAll(QuestionnaireReportProgressColumn.ProcurementSite,
                                                                                    s.SiteName);


                            qpwm.Date = dtNow;
                            qpwm.Year = Year;
                            qpwm.WeekNo = WeekNo;
                            qpwm.SiteId = s.SiteId;
                            qpwm.QuestionnairePresentlyWithActionId = qpwa.QuestionnairePresentlyWithActionId;
                            qpwm.QuestionnairePresentlyWithUserId = qpwa.QuestionnairePresentlyWithUserId;
                            qpwm.Metric = qrpList_filtered.Count;

                            if (insert)
                                qpwmList_insert.Add(qpwm);
                            else
                                qpwmList_update.Add(qpwm);
                        }
                    }
                    else
                    {
                        QuestionnairePresentlyWithMetric qpwm = qpwmList.Find(QuestionnairePresentlyWithMetricColumn.SiteId, 23);
                        bool insert = false;
                        if (qpwm == null)
                        {
                            qpwm = new QuestionnairePresentlyWithMetric();
                            insert = true;
                        }

                        VList<QuestionnaireReportProgress> qrpList_filtered = qrpList.FindAll(QuestionnaireReportProgressColumn.QuestionnairePresentlyWithActionId,
                                                                                qpwa.QuestionnairePresentlyWithActionId);

                        qpwm.Date = dtNow;
                        qpwm.Year = Year;
                        qpwm.WeekNo = WeekNo;
                        qpwm.SiteId = 23; //AllSites
                        qpwm.QuestionnairePresentlyWithActionId = qpwa.QuestionnairePresentlyWithActionId;
                        qpwm.QuestionnairePresentlyWithUserId = qpwa.QuestionnairePresentlyWithUserId;
                        qpwm.Metric = qrpList_filtered.Count;

                        if (insert)
                            qpwmList_insert.Add(qpwm);
                        else
                            qpwmList_update.Add(qpwm);
                    }
                }


                if (qpwmList_insert.Count > 0 || qpwmList_update.Count > 0)
                {
                    TransactionManager transactionManager = null;
                    try
                    {
                        transactionManager = ConnectionScope.ValidateOrCreateTransaction(true);

                        if (qpwmList_insert.Count > 0)
                            DataRepository.QuestionnairePresentlyWithMetricProvider.Insert(transactionManager, qpwmList_insert);

                        if (qpwmList_update.Count > 0)
                            DataRepository.QuestionnairePresentlyWithMetricProvider.Update(transactionManager, qpwmList_update);

                        transactionManager.Commit();
                    }
                    catch (Exception ex)
                    {
                        if ((transactionManager != null) && (transactionManager.IsOpen))
                            transactionManager.Rollback();
                        throw ex;
                    }
                }
            }
        }
    }
}