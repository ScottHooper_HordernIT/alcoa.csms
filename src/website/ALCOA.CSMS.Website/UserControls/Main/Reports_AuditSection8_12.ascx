﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="UserControls_Reports_AuditSection8_12" Codebehind="Reports_AuditSection8_12.ascx.cs" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<table border="0" cellpadding="2" cellspacing="0" width="100%">
    <tr>
        <td class="pageName" colspan="3" style="height: 43px">
        <span class="bodycopy"><span class="title">Reports</span><br><span class=date>
            Audit - S8.12</span>
            <br />
            <img src="images/grfc_dottedline.gif" width="24" height="1">&nbsp;</span>
                </td>
    </tr>
    <tr>
</table>
<br />
<table>
    <tr>
        <td style="width: 537px; text-align: right; height: 20px;">
            <strong><span style="font-size: 11pt">Please enter the following information:</span></strong></td>
        <td style="width: 225px; height: 20px;">
        </td>
    </tr>
    <tr>
        <td style="width: 537px; text-align: right">
            <span style="font-size: 11pt">
            Location:</span></td>
        <td style="width: 225px">
            <dxe:ASPxComboBox ID="cbLocation" runat="server" 
                CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
                CssPostfix="Office2003Blue" DataSourceID="ConfigSa812SitesDataSource" 
                ValueType="System.Int32" TextField="SiteName" ValueField="SiteId" 
                SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                </LoadingPanelImage>
                <ButtonStyle Width="13px">
                </ButtonStyle>
                <ValidationSettings ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True" ValidateOnLeave="False"
                    ValidationGroup="Create">
                    <RequiredField IsRequired="True" />
                </ValidationSettings>
            </dxe:ASPxComboBox>
        </td>
    </tr>
    <tr>
        <td style="width: 537px; text-align: right">
            <span style="font-size: 11pt">Number of contractors interviewed who identified the Contractor
                Services Supervisor and Environmental Health &amp; Safety Consultant:</span></td>
        <td style="width: 225px">
            <dxe:ASPxTextBox ID="tbNoInterviewed" runat="server" Width="170px" 
                CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
                CssPostfix="Office2003Blue" 
                SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                <ValidationSettings ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True" ValidationGroup="Create">
                    <RequiredField IsRequired="True" />
                </ValidationSettings>
            </dxe:ASPxTextBox>
        </td>
    </tr>
    <tr>
        <td style="width: 537px">
        </td>
        <td style="width: 225px">
            <dxe:ASPxButton ID="ASPxButton1" runat="server" Text="Create Audit Report" 
                Width="151px" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
                CssPostfix="Office2003Blue" OnClick="ASPxButton1_Click" 
                ValidationGroup="Create" 
                SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
            </dxe:ASPxButton>
        </td>
    </tr>
</table>

<data:ConfigSa812SitesDataSource ID="ConfigSa812SitesDataSource" runat="server" Filter="IsVisible = True" Sort="SiteName ASC"></data:ConfigSa812SitesDataSource> 

<data:EntityDataSource ID="UsersEhsConsultantsDataSource2" runat="server" EntityKeyTypeName="System.Int32"
    EntityTypeName="KaiZen.CSMS.UsersEhsConsultants, KaiZen.CSMS" Filter="Enabled = True"
    ProviderName="UsersEhsConsultantsProvider" SelectMethod="GetAll" Sort="UserFullNameLogon ASC">
</data:EntityDataSource>
&nbsp;&nbsp;
<br />
<asp:Panel ID="Panel1" runat="server" Visible="False" Width="900px">
    <asp:PlaceHolder ID="PlaceHolder1" runat="server"></asp:PlaceHolder>
</asp:Panel>
<br />
<br />
<br />
    