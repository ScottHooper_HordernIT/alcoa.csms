﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/popup.master" AutoEventWireup="true" Inherits="PopUps_SafetyPlans_Compare" Codebehind="SafetyPlans_Compare.aspx.cs" %>

<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dxw" %>

<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dxtc" %>

<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dxp" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .ci {COLOR:black; BACKGROUND-COLOR:#80FF80}
        .cd {COLOR:black; BACKGROUND-COLOR:#FF8080}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div style="text-align: left">
        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
            <tr>
                <td style="height: 40px; text-align: left">
                    <dxe:ASPxCheckBox ID="cbDifference" runat="server" Text="Show differences only" 
                        AutoPostBack="True" OnCheckedChanged="cbDifference_CheckedChanged" 
                        CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
                        CssPostfix="Office2003Blue" Visible="false"
                        SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                    </dxe:ASPxCheckBox>
                </td>
                <td style="width: 8px; height: 40px">
                    <dxe:ASPxButton ID="ASPxButton2" runat="server" AutoPostBack="False" CausesValidation="False"
                        CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue"
                        Text="Print" UseSubmitBehavior="False" 
                        SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                        <ClientSideEvents Click="function(s, e) {
	javascript:window.print();
}" />
                    </dxe:ASPxButton>
                </td>
                <td style="width: 8px; height: 40px;">
                    <dxe:ASPxButton ID="ASPxButton1" runat="server" AutoPostBack="False" CausesValidation="False"
                        CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue"
                        Text="Close" UseSubmitBehavior="False" 
                        SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                        <ClientSideEvents Click="function(s, e) {
	javascript:window.close();
}" />
                    </dxe:ASPxButton>
                </td>
            </tr>
            <tr>
                <td colspan="3" style="height: 13px; text-align: center">
                    <asp:Label ID="lblCompanyName" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label></td>
            </tr>
            <tr>
                <td colspan="3" style="text-align: center; height: 7px;">
                    &nbsp;<asp:Label runat="server" ForeColor="Red" ID="lblError"></asp:Label></td>
            </tr>
        </table>
        &nbsp;&nbsp;&nbsp;
        <dxtc:ASPxPageControl ID="ASPxPageControl1" runat="server" ActiveTabIndex="0" 
            CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
            CssPostfix="Office2003Blue" Width="100%">
            <TabPages>
                <dxtc:TabPage Name="SafetyPlan" Text="Safety Plan">
                    <ContentCollection>
                        <dxw:contentcontrol ID="Contentcontrol1" runat="server"><asp:PlaceHolder runat="server" ID="phSafetyPlan"></asp:PlaceHolder>
 </dxw:contentcontrol>
                    </ContentCollection>
                </dxtc:TabPage>
                <dxtc:TabPage Name="Questionnaire" Text="Questionnaire">
                    <ContentCollection>
                        <dxw:contentcontrol ID="Contentcontrol2" runat="server"><asp:PlaceHolder runat="server" ID="phQuestionnaire"></asp:PlaceHolder>
</dxw:contentcontrol>
                    </ContentCollection>
                </dxtc:TabPage>
                <dxtc:TabPage Name="Reviewal" Text="Reviewal">
                    <ContentCollection>
                        <dxw:contentcontrol ID="Contentcontrol3" runat="server"><asp:PlaceHolder runat="server" ID="phReviewal"></asp:PlaceHolder>
                        </dxw:contentcontrol>
                    </ContentCollection>
                </dxtc:TabPage>
            </TabPages>
            <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
            </LoadingPanelImage>
            <LoadingPanelStyle ImageSpacing="6px">
            </LoadingPanelStyle>
            <ContentStyle>
                <Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px" />
            </ContentStyle>
        </dxtc:ASPxPageControl>
    
    </div>
</asp:Content>

