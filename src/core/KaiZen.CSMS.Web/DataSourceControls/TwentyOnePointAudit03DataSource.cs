﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.TwentyOnePointAudit03Provider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(TwentyOnePointAudit03DataSourceDesigner))]
	public class TwentyOnePointAudit03DataSource : ProviderDataSource<TwentyOnePointAudit03, TwentyOnePointAudit03Key>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit03DataSource class.
		/// </summary>
		public TwentyOnePointAudit03DataSource() : base(new TwentyOnePointAudit03Service())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the TwentyOnePointAudit03DataSourceView used by the TwentyOnePointAudit03DataSource.
		/// </summary>
		protected TwentyOnePointAudit03DataSourceView TwentyOnePointAudit03View
		{
			get { return ( View as TwentyOnePointAudit03DataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the TwentyOnePointAudit03DataSource control invokes to retrieve data.
		/// </summary>
		public TwentyOnePointAudit03SelectMethod SelectMethod
		{
			get
			{
				TwentyOnePointAudit03SelectMethod selectMethod = TwentyOnePointAudit03SelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (TwentyOnePointAudit03SelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the TwentyOnePointAudit03DataSourceView class that is to be
		/// used by the TwentyOnePointAudit03DataSource.
		/// </summary>
		/// <returns>An instance of the TwentyOnePointAudit03DataSourceView class.</returns>
		protected override BaseDataSourceView<TwentyOnePointAudit03, TwentyOnePointAudit03Key> GetNewDataSourceView()
		{
			return new TwentyOnePointAudit03DataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the TwentyOnePointAudit03DataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class TwentyOnePointAudit03DataSourceView : ProviderDataSourceView<TwentyOnePointAudit03, TwentyOnePointAudit03Key>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit03DataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the TwentyOnePointAudit03DataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public TwentyOnePointAudit03DataSourceView(TwentyOnePointAudit03DataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal TwentyOnePointAudit03DataSource TwentyOnePointAudit03Owner
		{
			get { return Owner as TwentyOnePointAudit03DataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal TwentyOnePointAudit03SelectMethod SelectMethod
		{
			get { return TwentyOnePointAudit03Owner.SelectMethod; }
			set { TwentyOnePointAudit03Owner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal TwentyOnePointAudit03Service TwentyOnePointAudit03Provider
		{
			get { return Provider as TwentyOnePointAudit03Service; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<TwentyOnePointAudit03> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<TwentyOnePointAudit03> results = null;
			TwentyOnePointAudit03 item;
			count = 0;
			
			System.Int32 _id;

			switch ( SelectMethod )
			{
				case TwentyOnePointAudit03SelectMethod.Get:
					TwentyOnePointAudit03Key entityKey  = new TwentyOnePointAudit03Key();
					entityKey.Load(values);
					item = TwentyOnePointAudit03Provider.Get(entityKey);
					results = new TList<TwentyOnePointAudit03>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case TwentyOnePointAudit03SelectMethod.GetAll:
                    results = TwentyOnePointAudit03Provider.GetAll(StartIndex, PageSize, out count);
                    break;
				case TwentyOnePointAudit03SelectMethod.GetPaged:
					results = TwentyOnePointAudit03Provider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case TwentyOnePointAudit03SelectMethod.Find:
					if ( FilterParameters != null )
						results = TwentyOnePointAudit03Provider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = TwentyOnePointAudit03Provider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case TwentyOnePointAudit03SelectMethod.GetById:
					_id = ( values["Id"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["Id"], typeof(System.Int32)) : (int)0;
					item = TwentyOnePointAudit03Provider.GetById(_id);
					results = new TList<TwentyOnePointAudit03>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				// FK
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == TwentyOnePointAudit03SelectMethod.Get || SelectMethod == TwentyOnePointAudit03SelectMethod.GetById )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				TwentyOnePointAudit03 entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					TwentyOnePointAudit03Provider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<TwentyOnePointAudit03> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			TwentyOnePointAudit03Provider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region TwentyOnePointAudit03DataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the TwentyOnePointAudit03DataSource class.
	/// </summary>
	public class TwentyOnePointAudit03DataSourceDesigner : ProviderDataSourceDesigner<TwentyOnePointAudit03, TwentyOnePointAudit03Key>
	{
		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit03DataSourceDesigner class.
		/// </summary>
		public TwentyOnePointAudit03DataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public TwentyOnePointAudit03SelectMethod SelectMethod
		{
			get { return ((TwentyOnePointAudit03DataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new TwentyOnePointAudit03DataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region TwentyOnePointAudit03DataSourceActionList

	/// <summary>
	/// Supports the TwentyOnePointAudit03DataSourceDesigner class.
	/// </summary>
	internal class TwentyOnePointAudit03DataSourceActionList : DesignerActionList
	{
		private TwentyOnePointAudit03DataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit03DataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public TwentyOnePointAudit03DataSourceActionList(TwentyOnePointAudit03DataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public TwentyOnePointAudit03SelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion TwentyOnePointAudit03DataSourceActionList
	
	#endregion TwentyOnePointAudit03DataSourceDesigner
	
	#region TwentyOnePointAudit03SelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the TwentyOnePointAudit03DataSource.SelectMethod property.
	/// </summary>
	public enum TwentyOnePointAudit03SelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetById method.
		/// </summary>
		GetById
	}
	
	#endregion TwentyOnePointAudit03SelectMethod

	#region TwentyOnePointAudit03Filter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit03"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAudit03Filter : SqlFilter<TwentyOnePointAudit03Column>
	{
	}
	
	#endregion TwentyOnePointAudit03Filter

	#region TwentyOnePointAudit03ExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit03"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAudit03ExpressionBuilder : SqlExpressionBuilder<TwentyOnePointAudit03Column>
	{
	}
	
	#endregion TwentyOnePointAudit03ExpressionBuilder	

	#region TwentyOnePointAudit03Property
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;TwentyOnePointAudit03ChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit03"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAudit03Property : ChildEntityProperty<TwentyOnePointAudit03ChildEntityTypes>
	{
	}
	
	#endregion TwentyOnePointAudit03Property
}

