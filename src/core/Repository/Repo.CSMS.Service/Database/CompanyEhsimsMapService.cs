﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface ICompanyEhsimsMapService
    {
        CompaniesEhsimsMap Get(Expression<Func<CompaniesEhsimsMap, bool>> where, List<Expression<Func<CompaniesEhsimsMap, object>>> includes);
        List<CompaniesEhsimsMap> GetMany(int? top, Expression<Func<CompaniesEhsimsMap, bool>> where, Func<IQueryable<CompaniesEhsimsMap>, IOrderedQueryable<CompaniesEhsimsMap>> orderBy, List<Expression<Func<CompaniesEhsimsMap, object>>> includes);
        List<CompaniesEhsimsMap> GetManyPaged(int pageNumber, int pageSize, Expression<Func<CompaniesEhsimsMap, bool>> where, Func<IQueryable<CompaniesEhsimsMap>, IOrderedQueryable<CompaniesEhsimsMap>> orderBy, List<Expression<Func<CompaniesEhsimsMap, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<CompaniesEhsimsMap, bool>> where, List<Expression<Func<CompaniesEhsimsMap, object>>> includes);
        void Insert(CompaniesEhsimsMap item, bool saveChanges = true);
        void Update(CompaniesEhsimsMap item, bool saveChanges = true);
        void Delete(CompaniesEhsimsMap item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class CompanyEhsimsMapService : ICompanyEhsimsMapService
    {
        private readonly IRepository<CompaniesEhsimsMap> _repository;

        public CompanyEhsimsMapService(IRepository<CompaniesEhsimsMap> repository)
        {
            this._repository = repository;
        }

        public CompaniesEhsimsMap Get(Expression<Func<CompaniesEhsimsMap, bool>> where, List<Expression<Func<CompaniesEhsimsMap, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<CompaniesEhsimsMap> GetMany(int? top, Expression<Func<CompaniesEhsimsMap, bool>> where, Func<IQueryable<CompaniesEhsimsMap>, IOrderedQueryable<CompaniesEhsimsMap>> orderBy, List<Expression<Func<CompaniesEhsimsMap, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<CompaniesEhsimsMap> GetManyPaged(int pageNumber, int pageSize, Expression<Func<CompaniesEhsimsMap, bool>> where, Func<IQueryable<CompaniesEhsimsMap>, IOrderedQueryable<CompaniesEhsimsMap>> orderBy, List<Expression<Func<CompaniesEhsimsMap, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<CompaniesEhsimsMap, bool>> where, List<Expression<Func<CompaniesEhsimsMap, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(CompaniesEhsimsMap item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("CompaniesEhsimsMap");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(CompaniesEhsimsMap item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("CompaniesEhsimsMap");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(CompaniesEhsimsMap item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("CompaniesEhsimsMap");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
