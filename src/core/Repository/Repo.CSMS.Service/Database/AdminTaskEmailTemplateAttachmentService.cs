﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface IAdminTaskEmailTemplateAttachmentService
    {
        AdminTaskEmailTemplateAttachment Get(Expression<Func<AdminTaskEmailTemplateAttachment, bool>> where, List<Expression<Func<AdminTaskEmailTemplateAttachment, object>>> includes);
        List<AdminTaskEmailTemplateAttachment> GetMany(int? top, Expression<Func<AdminTaskEmailTemplateAttachment, bool>> where, Func<IQueryable<AdminTaskEmailTemplateAttachment>, IOrderedQueryable<AdminTaskEmailTemplateAttachment>> orderBy, List<Expression<Func<AdminTaskEmailTemplateAttachment, object>>> includes);
        List<AdminTaskEmailTemplateAttachment> GetManyPaged(int pageNumber, int pageSize, Expression<Func<AdminTaskEmailTemplateAttachment, bool>> where, Func<IQueryable<AdminTaskEmailTemplateAttachment>, IOrderedQueryable<AdminTaskEmailTemplateAttachment>> orderBy, List<Expression<Func<AdminTaskEmailTemplateAttachment, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<AdminTaskEmailTemplateAttachment, bool>> where, List<Expression<Func<AdminTaskEmailTemplateAttachment, object>>> includes);
        void Insert(AdminTaskEmailTemplateAttachment item, bool saveChanges = true);
        void Update(AdminTaskEmailTemplateAttachment item, bool saveChanges = true);
        void Delete(AdminTaskEmailTemplateAttachment item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class AdminTaskEmailTemplateAttachmentService : IAdminTaskEmailTemplateAttachmentService
    {
        private readonly IRepository<AdminTaskEmailTemplateAttachment> _repository;

        public AdminTaskEmailTemplateAttachmentService(IRepository<AdminTaskEmailTemplateAttachment> repository)
        {
            this._repository = repository;
        }

        public AdminTaskEmailTemplateAttachment Get(Expression<Func<AdminTaskEmailTemplateAttachment, bool>> where, List<Expression<Func<AdminTaskEmailTemplateAttachment, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<AdminTaskEmailTemplateAttachment> GetMany(int? top, Expression<Func<AdminTaskEmailTemplateAttachment, bool>> where, Func<IQueryable<AdminTaskEmailTemplateAttachment>, IOrderedQueryable<AdminTaskEmailTemplateAttachment>> orderBy, List<Expression<Func<AdminTaskEmailTemplateAttachment, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<AdminTaskEmailTemplateAttachment> GetManyPaged(int pageNumber, int pageSize, Expression<Func<AdminTaskEmailTemplateAttachment, bool>> where, Func<IQueryable<AdminTaskEmailTemplateAttachment>, IOrderedQueryable<AdminTaskEmailTemplateAttachment>> orderBy, List<Expression<Func<AdminTaskEmailTemplateAttachment, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<AdminTaskEmailTemplateAttachment, bool>> where, List<Expression<Func<AdminTaskEmailTemplateAttachment, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(AdminTaskEmailTemplateAttachment item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("AdminTaskEmailTemplateAttachment");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(AdminTaskEmailTemplateAttachment item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("AdminTaskEmailTemplateAttachment");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(AdminTaskEmailTemplateAttachment item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("AdminTaskEmailTemplateAttachment");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
