//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Repo.CSMS.DAL.EntityModels
{
    using System;
    using System.Collections.Generic;
    
    public partial class TwentyOnePointAudit_14
    {
        public TwentyOnePointAudit_14()
        {
            this.TwentyOnePointAudits = new HashSet<TwentyOnePointAudit>();
        }
    
        public int ID { get; set; }
        public Nullable<int> Achieved14a { get; set; }
        public Nullable<int> Achieved14b { get; set; }
        public Nullable<int> Achieved14c { get; set; }
        public Nullable<int> Achieved14d { get; set; }
        public Nullable<int> Achieved14e { get; set; }
        public Nullable<int> Achieved14f { get; set; }
        public Nullable<int> Achieved14g { get; set; }
        public Nullable<int> Achieved14h { get; set; }
        public string Observation14a { get; set; }
        public string Observation14b { get; set; }
        public string Observation14c { get; set; }
        public string Observation14d { get; set; }
        public string Observation14e { get; set; }
        public string Observation14f { get; set; }
        public string Observation14g { get; set; }
        public string Observation14h { get; set; }
        public Nullable<int> TotalScore { get; set; }
    
        public virtual ICollection<TwentyOnePointAudit> TwentyOnePointAudits { get; set; }
    }
}
