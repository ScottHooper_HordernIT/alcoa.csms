﻿#region Using directives

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="KpiListProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract class KpiListProviderBaseCore : EntityViewProviderBase<KpiList>
	{
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions
		
		/*
		///<summary>
		/// Fill an VList&lt;KpiList&gt; From a DataSet
		///</summary>
		/// <param name="dataSet">the DataSet</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList&lt;KpiList&gt;"/></returns>
		protected static VList&lt;KpiList&gt; Fill(DataSet dataSet, VList<KpiList> rows, int start, int pagelen)
		{
			if (dataSet.Tables.Count == 1)
			{
				return Fill(dataSet.Tables[0], rows, start, pagelen);
			}
			else
			{
				return new VList<KpiList>();
			}	
		}
		
		
		///<summary>
		/// Fill an VList&lt;KpiList&gt; From a DataTable
		///</summary>
		/// <param name="dataTable">the DataTable that hold the data.</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList<KpiList>"/></returns>
		protected static VList&lt;KpiList&gt; Fill(DataTable dataTable, VList<KpiList> rows, int start, int pagelen)
		{
			int recordnum = 0;
			
			System.Collections.IEnumerator dataRows =  dataTable.Rows.GetEnumerator();
			
			while (dataRows.MoveNext() && (pagelen != 0))
			{
				if(recordnum >= start)
				{
					DataRow row = (DataRow)dataRows.Current;
				
					KpiList c = new KpiList();
					c.KpiId = (Convert.IsDBNull(row["KpiId"]))?(int)0:(System.Int32)row["KpiId"];
					c.SiteId = (Convert.IsDBNull(row["SiteId"]))?(int)0:(System.Int32)row["SiteId"];
					c.CompanyId = (Convert.IsDBNull(row["CompanyId"]))?(int)0:(System.Int32)row["CompanyId"];
					c.CompanyName = (Convert.IsDBNull(row["CompanyName"]))?string.Empty:(System.String)row["CompanyName"];
					c.SiteName = (Convert.IsDBNull(row["SiteName"]))?string.Empty:(System.String)row["SiteName"];
					c.KpiDateTime = (Convert.IsDBNull(row["kpiDateTime"]))?DateTime.MinValue:(System.DateTime)row["kpiDateTime"];
					c.CreatedbyUserId = (Convert.IsDBNull(row["CreatedbyUserId"]))?(int)0:(System.Int32)row["CreatedbyUserId"];
					c.UserLogon = (Convert.IsDBNull(row["UserLogon"]))?string.Empty:(System.String)row["UserLogon"];
					c.LastName = (Convert.IsDBNull(row["LastName"]))?string.Empty:(System.String)row["LastName"];
					c.FirstName = (Convert.IsDBNull(row["FirstName"]))?string.Empty:(System.String)row["FirstName"];
					c.ModifiedbyUserId = (Convert.IsDBNull(row["ModifiedbyUserId"]))?(int)0:(System.Int32)row["ModifiedbyUserId"];
					c.ModifiedbyUserLogon = (Convert.IsDBNull(row["ModifiedbyUserLogon"]))?string.Empty:(System.String)row["ModifiedbyUserLogon"];
					c.ModifiedbyLastName = (Convert.IsDBNull(row["ModifiedbyLastName"]))?string.Empty:(System.String)row["ModifiedbyLastName"];
					c.ModifiedbyFirstName = (Convert.IsDBNull(row["ModifiedbyFirstName"]))?string.Empty:(System.String)row["ModifiedbyFirstName"];
					c.DateAdded = (Convert.IsDBNull(row["DateAdded"]))?DateTime.MinValue:(System.DateTime)row["DateAdded"];
					c.Datemodified = (Convert.IsDBNull(row["Datemodified"]))?DateTime.MinValue:(System.DateTime)row["Datemodified"];
					c.AcceptChanges();
					rows.Add(c);
					pagelen -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		*/	
						
		///<summary>
		/// Fill an <see cref="VList&lt;KpiList&gt;"/> From a DataReader.
		///</summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pageLength">number of row.</param>
		///<returns>a <see cref="VList&lt;KpiList&gt;"/></returns>
		protected VList<KpiList> Fill(IDataReader reader, VList<KpiList> rows, int start, int pageLength)
		{
			int recordnum = 0;
			while (reader.Read() && (pageLength != 0))
			{
				if(recordnum >= start)
				{
					KpiList entity = null;
					if (DataRepository.Provider.UseEntityFactory)
					{
						entity = EntityManager.CreateViewEntity<KpiList>("KpiList",  DataRepository.Provider.EntityCreationalFactoryType); 
					}
					else
					{
						entity = new KpiList();
					}
					
					entity.SuppressEntityEvents = true;

					entity.KpiId = (System.Int32)reader[((int)KpiListColumn.KpiId)];
					//entity.KpiId = (Convert.IsDBNull(reader["KpiId"]))?(int)0:(System.Int32)reader["KpiId"];
					entity.SiteId = (System.Int32)reader[((int)KpiListColumn.SiteId)];
					//entity.SiteId = (Convert.IsDBNull(reader["SiteId"]))?(int)0:(System.Int32)reader["SiteId"];
					entity.CompanyId = (System.Int32)reader[((int)KpiListColumn.CompanyId)];
					//entity.CompanyId = (Convert.IsDBNull(reader["CompanyId"]))?(int)0:(System.Int32)reader["CompanyId"];
					entity.CompanyName = (System.String)reader[((int)KpiListColumn.CompanyName)];
					//entity.CompanyName = (Convert.IsDBNull(reader["CompanyName"]))?string.Empty:(System.String)reader["CompanyName"];
					entity.SiteName = (System.String)reader[((int)KpiListColumn.SiteName)];
					//entity.SiteName = (Convert.IsDBNull(reader["SiteName"]))?string.Empty:(System.String)reader["SiteName"];
					entity.KpiDateTime = (System.DateTime)reader[((int)KpiListColumn.KpiDateTime)];
					//entity.KpiDateTime = (Convert.IsDBNull(reader["kpiDateTime"]))?DateTime.MinValue:(System.DateTime)reader["kpiDateTime"];
					entity.CreatedbyUserId = (System.Int32)reader[((int)KpiListColumn.CreatedbyUserId)];
					//entity.CreatedbyUserId = (Convert.IsDBNull(reader["CreatedbyUserId"]))?(int)0:(System.Int32)reader["CreatedbyUserId"];
					entity.UserLogon = (System.String)reader[((int)KpiListColumn.UserLogon)];
					//entity.UserLogon = (Convert.IsDBNull(reader["UserLogon"]))?string.Empty:(System.String)reader["UserLogon"];
					entity.LastName = (System.String)reader[((int)KpiListColumn.LastName)];
					//entity.LastName = (Convert.IsDBNull(reader["LastName"]))?string.Empty:(System.String)reader["LastName"];
					entity.FirstName = (System.String)reader[((int)KpiListColumn.FirstName)];
					//entity.FirstName = (Convert.IsDBNull(reader["FirstName"]))?string.Empty:(System.String)reader["FirstName"];
					entity.ModifiedbyUserId = (System.Int32)reader[((int)KpiListColumn.ModifiedbyUserId)];
					//entity.ModifiedbyUserId = (Convert.IsDBNull(reader["ModifiedbyUserId"]))?(int)0:(System.Int32)reader["ModifiedbyUserId"];
					entity.ModifiedbyUserLogon = (System.String)reader[((int)KpiListColumn.ModifiedbyUserLogon)];
					//entity.ModifiedbyUserLogon = (Convert.IsDBNull(reader["ModifiedbyUserLogon"]))?string.Empty:(System.String)reader["ModifiedbyUserLogon"];
					entity.ModifiedbyLastName = (System.String)reader[((int)KpiListColumn.ModifiedbyLastName)];
					//entity.ModifiedbyLastName = (Convert.IsDBNull(reader["ModifiedbyLastName"]))?string.Empty:(System.String)reader["ModifiedbyLastName"];
					entity.ModifiedbyFirstName = (System.String)reader[((int)KpiListColumn.ModifiedbyFirstName)];
					//entity.ModifiedbyFirstName = (Convert.IsDBNull(reader["ModifiedbyFirstName"]))?string.Empty:(System.String)reader["ModifiedbyFirstName"];
					entity.DateAdded = (System.DateTime)reader[((int)KpiListColumn.DateAdded)];
					//entity.DateAdded = (Convert.IsDBNull(reader["DateAdded"]))?DateTime.MinValue:(System.DateTime)reader["DateAdded"];
					entity.Datemodified = (System.DateTime)reader[((int)KpiListColumn.Datemodified)];
					//entity.Datemodified = (Convert.IsDBNull(reader["Datemodified"]))?DateTime.MinValue:(System.DateTime)reader["Datemodified"];
					entity.AcceptChanges();
					entity.SuppressEntityEvents = false;
					
					rows.Add(entity);
					pageLength -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		
		
		/// <summary>
		/// Refreshes the <see cref="KpiList"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KpiList"/> object to refresh.</param>
		protected void RefreshEntity(IDataReader reader, KpiList entity)
		{
			reader.Read();
			entity.KpiId = (System.Int32)reader[((int)KpiListColumn.KpiId)];
			//entity.KpiId = (Convert.IsDBNull(reader["KpiId"]))?(int)0:(System.Int32)reader["KpiId"];
			entity.SiteId = (System.Int32)reader[((int)KpiListColumn.SiteId)];
			//entity.SiteId = (Convert.IsDBNull(reader["SiteId"]))?(int)0:(System.Int32)reader["SiteId"];
			entity.CompanyId = (System.Int32)reader[((int)KpiListColumn.CompanyId)];
			//entity.CompanyId = (Convert.IsDBNull(reader["CompanyId"]))?(int)0:(System.Int32)reader["CompanyId"];
			entity.CompanyName = (System.String)reader[((int)KpiListColumn.CompanyName)];
			//entity.CompanyName = (Convert.IsDBNull(reader["CompanyName"]))?string.Empty:(System.String)reader["CompanyName"];
			entity.SiteName = (System.String)reader[((int)KpiListColumn.SiteName)];
			//entity.SiteName = (Convert.IsDBNull(reader["SiteName"]))?string.Empty:(System.String)reader["SiteName"];
			entity.KpiDateTime = (System.DateTime)reader[((int)KpiListColumn.KpiDateTime)];
			//entity.KpiDateTime = (Convert.IsDBNull(reader["kpiDateTime"]))?DateTime.MinValue:(System.DateTime)reader["kpiDateTime"];
			entity.CreatedbyUserId = (System.Int32)reader[((int)KpiListColumn.CreatedbyUserId)];
			//entity.CreatedbyUserId = (Convert.IsDBNull(reader["CreatedbyUserId"]))?(int)0:(System.Int32)reader["CreatedbyUserId"];
			entity.UserLogon = (System.String)reader[((int)KpiListColumn.UserLogon)];
			//entity.UserLogon = (Convert.IsDBNull(reader["UserLogon"]))?string.Empty:(System.String)reader["UserLogon"];
			entity.LastName = (System.String)reader[((int)KpiListColumn.LastName)];
			//entity.LastName = (Convert.IsDBNull(reader["LastName"]))?string.Empty:(System.String)reader["LastName"];
			entity.FirstName = (System.String)reader[((int)KpiListColumn.FirstName)];
			//entity.FirstName = (Convert.IsDBNull(reader["FirstName"]))?string.Empty:(System.String)reader["FirstName"];
			entity.ModifiedbyUserId = (System.Int32)reader[((int)KpiListColumn.ModifiedbyUserId)];
			//entity.ModifiedbyUserId = (Convert.IsDBNull(reader["ModifiedbyUserId"]))?(int)0:(System.Int32)reader["ModifiedbyUserId"];
			entity.ModifiedbyUserLogon = (System.String)reader[((int)KpiListColumn.ModifiedbyUserLogon)];
			//entity.ModifiedbyUserLogon = (Convert.IsDBNull(reader["ModifiedbyUserLogon"]))?string.Empty:(System.String)reader["ModifiedbyUserLogon"];
			entity.ModifiedbyLastName = (System.String)reader[((int)KpiListColumn.ModifiedbyLastName)];
			//entity.ModifiedbyLastName = (Convert.IsDBNull(reader["ModifiedbyLastName"]))?string.Empty:(System.String)reader["ModifiedbyLastName"];
			entity.ModifiedbyFirstName = (System.String)reader[((int)KpiListColumn.ModifiedbyFirstName)];
			//entity.ModifiedbyFirstName = (Convert.IsDBNull(reader["ModifiedbyFirstName"]))?string.Empty:(System.String)reader["ModifiedbyFirstName"];
			entity.DateAdded = (System.DateTime)reader[((int)KpiListColumn.DateAdded)];
			//entity.DateAdded = (Convert.IsDBNull(reader["DateAdded"]))?DateTime.MinValue:(System.DateTime)reader["DateAdded"];
			entity.Datemodified = (System.DateTime)reader[((int)KpiListColumn.Datemodified)];
			//entity.Datemodified = (Convert.IsDBNull(reader["Datemodified"]))?DateTime.MinValue:(System.DateTime)reader["Datemodified"];
			reader.Close();
	
			entity.AcceptChanges();
		}
		
		/*
		/// <summary>
		/// Refreshes the <see cref="KpiList"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KpiList"/> object.</param>
		protected static void RefreshEntity(DataSet dataSet, KpiList entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.KpiId = (Convert.IsDBNull(dataRow["KpiId"]))?(int)0:(System.Int32)dataRow["KpiId"];
			entity.SiteId = (Convert.IsDBNull(dataRow["SiteId"]))?(int)0:(System.Int32)dataRow["SiteId"];
			entity.CompanyId = (Convert.IsDBNull(dataRow["CompanyId"]))?(int)0:(System.Int32)dataRow["CompanyId"];
			entity.CompanyName = (Convert.IsDBNull(dataRow["CompanyName"]))?string.Empty:(System.String)dataRow["CompanyName"];
			entity.SiteName = (Convert.IsDBNull(dataRow["SiteName"]))?string.Empty:(System.String)dataRow["SiteName"];
			entity.KpiDateTime = (Convert.IsDBNull(dataRow["kpiDateTime"]))?DateTime.MinValue:(System.DateTime)dataRow["kpiDateTime"];
			entity.CreatedbyUserId = (Convert.IsDBNull(dataRow["CreatedbyUserId"]))?(int)0:(System.Int32)dataRow["CreatedbyUserId"];
			entity.UserLogon = (Convert.IsDBNull(dataRow["UserLogon"]))?string.Empty:(System.String)dataRow["UserLogon"];
			entity.LastName = (Convert.IsDBNull(dataRow["LastName"]))?string.Empty:(System.String)dataRow["LastName"];
			entity.FirstName = (Convert.IsDBNull(dataRow["FirstName"]))?string.Empty:(System.String)dataRow["FirstName"];
			entity.ModifiedbyUserId = (Convert.IsDBNull(dataRow["ModifiedbyUserId"]))?(int)0:(System.Int32)dataRow["ModifiedbyUserId"];
			entity.ModifiedbyUserLogon = (Convert.IsDBNull(dataRow["ModifiedbyUserLogon"]))?string.Empty:(System.String)dataRow["ModifiedbyUserLogon"];
			entity.ModifiedbyLastName = (Convert.IsDBNull(dataRow["ModifiedbyLastName"]))?string.Empty:(System.String)dataRow["ModifiedbyLastName"];
			entity.ModifiedbyFirstName = (Convert.IsDBNull(dataRow["ModifiedbyFirstName"]))?string.Empty:(System.String)dataRow["ModifiedbyFirstName"];
			entity.DateAdded = (Convert.IsDBNull(dataRow["DateAdded"]))?DateTime.MinValue:(System.DateTime)dataRow["DateAdded"];
			entity.Datemodified = (Convert.IsDBNull(dataRow["Datemodified"]))?DateTime.MinValue:(System.DateTime)dataRow["Datemodified"];
			entity.AcceptChanges();
		}
		*/
			
		#endregion Helper Functions
	}//end class

	#region KpiListFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="KpiList"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class KpiListFilterBuilder : SqlFilterBuilder<KpiListColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the KpiListFilterBuilder class.
		/// </summary>
		public KpiListFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the KpiListFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public KpiListFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the KpiListFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public KpiListFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion KpiListFilterBuilder

	#region KpiListParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="KpiList"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class KpiListParameterBuilder : ParameterizedSqlFilterBuilder<KpiListColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the KpiListParameterBuilder class.
		/// </summary>
		public KpiListParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the KpiListParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public KpiListParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the KpiListParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public KpiListParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion KpiListParameterBuilder
	
	#region KpiListSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="KpiList"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class KpiListSortBuilder : SqlSortBuilder<KpiListColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the KpiListSqlSortBuilder class.
		/// </summary>
		public KpiListSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion KpiListSortBuilder

} // end namespace
