﻿#region Using directives

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="UsersCompaniesActiveAllProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract class UsersCompaniesActiveAllProviderBaseCore : EntityViewProviderBase<UsersCompaniesActiveAll>
	{
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions
		
		/*
		///<summary>
		/// Fill an VList&lt;UsersCompaniesActiveAll&gt; From a DataSet
		///</summary>
		/// <param name="dataSet">the DataSet</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList&lt;UsersCompaniesActiveAll&gt;"/></returns>
		protected static VList&lt;UsersCompaniesActiveAll&gt; Fill(DataSet dataSet, VList<UsersCompaniesActiveAll> rows, int start, int pagelen)
		{
			if (dataSet.Tables.Count == 1)
			{
				return Fill(dataSet.Tables[0], rows, start, pagelen);
			}
			else
			{
				return new VList<UsersCompaniesActiveAll>();
			}	
		}
		
		
		///<summary>
		/// Fill an VList&lt;UsersCompaniesActiveAll&gt; From a DataTable
		///</summary>
		/// <param name="dataTable">the DataTable that hold the data.</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList<UsersCompaniesActiveAll>"/></returns>
		protected static VList&lt;UsersCompaniesActiveAll&gt; Fill(DataTable dataTable, VList<UsersCompaniesActiveAll> rows, int start, int pagelen)
		{
			int recordnum = 0;
			
			System.Collections.IEnumerator dataRows =  dataTable.Rows.GetEnumerator();
			
			while (dataRows.MoveNext() && (pagelen != 0))
			{
				if(recordnum >= start)
				{
					DataRow row = (DataRow)dataRows.Current;
				
					UsersCompaniesActiveAll c = new UsersCompaniesActiveAll();
					c.UserId = (Convert.IsDBNull(row["UserId"]))?(int)0:(System.Int32)row["UserId"];
					c.UserFullNameLogon = (Convert.IsDBNull(row["UserFullNameLogon"]))?string.Empty:(System.String)row["UserFullNameLogon"];
					c.UserFullNameLogonCompany = (Convert.IsDBNull(row["UserFullNameLogonCompany"]))?string.Empty:(System.String)row["UserFullNameLogonCompany"];
					c.CompanyUserFullNameLogon = (Convert.IsDBNull(row["CompanyUserFullNameLogon"]))?string.Empty:(System.String)row["CompanyUserFullNameLogon"];
					c.AcceptChanges();
					rows.Add(c);
					pagelen -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		*/	
						
		///<summary>
		/// Fill an <see cref="VList&lt;UsersCompaniesActiveAll&gt;"/> From a DataReader.
		///</summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pageLength">number of row.</param>
		///<returns>a <see cref="VList&lt;UsersCompaniesActiveAll&gt;"/></returns>
		protected VList<UsersCompaniesActiveAll> Fill(IDataReader reader, VList<UsersCompaniesActiveAll> rows, int start, int pageLength)
		{
			int recordnum = 0;
			while (reader.Read() && (pageLength != 0))
			{
				if(recordnum >= start)
				{
					UsersCompaniesActiveAll entity = null;
					if (DataRepository.Provider.UseEntityFactory)
					{
						entity = EntityManager.CreateViewEntity<UsersCompaniesActiveAll>("UsersCompaniesActiveAll",  DataRepository.Provider.EntityCreationalFactoryType); 
					}
					else
					{
						entity = new UsersCompaniesActiveAll();
					}
					
					entity.SuppressEntityEvents = true;

					entity.UserId = (System.Int32)reader[((int)UsersCompaniesActiveAllColumn.UserId)];
					//entity.UserId = (Convert.IsDBNull(reader["UserId"]))?(int)0:(System.Int32)reader["UserId"];
					entity.UserFullNameLogon = (System.String)reader[((int)UsersCompaniesActiveAllColumn.UserFullNameLogon)];
					//entity.UserFullNameLogon = (Convert.IsDBNull(reader["UserFullNameLogon"]))?string.Empty:(System.String)reader["UserFullNameLogon"];
					entity.UserFullNameLogonCompany = (System.String)reader[((int)UsersCompaniesActiveAllColumn.UserFullNameLogonCompany)];
					//entity.UserFullNameLogonCompany = (Convert.IsDBNull(reader["UserFullNameLogonCompany"]))?string.Empty:(System.String)reader["UserFullNameLogonCompany"];
					entity.CompanyUserFullNameLogon = (System.String)reader[((int)UsersCompaniesActiveAllColumn.CompanyUserFullNameLogon)];
					//entity.CompanyUserFullNameLogon = (Convert.IsDBNull(reader["CompanyUserFullNameLogon"]))?string.Empty:(System.String)reader["CompanyUserFullNameLogon"];
					entity.AcceptChanges();
					entity.SuppressEntityEvents = false;
					
					rows.Add(entity);
					pageLength -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		
		
		/// <summary>
		/// Refreshes the <see cref="UsersCompaniesActiveAll"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="UsersCompaniesActiveAll"/> object to refresh.</param>
		protected void RefreshEntity(IDataReader reader, UsersCompaniesActiveAll entity)
		{
			reader.Read();
			entity.UserId = (System.Int32)reader[((int)UsersCompaniesActiveAllColumn.UserId)];
			//entity.UserId = (Convert.IsDBNull(reader["UserId"]))?(int)0:(System.Int32)reader["UserId"];
			entity.UserFullNameLogon = (System.String)reader[((int)UsersCompaniesActiveAllColumn.UserFullNameLogon)];
			//entity.UserFullNameLogon = (Convert.IsDBNull(reader["UserFullNameLogon"]))?string.Empty:(System.String)reader["UserFullNameLogon"];
			entity.UserFullNameLogonCompany = (System.String)reader[((int)UsersCompaniesActiveAllColumn.UserFullNameLogonCompany)];
			//entity.UserFullNameLogonCompany = (Convert.IsDBNull(reader["UserFullNameLogonCompany"]))?string.Empty:(System.String)reader["UserFullNameLogonCompany"];
			entity.CompanyUserFullNameLogon = (System.String)reader[((int)UsersCompaniesActiveAllColumn.CompanyUserFullNameLogon)];
			//entity.CompanyUserFullNameLogon = (Convert.IsDBNull(reader["CompanyUserFullNameLogon"]))?string.Empty:(System.String)reader["CompanyUserFullNameLogon"];
			reader.Close();
	
			entity.AcceptChanges();
		}
		
		/*
		/// <summary>
		/// Refreshes the <see cref="UsersCompaniesActiveAll"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="UsersCompaniesActiveAll"/> object.</param>
		protected static void RefreshEntity(DataSet dataSet, UsersCompaniesActiveAll entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.UserId = (Convert.IsDBNull(dataRow["UserId"]))?(int)0:(System.Int32)dataRow["UserId"];
			entity.UserFullNameLogon = (Convert.IsDBNull(dataRow["UserFullNameLogon"]))?string.Empty:(System.String)dataRow["UserFullNameLogon"];
			entity.UserFullNameLogonCompany = (Convert.IsDBNull(dataRow["UserFullNameLogonCompany"]))?string.Empty:(System.String)dataRow["UserFullNameLogonCompany"];
			entity.CompanyUserFullNameLogon = (Convert.IsDBNull(dataRow["CompanyUserFullNameLogon"]))?string.Empty:(System.String)dataRow["CompanyUserFullNameLogon"];
			entity.AcceptChanges();
		}
		*/
			
		#endregion Helper Functions
	}//end class

	#region UsersCompaniesActiveAllFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="UsersCompaniesActiveAll"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UsersCompaniesActiveAllFilterBuilder : SqlFilterBuilder<UsersCompaniesActiveAllColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UsersCompaniesActiveAllFilterBuilder class.
		/// </summary>
		public UsersCompaniesActiveAllFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the UsersCompaniesActiveAllFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public UsersCompaniesActiveAllFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the UsersCompaniesActiveAllFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public UsersCompaniesActiveAllFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion UsersCompaniesActiveAllFilterBuilder

	#region UsersCompaniesActiveAllParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="UsersCompaniesActiveAll"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UsersCompaniesActiveAllParameterBuilder : ParameterizedSqlFilterBuilder<UsersCompaniesActiveAllColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UsersCompaniesActiveAllParameterBuilder class.
		/// </summary>
		public UsersCompaniesActiveAllParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the UsersCompaniesActiveAllParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public UsersCompaniesActiveAllParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the UsersCompaniesActiveAllParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public UsersCompaniesActiveAllParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion UsersCompaniesActiveAllParameterBuilder
	
	#region UsersCompaniesActiveAllSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="UsersCompaniesActiveAll"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class UsersCompaniesActiveAllSortBuilder : SqlSortBuilder<UsersCompaniesActiveAllColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UsersCompaniesActiveAllSqlSortBuilder class.
		/// </summary>
		public UsersCompaniesActiveAllSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion UsersCompaniesActiveAllSortBuilder

} // end namespace
