﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="DocumentsDownloadLogProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class DocumentsDownloadLogProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.DocumentsDownloadLog, KaiZen.CSMS.Entities.DocumentsDownloadLogKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.DocumentsDownloadLogKey key)
		{
			return Delete(transactionManager, key.AuditId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_auditId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _auditId)
		{
			return Delete(null, _auditId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_auditId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _auditId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_APSSDownloadLog_UserId key.
		///		FK_APSSDownloadLog_UserId Description: 
		/// </summary>
		/// <param name="_downloadedByUserId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.DocumentsDownloadLog objects.</returns>
		public TList<DocumentsDownloadLog> GetByDownloadedByUserId(System.Int32 _downloadedByUserId)
		{
			int count = -1;
			return GetByDownloadedByUserId(_downloadedByUserId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_APSSDownloadLog_UserId key.
		///		FK_APSSDownloadLog_UserId Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_downloadedByUserId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.DocumentsDownloadLog objects.</returns>
		/// <remarks></remarks>
		public TList<DocumentsDownloadLog> GetByDownloadedByUserId(TransactionManager transactionManager, System.Int32 _downloadedByUserId)
		{
			int count = -1;
			return GetByDownloadedByUserId(transactionManager, _downloadedByUserId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_APSSDownloadLog_UserId key.
		///		FK_APSSDownloadLog_UserId Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_downloadedByUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.DocumentsDownloadLog objects.</returns>
		public TList<DocumentsDownloadLog> GetByDownloadedByUserId(TransactionManager transactionManager, System.Int32 _downloadedByUserId, int start, int pageLength)
		{
			int count = -1;
			return GetByDownloadedByUserId(transactionManager, _downloadedByUserId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_APSSDownloadLog_UserId key.
		///		fkApssDownloadLogUserId Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_downloadedByUserId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.DocumentsDownloadLog objects.</returns>
		public TList<DocumentsDownloadLog> GetByDownloadedByUserId(System.Int32 _downloadedByUserId, int start, int pageLength)
		{
			int count =  -1;
			return GetByDownloadedByUserId(null, _downloadedByUserId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_APSSDownloadLog_UserId key.
		///		fkApssDownloadLogUserId Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_downloadedByUserId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.DocumentsDownloadLog objects.</returns>
		public TList<DocumentsDownloadLog> GetByDownloadedByUserId(System.Int32 _downloadedByUserId, int start, int pageLength,out int count)
		{
			return GetByDownloadedByUserId(null, _downloadedByUserId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_APSSDownloadLog_UserId key.
		///		FK_APSSDownloadLog_UserId Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_downloadedByUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.DocumentsDownloadLog objects.</returns>
		public abstract TList<DocumentsDownloadLog> GetByDownloadedByUserId(TransactionManager transactionManager, System.Int32 _downloadedByUserId, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.DocumentsDownloadLog Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.DocumentsDownloadLogKey key, int start, int pageLength)
		{
			return GetByAuditId(transactionManager, key.AuditId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_APSSDownloadLog index.
		/// </summary>
		/// <param name="_auditId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.DocumentsDownloadLog"/> class.</returns>
		public KaiZen.CSMS.Entities.DocumentsDownloadLog GetByAuditId(System.Int32 _auditId)
		{
			int count = -1;
			return GetByAuditId(null,_auditId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_APSSDownloadLog index.
		/// </summary>
		/// <param name="_auditId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.DocumentsDownloadLog"/> class.</returns>
		public KaiZen.CSMS.Entities.DocumentsDownloadLog GetByAuditId(System.Int32 _auditId, int start, int pageLength)
		{
			int count = -1;
			return GetByAuditId(null, _auditId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_APSSDownloadLog index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_auditId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.DocumentsDownloadLog"/> class.</returns>
		public KaiZen.CSMS.Entities.DocumentsDownloadLog GetByAuditId(TransactionManager transactionManager, System.Int32 _auditId)
		{
			int count = -1;
			return GetByAuditId(transactionManager, _auditId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_APSSDownloadLog index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_auditId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.DocumentsDownloadLog"/> class.</returns>
		public KaiZen.CSMS.Entities.DocumentsDownloadLog GetByAuditId(TransactionManager transactionManager, System.Int32 _auditId, int start, int pageLength)
		{
			int count = -1;
			return GetByAuditId(transactionManager, _auditId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_APSSDownloadLog index.
		/// </summary>
		/// <param name="_auditId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.DocumentsDownloadLog"/> class.</returns>
		public KaiZen.CSMS.Entities.DocumentsDownloadLog GetByAuditId(System.Int32 _auditId, int start, int pageLength, out int count)
		{
			return GetByAuditId(null, _auditId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_APSSDownloadLog index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_auditId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.DocumentsDownloadLog"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.DocumentsDownloadLog GetByAuditId(TransactionManager transactionManager, System.Int32 _auditId, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#region _DocumentsDownloadLog_Report_ByCompany 
		
		/// <summary>
		///	This method wrap the '_DocumentsDownloadLog_Report_ByCompany' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Report_ByCompany(System.Int32? companyId)
		{
			return Report_ByCompany(null, 0, int.MaxValue , companyId);
		}
		
		/// <summary>
		///	This method wrap the '_DocumentsDownloadLog_Report_ByCompany' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Report_ByCompany(int start, int pageLength, System.Int32? companyId)
		{
			return Report_ByCompany(null, start, pageLength , companyId);
		}
				
		/// <summary>
		///	This method wrap the '_DocumentsDownloadLog_Report_ByCompany' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Report_ByCompany(TransactionManager transactionManager, System.Int32? companyId)
		{
			return Report_ByCompany(transactionManager, 0, int.MaxValue , companyId);
		}
		
		/// <summary>
		///	This method wrap the '_DocumentsDownloadLog_Report_ByCompany' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet Report_ByCompany(TransactionManager transactionManager, int start, int pageLength , System.Int32? companyId);
		
		#endregion
		
		#region _DocumentsDownloadLog_MustReadDownloadedFiles_APSS 
		
		/// <summary>
		///	This method wrap the '_DocumentsDownloadLog_MustReadDownloadedFiles_APSS' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet MustReadDownloadedFiles_APSS(System.Int32? companyId)
		{
			return MustReadDownloadedFiles_APSS(null, 0, int.MaxValue , companyId);
		}
		
		/// <summary>
		///	This method wrap the '_DocumentsDownloadLog_MustReadDownloadedFiles_APSS' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet MustReadDownloadedFiles_APSS(int start, int pageLength, System.Int32? companyId)
		{
			return MustReadDownloadedFiles_APSS(null, start, pageLength , companyId);
		}
				
		/// <summary>
		///	This method wrap the '_DocumentsDownloadLog_MustReadDownloadedFiles_APSS' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet MustReadDownloadedFiles_APSS(TransactionManager transactionManager, System.Int32? companyId)
		{
			return MustReadDownloadedFiles_APSS(transactionManager, 0, int.MaxValue , companyId);
		}
		
		/// <summary>
		///	This method wrap the '_DocumentsDownloadLog_MustReadDownloadedFiles_APSS' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet MustReadDownloadedFiles_APSS(TransactionManager transactionManager, int start, int pageLength , System.Int32? companyId);
		
		#endregion
		
		#region _DocumentsDownloadLog_ComplianceReport_ByCompany 
		
		/// <summary>
		///	This method wrap the '_DocumentsDownloadLog_ComplianceReport_ByCompany' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet ComplianceReport_ByCompany(System.Int32? companyId)
		{
			return ComplianceReport_ByCompany(null, 0, int.MaxValue , companyId);
		}
		
		/// <summary>
		///	This method wrap the '_DocumentsDownloadLog_ComplianceReport_ByCompany' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet ComplianceReport_ByCompany(int start, int pageLength, System.Int32? companyId)
		{
			return ComplianceReport_ByCompany(null, start, pageLength , companyId);
		}
				
		/// <summary>
		///	This method wrap the '_DocumentsDownloadLog_ComplianceReport_ByCompany' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet ComplianceReport_ByCompany(TransactionManager transactionManager, System.Int32? companyId)
		{
			return ComplianceReport_ByCompany(transactionManager, 0, int.MaxValue , companyId);
		}
		
		/// <summary>
		///	This method wrap the '_DocumentsDownloadLog_ComplianceReport_ByCompany' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet ComplianceReport_ByCompany(TransactionManager transactionManager, int start, int pageLength , System.Int32? companyId);
		
		#endregion
		
		#region _DocumentsDownloadLog_Report 
		
		/// <summary>
		///	This method wrap the '_DocumentsDownloadLog_Report' stored procedure. 
		/// </summary>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Report()
		{
			return Report(null, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_DocumentsDownloadLog_Report' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Report(int start, int pageLength)
		{
			return Report(null, start, pageLength );
		}
				
		/// <summary>
		///	This method wrap the '_DocumentsDownloadLog_Report' stored procedure. 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Report(TransactionManager transactionManager)
		{
			return Report(transactionManager, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_DocumentsDownloadLog_Report' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet Report(TransactionManager transactionManager, int start, int pageLength );
		
		#endregion
		
		#region _DocumentsDownloadLog_MustReadDownloaded_APSS 
		
		/// <summary>
		///	This method wrap the '_DocumentsDownloadLog_MustReadDownloaded_APSS' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="regionId"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet MustReadDownloaded_APSS(System.Int32? companyId, System.Int32? regionId)
		{
			return MustReadDownloaded_APSS(null, 0, int.MaxValue , companyId, regionId);
		}
		
		/// <summary>
		///	This method wrap the '_DocumentsDownloadLog_MustReadDownloaded_APSS' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="regionId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet MustReadDownloaded_APSS(int start, int pageLength, System.Int32? companyId, System.Int32? regionId)
		{
			return MustReadDownloaded_APSS(null, start, pageLength , companyId, regionId);
		}
				
		/// <summary>
		///	This method wrap the '_DocumentsDownloadLog_MustReadDownloaded_APSS' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="regionId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet MustReadDownloaded_APSS(TransactionManager transactionManager, System.Int32? companyId, System.Int32? regionId)
		{
			return MustReadDownloaded_APSS(transactionManager, 0, int.MaxValue , companyId, regionId);
		}
		
		/// <summary>
		///	This method wrap the '_DocumentsDownloadLog_MustReadDownloaded_APSS' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="regionId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet MustReadDownloaded_APSS(TransactionManager transactionManager, int start, int pageLength , System.Int32? companyId, System.Int32? regionId);
		
		#endregion
		
		#region _DocumentsDownloadLog_MustReadCount_APSS 
		
		/// <summary>
		///	This method wrap the '_DocumentsDownloadLog_MustReadCount_APSS' stored procedure. 
		/// </summary>
		/// <param name="regionId"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet MustReadCount_APSS(System.Int32? regionId)
		{
			return MustReadCount_APSS(null, 0, int.MaxValue , regionId);
		}
		
		/// <summary>
		///	This method wrap the '_DocumentsDownloadLog_MustReadCount_APSS' stored procedure. 
		/// </summary>
		/// <param name="regionId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet MustReadCount_APSS(int start, int pageLength, System.Int32? regionId)
		{
			return MustReadCount_APSS(null, start, pageLength , regionId);
		}
				
		/// <summary>
		///	This method wrap the '_DocumentsDownloadLog_MustReadCount_APSS' stored procedure. 
		/// </summary>
		/// <param name="regionId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet MustReadCount_APSS(TransactionManager transactionManager, System.Int32? regionId)
		{
			return MustReadCount_APSS(transactionManager, 0, int.MaxValue , regionId);
		}
		
		/// <summary>
		///	This method wrap the '_DocumentsDownloadLog_MustReadCount_APSS' stored procedure. 
		/// </summary>
		/// <param name="regionId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet MustReadCount_APSS(TransactionManager transactionManager, int start, int pageLength , System.Int32? regionId);
		
		#endregion
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;DocumentsDownloadLog&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;DocumentsDownloadLog&gt;"/></returns>
		public static TList<DocumentsDownloadLog> Fill(IDataReader reader, TList<DocumentsDownloadLog> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.DocumentsDownloadLog c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("DocumentsDownloadLog")
					.Append("|").Append((System.Int32)reader[((int)DocumentsDownloadLogColumn.AuditId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<DocumentsDownloadLog>(
					key.ToString(), // EntityTrackingKey
					"DocumentsDownloadLog",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.DocumentsDownloadLog();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.AuditId = (System.Int32)reader[((int)DocumentsDownloadLogColumn.AuditId - 1)];
					c.DocumentFileName = (reader.IsDBNull(((int)DocumentsDownloadLogColumn.DocumentFileName - 1)))?null:(System.String)reader[((int)DocumentsDownloadLogColumn.DocumentFileName - 1)];
					c.DownloadedByUserId = (System.Int32)reader[((int)DocumentsDownloadLogColumn.DownloadedByUserId - 1)];
					c.DownloadedFrom = (reader.IsDBNull(((int)DocumentsDownloadLogColumn.DownloadedFrom - 1)))?null:(System.String)reader[((int)DocumentsDownloadLogColumn.DownloadedFrom - 1)];
					c.AuditedOn = (System.DateTime)reader[((int)DocumentsDownloadLogColumn.AuditedOn - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.DocumentsDownloadLog"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.DocumentsDownloadLog"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.DocumentsDownloadLog entity)
		{
			if (!reader.Read()) return;
			
			entity.AuditId = (System.Int32)reader[((int)DocumentsDownloadLogColumn.AuditId - 1)];
			entity.DocumentFileName = (reader.IsDBNull(((int)DocumentsDownloadLogColumn.DocumentFileName - 1)))?null:(System.String)reader[((int)DocumentsDownloadLogColumn.DocumentFileName - 1)];
			entity.DownloadedByUserId = (System.Int32)reader[((int)DocumentsDownloadLogColumn.DownloadedByUserId - 1)];
			entity.DownloadedFrom = (reader.IsDBNull(((int)DocumentsDownloadLogColumn.DownloadedFrom - 1)))?null:(System.String)reader[((int)DocumentsDownloadLogColumn.DownloadedFrom - 1)];
			entity.AuditedOn = (System.DateTime)reader[((int)DocumentsDownloadLogColumn.AuditedOn - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.DocumentsDownloadLog"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.DocumentsDownloadLog"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.DocumentsDownloadLog entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.AuditId = (System.Int32)dataRow["AuditId"];
			entity.DocumentFileName = Convert.IsDBNull(dataRow["DocumentFileName"]) ? null : (System.String)dataRow["DocumentFileName"];
			entity.DownloadedByUserId = (System.Int32)dataRow["DownloadedByUserId"];
			entity.DownloadedFrom = Convert.IsDBNull(dataRow["DownloadedFrom"]) ? null : (System.String)dataRow["DownloadedFrom"];
			entity.AuditedOn = (System.DateTime)dataRow["AuditedOn"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.DocumentsDownloadLog"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.DocumentsDownloadLog Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.DocumentsDownloadLog entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region DownloadedByUserIdSource	
			if (CanDeepLoad(entity, "Users|DownloadedByUserIdSource", deepLoadType, innerList) 
				&& entity.DownloadedByUserIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.DownloadedByUserId;
				Users tmpEntity = EntityManager.LocateEntity<Users>(EntityLocator.ConstructKeyFromPkItems(typeof(Users), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.DownloadedByUserIdSource = tmpEntity;
				else
					entity.DownloadedByUserIdSource = DataRepository.UsersProvider.GetByUserId(transactionManager, entity.DownloadedByUserId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'DownloadedByUserIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.DownloadedByUserIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.UsersProvider.DeepLoad(transactionManager, entity.DownloadedByUserIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion DownloadedByUserIdSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.DocumentsDownloadLog object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.DocumentsDownloadLog instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.DocumentsDownloadLog Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.DocumentsDownloadLog entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region DownloadedByUserIdSource
			if (CanDeepSave(entity, "Users|DownloadedByUserIdSource", deepSaveType, innerList) 
				&& entity.DownloadedByUserIdSource != null)
			{
				DataRepository.UsersProvider.Save(transactionManager, entity.DownloadedByUserIdSource);
				entity.DownloadedByUserId = entity.DownloadedByUserIdSource.UserId;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region DocumentsDownloadLogChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.DocumentsDownloadLog</c>
	///</summary>
	public enum DocumentsDownloadLogChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>Users</c> at DownloadedByUserIdSource
		///</summary>
		[ChildEntityType(typeof(Users))]
		Users,
		}
	
	#endregion DocumentsDownloadLogChildEntityTypes
	
	#region DocumentsDownloadLogFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;DocumentsDownloadLogColumn&gt;"/> class
	/// that is used exclusively with a <see cref="DocumentsDownloadLog"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class DocumentsDownloadLogFilterBuilder : SqlFilterBuilder<DocumentsDownloadLogColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the DocumentsDownloadLogFilterBuilder class.
		/// </summary>
		public DocumentsDownloadLogFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the DocumentsDownloadLogFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public DocumentsDownloadLogFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the DocumentsDownloadLogFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public DocumentsDownloadLogFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion DocumentsDownloadLogFilterBuilder
	
	#region DocumentsDownloadLogParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;DocumentsDownloadLogColumn&gt;"/> class
	/// that is used exclusively with a <see cref="DocumentsDownloadLog"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class DocumentsDownloadLogParameterBuilder : ParameterizedSqlFilterBuilder<DocumentsDownloadLogColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the DocumentsDownloadLogParameterBuilder class.
		/// </summary>
		public DocumentsDownloadLogParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the DocumentsDownloadLogParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public DocumentsDownloadLogParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the DocumentsDownloadLogParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public DocumentsDownloadLogParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion DocumentsDownloadLogParameterBuilder
	
	#region DocumentsDownloadLogSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;DocumentsDownloadLogColumn&gt;"/> class
	/// that is used exclusively with a <see cref="DocumentsDownloadLog"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class DocumentsDownloadLogSortBuilder : SqlSortBuilder<DocumentsDownloadLogColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the DocumentsDownloadLogSqlSortBuilder class.
		/// </summary>
		public DocumentsDownloadLogSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion DocumentsDownloadLogSortBuilder
	
} // end namespace
