using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using KaiZen.CSMS.Services;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxTabControl;
using DevExpress.Web.Data;
using DevExpress.Web.ASPxGridView;
using DevExpress.XtraCharts;
using DevExpress.Data;
using DevExpress.Data.Filtering;
using System.Text.RegularExpressions;


public partial class Adminv2_UserControls_Usage : System.Web.UI.UserControl
{
    Auth auth = new Auth();

    protected void Page_Load(object sender, EventArgs e) {  Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); }

    private void moduleInit(bool postBack)
    {
        if (!postBack)
        { //first time load
            if (auth.RoleId == (int)RoleList.Administrator)
            {
                grid.ExpandAll();
                WebChartControl1.Series[0].ArgumentScaleType = ScaleType.Qualitative;
                WebChartControl1.Series[0].ArgumentDataMember = "DayName";
                WebChartControl1.Series[0].ValueScaleType = ScaleType.Numerical;
                WebChartControl1.Series[0].ValueDataMembers.AddRange(new string[] { "Hits" });
                WebChartControl1.DataBind();

                WebChartControl3.Series[0].ArgumentScaleType = ScaleType.Qualitative;
                WebChartControl3.Series[0].ArgumentDataMember = "Hour";
                WebChartControl3.Series[0].ValueScaleType = ScaleType.Numerical;
                WebChartControl3.Series[0].ValueDataMembers.AddRange(new string[] { "Hits" });
                WebChartControl3.DataBind();
            }
            else
            {
                Response.Redirect("default.aspx");
            }

            // Export
            String exportFileName = @"ALCOA CSMS - Admin - Usage"; //Chrome & IE is fine.
            String userAgent = Request.Headers.Get("User-Agent");
            if (
                (userAgent.Contains("Mozilla") && !userAgent.Contains("AppleWebKit") && !userAgent.Contains("MSIE")) || //Mozilla
                (userAgent.Contains("AppleWebKit") && !userAgent.Contains("Chrome"))      //Safari
                )
            {
                exportFileName = exportFileName.Replace(" ", "_");
            }
            Helper.ExportGrid.Settings(ucExportButtons, exportFileName);
        }
    }

    protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (DropDownList1.SelectedValue.ToString() != "All")
        {
            grid.SettingsPager.Mode = GridViewPagerMode.ShowPager;
            grid.SettingsPager.PageSize = Convert.ToInt32(DropDownList1.SelectedValue.ToString());
        }
        else
        {
            grid.SettingsPager.Mode = GridViewPagerMode.ShowAllRecords;
        }
        grid.DataBind();
    }

    protected void grid_ProcessColumnAutoFilter(object sender, ASPxGridViewAutoFilterEventArgs e)
    {
        if (e.Column != grid.Columns["LogDateTime"]) return;

        Regex patternYYYYMMDD = new Regex("^((?:19|20)\\d\\d)[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])$");
        Regex patternYYYYMM = new Regex("^((?:19|20)\\d\\d)[- /.](0[1-9]|1[012])$");
        Regex patternYYYY = new Regex("^((?:19|20)\\d\\d)$");


        if (e.Kind == DevExpress.Web.ASPxGridView.GridViewAutoFilterEventKind.CreateCriteria)
        {
            // Creates a new filter criterion and applies it.
            e.Criteria = null;

            //YYYY-MM-DD
            //^((?:19|20)\d\d)[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])$

            //YYYY-MM
            //^((?:19|20)\d\d)[- /.](0[1-9]|1[012])$

            //YYYY
            //^((?:19|20)\d\d)$

            
            int year, month, day;

            if (patternYYYYMMDD.IsMatch(e.Value))
            {
                string[] date = e.Value.Split('-');
                if (int.TryParse(date[0], out year) && int.TryParse(date[1], out month) && int.TryParse(date[2], out day))
                {
                    DateTime from = new DateTime(year, month, day);
                    DateTime to = from.AddDays(1);

                    e.Criteria = (new DevExpress.Data.Filtering.OperandProperty("LogDateTime") >= from &
                                    (new DevExpress.Data.Filtering.OperandProperty("LogDateTime") < to));

                }
            }
            else
            {
                if (patternYYYYMM.IsMatch(e.Value))
                {
                    string[] date = e.Value.Split('-');
                    if (int.TryParse(date[0], out year) && int.TryParse(date[1], out month))
                    {
                        DateTime from = new DateTime(year, month, 1);
                        DateTime to = from.AddMonths(1);

                        e.Criteria = (new DevExpress.Data.Filtering.OperandProperty("LogDateTime") >= from &
                                        (new DevExpress.Data.Filtering.OperandProperty("LogDateTime") < to));

                    }
                }
                else
                {
                    if (patternYYYY.IsMatch(e.Value))
                    {
                        if (int.TryParse(e.Value, out year))
                        {
                            e.Criteria = (new DevExpress.Data.Filtering.OperandProperty("LogDateTime") >= new DateTime(year, 1, 1)) &
                                              (new DevExpress.Data.Filtering.OperandProperty("LogDateTime") < new DateTime(year + 1, 1, 1));
                        }
                    }
                }
            }
        }
        else
        {
            DateTime res;
            if (!DateTime.TryParse(e.Value, out res))
            {
                e.Value = string.Empty;
            }
            else
            {
                // Specifies the text dosplayed within the auto-filter row cell.
                //if (patternYYYYMMDD.IsMatch(e.Value))
                //{
                   //e.Value = res.ToString("yyyy-MM-dd");
                //}
                //else
                //{
                //    if (patternYYYYMM.IsMatch(e.Value))
                //    {
                //        e.Value = res.ToString("yyyy-MM");
                //    }
                //    else
                //    {
                //        if (patternYYYY.IsMatch(e.Value))
                //        {
                            e.Value = res.Year.ToString();
                //        }
                //    }
                //}
            }
        }
    }
    protected void grid_AutoFilterCellEditorCreate(object sender, ASPxGridViewEditorCreateEventArgs e)
    {

        // Replaces the default editor used to edit DateTime values with the text editor.
        if (e.Column == grid.Columns["LogDateTime"]) e.EditorProperties = new TextBoxProperties();
    }

}
