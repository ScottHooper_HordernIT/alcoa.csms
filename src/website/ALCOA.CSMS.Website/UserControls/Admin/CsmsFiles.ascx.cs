﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using KaiZen.CSMS.Services;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.Library;


namespace ALCOA.CSMS.Website.UserControls.Admin
{
    public partial class CsmsFiles : System.Web.UI.UserControl
    {
        Auth auth = new Auth();
        protected void Page_Load(object sender, EventArgs e) { Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); }

        private void moduleInit(bool postBack)
        {
            if (!postBack)
            {
                if (auth.RoleId != (int)RoleList.Administrator) Response.Redirect("default.aspx");
            }
            grid.ExpandAll();
        }

        protected void grid_RowCreated(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewTableRowEventArgs e)
        {
            if (e.RowType != DevExpress.Web.ASPxGridView.GridViewRowType.Data) return;

            HyperLink hlAssess = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "hlFile") as HyperLink;
            if (hlAssess != null)
            {
                hlAssess.Text = (string)grid.GetRowValues(e.VisibleIndex, "FileName");
                int CsmsFileId = (int)grid.GetRowValues(e.VisibleIndex, "CsmsFileId");
                hlAssess.NavigateUrl = String.Format("../../admin.aspx{0}",
                    QueryStringModule.Encrypt("s=" + "CsmsFileUpload&id=" + CsmsFileId));

            }
        }
    }
}