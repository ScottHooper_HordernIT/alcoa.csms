﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.SafetyPlansSeAnswersProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(SafetyPlansSeAnswersDataSourceDesigner))]
	public class SafetyPlansSeAnswersDataSource : ProviderDataSource<SafetyPlansSeAnswers, SafetyPlansSeAnswersKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SafetyPlansSeAnswersDataSource class.
		/// </summary>
		public SafetyPlansSeAnswersDataSource() : base(new SafetyPlansSeAnswersService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the SafetyPlansSeAnswersDataSourceView used by the SafetyPlansSeAnswersDataSource.
		/// </summary>
		protected SafetyPlansSeAnswersDataSourceView SafetyPlansSeAnswersView
		{
			get { return ( View as SafetyPlansSeAnswersDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the SafetyPlansSeAnswersDataSource control invokes to retrieve data.
		/// </summary>
		public SafetyPlansSeAnswersSelectMethod SelectMethod
		{
			get
			{
				SafetyPlansSeAnswersSelectMethod selectMethod = SafetyPlansSeAnswersSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (SafetyPlansSeAnswersSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the SafetyPlansSeAnswersDataSourceView class that is to be
		/// used by the SafetyPlansSeAnswersDataSource.
		/// </summary>
		/// <returns>An instance of the SafetyPlansSeAnswersDataSourceView class.</returns>
		protected override BaseDataSourceView<SafetyPlansSeAnswers, SafetyPlansSeAnswersKey> GetNewDataSourceView()
		{
			return new SafetyPlansSeAnswersDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the SafetyPlansSeAnswersDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class SafetyPlansSeAnswersDataSourceView : ProviderDataSourceView<SafetyPlansSeAnswers, SafetyPlansSeAnswersKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SafetyPlansSeAnswersDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the SafetyPlansSeAnswersDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public SafetyPlansSeAnswersDataSourceView(SafetyPlansSeAnswersDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal SafetyPlansSeAnswersDataSource SafetyPlansSeAnswersOwner
		{
			get { return Owner as SafetyPlansSeAnswersDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal SafetyPlansSeAnswersSelectMethod SelectMethod
		{
			get { return SafetyPlansSeAnswersOwner.SelectMethod; }
			set { SafetyPlansSeAnswersOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal SafetyPlansSeAnswersService SafetyPlansSeAnswersProvider
		{
			get { return Provider as SafetyPlansSeAnswersService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<SafetyPlansSeAnswers> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<SafetyPlansSeAnswers> results = null;
			SafetyPlansSeAnswers item;
			count = 0;
			
			System.Int32 _answerId;
			System.Int32 _questionId;
			System.Int32 _responseId;

			switch ( SelectMethod )
			{
				case SafetyPlansSeAnswersSelectMethod.Get:
					SafetyPlansSeAnswersKey entityKey  = new SafetyPlansSeAnswersKey();
					entityKey.Load(values);
					item = SafetyPlansSeAnswersProvider.Get(entityKey);
					results = new TList<SafetyPlansSeAnswers>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case SafetyPlansSeAnswersSelectMethod.GetAll:
                    results = SafetyPlansSeAnswersProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case SafetyPlansSeAnswersSelectMethod.GetPaged:
					results = SafetyPlansSeAnswersProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case SafetyPlansSeAnswersSelectMethod.Find:
					if ( FilterParameters != null )
						results = SafetyPlansSeAnswersProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = SafetyPlansSeAnswersProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case SafetyPlansSeAnswersSelectMethod.GetByAnswerId:
					_answerId = ( values["AnswerId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["AnswerId"], typeof(System.Int32)) : (int)0;
					item = SafetyPlansSeAnswersProvider.GetByAnswerId(_answerId);
					results = new TList<SafetyPlansSeAnswers>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				case SafetyPlansSeAnswersSelectMethod.GetByQuestionIdResponseId:
					_questionId = ( values["QuestionId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["QuestionId"], typeof(System.Int32)) : (int)0;
					_responseId = ( values["ResponseId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["ResponseId"], typeof(System.Int32)) : (int)0;
					item = SafetyPlansSeAnswersProvider.GetByQuestionIdResponseId(_questionId, _responseId);
					results = new TList<SafetyPlansSeAnswers>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// FK
				case SafetyPlansSeAnswersSelectMethod.GetByQuestionId:
					_questionId = ( values["QuestionId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["QuestionId"], typeof(System.Int32)) : (int)0;
					results = SafetyPlansSeAnswersProvider.GetByQuestionId(_questionId, this.StartIndex, this.PageSize, out count);
					break;
				case SafetyPlansSeAnswersSelectMethod.GetByResponseId:
					_responseId = ( values["ResponseId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["ResponseId"], typeof(System.Int32)) : (int)0;
					results = SafetyPlansSeAnswersProvider.GetByResponseId(_responseId, this.StartIndex, this.PageSize, out count);
					break;
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == SafetyPlansSeAnswersSelectMethod.Get || SelectMethod == SafetyPlansSeAnswersSelectMethod.GetByAnswerId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				SafetyPlansSeAnswers entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					SafetyPlansSeAnswersProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<SafetyPlansSeAnswers> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			SafetyPlansSeAnswersProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region SafetyPlansSeAnswersDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the SafetyPlansSeAnswersDataSource class.
	/// </summary>
	public class SafetyPlansSeAnswersDataSourceDesigner : ProviderDataSourceDesigner<SafetyPlansSeAnswers, SafetyPlansSeAnswersKey>
	{
		/// <summary>
		/// Initializes a new instance of the SafetyPlansSeAnswersDataSourceDesigner class.
		/// </summary>
		public SafetyPlansSeAnswersDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public SafetyPlansSeAnswersSelectMethod SelectMethod
		{
			get { return ((SafetyPlansSeAnswersDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new SafetyPlansSeAnswersDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region SafetyPlansSeAnswersDataSourceActionList

	/// <summary>
	/// Supports the SafetyPlansSeAnswersDataSourceDesigner class.
	/// </summary>
	internal class SafetyPlansSeAnswersDataSourceActionList : DesignerActionList
	{
		private SafetyPlansSeAnswersDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the SafetyPlansSeAnswersDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public SafetyPlansSeAnswersDataSourceActionList(SafetyPlansSeAnswersDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public SafetyPlansSeAnswersSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion SafetyPlansSeAnswersDataSourceActionList
	
	#endregion SafetyPlansSeAnswersDataSourceDesigner
	
	#region SafetyPlansSeAnswersSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the SafetyPlansSeAnswersDataSource.SelectMethod property.
	/// </summary>
	public enum SafetyPlansSeAnswersSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByAnswerId method.
		/// </summary>
		GetByAnswerId,
		/// <summary>
		/// Represents the GetByQuestionIdResponseId method.
		/// </summary>
		GetByQuestionIdResponseId,
		/// <summary>
		/// Represents the GetByQuestionId method.
		/// </summary>
		GetByQuestionId,
		/// <summary>
		/// Represents the GetByResponseId method.
		/// </summary>
		GetByResponseId
	}
	
	#endregion SafetyPlansSeAnswersSelectMethod

	#region SafetyPlansSeAnswersFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SafetyPlansSeAnswers"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SafetyPlansSeAnswersFilter : SqlFilter<SafetyPlansSeAnswersColumn>
	{
	}
	
	#endregion SafetyPlansSeAnswersFilter

	#region SafetyPlansSeAnswersExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SafetyPlansSeAnswers"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SafetyPlansSeAnswersExpressionBuilder : SqlExpressionBuilder<SafetyPlansSeAnswersColumn>
	{
	}
	
	#endregion SafetyPlansSeAnswersExpressionBuilder	

	#region SafetyPlansSeAnswersProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;SafetyPlansSeAnswersChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="SafetyPlansSeAnswers"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SafetyPlansSeAnswersProperty : ChildEntityProperty<SafetyPlansSeAnswersChildEntityTypes>
	{
	}
	
	#endregion SafetyPlansSeAnswersProperty
}

