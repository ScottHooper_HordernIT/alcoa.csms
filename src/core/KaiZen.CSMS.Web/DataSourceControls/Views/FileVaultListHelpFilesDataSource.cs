﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.FileVaultListHelpFilesProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[CLSCompliant(true)]
	[Designer(typeof(FileVaultListHelpFilesDataSourceDesigner))]
	public class FileVaultListHelpFilesDataSource : ReadOnlyDataSource<FileVaultListHelpFiles>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FileVaultListHelpFilesDataSource class.
		/// </summary>
		public FileVaultListHelpFilesDataSource() : base(new FileVaultListHelpFilesService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the FileVaultListHelpFilesDataSourceView used by the FileVaultListHelpFilesDataSource.
		/// </summary>
		protected FileVaultListHelpFilesDataSourceView FileVaultListHelpFilesView
		{
			get { return ( View as FileVaultListHelpFilesDataSourceView ); }
		}
		
		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the FileVaultListHelpFilesDataSourceView class that is to be
		/// used by the FileVaultListHelpFilesDataSource.
		/// </summary>
		/// <returns>An instance of the FileVaultListHelpFilesDataSourceView class.</returns>
		protected override BaseDataSourceView<FileVaultListHelpFiles, Object> GetNewDataSourceView()
		{
			return new FileVaultListHelpFilesDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the FileVaultListHelpFilesDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class FileVaultListHelpFilesDataSourceView : ReadOnlyDataSourceView<FileVaultListHelpFiles>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FileVaultListHelpFilesDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the FileVaultListHelpFilesDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public FileVaultListHelpFilesDataSourceView(FileVaultListHelpFilesDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal FileVaultListHelpFilesDataSource FileVaultListHelpFilesOwner
		{
			get { return Owner as FileVaultListHelpFilesDataSource; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal FileVaultListHelpFilesService FileVaultListHelpFilesProvider
		{
			get { return Provider as FileVaultListHelpFilesService; }
		}

		#endregion Properties
		
		#region Methods
		
		#endregion Methods
	}

	#region FileVaultListHelpFilesDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the FileVaultListHelpFilesDataSource class.
	/// </summary>
	public class FileVaultListHelpFilesDataSourceDesigner : ReadOnlyDataSourceDesigner<FileVaultListHelpFiles>
	{
	}

	#endregion FileVaultListHelpFilesDataSourceDesigner

	#region FileVaultListHelpFilesFilter

	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="FileVaultListHelpFiles"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FileVaultListHelpFilesFilter : SqlFilter<FileVaultListHelpFilesColumn>
	{
	}

	#endregion FileVaultListHelpFilesFilter

	#region FileVaultListHelpFilesExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="FileVaultListHelpFiles"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FileVaultListHelpFilesExpressionBuilder : SqlExpressionBuilder<FileVaultListHelpFilesColumn>
	{
	}
	
	#endregion FileVaultListHelpFilesExpressionBuilder		
}

