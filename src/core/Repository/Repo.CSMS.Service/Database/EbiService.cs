﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface IEbiService
    {
        Ebi Get(Expression<Func<Ebi, bool>> where, List<Expression<Func<Ebi, object>>> includes);
        List<Ebi> GetMany(int? top, Expression<Func<Ebi, bool>> where, Func<IQueryable<Ebi>, IOrderedQueryable<Ebi>> orderBy, List<Expression<Func<Ebi, object>>> includes);
        List<Ebi> GetManyPaged(int pageNumber, int pageSize, Expression<Func<Ebi, bool>> where, Func<IQueryable<Ebi>, IOrderedQueryable<Ebi>> orderBy, List<Expression<Func<Ebi, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<Ebi, bool>> where, List<Expression<Func<Ebi, object>>> includes);
        void Insert(Ebi item, bool saveChanges = true);
        void Update(Ebi item, bool saveChanges = true);
        void Delete(Ebi item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class EbiService : IEbiService
    {
        private readonly IRepository<Ebi> _repository;

        public EbiService(IRepository<Ebi> repository)
        {
            this._repository = repository;
        }

        public Ebi Get(Expression<Func<Ebi, bool>> where, List<Expression<Func<Ebi, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<Ebi> GetMany(int? top, Expression<Func<Ebi, bool>> where, Func<IQueryable<Ebi>, IOrderedQueryable<Ebi>> orderBy, List<Expression<Func<Ebi, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<Ebi> GetManyPaged(int pageNumber, int pageSize, Expression<Func<Ebi, bool>> where, Func<IQueryable<Ebi>, IOrderedQueryable<Ebi>> orderBy, List<Expression<Func<Ebi, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<Ebi, bool>> where, List<Expression<Func<Ebi, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(Ebi item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("Ebi");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(Ebi item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("Ebi");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(Ebi item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("Ebi");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
