﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Services;
using KaiZen.CSMS.Web;
using System.Text;

namespace ALCOA.CSMS.Website.PopUps
{
    public partial class KpiHelpFilePopup : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string HelpCaption="Default";
            try
            {
                if (Request.QueryString["Help"] != null)
                {
                    switch (Request.QueryString["Help"])
                    {
                        case "default": break;
                        case "JSA": HelpCaption = "JSA Field Audit Verifications";
                            break;
                        case "WSC": HelpCaption = "Workplace Safety & Compliance";
                            break;
                        case "MHS": HelpCaption = "Management Health Safety work contacts";
                            break;
                        case "BO": HelpCaption = "Behavioural Observations";
                            break;
                        case "TBM": HelpCaption = "Tool box meetings per month";
                            break;
                        case "AWCM": HelpCaption = "Alcoa weekly contractors meeting";
                            break;
                        case "AMCM": HelpCaption = "Alcoa monthly contractors meeting";
                            break;
                        case "FPDP": HelpCaption = "Fatality Prevention discussion and presentation";
                            break;
                        default: HelpCaption = "Default";
                            break;
                    }
                    KpiHelpFilesService kpihelpservice = new KpiHelpFilesService();
                    TList<KpiHelpFiles> kpihelplist = kpihelpservice.GetByKpiHelpCaption(HelpCaption);
                    if (kpihelplist.Count == 0)
                    {
                        TList<KpiHelpFiles> kpihelpdefaultlist = kpihelpservice.GetByKpiHelpCaption("Default");
                        if (kpihelpdefaultlist.Count>0 && kpihelpdefaultlist[0].Content != null)
                        {
                           //PlaceHolder1.Controls.Add(new LiteralControl(Encoding.ASCII.GetString(kpihelpdefaultlist[0].Content)));
                            //Response.OutputStream.Write(kpihelpdefaultlist[0].Content, 0, kpihelpdefaultlist[0].ContentLength);
                           // PlaceHolder1.Controls.Add(new LiteralControl(Encoding.Default.GetString(Encoding.Default.GetBytes(kpihelpdefaultlist[0].Content.ToString()))));
                            

                        }
                    }
                    else
                    {
                        KpiHelpFiles kpihelplatest = kpihelplist[kpihelplist.Count - 1];
                        if (kpihelplatest.Content != null)
                        {
                           // PlaceHolder1.Controls.Add(new LiteralControl(Encoding.ASCII.GetString(kpihelplatest.Content)));
                           // Response.OutputStream.Write(kpihelplatest.Content, 0, kpihelplatest.ContentLength);
                            //PlaceHolder1.Controls.Add(new LiteralControl(Encoding.Default.GetString(Encoding.Default.GetBytes(kpihelplatest.Content.ToString()))));
                            Response.Clear();
                            if (!String.IsNullOrEmpty(kpihelplatest.FileName))
                            {
                                Response.AppendHeader("Content-Type", "application/download");

                                string FileName = kpihelplatest.FileName;
                                string ContentDisposition = String.Format(@"attachment; filename={0}", Server.UrlEncode(FileName));
                                String userAgent = Request.Headers.Get("User-Agent");
                                if (userAgent.Contains("MSIE 7.0"))
                                {
                                    ContentDisposition = String.Format(@"attachment; filename={0}", FileName.Replace(" ", "%20"));
                                }
                                else
                                {
                                    ContentDisposition = String.Format("attachment; filename=\"{0}\"", FileName);
                                }
                                Response.AppendHeader("Content-Disposition", ContentDisposition);

                                //Response.BinaryWrite(d.UploadedFile);
                                Response.OutputStream.Write(kpihelplatest.Content, 0, kpihelplatest.ContentLength);
                                Response.End();
                            }
                            else
                            {
                                Response.Write("File Does not Exist");
                            }
                            Response.Flush();
                        }
                    }
                    
                }
                else
                    throw new Exception("Argument Expected.Help Caption not provided");
            }
            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromContext(Context).Raise(ex);
                //throw new Exception(ex.Message);
            }
        }
    }
}