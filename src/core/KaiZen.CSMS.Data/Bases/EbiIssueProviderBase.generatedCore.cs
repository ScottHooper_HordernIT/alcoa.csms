﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="EbiIssueProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class EbiIssueProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.EbiIssue, KaiZen.CSMS.Entities.EbiIssueKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.EbiIssueKey key)
		{
			return Delete(transactionManager, key.EbiIssueId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_ebiIssueId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _ebiIssueId)
		{
			return Delete(null, _ebiIssueId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_ebiIssueId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _ebiIssueId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.EbiIssue Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.EbiIssueKey key, int start, int pageLength)
		{
			return GetByEbiIssueId(transactionManager, key.EbiIssueId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_EbiIssue index.
		/// </summary>
		/// <param name="_ebiIssueId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.EbiIssue"/> class.</returns>
		public KaiZen.CSMS.Entities.EbiIssue GetByEbiIssueId(System.Int32 _ebiIssueId)
		{
			int count = -1;
			return GetByEbiIssueId(null,_ebiIssueId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_EbiIssue index.
		/// </summary>
		/// <param name="_ebiIssueId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.EbiIssue"/> class.</returns>
		public KaiZen.CSMS.Entities.EbiIssue GetByEbiIssueId(System.Int32 _ebiIssueId, int start, int pageLength)
		{
			int count = -1;
			return GetByEbiIssueId(null, _ebiIssueId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_EbiIssue index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_ebiIssueId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.EbiIssue"/> class.</returns>
		public KaiZen.CSMS.Entities.EbiIssue GetByEbiIssueId(TransactionManager transactionManager, System.Int32 _ebiIssueId)
		{
			int count = -1;
			return GetByEbiIssueId(transactionManager, _ebiIssueId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_EbiIssue index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_ebiIssueId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.EbiIssue"/> class.</returns>
		public KaiZen.CSMS.Entities.EbiIssue GetByEbiIssueId(TransactionManager transactionManager, System.Int32 _ebiIssueId, int start, int pageLength)
		{
			int count = -1;
			return GetByEbiIssueId(transactionManager, _ebiIssueId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_EbiIssue index.
		/// </summary>
		/// <param name="_ebiIssueId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.EbiIssue"/> class.</returns>
		public KaiZen.CSMS.Entities.EbiIssue GetByEbiIssueId(System.Int32 _ebiIssueId, int start, int pageLength, out int count)
		{
			return GetByEbiIssueId(null, _ebiIssueId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_EbiIssue index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_ebiIssueId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.EbiIssue"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.EbiIssue GetByEbiIssueId(TransactionManager transactionManager, System.Int32 _ebiIssueId, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key UK_EbiIssue index.
		/// </summary>
		/// <param name="_issueName"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.EbiIssue"/> class.</returns>
		public KaiZen.CSMS.Entities.EbiIssue GetByIssueName(System.String _issueName)
		{
			int count = -1;
			return GetByIssueName(null,_issueName, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the UK_EbiIssue index.
		/// </summary>
		/// <param name="_issueName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.EbiIssue"/> class.</returns>
		public KaiZen.CSMS.Entities.EbiIssue GetByIssueName(System.String _issueName, int start, int pageLength)
		{
			int count = -1;
			return GetByIssueName(null, _issueName, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the UK_EbiIssue index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_issueName"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.EbiIssue"/> class.</returns>
		public KaiZen.CSMS.Entities.EbiIssue GetByIssueName(TransactionManager transactionManager, System.String _issueName)
		{
			int count = -1;
			return GetByIssueName(transactionManager, _issueName, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the UK_EbiIssue index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_issueName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.EbiIssue"/> class.</returns>
		public KaiZen.CSMS.Entities.EbiIssue GetByIssueName(TransactionManager transactionManager, System.String _issueName, int start, int pageLength)
		{
			int count = -1;
			return GetByIssueName(transactionManager, _issueName, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the UK_EbiIssue index.
		/// </summary>
		/// <param name="_issueName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.EbiIssue"/> class.</returns>
		public KaiZen.CSMS.Entities.EbiIssue GetByIssueName(System.String _issueName, int start, int pageLength, out int count)
		{
			return GetByIssueName(null, _issueName, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the UK_EbiIssue index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_issueName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.EbiIssue"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.EbiIssue GetByIssueName(TransactionManager transactionManager, System.String _issueName, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;EbiIssue&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;EbiIssue&gt;"/></returns>
		public static TList<EbiIssue> Fill(IDataReader reader, TList<EbiIssue> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.EbiIssue c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("EbiIssue")
					.Append("|").Append((System.Int32)reader[((int)EbiIssueColumn.EbiIssueId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<EbiIssue>(
					key.ToString(), // EntityTrackingKey
					"EbiIssue",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.EbiIssue();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.EbiIssueId = (System.Int32)reader[((int)EbiIssueColumn.EbiIssueId - 1)];
					c.OriginalEbiIssueId = c.EbiIssueId;
					c.IssueName = (System.String)reader[((int)EbiIssueColumn.IssueName - 1)];
					c.IssueDesc = (System.String)reader[((int)EbiIssueColumn.IssueDesc - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.EbiIssue"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.EbiIssue"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.EbiIssue entity)
		{
			if (!reader.Read()) return;
			
			entity.EbiIssueId = (System.Int32)reader[((int)EbiIssueColumn.EbiIssueId - 1)];
			entity.OriginalEbiIssueId = (System.Int32)reader["EbiIssueId"];
			entity.IssueName = (System.String)reader[((int)EbiIssueColumn.IssueName - 1)];
			entity.IssueDesc = (System.String)reader[((int)EbiIssueColumn.IssueDesc - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.EbiIssue"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.EbiIssue"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.EbiIssue entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.EbiIssueId = (System.Int32)dataRow["EbiIssueId"];
			entity.OriginalEbiIssueId = (System.Int32)dataRow["EbiIssueId"];
			entity.IssueName = (System.String)dataRow["IssueName"];
			entity.IssueDesc = (System.String)dataRow["IssueDesc"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.EbiIssue"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.EbiIssue Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.EbiIssue entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetByEbiIssueId methods when available
			
			#region EbiDailyReportCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<EbiDailyReport>|EbiDailyReportCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'EbiDailyReportCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.EbiDailyReportCollection = DataRepository.EbiDailyReportProvider.GetByEbiIssueId(transactionManager, entity.EbiIssueId);

				if (deep && entity.EbiDailyReportCollection.Count > 0)
				{
					deepHandles.Add("EbiDailyReportCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<EbiDailyReport>) DataRepository.EbiDailyReportProvider.DeepLoad,
						new object[] { transactionManager, entity.EbiDailyReportCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.EbiIssue object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.EbiIssue instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.EbiIssue Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.EbiIssue entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<EbiDailyReport>
				if (CanDeepSave(entity.EbiDailyReportCollection, "List<EbiDailyReport>|EbiDailyReportCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(EbiDailyReport child in entity.EbiDailyReportCollection)
					{
						if(child.EbiIssueIdSource != null)
						{
							child.EbiIssueId = child.EbiIssueIdSource.EbiIssueId;
						}
						else
						{
							child.EbiIssueId = entity.EbiIssueId;
						}

					}

					if (entity.EbiDailyReportCollection.Count > 0 || entity.EbiDailyReportCollection.DeletedItems.Count > 0)
					{
						//DataRepository.EbiDailyReportProvider.Save(transactionManager, entity.EbiDailyReportCollection);
						
						deepHandles.Add("EbiDailyReportCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< EbiDailyReport >) DataRepository.EbiDailyReportProvider.DeepSave,
							new object[] { transactionManager, entity.EbiDailyReportCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region EbiIssueChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.EbiIssue</c>
	///</summary>
	public enum EbiIssueChildEntityTypes
	{

		///<summary>
		/// Collection of <c>EbiIssue</c> as OneToMany for EbiDailyReportCollection
		///</summary>
		[ChildEntityType(typeof(TList<EbiDailyReport>))]
		EbiDailyReportCollection,
	}
	
	#endregion EbiIssueChildEntityTypes
	
	#region EbiIssueFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EbiIssueColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EbiIssue"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EbiIssueFilterBuilder : SqlFilterBuilder<EbiIssueColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EbiIssueFilterBuilder class.
		/// </summary>
		public EbiIssueFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the EbiIssueFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public EbiIssueFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the EbiIssueFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public EbiIssueFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion EbiIssueFilterBuilder
	
	#region EbiIssueParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EbiIssueColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EbiIssue"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EbiIssueParameterBuilder : ParameterizedSqlFilterBuilder<EbiIssueColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EbiIssueParameterBuilder class.
		/// </summary>
		public EbiIssueParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the EbiIssueParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public EbiIssueParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the EbiIssueParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public EbiIssueParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion EbiIssueParameterBuilder
	
	#region EbiIssueSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EbiIssueColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EbiIssue"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class EbiIssueSortBuilder : SqlSortBuilder<EbiIssueColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EbiIssueSqlSortBuilder class.
		/// </summary>
		public EbiIssueSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion EbiIssueSortBuilder
	
} // end namespace
