﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface IQuestionnaireServicesSelectedAuditService
    {
        QuestionnaireServicesSelectedAudit Get(Expression<Func<QuestionnaireServicesSelectedAudit, bool>> where, List<Expression<Func<QuestionnaireServicesSelectedAudit, object>>> includes);
        List<QuestionnaireServicesSelectedAudit> GetMany(int? top, Expression<Func<QuestionnaireServicesSelectedAudit, bool>> where, Func<IQueryable<QuestionnaireServicesSelectedAudit>, IOrderedQueryable<QuestionnaireServicesSelectedAudit>> orderBy, List<Expression<Func<QuestionnaireServicesSelectedAudit, object>>> includes);
        List<QuestionnaireServicesSelectedAudit> GetManyPaged(int pageNumber, int pageSize, Expression<Func<QuestionnaireServicesSelectedAudit, bool>> where, Func<IQueryable<QuestionnaireServicesSelectedAudit>, IOrderedQueryable<QuestionnaireServicesSelectedAudit>> orderBy, List<Expression<Func<QuestionnaireServicesSelectedAudit, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<QuestionnaireServicesSelectedAudit, bool>> where, List<Expression<Func<QuestionnaireServicesSelectedAudit, object>>> includes);
        void Insert(QuestionnaireServicesSelectedAudit item, bool saveChanges = true);
        void Update(QuestionnaireServicesSelectedAudit item, bool saveChanges = true);
        void Delete(QuestionnaireServicesSelectedAudit item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class QuestionnaireServicesSelectedAuditService : IQuestionnaireServicesSelectedAuditService
    {
        private readonly IRepository<QuestionnaireServicesSelectedAudit> _repository;

        public QuestionnaireServicesSelectedAuditService(IRepository<QuestionnaireServicesSelectedAudit> repository)
        {
            this._repository = repository;
        }

        public QuestionnaireServicesSelectedAudit Get(Expression<Func<QuestionnaireServicesSelectedAudit, bool>> where, List<Expression<Func<QuestionnaireServicesSelectedAudit, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<QuestionnaireServicesSelectedAudit> GetMany(int? top, Expression<Func<QuestionnaireServicesSelectedAudit, bool>> where, Func<IQueryable<QuestionnaireServicesSelectedAudit>, IOrderedQueryable<QuestionnaireServicesSelectedAudit>> orderBy, List<Expression<Func<QuestionnaireServicesSelectedAudit, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<QuestionnaireServicesSelectedAudit> GetManyPaged(int pageNumber, int pageSize, Expression<Func<QuestionnaireServicesSelectedAudit, bool>> where, Func<IQueryable<QuestionnaireServicesSelectedAudit>, IOrderedQueryable<QuestionnaireServicesSelectedAudit>> orderBy, List<Expression<Func<QuestionnaireServicesSelectedAudit, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<QuestionnaireServicesSelectedAudit, bool>> where, List<Expression<Func<QuestionnaireServicesSelectedAudit, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(QuestionnaireServicesSelectedAudit item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("QuestionnaireServicesSelectedAudit");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(QuestionnaireServicesSelectedAudit item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("QuestionnaireServicesSelectedAudit");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(QuestionnaireServicesSelectedAudit item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("QuestionnaireServicesSelectedAudit");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
