﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using KaiZen.CSMS.Services;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.Library;
using System.Data.SqlClient;
using System.ComponentModel;
using System.Reflection;
using System.Data;
using System.Linq;
using repo = Repo.CSMS.DAL.EntityModels;

using DevExpress.Web.ASPxGridView;

/* Last Updated: 11/09/2009
 * Description: Non Compliance Module (Traffic Lights) which goes in centre of front page.
 */

public partial class UserControls_Other_nonCompliance2 : System.Web.UI.UserControl
{
    #region Services

    public Repo.CSMS.Service.Database.ISiteService SiteService { get; set; }

    #endregion

    Auth auth = new Auth();

    const string CellColourBad = "Red"; //"#ffb8b8";
    const string CellColourGood = "Lime"; //"#def3ca";
    bool preloadCompanyId = false; // DT228 Cindi Thornton 23/9/15 - used to determine if the control is preloaded with a company. If so, read only form

    Repo.CSMS.Service.Database.ICompanyService companyService = ALCOA.CSMS.Website.Global.GetInstance<Repo.CSMS.Service.Database.ICompanyService>();
    Repo.CSMS.Service.Database.ICompanySiteCategoryExceptionService companySiteCategoryExceptionService = ALCOA.CSMS.Website.Global.GetInstance<Repo.CSMS.Service.Database.ICompanySiteCategoryExceptionService>();

    private enum TrafficLightColor
    {
        [Description("~/Images/TrafficLights/tlRed.gif")]
        Red = 0,
        [Description("~/Images/TrafficLights/tlYellow.gif")]
        Yellow = 1,
        [Description("~/Images/TrafficLights/tlGreen.gif")]
        Green = 2,
        [Description("~/Images/redcross.gif")]
        RedCross = 3,
    }

    private enum TrafficLightColor2
    {
        [Description("0")]
        Grey = 0,
        [Description("1")]
        Red = 1,
        [Description("2")]
        Yellow = 2,
        [Description("3")]
        Green = 3,
    }

    public static string GetImageURL(Enum value)
    {
        FieldInfo fi = value.GetType().GetField(value.ToString());
        DescriptionAttribute[] attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);
        return (attributes.Length > 0) ? attributes[0].Description : value.ToString();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack);
    }

    private void moduleInit(bool postBack)
    {
        //DT228 Cindi Thornton 23/9/15 Added query string so this can be used as a pre-selected form
        string companyIdAsString = Request.QueryString["CompanyId"];
        string companyName = Request.QueryString["CompanyName"];
        int companyId = -1;

        if (companyIdAsString != null)
        {
            bool companyIdIsInteger = false;
            companyIdIsInteger = Int32.TryParse(companyIdAsString, out companyId);

            if (companyIdIsInteger)
            {
                ddlCompanies.Value = companyId;
                ddlCompanies.Text = companyName;
                preloadCompanyId = true;
                ddlYear.Text = DateTime.Now.Year.ToString();
                ddlYear.Value = DateTime.Now.Year;
            }
        }

        if (preloadCompanyId) // load the module read only
        {
            LoadModule(companyId, (int) ddlYear.Value);
            SetFormReadOnly();
        }
        else
        //End DT228

            if (!postBack)
            { //first time load
                ddlCompanies.ForeColor = System.Drawing.Color.Green;
                ddlCompanies.Font.Bold = true;

                if (auth.RoleId != (int)RoleList.Contractor)
                {
                    //Modified by Ashley Goldstraw 15/08/2016.  DT503.  Changed the datasourceId so that inactive companies will not show in the ddl.
                    //ddlCompanies.DataSourceID = "sqldsCompaniesList";  //Prior to change
                    ddlCompanies.DataSourceID = "sqldsActiveCompaniesList";  //After to change
                    //End modified by Ashley Goldstraw
                    ddlCompanies.TextField = "CompanyName";
                    ddlCompanies.ValueField = "CompanyId";
                    ddlCompanies.DataBind();
                    ddlCompanies.Items.Add("--------", -2);
                    ddlCompanies.Items.Add(" < Select Company... >", -1);
                    ddlCompanies.SelectedIndex = -1;
                    ddlCompanies.Value = " < Select Company... >";
                    ddlCompanies.Text = " < Select Company... >";
                }
                else
                {
                    ddlCompanies.DataSourceID = "sqldsCompaniesList_SubContractors";
                    ddlCompanies.TextField = "CompanyName";
                    ddlCompanies.ValueField = "CompanyId";
                    ddlCompanies.DataBind();
                    ddlCompanies.Items.Add("--------", -2);
                    ddlCompanies.Items.Add(auth.CompanyName, auth.CompanyId);
                    ddlCompanies.SelectedIndex = auth.CompanyId;
                    ddlCompanies.Value = auth.CompanyName;
                    ddlCompanies.Text = auth.CompanyName;

                }

                int cmpSelectedId = Convert.ToInt32(ddlCompanies.SelectedItem.Value);
                hlArpCrp.NavigateUrl = String.Format("javascript:popUp('ArpCrpHealthPopup.aspx?Help=Default&cmpId=" + cmpSelectedId + "')");



                ddlYear.Items.Clear();
                ddlYear.Items.Add((Convert.ToInt32(DateTime.Now.Year) - 1).ToString());
                ddlYear.Items.Add(DateTime.Now.Year.ToString());
                ddlYear.Value = DateTime.Now.Year.ToString();
                ddlYear.SelectedIndex = 1;
                ddlYear.Visible = false;

                //lblYear.Text = String.Format("Monthly Reports Submitted for {0}:", DateTime.Now.Year);

                switch (auth.RoleId)
                {
                    case ((int)RoleList.Reader):
                        //Panel1.Visible = false;

                        break;
                    case ((int)RoleList.Contractor):
                        //Panel1.Visible = true;
                        ddlCompanies.Visible = true;
                        SessionHandler.spVar_CompanyId = auth.CompanyId.ToString();
                        SessionHandler.spVar_Year = (DateTime.Now).Year.ToString();

                        ddlCompanies.Text = auth.CompanyName;
                        ddlCompanies.Value = auth.CompanyId;

                        LoadModule(auth.CompanyId, DateTime.Now.Year);
                        ASPxGridView1.DataBind();  //This line added by Ashley Goldstraw as the hyperlinks were not working correctly.  DT454 - 1/2/2016
                        ddlCompanies.Enabled = false;

                        break;
                    case ((int)RoleList.PreQual):
                        PanelAll.Visible = false;
                        PanelPreQual.Visible = true;
                        PanelSubContractor.Visible = false;

                        CompaniesService cService = new CompaniesService();
                        Companies c = cService.GetByCompanyId(auth.CompanyId);
                        if (c.CompanyStatus2Id == (int)CompanyStatus2List.SubContractor)
                        {
                            PanelAll.Visible = false;
                            PanelPreQual.Visible = false;
                            PanelSubContractor.Visible = true;
                        }
                        break;
                    case ((int)RoleList.Administrator):
                        //Panel1.Visible = false;
                        break;
                    case ((int)RoleList.CWKMaintainer):
                        //Panel1.Visible = false;

                        break;
                    default:
                        // RoleList.Disabled or No Role
                        Response.Redirect("AccessDenied.aspx?error=" + Server.UrlEncode("Unrecognised User."));
                        break;
                }
            }
            else
            {
                if ((int)ddlCompanies.SelectedItem.Value > 0)
                {   //Commented out by Ashley Goldstraw as the Load module was being called when the ddl was changed as well as when the page loaded even if it was a post back.  16/12/2015
                    //DT444 22/1/16 Cindi Thornton - put LoadModule back in, commenting this out broke the Radar hyperlink of the last row in the grid.
                    LoadModule((int)ddlCompanies.SelectedItem.Value, (int)ddlYear.SelectedItem.Value);
                    int cmpSelectedId = Convert.ToInt32(ddlCompanies.SelectedItem.Value);
                    hlArpCrp.NavigateUrl = String.Format("javascript:popUp('ArpCrpHealthPopup.aspx?Help=Default&cmpId=" + cmpSelectedId + "')");

                }
                else
                {
                    int cmpNId = -1;
                    hlArpCrp.NavigateUrl = String.Format("javascript:popUp('ArpCrpHealthPopup.aspx?Help=Default&cmpId=" + cmpNId + "')");

                    throw new Exception("Please pick a Company");
                }
            }


    }

    //DT228 Cindi Thornton 23/9/15 - new function to disable the form's links
    private void SetFormReadOnly()
    {
        //Set fields to read only
        ddlCompanies.Enabled = false;
        ddlYear.Enabled = false;

        //imgNewsHref.HRef = "";
        divNews.Visible = false;

        hlTL_SQ.Enabled = false;
        hlTL_SC.Enabled = false;
        hlTL_CC.Enabled = false;
        hlTL_MRD.Enabled = false;
        hlTL_SMP.Enabled = false;

        hlDefinitions.Enabled = false;
        hlCSR.Enabled = false;
        hlArpCrp.Enabled = false;
        hlRadar.Enabled = false;

        btnEnterKpi.Enabled = false;

        this.userProfile21.EnableBtnEdit = false;
        this.userProfile21.EnableEmailLink = false;
    }

    protected void ddlCompanies_SelectedIndexChanged1(object sender, EventArgs e)
    {
        if ((int)ddlCompanies.SelectedItem.Value > 0)
        {
            LoadModule((int)ddlCompanies.SelectedItem.Value, (int)ddlYear.SelectedItem.Value);
            int cmpSelectedId = Convert.ToInt32(ddlCompanies.SelectedItem.Value);
            hlArpCrp.NavigateUrl = String.Format("javascript:popUp('ArpCrpHealthPopup.aspx?Help=Default&cmpId=" + cmpSelectedId + "')");

        }
        else
        {
            int cmpNId = -1;
            hlArpCrp.NavigateUrl = String.Format("javascript:popUp('ArpCrpHealthPopup.aspx?Help=Default&cmpId=" + cmpNId + "')");

            throw new Exception("Please pick a Company");
        }

        //if ((int)ddlCompanies.SelectedItem.Value > 0)
        //{
        //    LoadModule((int)ddlCompanies.SelectedItem.Value, (int)ddlYear.SelectedItem.Value);
        //}
        //else
        //{
        //    throw new Exception("Please pick a Company");
        //}
    }
    protected void ddlYear_SelectedIndexChanged1(object sender, EventArgs e)
    {
        pnlHealthCheck.Visible = true;
        LoadModule((int)ddlCompanies.SelectedItem.Value, (int)ddlYear.SelectedItem.Value);
    }

    private void LoadModule(int CompanyId, int sYear)
    {
        //PanelAll.Height = Unit.Pixel(500);
        pnlHealthCheck.Visible = true;
        btnEnterKpi.Visible = true;

        int count = 0;
        int fCount = 0;
        int iCount = 0;
        int qid;


        ResetTrafficLights();

        //Added by Vikas for DT2443

        QuestionnaireService qrService = new QuestionnaireService();
        //Start:Enhancement_009
        DataSet dsQues = qrService.GetLatestAssessmentComplete_ByCompanyId(CompanyId);//get only last complete questionarrie
        DataSet dsAllQues = qrService.GetLastQuestionnaire_ByCompanyId(CompanyId); //get only last questionarrie
        String tfAll_comp = "";
        if (dsAllQues != null && dsAllQues.Tables[0].Rows.Count > 0)
        {
            //String ddAll = Helper.Questionnaire.Procurement.LoadAnswer(Convert.ToInt32(dsAllQues.Tables[0].Rows[i][0]), "11_8");
            String tfAll = Helper.Questionnaire.Procurement.LoadAnswer(Convert.ToInt32(dsAllQues.Tables[0].Rows[0][0]), "11_10");

            if (dsQues != null && dsQues.Tables[0].Rows.Count > 0)
            {
                int qid_complete = Convert.ToInt32(dsQues.Tables[0].Rows[0][0]);
                tfAll_comp = Helper.Questionnaire.Procurement.LoadAnswer(qid_complete, "11_10");
            }

            if (tfAll_comp == "Yes")// Last Status=Assessment Complete + 'Yes'
            {
                this.trNote.Visible = true;
            }
            else if (tfAll == "Yes" && dsAllQues.Tables[0].Rows[0][1].ToString() == "1") //New questionrraie(Incomplete)+ naswer='Yes'
            {
                this.trNote.Visible = true;
            }
            else
                this.trNote.Visible = false;

        }
        else
        {
            this.trNote.Visible = false;
        }
        /*
       DataSet dsQues = qrService.GetLatestAssessmentComplete_ByCompanyId(CompanyId);
        
       if (dsQues != null)
       {
           if (dsQues.Tables.Count > 0)
           {
               if (dsQues.Tables[0].Rows.Count > 0)
               {
                   qid = Convert.ToInt32(dsQues.Tables[0].Rows[0][0]);
                   String dd1 = Helper.Questionnaire.Procurement.LoadAnswer(qid, "11_8");
                   String tf = Helper.Questionnaire.Procurement.LoadAnswer(qid, "11_10");
                   if (dd1 == "Yes" || tf == "Yes")
                   {
                       this.trNote.Visible = true;
                   }
                   else
                   {
                       this.trNote.Visible = false;
                   }
               }
               else
               {
                   this.trNote.Visible = false;
               }
           }
           else
           {
               this.trNote.Visible = false;
           }
       }
       else 
       {
           this.trNote.Visible = false;
       }
        
       //End Added
       */
        //End:Enhancement_009

        ASPxGridView1.Columns["Month Safety Compliance Score"].Visible = true;
        ASPxGridView1.Columns["KPI Compliance Radar"].Visible = true;
        ASPxGridView1.Columns["Safety Statistics"].Visible = true;

        DateTime dtMinusOneMonth = DateTime.Now.AddMonths(-1);
        MonthsService mService = new MonthsService();
        Months m = mService.GetByMonthId(dtMinusOneMonth.Month);
        ASPxGridView1.Columns["SafetyComplianceScore_LastMonth"].Caption = StringUtilities.CaptiliseFirstLetter(m.MonthAbbrev.ToLower());
        ASPxGridView1.Columns["SafetyComplianceScore_LastMonth"].Visible = true;
        ASPxGridView1.Columns["SafetyComplianceScore_Ytd"].Visible = true;
        ASPxGridView1.Columns["MandatedTraining_Ytd"].Visible = true;
        ASPxGridView1.Columns["Csa_LastQtr"].Visible = true;
        ASPxGridView1.Columns["KPI Compliance Radar"].Visible = true;
        ASPxGridView1.Columns["LWDR"].Visible = true;
        ASPxGridView1.Columns["TRIR"].Visible = true;
        ASPxGridView1.Columns["AIFR"].Visible = true;

        //CompaniesService _cService = new CompaniesService();
        //Companies c = _cService.GetByCompanyId(CompanyId);
        repo.Company c = companyService.Get(i => i.CompanyId == CompanyId, null);

        if (c.CompanyStatus2Id == (int)CompanyStatus2List.Contractor)
        {
            lblCompanyType.Text = "Contractor";
        }
        if (c.CompanyStatus2Id == (int)CompanyStatus2List.SubContractor)
        {
            lblCompanyType.Text = "Sub Contractor";
        }

        #region Safety Qualification & Subcontractors
        QuestionnaireWithLocationApprovalViewService qService = new QuestionnaireWithLocationApprovalViewService();
        QuestionnaireWithLocationApprovalViewFilters qFilters = new QuestionnaireWithLocationApprovalViewFilters();
        qFilters.Append(QuestionnaireWithLocationApprovalViewColumn.CompanyId, CompanyId.ToString());
        VList<QuestionnaireWithLocationApprovalView> qVlist = qService.GetPaged(qFilters.ToString(), "QuestionnaireId DESC", 0, 100, out count);

        if (count > 0)
        {
            hlTL_SQ.Text = "View";

            bool requaling = false;
            if (qVlist[0].Status == (int)QuestionnaireStatusList.Incomplete)
            {
                lblTL_SQ.Text = "Safety Qualification - Incomplete";
                if (count > 1) requaling = true;
            }
            if (qVlist[0].Status == (int)QuestionnaireStatusList.BeingAssessed)
            {
                lblTL_SQ.Text = "Safety Qualification - Being Assessed";
                if (count > 1) requaling = true;
            }

            if (qVlist[0] != null)
            {
                hlTL_SQ.NavigateUrl = "~/SafetyPQ_QuestionnaireModify.aspx" + QueryStringModule.Encrypt("q=" + qVlist[0].QuestionnaireId);
            }
            //Code add to display updated valid date for safty qualification
            //Author: Bishwajit Sahoo
            //Date: 23/10/2012

            //if (qVlist.Count==1 && qVlist[0].ActionName == "SupplierQuestionnaireIncomplete" && qVlist[0].ActionDescription == "Supplier - Supplier Questionnaire - Resubmitting")
            if (qVlist[0].MainAssessmentValidTo != null && qVlist[0].Status != (int)QuestionnaireStatusList.AssessmentComplete)
            {
                DateTime dtValidTo = (DateTime)qVlist[0].MainAssessmentValidTo;
                if (dtValidTo >= DateTime.Now)
                {
                    lblTL_SQ.Text = "Safety Qualification valid until " + dtValidTo.ToString("dd/MM/yyyy");
                    gcTL_SQ.Value = GetImageURL(TrafficLightColor2.Green);
                }
                else
                {
                    lblTL_SQ.Text = "Safety Qualification Expired at " + dtValidTo.ToString("dd/MM/yyyy");
                }
            }
            bool done = false;
            int no = 0;
            int i = 0;
            while (i <= qVlist.Count - 1)
            {
                if (!done)
                {
                    if (qVlist[i] != null)
                    {
                        if (qVlist[i].Status == (int)QuestionnaireStatusList.AssessmentComplete)
                        {
                            lblTL_SQ.Text = "Safety Qualification - Assessment Complete";
                            if (qVlist[i].IsVerificationRequired)// && (c.CompanyStatus2Id == (int)CompanyStatus2List.Contractor)) //AShley Goldstraw 4/4/2016 DT328
                            {
                                //Changed by Pankaj DIkshit for <DT 2473, 03-Aug-2012> condtn:- '&& qVlist[i].MainAssessmentValidTo >= DateTime.Now' added
                                if (qVlist[i].LevelOfSupervision != "Direct" && qVlist[i].MainAssessmentValidTo >= DateTime.Now)
                                {
                                    hlTL_SC.Text = "List";
                                    hlTL_SC.NavigateUrl = String.Format("~/ListSubContractorsCompliance.aspx{0}", QueryStringModule.Encrypt("q=" + c.CompanyId));
                                    gcTL_SC.Value = GetImageURL(TrafficLightColor2.Green);
                                }
                            }

                            if (qVlist[i].MainAssessmentValidTo != null)
                            {
                                if (qVlist[i].MainAssessmentValidTo > DateTime.Now)
                                {
                                    DateTime dt = (DateTime)qVlist[i].MainAssessmentValidTo;
                                    if (qVlist[i].Status == (int)QuestionnaireStatusList.AssessmentComplete)
                                    {
                                        done = true;

                                        lblTL_SQ.Text = "Safety Qualification valid until " + dt.ToString("dd/MM/yyyy");
                                        if (requaling) lblTL_SQ.Text += " (Currently Re-Qualifying)";
                                        gcTL_SQ.Value = GetImageURL(TrafficLightColor2.Green);

                                        lblSupervisionRequirement.Text = qVlist[i].LevelOfSupervision;

                                        DateTime dtExpiry = Convert.ToDateTime(qVlist[i].MainAssessmentValidTo);
                                        TimeSpan ts = Convert.ToDateTime(dtExpiry) - DateTime.Now;
                                        if (ts.Days <= 60)
                                        {
                                            gcTL_SQ.Value = GetImageURL(TrafficLightColor2.Yellow);
                                            lblSupervisionRequirement.Text = qVlist[i].LevelOfSupervision;
                                            if (ts.Days == 1)
                                            {
                                                lblTL_SQ.Text = "Safety Qualification valid until " + dt.ToString("dd/MM/yyyy") + " (Expires in " + ts.Days + " Day)";
                                            }
                                            else
                                            {
                                                lblTL_SQ.Text = "Safety Qualification valid until " + dt.ToString("dd/MM/yyyy") + " (Expires in " + ts.Days + " Days)";
                                            }
                                            no = i;
                                        }
                                    }
                                    else
                                    {
                                        gcTL_SQ.Value = GetImageURL(TrafficLightColor2.Yellow);

                                        if (qVlist[i + 1] != null)
                                        {
                                            if (qVlist[i + 1].Status == (int)QuestionnaireStatusList.AssessmentComplete &&
                                                c.CompanyStatusId == (int)CompanyStatusList.Requal_Incomplete)
                                            {
                                                DateTime dtExpiry = Convert.ToDateTime(qVlist[i].MainAssessmentValidTo);
                                                TimeSpan ts = Convert.ToDateTime(dtExpiry) - DateTime.Now;
                                                if (ts.Days <= 60)
                                                {
                                                    lblSupervisionRequirement.Text = qVlist[i].LevelOfSupervision;
                                                    lblTL_SQ.Text = "Safety Qualification Valid until " + dt.ToString("dd/MM/yyyy") + " - Expires in " + ts.Days + " Days.";
                                                    no = i;
                                                    done = true;
                                                }
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    DateTime dt = (DateTime)qVlist[i].MainAssessmentValidTo;
                                    lblTL_SQ.Text = "Safety Qualification Expired at " + dt.ToString("dd/MM/yyyy");
                                    gcTL_SQ.Value = GetImageURL(TrafficLightColor2.Red);
                                    if (qVlist.Count >= i + 2) //todo: check this...
                                    {
                                        if (qVlist[i + 1].Status == (int)QuestionnaireStatusList.AssessmentComplete)
                                        {
                                            lblSupervisionRequirement.Text = qVlist[i + 1].LevelOfSupervision;
                                        }
                                    }
                                    no = i;
                                    done = true;

                                    //Code add to display updated valid date for safty qualification
                                    //Author: Bishwajit Sahoo
                                    //Date: 23/10/2012
                                    //if (qVlist[0].ActionName == "SupplierQuestionnaireIncomplete" && qVlist[0].ActionDescription == "Supplier - Supplier Questionnaire - Resubmitting")
                                    if (qVlist[0].MainAssessmentValidTo != null && qVlist[0].Status != (int)QuestionnaireStatusList.AssessmentComplete)
                                    {
                                        DateTime dt1 = (DateTime)qVlist[0].MainAssessmentValidTo;
                                        lblTL_SQ.Text = "Safety Qualification valid until " + dt1.ToString("dd/MM/yyyy");
                                        //DT 3208 By Ratikanta    
                                        if ((DateTime)qVlist[0].MainAssessmentValidTo > DateTime.Now)
                                        {
                                            gcTL_SQ.Value = GetImageURL(TrafficLightColor2.Green);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                i++;
            }


            //if (qVlist[0].MainAssessmentValidTo != null)
            //{
            //    if (qVlist[0].MainAssessmentValidTo > DateTime.Now)
            //    {
            //        DateTime dt = (DateTime)qVlist[0].MainAssessmentValidTo;
            //        if (qVlist[0].Status == (int)QuestionnaireStatusList.AssessmentComplete)
            //        {
            //            lblTL_SQ.Text = "Safety Qualification valid until " + dt.ToString("dd/MM/yyyy");
            //            gcTL_SQ.Value = GetImageURL(TrafficLightColor2.Green);
            //            lblSupervisionRequirement.Text = qVlist[0].LevelOfSupervision;

            //            DateTime dtExpiry = Convert.ToDateTime(qVlist[0].MainAssessmentValidTo);
            //            TimeSpan ts = Convert.ToDateTime(dtExpiry) - DateTime.Now;
            //            if (ts.Days <= 60)
            //            {
            //                gcTL_SQ.Value = GetImageURL(TrafficLightColor2.Yellow);
            //                lblSupervisionRequirement.Text = qVlist[0].LevelOfSupervision;
            //                if (ts.Days == 1)
            //                {
            //                    lblTL_SQ.Text = "Safety Qualification valid until " + dt.ToString("dd/MM/yyyy") + " (Expires in " + ts.Days + " Day)";
            //                }
            //                else
            //                {
            //                    lblTL_SQ.Text = "Safety Qualification valid until " + dt.ToString("dd/MM/yyyy") + " (Expires in " + ts.Days + " Days)";
            //                }
            //            }
            //        }
            //        else
            //        {
            //            gcTL_SQ.Value = GetImageURL(TrafficLightColor2.Yellow);

            //            if (qVlist[1].Status == (int)QuestionnaireStatusList.AssessmentComplete &&
            //                c.CompanyStatusId == (int)CompanyStatusList.Requal_Incomplete)
            //            {
            //                DateTime dtExpiry = Convert.ToDateTime(qVlist[0].MainAssessmentValidTo);
            //                TimeSpan ts = Convert.ToDateTime(dtExpiry) - DateTime.Now;
            //                if (ts.Days <= 60)
            //                {
            //                    lblSupervisionRequirement.Text = qVlist[0].LevelOfSupervision;
            //                    lblTL_SQ.Text = "Safety Qualification Valid until " + dt.ToString("dd/MM/yyyy") + " - Expires in " + ts.Days + " Days.";
            //                }
            //            }
            //        }
            //    }
            //    else
            //    {
            //        DateTime dt = (DateTime)qVlist[0].MainAssessmentValidTo;
            //        lblTL_SQ.Text = "Safety Qualification Expired at " + dt.ToString("dd/MM/yyyy");
            //        gcTL_SQ.Value = GetImageURL(TrafficLightColor2.Red);
            //        if (qVlist[1] != null)
            //        {
            //            if (qVlist[1].Status == (int)QuestionnaireStatusList.AssessmentComplete)
            //            {
            //                lblSupervisionRequirement.Text = qVlist[1].LevelOfSupervision;
            //            }
            //        }
            //    }
            //}
        }
        else
        {
            hlTL_SQ.Text = "";
            gcTL_SQ.Value = GetImageURL(TrafficLightColor2.Red);
            hlTL_SQ.NavigateUrl = "javascript:void(0);";
            gcTL_SQ.Visible = true;
            gcTL_SQ.ToolTip = "No Safety Qualification exists for your company. Please Contact your procurement contact.";
        }


        //if (CompanyId != 0)
        //{
        //    int _count = 0;
        //    QuestionnaireFilters qFilters = new QuestionnaireFilters();
        //    qFilters.Append(QuestionnaireColumn.CompanyId, CompanyId.ToString());
        //    TList<Questionnaire> qList = DataRepository.QuestionnaireProvider.GetPaged(String.Format("CompanyId = {0}", CompanyId), "CreatedDate DESC", 0, 9999, out _count);

        //    if (_count > 0)
        //    {
        //        switch (qList[0].Status)
        //        {
        //            case (int)QuestionnaireStatusList.Incomplete:
        //                imgTL_SQ.ImageUrl = GetImageURL(TrafficLightColor.Red);
        //                imgTL_SQ.ToolTip = "Your company's latest Safety Questionnaire is incomplete.";

        //                CompaniesService _cService0 = new CompaniesService();
        //                Companies _c = _cService0.GetByCompanyId(CompanyId);
        //                if (_c.CompanyStatusId == (int)CompanyStatusList.Requal_Incomplete)
        //                {
        //                    if (qList[1] != null)
        //                    {
        //                        if (qList[1].MainAssessmentValidTo != null)
        //                        {
        //                            imgTL_SQ.ImageUrl = GetImageURL(TrafficLightColor.Yellow);
        //                            imgTL_SQ.ToolTip = "Your company is being Re-Qualified but your previous questionnaire is valid until " + qList[1].MainAssessmentValidTo.ToString() + ".";
        //                        }
        //                    }
        //                }
        //                break;
        //            case (int)QuestionnaireStatusList.BeingAssessed:
        //                imgTL_SQ.ImageUrl = GetImageURL(TrafficLightColor.Yellow);
        //                imgTL_SQ.ToolTip = "Your company's latest Safety Questionnaire has been submitted and is currently being assessed.";
        //                break;
        //            case (int)QuestionnaireStatusList.AssessmentComplete:
        //                if (qList[0].MainAssessmentValidTo < DateTime.Now)
        //                {
        //                    imgTL_SQ.ImageUrl = GetImageURL(TrafficLightColor.RedCross);
        //                    imgTL_SQ.ToolTip = String.Format("Your company's latest Safety Questionnaire expired on {0}.", qList[0].MainAssessmentValidTo);
        //                }
        //                else
        //                {
        //                    if (qList[0].Recommended == true)
        //                    {
        //                        imgTL_SQ.ImageUrl = GetImageURL(TrafficLightColor.Green);
        //                        imgTL_SQ.ToolTip = "Your company's latest Safety Questionnaire has been assessed and has achieved a Recommended status.";
        //                    }
        //                    if (qList[0].Recommended == false)
        //                    {
        //                        imgTL_SQ.ImageUrl = GetImageURL(TrafficLightColor.RedCross);
        //                        imgTL_SQ.ToolTip = "Your company's latest Safety Questionnaire has been assessed and has achieved an NOT Recommended status.";
        //                    }
        //                }
        //                DateTime dtNow = DateTime.Now;
        //                if (qList[0].MainAssessmentValidTo != null)
        //                {
        //                    DateTime dtExpiry = Convert.ToDateTime(qList[0].MainAssessmentValidTo);

        //                    TimeSpan ts = Convert.ToDateTime(dtExpiry) - dtNow;
        //                    if (ts.Days <= 60)
        //                    {
        //                        if (ts.Days > 0)
        //                        {
        //                            imgTL_SQ.ImageUrl = GetImageURL(TrafficLightColor.Yellow);
        //                            imgTL_SQ.ToolTip = "Your company's latest Safety Questionnaire is due to expire in " + ts.Days.ToString() + " Days at " + dtExpiry.ToShortDateString() + ".";
        //                        }
        //                        else
        //                        {
        //                            imgTL_SQ.ImageUrl = GetImageURL(TrafficLightColor.Red);
        //                            imgTL_SQ.ToolTip = "Your company's latest Safety Questionnaire has expired.";
        //                        }
        //                    }
        //                }

        //                break;
        //        }
        //    }
        //    else
        //    {
        //        imgTL_SQ.ImageUrl = GetImageURL(TrafficLightColor.Red);
        //        imgTL_SQ.ToolTip = "Your company does NOT have a Safety Questionnaire";
        //    }
        //}
        #endregion

        count = 0;

        #region Safety Management Plans
        FileDbFilters queryFileDb = new FileDbFilters();
        queryFileDb.Append(FileDbColumn.CompanyId, CompanyId.ToString());
        queryFileDb.Append(FileDbColumn.Description, String.Format("%{0}%", sYear));
        TList<FileDb> FileDblist = DataRepository.FileDbProvider.GetPaged(queryFileDb.ToString(), null, 0, 100, out count);
        int spTotal = count;
        count = 0;

        FileDbFilters queryFileDb2 = new FileDbFilters();
        queryFileDb2.Append(FileDbColumn.CompanyId, CompanyId.ToString());
        queryFileDb2.Append(FileDbColumn.Description, String.Format("%{0}%", sYear));
        queryFileDb2.Append(FileDbColumn.StatusId, "1"); //Submitted
        TList<FileDb> FileDblist2 = DataRepository.FileDbProvider.GetPaged(queryFileDb2.ToString(), null, 0, 100, out count);
        int spSubmitted = count;
        count = 0;

        DataSet dsLastFileId = DataRepository.FileDbProvider.GetResponseIdLastSubmittedFile(sYear.ToString(), CompanyId);
        int ResponseId = -1;
        foreach (DataRow dr in dsLastFileId.Tables[0].Rows)
        {
            try
            {
                ResponseId = Convert.ToInt32(dr[0].ToString());
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromContext(Context).Raise(ex);
            }
        }
        int status = -1;
        if (ResponseId >= 0)
        {
            FileDbService f = new FileDbService();
            DataSet ds = f.GetStatusFromResponseId(ResponseId);
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                status = Convert.ToInt32(dr["StatusId"].ToString());
            }

        }
        else
        {
            FileDbService f = new FileDbService();
            DataSet ds = f.GetStatusLastSubmittedFile(sYear.ToString(), CompanyId);
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                status = Convert.ToInt32(dr["StatusId"].ToString());
            }
        }

        //Added by Sayani Sil for <Issue# 4, 05-Sept-2012> - Start
        DataSet dsPlanStatus = DataRepository.FileDbProvider.GetVisiblePlanStatus(sYear.ToString(), CompanyId);

        if (dsPlanStatus.Tables[0].Rows.Count > 0)
        {
            string CompSafetyCat = dsPlanStatus.Tables[0].Rows[0][1].ToString();

            if (CompSafetyCat == "Embedded" || CompSafetyCat == "Non-Embedded 1")
            {
                gcTL_SMP.Visible = true;
                hlTL_SMP.Visible = true;
                lblTL_SMP.Visible = true;
                Plan.Visible = true;
            }
            else
            {
                gcTL_SMP.Visible = false;
                hlTL_SMP.Visible = false;
                lblTL_SMP.Visible = false;
                Plan.Visible = false;
            }
        }
        else
        {
            gcTL_SMP.Visible = false;
            hlTL_SMP.Visible = false;
            lblTL_SMP.Visible = false;
            Plan.Visible = false;
        }
        //Added by Sayani Sil for <Issue# 4, 05-Sept-2012> - End
        gcTL_SMP.Value = GetImageURL(TrafficLightColor2.Red);
        gcTL_SMP.ToolTip = "No Safety Management Plan has been submitted by your company for this year.";
        hlTL_SMP.Text = "View";
        lblTL_SMP.Text = "Safety Management Plan - Not Submitted";
        try
        {
            switch (status)
            {
                case (int)SafetyPlanStatusList.Approved:
                    gcTL_SMP.Value = GetImageURL(TrafficLightColor2.Green);
                    gcTL_SMP.ToolTip = "Good Work. Your company has submitted a Safety Management Plan for this year (and it has been approved)!";
                    lblTL_SMP.Text = "Safety Management Plan - Approved";
                    break;
                case (int)SafetyPlanStatusList.Being_Assessed:
                    gcTL_SMP.Value = GetImageURL(TrafficLightColor2.Yellow);
                    gcTL_SMP.ToolTip = "Good Work. Your company has submitted a Safety Management Plan for this year (which is now being assessed)!";
                    lblTL_SMP.Text = "Safety Management Plan - Being Assessed";
                    break;
                case (int)SafetyPlanStatusList.Not_Approved:
                    gcTL_SMP.Value = GetImageURL(TrafficLightColor2.Red); //redcross
                    gcTL_SMP.ToolTip = "Your latest Safety Plan is 'Not Approved'";
                    lblTL_SMP.Text = "Safety Management Plan - Not Approved";
                    break;
                case (int)SafetyPlanStatusList.Submitted:
                    gcTL_SMP.Value = GetImageURL(TrafficLightColor2.Yellow);
                    gcTL_SMP.ToolTip = "Good Work. Your company has submitted a Safety Management Plan for this year.";
                    lblTL_SMP.Text = "Safety Management Plan - Currently Being Assessed";
                    break;
                case (int)SafetyPlanStatusList.Assigned:
                    gcTL_SMP.Value = GetImageURL(TrafficLightColor2.Yellow);
                    gcTL_SMP.ToolTip = "Good Work. Your company has submitted a Safety Management Plan for this year.";
                    lblTL_SMP.Text = "Safety Management Plan - Currently Being Assessed";
                    break;
                default:
                    break;
            }
        }
        catch (Exception ex)
        {
            Elmah.ErrorSignal.FromContext(Context).Raise(ex);
            //ToDo: Handle Exception
        }
        #endregion

        count = 0;

        #region Contractor Contacts
        ContactsContractorsFilters querycContacts = new ContactsContractorsFilters();
        querycContacts.Append(ContactsContractorsColumn.CompanyId, CompanyId.ToString());
        querycContacts.Append(ContactsContractorsColumn.ContactRole, "Principal");
        TList<ContactsContractors> cContactsList = DataRepository.ContactsContractorsProvider.GetPaged(querycContacts.ToString(), null, 0, 100, out count);
        if (count > 0) { } else { iCount += 1; };
        count = 0;

        querycContacts.Clear();
        querycContacts.Append(ContactsContractorsColumn.CompanyId, CompanyId.ToString());
        querycContacts.Append(ContactsContractorsColumn.ContactRole, "KPI%Administration%Contact");
        cContactsList = DataRepository.ContactsContractorsProvider.GetPaged(querycContacts.ToString(), null, 0, 100, out count);
        if (count > 0) { } else { iCount += 1; };
        count = 0;

        querycContacts.Clear();
        querycContacts.Append(ContactsContractorsColumn.CompanyId, CompanyId.ToString());
        querycContacts.Append(ContactsContractorsColumn.ContactRole, "Contract%Manager");
        cContactsList = DataRepository.ContactsContractorsProvider.GetPaged(querycContacts.ToString(), null, 0, 100, out count);
        if (count > 0) { } else { iCount += 1; };
        count = 0;

        querycContacts.Clear();
        querycContacts.Append(ContactsContractorsColumn.CompanyId, CompanyId.ToString());
        querycContacts.Append(ContactsContractorsColumn.ContactRole, "Operation%Manager");
        cContactsList = DataRepository.ContactsContractorsProvider.GetPaged(querycContacts.ToString(), null, 0, 100, out count);
        if (count > 0) { } else { iCount += 1; };
        count = 0;

        bool contactsOutofDate = false;
        DateTime nowMinus3 = DateTime.Now.AddMonths(-3);


        if (c.ContactsLastUpdated != null)
        {
            if (c.ContactsLastUpdated <= nowMinus3)
            {
                contactsOutofDate = true;
            }
        }
        else
        {
            contactsOutofDate = true;
        }

        if (c.ContactsLastUpdated != null)
        {
            DateTime dt = (DateTime)c.ContactsLastUpdated;
            lblTL_CC.Text += " last updated on " + dt.ToShortDateString();
        }

        if (iCount != 0)
        {
            gcTL_CC.Value = GetImageURL(TrafficLightColor2.Red);
            if (contactsOutofDate)
            {
                gcTL_CC.ToolTip = "Your company's contact list has not been verified as up-to-date within the last 3 months and also does not include the minimum entries of: Principal, KPI Administrator, Contract Manager and Operation Manager.";
            }
            else
            {
                gcTL_CC.ToolTip = "Your company's contact list does not include the minimum entries of: Principal, KPI Administrator, Contract Manager and Operation Manager.";
            }
            fCount += 1;
        }
        else
        {
            if (contactsOutofDate)
            {
                gcTL_CC.Value = GetImageURL(TrafficLightColor2.Red);
                gcTL_CC.ToolTip = "Your company's contact list has not been verified as up-to-date within the last 3 months.";
            }
            else
            {
                gcTL_CC.Value = GetImageURL(TrafficLightColor2.Green);
                gcTL_CC.ToolTip = "Good Work. Your company's contact list has the minimum entries of: Principal, KPI Administrator, Contractor Manager and Operation Manager.";
            }
        }

        #endregion

        #region Must Read Documents

        int? _CompanyId = CompanyId;

        RegionsService rService = new RegionsService();
        Regions rList_WAO = rService.GetByRegionInternalName("WAO");
        Regions rList_VICOPS = rService.GetByRegionInternalName("VICOPS");
        int? _RegionId_WAO = rList_WAO.RegionId;
        int? _RegionId_VICOPS = rList_VICOPS.RegionId;

        int TotalCD = 0;
        int ReadCD = 0;
        int UnreadCD = 0;

        DocumentsDownloadLogService ddlService = new DocumentsDownloadLogService();

        CompaniesService cService = new CompaniesService();

        DataSet dsCount = cService.GetCountSites_ByRegion(_CompanyId, _RegionId_WAO);
        int wCount = 0;
        foreach (DataRow dr in dsCount.Tables[0].Rows) wCount = Convert.ToInt32(dr[0].ToString());
        if (wCount > 0)
        {
            DataSet ddl_WAO_Read = ddlService.MustReadDownloaded_APSS(_CompanyId, _RegionId_WAO);
            DataSet ddl_WAO_Total = ddlService.MustReadCount_APSS(_RegionId_WAO);

            foreach (DataRow dr in ddl_WAO_Read.Tables[0].Rows) ReadCD = Convert.ToInt32(dr[0].ToString());
            foreach (DataRow dr in ddl_WAO_Total.Tables[0].Rows) TotalCD = Convert.ToInt32(dr[0].ToString());
        }


        dsCount = cService.GetCountSites_ByRegion(_CompanyId, _RegionId_VICOPS);
        int vCount = 0;
        foreach (DataRow dr in dsCount.Tables[0].Rows) vCount = Convert.ToInt32(dr[0].ToString());
        if (vCount > 0)
        {
            DataSet ddl_VICOPS_Read = ddlService.MustReadDownloaded_APSS(_CompanyId, _RegionId_VICOPS);
            DataSet ddl_VICOPS_Total = ddlService.MustReadCount_APSS(_RegionId_VICOPS);

            foreach (DataRow dr in ddl_VICOPS_Read.Tables[0].Rows) ReadCD += Convert.ToInt32(dr[0].ToString());
            foreach (DataRow dr in ddl_VICOPS_Total.Tables[0].Rows) TotalCD += Convert.ToInt32(dr[0].ToString());
        }

        if (wCount == 0 && vCount == 0)
        {
            gcTL_MRD.Value = GetImageURL(TrafficLightColor2.Grey);
            gcTL_MRD.ToolTip = "No Sites allocated for company.";
            //hlTL_MRD.Text = "View";
            hlTL_MRD.Text = "n/a";
            hlTL_MRD.NavigateUrl = "";
        }
        else
        {
            UnreadCD = TotalCD - ReadCD;
            if (UnreadCD > 0)
            {
                gcTL_MRD.Value = GetImageURL(TrafficLightColor2.Red);
                hlTL_MRD.Text = "Read";
            }
            else
            {
                gcTL_MRD.Value = GetImageURL(TrafficLightColor2.Green);
                hlTL_MRD.Text = "View";
            }
        }
        //lblTotalCD.Text = TotalCD.ToString();
        //lblUnreadCD.Text = UnreadCD.ToString();

        if (auth.RoleId == (int)RoleList.Contractor)
        {
            hlTL_MRD.NavigateUrl = "~/APSS_Top9.aspx";

            if (wCount == 0 && vCount > 0)
            {
                hlTL_MRD.NavigateUrl = "~/APSS_Top9.aspx?Region=VIC";
            }
            if (wCount > 0 && vCount == 0)
            {
                hlTL_MRD.NavigateUrl = "~/APSS_Top9.aspx";
            }
            if (wCount > 0 && vCount > 0)
            {
                hlTL_MRD.NavigateUrl = "~/APSS_Top9.aspx?i=1";
            }
        }
        else
        {
            hlTL_MRD.NavigateUrl = "~/APSS_Top9_Company.aspx" + QueryStringModule.Encrypt(String.Format("CompanyId={0}", CompanyId));
        }

        #endregion

        #region Compliance Summary Report

        #endregion

        count = 0;

        try
        {
            //CompanySiteCategoryStandardRegionService cscsrService = new CompanySiteCategoryStandardRegionService();
            //DataSet dsNonComplianceTable = cscsrService.NonComplianceTable(CompanyId, sYear);
            //if (dsNonComplianceTable.Tables[0] != null)
            //{
            //    ASPxGridView1.DataSource = dsNonComplianceTable.Tables[0];
            //    ASPxGridView1.DataBind();
            //}

            DataTable dtNonComplianceTable = new DataTable();
            dtNonComplianceTable.Columns.Add("CompanyId", typeof(int));
            dtNonComplianceTable.Columns.Add("SiteId", typeof(int));
            dtNonComplianceTable.Columns.Add("YearId", typeof(int));
            dtNonComplianceTable.Columns.Add("CategoryId", typeof(int));
            dtNonComplianceTable.Columns.Add("Radar", typeof(string));
            dtNonComplianceTable.Columns.Add("SiteName", typeof(string));
            dtNonComplianceTable.Columns.Add("SafetyComplianceScore_LastMonth", typeof(string));
            dtNonComplianceTable.Columns.Add("SafetyComplianceScore_Ytd", typeof(string));
            dtNonComplianceTable.Columns.Add("MandatedTraining_Ytd", typeof(string));
            dtNonComplianceTable.Columns.Add("Csa_LastQtr", typeof(string));
            dtNonComplianceTable.Columns.Add("Csa_CurrentQtr", typeof(string));
            dtNonComplianceTable.Columns.Add("LWDR", typeof(string));
            dtNonComplianceTable.Columns.Add("TRIR", typeof(string));
            dtNonComplianceTable.Columns.Add("AIFR", typeof(string));
            dtNonComplianceTable.Columns.Add("CompanySiteCategoryDesc", typeof(string));
            dtNonComplianceTable.Columns.Add("SiteAccessApproved", typeof(string));
            dtNonComplianceTable.Columns.Add("Comments", typeof(string));

            int Wcount = 0;
            int Vcount = 0;
            int Acount = 0;
            CompanySiteCategoryStandardRegionService cscsrService = new CompanySiteCategoryStandardRegionService();
            CompanySiteCategoryStandardRegionFilters cscsrFiltersWAO = new CompanySiteCategoryStandardRegionFilters();

            cscsrFiltersWAO.Append(CompanySiteCategoryStandardRegionColumn.CompanyId, CompanyId.ToString());
            cscsrFiltersWAO.Append(CompanySiteCategoryStandardRegionColumn.RegionId, "1"); //WAO

            CompanySiteCategoryStandardRegionFilters cscsrFiltersVICOPS = new CompanySiteCategoryStandardRegionFilters();
            cscsrFiltersVICOPS.Append(CompanySiteCategoryStandardRegionColumn.CompanyId, CompanyId.ToString());
            cscsrFiltersVICOPS.Append(CompanySiteCategoryStandardRegionColumn.RegionId, "4"); //VIC OPS

            CompanySiteCategoryStandardRegionFilters cscsrFiltersARP = new CompanySiteCategoryStandardRegionFilters();
            cscsrFiltersARP.Append(CompanySiteCategoryStandardRegionColumn.CompanyId, CompanyId.ToString());
            cscsrFiltersARP.Append(CompanySiteCategoryStandardRegionColumn.RegionId, "7"); //ARP

            VList<CompanySiteCategoryStandardRegion> cscsrWAO = cscsrService.GetPaged(cscsrFiltersWAO.ToString(), "SiteName ASC", 0, 100, out Wcount);
            VList<CompanySiteCategoryStandardRegion> cscsrVICOPS = cscsrService.GetPaged(cscsrFiltersVICOPS.ToString(), "SiteName ASC", 0, 100, out Vcount);
            VList<CompanySiteCategoryStandardRegion> cscsrARP = cscsrService.GetPaged(cscsrFiltersARP.ToString(), "SiteName ASC", 0, 100, out Acount);


            hlRadar.NavigateUrl = "~/Radar.aspx" + QueryStringModule.Encrypt(String.Format("CompanyId={0}&Year={1}", CompanyId, sYear));

            //WAO
            if (Wcount > 0)
            {
                foreach (CompanySiteCategoryStandardRegion cscsr in cscsrWAO)
                {
                    if (cscsr.SiteAbbrev != "HUN" && cscsr.SiteAbbrev != "WDL")
                    {
                        var site = SiteService.Get(e => e.SiteId == cscsr.SiteId, null);

                        if (cscsr.CompanySiteCategoryId != null)
                            dtNonComplianceTable.Rows.Add(AddRow(dtNonComplianceTable.NewRow(), Convert.ToInt32(cscsr.CompanySiteCategoryId), site, CompanyId, sYear));
                    }
                }
            }

            //MINE Sites
            if (Wcount > 0)
            {
                foreach (CompanySiteCategoryStandardRegion cscsr in cscsrWAO)
                {
                    if (cscsr.SiteAbbrev == "HUN" || cscsr.SiteAbbrev == "WDL")
                    {
                        var site = SiteService.Get(e => e.SiteId == cscsr.SiteId, null);

                        if (cscsr.CompanySiteCategoryId != null)
                            dtNonComplianceTable.Rows.Add(AddRow(dtNonComplianceTable.NewRow(), Convert.ToInt32(cscsr.CompanySiteCategoryId), site, CompanyId, sYear));
                    }
                }
            }

            //VIC OPS
            if (Vcount > 0)
            {
                foreach (CompanySiteCategoryStandardRegion cscsr in cscsrVICOPS)
                {
                    var site = SiteService.Get(e => e.SiteId == cscsr.SiteId, null);

                    if (cscsr.CompanySiteCategoryId != null)
                        dtNonComplianceTable.Rows.Add(AddRow(dtNonComplianceTable.NewRow(), Convert.ToInt32(cscsr.CompanySiteCategoryId), site, CompanyId, sYear));
                }
            }

            //ARP
            if (Acount > 0)
            {
                foreach (CompanySiteCategoryStandardRegion cscsr in cscsrARP)
                {
                    var site = SiteService.Get(e => e.SiteId == cscsr.SiteId, null);

                    if (cscsr.CompanySiteCategoryId != null)
                        dtNonComplianceTable.Rows.Add(AddRow(dtNonComplianceTable.NewRow(), Convert.ToInt32(cscsr.CompanySiteCategoryId), site, CompanyId, sYear));
                }
            }

            //-------------------Spec#4----------------------------------
            AnnualTargetsService atService = new AnnualTargetsService();
            AnnualTargetsFilters atFilters = new AnnualTargetsFilters();
            //atFilters.Append(AnnualTargetsColumn.CompanySiteCategoryId, CategoryId.ToString());
            TList<AnnualTargets> atTlist = atService.GetPaged("", "Year DESC", 0, 1000, out count);
            Session["AnnualTargetsSession"] = atTlist;
            //-------------------Spec#4----------------------------------
            ASPxGridView1.DataSource = dtNonComplianceTable;
            ASPxGridView1.DataBind();

            //viewing subcontractor
            if (c.CompanyStatus2Id == (int)CompanyStatus2List.SubContractor)
            {
                //hlTL_SQ.NavigateUrl = "";
                //hlTL_SQ.Text = "";

                gcTL_SMP.Value = GetImageURL(TrafficLightColor2.Grey);
                hlTL_SMP.NavigateUrl = "";
                hlTL_SMP.Text = "n/a";
                //hlTL_SMP.ForeColor = System.Drawing.Color.Silver;
                lblTL_SMP.Text = "Safety Management Plan";

                gcTL_CC.Value = GetImageURL(TrafficLightColor2.Grey);
                hlTL_CC.NavigateUrl = "";
                hlTL_CC.Text = "n/a";
                //hlTL_CC.ForeColor = System.Drawing.Color.Silver;

                gcTL_MRD.Value = GetImageURL(TrafficLightColor2.Grey);
                hlTL_MRD.NavigateUrl = "";
                hlTL_MRD.Text = "n/a";
                //hlTL_MRD.ForeColor = System.Drawing.Color.Silver;
                hlCSR.Text = "";
                hlCSR.NavigateUrl = "";


                VisibleControls(false);
            }
        }
        catch (Exception ex)
        {
            Elmah.ErrorSignal.FromContext(Context).Raise(ex);
        }
    }

    protected void ResetTrafficLights()
    {
        lblCompanyType.Text = "?";
        lblSupervisionRequirement.Text = "?";

        gcTL_SQ.Value = GetImageURL(TrafficLightColor2.Red);
        lblTL_SQ.Text = "Safety Qualification";

        hlTL_SC.NavigateUrl = "";
        hlTL_SC.Text = "";
        gcTL_SC.Value = GetImageURL(TrafficLightColor2.Red);
        lblTL_SC.Text = "Approval to engage Sub Contractors";

        gcTL_SMP.Value = GetImageURL(TrafficLightColor2.Red);
        lblTL_SMP.Text = "Safety Management Plan";

        gcTL_MRD.Value = GetImageURL(TrafficLightColor2.Red);
        lblTL_MRD.Text = "Critical Documents Have Been Reviewed";

        gcTL_CC.Value = GetImageURL(TrafficLightColor2.Red);
        lblTL_CC.Text = "Contractor Contacts";



        hlTL_CC.NavigateUrl = "~/cContacts.aspx";
        hlTL_CC.Text = "Update";
        hlCSR.NavigateUrl = "~/Reports_SummaryCompliance.aspx";
        hlCSR.Text = "View";
        lblCSR2.Text = "Compliance Summary Report";
        hlTL_SMP.NavigateUrl = "~/SafetyPlans.aspx";
        hlTL_SMP.Text = "View";

        //VisibleControls(true);
    }
    protected DataRow AddRow(DataRow dr, int CategoryId, Repo.CSMS.DAL.EntityModels.Site Site, int CompanyId, int Year)
    {
        DateTime dtNowMinusOneMonth = DateTime.Now.AddMonths(-1);
        Year = dtNowMinusOneMonth.Year;

        dr["CompanyId"] = CompanyId;
        dr["SiteId"] = Site.SiteId;
        dr["YearId"] = Year;
        dr["SiteName"] = Site.SiteName;
        dr["CategoryId"] = CategoryId;
        dr["Radar"] = "~/Radar.aspx" + QueryStringModule.Encrypt(String.Format("CompanyId={0}&SiteId={1}&Year={2}", CompanyId, Site.SiteId, Year));

        CompanySiteCategoryService cscService = new CompanySiteCategoryService();
        CompanySiteCategory csc = cscService.GetByCompanySiteCategoryId(CategoryId);

        CompanySiteCategoryStandardService cscsService = new CompanySiteCategoryStandardService();
        CompanySiteCategoryStandard cscs = cscsService.GetByCompanyIdSiteId(CompanyId, Site.SiteId);

        DataSet dsSummary = new DataSet();
        dsSummary.Tables.Add(Helper.Radar.CreateDataTable(CompanyId, Site.SiteId, 1, null));

        DataSet dsYtd = new DataSet();
        dsYtd.Tables.Add(Helper.Radar.CreateDataTable(CompanyId, Site.SiteId, 2, null));
        dsYtd.Tables.Add(Helper.Radar.CreateDataTable(CompanyId, Site.SiteId, 2, null));


        foreach (string KpiMeasure in Helper.Radar.GetKpiMeasurements(-1, false, true)) //Summary
        {
            dsSummary.Tables[0].Rows.Add(Helper.Radar.CreateDataRow_DataSet1_Summary(dsSummary.Tables[0], KpiMeasure,
                                                                                    CompanyId, Site.SiteId, 0, Year, csc.CompanySiteCategoryId));
        }
        foreach (string KpiMeasure in Helper.Radar.GetKpiMeasurements(CategoryId, false, false)) //Ytd
        {
            DataRow _dr = Helper.Radar.CreateDataRow_DataSet2_Ytd(dsYtd.Tables[0], KpiMeasure,
                                                                        CompanyId, Site, 0, dtNowMinusOneMonth.Year, csc);
            dsYtd.Tables[0].Rows.Add(_dr);

            DataRow _dr2 = Helper.Radar.CreateDataRow_DataSet2_Ytd(dsYtd.Tables[1], KpiMeasure,
                                                                        CompanyId, Site, dtNowMinusOneMonth.Month, dtNowMinusOneMonth.Year, csc);
            dsYtd.Tables[1].Rows.Add(_dr2);
        }

        int KpiMeasureCountYtd = 0;
        int KpiMeasureCountYtdCompliant = 0;
        int MandatedTrainingYtd = 0;
        int CsaCurrentQtr = 0;
        int CsaPreviousQtr = 0;

        bool OnSiteYtd = false;
        foreach (DataRow _dr in dsYtd.Tables[0].Rows)
        {
            string KpiMeasure = _dr["KpiMeasure"].ToString();
            if (KpiMeasure == "On site" || KpiMeasure == "Alcoa Annual Audit Score (%)" || KpiMeasure == "CSA - Self Audit (% Score) - Current Qtrxx" ||  //Exclude Current Qtr AG
                KpiMeasure == "Behavioural Observations - Plan" || KpiMeasure == "Behavioural Observations - Actual")
            {
                //ignore lol
                if (KpiMeasure == "On site")
                {
                    for (int i = 1; i <= 12; i++)
                    {
                        if (_dr[String.Format("KpiScoreMonth{0}", i)].ToString() == "Yes") OnSiteYtd = true;
                    }
                }
            }
            else
            {
                if (KpiMeasure != "CSA - Self Audit (% Score) - Previous Qtr") //Exclude Previous quarter as now calcuating on current quarter.  AG
                {
                    KpiMeasureCountYtd++;
                    if (_dr["KpiScoreYtdPercentage"].ToString() == "100")
                    {
                        KpiMeasureCountYtdCompliant++;
                    }
                }
                if (KpiMeasure == "CSA - Self Audit (% Score) - Current Qtr")
                {
                    Int32.TryParse(_dr["KpiScoreYtd"].ToString(), out CsaCurrentQtr);
                }
                else if (KpiMeasure == "CSA - Self Audit (% Score) - Previous Qtr")
                {
                    Int32.TryParse(_dr["KpiScoreYtd"].ToString(), out CsaPreviousQtr);
                    //Added by Ashley Goldstraw DT422 to show results by half for NE1 16/12/2015
                    if (CategoryId == (int)CompanySiteCategoryList.NE1)
                    {
                        int year = 0;
                        int qtr = 0;
                        DateTime dtNow = DateTime.Now;
                        if (dtNow.Month >= 7)
                        {
                            year = dtNow.Year;
                            qtr = 1;
                        }
                        else 
                        {
                            year = dtNow.Year - 1;
                            qtr = 3;
                        }


                        string score = Helper.Radar.GetCsaScore(CompanyId, Site.SiteId, qtr, year);
                        Int32.TryParse(score, out CsaPreviousQtr);
                    }
                    //End Added by Ashley Goldstraw DT422
                }
                else if (KpiMeasure == "% Mandated Training")
                {
                    Int32.TryParse(_dr["KpiScoreYtd"].ToString(), out MandatedTrainingYtd);
                }
            }
        }
        Decimal KpiYtdCompliantPercentage = Decimal.Round((Convert.ToDecimal(KpiMeasureCountYtdCompliant) / Convert.ToDecimal(KpiMeasureCountYtd)) * 100);

        bool OnSiteMonth = false;
        int KpiMeasureCountMonth = 0;
        int KpiMeasureCountMonthCompliant = 0;
        foreach (DataRow _dr in dsYtd.Tables[1].Rows)
        {
            string KpiMeasure = _dr["KpiMeasure"].ToString();
            if (KpiMeasure == "On site" || KpiMeasure == "Alcoa Annual Audit Score (%)" || KpiMeasure == "CSA - Self Audit (% Score) - Previous Qtr" || //Change to Previous Qtr AG DT318
                KpiMeasure == "Behavioural Observations - Plan" || KpiMeasure == "Behavioural Observations - Actual")
            {
                //ignore lol
                if (KpiMeasure == "On site")
                {
                    if (_dr[String.Format("KpiScoreMonth{0}", dtNowMinusOneMonth.Month)].ToString() == "Yes") OnSiteMonth = true;
                }
            }
            else
            {
                KpiMeasureCountMonth++;
                if (_dr[String.Format("KpiScoreMonth{0}Percentage", dtNowMinusOneMonth.Month)].ToString() == "100")
                {
                    KpiMeasureCountMonthCompliant++;
                }
            }
        }
        Decimal KpiMonthCompliantPercentage = Decimal.Round((Convert.ToDecimal(KpiMeasureCountMonthCompliant) / Convert.ToDecimal(KpiMeasureCountMonth)) * 100);

        string LWDR = "0";
        string TRIR = "0";
        string AIFR = "0";
        foreach (DataRow _dr in dsSummary.Tables[0].Rows)
        {
            switch (_dr["KpiMeasure"].ToString())
            {
                case "LWDFR":
                    LWDR = _dr["KpiScoreYtd"].ToString();
                    break;
                case "TRIFR":
                    TRIR = _dr["KpiScoreYtd"].ToString();
                    break;
                case "AIFR":
                    AIFR = _dr["KpiScoreYtd"].ToString();
                    break;
                default:
                    break;
            }
        }

        string SiteApproved = "Tentative";
        if (cscs.Approved == true)
        {
            SiteApproved = "Yes";
        }
        else if (cscs.Approved == false)
        {
            SiteApproved = "No";
        }

        dr["SafetyComplianceScore_LastMonth"] = (Convert.ToInt32(KpiMonthCompliantPercentage)).ToString();
        dr["SafetyComplianceScore_Ytd"] = (Convert.ToInt32(KpiYtdCompliantPercentage)).ToString();
        dr["MandatedTraining_Ytd"] = MandatedTrainingYtd.ToString();
        dr["Csa_LastQtr"] = CsaPreviousQtr.ToString();
        dr["Csa_CurrentQtr"] = CsaCurrentQtr.ToString();

        if (CsaPreviousQtr.ToString() == "0" && CategoryId == (int)CompanySiteCategoryList.NE3) dr["Csa_LastQtr"] = "n/a";
        if (CsaCurrentQtr.ToString() == "0" && CategoryId == (int)CompanySiteCategoryList.NE3) dr["Csa_CurrentQtr"] = "n/a";

        dr["LWDR"] = LWDR;
        dr["TRIR"] = TRIR;
        dr["AIFR"] = AIFR;

        //CompaniesService cService = new CompaniesService();
        //Companies c = cService.GetByCompanyId(CompanyId);
        //AG Get the first day of the month as all the exceptions are set to the first day of the month
        DateTime dtNowMinusOneMonthFirstDay = new DateTime(dtNowMinusOneMonth.Year, dtNowMinusOneMonth.Month, 1);

        repo.Company c = companyService.Get(i => i.CompanyId == CompanyId, null);

        //AG Get any exceptions that are in the current year.
        List<repo.CompanySiteCategoryException> csceList = companySiteCategoryExceptionService.GetMany(null, i => i.CompanyId == CompanyId && i.SiteId == Site.SiteId && (i.MonthYearFrom.Year == Year || i.MonthYearTo.Year == Year), o => o.OrderBy(m => m.MonthYearFrom), null);

        //AG If there are any exceptions, add a * on the end of the residential category name
        String suffix = (csceList.Count > 0) ? "*" : "";
        String category = csc.CategoryDesc;  // This is the default category for teh company / site.
        dr["Comments"] += csc.CategoryDesc + " Default\n";

        //AG Loop through the exceptions that exist for the current year
        foreach (repo.CompanySiteCategoryException csce in csceList)
        {
            //AG If the previous month is within an exception date, then change the Residential category to reflect what it is for that month.
            if (dtNowMinusOneMonthFirstDay >= csce.MonthYearFrom && dtNowMinusOneMonthFirstDay <= csce.MonthYearTo)
            {
                category = csce.CompanySiteCategory.CategoryDesc;
            }
            //AG Add any exception to the comments field of the data table so that it can be used later in the popup
            dr["Comments"] += String.Format("{0} {1}-{2}{3}", csce.CompanySiteCategory.CategoryDesc, csce.MonthYearFrom.ToString("MMM yyyy"), csce.MonthYearTo.ToString("MMM yyyy"), "\n");
        }
        //AG now that we have decided on the Residential category, set this in the data row for this company / site in the datatable.
        dr["CompanySiteCategoryDesc"] = String.Format("{0}{1}", category, suffix);

        dr["SiteAccessApproved"] = SiteApproved;

        if (!OnSiteYtd)
        {
            dr["SafetyComplianceScore_Ytd"] = "-";
            dr["MandatedTraining_Ytd"] = "-";
            dr["Csa_CurrentQtr"] = "-";
            dr["LWDR"] = "-";
            dr["TRIR"] = "-";
            dr["AIFR"] = "-";
        }

        if (!OnSiteMonth)
        {
            dr["SafetyComplianceScore_LastMonth"] = "-";
        }


        return dr;
    }

    protected void grid_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {
        if (e.RowType != GridViewRowType.Data) return;

        if (e.Row.Cells.Count < 10)
        {
            //Sub Contractor
            string colour = "";
            colour = "Yellow";
            string SiteAccessApproved = (string)ASPxGridView1.GetRowValues(e.VisibleIndex, "SiteAccessApproved");
            if (SiteAccessApproved == "Yes")
            {
                colour = CellColourGood;
            }
            else if (SiteAccessApproved == "No")
            {
                colour = CellColourBad;
            }
            e.Row.Cells[2].Style.Add("background-color", colour);

        }
        else
        {
            //Contractor
            int SiteId = (int)ASPxGridView1.GetRowValues(e.VisibleIndex, "SiteId");
            int count = 0;

            int CategoryId = (int)ASPxGridView1.GetRowValues(e.VisibleIndex, "CategoryId");
            AnnualTargetsService atService = new AnnualTargetsService();
            AnnualTargetsFilters atFilters = new AnnualTargetsFilters();
            atFilters.Append(AnnualTargetsColumn.CompanySiteCategoryId, CategoryId.ToString());
            TList<AnnualTargets> atTlist = atService.GetPaged(atFilters.ToString(), "Year DESC", 0, 1000, out count);

            //1
            string colour = "";
            string _KpiScoreLastMonth = (string)ASPxGridView1.GetRowValues(e.VisibleIndex, "SafetyComplianceScore_LastMonth");
            if (_KpiScoreLastMonth == "-")
            {
                colour = "";
            }
            else
            {
                if (_KpiScoreLastMonth == "100") colour = CellColourGood; else colour = CellColourBad;
            }
            e.Row.Cells[1].Style.Add("background-color", colour);

            //2
            colour = "";
            string _KpiScoreYtd = (string)ASPxGridView1.GetRowValues(e.VisibleIndex, "SafetyComplianceScore_Ytd");
            if (_KpiScoreYtd == "-")
            {
                colour = "";
            }
            else
            {
                if (_KpiScoreYtd == "100") colour = CellColourGood; else colour = CellColourBad;
            }
            e.Row.Cells[2].Style.Add("background-color", colour);

            //3
            colour = "";

            int MtTarget = 100;
            Int32.TryParse(atTlist[0].Training.ToString(), out MtTarget);

            string MandatedTrainingYtd = (string)ASPxGridView1.GetRowValues(e.VisibleIndex, "MandatedTraining_Ytd");
            int _MandatedTrainingYtd = 0;

            if (MandatedTrainingYtd == "-")
            {
                colour = "";
            }
            else
            {
                Int32.TryParse(MandatedTrainingYtd, out _MandatedTrainingYtd);
                if (_MandatedTrainingYtd >= MtTarget) colour = CellColourGood; else colour = CellColourBad;
            }
            e.Row.Cells[3].Style.Add("background-color", colour);

            //-------------------Spec#4----------------------------------
            var atTListSession = (TList<AnnualTargets>)Session["AnnualTargetsSession"];

            //Start:Change by Debashis for Tooltip issue       
            //if (((string)ASPxGridView1.GetRowValues(e.VisibleIndex, "CompanySiteCategoryDesc")).Trim() == "Embedded")
            //{
            //    Session["ToolTipValueEmbedded"] = atTListSession.Where(c => c.CompanySiteCategoryId == 1).OrderByDescending(y => y.Year).Select(x => x.Training).FirstOrDefault();
            //}

            //if (((string)ASPxGridView1.GetRowValues(e.VisibleIndex, "CompanySiteCategoryDesc")).Trim() == "Non-Embedded 1")
            //{
            //    Session["ToolTipValueNonEmbedded1"] = atTListSession.Where(c => c.CompanySiteCategoryId == 2).OrderByDescending(y => y.Year).Select(x => x.Training).FirstOrDefault();
            //}

            //if (((string)ASPxGridView1.GetRowValues(e.VisibleIndex, "CompanySiteCategoryDesc")).Trim() == "Non-Embedded 2")
            //{
            //    Session["ToolTipValueNonEmbedded2"] = atTListSession.Where(c => c.CompanySiteCategoryId == 3).OrderByDescending(y => y.Year).Select(x => x.Training).FirstOrDefault();
            //}

            //if (((string)ASPxGridView1.GetRowValues(e.VisibleIndex, "CompanySiteCategoryDesc")).Trim() == "Non-Embedded 3")
            //{
            //    Session["ToolTipValueNonEmbedded3"] = atTListSession.Where(c => c.CompanySiteCategoryId == 4).OrderByDescending(y => y.Year).Select(x => x.Training).FirstOrDefault();
            //}


            Session["ToolTipValueEmbedded"] = atTListSession.Where(c => c.CompanySiteCategoryId == 1).OrderByDescending(y => y.Year).Select(x => x.Training).FirstOrDefault();
            Session["ToolTipValueNonEmbedded1"] = atTListSession.Where(c => c.CompanySiteCategoryId == 2).OrderByDescending(y => y.Year).Select(x => x.Training).FirstOrDefault();
            Session["ToolTipValueNonEmbedded2"] = atTListSession.Where(c => c.CompanySiteCategoryId == 3).OrderByDescending(y => y.Year).Select(x => x.Training).FirstOrDefault();
            Session["ToolTipValueNonEmbedded3"] = atTListSession.Where(c => c.CompanySiteCategoryId == 4).OrderByDescending(y => y.Year).Select(x => x.Training).FirstOrDefault();
            //End:Change by Debashis for Tooltip issue

            String MandatedTrainingCaption = "";
            String MandatedTrainingToolTip = "";

            if (ASPxGridView1.VisibleRowCount - 1 == e.VisibleIndex)
            {
                var LowestVal = GetLowestValue(Convert.ToInt32(Session["ToolTipValueEmbedded"]), Convert.ToInt32(Session["ToolTipValueNonEmbedded1"]), Convert.ToInt32(Session["ToolTipValueNonEmbedded2"]), Convert.ToInt32(Session["ToolTipValueNonEmbedded3"])).ToString();

                if (AreAllValuesEqual(Convert.ToInt32(Session["ToolTipValueEmbedded"]), Convert.ToInt32(Session["ToolTipValueNonEmbedded1"]), Convert.ToInt32(Session["ToolTipValueNonEmbedded2"]), Convert.ToInt32(Session["ToolTipValueNonEmbedded3"])))
                {
                    MandatedTrainingCaption = "Mandated Training (Plan: " + LowestVal + ")";
                    //Start:Change by Debashis for Tooltip issue
                    MandatedTrainingToolTip = "Embedded: " + LowestVal + "\n";
                    MandatedTrainingToolTip += "Non-Embedded 1: " + LowestVal + "\n";
                    MandatedTrainingToolTip += "Non-Embedded 2: " + LowestVal + "\n";
                    MandatedTrainingToolTip += "Non-Embedded 3: " + LowestVal + "\n";
                    //End:Change by Debashis for Tooltip issue

                }
                else
                {
                    MandatedTrainingToolTip = "Embedded: " + Convert.ToString(Session["ToolTipValueEmbedded"]) + "\n";
                    MandatedTrainingToolTip += "Non-Embedded 1: " + Convert.ToString(Session["ToolTipValueNonEmbedded1"]) + "\n";
                    MandatedTrainingToolTip += "Non-Embedded 2: " + Convert.ToString(Session["ToolTipValueNonEmbedded2"]) + "\n";
                    MandatedTrainingToolTip += "Non-Embedded 3: " + Convert.ToString(Session["ToolTipValueNonEmbedded3"]) + "\n";

                    //ASPxGridView1.Columns["MandatedTraining_Ytd"].ToolTip = MandatedTrainingToolTip;
                    MandatedTrainingCaption = "Mandated Training (Plan:> " + LowestVal + "*)";
                }

                ASPxGridView1.Columns["MandatedTraining_Ytd"].Caption = MandatedTrainingCaption;
                ASPxGridView1.Columns["MandatedTraining_Ytd"].ToolTip = MandatedTrainingToolTip;
                ASPxGridView1.Columns["Csa_LastQtr"].ToolTip = "Embedded - Previous Qtr\nNon Embedded 1 - Previous Half";
                Session["ToolTipValueEmbedded"] = null;
                Session["ToolTipValueNonEmbedded1"] = null;
                Session["ToolTipValueNonEmbedded2"] = null;
                Session["ToolTipValueNonEmbedded3"] = null;
            }
            //-------------------Spec#4----------------------------------

            //4
            colour = "";

            int CsaTarget = 100;
            Int32.TryParse(atTlist[0].QuarterlyAuditScore.ToString(), out CsaTarget);

            string CsaLastQtr = (string)ASPxGridView1.GetRowValues(e.VisibleIndex, "Csa_LastQtr");
            int _CsaLastQtr = 0;

            if (CsaLastQtr == "-" || CsaLastQtr == "n/a")
            {
                colour = "";
            }
            else
            {
                Int32.TryParse(CsaLastQtr, out _CsaLastQtr);
                if (_CsaLastQtr >= CsaTarget) colour = CellColourGood; else colour = CellColourBad;
            }
            e.Row.Cells[4].Style.Add("background-color", colour);

            //5
            colour = "Yellow";
            string SiteAccessApproved = (string)ASPxGridView1.GetRowValues(e.VisibleIndex, "SiteAccessApproved");
            if (SiteAccessApproved == "Yes")
            {
                colour = CellColourGood;
            }
            else if (SiteAccessApproved == "No")
            {
                colour = CellColourBad;
            }
            if (e.Row.Cells.Count > 10)
            {
                e.Row.Cells[10].Style.Add("background-color", colour);
            }


            //Radar
            HyperLink hl = ASPxGridView1.FindRowCellTemplateControl(e.VisibleIndex, null, "hlRadar") as HyperLink;
           
            if (ASPxGridView1.GetRowValues(e.VisibleIndex, "Radar") != DBNull.Value && hl != null)
                //DT228 Cindi Thornton 23/9/15 - don't set a url if we want the read only form
                if (preloadCompanyId)
                    hl.NavigateUrl = "";
                else
                //End DT228
                {
                    hl.NavigateUrl = (string)ASPxGridView1.GetRowValues(e.VisibleIndex, "Radar");
                }
        }
    }

    protected void VisibleControls(bool yesno)
    {
        ASPxGridView1.Columns["Month Safety Compliance Score"].Visible = yesno;
        ASPxGridView1.Columns["KPI Compliance Radar"].Visible = yesno;
        ASPxGridView1.Columns["Safety Statistics"].Visible = yesno;

        ASPxGridView1.Columns["SafetyComplianceScore_LastMonth"].Visible = yesno;
        ASPxGridView1.Columns["SafetyComplianceScore_Ytd"].Visible = yesno;
        ASPxGridView1.Columns["MandatedTraining_Ytd"].Visible = yesno;
        ASPxGridView1.Columns["Csa_LastQtr"].Visible = yesno;
        //ASPxGridView1.Columns[" "].Visible = yesno;
        ASPxGridView1.Columns["LWDR"].Visible = yesno;
        ASPxGridView1.Columns["TRIR"].Visible = yesno;
        ASPxGridView1.Columns["AIFR"].Visible = yesno;

        btnEnterKpi.Visible = yesno;
    }

    //-------------------Spec#4----------------------------------
    private int GetLowestValue(int? ToolTipValueEmbadded, int? ToolTipValueNonEmbedded1, int? ToolTipValueNonEmbedded2, int? ToolTipValueNonEmbedded3)
    {
        List<int?> all = new List<int?>();
        all.Add(ToolTipValueEmbadded);
        all.Add(ToolTipValueNonEmbedded1);
        all.Add(ToolTipValueNonEmbedded2);
        all.Add(ToolTipValueNonEmbedded3);

        all.Sort();
        int? MyValue = all.FirstOrDefault() == null ? 0 : all.FirstOrDefault();
        return (MyValue.HasValue ? MyValue.Value : 0);
    }
    private bool AreAllValuesEqual(int? ToolTipValueEmbadded, int? ToolTipValueNonEmbedded1, int? ToolTipValueNonEmbedded2, int? ToolTipValueNonEmbedded3)
    {
        List<int?> all = new List<int?>();
        all.Add(ToolTipValueEmbadded);
        all.Add(ToolTipValueNonEmbedded1);
        all.Add(ToolTipValueNonEmbedded2);
        all.Add(ToolTipValueNonEmbedded3);

        List<int?> Final = all.Distinct().ToList();
        if (Final.Count > 1)
        { return false; }
        else { return true; }
    }



    protected void ASPxGridView1_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
    {
        if (e.DataColumn.FieldName == "CompanySiteCategoryDesc")
        {
            if (e.CellValue != null)
            {
                e.Cell.ToolTip = ASPxGridView1.GetRowValues(e.VisibleIndex, "Comments").ToString();
            }
        }
        //Added by AShley Goldstraw to handle NE1 only reporting CSA each half.  DT422 16/12/2015
        if (e.DataColumn.FieldName == "Csa_LastQtr")
        {
            if (e.CellValue != null)
            {
                string csc = ASPxGridView1.GetRowValues(e.VisibleIndex, "CompanySiteCategoryDesc").ToString();
                if (csc.Contains("Non-Embedded 1"))
                {
                    e.Cell.ToolTip = "Non-Embedded 1 - Previous Half";
                }
                else if (csc.Contains("Embedded") && csc.Contains("Non") == false)
                { 
                    e.Cell.ToolTip = "Embedded - Previous Quarter"; 
                }
            }
        }


    }
    //-------------------Spec#4----------------------------------
}
