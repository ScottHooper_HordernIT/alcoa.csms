using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Services;
using KaiZen.CSMS.Web;
using System.Drawing;

public partial class UserControls_Tiny_QuestionnaireReports_QuestionnaireReport_Services : System.Web.UI.UserControl
{
    Auth auth = new Auth();
    protected void Page_Load(object sender, EventArgs e) {  Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); }

    private void moduleInit(bool postBack)
    {
        Control uc = LoadControl("~/UserControls/Main/SqReports/QuestionnaireReport_Services_Other.ascx"); ;
        phOther.Controls.Add(uc);

        Control uc2 = LoadControl("~/UserControls/Main/SqReports/QuestionnaireReport_Services_Main.ascx"); ;
        phMain.Controls.Add(uc2);
    }


}
