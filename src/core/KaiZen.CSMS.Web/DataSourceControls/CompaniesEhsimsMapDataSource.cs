﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.CompaniesEhsimsMapProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(CompaniesEhsimsMapDataSourceDesigner))]
	public class CompaniesEhsimsMapDataSource : ProviderDataSource<CompaniesEhsimsMap, CompaniesEhsimsMapKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompaniesEhsimsMapDataSource class.
		/// </summary>
		public CompaniesEhsimsMapDataSource() : base(new CompaniesEhsimsMapService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the CompaniesEhsimsMapDataSourceView used by the CompaniesEhsimsMapDataSource.
		/// </summary>
		protected CompaniesEhsimsMapDataSourceView CompaniesEhsimsMapView
		{
			get { return ( View as CompaniesEhsimsMapDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the CompaniesEhsimsMapDataSource control invokes to retrieve data.
		/// </summary>
		public CompaniesEhsimsMapSelectMethod SelectMethod
		{
			get
			{
				CompaniesEhsimsMapSelectMethod selectMethod = CompaniesEhsimsMapSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (CompaniesEhsimsMapSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the CompaniesEhsimsMapDataSourceView class that is to be
		/// used by the CompaniesEhsimsMapDataSource.
		/// </summary>
		/// <returns>An instance of the CompaniesEhsimsMapDataSourceView class.</returns>
		protected override BaseDataSourceView<CompaniesEhsimsMap, CompaniesEhsimsMapKey> GetNewDataSourceView()
		{
			return new CompaniesEhsimsMapDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the CompaniesEhsimsMapDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class CompaniesEhsimsMapDataSourceView : ProviderDataSourceView<CompaniesEhsimsMap, CompaniesEhsimsMapKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompaniesEhsimsMapDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the CompaniesEhsimsMapDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public CompaniesEhsimsMapDataSourceView(CompaniesEhsimsMapDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal CompaniesEhsimsMapDataSource CompaniesEhsimsMapOwner
		{
			get { return Owner as CompaniesEhsimsMapDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal CompaniesEhsimsMapSelectMethod SelectMethod
		{
			get { return CompaniesEhsimsMapOwner.SelectMethod; }
			set { CompaniesEhsimsMapOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal CompaniesEhsimsMapService CompaniesEhsimsMapProvider
		{
			get { return Provider as CompaniesEhsimsMapService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<CompaniesEhsimsMap> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<CompaniesEhsimsMap> results = null;
			CompaniesEhsimsMap item;
			count = 0;
			
			System.Int32 _ehssimsMapId;
			System.String _contCompanyCode;
			System.Int32 _siteId;
			System.Int32 _companyId;

			switch ( SelectMethod )
			{
				case CompaniesEhsimsMapSelectMethod.Get:
					CompaniesEhsimsMapKey entityKey  = new CompaniesEhsimsMapKey();
					entityKey.Load(values);
					item = CompaniesEhsimsMapProvider.Get(entityKey);
					results = new TList<CompaniesEhsimsMap>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case CompaniesEhsimsMapSelectMethod.GetAll:
                    results = CompaniesEhsimsMapProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case CompaniesEhsimsMapSelectMethod.GetPaged:
					results = CompaniesEhsimsMapProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case CompaniesEhsimsMapSelectMethod.Find:
					if ( FilterParameters != null )
						results = CompaniesEhsimsMapProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = CompaniesEhsimsMapProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case CompaniesEhsimsMapSelectMethod.GetByEhssimsMapId:
					_ehssimsMapId = ( values["EhssimsMapId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["EhssimsMapId"], typeof(System.Int32)) : (int)0;
					item = CompaniesEhsimsMapProvider.GetByEhssimsMapId(_ehssimsMapId);
					results = new TList<CompaniesEhsimsMap>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				case CompaniesEhsimsMapSelectMethod.GetByContCompanyCodeSiteId:
					_contCompanyCode = ( values["ContCompanyCode"] != null ) ? (System.String) EntityUtil.ChangeType(values["ContCompanyCode"], typeof(System.String)) : string.Empty;
					_siteId = ( values["SiteId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["SiteId"], typeof(System.Int32)) : (int)0;
					item = CompaniesEhsimsMapProvider.GetByContCompanyCodeSiteId(_contCompanyCode, _siteId);
					results = new TList<CompaniesEhsimsMap>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// FK
				case CompaniesEhsimsMapSelectMethod.GetByCompanyId:
					_companyId = ( values["CompanyId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["CompanyId"], typeof(System.Int32)) : (int)0;
					results = CompaniesEhsimsMapProvider.GetByCompanyId(_companyId, this.StartIndex, this.PageSize, out count);
					break;
				case CompaniesEhsimsMapSelectMethod.GetBySiteId:
					_siteId = ( values["SiteId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["SiteId"], typeof(System.Int32)) : (int)0;
					results = CompaniesEhsimsMapProvider.GetBySiteId(_siteId, this.StartIndex, this.PageSize, out count);
					break;
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == CompaniesEhsimsMapSelectMethod.Get || SelectMethod == CompaniesEhsimsMapSelectMethod.GetByEhssimsMapId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				CompaniesEhsimsMap entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					CompaniesEhsimsMapProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<CompaniesEhsimsMap> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			CompaniesEhsimsMapProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region CompaniesEhsimsMapDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the CompaniesEhsimsMapDataSource class.
	/// </summary>
	public class CompaniesEhsimsMapDataSourceDesigner : ProviderDataSourceDesigner<CompaniesEhsimsMap, CompaniesEhsimsMapKey>
	{
		/// <summary>
		/// Initializes a new instance of the CompaniesEhsimsMapDataSourceDesigner class.
		/// </summary>
		public CompaniesEhsimsMapDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public CompaniesEhsimsMapSelectMethod SelectMethod
		{
			get { return ((CompaniesEhsimsMapDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new CompaniesEhsimsMapDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region CompaniesEhsimsMapDataSourceActionList

	/// <summary>
	/// Supports the CompaniesEhsimsMapDataSourceDesigner class.
	/// </summary>
	internal class CompaniesEhsimsMapDataSourceActionList : DesignerActionList
	{
		private CompaniesEhsimsMapDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the CompaniesEhsimsMapDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public CompaniesEhsimsMapDataSourceActionList(CompaniesEhsimsMapDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public CompaniesEhsimsMapSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion CompaniesEhsimsMapDataSourceActionList
	
	#endregion CompaniesEhsimsMapDataSourceDesigner
	
	#region CompaniesEhsimsMapSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the CompaniesEhsimsMapDataSource.SelectMethod property.
	/// </summary>
	public enum CompaniesEhsimsMapSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByEhssimsMapId method.
		/// </summary>
		GetByEhssimsMapId,
		/// <summary>
		/// Represents the GetByContCompanyCodeSiteId method.
		/// </summary>
		GetByContCompanyCodeSiteId,
		/// <summary>
		/// Represents the GetByCompanyId method.
		/// </summary>
		GetByCompanyId,
		/// <summary>
		/// Represents the GetBySiteId method.
		/// </summary>
		GetBySiteId
	}
	
	#endregion CompaniesEhsimsMapSelectMethod

	#region CompaniesEhsimsMapFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CompaniesEhsimsMap"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompaniesEhsimsMapFilter : SqlFilter<CompaniesEhsimsMapColumn>
	{
	}
	
	#endregion CompaniesEhsimsMapFilter

	#region CompaniesEhsimsMapExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CompaniesEhsimsMap"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompaniesEhsimsMapExpressionBuilder : SqlExpressionBuilder<CompaniesEhsimsMapColumn>
	{
	}
	
	#endregion CompaniesEhsimsMapExpressionBuilder	

	#region CompaniesEhsimsMapProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;CompaniesEhsimsMapChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="CompaniesEhsimsMap"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompaniesEhsimsMapProperty : ChildEntityProperty<CompaniesEhsimsMapChildEntityTypes>
	{
	}
	
	#endregion CompaniesEhsimsMapProperty
}

