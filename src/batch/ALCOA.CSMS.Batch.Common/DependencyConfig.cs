﻿using Autofac;
using Autofac.Integration.Mvc;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Service.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace ALCOA.CSMS.Batch.Common
{
    public class DependencyConfig
    {
        public static void RegisterDependencies(ContainerBuilder builder)
        {
            //Register services
            builder.RegisterAssemblyTypes(typeof(UserService).Assembly)
                .Where(t => t.Name.EndsWith("Service"))
                .AsImplementedInterfaces()
                .InstancePerDependency();

            builder.RegisterAssemblyTypes(typeof(ContextAdaptor).Assembly)
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();

            //Register entities
            builder.RegisterType<Entities>()
                .As<IDbContext>()
                .InstancePerLifetimeScope();

            //Register context
            builder.RegisterType<ContextAdaptor>()
                .As<IObjectSetFactory, IObjectContext>()
                .InstancePerLifetimeScope();

            //Register unit of work
            builder.RegisterType<UnitOfWork>()
                .As<IUnitOfWork>()
                .InstancePerDependency();

            //Register repository
            builder.RegisterGeneric(typeof(Repository<>))
                .As(typeof(IRepository<>))
                .InstancePerLifetimeScope();

            try
            {
                //Build autofac container
                var container = builder.Build();

                //Initialise lifetime scope for standalone contexts
                var lifetimeScopeProvider = new StubLifetimeScopeProvider(container);

                //Set the dependency resolver implementation.
                DependencyResolver.SetResolver(new AutofacDependencyResolver(container, lifetimeScopeProvider));

            }
            catch (Exception ex)
            {
                if (ex is ReflectionTypeLoadException)
                {
                    var typeLoadException = ex as ReflectionTypeLoadException;
                    var loaderExceptions = typeLoadException.LoaderExceptions;

                    foreach (var e in loaderExceptions)
                    {
                        Console.WriteLine("Loader Exception:: " + e.Message);
                    }

                    throw new AggregateException(typeLoadException.Message, loaderExceptions);
                }
                throw;
            }
        }
    }

    internal class StubLifetimeScopeProvider : ILifetimeScopeProvider
    {
        ILifetimeScope _lifetimeScope;
        readonly ILifetimeScope _container;

        public StubLifetimeScopeProvider(ILifetimeScope container)
        {
            _container = container;
        }

        public ILifetimeScope ApplicationContainer
        {
            get { return _container; }
        }

        public ILifetimeScope GetLifetimeScope(Action<ContainerBuilder> configurationAction)
        {
            return _lifetimeScope ?? (_lifetimeScope = BuildLifetimeScope(configurationAction));
        }

        public void EndLifetimeScope()
        {
            if (_lifetimeScope != null)
                _lifetimeScope.Dispose();
        }

        ILifetimeScope BuildLifetimeScope(Action<ContainerBuilder> configurationAction)
        {
            return (configurationAction == null)
                       ? _container.BeginLifetimeScope("httpRequest")
                       : _container.BeginLifetimeScope("httpRequest", configurationAction);
        }


        public ILifetimeScope GetLifetimeScope()
        {
            return GetLifetimeScope(null);
        }
    }
}
