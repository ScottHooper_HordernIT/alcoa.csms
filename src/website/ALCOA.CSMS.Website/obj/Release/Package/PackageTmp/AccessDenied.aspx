﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="AccessDenied" Codebehind="AccessDenied.aspx.cs" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <link rel="shortcut icon" href="./Images/favicon.ico" type="image/x-icon" />
    <title>Access Denied</title>
</head>
<body>
    <form id="form1" runat="server">
    <div style="text-align: center">
        <strong>
            <img border="0" height="33" src="Images/top_alcoa_logo_wide.gif" style="font-weight: bold"
                width="153" /><br />
        </strong>
        <div style="text-align: center">
            <table id="Table1" align="center" border="0" cellpadding="0" cellspacing="0" style="font-weight: bold"
                width="900">
                <tr>
                    <td align="right" style="height: 15px; text-align: center" valign="top">
                        <table border="0" cellpadding="0" cellspacing="0">
                            <tbody>
                                <tr>
                                    <td>
                                        <img height="3" src="Images/spacer.gif" /></td>
                                </tr>
                            </tbody>
                        </table>
                        <span style="color: #003399">Australian Operations</span></td>
                </tr>
                <tr>
                    <td align="right" style="text-align: center" valign="bottom">
                        <span class="sitetitleblue"><a class="sitetitleblue" href="default.aspx" onfocus="blur(this)">
                            Contractor Services Management System</a></span>
                    </td>
                </tr>
                <tr>
                    <td align="right" style="text-align: center; height: 13px;" valign="bottom">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td align="right" style="text-align: center; height: 64px;" valign="bottom">
                        <span style="font-size: 10pt; font-weight:normal;">
                            <asp:Panel ID="Panel1" runat="server" Height="100%" Width="100%">
        Your current security credentials are preventing you from accessing this page<br />
                                or an module within this page is not operating properly.<br />
                                <br />
        If you think that this is incorrect, please contact the <dxe:ASPxHyperLink ID="ASPxHyperLinkContact" runat="server" Text="AUA Alcoa Contractor Services Team"></dxe:ASPxHyperLink>.</asp:Panel>
                        </span>&nbsp;</td>
                </tr>
                <tr>
                    <td align="right" style="height: 15px; text-align: center" valign="bottom">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td align="right" style="height: 13px; text-align: center" valign="bottom">
                        <span style="font-size: 11pt; color: dimgray; text-decoration: underline">Details</span></td>
                </tr>
                <tr>
                    <td align="right" style="height: 13px; text-align: center" valign="bottom">
        <asp:Label ID="Label1" runat="server" Font-Bold="False" Font-Size="10pt" ForeColor="DimGray"></asp:Label></td>
                </tr>
            </table>
        </div>
    </div>
    </form>
</body>
</html>
