﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="CompanyStatusChangeApprovalProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class CompanyStatusChangeApprovalProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.CompanyStatusChangeApproval, KaiZen.CSMS.Entities.CompanyStatusChangeApprovalKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.CompanyStatusChangeApprovalKey key)
		{
			return Delete(transactionManager, key.CompanyStatusChangeApprovalId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_companyStatusChangeApprovalId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _companyStatusChangeApprovalId)
		{
			return Delete(null, _companyStatusChangeApprovalId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyStatusChangeApprovalId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _companyStatusChangeApprovalId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompanyStatusChangeApproval_RequestedByUserId key.
		///		FK_CompanyStatusChangeApproval_RequestedByUserId Description: 
		/// </summary>
		/// <param name="_requestedByUserId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanyStatusChangeApproval objects.</returns>
		public TList<CompanyStatusChangeApproval> GetByRequestedByUserId(System.Int32 _requestedByUserId)
		{
			int count = -1;
			return GetByRequestedByUserId(_requestedByUserId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompanyStatusChangeApproval_RequestedByUserId key.
		///		FK_CompanyStatusChangeApproval_RequestedByUserId Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_requestedByUserId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanyStatusChangeApproval objects.</returns>
		/// <remarks></remarks>
		public TList<CompanyStatusChangeApproval> GetByRequestedByUserId(TransactionManager transactionManager, System.Int32 _requestedByUserId)
		{
			int count = -1;
			return GetByRequestedByUserId(transactionManager, _requestedByUserId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompanyStatusChangeApproval_RequestedByUserId key.
		///		FK_CompanyStatusChangeApproval_RequestedByUserId Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_requestedByUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanyStatusChangeApproval objects.</returns>
		public TList<CompanyStatusChangeApproval> GetByRequestedByUserId(TransactionManager transactionManager, System.Int32 _requestedByUserId, int start, int pageLength)
		{
			int count = -1;
			return GetByRequestedByUserId(transactionManager, _requestedByUserId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompanyStatusChangeApproval_RequestedByUserId key.
		///		fkCompanyStatusChangeApprovalRequestedByUserId Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_requestedByUserId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanyStatusChangeApproval objects.</returns>
		public TList<CompanyStatusChangeApproval> GetByRequestedByUserId(System.Int32 _requestedByUserId, int start, int pageLength)
		{
			int count =  -1;
			return GetByRequestedByUserId(null, _requestedByUserId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompanyStatusChangeApproval_RequestedByUserId key.
		///		fkCompanyStatusChangeApprovalRequestedByUserId Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_requestedByUserId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanyStatusChangeApproval objects.</returns>
		public TList<CompanyStatusChangeApproval> GetByRequestedByUserId(System.Int32 _requestedByUserId, int start, int pageLength,out int count)
		{
			return GetByRequestedByUserId(null, _requestedByUserId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompanyStatusChangeApproval_RequestedByUserId key.
		///		FK_CompanyStatusChangeApproval_RequestedByUserId Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_requestedByUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanyStatusChangeApproval objects.</returns>
		public abstract TList<CompanyStatusChangeApproval> GetByRequestedByUserId(TransactionManager transactionManager, System.Int32 _requestedByUserId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompanyStatusChangeApproval_AssessedByUserId key.
		///		FK_CompanyStatusChangeApproval_AssessedByUserId Description: 
		/// </summary>
		/// <param name="_assessedByUserId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanyStatusChangeApproval objects.</returns>
		public TList<CompanyStatusChangeApproval> GetByAssessedByUserId(System.Int32? _assessedByUserId)
		{
			int count = -1;
			return GetByAssessedByUserId(_assessedByUserId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompanyStatusChangeApproval_AssessedByUserId key.
		///		FK_CompanyStatusChangeApproval_AssessedByUserId Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_assessedByUserId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanyStatusChangeApproval objects.</returns>
		/// <remarks></remarks>
		public TList<CompanyStatusChangeApproval> GetByAssessedByUserId(TransactionManager transactionManager, System.Int32? _assessedByUserId)
		{
			int count = -1;
			return GetByAssessedByUserId(transactionManager, _assessedByUserId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompanyStatusChangeApproval_AssessedByUserId key.
		///		FK_CompanyStatusChangeApproval_AssessedByUserId Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_assessedByUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanyStatusChangeApproval objects.</returns>
		public TList<CompanyStatusChangeApproval> GetByAssessedByUserId(TransactionManager transactionManager, System.Int32? _assessedByUserId, int start, int pageLength)
		{
			int count = -1;
			return GetByAssessedByUserId(transactionManager, _assessedByUserId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompanyStatusChangeApproval_AssessedByUserId key.
		///		fkCompanyStatusChangeApprovalAssessedByUserId Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_assessedByUserId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanyStatusChangeApproval objects.</returns>
		public TList<CompanyStatusChangeApproval> GetByAssessedByUserId(System.Int32? _assessedByUserId, int start, int pageLength)
		{
			int count =  -1;
			return GetByAssessedByUserId(null, _assessedByUserId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompanyStatusChangeApproval_AssessedByUserId key.
		///		fkCompanyStatusChangeApprovalAssessedByUserId Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_assessedByUserId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanyStatusChangeApproval objects.</returns>
		public TList<CompanyStatusChangeApproval> GetByAssessedByUserId(System.Int32? _assessedByUserId, int start, int pageLength,out int count)
		{
			return GetByAssessedByUserId(null, _assessedByUserId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompanyStatusChangeApproval_AssessedByUserId key.
		///		FK_CompanyStatusChangeApproval_AssessedByUserId Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_assessedByUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanyStatusChangeApproval objects.</returns>
		public abstract TList<CompanyStatusChangeApproval> GetByAssessedByUserId(TransactionManager transactionManager, System.Int32? _assessedByUserId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompanyStatusChangeApproval_CompanyId key.
		///		FK_CompanyStatusChangeApproval_CompanyId Description: 
		/// </summary>
		/// <param name="_companyId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanyStatusChangeApproval objects.</returns>
		public TList<CompanyStatusChangeApproval> GetByCompanyId(System.Int32 _companyId)
		{
			int count = -1;
			return GetByCompanyId(_companyId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompanyStatusChangeApproval_CompanyId key.
		///		FK_CompanyStatusChangeApproval_CompanyId Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanyStatusChangeApproval objects.</returns>
		/// <remarks></remarks>
		public TList<CompanyStatusChangeApproval> GetByCompanyId(TransactionManager transactionManager, System.Int32 _companyId)
		{
			int count = -1;
			return GetByCompanyId(transactionManager, _companyId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompanyStatusChangeApproval_CompanyId key.
		///		FK_CompanyStatusChangeApproval_CompanyId Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanyStatusChangeApproval objects.</returns>
		public TList<CompanyStatusChangeApproval> GetByCompanyId(TransactionManager transactionManager, System.Int32 _companyId, int start, int pageLength)
		{
			int count = -1;
			return GetByCompanyId(transactionManager, _companyId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompanyStatusChangeApproval_CompanyId key.
		///		fkCompanyStatusChangeApprovalCompanyId Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_companyId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanyStatusChangeApproval objects.</returns>
		public TList<CompanyStatusChangeApproval> GetByCompanyId(System.Int32 _companyId, int start, int pageLength)
		{
			int count =  -1;
			return GetByCompanyId(null, _companyId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompanyStatusChangeApproval_CompanyId key.
		///		fkCompanyStatusChangeApprovalCompanyId Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_companyId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanyStatusChangeApproval objects.</returns>
		public TList<CompanyStatusChangeApproval> GetByCompanyId(System.Int32 _companyId, int start, int pageLength,out int count)
		{
			return GetByCompanyId(null, _companyId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompanyStatusChangeApproval_CompanyId key.
		///		FK_CompanyStatusChangeApproval_CompanyId Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanyStatusChangeApproval objects.</returns>
		public abstract TList<CompanyStatusChangeApproval> GetByCompanyId(TransactionManager transactionManager, System.Int32 _companyId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompanyStatusChangeApproval_CurrentCompanyStatus key.
		///		FK_CompanyStatusChangeApproval_CurrentCompanyStatus Description: 
		/// </summary>
		/// <param name="_currentCompanyStatusId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanyStatusChangeApproval objects.</returns>
		public TList<CompanyStatusChangeApproval> GetByCurrentCompanyStatusId(System.Int32 _currentCompanyStatusId)
		{
			int count = -1;
			return GetByCurrentCompanyStatusId(_currentCompanyStatusId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompanyStatusChangeApproval_CurrentCompanyStatus key.
		///		FK_CompanyStatusChangeApproval_CurrentCompanyStatus Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_currentCompanyStatusId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanyStatusChangeApproval objects.</returns>
		/// <remarks></remarks>
		public TList<CompanyStatusChangeApproval> GetByCurrentCompanyStatusId(TransactionManager transactionManager, System.Int32 _currentCompanyStatusId)
		{
			int count = -1;
			return GetByCurrentCompanyStatusId(transactionManager, _currentCompanyStatusId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompanyStatusChangeApproval_CurrentCompanyStatus key.
		///		FK_CompanyStatusChangeApproval_CurrentCompanyStatus Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_currentCompanyStatusId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanyStatusChangeApproval objects.</returns>
		public TList<CompanyStatusChangeApproval> GetByCurrentCompanyStatusId(TransactionManager transactionManager, System.Int32 _currentCompanyStatusId, int start, int pageLength)
		{
			int count = -1;
			return GetByCurrentCompanyStatusId(transactionManager, _currentCompanyStatusId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompanyStatusChangeApproval_CurrentCompanyStatus key.
		///		fkCompanyStatusChangeApprovalCurrentCompanyStatus Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_currentCompanyStatusId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanyStatusChangeApproval objects.</returns>
		public TList<CompanyStatusChangeApproval> GetByCurrentCompanyStatusId(System.Int32 _currentCompanyStatusId, int start, int pageLength)
		{
			int count =  -1;
			return GetByCurrentCompanyStatusId(null, _currentCompanyStatusId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompanyStatusChangeApproval_CurrentCompanyStatus key.
		///		fkCompanyStatusChangeApprovalCurrentCompanyStatus Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_currentCompanyStatusId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanyStatusChangeApproval objects.</returns>
		public TList<CompanyStatusChangeApproval> GetByCurrentCompanyStatusId(System.Int32 _currentCompanyStatusId, int start, int pageLength,out int count)
		{
			return GetByCurrentCompanyStatusId(null, _currentCompanyStatusId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompanyStatusChangeApproval_CurrentCompanyStatus key.
		///		FK_CompanyStatusChangeApproval_CurrentCompanyStatus Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_currentCompanyStatusId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanyStatusChangeApproval objects.</returns>
		public abstract TList<CompanyStatusChangeApproval> GetByCurrentCompanyStatusId(TransactionManager transactionManager, System.Int32 _currentCompanyStatusId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompanyStatusChangeApproval_Questionnaire key.
		///		FK_CompanyStatusChangeApproval_Questionnaire Description: 
		/// </summary>
		/// <param name="_questionnaireId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanyStatusChangeApproval objects.</returns>
		public TList<CompanyStatusChangeApproval> GetByQuestionnaireId(System.Int32 _questionnaireId)
		{
			int count = -1;
			return GetByQuestionnaireId(_questionnaireId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompanyStatusChangeApproval_Questionnaire key.
		///		FK_CompanyStatusChangeApproval_Questionnaire Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanyStatusChangeApproval objects.</returns>
		/// <remarks></remarks>
		public TList<CompanyStatusChangeApproval> GetByQuestionnaireId(TransactionManager transactionManager, System.Int32 _questionnaireId)
		{
			int count = -1;
			return GetByQuestionnaireId(transactionManager, _questionnaireId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompanyStatusChangeApproval_Questionnaire key.
		///		FK_CompanyStatusChangeApproval_Questionnaire Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanyStatusChangeApproval objects.</returns>
		public TList<CompanyStatusChangeApproval> GetByQuestionnaireId(TransactionManager transactionManager, System.Int32 _questionnaireId, int start, int pageLength)
		{
			int count = -1;
			return GetByQuestionnaireId(transactionManager, _questionnaireId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompanyStatusChangeApproval_Questionnaire key.
		///		fkCompanyStatusChangeApprovalQuestionnaire Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_questionnaireId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanyStatusChangeApproval objects.</returns>
		public TList<CompanyStatusChangeApproval> GetByQuestionnaireId(System.Int32 _questionnaireId, int start, int pageLength)
		{
			int count =  -1;
			return GetByQuestionnaireId(null, _questionnaireId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompanyStatusChangeApproval_Questionnaire key.
		///		fkCompanyStatusChangeApprovalQuestionnaire Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_questionnaireId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanyStatusChangeApproval objects.</returns>
		public TList<CompanyStatusChangeApproval> GetByQuestionnaireId(System.Int32 _questionnaireId, int start, int pageLength,out int count)
		{
			return GetByQuestionnaireId(null, _questionnaireId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompanyStatusChangeApproval_Questionnaire key.
		///		FK_CompanyStatusChangeApproval_Questionnaire Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanyStatusChangeApproval objects.</returns>
		public abstract TList<CompanyStatusChangeApproval> GetByQuestionnaireId(TransactionManager transactionManager, System.Int32 _questionnaireId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompanyStatusChangeApproval_RequestedCompanyStatusId key.
		///		FK_CompanyStatusChangeApproval_RequestedCompanyStatusId Description: 
		/// </summary>
		/// <param name="_requestedCompanyStatusId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanyStatusChangeApproval objects.</returns>
		public TList<CompanyStatusChangeApproval> GetByRequestedCompanyStatusId(System.Int32 _requestedCompanyStatusId)
		{
			int count = -1;
			return GetByRequestedCompanyStatusId(_requestedCompanyStatusId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompanyStatusChangeApproval_RequestedCompanyStatusId key.
		///		FK_CompanyStatusChangeApproval_RequestedCompanyStatusId Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_requestedCompanyStatusId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanyStatusChangeApproval objects.</returns>
		/// <remarks></remarks>
		public TList<CompanyStatusChangeApproval> GetByRequestedCompanyStatusId(TransactionManager transactionManager, System.Int32 _requestedCompanyStatusId)
		{
			int count = -1;
			return GetByRequestedCompanyStatusId(transactionManager, _requestedCompanyStatusId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompanyStatusChangeApproval_RequestedCompanyStatusId key.
		///		FK_CompanyStatusChangeApproval_RequestedCompanyStatusId Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_requestedCompanyStatusId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanyStatusChangeApproval objects.</returns>
		public TList<CompanyStatusChangeApproval> GetByRequestedCompanyStatusId(TransactionManager transactionManager, System.Int32 _requestedCompanyStatusId, int start, int pageLength)
		{
			int count = -1;
			return GetByRequestedCompanyStatusId(transactionManager, _requestedCompanyStatusId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompanyStatusChangeApproval_RequestedCompanyStatusId key.
		///		fkCompanyStatusChangeApprovalRequestedCompanyStatusId Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_requestedCompanyStatusId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanyStatusChangeApproval objects.</returns>
		public TList<CompanyStatusChangeApproval> GetByRequestedCompanyStatusId(System.Int32 _requestedCompanyStatusId, int start, int pageLength)
		{
			int count =  -1;
			return GetByRequestedCompanyStatusId(null, _requestedCompanyStatusId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompanyStatusChangeApproval_RequestedCompanyStatusId key.
		///		fkCompanyStatusChangeApprovalRequestedCompanyStatusId Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_requestedCompanyStatusId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanyStatusChangeApproval objects.</returns>
		public TList<CompanyStatusChangeApproval> GetByRequestedCompanyStatusId(System.Int32 _requestedCompanyStatusId, int start, int pageLength,out int count)
		{
			return GetByRequestedCompanyStatusId(null, _requestedCompanyStatusId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompanyStatusChangeApproval_RequestedCompanyStatusId key.
		///		FK_CompanyStatusChangeApproval_RequestedCompanyStatusId Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_requestedCompanyStatusId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanyStatusChangeApproval objects.</returns>
		public abstract TList<CompanyStatusChangeApproval> GetByRequestedCompanyStatusId(TransactionManager transactionManager, System.Int32 _requestedCompanyStatusId, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.CompanyStatusChangeApproval Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.CompanyStatusChangeApprovalKey key, int start, int pageLength)
		{
			return GetByCompanyStatusChangeApprovalId(transactionManager, key.CompanyStatusChangeApprovalId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_CompanyStatusChangeApproval index.
		/// </summary>
		/// <param name="_companyStatusChangeApprovalId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompanyStatusChangeApproval"/> class.</returns>
		public KaiZen.CSMS.Entities.CompanyStatusChangeApproval GetByCompanyStatusChangeApprovalId(System.Int32 _companyStatusChangeApprovalId)
		{
			int count = -1;
			return GetByCompanyStatusChangeApprovalId(null,_companyStatusChangeApprovalId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CompanyStatusChangeApproval index.
		/// </summary>
		/// <param name="_companyStatusChangeApprovalId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompanyStatusChangeApproval"/> class.</returns>
		public KaiZen.CSMS.Entities.CompanyStatusChangeApproval GetByCompanyStatusChangeApprovalId(System.Int32 _companyStatusChangeApprovalId, int start, int pageLength)
		{
			int count = -1;
			return GetByCompanyStatusChangeApprovalId(null, _companyStatusChangeApprovalId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CompanyStatusChangeApproval index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyStatusChangeApprovalId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompanyStatusChangeApproval"/> class.</returns>
		public KaiZen.CSMS.Entities.CompanyStatusChangeApproval GetByCompanyStatusChangeApprovalId(TransactionManager transactionManager, System.Int32 _companyStatusChangeApprovalId)
		{
			int count = -1;
			return GetByCompanyStatusChangeApprovalId(transactionManager, _companyStatusChangeApprovalId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CompanyStatusChangeApproval index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyStatusChangeApprovalId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompanyStatusChangeApproval"/> class.</returns>
		public KaiZen.CSMS.Entities.CompanyStatusChangeApproval GetByCompanyStatusChangeApprovalId(TransactionManager transactionManager, System.Int32 _companyStatusChangeApprovalId, int start, int pageLength)
		{
			int count = -1;
			return GetByCompanyStatusChangeApprovalId(transactionManager, _companyStatusChangeApprovalId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CompanyStatusChangeApproval index.
		/// </summary>
		/// <param name="_companyStatusChangeApprovalId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompanyStatusChangeApproval"/> class.</returns>
		public KaiZen.CSMS.Entities.CompanyStatusChangeApproval GetByCompanyStatusChangeApprovalId(System.Int32 _companyStatusChangeApprovalId, int start, int pageLength, out int count)
		{
			return GetByCompanyStatusChangeApprovalId(null, _companyStatusChangeApprovalId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CompanyStatusChangeApproval index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyStatusChangeApprovalId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompanyStatusChangeApproval"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.CompanyStatusChangeApproval GetByCompanyStatusChangeApprovalId(TransactionManager transactionManager, System.Int32 _companyStatusChangeApprovalId, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#region _CompanyStatusChangeApproval_ListAwaitingApproval 
		
		/// <summary>
		///	This method wrap the '_CompanyStatusChangeApproval_ListAwaitingApproval' stored procedure. 
		/// </summary>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet ListAwaitingApproval()
		{
			return ListAwaitingApproval(null, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_CompanyStatusChangeApproval_ListAwaitingApproval' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet ListAwaitingApproval(int start, int pageLength)
		{
			return ListAwaitingApproval(null, start, pageLength );
		}
				
		/// <summary>
		///	This method wrap the '_CompanyStatusChangeApproval_ListAwaitingApproval' stored procedure. 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet ListAwaitingApproval(TransactionManager transactionManager)
		{
			return ListAwaitingApproval(transactionManager, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_CompanyStatusChangeApproval_ListAwaitingApproval' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet ListAwaitingApproval(TransactionManager transactionManager, int start, int pageLength );
		
		#endregion
		
		#region _CompanyStatusChangeApproval_ListHistory 
		
		/// <summary>
		///	This method wrap the '_CompanyStatusChangeApproval_ListHistory' stored procedure. 
		/// </summary>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet ListHistory()
		{
			return ListHistory(null, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_CompanyStatusChangeApproval_ListHistory' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet ListHistory(int start, int pageLength)
		{
			return ListHistory(null, start, pageLength );
		}
				
		/// <summary>
		///	This method wrap the '_CompanyStatusChangeApproval_ListHistory' stored procedure. 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet ListHistory(TransactionManager transactionManager)
		{
			return ListHistory(transactionManager, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_CompanyStatusChangeApproval_ListHistory' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet ListHistory(TransactionManager transactionManager, int start, int pageLength );
		
		#endregion
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;CompanyStatusChangeApproval&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;CompanyStatusChangeApproval&gt;"/></returns>
		public static TList<CompanyStatusChangeApproval> Fill(IDataReader reader, TList<CompanyStatusChangeApproval> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.CompanyStatusChangeApproval c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("CompanyStatusChangeApproval")
					.Append("|").Append((System.Int32)reader[((int)CompanyStatusChangeApprovalColumn.CompanyStatusChangeApprovalId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<CompanyStatusChangeApproval>(
					key.ToString(), // EntityTrackingKey
					"CompanyStatusChangeApproval",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.CompanyStatusChangeApproval();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.CompanyStatusChangeApprovalId = (System.Int32)reader[((int)CompanyStatusChangeApprovalColumn.CompanyStatusChangeApprovalId - 1)];
					c.RequestedDate = (System.DateTime)reader[((int)CompanyStatusChangeApprovalColumn.RequestedDate - 1)];
					c.RequestedByUserId = (System.Int32)reader[((int)CompanyStatusChangeApprovalColumn.RequestedByUserId - 1)];
					c.Comments = (System.String)reader[((int)CompanyStatusChangeApprovalColumn.Comments - 1)];
					c.CompanyId = (System.Int32)reader[((int)CompanyStatusChangeApprovalColumn.CompanyId - 1)];
					c.CurrentCompanyStatusId = (System.Int32)reader[((int)CompanyStatusChangeApprovalColumn.CurrentCompanyStatusId - 1)];
					c.RequestedCompanyStatusId = (System.Int32)reader[((int)CompanyStatusChangeApprovalColumn.RequestedCompanyStatusId - 1)];
					c.AssessedByUserId = (reader.IsDBNull(((int)CompanyStatusChangeApprovalColumn.AssessedByUserId - 1)))?null:(System.Int32?)reader[((int)CompanyStatusChangeApprovalColumn.AssessedByUserId - 1)];
					c.AssessedDate = (reader.IsDBNull(((int)CompanyStatusChangeApprovalColumn.AssessedDate - 1)))?null:(System.DateTime?)reader[((int)CompanyStatusChangeApprovalColumn.AssessedDate - 1)];
					c.AssessorComments = (reader.IsDBNull(((int)CompanyStatusChangeApprovalColumn.AssessorComments - 1)))?null:(System.String)reader[((int)CompanyStatusChangeApprovalColumn.AssessorComments - 1)];
					c.Approved = (reader.IsDBNull(((int)CompanyStatusChangeApprovalColumn.Approved - 1)))?null:(System.Boolean?)reader[((int)CompanyStatusChangeApprovalColumn.Approved - 1)];
					c.QuestionnaireId = (System.Int32)reader[((int)CompanyStatusChangeApprovalColumn.QuestionnaireId - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.CompanyStatusChangeApproval"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.CompanyStatusChangeApproval"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.CompanyStatusChangeApproval entity)
		{
			if (!reader.Read()) return;
			
			entity.CompanyStatusChangeApprovalId = (System.Int32)reader[((int)CompanyStatusChangeApprovalColumn.CompanyStatusChangeApprovalId - 1)];
			entity.RequestedDate = (System.DateTime)reader[((int)CompanyStatusChangeApprovalColumn.RequestedDate - 1)];
			entity.RequestedByUserId = (System.Int32)reader[((int)CompanyStatusChangeApprovalColumn.RequestedByUserId - 1)];
			entity.Comments = (System.String)reader[((int)CompanyStatusChangeApprovalColumn.Comments - 1)];
			entity.CompanyId = (System.Int32)reader[((int)CompanyStatusChangeApprovalColumn.CompanyId - 1)];
			entity.CurrentCompanyStatusId = (System.Int32)reader[((int)CompanyStatusChangeApprovalColumn.CurrentCompanyStatusId - 1)];
			entity.RequestedCompanyStatusId = (System.Int32)reader[((int)CompanyStatusChangeApprovalColumn.RequestedCompanyStatusId - 1)];
			entity.AssessedByUserId = (reader.IsDBNull(((int)CompanyStatusChangeApprovalColumn.AssessedByUserId - 1)))?null:(System.Int32?)reader[((int)CompanyStatusChangeApprovalColumn.AssessedByUserId - 1)];
			entity.AssessedDate = (reader.IsDBNull(((int)CompanyStatusChangeApprovalColumn.AssessedDate - 1)))?null:(System.DateTime?)reader[((int)CompanyStatusChangeApprovalColumn.AssessedDate - 1)];
			entity.AssessorComments = (reader.IsDBNull(((int)CompanyStatusChangeApprovalColumn.AssessorComments - 1)))?null:(System.String)reader[((int)CompanyStatusChangeApprovalColumn.AssessorComments - 1)];
			entity.Approved = (reader.IsDBNull(((int)CompanyStatusChangeApprovalColumn.Approved - 1)))?null:(System.Boolean?)reader[((int)CompanyStatusChangeApprovalColumn.Approved - 1)];
			entity.QuestionnaireId = (System.Int32)reader[((int)CompanyStatusChangeApprovalColumn.QuestionnaireId - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.CompanyStatusChangeApproval"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.CompanyStatusChangeApproval"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.CompanyStatusChangeApproval entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.CompanyStatusChangeApprovalId = (System.Int32)dataRow["CompanyStatusChangeApprovalId"];
			entity.RequestedDate = (System.DateTime)dataRow["RequestedDate"];
			entity.RequestedByUserId = (System.Int32)dataRow["RequestedByUserId"];
			entity.Comments = (System.String)dataRow["Comments"];
			entity.CompanyId = (System.Int32)dataRow["CompanyId"];
			entity.CurrentCompanyStatusId = (System.Int32)dataRow["CurrentCompanyStatusId"];
			entity.RequestedCompanyStatusId = (System.Int32)dataRow["RequestedCompanyStatusId"];
			entity.AssessedByUserId = Convert.IsDBNull(dataRow["AssessedByUserId"]) ? null : (System.Int32?)dataRow["AssessedByUserId"];
			entity.AssessedDate = Convert.IsDBNull(dataRow["AssessedDate"]) ? null : (System.DateTime?)dataRow["AssessedDate"];
			entity.AssessorComments = Convert.IsDBNull(dataRow["AssessorComments"]) ? null : (System.String)dataRow["AssessorComments"];
			entity.Approved = Convert.IsDBNull(dataRow["Approved"]) ? null : (System.Boolean?)dataRow["Approved"];
			entity.QuestionnaireId = (System.Int32)dataRow["QuestionnaireId"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.CompanyStatusChangeApproval"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.CompanyStatusChangeApproval Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.CompanyStatusChangeApproval entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region RequestedByUserIdSource	
			if (CanDeepLoad(entity, "Users|RequestedByUserIdSource", deepLoadType, innerList) 
				&& entity.RequestedByUserIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.RequestedByUserId;
				Users tmpEntity = EntityManager.LocateEntity<Users>(EntityLocator.ConstructKeyFromPkItems(typeof(Users), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.RequestedByUserIdSource = tmpEntity;
				else
					entity.RequestedByUserIdSource = DataRepository.UsersProvider.GetByUserId(transactionManager, entity.RequestedByUserId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'RequestedByUserIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.RequestedByUserIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.UsersProvider.DeepLoad(transactionManager, entity.RequestedByUserIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion RequestedByUserIdSource

			#region AssessedByUserIdSource	
			if (CanDeepLoad(entity, "Users|AssessedByUserIdSource", deepLoadType, innerList) 
				&& entity.AssessedByUserIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.AssessedByUserId ?? (int)0);
				Users tmpEntity = EntityManager.LocateEntity<Users>(EntityLocator.ConstructKeyFromPkItems(typeof(Users), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.AssessedByUserIdSource = tmpEntity;
				else
					entity.AssessedByUserIdSource = DataRepository.UsersProvider.GetByUserId(transactionManager, (entity.AssessedByUserId ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'AssessedByUserIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.AssessedByUserIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.UsersProvider.DeepLoad(transactionManager, entity.AssessedByUserIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion AssessedByUserIdSource

			#region CompanyIdSource	
			if (CanDeepLoad(entity, "Companies|CompanyIdSource", deepLoadType, innerList) 
				&& entity.CompanyIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.CompanyId;
				Companies tmpEntity = EntityManager.LocateEntity<Companies>(EntityLocator.ConstructKeyFromPkItems(typeof(Companies), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.CompanyIdSource = tmpEntity;
				else
					entity.CompanyIdSource = DataRepository.CompaniesProvider.GetByCompanyId(transactionManager, entity.CompanyId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CompanyIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.CompanyIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.CompaniesProvider.DeepLoad(transactionManager, entity.CompanyIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion CompanyIdSource

			#region CurrentCompanyStatusIdSource	
			if (CanDeepLoad(entity, "CompanyStatus|CurrentCompanyStatusIdSource", deepLoadType, innerList) 
				&& entity.CurrentCompanyStatusIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.CurrentCompanyStatusId;
				CompanyStatus tmpEntity = EntityManager.LocateEntity<CompanyStatus>(EntityLocator.ConstructKeyFromPkItems(typeof(CompanyStatus), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.CurrentCompanyStatusIdSource = tmpEntity;
				else
					entity.CurrentCompanyStatusIdSource = DataRepository.CompanyStatusProvider.GetByCompanyStatusId(transactionManager, entity.CurrentCompanyStatusId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CurrentCompanyStatusIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.CurrentCompanyStatusIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.CompanyStatusProvider.DeepLoad(transactionManager, entity.CurrentCompanyStatusIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion CurrentCompanyStatusIdSource

			#region QuestionnaireIdSource	
			if (CanDeepLoad(entity, "Questionnaire|QuestionnaireIdSource", deepLoadType, innerList) 
				&& entity.QuestionnaireIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.QuestionnaireId;
				Questionnaire tmpEntity = EntityManager.LocateEntity<Questionnaire>(EntityLocator.ConstructKeyFromPkItems(typeof(Questionnaire), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.QuestionnaireIdSource = tmpEntity;
				else
					entity.QuestionnaireIdSource = DataRepository.QuestionnaireProvider.GetByQuestionnaireId(transactionManager, entity.QuestionnaireId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'QuestionnaireIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.QuestionnaireIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.QuestionnaireProvider.DeepLoad(transactionManager, entity.QuestionnaireIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion QuestionnaireIdSource

			#region RequestedCompanyStatusIdSource	
			if (CanDeepLoad(entity, "CompanyStatus|RequestedCompanyStatusIdSource", deepLoadType, innerList) 
				&& entity.RequestedCompanyStatusIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.RequestedCompanyStatusId;
				CompanyStatus tmpEntity = EntityManager.LocateEntity<CompanyStatus>(EntityLocator.ConstructKeyFromPkItems(typeof(CompanyStatus), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.RequestedCompanyStatusIdSource = tmpEntity;
				else
					entity.RequestedCompanyStatusIdSource = DataRepository.CompanyStatusProvider.GetByCompanyStatusId(transactionManager, entity.RequestedCompanyStatusId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'RequestedCompanyStatusIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.RequestedCompanyStatusIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.CompanyStatusProvider.DeepLoad(transactionManager, entity.RequestedCompanyStatusIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion RequestedCompanyStatusIdSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.CompanyStatusChangeApproval object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.CompanyStatusChangeApproval instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.CompanyStatusChangeApproval Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.CompanyStatusChangeApproval entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region RequestedByUserIdSource
			if (CanDeepSave(entity, "Users|RequestedByUserIdSource", deepSaveType, innerList) 
				&& entity.RequestedByUserIdSource != null)
			{
				DataRepository.UsersProvider.Save(transactionManager, entity.RequestedByUserIdSource);
				entity.RequestedByUserId = entity.RequestedByUserIdSource.UserId;
			}
			#endregion 
			
			#region AssessedByUserIdSource
			if (CanDeepSave(entity, "Users|AssessedByUserIdSource", deepSaveType, innerList) 
				&& entity.AssessedByUserIdSource != null)
			{
				DataRepository.UsersProvider.Save(transactionManager, entity.AssessedByUserIdSource);
				entity.AssessedByUserId = entity.AssessedByUserIdSource.UserId;
			}
			#endregion 
			
			#region CompanyIdSource
			if (CanDeepSave(entity, "Companies|CompanyIdSource", deepSaveType, innerList) 
				&& entity.CompanyIdSource != null)
			{
				DataRepository.CompaniesProvider.Save(transactionManager, entity.CompanyIdSource);
				entity.CompanyId = entity.CompanyIdSource.CompanyId;
			}
			#endregion 
			
			#region CurrentCompanyStatusIdSource
			if (CanDeepSave(entity, "CompanyStatus|CurrentCompanyStatusIdSource", deepSaveType, innerList) 
				&& entity.CurrentCompanyStatusIdSource != null)
			{
				DataRepository.CompanyStatusProvider.Save(transactionManager, entity.CurrentCompanyStatusIdSource);
				entity.CurrentCompanyStatusId = entity.CurrentCompanyStatusIdSource.CompanyStatusId;
			}
			#endregion 
			
			#region QuestionnaireIdSource
			if (CanDeepSave(entity, "Questionnaire|QuestionnaireIdSource", deepSaveType, innerList) 
				&& entity.QuestionnaireIdSource != null)
			{
				DataRepository.QuestionnaireProvider.Save(transactionManager, entity.QuestionnaireIdSource);
				entity.QuestionnaireId = entity.QuestionnaireIdSource.QuestionnaireId;
			}
			#endregion 
			
			#region RequestedCompanyStatusIdSource
			if (CanDeepSave(entity, "CompanyStatus|RequestedCompanyStatusIdSource", deepSaveType, innerList) 
				&& entity.RequestedCompanyStatusIdSource != null)
			{
				DataRepository.CompanyStatusProvider.Save(transactionManager, entity.RequestedCompanyStatusIdSource);
				entity.RequestedCompanyStatusId = entity.RequestedCompanyStatusIdSource.CompanyStatusId;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region CompanyStatusChangeApprovalChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.CompanyStatusChangeApproval</c>
	///</summary>
	public enum CompanyStatusChangeApprovalChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>Users</c> at RequestedByUserIdSource
		///</summary>
		[ChildEntityType(typeof(Users))]
		Users,
			
		///<summary>
		/// Composite Property for <c>Companies</c> at CompanyIdSource
		///</summary>
		[ChildEntityType(typeof(Companies))]
		Companies,
			
		///<summary>
		/// Composite Property for <c>CompanyStatus</c> at CurrentCompanyStatusIdSource
		///</summary>
		[ChildEntityType(typeof(CompanyStatus))]
		CompanyStatus,
			
		///<summary>
		/// Composite Property for <c>Questionnaire</c> at QuestionnaireIdSource
		///</summary>
		[ChildEntityType(typeof(Questionnaire))]
		Questionnaire,
		}
	
	#endregion CompanyStatusChangeApprovalChildEntityTypes
	
	#region CompanyStatusChangeApprovalFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;CompanyStatusChangeApprovalColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CompanyStatusChangeApproval"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompanyStatusChangeApprovalFilterBuilder : SqlFilterBuilder<CompanyStatusChangeApprovalColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompanyStatusChangeApprovalFilterBuilder class.
		/// </summary>
		public CompanyStatusChangeApprovalFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the CompanyStatusChangeApprovalFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CompanyStatusChangeApprovalFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CompanyStatusChangeApprovalFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CompanyStatusChangeApprovalFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CompanyStatusChangeApprovalFilterBuilder
	
	#region CompanyStatusChangeApprovalParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;CompanyStatusChangeApprovalColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CompanyStatusChangeApproval"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompanyStatusChangeApprovalParameterBuilder : ParameterizedSqlFilterBuilder<CompanyStatusChangeApprovalColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompanyStatusChangeApprovalParameterBuilder class.
		/// </summary>
		public CompanyStatusChangeApprovalParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the CompanyStatusChangeApprovalParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CompanyStatusChangeApprovalParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CompanyStatusChangeApprovalParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CompanyStatusChangeApprovalParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CompanyStatusChangeApprovalParameterBuilder
	
	#region CompanyStatusChangeApprovalSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;CompanyStatusChangeApprovalColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CompanyStatusChangeApproval"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class CompanyStatusChangeApprovalSortBuilder : SqlSortBuilder<CompanyStatusChangeApprovalColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompanyStatusChangeApprovalSqlSortBuilder class.
		/// </summary>
		public CompanyStatusChangeApprovalSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion CompanyStatusChangeApprovalSortBuilder
	
} // end namespace
