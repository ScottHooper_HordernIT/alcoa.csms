﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.QuestionnaireInitialLocationAuditProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(QuestionnaireInitialLocationAuditDataSourceDesigner))]
	public class QuestionnaireInitialLocationAuditDataSource : ProviderDataSource<QuestionnaireInitialLocationAudit, QuestionnaireInitialLocationAuditKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialLocationAuditDataSource class.
		/// </summary>
		public QuestionnaireInitialLocationAuditDataSource() : base(new QuestionnaireInitialLocationAuditService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the QuestionnaireInitialLocationAuditDataSourceView used by the QuestionnaireInitialLocationAuditDataSource.
		/// </summary>
		protected QuestionnaireInitialLocationAuditDataSourceView QuestionnaireInitialLocationAuditView
		{
			get { return ( View as QuestionnaireInitialLocationAuditDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the QuestionnaireInitialLocationAuditDataSource control invokes to retrieve data.
		/// </summary>
		public QuestionnaireInitialLocationAuditSelectMethod SelectMethod
		{
			get
			{
				QuestionnaireInitialLocationAuditSelectMethod selectMethod = QuestionnaireInitialLocationAuditSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (QuestionnaireInitialLocationAuditSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the QuestionnaireInitialLocationAuditDataSourceView class that is to be
		/// used by the QuestionnaireInitialLocationAuditDataSource.
		/// </summary>
		/// <returns>An instance of the QuestionnaireInitialLocationAuditDataSourceView class.</returns>
		protected override BaseDataSourceView<QuestionnaireInitialLocationAudit, QuestionnaireInitialLocationAuditKey> GetNewDataSourceView()
		{
			return new QuestionnaireInitialLocationAuditDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the QuestionnaireInitialLocationAuditDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class QuestionnaireInitialLocationAuditDataSourceView : ProviderDataSourceView<QuestionnaireInitialLocationAudit, QuestionnaireInitialLocationAuditKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialLocationAuditDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the QuestionnaireInitialLocationAuditDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public QuestionnaireInitialLocationAuditDataSourceView(QuestionnaireInitialLocationAuditDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal QuestionnaireInitialLocationAuditDataSource QuestionnaireInitialLocationAuditOwner
		{
			get { return Owner as QuestionnaireInitialLocationAuditDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal QuestionnaireInitialLocationAuditSelectMethod SelectMethod
		{
			get { return QuestionnaireInitialLocationAuditOwner.SelectMethod; }
			set { QuestionnaireInitialLocationAuditOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal QuestionnaireInitialLocationAuditService QuestionnaireInitialLocationAuditProvider
		{
			get { return Provider as QuestionnaireInitialLocationAuditService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<QuestionnaireInitialLocationAudit> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<QuestionnaireInitialLocationAudit> results = null;
			QuestionnaireInitialLocationAudit item;
			count = 0;
			
			System.Int32 _auditId;
			System.Int32? _questionnaireId_nullable;
			System.Int32? _modifiedByUserId_nullable;

			switch ( SelectMethod )
			{
				case QuestionnaireInitialLocationAuditSelectMethod.Get:
					QuestionnaireInitialLocationAuditKey entityKey  = new QuestionnaireInitialLocationAuditKey();
					entityKey.Load(values);
					item = QuestionnaireInitialLocationAuditProvider.Get(entityKey);
					results = new TList<QuestionnaireInitialLocationAudit>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case QuestionnaireInitialLocationAuditSelectMethod.GetAll:
                    results = QuestionnaireInitialLocationAuditProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case QuestionnaireInitialLocationAuditSelectMethod.GetPaged:
					results = QuestionnaireInitialLocationAuditProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case QuestionnaireInitialLocationAuditSelectMethod.Find:
					if ( FilterParameters != null )
						results = QuestionnaireInitialLocationAuditProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = QuestionnaireInitialLocationAuditProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case QuestionnaireInitialLocationAuditSelectMethod.GetByAuditId:
					_auditId = ( values["AuditId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["AuditId"], typeof(System.Int32)) : (int)0;
					item = QuestionnaireInitialLocationAuditProvider.GetByAuditId(_auditId);
					results = new TList<QuestionnaireInitialLocationAudit>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				case QuestionnaireInitialLocationAuditSelectMethod.GetByQuestionnaireId:
					_questionnaireId_nullable = (System.Int32?) EntityUtil.ChangeType(values["QuestionnaireId"], typeof(System.Int32?));
					results = QuestionnaireInitialLocationAuditProvider.GetByQuestionnaireId(_questionnaireId_nullable, this.StartIndex, this.PageSize, out count);
					break;
				case QuestionnaireInitialLocationAuditSelectMethod.GetByModifiedByUserId:
					_modifiedByUserId_nullable = (System.Int32?) EntityUtil.ChangeType(values["ModifiedByUserId"], typeof(System.Int32?));
					results = QuestionnaireInitialLocationAuditProvider.GetByModifiedByUserId(_modifiedByUserId_nullable, this.StartIndex, this.PageSize, out count);
					break;
				// FK
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == QuestionnaireInitialLocationAuditSelectMethod.Get || SelectMethod == QuestionnaireInitialLocationAuditSelectMethod.GetByAuditId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				QuestionnaireInitialLocationAudit entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					QuestionnaireInitialLocationAuditProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<QuestionnaireInitialLocationAudit> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			QuestionnaireInitialLocationAuditProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region QuestionnaireInitialLocationAuditDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the QuestionnaireInitialLocationAuditDataSource class.
	/// </summary>
	public class QuestionnaireInitialLocationAuditDataSourceDesigner : ProviderDataSourceDesigner<QuestionnaireInitialLocationAudit, QuestionnaireInitialLocationAuditKey>
	{
		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialLocationAuditDataSourceDesigner class.
		/// </summary>
		public QuestionnaireInitialLocationAuditDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public QuestionnaireInitialLocationAuditSelectMethod SelectMethod
		{
			get { return ((QuestionnaireInitialLocationAuditDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new QuestionnaireInitialLocationAuditDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region QuestionnaireInitialLocationAuditDataSourceActionList

	/// <summary>
	/// Supports the QuestionnaireInitialLocationAuditDataSourceDesigner class.
	/// </summary>
	internal class QuestionnaireInitialLocationAuditDataSourceActionList : DesignerActionList
	{
		private QuestionnaireInitialLocationAuditDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialLocationAuditDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public QuestionnaireInitialLocationAuditDataSourceActionList(QuestionnaireInitialLocationAuditDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public QuestionnaireInitialLocationAuditSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion QuestionnaireInitialLocationAuditDataSourceActionList
	
	#endregion QuestionnaireInitialLocationAuditDataSourceDesigner
	
	#region QuestionnaireInitialLocationAuditSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the QuestionnaireInitialLocationAuditDataSource.SelectMethod property.
	/// </summary>
	public enum QuestionnaireInitialLocationAuditSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByAuditId method.
		/// </summary>
		GetByAuditId,
		/// <summary>
		/// Represents the GetByQuestionnaireId method.
		/// </summary>
		GetByQuestionnaireId,
		/// <summary>
		/// Represents the GetByModifiedByUserId method.
		/// </summary>
		GetByModifiedByUserId
	}
	
	#endregion QuestionnaireInitialLocationAuditSelectMethod

	#region QuestionnaireInitialLocationAuditFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireInitialLocationAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireInitialLocationAuditFilter : SqlFilter<QuestionnaireInitialLocationAuditColumn>
	{
	}
	
	#endregion QuestionnaireInitialLocationAuditFilter

	#region QuestionnaireInitialLocationAuditExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireInitialLocationAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireInitialLocationAuditExpressionBuilder : SqlExpressionBuilder<QuestionnaireInitialLocationAuditColumn>
	{
	}
	
	#endregion QuestionnaireInitialLocationAuditExpressionBuilder	

	#region QuestionnaireInitialLocationAuditProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;QuestionnaireInitialLocationAuditChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireInitialLocationAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireInitialLocationAuditProperty : ChildEntityProperty<QuestionnaireInitialLocationAuditChildEntityTypes>
	{
	}
	
	#endregion QuestionnaireInitialLocationAuditProperty
}

