﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface IAdminTaskEmailRecipientService
    {
        AdminTaskEmailRecipient Get(Expression<Func<AdminTaskEmailRecipient, bool>> where, List<Expression<Func<AdminTaskEmailRecipient, object>>> includes);
        List<AdminTaskEmailRecipient> GetMany(int? top, Expression<Func<AdminTaskEmailRecipient, bool>> where, Func<IQueryable<AdminTaskEmailRecipient>, IOrderedQueryable<AdminTaskEmailRecipient>> orderBy, List<Expression<Func<AdminTaskEmailRecipient, object>>> includes);
        List<AdminTaskEmailRecipient> GetManyPaged(int pageNumber, int pageSize, Expression<Func<AdminTaskEmailRecipient, bool>> where, Func<IQueryable<AdminTaskEmailRecipient>, IOrderedQueryable<AdminTaskEmailRecipient>> orderBy, List<Expression<Func<AdminTaskEmailRecipient, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<AdminTaskEmailRecipient, bool>> where, List<Expression<Func<AdminTaskEmailRecipient, object>>> includes);
        void Insert(AdminTaskEmailRecipient item, bool saveChanges = true);
        void Update(AdminTaskEmailRecipient item, bool saveChanges = true);
        void Delete(AdminTaskEmailRecipient item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class AdminTaskEmailRecipientService : IAdminTaskEmailRecipientService
    {
        private readonly IRepository<AdminTaskEmailRecipient> _repository;

        public AdminTaskEmailRecipientService(IRepository<AdminTaskEmailRecipient> repository)
        {
            this._repository = repository;
        }

        public AdminTaskEmailRecipient Get(Expression<Func<AdminTaskEmailRecipient, bool>> where, List<Expression<Func<AdminTaskEmailRecipient, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<AdminTaskEmailRecipient> GetMany(int? top, Expression<Func<AdminTaskEmailRecipient, bool>> where, Func<IQueryable<AdminTaskEmailRecipient>, IOrderedQueryable<AdminTaskEmailRecipient>> orderBy, List<Expression<Func<AdminTaskEmailRecipient, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<AdminTaskEmailRecipient> GetManyPaged(int pageNumber, int pageSize, Expression<Func<AdminTaskEmailRecipient, bool>> where, Func<IQueryable<AdminTaskEmailRecipient>, IOrderedQueryable<AdminTaskEmailRecipient>> orderBy, List<Expression<Func<AdminTaskEmailRecipient, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<AdminTaskEmailRecipient, bool>> where, List<Expression<Func<AdminTaskEmailRecipient, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(AdminTaskEmailRecipient item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("AdminTaskEmailRecipient");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(AdminTaskEmailRecipient item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("AdminTaskEmailRecipient");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(AdminTaskEmailRecipient item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("AdminTaskEmailRecipient");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
