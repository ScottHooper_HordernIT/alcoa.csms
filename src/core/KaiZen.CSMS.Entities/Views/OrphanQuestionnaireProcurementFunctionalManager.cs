﻿#region Using directives

using System;

#endregion

namespace KaiZen.CSMS.Entities
{	
	///<summary>
	/// An object representation of the '_Orphan_Questionnaire_ProcurementFunctionalManager' view. [No description found in the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class OrphanQuestionnaireProcurementFunctionalManager : OrphanQuestionnaireProcurementFunctionalManagerBase
	{
		#region Constructors

		///<summary>
		/// Creates a new <see cref="OrphanQuestionnaireProcurementFunctionalManager"/> instance.
		///</summary>
		public OrphanQuestionnaireProcurementFunctionalManager():base(){}	
		
		#endregion
	}
}
