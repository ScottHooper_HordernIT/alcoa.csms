﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------



public partial class UserControls_Other_userProfile2 {
    
    /// <summary>
    /// lblTitle_Name control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::DevExpress.Web.ASPxEditors.ASPxLabel lblTitle_Name;
    
    /// <summary>
    /// lblName control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::DevExpress.Web.ASPxEditors.ASPxLabel lblName;
    
    /// <summary>
    /// tbFirstName control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::DevExpress.Web.ASPxEditors.ASPxTextBox tbFirstName;
    
    /// <summary>
    /// tbLastName control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::DevExpress.Web.ASPxEditors.ASPxTextBox tbLastName;
    
    /// <summary>
    /// lblTitle_Title control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::DevExpress.Web.ASPxEditors.ASPxLabel lblTitle_Title;
    
    /// <summary>
    /// lblTitle control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::DevExpress.Web.ASPxEditors.ASPxLabel lblTitle;
    
    /// <summary>
    /// tbTitle control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::DevExpress.Web.ASPxEditors.ASPxTextBox tbTitle;
    
    /// <summary>
    /// lblTitle_Company control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::DevExpress.Web.ASPxEditors.ASPxLabel lblTitle_Company;
    
    /// <summary>
    /// lblCompany control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::DevExpress.Web.ASPxEditors.ASPxLabel lblCompany;
    
    /// <summary>
    /// lblTitle_Email control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::DevExpress.Web.ASPxEditors.ASPxLabel lblTitle_Email;
    
    /// <summary>
    /// hlEmail control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::DevExpress.Web.ASPxEditors.ASPxHyperLink hlEmail;
    
    /// <summary>
    /// tbEmail control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::DevExpress.Web.ASPxEditors.ASPxTextBox tbEmail;
    
    /// <summary>
    /// btnEdit control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::System.Web.UI.WebControls.Button btnEdit;
    
    /// <summary>
    /// btnSave control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::System.Web.UI.WebControls.Button btnSave;
    
    /// <summary>
    /// btnCancel control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::System.Web.UI.WebControls.Button btnCancel;
}
