﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.CompanySiteCategoryStandardRegionProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[CLSCompliant(true)]
	[Designer(typeof(CompanySiteCategoryStandardRegionDataSourceDesigner))]
	public class CompanySiteCategoryStandardRegionDataSource : ReadOnlyDataSource<CompanySiteCategoryStandardRegion>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompanySiteCategoryStandardRegionDataSource class.
		/// </summary>
		public CompanySiteCategoryStandardRegionDataSource() : base(new CompanySiteCategoryStandardRegionService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the CompanySiteCategoryStandardRegionDataSourceView used by the CompanySiteCategoryStandardRegionDataSource.
		/// </summary>
		protected CompanySiteCategoryStandardRegionDataSourceView CompanySiteCategoryStandardRegionView
		{
			get { return ( View as CompanySiteCategoryStandardRegionDataSourceView ); }
		}
		
		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the CompanySiteCategoryStandardRegionDataSourceView class that is to be
		/// used by the CompanySiteCategoryStandardRegionDataSource.
		/// </summary>
		/// <returns>An instance of the CompanySiteCategoryStandardRegionDataSourceView class.</returns>
		protected override BaseDataSourceView<CompanySiteCategoryStandardRegion, Object> GetNewDataSourceView()
		{
			return new CompanySiteCategoryStandardRegionDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the CompanySiteCategoryStandardRegionDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class CompanySiteCategoryStandardRegionDataSourceView : ReadOnlyDataSourceView<CompanySiteCategoryStandardRegion>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompanySiteCategoryStandardRegionDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the CompanySiteCategoryStandardRegionDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public CompanySiteCategoryStandardRegionDataSourceView(CompanySiteCategoryStandardRegionDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal CompanySiteCategoryStandardRegionDataSource CompanySiteCategoryStandardRegionOwner
		{
			get { return Owner as CompanySiteCategoryStandardRegionDataSource; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal CompanySiteCategoryStandardRegionService CompanySiteCategoryStandardRegionProvider
		{
			get { return Provider as CompanySiteCategoryStandardRegionService; }
		}

		#endregion Properties
		
		#region Methods
		
		#endregion Methods
	}

	#region CompanySiteCategoryStandardRegionDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the CompanySiteCategoryStandardRegionDataSource class.
	/// </summary>
	public class CompanySiteCategoryStandardRegionDataSourceDesigner : ReadOnlyDataSourceDesigner<CompanySiteCategoryStandardRegion>
	{
	}

	#endregion CompanySiteCategoryStandardRegionDataSourceDesigner

	#region CompanySiteCategoryStandardRegionFilter

	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CompanySiteCategoryStandardRegion"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompanySiteCategoryStandardRegionFilter : SqlFilter<CompanySiteCategoryStandardRegionColumn>
	{
	}

	#endregion CompanySiteCategoryStandardRegionFilter

	#region CompanySiteCategoryStandardRegionExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CompanySiteCategoryStandardRegion"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompanySiteCategoryStandardRegionExpressionBuilder : SqlExpressionBuilder<CompanySiteCategoryStandardRegionColumn>
	{
	}
	
	#endregion CompanySiteCategoryStandardRegionExpressionBuilder		
}

