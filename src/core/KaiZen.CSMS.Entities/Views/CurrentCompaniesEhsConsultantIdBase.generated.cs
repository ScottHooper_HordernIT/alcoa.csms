﻿/*
	File generated by NetTiers templates [www.nettiers.com]
	Important: Do not modify this file. Edit the file CurrentCompaniesEhsConsultantId.cs instead.
*/
#region Using Directives
using System;
using System.ComponentModel;
using System.Collections;
using System.Runtime.Serialization;
using System.Xml.Serialization;
#endregion

namespace KaiZen.CSMS.Entities
{
	///<summary>
	/// An object representation of the '_Current_Companies_EhsConsultantId' view. [No description found in the database]	
	///</summary>
	[Serializable]
	[CLSCompliant(true)]
	[ToolboxItem("CurrentCompaniesEhsConsultantIdBase")]
	public abstract partial class CurrentCompaniesEhsConsultantIdBase : System.IComparable, System.ICloneable, INotifyPropertyChanged
	{
		
		#region Variable Declarations
		
		/// <summary>
		/// EHSConsultantId : 
		/// </summary>
		private System.Int32?		  _ehsConsultantId = null;
		
		/// <summary>
		/// UserFullName : 
		/// </summary>
		private System.String		  _userFullName = null;
		
		/// <summary>
		/// Object that contains data to associate with this object
		/// </summary>
		private object _tag;
		
		/// <summary>
		/// Suppresses Entity Events from Firing, 
		/// useful when loading the entities from the database.
		/// </summary>
	    [NonSerialized] 
		private bool suppressEntityEvents = false;
		
		#endregion Variable Declarations
		
		#region Constructors
		///<summary>
		/// Creates a new <see cref="CurrentCompaniesEhsConsultantIdBase"/> instance.
		///</summary>
		public CurrentCompaniesEhsConsultantIdBase()
		{
		}		
		
		///<summary>
		/// Creates a new <see cref="CurrentCompaniesEhsConsultantIdBase"/> instance.
		///</summary>
		///<param name="_ehsConsultantId"></param>
		///<param name="_userFullName"></param>
		public CurrentCompaniesEhsConsultantIdBase(System.Int32? _ehsConsultantId, System.String _userFullName)
		{
			this._ehsConsultantId = _ehsConsultantId;
			this._userFullName = _userFullName;
		}
		
		///<summary>
		/// A simple factory method to create a new <see cref="CurrentCompaniesEhsConsultantId"/> instance.
		///</summary>
		///<param name="_ehsConsultantId"></param>
		///<param name="_userFullName"></param>
		public static CurrentCompaniesEhsConsultantId CreateCurrentCompaniesEhsConsultantId(System.Int32? _ehsConsultantId, System.String _userFullName)
		{
			CurrentCompaniesEhsConsultantId newCurrentCompaniesEhsConsultantId = new CurrentCompaniesEhsConsultantId();
			newCurrentCompaniesEhsConsultantId.EhsConsultantId = _ehsConsultantId;
			newCurrentCompaniesEhsConsultantId.UserFullName = _userFullName;
			return newCurrentCompaniesEhsConsultantId;
		}
				
		#endregion Constructors
		
		#region Properties	
		/// <summary>
		/// 	Gets or Sets the EHSConsultantId property. 
		///		
		/// </summary>
		/// <value>This type is int</value>
		/// <remarks>
		/// This property can be set to null. 
		/// If this column is null, this property will return (int)0. It is up to the developer
		/// to check the value of IsEhsConsultantIdNull() and perform business logic appropriately.
		/// </remarks>
		[XmlElement(IsNullable=true)]
		[DescriptionAttribute(""), System.ComponentModel.Bindable( System.ComponentModel.BindableSupport.Yes)]
		public virtual System.Int32? EhsConsultantId
		{
			get
			{
				return this._ehsConsultantId; 
			}
			set
			{
				if (_ehsConsultantId == value && EhsConsultantId != null )
					return;
					
				this._ehsConsultantId = value;
				this._isDirty = true;
				
				OnPropertyChanged("EhsConsultantId");
			}
		}
		
		/// <summary>
		/// 	Gets or Sets the UserFullName property. 
		///		
		/// </summary>
		/// <value>This type is varchar</value>
		/// <remarks>
		/// This property can be set to null. 
		/// </remarks>
		[XmlElement(IsNullable=true)]
		[DescriptionAttribute(""), System.ComponentModel.Bindable( System.ComponentModel.BindableSupport.Yes)]
		public virtual System.String UserFullName
		{
			get
			{
				return this._userFullName; 
			}
			set
			{
				if (_userFullName == value)
					return;
					
				this._userFullName = value;
				this._isDirty = true;
				
				OnPropertyChanged("UserFullName");
			}
		}
		
		
		/// <summary>
		///     Gets or sets the object that contains supplemental data about this object.
		/// </summary>
		/// <value>Object</value>
		[System.ComponentModel.Bindable(false)]
		[LocalizableAttribute(false)]
		[DescriptionAttribute("Object containing data to be associated with this object")]
		public virtual object Tag
		{
			get
			{
				return this._tag;
			}
			set
			{
				if (this._tag == value)
					return;
		
				this._tag = value;
			}
		}
	
		/// <summary>
		/// Determines whether this entity is to suppress events while set to true.
		/// </summary>
		[System.ComponentModel.Bindable(false)]
		[BrowsableAttribute(false), XmlIgnoreAttribute()]
		public bool SuppressEntityEvents
		{	
			get
			{
				return suppressEntityEvents;
			}
			set
			{
				suppressEntityEvents = value;
			}	
		}

		private bool _isDeleted = false;
		/// <summary>
		/// Gets a value indicating if object has been <see cref="MarkToDelete"/>. ReadOnly.
		/// </summary>
		[BrowsableAttribute(false), XmlIgnoreAttribute()]
		public virtual bool IsDeleted
		{
			get { return this._isDeleted; }
		}


		private bool _isDirty = false;
		/// <summary>
		///	Gets a value indicating  if the object has been modified from its original state.
		/// </summary>
		///<value>True if object has been modified from its original state; otherwise False;</value>
		[BrowsableAttribute(false), XmlIgnoreAttribute()]
		public virtual bool IsDirty
		{
			get { return this._isDirty; }
		}
		

		private bool _isNew = true;
		/// <summary>
		///	Gets a value indicating if the object is new.
		/// </summary>
		///<value>True if objectis new; otherwise False;</value>
		[BrowsableAttribute(false), XmlIgnoreAttribute()]
		public virtual bool IsNew
		{
			get { return this._isNew; }
			set { this._isNew = value; }
		}

		/// <summary>
		///		The name of the underlying database table.
		/// </summary>
		[BrowsableAttribute(false), XmlIgnoreAttribute()]
		public string ViewName
		{
			get { return "_Current_Companies_EhsConsultantId"; }
		}

		
		#endregion
		
		#region Methods	
		
		/// <summary>
		/// Accepts the changes made to this object by setting each flags to false.
		/// </summary>
		public virtual void AcceptChanges()
		{
			this._isDeleted = false;
			this._isDirty = false;
			this._isNew = false;
			OnPropertyChanged(string.Empty);
		}
		
		
		///<summary>
		///  Revert all changes and restore original values.
		///  Currently not supported.
		///</summary>
		/// <exception cref="NotSupportedException">This method is not currently supported and always throws this exception.</exception>
		public virtual void CancelChanges()
		{
			throw new NotSupportedException("Method currently not Supported.");
		}
		
		///<summary>
		///   Marks entity to be deleted.
		///</summary>
		public virtual void MarkToDelete()
		{
			this._isDeleted = true;
		}
		
		#region ICloneable Members
		///<summary>
		///  Returns a Typed CurrentCompaniesEhsConsultantIdBase Entity 
		///</summary>
		public virtual CurrentCompaniesEhsConsultantIdBase Copy()
		{
			//shallow copy entity
			CurrentCompaniesEhsConsultantId copy = new CurrentCompaniesEhsConsultantId();
				copy.EhsConsultantId = this.EhsConsultantId;
				copy.UserFullName = this.UserFullName;
			copy.AcceptChanges();
			return (CurrentCompaniesEhsConsultantId)copy;
		}
		
		///<summary>
		/// ICloneable.Clone() Member, returns the Deep Copy of this entity.
		///</summary>
		public object Clone(){
			return this.Copy();
		}
		
		///<summary>
		/// Returns a deep copy of the child collection object passed in.
		///</summary>
		public static object MakeCopyOf(object x)
		{
			if (x is ICloneable)
			{
				// Return a deep copy of the object
				return ((ICloneable)x).Clone();
			}
			else
				throw new System.NotSupportedException("Object Does Not Implement the ICloneable Interface.");
		}
		#endregion
		
		
		///<summary>
		/// Returns a value indicating whether this instance is equal to a specified object.
		///</summary>
		///<param name="toObject">An object to compare to this instance.</param>
		///<returns>true if toObject is a <see cref="CurrentCompaniesEhsConsultantIdBase"/> and has the same value as this instance; otherwise, false.</returns>
		public virtual bool Equals(CurrentCompaniesEhsConsultantIdBase toObject)
		{
			if (toObject == null)
				return false;
			return Equals(this, toObject);
		}
		
		
		///<summary>
		/// Determines whether the specified <see cref="CurrentCompaniesEhsConsultantIdBase"/> instances are considered equal.
		///</summary>
		///<param name="Object1">The first <see cref="CurrentCompaniesEhsConsultantIdBase"/> to compare.</param>
		///<param name="Object2">The second <see cref="CurrentCompaniesEhsConsultantIdBase"/> to compare. </param>
		///<returns>true if Object1 is the same instance as Object2 or if both are null references or if objA.Equals(objB) returns true; otherwise, false.</returns>
		public static bool Equals(CurrentCompaniesEhsConsultantIdBase Object1, CurrentCompaniesEhsConsultantIdBase Object2)
		{
			// both are null
			if (Object1 == null && Object2 == null)
				return true;

			// one or the other is null, but not both
			if (Object1 == null ^ Object2 == null)
				return false;

			bool equal = true;
			if (Object1.EhsConsultantId != null && Object2.EhsConsultantId != null )
			{
				if (Object1.EhsConsultantId != Object2.EhsConsultantId)
					equal = false;
			}
			else if (Object1.EhsConsultantId == null ^ Object1.EhsConsultantId == null )
			{
				equal = false;
			}
			if (Object1.UserFullName != null && Object2.UserFullName != null )
			{
				if (Object1.UserFullName != Object2.UserFullName)
					equal = false;
			}
			else if (Object1.UserFullName == null ^ Object1.UserFullName == null )
			{
				equal = false;
			}
			return equal;
		}
		
		#endregion
		
		#region IComparable Members
		///<summary>
		/// Compares this instance to a specified object and returns an indication of their relative values.
		///<param name="obj">An object to compare to this instance, or a null reference (Nothing in Visual Basic).</param>
		///</summary>
		///<returns>A signed integer that indicates the relative order of this instance and obj.</returns>
		public virtual int CompareTo(object obj)
		{
			throw new NotImplementedException();
		}
	
		#endregion
		
		#region INotifyPropertyChanged Members
		
		/// <summary>
      /// Event to indicate that a property has changed.
      /// </summary>
		[field:NonSerialized]
		public event PropertyChangedEventHandler PropertyChanged;

		/// <summary>
      /// Called when a property is changed
      /// </summary>
      /// <param name="propertyName">The name of the property that has changed.</param>
		protected virtual void OnPropertyChanged(string propertyName)
		{ 
			OnPropertyChanged(new PropertyChangedEventArgs(propertyName));
		}
		
		/// <summary>
      /// Called when a property is changed
      /// </summary>
      /// <param name="e">PropertyChangedEventArgs</param>
		protected virtual void OnPropertyChanged(PropertyChangedEventArgs e)
		{
			if (!SuppressEntityEvents)
			{
				if (null != PropertyChanged)
				{
					PropertyChanged(this, e);
				}
			}
		}
		
		#endregion
				
		/// <summary>
		/// Gets the property value by name.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <param name="propertyName">Name of the property.</param>
		/// <returns></returns>
		public static object GetPropertyValueByName(CurrentCompaniesEhsConsultantId entity, string propertyName)
		{
			switch (propertyName)
			{
				case "EhsConsultantId":
					return entity.EhsConsultantId;
				case "UserFullName":
					return entity.UserFullName;
			}
			return null;
		}
				
		/// <summary>
		/// Gets the property value by name.
		/// </summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <returns></returns>
		public object GetPropertyValueByName(string propertyName)
		{			
			return GetPropertyValueByName(this as CurrentCompaniesEhsConsultantId, propertyName);
		}
		
		///<summary>
		/// Returns a String that represents the current object.
		///</summary>
		public override string ToString()
		{
			return string.Format(System.Globalization.CultureInfo.InvariantCulture,
				"{3}{2}- EhsConsultantId: {0}{2}- UserFullName: {1}{2}", 
				(this.EhsConsultantId == null) ? string.Empty : this.EhsConsultantId.ToString(),
			     
				(this.UserFullName == null) ? string.Empty : this.UserFullName.ToString(),
			     
				System.Environment.NewLine, 
				this.GetType());
		}
	
	}//End Class
	
	
	/// <summary>
	/// Enumerate the CurrentCompaniesEhsConsultantId columns.
	/// </summary>
	[Serializable]
	public enum CurrentCompaniesEhsConsultantIdColumn
	{
		/// <summary>
		/// EHSConsultantId : 
		/// </summary>
		[EnumTextValue("EHSConsultantId")]
		[ColumnEnum("EHSConsultantId", typeof(System.Int32), System.Data.DbType.Int32, false, false, true)]
		EhsConsultantId,
		/// <summary>
		/// UserFullName : 
		/// </summary>
		[EnumTextValue("UserFullName")]
		[ColumnEnum("UserFullName", typeof(System.String), System.Data.DbType.AnsiString, false, false, true, 213)]
		UserFullName
	}//End enum

} // end namespace
