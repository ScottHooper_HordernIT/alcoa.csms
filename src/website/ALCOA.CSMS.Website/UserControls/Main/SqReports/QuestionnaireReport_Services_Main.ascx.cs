using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using DevExpress.Data.Filtering;

using KaiZen.CSMS.Data;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Services;
using KaiZen.CSMS.Web;
using System.Drawing;
using DevExpress.Web.ASPxGridView;

public partial class UserControls_Tiny_QuestionnaireReports_QuestionnaireReport_Services_Main : System.Web.UI.UserControl
{
    Auth auth = new Auth();
    protected void Page_Load(object sender, EventArgs e) {  Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); }

    private void moduleInit(bool postBack)
    {
        if (!postBack)
        {
            if (auth.RoleId == (int)RoleList.Contractor)
            {
                QuestionnaireReportServicesService qrsService = new QuestionnaireReportServicesService();
                DataSet dsServices = qrsService.GetAllWithEbiActiveSitesThisYear_ByCompanyId(auth.CompanyId);
                Session["dtQuestionnaireReportServices"] = dsServices.Tables[0];
                grid.Columns["Company"].Visible = false;
            }
            else
            {
                QuestionnaireReportServicesService qrsService = new QuestionnaireReportServicesService();
                DataSet dsServices = qrsService.GetAllWithEbiActiveSitesThisYear();
                Session["dtQuestionnaireReportServices"] = dsServices.Tables[0];
                
            }
            grid.DataSource = (DataTable)Session["dtQuestionnaireReportServices"];
            grid.DataBind();
           // grid.FilterExpression = "Status = 3 AND Recommended = 1";// change by Debashis for 'No data issue'
        }
        else
        {
            grid.DataSource = (DataTable)Session["dtQuestionnaireReportServices"];
            //grid.DataBind();
            //Session["SelectedSiteQSRM"] = Convert.ToString(Session["SelectedSiteQSRM"]).ToUpper();
            //if (Session["SelectedSiteQSRM"] != "" && Session["SelectedSiteQSRM"] != "ALL")
            //    grid.FilterExpression = "ActiveSites like '%" + Session["SelectedSiteQSRM"] + "%'";
        }

        // Export
        String exportFileName = @"ALCOA CSMS - Safety Qualification Report - Services - Main"; //Chrome & IE is fine.
        String userAgent = Request.Headers.Get("User-Agent");
        if (
            (userAgent.Contains("Mozilla") && !userAgent.Contains("AppleWebKit") && !userAgent.Contains("MSIE")) || //Mozilla
            (userAgent.Contains("AppleWebKit") && !userAgent.Contains("Chrome"))      //Safari
            )
        {
            exportFileName = exportFileName.Replace(" ", "_");
        }
        Helper.ExportGrid.Settings(ucExportButtons, exportFileName, true, true);
    }

    protected void grid_HtmlDataCellPrepared(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewTableDataCellEventArgs e)
    {
        try
        {
            if (e.DataColumn == null || e.DataColumn.FieldName != "Recommended") return;
            bool value = (bool)e.GetValue("Recommended");
            if (value == true)
            {
                e.Cell.ForeColor = Color.Green;
            }
            else
            {
                e.Cell.ForeColor = Color.Red;
            }
        }
        catch { }
    }

    protected void grid_HeaderFilterFillItems(object sender, ASPxGridViewHeaderFilterEventArgs e)
    {
        DataTable dtTable = ((DataView)SitesDataSource1.Select(DataSourceSelectArguments.Empty)).ToTable();
        string sitename = string.Empty;
        if (e.Column.FieldName == "ActiveSites")
        {
            e.Values.Clear();
            e.AddShowAll();
            e.AddValue("(Blanks)", "", "[ActiveSites] is null");
            e.AddValue("(Non Blanks)", "", "[ActiveSites] is not null");
            for (int i = 0; i < dtTable.Rows.Count; i++)
            {
                sitename = dtTable.Rows[i]["SiteNameEbi"].ToString();
                e.AddValue(sitename, "", "[ActiveSites] like '%" + sitename + "%'");
            }

        }


    }

}
