﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.CompanySiteCategoryException2Provider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(CompanySiteCategoryException2DataSourceDesigner))]
	public class CompanySiteCategoryException2DataSource : ProviderDataSource<CompanySiteCategoryException2, CompanySiteCategoryException2Key>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompanySiteCategoryException2DataSource class.
		/// </summary>
		public CompanySiteCategoryException2DataSource() : base(new CompanySiteCategoryException2Service())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the CompanySiteCategoryException2DataSourceView used by the CompanySiteCategoryException2DataSource.
		/// </summary>
		protected CompanySiteCategoryException2DataSourceView CompanySiteCategoryException2View
		{
			get { return ( View as CompanySiteCategoryException2DataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the CompanySiteCategoryException2DataSource control invokes to retrieve data.
		/// </summary>
		public CompanySiteCategoryException2SelectMethod SelectMethod
		{
			get
			{
				CompanySiteCategoryException2SelectMethod selectMethod = CompanySiteCategoryException2SelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (CompanySiteCategoryException2SelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the CompanySiteCategoryException2DataSourceView class that is to be
		/// used by the CompanySiteCategoryException2DataSource.
		/// </summary>
		/// <returns>An instance of the CompanySiteCategoryException2DataSourceView class.</returns>
		protected override BaseDataSourceView<CompanySiteCategoryException2, CompanySiteCategoryException2Key> GetNewDataSourceView()
		{
			return new CompanySiteCategoryException2DataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the CompanySiteCategoryException2DataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class CompanySiteCategoryException2DataSourceView : ProviderDataSourceView<CompanySiteCategoryException2, CompanySiteCategoryException2Key>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompanySiteCategoryException2DataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the CompanySiteCategoryException2DataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public CompanySiteCategoryException2DataSourceView(CompanySiteCategoryException2DataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal CompanySiteCategoryException2DataSource CompanySiteCategoryException2Owner
		{
			get { return Owner as CompanySiteCategoryException2DataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal CompanySiteCategoryException2SelectMethod SelectMethod
		{
			get { return CompanySiteCategoryException2Owner.SelectMethod; }
			set { CompanySiteCategoryException2Owner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal CompanySiteCategoryException2Service CompanySiteCategoryException2Provider
		{
			get { return Provider as CompanySiteCategoryException2Service; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<CompanySiteCategoryException2> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<CompanySiteCategoryException2> results = null;
			CompanySiteCategoryException2 item;
			count = 0;
			
			System.Int32 _companySiteCategoryException2Id;
			System.Int32 _companyId;
			System.Int32 _siteId;
			System.Int32 _qtr;
			System.Int32 _companySiteCategoryId;

			switch ( SelectMethod )
			{
				case CompanySiteCategoryException2SelectMethod.Get:
					CompanySiteCategoryException2Key entityKey  = new CompanySiteCategoryException2Key();
					entityKey.Load(values);
					item = CompanySiteCategoryException2Provider.Get(entityKey);
					results = new TList<CompanySiteCategoryException2>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case CompanySiteCategoryException2SelectMethod.GetAll:
                    results = CompanySiteCategoryException2Provider.GetAll(StartIndex, PageSize, out count);
                    break;
				case CompanySiteCategoryException2SelectMethod.GetPaged:
					results = CompanySiteCategoryException2Provider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case CompanySiteCategoryException2SelectMethod.Find:
					if ( FilterParameters != null )
						results = CompanySiteCategoryException2Provider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = CompanySiteCategoryException2Provider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case CompanySiteCategoryException2SelectMethod.GetByCompanySiteCategoryException2Id:
					_companySiteCategoryException2Id = ( values["CompanySiteCategoryException2Id"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["CompanySiteCategoryException2Id"], typeof(System.Int32)) : (int)0;
					item = CompanySiteCategoryException2Provider.GetByCompanySiteCategoryException2Id(_companySiteCategoryException2Id);
					results = new TList<CompanySiteCategoryException2>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				case CompanySiteCategoryException2SelectMethod.GetByCompanyIdSiteIdQtr:
					_companyId = ( values["CompanyId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["CompanyId"], typeof(System.Int32)) : (int)0;
					_siteId = ( values["SiteId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["SiteId"], typeof(System.Int32)) : (int)0;
					_qtr = ( values["Qtr"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["Qtr"], typeof(System.Int32)) : (int)0;
					item = CompanySiteCategoryException2Provider.GetByCompanyIdSiteIdQtr(_companyId, _siteId, _qtr);
					results = new TList<CompanySiteCategoryException2>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case CompanySiteCategoryException2SelectMethod.GetByCompanyIdSiteId:
					_companyId = ( values["CompanyId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["CompanyId"], typeof(System.Int32)) : (int)0;
					_siteId = ( values["SiteId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["SiteId"], typeof(System.Int32)) : (int)0;
					results = CompanySiteCategoryException2Provider.GetByCompanyIdSiteId(_companyId, _siteId, this.StartIndex, this.PageSize, out count);
					break;
				// FK
				case CompanySiteCategoryException2SelectMethod.GetByCompanyId:
					_companyId = ( values["CompanyId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["CompanyId"], typeof(System.Int32)) : (int)0;
					results = CompanySiteCategoryException2Provider.GetByCompanyId(_companyId, this.StartIndex, this.PageSize, out count);
					break;
				case CompanySiteCategoryException2SelectMethod.GetByCompanySiteCategoryId:
					_companySiteCategoryId = ( values["CompanySiteCategoryId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["CompanySiteCategoryId"], typeof(System.Int32)) : (int)0;
					results = CompanySiteCategoryException2Provider.GetByCompanySiteCategoryId(_companySiteCategoryId, this.StartIndex, this.PageSize, out count);
					break;
				case CompanySiteCategoryException2SelectMethod.GetBySiteId:
					_siteId = ( values["SiteId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["SiteId"], typeof(System.Int32)) : (int)0;
					results = CompanySiteCategoryException2Provider.GetBySiteId(_siteId, this.StartIndex, this.PageSize, out count);
					break;
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == CompanySiteCategoryException2SelectMethod.Get || SelectMethod == CompanySiteCategoryException2SelectMethod.GetByCompanySiteCategoryException2Id )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				CompanySiteCategoryException2 entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					CompanySiteCategoryException2Provider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<CompanySiteCategoryException2> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			CompanySiteCategoryException2Provider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region CompanySiteCategoryException2DataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the CompanySiteCategoryException2DataSource class.
	/// </summary>
	public class CompanySiteCategoryException2DataSourceDesigner : ProviderDataSourceDesigner<CompanySiteCategoryException2, CompanySiteCategoryException2Key>
	{
		/// <summary>
		/// Initializes a new instance of the CompanySiteCategoryException2DataSourceDesigner class.
		/// </summary>
		public CompanySiteCategoryException2DataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public CompanySiteCategoryException2SelectMethod SelectMethod
		{
			get { return ((CompanySiteCategoryException2DataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new CompanySiteCategoryException2DataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region CompanySiteCategoryException2DataSourceActionList

	/// <summary>
	/// Supports the CompanySiteCategoryException2DataSourceDesigner class.
	/// </summary>
	internal class CompanySiteCategoryException2DataSourceActionList : DesignerActionList
	{
		private CompanySiteCategoryException2DataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the CompanySiteCategoryException2DataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public CompanySiteCategoryException2DataSourceActionList(CompanySiteCategoryException2DataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public CompanySiteCategoryException2SelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion CompanySiteCategoryException2DataSourceActionList
	
	#endregion CompanySiteCategoryException2DataSourceDesigner
	
	#region CompanySiteCategoryException2SelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the CompanySiteCategoryException2DataSource.SelectMethod property.
	/// </summary>
	public enum CompanySiteCategoryException2SelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByCompanySiteCategoryException2Id method.
		/// </summary>
		GetByCompanySiteCategoryException2Id,
		/// <summary>
		/// Represents the GetByCompanyIdSiteIdQtr method.
		/// </summary>
		GetByCompanyIdSiteIdQtr,
		/// <summary>
		/// Represents the GetByCompanyIdSiteId method.
		/// </summary>
		GetByCompanyIdSiteId,
		/// <summary>
		/// Represents the GetByCompanyId method.
		/// </summary>
		GetByCompanyId,
		/// <summary>
		/// Represents the GetByCompanySiteCategoryId method.
		/// </summary>
		GetByCompanySiteCategoryId,
		/// <summary>
		/// Represents the GetBySiteId method.
		/// </summary>
		GetBySiteId
	}
	
	#endregion CompanySiteCategoryException2SelectMethod

	#region CompanySiteCategoryException2Filter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CompanySiteCategoryException2"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompanySiteCategoryException2Filter : SqlFilter<CompanySiteCategoryException2Column>
	{
	}
	
	#endregion CompanySiteCategoryException2Filter

	#region CompanySiteCategoryException2ExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CompanySiteCategoryException2"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompanySiteCategoryException2ExpressionBuilder : SqlExpressionBuilder<CompanySiteCategoryException2Column>
	{
	}
	
	#endregion CompanySiteCategoryException2ExpressionBuilder	

	#region CompanySiteCategoryException2Property
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;CompanySiteCategoryException2ChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="CompanySiteCategoryException2"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompanySiteCategoryException2Property : ChildEntityProperty<CompanySiteCategoryException2ChildEntityTypes>
	{
	}
	
	#endregion CompanySiteCategoryException2Property
}

