-- Server : AUABGN-tst5 -- 

USE [AuA_CSMS_Dev_Medium] 
GO 













  
       
/*          
----------------------------------------------------------------------------------------------------          
-- Date Created: Friday, 18 March 2011          
          
-- Created By: Yujia Li @ Kai-Zen Business-IT Solutions (www.kai-zen.com.au)          
-- Modified By: Sayani Sil @ Tata Consultency Services       
-- Purpose: ComplianceTable - All Companies - Specific Site     
--
--Modified by Ashley Goldstraw to add in Company Site Category Exception     15/9/2015
----------------------------------------------------------------------------------------------------          
          
*/          
CREATE PROCEDURE [dbo].[_KpiCompanySiteCategory_Compliance_Table_AS]          
(          
 @SiteId int,          
 @Year int,          
 @CompanySiteCategoryId varchar(1)          
)          
AS          

SELECT --Yearly  
	0 as MonthId,
	'Year' as [MonthAbbrev],		        
	ISNULL((SELECT SUM(aheaPeakNopplSiteWeek) FROM KpiCompanySiteCategory WHERE YEAR(kpiDateTime) = @Year AND Month(kpiDateTime) = MONTH(dateadd("m",-1, GETDATE())) AND CompanySiteCategoryId LIKE @CompanySiteCategoryId AND SiteId = @SiteId),0) As [Peak No Employees (Previous Month)],          
	ISNULL(AVG(aheaPeakNopplSiteWeek),0) As [Peak No. Of People On Site],          
	ISNULL(SUM(aheaAvgNopplSiteMonth),0) As [Average No. Of People On Site],          
	(SELECT COUNT(DISTINCT CompanyId) FROM CompanySiteCategoryStandard WHERE SiteId = @SiteId AND dbo.GetCompanySiteCategoryExceptionId(SiteId,CompanyId,DateFromParts(@Year,MONTH(dateadd("m",0, GETDATE())),1),CompanySiteCategoryId) like @CompanySiteCategoryId) As [Number Of Companies],   
	(SELECT COUNT(DISTINCT CompanyId) FROM KpiCompanySiteCategory WHERE YEAR(kpiDateTime) = @Year AND dbo.GetCompanySiteCategoryExceptionId(SiteId,CompanyId,DateFromParts(@Year,MONTH(dateadd("m",0, GETDATE())),1),CompanySiteCategoryId) LIKE @CompanySiteCategoryId AND SiteId = @SiteId AND (EbiOnSite = 1 OR AheaTotalManHours > 0)) As [Number of Companies On Site],
	ISNULL(SUM(COALESCE(aheaTotalManHours,0)),0) As [Total Man Hours],                
	(SELECT COUNT(DISTINCT FileDb.CompanyId) FROM FileDb INNER JOIN CompanySiteCategoryStandard ON FileDb.CompanyId = CompanySiteCategoryStandard.CompanyId WHERE CompanySiteCategoryStandard.SiteId = @SiteId AND FileDb.Description LIKE CAST(@Year As varchar(4)) + '%' AND dbo.GetCompanySiteCategoryExceptionId(SiteId,FileDb.CompanyId,DateFromParts(@Year,MONTH(dateadd("m",0, GETDATE())),1),CompanySiteCategoryStandard.CompanySiteCategoryId) LIKE @CompanySiteCategoryId) As [SmpNo],          
	(SELECT COUNT(DISTINCT(CAST(CompanyId as varchar(5)) + '&' + CAST(SiteId As varchar(5)))) FROM CompanySiteCategoryStandard WHERE CompanySiteCategoryStandard.SiteId = @SiteId AND dbo.GetCompanySiteCategoryExceptionId(SiteId,CompanyId,DateFromParts(@Year,MONTH(dateadd("m",0, GETDATE())),1),CompanySiteCategoryStandard.CompanySiteCategoryId) LIKE @CompanySiteCategoryId) As [SmpExpected],          
	ISNULL(CAST((ISNULL((SUM(COALESCE(ipLTI,0)) * 200000),0) / NULLIF(ISNULL(COALESCE((SUM(COALESCE(aheaTotalManHours,0))),0),0),0)) AS Decimal(18,2)),0) As [LWDFR],          
	ISNULL(((CAST((CASE WHEN (SUM(COALESCE(aheaTotalManHours,0)) = 0.0) THEN 0.0 ELSE (((SUM(COALESCE(ipMTI,0)) + SUM(COALESCE(ipRDI,0)) + SUM(COALESCE(ipLTI,0))) * 200000) / (SUM(COALESCE(aheaTotalManHours,0)))) END) AS DECIMAL(18,2)))),0) As [TRIFR],          
	ISNULL(((CAST((CASE WHEN (SUM(COALESCE(aheaTotalManHours,0)) = 0.0) THEN 0.0 ELSE (((SUM(COALESCE(ipFATI,0)) + SUM(COALESCE(ipMTI,0)) + SUM(COALESCE(ipRDI,0)) + SUM(COALESCE(ipLTI,0))) * 200000) / (SUM(COALESCE(aheaTotalManHours,0)))) END) AS DECIMAL(18,2)))),0) As [AIFR],          
	(SELECT ISNULL(CAST((CASE WHEN ((SUM(COALESCE(dbo.KpiCompanySiteCategory.ipLTI,0)) + SUM(COALESCE(dbo.KpiCompanySiteCategory.ipRDI,0)) + SUM(COALESCE(dbo.KpiCompanySiteCategory.ipMTI,0)) + SUM(COALESCE(dbo.KpiCompanySiteCategory.ipFATI,0)) ) = 0.0) THEN CAST(SUM(COALESCE(dbo.KpiCompanySiteCategory.ipIFE,0)) AS DECIMAL(18,2)) ELSE (CAST(SUM(COALESCE(dbo.KpiCompanySiteCategory.ipIFE,0)) AS DECIMAL(18,2)) / CAST((SUM(COALESCE(dbo.KpiCompanySiteCategory.ipLTI,0)) + SUM(COALESCE(dbo.KpiCompanySiteCategory.ipRDI,0)) + SUM(COALESCE(dbo.KpiCompanySiteCategory.ipMTI,0)) + SUM(COALESCE(dbo.KpiCompanySiteCategory.ipFATI,0))) AS DECIMAL(18,2))) END) AS DECIMAL(18,2)),0)) As [IFEInjuryRatio],
	(SELECT ISNULL(CAST(CASE WHEN (COALESCE(ISNULL(SUM(dbo.KpiCompanySiteCategory.aheaTotalManHours),0),0) = 0) THEN (0) ELSE ((((ISNULL(SUM(dbo.KpiCompanySiteCategory.ipLTI),0) + (ISNULL(SUM(dbo.KpiCompanySiteCategory.ipRDI),0)))) * 200000) / (SUM(dbo.KpiCompanySiteCategory.aheaTotalManHours))) END As Decimal(18,2)),0)) As [DART],        
	(SELECT ISNULL(((CAST((CASE  WHEN (SUM(COALESCE(dbo.KpiCompanySiteCategory.ipRN,0)) = 0) THEN 0 ELSE (SUM(COALESCE(dbo.KpiCompanySiteCategory.ipRN,0))) END) AS int))),0)) as [Risk Notifications],            
	(SELECT COUNT(*) As [NoCsa] FROM CSA WHERE SiteId = @SiteId and Year = @Year and QtrId <> 5 AND ((SELECT COUNT(*) FROM CompanySiteCategoryStandard WHERE CompanySiteCategoryStandard.CompanyId = CSA.CompanyId AND CompanySiteCategoryStandard.SiteId = CSA.SiteId and dbo.GetCompanySiteCategoryExceptionId(SiteId,CompanyId,DateFromParts(@Year,MONTH(dateadd("m",0, GETDATE())),1),CompanySiteCategoryStandard.CompanySiteCategoryId) LIKE @CompanySiteCategoryId) > 0)) As [NoCsa]
FROM KpiCompanySiteCategory 
WHERE YEAR(kpiDateTime) = @Year 
AND dbo.GetCompanySiteCategoryExceptionId(SiteId,CompanyId,kpiDateTime,CompanySiteCategoryId) LIKE @CompanySiteCategoryId          
AND SiteId = @SiteId   
UNION
SELECT --Monthly  
	m.MonthId,
	m.MonthAbbrev,        
	0 As [Peak No Employees (Previous Month)],  --Previous month calculation only, no breakdown required      
	ISNULL(AVG(aheaPeakNopplSiteWeek),0) As [Peak No. Of People On Site],          
	ISNULL(SUM(aheaAvgNopplSiteMonth),0) As [Average No. Of People On Site],          
	COUNT(distinct(CompanyId)) As [Number Of Companies],  
	(SELECT COUNT(DISTINCT CompanyId) FROM KpiCompanySiteCategory WHERE YEAR(kpiDateTime) = @Year AND MONTH(kpiDateTime) = m.MonthId AND dbo.GetCompanySiteCategoryExceptionId(SiteId,CompanyId,DateFromParts(@Year,MONTH(dateadd("m",0, GETDATE())),1),CompanySiteCategoryId) LIKE @CompanySiteCategoryId AND SiteId = @SiteId AND (EbiOnSite = 1 OR AheaTotalManHours > 0)) As [Number of Companies On Site],
	ISNULL(SUM(COALESCE(aheaTotalManHours,0)),0) As [Total Man Hours],                
	0 As [SmpNo], --This is ridiculous, searching year on part of the description???  I can't filter this down by month as it's not in the description
	0 As [SmpExpected],  --Again, not searching by year so how can I search by month??        
	ISNULL(CAST((ISNULL((SUM(COALESCE(ipLTI,0)) * 200000),0) / NULLIF(ISNULL(COALESCE((SUM(COALESCE(aheaTotalManHours,0))),0),0),0)) AS Decimal(18,2)),0) As [LWDFR],          
	ISNULL(((CAST((CASE WHEN (SUM(COALESCE(aheaTotalManHours,0)) = 0.0) THEN 0.0 ELSE (((SUM(COALESCE(ipMTI,0)) + SUM(COALESCE(ipRDI,0)) + SUM(COALESCE(ipLTI,0))) * 200000) / (SUM(COALESCE(aheaTotalManHours,0)))) END) AS DECIMAL(18,2)))),0) As [TRIFR],          
	ISNULL(((CAST((CASE WHEN (SUM(COALESCE(aheaTotalManHours,0)) = 0.0) THEN 0.0 ELSE (((SUM(COALESCE(ipFATI,0)) + SUM(COALESCE(ipMTI,0)) + SUM(COALESCE(ipRDI,0)) + SUM(COALESCE(ipLTI,0))) * 200000) / (SUM(COALESCE(aheaTotalManHours,0)))) END) AS DECIMAL(18,2)))),0) As [AIFR],          
	(SELECT ISNULL(CAST((CASE WHEN ((SUM(COALESCE(k.ipLTI,0)) + SUM(COALESCE(k.ipRDI,0)) + SUM(COALESCE(k.ipMTI,0)) + SUM(COALESCE(k.ipFATI,0)) ) = 0.0) THEN CAST(SUM(COALESCE(k.ipIFE,0)) AS DECIMAL(18,2)) ELSE (CAST(SUM(COALESCE(k.ipIFE,0)) AS DECIMAL(18,2)) / CAST((SUM(COALESCE(k.ipLTI,0)) + SUM(COALESCE(k.ipRDI,0)) + SUM(COALESCE(k.ipMTI,0)) + SUM(COALESCE(k.ipFATI,0))) AS DECIMAL(18,2))) END) AS DECIMAL(18,2)),0)) As [IFEInjuryRatio],
	(SELECT ISNULL(CAST(CASE WHEN (COALESCE(ISNULL(SUM(k.aheaTotalManHours),0),0) = 0) THEN (0) ELSE ((((ISNULL(SUM(k.ipLTI),0) + (ISNULL(SUM(k.ipRDI),0)))) * 200000) / (SUM(k.aheaTotalManHours))) END As Decimal(18,2)),0)) As [DART],        
	(SELECT ISNULL(((CAST((CASE  WHEN (SUM(COALESCE(k.ipRN,0)) = 0) THEN 0 ELSE (SUM(COALESCE(k.ipRN,0))) END) AS int))),0)) as [Risk Notifications],            
	0 As [NoCsa] --No month field in CSA table?? 
FROM	KpiCompanySiteCategory k
		left join Months m on m.MonthId = MONTH(k.kpiDateTime)
WHERE YEAR(kpiDateTime) = @Year 
AND dbo.GetCompanySiteCategoryExceptionId(SiteId,CompanyId,kpiDateTime,CompanySiteCategoryId) LIKE @CompanySiteCategoryId          
AND SiteId = @SiteId  	
GROUP BY m.MonthId, m.MonthAbbrev	          
          
Select @@ROWCOUNT 
------------------------------------

--SELECT * FROM dbo.KpiCompanySiteCategory kcsc	



