﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface IMonthService
    {
        Month Get(Expression<Func<Month, bool>> where, List<Expression<Func<Month, object>>> includes);
        List<Month> GetMany(int? top, Expression<Func<Month, bool>> where, Func<IQueryable<Month>, IOrderedQueryable<Month>> orderBy, List<Expression<Func<Month, object>>> includes);
        List<Month> GetManyPaged(int pageNumber, int pageSize, Expression<Func<Month, bool>> where, Func<IQueryable<Month>, IOrderedQueryable<Month>> orderBy, List<Expression<Func<Month, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<Month, bool>> where, List<Expression<Func<Month, object>>> includes);
        void Insert(Month item, bool saveChanges = true);
        void Update(Month item, bool saveChanges = true);
        void Delete(Month item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class MonthService : IMonthService
    {
        private readonly IRepository<Month> _repository;

        public MonthService(IRepository<Month> repository)
        {
            this._repository = repository;
        }

        public Month Get(Expression<Func<Month, bool>> where, List<Expression<Func<Month, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<Month> GetMany(int? top, Expression<Func<Month, bool>> where, Func<IQueryable<Month>, IOrderedQueryable<Month>> orderBy, List<Expression<Func<Month, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<Month> GetManyPaged(int pageNumber, int pageSize, Expression<Func<Month, bool>> where, Func<IQueryable<Month>, IOrderedQueryable<Month>> orderBy, List<Expression<Func<Month, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<Month, bool>> where, List<Expression<Func<Month, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(Month item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("Month");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(Month item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("Month");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(Month item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("Month");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
