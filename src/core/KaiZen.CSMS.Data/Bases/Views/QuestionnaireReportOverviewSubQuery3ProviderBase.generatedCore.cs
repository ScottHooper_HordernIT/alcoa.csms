﻿#region Using directives

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="QuestionnaireReportOverviewSubQuery3ProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract class QuestionnaireReportOverviewSubQuery3ProviderBaseCore : EntityViewProviderBase<QuestionnaireReportOverviewSubQuery3>
	{
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions
		
		/*
		///<summary>
		/// Fill an VList&lt;QuestionnaireReportOverviewSubQuery3&gt; From a DataSet
		///</summary>
		/// <param name="dataSet">the DataSet</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList&lt;QuestionnaireReportOverviewSubQuery3&gt;"/></returns>
		protected static VList&lt;QuestionnaireReportOverviewSubQuery3&gt; Fill(DataSet dataSet, VList<QuestionnaireReportOverviewSubQuery3> rows, int start, int pagelen)
		{
			if (dataSet.Tables.Count == 1)
			{
				return Fill(dataSet.Tables[0], rows, start, pagelen);
			}
			else
			{
				return new VList<QuestionnaireReportOverviewSubQuery3>();
			}	
		}
		
		
		///<summary>
		/// Fill an VList&lt;QuestionnaireReportOverviewSubQuery3&gt; From a DataTable
		///</summary>
		/// <param name="dataTable">the DataTable that hold the data.</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList<QuestionnaireReportOverviewSubQuery3>"/></returns>
		protected static VList&lt;QuestionnaireReportOverviewSubQuery3&gt; Fill(DataTable dataTable, VList<QuestionnaireReportOverviewSubQuery3> rows, int start, int pagelen)
		{
			int recordnum = 0;
			
			System.Collections.IEnumerator dataRows =  dataTable.Rows.GetEnumerator();
			
			while (dataRows.MoveNext() && (pagelen != 0))
			{
				if(recordnum >= start)
				{
					DataRow row = (DataRow)dataRows.Current;
				
					QuestionnaireReportOverviewSubQuery3 c = new QuestionnaireReportOverviewSubQuery3();
					c.CompanyId = (Convert.IsDBNull(row["CompanyId"]))?(int)0:(System.Int32)row["CompanyId"];
					c.KwiSponsorName = (Convert.IsDBNull(row["KWI_Sponsor_Name"]))?string.Empty:(System.String)row["KWI_Sponsor_Name"];
					c.KwiSponsor = (Convert.IsDBNull(row["KWI_Sponsor"]))?(int)0:(System.Int32?)row["KWI_Sponsor"];
					c.KwiSpaName = (Convert.IsDBNull(row["KWI_Spa_Name"]))?string.Empty:(System.String)row["KWI_Spa_Name"];
					c.KwiSpa = (Convert.IsDBNull(row["KWI_Spa"]))?(int)0:(System.Int32?)row["KWI_Spa"];
					c.PinSponsorName = (Convert.IsDBNull(row["PIN_Sponsor_Name"]))?string.Empty:(System.String)row["PIN_Sponsor_Name"];
					c.PinSponsor = (Convert.IsDBNull(row["PIN_Sponsor"]))?(int)0:(System.Int32?)row["PIN_Sponsor"];
					c.PinSpaName = (Convert.IsDBNull(row["PIN_Spa_Name"]))?string.Empty:(System.String)row["PIN_Spa_Name"];
					c.PinSpa = (Convert.IsDBNull(row["PIN_Spa"]))?(int)0:(System.Int32?)row["PIN_Spa"];
					c.WgpSponsorName = (Convert.IsDBNull(row["WGP_Sponsor_Name"]))?string.Empty:(System.String)row["WGP_Sponsor_Name"];
					c.WgpSponsor = (Convert.IsDBNull(row["WGP_Sponsor"]))?(int)0:(System.Int32?)row["WGP_Sponsor"];
					c.WgpSpaName = (Convert.IsDBNull(row["WGP_Spa_Name"]))?string.Empty:(System.String)row["WGP_Spa_Name"];
					c.WgpSpa = (Convert.IsDBNull(row["WGP_Spa"]))?(int)0:(System.Int32?)row["WGP_Spa"];
					c.HunSponsorName = (Convert.IsDBNull(row["HUN_Sponsor_Name"]))?string.Empty:(System.String)row["HUN_Sponsor_Name"];
					c.HunSponsor = (Convert.IsDBNull(row["HUN_Sponsor"]))?(int)0:(System.Int32?)row["HUN_Sponsor"];
					c.HunSpaName = (Convert.IsDBNull(row["HUN_Spa_Name"]))?string.Empty:(System.String)row["HUN_Spa_Name"];
					c.HunSpa = (Convert.IsDBNull(row["HUN_Spa"]))?(int)0:(System.Int32?)row["HUN_Spa"];
					c.WdlSponsorName = (Convert.IsDBNull(row["WDL_Sponsor_Name"]))?string.Empty:(System.String)row["WDL_Sponsor_Name"];
					c.WdlSponsor = (Convert.IsDBNull(row["WDL_Sponsor"]))?(int)0:(System.Int32?)row["WDL_Sponsor"];
					c.WdlSpaName = (Convert.IsDBNull(row["WDL_Spa_Name"]))?string.Empty:(System.String)row["WDL_Spa_Name"];
					c.WdlSpa = (Convert.IsDBNull(row["WDL_Spa"]))?(int)0:(System.Int32?)row["WDL_Spa"];
					c.BunSponsorName = (Convert.IsDBNull(row["BUN_Sponsor_Name"]))?string.Empty:(System.String)row["BUN_Sponsor_Name"];
					c.BunSponsor = (Convert.IsDBNull(row["BUN_Sponsor"]))?(int)0:(System.Int32?)row["BUN_Sponsor"];
					c.BunSpaName = (Convert.IsDBNull(row["BUN_Spa_Name"]))?string.Empty:(System.String)row["BUN_Spa_Name"];
					c.BunSpa = (Convert.IsDBNull(row["BUN_Spa"]))?(int)0:(System.Int32?)row["BUN_Spa"];
					c.FmlSponsorName = (Convert.IsDBNull(row["FML_Sponsor_Name"]))?string.Empty:(System.String)row["FML_Sponsor_Name"];
					c.FmlSponsor = (Convert.IsDBNull(row["FML_Sponsor"]))?(int)0:(System.Int32?)row["FML_Sponsor"];
					c.FmlSpaName = (Convert.IsDBNull(row["FML_Spa_Name"]))?string.Empty:(System.String)row["FML_Spa_Name"];
					c.FmlSpa = (Convert.IsDBNull(row["FML_Spa"]))?(int)0:(System.Int32?)row["FML_Spa"];
					c.BgnSponsorName = (Convert.IsDBNull(row["BGN_Sponsor_Name"]))?string.Empty:(System.String)row["BGN_Sponsor_Name"];
					c.BgnSponsor = (Convert.IsDBNull(row["BGN_Sponsor"]))?(int)0:(System.Int32?)row["BGN_Sponsor"];
					c.BgnSpaName = (Convert.IsDBNull(row["BGN_Spa_Name"]))?string.Empty:(System.String)row["BGN_Spa_Name"];
					c.BgnSpa = (Convert.IsDBNull(row["BGN_Spa"]))?(int)0:(System.Int32?)row["BGN_Spa"];
					c.CeSponsorName = (Convert.IsDBNull(row["CE_Sponsor_Name"]))?string.Empty:(System.String)row["CE_Sponsor_Name"];
					c.CeSponsor = (Convert.IsDBNull(row["CE_Sponsor"]))?(int)0:(System.Int32?)row["CE_Sponsor"];
					c.CeSpaName = (Convert.IsDBNull(row["CE_Spa_Name"]))?string.Empty:(System.String)row["CE_Spa_Name"];
					c.CeSpa = (Convert.IsDBNull(row["CE_Spa"]))?(int)0:(System.Int32?)row["CE_Spa"];
					c.AngSponsorName = (Convert.IsDBNull(row["ANG_Sponsor_Name"]))?string.Empty:(System.String)row["ANG_Sponsor_Name"];
					c.AngSponsor = (Convert.IsDBNull(row["ANG_Sponsor"]))?(int)0:(System.Int32?)row["ANG_Sponsor"];
					c.AngSpaName = (Convert.IsDBNull(row["ANG_Spa_Name"]))?string.Empty:(System.String)row["ANG_Spa_Name"];
					c.AngSpa = (Convert.IsDBNull(row["ANG_Spa"]))?(int)0:(System.Int32?)row["ANG_Spa"];
					c.PtlSponsorName = (Convert.IsDBNull(row["PTL_Sponsor_Name"]))?string.Empty:(System.String)row["PTL_Sponsor_Name"];
					c.PtlSponsor = (Convert.IsDBNull(row["PTL_Sponsor"]))?(int)0:(System.Int32?)row["PTL_Sponsor"];
					c.PtlSpaName = (Convert.IsDBNull(row["PTL_Spa_Name"]))?string.Empty:(System.String)row["PTL_Spa_Name"];
					c.PtlSpa = (Convert.IsDBNull(row["PTL_Spa"]))?(int)0:(System.Int32?)row["PTL_Spa"];
					c.PthSponsorName = (Convert.IsDBNull(row["PTH_Sponsor_Name"]))?string.Empty:(System.String)row["PTH_Sponsor_Name"];
					c.PthSponsor = (Convert.IsDBNull(row["PTH_Sponsor"]))?(int)0:(System.Int32?)row["PTH_Sponsor"];
					c.PthSpaName = (Convert.IsDBNull(row["PTH_Spa_Name"]))?string.Empty:(System.String)row["PTH_Spa_Name"];
					c.PthSpa = (Convert.IsDBNull(row["PTH_Spa"]))?(int)0:(System.Int32?)row["PTH_Spa"];
					c.PelSponsorName = (Convert.IsDBNull(row["PEL_Sponsor_Name"]))?string.Empty:(System.String)row["PEL_Sponsor_Name"];
					c.PelSponsor = (Convert.IsDBNull(row["PEL_Sponsor"]))?(int)0:(System.Int32?)row["PEL_Sponsor"];
					c.PelSpaName = (Convert.IsDBNull(row["PEL_Spa_Name"]))?string.Empty:(System.String)row["PEL_Spa_Name"];
					c.PelSpa = (Convert.IsDBNull(row["PEL_Spa"]))?(int)0:(System.Int32?)row["PEL_Spa"];
					c.ArpSponsorName = (Convert.IsDBNull(row["ARP_Sponsor_Name"]))?string.Empty:(System.String)row["ARP_Sponsor_Name"];
					c.ArpSponsor = (Convert.IsDBNull(row["ARP_Sponsor"]))?(int)0:(System.Int32?)row["ARP_Sponsor"];
					c.ArpSpaName = (Convert.IsDBNull(row["ARP_Spa_Name"]))?string.Empty:(System.String)row["ARP_Spa_Name"];
					c.ArpSpa = (Convert.IsDBNull(row["ARP_Spa"]))?(int)0:(System.Int32?)row["ARP_Spa"];
					c.YenSponsorName = (Convert.IsDBNull(row["YEN_Sponsor_Name"]))?string.Empty:(System.String)row["YEN_Sponsor_Name"];
					c.YenSponsor = (Convert.IsDBNull(row["YEN_Sponsor"]))?(int)0:(System.Int32?)row["YEN_Sponsor"];
					c.YenSpaName = (Convert.IsDBNull(row["YEN_Spa_Name"]))?string.Empty:(System.String)row["YEN_Spa_Name"];
					c.YenSpa = (Convert.IsDBNull(row["YEN_Spa"]))?(int)0:(System.Int32?)row["YEN_Spa"];
					c.AcceptChanges();
					rows.Add(c);
					pagelen -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		*/	
						
		///<summary>
		/// Fill an <see cref="VList&lt;QuestionnaireReportOverviewSubQuery3&gt;"/> From a DataReader.
		///</summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pageLength">number of row.</param>
		///<returns>a <see cref="VList&lt;QuestionnaireReportOverviewSubQuery3&gt;"/></returns>
		protected VList<QuestionnaireReportOverviewSubQuery3> Fill(IDataReader reader, VList<QuestionnaireReportOverviewSubQuery3> rows, int start, int pageLength)
		{
			int recordnum = 0;
			while (reader.Read() && (pageLength != 0))
			{
				if(recordnum >= start)
				{
					QuestionnaireReportOverviewSubQuery3 entity = null;
					if (DataRepository.Provider.UseEntityFactory)
					{
						entity = EntityManager.CreateViewEntity<QuestionnaireReportOverviewSubQuery3>("QuestionnaireReportOverviewSubQuery3",  DataRepository.Provider.EntityCreationalFactoryType); 
					}
					else
					{
						entity = new QuestionnaireReportOverviewSubQuery3();
					}
					
					entity.SuppressEntityEvents = true;

					entity.CompanyId = (System.Int32)reader[((int)QuestionnaireReportOverviewSubQuery3Column.CompanyId)];
					//entity.CompanyId = (Convert.IsDBNull(reader["CompanyId"]))?(int)0:(System.Int32)reader["CompanyId"];
					entity.KwiSponsorName = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery3Column.KwiSponsorName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery3Column.KwiSponsorName)];
					//entity.KwiSponsorName = (Convert.IsDBNull(reader["KWI_Sponsor_Name"]))?string.Empty:(System.String)reader["KWI_Sponsor_Name"];
					entity.KwiSponsor = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery3Column.KwiSponsor)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewSubQuery3Column.KwiSponsor)];
					//entity.KwiSponsor = (Convert.IsDBNull(reader["KWI_Sponsor"]))?(int)0:(System.Int32?)reader["KWI_Sponsor"];
					entity.KwiSpaName = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery3Column.KwiSpaName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery3Column.KwiSpaName)];
					//entity.KwiSpaName = (Convert.IsDBNull(reader["KWI_Spa_Name"]))?string.Empty:(System.String)reader["KWI_Spa_Name"];
					entity.KwiSpa = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery3Column.KwiSpa)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewSubQuery3Column.KwiSpa)];
					//entity.KwiSpa = (Convert.IsDBNull(reader["KWI_Spa"]))?(int)0:(System.Int32?)reader["KWI_Spa"];
					entity.PinSponsorName = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery3Column.PinSponsorName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery3Column.PinSponsorName)];
					//entity.PinSponsorName = (Convert.IsDBNull(reader["PIN_Sponsor_Name"]))?string.Empty:(System.String)reader["PIN_Sponsor_Name"];
					entity.PinSponsor = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery3Column.PinSponsor)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewSubQuery3Column.PinSponsor)];
					//entity.PinSponsor = (Convert.IsDBNull(reader["PIN_Sponsor"]))?(int)0:(System.Int32?)reader["PIN_Sponsor"];
					entity.PinSpaName = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery3Column.PinSpaName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery3Column.PinSpaName)];
					//entity.PinSpaName = (Convert.IsDBNull(reader["PIN_Spa_Name"]))?string.Empty:(System.String)reader["PIN_Spa_Name"];
					entity.PinSpa = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery3Column.PinSpa)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewSubQuery3Column.PinSpa)];
					//entity.PinSpa = (Convert.IsDBNull(reader["PIN_Spa"]))?(int)0:(System.Int32?)reader["PIN_Spa"];
					entity.WgpSponsorName = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery3Column.WgpSponsorName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery3Column.WgpSponsorName)];
					//entity.WgpSponsorName = (Convert.IsDBNull(reader["WGP_Sponsor_Name"]))?string.Empty:(System.String)reader["WGP_Sponsor_Name"];
					entity.WgpSponsor = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery3Column.WgpSponsor)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewSubQuery3Column.WgpSponsor)];
					//entity.WgpSponsor = (Convert.IsDBNull(reader["WGP_Sponsor"]))?(int)0:(System.Int32?)reader["WGP_Sponsor"];
					entity.WgpSpaName = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery3Column.WgpSpaName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery3Column.WgpSpaName)];
					//entity.WgpSpaName = (Convert.IsDBNull(reader["WGP_Spa_Name"]))?string.Empty:(System.String)reader["WGP_Spa_Name"];
					entity.WgpSpa = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery3Column.WgpSpa)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewSubQuery3Column.WgpSpa)];
					//entity.WgpSpa = (Convert.IsDBNull(reader["WGP_Spa"]))?(int)0:(System.Int32?)reader["WGP_Spa"];
					entity.HunSponsorName = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery3Column.HunSponsorName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery3Column.HunSponsorName)];
					//entity.HunSponsorName = (Convert.IsDBNull(reader["HUN_Sponsor_Name"]))?string.Empty:(System.String)reader["HUN_Sponsor_Name"];
					entity.HunSponsor = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery3Column.HunSponsor)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewSubQuery3Column.HunSponsor)];
					//entity.HunSponsor = (Convert.IsDBNull(reader["HUN_Sponsor"]))?(int)0:(System.Int32?)reader["HUN_Sponsor"];
					entity.HunSpaName = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery3Column.HunSpaName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery3Column.HunSpaName)];
					//entity.HunSpaName = (Convert.IsDBNull(reader["HUN_Spa_Name"]))?string.Empty:(System.String)reader["HUN_Spa_Name"];
					entity.HunSpa = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery3Column.HunSpa)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewSubQuery3Column.HunSpa)];
					//entity.HunSpa = (Convert.IsDBNull(reader["HUN_Spa"]))?(int)0:(System.Int32?)reader["HUN_Spa"];
					entity.WdlSponsorName = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery3Column.WdlSponsorName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery3Column.WdlSponsorName)];
					//entity.WdlSponsorName = (Convert.IsDBNull(reader["WDL_Sponsor_Name"]))?string.Empty:(System.String)reader["WDL_Sponsor_Name"];
					entity.WdlSponsor = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery3Column.WdlSponsor)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewSubQuery3Column.WdlSponsor)];
					//entity.WdlSponsor = (Convert.IsDBNull(reader["WDL_Sponsor"]))?(int)0:(System.Int32?)reader["WDL_Sponsor"];
					entity.WdlSpaName = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery3Column.WdlSpaName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery3Column.WdlSpaName)];
					//entity.WdlSpaName = (Convert.IsDBNull(reader["WDL_Spa_Name"]))?string.Empty:(System.String)reader["WDL_Spa_Name"];
					entity.WdlSpa = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery3Column.WdlSpa)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewSubQuery3Column.WdlSpa)];
					//entity.WdlSpa = (Convert.IsDBNull(reader["WDL_Spa"]))?(int)0:(System.Int32?)reader["WDL_Spa"];
					entity.BunSponsorName = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery3Column.BunSponsorName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery3Column.BunSponsorName)];
					//entity.BunSponsorName = (Convert.IsDBNull(reader["BUN_Sponsor_Name"]))?string.Empty:(System.String)reader["BUN_Sponsor_Name"];
					entity.BunSponsor = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery3Column.BunSponsor)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewSubQuery3Column.BunSponsor)];
					//entity.BunSponsor = (Convert.IsDBNull(reader["BUN_Sponsor"]))?(int)0:(System.Int32?)reader["BUN_Sponsor"];
					entity.BunSpaName = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery3Column.BunSpaName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery3Column.BunSpaName)];
					//entity.BunSpaName = (Convert.IsDBNull(reader["BUN_Spa_Name"]))?string.Empty:(System.String)reader["BUN_Spa_Name"];
					entity.BunSpa = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery3Column.BunSpa)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewSubQuery3Column.BunSpa)];
					//entity.BunSpa = (Convert.IsDBNull(reader["BUN_Spa"]))?(int)0:(System.Int32?)reader["BUN_Spa"];
					entity.FmlSponsorName = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery3Column.FmlSponsorName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery3Column.FmlSponsorName)];
					//entity.FmlSponsorName = (Convert.IsDBNull(reader["FML_Sponsor_Name"]))?string.Empty:(System.String)reader["FML_Sponsor_Name"];
					entity.FmlSponsor = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery3Column.FmlSponsor)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewSubQuery3Column.FmlSponsor)];
					//entity.FmlSponsor = (Convert.IsDBNull(reader["FML_Sponsor"]))?(int)0:(System.Int32?)reader["FML_Sponsor"];
					entity.FmlSpaName = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery3Column.FmlSpaName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery3Column.FmlSpaName)];
					//entity.FmlSpaName = (Convert.IsDBNull(reader["FML_Spa_Name"]))?string.Empty:(System.String)reader["FML_Spa_Name"];
					entity.FmlSpa = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery3Column.FmlSpa)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewSubQuery3Column.FmlSpa)];
					//entity.FmlSpa = (Convert.IsDBNull(reader["FML_Spa"]))?(int)0:(System.Int32?)reader["FML_Spa"];
					entity.BgnSponsorName = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery3Column.BgnSponsorName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery3Column.BgnSponsorName)];
					//entity.BgnSponsorName = (Convert.IsDBNull(reader["BGN_Sponsor_Name"]))?string.Empty:(System.String)reader["BGN_Sponsor_Name"];
					entity.BgnSponsor = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery3Column.BgnSponsor)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewSubQuery3Column.BgnSponsor)];
					//entity.BgnSponsor = (Convert.IsDBNull(reader["BGN_Sponsor"]))?(int)0:(System.Int32?)reader["BGN_Sponsor"];
					entity.BgnSpaName = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery3Column.BgnSpaName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery3Column.BgnSpaName)];
					//entity.BgnSpaName = (Convert.IsDBNull(reader["BGN_Spa_Name"]))?string.Empty:(System.String)reader["BGN_Spa_Name"];
					entity.BgnSpa = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery3Column.BgnSpa)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewSubQuery3Column.BgnSpa)];
					//entity.BgnSpa = (Convert.IsDBNull(reader["BGN_Spa"]))?(int)0:(System.Int32?)reader["BGN_Spa"];
					entity.CeSponsorName = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery3Column.CeSponsorName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery3Column.CeSponsorName)];
					//entity.CeSponsorName = (Convert.IsDBNull(reader["CE_Sponsor_Name"]))?string.Empty:(System.String)reader["CE_Sponsor_Name"];
					entity.CeSponsor = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery3Column.CeSponsor)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewSubQuery3Column.CeSponsor)];
					//entity.CeSponsor = (Convert.IsDBNull(reader["CE_Sponsor"]))?(int)0:(System.Int32?)reader["CE_Sponsor"];
					entity.CeSpaName = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery3Column.CeSpaName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery3Column.CeSpaName)];
					//entity.CeSpaName = (Convert.IsDBNull(reader["CE_Spa_Name"]))?string.Empty:(System.String)reader["CE_Spa_Name"];
					entity.CeSpa = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery3Column.CeSpa)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewSubQuery3Column.CeSpa)];
					//entity.CeSpa = (Convert.IsDBNull(reader["CE_Spa"]))?(int)0:(System.Int32?)reader["CE_Spa"];
					entity.AngSponsorName = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery3Column.AngSponsorName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery3Column.AngSponsorName)];
					//entity.AngSponsorName = (Convert.IsDBNull(reader["ANG_Sponsor_Name"]))?string.Empty:(System.String)reader["ANG_Sponsor_Name"];
					entity.AngSponsor = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery3Column.AngSponsor)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewSubQuery3Column.AngSponsor)];
					//entity.AngSponsor = (Convert.IsDBNull(reader["ANG_Sponsor"]))?(int)0:(System.Int32?)reader["ANG_Sponsor"];
					entity.AngSpaName = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery3Column.AngSpaName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery3Column.AngSpaName)];
					//entity.AngSpaName = (Convert.IsDBNull(reader["ANG_Spa_Name"]))?string.Empty:(System.String)reader["ANG_Spa_Name"];
					entity.AngSpa = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery3Column.AngSpa)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewSubQuery3Column.AngSpa)];
					//entity.AngSpa = (Convert.IsDBNull(reader["ANG_Spa"]))?(int)0:(System.Int32?)reader["ANG_Spa"];
					entity.PtlSponsorName = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery3Column.PtlSponsorName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery3Column.PtlSponsorName)];
					//entity.PtlSponsorName = (Convert.IsDBNull(reader["PTL_Sponsor_Name"]))?string.Empty:(System.String)reader["PTL_Sponsor_Name"];
					entity.PtlSponsor = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery3Column.PtlSponsor)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewSubQuery3Column.PtlSponsor)];
					//entity.PtlSponsor = (Convert.IsDBNull(reader["PTL_Sponsor"]))?(int)0:(System.Int32?)reader["PTL_Sponsor"];
					entity.PtlSpaName = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery3Column.PtlSpaName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery3Column.PtlSpaName)];
					//entity.PtlSpaName = (Convert.IsDBNull(reader["PTL_Spa_Name"]))?string.Empty:(System.String)reader["PTL_Spa_Name"];
					entity.PtlSpa = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery3Column.PtlSpa)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewSubQuery3Column.PtlSpa)];
					//entity.PtlSpa = (Convert.IsDBNull(reader["PTL_Spa"]))?(int)0:(System.Int32?)reader["PTL_Spa"];
					entity.PthSponsorName = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery3Column.PthSponsorName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery3Column.PthSponsorName)];
					//entity.PthSponsorName = (Convert.IsDBNull(reader["PTH_Sponsor_Name"]))?string.Empty:(System.String)reader["PTH_Sponsor_Name"];
					entity.PthSponsor = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery3Column.PthSponsor)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewSubQuery3Column.PthSponsor)];
					//entity.PthSponsor = (Convert.IsDBNull(reader["PTH_Sponsor"]))?(int)0:(System.Int32?)reader["PTH_Sponsor"];
					entity.PthSpaName = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery3Column.PthSpaName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery3Column.PthSpaName)];
					//entity.PthSpaName = (Convert.IsDBNull(reader["PTH_Spa_Name"]))?string.Empty:(System.String)reader["PTH_Spa_Name"];
					entity.PthSpa = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery3Column.PthSpa)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewSubQuery3Column.PthSpa)];
					//entity.PthSpa = (Convert.IsDBNull(reader["PTH_Spa"]))?(int)0:(System.Int32?)reader["PTH_Spa"];
					entity.PelSponsorName = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery3Column.PelSponsorName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery3Column.PelSponsorName)];
					//entity.PelSponsorName = (Convert.IsDBNull(reader["PEL_Sponsor_Name"]))?string.Empty:(System.String)reader["PEL_Sponsor_Name"];
					entity.PelSponsor = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery3Column.PelSponsor)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewSubQuery3Column.PelSponsor)];
					//entity.PelSponsor = (Convert.IsDBNull(reader["PEL_Sponsor"]))?(int)0:(System.Int32?)reader["PEL_Sponsor"];
					entity.PelSpaName = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery3Column.PelSpaName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery3Column.PelSpaName)];
					//entity.PelSpaName = (Convert.IsDBNull(reader["PEL_Spa_Name"]))?string.Empty:(System.String)reader["PEL_Spa_Name"];
					entity.PelSpa = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery3Column.PelSpa)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewSubQuery3Column.PelSpa)];
					//entity.PelSpa = (Convert.IsDBNull(reader["PEL_Spa"]))?(int)0:(System.Int32?)reader["PEL_Spa"];
					entity.ArpSponsorName = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery3Column.ArpSponsorName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery3Column.ArpSponsorName)];
					//entity.ArpSponsorName = (Convert.IsDBNull(reader["ARP_Sponsor_Name"]))?string.Empty:(System.String)reader["ARP_Sponsor_Name"];
					entity.ArpSponsor = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery3Column.ArpSponsor)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewSubQuery3Column.ArpSponsor)];
					//entity.ArpSponsor = (Convert.IsDBNull(reader["ARP_Sponsor"]))?(int)0:(System.Int32?)reader["ARP_Sponsor"];
					entity.ArpSpaName = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery3Column.ArpSpaName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery3Column.ArpSpaName)];
					//entity.ArpSpaName = (Convert.IsDBNull(reader["ARP_Spa_Name"]))?string.Empty:(System.String)reader["ARP_Spa_Name"];
					entity.ArpSpa = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery3Column.ArpSpa)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewSubQuery3Column.ArpSpa)];
					//entity.ArpSpa = (Convert.IsDBNull(reader["ARP_Spa"]))?(int)0:(System.Int32?)reader["ARP_Spa"];
					entity.YenSponsorName = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery3Column.YenSponsorName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery3Column.YenSponsorName)];
					//entity.YenSponsorName = (Convert.IsDBNull(reader["YEN_Sponsor_Name"]))?string.Empty:(System.String)reader["YEN_Sponsor_Name"];
					entity.YenSponsor = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery3Column.YenSponsor)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewSubQuery3Column.YenSponsor)];
					//entity.YenSponsor = (Convert.IsDBNull(reader["YEN_Sponsor"]))?(int)0:(System.Int32?)reader["YEN_Sponsor"];
					entity.YenSpaName = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery3Column.YenSpaName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery3Column.YenSpaName)];
					//entity.YenSpaName = (Convert.IsDBNull(reader["YEN_Spa_Name"]))?string.Empty:(System.String)reader["YEN_Spa_Name"];
					entity.YenSpa = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery3Column.YenSpa)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewSubQuery3Column.YenSpa)];
					//entity.YenSpa = (Convert.IsDBNull(reader["YEN_Spa"]))?(int)0:(System.Int32?)reader["YEN_Spa"];
					entity.AcceptChanges();
					entity.SuppressEntityEvents = false;
					
					rows.Add(entity);
					pageLength -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		
		
		/// <summary>
		/// Refreshes the <see cref="QuestionnaireReportOverviewSubQuery3"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="QuestionnaireReportOverviewSubQuery3"/> object to refresh.</param>
		protected void RefreshEntity(IDataReader reader, QuestionnaireReportOverviewSubQuery3 entity)
		{
			reader.Read();
			entity.CompanyId = (System.Int32)reader[((int)QuestionnaireReportOverviewSubQuery3Column.CompanyId)];
			//entity.CompanyId = (Convert.IsDBNull(reader["CompanyId"]))?(int)0:(System.Int32)reader["CompanyId"];
			entity.KwiSponsorName = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery3Column.KwiSponsorName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery3Column.KwiSponsorName)];
			//entity.KwiSponsorName = (Convert.IsDBNull(reader["KWI_Sponsor_Name"]))?string.Empty:(System.String)reader["KWI_Sponsor_Name"];
			entity.KwiSponsor = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery3Column.KwiSponsor)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewSubQuery3Column.KwiSponsor)];
			//entity.KwiSponsor = (Convert.IsDBNull(reader["KWI_Sponsor"]))?(int)0:(System.Int32?)reader["KWI_Sponsor"];
			entity.KwiSpaName = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery3Column.KwiSpaName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery3Column.KwiSpaName)];
			//entity.KwiSpaName = (Convert.IsDBNull(reader["KWI_Spa_Name"]))?string.Empty:(System.String)reader["KWI_Spa_Name"];
			entity.KwiSpa = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery3Column.KwiSpa)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewSubQuery3Column.KwiSpa)];
			//entity.KwiSpa = (Convert.IsDBNull(reader["KWI_Spa"]))?(int)0:(System.Int32?)reader["KWI_Spa"];
			entity.PinSponsorName = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery3Column.PinSponsorName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery3Column.PinSponsorName)];
			//entity.PinSponsorName = (Convert.IsDBNull(reader["PIN_Sponsor_Name"]))?string.Empty:(System.String)reader["PIN_Sponsor_Name"];
			entity.PinSponsor = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery3Column.PinSponsor)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewSubQuery3Column.PinSponsor)];
			//entity.PinSponsor = (Convert.IsDBNull(reader["PIN_Sponsor"]))?(int)0:(System.Int32?)reader["PIN_Sponsor"];
			entity.PinSpaName = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery3Column.PinSpaName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery3Column.PinSpaName)];
			//entity.PinSpaName = (Convert.IsDBNull(reader["PIN_Spa_Name"]))?string.Empty:(System.String)reader["PIN_Spa_Name"];
			entity.PinSpa = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery3Column.PinSpa)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewSubQuery3Column.PinSpa)];
			//entity.PinSpa = (Convert.IsDBNull(reader["PIN_Spa"]))?(int)0:(System.Int32?)reader["PIN_Spa"];
			entity.WgpSponsorName = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery3Column.WgpSponsorName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery3Column.WgpSponsorName)];
			//entity.WgpSponsorName = (Convert.IsDBNull(reader["WGP_Sponsor_Name"]))?string.Empty:(System.String)reader["WGP_Sponsor_Name"];
			entity.WgpSponsor = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery3Column.WgpSponsor)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewSubQuery3Column.WgpSponsor)];
			//entity.WgpSponsor = (Convert.IsDBNull(reader["WGP_Sponsor"]))?(int)0:(System.Int32?)reader["WGP_Sponsor"];
			entity.WgpSpaName = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery3Column.WgpSpaName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery3Column.WgpSpaName)];
			//entity.WgpSpaName = (Convert.IsDBNull(reader["WGP_Spa_Name"]))?string.Empty:(System.String)reader["WGP_Spa_Name"];
			entity.WgpSpa = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery3Column.WgpSpa)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewSubQuery3Column.WgpSpa)];
			//entity.WgpSpa = (Convert.IsDBNull(reader["WGP_Spa"]))?(int)0:(System.Int32?)reader["WGP_Spa"];
			entity.HunSponsorName = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery3Column.HunSponsorName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery3Column.HunSponsorName)];
			//entity.HunSponsorName = (Convert.IsDBNull(reader["HUN_Sponsor_Name"]))?string.Empty:(System.String)reader["HUN_Sponsor_Name"];
			entity.HunSponsor = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery3Column.HunSponsor)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewSubQuery3Column.HunSponsor)];
			//entity.HunSponsor = (Convert.IsDBNull(reader["HUN_Sponsor"]))?(int)0:(System.Int32?)reader["HUN_Sponsor"];
			entity.HunSpaName = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery3Column.HunSpaName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery3Column.HunSpaName)];
			//entity.HunSpaName = (Convert.IsDBNull(reader["HUN_Spa_Name"]))?string.Empty:(System.String)reader["HUN_Spa_Name"];
			entity.HunSpa = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery3Column.HunSpa)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewSubQuery3Column.HunSpa)];
			//entity.HunSpa = (Convert.IsDBNull(reader["HUN_Spa"]))?(int)0:(System.Int32?)reader["HUN_Spa"];
			entity.WdlSponsorName = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery3Column.WdlSponsorName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery3Column.WdlSponsorName)];
			//entity.WdlSponsorName = (Convert.IsDBNull(reader["WDL_Sponsor_Name"]))?string.Empty:(System.String)reader["WDL_Sponsor_Name"];
			entity.WdlSponsor = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery3Column.WdlSponsor)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewSubQuery3Column.WdlSponsor)];
			//entity.WdlSponsor = (Convert.IsDBNull(reader["WDL_Sponsor"]))?(int)0:(System.Int32?)reader["WDL_Sponsor"];
			entity.WdlSpaName = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery3Column.WdlSpaName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery3Column.WdlSpaName)];
			//entity.WdlSpaName = (Convert.IsDBNull(reader["WDL_Spa_Name"]))?string.Empty:(System.String)reader["WDL_Spa_Name"];
			entity.WdlSpa = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery3Column.WdlSpa)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewSubQuery3Column.WdlSpa)];
			//entity.WdlSpa = (Convert.IsDBNull(reader["WDL_Spa"]))?(int)0:(System.Int32?)reader["WDL_Spa"];
			entity.BunSponsorName = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery3Column.BunSponsorName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery3Column.BunSponsorName)];
			//entity.BunSponsorName = (Convert.IsDBNull(reader["BUN_Sponsor_Name"]))?string.Empty:(System.String)reader["BUN_Sponsor_Name"];
			entity.BunSponsor = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery3Column.BunSponsor)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewSubQuery3Column.BunSponsor)];
			//entity.BunSponsor = (Convert.IsDBNull(reader["BUN_Sponsor"]))?(int)0:(System.Int32?)reader["BUN_Sponsor"];
			entity.BunSpaName = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery3Column.BunSpaName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery3Column.BunSpaName)];
			//entity.BunSpaName = (Convert.IsDBNull(reader["BUN_Spa_Name"]))?string.Empty:(System.String)reader["BUN_Spa_Name"];
			entity.BunSpa = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery3Column.BunSpa)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewSubQuery3Column.BunSpa)];
			//entity.BunSpa = (Convert.IsDBNull(reader["BUN_Spa"]))?(int)0:(System.Int32?)reader["BUN_Spa"];
			entity.FmlSponsorName = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery3Column.FmlSponsorName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery3Column.FmlSponsorName)];
			//entity.FmlSponsorName = (Convert.IsDBNull(reader["FML_Sponsor_Name"]))?string.Empty:(System.String)reader["FML_Sponsor_Name"];
			entity.FmlSponsor = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery3Column.FmlSponsor)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewSubQuery3Column.FmlSponsor)];
			//entity.FmlSponsor = (Convert.IsDBNull(reader["FML_Sponsor"]))?(int)0:(System.Int32?)reader["FML_Sponsor"];
			entity.FmlSpaName = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery3Column.FmlSpaName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery3Column.FmlSpaName)];
			//entity.FmlSpaName = (Convert.IsDBNull(reader["FML_Spa_Name"]))?string.Empty:(System.String)reader["FML_Spa_Name"];
			entity.FmlSpa = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery3Column.FmlSpa)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewSubQuery3Column.FmlSpa)];
			//entity.FmlSpa = (Convert.IsDBNull(reader["FML_Spa"]))?(int)0:(System.Int32?)reader["FML_Spa"];
			entity.BgnSponsorName = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery3Column.BgnSponsorName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery3Column.BgnSponsorName)];
			//entity.BgnSponsorName = (Convert.IsDBNull(reader["BGN_Sponsor_Name"]))?string.Empty:(System.String)reader["BGN_Sponsor_Name"];
			entity.BgnSponsor = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery3Column.BgnSponsor)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewSubQuery3Column.BgnSponsor)];
			//entity.BgnSponsor = (Convert.IsDBNull(reader["BGN_Sponsor"]))?(int)0:(System.Int32?)reader["BGN_Sponsor"];
			entity.BgnSpaName = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery3Column.BgnSpaName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery3Column.BgnSpaName)];
			//entity.BgnSpaName = (Convert.IsDBNull(reader["BGN_Spa_Name"]))?string.Empty:(System.String)reader["BGN_Spa_Name"];
			entity.BgnSpa = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery3Column.BgnSpa)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewSubQuery3Column.BgnSpa)];
			//entity.BgnSpa = (Convert.IsDBNull(reader["BGN_Spa"]))?(int)0:(System.Int32?)reader["BGN_Spa"];
			entity.CeSponsorName = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery3Column.CeSponsorName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery3Column.CeSponsorName)];
			//entity.CeSponsorName = (Convert.IsDBNull(reader["CE_Sponsor_Name"]))?string.Empty:(System.String)reader["CE_Sponsor_Name"];
			entity.CeSponsor = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery3Column.CeSponsor)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewSubQuery3Column.CeSponsor)];
			//entity.CeSponsor = (Convert.IsDBNull(reader["CE_Sponsor"]))?(int)0:(System.Int32?)reader["CE_Sponsor"];
			entity.CeSpaName = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery3Column.CeSpaName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery3Column.CeSpaName)];
			//entity.CeSpaName = (Convert.IsDBNull(reader["CE_Spa_Name"]))?string.Empty:(System.String)reader["CE_Spa_Name"];
			entity.CeSpa = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery3Column.CeSpa)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewSubQuery3Column.CeSpa)];
			//entity.CeSpa = (Convert.IsDBNull(reader["CE_Spa"]))?(int)0:(System.Int32?)reader["CE_Spa"];
			entity.AngSponsorName = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery3Column.AngSponsorName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery3Column.AngSponsorName)];
			//entity.AngSponsorName = (Convert.IsDBNull(reader["ANG_Sponsor_Name"]))?string.Empty:(System.String)reader["ANG_Sponsor_Name"];
			entity.AngSponsor = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery3Column.AngSponsor)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewSubQuery3Column.AngSponsor)];
			//entity.AngSponsor = (Convert.IsDBNull(reader["ANG_Sponsor"]))?(int)0:(System.Int32?)reader["ANG_Sponsor"];
			entity.AngSpaName = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery3Column.AngSpaName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery3Column.AngSpaName)];
			//entity.AngSpaName = (Convert.IsDBNull(reader["ANG_Spa_Name"]))?string.Empty:(System.String)reader["ANG_Spa_Name"];
			entity.AngSpa = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery3Column.AngSpa)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewSubQuery3Column.AngSpa)];
			//entity.AngSpa = (Convert.IsDBNull(reader["ANG_Spa"]))?(int)0:(System.Int32?)reader["ANG_Spa"];
			entity.PtlSponsorName = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery3Column.PtlSponsorName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery3Column.PtlSponsorName)];
			//entity.PtlSponsorName = (Convert.IsDBNull(reader["PTL_Sponsor_Name"]))?string.Empty:(System.String)reader["PTL_Sponsor_Name"];
			entity.PtlSponsor = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery3Column.PtlSponsor)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewSubQuery3Column.PtlSponsor)];
			//entity.PtlSponsor = (Convert.IsDBNull(reader["PTL_Sponsor"]))?(int)0:(System.Int32?)reader["PTL_Sponsor"];
			entity.PtlSpaName = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery3Column.PtlSpaName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery3Column.PtlSpaName)];
			//entity.PtlSpaName = (Convert.IsDBNull(reader["PTL_Spa_Name"]))?string.Empty:(System.String)reader["PTL_Spa_Name"];
			entity.PtlSpa = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery3Column.PtlSpa)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewSubQuery3Column.PtlSpa)];
			//entity.PtlSpa = (Convert.IsDBNull(reader["PTL_Spa"]))?(int)0:(System.Int32?)reader["PTL_Spa"];
			entity.PthSponsorName = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery3Column.PthSponsorName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery3Column.PthSponsorName)];
			//entity.PthSponsorName = (Convert.IsDBNull(reader["PTH_Sponsor_Name"]))?string.Empty:(System.String)reader["PTH_Sponsor_Name"];
			entity.PthSponsor = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery3Column.PthSponsor)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewSubQuery3Column.PthSponsor)];
			//entity.PthSponsor = (Convert.IsDBNull(reader["PTH_Sponsor"]))?(int)0:(System.Int32?)reader["PTH_Sponsor"];
			entity.PthSpaName = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery3Column.PthSpaName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery3Column.PthSpaName)];
			//entity.PthSpaName = (Convert.IsDBNull(reader["PTH_Spa_Name"]))?string.Empty:(System.String)reader["PTH_Spa_Name"];
			entity.PthSpa = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery3Column.PthSpa)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewSubQuery3Column.PthSpa)];
			//entity.PthSpa = (Convert.IsDBNull(reader["PTH_Spa"]))?(int)0:(System.Int32?)reader["PTH_Spa"];
			entity.PelSponsorName = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery3Column.PelSponsorName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery3Column.PelSponsorName)];
			//entity.PelSponsorName = (Convert.IsDBNull(reader["PEL_Sponsor_Name"]))?string.Empty:(System.String)reader["PEL_Sponsor_Name"];
			entity.PelSponsor = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery3Column.PelSponsor)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewSubQuery3Column.PelSponsor)];
			//entity.PelSponsor = (Convert.IsDBNull(reader["PEL_Sponsor"]))?(int)0:(System.Int32?)reader["PEL_Sponsor"];
			entity.PelSpaName = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery3Column.PelSpaName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery3Column.PelSpaName)];
			//entity.PelSpaName = (Convert.IsDBNull(reader["PEL_Spa_Name"]))?string.Empty:(System.String)reader["PEL_Spa_Name"];
			entity.PelSpa = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery3Column.PelSpa)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewSubQuery3Column.PelSpa)];
			//entity.PelSpa = (Convert.IsDBNull(reader["PEL_Spa"]))?(int)0:(System.Int32?)reader["PEL_Spa"];
			entity.ArpSponsorName = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery3Column.ArpSponsorName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery3Column.ArpSponsorName)];
			//entity.ArpSponsorName = (Convert.IsDBNull(reader["ARP_Sponsor_Name"]))?string.Empty:(System.String)reader["ARP_Sponsor_Name"];
			entity.ArpSponsor = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery3Column.ArpSponsor)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewSubQuery3Column.ArpSponsor)];
			//entity.ArpSponsor = (Convert.IsDBNull(reader["ARP_Sponsor"]))?(int)0:(System.Int32?)reader["ARP_Sponsor"];
			entity.ArpSpaName = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery3Column.ArpSpaName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery3Column.ArpSpaName)];
			//entity.ArpSpaName = (Convert.IsDBNull(reader["ARP_Spa_Name"]))?string.Empty:(System.String)reader["ARP_Spa_Name"];
			entity.ArpSpa = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery3Column.ArpSpa)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewSubQuery3Column.ArpSpa)];
			//entity.ArpSpa = (Convert.IsDBNull(reader["ARP_Spa"]))?(int)0:(System.Int32?)reader["ARP_Spa"];
			entity.YenSponsorName = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery3Column.YenSponsorName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery3Column.YenSponsorName)];
			//entity.YenSponsorName = (Convert.IsDBNull(reader["YEN_Sponsor_Name"]))?string.Empty:(System.String)reader["YEN_Sponsor_Name"];
			entity.YenSponsor = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery3Column.YenSponsor)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewSubQuery3Column.YenSponsor)];
			//entity.YenSponsor = (Convert.IsDBNull(reader["YEN_Sponsor"]))?(int)0:(System.Int32?)reader["YEN_Sponsor"];
			entity.YenSpaName = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery3Column.YenSpaName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery3Column.YenSpaName)];
			//entity.YenSpaName = (Convert.IsDBNull(reader["YEN_Spa_Name"]))?string.Empty:(System.String)reader["YEN_Spa_Name"];
			entity.YenSpa = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery3Column.YenSpa)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewSubQuery3Column.YenSpa)];
			//entity.YenSpa = (Convert.IsDBNull(reader["YEN_Spa"]))?(int)0:(System.Int32?)reader["YEN_Spa"];
			reader.Close();
	
			entity.AcceptChanges();
		}
		
		/*
		/// <summary>
		/// Refreshes the <see cref="QuestionnaireReportOverviewSubQuery3"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="QuestionnaireReportOverviewSubQuery3"/> object.</param>
		protected static void RefreshEntity(DataSet dataSet, QuestionnaireReportOverviewSubQuery3 entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.CompanyId = (Convert.IsDBNull(dataRow["CompanyId"]))?(int)0:(System.Int32)dataRow["CompanyId"];
			entity.KwiSponsorName = (Convert.IsDBNull(dataRow["KWI_Sponsor_Name"]))?string.Empty:(System.String)dataRow["KWI_Sponsor_Name"];
			entity.KwiSponsor = (Convert.IsDBNull(dataRow["KWI_Sponsor"]))?(int)0:(System.Int32?)dataRow["KWI_Sponsor"];
			entity.KwiSpaName = (Convert.IsDBNull(dataRow["KWI_Spa_Name"]))?string.Empty:(System.String)dataRow["KWI_Spa_Name"];
			entity.KwiSpa = (Convert.IsDBNull(dataRow["KWI_Spa"]))?(int)0:(System.Int32?)dataRow["KWI_Spa"];
			entity.PinSponsorName = (Convert.IsDBNull(dataRow["PIN_Sponsor_Name"]))?string.Empty:(System.String)dataRow["PIN_Sponsor_Name"];
			entity.PinSponsor = (Convert.IsDBNull(dataRow["PIN_Sponsor"]))?(int)0:(System.Int32?)dataRow["PIN_Sponsor"];
			entity.PinSpaName = (Convert.IsDBNull(dataRow["PIN_Spa_Name"]))?string.Empty:(System.String)dataRow["PIN_Spa_Name"];
			entity.PinSpa = (Convert.IsDBNull(dataRow["PIN_Spa"]))?(int)0:(System.Int32?)dataRow["PIN_Spa"];
			entity.WgpSponsorName = (Convert.IsDBNull(dataRow["WGP_Sponsor_Name"]))?string.Empty:(System.String)dataRow["WGP_Sponsor_Name"];
			entity.WgpSponsor = (Convert.IsDBNull(dataRow["WGP_Sponsor"]))?(int)0:(System.Int32?)dataRow["WGP_Sponsor"];
			entity.WgpSpaName = (Convert.IsDBNull(dataRow["WGP_Spa_Name"]))?string.Empty:(System.String)dataRow["WGP_Spa_Name"];
			entity.WgpSpa = (Convert.IsDBNull(dataRow["WGP_Spa"]))?(int)0:(System.Int32?)dataRow["WGP_Spa"];
			entity.HunSponsorName = (Convert.IsDBNull(dataRow["HUN_Sponsor_Name"]))?string.Empty:(System.String)dataRow["HUN_Sponsor_Name"];
			entity.HunSponsor = (Convert.IsDBNull(dataRow["HUN_Sponsor"]))?(int)0:(System.Int32?)dataRow["HUN_Sponsor"];
			entity.HunSpaName = (Convert.IsDBNull(dataRow["HUN_Spa_Name"]))?string.Empty:(System.String)dataRow["HUN_Spa_Name"];
			entity.HunSpa = (Convert.IsDBNull(dataRow["HUN_Spa"]))?(int)0:(System.Int32?)dataRow["HUN_Spa"];
			entity.WdlSponsorName = (Convert.IsDBNull(dataRow["WDL_Sponsor_Name"]))?string.Empty:(System.String)dataRow["WDL_Sponsor_Name"];
			entity.WdlSponsor = (Convert.IsDBNull(dataRow["WDL_Sponsor"]))?(int)0:(System.Int32?)dataRow["WDL_Sponsor"];
			entity.WdlSpaName = (Convert.IsDBNull(dataRow["WDL_Spa_Name"]))?string.Empty:(System.String)dataRow["WDL_Spa_Name"];
			entity.WdlSpa = (Convert.IsDBNull(dataRow["WDL_Spa"]))?(int)0:(System.Int32?)dataRow["WDL_Spa"];
			entity.BunSponsorName = (Convert.IsDBNull(dataRow["BUN_Sponsor_Name"]))?string.Empty:(System.String)dataRow["BUN_Sponsor_Name"];
			entity.BunSponsor = (Convert.IsDBNull(dataRow["BUN_Sponsor"]))?(int)0:(System.Int32?)dataRow["BUN_Sponsor"];
			entity.BunSpaName = (Convert.IsDBNull(dataRow["BUN_Spa_Name"]))?string.Empty:(System.String)dataRow["BUN_Spa_Name"];
			entity.BunSpa = (Convert.IsDBNull(dataRow["BUN_Spa"]))?(int)0:(System.Int32?)dataRow["BUN_Spa"];
			entity.FmlSponsorName = (Convert.IsDBNull(dataRow["FML_Sponsor_Name"]))?string.Empty:(System.String)dataRow["FML_Sponsor_Name"];
			entity.FmlSponsor = (Convert.IsDBNull(dataRow["FML_Sponsor"]))?(int)0:(System.Int32?)dataRow["FML_Sponsor"];
			entity.FmlSpaName = (Convert.IsDBNull(dataRow["FML_Spa_Name"]))?string.Empty:(System.String)dataRow["FML_Spa_Name"];
			entity.FmlSpa = (Convert.IsDBNull(dataRow["FML_Spa"]))?(int)0:(System.Int32?)dataRow["FML_Spa"];
			entity.BgnSponsorName = (Convert.IsDBNull(dataRow["BGN_Sponsor_Name"]))?string.Empty:(System.String)dataRow["BGN_Sponsor_Name"];
			entity.BgnSponsor = (Convert.IsDBNull(dataRow["BGN_Sponsor"]))?(int)0:(System.Int32?)dataRow["BGN_Sponsor"];
			entity.BgnSpaName = (Convert.IsDBNull(dataRow["BGN_Spa_Name"]))?string.Empty:(System.String)dataRow["BGN_Spa_Name"];
			entity.BgnSpa = (Convert.IsDBNull(dataRow["BGN_Spa"]))?(int)0:(System.Int32?)dataRow["BGN_Spa"];
			entity.CeSponsorName = (Convert.IsDBNull(dataRow["CE_Sponsor_Name"]))?string.Empty:(System.String)dataRow["CE_Sponsor_Name"];
			entity.CeSponsor = (Convert.IsDBNull(dataRow["CE_Sponsor"]))?(int)0:(System.Int32?)dataRow["CE_Sponsor"];
			entity.CeSpaName = (Convert.IsDBNull(dataRow["CE_Spa_Name"]))?string.Empty:(System.String)dataRow["CE_Spa_Name"];
			entity.CeSpa = (Convert.IsDBNull(dataRow["CE_Spa"]))?(int)0:(System.Int32?)dataRow["CE_Spa"];
			entity.AngSponsorName = (Convert.IsDBNull(dataRow["ANG_Sponsor_Name"]))?string.Empty:(System.String)dataRow["ANG_Sponsor_Name"];
			entity.AngSponsor = (Convert.IsDBNull(dataRow["ANG_Sponsor"]))?(int)0:(System.Int32?)dataRow["ANG_Sponsor"];
			entity.AngSpaName = (Convert.IsDBNull(dataRow["ANG_Spa_Name"]))?string.Empty:(System.String)dataRow["ANG_Spa_Name"];
			entity.AngSpa = (Convert.IsDBNull(dataRow["ANG_Spa"]))?(int)0:(System.Int32?)dataRow["ANG_Spa"];
			entity.PtlSponsorName = (Convert.IsDBNull(dataRow["PTL_Sponsor_Name"]))?string.Empty:(System.String)dataRow["PTL_Sponsor_Name"];
			entity.PtlSponsor = (Convert.IsDBNull(dataRow["PTL_Sponsor"]))?(int)0:(System.Int32?)dataRow["PTL_Sponsor"];
			entity.PtlSpaName = (Convert.IsDBNull(dataRow["PTL_Spa_Name"]))?string.Empty:(System.String)dataRow["PTL_Spa_Name"];
			entity.PtlSpa = (Convert.IsDBNull(dataRow["PTL_Spa"]))?(int)0:(System.Int32?)dataRow["PTL_Spa"];
			entity.PthSponsorName = (Convert.IsDBNull(dataRow["PTH_Sponsor_Name"]))?string.Empty:(System.String)dataRow["PTH_Sponsor_Name"];
			entity.PthSponsor = (Convert.IsDBNull(dataRow["PTH_Sponsor"]))?(int)0:(System.Int32?)dataRow["PTH_Sponsor"];
			entity.PthSpaName = (Convert.IsDBNull(dataRow["PTH_Spa_Name"]))?string.Empty:(System.String)dataRow["PTH_Spa_Name"];
			entity.PthSpa = (Convert.IsDBNull(dataRow["PTH_Spa"]))?(int)0:(System.Int32?)dataRow["PTH_Spa"];
			entity.PelSponsorName = (Convert.IsDBNull(dataRow["PEL_Sponsor_Name"]))?string.Empty:(System.String)dataRow["PEL_Sponsor_Name"];
			entity.PelSponsor = (Convert.IsDBNull(dataRow["PEL_Sponsor"]))?(int)0:(System.Int32?)dataRow["PEL_Sponsor"];
			entity.PelSpaName = (Convert.IsDBNull(dataRow["PEL_Spa_Name"]))?string.Empty:(System.String)dataRow["PEL_Spa_Name"];
			entity.PelSpa = (Convert.IsDBNull(dataRow["PEL_Spa"]))?(int)0:(System.Int32?)dataRow["PEL_Spa"];
			entity.ArpSponsorName = (Convert.IsDBNull(dataRow["ARP_Sponsor_Name"]))?string.Empty:(System.String)dataRow["ARP_Sponsor_Name"];
			entity.ArpSponsor = (Convert.IsDBNull(dataRow["ARP_Sponsor"]))?(int)0:(System.Int32?)dataRow["ARP_Sponsor"];
			entity.ArpSpaName = (Convert.IsDBNull(dataRow["ARP_Spa_Name"]))?string.Empty:(System.String)dataRow["ARP_Spa_Name"];
			entity.ArpSpa = (Convert.IsDBNull(dataRow["ARP_Spa"]))?(int)0:(System.Int32?)dataRow["ARP_Spa"];
			entity.YenSponsorName = (Convert.IsDBNull(dataRow["YEN_Sponsor_Name"]))?string.Empty:(System.String)dataRow["YEN_Sponsor_Name"];
			entity.YenSponsor = (Convert.IsDBNull(dataRow["YEN_Sponsor"]))?(int)0:(System.Int32?)dataRow["YEN_Sponsor"];
			entity.YenSpaName = (Convert.IsDBNull(dataRow["YEN_Spa_Name"]))?string.Empty:(System.String)dataRow["YEN_Spa_Name"];
			entity.YenSpa = (Convert.IsDBNull(dataRow["YEN_Spa"]))?(int)0:(System.Int32?)dataRow["YEN_Spa"];
			entity.AcceptChanges();
		}
		*/
			
		#endregion Helper Functions
	}//end class

	#region QuestionnaireReportOverviewSubQuery3FilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireReportOverviewSubQuery3"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireReportOverviewSubQuery3FilterBuilder : SqlFilterBuilder<QuestionnaireReportOverviewSubQuery3Column>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportOverviewSubQuery3FilterBuilder class.
		/// </summary>
		public QuestionnaireReportOverviewSubQuery3FilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportOverviewSubQuery3FilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireReportOverviewSubQuery3FilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportOverviewSubQuery3FilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireReportOverviewSubQuery3FilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireReportOverviewSubQuery3FilterBuilder

	#region QuestionnaireReportOverviewSubQuery3ParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireReportOverviewSubQuery3"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireReportOverviewSubQuery3ParameterBuilder : ParameterizedSqlFilterBuilder<QuestionnaireReportOverviewSubQuery3Column>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportOverviewSubQuery3ParameterBuilder class.
		/// </summary>
		public QuestionnaireReportOverviewSubQuery3ParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportOverviewSubQuery3ParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireReportOverviewSubQuery3ParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportOverviewSubQuery3ParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireReportOverviewSubQuery3ParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireReportOverviewSubQuery3ParameterBuilder
	
	#region QuestionnaireReportOverviewSubQuery3SortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireReportOverviewSubQuery3"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class QuestionnaireReportOverviewSubQuery3SortBuilder : SqlSortBuilder<QuestionnaireReportOverviewSubQuery3Column>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportOverviewSubQuery3SqlSortBuilder class.
		/// </summary>
		public QuestionnaireReportOverviewSubQuery3SortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion QuestionnaireReportOverviewSubQuery3SortBuilder

} // end namespace
