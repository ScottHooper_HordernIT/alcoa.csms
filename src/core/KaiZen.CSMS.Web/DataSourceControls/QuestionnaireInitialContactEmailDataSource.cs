﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.QuestionnaireInitialContactEmailProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(QuestionnaireInitialContactEmailDataSourceDesigner))]
	public class QuestionnaireInitialContactEmailDataSource : ProviderDataSource<QuestionnaireInitialContactEmail, QuestionnaireInitialContactEmailKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialContactEmailDataSource class.
		/// </summary>
		public QuestionnaireInitialContactEmailDataSource() : base(new QuestionnaireInitialContactEmailService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the QuestionnaireInitialContactEmailDataSourceView used by the QuestionnaireInitialContactEmailDataSource.
		/// </summary>
		protected QuestionnaireInitialContactEmailDataSourceView QuestionnaireInitialContactEmailView
		{
			get { return ( View as QuestionnaireInitialContactEmailDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the QuestionnaireInitialContactEmailDataSource control invokes to retrieve data.
		/// </summary>
		public QuestionnaireInitialContactEmailSelectMethod SelectMethod
		{
			get
			{
				QuestionnaireInitialContactEmailSelectMethod selectMethod = QuestionnaireInitialContactEmailSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (QuestionnaireInitialContactEmailSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the QuestionnaireInitialContactEmailDataSourceView class that is to be
		/// used by the QuestionnaireInitialContactEmailDataSource.
		/// </summary>
		/// <returns>An instance of the QuestionnaireInitialContactEmailDataSourceView class.</returns>
		protected override BaseDataSourceView<QuestionnaireInitialContactEmail, QuestionnaireInitialContactEmailKey> GetNewDataSourceView()
		{
			return new QuestionnaireInitialContactEmailDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the QuestionnaireInitialContactEmailDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class QuestionnaireInitialContactEmailDataSourceView : ProviderDataSourceView<QuestionnaireInitialContactEmail, QuestionnaireInitialContactEmailKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialContactEmailDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the QuestionnaireInitialContactEmailDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public QuestionnaireInitialContactEmailDataSourceView(QuestionnaireInitialContactEmailDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal QuestionnaireInitialContactEmailDataSource QuestionnaireInitialContactEmailOwner
		{
			get { return Owner as QuestionnaireInitialContactEmailDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal QuestionnaireInitialContactEmailSelectMethod SelectMethod
		{
			get { return QuestionnaireInitialContactEmailOwner.SelectMethod; }
			set { QuestionnaireInitialContactEmailOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal QuestionnaireInitialContactEmailService QuestionnaireInitialContactEmailProvider
		{
			get { return Provider as QuestionnaireInitialContactEmailService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<QuestionnaireInitialContactEmail> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<QuestionnaireInitialContactEmail> results = null;
			QuestionnaireInitialContactEmail item;
			count = 0;
			
			System.Int32 _questionnaireInitialContactEmailId;
			System.Int32 _emailTypeId;
			System.Int32 _contactId;
			System.String _contactEmail;
			System.Int32 _questionnaireId;
			System.Int32 _sentByUserId;

			switch ( SelectMethod )
			{
				case QuestionnaireInitialContactEmailSelectMethod.Get:
					QuestionnaireInitialContactEmailKey entityKey  = new QuestionnaireInitialContactEmailKey();
					entityKey.Load(values);
					item = QuestionnaireInitialContactEmailProvider.Get(entityKey);
					results = new TList<QuestionnaireInitialContactEmail>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case QuestionnaireInitialContactEmailSelectMethod.GetAll:
                    results = QuestionnaireInitialContactEmailProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case QuestionnaireInitialContactEmailSelectMethod.GetPaged:
					results = QuestionnaireInitialContactEmailProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case QuestionnaireInitialContactEmailSelectMethod.Find:
					if ( FilterParameters != null )
						results = QuestionnaireInitialContactEmailProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = QuestionnaireInitialContactEmailProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case QuestionnaireInitialContactEmailSelectMethod.GetByQuestionnaireInitialContactEmailId:
					_questionnaireInitialContactEmailId = ( values["QuestionnaireInitialContactEmailId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["QuestionnaireInitialContactEmailId"], typeof(System.Int32)) : (int)0;
					item = QuestionnaireInitialContactEmailProvider.GetByQuestionnaireInitialContactEmailId(_questionnaireInitialContactEmailId);
					results = new TList<QuestionnaireInitialContactEmail>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				case QuestionnaireInitialContactEmailSelectMethod.GetByEmailTypeIdContactId:
					_emailTypeId = ( values["EmailTypeId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["EmailTypeId"], typeof(System.Int32)) : (int)0;
					_contactId = ( values["ContactId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["ContactId"], typeof(System.Int32)) : (int)0;
					results = QuestionnaireInitialContactEmailProvider.GetByEmailTypeIdContactId(_emailTypeId, _contactId, this.StartIndex, this.PageSize, out count);
					break;
				case QuestionnaireInitialContactEmailSelectMethod.GetByEmailTypeIdContactIdContactEmail:
					_emailTypeId = ( values["EmailTypeId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["EmailTypeId"], typeof(System.Int32)) : (int)0;
					_contactId = ( values["ContactId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["ContactId"], typeof(System.Int32)) : (int)0;
					_contactEmail = ( values["ContactEmail"] != null ) ? (System.String) EntityUtil.ChangeType(values["ContactEmail"], typeof(System.String)) : string.Empty;
					results = QuestionnaireInitialContactEmailProvider.GetByEmailTypeIdContactIdContactEmail(_emailTypeId, _contactId, _contactEmail, this.StartIndex, this.PageSize, out count);
					break;
				// FK
				case QuestionnaireInitialContactEmailSelectMethod.GetByQuestionnaireId:
					_questionnaireId = ( values["QuestionnaireId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["QuestionnaireId"], typeof(System.Int32)) : (int)0;
					results = QuestionnaireInitialContactEmailProvider.GetByQuestionnaireId(_questionnaireId, this.StartIndex, this.PageSize, out count);
					break;
				case QuestionnaireInitialContactEmailSelectMethod.GetByEmailTypeId:
					_emailTypeId = ( values["EmailTypeId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["EmailTypeId"], typeof(System.Int32)) : (int)0;
					results = QuestionnaireInitialContactEmailProvider.GetByEmailTypeId(_emailTypeId, this.StartIndex, this.PageSize, out count);
					break;
				case QuestionnaireInitialContactEmailSelectMethod.GetByContactId:
					_contactId = ( values["ContactId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["ContactId"], typeof(System.Int32)) : (int)0;
					results = QuestionnaireInitialContactEmailProvider.GetByContactId(_contactId, this.StartIndex, this.PageSize, out count);
					break;
				case QuestionnaireInitialContactEmailSelectMethod.GetBySentByUserId:
					_sentByUserId = ( values["SentByUserId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["SentByUserId"], typeof(System.Int32)) : (int)0;
					results = QuestionnaireInitialContactEmailProvider.GetBySentByUserId(_sentByUserId, this.StartIndex, this.PageSize, out count);
					break;
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == QuestionnaireInitialContactEmailSelectMethod.Get || SelectMethod == QuestionnaireInitialContactEmailSelectMethod.GetByQuestionnaireInitialContactEmailId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				QuestionnaireInitialContactEmail entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					QuestionnaireInitialContactEmailProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<QuestionnaireInitialContactEmail> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			QuestionnaireInitialContactEmailProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region QuestionnaireInitialContactEmailDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the QuestionnaireInitialContactEmailDataSource class.
	/// </summary>
	public class QuestionnaireInitialContactEmailDataSourceDesigner : ProviderDataSourceDesigner<QuestionnaireInitialContactEmail, QuestionnaireInitialContactEmailKey>
	{
		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialContactEmailDataSourceDesigner class.
		/// </summary>
		public QuestionnaireInitialContactEmailDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public QuestionnaireInitialContactEmailSelectMethod SelectMethod
		{
			get { return ((QuestionnaireInitialContactEmailDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new QuestionnaireInitialContactEmailDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region QuestionnaireInitialContactEmailDataSourceActionList

	/// <summary>
	/// Supports the QuestionnaireInitialContactEmailDataSourceDesigner class.
	/// </summary>
	internal class QuestionnaireInitialContactEmailDataSourceActionList : DesignerActionList
	{
		private QuestionnaireInitialContactEmailDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialContactEmailDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public QuestionnaireInitialContactEmailDataSourceActionList(QuestionnaireInitialContactEmailDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public QuestionnaireInitialContactEmailSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion QuestionnaireInitialContactEmailDataSourceActionList
	
	#endregion QuestionnaireInitialContactEmailDataSourceDesigner
	
	#region QuestionnaireInitialContactEmailSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the QuestionnaireInitialContactEmailDataSource.SelectMethod property.
	/// </summary>
	public enum QuestionnaireInitialContactEmailSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByQuestionnaireInitialContactEmailId method.
		/// </summary>
		GetByQuestionnaireInitialContactEmailId,
		/// <summary>
		/// Represents the GetByEmailTypeIdContactId method.
		/// </summary>
		GetByEmailTypeIdContactId,
		/// <summary>
		/// Represents the GetByEmailTypeIdContactIdContactEmail method.
		/// </summary>
		GetByEmailTypeIdContactIdContactEmail,
		/// <summary>
		/// Represents the GetByQuestionnaireId method.
		/// </summary>
		GetByQuestionnaireId,
		/// <summary>
		/// Represents the GetByEmailTypeId method.
		/// </summary>
		GetByEmailTypeId,
		/// <summary>
		/// Represents the GetByContactId method.
		/// </summary>
		GetByContactId,
		/// <summary>
		/// Represents the GetBySentByUserId method.
		/// </summary>
		GetBySentByUserId
	}
	
	#endregion QuestionnaireInitialContactEmailSelectMethod

	#region QuestionnaireInitialContactEmailFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireInitialContactEmail"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireInitialContactEmailFilter : SqlFilter<QuestionnaireInitialContactEmailColumn>
	{
	}
	
	#endregion QuestionnaireInitialContactEmailFilter

	#region QuestionnaireInitialContactEmailExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireInitialContactEmail"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireInitialContactEmailExpressionBuilder : SqlExpressionBuilder<QuestionnaireInitialContactEmailColumn>
	{
	}
	
	#endregion QuestionnaireInitialContactEmailExpressionBuilder	

	#region QuestionnaireInitialContactEmailProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;QuestionnaireInitialContactEmailChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireInitialContactEmail"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireInitialContactEmailProperty : ChildEntityProperty<QuestionnaireInitialContactEmailChildEntityTypes>
	{
	}
	
	#endregion QuestionnaireInitialContactEmailProperty
}

