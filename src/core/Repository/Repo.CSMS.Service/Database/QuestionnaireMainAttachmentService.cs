﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface IQuestionnaireMainAttachmentService
    {
        QuestionnaireMainAttachment Get(Expression<Func<QuestionnaireMainAttachment, bool>> where, List<Expression<Func<QuestionnaireMainAttachment, object>>> includes);
        List<QuestionnaireMainAttachment> GetMany(int? top, Expression<Func<QuestionnaireMainAttachment, bool>> where, Func<IQueryable<QuestionnaireMainAttachment>, IOrderedQueryable<QuestionnaireMainAttachment>> orderBy, List<Expression<Func<QuestionnaireMainAttachment, object>>> includes);
        List<QuestionnaireMainAttachment> GetManyPaged(int pageNumber, int pageSize, Expression<Func<QuestionnaireMainAttachment, bool>> where, Func<IQueryable<QuestionnaireMainAttachment>, IOrderedQueryable<QuestionnaireMainAttachment>> orderBy, List<Expression<Func<QuestionnaireMainAttachment, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<QuestionnaireMainAttachment, bool>> where, List<Expression<Func<QuestionnaireMainAttachment, object>>> includes);
        void Insert(QuestionnaireMainAttachment item, bool saveChanges = true);
        void Update(QuestionnaireMainAttachment item, bool saveChanges = true);
        void Delete(QuestionnaireMainAttachment item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class QuestionnaireMainAttachmentService : IQuestionnaireMainAttachmentService
    {
        private readonly IRepository<QuestionnaireMainAttachment> _repository;

        public QuestionnaireMainAttachmentService(IRepository<QuestionnaireMainAttachment> repository)
        {
            this._repository = repository;
        }

        public QuestionnaireMainAttachment Get(Expression<Func<QuestionnaireMainAttachment, bool>> where, List<Expression<Func<QuestionnaireMainAttachment, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<QuestionnaireMainAttachment> GetMany(int? top, Expression<Func<QuestionnaireMainAttachment, bool>> where, Func<IQueryable<QuestionnaireMainAttachment>, IOrderedQueryable<QuestionnaireMainAttachment>> orderBy, List<Expression<Func<QuestionnaireMainAttachment, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<QuestionnaireMainAttachment> GetManyPaged(int pageNumber, int pageSize, Expression<Func<QuestionnaireMainAttachment, bool>> where, Func<IQueryable<QuestionnaireMainAttachment>, IOrderedQueryable<QuestionnaireMainAttachment>> orderBy, List<Expression<Func<QuestionnaireMainAttachment, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<QuestionnaireMainAttachment, bool>> where, List<Expression<Func<QuestionnaireMainAttachment, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(QuestionnaireMainAttachment item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("QuestionnaireMainAttachment");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(QuestionnaireMainAttachment item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("QuestionnaireMainAttachment");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(QuestionnaireMainAttachment item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("QuestionnaireMainAttachment");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
