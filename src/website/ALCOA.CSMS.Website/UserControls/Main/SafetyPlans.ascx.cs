using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using KaiZen.CSMS.Services;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.Library;
using System.Security.Cryptography;
using System.IO;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxTabControl;
using DevExpress.Web.Data;
using DevExpress.Web.ASPxGridView;
using DevExpress.XtraPrinting;
using model = Repo.CSMS.DAL.EntityModels;
using repo = Repo.CSMS.Service.Database;

public partial class UserControls_SafetyPlans : System.Web.UI.UserControl
{
    int argCompanyId = 0;
    Auth auth = new Auth();
    repo.IAdminTaskEmailTemplateService adminTaskEmailTemplateService = ALCOA.CSMS.Website.Global.GetInstance<repo.IAdminTaskEmailTemplateService>();
    repo.IEmailTemplateService emailTemplateService = ALCOA.CSMS.Website.Global.GetInstance<repo.IEmailTemplateService>();
    repo.IEmailLogService emailLogService = ALCOA.CSMS.Website.Global.GetInstance<repo.IEmailLogService>();
    const string GridCustomPageSizeName = "gridCustomPageSize";


    protected void Page_Load(object sender, EventArgs e) { Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); }

    private void moduleInit(bool postBack)
    {
        if (Session[GridCustomPageSizeName] != null)
        {
            grid.SettingsPager.PageSize = (int)Session[GridCustomPageSizeName];
        }

        if (!postBack)
        {
            grid.Visible = false;

            SessionHandler.spVar_CompanyId = auth.CompanyId.ToString();

            if (auth.CompanyId == 0)
            {
                lblYour.Text = "";
                lblAlcoaContact.Text = "";
            }

            if (auth.isPrivledged(auth.UserId)) { SessionHandler.IsPrivledged = "1"; }
            else { SessionHandler.IsPrivledged = "0"; };

            switch (auth.RoleId)
            {
                case ((int)RoleList.Reader):
                    grid.Columns["commandCol"].Visible = false; //hide action column
                    grid.Visible = true;
                    grid.Columns[0].Visible = true;
                    grid.Settings.ShowFilterRow = true;
                    //can view all.
                    GridViewDataComboBoxColumn combo3 = grid.Columns["CompanyBox"] as GridViewDataComboBoxColumn;
                    combo3.PropertiesComboBox.DataSourceID = "sqldsCompaniesList";

                    btnSubmitSE.Visible = false;

                    //Check if User is an EHS Consultant
                    int count;
                    EhsConsultantFilters queryEhsConsultant = new EhsConsultantFilters();
                    queryEhsConsultant.Append(EhsConsultantColumn.UserId, auth.UserId.ToString());
                    using (TList<EhsConsultant> EhsConsultantList =
                        DataRepository.EhsConsultantProvider.GetPaged(queryEhsConsultant.ToString(), null, 0, 100, out count))
                    {
                        if (count > 0)
                        {
                            GridViewCommandColumn commandCol = grid.Columns["Action"] as GridViewCommandColumn;
                            commandCol.EditButton.Visible = true;
                            btnCompare.Visible = true;
                        }
                        else
                        {
                            GridViewCommandColumn commandCol = grid.Columns["Action"] as GridViewCommandColumn;
                            commandCol.EditButton.Visible = false;
                            commandCol.Visible = false;
                        }
                    }
                    break;
                case ((int)RoleList.Contractor):
                    argCompanyId = auth.CompanyId;
                    grid.Columns["commandCol"].Visible = false; //hide action column
                    grid.Visible = true;
                    grid.Settings.ShowFilterRow = false;

                    grid.DataSourceID = "odsFileDbGetByCompanyId_Contractor";
                    grid.DataBind();

                    lblAlcoaContact.Text = Helper.General.getEhsConsultantNameByCompany(auth.CompanyId);

                    grid.Columns["Company"].Visible = false;
                    break;
                case ((int)RoleList.Administrator):
                    grid.Visible = true;
                    grid.Columns[0].Visible = true;
                    grid.Settings.ShowFilterRow = true;
                    btnCompare.Visible = true;
                    //btnSubmitSE.Enabled  = false;
                    //can view all.
                    GridViewDataComboBoxColumn combo2 = grid.Columns["CompanyBox"] as GridViewDataComboBoxColumn;
                    combo2.PropertiesComboBox.DataSourceID = "sqldsCompaniesList";

                    break;
                default:
                    // RoleList.Disabled or No Role
                    Response.Redirect("AccessDenied.aspx?error=" + Server.UrlEncode("Unrecognised User."));
                    break;
            }
            //Default View = Current Year
            //grid.FilterExpression = "Description CONTAINS '%" + DateTime.Now.Year.ToString() + "%'";

            if (auth.CompanyId != 0)
            {
                SafetyPlansSeResponsesService sers = new SafetyPlansSeResponsesService();
                try
                {
                    DataSet dsSer = sers.GetByCompanyId_Latest_NotSubmitted(auth.CompanyId);
                    if (dsSer.Tables[0].Rows.Count == 0)
                    {
                        //no existing 'incomplete' smp...
                        //ok. check if a last 'complete' smp exists. if so, copy it to a new responseid.

                        DataSet dsSer2 = sers.GetByCompanyId_Latest_Submitted(auth.CompanyId);
                        if (dsSer2.Tables[0].Rows.Count > 0)
                        {
                            btnSubmitSE.Text = "Use Previous Questionnaire & SMP to create new plan";
                        }
                    }
                    else
                    {
                        btnSubmitSE.Text = "Submit Questionnaire & Safety Management Plan";
                    }
                }
                catch (Exception ex)
                {
                    Elmah.ErrorSignal.FromContext(Context).Raise(ex);
                    throw new Exception(ex.Message);
                }
            }

            // Export
            String exportFileName = @"ALCOA CSMS - Safety Plans"; //Chrome & IE is fine.
            String userAgent = Request.Headers.Get("User-Agent");
            if (
                (userAgent.Contains("Mozilla") && !userAgent.Contains("AppleWebKit") && !userAgent.Contains("MSIE")) || //Mozilla
                (userAgent.Contains("AppleWebKit") && !userAgent.Contains("Chrome"))      //Safari
                )
            {
                exportFileName = exportFileName.Replace(" ", "_");
            }
            Helper.ExportGrid.Settings(ucExportButtons, exportFileName);
        }
        else
        { //postback
            //1. Load selected ddl.itemvalues into args
            if (argCompanyId > 0)
            {
                //FIX THIS.
                lblAlcoaContact.Text = Helper.General.getEhsConsultantNameByCompany(argCompanyId);
            }
        }
    }

    protected void grid_AutoFilterCellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
    {
        if (e.Column.FieldName == "CompanyId")
        {
            ASPxComboBox comboBox = e.Editor as ASPxComboBox;
            //comboBox.Items.Clear();
            comboBox.ClientSideEvents.Init = "function(s, e) {s.InsertItem(0, '(ALL)', ''); s.RemoveItem(-1);}";
        }

        if (e.Column.FieldName == "EhsConsultantId")
        {
        }
    }

    protected void grid_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        CompaniesService cService = new CompaniesService();
        Companies c = cService.GetByCompanyId((int)e.OldValues["CompanyId"]);

        UsersService uService = new UsersService();
        Users userEntity = uService.GetByUserId(auth.UserId);
        Companies companiesEntity2 = cService.GetByCompanyId(auth.CompanyId);

        int? EhsConsultantId_New = (int?)e.NewValues["EhsConsultantId"];
        int? EhsConsultantId_Old = (int?)e.OldValues["EhsConsultantId"];

        string EhsConsultant_New = "";
        string EhsConsultant_Old = "";
        string EhsConsultant_New_Email = "";
        string EhsConsultant_Old_Email = "";

        EhsConsultantService eService = new EhsConsultantService();
        if (EhsConsultantId_New == null)
        {
            EhsConsultant_New = "n/a";
        }
        else
        {
            EhsConsultant ec = eService.GetByEhsConsultantId(Convert.ToInt32(EhsConsultantId_New));
            Users u = uService.GetByUserId(ec.UserId);
            EhsConsultant_New = String.Format("{0},{1}", u.LastName, u.FirstName);
            EhsConsultant_New_Email = u.Email;
        }
        if (EhsConsultantId_Old == null)
        {
            EhsConsultant_Old = "n/a";
        }
        else
        {
            EhsConsultant ec = eService.GetByEhsConsultantId(Convert.ToInt32(EhsConsultantId_Old));
            Users u = uService.GetByUserId(ec.UserId);
            EhsConsultant_Old = String.Format("{0},{1}", u.LastName, u.FirstName);
            EhsConsultant_Old_Email = u.Email;
        }


        //string subject = String.Format("Safety Management Plan: {0}: Safety Assessor Changed", c.CompanyName);
        //string body = String.Format("A Companies Safety Management Plan Safety assessor has been changed as follows:\nCompany Name: {0}\nCompany Assessor: {1} (Previously: {2})\n\n\nChanged by: {3}, {4} ({5}) at {6}\n", c.CompanyName, EhsConsultant_New, EhsConsultant_Old, userEntity.LastName, userEntity.FirstName, companiesEntity2.CompanyName, DateTime.Now);

        model.EmailTemplate atemt = emailTemplateService.Get(i => i.EmailTemplateName == "Safety Assessor Changed", null);
        string subject = atemt.EmailTemplateSubject;
        subject = subject.Replace("{CompanyName}",c.CompanyName);
        subject = subject.Replace("{EhsConsultant_New}", EhsConsultant_New);
        subject = subject.Replace("{EhsConsultant_Old}", EhsConsultant_Old);
        subject = subject.Replace("{UserLastName}", userEntity.LastName);
        subject = subject.Replace("{UserFirstName}", userEntity.FirstName);
        subject = subject.Replace("{UserCompanyName}", companiesEntity2.CompanyName);
        subject = subject.Replace("{CurrentDateTime}", DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));

        byte[] bodyByte = atemt.EmailBody;
        string body = System.Text.Encoding.ASCII.GetString(bodyByte);
        
        body = body.Replace("{CompanyName}",c.CompanyName);
        body = body.Replace("{EhsConsultant_New}",EhsConsultant_New);
        body = body.Replace("{EhsConsultant_Old}",EhsConsultant_Old);
        body = body.Replace("{UserLastName}",userEntity.LastName);
        body = body.Replace("{UserFirstName}",userEntity.FirstName);
        body = body.Replace("{UserCompanyName}",companiesEntity2.CompanyName);
        body = body.Replace("{CurrentDateTime}",DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));

        Configuration configuration = new Configuration();
        string[] to = { EhsConsultant_New_Email };

        if (!String.IsNullOrEmpty(EhsConsultant_Old_Email))
        {
            string[] csmteam = { configuration.GetValue(ConfigList.ContactEmail), EhsConsultant_Old_Email  };
            
            if (!String.IsNullOrEmpty(EhsConsultant_New_Email))
            {
                
               // Helper.Email.sendEmail(to, csmteam, null, subject, body, null);
                //Helper.Email.logEmail(null, null, to, csmteam, null, subject, body, EmailLogTypeList.SafetyPlans);
                emailLogService.Insert(to, csmteam, null, subject, body, "SafetyPlans",true,null,null);
                //These sections modified by Ashley Goldstraw on 7/9/2015.  Now just needs to insert the email into the Log table and a service will pick it up to send it at some time.
              
            }
            else
            { // no safety assessor selected?! send to waocsm team!
                //Helper.Email.sendEmail(csmteam, null, null, subject, body, null);
                //Helper.Email.logEmail(null, null, csmteam, null, null, subject, body, EmailLogTypeList.SafetyPlans);
                emailLogService.Insert(csmteam, null, null, subject, body, "SafetyPlans", true, null,null);
            }
        }
        else
        {
            string[] csmteam = { configuration.GetValue(ConfigList.ContactEmail) };

            if (!String.IsNullOrEmpty(EhsConsultant_New_Email))
            {
                emailLogService.Insert(to, csmteam, null, subject, body, "SafetyPlans", true, null, null);
               // Helper.Email.sendEmail(to, csmteam, null, subject, body, null);
                //Helper.Email.logEmail(null, null, to, csmteam, null, subject, body, EmailLogTypeList.SafetyPlans);
            }
            else
            { // no safety assessor selected?! send to waocsm team!
              //  Helper.Email.sendEmail(csmteam, null, null, subject, body, null);
               // Helper.Email.logEmail(null, null, csmteam, null, null, subject, body, EmailLogTypeList.SafetyPlans);
                emailLogService.Insert(csmteam, null, null, subject, body, "SafetyPlans", true, null, null);
            }
        }
    }

    protected void grid_RowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {
        if (e.RowType != GridViewRowType.Data) return;
        ASPxHyperLink hl = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "hlSE") as ASPxHyperLink;
        ASPxHyperLink hlAttachment = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "hlAttachment") as ASPxHyperLink;
        int r = NumberUtilities.Convert.parseObjectToInt(grid.GetRowValues(e.VisibleIndex, "SelfAssessment_ResponseId"));
        int f = NumberUtilities.Convert.parseObjectToInt(grid.GetRowValues(e.VisibleIndex, "FileId"));
        //Start:Change by Debashis for Deployment issue
        if (hl != null && r != null) 
        {
            if (r > 0)
            {
                hl.NavigateUrl = "~/SafetyPlans_SelfAssessment.aspx" + QueryStringModule.Encrypt(String.Format("R={0}", r));
                hl.Text = "View";

                hlAttachment.NavigateUrl = "~/Common/GetFile.aspx" + QueryStringModule.Encrypt(String.Format("Type=CSMS&SubType=SafetyPlans&File={0}", f));
                hlAttachment.Text = (string)grid.GetRowValues(e.VisibleIndex, "FileName");
            }
            else
            {
                hl.NavigateUrl = "";
                hl.Text = "";
            }
        }
        //End:Change by Debashis for Deployment issue
    }

    protected void btnSubmitSE_Click(object sender, EventArgs e)
    {
        if (auth.CompanyId != 0)
        {
            SafetyPlansSeResponsesService sers = new SafetyPlansSeResponsesService();
            int ResponseId_New = -1;
            try
            {

                DataSet dsSer = sers.GetByCompanyId_Latest_NotSubmitted(Convert.ToInt32(SessionHandler.spVar_CompanyId));
                if (dsSer.Tables[0].Rows.Count == 0)
                {
                    //no existing 'incomplete' smp...
                    //ok. check if a last 'complete' smp exists. if so, copy it to a new responseid.
                    DataSet dsSer2 = sers.GetByCompanyId_Latest_Submitted(Convert.ToInt32(SessionHandler.spVar_CompanyId));
                    if (dsSer2.Tables[0].Rows.Count > 0 && auth.RoleId != (int)RoleList.Administrator)
                    {
                        int? ResponseId_Old = null;
                        foreach (DataRow dr2 in dsSer2.Tables[0].Rows)
                        {
                            ResponseId_Old = Convert.ToInt32(dr2[0].ToString());
                        }
                        ResponseId_New = CopySmp(ResponseId_Old);
                    }
                }
                if (ResponseId_New > 0)
                {
                    Response.Redirect(String.Format("~/SafetyPlans_SelfAssessment.aspx?{0}';", QueryStringModule.Encrypt(String.Format("R={0}", ResponseId_New))), false);
                }
                else
                {
                    Response.Redirect("~/SafetyPlans_SelfAssessment.aspx?i=1", false);
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromContext(Context).Raise(ex);
                throw new Exception(ex.Message);
            }
        }
        else
        {
            Response.Redirect("~/SafetyPlans_SelfAssessment.aspx?i=1", false);
        }
    }

    protected int CopySmp(int? ResponseId_Old)
    {
        int ResponseId_New = -1;
        if (ResponseId_Old != null)
        {
            SafetyPlansSeResponsesService spseResponsesService = new SafetyPlansSeResponsesService();
            SafetyPlansSeResponses spseResponses_original = spseResponsesService.GetByResponseId(Convert.ToInt32(ResponseId_Old));
            if (spseResponses_original != null)
            {
                SafetyPlansSeResponses spseResponses_new = spseResponses_original.Copy();
                spseResponses_new.EntityState = EntityState.Added;
                spseResponses_new.ModifiedByUserId = auth.UserId;
                spseResponses_new.ModifiedDate = DateTime.Now;
                spseResponses_new.CreatedDate = DateTime.Now;
                spseResponses_new.Year = DateTime.Now.Year;
                spseResponses_new.AsessorComment = null;
                spseResponses_new.Submitted = false;
                spseResponses_new.IsCopy = true;
                spseResponsesService.Save(spseResponses_new);

                ResponseId_New = spseResponses_new.ResponseId;

                //Answers
                SafetyPlansSeAnswersService spseAnswersService = new SafetyPlansSeAnswersService();
                TList<SafetyPlansSeAnswers> spseAnswers_original_list = spseAnswersService.GetByResponseId(Convert.ToInt32(ResponseId_Old));
                if (spseAnswers_original_list != null)
                {
                    foreach (SafetyPlansSeAnswers spseAnswers_original in spseAnswers_original_list)
                    {
                        SafetyPlansSeAnswers spseAnswers_new = spseAnswers_original.Copy();
                        spseAnswers_new.EntityState = EntityState.Added;
                        spseAnswers_new.AssessorApproval = null;
                        spseAnswers_new.AssessorComment = null;
                        spseAnswers_new.ResponseId = ResponseId_New;
                        spseAnswersService.Save(spseAnswers_new);
                        spseAnswers_new.Dispose();
                    }
                }
                spseAnswers_original_list.Dispose();
            }
        }
        return ResponseId_New;
    }

    protected void grid_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        int newPageSize;
        if (!int.TryParse(e.Parameters, out newPageSize)) return;
        grid.SettingsPager.PageSize = newPageSize;
        Session[GridCustomPageSizeName] = newPageSize;
        grid.DataBind();
    }
    protected string WriteSelectedIndex(int pageSize)
    {
        return pageSize == grid.SettingsPager.PageSize ? "selected='selected'" : string.Empty;
    }
    protected void ASPxButton1_Click(object sender, EventArgs e)
    {
        System.Collections.Generic.List<object> keyValues = grid.GetSelectedFieldValues("SelfAssessment_ResponseId");
        int i = 0;
        string Compare1 = "";
        string Compare2 = "";

        if (keyValues.Count == 2)
        {
            foreach (int key in keyValues)
            {
                if (i == 0)
                {
                    Compare1 = key.ToString();
                    i++;
                }
                else
                {
                    Compare2 = key.ToString();
                }
            }
            Response.Redirect(String.Format("javascript:popUp('PopUps/SafetyPlans_Compare.aspx{0}'); window.location='SafetyPlans.aspx';", QueryStringModule.Encrypt(String.Format("R={0}&S={1}", Compare1, Compare2))));
        }
        else
        {
            throw new Exception("Please Select Only 2 Safety Management Plans");
        }
    }

    protected void grid_CellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
    {
        if (e.Column.FieldName == "EhsConsultantId")
        {

            (e.Editor as ASPxComboBox).DataBound += new EventHandler(EhsConsultantId_EditComboBox_DataBound);
        }
    }

    private void EhsConsultantId_EditComboBox_DataBound(object sender, EventArgs e)
    {
        ListEditItem noneItem = new ListEditItem("(None)", null);
        (sender as ASPxComboBox).Items.Insert(0, noneItem);
    }
}