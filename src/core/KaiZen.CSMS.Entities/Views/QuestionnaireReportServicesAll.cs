﻿#region Using directives

using System;

#endregion

namespace KaiZen.CSMS.Entities
{	
	///<summary>
	/// An object representation of the 'QuestionnaireReportServices_All' view. [No description found in the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class QuestionnaireReportServicesAll : QuestionnaireReportServicesAllBase
	{
		#region Constructors

		///<summary>
		/// Creates a new <see cref="QuestionnaireReportServicesAll"/> instance.
		///</summary>
		public QuestionnaireReportServicesAll():base(){}	
		
		#endregion
	}
}
