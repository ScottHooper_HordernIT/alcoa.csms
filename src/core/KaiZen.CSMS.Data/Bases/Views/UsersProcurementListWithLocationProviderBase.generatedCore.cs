﻿#region Using directives

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="UsersProcurementListWithLocationProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract class UsersProcurementListWithLocationProviderBaseCore : EntityViewProviderBase<UsersProcurementListWithLocation>
	{
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions
		
		/*
		///<summary>
		/// Fill an VList&lt;UsersProcurementListWithLocation&gt; From a DataSet
		///</summary>
		/// <param name="dataSet">the DataSet</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList&lt;UsersProcurementListWithLocation&gt;"/></returns>
		protected static VList&lt;UsersProcurementListWithLocation&gt; Fill(DataSet dataSet, VList<UsersProcurementListWithLocation> rows, int start, int pagelen)
		{
			if (dataSet.Tables.Count == 1)
			{
				return Fill(dataSet.Tables[0], rows, start, pagelen);
			}
			else
			{
				return new VList<UsersProcurementListWithLocation>();
			}	
		}
		
		
		///<summary>
		/// Fill an VList&lt;UsersProcurementListWithLocation&gt; From a DataTable
		///</summary>
		/// <param name="dataTable">the DataTable that hold the data.</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList<UsersProcurementListWithLocation>"/></returns>
		protected static VList&lt;UsersProcurementListWithLocation&gt; Fill(DataTable dataTable, VList<UsersProcurementListWithLocation> rows, int start, int pagelen)
		{
			int recordnum = 0;
			
			System.Collections.IEnumerator dataRows =  dataTable.Rows.GetEnumerator();
			
			while (dataRows.MoveNext() && (pagelen != 0))
			{
				if(recordnum >= start)
				{
					DataRow row = (DataRow)dataRows.Current;
				
					UsersProcurementListWithLocation c = new UsersProcurementListWithLocation();
					c.UsersProcurementId = (Convert.IsDBNull(row["UsersProcurementId"]))?(int)0:(System.Int32)row["UsersProcurementId"];
					c.UserId = (Convert.IsDBNull(row["UserId"]))?(int)0:(System.Int32)row["UserId"];
					c.UserFullName = (Convert.IsDBNull(row["UserFullName"]))?string.Empty:(System.String)row["UserFullName"];
					c.UserFullNameLogon = (Convert.IsDBNull(row["UserFullNameLogon"]))?string.Empty:(System.String)row["UserFullNameLogon"];
					c.Enabled = (Convert.IsDBNull(row["Enabled"]))?false:(System.Boolean)row["Enabled"];
					c.Email = (Convert.IsDBNull(row["Email"]))?string.Empty:(System.String)row["Email"];
					c.NoQuestionnairesAssignedTo = (Convert.IsDBNull(row["NoQuestionnairesAssignedTo"]))?(int)0:(System.Int32?)row["NoQuestionnairesAssignedTo"];
					c.SiteId = (Convert.IsDBNull(row["SiteId"]))?(int)0:(System.Int32?)row["SiteId"];
					c.SiteName = (Convert.IsDBNull(row["SiteName"]))?string.Empty:(System.String)row["SiteName"];
					c.RegionId = (Convert.IsDBNull(row["RegionId"]))?(int)0:(System.Int32?)row["RegionId"];
					c.RegionName = (Convert.IsDBNull(row["RegionName"]))?string.Empty:(System.String)row["RegionName"];
					c.RegionNameAbbrev = (Convert.IsDBNull(row["RegionNameAbbrev"]))?string.Empty:(System.String)row["RegionNameAbbrev"];
					c.AcceptChanges();
					rows.Add(c);
					pagelen -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		*/	
						
		///<summary>
		/// Fill an <see cref="VList&lt;UsersProcurementListWithLocation&gt;"/> From a DataReader.
		///</summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pageLength">number of row.</param>
		///<returns>a <see cref="VList&lt;UsersProcurementListWithLocation&gt;"/></returns>
		protected VList<UsersProcurementListWithLocation> Fill(IDataReader reader, VList<UsersProcurementListWithLocation> rows, int start, int pageLength)
		{
			int recordnum = 0;
			while (reader.Read() && (pageLength != 0))
			{
				if(recordnum >= start)
				{
					UsersProcurementListWithLocation entity = null;
					if (DataRepository.Provider.UseEntityFactory)
					{
						entity = EntityManager.CreateViewEntity<UsersProcurementListWithLocation>("UsersProcurementListWithLocation",  DataRepository.Provider.EntityCreationalFactoryType); 
					}
					else
					{
						entity = new UsersProcurementListWithLocation();
					}
					
					entity.SuppressEntityEvents = true;

					entity.UsersProcurementId = (System.Int32)reader[((int)UsersProcurementListWithLocationColumn.UsersProcurementId)];
					//entity.UsersProcurementId = (Convert.IsDBNull(reader["UsersProcurementId"]))?(int)0:(System.Int32)reader["UsersProcurementId"];
					entity.UserId = (System.Int32)reader[((int)UsersProcurementListWithLocationColumn.UserId)];
					//entity.UserId = (Convert.IsDBNull(reader["UserId"]))?(int)0:(System.Int32)reader["UserId"];
					entity.UserFullName = (System.String)reader[((int)UsersProcurementListWithLocationColumn.UserFullName)];
					//entity.UserFullName = (Convert.IsDBNull(reader["UserFullName"]))?string.Empty:(System.String)reader["UserFullName"];
					entity.UserFullNameLogon = (System.String)reader[((int)UsersProcurementListWithLocationColumn.UserFullNameLogon)];
					//entity.UserFullNameLogon = (Convert.IsDBNull(reader["UserFullNameLogon"]))?string.Empty:(System.String)reader["UserFullNameLogon"];
					entity.Enabled = (System.Boolean)reader[((int)UsersProcurementListWithLocationColumn.Enabled)];
					//entity.Enabled = (Convert.IsDBNull(reader["Enabled"]))?false:(System.Boolean)reader["Enabled"];
					entity.Email = (System.String)reader[((int)UsersProcurementListWithLocationColumn.Email)];
					//entity.Email = (Convert.IsDBNull(reader["Email"]))?string.Empty:(System.String)reader["Email"];
					entity.NoQuestionnairesAssignedTo = (reader.IsDBNull(((int)UsersProcurementListWithLocationColumn.NoQuestionnairesAssignedTo)))?null:(System.Int32?)reader[((int)UsersProcurementListWithLocationColumn.NoQuestionnairesAssignedTo)];
					//entity.NoQuestionnairesAssignedTo = (Convert.IsDBNull(reader["NoQuestionnairesAssignedTo"]))?(int)0:(System.Int32?)reader["NoQuestionnairesAssignedTo"];
					entity.SiteId = (reader.IsDBNull(((int)UsersProcurementListWithLocationColumn.SiteId)))?null:(System.Int32?)reader[((int)UsersProcurementListWithLocationColumn.SiteId)];
					//entity.SiteId = (Convert.IsDBNull(reader["SiteId"]))?(int)0:(System.Int32?)reader["SiteId"];
					entity.SiteName = (reader.IsDBNull(((int)UsersProcurementListWithLocationColumn.SiteName)))?null:(System.String)reader[((int)UsersProcurementListWithLocationColumn.SiteName)];
					//entity.SiteName = (Convert.IsDBNull(reader["SiteName"]))?string.Empty:(System.String)reader["SiteName"];
					entity.RegionId = (reader.IsDBNull(((int)UsersProcurementListWithLocationColumn.RegionId)))?null:(System.Int32?)reader[((int)UsersProcurementListWithLocationColumn.RegionId)];
					//entity.RegionId = (Convert.IsDBNull(reader["RegionId"]))?(int)0:(System.Int32?)reader["RegionId"];
					entity.RegionName = (reader.IsDBNull(((int)UsersProcurementListWithLocationColumn.RegionName)))?null:(System.String)reader[((int)UsersProcurementListWithLocationColumn.RegionName)];
					//entity.RegionName = (Convert.IsDBNull(reader["RegionName"]))?string.Empty:(System.String)reader["RegionName"];
					entity.RegionNameAbbrev = (reader.IsDBNull(((int)UsersProcurementListWithLocationColumn.RegionNameAbbrev)))?null:(System.String)reader[((int)UsersProcurementListWithLocationColumn.RegionNameAbbrev)];
					//entity.RegionNameAbbrev = (Convert.IsDBNull(reader["RegionNameAbbrev"]))?string.Empty:(System.String)reader["RegionNameAbbrev"];
					entity.AcceptChanges();
					entity.SuppressEntityEvents = false;
					
					rows.Add(entity);
					pageLength -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		
		
		/// <summary>
		/// Refreshes the <see cref="UsersProcurementListWithLocation"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="UsersProcurementListWithLocation"/> object to refresh.</param>
		protected void RefreshEntity(IDataReader reader, UsersProcurementListWithLocation entity)
		{
			reader.Read();
			entity.UsersProcurementId = (System.Int32)reader[((int)UsersProcurementListWithLocationColumn.UsersProcurementId)];
			//entity.UsersProcurementId = (Convert.IsDBNull(reader["UsersProcurementId"]))?(int)0:(System.Int32)reader["UsersProcurementId"];
			entity.UserId = (System.Int32)reader[((int)UsersProcurementListWithLocationColumn.UserId)];
			//entity.UserId = (Convert.IsDBNull(reader["UserId"]))?(int)0:(System.Int32)reader["UserId"];
			entity.UserFullName = (System.String)reader[((int)UsersProcurementListWithLocationColumn.UserFullName)];
			//entity.UserFullName = (Convert.IsDBNull(reader["UserFullName"]))?string.Empty:(System.String)reader["UserFullName"];
			entity.UserFullNameLogon = (System.String)reader[((int)UsersProcurementListWithLocationColumn.UserFullNameLogon)];
			//entity.UserFullNameLogon = (Convert.IsDBNull(reader["UserFullNameLogon"]))?string.Empty:(System.String)reader["UserFullNameLogon"];
			entity.Enabled = (System.Boolean)reader[((int)UsersProcurementListWithLocationColumn.Enabled)];
			//entity.Enabled = (Convert.IsDBNull(reader["Enabled"]))?false:(System.Boolean)reader["Enabled"];
			entity.Email = (System.String)reader[((int)UsersProcurementListWithLocationColumn.Email)];
			//entity.Email = (Convert.IsDBNull(reader["Email"]))?string.Empty:(System.String)reader["Email"];
			entity.NoQuestionnairesAssignedTo = (reader.IsDBNull(((int)UsersProcurementListWithLocationColumn.NoQuestionnairesAssignedTo)))?null:(System.Int32?)reader[((int)UsersProcurementListWithLocationColumn.NoQuestionnairesAssignedTo)];
			//entity.NoQuestionnairesAssignedTo = (Convert.IsDBNull(reader["NoQuestionnairesAssignedTo"]))?(int)0:(System.Int32?)reader["NoQuestionnairesAssignedTo"];
			entity.SiteId = (reader.IsDBNull(((int)UsersProcurementListWithLocationColumn.SiteId)))?null:(System.Int32?)reader[((int)UsersProcurementListWithLocationColumn.SiteId)];
			//entity.SiteId = (Convert.IsDBNull(reader["SiteId"]))?(int)0:(System.Int32?)reader["SiteId"];
			entity.SiteName = (reader.IsDBNull(((int)UsersProcurementListWithLocationColumn.SiteName)))?null:(System.String)reader[((int)UsersProcurementListWithLocationColumn.SiteName)];
			//entity.SiteName = (Convert.IsDBNull(reader["SiteName"]))?string.Empty:(System.String)reader["SiteName"];
			entity.RegionId = (reader.IsDBNull(((int)UsersProcurementListWithLocationColumn.RegionId)))?null:(System.Int32?)reader[((int)UsersProcurementListWithLocationColumn.RegionId)];
			//entity.RegionId = (Convert.IsDBNull(reader["RegionId"]))?(int)0:(System.Int32?)reader["RegionId"];
			entity.RegionName = (reader.IsDBNull(((int)UsersProcurementListWithLocationColumn.RegionName)))?null:(System.String)reader[((int)UsersProcurementListWithLocationColumn.RegionName)];
			//entity.RegionName = (Convert.IsDBNull(reader["RegionName"]))?string.Empty:(System.String)reader["RegionName"];
			entity.RegionNameAbbrev = (reader.IsDBNull(((int)UsersProcurementListWithLocationColumn.RegionNameAbbrev)))?null:(System.String)reader[((int)UsersProcurementListWithLocationColumn.RegionNameAbbrev)];
			//entity.RegionNameAbbrev = (Convert.IsDBNull(reader["RegionNameAbbrev"]))?string.Empty:(System.String)reader["RegionNameAbbrev"];
			reader.Close();
	
			entity.AcceptChanges();
		}
		
		/*
		/// <summary>
		/// Refreshes the <see cref="UsersProcurementListWithLocation"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="UsersProcurementListWithLocation"/> object.</param>
		protected static void RefreshEntity(DataSet dataSet, UsersProcurementListWithLocation entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.UsersProcurementId = (Convert.IsDBNull(dataRow["UsersProcurementId"]))?(int)0:(System.Int32)dataRow["UsersProcurementId"];
			entity.UserId = (Convert.IsDBNull(dataRow["UserId"]))?(int)0:(System.Int32)dataRow["UserId"];
			entity.UserFullName = (Convert.IsDBNull(dataRow["UserFullName"]))?string.Empty:(System.String)dataRow["UserFullName"];
			entity.UserFullNameLogon = (Convert.IsDBNull(dataRow["UserFullNameLogon"]))?string.Empty:(System.String)dataRow["UserFullNameLogon"];
			entity.Enabled = (Convert.IsDBNull(dataRow["Enabled"]))?false:(System.Boolean)dataRow["Enabled"];
			entity.Email = (Convert.IsDBNull(dataRow["Email"]))?string.Empty:(System.String)dataRow["Email"];
			entity.NoQuestionnairesAssignedTo = (Convert.IsDBNull(dataRow["NoQuestionnairesAssignedTo"]))?(int)0:(System.Int32?)dataRow["NoQuestionnairesAssignedTo"];
			entity.SiteId = (Convert.IsDBNull(dataRow["SiteId"]))?(int)0:(System.Int32?)dataRow["SiteId"];
			entity.SiteName = (Convert.IsDBNull(dataRow["SiteName"]))?string.Empty:(System.String)dataRow["SiteName"];
			entity.RegionId = (Convert.IsDBNull(dataRow["RegionId"]))?(int)0:(System.Int32?)dataRow["RegionId"];
			entity.RegionName = (Convert.IsDBNull(dataRow["RegionName"]))?string.Empty:(System.String)dataRow["RegionName"];
			entity.RegionNameAbbrev = (Convert.IsDBNull(dataRow["RegionNameAbbrev"]))?string.Empty:(System.String)dataRow["RegionNameAbbrev"];
			entity.AcceptChanges();
		}
		*/
			
		#endregion Helper Functions
	}//end class

	#region UsersProcurementListWithLocationFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="UsersProcurementListWithLocation"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UsersProcurementListWithLocationFilterBuilder : SqlFilterBuilder<UsersProcurementListWithLocationColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UsersProcurementListWithLocationFilterBuilder class.
		/// </summary>
		public UsersProcurementListWithLocationFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the UsersProcurementListWithLocationFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public UsersProcurementListWithLocationFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the UsersProcurementListWithLocationFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public UsersProcurementListWithLocationFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion UsersProcurementListWithLocationFilterBuilder

	#region UsersProcurementListWithLocationParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="UsersProcurementListWithLocation"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UsersProcurementListWithLocationParameterBuilder : ParameterizedSqlFilterBuilder<UsersProcurementListWithLocationColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UsersProcurementListWithLocationParameterBuilder class.
		/// </summary>
		public UsersProcurementListWithLocationParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the UsersProcurementListWithLocationParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public UsersProcurementListWithLocationParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the UsersProcurementListWithLocationParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public UsersProcurementListWithLocationParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion UsersProcurementListWithLocationParameterBuilder
	
	#region UsersProcurementListWithLocationSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="UsersProcurementListWithLocation"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class UsersProcurementListWithLocationSortBuilder : SqlSortBuilder<UsersProcurementListWithLocationColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UsersProcurementListWithLocationSqlSortBuilder class.
		/// </summary>
		public UsersProcurementListWithLocationSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion UsersProcurementListWithLocationSortBuilder

} // end namespace
