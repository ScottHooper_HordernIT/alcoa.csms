using System;
using System.Data;
using System.Configuration;
using System.Web.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text.RegularExpressions;

using KaiZen.CSMS.Web;
using KaiZen.CSMS.Services;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Entities;
using KaiZen.Library;

using System.Net.Mail; //new
using System.Collections.Specialized;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.IO;
using System.Security.Principal;
using System.Runtime.InteropServices;
using System.Text;
using System.Linq;

using DevExpress.Web;
using DevExpress.Web.ASPxClasses;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxUploadControl;
using DevExpress.Web.ASPxCallbackPanel;
using DevExpress.Web.ASPxSiteMapControl;
using DevExpress.Web.ASPxGridView.Export;

using Elmah;
using System.Reflection;
//using model = Repo.CSMS.DAL.EntityModels;
//using repo = Repo.CSMS.Service.Database;

/// <summary>
/// Summary description for Helper
/// </summary>
public class Helper
{

    public class Email
    {
        //public static bool notifyEHSConsultant(int CompanyId, string[] cc, string[] bcc, string subject, string body, string attachmentPath)
        //{
        //    try
        //    {
        //        string[] to = { Helper.General.getEhsConsultantEmailByCompany(CompanyId, true) };
        //        logEmail(null, null, to, cc, bcc, subject, body, EmailLogTypeList.Questionnaire);
        //        return sendEmail(to, cc, bcc, subject, body, attachmentPath);
        //    }
        //    catch (Exception)
        //    {
        //        return false;
        //    }
        //}
        /*public static bool sendEmail(string[] to, string[] cc, string[] bcc, string subject, string body, string attachmentPath, bool isBodyHtml)
        {
            try
            {
                Configuration configuration = new Configuration();
                string fromEmail = configuration.GetValue(ConfigList.ContactEmail);
                string fromName = "AUA Alcoa Contractor Services Team";
                //string mailServer = configuration.GetValue(ConfigList.MailServer);

                using (MailMessage mail = new MailMessage())
                {
                    mail.From = new MailAddress(fromEmail, fromName);
                    if (to != null)
                        foreach (string s in to)
                            mail.To.Add(s);
                    if (cc != null)
                        foreach (string s in cc)
                            mail.CC.Add(s);
                    if (bcc != null)
                        foreach (string s in bcc)
                            mail.Bcc.Add(s);
                    if (!String.IsNullOrEmpty(attachmentPath))
                        if (System.IO.File.Exists(attachmentPath))
                            mail.Attachments.Add(new Attachment(attachmentPath));
                    mail.Subject = subject;
                    mail.IsBodyHtml = isBodyHtml;
                    mail.Body = body;
                    
                    //SmtpClient smtp = new SmtpClient(mailServer);
                    SmtpClient smtp = new SmtpClient();
                    smtp.Send(mail);
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static bool sendEmail(string[] to, string[] cc, string[] bcc, string subject, string body, string attachmentPath)
        {
            return sendEmail(to, cc, bcc, subject, body, attachmentPath, false);
        }
        */
        /*public static bool sendEmailFrom(string fromName, string fromEmail, string[] to, string[] cc, string[] bcc, string subject, string body, string attachmentPath, bool isBodyHtml)
        {
            try
            {
                Configuration configuration = new Configuration();
                //string mailServer = configuration.GetValue(ConfigList.MailServer);

                using (MailMessage mail = new MailMessage())
                {
                    mail.From = new MailAddress(fromEmail, fromName);
                    if (to != null)
                        foreach (string s in to)
                            mail.To.Add(s);
                    if (cc != null)
                        foreach (string s in cc)
                            mail.CC.Add(s);
                    if (bcc != null)
                        foreach (string s in bcc)
                            mail.Bcc.Add(s);
                    if (!String.IsNullOrEmpty(attachmentPath))
                        if (System.IO.File.Exists(attachmentPath))
                            mail.Attachments.Add(new Attachment(attachmentPath));
                    mail.Subject = subject;
                    mail.IsBodyHtml = isBodyHtml;
                    mail.Body = body;
                    
                    //SmtpClient smtp = new SmtpClient(mailServer);
                    SmtpClient smtp = new SmtpClient();
                    smtp.Send(mail);
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static bool sendEmailFrom(string fromName, string fromEmail, string[] to, string[] cc, string[] bcc, string subject, string body, string attachmentPath)
        {
            return sendEmailFrom(fromName, fromEmail, to, cc, bcc, subject, body, attachmentPath, false);
        }
        */
        /*public static bool logEmail(string fromName, string fromEmail, string[] to, string[] cc, string[] bcc, string subject, string body, EmailLogTypeList eltl)
        {
            try
            {
                EmailLogService elService = new EmailLogService();
                EmailLog el = new EmailLog();
                el.EmailLogTypeId = (int)eltl;
                el.EmailDateTime = DateTime.Now;

                if (!String.IsNullOrEmpty(fromName))
                    el.EmailFrom = fromName;
                if (!String.IsNullOrEmpty(fromEmail))
                {
                    if (String.IsNullOrEmpty(fromName))
                    {
                        el.EmailFrom = fromEmail;
                    }
                    else
                    {
                        el.EmailFrom = fromName + " <" + fromEmail + ">";
                    }
                }

                el.EmailTo = StringUtilities.Convert.StringArrayToString(to);
                if (cc != null)
                {
                    if (!string.IsNullOrEmpty(StringUtilities.Convert.StringArrayToString(cc)))
                        el.EmailCc = StringUtilities.Convert.StringArrayToString(cc);
                }
                if (bcc != null)
                {
                    if (!string.IsNullOrEmpty(StringUtilities.Convert.StringArrayToString(bcc)))
                        el.EmailBcc = StringUtilities.Convert.StringArrayToString(bcc);
                }

                el.EmailLogMessageSubject = subject;

                System.Text.ASCIIEncoding encoding = new System.Text.ASCIIEncoding();
                el.EmailLogMessageBody = (byte[])encoding.GetBytes(body);

                elService.Save(el);

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        */
        /*public void sendRegionalManagerApprovedOrNotApprovedEmail()
        {

        }
         */
    }


    public class ExportGrid
    {
        static String ucGridExporter = "gridExporter";
        static String fileNamePrefix = @""; //Alcoa CSMS - 
        public static void Settings(UserControl ucExportButtons, String @FileName)
        {
            ASPxGridViewExporter ex = (ASPxGridViewExporter)ucExportButtons.FindControl(ucGridExporter);
            ex.FileName = fileNamePrefix + FileName;
            //ex.FileName = ex.FileName.Replace(" ", "_"); //Spaces changed to Underscore to suit FireFox/etc.
        }
        public static void Settings(UserControl ucExportButtons, String @FileName, string grid)
        {
            
            ASPxGridViewExporter ex = (ASPxGridViewExporter)ucExportButtons.FindControl(ucGridExporter);
            ex.FileName = fileNamePrefix + FileName;
            ex.GridViewID = grid;
        }
        public static void Settings(UserControl ucExportButtons, String @FileName, Boolean Landscape)
        {
            ASPxGridViewExporter ex = (ASPxGridViewExporter)ucExportButtons.FindControl(ucGridExporter);
            ex.FileName = fileNamePrefix + FileName;
            //ex.FileName = ex.FileName.Replace(" ", "_"); //Spaces changed to Underscore to suit FireFox/etc.
            ex.Landscape = Landscape;
        }
        public static void Settings(UserControl ucExportButtons, String @FileName, Boolean? Landscape, Boolean PreserveGroupRowStates)
        {
            ASPxGridViewExporter ex = (ASPxGridViewExporter)ucExportButtons.FindControl(ucGridExporter);
            ex.FileName = fileNamePrefix + FileName;
            //ex.FileName = ex.FileName.Replace(" ", "_"); //Spaces changed to Underscore to suit FireFox/etc.
            if (Landscape != null) ex.Landscape = Convert.ToBoolean(Landscape);
            ex.PreserveGroupRowStates = PreserveGroupRowStates;
        }
    }

    public class Questionnaire
    {
        public class ActionLog
        {
            public static bool Add(int ActionId, int QuestionnaireId, int UserId, string FirstName, string LastName, int RoleId, int CompanyId, string CompanyName, string Comments, string OldAssigned, string NewAssigned, string NewAssigned2, string NewAssigned3, string NewAssigned4)
            {
                bool ok = false;
                try
                {
                    HttpServerUtility hsu = HttpContext.Current.Server;
                    //Modified by Ashley Goldstraw 9/9/2015 as the Kaizen services kept locking the QuestionnaireActionLog table
                    QuestionnaireActionLogService aclService = new QuestionnaireActionLogService();
                    QuestionnaireActionLog acl = new QuestionnaireActionLog();
                    //repo.IQuestionnaireActionLogService aclService = ALCOA.CSMS.Website.Global.GetInstance<repo.IQuestionnaireActionLogService>();
                    //model.QuestionnaireActionLog acl = new model.QuestionnaireActionLog();
                    
                    acl.QuestionnaireId = QuestionnaireId;
                    acl.QuestionnaireActionId = ActionId;
                    acl.CreatedDate = DateTime.Now;
                    acl.CreatedByUserId = UserId;
                    acl.CreatedByCompanyId = CompanyId;
                    acl.CreatedByRoleId = RoleId;

                    string PrivledgedRole = "-";
                    if (UserId != 0)
                    {
                        switch (RoleId)
                        {
                            case (int)RoleList.Administrator:
                                PrivledgedRole = "Admin";
                                break;
                            case (int)RoleList.Reader:
                                if (CompanyId == 0) //alcoa
                                {
                                    if (Helper.General.isEhsConsultant(UserId)) PrivledgedRole = "H&S Assessor";
                                    if (Helper.General.isProcurementUser(UserId))
                                    {
                                        if (PrivledgedRole == "H&S Assessor")
                                        {
                                            PrivledgedRole = "H&S Assessor, Procurement";
                                        }
                                        else
                                        {
                                            PrivledgedRole = "Procurement";
                                        }
                                    }
                                }
                                break;
                            default:
                                if (CompanyId != 0) //alcoa
                                {
                                    if (RoleId == (int)RoleList.Contractor || RoleId == (int)RoleList.PreQual)
                                    {
                                        PrivledgedRole = "Contractor";
                                    }
                                }
                                break;
                        }
                    }
                    else
                    {
                        PrivledgedRole = "Service Account";
                    }


                    if (!String.IsNullOrEmpty(PrivledgedRole)) acl.CreatedByPrivledgedRole = PrivledgedRole;

                    QuestionnaireActionService acService = new QuestionnaireActionService();
                    QuestionnaireAction ac = acService.GetByQuestionnaireActionId(ActionId);

                    string HistoryLog = ac.HistoryLogFormat;
                    if (HistoryLog.Contains("[CreatedByUser]"))
                    {
                        FirstName = hsu.HtmlEncode(FirstName);
                        LastName = hsu.HtmlEncode(LastName);
                        HistoryLog = HistoryLog.Replace("[CreatedByUser]", FirstName + " " + LastName);
                    }
                    if (HistoryLog.Contains("[UserPrivledgedRole_UserCompany]"))
                    {
                        PrivledgedRole = hsu.HtmlEncode(PrivledgedRole);
                        CompanyName = hsu.HtmlEncode(CompanyName);
                        HistoryLog = HistoryLog.Replace("[UserPrivledgedRole_UserCompany]", PrivledgedRole + " - " + CompanyName);
                    }
                    if (HistoryLog.Contains("[CreatedDate]"))
                    {
                        HistoryLog = HistoryLog.Replace("[CreatedDate]", acl.CreatedDate.ToString());
                    }

                    if (HistoryLog.Contains("[Comments]"))
                    {
                        if (String.IsNullOrEmpty(Comments)) Comments = "(n/a)";
                        Comments = hsu.HtmlEncode(Comments);
                        HistoryLog = HistoryLog.Replace("[Comments]", Comments);
                    }

                    if (!String.IsNullOrEmpty(OldAssigned))
                    {
                        if (HistoryLog.Contains("[OldAssigned]"))
                        {
                            OldAssigned = hsu.HtmlEncode(OldAssigned);
                            HistoryLog = HistoryLog.Replace("[OldAssigned]", OldAssigned);
                        }
                    }
                    if (!String.IsNullOrEmpty(NewAssigned))
                    {
                        if (HistoryLog.Contains("[NewAssigned]"))
                        {
                            NewAssigned = hsu.HtmlEncode(NewAssigned);
                            HistoryLog = HistoryLog.Replace("[NewAssigned]", NewAssigned);
                        }
                    }
                    if (!String.IsNullOrEmpty(NewAssigned2))
                    {
                        if (HistoryLog.Contains("[NewAssigned2]"))
                        {
                            NewAssigned2 = hsu.HtmlEncode(NewAssigned2);
                            HistoryLog = HistoryLog.Replace("[NewAssigned2]", NewAssigned2);
                        }
                    }
                    if (!String.IsNullOrEmpty(NewAssigned3))
                    {
                        if (HistoryLog.Contains("[NewAssigned3]"))
                        {
                            NewAssigned3 = hsu.HtmlEncode(NewAssigned3);
                            HistoryLog = HistoryLog.Replace("[NewAssigned3]", NewAssigned3);
                        }
                    }
                    if (!String.IsNullOrEmpty(NewAssigned4))
                    {
                        if (HistoryLog.Contains("[NewAssigned4]"))
                        {
                            NewAssigned4 = hsu.HtmlEncode(NewAssigned4);
                            HistoryLog = HistoryLog.Replace("[NewAssigned4]", NewAssigned4);
                        }
                    }

                    acl.HistoryLog = HistoryLog;

                    try
                    {
                        aclService.Save(acl); //commented out by AG
                        //aclService.Insert(acl);
                        //ok = true;  //Update by AG
                    }
                    catch (Exception ex)
                    {
                        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                    }
                    if (acl.EntityState == EntityState.Added) ok = true;
                    
                }
                catch (Exception ex)
                {
                    Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                }
                return ok;
            }
        }

        

       /// <summary>
        /// DT220 - add a new Company Note
        /// By TCS?? and Cindi Thornton (15/9/2015)
        /// NOTE: CompanyId is in the db table as CreatedByCompanyId, this is the Company Id that the note is about
       /// </summary>
        public class CompanyNoteEntry
        {
            public static bool Add(int ActionId, int UserId, string FirstName, string LastName, int RoleId, int CompanyId, string CompanyName, string Comments)
            {
                bool ok = false;
                try
                {
                    if (ActionId >= (int)QuestionnaireActionList.CompanyNoteAdded)
                    {
                        HttpServerUtility hsu = HttpContext.Current.Server;

                        //DT220 Cindi Thornton 15/9/2015 - Changed to use new Repo instead of Kaizen
                        //repo.ICompanyNoteService cnService = ALCOA.CSMS.Website.Global.GetInstance<repo.ICompanyNoteService>();
                        //model.CompanyNote cNote = new model.CompanyNote();
                        CompanyNoteService cnService = new CompanyNoteService();
                        CompanyNote cNote = new CompanyNote();

                        cNote.CreatedDate = DateTime.Now;
                        cNote.CreatedByUserId = UserId;
                        cNote.CreatedByCompanyId = CompanyId;
                        cNote.CreatedByRoleId = RoleId;

                        string PrivledgedRole = null;
                        string Visibility = null;
                        if (UserId != 0)
                        {
                            switch (RoleId)
                            {
                                case (int)RoleList.Administrator:
                                    PrivledgedRole = "Admin";
                                    Visibility = "All";
                                    break;
                                case (int)RoleList.Reader:
                                    //if (CompanyId == 0) //alcoa -- Cindi Thornton 15/9/2015 - using the Company that the note is for here, whether the tab is available is part of IsEligibleToAddCompanyNote
                                    //{
                                        if (Helper.General.isEhsConsultant(UserId))
                                        {
                                            if (Helper.General.isProcurementUser(UserId))
                                                PrivledgedRole = "H&S Assessor, Procurement";
                                            else
                                                PrivledgedRole = "H&S Assessor";
                                            Visibility = "H&S";
                                        }
                                        else if (Helper.General.isProcurementUser(UserId))
                                        {
                                            PrivledgedRole = "Procurement";
                                            Visibility = "Procurement";
                                        }
                                        //} Cindi Thornton 15/9/2015
                                    break;
                                //default:
                                //    if (CompanyId != 0) //alcoa
                                //    {
                                //        if (RoleId == (int)RoleList.Contractor || RoleId == (int)RoleList.PreQual)
                                //        {
                                //            PrivledgedRole = "Contractor";
                                //        }
                                //    }
                                //    break;
                            }
                        }
                        else
                        {
                            //PrivledgedRole = "Service Account";
                        }


                        if (!String.IsNullOrEmpty(PrivledgedRole) && !String.IsNullOrEmpty(Visibility))
                        {
                            cNote.CreatedByPrivledgedRole = PrivledgedRole;
                            cNote.Visibility = Visibility;
                        }
                        else
                        {
                            return false;
                        }

                        QuestionnaireActionService acService = new QuestionnaireActionService();
                        QuestionnaireAction ac = acService.GetByQuestionnaireActionId(ActionId);

                        string HistoryLog = ac.HistoryLogFormat;
                        if (HistoryLog.Contains("[CreatedByUser]"))
                        {
                            FirstName = hsu.HtmlEncode(FirstName);
                            LastName = hsu.HtmlEncode(LastName);
                            HistoryLog = HistoryLog.Replace("[CreatedByUser]", FirstName + " " + LastName);
                        }
                        if (HistoryLog.Contains("[UserPrivledgedRole_UserCompany]"))
                        {
                            PrivledgedRole = hsu.HtmlEncode(PrivledgedRole);
                            CompanyName = hsu.HtmlEncode(CompanyName);
                            HistoryLog = HistoryLog.Replace("[UserPrivledgedRole_UserCompany]", PrivledgedRole + " - " + CompanyName);
                        }
                        if (HistoryLog.Contains("[CreatedDate]"))
                        {
                            HistoryLog = HistoryLog.Replace("[CreatedDate]", cNote.CreatedDate.ToString());
                        }

                        if (HistoryLog.Contains("[Comments]"))
                        {
                            if (String.IsNullOrEmpty(Comments)) Comments = "(n/a)";
                            Comments = hsu.HtmlEncode(Comments);
                            HistoryLog = HistoryLog.Replace("[Comments]", Comments);
                        }

                        cNote.ActionNote = HistoryLog;

                        try
                        {
                            //DT220 - changed to new repo.
                            cnService.Save(cNote);
                            //cnService.Insert(cNote);
                            ok = true;
                        }
                        catch (Exception ex)
                        {
                            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                        }
                        if (cNote.EntityState == EntityState.Added) ok = true; //DT220
                    }
                }
                catch (Exception ex)
                {
                    Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                }
                return ok;
            }


            public static NoteAccessList IsEligibleToAddCompanyNote(int UserId, int RoleId, int CompanyId)
            {
                try
                {
                    if (UserId != 0)
                    {
                        switch (RoleId)
                        {
                            case (int)RoleList.Administrator:
                                return NoteAccessList.Administrator;
                            case (int)RoleList.Reader:
                                if (CompanyId == 0) //alcoa
                                {
                                    if (Helper.General.isEhsConsultant(UserId))
                                    {
                                        return NoteAccessList.Ehs;
                                    }
                                    else if (Helper.General.isProcurementUser(UserId))
                                    {
                                        return NoteAccessList.Procurement;
                                    }
                                }
                                break;
                        }
                    }
                }
                catch (Exception ex)
                {
                    Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                }
                return NoteAccessList.None;
            }
        }

        public static int getRiskLevel(string riskLevel)
        {
            int i = 0;
            if (riskLevel.Contains("Low")) i = 1;
            if (riskLevel.Contains("Medium")) i = 2;
            if (riskLevel.Contains("High")) i = 3;
            if (riskLevel.Contains("Forced High")) i = 4;

            return i;
        }

        public class General
        {
            public static bool IsValidForSubContractors(int CompanyId)
            {
                bool isValid = false;

                QuestionnaireService qService = new QuestionnaireService();
                DataSet ds = qService.ListIfValidAndAllowedSubContractors_ByCompanyId(CompanyId);

                if (ds != null)
                {
                    if (ds.Tables.Count == 1)
                    {
                        if (ds.Tables[0].Rows.Count == 1)
                        {
                            //Is Valid
                            isValid = true;
                        }
                    }
                }

                return isValid;
            }

            public static void UpdatePresentlyWithStatus(int QuestionnaireId)
            {
                QuestionnaireService qService = new QuestionnaireService();
                KaiZen.CSMS.Entities.Questionnaire q = qService.GetByQuestionnaireId(QuestionnaireId);

                CompaniesService cService = new CompaniesService();
                Companies c = cService.GetByCompanyId(q.CompanyId);

                UsersService uService = new UsersService();
                TList<Users> uList = uService.GetByCompanyId(c.CompanyId);

                UpdatePresentlyWithStatus(q, c, uList, true);
            }

            public static void UpdatePresentlyWithStatusUpdatePresentlyWithStatus(KaiZen.CSMS.Entities.Questionnaire q, KaiZen.CSMS.Entities.Companies c, TList<KaiZen.CSMS.Entities.Users> uList, bool useCurrentDateTime)
            {
                UpdatePresentlyWithStatus(q, c, uList, useCurrentDateTime);
            }


            private static string UpdatePresentlyWithStatus(KaiZen.CSMS.Entities.Questionnaire q, KaiZen.CSMS.Entities.Companies c, TList<KaiZen.CSMS.Entities.Users> uList, bool useCurrentDateTime)
            {
                string companyStatus = "";
                string whatChanged = "Nothing";

                switch (c.CompanyStatusId)
                {
                    //In Progress
                    case (int)CompanyStatusList.Being_Assessed:
                        companyStatus = "inprogress";
                        break;
                    case (int)CompanyStatusList.Requal_Incomplete:
                        companyStatus = "inprogress";
                        break;

                    //Status (Inactive)
                    case (int)CompanyStatusList.InActive_No_Requal_Required:
                        companyStatus = "inactive";
                        break;
                    case (int)CompanyStatusList.InActive_SQ_Incomplete:
                        companyStatus = "inactive";
                        break;

                    //Status
                    case (int)CompanyStatusList.InActive: //sq complete & not recommended
                        companyStatus = "sqcomplete";
                        break;
                    case (int)CompanyStatusList.Acceptable:
                        companyStatus = "sqcomplete";
                        break;
                    case (int)CompanyStatusList.Active_No_PQ_Required:
                        companyStatus = "sqcomplete";
                        break;
                    case (int)CompanyStatusList.SubContractor_Not_Recommended:
                        companyStatus = "sqcomplete";
                        break;

                    //Database
                    case (int)CompanyStatusList.SubContractor:
                        companyStatus = "sqcomplete";
                        break;
                    case (int)CompanyStatusList.Active:
                        companyStatus = "sqcomplete";
                        break;
                    case (int)CompanyStatusList.Active_No_PQ_Required_Restricted_Access_to_Site:
                        companyStatus = "sqcomplete";
                        break;

                    default:
                        break;
                }
                
                switch(companyStatus)
                {
                    case "inprogress":
                        switch (q.Status)
                        {
                            case (int)QuestionnaireStatusList.Incomplete:
                                switch (q.InitialStatus)
                                {
                                    case (int)QuestionnaireStatusList.Incomplete:
                                        if (q.QuestionnairePresentlyWithActionId != (int)QuestionnairePresentlyWithActionList.ProcurementQuestionnaire)
                                        {
                                            q.QuestionnairePresentlyWithActionId = (int)QuestionnairePresentlyWithActionList.ProcurementQuestionnaire;
                                            if (useCurrentDateTime)
                                                q.QuestionnairePresentlyWithSince = DateTime.Now;
                                            else
                                                q.QuestionnairePresentlyWithSince = q.ModifiedDate;
                                            whatChanged = "Procurement Questionnaire Incomplete with Procurement, Presently With Updated.";
                                        }
                                        break;
                                    case (int)QuestionnaireStatusList.BeingAssessed:
                                        if (q.QuestionnairePresentlyWithActionId != (int)QuestionnairePresentlyWithActionList.HSAssessorQuestionnaireProcurement)
                                        {
                                            q.QuestionnairePresentlyWithActionId = (int)QuestionnairePresentlyWithActionList.HSAssessorQuestionnaireProcurement;
                                            if (useCurrentDateTime)
                                                q.QuestionnairePresentlyWithSince = DateTime.Now;
                                            else
                                                q.QuestionnairePresentlyWithSince = q.ModifiedDate;
                                            whatChanged = "Procurement Questionnaire Incomplete with H&S Assessor, Presently With Updated.";
                                        }
                                        break;
                                    case (int)QuestionnaireStatusList.AssessmentComplete:
                                        QuestionnaireMainAssessmentService qmaService = new QuestionnaireMainAssessmentService();
                                        TList<QuestionnaireMainAssessment> qmaTlist_all = qmaService.GetByQuestionnaireId(q.QuestionnaireId);

                                        if (qmaTlist_all != null)
                                        {
                                            if (qmaTlist_all.Count == 22)
                                            {
                                                if (q.QuestionnairePresentlyWithActionId != (int)QuestionnairePresentlyWithActionList.SupplierQuestionnaireIncomplete)
                                                {
                                                    if (useCurrentDateTime)
                                                    {
                                                        q.QuestionnairePresentlyWithSince = DateTime.Now;
                                                    }
                                                    else
                                                    {
                                                        if (q.AssessedDate != null)
                                                        {
                                                            q.QuestionnairePresentlyWithSince = q.AssessedDate;
                                                        }
                                                        else
                                                        {
                                                            if (q.MainAssessmentDate != null)
                                                                q.QuestionnairePresentlyWithSince = q.MainAssessmentDate;
                                                            else
                                                                q.QuestionnairePresentlyWithSince = q.ModifiedDate;
                                                        }
                                                    }
                                                    whatChanged = "Supplier Questionnaire Incomplete with Supplier (Re-Submitting), Presently With Updated.";
                                                }
                                            }
                                            else
                                            {
                                                if (q.QuestionnairePresentlyWithActionId != (int)QuestionnairePresentlyWithActionList.SupplierQuestionnaireSupplier)
                                                {
                                                    if (useCurrentDateTime)
                                                    {
                                                        q.QuestionnairePresentlyWithSince = DateTime.Now;
                                                    }
                                                    else
                                                    {
                                                        if (q.InitialSubmittedDate != null)
                                                            q.QuestionnairePresentlyWithSince = q.InitialSubmittedDate;
                                                        else
                                                            q.QuestionnairePresentlyWithSince = q.InitialModifiedDate;
                                                    }
                                                    whatChanged = "Supplier Questionnaire Incomplete with Supplier, Presently With Updated.";
                                                }
                                            }
                                        }

                                        break;
                                    default:
                                        throw new Exception("Could not get Procurement Questionnaire Status");
                                }
                                break;
                            case (int)QuestionnaireStatusList.BeingAssessed:
                                if (q.QuestionnairePresentlyWithActionId != (int)QuestionnairePresentlyWithActionList.HSAssessorQuestionnaire)
                                {
                                    q.QuestionnairePresentlyWithActionId = (int)QuestionnairePresentlyWithActionList.HSAssessorQuestionnaire;
                                    if (useCurrentDateTime)
                                        q.QuestionnairePresentlyWithSince = DateTime.Now;
                                    else
                                        q.QuestionnairePresentlyWithSince = q.ModifiedDate;

                                    whatChanged = "Safety Questionnaire Being Assessed with H&S Assessor, Presently With Updated.";
                                }
                                break;
                            case (int)QuestionnaireStatusList.AssessmentComplete:
                                if (q.QuestionnairePresentlyWithActionId != (int)QuestionnairePresentlyWithActionList.ProcurementAssignCompanyStatus)
                                {
                                    q.QuestionnairePresentlyWithActionId = (int)QuestionnairePresentlyWithActionList.ProcurementAssignCompanyStatus;

                                    if (useCurrentDateTime)
                                    {
                                        q.QuestionnairePresentlyWithSince = DateTime.Now;
                                    }
                                    else
                                    {
                                        if (q.ApprovedDate != null)
                                            q.QuestionnairePresentlyWithSince = q.ApprovedDate;
                                        else
                                            q.QuestionnairePresentlyWithSince = q.MainAssessmentDate;
                                    }

                                    whatChanged = "Company Status Awaiting Assignment with Procurement, Presently With Updated.";
                                }
                                break;
                            default:
                                throw new Exception("Could not get Questionnaire Status");
                        }
                    break;
                    case "inactive":
                    if (q.QuestionnairePresentlyWithActionId != (int)QuestionnairePresentlyWithActionList.NoActionRequired)
                    {
                        q.QuestionnairePresentlyWithActionId = (int)QuestionnairePresentlyWithActionList.NoActionRequired;

                        if (useCurrentDateTime)
                        {
                            q.QuestionnairePresentlyWithSince = DateTime.Now;
                        }
                        else
                        {
                            if (q.QuestionnairePresentlyWithSince != null)
                                q.QuestionnairePresentlyWithSince = null;
                        }

                        whatChanged = "In-Active (SQ Incomplete / No Requal Required) - No Action Required, Presently With Updated.";
                    }
                        break;
                    case "sqcomplete":
                        //check expiry
                        bool expiringOrExpired = false;
                        DateTime? validTo = null;

                        if (validTo != null)
                        {
                            DateTime dtExpiry = Convert.ToDateTime(validTo);
                            TimeSpan ts = Convert.ToDateTime(dtExpiry) - DateTime.Now;
                            if (ts.Days <= 60)
                            {
                                if (ts.Days > 0)
                                {
                                    //expiring
                                    if (q.QuestionnairePresentlyWithActionId != (int)QuestionnairePresentlyWithActionList.ProcurementExpiring)
                                    {
                                        q.QuestionnairePresentlyWithActionId = (int)QuestionnairePresentlyWithActionList.ProcurementExpiring;

                                        if (validTo != null)
                                        {
                                            DateTime dt = Convert.ToDateTime(validTo);
                                            dt = dt.AddDays(-60);
                                            q.QuestionnairePresentlyWithSince = dt;
                                        }
                                        else
                                        {
                                            q.QuestionnairePresentlyWithSince = null;
                                        }
                                        expiringOrExpired = true;
                                        whatChanged = "Expiring, Presently With Updated.";
                                    }
                                }
                                else
                                {
                                    if (q.QuestionnairePresentlyWithActionId != (int)QuestionnairePresentlyWithActionList.ProcurementExpired)
                                    {
                                        q.QuestionnairePresentlyWithActionId = (int)QuestionnairePresentlyWithActionList.ProcurementExpired;
                                        if (validTo != null)
                                        {
                                            DateTime dt = Convert.ToDateTime(validTo);
                                            dt = dt.AddDays(-60);
                                            q.QuestionnairePresentlyWithSince = dt;
                                        }
                                        else
                                        {
                                            q.QuestionnairePresentlyWithSince = null;
                                        }
                                        expiringOrExpired = true;
                                        whatChanged = "Expired, Presently With Updated.";
                                    }
                                }
                            }
                        }

                        if (!expiringOrExpired)
                        {
                            if (q.QuestionnairePresentlyWithActionId != (int)QuestionnairePresentlyWithActionList.NoActionRequired)
                            {
                                q.QuestionnairePresentlyWithActionId = (int)QuestionnairePresentlyWithActionList.NoActionRequired;
                                q.QuestionnairePresentlyWithSince = null;
                                whatChanged = "No Action Required, Presently With Updated.";
                            }
                        }

                        break;
                    default:
                        throw new Exception("Could not get Company Status");
                }

                if (q.EntityState == EntityState.Changed)
                {
                    QuestionnaireService qService = new QuestionnaireService();
                    qService.Save(q);
                }

                return whatChanged;
            }

            //public static bool IsValid(int CompanyId, bool? AllowedSubContractors)
            //{
            //    bool isvalid = false;

            //    int count = 0;

            //    QuestionnaireReportExpiryService qreService = new QuestionnaireReportExpiryService();
            //    QuestionnaireReportExpiryFilters qreFilters = new QuestionnaireReportExpiryFilters();
            //    qreFilters.Append(QuestionnaireReportExpiryColumn.CompanyId, CompanyId.ToString());

            //    VList<QuestionnaireReportExpiry> qreVlist = qreService.GetPaged(qreFilters.ToString(), null, 0, 100, out count);

            //    //if (qreVlist.Count == 1)
            //    //{
            //    //    if(
            //    //}

            //    return isvalid;
            //}
        }
        public class Procurement
        {
            public static string LoadAnswer(int q, string AnswerId)
            {
                try
                {
                    QuestionnaireInitialResponseService qrs = new QuestionnaireInitialResponseService();
                    QuestionnaireInitialResponse qr = new QuestionnaireInitialResponse();
                    qr = qrs.GetByQuestionnaireIdQuestionId(q, AnswerId);
                    if (qr == null) { return ""; };
                    return qr.AnswerText;
                }
                catch (Exception) { return ""; }
            }
            public static bool SaveAnswer(int q, string AnswerId, string AnswerText, int UserId)
            {
                if (String.IsNullOrEmpty(AnswerText)) { AnswerText = ""; };
                QuestionnaireInitialResponseService qrs = new QuestionnaireInitialResponseService();
                QuestionnaireInitialResponse qr = new QuestionnaireInitialResponse();
                qr = qrs.GetByQuestionnaireIdQuestionId(q, AnswerId);
                if (qr == null) { qr = new QuestionnaireInitialResponse(); qr.QuestionnaireId = q; qr.QuestionId = AnswerId; qr.ModifiedByUserId = UserId; qr.ModifiedDate = DateTime.Now; };
                qr.AnswerText = AnswerText;
                try { qrs.Save(qr); }
                catch (Exception)
                {
                    ErrorSignal.FromCurrentContext();
                    return false;
                };
                return true;
            }


        }
        public class Supplier
        {
            public class Main
            {
                public static bool HasExample(int q, string AnswerId)
                {
                    try
                    {
                        using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["ALCOA_WAO_CSMWP_devConnectionString"].ToString()))
                        {
                            cn.Open();
                            SqlCommand cmd = new SqlCommand("SELECT COUNT(*) FROM dbo.QuestionnaireMainAttachment INNER JOIN dbo.QuestionnaireMainAnswer ON dbo.QuestionnaireMainAttachment.AnswerId = dbo.QuestionnaireMainAnswer.AnswerId WHERE QuestionnaireId = " + q.ToString() + " AND dbo.QuestionnaireMainAnswer.QuestionNo + dbo.QuestionnaireMainAnswer.AnswerNo = '" + AnswerId + "'", cn);
                            SqlDataReader rdr = cmd.ExecuteReader();
                            rdr.Read();
                            if (rdr[0].ToString() != "0")
                            {
                                return true;
                            }
                            else
                            {
                                return false;
                            }
                        }
                    }
                    catch (Exception)
                    {
                        return false;
                    }
                }
                public static int GetAnswerId(string q, string a)
                {
                    QuestionnaireMainAnswerService qas = new QuestionnaireMainAnswerService();
                    QuestionnaireMainAnswer _q = qas.GetByQuestionNoAnswerNo(q, a);
                    return _q.AnswerId;
                }
                public static string LoadAnswer(int q, int AnswerId)
                {
                    QuestionnaireMainResponseService qrs = new QuestionnaireMainResponseService();
                    QuestionnaireMainResponse qr = qrs.GetByQuestionnaireIdAnswerId(q, AnswerId);
                    if (qr == null) { return ""; };
                    return qr.AnswerText;
                }
                public static bool SaveAnswer(int q, int AnswerId, string AnswerText, int UserId)
                {
                    //DT 3290-SQ data missing change.
                    //Checking/proceeding save if anser text is not NULL.
                    //if (String.IsNullOrEmpty(AnswerText)==false)  //This if statment removed by Ashley Goldstraw to save null or empty values back to the DB.  DT354. 22/12/2015
                    //{
                        if (String.IsNullOrEmpty(AnswerText)) { AnswerText = ""; };
                        QuestionnaireMainResponseService qrs = new QuestionnaireMainResponseService();
                        QuestionnaireMainResponse qr = new QuestionnaireMainResponse();
                        qr = qrs.GetByQuestionnaireIdAnswerId(q, AnswerId);
                        if (qr == null) { qr = new QuestionnaireMainResponse(); qr.QuestionnaireId = q; qr.AnswerId = AnswerId; qr.ModifiedByUserId = UserId; qr.ModifiedDate = DateTime.Now; };
                        qr.AnswerText = AnswerText;
                        try { qrs.Save(qr); }
                        catch (Exception) { ErrorSignal.FromCurrentContext(); return false; };
                    //}
                    return true;
                }

                public static bool SaveAssessment(int QuestionnaireId, string QuestionNo, object AssessorApproval, string AssessorComments, int ModifiedByUserId)
                {
                    try
                    {
                        QuestionnaireMainAssessmentService qmaService = new QuestionnaireMainAssessmentService();
                        QuestionnaireMainAssessment qma = qmaService.GetByQuestionnaireIdQuestionNo(QuestionnaireId, QuestionNo);
                        if (qma == null)
                        {
                            qma = new QuestionnaireMainAssessment();
                            qma.QuestionnaireId = QuestionnaireId;
                            qma.QuestionNo = QuestionNo;
                        }
                        qma.AssessorApproval = null;
                        try
                        {
                            if ((int)AssessorApproval == 1) qma.AssessorApproval = true;
                            if ((int)AssessorApproval == 0) qma.AssessorApproval = false;
                        }
                        catch (Exception) { } //lol.

                        qma.AssessorComment = AssessorComments;
                        qma.ModifiedByUserId = ModifiedByUserId;
                        qma.ModifiedDate = DateTime.Now;
                        qmaService.Save(qma);
                    }
                    catch (Exception) { ErrorSignal.FromCurrentContext(); return false; };
                    return true;
                }
            }

            public class Verification
            {
                public static bool SaveAnswer(int q, int SectionId, int QuestionId, string AnswerText, byte[] AnswerText2, int ModifiedByUserId)
                {
                    if (String.IsNullOrEmpty(AnswerText)) { AnswerText = ""; };
                    QuestionnaireVerificationResponseService qrs = new QuestionnaireVerificationResponseService();
                    QuestionnaireVerificationResponse qr = new QuestionnaireVerificationResponse();
                    qr = qrs.GetByQuestionnaireIdSectionIdQuestionId(q, SectionId, QuestionId);
                    if (qr == null)
                    {
                        qr = new QuestionnaireVerificationResponse(); qr.QuestionnaireId = q; qr.SectionId = SectionId; qr.QuestionId = QuestionId;
                        qr.AdditionalEvidence = AnswerText2; qr.ModifiedByUserId = ModifiedByUserId; qr.ModifiedDate = DateTime.Now;
                    };
                    qr.AnswerText = AnswerText;
                    qr.AdditionalEvidence = AnswerText2;
                    try
                    {
                        if (qr.EntityState != EntityState.Unchanged)
                        {
                            qrs.Save(qr);
                        }
                    }
                    catch (Exception) { return false; };
                    return true;
                }

                public static string GetVal(object o)
                {
                    try
                    {
                        return o.ToString();
                    }
                    catch (Exception)
                    {
                        return "";
                    }
                }
                public static byte[] GetByte(string s)
                {
                    try
                    {
                        if (String.IsNullOrEmpty(s)) { s = ""; };
                        ASCIIEncoding encoding = new ASCIIEncoding();
                        return (byte[])encoding.GetBytes(s);
                    }
                    catch (Exception)
                    {
                        return null;
                    }
                }
                public static string LoadAnswerText(int q, int SectionId, int QuestionId)
                {
                    try
                    {
                        QuestionnaireVerificationResponseService qrs = new QuestionnaireVerificationResponseService();
                        QuestionnaireVerificationResponse qr = new QuestionnaireVerificationResponse();
                        qr = qrs.GetByQuestionnaireIdSectionIdQuestionId(q, SectionId, QuestionId);
                        if (qr == null) { return ""; };
                        return qr.AnswerText;
                    }
                    catch (Exception) { return null; }
                }
                public static string LoadAnswerByte(int q, int SectionId, int QuestionId)
                {
                    try
                    {
                        QuestionnaireVerificationResponseService qrs = new QuestionnaireVerificationResponseService();
                        QuestionnaireVerificationResponse qr = new QuestionnaireVerificationResponse();
                        qr = qrs.GetByQuestionnaireIdSectionIdQuestionId(q, SectionId, QuestionId);
                        if (qr == null) { return ""; };
                        return Encoding.ASCII.GetString(qr.AdditionalEvidence);
                    }
                    catch (Exception) { return null; }
                }


                public static void LoadAttachmentFromSupplier(int q, string QuestionId, string AnswerId, Label Text, ASPxHyperLink Link)
                {
                    try
                    {
                        //Label Text = (Label)this.FindControl("qAnswer" + QuestionId + AnswerId + "Text");
                        //ASPxHyperLink Link = (ASPxHyperLink)this.FindControl("qAnswer" + QuestionId + AnswerId + "Link");

                        QuestionnaireMainAttachmentService qas = new QuestionnaireMainAttachmentService();
                        QuestionnaireMainAttachment qa = qas.GetByQuestionnaireIdAnswerId(q, Helper.Questionnaire.Supplier.Main.GetAnswerId(QuestionId, AnswerId));
                        if (qa != null)
                        {
                            Text.Visible = true;
                            Link.Text = qa.FileName;
                            Link.NavigateUrl = String.Format("~/Common/GetFile.aspx{0}",
                                                QueryStringModule.Encrypt(String.Format("Type=SQ&SubType=Supplier&File={0}&Seed={1}", qa.AttachmentId, StringUtilities.RandomKey())));
                            Link.Target = "_blank";
                        }
                    }
                    catch (Exception ex)
                    {
                        // Elmah.ErrorSignal.FromContext(Context).Raise(ex);
                        //Response.Write(ex.Message.ToString());
                    }
                }

                public static bool SaveAttachment(int q, int SectionId, int QuestionId, ASPxUploadControl fu, int ModifiedByUserId)
                {
                    if (fu.UploadedFiles != null && fu.UploadedFiles.Length > 0)
                    {
                        if (!String.IsNullOrEmpty(fu.UploadedFiles[0].FileName) && fu.UploadedFiles[0].IsValid)
                        {
                            Configuration configuration = new Configuration();
                            string tempPath = @configuration.GetValue(ConfigList.TempDir).ToString();
                            //tempPath = "C:\\dev\\";
                            string fullPath = tempPath + fu.UploadedFiles[0].FileName;
                            fu.UploadedFiles[0].SaveAs(fullPath);
                            FileStream fs = new FileStream(fullPath, FileMode.Open,
                                                                     FileAccess.Read,
                                                                     FileShare.ReadWrite);

                            QuestionnaireVerificationAttachmentService qas = new QuestionnaireVerificationAttachmentService();
                            QuestionnaireVerificationAttachment qa = new QuestionnaireVerificationAttachment();
                            qa = qas.GetByQuestionnaireIdSectionIdQuestionId(q, SectionId, QuestionId);
                            if (qa == null) { qa = new QuestionnaireVerificationAttachment(); qa.QuestionnaireId = q; qa.SectionId = SectionId; qa.QuestionId = QuestionId; };
                            qa.FileName = fu.UploadedFiles[0].FileName;
                            qa.Content = FileUtilities.Read.ReadFully(fs, 51200);
                            qa.FileHash = FileUtilities.Calculate.HashFile(qa.Content);
                            qa.ContentLength = Convert.ToInt32(fs.Length);


                            bool fail = false;
                            try
                            {
                                if (qa.EntityState != EntityState.Unchanged)
                                {
                                    qa.ModifiedByUserId = ModifiedByUserId;
                                    qa.ModifiedDate = DateTime.Now;
                                    qas.Save(qa);
                                }
                            }
                            catch (Exception) { fail = true; };

                            fs.Close();
                            fu.Dispose();
                            File.Delete(fullPath);

                            if (fail != true) { return true; } else { return false; };
                        }
                        else
                        {
                            return true;
                        }
                    }
                    else
                    {
                        return true;
                    }
                }
                public static void LoadAttachment(int q, int SectionId, int QuestionId, Label Text, ASPxHyperLink Link)
                {
                    try
                    {
                        QuestionnaireVerificationAttachmentService qas = new QuestionnaireVerificationAttachmentService();
                        QuestionnaireVerificationAttachment qa = qas.GetByQuestionnaireIdSectionIdQuestionId(q, SectionId, QuestionId);
                        if (qa != null)
                        {
                            Text.Visible = true;
                            Link.Text = qa.FileName;
                            Link.NavigateUrl = String.Format("~/Common/GetFile.aspx{0}",
                                                QueryStringModule.Encrypt(String.Format("Type=SQ&SubType=Verification&File={0}&Seed={1}", qa.AttachmentId, StringUtilities.RandomKey())));
                            Link.Target = "_blank";
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }

                public static bool SaveAssessment(int QuestionnaireId, int SectionId, int QuestionId, object AssessorApproval, string AssessorComments, int ModifiedByUserId)
                {
                    try
                    {
                        QuestionnaireVerificationAssessmentService qvaService = new QuestionnaireVerificationAssessmentService();
                        QuestionnaireVerificationAssessment qva = qvaService.GetByQuestionnaireIdSectionIdQuestionId(QuestionnaireId, SectionId, QuestionId);
                        if (qva == null)
                        {
                            qva = new QuestionnaireVerificationAssessment();
                            qva.QuestionnaireId = QuestionnaireId;
                            qva.SectionId = SectionId;
                            qva.QuestionId = QuestionId;
                        }

                        qva.AssessorApproval = null;
                        try
                        {
                            if ((int)AssessorApproval == 1) qva.AssessorApproval = true;
                            if ((int)AssessorApproval == 0) qva.AssessorApproval = false;
                        }
                        catch (Exception) { ErrorSignal.FromCurrentContext(); }

                        qva.AssessorComment = AssessorComments;
                        if (qva.EntityState != EntityState.Unchanged)
                        {
                            qva.ModifiedByUserId = ModifiedByUserId;
                            qva.ModifiedDate = DateTime.Now;
                            qvaService.Save(qva);
                        }
                    }
                    catch (Exception) { ErrorSignal.FromCurrentContext(); return false; };
                    return true;
                }
            }
        }

    }

    public class SaftyPlanSeResponseAttachment
    {
        public class ActionLog
        {
            public static bool SaveSaftyPlanSEResponseAttachment(int ResponseId, int QuestionId, ASPxUploadControl fu, int ModifiedByUserId)
            {
                bool fail = false;
                if (fu.UploadedFiles != null && fu.UploadedFiles.Length > 0)
                {
                    if (!String.IsNullOrEmpty(fu.UploadedFiles[0].FileName) && fu.UploadedFiles[0].IsValid)
                    {
                        Configuration configuration = new Configuration();
                        //Bishwajit Sahoo
                        string tempPath = @configuration.GetValue(ConfigList.TempDir).ToString();
                        //string tempPath = @"C:\Bishwajit Sahoo\";
                        //tempPath = "C:\\dev\\";
                        for (int i = 0; i < fu.UploadedFiles.Length; i++)
                        {
                            string fullPath = tempPath + fu.UploadedFiles[i].FileName;
                            //string fullPath =@"C:\Bishwajit\File3.pdf";

                            fu.UploadedFiles[i].SaveAs(fullPath);
                            FileStream fs = new FileStream(fullPath, FileMode.Open,
                                                                     FileAccess.Read,
                                                                     FileShare.ReadWrite);

                            fail = false;

                            SafetyPlansSEResponsesAttachmentService qas = new SafetyPlansSEResponsesAttachmentService();
                            SafetyPlansSEResponsesAttachment qa = new SafetyPlansSEResponsesAttachment();
                            qa.QuestionId = QuestionId;
                            qa.ResponseId = ResponseId;
                            qa.FileName = fu.UploadedFiles[i].FileName;

                            qa.Content = FileUtilities.Read.ReadFully(fs, 51200);
                            qa.FileHash = FileUtilities.Calculate.HashFile(qa.Content);
                            qa.ContentLength = Convert.ToInt32(fs.Length);

                            try
                            {
                                if (qa.EntityState != EntityState.Unchanged)
                                {
                                    qa.ModifiedByUserId = ModifiedByUserId;
                                    qa.ModifiedDate = DateTime.Now;
                                    qas.Save(qa);
                                    fail = false;
                                }
                            }
                            catch (Exception) { fail = true; };

                            fs.Close();
                            File.Delete(fullPath);

                        }
                        return fail;
                    }


                    else
                    {
                        return true;
                    }
                }
                else
                {
                    return true;
                }
            }


            public static bool SaveSafetyPlanSeResAttachmentFromVerification(int qUid, int sectionId, int questionId, int modifiedBy, int responseId, int resQuestionId)
            {
                bool isPass = false;
                try
                {
                    QuestionnaireVerificationAttachmentService qas = new QuestionnaireVerificationAttachmentService();
                    QuestionnaireVerificationAttachment qa;

                    SafetyPlansSEResponsesAttachmentService seResSer = new SafetyPlansSEResponsesAttachmentService();
                    SafetyPlansSEResponsesAttachment seSer = new SafetyPlansSEResponsesAttachment();

                    qa = qas.GetByQuestionnaireIdSectionIdQuestionId(qUid, sectionId, questionId);
                    seSer.QuestionId = resQuestionId;
                    seSer.ResponseId = Convert.ToInt32(responseId);
                    seSer.FileName = qa.FileName;
                    seSer.FileHash = qa.FileHash;
                    seSer.Content = qa.Content;
                    seSer.ContentLength = qa.ContentLength;

                    try
                    {
                        if (seSer.EntityState != EntityState.Unchanged)
                        {
                            seSer.ModifiedByUserId = modifiedBy;
                            seSer.ModifiedDate = DateTime.Now;
                            seResSer.Save(seSer);
                            isPass = true;
                        }
                    }
                    catch (Exception) { isPass = false; };
                }
                catch (Exception)
                {

                    isPass = false;
                }
                return isPass;
            }



            public static bool SaveSafetyPlanSeResAttachmentFromSupplier(int qUid, string questionId, string answerId, int modifiedBy, int responseId, int resQuestionId)
            {
                bool isPass = false;
                try
                {

                    SafetyPlansSEResponsesAttachmentService seResSer = new SafetyPlansSEResponsesAttachmentService();
                    SafetyPlansSEResponsesAttachment seSer = new SafetyPlansSEResponsesAttachment();

                    QuestionnaireMainAttachmentService qas = new QuestionnaireMainAttachmentService();
                    QuestionnaireMainAttachment qa = qas.GetByQuestionnaireIdAnswerId(qUid, Helper.Questionnaire.Supplier.Main.GetAnswerId(questionId, answerId));

                    seSer.QuestionId = resQuestionId;
                    seSer.ResponseId = Convert.ToInt32(responseId);
                    seSer.FileName = qa.FileName;
                    seSer.FileHash = qa.FileHash;
                    seSer.Content = qa.Content;
                    seSer.ContentLength = qa.ContentLength;

                    try
                    {
                        if (seSer.EntityState != EntityState.Unchanged)
                        {
                            seSer.ModifiedByUserId = modifiedBy;
                            seSer.ModifiedDate = DateTime.Now;
                            seResSer.Save(seSer);
                            isPass = true;
                        }
                    }
                    catch (Exception) { isPass = false; };
                }
                catch (Exception)
                {

                    isPass = false;
                }
                return isPass;
            }

        }
    }



    public class General
    {
        public static bool isEbiUser(int UserId)
        {
            try
            {
                int count;
                UsersEbiFilters queryUsersEbi = new UsersEbiFilters();
                queryUsersEbi.Append(UsersEbiColumn.UserId, UserId.ToString());
                TList<UsersEbi> UsersEbiList = DataRepository.UsersEbiProvider.GetPaged(queryUsersEbi.ToString(), null, 0, 100, out count);
                if (count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static bool isEhsConsultant(int UserId)
        {
            try
            {
                int count;
                EhsConsultantFilters queryEhsConsultant = new EhsConsultantFilters();
                queryEhsConsultant.Append(EhsConsultantColumn.UserId, UserId.ToString());
                TList<EhsConsultant> EhsConsultantList = DataRepository.EhsConsultantProvider.GetPaged(queryEhsConsultant.ToString(), null, 0, 100, out count);
                if (count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static bool isProcurementUser(int UserId)
        {
            try
            {
                int count;
                UsersProcurementFilters queryUsersProcurement = new UsersProcurementFilters();
                queryUsersProcurement.Append(UsersProcurementColumn.UserId, UserId.ToString());
                queryUsersProcurement.Append(UsersProcurementColumn.Enabled, "True");
                queryUsersProcurement.Append(UsersProcurementColumn.Enabled, "1");
                TList<UsersProcurement> UsersProcurementList = DataRepository.UsersProcurementProvider.GetPaged(queryUsersProcurement.ToString(), null, 0, 100, out count);
                if (count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static bool isPrivilegedUser(int UserId)
        {
            UsersPrivilegedService upService = new UsersPrivilegedService();
            UsersPrivileged up = upService.GetByUserId(UserId);
            if (up != null) return true;
            else return false;
        }
        public static bool isPrivileged2User(int UserId)
        {
            UsersPrivileged2Service up2Service = new UsersPrivileged2Service();
            UsersPrivileged2 up2 = up2Service.GetByUserId(UserId);
            if (up2 != null) return true;
            else return false;
        }
        public static bool isSQReadAccessUser(int UserId)
        {
            UsersPrivileged3Service up3Service = new UsersPrivileged3Service();
            UsersPrivileged3 up3 = up3Service.GetByUserId(UserId);
            if (up3 != null) return true;
            else return false;
        }
        public static bool hasActivePrivilege(int UserId, int PrivilegeId)
        {
            bool hasperm = false;
            UserPrivilegeService upService = new UserPrivilegeService();
            UserPrivilege up = upService.GetByUserIdPrivilegeId(UserId, PrivilegeId);

            if (up != null)
            {
                if (up.Active) hasperm = true;
            }

            return hasperm;
        }

        public static bool isRegionalManager(int UserId)
        {
            UsersService uService = new UsersService();
            Users u = uService.GetByUserId(UserId);

            Configuration config = new Configuration();
            if(u.Email.ToUpper() == (config.GetValue(ConfigList.ProcurementRegionalManager)).ToUpper())
                return true;
            else
                return false;
        }

        public static int? getEhsConsultantIdByCompany(int CompanyId)
        {
            try
            {
                CompaniesService companiesService = new CompaniesService();
                Companies c = companiesService.GetByCompanyId(CompanyId);

                EhsConsultantService ehsconsultantService = new EhsConsultantService();
                int ehsConsultantId = -1;
                ehsConsultantId = Convert.ToInt32(c.EhsConsultantId);

                if (ehsConsultantId > 0)
                {
                    return ehsConsultantId;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception)
            {
                return null;
            }
        }
        public static string getEhsConsultantNameByCompany(int CompanyId)
        {
            const string na = "[Not Designated]";
            try
            {
                CompaniesService companiesService = new CompaniesService();
                Companies c = companiesService.GetByCompanyId(CompanyId);

                EhsConsultantService ehsconsultantService = new EhsConsultantService();
                int ehsConsultantId = -1;
                ehsConsultantId = Convert.ToInt32(c.EhsConsultantId);
                EhsConsultant ehsconsultant = ehsconsultantService.GetByEhsConsultantId(ehsConsultantId);

                UsersService usersService = new UsersService();
                Users u = usersService.GetByUserId(ehsconsultant.UserId);

                String Name = String.Format("{0}, {1}", u.LastName, u.FirstName);
                if (Name != ", ")
                {
                    return Name;
                }
                else
                {
                    return na;
                }
            }
            catch (Exception)
            {
                return na;
            }
        }
        public static string getEhsConsultantEmailByCompany(int CompanyId, bool useDefaultEmail)
        {
            Configuration configuration = new Configuration();
            string defaultEmail = "";
            if (useDefaultEmail) configuration.GetValue(ConfigList.DefaultEhsConsultant_WAO);
            //TODO: Vic Ops
            try
            {
                CompaniesService companiesService = new CompaniesService();
                Companies c = companiesService.GetByCompanyId(CompanyId);

                EhsConsultantService ehsconsultantService = new EhsConsultantService();
                int ehsConsultantId = -1;
                ehsConsultantId = Convert.ToInt32(c.EhsConsultantId);
                EhsConsultant ehsconsultant = ehsconsultantService.GetByEhsConsultantId(ehsConsultantId);

                UsersService usersService = new UsersService();
                Users u = usersService.GetByUserId(ehsconsultant.UserId);
                if (!String.IsNullOrEmpty(u.Email))
                {
                    return u.Email;
                }
                else
                {
                    return defaultEmail;
                }
            }
            catch (Exception)
            {
                return defaultEmail;
            }
        }

        public static void Message(Page page, string l_msg)
        {
            ScriptManager l_oSM = ScriptManager.GetCurrent(page);
            if (l_oSM != null)
                ScriptManager.RegisterStartupScript(page, page.GetType(), "wsAlertMessage", String.Format("alert('{0}');", l_msg), true);
            else
                page.ClientScript.RegisterStartupScript(page.GetType(), "wsAlertMessage", String.Format("alert('{0}');", l_msg), true);
        }

        public static void Redirect(Page page, string l_msg, string l_sURL)
        {
            ScriptManager l_oSM = ScriptManager.GetCurrent(page);
            if (l_oSM != null)
                ScriptManager.RegisterStartupScript(page, page.GetType(), "wsReturnToPreviousPage", String.Format("alert('{0}');window.location.href='{1}';", l_msg, l_sURL), true);
            else
                page.ClientScript.RegisterStartupScript(page.GetType(), "wsReturnToPreviousPage", String.Format("alert('{0}');window.location.href='{1}';", l_msg, l_sURL), true);
        }
        public static void Redirect(Page page, string l_sURL)
        {
            ScriptManager l_oSM = ScriptManager.GetCurrent(page);
            if (l_oSM != null)
                ScriptManager.RegisterStartupScript(page, page.GetType(), "wsReturnToPreviousPage", String.Format("window.location.href='{0}';", l_sURL), true);
            else
                page.ClientScript.RegisterStartupScript(page.GetType(), "wsReturnToPreviousPage", String.Format("window.location.href='{0}';", l_sURL), true);
        }
        public static void Redirect(string url, bool endResponse)
        {
            System.Web.HttpContext.Current.Response.Redirect(url, endResponse);
        }
    }

    public class Config
    {
        public static string GetConnectionStringFromWebConfig(string Name)
        {
            string ConnString = "";

            ConnectionStringSettingsCollection connectionStrings = WebConfigurationManager.ConnectionStrings;

            foreach (ConnectionStringSettings c in connectionStrings)
            {
                //Name = LocalSqlServer, "KaiZen.CSMS.Data.ConnectionString", "ALCOA_WAO_CSMWP_devConnectionString"
                if (c.Name == Name)
                {
                    ConnString = c.ConnectionString;
                }
            }
            return ConnString;
        }
        public static string GetSqlStatusFromConnectionStringFromWebConfig(string Name)
        {
            string ConnString = "";

            ConnectionStringSettingsCollection connectionStrings = ConfigurationManager.ConnectionStrings;

            foreach (ConnectionStringSettings c in connectionStrings)
            {
                //Name = LocalSqlServer, "KaiZen.CSMS.Data.ConnectionString", "ALCOA_WAO_CSMWP_devConnectionString"
                if (c.Name == Name)
                {
                    //ConnString = c.ConnectionString;
                    if (c.ConnectionString.Contains("AUABGN-SQLTEST"))
                    {
                        ConnString = "Test";
                    }
                    if (c.ConnectionString.Contains("AUABGN-SQLPROD"))
                    {
                        ConnString = "Production";
                    }
                }
            }
            return ConnString;
        }
        public static string GetServiceAccountUserFromWebConfig(bool debug)
        {
            string user = "";
            user = String.Format("{0}\\{1}", WebConfigurationManager.AppSettings["WAOCSM_domain"], WebConfigurationManager.AppSettings["WAOCSM_user"]);
            if (debug)
            {
                user += " " + WebConfigurationManager.AppSettings["WAOCSM_pass"];
            }

            return user;
        }

        public static string GetValue(ConfigList Key)
        {
            ConfigService configService = new ConfigService();
            TList<KaiZen.CSMS.Entities.Config> configList = configService.GetAll(); //All KPI
            TList<KaiZen.CSMS.Entities.Config> configList2 = configList.FindAll( //Find KPI We Need
                delegate(KaiZen.CSMS.Entities.Config config)
                {
                    return
                        config.Key == Key.ToString();
                }
            );

            return configList2[0].Value.ToString();
        }
    }

    public class _Auth
    {

        public static bool PageLoad(Auth auth)
        {
            try
            {
                bool ret = false;
                if (SessionHandler.LoggedIn != "Yes")
                {
                    if (!auth.isAlcoaDirect)
                    {
                        auth.authAlcoaLan();

                        if (!auth.isValid)
                        {
                            
                            General.Redirect(String.Format("AccessDenied.aspx?error={0}", System.Web.HttpUtility.UrlEncode(auth.errorMsg.ToString())), true); 
                        }
                        else
                        {
                            auth.getUserDetails();
                            SessionHandler.LoggedIn = "Yes";
                            SessionHandler.UserLogon = auth.UserLogon;
                            SessionHandler.UserId = auth.UserId.ToString();
                            SessionHandler.UserLogon_UserEmail = auth.Email;
                            SessionHandler.CompanyId = auth.CompanyId.ToString();

                            SessionHandler.spVar_CompanyId = auth.CompanyId.ToString();
                            SessionHandler.spVar_UserId = auth.UserId.ToString();

                            if (auth.RoleId != (int)RoleList.Administrator)
                            {
                                Configuration configuration = new Configuration();
                                if (configuration.GetValue(ConfigList.SiteOfflineStatus) == "1") General.Redirect("~/SiteDown.aspx", true);
                            }

                            ret = true;
                        
                        }
                    }
                    else
                    {
                        General.Redirect(String.Format("AccessDenied.aspx?error={0}", System.Web.HttpUtility.UrlEncode("Not Logged In.")), true);
                    }
                }
                else
                {
                    if (!auth.isAlcoaDirect) { auth.authAlcoaLan(); }
                    else { auth.authAlcoaDirect(SessionHandler.UserLogon); }

                    if (!auth.isValid) { General.Redirect(String.Format("AccessDenied.aspx?error={0}", System.Web.HttpUtility.UrlEncode(auth.errorMsg.ToString())), true); }
                    else
                    {
                        auth.getUserDetails();
                        ret = true;
                    }
                }

                return ret;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex); //added by Debashis to track error of 'Access Denied'
                General.Redirect(String.Format("~/AccessDenied.aspx?error={0}", System.Web.HttpUtility.UrlEncode(ex.Message.ToString())), true);
                return false;
            }
        }

    }

    public class Navigation
    {

        public static SiteMapDataSource GenerateSiteMapHierarchy(int RoleId, int UserId)
        {
            using (SqlDataSource dataSource = new SqlDataSource(ConfigurationManager.ConnectionStrings["ALCOA_WAO_CSMWP_devConnectionString"].ToString(), "SELECT * FROM ConfigNavigation"))
            {
                SiteMapDataSource ret = new SiteMapDataSource();
                DataSourceSelectArguments arg = new DataSourceSelectArguments("ParentId");
                DataView dataView = dataSource.Select(arg) as DataView;
                DataTable table = dataView.Table;
                DataRow rowRootNode = table.Select("ParentId = 0")[0];
                UnboundSiteMapProvider provider = new UnboundSiteMapProvider(rowRootNode["NavigateURL"].ToString(), rowRootNode["Text"].ToString());
                AddNodeToProviderRecursive(provider.RootNode, rowRootNode["ConfigNavigationId"].ToString(), table, provider, RoleId, UserId);
                ret.Provider = provider;
                return ret;
            }
        }
        private static void AddNodeToProviderRecursive(SiteMapNode parentNode, string parentID, DataTable table, UnboundSiteMapProvider provider, int RoleId, int UserId)
        {

            DataRow[] childRows = table.Select("ParentId = " + parentID);
            foreach (DataRow row in childRows)
            {
                try
                {
                    if (isVisible(RoleId, UserId, row["VisibleBy"].ToString()))
                    {
                        SiteMapNode childNode = CreateSiteMapNode(row, provider, RoleId);
                        provider.AddSiteMapNode(childNode, parentNode);
                        AddNodeToProviderRecursive(childNode, row["ConfigNavigationId"].ToString(), table, provider, RoleId, UserId);
                    }
                }
                catch (Exception)
                {
                    //throw new exception
                }
            }
        }
        private static SiteMapNode CreateSiteMapNode(DataRow dataRow, UnboundSiteMapProvider provider, int RoleId)
        {
            string NavigateURL;
            if (RoleId == (int)RoleList.Contractor && !String.IsNullOrEmpty(dataRow["NavigateURL_Contractor"].ToString()))
                NavigateURL = dataRow["NavigateURL_Contractor"].ToString();
            else
                NavigateURL = dataRow["NavigateURL"].ToString();

            string Text;
            if (RoleId == (int)RoleList.Contractor && !String.IsNullOrEmpty(dataRow["Text_Contractor"].ToString()))
                Text = dataRow["Text_Contractor"].ToString();
            else
                Text = dataRow["Text"].ToString();

            return provider.CreateNode(NavigateURL, Text, "", null, new NameValueCollection());
        }

        private static bool isVisible(int RoleId, int UserId, string VisibleTo)
        {
            bool _isVisible = false;

            if (!String.IsNullOrEmpty(VisibleTo))
            {
                switch (RoleId)
                {
                    case ((int)RoleList.Reader):
                        if (VisibleTo.Contains("Reader(*)"))
                        {
                            _isVisible = true;
                        }
                        else
                        {
                            if (Helper.General.isPrivilegedUser(UserId) && VisibleTo.Contains("Reader(Privileged)")) _isVisible = true;
                            if (Helper.General.isEhsConsultant(UserId) && VisibleTo.Contains("Reader(EHSConsultant)")) _isVisible = true;
                            if (Helper.General.isProcurementUser(UserId) && VisibleTo.Contains("Reader(Procurement)")) _isVisible = true;
                        }
                        break;
                    case ((int)RoleList.Contractor):
                        if (VisibleTo.Contains("Contractor")) _isVisible = true;
                        break;
                    case ((int)RoleList.Administrator):
                        if (VisibleTo.Contains("Admin")) _isVisible = true;
                        break;
                    case ((int)RoleList.PreQual):
                        if (VisibleTo.Contains("PreQual")) _isVisible = true;
                        break;
                    default: //Includes 'Disabled'
                        break;
                }
            }
            return _isVisible;
        }
    }

    public static WindowsImpersonationContext ImpersonateWAOCSMUser()
    {
        return KaiZen.Library.NetworkSecurity.Impersonation.ImpersonateUser(
                                WebConfigurationManager.AppSettings.Get("WAOCSM_domain"),
                                WebConfigurationManager.AppSettings.Get("WAOCSM_user"),
                                WebConfigurationManager.AppSettings.Get("WAOCSM_pass"));
    }

    public class Radar
    {
        protected static AnnualTargets AnnualTarget(int Year, int Month, int Qtr, bool returnQtr)
        {
            AnnualTargetsService atService = new AnnualTargetsService();
            AnnualTargets atMonth = atService.GetByYearCompanySiteCategoryId(Year, Month);
            AnnualTargets atQtr = atService.GetByYearCompanySiteCategoryId(Year, Qtr);

            //If Can't Find Annual Targets. Try to Find and Use a Previous Year's one...
            if (atMonth == null)
            {
                //find latest
                int count = 0;
                AnnualTargetsFilters atFilters = new AnnualTargetsFilters();
                atFilters.Append(AnnualTargetsColumn.CompanySiteCategoryId, Month.ToString());
                //Changes done for DT#2681
                atFilters.AppendLessThanOrEqual(AnnualTargetsColumn.Year, Year.ToString());
                TList<AnnualTargets> atList = DataRepository.AnnualTargetsProvider.GetPaged(
                                                atFilters.ToString(), "Year DESC", 0, 100, out count);
                if (count > 0)
                {
                    atMonth = atList[0].Copy();
                }
                else
                {
                    CompanySiteCategoryService cscService = new CompanySiteCategoryService();
                    CompanySiteCategory csc = cscService.GetByCompanySiteCategoryId(Month);
                    throw new Exception("Cannot find Annual Targets for '" + csc.CategoryDesc + "'");
                }
            }
            if (atQtr == null)
            {
                //find latest
                int count = 0;
                AnnualTargetsFilters atFilters = new AnnualTargetsFilters();
                atFilters.Append(AnnualTargetsColumn.CompanySiteCategoryId, Qtr.ToString());
                //Changes done for DT#2681
                atFilters.AppendLessThanOrEqual(AnnualTargetsColumn.Year, Year.ToString());
                TList<AnnualTargets> atList = DataRepository.AnnualTargetsProvider.GetPaged(
                                                atFilters.ToString(), "Year DESC", 0, 100, out count);
                if (count > 0)
                {
                    atQtr = atList[0].Copy();
                }
                else
                {
                    //CompanySiteCategoryService cscService = new CompanySiteCategoryService();
                    //CompanySiteCategory csc = cscService.GetByCompanySiteCategoryId(CompanySiteCategoryIdQtr);
                    //throw new Exception("Cannot find Annual Targets for '" + csc.CategoryDesc + "'");
                }

            }
            if (!returnQtr)
                return atMonth;
            else
                return atQtr;
        }

        public static void CheckInputVariables(bool AllCompanies)
        {
            if (AllCompanies) //Expecting User to select All Companies
            {
                if (SessionHandler.spVar_CompanyId != "-1") throw new Exception("All Companies Not Selected.");
            }
            else//Expecting User to a specific Company
            {
                if (SessionHandler.spVar_CompanyId == "-1") throw new Exception("Company Not Selected.");
            }

            int i = 0;

            if (!int.TryParse(SessionHandler.spVar_CompanyId, out i)) throw new Exception("Company Not Selected");
            if (!int.TryParse(SessionHandler.spVar_SiteId, out i)) throw new Exception("Site Not Selected.");
            if (!int.TryParse(SessionHandler.spVar_Year, out i)) throw new Exception("Year Not Selected.");
        }
        public static void ValidateCompanyCategorySetup(int _CompanyId, int _SiteId)
        {

            //Check CategoryId
            
            if (_SiteId > 0)
            {
                int i = GetCompanyCategoryId_ByCompanyIdSiteId(_CompanyId, _SiteId);
            }
            else
            {
                CompanySiteCategoryStandardService cscsService = new CompanySiteCategoryStandardService();
                RegionsSitesService rsService = new RegionsSitesService();
                TList<RegionsSites> rsTlist = rsService.GetByRegionId(_SiteId * -1);

                foreach (RegionsSites rs in rsTlist)
                {
                    int i = 0;
                    CompanySiteCategoryStandard cscs = cscsService.GetByCompanyIdSiteId(_CompanyId, rs.SiteId);
                    if(cscs != null) i = GetCompanyCategoryId_ByCompanyIdSiteId(_CompanyId, rs.SiteId);
                }
            }
        }
        public static int GetCompanyCategoryId_ByCompanyIdSiteId(int _CompanyId, int _SiteId)
        {
            try
            {
                const string errorMessage = "Category has not been Assigned for the selected Company and Site. Contact CSMS Team.";

                CompanySiteCategoryStandardService cscsService = new CompanySiteCategoryStandardService();
                CompanySiteCategoryStandard cscs = cscsService.GetByCompanyIdSiteId(_CompanyId, _SiteId);
                if (cscs == null)
                {
                    throw new Exception(errorMessage);
                }
                else
                {
                    if (cscs.CompanySiteCategoryId == null)
                    {
                        throw new Exception(errorMessage);
                    }
                    else
                    {
                        return Convert.ToInt32(cscs.CompanySiteCategoryId);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public static string[] GetKpiMeasurements(int CategoryId, bool Internal, bool All)
        {
            string[] KpiArray1 = { 
                                    "Residential Category",
                                 };
            //Added by Sayani
            string[] KpiArray1_All = { 
                                    "Residential Category",
                                    "LWDFR",
                                    "TRIFR",
                                    "AIFR",
                                    "DART",
                                    "Risk Notifications"
                                 };
            string[] KpiArray1_Internal = { 
                                    "Residential Category",
                                    "Average No. Of People On Site",
                                    "Total Man Hours"
                                 };
            string[] KpiArray2_Embedded = { 
                                    "On site",
                                    "Alcoa Annual Audit Score (%)",
                                    "CSA - Self Audit (% Score) - Current Qtr", //These two lines swapped around by AG DT318 Switched Back DT445
                                    "CSA - Self Audit (% Score) - Previous Qtr", //These two lines swapped around by AG DT318
                                    "IFE : Injury Ratio",
                                    //"RN : Injury Ratio",  //AG DT367 6/1/2016
                                    "Workplace Safety & Compliance",
                                    "Management Health Safety Work Contacts",
                                    "Behavioural Observations (%)",
                                    "Behavioural Observations - Plan",
                                    "Behavioural Observations - Actual",
                                    "Fatality Prevention Program",
                                    "JSA Field Audit Verifications",
                                    "Toolbox Meetings",
                                    "Alcoa Weekly Contractors Meeting",
                                    "Alcoa Monthly Contractors Meeting",
                                    "Safety Plan(s) Submitted",
                                    "% Mandated Training"
                                 };
            string[] KpiArray2_NonEmbedded_1 = { 
                                    "On site",
                                    "Alcoa Annual Audit Score (%)",
                                    "CSA - Self Audit (% Score) - Current Qtr", 
                                    "CSA - Self Audit (% Score) - Previous Qtr",
                                    "IFE : Injury Ratio",
                                    //"RN : Injury Ratio", //AG DT367
                                    "Workplace Safety & Compliance",
                                    "Management Health Safety Work Contacts",
                                    "Behavioural Observations (%)",
                                    "Behavioural Observations - Plan",
                                    "Behavioural Observations - Actual",
                                    "JSA Field Audit Verifications",
                                    "Toolbox Meetings",
                                    "Alcoa Weekly Contractors Meeting",
                                    "Alcoa Monthly Contractors Meeting",
                                    "% Mandated Training"
                                 };
            string[] KpiArray2_NonEmbedded_2 = { 
                                    "On site",
                                    "IFE : Injury Ratio",
                                    //"RN : Injury Ratio",  //AG DT367
                                    "Workplace Safety & Compliance",
                                    "Management Health Safety Work Contacts",
                                    "Behavioural Observations (%)",
                                    "Behavioural Observations - Plan",
                                    "Behavioural Observations - Actual",
                                    "JSA Field Audit Verifications",
                                    "Toolbox Meetings",
                                    "Alcoa Monthly Contractors Meeting",
                                    "% Mandated Training"
                                 };

            string[] KpiArray2_NonEmbedded_3 = { 
                                    "On site",
                                    //"IFE : Injury Ratio",
                                    //"JSA Field Audit Verifications", -- Removed and replaced with Toolbox Meetings (13.Oct.2011)
                                    "Toolbox Meetings",
                                    "% Mandated Training"
                                 };
            switch (CategoryId)
            {
                case -1:
                    if (All)
                        return KpiArray1_All;
                    else
                    {
                        if (Internal)
                        {
                            return KpiArray1_Internal;
                        }
                        else
                        {
                            return KpiArray1;
                        }
                    }
                    break;
                case (int)CompanySiteCategoryList.E:
                    return KpiArray2_Embedded;
                    break;
                case (int)CompanySiteCategoryList.NE1:
                    return KpiArray2_NonEmbedded_1;
                    break;
                case (int)CompanySiteCategoryList.NE2:
                    return KpiArray2_NonEmbedded_2;
                    break;
                case (int)CompanySiteCategoryList.NE3:
                    return KpiArray2_NonEmbedded_3;
                    break;
                default:
                    throw new Exception("Unexpected value of CategoryId received.");
                    break;
            }
        }
        public static void CreateGridColumns(int SiteId, int MonthId, ASPxGridView grid, ASPxGridView grid2, ASPxGridView gridMonthlyE, ASPxGridView gridRawScores, ASPxGridView gridQtrly, DevExpress.XtraCharts.Web.WebChartControl wcSpider)
        {
            //Instantiate services
            SitesService sService = new SitesService();

            //Instantiate Variables
            int grid_VisibleIndex = 0;

            Sites s = sService.GetBySiteId(Convert.ToInt32(SiteId));
            if (s == null) throw new Exception("Error Occurred whilst Loading Site Columns");
            string SiteName = s.SiteName;
            if (SiteName.Length > 10) SiteName = s.SiteAbbrev;
            //Create Grid 1 - Summary
            grid.Width = Unit.Percentage(100);
            grid.KeyFieldName = "KpiMeasure";
            grid_VisibleIndex = Helper.Radar.AddGridColumn(grid, "KpiMeasure", "Key Performance Indicators", "Key Performance Indicators (KPI)", 300, grid_VisibleIndex);
            grid_VisibleIndex = Helper.Radar.AddGridColumn(grid, "KpiScoreYtd", SiteName, SiteName, 200, grid_VisibleIndex);

            grid_VisibleIndex = 0;

            //Create Grid 2 - YTD
            grid2.Width = Unit.Percentage(100);
            grid2.KeyFieldName = "KpiMeasure";
            grid_VisibleIndex = Helper.Radar.AddGridColumn(grid2, "KpiMeasure", "Key Performance Indicators", "Key Performance Indicators (KPI)", 300, grid_VisibleIndex);
            grid_VisibleIndex = Helper.Radar.AddGridColumn(grid2, "PlanYtd", "Plan", "Plan", null, grid_VisibleIndex);
            grid_VisibleIndex = Helper.Radar.AddGridColumn(grid2, "KpiScoreYtd", SiteName, SiteName, null, grid_VisibleIndex);


            ASPxSummaryItem totalYear = new ASPxSummaryItem();
                totalYear.FieldName = "KpiScoreYtdPercentage";
                totalYear.ShowInColumn = "KpiScoreYtdPercentage";
            totalYear.SummaryType = DevExpress.Data.SummaryItemType.Custom;
            grid2.TotalSummary.Clear();
            grid2.TotalSummary.Add(totalYear);
            grid_VisibleIndex = Helper.Radar.AddGridColumn(grid2, "KpiScoreYtdPercentage", "%", "%", null, grid_VisibleIndex);

            grid_VisibleIndex = 0;

            //Create Grid 3 - Monthly Progress
            gridMonthlyE.Width = Unit.Percentage(100);
            gridMonthlyE.KeyFieldName = "KpiMeasure";
            grid_VisibleIndex = Helper.Radar.AddGridColumn(gridMonthlyE, "KpiMeasure", "Key Performance Indicators", "Key Performance Indicators (KPI)", 300, grid_VisibleIndex);
            grid_VisibleIndex = Helper.Radar.AddGridColumn(gridMonthlyE, "PlanMonth", "Plan", "Plan", null, grid_VisibleIndex);
            MonthsService mService = new MonthsService();
            TList<Months> mTlist = mService.GetAll();
            int MonthNo = 1;
            foreach (Months m in mTlist)
            {
                grid_VisibleIndex = Helper.Radar.AddGridColumn(gridMonthlyE, String.Format("KpiScoreMonth{0}", MonthNo),
                                                                m.MonthAbbrev.ToUpper(), m.MonthAbbrev.ToUpper(),
                                                                40, grid_VisibleIndex);
                MonthNo++;
            }

            grid_VisibleIndex = 0;

            //Create Grid 4 - Raw Scores
            gridRawScores.Width = Unit.Percentage(100);
            gridRawScores.KeyFieldName = "KpiMeasure";
            grid_VisibleIndex = Helper.Radar.AddGridColumn(gridRawScores, "KpiMeasure", "Key Performance Indicators", "Key Performance Indicators (KPI)", 300, grid_VisibleIndex);
            grid_VisibleIndex = Helper.Radar.AddGridColumn(gridRawScores, "PlanMonth", "YTD", "YTD", null, grid_VisibleIndex);
            mTlist = mService.GetAll();
            MonthNo = 1;
            foreach (Months m in mTlist)
            {
                grid_VisibleIndex = Helper.Radar.AddGridColumn(gridRawScores, String.Format("KpiScoreMonth{0}", MonthNo),
                                                                m.MonthAbbrev.ToUpper(), m.MonthAbbrev.ToUpper(),
                                                                40, grid_VisibleIndex);
                MonthNo++;
            }

            grid_VisibleIndex = 0;

            //Create Grid 5 - Qtrly Progress
            gridQtrly.Width = Unit.Percentage(100);
            gridQtrly.KeyFieldName = "KpiMeasure";
            grid_VisibleIndex = Helper.Radar.AddGridColumn(gridQtrly, "KpiMeasure", "Key Performance Indicators", "Key Performance Indicators (KPI)", 300, grid_VisibleIndex);
            grid_VisibleIndex = Helper.Radar.AddGridColumn(gridQtrly, "PlanYtd", "Plan", "Plan", null, grid_VisibleIndex);
            for (int i = 1; i <= 4; i++)
                grid_VisibleIndex = Helper.Radar.AddGridColumn(gridQtrly,
                                                               String.Format("KpiScoreMonth{0}", i * 3), String.Format("Qtr {0}", i),
                                                               String.Format("Qtr {0}", i), 186, grid_VisibleIndex);

            grid_VisibleIndex = 0;

            //Create Spider Data
            DevExpress.XtraCharts.Series series = new DevExpress.XtraCharts.Series("Spider", DevExpress.XtraCharts.ViewType.RadarArea);
            wcSpider.Series.Add(series);
            wcSpider.Series[0].ArgumentScaleType = DevExpress.XtraCharts.ScaleType.Qualitative;
            wcSpider.Series[0].ArgumentDataMember = "Key";
            wcSpider.Series[0].ValueScaleType = DevExpress.XtraCharts.ScaleType.Numerical;
            wcSpider.Series[0].ValueDataMembers.AddRange(new string[] { "Value" });
            wcSpider.Series[0].Label.Antialiasing = true;
            wcSpider.Series[0].Label.Visible = false;
        }
        public static void CreateGridColumns(int SiteId, int MonthId, ASPxGridView grid, ASPxGridView grid2, DevExpress.XtraCharts.Web.WebChartControl wcSpider)
        {
            ASPxGridView grid3 = new ASPxGridView();
            ASPxGridView grid4 = new ASPxGridView();
            ASPxGridView grid5 = new ASPxGridView();
            CreateGridColumns(SiteId, MonthId, grid, grid2, grid3, grid4, grid5, wcSpider);
        }

        public static DataTable CreateDataTable(int _CompanyId, int _SiteId, int No, int? _CategoryId)
        {
            DataTable dt = new DataTable();

            dt.Columns.Add("KpiMeasure", typeof(string));
            dt.Columns.Add("PlanYtd", typeof(string));

            if (_SiteId < 0)
            {
                CompanySiteCategoryStandardService cscsService = new CompanySiteCategoryStandardService();
                RegionsSitesService rService = new RegionsSitesService();
                SitesService sService = new SitesService();
                
                TList<RegionsSites> rsTlist = rService.GetByRegionId(-1 * _SiteId);
                TList<CompanySiteCategoryStandard> cscsTlist = cscsService.GetByCompanyId(_CompanyId);

                foreach (RegionsSites rs in rsTlist)
                {
                    foreach (CompanySiteCategoryStandard cscs in cscsTlist)
                    {
                        if (cscs.SiteId == rs.SiteId)
                        {
                            bool cont = true;
                            if (_CategoryId != null)
                            {
                                if (cscs.CompanySiteCategoryId != _CategoryId) cont = false;
                            }

                            if (cont)
                            {
                                Sites s = sService.GetBySiteId(rs.SiteId);
                                if (s == null) throw new Exception("Error Occurred whilst Loading Site Columns");
                                string SiteName = s.SiteName;
                                if (SiteName.Length > 10) SiteName = s.SiteAbbrev;
                                dt.Columns.Add(String.Format("PlanYtd{0}", SiteName), typeof(string));
                                dt.Columns.Add(String.Format("KpiScoreYtd{0}", SiteName), typeof(string));
                            }
                        }
                    }
                }
            }
            else
            {
                dt.Columns.Add("KpiScoreYtd", typeof(string));
                dt.Columns.Add("KpiScoreYtdPercentage", typeof(string));

                if (No == 2)
                {
                    dt.Columns.Add("PlanMonth", typeof(string));
                    for (int i = 1; i <= 12; i++) dt.Columns.Add(String.Format("KpiScoreMonth{0}", i), typeof(string));
                    for (int i = 1; i <= 12; i++) dt.Columns.Add(String.Format("KpiScoreMonth{0}Percentage", i), typeof(string));

                }
            }

            return dt;
        }
        public static DataRow CreateDataRow_DataSet1_Summary(DataTable dt, string KpiMeasure, int _CompanyId, int _SiteId, int _MonthId, int _Year, int _CategoryId)
        {
            CompanySiteCategoryService cscService = new CompanySiteCategoryService();
            CompanySiteCategoryStandardService cscsService = new CompanySiteCategoryStandardService();
            DataRow dr = dt.NewRow();

            string KpiScoreYtd = String.Empty;


            if (KpiMeasure == "Residential Category")
            {
                CompanySiteCategory csc = cscService.GetByCompanySiteCategoryId(_CategoryId);
                KpiScoreYtd = csc.CategoryDesc;
            }
            else
            {
                KpiCompanySiteCategoryService kcscService = new KpiCompanySiteCategoryService();
                DataSet ds;
                if (_MonthId > 0)
                {
                    ds = kcscService.Compliance_Table_SS_Month(_CompanyId, _SiteId, _MonthId, _Year, _CategoryId.ToString()); //Month
                }
                else
                {
                    ds = kcscService.Compliance_Table_SS(_CompanyId, _SiteId, _Year, _CategoryId.ToString()); //Year
                }
                if (ds.Tables[0] != null)
                {
                    foreach (DataRow drKpi in ds.Tables[0].Rows)
                    {
                        if (KpiMeasure == "Average No. Of People On Site" || KpiMeasure == "Total Man Hours")
                        {
                            int num = 0;
                            Int32.TryParse(drKpi[KpiMeasure].ToString(), out num);
                            KpiScoreYtd = num.ToString("#,##0");
                        }
                        else if (KpiMeasure == "LWDFR" || KpiMeasure == "TRIFR"
                                    || KpiMeasure == "AIFR" || KpiMeasure == "Risk Notifications" ||
                            KpiMeasure == "DART")
                        {
                            KpiScoreYtd = drKpi[KpiMeasure].ToString();
                        }
                    }
                }
                else
                {
                    KpiScoreYtd = "n/a";
                }
            }

            dr["KpiMeasure"] = KpiMeasure;
            dr["KpiScoreYtd"] = KpiScoreYtd;

            return dr;
        }
        public static DataRow CreateDataRow_DataSet2_Ytd(DataTable dt, string KpiMeasure, int _CompanyId, Repo.CSMS.DAL.EntityModels.Site _Site, int _MonthId, int _Year, CompanySiteCategory csc)
        {
            //CompanySiteCategoryService cscService = new CompanySiteCategoryService();
            int _CategoryId = csc.CompanySiteCategoryId;
            //CompanySiteCategoryStandardService cscsService = new CompanySiteCategoryStandardService();
            KpiCompanySiteCategoryService kcscService = new KpiCompanySiteCategoryService();
            VList<KpiCompanySiteCategory> kcscVlist = kcscService.GetByCompanyIdSiteIdYear(_CompanyId, _Site.SiteId, _Year);
            var kpiService = ALCOA.CSMS.Website.Global.GetInstance<Repo.CSMS.Service.Database.IKpiService>();

            DataRow dr = dt.NewRow();

            string PlanYtd = "-";
            string PlanMonth = "-";
            string KpiScoreYtd = "-";
            string KpiScoreYtdPercentage = "-";

            string[] KpiScoreMonth = new string[13];
            string[] KpiScoreMonthPercentage = new string[13];
            for (int i = 1; i <= 12; i++) KpiScoreMonth[i] = "-";
            for (int i = 1; i <= 12; i++) KpiScoreMonthPercentage[i] = "-";


            AnnualTargetsService atService = new AnnualTargetsService();
            AnnualTargets at = atService.GetByYearCompanySiteCategoryId(_Year, csc.CompanySiteCategoryId);
            
            //CompanySiteCategory csc = cscService.GetByCompanySiteCategoryId(_CategoryId);
            //If Can't Find Annual Targets. Try to Find and Use a Previous Year's one...
            if (at == null)
            {
                //find latest
                int count = 0;
                AnnualTargetsFilters atFilters = new AnnualTargetsFilters();
                atFilters.Append(AnnualTargetsColumn.CompanySiteCategoryId, _CategoryId.ToString());
                //Changes done for DT#2681
                atFilters.AppendLessThanOrEqual(AnnualTargetsColumn.Year, _Year.ToString());
                TList<AnnualTargets> atList = DataRepository.AnnualTargetsProvider.GetPaged(
                                                atFilters.ToString(), "Year DESC", 0, 100, out count);
                if (count > 0)
                {
                    at = atList[0].Copy();
                }
                else
                {
                    throw new Exception("Cannot find Annual Targets for '" + csc.CategoryDesc + "'");
                }
            }

            DateTime dtNowMinusOneMonth = DateTime.Now;
            DateTime dtNow = DateTime.Now;
            if (_MonthId == 0)
            {
                if (DateTime.Now.Month != 1)
                {
                    
                    dtNowMinusOneMonth = dtNowMinusOneMonth.AddMonths(-1);
                }

                if (_Year < DateTime.Now.Year) dtNowMinusOneMonth = new DateTime(_Year, 12, 1);
                if (_Year != dtNow.Year) dtNow = new DateTime(_Year, 12, 1);
            }
            else
            {
                dtNowMinusOneMonth = new DateTime(_Year, _MonthId, 1);
            }

            int upto = 12;
            if (_Year == DateTime.Now.Year) upto = dtNowMinusOneMonth.Month;

            int NoMonths = GetCountMonthsExpectedOnSite(_CompanyId, _Site.SiteId, upto, _Year);

            //Get all kpis for the year
            var lstMonthYear = new List<DateTime>();
            for (int i = 1; i <= 12; i++)
            {
                lstMonthYear.Add(new DateTime(_Year, i, 1));
            }
            var kpis = kpiService.GetMany(null, e => e.CompanyId == _CompanyId && e.SiteId == _Site.SiteId && lstMonthYear.Contains(e.kpiDateTime), null, null);

            if (KpiMeasure != "On site")
            {
                for (int i = 1; i <= upto; i++)
                {
                    switch (ExpectedOnSite(kpis,_CompanyId, _Site, i, _Year))
                    {
                        case true: //On Site
                            KpiScoreMonthPercentage[i] = "0";
                            KpiScoreMonth[i] = "0";
                            break;
                        case false: //Not On Site
                            KpiScoreMonthPercentage[i] = "-";
                            KpiScoreMonth[i] = "-";
                            break;
                        default: //???
                            KpiScoreMonthPercentage[i] = "-";
                            KpiScoreMonth[i] = "-";
                            break;
                    }
                }
            }

            string[] KpiScoreMonth_Temp = KpiScoreMonth;
            string[] KpiScoreMonthPercentage_Temp = KpiScoreMonthPercentage;

            switch (KpiMeasure)
            {
                #region "On site"
                case "On site":
                    PlanYtd = "-";
                    PlanMonth = "-";

                    SitesService sService = new SitesService();

                    for (int i = 1; i <= upto; i++) KpiScoreMonth[i] = "-";

                    if (!String.IsNullOrEmpty(_Site.SiteNameEbi))
                    {
                        for (int i = 1; i <= upto; i++) KpiScoreMonth[i] = "No";
                    }

                    foreach (KpiCompanySiteCategory kcsc in kcscVlist)
                    {
                        if (kcsc.EbiOnSite == true || kcsc.AheaTotalManHours > 0) KpiScoreMonth[kcsc.KpiDateTime.Month] = "Yes"; 
                    }

                    break;
                #endregion
                #region "CSA"
                case "Alcoa Annual Audit Score (%)":
                    PlanYtd = "0";
                    PlanMonth = "0";
                    //KpiScoreYtd = "0";
                    KpiScoreYtdPercentage = "-"; //Not Calculated in Compliance %
                    for (int i = 1; i <= upto; i++) KpiScoreMonthPercentage[i] = "-"; //Not Calculated in Compliance %

                    if (at.QuarterlyAuditScore > 0) PlanYtd = at.QuarterlyAuditScore.ToString();
                    PlanMonth = PlanYtd;

                    if (NoMonths > 0)
                    {
                        KpiScoreYtd = GetCsaScore(_CompanyId, _Site.SiteId, 5, _Year);

                        for (int i = 1; i <= 12; i++)
                        {
                            KpiScoreMonth[i] = KpiScoreYtd;
                        }
                    }

                    break;

                case "CSA - Self Audit (% Score) - Current Qtr":
                    PlanYtd = "0";
                    if (NoMonths > 0) KpiScoreYtd = "0";
                    KpiScoreYtdPercentage = "-"; //Not Calculated in Compliance %
                    for (int i = 1; i <= upto; i++) KpiScoreMonthPercentage[i] = "-"; //Not Calculated in Compliance %

                    if (at.QuarterlyAuditScore > 0) PlanYtd = at.QuarterlyAuditScore.ToString();
                    PlanMonth = PlanYtd;

                    bool cont = false;
                    int QtrId = 0;
                    if (dtNowMinusOneMonth.Month >= 1)
                    {
                        QtrId = 1;
                        if (ExpectedOnSite(kpis, _CompanyId, _Site, 1, _Year) == true ||
                            ExpectedOnSite(kpis, _CompanyId, _Site, 2, _Year) == true ||
                            ExpectedOnSite(kpis, _CompanyId, _Site, 3, _Year) == true)
                        {
                            cont = true;
                        }
                        if (dtNowMinusOneMonth.Month >= 4)
                        {
                            QtrId = 2;
                            if (ExpectedOnSite(kpis, _CompanyId, _Site, 4, _Year) == true ||
                                ExpectedOnSite(kpis, _CompanyId, _Site, 5, _Year) == true ||
                                ExpectedOnSite(kpis, _CompanyId, _Site, 6, _Year) == true)
                            {
                                cont = true;
                            }
                            if (dtNowMinusOneMonth.Month >= 7)
                            {
                                QtrId = 3;
                                if (ExpectedOnSite(kpis, _CompanyId, _Site, 7, _Year) == true ||
                                    ExpectedOnSite(kpis, _CompanyId, _Site, 8, _Year) == true ||
                                    ExpectedOnSite(kpis, _CompanyId, _Site, 9, _Year) == true)
                                {
                                    cont = true;
                                }
                                if (dtNowMinusOneMonth.Month >= 10)
                                {
                                    QtrId = 4;
                                    if (ExpectedOnSite(kpis, _CompanyId, _Site, 10, _Year) == true ||
                                        ExpectedOnSite(kpis, _CompanyId, _Site, 11, _Year) == true ||
                                        ExpectedOnSite(kpis, _CompanyId, _Site, 12, _Year) == true)
                                    {
                                        cont = true;
                                    }
                                }
                            }
                        }
                    }
                    if (cont)
                    {
                        KpiScoreYtd = GetCsaScore(_CompanyId, _Site.SiteId, QtrId, _Year);
                        
                        int MonthStart = 0;
                        int csaScoreYtd = 0;
                        for (int i = 1; i <= upto; i = i + 3)
                        {
                            if (i >= 1)
                            {
                                if (ExpectedOnSite(kpis, _CompanyId, _Site, 1, _Year) == true ||
                                        ExpectedOnSite(kpis, _CompanyId, _Site, 2, _Year) == true ||
                                        ExpectedOnSite(kpis, _CompanyId, _Site, 3, _Year) == true)
                                {
                                    MonthStart = 1;
                                    KpiScoreMonth[MonthStart] = GetCsaScore(_CompanyId, _Site.SiteId, 1, _Year);
                                    if (KpiScoreMonth[MonthStart] != "0" && KpiScoreMonth[MonthStart] != "-")
                                    { 
                                        KpiScoreMonthPercentage[MonthStart] = "100";
                                        if (i == 1) csaScoreYtd += 3;
                                    }
                                    else KpiScoreMonthPercentage[MonthStart] = "0";
                                    KpiScoreMonth[MonthStart + 1] = KpiScoreMonth[MonthStart];
                                    KpiScoreMonthPercentage[MonthStart + 1] = KpiScoreMonthPercentage[MonthStart];
                                    KpiScoreMonth[MonthStart + 2] = KpiScoreMonth[MonthStart];
                                    KpiScoreMonthPercentage[MonthStart + 2] = KpiScoreMonthPercentage[MonthStart];
                                }
                                if (i >= 4)
                                {
                                    if (ExpectedOnSite(kpis, _CompanyId, _Site, 4, _Year) == true ||
                                        ExpectedOnSite(kpis, _CompanyId, _Site, 5, _Year) == true ||
                                        ExpectedOnSite(kpis, _CompanyId, _Site, 6, _Year) == true)
                                    {
                                        MonthStart = 4;
                                        string csaVal = GetCsaScore(_CompanyId, _Site.SiteId, 2, _Year);
                                        if (_CategoryId == 2) //Added by Ashley Goldstraw to handle Non Embedded 1 only reporting by each half.
                                        {  
                                            string csaValTmp;
                                            if (csaVal == "-") { csaValTmp = "0"; } else { csaValTmp = csaVal; }
                                            string csaValQtr1 = GetCsaScore(_CompanyId, _Site.SiteId, 1, _Year);
                                            if (csaValQtr1 == "-") { csaValQtr1 = "0"; }
                                            csaVal = Int32.Parse(csaValTmp) > Int32.Parse(csaValQtr1) ? csaValTmp : csaValQtr1;
                                        }

                                        KpiScoreMonth[MonthStart] = csaVal;

                                        if (KpiScoreMonth[MonthStart] != "0" && KpiScoreMonth[MonthStart] != "-")
                                        {
                                            KpiScoreMonthPercentage[MonthStart] = "100";
                                            if (i==4) csaScoreYtd += 3;
                                        }
                                        else KpiScoreMonthPercentage[MonthStart] = "0";
                                        KpiScoreMonth[MonthStart + 1] = KpiScoreMonth[MonthStart];
                                        KpiScoreMonthPercentage[MonthStart + 1] = KpiScoreMonthPercentage[MonthStart];
                                        KpiScoreMonth[MonthStart + 2] = KpiScoreMonth[MonthStart];
                                        KpiScoreMonthPercentage[MonthStart + 2] = KpiScoreMonthPercentage[MonthStart];
                                        if (_CategoryId == 2) //Added by Ashley Goldstraw to handle Non Embedded 1 only reporting by each half.
                                        {
                                            KpiScoreMonth[MonthStart - 1] = KpiScoreMonth[MonthStart];
                                            KpiScoreMonthPercentage[MonthStart - 1] = KpiScoreMonthPercentage[MonthStart];
                                            KpiScoreMonth[MonthStart - 2] = KpiScoreMonth[MonthStart];
                                            KpiScoreMonthPercentage[MonthStart - 2] = KpiScoreMonthPercentage[MonthStart];
                                            KpiScoreMonth[MonthStart - 3] = KpiScoreMonth[MonthStart];
                                            KpiScoreMonthPercentage[MonthStart - 3] = KpiScoreMonthPercentage[MonthStart];
                                        }
                                        
                                    }
                                    if (i >= 7)
                                    {
                                        if (ExpectedOnSite(kpis, _CompanyId, _Site, 7, _Year) == true ||
                                            ExpectedOnSite(kpis, _CompanyId, _Site, 8, _Year) == true ||
                                            ExpectedOnSite(kpis, _CompanyId, _Site, 9, _Year) == true)
                                        {
                                            MonthStart = 7;
                                            KpiScoreMonth[MonthStart] = GetCsaScore(_CompanyId, _Site.SiteId, 3, _Year);
                                            if (KpiScoreMonth[MonthStart] != "0" && KpiScoreMonth[MonthStart] != "-")
                                            {
                                                KpiScoreMonthPercentage[MonthStart] = "100";
                                                if (i==7) csaScoreYtd += 3;
                                            }
                                            else KpiScoreMonthPercentage[MonthStart] = "0";
                                            KpiScoreMonth[MonthStart + 1] = KpiScoreMonth[MonthStart];
                                            KpiScoreMonthPercentage[MonthStart + 1] = KpiScoreMonthPercentage[MonthStart];
                                            KpiScoreMonth[MonthStart + 2] = KpiScoreMonth[MonthStart];
                                            KpiScoreMonthPercentage[MonthStart + 2] = KpiScoreMonthPercentage[MonthStart];
                                        }
                                        if (i >= 10)
                                        {
                                            if (ExpectedOnSite(kpis, _CompanyId, _Site, 10, _Year) == true ||
                                                ExpectedOnSite(kpis, _CompanyId, _Site, 11, _Year) == true ||
                                                ExpectedOnSite(kpis, _CompanyId, _Site, 12, _Year) == true)
                                            {
                                                MonthStart = 10;
                                                string csaVal = GetCsaScore(_CompanyId, _Site.SiteId, 4, _Year);
                                                if (_CategoryId == 2) //Added by Ashley Goldstraw to handle Non Embedded 1 only reporting by each half.
                                                {
                                                    string csaValTmp;
                                                    if (csaVal == "-") { csaValTmp = "0"; } else { csaValTmp = csaVal; }
                                                    string csaValQtr3 = GetCsaScore(_CompanyId, _Site.SiteId, 3, _Year);
                                                    if (csaValQtr3 == "-") { csaValQtr3 = "0"; }
                                                    csaVal = Int32.Parse(csaValTmp) > Int32.Parse(csaValQtr3) ? csaValTmp : csaValQtr3;
                                                }
                                                KpiScoreMonth[MonthStart] = csaVal;
                                                //KpiScoreMonth[MonthStart] = GetCsaScore(_CompanyId, _Site.SiteId, 4, _Year);
                                                if (KpiScoreMonth[MonthStart] != "0" && KpiScoreMonth[MonthStart] != "-")
                                                {
                                                    KpiScoreMonthPercentage[MonthStart] = "100";
                                                    if (i==10) csaScoreYtd += 3;
                                                }
                                                else KpiScoreMonthPercentage[MonthStart] = "0";
                                                KpiScoreMonth[MonthStart + 1] = KpiScoreMonth[MonthStart];
                                                KpiScoreMonthPercentage[MonthStart + 1] = KpiScoreMonthPercentage[MonthStart];
                                                KpiScoreMonth[MonthStart + 2] = KpiScoreMonth[MonthStart];
                                                KpiScoreMonthPercentage[MonthStart + 2] = KpiScoreMonthPercentage[MonthStart];
                                                if (_CategoryId == 2) //Added by Ashley Goldstraw to handle Non Embedded 1 only reporting by each half.
                                                {
                                                    KpiScoreMonth[MonthStart - 1] = KpiScoreMonth[MonthStart];
                                                    KpiScoreMonthPercentage[MonthStart - 1] = KpiScoreMonthPercentage[MonthStart];
                                                    KpiScoreMonth[MonthStart - 2] = KpiScoreMonth[MonthStart];
                                                    KpiScoreMonthPercentage[MonthStart - 2] = KpiScoreMonthPercentage[MonthStart];
                                                    KpiScoreMonth[MonthStart - 3] = KpiScoreMonth[MonthStart];
                                                    KpiScoreMonthPercentage[MonthStart - 3] = KpiScoreMonthPercentage[MonthStart];
                                                }
                                            }
                                        }
                                    }
                                }
                            }


                        }
                        int _ytdYear = 0;
                        int _ytdQtr = 0;
                        int _ytdMonthId;
                        if (_MonthId == 0)
                        {
                            _ytdMonthId = dtNow.Month;
                        }
                        else _ytdMonthId = _MonthId;
                        int _ytdYearId = _Year; 
                        _ytdYear = _ytdYearId;
                        if (_ytdMonthId == 1 || _ytdMonthId == 2 || _ytdMonthId == 3)
                        {
                            _ytdQtr = 1;
                        }
                        if (_ytdMonthId == 4 || _ytdMonthId == 5 || _ytdMonthId == 6)
                        {
                            _ytdQtr = 2;
                        }
                        if (_ytdMonthId == 7 || _ytdMonthId == 8 || _ytdMonthId == 9)
                        {
                            _ytdQtr = 3;
                        }
                        if (_ytdMonthId == 10 || _ytdMonthId == 11 || _ytdMonthId == 12)
                        {
                            _ytdQtr = 4;
                        }
                        if (_CategoryId == 2)
                        {
                            if (_ytdQtr == 4)
                                _ytdQtr = 3;
                            if (_ytdQtr == 2)
                                _ytdQtr = 1;
                        }
                        KpiScoreYtd = GetCsaScore(_CompanyId, _Site.SiteId, _ytdQtr, _ytdYear);
                        int csaPrevKpi;
                        int.TryParse(KpiScoreYtd, out csaPrevKpi);
                        if (csaPrevKpi > 0)
                            KpiScoreYtdPercentage = "100";
                        else
                            KpiScoreYtdPercentage = "0";
                    }

                    break;
                
                case "CSA - Self Audit (% Score) - Previous Qtr":
                    PlanYtd = "0";
                    //KpiScoreYtd = "0";
                    //KpiScoreYtdPercentage = "0";

                    if (at.QuarterlyAuditScore > 0) PlanYtd = at.QuarterlyAuditScore.ToString();
                    PlanMonth = PlanYtd;

                    int _QtrId = 0;
                    if (dtNowMinusOneMonth.Month >= 1)
                    {
                        _QtrId = 1;
                        if (dtNowMinusOneMonth.Month >= 4)
                        {
                            _QtrId = 2;
                            if (dtNowMinusOneMonth.Month >= 7)
                            {
                                _QtrId = 3;
                                if (dtNowMinusOneMonth.Month >= 10)
                                {
                                    _QtrId = 4;
                                }
                            }
                        }
                    }
                    int SearchYear = _Year;
                    if (_QtrId == 1 )
                    {
                        _QtrId = 4;
                        SearchYear = _Year - 1;
                    }
                    else if (_QtrId == 4 && SearchYear != DateTime.Now.Year)
                    {
                        //do nothing.
                    }
                    else
                    {
                        _QtrId = _QtrId - 1;
                    }

                    int Month1 = 10;
                    int Month2 = 11;
                    int Month3 = 12;
                    switch (_QtrId)
                    {
                        case 1:
                            Month1 = 1;
                            Month2 = 2;
                            Month3 = 3;
                            break;
                        case 2:
                            Month1 = 4;
                            Month2 = 5;
                            Month3 = 6;
                            break;
                        case 3:
                            Month1 = 7;
                            Month2 = 8;
                            Month3 = 9;
                            break;
                        case 4:
                            Month1 = 10;
                            Month2 = 11;
                            Month3 = 12;
                            break;
                        default:
                            break;
                    }

                    //Get all kpis for the year
                    var lstSearchMonthYear = new List<DateTime>();
                    lstSearchMonthYear.Add(new DateTime(SearchYear, Month1, 1));
                    lstSearchMonthYear.Add(new DateTime(SearchYear, Month2, 1));
                    lstSearchMonthYear.Add(new DateTime(SearchYear, Month3, 1));
                    //The three lines above were adding data to the wrong list.  Changed to fix DT423. ashley Goldstraw 7/12/2015

                    var searchKpis = kpiService.GetMany(null, e => e.CompanyId == _CompanyId && e.SiteId == _Site.SiteId && lstSearchMonthYear.Contains(e.kpiDateTime), null, null);

                    if (ExpectedOnSite(searchKpis, _CompanyId, _Site, Month1, SearchYear) == true ||
                        ExpectedOnSite(searchKpis, _CompanyId, _Site, Month2, SearchYear) == true ||
                        ExpectedOnSite(searchKpis, _CompanyId, _Site, Month3, SearchYear) == true)
                    {
                        KpiScoreYtdPercentage = "0";
                        KpiScoreYtd = GetCsaScore(_CompanyId, _Site.SiteId, _QtrId, SearchYear);
                        if (KpiScoreYtd != "0") KpiScoreYtdPercentage = "100";

                        for (int i = 1; i <= upto; i++)
                        {
                            int _QtrId2 = 0;
                            int _SearchYear = _Year;
                            if (i >= 1)
                            {
                                _QtrId2 = 4;
                                _SearchYear = _Year -1; 
                                if (i >= 4)
                                {
                                    _QtrId2 = 1;
                                    _SearchYear = _Year;
                                    if (i >= 7)
                                    {
                                        _QtrId2 = 2;
                                        _SearchYear = _Year;
                                        if (i >= 10)
                                        {
                                            _QtrId2 = 3;
                                            _SearchYear = _Year;
                                        }
                                    }
                                }
                            }
                            if (_CategoryId == 2)
                            {
                                if (_QtrId2 == 4)
                                {
                                    _QtrId2 = 3;
                                }
                                if (_QtrId2 == 2)
                                {
                                    _QtrId2 = 1;
                                }
 
                            }
                            KpiScoreMonth[i] = GetCsaScore(_CompanyId, _Site.SiteId, _QtrId2, _SearchYear);
                            //KpiScoreMonth[i] = GetCsaScore(_CompanyId, _Site.SiteId, _QtrId2, _SearchYear);
                            KpiScoreMonthPercentage[i] = "0";
                            if (KpiScoreMonth[i] != "0" && KpiScoreMonth[i] != "-") KpiScoreMonthPercentage[i] = "100";
                        }

                        //Get the ytd
                        int _ytdYear = 0;
                        int _ytdQtr = 0;
                        int _ytdMonthId;
                        if (_MonthId == 0)
                        {
                            _ytdMonthId = dtNow.Month;
                        }
                        else _ytdMonthId = _MonthId;
                        int _ytdYearId = _Year; 
                        if (_ytdMonthId == 1 || _ytdMonthId == 2 || _ytdMonthId == 3)
                        {
                            _ytdYear = _ytdYearId - 1;
                            _ytdQtr = 4;
                        }
                        if (_ytdMonthId == 4 || _ytdMonthId == 5 || _ytdMonthId == 6)
                        {
                            _ytdYear = _ytdYearId;
                            _ytdQtr = 1;
                        }
                        if (_ytdMonthId == 7 || _ytdMonthId == 8 || _ytdMonthId == 9)
                        {
                            _ytdYear = _ytdYearId;
                            _ytdQtr = 2;
                        }
                        if (_ytdMonthId == 10 || _ytdMonthId == 11 || _ytdMonthId == 12)
                        {
                            _ytdYear = _ytdYearId;
                            _ytdQtr = 3;
                        }
                        if (_CategoryId == 2)
                        {
                            int _ytdCat2Qtr = 0;
                            if (_ytdQtr == 4)
                                _ytdCat2Qtr = 3;
                            if (_ytdQtr == 3)
                                _ytdCat2Qtr = 1;
                            if (_ytdQtr == 2)
                                _ytdCat2Qtr = 1;
                            if (_ytdQtr == 1)
                            {
                                _ytdCat2Qtr = 3;
                                _ytdYear = _ytdYear - 1;
                            }
                            _ytdQtr = _ytdCat2Qtr;
                        }
                        KpiScoreYtd = GetCsaScore(_CompanyId, _Site.SiteId, _ytdQtr, _ytdYear);
                        int csaPrevKpi;
                        int.TryParse(KpiScoreYtd, out csaPrevKpi);
                        if (csaPrevKpi > 0)
                            KpiScoreYtdPercentage = "100";
                        else
                            KpiScoreYtdPercentage = "0";

                    }
                    break;

                #endregion
                #region "IFE : Injury Ratio"
                case "IFE : Injury Ratio":

                    Configuration config = new Configuration();

                    //Change by Sayani For Task# 1
                    PlanYtd = at.IFEInjuryRatioTargetYTD.ToString();
                    PlanMonth = at.IFEInjuryRatioTargetMon.ToString();
                    //PlanYtd = config.GetValue(ConfigList.IfeInjuryRatioYtd);
                    //PlanMonth = config.GetValue(ConfigList.IfeInjuryRatioMonth);

                    if (NoMonths > 0) KpiScoreYtd = "0";

                    int YTDipFATI = 0;
                    int YTDipMTI = 0;
                    int YTDipRDI = 0;
                    int YTDipLTI = 0;
                    int YTDipIFE = 0;
                    int[] ipFATI = new int[13];
                    int[] ipMTI = new int[13];
                    int[] ipRDI = new int[13];
                    int[] ipLTI = new int[13];
                    int[] ipIFE = new int[13];

                    foreach (KpiCompanySiteCategory kcsc in kcscVlist)
                    {
                        //if (kcsc.KpiDateTime.Month <= upto)
                        //{
                        if (kcsc.IpFati != null)
                        {
                            if (kcsc.KpiDateTime.Month <= upto) YTDipFATI += Convert.ToInt32(kcsc.IpFati);
                            ipFATI[kcsc.KpiDateTime.Month] = Convert.ToInt32(kcsc.IpFati);
                        }
                        if (kcsc.IpMti != null)
                        {
                            if (kcsc.KpiDateTime.Month <= upto) YTDipMTI += Convert.ToInt32(kcsc.IpMti);
                            ipMTI[kcsc.KpiDateTime.Month] = Convert.ToInt32(kcsc.IpMti);
                        }
                        if (kcsc.IpRdi != null)
                        {
                            if (kcsc.KpiDateTime.Month <= upto) YTDipRDI += Convert.ToInt32(kcsc.IpRdi);
                            ipRDI[kcsc.KpiDateTime.Month] = Convert.ToInt32(kcsc.IpRdi);
                        }
                        if (kcsc.IpLti != null)
                        {
                            if (kcsc.KpiDateTime.Month <= upto) YTDipLTI += Convert.ToInt32(kcsc.IpLti);
                            ipLTI[kcsc.KpiDateTime.Month] = Convert.ToInt32(kcsc.IpLti);
                        }
                        if (kcsc.IpIfe != null)
                        {
                            if (kcsc.KpiDateTime.Month <= upto) YTDipIFE += Convert.ToInt32(kcsc.IpIfe);
                            ipIFE[kcsc.KpiDateTime.Month] = Convert.ToInt32(kcsc.IpIfe);
                        }
                        //}
                    }

                    for (int i = 1; i <= 12; i++)
                    {
                        if (ExpectedOnSite(kpis, _CompanyId, _Site, i, _Year) == true)
                        {
                            KpiScoreMonth[i] = "0";
                            if ((ipFATI[i] + ipMTI[i] + ipRDI[i] + ipLTI[i]) == 0)
                            {
                                KpiScoreMonth[i] = Convert.ToDecimal(ipIFE[i]).ToString();
                                //KpiScoreMonth[i] = "0";
                            }
                            else
                            {
                                KpiScoreMonth[i] = Decimal.Round((Convert.ToDecimal(ipIFE[i]) /
                                                                    Convert.ToDecimal(ipFATI[i] + ipMTI[i] + ipRDI[i] + ipLTI[i])), 2).ToString();
                            }
                        }
                    }
                    if (YTDipIFE > 0)
                    {

                        if ((YTDipFATI + YTDipMTI + YTDipRDI + YTDipLTI) == 0)
                        {
                            KpiScoreYtd = Convert.ToDecimal(YTDipIFE).ToString();
                            //KpiScoreYtd = "0";
                        }
                        else
                        {
                            Decimal Score = 0;
                            for (int i = 1; i <= upto; i++)
                            {
                                Decimal _Score = 0;
                                Decimal.TryParse(KpiScoreMonth[i], out _Score);
                                Score += _Score;
                            }
                            if (Score > 0)
                            {
                                KpiScoreYtd = Decimal.Round(Convert.ToDecimal(Score / Convert.ToDecimal(NoMonths)), 2).ToString();
                            }
                        }
                    }

                    break;
                #endregion
                #region "Workplace Safety & Compliance"
                case "Workplace Safety & Compliance":
                    PlanYtd = Decimal.Round(((NoMonths / 12.0m) * Convert.ToDecimal(at.WorkplaceSafetyCompliance))).ToString();
                    PlanMonth = Decimal.Round(((1 / 12.0m) * Convert.ToDecimal(at.WorkplaceSafetyCompliance))).ToString();

                    foreach (KpiCompanySiteCategory kcsc in kcscVlist)
                    {
                        if (kcsc.EbiOnSite == true || kcsc.AheaTotalManHours > 0) KpiScoreMonth[kcsc.KpiDateTime.Month] = kcsc.IWsc.ToString();
                    }

                    break;
                #endregion
                #region "Management Health Safety Work Contacts"
                case "Management Health Safety Work Contacts":
                    PlanYtd = Decimal.Round(((NoMonths / 12.0m) * Convert.ToDecimal(at.HealthSafetyWorkContacts))).ToString();
                    PlanMonth = Decimal.Round(((1 / 12.0m) * Convert.ToDecimal(at.HealthSafetyWorkContacts))).ToString();
                    foreach (KpiCompanySiteCategory kcsc in kcscVlist)
                    {
                        if (kcsc.EbiOnSite == true || kcsc.AheaTotalManHours > 0) KpiScoreMonth[kcsc.KpiDateTime.Month] = kcsc.ONoHswc.ToString();

                        int Score = 0;
                        int.TryParse(KpiScoreYtd, out Score);

                        int _Score = 0;
                        int.TryParse(KpiScoreMonth[kcsc.KpiDateTime.Month], out _Score);

                        KpiScoreYtd = (Score + _Score).ToString();
                    }

                    break;
                #endregion
                #region "Behavioural Observations (%)"
                case "Behavioural Observations (%)":
                    PlanYtd = "100";
                    PlanMonth = "100";

                    for (int i = 1; i <= 12; i++)
                    {
                        if (ExpectedOnSite(kpis, _CompanyId, _Site, i, _Year) == true)
                        {
                            KpiScoreMonthPercentage[i] = "0";
                            KpiScoreMonth[i] = "0";
                        }
                    }

                    foreach (KpiCompanySiteCategory kcsc in kcscVlist)
                    {
                        if (kcsc.ONoBsp != null)
                        {
                            Decimal Divider = Convert.ToDecimal(at.BehaviouralSafetyProgram) * Convert.ToDecimal(kcsc.AheaAvgNopplSiteMonth);
                            if (Divider != 0)
                            {
                                if (kcsc.EbiOnSite == true || kcsc.AheaTotalManHours > 0)
                                    KpiScoreMonth[kcsc.KpiDateTime.Month] = Decimal.Round((Convert.ToDecimal(kcsc.ONoBsp / Divider) * 100)).ToString();
                            }
                            else
                            {
                                if (kcsc.EbiOnSite == true || kcsc.AheaTotalManHours > 0) KpiScoreMonth[kcsc.KpiDateTime.Month] = "";
                            }
                        }
                    }

                    break;
                case "Behavioural Observations - Plan":
                    PlanYtd = "-";
                    PlanMonth = "-";

                    int _Total = 0;
                    foreach (KpiCompanySiteCategory kcsc in kcscVlist)
                    {
                        KpiScoreMonth[kcsc.KpiDateTime.Month] = "";
                        if (kcsc.ONoBsp != null)
                        {
                            Decimal Divider = Convert.ToDecimal(at.BehaviouralSafetyProgram) * Convert.ToDecimal(kcsc.AheaAvgNopplSiteMonth);
                            if (Divider != 0)
                            {
                                if (kcsc.EbiOnSite == true || kcsc.AheaTotalManHours > 0)
                                {
                                    KpiScoreMonth[kcsc.KpiDateTime.Month] = Convert.ToInt32(Divider).ToString();
                                    _Total += Convert.ToInt32(Divider);
                                }
                            }
                            else
                            {
                                if (kcsc.EbiOnSite == true || kcsc.AheaTotalManHours > 0) KpiScoreMonth[kcsc.KpiDateTime.Month] = "-";
                            }
                        }
                    }

                    PlanMonth = _Total.ToString();

                    break;
                case "Behavioural Observations - Actual":
                    PlanYtd = "-";
                    PlanMonth = "-";

                    int _Total2 = 0;
                    foreach (KpiCompanySiteCategory kcsc in kcscVlist)
                    {
                        if (kcsc.ONoBsp != null)
                        {
                            Decimal Divider = Convert.ToDecimal(at.BehaviouralSafetyProgram) * Convert.ToDecimal(kcsc.AheaAvgNopplSiteMonth);
                            if (Divider != 0)
                            {
                                if (kcsc.EbiOnSite == true || kcsc.AheaTotalManHours > 0)
                                    KpiScoreMonth_Temp[kcsc.KpiDateTime.Month] = Decimal.Round((Convert.ToDecimal(kcsc.ONoBsp / Divider) * 100)).ToString();
                            }
                            else
                            {
                                if (kcsc.EbiOnSite == true || kcsc.AheaTotalManHours > 0) KpiScoreMonth_Temp[kcsc.KpiDateTime.Month] = "";
                            }
                        }
                    }

                    //

                    foreach (KpiCompanySiteCategory kcsc in kcscVlist)
                    {
                        KpiScoreMonth[kcsc.KpiDateTime.Month] = "-";

                        if (kcsc.EbiOnSite == true || kcsc.AheaTotalManHours > 0) KpiScoreMonth[kcsc.KpiDateTime.Month] = "";

                        if (kcsc.ONoBsp != null)
                        {
                            int onobsp = 0;
                            bool isInt = Int32.TryParse(kcsc.ONoBsp.ToString(), out onobsp);
                            if (isInt)
                            {
                                KpiScoreMonth[kcsc.KpiDateTime.Month] = onobsp.ToString();
                                _Total2 += Convert.ToInt32(onobsp);
                            }
                        }
                    }

                    PlanMonth = _Total2.ToString();

                    break;
                #endregion
                #region "Fatality Prevention Program"
                case "Fatality Prevention Program":
                    PlanYtd = Decimal.Round(((NoMonths / 12.0m) * Convert.ToDecimal(at.FatalityPrevention))).ToString();
                    PlanMonth = Decimal.Round(((1 / 12.0m) * Convert.ToDecimal(at.FatalityPrevention))).ToString();
                    foreach (KpiCompanySiteCategory kcsc in kcscVlist)
                    {
                        if (kcsc.EbiOnSite == true || kcsc.AheaTotalManHours > 0) KpiScoreMonth[kcsc.KpiDateTime.Month] = kcsc.MFatality.ToString();
                    }

                    break;
                #endregion
                #region "JSA Field Audit Verifications"
                case "JSA Field Audit Verifications":
                    PlanYtd = Decimal.Round(((NoMonths / 12.0m) * Convert.ToDecimal(at.JsaScore))).ToString();
                    PlanMonth = Decimal.Round(((1 / 12.0m) * Convert.ToDecimal(at.JsaScore))).ToString();
                    foreach (KpiCompanySiteCategory kcsc in kcscVlist)
                    {
                        if (kcsc.EbiOnSite == true || kcsc.AheaTotalManHours > 0) KpiScoreMonth[kcsc.KpiDateTime.Month] = kcsc.JsaAudits.ToString();
                    }

                    break;
                #endregion
                #region "Toolbox Meetings"
                case "Toolbox Meetings":
                    PlanYtd = Decimal.Round(((NoMonths / 12.0m) * Convert.ToDecimal(at.ToolboxMeetingsPerMonth))).ToString();
                    PlanMonth = Decimal.Round(((1 / 12.0m) * Convert.ToDecimal(at.ToolboxMeetingsPerMonth))).ToString();
                    foreach (KpiCompanySiteCategory kcsc in kcscVlist)
                    {
                        if (kcsc.EbiOnSite == true || kcsc.AheaTotalManHours > 0) KpiScoreMonth[kcsc.KpiDateTime.Month] = kcsc.MTbmpm.ToString();
                    }

                    break;
                #endregion
                #region "Alcoa Weekly Contractors Meeting"
                case "Alcoa Weekly Contractors Meeting":
                    PlanYtd = Decimal.Round(((NoMonths / 12.0m) * Convert.ToDecimal(at.AlcoaWeeklyContractorsMeeting))).ToString();
                    PlanMonth = Decimal.Round(((1 / 12.0m) * Convert.ToDecimal(at.AlcoaWeeklyContractorsMeeting))).ToString();
                    foreach (KpiCompanySiteCategory kcsc in kcscVlist)
                    {
                        if (kcsc.EbiOnSite == true || kcsc.AheaTotalManHours > 0) KpiScoreMonth[kcsc.KpiDateTime.Month] = kcsc.MAwcm.ToString();
                    }

                    break;
                #endregion
                #region "Alcoa Monthly Contractors Meeting"
                case "Alcoa Monthly Contractors Meeting":
                    PlanYtd = Decimal.Round(((NoMonths / 12.0m) * Convert.ToDecimal(at.AlcoaMonthlyContractorsMeeting))).ToString();
                    PlanMonth = Decimal.Round(((1 / 12.0m) * Convert.ToDecimal(at.AlcoaMonthlyContractorsMeeting))).ToString();
                    foreach (KpiCompanySiteCategory kcsc in kcscVlist)
                    {
                        if (kcsc.EbiOnSite == true || kcsc.AheaTotalManHours > 0) KpiScoreMonth[kcsc.KpiDateTime.Month] = kcsc.MAmcm.ToString();
                    }

                    break;
                #endregion
                #region "Safety Plan(s) Submitted"
                case "Safety Plan(s) Submitted":
                    PlanYtd = at.SafetyPlan.ToString();
                    PlanMonth = PlanYtd;
                    //Count # Safety Plans submitted for year
                    FileDbService fService = new FileDbService();
                    DataSet dsSafetyPlans = fService.ComplianceReport_GetByByCompanyIdYear(_CompanyId, dtNowMinusOneMonth.Year);
                    if (dsSafetyPlans.Tables[0].Rows.Count == 1)
                    {
                        if (dsSafetyPlans.Tables[0].Rows[0].ItemArray[0] != null)
                        {
                            KpiScoreYtd = dsSafetyPlans.Tables[0].Rows[0].ItemArray[0].ToString();
                        }
                    }

                    for (int i = 1; i <= 12; i++) KpiScoreMonth[i] = KpiScoreYtd;

                    break;
                #endregion
                #region "Mandated Training"
                case "% Mandated Training":
                    PlanYtd = at.Training.ToString();
                    PlanMonth = at.Training.ToString();

                    foreach (KpiCompanySiteCategory kcsc in kcscVlist)
                    {
                        if (kcsc.EbiOnSite == true || kcsc.AheaTotalManHours > 0) KpiScoreMonth[kcsc.KpiDateTime.Month] = kcsc.Training.ToString();
                    }

                    break;
                #endregion

                    //Added By Sayani for Task# 1

                #region "RN : Injury Ratio"
                case "RN : Injury Ratio":


                    PlanYtd = at.RNInjuryTargetYTD.ToString();
                    PlanMonth = at.RNInjuryRatioTargetMon.ToString();
                    //PlanYtd = config.GetValue(ConfigList.IfeInjuryRatioYtd);
                    //PlanMonth = config.GetValue(ConfigList.IfeInjuryRatioMonth);

                    if (NoMonths > 0) KpiScoreYtd = "0";

                    int YTDipFATI_RN = 0;
                    int YTDipMTI_RN = 0;
                    int YTDipRDI_RN = 0;
                    int YTDipLTI_RN = 0;
                    int YTDipRN = 0;
                    int[] ipFATI_RN = new int[13];
                    int[] ipMTI_RN = new int[13];
                    int[] ipRDI_RN = new int[13];
                    int[] ipLTI_RN = new int[13];
                    int[] ipRN = new int[13];

                    foreach (KpiCompanySiteCategory kcsc in kcscVlist)
                    {
                        //if (kcsc.KpiDateTime.Month <= upto)
                        //{
                        if (kcsc.IpFati != null)
                        {
                            if (kcsc.KpiDateTime.Month <= upto) YTDipFATI_RN += Convert.ToInt32(kcsc.IpFati);
                            ipFATI_RN[kcsc.KpiDateTime.Month] = Convert.ToInt32(kcsc.IpFati);
                        }
                        if (kcsc.IpMti != null)
                        {
                            if (kcsc.KpiDateTime.Month <= upto) YTDipMTI_RN += Convert.ToInt32(kcsc.IpMti);
                            ipMTI_RN[kcsc.KpiDateTime.Month] = Convert.ToInt32(kcsc.IpMti);
                        }
                        if (kcsc.IpRdi != null)
                        {
                            if (kcsc.KpiDateTime.Month <= upto) YTDipRDI_RN += Convert.ToInt32(kcsc.IpRdi);
                            ipRDI_RN[kcsc.KpiDateTime.Month] = Convert.ToInt32(kcsc.IpRdi);
                        }
                        if (kcsc.IpLti != null)
                        {
                            if (kcsc.KpiDateTime.Month <= upto) YTDipLTI_RN += Convert.ToInt32(kcsc.IpLti);
                            ipLTI_RN[kcsc.KpiDateTime.Month] = Convert.ToInt32(kcsc.IpLti);
                        }
                        if (kcsc.IpRN != null)
                        {
                            if (kcsc.KpiDateTime.Month <= upto) YTDipRN += Convert.ToInt32(kcsc.IpRN);
                            ipRN[kcsc.KpiDateTime.Month] = Convert.ToInt32(kcsc.IpRN);
                        }
                        //}
                    }

                    for (int i = 1; i <= 12; i++)
                    {
                        if (ExpectedOnSite(kpis, _CompanyId, _Site, i, _Year) == true)
                        {
                            KpiScoreMonth[i] = "0";
                            if ((ipFATI_RN[i] + ipMTI_RN[i] + ipRDI_RN[i] + ipLTI_RN[i]) == 0)
                            {
                                KpiScoreMonth[i] = Convert.ToDecimal(ipRN[i]).ToString();
                                //KpiScoreMonth[i] = "0";
                            }
                            else
                            {
                                KpiScoreMonth[i] = Decimal.Round((Convert.ToDecimal(ipRN[i]) /
                                                                    Convert.ToDecimal(ipFATI_RN[i] + ipMTI_RN[i] + ipRDI_RN[i] + ipLTI_RN[i])), 2).ToString();
                            }
                        }
                    }
                    if (YTDipRN > 0)
                    {

                        if ((YTDipFATI_RN + YTDipMTI_RN + YTDipRDI_RN + YTDipLTI_RN) == 0)
                        {
                            KpiScoreYtd = Convert.ToDecimal(YTDipRN).ToString();
                            //KpiScoreYtd = "0";
                        }
                        else
                        {
                            Decimal Score = 0;
                            for (int i = 1; i <= upto; i++)
                            {
                                Decimal _Score = 0;
                                Decimal.TryParse(KpiScoreMonth[i], out _Score);
                                Score += _Score;
                            }
                            if (Score > 0)
                            {
                                KpiScoreYtd = Decimal.Round(Convert.ToDecimal(Score / Convert.ToDecimal(NoMonths)), 2).ToString();
                            }
                        }
                    }

                    break;
                #endregion

                    //End

                default:
                    #region "Training Schedules" / "Medical Schedules"
                    if (KpiMeasure == "Training Schedules" || KpiMeasure == "Medical Schedules")
                    {
                        PlanYtd = "1";
                        PlanMonth = PlanYtd;

                        int _SearchYear_TS = _Year;

                        for (int i = 1; i <= 12; i++) dr[String.Format("KpiScoreMonth{0}", i)] = "0";
                        for (int i = 1; i <= 12; i++) dr[String.Format("KpiScoreMonth{0}Percentage", i)] = "0";

                        FileDbMedicalTrainingService fdbmtService = new FileDbMedicalTrainingService();
                        string MSTS = "";
                        if(KpiMeasure == "Training Schedules") MSTS = "TS";
                        if(KpiMeasure == "Medical Schedules") MSTS = "MS";

                        DataSet dsFdbmt = fdbmtService.ComplianceReport_ByCompany(_SearchYear_TS, MSTS, _CompanyId, _Site.SiteId, "%");

                        //Get all kpis for the year
                        var lstSearchMonthYearTS = new List<DateTime>();
                        for (int i = 1; i <= 12; i++)
                        {
                            lstSearchMonthYearTS.Add(new DateTime(_SearchYear_TS, i, 1));
                        }
                        var searchTSKpis = kpiService.GetMany(null, e => e.CompanyId == _CompanyId && e.SiteId == _Site.SiteId && lstSearchMonthYearTS.Contains(e.kpiDateTime), null, null);

                        if (dsFdbmt.Tables[0] != null)
                        {
                            if (dsFdbmt.Tables[0].Rows.Count == 1)
                            {
                                foreach (DataRow drFdbmt in dsFdbmt.Tables[0].Rows)
                                {
                                    KpiScoreMonth[1] = drFdbmt["Qtr1"].ToString();
                                    KpiScoreMonth[2] = drFdbmt["Qtr1"].ToString();
                                    KpiScoreMonth[3] = drFdbmt["Qtr1"].ToString();
                                    KpiScoreMonth[4] = drFdbmt["Qtr2"].ToString();
                                    KpiScoreMonth[5] = drFdbmt["Qtr2"].ToString();
                                    KpiScoreMonth[6] = drFdbmt["Qtr2"].ToString();
                                    KpiScoreMonth[7] = drFdbmt["Qtr3"].ToString();
                                    KpiScoreMonth[8] = drFdbmt["Qtr3"].ToString();
                                    KpiScoreMonth[9] = drFdbmt["Qtr3"].ToString();
                                    KpiScoreMonth[10] = drFdbmt["Qtr4"].ToString();
                                    KpiScoreMonth[11] = drFdbmt["Qtr4"].ToString();
                                    KpiScoreMonth[12] = drFdbmt["Qtr4"].ToString();

                                    KpiScoreMonthPercentage[1] = drFdbmt["Qtr1"].ToString();
                                    KpiScoreMonthPercentage[2] = drFdbmt["Qtr1"].ToString();
                                    KpiScoreMonthPercentage[3] = drFdbmt["Qtr1"].ToString();
                                    KpiScoreMonthPercentage[4] = drFdbmt["Qtr2"].ToString();
                                    KpiScoreMonthPercentage[5] = drFdbmt["Qtr2"].ToString();
                                    KpiScoreMonthPercentage[6] = drFdbmt["Qtr2"].ToString();
                                    KpiScoreMonthPercentage[7] = drFdbmt["Qtr3"].ToString();
                                    KpiScoreMonthPercentage[8] = drFdbmt["Qtr3"].ToString();
                                    KpiScoreMonthPercentage[9] = drFdbmt["Qtr3"].ToString();
                                    KpiScoreMonthPercentage[10] = drFdbmt["Qtr4"].ToString();
                                    KpiScoreMonthPercentage[11] = drFdbmt["Qtr4"].ToString();
                                    KpiScoreMonthPercentage[12] = drFdbmt["Qtr4"].ToString();
                                }
                            }
                        }

                        if (ExpectedOnSite(searchTSKpis, _CompanyId, _Site, 1, _SearchYear_TS) == true ||
                            ExpectedOnSite(searchTSKpis, _CompanyId, _Site, 2, _SearchYear_TS) == true ||
                            ExpectedOnSite(searchTSKpis, _CompanyId, _Site, 3, _SearchYear_TS) == true)
                        {

                        }
                        else
                        {
                            if (KpiScoreMonth[1].ToString() == "0") KpiScoreMonth[1] = "-";
                            if (KpiScoreMonth[2].ToString() == "0") KpiScoreMonth[2] = "-";
                            if (KpiScoreMonth[3].ToString() == "0") KpiScoreMonth[3] = "-";
                            if (KpiScoreMonthPercentage[1].ToString() == "0") KpiScoreMonthPercentage[1] = "-";
                            if (KpiScoreMonthPercentage[2].ToString() == "0") KpiScoreMonthPercentage[2] = "-";
                            if (KpiScoreMonthPercentage[3].ToString() == "0") KpiScoreMonthPercentage[3] = "-";
                        }
                        if (ExpectedOnSite(searchTSKpis, _CompanyId, _Site, 4, _SearchYear_TS) == true ||
                            ExpectedOnSite(searchTSKpis, _CompanyId, _Site, 5, _SearchYear_TS) == true ||
                            ExpectedOnSite(searchTSKpis, _CompanyId, _Site, 6, _SearchYear_TS) == true)
                        {

                        }
                        else
                        {
                            if (KpiScoreMonth[4].ToString() == "0") KpiScoreMonth[4] = "-";
                            if (KpiScoreMonth[5].ToString() == "0") KpiScoreMonth[5] = "-";
                            if (KpiScoreMonth[6].ToString() == "0") KpiScoreMonth[6] = "-";
                            if (KpiScoreMonthPercentage[4].ToString() == "0") KpiScoreMonthPercentage[4] = "-";
                            if (KpiScoreMonthPercentage[5].ToString() == "0") KpiScoreMonthPercentage[5] = "-";
                            if (KpiScoreMonthPercentage[6].ToString() == "0") KpiScoreMonthPercentage[6] = "-";
                        }
                        if (ExpectedOnSite(searchTSKpis, _CompanyId, _Site, 7, _SearchYear_TS) == true ||
                            ExpectedOnSite(searchTSKpis, _CompanyId, _Site, 8, _SearchYear_TS) == true ||
                            ExpectedOnSite(searchTSKpis, _CompanyId, _Site, 9, _SearchYear_TS) == true)
                        {
                            if (KpiScoreMonth[7].ToString() == "0") KpiScoreMonth[7] = "-";
                            if (KpiScoreMonth[8].ToString() == "0") KpiScoreMonth[8] = "-";
                            if (KpiScoreMonth[9].ToString() == "0") KpiScoreMonth[9] = "-";
                            if (KpiScoreMonthPercentage[7].ToString() == "0") KpiScoreMonthPercentage[7] = "-";
                            if (KpiScoreMonthPercentage[8].ToString() == "0") KpiScoreMonthPercentage[8] = "-";
                            if (KpiScoreMonthPercentage[9].ToString() == "0") KpiScoreMonthPercentage[9] = "-";
                        }
                        else
                        {
                        }
                        if (ExpectedOnSite(searchTSKpis, _CompanyId, _Site, 10, _SearchYear_TS) == true ||
                            ExpectedOnSite(searchTSKpis, _CompanyId, _Site, 11, _SearchYear_TS) == true ||
                            ExpectedOnSite(searchTSKpis, _CompanyId, _Site, 12, _SearchYear_TS) == true)
                        {

                        }
                        else
                        {
                            if (KpiScoreMonth[10].ToString() == "0") KpiScoreMonth[10] = "-";
                            if (KpiScoreMonth[11].ToString() == "0") KpiScoreMonth[11] = "-";
                            if (KpiScoreMonth[12].ToString() == "0") KpiScoreMonth[12] = "-";
                            if (KpiScoreMonthPercentage[10].ToString() == "0") KpiScoreMonthPercentage[10] = "-";
                            if (KpiScoreMonthPercentage[11].ToString() == "0") KpiScoreMonthPercentage[11] = "-";
                            if (KpiScoreMonthPercentage[12].ToString() == "0") KpiScoreMonthPercentage[12] = "-";
                        }

                        if (dtNowMinusOneMonth.Month <= 9)
                        {
                            if (KpiScoreMonth[10].ToString() == "0" && KpiScoreMonth[11].ToString() == "0" && KpiScoreMonth[12].ToString() == "0")
                            {
                                KpiScoreMonth[10] = "-";
                                KpiScoreMonth[11] = "-";
                                KpiScoreMonth[12] = "-";
                                KpiScoreMonthPercentage[10] = "-";
                                KpiScoreMonthPercentage[11] = "-";
                                KpiScoreMonthPercentage[12] = "-";
                            }
                            if (dtNowMinusOneMonth.Month <= 6)
                            {
                                if (KpiScoreMonth[7].ToString() == "0" && KpiScoreMonth[8].ToString() == "0" && KpiScoreMonth[9].ToString() == "0")
                                {
                                    KpiScoreMonth[7] = "-";
                                    KpiScoreMonth[8] = "-";
                                    KpiScoreMonth[9] = "-";
                                    KpiScoreMonthPercentage[7] = "-";
                                    KpiScoreMonthPercentage[8] = "-";
                                    KpiScoreMonthPercentage[9] = "-";
                                }
                                if (dtNowMinusOneMonth.Month <= 3)
                                {
                                    if (KpiScoreMonth[4].ToString() == "0" && KpiScoreMonth[5].ToString() == "0" && KpiScoreMonth[6].ToString() == "0")
                                    {
                                        KpiScoreMonth[4] = "-";
                                        KpiScoreMonth[5] = "-";
                                        KpiScoreMonth[6] = "-";
                                        KpiScoreMonthPercentage[4] = "-";
                                        KpiScoreMonthPercentage[5] = "-";
                                        KpiScoreMonthPercentage[6] = "-";
                                    }
                                }
                            }
                        }
                    }
                #endregion
                    break;
            }


            #region "Sum/Avg YTD Score & Calculate Percentage"
            if (KpiMeasure != "On site" &&
                KpiMeasure != "Alcoa Annual Audit Score (%)" &&
                !KpiMeasure.Contains("CSA - Self Audit (% Score)") &&
                KpiMeasure != "Management Health Safety Work Contacts" &&
                KpiMeasure != "Safety Plan(s) Submitted" &&
                KpiMeasure != "IFE : Injury Ratio" &&
                KpiMeasure != "RN : Injury Ratio" && // Pankaj - RN error
                KpiMeasure != "Behavioural Observations - Plan"
                )
            {
                if (NoMonths > 0)
                {
                    for (int i = 1; i <= upto; i++)
                    {
                        if (ExpectedOnSite(kpis, _CompanyId, _Site, i, _Year) == true)
                        {
                            string _KpiScoreMonth = KpiScoreMonth[i];
                            if (KpiMeasure == "Behavioural Observations - Actual") _KpiScoreMonth = KpiScoreMonth_Temp[i];

                            Decimal Score = 0;
                            Decimal.TryParse(KpiScoreYtd, out Score);
                            Decimal _Score = 0;
                            Decimal.TryParse(_KpiScoreMonth, out _Score);
                            KpiScoreYtd = (Score + _Score).ToString(); //SUM
                        }
                    }

                    if (KpiMeasure == "Behavioural Observations (%)" ||
                        KpiMeasure == "% Mandated Training") //AVERAGE
                    {
                        Decimal Score = 0;
                        Decimal.TryParse(KpiScoreYtd, out Score);
                        if (Score != 0)
                        {
                            KpiScoreYtd = Decimal.Round((Score / NoMonths)).ToString();
                        }
                        else
                        {
                            if (PlanYtd == "0") KpiScoreYtdPercentage = "100";
                        }
                    }

                }
            }

            if (KpiMeasure != "On site" &&
                KpiMeasure != "Alcoa Annual Audit Score (%)" &&
                !KpiMeasure.Contains("CSA - Self Audit (% Score)"))
            {
                //DT 3201 Changes
                
                if (PlanYtd != "0" && PlanYtd != "0.0" && PlanYtd != "0.00")                
                {
                    Decimal Top = 0;
                    Decimal Bottom = 0;
                    Decimal.TryParse(PlanYtd, out Bottom);
                    Decimal.TryParse(KpiScoreYtd, out Top);

                    if (Bottom != 0 && KpiScoreYtd != "-")
                    {
                        Decimal Score = (Top / Bottom) * 100;
                        if (Score > 100) Score = 100;
                        KpiScoreYtdPercentage = Decimal.Round(Score).ToString();
                    }
                    
                }
                else if (PlanYtd == "0" || PlanYtd == "0.0" || PlanYtd == "0.00")
                {
                    //This needs to be un commented
                    KpiScoreYtdPercentage = "100"; //Uncommented by Ashley Goldstraw 30/9/2015 DT319
                }
                if (PlanMonth != "0")
                {
                    for (int i = 1; i <= upto; i++)
                    {
                        Decimal Top = 0;
                        Decimal Bottom = 0;
                        Decimal.TryParse(PlanMonth, out Bottom);
                        Decimal.TryParse(KpiScoreMonth[i], out Top);

                        if (Bottom != 0 && KpiScoreMonth[i] != "-")
                        {
                            Decimal Score = (Top / Bottom) * 100;
                            if (Score > 100) Score = 100;
                            KpiScoreMonthPercentage[i] = Decimal.Round(Score).ToString();
                        }
                    }
                }
            }
            #endregion

            dr["KpiMeasure"] = KpiMeasure;
            dr["PlanYtd"] = PlanYtd;
            dr["PlanMonth"] = PlanMonth;
            if (_MonthId == 0)
            {
                dr["KpiScoreYtd"] = KpiScoreYtd;                
                dr["KpiScoreYtdPercentage"] = KpiScoreYtdPercentage;
            }
            else
            {
                int _MonthId2 = _MonthId;
                if (KpiMeasure == "CSA - Self Audit (% Score) - Previous Qtr")
                {
                    int _QtrId = 0;
                    if (dtNowMinusOneMonth.Month >= 1)
                    {
                        _QtrId = 1;
                        if (dtNowMinusOneMonth.Month >= 4)
                        {
                            _QtrId = 2;
                            if (dtNowMinusOneMonth.Month >= 7)
                            {
                                _QtrId = 3;
                                if (dtNowMinusOneMonth.Month >= 10)
                                {
                                    _QtrId = 4;
                                }
                            }
                        }
                    }
                    int SearchYear = _Year;
                    if (_QtrId == 1)
                    {
                        _QtrId = 1; //Changed from 4 to 1 as already offset in data
                        SearchYear = _Year; //-1 Removed by AG
                    }
                    else if (_QtrId == 4 && SearchYear != DateTime.Now.Year)
                    {
                        //do nothing.
                    }
                    else
                    {
                        _QtrId = _QtrId + 0; //-1 Removed by AG
                    }

                    switch (_QtrId)
                    {
                        case 1:
                            _MonthId2 = 1;

                            break;
                        case 2:
                            _MonthId2 = 4;
                            break;
                        case 3:
                            _MonthId2 = 7;
                            break;
                        case 4:
                            _MonthId2 = 10;
                            break;
                        default:
                            break;
                    }
                }

                dr["PlanYtd"] = dr["PlanMonth"];
                dr["KpiScoreYtd"] = KpiScoreMonth[_MonthId2];
                dr["KpiScoreYtdPercentage"] = KpiScoreMonthPercentage[_MonthId2];
            }

            for (int i = 1; i <= 12; i++) dr[String.Format("KpiScoreMonth{0}", i)] = KpiScoreMonth[i];
            for (int i = 1; i <= 12; i++) dr[String.Format("KpiScoreMonth{0}Percentage", i)] = KpiScoreMonthPercentage[i];

            if (KpiMeasure == "Behavioural Observations - Plan" || KpiMeasure == "Behavioural Observations - Actual")
            {
                dr["KpiScoreYtd"] = KpiScoreMonth_Temp[_MonthId];
                dr["KpiScoreYtdPercentage"] = KpiScoreMonthPercentage_Temp[_MonthId];
            }

            if (KpiMeasure == "Behavioural Observations (%)")
            {
                int _temp = 0;
                bool _kpiscore_ytd = Int32.TryParse(dr["KpiScoreYtd"].ToString(), out _temp);
                if(_kpiscore_ytd)
                {
                    if (_temp > 100) dr["KpiScoreYtd"] = "100";
                }

                _temp = 0;

                bool _kpiscore_ytdperc = Int32.TryParse(dr["KpiScoreYtdPercentage"].ToString(), out _temp);
                if(_kpiscore_ytdperc)
                {
                    if (_temp > 100) dr["KpiScoreYtdPercentage"] = "100";
                }

                for (int i = 1; i <= 12; i++)
                {
                    if (dr[String.Format("KpiScoreMonth{0}", i)].ToString() != "-")
                    {
                        _temp = 0;
                        bool _change = Int32.TryParse(dr[String.Format("KpiScoreMonth{0}", i)].ToString(), out _temp);
                        if (_change)
                        {
                            if (_temp > 100) dr[String.Format("KpiScoreMonth{0}", i)] = "100";
                        }
                    }
                }
            }
            

            return dr;
        }
        public static string GetCsaScore(int _CompanyId, int _SiteId, int _QtrId, int _Year)
        {
            string TotalRating = "0";
            CsaService csaService = new CsaService();
            Csa csa = csaService.GetByCompanyIdSiteIdQtrIdYear(_CompanyId, _SiteId, _QtrId, _Year);
            if (csa != null)
            {
                if (csa.TotalRating != null)
                {
                    if (csa.TotalRating > 0) TotalRating = csa.TotalRating.ToString();
                }
            }
            return TotalRating;
        }

        public static string GetRateScore(string rate, int _CompanyId, int _SiteId, int _CategoryId, int _MonthId, int _Year)
        {
            string RateScore = "";

            KpiCompanySiteCategoryService kcscService = new KpiCompanySiteCategoryService();
            DataSet dsKpiData;

            if (_MonthId == 0)
            {
                dsKpiData = kcscService.Compliance_Table_SS(_CompanyId, _SiteId, _Year, _CategoryId.ToString());
            }
            else
            {
                dsKpiData = kcscService.Compliance_Table_SS_Month(_CompanyId, _SiteId, _MonthId, _Year, _CategoryId.ToString());
            }

            foreach (DataRow dr in dsKpiData.Tables[0].Rows)
            {
                RateScore = dr[rate].ToString();
            }

            return RateScore;
        }
        public static bool? ExpectedOnSite(List<Repo.CSMS.DAL.EntityModels.Kpi> _Kpis, int _CompanyId, Repo.CSMS.DAL.EntityModels.Site _Site, int _MonthId, int _Year)
        {
            bool? _ExpectedOnSite = null;
            DateTime dtSearchMonthYear = new DateTime(_Year, _MonthId, 1);

            var kpi = _Kpis.Where(e => e.CompanyId == _CompanyId && e.SiteId == _Site.SiteId && e.kpiDateTime == dtSearchMonthYear).FirstOrDefault();
            if (!String.IsNullOrEmpty(_Site.SiteNameEbi))
            {
                _ExpectedOnSite = false;
            }

            if (kpi != null)
            {
                if (kpi.EbiOnSite == true || kpi.aheaTotalManHours > 0)
                {
                    _ExpectedOnSite = true;
                }
            }

            return _ExpectedOnSite;
        }
        public static int GetCountMonthsExpectedOnSite(int _CompanyId, int _SiteId, int upto, int _Year)
        {
            int ExpectedCount = 0;
            
            var kService = ALCOA.CSMS.Website.Global.GetInstance<Repo.CSMS.Service.Database.IKpiService>();

            var lstsearchMonthYear = new List<DateTime>();
            for (int i = 1; i <= upto; i++)
            {
                lstsearchMonthYear.Add(new DateTime(_Year, i, 1));
            }

            var kpis = kService.GetMany(null, e => e.CompanyId == _CompanyId && e.SiteId == _SiteId && lstsearchMonthYear.Contains(e.kpiDateTime), null, null);

            foreach (var kpi in kpis)
            {
                if (kpi.EbiOnSite == true || kpi.aheaTotalManHours > 0)
                {
                    ExpectedCount++;
                }
            }

            return ExpectedCount;
        }



        public static int[] GetResidentialCategories(int CompanyId, int SiteId)
        {
            int[] i = new int[1];
            CompanySiteCategoryStandardService cscsService = new CompanySiteCategoryStandardService();
            CompanySiteCategoryStandard cscs = cscsService.GetByCompanyIdSiteId(CompanyId, SiteId);

            bool error = false;

            if (cscs == null) error = true;
            else if (cscs.CompanySiteCategoryId == null) error = true;

            if (error)
                throw new Exception("Residential Category not assigned for selected Company and Site.");
            else
            {
                i[0] = Convert.ToInt32(cscs.CompanySiteCategoryId);
            }
            return i;
        }
        public static int[] GetResidentialCategories()
        {
            int[] i = null;
            CompanySiteCategoryService cscService = new CompanySiteCategoryService();
            TList<CompanySiteCategory> cscList = cscService.GetAll();

            bool error = false;

            if (cscList == null) error = true;
            else if (cscList.Count == 0) error = true;

            if (error)
                throw new Exception("Residential Categories not available.");
            {
                i = new int[cscList.Count];
                int _i = 0;
                i[0] = 0;
                foreach (CompanySiteCategory csc in cscList)
                {
                    i[_i] = csc.CompanySiteCategoryId;
                    _i++;
                }
            }
            return i;
        }
        public static void CheckAnnualTargets(int[] CategoryList, int Year)
        {
            foreach (int CategoryId in CategoryList)
            {
                AnnualTargetsService atService = new AnnualTargetsService();
                AnnualTargets at = atService.GetByYearCompanySiteCategoryId(Year, CategoryId);
                if (at == null)
                {
                    int count = 0;
                    AnnualTargetsFilters atFilters = new AnnualTargetsFilters();
                    atFilters.Append(AnnualTargetsColumn.CompanySiteCategoryId, CategoryId.ToString());
                    //Changes done for DT#2681
                    atFilters.AppendLessThanOrEqual(AnnualTargetsColumn.Year, Year.ToString());
                    TList<AnnualTargets> atList = DataRepository.AnnualTargetsProvider.GetPaged(
                                                    atFilters.ToString(), "Year DESC", 0, 100, out count);
                    if (count > 0)
                    { //Using a Previous Year's Annual Targets.
                        at = atList[0].Copy();
                    }
                    else
                    {
                        CompanySiteCategoryService cscService = new CompanySiteCategoryService();
                        CompanySiteCategory csc = cscService.GetByCompanySiteCategoryId(CategoryId);
                        throw new Exception("Cannot find Annual Targets for '" + csc.CategoryDesc + "'");
                    }
                }
                else
                {
                    if (CategoryId != (int)CompanySiteCategoryList.NE3)
                    { //Embedded, Non-Embedded 1, Non-Embedded 2
                        if (at.QuarterlyAuditScore == null) throw new Exception("Annual Targets Not Set (EHS Audits)");
                        if (at.JsaScore == null) throw new Exception("Annual Targets Not Set (JSA Field Audit Verifications)");
                        if (at.WorkplaceSafetyCompliance == null) throw new Exception("Annual Targets Not Set (Workplace Safety & Compliance Inspection)");
                        if (at.HealthSafetyWorkContacts == null) throw new Exception("Annual Targets Not Set (Management Safety Work Contacts)");
                        if (at.BehaviouralSafetyProgram == null) throw new Exception("Annual Targets Not Set (Behavioural Observations)");
                        if (at.ToolboxMeetingsPerMonth == null) throw new Exception("Annual Targets Not Set (Toolbox Meetings)");
                        if (at.AlcoaMonthlyContractorsMeeting == null) throw new Exception("Annual Targets Not Set (Alcoa Monthly Contractors Meeting)");
                        if (at.AlcoaWeeklyContractorsMeeting == null) throw new Exception("Annual Targets Not Set (Alcoa Weekly Contractors Meeting)");
                        if (at.Training == null) throw new Exception("Annual Targets Not Set (Training)");

                        if (at.Aifr == null) throw new Exception("Annual Targets Not Set (AIFR)");
                        if (at.Trifr == null) throw new Exception("Annual Targets Not Set (TRIFR)");
                        if (at.SafetyPlan == null) throw new Exception("Annual Targets Not Set (Safety Plan)");
                        if (at.FatalityPrevention == null) throw new Exception("Annual Targets Not Set (Fatality Prevention)");

                        if (at.MedicalSchedule == null) throw new Exception("Annual Targets Not Set (Medical Schedule)");
                        if (at.TrainingSchedule == null) throw new Exception("Annual Targets Not Set (Training Schedule)");

                        if (CategoryId != (int)CompanySiteCategoryList.NE2)
                        { //Not Required for Non-Embedded 2
                            if (at.EhsAudits == null) throw new Exception("Annual Targets Not Set (EHS Audits)");
                        }
                    }
                    else // Non-Embedded 3
                    {
                        if (at.JsaScore == null) throw new Exception("Annual Targets Not Set (JSA Field Audit Verifications)");
                        if (at.AlcoaMonthlyContractorsMeeting == null) throw new Exception("Annual Targets Not Set (Alcoa Monthly Contractors Meeting)");
                        if (at.Training == null) throw new Exception("Annual Targets Not Set (Training)");
                    }
                }
            }
        }


        public static int AddGridColumn(ASPxGridView grid, string FieldName, string ToolTip, string Caption, int? Width, int Index)
        {
            GridViewDataTextColumn gvCol = new GridViewDataTextColumn();
            gvCol.VisibleIndex = Index;
            if (Width != null) gvCol.Width = Unit.Pixel(Convert.ToInt32(Width));
            gvCol.FieldName = FieldName;
            gvCol.Name = FieldName;
            gvCol.ToolTip = ToolTip;
            gvCol.Caption = Caption;

            if (FieldName == "KpiMeasure" || FieldName.Contains("CompaniesNotCompliant"))
            {
                gvCol.CellStyle.HorizontalAlign = HorizontalAlign.Left;
                if (FieldName.Contains("CompaniesNotCompliant"))
                {
                    gvCol.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                }
            }
            else
            {
                gvCol.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                gvCol.CellStyle.HorizontalAlign = HorizontalAlign.Center;
            }

            if (FieldName == "WAO" || FieldName == "VIC")
            {
                gvCol.HeaderStyle.Font.Bold = true;
            }

            grid.Columns.Add(gvCol);
            Index = Index + 1;
            return Index;
        }
        public static int AddGridColumn_Hyperlink(ASPxGridView grid, string FieldName, string ToolTip, string Caption, int? Width, int Index)
        {
            GridViewDataHyperLinkColumn gvCol = new GridViewDataHyperLinkColumn();
            gvCol.VisibleIndex = Index;
            if (Width != null) gvCol.Width = Unit.Pixel(Convert.ToInt32(Width));
            gvCol.FieldName = FieldName;
            gvCol.Name = FieldName;
            gvCol.ToolTip = ToolTip;
            gvCol.Caption = Caption;

            if (FieldName == "KpiMeasure")
            {
                gvCol.CellStyle.HorizontalAlign = HorizontalAlign.Left;
            }
            else
            {
                gvCol.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                gvCol.CellStyle.HorizontalAlign = HorizontalAlign.Center;
            }

            if (FieldName == "WAO" || FieldName == "VIC")
            {
                gvCol.HeaderStyle.Font.Bold = true;
            }

            grid.Columns.Add(gvCol);
            Index = Index + 1;
            return Index;
        }

        public static int AddGridColumn(ASPxGridView grid, string FieldName, string ToolTip, int? Width, int Index)
        {
            return AddGridColumn(grid, FieldName, ToolTip, "", Width, Index);
        }
        public static int AddGridColumn(ASPxGridView grid, string FieldName, int? Width, int Index)
        {
            return AddGridColumn(grid, FieldName, "", "", Width, Index);
        }

        public static void SetKpiScore(Repo.CSMS.DAL.EntityModels.AdHoc_Radar_Items ahri, string monthAbbrev, string score)
        {
            if (ahri != null)
            {
                if (monthAbbrev.ToUpper() == "YEAR")
                {
                    ahri.KpiScoreYtd = score;
                }
                else
                {
                    PropertyInfo prop = ahri.GetType().GetProperty("KpiScore" + monthAbbrev, BindingFlags.Public | BindingFlags.Instance);
                    if (prop != null && prop.CanWrite)
                    {
                        prop.SetValue(ahri, score, null);
                    }
                }
            }
        }

        public static string GetKpiScore(Repo.CSMS.DAL.EntityModels.AdHoc_Radar_Items ahri, Repo.CSMS.DAL.EntityModels.Month month)
        {
            string kpiScore = "n/a";

            string monthAbbrev = "";
            if (month != null)
                monthAbbrev = char.ToUpper(month.MonthAbbrev[0]) + ((month.MonthAbbrev.Length > 1) ? month.MonthAbbrev.Substring(1).ToLower() : string.Empty);  //Make first char uppercase
            else
                monthAbbrev = "Year";

            if (ahri != null)
            {
                if (monthAbbrev.ToUpper() == "YEAR")
                {
                    kpiScore = ahri.KpiScoreYtd;
                }
                else
                {
                    PropertyInfo prop = ahri.GetType().GetProperty("KpiScore" + monthAbbrev, BindingFlags.Public | BindingFlags.Instance);
                    if (prop != null && prop.CanWrite)
                    {
                        kpiScore = (prop.GetValue(ahri) != null ? prop.GetValue(ahri).ToString() : "n/a");
                    }
                }
            }

            return kpiScore;
        }
    }
}

public static class StyleReferenceManager
{
    private static Dictionary<Type, string> defaultCssStyleInfo = new Dictionary<Type, string>();

    static StyleReferenceManager()
    {
        defaultCssStyleInfo.Add(typeof(ASPxWebControl), "DevExpress.Web.Css.Default.css");
        defaultCssStyleInfo.Add(typeof(ASPxEdit), "DevExpress.Web.ASPxEditors.Css.Default.css");
        defaultCssStyleInfo.Add(typeof(ASPxGridView), "DevExpress.Web.ASPxGridView.Css.default.css");
    }

    public static void AddStyleLinksToHead(Page page)
    {
        foreach (KeyValuePair<Type, string> pair in defaultCssStyleInfo)
        {
            string url = page.ClientScript.GetWebResourceUrl(pair.Key, pair.Value);
            WebControl link = new WebControl(HtmlTextWriterTag.Link);
            link.Attributes["type"] = "text/css";
            link.Attributes["rel"] = "stylesheet";
            link.Attributes["href"] = url;
            page.Header.Controls.Add(link);
        }
    }



}


public class Utility
{
    public static string GetKpiByIndex(int index)
    {
        string retValue = string.Empty;
        switch (index)
        {

            case 0:
                {
                    retValue = "Lost Work Days Frequency Rate   LWDFR";
                    break;
                }
            case 1:
                {
                    retValue = "Total Recordable Injuries Frequency Rate    TRIFR";
                    break;
                }
            case 2:
                {
                    retValue = "All Injury Frequency Rate   AIFR";
                    break;
                }
            case 3:
                {
                    retValue = "DART";
                    break;
                }
            case 4:
                {
                    retValue = "Lost Work Day   LWD";
                    break;
                }
            case 5:
                {
                    retValue = "Restricted Duty   RST";
                    break;
                }
            case 6:
                {
                    retValue = "Medical Treatments   MT";
                    break;
                }
            case 7:
                {
                    retValue = "First Aids   FA";
                    break;
                }
            case 8:
                {
                    retValue = "Injury Free Events   IFE";
                    break;
                }
            case 9:
                {
                    retValue = "Risk Notifications";
                    break;
                }
            case 10:
                {
                    retValue = "IFE / Injury ratio";
                    break;
                }
            case 11:
                {
                    retValue = "RN / Injury ratio";
                    break;
                }
            case 12:
                {
                    retValue = "Expired - Currently With Procurement";
                    break;
                }
            case 13:
                {
                    retValue = "Expired - Currently With Supplier";
                    break;
                }
            case 14:
                {
                    retValue = "Expiring  > 7 days";
                    break;
                }
            case 15:
                {
                    retValue = "Questionnaire  > 7 days";
                    break;
                }
            case 16:
                {
                    retValue = "Assign Company Status   > 7 days";
                    break;
                }
            case 17:
                {
                    retValue = "Questionnaire  > 28 Days";
                    break;
                }
            case 18:
                {
                    retValue = "Questionnaire - Resubmitting";
                    break;
                }
            case 19:
                {
                    retValue = "Access > 7 Days";
                    break;
                }
            case 20:
                {
                    retValue = "Expired - Currently With H&S Assessor";
                    break;
                }
            case 21:
                {
                    retValue = "Assessing Procurement Questionnaire > 7 days";
                    break;
                }
            case 22:
                {
                    retValue = "Assessing Questionnaire > 7 days";
                    break;
                }
            case 23:
                {
                    retValue = "SQ process total";
                    break;
                }
            case 24:
                {
                    retValue = "2) Yes - SQ Current (expiring. in 60 days) / Site access granted";
                    break;
                }
            case 25:
                {
                    retValue = "3) No - Current SQ not found / Access to Site Not Granted";
                    break;
                }
            case 26:
                {
                    retValue = "4) No - SQ Expired / Access to Site Not Granted";
                    break;
                }
            case 27:
                {
                    retValue = "5) No - Access to Site Not Granted";
                    break;
                }
            case 28:
                {
                    retValue = "6) No - Safety Qualification not found";
                    break;
                }
            case 29:
                {
                    retValue = "7) No - Information could not be found";
                    break;
                }
            case 30:
                {
                    retValue = "8) Company has a valid SQ exemption";
                    break;
                }
            case 31:
                {
                    retValue = "Required (# Companies)";
                    break;
                }
            case 32:
                {
                    retValue = "Being Assessed (# Companies)";
                    break;
                }
            case 33:
                {
                    retValue = "Awaiting Assignment (# Companies)";
                    break;
                }
            case 34:
                {
                    retValue = "Approved";
                    break;
                }
            case 35:
                {
                    retValue = "Approved %";
                    break;
                }
            case 36:
                {
                    retValue = "Number of Companies";
                    break;
                }
            case 37:
                {
                    retValue = "Contractor Services Audit - Self (Conducted)";
                    break;
                }
            case 38:
                {
                    retValue = "Workplace Safety & Compliance";
                    break;
                }
            case 39:
                {
                    retValue = "Management Health Safety Work Contacts";
                    break;
                }
            case 40:
                {
                    retValue = "Behavioural Observations";
                    break;
                }
            case 41:
                {
                    retValue = "Fatality Prevention Program";
                    break;
                }
            case 42:
                {
                    retValue = "JSA Field Audit Verifications";
                    break;
                }
            case 43:
                {
                    retValue = "Toolbox Meetings";
                    break;
                }
            case 44:
                {
                    retValue = "Alcoa Weekly Contractors Meeting";
                    break;
                }
            case 45:
                {
                    retValue = "Alcoa Monthly Contractors Meeting";
                    break;
                }
            case 46:
                {
                    retValue = "Safety Plan(s) Submitted";
                    break;
                }
            case 47:
                {
                    retValue = "% Mandated Training";
                    break;
                }
            case 48:
                {
                    retValue = "Number of Companies - NE1";
                    break;
                }
            case 49:
                {
                    retValue = "Contractor Services Audit - Self (Conducted) - NE1";
                    break;
                }
            case 50:
                {
                    retValue = "Workplace Safety & Compliance - NE1";
                    break;
                }
            case 51:
                {
                    retValue = "Management Health Safety Work Contacts - NE1";
                    break;
                }
            case 52:
                {
                    retValue = "Behavioural Observations - NE1";
                    break;
                }
            case 53:
                {
                    retValue = "JSA Field Audit Verifications - NE1";
                    break;
                }
            case 54:
                {
                    retValue = "Toolbox Meetings - NE1";
                    break;
                }
            case 55:
                {
                    retValue = "Alcoa Weekly Contractors Meeting - NE1";
                    break;
                }
            case 56:
                {
                    retValue = "Alcoa Monthly Contractors Meeting - NE1";
                    break;
                }
            case 57:
                {
                    retValue = "% Mandated Training - NE1";
                    break;
                }
            case 58:
                {
                    retValue = "Total required";
                    break;
                }
            case 59:
                {
                    retValue = "Required to date (month)";
                    break;
                }
            case 60:
                {
                    retValue = "Actual completed (Cumulative total)";
                    break;
                }
            case 61:
                {
                    retValue = "Required";
                    break;
                }
            case 62:
                {
                    retValue = "Required to date";
                    break;
                }
            case 63:
                {
                    retValue = "Actual completed (Cumulative total) - NE1";
                    break;
                }
            default:
                {
                    retValue = "";
                    break;
                }
            //case 59:
            //    {
            //        retValue = "Workplace Safety & Compliance";
            //        break;
            //    }
            //case 60:
            //    {
            //        retValue = "Workplace Safety & Compliance";
            //        break;
            //    }
            //case 61:
            //    {
            //        retValue = "Workplace Safety & Compliance";
            //        break;
            //    }
            //case 62:
            //    {
            //        retValue = "Workplace Safety & Compliance";
            //        break;
            //    }
            //case 63:
            //    {
            //        retValue = "Workplace Safety & Compliance";
            //        break;
            //    }
            //case 64:
            //    {
            //        retValue = "Workplace Safety & Compliance";
            //        break;
            //    }
            //case 65:
            //    {
            //        retValue = "Workplace Safety & Compliance";
            //        break;
            //    }
        }
        return retValue;
    }
    public static bool IsInteger(string strNumber)
    {
        Regex objNotIntPattern = new Regex("[^0-9-]");
        Regex objIntPattern = new Regex("^-[0-9]+$|^[0-9]+$");
        return !objNotIntPattern.IsMatch(strNumber) &&
                objIntPattern.IsMatch(strNumber);
    }

    public static string GetEnumTextValue(Enum e)
    {
        string ret = "";
        Type t = e.GetType();
        MemberInfo[] members = t.GetMember(e.ToString());
        if (members.Length == 1)
        {
            object[] attrs = members[0].GetCustomAttributes(typeof(EnumTextValueAttribute), false);
            if (attrs.Length == 1)
            {
                ret = ((EnumTextValueAttribute)attrs[0]).Text;
            }
        }
        return ret;
    }

}