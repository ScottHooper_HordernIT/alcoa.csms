﻿#region Using directives

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="QuestionnaireReportExpiryProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract class QuestionnaireReportExpiryProviderBaseCore : EntityViewProviderBase<QuestionnaireReportExpiry>
	{
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions
		
		/*
		///<summary>
		/// Fill an VList&lt;QuestionnaireReportExpiry&gt; From a DataSet
		///</summary>
		/// <param name="dataSet">the DataSet</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList&lt;QuestionnaireReportExpiry&gt;"/></returns>
		protected static VList&lt;QuestionnaireReportExpiry&gt; Fill(DataSet dataSet, VList<QuestionnaireReportExpiry> rows, int start, int pagelen)
		{
			if (dataSet.Tables.Count == 1)
			{
				return Fill(dataSet.Tables[0], rows, start, pagelen);
			}
			else
			{
				return new VList<QuestionnaireReportExpiry>();
			}	
		}
		
		
		///<summary>
		/// Fill an VList&lt;QuestionnaireReportExpiry&gt; From a DataTable
		///</summary>
		/// <param name="dataTable">the DataTable that hold the data.</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList<QuestionnaireReportExpiry>"/></returns>
		protected static VList&lt;QuestionnaireReportExpiry&gt; Fill(DataTable dataTable, VList<QuestionnaireReportExpiry> rows, int start, int pagelen)
		{
			int recordnum = 0;
			
			System.Collections.IEnumerator dataRows =  dataTable.Rows.GetEnumerator();
			
			while (dataRows.MoveNext() && (pagelen != 0))
			{
				if(recordnum >= start)
				{
					DataRow row = (DataRow)dataRows.Current;
				
					QuestionnaireReportExpiry c = new QuestionnaireReportExpiry();
					c.QuestionnaireId = (Convert.IsDBNull(row["QuestionnaireId"]))?(int)0:(System.Int32)row["QuestionnaireId"];
					c.CompanyId = (Convert.IsDBNull(row["CompanyId"]))?(int)0:(System.Int32)row["CompanyId"];
					c.CreatedByUserId = (Convert.IsDBNull(row["CreatedByUserId"]))?(int)0:(System.Int32)row["CreatedByUserId"];
					c.CreatedByUser = (Convert.IsDBNull(row["CreatedByUser"]))?string.Empty:(System.String)row["CreatedByUser"];
					c.CreatedDate = (Convert.IsDBNull(row["CreatedDate"]))?DateTime.MinValue:(System.DateTime)row["CreatedDate"];
					c.ModifiedByUserId = (Convert.IsDBNull(row["ModifiedByUserId"]))?(int)0:(System.Int32)row["ModifiedByUserId"];
					c.ModifiedByUser = (Convert.IsDBNull(row["ModifiedByUser"]))?string.Empty:(System.String)row["ModifiedByUser"];
					c.ModifiedDate = (Convert.IsDBNull(row["ModifiedDate"]))?DateTime.MinValue:(System.DateTime)row["ModifiedDate"];
					c.Status = (Convert.IsDBNull(row["Status"]))?(int)0:(System.Int32)row["Status"];
					c.MainAssessmentValidTo = (Convert.IsDBNull(row["MainAssessmentValidTo"]))?DateTime.MinValue:(System.DateTime?)row["MainAssessmentValidTo"];
					c.ExpiresIn = (Convert.IsDBNull(row["ExpiresIn"]))?(int)0:(System.Int32?)row["ExpiresIn"];
					c.CompanyStatusDesc = (Convert.IsDBNull(row["CompanyStatusDesc"]))?string.Empty:(System.String)row["CompanyStatusDesc"];
					c.CompanyName = (Convert.IsDBNull(row["CompanyName"]))?string.Empty:(System.String)row["CompanyName"];
					c.Type = (Convert.IsDBNull(row["Type"]))?string.Empty:(System.String)row["Type"];
					c.SupplierContact = (Convert.IsDBNull(row["SupplierContact"]))?string.Empty:(System.String)row["SupplierContact"];
					c.SupplierContactEmail = (Convert.IsDBNull(row["SupplierContactEmail"]))?string.Empty:(System.String)row["SupplierContactEmail"];
					c.SupplierContactPhone = (Convert.IsDBNull(row["SupplierContactPhone"]))?string.Empty:(System.String)row["SupplierContactPhone"];
					c.SupplierContactEmailPhone = (Convert.IsDBNull(row["SupplierContactEmailPhone"]))?string.Empty:(System.String)row["SupplierContactEmailPhone"];
					c.ProcurementContact = (Convert.IsDBNull(row["ProcurementContact"]))?string.Empty:(System.String)row["ProcurementContact"];
					c.ProcurementContactUser = (Convert.IsDBNull(row["ProcurementContactUser"]))?string.Empty:(System.String)row["ProcurementContactUser"];
					c.ContractManager = (Convert.IsDBNull(row["ContractManager"]))?string.Empty:(System.String)row["ContractManager"];
					c.ContractManagerUser = (Convert.IsDBNull(row["ContractManagerUser"]))?string.Empty:(System.String)row["ContractManagerUser"];
					c.EhsConsultantId = (Convert.IsDBNull(row["EHSConsultantId"]))?(int)0:(System.Int32?)row["EHSConsultantId"];
					c.SafetyAssessor = (Convert.IsDBNull(row["SafetyAssessor"]))?string.Empty:(System.String)row["SafetyAssessor"];
					c.QuestionnairePresentlyWithActionId = (Convert.IsDBNull(row["QuestionnairePresentlyWithActionId"]))?(int)0:(System.Int32?)row["QuestionnairePresentlyWithActionId"];
					c.QuestionnairePresentlyWithActionDesc = (Convert.IsDBNull(row["QuestionnairePresentlyWithActionDesc"]))?string.Empty:(System.String)row["QuestionnairePresentlyWithActionDesc"];
					c.QuestionnairePresentlyWithUserId = (Convert.IsDBNull(row["QuestionnairePresentlyWithUserId"]))?(int)0:(System.Int32?)row["QuestionnairePresentlyWithUserId"];
					c.QuestionnairePresentlyWithUserDesc = (Convert.IsDBNull(row["QuestionnairePresentlyWIthUserDesc"]))?string.Empty:(System.String)row["QuestionnairePresentlyWIthUserDesc"];
					c.AcceptChanges();
					rows.Add(c);
					pagelen -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		*/	
						
		///<summary>
		/// Fill an <see cref="VList&lt;QuestionnaireReportExpiry&gt;"/> From a DataReader.
		///</summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pageLength">number of row.</param>
		///<returns>a <see cref="VList&lt;QuestionnaireReportExpiry&gt;"/></returns>
		protected VList<QuestionnaireReportExpiry> Fill(IDataReader reader, VList<QuestionnaireReportExpiry> rows, int start, int pageLength)
		{
			int recordnum = 0;
			while (reader.Read() && (pageLength != 0))
			{
				if(recordnum >= start)
				{
					QuestionnaireReportExpiry entity = null;
					if (DataRepository.Provider.UseEntityFactory)
					{
						entity = EntityManager.CreateViewEntity<QuestionnaireReportExpiry>("QuestionnaireReportExpiry",  DataRepository.Provider.EntityCreationalFactoryType); 
					}
					else
					{
						entity = new QuestionnaireReportExpiry();
					}
					
					entity.SuppressEntityEvents = true;

					entity.QuestionnaireId = (System.Int32)reader[((int)QuestionnaireReportExpiryColumn.QuestionnaireId)];
					//entity.QuestionnaireId = (Convert.IsDBNull(reader["QuestionnaireId"]))?(int)0:(System.Int32)reader["QuestionnaireId"];
					entity.CompanyId = (System.Int32)reader[((int)QuestionnaireReportExpiryColumn.CompanyId)];
					//entity.CompanyId = (Convert.IsDBNull(reader["CompanyId"]))?(int)0:(System.Int32)reader["CompanyId"];
					entity.CreatedByUserId = (System.Int32)reader[((int)QuestionnaireReportExpiryColumn.CreatedByUserId)];
					//entity.CreatedByUserId = (Convert.IsDBNull(reader["CreatedByUserId"]))?(int)0:(System.Int32)reader["CreatedByUserId"];
					entity.CreatedByUser = (reader.IsDBNull(((int)QuestionnaireReportExpiryColumn.CreatedByUser)))?null:(System.String)reader[((int)QuestionnaireReportExpiryColumn.CreatedByUser)];
					//entity.CreatedByUser = (Convert.IsDBNull(reader["CreatedByUser"]))?string.Empty:(System.String)reader["CreatedByUser"];
					entity.CreatedDate = (System.DateTime)reader[((int)QuestionnaireReportExpiryColumn.CreatedDate)];
					//entity.CreatedDate = (Convert.IsDBNull(reader["CreatedDate"]))?DateTime.MinValue:(System.DateTime)reader["CreatedDate"];
					entity.ModifiedByUserId = (System.Int32)reader[((int)QuestionnaireReportExpiryColumn.ModifiedByUserId)];
					//entity.ModifiedByUserId = (Convert.IsDBNull(reader["ModifiedByUserId"]))?(int)0:(System.Int32)reader["ModifiedByUserId"];
					entity.ModifiedByUser = (reader.IsDBNull(((int)QuestionnaireReportExpiryColumn.ModifiedByUser)))?null:(System.String)reader[((int)QuestionnaireReportExpiryColumn.ModifiedByUser)];
					//entity.ModifiedByUser = (Convert.IsDBNull(reader["ModifiedByUser"]))?string.Empty:(System.String)reader["ModifiedByUser"];
					entity.ModifiedDate = (System.DateTime)reader[((int)QuestionnaireReportExpiryColumn.ModifiedDate)];
					//entity.ModifiedDate = (Convert.IsDBNull(reader["ModifiedDate"]))?DateTime.MinValue:(System.DateTime)reader["ModifiedDate"];
					entity.Status = (System.Int32)reader[((int)QuestionnaireReportExpiryColumn.Status)];
					//entity.Status = (Convert.IsDBNull(reader["Status"]))?(int)0:(System.Int32)reader["Status"];
					entity.MainAssessmentValidTo = (reader.IsDBNull(((int)QuestionnaireReportExpiryColumn.MainAssessmentValidTo)))?null:(System.DateTime?)reader[((int)QuestionnaireReportExpiryColumn.MainAssessmentValidTo)];
					//entity.MainAssessmentValidTo = (Convert.IsDBNull(reader["MainAssessmentValidTo"]))?DateTime.MinValue:(System.DateTime?)reader["MainAssessmentValidTo"];
					entity.ExpiresIn = (reader.IsDBNull(((int)QuestionnaireReportExpiryColumn.ExpiresIn)))?null:(System.Int32?)reader[((int)QuestionnaireReportExpiryColumn.ExpiresIn)];
					//entity.ExpiresIn = (Convert.IsDBNull(reader["ExpiresIn"]))?(int)0:(System.Int32?)reader["ExpiresIn"];
					entity.CompanyStatusDesc = (reader.IsDBNull(((int)QuestionnaireReportExpiryColumn.CompanyStatusDesc)))?null:(System.String)reader[((int)QuestionnaireReportExpiryColumn.CompanyStatusDesc)];
					//entity.CompanyStatusDesc = (Convert.IsDBNull(reader["CompanyStatusDesc"]))?string.Empty:(System.String)reader["CompanyStatusDesc"];
					entity.CompanyName = (System.String)reader[((int)QuestionnaireReportExpiryColumn.CompanyName)];
					//entity.CompanyName = (Convert.IsDBNull(reader["CompanyName"]))?string.Empty:(System.String)reader["CompanyName"];
					entity.Type = (System.String)reader[((int)QuestionnaireReportExpiryColumn.Type)];
					//entity.Type = (Convert.IsDBNull(reader["Type"]))?string.Empty:(System.String)reader["Type"];
					entity.SupplierContact = (reader.IsDBNull(((int)QuestionnaireReportExpiryColumn.SupplierContact)))?null:(System.String)reader[((int)QuestionnaireReportExpiryColumn.SupplierContact)];
					//entity.SupplierContact = (Convert.IsDBNull(reader["SupplierContact"]))?string.Empty:(System.String)reader["SupplierContact"];
					entity.SupplierContactEmail = (reader.IsDBNull(((int)QuestionnaireReportExpiryColumn.SupplierContactEmail)))?null:(System.String)reader[((int)QuestionnaireReportExpiryColumn.SupplierContactEmail)];
					//entity.SupplierContactEmail = (Convert.IsDBNull(reader["SupplierContactEmail"]))?string.Empty:(System.String)reader["SupplierContactEmail"];
					entity.SupplierContactPhone = (reader.IsDBNull(((int)QuestionnaireReportExpiryColumn.SupplierContactPhone)))?null:(System.String)reader[((int)QuestionnaireReportExpiryColumn.SupplierContactPhone)];
					//entity.SupplierContactPhone = (Convert.IsDBNull(reader["SupplierContactPhone"]))?string.Empty:(System.String)reader["SupplierContactPhone"];
					entity.SupplierContactEmailPhone = (reader.IsDBNull(((int)QuestionnaireReportExpiryColumn.SupplierContactEmailPhone)))?null:(System.String)reader[((int)QuestionnaireReportExpiryColumn.SupplierContactEmailPhone)];
					//entity.SupplierContactEmailPhone = (Convert.IsDBNull(reader["SupplierContactEmailPhone"]))?string.Empty:(System.String)reader["SupplierContactEmailPhone"];
					entity.ProcurementContact = (reader.IsDBNull(((int)QuestionnaireReportExpiryColumn.ProcurementContact)))?null:(System.String)reader[((int)QuestionnaireReportExpiryColumn.ProcurementContact)];
					//entity.ProcurementContact = (Convert.IsDBNull(reader["ProcurementContact"]))?string.Empty:(System.String)reader["ProcurementContact"];
					entity.ProcurementContactUser = (reader.IsDBNull(((int)QuestionnaireReportExpiryColumn.ProcurementContactUser)))?null:(System.String)reader[((int)QuestionnaireReportExpiryColumn.ProcurementContactUser)];
					//entity.ProcurementContactUser = (Convert.IsDBNull(reader["ProcurementContactUser"]))?string.Empty:(System.String)reader["ProcurementContactUser"];
					entity.ContractManager = (reader.IsDBNull(((int)QuestionnaireReportExpiryColumn.ContractManager)))?null:(System.String)reader[((int)QuestionnaireReportExpiryColumn.ContractManager)];
					//entity.ContractManager = (Convert.IsDBNull(reader["ContractManager"]))?string.Empty:(System.String)reader["ContractManager"];
					entity.ContractManagerUser = (reader.IsDBNull(((int)QuestionnaireReportExpiryColumn.ContractManagerUser)))?null:(System.String)reader[((int)QuestionnaireReportExpiryColumn.ContractManagerUser)];
					//entity.ContractManagerUser = (Convert.IsDBNull(reader["ContractManagerUser"]))?string.Empty:(System.String)reader["ContractManagerUser"];
					entity.EhsConsultantId = (reader.IsDBNull(((int)QuestionnaireReportExpiryColumn.EhsConsultantId)))?null:(System.Int32?)reader[((int)QuestionnaireReportExpiryColumn.EhsConsultantId)];
					//entity.EhsConsultantId = (Convert.IsDBNull(reader["EHSConsultantId"]))?(int)0:(System.Int32?)reader["EHSConsultantId"];
					entity.SafetyAssessor = (reader.IsDBNull(((int)QuestionnaireReportExpiryColumn.SafetyAssessor)))?null:(System.String)reader[((int)QuestionnaireReportExpiryColumn.SafetyAssessor)];
					//entity.SafetyAssessor = (Convert.IsDBNull(reader["SafetyAssessor"]))?string.Empty:(System.String)reader["SafetyAssessor"];
					entity.QuestionnairePresentlyWithActionId = (reader.IsDBNull(((int)QuestionnaireReportExpiryColumn.QuestionnairePresentlyWithActionId)))?null:(System.Int32?)reader[((int)QuestionnaireReportExpiryColumn.QuestionnairePresentlyWithActionId)];
					//entity.QuestionnairePresentlyWithActionId = (Convert.IsDBNull(reader["QuestionnairePresentlyWithActionId"]))?(int)0:(System.Int32?)reader["QuestionnairePresentlyWithActionId"];
					entity.QuestionnairePresentlyWithActionDesc = (reader.IsDBNull(((int)QuestionnaireReportExpiryColumn.QuestionnairePresentlyWithActionDesc)))?null:(System.String)reader[((int)QuestionnaireReportExpiryColumn.QuestionnairePresentlyWithActionDesc)];
					//entity.QuestionnairePresentlyWithActionDesc = (Convert.IsDBNull(reader["QuestionnairePresentlyWithActionDesc"]))?string.Empty:(System.String)reader["QuestionnairePresentlyWithActionDesc"];
					entity.QuestionnairePresentlyWithUserId = (reader.IsDBNull(((int)QuestionnaireReportExpiryColumn.QuestionnairePresentlyWithUserId)))?null:(System.Int32?)reader[((int)QuestionnaireReportExpiryColumn.QuestionnairePresentlyWithUserId)];
					//entity.QuestionnairePresentlyWithUserId = (Convert.IsDBNull(reader["QuestionnairePresentlyWithUserId"]))?(int)0:(System.Int32?)reader["QuestionnairePresentlyWithUserId"];
					entity.QuestionnairePresentlyWithUserDesc = (reader.IsDBNull(((int)QuestionnaireReportExpiryColumn.QuestionnairePresentlyWithUserDesc)))?null:(System.String)reader[((int)QuestionnaireReportExpiryColumn.QuestionnairePresentlyWithUserDesc)];
					//entity.QuestionnairePresentlyWithUserDesc = (Convert.IsDBNull(reader["QuestionnairePresentlyWIthUserDesc"]))?string.Empty:(System.String)reader["QuestionnairePresentlyWIthUserDesc"];
					entity.AcceptChanges();
					entity.SuppressEntityEvents = false;
					
					rows.Add(entity);
					pageLength -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		
		
		/// <summary>
		/// Refreshes the <see cref="QuestionnaireReportExpiry"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="QuestionnaireReportExpiry"/> object to refresh.</param>
		protected void RefreshEntity(IDataReader reader, QuestionnaireReportExpiry entity)
		{
			reader.Read();
			entity.QuestionnaireId = (System.Int32)reader[((int)QuestionnaireReportExpiryColumn.QuestionnaireId)];
			//entity.QuestionnaireId = (Convert.IsDBNull(reader["QuestionnaireId"]))?(int)0:(System.Int32)reader["QuestionnaireId"];
			entity.CompanyId = (System.Int32)reader[((int)QuestionnaireReportExpiryColumn.CompanyId)];
			//entity.CompanyId = (Convert.IsDBNull(reader["CompanyId"]))?(int)0:(System.Int32)reader["CompanyId"];
			entity.CreatedByUserId = (System.Int32)reader[((int)QuestionnaireReportExpiryColumn.CreatedByUserId)];
			//entity.CreatedByUserId = (Convert.IsDBNull(reader["CreatedByUserId"]))?(int)0:(System.Int32)reader["CreatedByUserId"];
			entity.CreatedByUser = (reader.IsDBNull(((int)QuestionnaireReportExpiryColumn.CreatedByUser)))?null:(System.String)reader[((int)QuestionnaireReportExpiryColumn.CreatedByUser)];
			//entity.CreatedByUser = (Convert.IsDBNull(reader["CreatedByUser"]))?string.Empty:(System.String)reader["CreatedByUser"];
			entity.CreatedDate = (System.DateTime)reader[((int)QuestionnaireReportExpiryColumn.CreatedDate)];
			//entity.CreatedDate = (Convert.IsDBNull(reader["CreatedDate"]))?DateTime.MinValue:(System.DateTime)reader["CreatedDate"];
			entity.ModifiedByUserId = (System.Int32)reader[((int)QuestionnaireReportExpiryColumn.ModifiedByUserId)];
			//entity.ModifiedByUserId = (Convert.IsDBNull(reader["ModifiedByUserId"]))?(int)0:(System.Int32)reader["ModifiedByUserId"];
			entity.ModifiedByUser = (reader.IsDBNull(((int)QuestionnaireReportExpiryColumn.ModifiedByUser)))?null:(System.String)reader[((int)QuestionnaireReportExpiryColumn.ModifiedByUser)];
			//entity.ModifiedByUser = (Convert.IsDBNull(reader["ModifiedByUser"]))?string.Empty:(System.String)reader["ModifiedByUser"];
			entity.ModifiedDate = (System.DateTime)reader[((int)QuestionnaireReportExpiryColumn.ModifiedDate)];
			//entity.ModifiedDate = (Convert.IsDBNull(reader["ModifiedDate"]))?DateTime.MinValue:(System.DateTime)reader["ModifiedDate"];
			entity.Status = (System.Int32)reader[((int)QuestionnaireReportExpiryColumn.Status)];
			//entity.Status = (Convert.IsDBNull(reader["Status"]))?(int)0:(System.Int32)reader["Status"];
			entity.MainAssessmentValidTo = (reader.IsDBNull(((int)QuestionnaireReportExpiryColumn.MainAssessmentValidTo)))?null:(System.DateTime?)reader[((int)QuestionnaireReportExpiryColumn.MainAssessmentValidTo)];
			//entity.MainAssessmentValidTo = (Convert.IsDBNull(reader["MainAssessmentValidTo"]))?DateTime.MinValue:(System.DateTime?)reader["MainAssessmentValidTo"];
			entity.ExpiresIn = (reader.IsDBNull(((int)QuestionnaireReportExpiryColumn.ExpiresIn)))?null:(System.Int32?)reader[((int)QuestionnaireReportExpiryColumn.ExpiresIn)];
			//entity.ExpiresIn = (Convert.IsDBNull(reader["ExpiresIn"]))?(int)0:(System.Int32?)reader["ExpiresIn"];
			entity.CompanyStatusDesc = (reader.IsDBNull(((int)QuestionnaireReportExpiryColumn.CompanyStatusDesc)))?null:(System.String)reader[((int)QuestionnaireReportExpiryColumn.CompanyStatusDesc)];
			//entity.CompanyStatusDesc = (Convert.IsDBNull(reader["CompanyStatusDesc"]))?string.Empty:(System.String)reader["CompanyStatusDesc"];
			entity.CompanyName = (System.String)reader[((int)QuestionnaireReportExpiryColumn.CompanyName)];
			//entity.CompanyName = (Convert.IsDBNull(reader["CompanyName"]))?string.Empty:(System.String)reader["CompanyName"];
			entity.Type = (System.String)reader[((int)QuestionnaireReportExpiryColumn.Type)];
			//entity.Type = (Convert.IsDBNull(reader["Type"]))?string.Empty:(System.String)reader["Type"];
			entity.SupplierContact = (reader.IsDBNull(((int)QuestionnaireReportExpiryColumn.SupplierContact)))?null:(System.String)reader[((int)QuestionnaireReportExpiryColumn.SupplierContact)];
			//entity.SupplierContact = (Convert.IsDBNull(reader["SupplierContact"]))?string.Empty:(System.String)reader["SupplierContact"];
			entity.SupplierContactEmail = (reader.IsDBNull(((int)QuestionnaireReportExpiryColumn.SupplierContactEmail)))?null:(System.String)reader[((int)QuestionnaireReportExpiryColumn.SupplierContactEmail)];
			//entity.SupplierContactEmail = (Convert.IsDBNull(reader["SupplierContactEmail"]))?string.Empty:(System.String)reader["SupplierContactEmail"];
			entity.SupplierContactPhone = (reader.IsDBNull(((int)QuestionnaireReportExpiryColumn.SupplierContactPhone)))?null:(System.String)reader[((int)QuestionnaireReportExpiryColumn.SupplierContactPhone)];
			//entity.SupplierContactPhone = (Convert.IsDBNull(reader["SupplierContactPhone"]))?string.Empty:(System.String)reader["SupplierContactPhone"];
			entity.SupplierContactEmailPhone = (reader.IsDBNull(((int)QuestionnaireReportExpiryColumn.SupplierContactEmailPhone)))?null:(System.String)reader[((int)QuestionnaireReportExpiryColumn.SupplierContactEmailPhone)];
			//entity.SupplierContactEmailPhone = (Convert.IsDBNull(reader["SupplierContactEmailPhone"]))?string.Empty:(System.String)reader["SupplierContactEmailPhone"];
			entity.ProcurementContact = (reader.IsDBNull(((int)QuestionnaireReportExpiryColumn.ProcurementContact)))?null:(System.String)reader[((int)QuestionnaireReportExpiryColumn.ProcurementContact)];
			//entity.ProcurementContact = (Convert.IsDBNull(reader["ProcurementContact"]))?string.Empty:(System.String)reader["ProcurementContact"];
			entity.ProcurementContactUser = (reader.IsDBNull(((int)QuestionnaireReportExpiryColumn.ProcurementContactUser)))?null:(System.String)reader[((int)QuestionnaireReportExpiryColumn.ProcurementContactUser)];
			//entity.ProcurementContactUser = (Convert.IsDBNull(reader["ProcurementContactUser"]))?string.Empty:(System.String)reader["ProcurementContactUser"];
			entity.ContractManager = (reader.IsDBNull(((int)QuestionnaireReportExpiryColumn.ContractManager)))?null:(System.String)reader[((int)QuestionnaireReportExpiryColumn.ContractManager)];
			//entity.ContractManager = (Convert.IsDBNull(reader["ContractManager"]))?string.Empty:(System.String)reader["ContractManager"];
			entity.ContractManagerUser = (reader.IsDBNull(((int)QuestionnaireReportExpiryColumn.ContractManagerUser)))?null:(System.String)reader[((int)QuestionnaireReportExpiryColumn.ContractManagerUser)];
			//entity.ContractManagerUser = (Convert.IsDBNull(reader["ContractManagerUser"]))?string.Empty:(System.String)reader["ContractManagerUser"];
			entity.EhsConsultantId = (reader.IsDBNull(((int)QuestionnaireReportExpiryColumn.EhsConsultantId)))?null:(System.Int32?)reader[((int)QuestionnaireReportExpiryColumn.EhsConsultantId)];
			//entity.EhsConsultantId = (Convert.IsDBNull(reader["EHSConsultantId"]))?(int)0:(System.Int32?)reader["EHSConsultantId"];
			entity.SafetyAssessor = (reader.IsDBNull(((int)QuestionnaireReportExpiryColumn.SafetyAssessor)))?null:(System.String)reader[((int)QuestionnaireReportExpiryColumn.SafetyAssessor)];
			//entity.SafetyAssessor = (Convert.IsDBNull(reader["SafetyAssessor"]))?string.Empty:(System.String)reader["SafetyAssessor"];
			entity.QuestionnairePresentlyWithActionId = (reader.IsDBNull(((int)QuestionnaireReportExpiryColumn.QuestionnairePresentlyWithActionId)))?null:(System.Int32?)reader[((int)QuestionnaireReportExpiryColumn.QuestionnairePresentlyWithActionId)];
			//entity.QuestionnairePresentlyWithActionId = (Convert.IsDBNull(reader["QuestionnairePresentlyWithActionId"]))?(int)0:(System.Int32?)reader["QuestionnairePresentlyWithActionId"];
			entity.QuestionnairePresentlyWithActionDesc = (reader.IsDBNull(((int)QuestionnaireReportExpiryColumn.QuestionnairePresentlyWithActionDesc)))?null:(System.String)reader[((int)QuestionnaireReportExpiryColumn.QuestionnairePresentlyWithActionDesc)];
			//entity.QuestionnairePresentlyWithActionDesc = (Convert.IsDBNull(reader["QuestionnairePresentlyWithActionDesc"]))?string.Empty:(System.String)reader["QuestionnairePresentlyWithActionDesc"];
			entity.QuestionnairePresentlyWithUserId = (reader.IsDBNull(((int)QuestionnaireReportExpiryColumn.QuestionnairePresentlyWithUserId)))?null:(System.Int32?)reader[((int)QuestionnaireReportExpiryColumn.QuestionnairePresentlyWithUserId)];
			//entity.QuestionnairePresentlyWithUserId = (Convert.IsDBNull(reader["QuestionnairePresentlyWithUserId"]))?(int)0:(System.Int32?)reader["QuestionnairePresentlyWithUserId"];
			entity.QuestionnairePresentlyWithUserDesc = (reader.IsDBNull(((int)QuestionnaireReportExpiryColumn.QuestionnairePresentlyWithUserDesc)))?null:(System.String)reader[((int)QuestionnaireReportExpiryColumn.QuestionnairePresentlyWithUserDesc)];
			//entity.QuestionnairePresentlyWithUserDesc = (Convert.IsDBNull(reader["QuestionnairePresentlyWIthUserDesc"]))?string.Empty:(System.String)reader["QuestionnairePresentlyWIthUserDesc"];
			reader.Close();
	
			entity.AcceptChanges();
		}
		
		/*
		/// <summary>
		/// Refreshes the <see cref="QuestionnaireReportExpiry"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="QuestionnaireReportExpiry"/> object.</param>
		protected static void RefreshEntity(DataSet dataSet, QuestionnaireReportExpiry entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.QuestionnaireId = (Convert.IsDBNull(dataRow["QuestionnaireId"]))?(int)0:(System.Int32)dataRow["QuestionnaireId"];
			entity.CompanyId = (Convert.IsDBNull(dataRow["CompanyId"]))?(int)0:(System.Int32)dataRow["CompanyId"];
			entity.CreatedByUserId = (Convert.IsDBNull(dataRow["CreatedByUserId"]))?(int)0:(System.Int32)dataRow["CreatedByUserId"];
			entity.CreatedByUser = (Convert.IsDBNull(dataRow["CreatedByUser"]))?string.Empty:(System.String)dataRow["CreatedByUser"];
			entity.CreatedDate = (Convert.IsDBNull(dataRow["CreatedDate"]))?DateTime.MinValue:(System.DateTime)dataRow["CreatedDate"];
			entity.ModifiedByUserId = (Convert.IsDBNull(dataRow["ModifiedByUserId"]))?(int)0:(System.Int32)dataRow["ModifiedByUserId"];
			entity.ModifiedByUser = (Convert.IsDBNull(dataRow["ModifiedByUser"]))?string.Empty:(System.String)dataRow["ModifiedByUser"];
			entity.ModifiedDate = (Convert.IsDBNull(dataRow["ModifiedDate"]))?DateTime.MinValue:(System.DateTime)dataRow["ModifiedDate"];
			entity.Status = (Convert.IsDBNull(dataRow["Status"]))?(int)0:(System.Int32)dataRow["Status"];
			entity.MainAssessmentValidTo = (Convert.IsDBNull(dataRow["MainAssessmentValidTo"]))?DateTime.MinValue:(System.DateTime?)dataRow["MainAssessmentValidTo"];
			entity.ExpiresIn = (Convert.IsDBNull(dataRow["ExpiresIn"]))?(int)0:(System.Int32?)dataRow["ExpiresIn"];
			entity.CompanyStatusDesc = (Convert.IsDBNull(dataRow["CompanyStatusDesc"]))?string.Empty:(System.String)dataRow["CompanyStatusDesc"];
			entity.CompanyName = (Convert.IsDBNull(dataRow["CompanyName"]))?string.Empty:(System.String)dataRow["CompanyName"];
			entity.Type = (Convert.IsDBNull(dataRow["Type"]))?string.Empty:(System.String)dataRow["Type"];
			entity.SupplierContact = (Convert.IsDBNull(dataRow["SupplierContact"]))?string.Empty:(System.String)dataRow["SupplierContact"];
			entity.SupplierContactEmail = (Convert.IsDBNull(dataRow["SupplierContactEmail"]))?string.Empty:(System.String)dataRow["SupplierContactEmail"];
			entity.SupplierContactPhone = (Convert.IsDBNull(dataRow["SupplierContactPhone"]))?string.Empty:(System.String)dataRow["SupplierContactPhone"];
			entity.SupplierContactEmailPhone = (Convert.IsDBNull(dataRow["SupplierContactEmailPhone"]))?string.Empty:(System.String)dataRow["SupplierContactEmailPhone"];
			entity.ProcurementContact = (Convert.IsDBNull(dataRow["ProcurementContact"]))?string.Empty:(System.String)dataRow["ProcurementContact"];
			entity.ProcurementContactUser = (Convert.IsDBNull(dataRow["ProcurementContactUser"]))?string.Empty:(System.String)dataRow["ProcurementContactUser"];
			entity.ContractManager = (Convert.IsDBNull(dataRow["ContractManager"]))?string.Empty:(System.String)dataRow["ContractManager"];
			entity.ContractManagerUser = (Convert.IsDBNull(dataRow["ContractManagerUser"]))?string.Empty:(System.String)dataRow["ContractManagerUser"];
			entity.EhsConsultantId = (Convert.IsDBNull(dataRow["EHSConsultantId"]))?(int)0:(System.Int32?)dataRow["EHSConsultantId"];
			entity.SafetyAssessor = (Convert.IsDBNull(dataRow["SafetyAssessor"]))?string.Empty:(System.String)dataRow["SafetyAssessor"];
			entity.QuestionnairePresentlyWithActionId = (Convert.IsDBNull(dataRow["QuestionnairePresentlyWithActionId"]))?(int)0:(System.Int32?)dataRow["QuestionnairePresentlyWithActionId"];
			entity.QuestionnairePresentlyWithActionDesc = (Convert.IsDBNull(dataRow["QuestionnairePresentlyWithActionDesc"]))?string.Empty:(System.String)dataRow["QuestionnairePresentlyWithActionDesc"];
			entity.QuestionnairePresentlyWithUserId = (Convert.IsDBNull(dataRow["QuestionnairePresentlyWithUserId"]))?(int)0:(System.Int32?)dataRow["QuestionnairePresentlyWithUserId"];
			entity.QuestionnairePresentlyWithUserDesc = (Convert.IsDBNull(dataRow["QuestionnairePresentlyWIthUserDesc"]))?string.Empty:(System.String)dataRow["QuestionnairePresentlyWIthUserDesc"];
			entity.AcceptChanges();
		}
		*/
			
		#endregion Helper Functions
	}//end class

	#region QuestionnaireReportExpiryFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireReportExpiry"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireReportExpiryFilterBuilder : SqlFilterBuilder<QuestionnaireReportExpiryColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportExpiryFilterBuilder class.
		/// </summary>
		public QuestionnaireReportExpiryFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportExpiryFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireReportExpiryFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportExpiryFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireReportExpiryFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireReportExpiryFilterBuilder

	#region QuestionnaireReportExpiryParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireReportExpiry"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireReportExpiryParameterBuilder : ParameterizedSqlFilterBuilder<QuestionnaireReportExpiryColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportExpiryParameterBuilder class.
		/// </summary>
		public QuestionnaireReportExpiryParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportExpiryParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireReportExpiryParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportExpiryParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireReportExpiryParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireReportExpiryParameterBuilder
	
	#region QuestionnaireReportExpirySortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireReportExpiry"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class QuestionnaireReportExpirySortBuilder : SqlSortBuilder<QuestionnaireReportExpiryColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportExpirySqlSortBuilder class.
		/// </summary>
		public QuestionnaireReportExpirySortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion QuestionnaireReportExpirySortBuilder

} // end namespace
