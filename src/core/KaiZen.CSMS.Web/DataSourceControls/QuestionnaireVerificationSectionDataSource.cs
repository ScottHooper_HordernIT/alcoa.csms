﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.QuestionnaireVerificationSectionProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(QuestionnaireVerificationSectionDataSourceDesigner))]
	public class QuestionnaireVerificationSectionDataSource : ProviderDataSource<QuestionnaireVerificationSection, QuestionnaireVerificationSectionKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireVerificationSectionDataSource class.
		/// </summary>
		public QuestionnaireVerificationSectionDataSource() : base(new QuestionnaireVerificationSectionService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the QuestionnaireVerificationSectionDataSourceView used by the QuestionnaireVerificationSectionDataSource.
		/// </summary>
		protected QuestionnaireVerificationSectionDataSourceView QuestionnaireVerificationSectionView
		{
			get { return ( View as QuestionnaireVerificationSectionDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the QuestionnaireVerificationSectionDataSource control invokes to retrieve data.
		/// </summary>
		public QuestionnaireVerificationSectionSelectMethod SelectMethod
		{
			get
			{
				QuestionnaireVerificationSectionSelectMethod selectMethod = QuestionnaireVerificationSectionSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (QuestionnaireVerificationSectionSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the QuestionnaireVerificationSectionDataSourceView class that is to be
		/// used by the QuestionnaireVerificationSectionDataSource.
		/// </summary>
		/// <returns>An instance of the QuestionnaireVerificationSectionDataSourceView class.</returns>
		protected override BaseDataSourceView<QuestionnaireVerificationSection, QuestionnaireVerificationSectionKey> GetNewDataSourceView()
		{
			return new QuestionnaireVerificationSectionDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the QuestionnaireVerificationSectionDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class QuestionnaireVerificationSectionDataSourceView : ProviderDataSourceView<QuestionnaireVerificationSection, QuestionnaireVerificationSectionKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireVerificationSectionDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the QuestionnaireVerificationSectionDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public QuestionnaireVerificationSectionDataSourceView(QuestionnaireVerificationSectionDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal QuestionnaireVerificationSectionDataSource QuestionnaireVerificationSectionOwner
		{
			get { return Owner as QuestionnaireVerificationSectionDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal QuestionnaireVerificationSectionSelectMethod SelectMethod
		{
			get { return QuestionnaireVerificationSectionOwner.SelectMethod; }
			set { QuestionnaireVerificationSectionOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal QuestionnaireVerificationSectionService QuestionnaireVerificationSectionProvider
		{
			get { return Provider as QuestionnaireVerificationSectionService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<QuestionnaireVerificationSection> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<QuestionnaireVerificationSection> results = null;
			QuestionnaireVerificationSection item;
			count = 0;
			
			System.Int32 _sectionResponseId;
			System.Int32 _questionnaireId;

			switch ( SelectMethod )
			{
				case QuestionnaireVerificationSectionSelectMethod.Get:
					QuestionnaireVerificationSectionKey entityKey  = new QuestionnaireVerificationSectionKey();
					entityKey.Load(values);
					item = QuestionnaireVerificationSectionProvider.Get(entityKey);
					results = new TList<QuestionnaireVerificationSection>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case QuestionnaireVerificationSectionSelectMethod.GetAll:
                    results = QuestionnaireVerificationSectionProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case QuestionnaireVerificationSectionSelectMethod.GetPaged:
					results = QuestionnaireVerificationSectionProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case QuestionnaireVerificationSectionSelectMethod.Find:
					if ( FilterParameters != null )
						results = QuestionnaireVerificationSectionProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = QuestionnaireVerificationSectionProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case QuestionnaireVerificationSectionSelectMethod.GetBySectionResponseId:
					_sectionResponseId = ( values["SectionResponseId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["SectionResponseId"], typeof(System.Int32)) : (int)0;
					item = QuestionnaireVerificationSectionProvider.GetBySectionResponseId(_sectionResponseId);
					results = new TList<QuestionnaireVerificationSection>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				case QuestionnaireVerificationSectionSelectMethod.GetByQuestionnaireId:
					_questionnaireId = ( values["QuestionnaireId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["QuestionnaireId"], typeof(System.Int32)) : (int)0;
					item = QuestionnaireVerificationSectionProvider.GetByQuestionnaireId(_questionnaireId);
					results = new TList<QuestionnaireVerificationSection>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// FK
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == QuestionnaireVerificationSectionSelectMethod.Get || SelectMethod == QuestionnaireVerificationSectionSelectMethod.GetBySectionResponseId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				QuestionnaireVerificationSection entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					QuestionnaireVerificationSectionProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<QuestionnaireVerificationSection> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			QuestionnaireVerificationSectionProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region QuestionnaireVerificationSectionDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the QuestionnaireVerificationSectionDataSource class.
	/// </summary>
	public class QuestionnaireVerificationSectionDataSourceDesigner : ProviderDataSourceDesigner<QuestionnaireVerificationSection, QuestionnaireVerificationSectionKey>
	{
		/// <summary>
		/// Initializes a new instance of the QuestionnaireVerificationSectionDataSourceDesigner class.
		/// </summary>
		public QuestionnaireVerificationSectionDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public QuestionnaireVerificationSectionSelectMethod SelectMethod
		{
			get { return ((QuestionnaireVerificationSectionDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new QuestionnaireVerificationSectionDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region QuestionnaireVerificationSectionDataSourceActionList

	/// <summary>
	/// Supports the QuestionnaireVerificationSectionDataSourceDesigner class.
	/// </summary>
	internal class QuestionnaireVerificationSectionDataSourceActionList : DesignerActionList
	{
		private QuestionnaireVerificationSectionDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the QuestionnaireVerificationSectionDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public QuestionnaireVerificationSectionDataSourceActionList(QuestionnaireVerificationSectionDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public QuestionnaireVerificationSectionSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion QuestionnaireVerificationSectionDataSourceActionList
	
	#endregion QuestionnaireVerificationSectionDataSourceDesigner
	
	#region QuestionnaireVerificationSectionSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the QuestionnaireVerificationSectionDataSource.SelectMethod property.
	/// </summary>
	public enum QuestionnaireVerificationSectionSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetBySectionResponseId method.
		/// </summary>
		GetBySectionResponseId,
		/// <summary>
		/// Represents the GetByQuestionnaireId method.
		/// </summary>
		GetByQuestionnaireId
	}
	
	#endregion QuestionnaireVerificationSectionSelectMethod

	#region QuestionnaireVerificationSectionFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireVerificationSection"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireVerificationSectionFilter : SqlFilter<QuestionnaireVerificationSectionColumn>
	{
	}
	
	#endregion QuestionnaireVerificationSectionFilter

	#region QuestionnaireVerificationSectionExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireVerificationSection"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireVerificationSectionExpressionBuilder : SqlExpressionBuilder<QuestionnaireVerificationSectionColumn>
	{
	}
	
	#endregion QuestionnaireVerificationSectionExpressionBuilder	

	#region QuestionnaireVerificationSectionProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;QuestionnaireVerificationSectionChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireVerificationSection"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireVerificationSectionProperty : ChildEntityProperty<QuestionnaireVerificationSectionChildEntityTypes>
	{
	}
	
	#endregion QuestionnaireVerificationSectionProperty
}

