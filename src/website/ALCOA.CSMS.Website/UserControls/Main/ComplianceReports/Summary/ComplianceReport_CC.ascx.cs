using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using KaiZen.CSMS.Data;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Services;

using System.ComponentModel;
using System.Reflection;

public partial class UserControls_Tiny_ComplianceReports_ComplianceReport_CC : System.Web.UI.UserControl
{
    Auth auth = new Auth();
    bool uptodate = false;
    protected void Page_Load(object sender, EventArgs e) {  Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); }

    private enum TrafficLightColor
    {
        [Description("~/Images/TrafficLights/tlRed.gif")]
        Red = 0,
        [Description("~/Images/TrafficLights/tlYellow.gif")]
        Yellow = 1,
        [Description("~/Images/TrafficLights/tlGreen.gif")]
        Green = 2,
        [Description("~/Images/redcross.gif")]
        RedCross = 3,
    }

    public static string GetImageURL(Enum value)
    {
        FieldInfo fi = value.GetType().GetField(value.ToString());
        DescriptionAttribute[] attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);
        return (attributes.Length > 0) ? attributes[0].Description : value.ToString();
    }


    private void moduleInit(bool postBack)
    {
        switch (auth.RoleId)
        {
            case ((int)RoleList.Reader):
                break;
            case ((int)RoleList.Contractor):
                break;
            case ((int)RoleList.Administrator):
                break;
            default:
                // do something
                Response.Redirect(String.Format("AccessDenied.aspx?error={0}", Server.UrlEncode("Unrecognised User.")));
                break;
        }
		
		//Added By Bishwajit for Item#31
        //Session Created by Bishwajit Sahoo to Create PDF files
        Session["ComplianceReportCC_lblText"] = null;
        Session["ComplianceReportCC_imgAlt"] = null;
        Session["ComplianceReportCC_imgURL"] = null;
		//End Added By Bishwajit for Item#31
		
        int CompanyId = Convert.ToInt32(SessionHandler.spVar_CompanyId);
        int count = 0;
        int iCount = 0;
        int fCount = 0;

        //--Contacts List
        ContactsContractorsFilters querycContacts = new ContactsContractorsFilters();
        querycContacts.Append(ContactsContractorsColumn.CompanyId, CompanyId.ToString());
        querycContacts.Append(ContactsContractorsColumn.ContactRole, "Principal");
        TList<ContactsContractors> cContactsList = DataRepository.ContactsContractorsProvider.GetPaged(querycContacts.ToString(), null, 0, 100, out count);
        if (count > 0) { } else { iCount += 1; };
        count = 0;

        querycContacts.Clear();
        querycContacts.Append(ContactsContractorsColumn.CompanyId, CompanyId.ToString());
        querycContacts.Append(ContactsContractorsColumn.ContactRole, "KPI%Administration%Contact");
        cContactsList = DataRepository.ContactsContractorsProvider.GetPaged(querycContacts.ToString(), null, 0, 100, out count);
        if (count > 0) { } else { iCount += 1; };
        count = 0;

        querycContacts.Clear();
        querycContacts.Append(ContactsContractorsColumn.CompanyId, CompanyId.ToString());
        querycContacts.Append(ContactsContractorsColumn.ContactRole, "Contract%Manager");
        cContactsList = DataRepository.ContactsContractorsProvider.GetPaged(querycContacts.ToString(), null, 0, 100, out count);
        if (count > 0) { } else { iCount += 1; };
        count = 0;

        querycContacts.Clear();
        querycContacts.Append(ContactsContractorsColumn.CompanyId, CompanyId.ToString());
        querycContacts.Append(ContactsContractorsColumn.ContactRole, "Operation%Manager");
        cContactsList = DataRepository.ContactsContractorsProvider.GetPaged(querycContacts.ToString(), null, 0, 100, out count);
        if (count > 0) { } else { iCount += 1; };
        count = 0;

        bool contactsOutofDate = false;
        DateTime nowMinus3 = DateTime.Now.AddMonths(-3);
        CompaniesService _cService = new CompaniesService();
        Companies c = _cService.GetByCompanyId(CompanyId);
        if (c.ContactsLastUpdated != null)
        {
            if (c.ContactsLastUpdated <= nowMinus3)
            {
                contactsOutofDate = true;
            }
        }
        else
        {
            contactsOutofDate = true;
        }

        string lastUpdated = c.ContactsLastUpdated.ToString();
        if(String.IsNullOrEmpty(lastUpdated)) { lastUpdated = "-"; };
        Label1.Text = String.Format("Last Updated: {0}", lastUpdated);
        if (iCount != 0)
        {
            Image1.ImageUrl = GetImageURL(TrafficLightColor.Red);
            if (contactsOutofDate)
            {
                uptodate = true;
                Image1.ToolTip = "Your company's contact list has not been verified as up-to-date within the last 3 months and also does not include the minimum entries of: Principal, KPI Administrator, Contract Manager and Operation Manager.";
            }
            else
            {
                Image1.ToolTip = "Your company's contact list does not include the minimum entries of: Principal, KPI Administrator, Contract Manager and Operation Manager.";
            }
            fCount += 1;
        }
        else
        {
            if (contactsOutofDate)
            {
                Image1.ImageUrl = GetImageURL(TrafficLightColor.Red);
                Image1.ToolTip = "Your company's contact list has not been verified as up-to-date within the last 3 months.";
            }
            else
            {
                Image1.ImageUrl = GetImageURL(TrafficLightColor.Green);
                Image1.ToolTip = "Good Work. Your company's contact list has the minimum entries of: Principal, KPI Administrator, Contractor Manager and Operation Manager.";
            }
        }
		//Added By Bishwajit for Item#31
        //Session Created by Bishwajit Sahoo to Create PDF files
        Session["ComplianceReportCC_lblText"]=Label1.Text;
        Session["ComplianceReportCC_imgAlt"] = Image1.ToolTip;
        Session["ComplianceReportCC_imgURL"] = Image1.ImageUrl;
    	//Added By Bishwajit for Item#31
	}

    public int getExpected()
    {
        if (uptodate == true) { return 1; } else { return 0; };
    }
}
