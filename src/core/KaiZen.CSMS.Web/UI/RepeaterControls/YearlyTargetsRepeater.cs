﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.ComponentModel;
using System.Web.UI.Design.WebControls;
using System.Web.UI.Design;
using System.Web.UI.WebControls;

namespace KaiZen.CSMS.Web.UI
{
    /// <summary>
    /// A designer class for a strongly typed repeater <c>YearlyTargetsRepeater</c>
    /// </summary>
	public class YearlyTargetsRepeaterDesigner : System.Web.UI.Design.ControlDesigner
	{
	    /// <summary>
        /// Initializes a new instance of the <see cref="T:YearlyTargetsRepeaterDesigner"/> class.
        /// </summary>
		public YearlyTargetsRepeaterDesigner()
		{
		}

        /// <summary>
        /// Initializes the control designer and loads the specified component.
        /// </summary>
        /// <param name="component">The control being designed.</param>
		public override void Initialize(IComponent component)
		{
			if (!(component is YearlyTargetsRepeater))
			{ 
				throw new ArgumentException("Component is not a YearlyTargetsRepeater."); 
			} 
			base.Initialize(component); 
			base.SetViewFlags(ViewFlags.TemplateEditing, true); 
		}


		/// <summary>
		/// Generate HTML for the designer
		/// </summary>
		/// <returns>a string of design time HTML</returns>
		public override string GetDesignTimeHtml()
		{

			// Get the instance this designer applies to
			//
			YearlyTargetsRepeater z = (YearlyTargetsRepeater)Component;
			z.DataBind();

			return base.GetDesignTimeHtml();

			//return z.RenderAtDesignTime();

			//	ControlCollection c = z.Controls;
			//Totem z = (Totem) Component;
			//Totem z = (Totem) Component;
			//return ("<div style='border: 1px gray dotted; background-color: lightgray'><b>TagStat :</b> zae |  qsdds</div>");

		}
	}

    /// <summary>
    /// A strongly typed repeater control for the <see cref="YearlyTargetsRepeater"/> Type.
    /// </summary>
	[Designer(typeof(YearlyTargetsRepeaterDesigner))]
	[ParseChildren(true)]
	[ToolboxData("<{0}:YearlyTargetsRepeater runat=\"server\"></{0}:YearlyTargetsRepeater>")]
	public class YearlyTargetsRepeater : CompositeDataBoundControl, System.Web.UI.INamingContainer
	{
	    /// <summary>
        /// Initializes a new instance of the <see cref="T:YearlyTargetsRepeater"/> class.
        /// </summary>
		public YearlyTargetsRepeater()
		{
		}

		/// <summary>
        /// Gets a <see cref="T:System.Web.UI.ControlCollection"></see> object that represents the child controls for a specified server control in the UI hierarchy.
        /// </summary>
        /// <value></value>
        /// <returns>The collection of child controls for the specified server control.</returns>
		public override ControlCollection Controls
		{
			get
			{
				this.EnsureChildControls();
				return base.Controls;
			}
		}

		private ITemplate m_headerTemplate;
		/// <summary>
        /// Gets or sets the header template.
        /// </summary>
        /// <value>The header template.</value>
		[Browsable(false)]
		[TemplateContainer(typeof(YearlyTargetsItem))]
		[PersistenceMode(PersistenceMode.InnerDefaultProperty)]
		public ITemplate HeaderTemplate
		{
			get { return m_headerTemplate; }
			set { m_headerTemplate = value; }
		}

		private ITemplate m_itemTemplate;
		/// <summary>
        /// Gets or sets the item template.
        /// </summary>
        /// <value>The item template.</value>
		[Browsable(false)]
		[TemplateContainer(typeof(YearlyTargetsItem))]
		[PersistenceMode(PersistenceMode.InnerDefaultProperty)]
		public ITemplate ItemTemplate
		{
			get { return m_itemTemplate; }
			set { m_itemTemplate = value; }
		}

		private ITemplate m_seperatorTemplate;
        /// <summary>
        /// Gets or sets the Seperator Template
        /// </summary>
        [Browsable(false)]
        [TemplateContainer(typeof(YearlyTargetsItem))]
        [PersistenceMode(PersistenceMode.InnerDefaultProperty)]
        public ITemplate SeperatorTemplate
        {
            get { return m_seperatorTemplate; }
            set { m_seperatorTemplate = value; }
        }
			
		private ITemplate m_altenateItemTemplate;
        /// <summary>
        /// Gets or sets the alternating item template.
        /// </summary>
        /// <value>The alternating item template.</value>
		[Browsable(false)]
		[TemplateContainer(typeof(YearlyTargetsItem))]
		[PersistenceMode(PersistenceMode.InnerDefaultProperty)]
		public ITemplate AlternatingItemTemplate
		{
			get { return m_altenateItemTemplate; }
			set { m_altenateItemTemplate = value; }
		}

		private ITemplate m_footerTemplate;
        /// <summary>
        /// Gets or sets the footer template.
        /// </summary>
        /// <value>The footer template.</value>
		[Browsable(false)]
		[TemplateContainer(typeof(YearlyTargetsItem))]
		[PersistenceMode(PersistenceMode.InnerDefaultProperty)]
		public ITemplate FooterTemplate
		{
			get { return m_footerTemplate; }
			set { m_footerTemplate = value; }
		}

//      /// <summary>
//      /// Called by the ASP.NET page framework to notify server controls that use composition-based implementation to create any child controls they contain in preparation for posting back or rendering.
//      /// </summary>
//		protected override void CreateChildControls()
//      {
//         if (ChildControlsCreated)
//         {
//            return;
//         }

//         Controls.Clear();

//         //Instantiate the Header template (if exists)
//         if (m_headerTemplate != null)
//         {
//            Control headerItem = new Control();
//            m_headerTemplate.InstantiateIn(headerItem);
//            Controls.Add(headerItem);
//         }

//         //Instantiate the Footer template (if exists)
//         if (m_footerTemplate != null)
//         {
//            Control footerItem = new Control();
//            m_footerTemplate.InstantiateIn(footerItem);
//            Controls.Add(footerItem);
//         }
//
//         ChildControlsCreated = true;
//      }
	
		/// <summary>
        /// Overridden and Empty so that span tags are not written
        /// </summary>
        /// <param name="writer"></param>
        public override void RenderBeginTag(HtmlTextWriter writer)
        {
            
        }

        /// <summary>
        /// Overridden and Empty so that span tags are not written
        /// </summary>
        /// <param name="writer"></param>
        public override void RenderEndTag(HtmlTextWriter writer)
        {
                
        }		
		
		/// <summary>
      	/// Called by the ASP.NET page framework to notify server controls that use composition-based implementation to create any child controls they contain in preparation for posting back or rendering.
      	/// </summary>
		protected override int CreateChildControls(System.Collections.IEnumerable dataSource, bool dataBinding)
      	{
         int pos = 0;

         if (dataBinding)
         {
            //Instantiate the Header template (if exists)
            if (m_headerTemplate != null)
            {
                Control headerItem = new Control();
                m_headerTemplate.InstantiateIn(headerItem);
                Controls.Add(headerItem);
            }
			if (dataSource != null)
			{
				foreach (object o in dataSource)
				{
						KaiZen.CSMS.Entities.YearlyTargets entity = o as KaiZen.CSMS.Entities.YearlyTargets;
						YearlyTargetsItem container = new YearlyTargetsItem(entity);
	
						if (m_itemTemplate != null && (pos % 2) == 0)
						{
							m_itemTemplate.InstantiateIn(container);
							
							if (m_seperatorTemplate != null)
							{
								m_seperatorTemplate.InstantiateIn(container);
							}
						}
						else
						{
							if (m_altenateItemTemplate != null)
							{
								m_altenateItemTemplate.InstantiateIn(container);
								
								if (m_seperatorTemplate != null)
								{
									m_seperatorTemplate.InstantiateIn(container);
								}
								
							}
							else if (m_itemTemplate != null)
							{
								m_itemTemplate.InstantiateIn(container);
								
								if (m_seperatorTemplate != null)
								{
									m_seperatorTemplate.InstantiateIn(container);
								}
							}
							else
							{
								// no template !!!
							}
						}
						Controls.Add(container);
						
						container.DataBind();
						
						pos++;
				}
			}
            //Instantiate the Footer template (if exists)
            if (m_footerTemplate != null)
            {
                Control footerItem = new Control();
                m_footerTemplate.InstantiateIn(footerItem);
                Controls.Add(footerItem);
            }

		}
			
			return pos;
		}

        /// <summary>
        /// Raises the <see cref="E:System.Web.UI.Control.PreRender"></see> event.
        /// </summary>
        /// <param name="e">An <see cref="T:System.EventArgs"></see> object that contains the event data.</param>
		protected override void OnPreRender(EventArgs e)
		{
			base.DataBind();
		}

		#region Design time
        /// <summary>
        /// Renders at design time.
        /// </summary>
        /// <returns>a  string of the Designed HTML</returns>
		internal string RenderAtDesignTime()
		{			
			return "Designer currently not implemented"; 
		}

		#endregion
	}

    /// <summary>
    /// A wrapper type for the entity
    /// </summary>
	[System.ComponentModel.ToolboxItem(false)]
	public class YearlyTargetsItem : System.Web.UI.Control, System.Web.UI.INamingContainer
	{
		private KaiZen.CSMS.Entities.YearlyTargets _entity;

        /// <summary>
        /// Initializes a new instance of the <see cref="T:YearlyTargetsItem"/> class.
        /// </summary>
		public YearlyTargetsItem()
			: base()
		{ }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:YearlyTargetsItem"/> class.
        /// </summary>
		public YearlyTargetsItem(KaiZen.CSMS.Entities.YearlyTargets entity)
			: base()
		{
			_entity = entity;
		}
		
        /// <summary>
        /// Gets the Id
        /// </summary>
        /// <value>The Id.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32 Id
		{
			get { return _entity.Id; }
		}
        /// <summary>
        /// Gets the Year
        /// </summary>
        /// <value>The Year.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32 Year
		{
			get { return _entity.Year; }
		}
        /// <summary>
        /// Gets the EhsAudits
        /// </summary>
        /// <value>The EhsAudits.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? EhsAudits
		{
			get { return _entity.EhsAudits; }
		}
        /// <summary>
        /// Gets the WorkplaceSafetyCompliance
        /// </summary>
        /// <value>The WorkplaceSafetyCompliance.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? WorkplaceSafetyCompliance
		{
			get { return _entity.WorkplaceSafetyCompliance; }
		}
        /// <summary>
        /// Gets the HealthSafetyWorkContacts
        /// </summary>
        /// <value>The HealthSafetyWorkContacts.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? HealthSafetyWorkContacts
		{
			get { return _entity.HealthSafetyWorkContacts; }
		}
        /// <summary>
        /// Gets the BehaviouralSafetyProgram
        /// </summary>
        /// <value>The BehaviouralSafetyProgram.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Decimal? BehaviouralSafetyProgram
		{
			get { return _entity.BehaviouralSafetyProgram; }
		}
        /// <summary>
        /// Gets the QuarterlyAuditScore
        /// </summary>
        /// <value>The QuarterlyAuditScore.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? QuarterlyAuditScore
		{
			get { return _entity.QuarterlyAuditScore; }
		}
        /// <summary>
        /// Gets the ToolboxMeetingsPerMonth
        /// </summary>
        /// <value>The ToolboxMeetingsPerMonth.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? ToolboxMeetingsPerMonth
		{
			get { return _entity.ToolboxMeetingsPerMonth; }
		}
        /// <summary>
        /// Gets the AlcoaWeeklyContractorsMeeting
        /// </summary>
        /// <value>The AlcoaWeeklyContractorsMeeting.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? AlcoaWeeklyContractorsMeeting
		{
			get { return _entity.AlcoaWeeklyContractorsMeeting; }
		}
        /// <summary>
        /// Gets the AlcoaMonthlyContractorsMeeting
        /// </summary>
        /// <value>The AlcoaMonthlyContractorsMeeting.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? AlcoaMonthlyContractorsMeeting
		{
			get { return _entity.AlcoaMonthlyContractorsMeeting; }
		}
        /// <summary>
        /// Gets the SafetyPlan
        /// </summary>
        /// <value>The SafetyPlan.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? SafetyPlan
		{
			get { return _entity.SafetyPlan; }
		}
        /// <summary>
        /// Gets the JsaScore
        /// </summary>
        /// <value>The JsaScore.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? JsaScore
		{
			get { return _entity.JsaScore; }
		}
        /// <summary>
        /// Gets the FatalityPrevention
        /// </summary>
        /// <value>The FatalityPrevention.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? FatalityPrevention
		{
			get { return _entity.FatalityPrevention; }
		}
        /// <summary>
        /// Gets the Trifr
        /// </summary>
        /// <value>The Trifr.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Decimal? Trifr
		{
			get { return _entity.Trifr; }
		}
        /// <summary>
        /// Gets the Aifr
        /// </summary>
        /// <value>The Aifr.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Decimal? Aifr
		{
			get { return _entity.Aifr; }
		}
        /// <summary>
        /// Gets the Training
        /// </summary>
        /// <value>The Training.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Training
		{
			get { return _entity.Training; }
		}
        /// <summary>
        /// Gets the MedicalSchedule
        /// </summary>
        /// <value>The MedicalSchedule.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? MedicalSchedule
		{
			get { return _entity.MedicalSchedule; }
		}
        /// <summary>
        /// Gets the TrainingSchedule
        /// </summary>
        /// <value>The TrainingSchedule.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? TrainingSchedule
		{
			get { return _entity.TrainingSchedule; }
		}

        /// <summary>
        /// Gets a <see cref="T:KaiZen.CSMS.Entities.YearlyTargets"></see> object
        /// </summary>
        /// <value></value>
        [System.ComponentModel.Bindable(true)]
        public KaiZen.CSMS.Entities.YearlyTargets Entity
        {
            get { return _entity; }
        }
	}
}
