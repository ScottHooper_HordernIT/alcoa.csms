﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.CsmsAccessProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(CsmsAccessDataSourceDesigner))]
	public class CsmsAccessDataSource : ProviderDataSource<CsmsAccess, CsmsAccessKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CsmsAccessDataSource class.
		/// </summary>
		public CsmsAccessDataSource() : base(new CsmsAccessService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the CsmsAccessDataSourceView used by the CsmsAccessDataSource.
		/// </summary>
		protected CsmsAccessDataSourceView CsmsAccessView
		{
			get { return ( View as CsmsAccessDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the CsmsAccessDataSource control invokes to retrieve data.
		/// </summary>
		public CsmsAccessSelectMethod SelectMethod
		{
			get
			{
				CsmsAccessSelectMethod selectMethod = CsmsAccessSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (CsmsAccessSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the CsmsAccessDataSourceView class that is to be
		/// used by the CsmsAccessDataSource.
		/// </summary>
		/// <returns>An instance of the CsmsAccessDataSourceView class.</returns>
		protected override BaseDataSourceView<CsmsAccess, CsmsAccessKey> GetNewDataSourceView()
		{
			return new CsmsAccessDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the CsmsAccessDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class CsmsAccessDataSourceView : ProviderDataSourceView<CsmsAccess, CsmsAccessKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CsmsAccessDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the CsmsAccessDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public CsmsAccessDataSourceView(CsmsAccessDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal CsmsAccessDataSource CsmsAccessOwner
		{
			get { return Owner as CsmsAccessDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal CsmsAccessSelectMethod SelectMethod
		{
			get { return CsmsAccessOwner.SelectMethod; }
			set { CsmsAccessOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal CsmsAccessService CsmsAccessProvider
		{
			get { return Provider as CsmsAccessService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<CsmsAccess> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<CsmsAccess> results = null;
			CsmsAccess item;
			count = 0;
			
			System.Int32 _csmsAccessId;
			System.String _accessName;

			switch ( SelectMethod )
			{
				case CsmsAccessSelectMethod.Get:
					CsmsAccessKey entityKey  = new CsmsAccessKey();
					entityKey.Load(values);
					item = CsmsAccessProvider.Get(entityKey);
					results = new TList<CsmsAccess>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case CsmsAccessSelectMethod.GetAll:
                    results = CsmsAccessProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case CsmsAccessSelectMethod.GetPaged:
					results = CsmsAccessProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case CsmsAccessSelectMethod.Find:
					if ( FilterParameters != null )
						results = CsmsAccessProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = CsmsAccessProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case CsmsAccessSelectMethod.GetByCsmsAccessId:
					_csmsAccessId = ( values["CsmsAccessId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["CsmsAccessId"], typeof(System.Int32)) : (int)0;
					item = CsmsAccessProvider.GetByCsmsAccessId(_csmsAccessId);
					results = new TList<CsmsAccess>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				case CsmsAccessSelectMethod.GetByAccessName:
					_accessName = ( values["AccessName"] != null ) ? (System.String) EntityUtil.ChangeType(values["AccessName"], typeof(System.String)) : string.Empty;
					item = CsmsAccessProvider.GetByAccessName(_accessName);
					results = new TList<CsmsAccess>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// FK
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == CsmsAccessSelectMethod.Get || SelectMethod == CsmsAccessSelectMethod.GetByCsmsAccessId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				CsmsAccess entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					CsmsAccessProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<CsmsAccess> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			CsmsAccessProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region CsmsAccessDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the CsmsAccessDataSource class.
	/// </summary>
	public class CsmsAccessDataSourceDesigner : ProviderDataSourceDesigner<CsmsAccess, CsmsAccessKey>
	{
		/// <summary>
		/// Initializes a new instance of the CsmsAccessDataSourceDesigner class.
		/// </summary>
		public CsmsAccessDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public CsmsAccessSelectMethod SelectMethod
		{
			get { return ((CsmsAccessDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new CsmsAccessDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region CsmsAccessDataSourceActionList

	/// <summary>
	/// Supports the CsmsAccessDataSourceDesigner class.
	/// </summary>
	internal class CsmsAccessDataSourceActionList : DesignerActionList
	{
		private CsmsAccessDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the CsmsAccessDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public CsmsAccessDataSourceActionList(CsmsAccessDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public CsmsAccessSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion CsmsAccessDataSourceActionList
	
	#endregion CsmsAccessDataSourceDesigner
	
	#region CsmsAccessSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the CsmsAccessDataSource.SelectMethod property.
	/// </summary>
	public enum CsmsAccessSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByCsmsAccessId method.
		/// </summary>
		GetByCsmsAccessId,
		/// <summary>
		/// Represents the GetByAccessName method.
		/// </summary>
		GetByAccessName
	}
	
	#endregion CsmsAccessSelectMethod

	#region CsmsAccessFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CsmsAccess"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CsmsAccessFilter : SqlFilter<CsmsAccessColumn>
	{
	}
	
	#endregion CsmsAccessFilter

	#region CsmsAccessExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CsmsAccess"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CsmsAccessExpressionBuilder : SqlExpressionBuilder<CsmsAccessColumn>
	{
	}
	
	#endregion CsmsAccessExpressionBuilder	

	#region CsmsAccessProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;CsmsAccessChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="CsmsAccess"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CsmsAccessProperty : ChildEntityProperty<CsmsAccessChildEntityTypes>
	{
	}
	
	#endregion CsmsAccessProperty
}

