﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.UserPrivilegeProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(UserPrivilegeDataSourceDesigner))]
	public class UserPrivilegeDataSource : ProviderDataSource<UserPrivilege, UserPrivilegeKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UserPrivilegeDataSource class.
		/// </summary>
		public UserPrivilegeDataSource() : base(new UserPrivilegeService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the UserPrivilegeDataSourceView used by the UserPrivilegeDataSource.
		/// </summary>
		protected UserPrivilegeDataSourceView UserPrivilegeView
		{
			get { return ( View as UserPrivilegeDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the UserPrivilegeDataSource control invokes to retrieve data.
		/// </summary>
		public UserPrivilegeSelectMethod SelectMethod
		{
			get
			{
				UserPrivilegeSelectMethod selectMethod = UserPrivilegeSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (UserPrivilegeSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the UserPrivilegeDataSourceView class that is to be
		/// used by the UserPrivilegeDataSource.
		/// </summary>
		/// <returns>An instance of the UserPrivilegeDataSourceView class.</returns>
		protected override BaseDataSourceView<UserPrivilege, UserPrivilegeKey> GetNewDataSourceView()
		{
			return new UserPrivilegeDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the UserPrivilegeDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class UserPrivilegeDataSourceView : ProviderDataSourceView<UserPrivilege, UserPrivilegeKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UserPrivilegeDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the UserPrivilegeDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public UserPrivilegeDataSourceView(UserPrivilegeDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal UserPrivilegeDataSource UserPrivilegeOwner
		{
			get { return Owner as UserPrivilegeDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal UserPrivilegeSelectMethod SelectMethod
		{
			get { return UserPrivilegeOwner.SelectMethod; }
			set { UserPrivilegeOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal UserPrivilegeService UserPrivilegeProvider
		{
			get { return Provider as UserPrivilegeService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<UserPrivilege> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<UserPrivilege> results = null;
			UserPrivilege item;
			count = 0;
			
			System.Int32 _userPrivilegeId;
			System.Int32 _userId;
			System.Int32 _privilegeId;
			System.Int32 _createdByUserId;
			System.Int32 _modifiedByUserId;

			switch ( SelectMethod )
			{
				case UserPrivilegeSelectMethod.Get:
					UserPrivilegeKey entityKey  = new UserPrivilegeKey();
					entityKey.Load(values);
					item = UserPrivilegeProvider.Get(entityKey);
					results = new TList<UserPrivilege>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case UserPrivilegeSelectMethod.GetAll:
                    results = UserPrivilegeProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case UserPrivilegeSelectMethod.GetPaged:
					results = UserPrivilegeProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case UserPrivilegeSelectMethod.Find:
					if ( FilterParameters != null )
						results = UserPrivilegeProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = UserPrivilegeProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case UserPrivilegeSelectMethod.GetByUserPrivilegeId:
					_userPrivilegeId = ( values["UserPrivilegeId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["UserPrivilegeId"], typeof(System.Int32)) : (int)0;
					item = UserPrivilegeProvider.GetByUserPrivilegeId(_userPrivilegeId);
					results = new TList<UserPrivilege>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				case UserPrivilegeSelectMethod.GetByUserIdPrivilegeId:
					_userId = ( values["UserId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["UserId"], typeof(System.Int32)) : (int)0;
					_privilegeId = ( values["PrivilegeId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["PrivilegeId"], typeof(System.Int32)) : (int)0;
					item = UserPrivilegeProvider.GetByUserIdPrivilegeId(_userId, _privilegeId);
					results = new TList<UserPrivilege>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// FK
				case UserPrivilegeSelectMethod.GetByCreatedByUserId:
					_createdByUserId = ( values["CreatedByUserId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["CreatedByUserId"], typeof(System.Int32)) : (int)0;
					results = UserPrivilegeProvider.GetByCreatedByUserId(_createdByUserId, this.StartIndex, this.PageSize, out count);
					break;
				case UserPrivilegeSelectMethod.GetByModifiedByUserId:
					_modifiedByUserId = ( values["ModifiedByUserId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["ModifiedByUserId"], typeof(System.Int32)) : (int)0;
					results = UserPrivilegeProvider.GetByModifiedByUserId(_modifiedByUserId, this.StartIndex, this.PageSize, out count);
					break;
				case UserPrivilegeSelectMethod.GetByPrivilegeId:
					_privilegeId = ( values["PrivilegeId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["PrivilegeId"], typeof(System.Int32)) : (int)0;
					results = UserPrivilegeProvider.GetByPrivilegeId(_privilegeId, this.StartIndex, this.PageSize, out count);
					break;
				case UserPrivilegeSelectMethod.GetByUserId:
					_userId = ( values["UserId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["UserId"], typeof(System.Int32)) : (int)0;
					results = UserPrivilegeProvider.GetByUserId(_userId, this.StartIndex, this.PageSize, out count);
					break;
				// M:M
				// Custom
				case UserPrivilegeSelectMethod.GetAll_ProjectHoursVisibleForAllCompanies:
					results = UserPrivilegeProvider.GetAll_ProjectHoursVisibleForAllCompanies(StartIndex, PageSize);
					break;
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == UserPrivilegeSelectMethod.Get || SelectMethod == UserPrivilegeSelectMethod.GetByUserPrivilegeId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				UserPrivilege entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					UserPrivilegeProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<UserPrivilege> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			UserPrivilegeProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region UserPrivilegeDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the UserPrivilegeDataSource class.
	/// </summary>
	public class UserPrivilegeDataSourceDesigner : ProviderDataSourceDesigner<UserPrivilege, UserPrivilegeKey>
	{
		/// <summary>
		/// Initializes a new instance of the UserPrivilegeDataSourceDesigner class.
		/// </summary>
		public UserPrivilegeDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public UserPrivilegeSelectMethod SelectMethod
		{
			get { return ((UserPrivilegeDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new UserPrivilegeDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region UserPrivilegeDataSourceActionList

	/// <summary>
	/// Supports the UserPrivilegeDataSourceDesigner class.
	/// </summary>
	internal class UserPrivilegeDataSourceActionList : DesignerActionList
	{
		private UserPrivilegeDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the UserPrivilegeDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public UserPrivilegeDataSourceActionList(UserPrivilegeDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public UserPrivilegeSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion UserPrivilegeDataSourceActionList
	
	#endregion UserPrivilegeDataSourceDesigner
	
	#region UserPrivilegeSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the UserPrivilegeDataSource.SelectMethod property.
	/// </summary>
	public enum UserPrivilegeSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByUserPrivilegeId method.
		/// </summary>
		GetByUserPrivilegeId,
		/// <summary>
		/// Represents the GetByUserIdPrivilegeId method.
		/// </summary>
		GetByUserIdPrivilegeId,
		/// <summary>
		/// Represents the GetByCreatedByUserId method.
		/// </summary>
		GetByCreatedByUserId,
		/// <summary>
		/// Represents the GetByModifiedByUserId method.
		/// </summary>
		GetByModifiedByUserId,
		/// <summary>
		/// Represents the GetByPrivilegeId method.
		/// </summary>
		GetByPrivilegeId,
		/// <summary>
		/// Represents the GetByUserId method.
		/// </summary>
		GetByUserId,
		/// <summary>
		/// Represents the GetAll_ProjectHoursVisibleForAllCompanies method.
		/// </summary>
		GetAll_ProjectHoursVisibleForAllCompanies
	}
	
	#endregion UserPrivilegeSelectMethod

	#region UserPrivilegeFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="UserPrivilege"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UserPrivilegeFilter : SqlFilter<UserPrivilegeColumn>
	{
	}
	
	#endregion UserPrivilegeFilter

	#region UserPrivilegeExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="UserPrivilege"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UserPrivilegeExpressionBuilder : SqlExpressionBuilder<UserPrivilegeColumn>
	{
	}
	
	#endregion UserPrivilegeExpressionBuilder	

	#region UserPrivilegeProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;UserPrivilegeChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="UserPrivilege"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UserPrivilegeProperty : ChildEntityProperty<UserPrivilegeChildEntityTypes>
	{
	}
	
	#endregion UserPrivilegeProperty
}

