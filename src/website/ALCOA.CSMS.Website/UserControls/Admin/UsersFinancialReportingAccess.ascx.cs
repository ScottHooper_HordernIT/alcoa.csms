using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxTabControl;
using DevExpress.Web.Data;
using DevExpress.Web.ASPxGridView;
using KaiZen.CSMS.Services;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

public partial class Adminv2_UserControls_UsersFinancialReportingAccess : System.Web.UI.UserControl
{

    Auth auth = new Auth();

    protected void Page_Load(object sender, EventArgs e) {  Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); }

    private void moduleInit(bool postBack)
    {
        if (!postBack)
        { //first time load
            if (auth.RoleId != (int)RoleList.Administrator)
            {
                Response.Redirect("default.aspx");
            }
        }
    }

    protected void grid_InitNewRow(object sender, ASPxDataInitNewRowEventArgs e) //Default Values 
    {
    }
    protected void grid_RowInserting(object sender, ASPxDataInsertingEventArgs e)
    {
        e.NewValues["UserId"] = GetMemoText();
    }
    protected void grid_RowUpdating(object sender, ASPxDataUpdatingEventArgs e)
    {

        e.NewValues["UserId"] = GetMemoText();
    }
    protected int GetMemoText()
    {
        GridViewDataColumn cl = ASPxGridView1.Columns["UserId"] as GridViewDataColumn;
        ASPxComboBox cmbEdit = ASPxGridView1.FindEditRowCellTemplateControl(cl, "usersp1") as ASPxComboBox;

        return Convert.ToInt32(cmbEdit.SelectedItem.Value);
    }
}
