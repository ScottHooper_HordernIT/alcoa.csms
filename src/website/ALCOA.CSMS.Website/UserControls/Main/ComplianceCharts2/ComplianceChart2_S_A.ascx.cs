﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Data;
using KaiZen.CSMS.Services;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

using DevExpress.Data;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.Data;
using DevExpress.Web.ASPxPopupControl;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxCallbackPanel;
using DevExpress.Web.ASPxClasses;
using DevExpress.XtraCharts;

using System.Collections;

public partial class UserControls_Main_ComplianceCharts2_ComplianceChart2_S_A : System.Web.UI.UserControl
{
    #region Services

    public Repo.CSMS.Service.Database.ISiteService SiteService { get; set; }

    #endregion

    //User Customisable Colors
    const string CellColourBad = "Red"; //"#ffb8b8";
    const string CellColourGood = "Lime"; //"#def3ca";
    const string CellColourUnsure = "";

    int CompanyId, RegionId, Year, MonthId, CategoryId;
    int NoKpiGreen = 0;
    int NoKpi = 0;

    bool E, NE1, NE2, NE3;

    Auth auth = new Auth();

    Hashtable htCompliance = new Hashtable();

    protected void Page_Load(object sender, EventArgs e) { Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); }
    private void moduleInit(bool postBack)
    {
        if (!postBack)
        {
            try
            {
                //0. Reset
                ResetData();

                //0. Set Summary Calculation (Grid YTD Averages)
                //this.grid2_E.CustomSummaryCalculate += new DevExpress.Data.CustomSummaryEventHandler(grid2_E_CustomSummaryCalculate);

                //1. Check Session Variables
                Helper.Radar.CheckInputVariables(false);

                //2. Assign Session Variables
                CompanyId = Convert.ToInt32(SessionHandler.spVar_CompanyId);
                RegionId = Convert.ToInt32(SessionHandler.spVar_SiteId);
                Year = Convert.ToInt32(SessionHandler.spVar_Year);
                MonthId = Convert.ToInt32(SessionHandler.spVar_MonthId);

                //3. Validate Company Setup Correctly & Set CategoryId
                //Helper.Radar.ValidateCompanyCategorySetup(CompanyId, RegionId);
                //CategoryId = Helper.Radar.GetCompanyCategoryId_ByCompanyIdSiteId(CompanyId, SiteId);

                //4. Generate Table Data
                GenerateData();

                //5. Bind Table Data
                BindData();

                wcSpider_E.Visible = false;
                wcSpider_NE1.Visible = false;
                wcSpider_NE2.Visible = false;
                wcSpider_NE3.Visible = false;



                //Session["DropDownVal"] = ddlMonth.Text;
                //if (divRawScores.Visible)
                //{
                //    Session["RawVisible"] = true;
                //}
                //else
                //{
                //    Session["RawVisible"] = false;
                //}
                //if (wcSpider.Visible)
                //{
                //    Session["SpiderIsVisible"] = true;
                //}
                //else
                //{
                //    Session["SpiderIsVisible"] = false;
                //}

                //Added By Bishwajit for Item#31
				if(E)
                {Session["E"]=true;}
                else{ Session["E"]=false;}

                if(NE1)
                { Session["NE1"]=true;}
                else{ Session["NE1"]=false;}

                if(NE2)
                { Session["NE2"]=true;}
                else{ Session["NE2"]=false;}

                if(NE3)
                { Session["NE3"]=true;}
                else{ Session["NE3"]=false;}
				//End Added By Bishwajit for Item#31

            }
            catch (Exception ex)
            {
                lblResidential.Text = "Error: " + ex.Message;
                //throw new Exception(ex.Message);
            }
        }
    }

    protected void ResetData()
    {
        E = false;
        NE1 = false;
        NE2 = false;
        NE3 = false;

        NoKpi = 0;
        NoKpiGreen = 0;

        htCompliance.Clear();

        Session["Radar_Table1-Summary"] = null;
        Session["Radar_Table2-Ytd"] = null;
        Session["Radar_Table3-Spider"] = null;

        ASPxPageControl1.TabPages[0].Enabled = false;
        ASPxPageControl1.TabPages[1].Enabled = false;
        ASPxPageControl1.TabPages[2].Enabled = false;
        ASPxPageControl1.TabPages[3].Enabled = false;

        grid_E.DataSource = null;
        grid_E.Columns.Clear();
        grid2_E.DataSource = null;
        grid2_E.Columns.Clear();
        wcSpider_E.Series.Clear();
        wcSpider_E.DataSource = null;
        wcSpider_E.DataSourceID = String.Empty;

        grid_NE1.DataSource = null;
        grid_NE1.Columns.Clear();
        grid2_NE1.DataSource = null;
        grid2_NE1.Columns.Clear();
        wcSpider_NE1.Series.Clear();
        wcSpider_NE1.DataSource = null;
        wcSpider_NE1.DataSourceID = String.Empty;

        grid_E.DataSource = null;
        grid_NE2.Columns.Clear();
        grid2_NE2.DataSource = null;
        grid2_NE2.Columns.Clear();
        wcSpider_NE2.Series.Clear();
        wcSpider_NE2.DataSource = null;
        wcSpider_NE2.DataSourceID = String.Empty;

        grid_NE3.DataSource = null;
        grid_NE3.Columns.Clear();
        grid2_NE3.DataSource = null;
        grid2_NE3.Columns.Clear();
        wcSpider_NE3.Series.Clear();
        wcSpider_NE3.DataSource = null;
        wcSpider_NE3.DataSourceID = String.Empty;
    }

    public void CreateGridColumns(int _CompanyId, int _RegionId, int _MonthId, int _CategoryId, ASPxGridView _grid, ASPxGridView _grid2, DevExpress.XtraCharts.Web.WebChartControl _wcSpider)
    {
        //Instantiate services
        SitesService sService = new SitesService();
        RegionsSitesService rService = new RegionsSitesService();
        CompanySiteCategoryStandardService cscsService = new CompanySiteCategoryStandardService();
        //Instantiate Variables
        int grid_VisibleIndex = 0;

        string[] Sites_E;
        string[] Sites_NE1;
        string[] Sites_NE2;
        string[] Sites_NE3;

        TList<RegionsSites> rsTlist = rService.GetByRegionId(-1 * _RegionId);
        TList<CompanySiteCategoryStandard> cscsTlist = cscsService.GetByCompanyId(_CompanyId);
        
        //Create Grid 1 - Summary
        _grid.Width = Unit.Percentage(100);
        _grid.KeyFieldName = "KpiMeasure";
        grid_VisibleIndex = Helper.Radar.AddGridColumn(_grid, "KpiMeasure", "Key Performance Indicators", "Key Performance Indicators (KPI)", 300, grid_VisibleIndex);
        
        foreach (RegionsSites rs in rsTlist)
        {
            bool cont = false;
            foreach(CompanySiteCategoryStandard cscs in cscsTlist)
            {
                if (cscs.SiteId == rs.SiteId)
                {
                    bool conte = true;
                    if (_CategoryId != null)
                    {
                        if (cscs.CompanySiteCategoryId != _CategoryId) conte = false;
                    }

                    if (conte)
                    {
                        
                        Sites s = sService.GetBySiteId(rs.SiteId);
                        
                        if (s == null) throw new Exception("Error Occurred whilst Loading Site Columns");
                        string SiteName = s.SiteName;
                        if (SiteName.Length > 10) SiteName = s.SiteAbbrev;
                        
                        grid_VisibleIndex = Helper.Radar.AddGridColumn(_grid, String.Format("KpiScoreYtd{0}", SiteName), SiteName, SiteName, 200, grid_VisibleIndex);
                        switch (_CategoryId)
                        {
                            case (int)CompanySiteCategoryList.E:
                                E = true;
                                break;
                            case (int)CompanySiteCategoryList.NE1:
                                NE1 = true;
                                break;
                            case (int)CompanySiteCategoryList.NE2:
                                NE2 = true;
                                break;
                            case (int)CompanySiteCategoryList.NE3:
                                NE3 = true;
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }
        grid_VisibleIndex = 0;

        //Create Grid 2 - YTD
        _grid2.Width = Unit.Percentage(100);
        _grid2.KeyFieldName = "KpiMeasure";
        grid_VisibleIndex = Helper.Radar.AddGridColumn(_grid2, "KpiMeasure", "Key Performance Indicators", "Key Performance Indicators (KPI)", 300, grid_VisibleIndex);
        //grid_VisibleIndex = Helper.Radar.AddGridColumn(_grid2, "PlanYtd", "Plan", "Plan", null, grid_VisibleIndex);

        foreach (RegionsSites rs in rsTlist)
        {
            foreach (CompanySiteCategoryStandard cscs in cscsTlist)
            {
                if (cscs.SiteId == rs.SiteId)
                {
                    bool conte = true;
                    if (_CategoryId != null)
                    {
                        if (cscs.CompanySiteCategoryId != _CategoryId) conte = false;
                    }

                    if (conte)
                    {
                        Sites s = sService.GetBySiteId(rs.SiteId);
                        if (s == null) throw new Exception("Error Occurred whilst Loading Site Columns");
                        string SiteName = s.SiteName;
                        if (SiteName.Length > 10) SiteName = s.SiteAbbrev;
                        string SiteName2 = "Plan (" + SiteName + ")";
                        grid_VisibleIndex = Helper.Radar.AddGridColumn(_grid2, String.Format("PlanYtd{0}", SiteName), SiteName2, SiteName2, null, grid_VisibleIndex);
                        grid_VisibleIndex = Helper.Radar.AddGridColumn(_grid2, String.Format("KpiScoreYtd{0}", SiteName), SiteName, SiteName, null, grid_VisibleIndex);
                    }
                }
            }
        }
    }


    protected DataRow newdr(DataTable dt, int _CompanyId, int _RegionId, string KpiMeasure, int CategoryId, int No)
    {
        DataSet DataSet1_Summary_temp = new DataSet();


        DataRow dr = dt.NewRow();
        dr["KpiMeasure"] = KpiMeasure;
        CompanySiteCategoryService cscService = new CompanySiteCategoryService();
        CompanySiteCategoryStandardService cscsService = new CompanySiteCategoryStandardService();
        RegionsSitesService rService = new RegionsSitesService();
        SitesService sService = new SitesService();
        CompanySiteCategory csc = cscService.GetByCompanySiteCategoryId(CategoryId);
        TList<RegionsSites> rsTlist = rService.GetByRegionId(-1 * RegionId);
        TList<CompanySiteCategoryStandard> cscsTlist = cscsService.GetByCompanyId(CompanyId);
       
        
        foreach (RegionsSites rs in rsTlist)
        {
            foreach (CompanySiteCategoryStandard cscs in cscsTlist)
            {
                if (cscs.SiteId == rs.SiteId)
                {
                    bool cont = true;
                    if (CategoryId != null)
                    {
                        if (cscs.CompanySiteCategoryId != CategoryId) cont = false;
                    }

                    if (cont)
                    {
                        var site = SiteService.Get(e => e.SiteId == rs.SiteId, null);
                        if (site == null) throw new Exception("Error Occurred whilst Loading Site Columns");
                        string SiteName = site.SiteName;
                        if (SiteName.Length > 10) SiteName = site.SiteAbbrev;
                        DataSet1_Summary_temp.Tables.Add(Helper.Radar.CreateDataTable(_CompanyId, site.SiteId, No, null));
                        if (No == 1)
                        {
                            DataSet1_Summary_temp.Tables[0].Rows.Add(Helper.Radar.CreateDataRow_DataSet1_Summary(DataSet1_Summary_temp.Tables[0], KpiMeasure,
                                                                                    CompanyId, site.SiteId, MonthId, Year, CategoryId));
                        }
                        if (No == 2)
                        {
                            DataSet1_Summary_temp.Tables[0].Rows.Add(Helper.Radar.CreateDataRow_DataSet2_Ytd(DataSet1_Summary_temp.Tables[0], KpiMeasure,
                                                                                    CompanyId, site, MonthId, Year, csc));
                        }

                        foreach (DataRow _dr in DataSet1_Summary_temp.Tables[0].Rows)
                        {
                            dr[String.Format("KpiScoreYtd{0}", SiteName)] = _dr["KpiScoreYtd"].ToString();
                            dr[String.Format("PlanYtd{0}", SiteName)] = _dr["PlanYtd"].ToString();

                            if (_dr["PlanYtd"].ToString() != "-" && !String.IsNullOrEmpty(_dr["PlanYtd"].ToString()))
                            {
                                int count = 0;
                                if (!htCompliance.ContainsKey("Plan (" + SiteName + ")"))
                                {
                                    htCompliance.Add("Plan (" + SiteName + ")", 0);
                                }
                                else
                                {
                                    count = (int)htCompliance["Plan (" + SiteName + ")"];
                                };

                                if (_dr["KpiScoreYtd"].ToString() != "-" && !String.IsNullOrEmpty(_dr["KpiScoreYtd"].ToString()))
                                {
                                    Decimal d = Convert.ToDecimal(_dr["KpiScoreYtd"].ToString());
                                    Decimal d2 = Convert.ToDecimal(_dr["PlanYtd"].ToString());

                                    if (d >= d2) count++;
                                }
                                htCompliance["Plan (" + SiteName + ")"] = count;
                            }
                        }
                        DataSet1_Summary_temp.Tables.Clear();
                    }
                }
            }
        }

        return dr;
    }

    protected void GenerateData()
    {
		//Added By Bishwajit for Item#31
        //Added By Bishwajit to check if the proper value is exist or not
        Session["IsValueTrue"] = false;
		//End Added By Bishwajit for Item#31
        CreateGridColumns(CompanyId, RegionId, MonthId, (int)CompanySiteCategoryList.E, grid_E, grid2_E, wcSpider_E);
        CreateGridColumns(CompanyId, RegionId, MonthId, (int)CompanySiteCategoryList.NE1, grid_NE1, grid2_NE1, wcSpider_NE1);
        CreateGridColumns(CompanyId, RegionId, MonthId, (int)CompanySiteCategoryList.NE2, grid_NE2, grid2_NE2, wcSpider_NE2);
        CreateGridColumns(CompanyId, RegionId, MonthId, (int)CompanySiteCategoryList.NE3, grid_NE3, grid2_NE3, wcSpider_NE3);

        
        DataSet DataSet1_Summary = new DataSet();
        DataSet DataSet2 = new DataSet();
        DataSet DataSet2_Ytd = new DataSet();

        DataSet1_Summary.Tables.Add(Helper.Radar.CreateDataTable(CompanyId, RegionId, 1, (int)CompanySiteCategoryList.E));
        DataSet2.Tables.Add(Helper.Radar.CreateDataTable(CompanyId, RegionId, 2, (int)CompanySiteCategoryList.E));
        DataSet2_Ytd.Tables.Add(Helper.Radar.CreateDataTable(CompanyId, RegionId, 2, (int)CompanySiteCategoryList.E));

        DataSet1_Summary.Tables.Add(Helper.Radar.CreateDataTable(CompanyId, RegionId, 1, (int)CompanySiteCategoryList.NE1));
        DataSet2.Tables.Add(Helper.Radar.CreateDataTable(CompanyId, RegionId, 2, (int)CompanySiteCategoryList.NE1));
        DataSet2_Ytd.Tables.Add(Helper.Radar.CreateDataTable(CompanyId, RegionId, 2, (int)CompanySiteCategoryList.NE1));

        DataSet1_Summary.Tables.Add(Helper.Radar.CreateDataTable(CompanyId, RegionId, 1, (int)CompanySiteCategoryList.NE2));
        DataSet2.Tables.Add(Helper.Radar.CreateDataTable(CompanyId, RegionId, 2, (int)CompanySiteCategoryList.NE2));
        DataSet2_Ytd.Tables.Add(Helper.Radar.CreateDataTable(CompanyId, RegionId, 2, (int)CompanySiteCategoryList.NE2));

        DataSet1_Summary.Tables.Add(Helper.Radar.CreateDataTable(CompanyId, RegionId, 1, (int)CompanySiteCategoryList.NE3));
        DataSet2.Tables.Add(Helper.Radar.CreateDataTable(CompanyId, RegionId, 2, (int)CompanySiteCategoryList.NE3));
        DataSet2_Ytd.Tables.Add(Helper.Radar.CreateDataTable(CompanyId, RegionId, 2, (int)CompanySiteCategoryList.NE3));

        DataSet DataSet3_Spider = new DataSet();
        DataTable dtSpiderE = new DataTable();
        dtSpiderE = DataSet3_Spider.Tables.Add(string.Format("Spider{0}", (int)CompanySiteCategoryList.E));
        dtSpiderE.Columns.Add("Key", typeof(string));
        dtSpiderE.Columns.Add("Value", typeof(decimal));
        DataTable dtSpiderNE1 = new DataTable();
        dtSpiderNE1 = DataSet3_Spider.Tables.Add(string.Format("Spider{0}", (int)CompanySiteCategoryList.NE1));
        dtSpiderNE1.Columns.Add("Key", typeof(string));
        dtSpiderNE1.Columns.Add("Value", typeof(decimal));
        DataTable dtSpiderNE2 = new DataTable();
        dtSpiderNE2 = DataSet3_Spider.Tables.Add(string.Format("Spider{0}", (int)CompanySiteCategoryList.NE2));
        dtSpiderNE2.Columns.Add("Key", typeof(string));
        dtSpiderNE2.Columns.Add("Value", typeof(decimal));
        DataTable dtSpiderNE3 = new DataTable();
        dtSpiderNE3 = DataSet3_Spider.Tables.Add(string.Format("Spider{0}", (int)CompanySiteCategoryList.NE3));
        dtSpiderNE3.Columns.Add("Key", typeof(string));
        dtSpiderNE3.Columns.Add("Value", typeof(decimal));


        //NEW****
        foreach (string KpiMeasure in Helper.Radar.GetKpiMeasurements(-1, false, true)) //Summary
        {
            DataSet1_Summary.Tables[0].Rows.Add(newdr(DataSet1_Summary.Tables[0], CompanyId, RegionId, KpiMeasure, (int)CompanySiteCategoryList.E, 1));
            DataSet1_Summary.Tables[1].Rows.Add(newdr(DataSet1_Summary.Tables[1], CompanyId, RegionId, KpiMeasure, (int)CompanySiteCategoryList.NE1, 1));
            DataSet1_Summary.Tables[2].Rows.Add(newdr(DataSet1_Summary.Tables[2], CompanyId, RegionId, KpiMeasure, (int)CompanySiteCategoryList.NE2, 1));
            DataSet1_Summary.Tables[3].Rows.Add(newdr(DataSet1_Summary.Tables[3], CompanyId, RegionId, KpiMeasure, (int)CompanySiteCategoryList.NE3, 1)); 
        }

        foreach (string KpiMeasure in Helper.Radar.GetKpiMeasurements((int)CompanySiteCategoryList.E, false, false)) //Ytd
        {
            if (KpiMeasure != "Behavioural Observations - Plan" && KpiMeasure != "Behavioural Observations - Actual")
            {
                DataSet2.Tables[0].Rows.Add(newdr(DataSet2.Tables[0], CompanyId, RegionId, KpiMeasure, (int)CompanySiteCategoryList.E, 2));
            }
        }

        foreach (string KpiMeasure in Helper.Radar.GetKpiMeasurements((int)CompanySiteCategoryList.NE1, false, false)) //Ytd
        {
            if (KpiMeasure != "Behavioural Observations - Plan" && KpiMeasure != "Behavioural Observations - Actual")
            {
                DataSet2.Tables[1].Rows.Add(newdr(DataSet2.Tables[1], CompanyId, RegionId, KpiMeasure, (int)CompanySiteCategoryList.NE1, 2));
            }
        }

        foreach (string KpiMeasure in Helper.Radar.GetKpiMeasurements((int)CompanySiteCategoryList.NE2, false, false)) //Ytd
        {
            DataSet2.Tables[2].Rows.Add(newdr(DataSet2.Tables[2], CompanyId, RegionId, KpiMeasure, (int)CompanySiteCategoryList.NE2, 2));
        }

        foreach (string KpiMeasure in Helper.Radar.GetKpiMeasurements((int)CompanySiteCategoryList.NE3, false, false)) //Ytd
        {
            if (KpiMeasure != "IFE : Injury Ratio")
            {
                DataSet2.Tables[3].Rows.Add(newdr(DataSet2.Tables[3], CompanyId, RegionId, KpiMeasure, (int)CompanySiteCategoryList.NE3, 2));
            }
        }

        filterGridRows(DataSet2, DataSet2_Ytd, 0);
        filterGridRows(DataSet2, DataSet2_Ytd, 1);
        filterGridRows(DataSet2, DataSet2_Ytd, 2);
        filterGridRows(DataSet2, DataSet2_Ytd, 3);

        Session["Radar_Table1-Summary"] = DataSet1_Summary;
        Session["Radar_Table2-Ytd"] = DataSet2_Ytd;
		//Added By Bishwajit for Item#31
        //Added By Bishwajit to check if the proper value is exist or not
        Session["IsValueTrue"] = true;
		//End Added By Bishwajit for Item#31
    }
    protected void BindData()
    {
        if (NE3)
        {
            grid_NE3.DataSource = (DataTable)(((DataSet)Session["Radar_Table1-Summary"]).Tables[3]);
            grid_NE3.DataBind();
            grid2_NE3.DataSource = (DataTable)(((DataSet)Session["Radar_Table2-Ytd"]).Tables[3]);
            grid2_NE3.DataBind();
            ASPxPageControl1.TabPages[3].Enabled = true;
            ASPxPageControl1.ActiveTabIndex = 3;
        }

        if (NE2)
        {
            grid_NE2.DataSource = (DataTable)(((DataSet)Session["Radar_Table1-Summary"]).Tables[2]);
            grid_NE2.DataBind();
            grid2_NE2.DataSource = (DataTable)(((DataSet)Session["Radar_Table2-Ytd"]).Tables[2]);
            grid2_NE2.DataBind();
            ASPxPageControl1.TabPages[2].Enabled = true;
            ASPxPageControl1.ActiveTabIndex = 2;
        }

        if (NE1)
        {
            grid_NE1.DataSource = (DataTable)(((DataSet)Session["Radar_Table1-Summary"]).Tables[1]);
            grid_NE1.DataBind();
            grid2_NE1.DataSource = (DataTable)(((DataSet)Session["Radar_Table2-Ytd"]).Tables[1]);
            grid2_NE1.DataBind();
            ASPxPageControl1.TabPages[1].Enabled = true;
            ASPxPageControl1.ActiveTabIndex = 1;
        }

        if (E)
        {
            grid_E.DataSource = (DataTable)(((DataSet)Session["Radar_Table1-Summary"]).Tables[0]);
            grid_E.DataBind();
            grid2_E.DataSource = (DataTable)(((DataSet)Session["Radar_Table2-Ytd"]).Tables[0]);
            grid2_E.DataBind();
            ASPxPageControl1.TabPages[0].Enabled = true;
            ASPxPageControl1.ActiveTabIndex = 0;
        }

        lblLastViewedAt_E.Text = String.Format("Last Viewed at: {0}", DateTime.Now);
        lblLastViewedAt_NE1.Text = lblLastViewedAt_E.Text;
        lblLastViewedAt_NE2.Text = lblLastViewedAt_E.Text;
        lblLastViewedAt_NE3.Text = lblLastViewedAt_E.Text;
    }

    protected void filterGridRows(DataSet DataSet2, DataSet DataSet2_Ytd, int No)
    {
        foreach (DataRow dr in DataSet2.Tables[No].Rows)
        {
            if (dr["KpiMeasure"].ToString() != "On site")
            {
                bool skip = false;

                if (Year != DateTime.Now.Year)
                {
                    if (dr["KpiMeasure"].ToString() != "CSA - Self Audit (% Score) - Current Qtr")
                    {
                        if (dr["KpiMeasure"].ToString() == "CSA - Self Audit (% Score) - Previous Qtr")
                        {
                            if (MonthId == 0)
                            {
                                dr["KpiMeasure"] = "CSA - Self Audit (% Score) - Qtr 4";
                            }
                            else
                            {
                                dr["KpiMeasure"] = "CSA - Self Audit (% Score) - Qtr";
                            }
                        }

                        DataSet2_Ytd.Tables[No].ImportRow(dr);

                    }
                }
                else
                {
                    DataSet2_Ytd.Tables[No].ImportRow(dr);

                }
            }
        }
    }

    protected void grid2_E_CustomSummaryCalculate(object sender, DevExpress.Data.CustomSummaryEventArgs e)
    {
        if(E) CustomSummaryCalculation(grid2_E, sender, e); 
    }


    protected void grid2_E_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {
        if (E) ColorYtdGridCells(grid2_E, sender, e);
    }
    protected void grid2_NE1_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {
        if(NE1) ColorYtdGridCells(grid2_NE1, sender, e);
    }
    protected void grid2_NE2_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {
        if(NE2) ColorYtdGridCells(grid2_NE2, sender, e);
    }
    protected void grid2_NE3_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {
        if (NE3) ColorYtdGridCells(grid2_NE3, sender, e);
    }
    protected void ColorYtdGridCells(ASPxGridView _grid, object sender, ASPxGridViewTableRowEventArgs e)
    {
        if (e.RowType != GridViewRowType.Data) return;
        string Kpi = (string)_grid.GetRowValues(e.VisibleIndex, "KpiMeasure");

        if (!String.IsNullOrEmpty(Kpi))
        {
            if (Kpi != "Alcoa Annual Audit Score (%)" && Kpi != "CSA - Self Audit (% Score) - Current Qtr")
            {
                for (int i = 1; i < _grid.Columns.Count; i += 2)
                {
                    string _KpiTarget = (string)_grid.GetRowValues(e.VisibleIndex, _grid.Columns[i].Name);
                    

                    if (!String.IsNullOrEmpty(_KpiTarget))
                    {
                        string colour = "";
                        string _KpiValue = (string)_grid.GetRowValues(e.VisibleIndex, _grid.Columns[i + 1].Name);

                        if (!String.IsNullOrEmpty(_KpiTarget))
                        {
                            if (_KpiValue != "-" && _KpiTarget != "-")
                            {
                                if (_KpiValue == "?")
                                {
                                    colour = CellColourUnsure;
                                }
                                else
                                {
                                    if (_KpiTarget == "0")
                                    {
                                        colour = CellColourGood;
                                    }
                                    else
                                    {
                                        if (String.IsNullOrEmpty(_KpiValue))
                                        {
                                            colour = CellColourBad;
                                        }
                                        else
                                        {
                                            Decimal KpiTarget = Convert.ToDecimal(_KpiTarget);
                                            Decimal KpiValue = Convert.ToDecimal(_KpiValue);

                                            if (KpiValue >= KpiTarget) colour = CellColourGood;
                                            else colour = CellColourBad;
                                        }

                                    }
                                }
                            }
                        }

                        if (!String.IsNullOrEmpty(colour))
                        {
                            e.Row.Cells[i + 1].Style.Add("background-color", colour);
                        }
                    }

                }
            }
        }

    }


    protected void CustomSummaryCalculation(ASPxGridView _grid, object sender, CustomSummaryEventArgs e)
    {
        if (e.SummaryProcess == CustomSummaryProcess.Finalize)
        {
            ASPxSummaryItem _col = (ASPxSummaryItem)e.Item;
            foreach (GridViewDataTextColumn gvCol in _grid.Columns)
            {
                if (gvCol.Caption.Contains("Plan ("))
                {
                    if (_col.FieldName == gvCol.Caption)
                    {
                        if (htCompliance.Contains(gvCol.Caption) && NoKpi > 0)
                        {
                            decimal c = Convert.ToDecimal(htCompliance[gvCol.Caption].ToString());
                            if (c > 0)
                            {
                                decimal c2 = (c / Convert.ToDecimal(NoKpi)) * 100;
                                e.TotalValue = String.Format("{0}%", Convert.ToInt32(c2));
                            }
                            else
                            {
                                e.TotalValue = "0%";
                            }
                        }
                        else
                        {
                            e.TotalValue = "0%";
                        }
                    }
                }
            }
        }
    }
}