﻿#region Using directives

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="EhsimsExceptionsCompaniesSitesProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract class EhsimsExceptionsCompaniesSitesProviderBaseCore : EntityViewProviderBase<EhsimsExceptionsCompaniesSites>
	{
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions
		
		/*
		///<summary>
		/// Fill an VList&lt;EhsimsExceptionsCompaniesSites&gt; From a DataSet
		///</summary>
		/// <param name="dataSet">the DataSet</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList&lt;EhsimsExceptionsCompaniesSites&gt;"/></returns>
		protected static VList&lt;EhsimsExceptionsCompaniesSites&gt; Fill(DataSet dataSet, VList<EhsimsExceptionsCompaniesSites> rows, int start, int pagelen)
		{
			if (dataSet.Tables.Count == 1)
			{
				return Fill(dataSet.Tables[0], rows, start, pagelen);
			}
			else
			{
				return new VList<EhsimsExceptionsCompaniesSites>();
			}	
		}
		
		
		///<summary>
		/// Fill an VList&lt;EhsimsExceptionsCompaniesSites&gt; From a DataTable
		///</summary>
		/// <param name="dataTable">the DataTable that hold the data.</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList<EhsimsExceptionsCompaniesSites>"/></returns>
		protected static VList&lt;EhsimsExceptionsCompaniesSites&gt; Fill(DataTable dataTable, VList<EhsimsExceptionsCompaniesSites> rows, int start, int pagelen)
		{
			int recordnum = 0;
			
			System.Collections.IEnumerator dataRows =  dataTable.Rows.GetEnumerator();
			
			while (dataRows.MoveNext() && (pagelen != 0))
			{
				if(recordnum >= start)
				{
					DataRow row = (DataRow)dataRows.Current;
				
					EhsimsExceptionsCompaniesSites c = new EhsimsExceptionsCompaniesSites();
					c.EhsimsExceptionId = (Convert.IsDBNull(row["EhsimsExceptionId"]))?(int)0:(System.Int32)row["EhsimsExceptionId"];
					c.ReportDateTime = (Convert.IsDBNull(row["ReportDateTime"]))?DateTime.MinValue:(System.DateTime)row["ReportDateTime"];
					c.CompanyId = (Convert.IsDBNull(row["CompanyId"]))?(int)0:(System.Int32?)row["CompanyId"];
					c.SiteId = (Convert.IsDBNull(row["SiteId"]))?(int)0:(System.Int32?)row["SiteId"];
					c.KpiDateTime = (Convert.IsDBNull(row["kpiDateTime"]))?DateTime.MinValue:(System.DateTime?)row["kpiDateTime"];
					c.ErrorMsg = (Convert.IsDBNull(row["ErrorMsg"]))?string.Empty:(System.String)row["ErrorMsg"];
					c.CompanyName = (Convert.IsDBNull(row["CompanyName"]))?string.Empty:(System.String)row["CompanyName"];
					c.SiteName = (Convert.IsDBNull(row["SiteName"]))?string.Empty:(System.String)row["SiteName"];
					c.AcceptChanges();
					rows.Add(c);
					pagelen -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		*/	
						
		///<summary>
		/// Fill an <see cref="VList&lt;EhsimsExceptionsCompaniesSites&gt;"/> From a DataReader.
		///</summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pageLength">number of row.</param>
		///<returns>a <see cref="VList&lt;EhsimsExceptionsCompaniesSites&gt;"/></returns>
		protected VList<EhsimsExceptionsCompaniesSites> Fill(IDataReader reader, VList<EhsimsExceptionsCompaniesSites> rows, int start, int pageLength)
		{
			int recordnum = 0;
			while (reader.Read() && (pageLength != 0))
			{
				if(recordnum >= start)
				{
					EhsimsExceptionsCompaniesSites entity = null;
					if (DataRepository.Provider.UseEntityFactory)
					{
						entity = EntityManager.CreateViewEntity<EhsimsExceptionsCompaniesSites>("EhsimsExceptionsCompaniesSites",  DataRepository.Provider.EntityCreationalFactoryType); 
					}
					else
					{
						entity = new EhsimsExceptionsCompaniesSites();
					}
					
					entity.SuppressEntityEvents = true;

					entity.EhsimsExceptionId = (System.Int32)reader[((int)EhsimsExceptionsCompaniesSitesColumn.EhsimsExceptionId)];
					//entity.EhsimsExceptionId = (Convert.IsDBNull(reader["EhsimsExceptionId"]))?(int)0:(System.Int32)reader["EhsimsExceptionId"];
					entity.ReportDateTime = (System.DateTime)reader[((int)EhsimsExceptionsCompaniesSitesColumn.ReportDateTime)];
					//entity.ReportDateTime = (Convert.IsDBNull(reader["ReportDateTime"]))?DateTime.MinValue:(System.DateTime)reader["ReportDateTime"];
					entity.CompanyId = (reader.IsDBNull(((int)EhsimsExceptionsCompaniesSitesColumn.CompanyId)))?null:(System.Int32?)reader[((int)EhsimsExceptionsCompaniesSitesColumn.CompanyId)];
					//entity.CompanyId = (Convert.IsDBNull(reader["CompanyId"]))?(int)0:(System.Int32?)reader["CompanyId"];
					entity.SiteId = (reader.IsDBNull(((int)EhsimsExceptionsCompaniesSitesColumn.SiteId)))?null:(System.Int32?)reader[((int)EhsimsExceptionsCompaniesSitesColumn.SiteId)];
					//entity.SiteId = (Convert.IsDBNull(reader["SiteId"]))?(int)0:(System.Int32?)reader["SiteId"];
					entity.KpiDateTime = (reader.IsDBNull(((int)EhsimsExceptionsCompaniesSitesColumn.KpiDateTime)))?null:(System.DateTime?)reader[((int)EhsimsExceptionsCompaniesSitesColumn.KpiDateTime)];
					//entity.KpiDateTime = (Convert.IsDBNull(reader["kpiDateTime"]))?DateTime.MinValue:(System.DateTime?)reader["kpiDateTime"];
					entity.ErrorMsg = (reader.IsDBNull(((int)EhsimsExceptionsCompaniesSitesColumn.ErrorMsg)))?null:(System.String)reader[((int)EhsimsExceptionsCompaniesSitesColumn.ErrorMsg)];
					//entity.ErrorMsg = (Convert.IsDBNull(reader["ErrorMsg"]))?string.Empty:(System.String)reader["ErrorMsg"];
					entity.CompanyName = (System.String)reader[((int)EhsimsExceptionsCompaniesSitesColumn.CompanyName)];
					//entity.CompanyName = (Convert.IsDBNull(reader["CompanyName"]))?string.Empty:(System.String)reader["CompanyName"];
					entity.SiteName = (System.String)reader[((int)EhsimsExceptionsCompaniesSitesColumn.SiteName)];
					//entity.SiteName = (Convert.IsDBNull(reader["SiteName"]))?string.Empty:(System.String)reader["SiteName"];
					entity.AcceptChanges();
					entity.SuppressEntityEvents = false;
					
					rows.Add(entity);
					pageLength -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		
		
		/// <summary>
		/// Refreshes the <see cref="EhsimsExceptionsCompaniesSites"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="EhsimsExceptionsCompaniesSites"/> object to refresh.</param>
		protected void RefreshEntity(IDataReader reader, EhsimsExceptionsCompaniesSites entity)
		{
			reader.Read();
			entity.EhsimsExceptionId = (System.Int32)reader[((int)EhsimsExceptionsCompaniesSitesColumn.EhsimsExceptionId)];
			//entity.EhsimsExceptionId = (Convert.IsDBNull(reader["EhsimsExceptionId"]))?(int)0:(System.Int32)reader["EhsimsExceptionId"];
			entity.ReportDateTime = (System.DateTime)reader[((int)EhsimsExceptionsCompaniesSitesColumn.ReportDateTime)];
			//entity.ReportDateTime = (Convert.IsDBNull(reader["ReportDateTime"]))?DateTime.MinValue:(System.DateTime)reader["ReportDateTime"];
			entity.CompanyId = (reader.IsDBNull(((int)EhsimsExceptionsCompaniesSitesColumn.CompanyId)))?null:(System.Int32?)reader[((int)EhsimsExceptionsCompaniesSitesColumn.CompanyId)];
			//entity.CompanyId = (Convert.IsDBNull(reader["CompanyId"]))?(int)0:(System.Int32?)reader["CompanyId"];
			entity.SiteId = (reader.IsDBNull(((int)EhsimsExceptionsCompaniesSitesColumn.SiteId)))?null:(System.Int32?)reader[((int)EhsimsExceptionsCompaniesSitesColumn.SiteId)];
			//entity.SiteId = (Convert.IsDBNull(reader["SiteId"]))?(int)0:(System.Int32?)reader["SiteId"];
			entity.KpiDateTime = (reader.IsDBNull(((int)EhsimsExceptionsCompaniesSitesColumn.KpiDateTime)))?null:(System.DateTime?)reader[((int)EhsimsExceptionsCompaniesSitesColumn.KpiDateTime)];
			//entity.KpiDateTime = (Convert.IsDBNull(reader["kpiDateTime"]))?DateTime.MinValue:(System.DateTime?)reader["kpiDateTime"];
			entity.ErrorMsg = (reader.IsDBNull(((int)EhsimsExceptionsCompaniesSitesColumn.ErrorMsg)))?null:(System.String)reader[((int)EhsimsExceptionsCompaniesSitesColumn.ErrorMsg)];
			//entity.ErrorMsg = (Convert.IsDBNull(reader["ErrorMsg"]))?string.Empty:(System.String)reader["ErrorMsg"];
			entity.CompanyName = (System.String)reader[((int)EhsimsExceptionsCompaniesSitesColumn.CompanyName)];
			//entity.CompanyName = (Convert.IsDBNull(reader["CompanyName"]))?string.Empty:(System.String)reader["CompanyName"];
			entity.SiteName = (System.String)reader[((int)EhsimsExceptionsCompaniesSitesColumn.SiteName)];
			//entity.SiteName = (Convert.IsDBNull(reader["SiteName"]))?string.Empty:(System.String)reader["SiteName"];
			reader.Close();
	
			entity.AcceptChanges();
		}
		
		/*
		/// <summary>
		/// Refreshes the <see cref="EhsimsExceptionsCompaniesSites"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="EhsimsExceptionsCompaniesSites"/> object.</param>
		protected static void RefreshEntity(DataSet dataSet, EhsimsExceptionsCompaniesSites entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.EhsimsExceptionId = (Convert.IsDBNull(dataRow["EhsimsExceptionId"]))?(int)0:(System.Int32)dataRow["EhsimsExceptionId"];
			entity.ReportDateTime = (Convert.IsDBNull(dataRow["ReportDateTime"]))?DateTime.MinValue:(System.DateTime)dataRow["ReportDateTime"];
			entity.CompanyId = (Convert.IsDBNull(dataRow["CompanyId"]))?(int)0:(System.Int32?)dataRow["CompanyId"];
			entity.SiteId = (Convert.IsDBNull(dataRow["SiteId"]))?(int)0:(System.Int32?)dataRow["SiteId"];
			entity.KpiDateTime = (Convert.IsDBNull(dataRow["kpiDateTime"]))?DateTime.MinValue:(System.DateTime?)dataRow["kpiDateTime"];
			entity.ErrorMsg = (Convert.IsDBNull(dataRow["ErrorMsg"]))?string.Empty:(System.String)dataRow["ErrorMsg"];
			entity.CompanyName = (Convert.IsDBNull(dataRow["CompanyName"]))?string.Empty:(System.String)dataRow["CompanyName"];
			entity.SiteName = (Convert.IsDBNull(dataRow["SiteName"]))?string.Empty:(System.String)dataRow["SiteName"];
			entity.AcceptChanges();
		}
		*/
			
		#endregion Helper Functions
	}//end class

	#region EhsimsExceptionsCompaniesSitesFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EhsimsExceptionsCompaniesSites"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EhsimsExceptionsCompaniesSitesFilterBuilder : SqlFilterBuilder<EhsimsExceptionsCompaniesSitesColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EhsimsExceptionsCompaniesSitesFilterBuilder class.
		/// </summary>
		public EhsimsExceptionsCompaniesSitesFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the EhsimsExceptionsCompaniesSitesFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public EhsimsExceptionsCompaniesSitesFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the EhsimsExceptionsCompaniesSitesFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public EhsimsExceptionsCompaniesSitesFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion EhsimsExceptionsCompaniesSitesFilterBuilder

	#region EhsimsExceptionsCompaniesSitesParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EhsimsExceptionsCompaniesSites"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EhsimsExceptionsCompaniesSitesParameterBuilder : ParameterizedSqlFilterBuilder<EhsimsExceptionsCompaniesSitesColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EhsimsExceptionsCompaniesSitesParameterBuilder class.
		/// </summary>
		public EhsimsExceptionsCompaniesSitesParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the EhsimsExceptionsCompaniesSitesParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public EhsimsExceptionsCompaniesSitesParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the EhsimsExceptionsCompaniesSitesParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public EhsimsExceptionsCompaniesSitesParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion EhsimsExceptionsCompaniesSitesParameterBuilder
	
	#region EhsimsExceptionsCompaniesSitesSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EhsimsExceptionsCompaniesSites"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class EhsimsExceptionsCompaniesSitesSortBuilder : SqlSortBuilder<EhsimsExceptionsCompaniesSitesColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EhsimsExceptionsCompaniesSitesSqlSortBuilder class.
		/// </summary>
		public EhsimsExceptionsCompaniesSitesSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion EhsimsExceptionsCompaniesSitesSortBuilder

} // end namespace
