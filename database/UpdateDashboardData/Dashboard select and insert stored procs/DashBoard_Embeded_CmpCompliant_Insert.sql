USE [AuA_CSMS_Dev]
GO

/****** Object:  StoredProcedure [dbo].[DashBoard_Embeded_CmpCompliant_Insert]    Script Date: 3/08/2015 1:25:59 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[DashBoard_Embeded_CmpCompliant_Insert]          
(          
 @Site int          
 ,@Month int          
 ,@Year int          
           
 ,@NumberofCompanies varchar(200)          
 ,@ContractorServicesAudit varchar(200)         
 ,@WorkplaceSafetyCompliance varchar(200)         
 ,@ManagementHealthSafetyWorkContacts varchar(200)          
 ,@BehaviouralObservations varchar(200)        
 ,@FatalityPreventionProgram varchar(200)       
 ,@JSAFieldAuditVerifications varchar(200)          
 ,@ToolboxMeetings varchar(200)         
 ,@AlcoaWeeklyContractorsMeeting varchar(200)          
 ,@AlcoaMonthlyContractorsMeeting varchar(200)          
 ,@SafetyPlansSubmitted varchar(200)          
 ,@MandatedTraining varchar(200)          
          
)          
AS    
if exists(Select * from  DashBoard_Embeded_CmpCompliant where SiteId=@Site and [Month]=@Month  
 and [Year]=@Year)  
 begin   
 Update DashBoard_Embeded_CmpCompliant Set  
   [Number of Companies:] = @NumberofCompanies         
     ,[Contractor Services Audit] = @ContractorServicesAudit         
     ,[Workplace Safety Compliance] = @WorkplaceSafetyCompliance         
     ,[Management Health Safety Work Contacts]  = @ManagementHealthSafetyWorkContacts        
     ,[Behavioural Observations] = @BehaviouralObservations         
     ,[Fatality Prevention Program]  = @FatalityPreventionProgram        
     ,[JSA Field Audit Verifications] = @JSAFieldAuditVerifications          
     ,[Toolbox Meetings] = @ToolboxMeetings         
     ,[Alcoa Weekly Contractors Meeting] = @AlcoaWeeklyContractorsMeeting         
     ,[Alcoa Monthly Contractors Meeting] = @AlcoaMonthlyContractorsMeeting         
     ,[Safety Plan(s) Submitted] = @SafetyPlansSubmitted         
     ,[Mandated Training] = @MandatedTraining
     ,[LastModifiedDate] = GETDATE()  
    where SiteId=@Site AND [Month]=@Month AND [Year]=@Year  
   end   
else  
    begin      
    INSERT INTO [dbo].[DashBoard_Embeded_CmpCompliant]          
     (          
     [SiteId]          
     ,[Month]          
     ,[Year]          
     ,[Number of Companies:]          
     ,[Contractor Services Audit]          
     ,[Workplace Safety Compliance]          
     ,[Management Health Safety Work Contacts]          
     ,[Behavioural Observations]          
     ,[Fatality Prevention Program]          
     ,[JSA Field Audit Verifications]          
     ,[Toolbox Meetings]          
     ,[Alcoa Weekly Contractors Meeting]          
     ,[Alcoa Monthly Contractors Meeting]          
     ,[Safety Plan(s) Submitted]          
     ,[Mandated Training]          
     ,[LastModifiedDate]    
     )          
    VALUES          
     (          
     @Site          
     ,@Month          
     ,@Year          
     ,@NumberofCompanies          
     ,@ContractorServicesAudit          
     ,@WorkplaceSafetyCompliance          
     ,@ManagementHealthSafetyWorkContacts          
     ,@BehaviouralObservations          
     ,@FatalityPreventionProgram          
     ,@JSAFieldAuditVerifications          
     ,@ToolboxMeetings          
     ,@AlcoaWeeklyContractorsMeeting          
     ,@AlcoaMonthlyContractorsMeeting          
     ,@SafetyPlansSubmitted          
     ,@MandatedTraining 
     ,GETDATE()         
     )     
end       
----------------------------------------

GO


