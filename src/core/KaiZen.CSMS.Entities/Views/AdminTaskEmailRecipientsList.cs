﻿#region Using directives

using System;

#endregion

namespace KaiZen.CSMS.Entities
{	
	///<summary>
	/// An object representation of the 'AdminTaskEmailRecipientsList' view. [No description found in the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class AdminTaskEmailRecipientsList : AdminTaskEmailRecipientsListBase
	{
		#region Constructors

		///<summary>
		/// Creates a new <see cref="AdminTaskEmailRecipientsList"/> instance.
		///</summary>
		public AdminTaskEmailRecipientsList():base(){}	
		
		#endregion
	}
}
