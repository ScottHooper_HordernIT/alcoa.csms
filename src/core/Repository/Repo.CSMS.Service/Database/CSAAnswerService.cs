﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface ICSAAnswersService
    {
        CSA_Answers Get(Expression<Func<CSA_Answers, bool>> where, List<Expression<Func<CSA_Answers, object>>> includes);
        List<CSA_Answers> GetMany(int? top, Expression<Func<CSA_Answers, bool>> where, Func<IQueryable<CSA_Answers>, IOrderedQueryable<CSA_Answers>> orderBy, List<Expression<Func<CSA_Answers, object>>> includes);
        List<CSA_Answers> GetManyPaged(int pageNumber, int pageSize, Expression<Func<CSA_Answers, bool>> where, Func<IQueryable<CSA_Answers>, IOrderedQueryable<CSA_Answers>> orderBy, List<Expression<Func<CSA_Answers, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<CSA_Answers, bool>> where, List<Expression<Func<CSA_Answers, object>>> includes);
        void Insert(CSA_Answers item, bool saveChanges = true);
        void Update(CSA_Answers item, bool saveChanges = true);
        void Delete(CSA_Answers item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class CSAAnswersService : ICSAAnswersService
    {
        private readonly IRepository<CSA_Answers> _repository;

        public CSAAnswersService(IRepository<CSA_Answers> repository)
        {
            this._repository = repository;
        }

        public CSA_Answers Get(Expression<Func<CSA_Answers, bool>> where, List<Expression<Func<CSA_Answers, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<CSA_Answers> GetMany(int? top, Expression<Func<CSA_Answers, bool>> where, Func<IQueryable<CSA_Answers>, IOrderedQueryable<CSA_Answers>> orderBy, List<Expression<Func<CSA_Answers, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<CSA_Answers> GetManyPaged(int pageNumber, int pageSize, Expression<Func<CSA_Answers, bool>> where, Func<IQueryable<CSA_Answers>, IOrderedQueryable<CSA_Answers>> orderBy, List<Expression<Func<CSA_Answers, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<CSA_Answers, bool>> where, List<Expression<Func<CSA_Answers, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(CSA_Answers item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("CSA_Answers");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(CSA_Answers item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("CSA_Answers");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(CSA_Answers item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("CSA_Answers");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
