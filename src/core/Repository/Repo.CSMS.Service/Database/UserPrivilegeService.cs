﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface IUserPrivilegeService
    {
        UserPrivilege Get(Expression<Func<UserPrivilege, bool>> where, List<Expression<Func<UserPrivilege, object>>> includes);
        List<UserPrivilege> GetMany(int? top, Expression<Func<UserPrivilege, bool>> where, Func<IQueryable<UserPrivilege>, IOrderedQueryable<UserPrivilege>> orderBy, List<Expression<Func<UserPrivilege, object>>> includes);
        List<UserPrivilege> GetManyPaged(int pageNumber, int pageSize, Expression<Func<UserPrivilege, bool>> where, Func<IQueryable<UserPrivilege>, IOrderedQueryable<UserPrivilege>> orderBy, List<Expression<Func<UserPrivilege, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<UserPrivilege, bool>> where, List<Expression<Func<UserPrivilege, object>>> includes);
        void Insert(UserPrivilege item, bool saveChanges = true);
        void Update(UserPrivilege item, bool saveChanges = true);
        void Delete(UserPrivilege item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class UserPrivilegeService : IUserPrivilegeService
    {
        private readonly IRepository<UserPrivilege> _repository;

        public UserPrivilegeService(IRepository<UserPrivilege> repository)
        {
            this._repository = repository;
        }

        public UserPrivilege Get(Expression<Func<UserPrivilege, bool>> where, List<Expression<Func<UserPrivilege, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<UserPrivilege> GetMany(int? top, Expression<Func<UserPrivilege, bool>> where, Func<IQueryable<UserPrivilege>, IOrderedQueryable<UserPrivilege>> orderBy, List<Expression<Func<UserPrivilege, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<UserPrivilege> GetManyPaged(int pageNumber, int pageSize, Expression<Func<UserPrivilege, bool>> where, Func<IQueryable<UserPrivilege>, IOrderedQueryable<UserPrivilege>> orderBy, List<Expression<Func<UserPrivilege, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<UserPrivilege, bool>> where, List<Expression<Func<UserPrivilege, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(UserPrivilege item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("UserPrivilege");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(UserPrivilege item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("UserPrivilege");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(UserPrivilege item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("UserPrivilege");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
