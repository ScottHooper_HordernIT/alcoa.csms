﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.EbiMetricProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(EbiMetricDataSourceDesigner))]
	public class EbiMetricDataSource : ProviderDataSource<EbiMetric, EbiMetricKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EbiMetricDataSource class.
		/// </summary>
		public EbiMetricDataSource() : base(new EbiMetricService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the EbiMetricDataSourceView used by the EbiMetricDataSource.
		/// </summary>
		protected EbiMetricDataSourceView EbiMetricView
		{
			get { return ( View as EbiMetricDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the EbiMetricDataSource control invokes to retrieve data.
		/// </summary>
		public EbiMetricSelectMethod SelectMethod
		{
			get
			{
				EbiMetricSelectMethod selectMethod = EbiMetricSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (EbiMetricSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the EbiMetricDataSourceView class that is to be
		/// used by the EbiMetricDataSource.
		/// </summary>
		/// <returns>An instance of the EbiMetricDataSourceView class.</returns>
		protected override BaseDataSourceView<EbiMetric, EbiMetricKey> GetNewDataSourceView()
		{
			return new EbiMetricDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the EbiMetricDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class EbiMetricDataSourceView : ProviderDataSourceView<EbiMetric, EbiMetricKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EbiMetricDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the EbiMetricDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public EbiMetricDataSourceView(EbiMetricDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal EbiMetricDataSource EbiMetricOwner
		{
			get { return Owner as EbiMetricDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal EbiMetricSelectMethod SelectMethod
		{
			get { return EbiMetricOwner.SelectMethod; }
			set { EbiMetricOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal EbiMetricService EbiMetricProvider
		{
			get { return Provider as EbiMetricService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<EbiMetric> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<EbiMetric> results = null;
			EbiMetric item;
			count = 0;
			
			System.Int32 _ebiMetricId;
			System.Int32 _siteId;

			switch ( SelectMethod )
			{
				case EbiMetricSelectMethod.Get:
					EbiMetricKey entityKey  = new EbiMetricKey();
					entityKey.Load(values);
					item = EbiMetricProvider.Get(entityKey);
					results = new TList<EbiMetric>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case EbiMetricSelectMethod.GetAll:
                    results = EbiMetricProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case EbiMetricSelectMethod.GetPaged:
					results = EbiMetricProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case EbiMetricSelectMethod.Find:
					if ( FilterParameters != null )
						results = EbiMetricProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = EbiMetricProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case EbiMetricSelectMethod.GetByEbiMetricId:
					_ebiMetricId = ( values["EbiMetricId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["EbiMetricId"], typeof(System.Int32)) : (int)0;
					item = EbiMetricProvider.GetByEbiMetricId(_ebiMetricId);
					results = new TList<EbiMetric>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				// FK
				case EbiMetricSelectMethod.GetBySiteId:
					_siteId = ( values["SiteId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["SiteId"], typeof(System.Int32)) : (int)0;
					results = EbiMetricProvider.GetBySiteId(_siteId, this.StartIndex, this.PageSize, out count);
					break;
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == EbiMetricSelectMethod.Get || SelectMethod == EbiMetricSelectMethod.GetByEbiMetricId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				EbiMetric entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					EbiMetricProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<EbiMetric> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			EbiMetricProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region EbiMetricDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the EbiMetricDataSource class.
	/// </summary>
	public class EbiMetricDataSourceDesigner : ProviderDataSourceDesigner<EbiMetric, EbiMetricKey>
	{
		/// <summary>
		/// Initializes a new instance of the EbiMetricDataSourceDesigner class.
		/// </summary>
		public EbiMetricDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public EbiMetricSelectMethod SelectMethod
		{
			get { return ((EbiMetricDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new EbiMetricDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region EbiMetricDataSourceActionList

	/// <summary>
	/// Supports the EbiMetricDataSourceDesigner class.
	/// </summary>
	internal class EbiMetricDataSourceActionList : DesignerActionList
	{
		private EbiMetricDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the EbiMetricDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public EbiMetricDataSourceActionList(EbiMetricDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public EbiMetricSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion EbiMetricDataSourceActionList
	
	#endregion EbiMetricDataSourceDesigner
	
	#region EbiMetricSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the EbiMetricDataSource.SelectMethod property.
	/// </summary>
	public enum EbiMetricSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByEbiMetricId method.
		/// </summary>
		GetByEbiMetricId,
		/// <summary>
		/// Represents the GetBySiteId method.
		/// </summary>
		GetBySiteId
	}
	
	#endregion EbiMetricSelectMethod

	#region EbiMetricFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EbiMetric"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EbiMetricFilter : SqlFilter<EbiMetricColumn>
	{
	}
	
	#endregion EbiMetricFilter

	#region EbiMetricExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EbiMetric"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EbiMetricExpressionBuilder : SqlExpressionBuilder<EbiMetricColumn>
	{
	}
	
	#endregion EbiMetricExpressionBuilder	

	#region EbiMetricProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;EbiMetricChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="EbiMetric"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EbiMetricProperty : ChildEntityProperty<EbiMetricChildEntityTypes>
	{
	}
	
	#endregion EbiMetricProperty
}

