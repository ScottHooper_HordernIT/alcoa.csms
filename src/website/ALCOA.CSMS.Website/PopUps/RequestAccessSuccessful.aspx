﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/popup.master" AutoEventWireup="true" CodeBehind="RequestAccessSuccessful.aspx.cs" Inherits="ALCOA.CSMS.Website.PopUps.RequestAccessSuccessful" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    Request for access completed successfully. The Alcoa Contractor Services Team will be in contact with the new user shortly.
</asp:Content>
