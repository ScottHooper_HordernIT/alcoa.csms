﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Library_APSS_Search" Codebehind="APSS_Search.ascx.cs" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallback" TagPrefix="dxcb" %>
<table border="0" cellpadding="2" cellspacing="0" width="900" onload="ASPxbtnSearch.Focus()">
    <tr>
        <td class="pageName" colspan="7">
            <span class="title">Library Documents</span><br />
            <span class="date">Categories:</span>
                <asp:LinkButton ID="LinkButton2" runat="server" PostBackUrl="~/APSS_Top9.aspx">Must Read these...</asp:LinkButton>,
                <asp:LinkButton ID="LinkButton1" runat="server" PostBackUrl="~/APSS_MFU.aspx">Most Frequently Used</asp:LinkButton>, Search.<br />
            <img height="1" src="images/grfc_dottedline.gif" width="24" /><br /></td>
        <td class="pageName" colspan="1">
        </td>
    </tr>
    <tr>
        <td colspan="8" align="center" style="height: 8px; text-align: center; text-align:-moz-center; ">
            <br />
            <div align="center">
                <dxe:ASPxTextBox ID="ASPxtbSearch" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                    CssPostfix="Office2003Blue" Height="27px" Width="271px" 
                    NullText="eg. Manual" 
                    SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                    <ClientSideEvents
                        GotFocus="function(s) { changeColor(s); }"
                        LostFocus="function(s) { changeColor(s); }" 
                    />
                </dxe:ASPxTextBox>
            </div>
        </td>
    </tr>
    <tr>
        <td colspan="8" align="center" style="height: 8px; text-align: center; text-align:-moz-center;">
            <div align="center">
                <dxe:ASPxButton ID="ASPxbtnSearch" runat="server" 
                    CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" ClientInstanceName="ASPxbtnSearch" ClientEnabled="true"
                    CssPostfix="Office2003Blue" Text="Document Search" Width="138px" 
                    OnClick="ASPxbtnSearch_Click" 
                    SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                </dxe:ASPxButton>
            </div>
        </td>
    </tr>
    <tr>
        <td colspan="8" style="height: 8px; text-align: left">
                    <dxwgv:aspxgridview
                                    id="grid"
                                    runat="server"
                                    AutoGenerateColumns="False"
                                    CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                    CssPostfix="Office2003Blue"
                                    OnHtmlRowCreated="grid_RowCreated" Width="900px" Visible="False"
                                    >
<Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
<ExpandedButton Height="12px" Width="11px"></ExpandedButton>

    <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
    </LoadingPanelOnStatusBar>

<CollapsedButton Height="12px" Width="11px"></CollapsedButton>

<DetailCollapsedButton Height="12px" Width="11px"></DetailCollapsedButton>

<DetailExpandedButton Height="12px" Width="11px"></DetailExpandedButton>
    <LoadingPanel Url="~/App_Themes/Office2003Blue/GridView/Loading.gif">
    </LoadingPanel>
</Images>

                        <ImagesFilterControl>
                            <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
                            </LoadingPanel>
                        </ImagesFilterControl>

<Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue">
                    <Header ImageSpacing="5px" SortingImageSpacing="5px">
                    </Header>
                    <LoadingPanel ImageSpacing="10px">
                    </LoadingPanel>
                </Styles>
                
                        <Columns>
                            <dxwgv:GridViewDataTextColumn Caption="Rank" FieldName="RANK" VisibleIndex="0" Width="65px" SortIndex="0" SortOrder="Descending">
                            </dxwgv:GridViewDataTextColumn>
                            <dxwgv:GridViewDataTextColumn Caption="Document Title" FieldName="DOCTITLE" VisibleIndex="1" Width="560px">
                                <PropertiesTextEdit NullDisplayText="-">
                                </PropertiesTextEdit>
                                <Settings AutoFilterCondition="Contains" FilterMode="DisplayText" />
                            </dxwgv:GridViewDataTextColumn>
                            <dxwgv:GridViewDataTextColumn Caption="FileName" FieldName="FILENAME" VisibleIndex="2" Width="330px">
                            <DataItemTemplate>
                             <table cellpadding="0" cellspacing="0"><tr>
                             <td style="">
                                 <dxe:ASPxHyperLink ID="hlFileName" runat="server" Text="">
                                 </dxe:ASPxHyperLink>
                             </td>
                             </tr></table>
                            </DataItemTemplate>
                                <Settings AutoFilterCondition="Contains" FilterMode="DisplayText" />
                            </dxwgv:GridViewDataTextColumn>
                            <dxwgv:GridViewDataTextColumn Caption="VPath" FieldName="VPATH" Visible="False">
                            </dxwgv:GridViewDataTextColumn>
                            <dxwgv:GridViewDataTextColumn Caption="Characterization" FieldName="CHARACTERIZATION" Visible="False">
                            </dxwgv:GridViewDataTextColumn>
                        </Columns>
                        <SettingsPager>
                            <AllButton Visible="True">
                            </AllButton>
                        </SettingsPager>
                        <SettingsText EmptyDataRow="No documents containing all your search terms were found.  " />
                        <Settings ShowFilterRow="True" />
                        <SettingsBehavior ColumnResizeMode="NextColumn" />
</dxwgv:aspxgridview>
            <br />
                    <asp:Label ID="lbl" runat="server"></asp:Label></td>
    </tr>
</table>
<p>
    &nbsp;</p>
