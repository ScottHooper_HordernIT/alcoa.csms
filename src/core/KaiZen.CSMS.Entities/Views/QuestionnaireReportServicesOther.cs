﻿#region Using directives

using System;

#endregion

namespace KaiZen.CSMS.Entities
{	
	///<summary>
	/// An object representation of the 'QuestionnaireReportServicesOther' view. [No description found in the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class QuestionnaireReportServicesOther : QuestionnaireReportServicesOtherBase
	{
		#region Constructors

		///<summary>
		/// Creates a new <see cref="QuestionnaireReportServicesOther"/> instance.
		///</summary>
		public QuestionnaireReportServicesOther():base(){}	
		
		#endregion
	}
}
