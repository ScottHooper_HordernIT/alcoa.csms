﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Services;
using System.Text;
using DevExpress.Web.ASPxUploadControl;
using DevExpress.Web.ASPxClasses.Internal;
using System.IO;
using DevExpress.Web.ASPxGridView;
using KaiZen.Library;
using DevExpress.Web.Data;
using System.Data;

namespace ALCOA.CSMS.Website.UserControls.Main
{
    public partial class ContractReviewsRegister : System.Web.UI.UserControl
    {
        Auth auth = new Auth();

        public DataTable dtContract = new DataTable();
        public void Page_Load(object sender, EventArgs e) { Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); }

        private void moduleInit(bool postBack)
        {
            
            if (!IsPostBack)
            {
                SitesService siteSer = new SitesService();
                TList<Sites> sites = siteSer.GetAll();
                DataSet dt12 = sites.ToDataSet(true);

                FillYearCombo();
                Session["dtContract"] = null;

                Session["authRoleId"]=auth.RoleId;

                //FillDataIngrid();
               
                dtContract.Columns.Add("CompanySiteCategoryStandardId", typeof(Int32));
                dtContract.Columns.Add("CompanyId", typeof(Int32));
                dtContract.Columns.Add("SiteId", typeof(Int32));
                dtContract.Columns.Add("Year", typeof(Int32));

                dtContract.Columns.Add("January", typeof(Boolean));
                dtContract.Columns.Add("February", typeof(Boolean));
                dtContract.Columns.Add("March", typeof(Boolean));
                dtContract.Columns.Add("April", typeof(Boolean));
                dtContract.Columns.Add("May", typeof(Boolean));
                dtContract.Columns.Add("June", typeof(Boolean));
                dtContract.Columns.Add("July", typeof(Boolean));
                dtContract.Columns.Add("August", typeof(Boolean));
                dtContract.Columns.Add("September", typeof(Boolean));
                dtContract.Columns.Add("October", typeof(Boolean));
                dtContract.Columns.Add("November", typeof(Boolean));
                dtContract.Columns.Add("December", typeof(Boolean));

                dtContract.Columns.Add("LocationSponsorUserId", typeof(Int32));
                dtContract.Columns.Add("LocationSpaUserId", typeof(Int32));

                FillDataTable(dtContract);
                grid.DataSource = dtContract;
                grid.DataBind();
            }
            else
            {
                DataTable dt = (DataTable)Session["dtContract"];
                grid.DataSource = dt;
                grid.DataBind();
            }

        }
        protected void FillYearCombo()
        { 
            int yearToStart=2012;
            int currentYear=DateTime.Now.Year;

            while (currentYear >=yearToStart)
            {
                cmbYear.Items.Add(currentYear.ToString(),currentYear);
                currentYear--;
            }
            cmbYear.SelectedIndex = 0;
        }

        


        protected void FillDataTable(DataTable dtData)
        {
            try
            {
                int _year = Convert.ToInt32(cmbYear.SelectedItem.ToString());
                CompanySiteCategoryStandardService stdSer = new CompanySiteCategoryStandardService();

                int checkSiteID = 1;
                int CountCompanyId = 1;
                int checkCompanyId = 1;
                StringBuilder sb = new StringBuilder();

                TList<CompanySiteCategoryStandard> stdList = stdSer.GetAll();
                if (stdList != null && stdList.Count > 0)
                {
                    ContractReviewsRegisterService conser = new ContractReviewsRegisterService();
                    foreach (CompanySiteCategoryStandard std in stdList)
                    {
                        DataRow dr = dtData.NewRow();
                        if (std.CompanySiteCategoryStandardId != null)
                        {
                            dr["CompanySiteCategoryStandardId"] = Convert.ToInt32(std.CompanySiteCategoryStandardId);
                        }
                        else
                        {
                            dr["CompanySiteCategoryStandardId"] = DBNull.Value;
                        }
                        if (std.CompanyId != null)
                        {
                            dr["CompanyId"] = Convert.ToInt32(std.CompanyId);

                        }
                        else
                        {
                            dr["CompanyId"] = DBNull.Value; 
                        }
                        if (std.SiteId != null)
                        {
                            dr["SiteId"] = Convert.ToInt32(std.SiteId);
                            checkSiteID = Convert.ToInt32(std.SiteId);
                        }
                        else
                        {
                            dr["SiteId"] = DBNull.Value;
                        }
                        


                        KaiZen.CSMS.Entities.ContractReviewsRegister cont = conser.GetByCompanyIdSiteIdYear(std.CompanyId, std.SiteId, _year);
                        if (cont != null)
                        {

                            //dtData.Rows.Add(Convert.ToInt32(std.CompanySiteCategoryStandardId), Convert.ToInt32(std.CompanyId),
                            //               Convert.ToInt32(std.SiteId), Convert.ToInt32(cont.Year),
                            //               Convert.ToBoolean(cont.January), Convert.ToBoolean(cont.February), Convert.ToBoolean(cont.March),
                            //               Convert.ToBoolean(cont.April), Convert.ToBoolean(cont.May), Convert.ToBoolean(cont.June),
                            //               Convert.ToBoolean(cont.July), Convert.ToBoolean(cont.August), Convert.ToBoolean(cont.September),
                            //               Convert.ToBoolean(cont.October), Convert.ToBoolean(cont.November), Convert.ToBoolean(cont.December),
                            //               Convert.ToInt32(std.LocationSponsorUserId), Convert.ToInt32(std.LocationSpaUserId));

                            if (cont.Year != null)
                            {
                                dr["Year"] = Convert.ToInt32(cont.Year);
                            }
                            else
                            {
                                dr["Year"] = Convert.ToInt32(cmbYear.SelectedItem.ToString());
                            }
                            if (cont.January != null)
                            {
                                dr["January"] = Convert.ToBoolean(cont.January);
                            }
                            else
                            {
                                dr["January"] = false;
                            }
                            if (cont.February != null)
                            {
                                dr["February"] = Convert.ToBoolean(cont.February);
                            }
                            else
                            {
                                dr["February"] = false;
                            }
                            if (cont.March != null)
                            {
                                dr["March"] = Convert.ToBoolean(cont.March);
                            }
                            else
                            {
                                dr["March"] = false;
                            }
                            if (cont.April != null)
                            {
                                dr["April"] = Convert.ToBoolean(cont.April);
                            }
                            else
                            {
                                dr["April"] = false;
                            }
                            if (cont.May != null)
                            {
                                dr["May"] = Convert.ToBoolean(cont.May);
                            }
                            else
                            {
                                dr["May"] = false;
                            }
                            if (cont.June != null)
                            {
                                dr["June"] = Convert.ToBoolean(cont.June);
                            }
                            else
                            {
                                dr["June"] = false;
                            }
                            if (cont.July != null)
                            {
                                dr["July"] = Convert.ToBoolean(cont.July);
                            }
                            else
                            {
                                dr["July"] = false;
                            }
                            if (cont.August != null)
                            {
                                dr["August"] = Convert.ToBoolean(cont.August);
                            }
                            else
                            {
                                dr["August"] = false;
                            }
                            if (cont.September != null)
                            {
                                dr["September"] = Convert.ToBoolean(cont.September);
                            }
                            else
                            {
                                dr["September"] = false;
                            }
                            if (cont.October != null)
                            {
                                dr["October"] = Convert.ToBoolean(cont.October);
                            }
                            else
                            {
                                dr["October"] = false;
                            }
                            if (cont.November != null)
                            {
                                dr["November"] = Convert.ToBoolean(cont.November);
                            }
                            else
                            {
                                dr["November"] = false;
                            }
                            if (cont.December != null)
                            {
                                dr["December"] = Convert.ToBoolean(cont.December);
                            }
                            else
                            {
                                dr["December"] = false;
                            }
                                
                            

                        
                        }
                        else
                        {
                            //dtData.Rows.Add(Convert.ToInt32(std.CompanySiteCategoryStandardId), Convert.ToInt32(std.CompanyId),
                            //                Convert.ToInt32(std.SiteId), Convert.ToInt32(cont.Year),
                            //               false, false, false, false, false, false, false, false, false, false, false, false,
                            //                Convert.ToInt32(std.LocationSponsorUserId), Convert.ToInt32(std.LocationSpaUserId));

                           
                          dr["Year"] = Convert.ToInt32(cmbYear.SelectedItem.ToString());
                          dr["January"] = false;
                          dr["February"] = false;
                          dr["March"] = false;
                          dr["April"] = false;
                          dr["May"] = false;
                          dr["June"] = false;
                          dr["July"] = false;
                          dr["August"] = false;
                          dr["September"] = false;
                          dr["October"] = false;
                          dr["November"] = false;
                          dr["December"] = false;  
                        
                        }

                        if (std.LocationSponsorUserId != null)
                        {
                            dr["LocationSponsorUserId"] = Convert.ToInt32(std.LocationSponsorUserId);
                        }
                        else
                        {
                            dr["LocationSponsorUserId"] = DBNull.Value;
                        }
                        if (std.LocationSpaUserId != null)
                        {
                            dr["LocationSpaUserId"] = Convert.ToInt32(std.LocationSpaUserId);
                        }
                        else
                        {
                            dr["LocationSpaUserId"] = DBNull.Value;
                        }
                        dtData.Rows.Add(dr);
                    }
                    string[] columnNames = {"CompanyId"};


                    DataTable dtDistinct = dtData.DefaultView.ToTable(true, columnNames);
                    int count = dtDistinct.Rows.Count;
                    int compSiteStsId = 5000;

                    foreach(DataRow drComp in dtDistinct.Rows)
                    {
                        CompanySiteCategoryStandardService compSiteSer = new CompanySiteCategoryStandardService();
                        TList<CompanySiteCategoryStandard> compSiteList = compSiteSer.GetByCompanyId(Convert.ToInt32(drComp["CompanyId"]));

                        bool wstnAustralia = false;
                        bool victoria = false;
                        bool rolledProducts = false;

                        foreach (CompanySiteCategoryStandard compSite in compSiteList)
                        {
                            
                            RegionsSitesService rsSer=new RegionsSitesService();
                            TList<RegionsSites> rsList=rsSer.GetBySiteId(Convert.ToInt32(compSite.SiteId));
                            foreach (RegionsSites rs in rsList)
                            {
                                //For Western Australian Region
                                if (rs.RegionId == 1 && !wstnAustralia)
                                {
                                    wstnAustralia = true;
                                    
                                    DataRow drNewSites = dtData.NewRow();
                                    drNewSites = FillDataRow(drNewSites,compSite.CompanyId, rs.RegionId, compSiteStsId);
                                    dtData.Rows.Add(drNewSites);
                                    compSiteStsId++;

                                }
                                //Victoria
                                else if (rs.RegionId == 4 && !victoria)
                                {
                                    victoria = true;
                                    DataRow drNewSites = dtData.NewRow();
                                    drNewSites = FillDataRow(drNewSites, compSite.CompanyId, rs.RegionId, compSiteStsId);
                                    dtData.Rows.Add(drNewSites);
                                    compSiteStsId++;
                                }
                                //Rolled Product
                                else if (rs.RegionId == 7 && !rolledProducts)
                                {
                                    rolledProducts = true;

                                    DataRow drNewSites = dtData.NewRow();
                                    drNewSites = FillDataRow(drNewSites, compSite.CompanyId, rs.RegionId, compSiteStsId);
                                    dtData.Rows.Add(drNewSites);
                                    compSiteStsId++;
                                }
                            }
                        }

                    }

                    Session["dtContract"] = dtData;
                }
            }
            catch (Exception ex)
            {
                
                
            }
           
        }



        protected DataRow FillDataRow(DataRow drData,int companyId,int regionId, int compSiteStdId)
        {
            int _year = Convert.ToInt32(cmbYear.SelectedItem.ToString());
            int _siteId = 0;
            if (regionId == 7)
            {
                _siteId = 99;
            }
            else
            {

                

                RegionsService regSer = new RegionsService();
                Regions reg = regSer.GetByRegionId(regionId);

                SitesService SiteSer = new SitesService();
                Sites site = SiteSer.GetBySiteName(reg.RegionName);

                _siteId = site.SiteId;
            }
                CompanySiteCategoryStandardService stdSer = new CompanySiteCategoryStandardService();


                //CompanySiteCategoryStandard std = stdSer.GetByCompanyIdSiteId(companyId, site.SiteId);
                //if (std != null)
                //{
                    ContractReviewsRegisterService conser = new ContractReviewsRegisterService();
                    //foreach (CompanySiteCategoryStandard std in stdList)
                    //{

                    if (compSiteStdId != null)
                        {
                            drData["CompanySiteCategoryStandardId"] = compSiteStdId;
                        }
                        else
                        {
                            drData["CompanySiteCategoryStandardId"] = DBNull.Value;
                        }
                        if (companyId != null)
                        {
                            drData["CompanyId"] = Convert.ToInt32(companyId);

                        }
                        else
                        {
                            drData["CompanyId"] = DBNull.Value;
                        }
                        if (_siteId != null)
                        {
                            drData["SiteId"] = Convert.ToInt32(_siteId);                            
                        }
                        else
                        {
                            drData["SiteId"] = DBNull.Value;
                        }



                        KaiZen.CSMS.Entities.ContractReviewsRegister cont = conser.GetByCompanyIdSiteIdYear(companyId, _siteId, _year);
                        if (cont != null)
                        {

                            if (cont.Year != null)
                            {
                                drData["Year"] = Convert.ToInt32(cont.Year);
                            }
                            else
                            {
                                drData["Year"] = Convert.ToInt32(cmbYear.SelectedItem.ToString());
                            }
                            if (cont.January != null)
                            {
                                drData["January"] = Convert.ToBoolean(cont.January);
                            }
                            else
                            {
                                drData["January"] = false;
                            }
                            if (cont.February != null)
                            {
                                drData["February"] = Convert.ToBoolean(cont.February);
                            }
                            else
                            {
                                drData["February"] = false;
                            }
                            if (cont.March != null)
                            {
                                drData["March"] = Convert.ToBoolean(cont.March);
                            }
                            else
                            {
                                drData["March"] = false;
                            }
                            if (cont.April != null)
                            {
                                drData["April"] = Convert.ToBoolean(cont.April);
                            }
                            else
                            {
                                drData["April"] = false;
                            }
                            if (cont.May != null)
                            {
                                drData["May"] = Convert.ToBoolean(cont.May);
                            }
                            else
                            {
                                drData["May"] = false;
                            }
                            if (cont.June != null)
                            {
                                drData["June"] = Convert.ToBoolean(cont.June);
                            }
                            else
                            {
                                drData["June"] = false;
                            }
                            if (cont.July != null)
                            {
                                drData["July"] = Convert.ToBoolean(cont.July);
                            }
                            else
                            {
                                drData["July"] = false;
                            }
                            if (cont.August != null)
                            {
                                drData["August"] = Convert.ToBoolean(cont.August);
                            }
                            else
                            {
                                drData["August"] = false;
                            }
                            if (cont.September != null)
                            {
                                drData["September"] = Convert.ToBoolean(cont.September);
                            }
                            else
                            {
                                drData["September"] = false;
                            }
                            if (cont.October != null)
                            {
                                drData["October"] = Convert.ToBoolean(cont.October);
                            }
                            else
                            {
                                drData["October"] = false;
                            }
                            if (cont.November != null)
                            {
                                drData["November"] = Convert.ToBoolean(cont.November);
                            }
                            else
                            {
                                drData["November"] = false;
                            }
                            if (cont.December != null)
                            {
                                drData["December"] = Convert.ToBoolean(cont.December);
                            }
                            else
                            {
                                drData["December"] = false;
                            }




                        }
                        else
                        {
                            drData["Year"] = Convert.ToInt32(cmbYear.SelectedItem.ToString());
                            drData["January"] = false;
                            drData["February"] = false;
                            drData["March"] = false;
                            drData["April"] = false;
                            drData["May"] = false;
                            drData["June"] = false;
                            drData["July"] = false;
                            drData["August"] = false;
                            drData["September"] = false;
                            drData["October"] = false;
                            drData["November"] = false;
                            drData["December"] = false;

                        }

                        
                            drData["LocationSponsorUserId"] = DBNull.Value;
                        
                            drData["LocationSpaUserId"] = DBNull.Value;
                       

                                          
                //}

                return drData; 

        }





        protected void grid_RowCreated(object sender, ASPxGridViewTableRowEventArgs e)
        {
            
            if (e.RowType == GridViewRowType.Data)
            {

                
                int yearCount = 0;
                if ((bool)grid.GetRowValues(e.VisibleIndex, "January"))
                {
                    Image imgJan = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "imgJan") as Image;
                    imgJan.Visible = true;
                    imgJan.ImageUrl = "~/Images/greentick.gif";
                    yearCount++;
                }
                if ((bool)grid.GetRowValues(e.VisibleIndex, "February"))
                {
                    Image imgFeb = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "imgFeb") as Image;
                    imgFeb.Visible = true;
                    imgFeb.ImageUrl = "~/Images/greentick.gif";
                    yearCount++;
                }
                if ((bool)grid.GetRowValues(e.VisibleIndex, "March"))
                {
                    Image imgMarch = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "imgMar") as Image;
                    imgMarch.Visible = true;
                    imgMarch.ImageUrl = "~/Images/greentick.gif";
                    yearCount++;
                }
                if ((bool)grid.GetRowValues(e.VisibleIndex, "April"))
                {
                    Image imgApril = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "imgApr") as Image;
                    imgApril.Visible = true;
                    imgApril.ImageUrl = "~/Images/greentick.gif";
                    yearCount++;
                }

                if ((bool)grid.GetRowValues(e.VisibleIndex, "May"))
                {
                    Image imgMay = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "imgMay") as Image;
                    imgMay.Visible = true;
                    imgMay.ImageUrl = "~/Images/greentick.gif";
                    yearCount++;
                }
                if ((bool)grid.GetRowValues(e.VisibleIndex, "June"))
                {
                    Image imgJune = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "imgJun") as Image;
                    imgJune.Visible = true;
                    imgJune.ImageUrl = "~/Images/greentick.gif";
                    yearCount++;
                }
                if ((bool)grid.GetRowValues(e.VisibleIndex, "July"))
                {
                    Image imgJuly = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "imgJul") as Image;
                    imgJuly.Visible = true;
                    imgJuly.ImageUrl = "~/Images/greentick.gif";
                    yearCount++;
                }
                if ((bool)grid.GetRowValues(e.VisibleIndex, "August"))
                {
                    Image imgAugust = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "imgAug") as Image;
                    imgAugust.Visible = true;
                    imgAugust.ImageUrl = "~/Images/greentick.gif";
                    yearCount++;
                }


                if ((bool)grid.GetRowValues(e.VisibleIndex, "September"))
                {
                    Image imgSeptember = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "imgSep") as Image;
                    imgSeptember.Visible = true;
                    imgSeptember.ImageUrl = "~/Images/greentick.gif";
                    yearCount++;
                }
                if ((bool)grid.GetRowValues(e.VisibleIndex, "October"))
                {
                    Image imgOctober = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "imgOct") as Image;
                    imgOctober.Visible = true;
                    imgOctober.ImageUrl = "~/Images/greentick.gif";
                    yearCount++;
                }
                if ((bool)grid.GetRowValues(e.VisibleIndex, "November"))
                {
                    Image imgNov = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "imgNov") as Image;
                    imgNov.Visible = true;
                    imgNov.ImageUrl = "~/Images/greentick.gif";
                    yearCount++;
                }
                if ((bool)grid.GetRowValues(e.VisibleIndex, "December"))
                {
                    Image imgDec = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "imgDec") as Image;
                    imgDec.Visible = true;
                    imgDec.ImageUrl = "~/Images/greentick.gif";
                    yearCount++;
                }
                Label lblYear = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "lblYear") as Label;
                lblYear.Text = yearCount.ToString();



            }
            if (Convert.ToInt32(Session["authRoleId"]) != (int)RoleList.Administrator)
            {
                grid.Columns["Action"].Visible = false;
            }

                
        }

        protected void grid_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
        {
            TransactionManager transactionManager = null;
            try
            {
                transactionManager = ConnectionScope.ValidateOrCreateTransaction(true);

                bool _january = (Boolean)e.NewValues["January"];
                bool _february = (Boolean)e.NewValues["February"];
                bool _march = (Boolean)e.NewValues["March"];

                bool _april = (Boolean)e.NewValues["April"];
                bool _may = (Boolean)e.NewValues["May"];
                bool _june = (Boolean)e.NewValues["June"];

                bool _july = (Boolean)e.NewValues["July"];
                bool _august = (Boolean)e.NewValues["August"];
                bool _september = (Boolean)e.NewValues["September"];

                bool _october = (Boolean)e.NewValues["October"];
                bool _november = (Boolean)e.NewValues["November"];
                bool _december = (Boolean)e.NewValues["December"];

                int _siteId = (Int32)e.OldValues["SiteId"];
                int _companyId = (Int32)e.OldValues["CompanyId"];
                int _year = Convert.ToInt32(cmbYear.SelectedItem.ToString());

                int _locationSponsorUserId = Convert.ToInt32(e.OldValues["LocationSponsorUserId"]);
                int _locationSpaUserId = Convert.ToInt32(e.OldValues["LocationSpaUserId"]);
                


                ContractReviewsRegisterService conser = new ContractReviewsRegisterService();
                KaiZen.CSMS.Entities.ContractReviewsRegister cont = conser.GetByCompanyIdSiteIdYear(_companyId, _siteId, _year);
                if (cont != null)
                {
                    KaiZen.CSMS.Entities.ContractReviewsRegister contUpdate = new KaiZen.CSMS.Entities.ContractReviewsRegister();

                    contUpdate.ContractId = cont.ContractId;
                    contUpdate.CompanyId = _companyId;
                    contUpdate.SiteId = _siteId;
                    contUpdate.Year = Convert.ToInt32(cmbYear.SelectedItem.ToString());

                    contUpdate.January = _january;
                    contUpdate.February = _february;
                    contUpdate.March = _march;
                    contUpdate.April = _april;
                    contUpdate.May = _may;

                    contUpdate.June = _june;
                    contUpdate.July = _july;
                    contUpdate.August = _august;
                    contUpdate.September = _september;
                    contUpdate.October = _october;
                    contUpdate.November = _november;
                    contUpdate.December = _december;

                    if (contUpdate.EntityState != EntityState.Unchanged)
                    {
                        contUpdate.ModifiedBy = auth.UserId;
                        contUpdate.ModifiedDate = DateTime.Now;
                        //conser.Save(contNew);
                        DataRepository.ContractReviewsRegisterProvider.Update(transactionManager, contUpdate);
                        transactionManager.Commit();
                    }
                }
                else
                {
                    
                    KaiZen.CSMS.Entities.ContractReviewsRegister contNew = new KaiZen.CSMS.Entities.ContractReviewsRegister();

                    contNew.CompanyId = _companyId;
                    contNew.SiteId = _siteId;
                    contNew.Year = Convert.ToInt32(cmbYear.SelectedItem.ToString());

                    contNew.January = _january;
                    contNew.February = _february;
                    contNew.March = _march;
                    contNew.April = _april;
                    contNew.May = _may;

                    contNew.June = _june;
                    contNew.July = _july;
                    contNew.August = _august;
                    contNew.September = _september;
                    contNew.October = _october;
                    contNew.November = _november;
                    contNew.December = _december;

                    if (contNew.EntityState != EntityState.Unchanged)
                    {
                        contNew.ModifiedBy = auth.UserId;
                        contNew.ModifiedDate = DateTime.Now;
                        //conser.Save(contNew);
                        DataRepository.ContractReviewsRegisterProvider.Insert(transactionManager, contNew);
                        transactionManager.Commit();
                    }
                }


                e.Cancel = true;
                grid.CancelEdit();

                dtContract.Rows.Clear();
                dtContract.Columns.Add("CompanySiteCategoryStandardId", typeof(Int32));
                dtContract.Columns.Add("CompanyId", typeof(Int32));
                dtContract.Columns.Add("SiteId", typeof(Int32));
                dtContract.Columns.Add("Year", typeof(Int32));

                dtContract.Columns.Add("January", typeof(Boolean));
                dtContract.Columns.Add("February", typeof(Boolean));
                dtContract.Columns.Add("March", typeof(Boolean));
                dtContract.Columns.Add("April", typeof(Boolean));
                dtContract.Columns.Add("May", typeof(Boolean));
                dtContract.Columns.Add("June", typeof(Boolean));
                dtContract.Columns.Add("July", typeof(Boolean));
                dtContract.Columns.Add("August", typeof(Boolean));
                dtContract.Columns.Add("September", typeof(Boolean));
                dtContract.Columns.Add("October", typeof(Boolean));
                dtContract.Columns.Add("November", typeof(Boolean));
                dtContract.Columns.Add("December", typeof(Boolean));

                dtContract.Columns.Add("LocationSponsorUserId", typeof(Int32));
                dtContract.Columns.Add("LocationSpaUserId", typeof(Int32));

                FillDataTable(dtContract);
                Session["dtContract"] = dtContract;
                grid.DataSource = dtContract;
                grid.DataBind();
            }
            catch (Exception ex)
            { 
            
            }

        }

        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            try
            {
                if (cmbYear.SelectedItem.ToString() != "")
                {
                    dtContract.Rows.Clear();
                    dtContract.Columns.Add("CompanySiteCategoryStandardId", typeof(Int32));
                    dtContract.Columns.Add("CompanyId", typeof(Int32));
                    dtContract.Columns.Add("SiteId", typeof(Int32));
                    dtContract.Columns.Add("Year", typeof(Int32));

                    dtContract.Columns.Add("January", typeof(Boolean));
                    dtContract.Columns.Add("February", typeof(Boolean));
                    dtContract.Columns.Add("March", typeof(Boolean));
                    dtContract.Columns.Add("April", typeof(Boolean));
                    dtContract.Columns.Add("May", typeof(Boolean));
                    dtContract.Columns.Add("June", typeof(Boolean));
                    dtContract.Columns.Add("July", typeof(Boolean));
                    dtContract.Columns.Add("August", typeof(Boolean));
                    dtContract.Columns.Add("September", typeof(Boolean));
                    dtContract.Columns.Add("October", typeof(Boolean));
                    dtContract.Columns.Add("November", typeof(Boolean));
                    dtContract.Columns.Add("December", typeof(Boolean));

                    dtContract.Columns.Add("LocationSponsorUserId", typeof(Int32));
                    dtContract.Columns.Add("LocationSpaUserId", typeof(Int32));
                    FillDataTable(dtContract);
                    Session["dtContract"] = dtContract;
                    grid.DataSource = dtContract;
                    grid.DataBind();
                }
            }
            catch (Exception)
            {
                
               
            }
        }


    }
}