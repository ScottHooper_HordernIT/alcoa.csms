﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SafetyCategoryUpdateManagerNew.ascx.cs" Inherits="UserControls_Main_SafetyCategoryUpdateManagerNew" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register Src="../Other/ExportButtons.ascx" TagName="ExportButtons" TagPrefix="uc1" %>
<%@ Register assembly="DevExpress.Web.v14.1" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>


<script type="text/javascript">
    // <![CDATA[
    var textSeparator = "; ";
    var IdSeparator = ", ";
    function OnListBoxSelectionChanged(listBox, args) {
        //if (args.index == 0)
        //args.isSelected ? listBox.SelectAll() : listBox.UnselectAll();
        UpdateSelectAllItemState();
        UpdateText();
    }
    function UpdateSelectAllItemState() {
        IsAllSelected() //? checkListBox.SelectIndices([0]) : checkListBox.UnselectIndices([0]);
    }
    function IsAllSelected() {
        var selectedDataItemCount = checkListBox.GetItemCount() - (checkListBox.GetItem(0).selected ? 0 : 1);
        return checkListBox.GetSelectedItems().length == selectedDataItemCount;
    }
    function UpdateText() {
        var selectedItems = checkListBox.GetSelectedItems();
        var values = GetSelectedItemsValue(selectedItems);
        document.getElementById("ctl00_body_SafetyCategoryUpdateManager1_hdnPersonIds").value = values;
        checkComboBox.SetText(GetSelectedItemsText(selectedItems));
    }
    function SynchronizeListBoxValues(dropDown, args) {
        checkListBox.UnselectAll();
        var texts = dropDown.GetText().split(textSeparator);
        var values = GetValuesByTexts(texts);
        checkListBox.SelectValues(values);
        UpdateSelectAllItemState();
        UpdateText(); // for remove non-existing texts
    }
    function GetSelectedItemsText(items) {
        var texts = [];
        for (var i = 0; i < items.length; i++)
        //if (items[i].index != 0)
            texts.push(items[i].text);
        return texts.join(textSeparator);
    }
    function GetSelectedItemsValue(items) {
        var values = [];
        var texts = [];
        for (var i = 0; i < items.length; i++)
            texts.push(items[i].text);
        var values = GetValuesByTexts(texts);
        return values.join(IdSeparator);
    }
    function GetValuesByTexts(texts) {
        var actualValues = [];
        var item;
        for (var i = 0; i < texts.length; i++) {
            item = checkListBox.FindItemByText(texts[i]);
            if (item != null)
                actualValues.push(item.value);
        }
        return actualValues;
    }
    // ]]>
    </script>


<input type="hidden" runat="server" id="hdnPersonIds" />

<table border="0" cellpadding="2" cellspacing="0" width="100%">
    <tr>
        <td class="pageName" colspan="3" style="height: 16px">
            <span class="bodycopy"><span class="title">Performance Management > Tools</span><br />
                <span class="date">Update Company Safety Categories</span><br />
                <img height="1" src="images/grfc_dottedline.gif" width="24" /><br />
            </span>
        </td>
    </tr>
    <tr>
        <td>
            <dxwgv:ASPxGridView ID="grid" runat="server" AutoGenerateColumns="False" ClientInstanceName="grid"
                CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue"
                KeyFieldName="CompanySiteCategoryStandardId"  DataSourceID ="dsCompanySiteCategoryStandard"
                OnCellEditorInitialize="gridStandard_CellEditorInitialize" Width="900px" 
                OnRowUpdating="grid_RowUpdating"  OnRowInserting="grid_RowInserting" 
                onstartrowediting="grid_StartRowEditing">
                <Columns>
                    <dxwgv:GridViewCommandColumn Caption="Action" VisibleIndex="0" Width="105px">
                        <EditButton Visible="True">
                        </EditButton>
                        <NewButton Visible="True">
                        </NewButton>
                        <DeleteButton Visible="False">
                        </DeleteButton>
                        <ClearFilterButton Visible="True">
                        </ClearFilterButton>
                    </dxwgv:GridViewCommandColumn>
                    <dxwgv:GridViewDataTextColumn FieldName="CompanySiteCategoryStandardId" VisibleIndex="0"
                        ReadOnly="True" Visible="False">
                        <EditFormSettings Visible="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <%--<dxwgv:GridViewDataComboBoxColumn Caption="Company" FieldName="CompanyId" VisibleIndex="1"
                        SortIndex="1" SortOrder="Ascending">
                        <PropertiesComboBox DataSourceID="sqldsCompaniesList" TextField="CompanyName" ValueField="CompanyId"
                            ValueType="System.Int32" IncrementalFilteringMode="StartsWith">
                        </PropertiesComboBox>
                        <Settings SortMode="DisplayText" />
                    </dxwgv:GridViewDataComboBoxColumn>--%>
                    <%--DT2973:ARP issue--%>
                    <dxwgv:GridViewDataTextColumn FieldName="CompanyId" VisibleIndex="1"
                        ReadOnly="True" Visible="False">
                    <EditFormSettings Visible="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="Company" name="CompanyName" FieldName="CompanyName" VisibleIndex="2"
                        ReadOnly="false" Visible="True" SortIndex="1" SortOrder="Ascending">
                        <EditFormSettings Visible="True" />
                        <Settings SortMode="DisplayText" />
                        <EditItemTemplate>
                        <dxe:ASPxComboBox ID="cbComp" runat="server" DataSourceID="sqldsCompaniesList"  TextField="CompanyName" ValueField="CompanyId" ValueType="System.Int32" Value='<%# Eval("CompanyName")%>' IncrementalFilteringMode="StartsWith" Width="100%" 
                            CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue"  SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"></dxe:ASPxComboBox>
                        </EditItemTemplate>
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataComboBoxColumn Caption="Site" FieldName="SiteId" VisibleIndex="3"
                        SortIndex="2" SortOrder="Ascending">
                        <PropertiesComboBox DataSourceID="dsSitesFilter" TextField="SiteName" ValueField="SiteId"
                            ValueType="System.Int32" IncrementalFilteringMode="StartsWith">
                        </PropertiesComboBox>
                        <Settings SortMode="DisplayText" AllowHeaderFilter="False" />
                    </dxwgv:GridViewDataComboBoxColumn>
                    <dxwgv:GridViewDataComboBoxColumn Caption="Residential Category" FieldName="CompanySiteCategoryId"
                        VisibleIndex="4" SortOrder="Ascending">
                        <PropertiesComboBox DataSourceID="CompanySiteCategoryDataSource" TextField="CategoryDesc"
                            ValueField="CompanySiteCategoryId" ValueType="System.Int32" IncrementalFilteringMode="StartsWith">
                        </PropertiesComboBox>
                        <Settings SortMode="DisplayText" AllowHeaderFilter="False" />
                    </dxwgv:GridViewDataComboBoxColumn>
                    <dxwgv:GridViewDataComboBoxColumn Caption="Location Sponsor" FieldName="LocationSponsorUserId"
                        VisibleIndex="5">
                        <PropertiesComboBox IncrementalFilteringMode="StartsWith" DataSourceID="sqlUsersAlcoan2"
                            TextField="UserFullName" ValueField="UserId" ValueType="System.Int32" DropDownHeight="150px">
                        </PropertiesComboBox>
                        <Settings SortMode="DisplayText" AllowHeaderFilter="False" />
                    </dxwgv:GridViewDataComboBoxColumn>
                    <dxwgv:GridViewDataComboBoxColumn Caption="Location SPA" FieldName="LocationSpaUserId"
                        VisibleIndex="6">
                        <PropertiesComboBox IncrementalFilteringMode="StartsWith" DataSourceID="sqlUsersAlcoan2"
                            TextField="UserFullName" ValueField="UserId" ValueType="System.Int32" DropDownHeight="150px">
                        </PropertiesComboBox>
                        <Settings SortMode="DisplayText" AllowHeaderFilter="False" />
                    </dxwgv:GridViewDataComboBoxColumn>
                    <dxwgv:GridViewDataComboBoxColumn FieldName="Approved" VisibleIndex="7">
                        <PropertiesComboBox ValueType="System.Int32" NullDisplayText="Tentative">
                            <Items>
                                <dxe:ListEditItem Text="Yes" Value="1"></dxe:ListEditItem>
                                <dxe:ListEditItem Text="No" Value="0"></dxe:ListEditItem>
                                <%--<dxe:ListEditItem Text="Tentative" Value=""></dxe:ListEditItem>--%>
                            </Items>
                        </PropertiesComboBox>
                        <Settings AllowHeaderFilter="False" />
                    </dxwgv:GridViewDataComboBoxColumn>

                    <%--//Change By Sayani Sil For Item# 19--%>
                   <%-- <dxwgv:GridViewDataComboBoxColumn Caption="ARP" FieldName="ArpUserId" VisibleIndex="7">
                        <PropertiesComboBox IncrementalFilteringMode="StartsWith" DataSourceID="UsersCompaniesActiveAllDataSource"
                            TextField="CompanyUserFullNameLogon" ValueField="UserId" ValueType="System.Int32" DropDownHeight="150px">
                        </PropertiesComboBox>
                        <Settings SortMode="DisplayText" />
                    </dxwgv:GridViewDataComboBoxColumn>--%>

                    <dxwgv:GridViewDataTextColumn Caption="CRP" Name="ARP" FieldName="ARPName" VisibleIndex="8">
                        <Settings AllowHeaderFilter="False" />
                        <EditItemTemplate>
                            <dx:ASPxDropDownEdit ClientInstanceName="checkComboBox" ID="ASPxDropDownEdit1" Width="111px" runat="server" EnableAnimation="False"
                                    CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
                                     CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" Text='<%# Bind("ARPName") %>'>
                            <DropDownWindowStyle BackColor="#EDEDED" />
                            <DropDownWindowTemplate>
           
                                 <%--DT2973:ARP issue code here 300%--%>
                                 <dx:ASPxListBox Width="300px" ID="listBox" ClientInstanceName="checkListBox" SelectionMode="CheckColumn"
                                      DataSourceID="dsGetValidARPddl" TextField="Full_Name" ValueField="Person_id" 
                                       runat="server" ItemStyle-Wrap="true">

                
                                        <Border BorderStyle="None" />
                                        <BorderBottom BorderStyle="Solid" BorderWidth="1px" BorderColor="#DCDCDC" />
                
                                        <ClientSideEvents SelectedIndexChanged="OnListBoxSelectionChanged" />
                                 </dx:ASPxListBox>
                                 <table style="width: 100%" cellspacing="0" cellpadding="4">
                                    <tr>
                                        <td align="right">
                                            <dx:ASPxButton ID="ASPxButton1" AutoPostBack="False" runat="server" Text="Close"
                                            CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                     CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                                <ClientSideEvents Click="function(s, e){ checkComboBox.HideDropDown(); }" />
                                            </dx:ASPxButton>
                                        </td>
                                    </tr>
                                </table>
                                 <%--alert(document.getElementById('hdnPersonIds').value);--%>
                             </DropDownWindowTemplate>
                            <ClientSideEvents TextChanged="SynchronizeListBoxValues" DropDown="SynchronizeListBoxValues" />
                            </dx:ASPxDropDownEdit>

            
                         </EditItemTemplate>
                    </dxwgv:GridViewDataTextColumn>
                    <%--//END--%>
                    
                </Columns>
                <SettingsBehavior ColumnResizeMode="NextColumn" ConfirmDelete="True"></SettingsBehavior>
                <SettingsPager>
                    <AllButton Visible="True">
                    </AllButton>
                </SettingsPager>
                <SettingsEditing Mode="Inline" />
                <Settings ShowFilterBar="Visible" ShowFilterRow="True" ShowGroupPanel="True" ShowHeaderFilterButton="true" />
                <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                    <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
                    </LoadingPanelOnStatusBar>
                    <CollapsedButton Height="12px" Width="11px">
                    </CollapsedButton>
                    <ExpandedButton Height="12px" Width="11px">
                    </ExpandedButton>
                    <DetailCollapsedButton Height="12px" Width="11px">
                    </DetailCollapsedButton>
                    <DetailExpandedButton Height="12px" Width="11px">
                    </DetailExpandedButton>
                    <LoadingPanel Url="~/App_Themes/Office2003BlueOld/GridView/Loading.gif"><%--Enhancement_023:change by debashis for testing platform upgradation--%>
                    </LoadingPanel>
                </Images>
                <ImagesFilterControl>
                    <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
                    </LoadingPanel>
                </ImagesFilterControl>
                <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue">
                    <Header ImageSpacing="5px" SortingImageSpacing="5px">
                    </Header>
                    <LoadingPanel ImageSpacing="10px">
                    </LoadingPanel>
                </Styles>
            </dxwgv:ASPxGridView>
            <br />
            <dx:ASPxHyperLink ID="hlArpCrp" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                        CssPostfix="Office2003Blue" runat="server" Text="View Alcoa Responsible Persons (ARPs – Alcoa Employees) listing"
                        NavigateUrl="javascript:popUp('ViewArpListing.aspx?Help=Default');">
                    </dx:ASPxHyperLink>

            <%--<asp:SqlDataSource ID="sqldsEHS" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
                SelectCommand="SELECT DISTINCT(LocCode) As Id, LocCode As Name FROM Sites WHERE LocCode NOT LIKE ''">
            </asp:SqlDataSource>--%>
           <%-- //Change for Sayani Sil for Item# 19--%>
           <%-- <data:CompanySiteCategoryStandardDataSource ID="CompanySiteCategoryStandardDataSource"
                runat="server" SelectMethod="GetPaged" EnablePaging="True" EnableSorting="True">
            </data:CompanySiteCategoryStandardDataSource>--%>

           <%-- <asp:SqlDataSource ID="dsCompanySiteCategoryStandard" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
                SelectCommand="get_CompanySiteCategoryStandard" SelectCommandType="StoredProcedure">
            </asp:SqlDataSource>--%>

            <%--//END--%>

            <%--//Added By Sayani Sil For Item# 19--%>

           <%-- <asp:SqlDataSource ID="dsGetValidARPddl" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
                SelectCommand="Get_Valid_ARP_DATASOURCE" SelectCommandType="StoredProcedure">
            </asp:SqlDataSource>--%>

        <%--    //END--%>


            <%--<data:CompanySiteCategoryDataSource ID="CompanySiteCategoryDataSource" runat="server"
                SelectMethod="GetPaged" EnablePaging="True" EnableSorting="True">
            </data:CompanySiteCategoryDataSource>
            <asp:SqlDataSource ID="sqldsCompaniesList" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
                SelectCommand="_Companies_GetCompanyNameList" SelectCommandType="StoredProcedure">
            </asp:SqlDataSource>
            <data:SitesDataSource ID="dsSitesFilter" runat="server" Filter="IsVisible = True AND Editable = True"
                Sort="SiteName ASC">
            </data:SitesDataSource>--%>
            <%--<data:EntityDataSource ID="UsersAlcoanDataSource2" runat="server" ProviderName="UsersAlcoanProvider"
                EntityTypeName="KaiZen.CSMS.UsersAlcoan, KaiZen.CSMS" EntityKeyTypeName="System.Int32"
                SelectMethod="GetAll" Sort="UserFullNameLogon ASC" />--%>

<%--<asp:SqlDataSource ID="sqlUsersAlcoan2" runat="server"
     ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand=" SELECT  
     [UserId],  
     [UserFullName],  
     [UserFullNameLogon],  
     [Email]  
    FROM  
     [dbo].[UsersAlcoan] Order By  [UserFullNameLogon]" SelectCommandType="Text">
</asp:SqlDataSource>--%>

         <%--   <data:EntityDataSource ID="UsersCompaniesActiveContractorsDataSource" runat="server" ProviderName="UsersCompaniesActiveContractorsProvider"
                EntityTypeName="KaiZen.CSMS.UsersCompaniesActiveContractors, KaiZen.CSMS" EntityKeyTypeName="System.Int32"
                SelectMethod="GetAll" Sort="CompanyUserFullNameLogon ASC" />--%>
<%--
            <data:EntityDataSource ID="UsersCompaniesActiveAllDataSource" runat="server" ProviderName="UsersCompaniesActiveAllProvider"
                EntityTypeName="KaiZen.CSMS.UsersCompaniesActiveContractors, KaiZen.CSMS" EntityKeyTypeName="System.Int32"
                SelectMethod="GetAll" Sort="CompanyUserFullNameLogon ASC" />--%>


<asp:SqlDataSource ID="dsCompanySiteCategoryStandard" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="get_CompanySiteCategoryStandard_ARP" SelectCommandType="StoredProcedure">
</asp:SqlDataSource>

<asp:SqlDataSource ID="sqldsCompaniesList" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="_Companies_GetNotDeactivatedCompanyNameList" SelectCommandType="StoredProcedure">
</asp:SqlDataSource>

<asp:SqlDataSource ID="dsSitesFilter" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="getAllSite" SelectCommandType="StoredProcedure">
</asp:SqlDataSource>

<asp:SqlDataSource ID="CompanySiteCategoryDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="_CompanySiteCategory_SiteCategory" SelectCommandType="StoredProcedure">
</asp:SqlDataSource>

 <asp:SqlDataSource ID="sqlUsersAlcoan2" runat="server"
     ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand=" SELECT  
     [UserId],  
     [UserFullName],  
     [UserFullNameLogon],  
     [Email]  
    FROM  
     [dbo].[UsersAlcoan] Order By  [UserFullNameLogon]" SelectCommandType="Text">
</asp:SqlDataSource>


<asp:SqlDataSource ID="dsGetValidARPddl" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="Get_Valid_ARP_DATASOURCE" SelectCommandType="StoredProcedure">
</asp:SqlDataSource>
        </td>
    </tr>
    <tr align="right">
        <td colspan="3" style="padding-top: 6px; text-align: right; text-align: -moz-right;
            width: 100%" align="right">
            <div align="right">
                <uc1:ExportButtons ID="ucExportButtons" runat="server" />
            </div>
        </td>
    </tr>
</table>
<br />
