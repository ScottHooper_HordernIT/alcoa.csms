var sec = 0;
// change by Sayani for the requirement "NewMinor Task 02 - Safety Qualification Questionnaire - Timeout Clock"
//of the excel sheet "Project Items - TCS Estimate".
//var min = 60;
var min = 30;

function countDown() {
  sec--;
  if (sec == -01) {
    sec = 59;
    min = min - 1;
  } else {
   min = min;
  }
if (sec<=9) { sec = "0" + sec; }
  time = (min<=9 ? "0" + min : min) + "." + sec + "";
if (document.getElementById) { theTime.innerHTML = time; }
SD = window.setTimeout("countDown();", 1000);
if (min == '00' && sec == '00') { sec = "00"; window.clearTimeout(SD);}
}

function addLoadEvent(func) {
  var oldonload = window.onload;
  if (typeof window.onload != 'function') {
    window.onload = func;
  } else {
    window.onload = function() {
      if (oldonload) {
        oldonload();
      }
      func();
    }
  }
}

addLoadEvent(function() {
    if(typeof(theTime) !== 'undefined' && theTime != null)
    {
        countDown();
    }
});
