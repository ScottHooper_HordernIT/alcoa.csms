﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Text;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using KaiZen.CSMS.Web;
using KaiZen.CSMS.Services;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxTabControl;
using DevExpress.Web.Data;
using DevExpress.Web.ASPxGridView;
using DevExpress.XtraPrinting;
using System.Collections.Generic;
using System.Text.RegularExpressions;

using System.Drawing;
using DevExpress.Web.ASPxCallbackPanel;
using DevExpress.Web.ASPxClasses;
using DevExpress.Web.ASPxPopupControl;
using System.Data.SqlClient;

public partial class UserControls_ContractorManagementSafetyDashboard : System.Web.UI.UserControl
{
    TList<Sites> sitesList;
    DataTable dtYtdSummary = new DataTable();
    Auth auth = new Auth();
    int prevVal;
    protected void Page_Load(object sender, EventArgs e)
    {
        Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack);
    }

    private void moduleInit(bool postBack)
    {

        if (!postBack)
        {
            int _i = 0;
            //ViewState["IsInvokedGetValue"] = false;

            

         for (int i = 4; i < grid_Dash.Columns.Count; i++)
         {
             grid_Dash.Columns[i].Width  = new Unit(68, UnitType.Pixel);
             if (i == 11)
             {
                 GridViewBandColumn bandCol = new GridViewBandColumn();
                 bandCol = (GridViewBandColumn)grid_Dash.Columns[11];
                
                 bandCol.Columns[0].Width = new Unit(68, UnitType.Pixel);
                 bandCol.Columns[1].Width = new Unit(68, UnitType.Pixel);
                 bandCol.Columns[2].Width = new Unit(68, UnitType.Pixel);
             }
         }
         

            cbRegionSite.Items.Clear();
            SitesFilters sitesFilters = new SitesFilters();
            sitesFilters.Append(SitesColumn.IsVisible, "1");
            sitesList = DataRepository.SitesProvider.GetPaged(sitesFilters.ToString(), "SiteName ASC", 0, 100, out _i);
            Session["SiteList"] = sitesList;
            foreach (Sites s in sitesList)
            {
                if (s.SiteId != 17)
                {
                    cbRegionSite.Items.Add(s.SiteName, s.SiteId);
                }
            }
            cbRegionSite.Items.Add("----------------", 0);

            RegionsFilters regionsFilters = new RegionsFilters();
            //Commented by Pankaj Dikshit for AUS GPP
            //regionsFilters.Append(RegionsColumn.IsVisible, "1");
            TList<Regions> regionsList = DataRepository.RegionsProvider.GetPaged(regionsFilters.ToString(), "Ordinal ASC", 0, 100, out _i);
            foreach (Regions r in regionsList)
            {
                int NegRegionId = -1 * r.RegionId;

                //if (NegRegionId != -3)
                //{
                    cbRegionSite.Items.Add(r.RegionName, NegRegionId);
                //}

            }
            int indexAus = cbRegionSite.Items.IndexOfText("Australia");

            cbRegionSite.SelectedIndex = indexAus;

            int Current_year = DateTime.Now.Year;
            while (Current_year >= 2012)
            {
                ddlYear.Items.Add(Current_year.ToString(), Convert.ToInt32(Current_year));
                Current_year--;
            } //End
            ddlYear.Value = DateTime.Today.Year;
            ddlMonth.Value = DateTime.Today.Month;
            int siteid = 0;
            try
            {
                 siteid = Convert.ToInt32(cbRegionSite.SelectedItem.Value);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            Session["dsTotalGrid"] = null;
           
            Session["Dash_Year"] = Convert.ToInt32(ddlYear.SelectedItem.Value);
            Session["Dash_Month"] = Convert.ToInt32(ddlMonth.SelectedItem.Value);

            ViewState["Dash_Year"] = Convert.ToInt32(ddlYear.SelectedItem.Value);
            ViewState["Dash_Month"] = Convert.ToInt32(ddlMonth.SelectedItem.Value);

            int year = Convert.ToInt32(ViewState["Dash_Year"]);
            int month = Convert.ToInt32(ViewState["Dash_Month"]);

            DataSet dsAuditScoreTemp = DataRepository.KpiProvider.AuditScoreDashBoard(year);
            Session["dsAuditScore"] = dsAuditScoreTemp;
           
            GetValues(siteid, year, month);

            grid_Dash.SettingsBehavior.AllowDragDrop = false;
            grid_Dash.SettingsBehavior.AllowSort = false;

           
            // Export
            String exportFileName = @"ALCOA CSMS - Non Compliance Report - SQ Validation against KPI data"; //Chrome & IE is fine.
            String userAgent = Request.Headers.Get("User-Agent");
            if (
                (userAgent.Contains("Mozilla") && !userAgent.Contains("AppleWebKit") && !userAgent.Contains("MSIE")) || //Mozilla
                (userAgent.Contains("AppleWebKit") && !userAgent.Contains("Chrome"))      //Safari
                )
            {
                exportFileName = exportFileName.Replace(" ", "_");
            }
            Helper.ExportGrid.Settings(ucExportButtons1, exportFileName, "grid_Dash");
           
        }
        
        if(Session["dsTotalGrid"]!=null)
        {
            if (ViewState["SetNull"] != null)
            {
                if (Convert.ToBoolean(ViewState["SetNull"])==false)
                {
                    grid_Dash.DataSource = (DataTable)Session["dsTotalGrid"];
                    grid_Dash.DataBind();
                }
                else
                {
                    grid_Dash.DataSource = null;
                    grid_Dash.DataBind();
                }
            }
            else
            {
                grid_Dash.DataSource = (DataTable)Session["dsTotalGrid"];
                grid_Dash.DataBind();
            }
        }
        else
        {
            int siteid = 0;
            try
            {
                siteid = Convert.ToInt32(cbRegionSite.SelectedItem.Value);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            Session["dsTotalGrid"] = null;
            int year = 0;
            int month = 0;
            if (ViewState["Dash_Year"] == null)
            {
               year = Convert.ToInt32(ddlYear.SelectedItem.Value);
            }
            else
            {
                year = Convert.ToInt32(ViewState["Dash_Year"]); 
            }
            if (ViewState["Dash_Month"] == null)
            {
                month = Convert.ToInt32(ddlMonth.SelectedItem.Value);
            }
            else
            {
                month = Convert.ToInt32(ViewState["Dash_Month"]);
            }
            Session["Dash_Year"] = Convert.ToInt32(ViewState["Dash_Year"]);
            Session["Dash_Month"] = Convert.ToInt32(ViewState["Dash_Month"]);
            DataSet dsAuditScoreTemp = DataRepository.KpiProvider.AuditScoreDashBoard(year);
            Session["dsAuditScore"] = dsAuditScoreTemp;

            GetValues(siteid, year, month);
            if (ViewState["SetNull"] != null)
            {
                if (Convert.ToBoolean(ViewState["SetNull"])==false)
                {
                    grid_Dash.DataSource = (DataTable)Session["dsTotalGrid"];
                    grid_Dash.DataBind();
                }
                else
                {
                    grid_Dash.DataSource = null;
                    grid_Dash.DataBind();
                }
            }
            else
            {
                grid_Dash.DataSource = (DataTable)Session["dsTotalGrid"];
                grid_Dash.DataBind();
            }
        }
      }



    protected void ASPxButton1_Click(object sender, EventArgs e)
    {
        int siteid = 0;
        try
        {
            ViewState["Dash_Year"] = Convert.ToInt32(ddlYear.SelectedItem.Value);
            ViewState["Dash_Month"] = Convert.ToInt32(ddlMonth.SelectedItem.Value);
            siteid = Convert.ToInt32(cbRegionSite.SelectedItem.Value);
            //ucExportButtons.Visible = false;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        int year = Convert.ToInt32(ddlYear.SelectedItem.Value);
        int month = Convert.ToInt32(ddlMonth.SelectedItem.Value);
        Session["Dash_Year"] = Convert.ToInt32(ddlYear.SelectedItem.Value);
        Session["Dash_Month"] = Convert.ToInt32(ddlMonth.SelectedItem.Value);

       


        DataSet dsAuditScoreTemp = DataRepository.KpiProvider.AuditScoreDashBoard(year);
        Session["dsAuditScore"] = dsAuditScoreTemp;

        DataTable dtData = ReturnDataTable(month, year);

        if (dtData != null && dtData.Rows.Count > 0)
        {

            if (year == System.DateTime.Now.Year)
            {
                if (month <= System.DateTime.Now.Month)
                {
                    GetValues(siteid, year, month);
                    //if (siteid > 0)
                    //{
                    //    grid_Dash.Columns[1].Width = new Unit(250, UnitType.Pixel);
                    //    grid_Dash.Columns[2].Width = new Unit(390, UnitType.Pixel);
                    //}
                    //else
                    //{
                    //    grid_Dash.Columns[1].Width = new Unit(80, UnitType.Pixel);
                    //    grid_Dash.Columns[2].Width = new Unit(190, UnitType.Pixel);
                    //}
                    ViewState["SetNull"] = false;
                }
                else
                {
                    PopupWindow pcWindow = new PopupWindow("No data avalable for this month/year combination !.");
                    pcWindow.FooterText = "";
                    pcWindow.ShowOnPageLoad = true;
                    pcWindow.Modal = true;
                    ASPxPopupControl1.Windows.Add(pcWindow);
                    grid_Dash.DataSource = null;
                    grid_Dash.DataBind();
                    ViewState["SetNull"] = true;
                    ValidAsOf.Text = "Data Valid As Of: n/a";
                }
            }
            else
            {
                GetValues(siteid, year, month);
                if (siteid > 0)
                {
                    grid_Dash.Columns[1].Width = new Unit(300, UnitType.Pixel);
                    grid_Dash.Columns[2].Width = new Unit(300, UnitType.Pixel);
                }
                else
                {
                    grid_Dash.Columns[1].Width = new Unit(144, UnitType.Pixel);
                    grid_Dash.Columns[2].Width = new Unit(117, UnitType.Pixel);

                }
                ViewState["SetNull"] = false;
            }
        }
        else
        {
            PopupWindow pcWindow = new PopupWindow("No data available for this Month/Year combination !");
            pcWindow.FooterText = "";
            pcWindow.ShowOnPageLoad = true;
            pcWindow.Modal = true;
            ASPxPopupControl1.Windows.Add(pcWindow);
            grid_Dash.DataSource = null;
            grid_Dash.DataBind();
            ViewState["SetNull"] = true;
            ValidAsOf.Text = "Data Valid As Of: n/a";
        }
    }


    protected void GetValues(int siteid, int year, int month)
    {
        #region Value
        ///New///
        DataSet dsKpiComplianceN = null;
        DataSet dsSafetyQualificationN = null;
        DataSet dsEbiN = null;
        DataSet dsSMPN = null;
        DataSet dsEmbededN = null;
        DataSet dsNonEmbededN = null;
        DataSet dsCSAN = null;


        dsKpiComplianceN = DataRepository.KpiProvider.KpiComplianceDashboard(year, month);
        dsSafetyQualificationN = DataRepository.KpiProvider.SafetyQualificationDashboard(year, month);
        dsEbiN = DataRepository.KpiProvider.EbiDashboard(year, month);
        dsSMPN = DataRepository.KpiProvider.SMPDashboard(year, month);
        dsCSAN = DataRepository.KpiProvider.CSADashBoard(year, month);
        dsEmbededN = DataRepository.KpiProvider.EmbededComplianceDashboard(year, month);
        dsNonEmbededN = DataRepository.KpiProvider.NonEmbededComplianceDashboard(year, month);
        ///
        if (dsKpiComplianceN != null && dsKpiComplianceN.Tables.Count > 0)
        {
            if (dsKpiComplianceN.Tables[0].Rows.Count > 0)
            {
                string valid = "Data Valid As Of: " + Convert.ToString(dsKpiComplianceN.Tables[0].Rows[0]["LastModifiedDate"]);
                //string valid = "Data Valid As Of: " + "14/11/2012";
                ValidAsOf.Text = valid;
            }
        }
        #endregion

        #region Datatable

        DataSet dstext = null;
        DataTable dtDashboard = new DataTable();

        #region Add column in datatable


        dtYtdSummary.Clear();
        if (dtYtdSummary.Columns.Count > 0)
        {
            dtYtdSummary.Columns.Clear();
        }

        dtYtdSummary.Columns.Add("Row1", typeof(string));
        dtYtdSummary.Columns.Add("Row2", typeof(string));

        dtYtdSummary.Columns.Add("Management", typeof(string));

        dtYtdSummary.Columns.Add("FieldName", typeof(string));
        dtYtdSummary.Columns.Add("Plan", typeof(string));
        dtYtdSummary.Columns.Add("AUS GPP", typeof(string));
        dtYtdSummary.Columns.Add("WAO", typeof(string));

        dtYtdSummary.Columns.Add("Kwinana", typeof(string));
        dtYtdSummary.Columns.Add("Pinjara", typeof(string));
        dtYtdSummary.Columns.Add("Wagerup", typeof(string));
        dtYtdSummary.Columns.Add("Bunbury Port", typeof(string));

        dtYtdSummary.Columns.Add("Huntly (Mine)", typeof(string));
        dtYtdSummary.Columns.Add("Willowdale (Mine)", typeof(string));
        dtYtdSummary.Columns.Add("FML", typeof(string));
        dtYtdSummary.Columns.Add("BGN", typeof(string));

        dtYtdSummary.Columns.Add("Peel", typeof(string));
        dtYtdSummary.Columns.Add("VIC OPS", typeof(string));
        dtYtdSummary.Columns.Add("Anglesea", typeof(string));
        dtYtdSummary.Columns.Add("Point Henry", typeof(string));

        dtYtdSummary.Columns.Add("Portland", typeof(string));
        dtYtdSummary.Columns.Add("ARP", typeof(string));
        dtYtdSummary.Columns.Add("ARP Point Henry", typeof(string));
        dtYtdSummary.Columns.Add("Yennora", typeof(string));

        for (int i = 0; i < 65; i++)
        {
            DataRow drYtdSummary = dtYtdSummary.NewRow();
            dtYtdSummary.Rows.Add(drYtdSummary);
        }
        #endregion

        dstext = DataRepository.KpiProvider.Text();

        int count = 0;
        foreach (DataRow dr in dstext.Tables[0].Rows)
        {
            dtYtdSummary.Rows[count][3] = dr["FieldName"].ToString();
            count++;
        }

        #region Plan
        //Issue# 9 need to change
        DataSet dsPlan = null;
        dsPlan = DataRepository.KpiProvider.Plan(year, month);
        //int pl = 0;
        //foreach (DataRow dr in dsPlan.Tables[0].Rows)
        //{
        //    dtYtdSummary.Rows[pl][3] = dr["PlanText"].ToString();
        //    pl++;
        //}
        if (dsPlan.Tables[0].Rows.Count > 0)
        {
            for (int i = 0; i < 31; i++)
            {
                dtYtdSummary.Rows[i][4] = dsPlan.Tables[0].Rows[i]["PlanText"].ToString();

            }

            dtYtdSummary.Rows[35][4] = dsPlan.Tables[0].Rows[31]["PlanText"].ToString();

            int pl = 37;
            for (int i = 32; i < 43; i++)
            {
                if (pl != 48)
                {
                    dtYtdSummary.Rows[pl][4] = dsPlan.Tables[0].Rows[i]["PlanText"].ToString();
                }
                pl++;
                
            }
            dtYtdSummary.Rows[36][4] = "As is";
            dtYtdSummary.Rows[48][4] = "As is";
            //For Non Embeded
            #region Non Embeded 1
            //dtYtdSummary.Rows[48][3] = dsPlan.Tables[0].Rows[32]["PlanText"].ToString();
            dtYtdSummary.Rows[49][4] = dsPlan.Tables[0].Rows[32]["PlanText"].ToString();
            dtYtdSummary.Rows[50][4] = dsPlan.Tables[0].Rows[33]["PlanText"].ToString();
            dtYtdSummary.Rows[51][4] = dsPlan.Tables[0].Rows[34]["PlanText"].ToString();
            dtYtdSummary.Rows[52][4] = dsPlan.Tables[0].Rows[35]["PlanText"].ToString();
            dtYtdSummary.Rows[53][4] = dsPlan.Tables[0].Rows[37]["PlanText"].ToString();
            dtYtdSummary.Rows[54][4] = dsPlan.Tables[0].Rows[38]["PlanText"].ToString();
            dtYtdSummary.Rows[55][4] = dsPlan.Tables[0].Rows[39]["PlanText"].ToString();
            dtYtdSummary.Rows[56][4] = dsPlan.Tables[0].Rows[40]["PlanText"].ToString();
            dtYtdSummary.Rows[57][4] = dsPlan.Tables[0].Rows[42]["PlanText"].ToString();
            #endregion

            //dtYtdSummary.Rows[60][3] = dsPlan.Tables[0].Rows[44]["PlanText"].ToString();
            //dtYtdSummary.Rows[63][3] = dsPlan.Tables[0].Rows[44]["PlanText"].ToString();

            //dtYtdSummary.Rows[pl][31]
            // dtYtdSummary.Rows[32][3] = " ";
            //dtYtdSummary.Rows[33][3] = " ";
            // dtYtdSummary.Rows[pl][34]

            //for Plan
            DataRow[] drSMP = dsSMPN.Tables[0].Select("SiteId = " + -2);
            DataRow[] drCSA = dsCSAN.Tables[0].Select("SiteId = " + -2);

            if (drSMP.Length > 0)
            {
                dtYtdSummary.Rows[31][4] = drSMP[0]["HS Assessors – Required (# Companies)"];
                dtYtdSummary.Rows[32][4] = drSMP[0]["HS Assessors – Being Assessed (# Companies)"];
                dtYtdSummary.Rows[33][4] = drSMP[0]["HS Assessors – Awaiting Assignment"];
                dtYtdSummary.Rows[34][4] = drSMP[0]["HS Assessors – Approved"];
            }

            if (drCSA.Length > 0)
            {
                dtYtdSummary.Rows[58][4] = drCSA[0]["CAS Embedded –Required"];
                dtYtdSummary.Rows[59][4] = drCSA[0]["CSA Embedded – Required to date"];
                dtYtdSummary.Rows[61][4] = drCSA[0]["CSA Non-Embedded 1 – Total Required"];
                dtYtdSummary.Rows[62][4] = drCSA[0]["CSA – Non-Embedded 1 – Required to date"];

                Session["EReqToDate"] = drCSA[0]["CSA Embedded – Required to date"];
                Session["NEReqToDate"] = drCSA[0]["CSA Non-Embedded 1 – Total Required"];
            }
            
        }
            
        Session["dsPlan_S"] = dsPlan;
        #endregion

        #endregion

        
        //if (!IsPostBack)
        //{
        if (siteid == -1)
        {
            //getValueSiteRegion(20, year, month, 5, dtYtdSummary);
            getValueSiteRegion(-1, year, month, 6, dtYtdSummary, dsKpiComplianceN, dsSafetyQualificationN, dsEbiN, dsSMPN
                , dsCSAN, dsEmbededN, dsNonEmbededN);
            getValueSiteRegion(1, year, month, 7, dtYtdSummary, dsKpiComplianceN, dsSafetyQualificationN, dsEbiN, dsSMPN
                , dsCSAN, dsEmbededN, dsNonEmbededN);
            getValueSiteRegion(2, year, month, 8, dtYtdSummary, dsKpiComplianceN, dsSafetyQualificationN, dsEbiN, dsSMPN
                , dsCSAN, dsEmbededN, dsNonEmbededN);
            getValueSiteRegion(3, year, month, 9, dtYtdSummary, dsKpiComplianceN, dsSafetyQualificationN, dsEbiN, dsSMPN
                , dsCSAN, dsEmbededN, dsNonEmbededN);
            getValueSiteRegion(7, year, month, 10, dtYtdSummary, dsKpiComplianceN, dsSafetyQualificationN, dsEbiN, dsSMPN
                , dsCSAN, dsEmbededN, dsNonEmbededN);
            getValueSiteRegion(4, year, month, 11, dtYtdSummary, dsKpiComplianceN, dsSafetyQualificationN, dsEbiN, dsSMPN
                , dsCSAN, dsEmbededN, dsNonEmbededN);
            getValueSiteRegion(5, year, month, 12, dtYtdSummary, dsKpiComplianceN, dsSafetyQualificationN, dsEbiN, dsSMPN
                , dsCSAN, dsEmbededN, dsNonEmbededN);
            getValueSiteRegion(6, year, month, 13, dtYtdSummary, dsKpiComplianceN, dsSafetyQualificationN, dsEbiN, dsSMPN
                , dsCSAN, dsEmbededN, dsNonEmbededN);
            getValueSiteRegion(8, year, month, 14, dtYtdSummary, dsKpiComplianceN, dsSafetyQualificationN, dsEbiN, dsSMPN
                , dsCSAN, dsEmbededN, dsNonEmbededN);
            getValueSiteRegion(18, year, month, 15, dtYtdSummary, dsKpiComplianceN, dsSafetyQualificationN, dsEbiN, dsSMPN
                , dsCSAN, dsEmbededN, dsNonEmbededN);
        }

        if (siteid == -2)
        {
            getValueSiteRegion(-1, year, month, 6, dtYtdSummary, dsKpiComplianceN, dsSafetyQualificationN, dsEbiN, dsSMPN
                , dsCSAN, dsEmbededN, dsNonEmbededN);
            //Changed by Pankaj Dikshit for AUS GPP
            getValueSiteRegion(-8, year, month, 5, dtYtdSummary, dsKpiComplianceN, dsSafetyQualificationN, dsEbiN, dsSMPN
                , dsCSAN, dsEmbededN, dsNonEmbededN);
            getValueSiteRegion(-4, year, month, 16, dtYtdSummary, dsKpiComplianceN, dsSafetyQualificationN, dsEbiN, dsSMPN
                , dsCSAN, dsEmbededN, dsNonEmbededN);
            getValueSiteRegion(-7, year, month, 20, dtYtdSummary, dsKpiComplianceN, dsSafetyQualificationN, dsEbiN, dsSMPN
                , dsCSAN, dsEmbededN, dsNonEmbededN);

            getValueSiteRegion(13, year, month, 17, dtYtdSummary, dsKpiComplianceN, dsSafetyQualificationN, dsEbiN, dsSMPN
                , dsCSAN, dsEmbededN, dsNonEmbededN);
            getValueSiteRegion(25, year, month, 21, dtYtdSummary, dsKpiComplianceN, dsSafetyQualificationN, dsEbiN, dsSMPN
                , dsCSAN, dsEmbededN, dsNonEmbededN);
            getValueSiteRegion(8, year, month, 14, dtYtdSummary, dsKpiComplianceN, dsSafetyQualificationN, dsEbiN, dsSMPN
                , dsCSAN, dsEmbededN, dsNonEmbededN);
            getValueSiteRegion(7, year, month, 10, dtYtdSummary, dsKpiComplianceN, dsSafetyQualificationN, dsEbiN, dsSMPN
                , dsCSAN, dsEmbededN, dsNonEmbededN);

            getValueSiteRegion(6, year, month, 13, dtYtdSummary, dsKpiComplianceN, dsSafetyQualificationN, dsEbiN, dsSMPN
                , dsCSAN, dsEmbededN, dsNonEmbededN);
            getValueSiteRegion(4, year, month, 11, dtYtdSummary, dsKpiComplianceN, dsSafetyQualificationN, dsEbiN, dsSMPN
                , dsCSAN, dsEmbededN, dsNonEmbededN);
            getValueSiteRegion(1, year, month, 7, dtYtdSummary, dsKpiComplianceN, dsSafetyQualificationN, dsEbiN, dsSMPN
                , dsCSAN, dsEmbededN, dsNonEmbededN);
            getValueSiteRegion(18, year, month, 15, dtYtdSummary, dsKpiComplianceN, dsSafetyQualificationN, dsEbiN, dsSMPN
                , dsCSAN, dsEmbededN, dsNonEmbededN);

            getValueSiteRegion(2, year, month, 8, dtYtdSummary, dsKpiComplianceN, dsSafetyQualificationN, dsEbiN, dsSMPN
                , dsCSAN, dsEmbededN, dsNonEmbededN);
            getValueSiteRegion(14, year, month, 18, dtYtdSummary, dsKpiComplianceN, dsSafetyQualificationN, dsEbiN, dsSMPN
                , dsCSAN, dsEmbededN, dsNonEmbededN);
            getValueSiteRegion(15, year, month, 19, dtYtdSummary, dsKpiComplianceN, dsSafetyQualificationN, dsEbiN, dsSMPN
                , dsCSAN, dsEmbededN, dsNonEmbededN);
            getValueSiteRegion(3, year, month, 9, dtYtdSummary, dsKpiComplianceN, dsSafetyQualificationN, dsEbiN, dsSMPN
                , dsCSAN, dsEmbededN, dsNonEmbededN);

            getValueSiteRegion(5, year, month, 12, dtYtdSummary, dsKpiComplianceN, dsSafetyQualificationN, dsEbiN, dsSMPN
                , dsCSAN, dsEmbededN, dsNonEmbededN);
            getValueSiteRegion(24, year, month, 22, dtYtdSummary, dsKpiComplianceN, dsSafetyQualificationN, dsEbiN, dsSMPN
                , dsCSAN, dsEmbededN, dsNonEmbededN);
        }

        if (siteid == -3)
        {
            getValueSiteRegion(4, year, month, 11, dtYtdSummary, dsKpiComplianceN, dsSafetyQualificationN, dsEbiN, dsSMPN
                , dsCSAN, dsEmbededN, dsNonEmbededN);
            getValueSiteRegion(5, year, month, 12, dtYtdSummary, dsKpiComplianceN, dsSafetyQualificationN, dsEbiN, dsSMPN
                , dsCSAN, dsEmbededN, dsNonEmbededN);
            getValueSiteRegion(6, year, month, 13, dtYtdSummary, dsKpiComplianceN, dsSafetyQualificationN, dsEbiN, dsSMPN
                , dsCSAN, dsEmbededN, dsNonEmbededN);
           
        }

        if (siteid == -4)
        {
            //getValueSiteRegion(22, year, month, 15, dtYtdSummary);
            getValueSiteRegion(-4, year, month, 16, dtYtdSummary, dsKpiComplianceN, dsSafetyQualificationN, dsEbiN, dsSMPN
                , dsCSAN, dsEmbededN, dsNonEmbededN);
            getValueSiteRegion(13, year, month, 17, dtYtdSummary, dsKpiComplianceN, dsSafetyQualificationN, dsEbiN, dsSMPN
                , dsCSAN, dsEmbededN, dsNonEmbededN);
            getValueSiteRegion(14, year, month, 18, dtYtdSummary, dsKpiComplianceN, dsSafetyQualificationN, dsEbiN, dsSMPN
                , dsCSAN, dsEmbededN, dsNonEmbededN);
            getValueSiteRegion(15, year, month, 19, dtYtdSummary, dsKpiComplianceN, dsSafetyQualificationN, dsEbiN, dsSMPN
                , dsCSAN, dsEmbededN, dsNonEmbededN);

        }
        if (siteid == -7)
        {
           // getValueSiteRegion(27, year, month, 19, dtYtdSummary);
            getValueSiteRegion(-7, year, month, 20, dtYtdSummary, dsKpiComplianceN, dsSafetyQualificationN, dsEbiN, dsSMPN
                , dsCSAN, dsEmbededN, dsNonEmbededN);
            getValueSiteRegion(25, year, month, 21, dtYtdSummary, dsKpiComplianceN, dsSafetyQualificationN, dsEbiN, dsSMPN
                , dsCSAN, dsEmbededN, dsNonEmbededN);
            getValueSiteRegion(24, year, month, 22, dtYtdSummary, dsKpiComplianceN, dsSafetyQualificationN, dsEbiN, dsSMPN
                , dsCSAN, dsEmbededN, dsNonEmbededN);

        }

        //Below if-section added by Pankaj for AUS GPP
        if (siteid == -8)
        {
            getValueSiteRegion(-1, year, month, 6, dtYtdSummary, dsKpiComplianceN, dsSafetyQualificationN, dsEbiN, dsSMPN
                , dsCSAN, dsEmbededN, dsNonEmbededN);
            getValueSiteRegion(-8, year, month, 5, dtYtdSummary, dsKpiComplianceN, dsSafetyQualificationN, dsEbiN, dsSMPN
                , dsCSAN, dsEmbededN, dsNonEmbededN); 
            getValueSiteRegion(-4, year, month, 16, dtYtdSummary, dsKpiComplianceN, dsSafetyQualificationN, dsEbiN, dsSMPN
                , dsCSAN, dsEmbededN, dsNonEmbededN);

            getValueSiteRegion(13, year, month, 17, dtYtdSummary, dsKpiComplianceN, dsSafetyQualificationN, dsEbiN, dsSMPN
                , dsCSAN, dsEmbededN, dsNonEmbededN);
            getValueSiteRegion(8, year, month, 14, dtYtdSummary, dsKpiComplianceN, dsSafetyQualificationN, dsEbiN, dsSMPN
                , dsCSAN, dsEmbededN, dsNonEmbededN);
            getValueSiteRegion(7, year, month, 10, dtYtdSummary, dsKpiComplianceN, dsSafetyQualificationN, dsEbiN, dsSMPN
                , dsCSAN, dsEmbededN, dsNonEmbededN);

            getValueSiteRegion(6, year, month, 13, dtYtdSummary, dsKpiComplianceN, dsSafetyQualificationN, dsEbiN, dsSMPN
                , dsCSAN, dsEmbededN, dsNonEmbededN);
            getValueSiteRegion(4, year, month, 11, dtYtdSummary, dsKpiComplianceN, dsSafetyQualificationN, dsEbiN, dsSMPN
                , dsCSAN, dsEmbededN, dsNonEmbededN);
            getValueSiteRegion(1, year, month, 7, dtYtdSummary, dsKpiComplianceN, dsSafetyQualificationN, dsEbiN, dsSMPN
                , dsCSAN, dsEmbededN, dsNonEmbededN);
            getValueSiteRegion(18, year, month, 15, dtYtdSummary, dsKpiComplianceN, dsSafetyQualificationN, dsEbiN, dsSMPN
                , dsCSAN, dsEmbededN, dsNonEmbededN);

            getValueSiteRegion(2, year, month, 8, dtYtdSummary, dsKpiComplianceN, dsSafetyQualificationN, dsEbiN, dsSMPN
                , dsCSAN, dsEmbededN, dsNonEmbededN);
            getValueSiteRegion(14, year, month, 18, dtYtdSummary, dsKpiComplianceN, dsSafetyQualificationN, dsEbiN, dsSMPN
                , dsCSAN, dsEmbededN, dsNonEmbededN);
            getValueSiteRegion(15, year, month, 19, dtYtdSummary, dsKpiComplianceN, dsSafetyQualificationN, dsEbiN, dsSMPN
                , dsCSAN, dsEmbededN, dsNonEmbededN);
            getValueSiteRegion(3, year, month, 9, dtYtdSummary, dsKpiComplianceN, dsSafetyQualificationN, dsEbiN, dsSMPN
                , dsCSAN, dsEmbededN, dsNonEmbededN);

            getValueSiteRegion(5, year, month, 12, dtYtdSummary, dsKpiComplianceN, dsSafetyQualificationN, dsEbiN, dsSMPN
                , dsCSAN, dsEmbededN, dsNonEmbededN);

        }
        if (siteid == 13)
        {
            getValueSiteRegion(13, year, month, 17, dtYtdSummary, dsKpiComplianceN, dsSafetyQualificationN, dsEbiN, dsSMPN
                , dsCSAN, dsEmbededN, dsNonEmbededN);

        }
        if (siteid == 25)
        {
            getValueSiteRegion(25, year, month, 21, dtYtdSummary, dsKpiComplianceN, dsSafetyQualificationN, dsEbiN, dsSMPN
                , dsCSAN, dsEmbededN, dsNonEmbededN);
        }
        if (siteid == 8)
        {
            getValueSiteRegion(8, year, month, 14, dtYtdSummary, dsKpiComplianceN, dsSafetyQualificationN, dsEbiN, dsSMPN
                , dsCSAN, dsEmbededN, dsNonEmbededN);
        }
        if (siteid == 7)
        {
            getValueSiteRegion(7, year, month, 10, dtYtdSummary, dsKpiComplianceN, dsSafetyQualificationN, dsEbiN, dsSMPN
                , dsCSAN, dsEmbededN, dsNonEmbededN);
        }
        if (siteid == 6)
        {
            getValueSiteRegion(6, year, month, 13, dtYtdSummary, dsKpiComplianceN, dsSafetyQualificationN, dsEbiN, dsSMPN
                , dsCSAN, dsEmbededN, dsNonEmbededN);
        }
        if (siteid == 4)
        {
            getValueSiteRegion(4, year, month, 11, dtYtdSummary, dsKpiComplianceN, dsSafetyQualificationN, dsEbiN, dsSMPN
                , dsCSAN, dsEmbededN, dsNonEmbededN);
        }
        if (siteid == 1)
        {
            getValueSiteRegion(1, year, month, 7, dtYtdSummary, dsKpiComplianceN, dsSafetyQualificationN, dsEbiN, dsSMPN
                , dsCSAN, dsEmbededN, dsNonEmbededN);
        }
        if (siteid == 18)
        {
            getValueSiteRegion(18, year, month, 15, dtYtdSummary, dsKpiComplianceN, dsSafetyQualificationN, dsEbiN, dsSMPN
                , dsCSAN, dsEmbededN, dsNonEmbededN);
        }
        if (siteid == 2)
        {
            getValueSiteRegion(2, year, month, 8, dtYtdSummary, dsKpiComplianceN, dsSafetyQualificationN, dsEbiN, dsSMPN
                , dsCSAN, dsEmbededN, dsNonEmbededN);
        }
        if (siteid == 14)
        {
            getValueSiteRegion(14, year, month, 18, dtYtdSummary, dsKpiComplianceN, dsSafetyQualificationN, dsEbiN, dsSMPN
                , dsCSAN, dsEmbededN, dsNonEmbededN);
        }
        if (siteid == 15)
        {
            getValueSiteRegion(15, year, month, 19, dtYtdSummary, dsKpiComplianceN, dsSafetyQualificationN, dsEbiN, dsSMPN
                , dsCSAN, dsEmbededN, dsNonEmbededN);
        }
        if (siteid == 3)
        {
            getValueSiteRegion(3, year, month, 9, dtYtdSummary, dsKpiComplianceN, dsSafetyQualificationN, dsEbiN, dsSMPN
                , dsCSAN, dsEmbededN, dsNonEmbededN);
        }
        if (siteid == 5)
        {
            getValueSiteRegion(5, year, month, 12, dtYtdSummary, dsKpiComplianceN, dsSafetyQualificationN, dsEbiN, dsSMPN
                , dsCSAN, dsEmbededN, dsNonEmbededN);
        }
        if (siteid == 24)
        {
            getValueSiteRegion(24, year, month, 22, dtYtdSummary, dsKpiComplianceN, dsSafetyQualificationN, dsEbiN, dsSMPN
                , dsCSAN, dsEmbededN, dsNonEmbededN);
        }

        //getValueSiteRegion(20, year, month, 5, dtYtdSummary);
        //getValueSiteRegion(26, year, month, 4, dtYtdSummary);
        //getValueSiteRegion(22, year, month, 15, dtYtdSummary);
        //getValueSiteRegion(27, year, month, 19, dtYtdSummary);

        //getValueSiteRegion(13, year, month, 16, dtYtdSummary);
        //getValueSiteRegion(25, year, month, 20, dtYtdSummary);
        //getValueSiteRegion(8, year, month, 13, dtYtdSummary);
        //getValueSiteRegion(7, year, month, 9, dtYtdSummary);

        //getValueSiteRegion(6, year, month, 12, dtYtdSummary);
        //getValueSiteRegion(4, year, month, 10, dtYtdSummary);
        //getValueSiteRegion(1, year, month, 6, dtYtdSummary);
        //getValueSiteRegion(18, year, month, 14, dtYtdSummary);

        //getValueSiteRegion(2, year, month, 7, dtYtdSummary);
        //getValueSiteRegion(14, year, month, 17, dtYtdSummary);
        //getValueSiteRegion(15, year, month, 18, dtYtdSummary);
        //getValueSiteRegion(3, year, month, 8, dtYtdSummary);

        //getValueSiteRegion(5, year, month, 11, dtYtdSummary);
        //getValueSiteRegion(24, year, month, 21, dtYtdSummary);

        Session["dtYtdSummary_S"] = dtYtdSummary;
        //}


        #region Visibility col
        for (int i = 5; i < 21; i++)
        {
            if (i == 11)
            {
                GridViewBandColumn db = new GridViewBandColumn();
                db = (GridViewBandColumn)grid_Dash.Columns[i];
                for (int j = 0; j < 3; j++)
                    db.Columns[j].Visible = false;
                grid_Dash.Columns[i].Visible = false;
            }
            else
            {
                grid_Dash.Columns[i].Visible = false;
            }
        }

        if (siteid == -1)
        {
            for (int i = 6; i < 14; i++)
            {
                grid_Dash.Columns[i].Visible = true;
            }
            GridViewBandColumn bandCol = new GridViewBandColumn();
            bandCol = (GridViewBandColumn)grid_Dash.Columns[11];
            //grid_Dash.Columns[10].Visible = true;
            bandCol.Columns[0].Visible = true;
            bandCol.Columns[1].Visible = true;
            bandCol.Columns[2].Visible = true;
        }

        if (siteid == -2)
        {
            for (int i = 5; i < 21; i++)
            {
                grid_Dash.Columns[i].Visible = true;
            }
            GridViewBandColumn bandCol = new GridViewBandColumn();
            bandCol = (GridViewBandColumn)grid_Dash.Columns[11];
            //grid_Dash.Columns[10].Visible = true;
            bandCol.Columns[0].Visible = true;
            bandCol.Columns[1].Visible = true;
            bandCol.Columns[2].Visible = true;
        }

        if (siteid == -3)
        {
            for (int i =11; i < 12; i++)
            {
                grid_Dash.Columns[i].Visible = true;
            }
            GridViewBandColumn bandCol = new GridViewBandColumn();
            bandCol = (GridViewBandColumn)grid_Dash.Columns[11];
            //grid_Dash.Columns[10].Visible = true;
            bandCol.Columns[0].Visible = true;
            bandCol.Columns[1].Visible = true;
            bandCol.Columns[2].Visible = true;
        }

        if (siteid == -4)
        {
            for (int i = 14; i < 18; i++)
            {
                grid_Dash.Columns[i].Visible = true;
            }
        }
        if (siteid == -7)
        {
            for (int i = 18; i < 21; i++)
            {
                grid_Dash.Columns[i].Visible = true;
            }
        }
        //Below if-section added by Pankaj Dikshit for AUS GPP
        if (siteid == -8)
        {
            for (int i = 5; i < 18; i++)
            {
                grid_Dash.Columns[i].Visible = true;
            }
            GridViewBandColumn bandCol = new GridViewBandColumn();
            bandCol = (GridViewBandColumn)grid_Dash.Columns[11];
            //grid_Dash.Columns[10].Visible = true;
            bandCol.Columns[0].Visible = true;
            bandCol.Columns[1].Visible = true;
            bandCol.Columns[2].Visible = true;
        }
        if (siteid == 13)
        {
            grid_Dash.Columns[15].Visible = true;

        }
        if (siteid == 25)
        {
            grid_Dash.Columns[19].Visible = true;
        }
        if (siteid == 8)
        {
            grid_Dash.Columns[12].Visible = true;
        }
        if (siteid == 7)
        {
            grid_Dash.Columns[10].Visible = true;
        }
        if (siteid == 6)
        {
            //grid_Dash.Columns[12].Visible = true;
            GridViewBandColumn bandCol = new GridViewBandColumn();
            bandCol = (GridViewBandColumn)grid_Dash.Columns[11];
            grid_Dash.Columns[11].Visible = true;
            bandCol.Columns[0].Visible = false;
            bandCol.Columns[1].Visible = false;
            bandCol.Columns[2].Visible = true;
        }
        if (siteid == 4)
        {
            //grid_Dash.Columns[10].Visible = true;
            GridViewBandColumn bandCol = new GridViewBandColumn();
            bandCol = (GridViewBandColumn)grid_Dash.Columns[11];
            grid_Dash.Columns[11].Visible = true;
            bandCol.Columns[0].Visible = true;
            bandCol.Columns[1].Visible = false;
            bandCol.Columns[2].Visible = false;
        }
        if (siteid == 1)
        {
            grid_Dash.Columns[7].Visible = true;
        }
        if (siteid == 18)
        {
            grid_Dash.Columns[13].Visible = true;
        }
        if (siteid == 2)
        {
            grid_Dash.Columns[8].Visible = true;
        }
        if (siteid == 14)
        {
            grid_Dash.Columns[16].Visible = true;
        }
        if (siteid == 15)
        {
            grid_Dash.Columns[17].Visible = true;
        }
        if (siteid == 3)
        {
            grid_Dash.Columns[9].Visible = true;
        }
        if (siteid == 5)
        {
            //grid_Dash.Columns[11].Visible = true;
            GridViewBandColumn bandCol = new GridViewBandColumn();
            bandCol = (GridViewBandColumn)grid_Dash.Columns[11];
            grid_Dash.Columns[11].Visible = true;
            bandCol.Columns[0].Visible = false;
            bandCol.Columns[1].Visible = true;
            bandCol.Columns[2].Visible = false;
        }
        if (siteid == 24)
        {
            grid_Dash.Columns[20].Visible = true;
        }

        #endregion
        DataTable temp = (DataTable)Session["dtYtdSummary_S"];
        grid_Dash.DataSource = temp;
        Session["dsTotalGrid"] = temp;
        grid_Dash.DataBind();

    }

    public void getValueSiteRegion(int Nsiteid, int year, int month, int col, DataTable dtYtdSummary
        , DataSet dsKpiComplianceN, DataSet dsSafetyQualificationN, DataSet dsEbiN
        , DataSet dsSMPN, DataSet dsCSAN, DataSet dsEmbededN, DataSet dsNonEmbededN)
    {
        DataSet dsAuditScore = null;

        DataRow[] drKpiCompliance = null;
        DataRow[] drSafetyQualification = null;
        DataRow[] drEbi = null;
        DataRow[] drSMP = null;
        DataRow[] drEmbeded = null;
        DataRow[] drNonEmbeded = null;
        DataRow[] drCSA = null;
        

        drKpiCompliance = dsKpiComplianceN.Tables[0].Select("SiteId = " + Nsiteid);
        drSafetyQualification = dsSafetyQualificationN.Tables[0].Select("SiteId = " + Nsiteid);
        drEbi = dsEbiN.Tables[0].Select("SiteId = " + Nsiteid);
        drSMP = dsSMPN.Tables[0].Select("SiteId = " + Nsiteid);
        drEmbeded = dsEmbededN.Tables[0].Select("SiteId = " + Nsiteid);
        drNonEmbeded = dsNonEmbededN.Tables[0].Select("SiteId = " + Nsiteid);
        drCSA = dsCSAN.Tables[0].Select("SiteId = " + Nsiteid);

        //dsKpiCompliance = DataRepository.KpiProvider.KpiComplianceDashboard(Nsiteid, year, month);
        //dsSafetyQualification = DataRepository.KpiProvider.SafetyQualificationDashboard(Nsiteid, year, month);
        //dsEbi = DataRepository.KpiProvider.EbiDashboard(Nsiteid, year, month);
        //dsSMP = DataRepository.KpiProvider.SMPDashboard(Nsiteid, year, month);
       
        //dsCSA = DataRepository.KpiProvider.CSADashBoard(Nsiteid, year, month);
        //dsEmbeded = DataRepository.KpiProvider.EmbededComplianceDashboard(Nsiteid, year, month);
        //dsNonEmbeded = DataRepository.KpiProvider.NonEmbededComplianceDashboard(Nsiteid, year, month);

        //switch(Nsiteid)
        //{
        //    case -1:
        //        dsEmbeded = DataRepository.KpiProvider.EmbededComplianceDashboard(20, year, month);
        //        dsNonEmbeded = DataRepository.KpiProvider.NonEmbededComplianceDashboard(20, year, month);
        //        break;
        //    case -2:
        //        dsEmbeded = DataRepository.KpiProvider.EmbededComplianceDashboard(26, year, month);
        //        dsNonEmbeded = DataRepository.KpiProvider.NonEmbededComplianceDashboard(26, year, month);
        //        break;
        //    case -4:
        //        dsEmbeded = DataRepository.KpiProvider.EmbededComplianceDashboard(22, year, month);
        //        dsNonEmbeded = DataRepository.KpiProvider.NonEmbededComplianceDashboard(22, year, month);
        //        break;
        //    case -7:
        //        dsEmbeded = DataRepository.KpiProvider.EmbededComplianceDashboard(27, year, month);
        //        dsNonEmbeded = DataRepository.KpiProvider.NonEmbededComplianceDashboard(27, year, month);
        //        break;
        //    default:
        //        dsEmbeded = DataRepository.KpiProvider.EmbededComplianceDashboard(Nsiteid, year, month);
        //        dsNonEmbeded = DataRepository.KpiProvider.NonEmbededComplianceDashboard(Nsiteid, year, month);
        //        break;
        //}





        //dsAuditScore = DataRepository.KpiProvider.AuditScoreDashBoard( year);
        //Added By Bishwajit to Manage Session
        if (Session["dsAuditScore"] == null)
        {
            DataSet dsAuditScoreTemp = DataRepository.KpiProvider.AuditScoreDashBoard(year);
            Session["dsAuditScore"] = dsAuditScoreTemp;
        }
        //End Added By Bishwajit to Manage Session

       
        dsAuditScore = (DataSet)Session["dsAuditScore"];
        //for (int i=0; i < 35; i++)
        //    dtYtdSummary.Rows[i][0] = "SAFETY Lagging Indicators - Compliance";

        dtYtdSummary.Rows[0][0] = "SAFETY Lagging Indicators - Compliance";
        dtYtdSummary.Rows[36][0] = "SAFETY Leading Indicators - Compliance";
        dtYtdSummary.Rows[0][1] = "Rates (YTD)";
        dtYtdSummary.Rows[4][1] = "Counts (YTD)";
        dtYtdSummary.Rows[12][1] = "Safety Qualification process";
        dtYtdSummary.Rows[31][1] = "Safety Management Plans ( E / NE1 )";
        dtYtdSummary.Rows[36][1] = "Embedded compliance (only) % of companies 100% compliant";
        dtYtdSummary.Rows[48][1] = "Non-Embedded 1 compliance (only) % of companies 100% compliant";
        dtYtdSummary.Rows[58][1] = "Alcoa Contractor Services Audits";
        dtYtdSummary.Rows[64][1] = "S8.12 Audit";

        if (drKpiCompliance != null && drKpiCompliance.Length > 0)
        {
            dtYtdSummary.Rows[0][col] = drKpiCompliance[0]["LWDFR"];
            dtYtdSummary.Rows[1][col] = drKpiCompliance[0]["TRIFR"];
            dtYtdSummary.Rows[2][col] = drKpiCompliance[0]["AIFR"];
            dtYtdSummary.Rows[3][col] = drKpiCompliance[0]["DART"];

            dtYtdSummary.Rows[4][col] = Convert.ToInt32(drKpiCompliance[0]["LWD"]);
            dtYtdSummary.Rows[5][col] = Convert.ToInt32(drKpiCompliance[0]["RST"]);
            dtYtdSummary.Rows[6][col] = Convert.ToInt32(drKpiCompliance[0]["MT"]);
            dtYtdSummary.Rows[7][col] = Convert.ToInt32(drKpiCompliance[0]["FA"]);

            dtYtdSummary.Rows[8][col] = Convert.ToInt32(drKpiCompliance[0]["IFE"]);
            dtYtdSummary.Rows[9][col] = Convert.ToInt32(drKpiCompliance[0]["RN"]);
            dtYtdSummary.Rows[10][col] = drKpiCompliance[0]["IFE Injury Ratio"];
            dtYtdSummary.Rows[11][col] = drKpiCompliance[0]["RN Injury Ratio"];
        }

        /////////
        if (drSafetyQualification != null && drSafetyQualification.Length>0)
        {
            dtYtdSummary.Rows[12][col] = Convert.ToInt32(drSafetyQualification[0]["Expired - Currently With Procurement"]);
            dtYtdSummary.Rows[13][col] = Convert.ToInt32(drSafetyQualification[0]["Expired - Currently With Supplier"]);
            dtYtdSummary.Rows[14][col] = Convert.ToInt32(drSafetyQualification[0]["Expiring > 7 days"]);
            dtYtdSummary.Rows[15][col] = Convert.ToInt32(drSafetyQualification[0]["Questionnaire  > 7 days"]);

            dtYtdSummary.Rows[16][col] = Convert.ToInt32(drSafetyQualification[0]["Assign Company Status > 7 days"]);
            dtYtdSummary.Rows[17][col] = Convert.ToInt32(drSafetyQualification[0]["Questionnaire > 28 Days"]);
            dtYtdSummary.Rows[18][col] = Convert.ToInt32(drSafetyQualification[0]["Questionnaire - Resubmitting"]);
            dtYtdSummary.Rows[19][col] = Convert.ToInt32(drSafetyQualification[0]["Access > 7 Days"]);

            dtYtdSummary.Rows[20][col] = Convert.ToInt32(drSafetyQualification[0]["Expired - Currently With H&S Assessor"]);
            dtYtdSummary.Rows[21][col] = Convert.ToInt32(drSafetyQualification[0]["Assessing Procurement Questionnaire > 7 days"]);
            dtYtdSummary.Rows[22][col] = Convert.ToInt32(drSafetyQualification[0]["Assessing Questionnaire > 7 days"]);
            dtYtdSummary.Rows[23][col] = Convert.ToInt32(drSafetyQualification[0]["SQ process total"]);
        }
        
        if (!(Nsiteid == 7 || Nsiteid == 5 || Nsiteid == 6 || Nsiteid == 13 || Nsiteid == 25 || Nsiteid == 24))
        {
            if (drEbi != null && drEbi.Length>0)
            {
                dtYtdSummary.Rows[24][col] = Convert.ToInt32(drEbi[0]["Yes - SQ Current"]);
                dtYtdSummary.Rows[25][col] = Convert.ToInt32(drEbi[0]["No - Current SQ not found"]);
                dtYtdSummary.Rows[26][col] = Convert.ToInt32(drEbi[0]["No - SQ Expired"]);
                dtYtdSummary.Rows[27][col] = Convert.ToInt32(drEbi[0]["No - Access to Site Not Granted"]);

                dtYtdSummary.Rows[28][col] = Convert.ToInt32(drEbi[0]["No - Safety Qualification not found"]);
                dtYtdSummary.Rows[29][col] = Convert.ToInt32(drEbi[0]["No - Information could not be found"]);
                dtYtdSummary.Rows[30][col] = Convert.ToInt32(drEbi[0]["Company has a valid SQ Exemption"]);
            }
        }

        ///////
        //Add condition
        if (Nsiteid == 6 || Nsiteid == 8 || Nsiteid == 18)
        {
            for (int i = 31; i < 64; i++)
            {
                dtYtdSummary.Rows[i][col] = "n/a";
            }

        }
        else
        {
            if (drSMP != null && drSMP.Length>0)
            {
                dtYtdSummary.Rows[31][col] = Convert.ToInt32(drSMP[0]["HS Assessors – Required (# Companies)"]);
                dtYtdSummary.Rows[32][col] = Convert.ToInt32(drSMP[0]["HS Assessors – Being Assessed (# Companies)"]);
                dtYtdSummary.Rows[33][col] = Convert.ToInt32(drSMP[0]["HS Assessors – Awaiting Assignment"]);
                dtYtdSummary.Rows[34][col] = Convert.ToInt32(drSMP[0]["HS Assessors – Approved"]);
                dtYtdSummary.Rows[35][col] = drSMP[0]["HS Assessors – Approved %"];
            }

            /////
            if (drEmbeded.Length > 0 && drEmbeded!=null)
            {
                dtYtdSummary.Rows[36][col] = drEmbeded[0]["Number of Companies:"];
                dtYtdSummary.Rows[37][col] = drEmbeded[0]["Contractor Services Audit"];
                dtYtdSummary.Rows[38][col] = drEmbeded[0]["Workplace Safety Compliance"];
                dtYtdSummary.Rows[39][col] = drEmbeded[0]["Management Health Safety Work Contacts"];

                dtYtdSummary.Rows[40][col] = drEmbeded[0]["Behavioural Observations"];
                dtYtdSummary.Rows[41][col] = drEmbeded[0]["Fatality Prevention Program"];
                dtYtdSummary.Rows[42][col] = drEmbeded[0]["JSA Field Audit Verifications"];
                dtYtdSummary.Rows[43][col] = drEmbeded[0]["Toolbox Meetings"];

                dtYtdSummary.Rows[44][col] = drEmbeded[0]["Alcoa Weekly Contractors Meeting"];
                dtYtdSummary.Rows[45][col] = drEmbeded[0]["Alcoa Monthly Contractors Meeting"];
                dtYtdSummary.Rows[46][col] = drEmbeded[0]["Safety Plan(s) Submitted"];
                dtYtdSummary.Rows[47][col] = drEmbeded[0]["Mandated Training"];

            }
            //////

            if (drNonEmbeded != null && drNonEmbeded.Length>0)
            {
                dtYtdSummary.Rows[48][col] = drNonEmbeded[0]["NE_Number of Companies:"];
                dtYtdSummary.Rows[49][col] = drNonEmbeded[0]["NE_Contractor Services Audit"];
                dtYtdSummary.Rows[50][col] = drNonEmbeded[0]["NE_Workplace Safety Compliance"];
                dtYtdSummary.Rows[51][col] = drNonEmbeded[0]["NE_Management Health Safety Work Contacts"];

                dtYtdSummary.Rows[52][col] = drNonEmbeded[0]["NE_Behavioural Observations"];
                dtYtdSummary.Rows[53][col] = drNonEmbeded[0]["NE_JSA Field Audit Verifications"];
                dtYtdSummary.Rows[54][col] = drNonEmbeded[0]["NE_Toolbox Meetings"];
                dtYtdSummary.Rows[55][col] = drNonEmbeded[0]["NE_Alcoa Weekly Contractors Meeting"];

                dtYtdSummary.Rows[56][col] = drNonEmbeded[0]["NE_Alcoa Monthly Contractors Meeting"];
                dtYtdSummary.Rows[57][col] = drNonEmbeded[0]["NE_Mandated Training"];
            }

            /////////
            if (drCSA != null && drCSA.Length>0)
            {
                dtYtdSummary.Rows[58][col] = Convert.ToInt32(drCSA[0]["CAS Embedded –Required"]);
                dtYtdSummary.Rows[59][col] = Convert.ToInt32(drCSA[0]["CSA Embedded – Required to date"]);
                dtYtdSummary.Rows[60][col] = Convert.ToInt32(drCSA[0]["Embedded – Actual completed"]);
                dtYtdSummary.Rows[61][col] = Convert.ToInt32(drCSA[0]["CSA Non-Embedded 1 – Total Required"]);



                dtYtdSummary.Rows[62][col] = Convert.ToInt32(drCSA[0]["CSA – Non-Embedded 1 – Required to date"]);
                dtYtdSummary.Rows[63][col] = Convert.ToInt32(drCSA[0]["Non-Embedded 1 – Actual completed"]);
            }
            //end

            
        }

        #region Audit Score
        if (dsAuditScore.Tables[0].Rows.Count > 0)
        {
            //dtYtdSummary.Rows[64][col] = dsAuditScore.Tables[0].Rows[0]["Description"];
            //DataView dvData = new DataView(dsAuditScore.Tables[0]);
            //dvData.RowFilter = "SiteId = '" + Val + "'";//Change
            DataView dvData;

            int ValPlan = 0;
            dvData = new DataView(dsAuditScore.Tables[0]);
            dvData.RowFilter = "SiteId = '" + ValPlan + "'";//Change

            if (dvData.ToTable().Rows.Count > 0)
                dtYtdSummary.Rows[64][3] = dvData.ToTable().Rows[0]["Description"];

            if (col == 16)
            {
                // grid_Dash.Columns[16].Visible = true;

                int Val = 13;
                dvData = new DataView(dsAuditScore.Tables[0]);
                dvData.RowFilter = "SiteId = '" + Val + "'";//Change

                if (dvData.ToTable().Rows.Count > 0)
                    dtYtdSummary.Rows[64][col] = dvData.ToTable().Rows[0]["Description"];
            }
            else if (col == 20)
            {
                // grid_Dash.Columns[20].Visible = true;

                int Val = 25;
                dvData = new DataView(dsAuditScore.Tables[0]);
                dvData.RowFilter = "SiteId = '" + Val + "'";//Change

                if (dvData.ToTable().Rows.Count > 0)
                    dtYtdSummary.Rows[64][col] = dvData.ToTable().Rows[0]["Description"];
            }
            else if (col == 13)
            {
                // grid_Dash.Columns[13].Visible = true;

                int Val = 8;
                dvData = new DataView(dsAuditScore.Tables[0]);
                dvData.RowFilter = "SiteId = '" + Val + "'";//Change

                if (dvData.ToTable().Rows.Count > 0)
                    dtYtdSummary.Rows[64][col] = dvData.ToTable().Rows[0]["Description"];
            }
            else if (col == 9)
            {
                //grid_Dash.Columns[9].Visible = true;

                int Val = 7;
                dvData = new DataView(dsAuditScore.Tables[0]);
                dvData.RowFilter = "SiteId = '" + Val + "'";//Change

                if (dvData.ToTable().Rows.Count > 0)
                    dtYtdSummary.Rows[64][col] = dvData.ToTable().Rows[0]["Description"];
            }
            else if (col == 12)
            {
                //grid_Dash.Columns[12].Visible = true;

                int Val = 6;
                dvData = new DataView(dsAuditScore.Tables[0]);
                dvData.RowFilter = "SiteId = '" + Val + "'";//Change

                if (dvData.ToTable().Rows.Count > 0)
                    dtYtdSummary.Rows[64][col] = dvData.ToTable().Rows[0]["Description"];
            }
            else if (col == 10)
            {
                //grid_Dash.Columns[10].Visible = true;

                int Val = 4;
                dvData = new DataView(dsAuditScore.Tables[0]);
                dvData.RowFilter = "SiteId = '" + Val + "'";//Change

                if (dvData.ToTable().Rows.Count > 0)
                    dtYtdSummary.Rows[64][col] = dvData.ToTable().Rows[0]["Description"];
            }
            else if (col == 6)
            {
                // grid_Dash.Columns[6].Visible = true;

                int Val = 1;
                dvData = new DataView(dsAuditScore.Tables[0]);
                dvData.RowFilter = "SiteId = '" + Val + "'";//Change

                if (dvData.ToTable().Rows.Count > 0)
                    dtYtdSummary.Rows[64][col] = dvData.ToTable().Rows[0]["Description"];
            }
            else if (col == 14)
            {
                // grid_Dash.Columns[14].Visible = true;


                int Val = 18;
                dvData = new DataView(dsAuditScore.Tables[0]);
                dvData.RowFilter = "SiteId = '" + Val + "'";//Change

                if (dvData.ToTable().Rows.Count > 0)
                    dtYtdSummary.Rows[64][col] = dvData.ToTable().Rows[0]["Description"];
            }
            else if (col == 7)
            {
                // grid_Dash.Columns[7].Visible = true;

                int Val = 2;
                dvData = new DataView(dsAuditScore.Tables[0]);
                dvData.RowFilter = "SiteId = '" + Val + "'";//Change

                if (dvData.ToTable().Rows.Count > 0)
                    dtYtdSummary.Rows[64][col] = dvData.ToTable().Rows[0]["Description"];
            }
            else if (col == 17)
            {
                //grid_Dash.Columns[17].Visible = true;

                int Val = 14;
                dvData = new DataView(dsAuditScore.Tables[0]);
                dvData.RowFilter = "SiteId = '" + Val + "'";//Change

                if (dvData.ToTable().Rows.Count > 0)
                    dtYtdSummary.Rows[64][col] = dvData.ToTable().Rows[0]["Description"];
            }
            else if (col == 18)
            {
                //grid_Dash.Columns[18].Visible = true;

                int Val = 15;
                dvData = new DataView(dsAuditScore.Tables[0]);
                dvData.RowFilter = "SiteId = '" + Val + "'";//Change

                if (dvData.ToTable().Rows.Count > 0)
                    dtYtdSummary.Rows[64][col] = dvData.ToTable().Rows[0]["Description"];
            }
            else if (col == 8)
            {
                //grid_Dash.Columns[8].Visible = true;

                int Val = 3;
                dvData = new DataView(dsAuditScore.Tables[0]);
                dvData.RowFilter = "SiteId = '" + Val + "'";//Change

                if (dvData.ToTable().Rows.Count > 0)
                    dtYtdSummary.Rows[64][col] = dvData.ToTable().Rows[0]["Description"];
            }
            else if (col == 11)
            {
                //grid_Dash.Columns[11].Visible = true;

                int Val = 5;
                dvData = new DataView(dsAuditScore.Tables[0]);
                dvData.RowFilter = "SiteId = '" + Val + "'";//Change

                if (dvData.ToTable().Rows.Count > 0)
                    dtYtdSummary.Rows[64][col] = dvData.ToTable().Rows[0]["Description"];
            }
            else if (col == 21)
            {
                //grid_Dash.Columns[21].Visible = true;

                int Val = 24;
                dvData = new DataView(dsAuditScore.Tables[0]);
                dvData.RowFilter = "SiteId = '" + Val + "'";//Change

                if (dvData.ToTable().Rows.Count > 0)
                    dtYtdSummary.Rows[64][col] = dvData.ToTable().Rows[0]["Description"];
            }
            else if (col == 5)
            {
                //grid_Dash.Columns[11].Visible = true;

                int Val = -1;
                dvData = new DataView(dsAuditScore.Tables[0]);
                dvData.RowFilter = "SiteId = '" + Val + "'";//Change

                if (dvData.ToTable().Rows.Count > 0)
                    dtYtdSummary.Rows[64][col] = dvData.ToTable().Rows[0]["Description"];
            }
            else if (col == 4)
            {
                //grid_Dash.Columns[11].Visible = true;

                int Val = -8; //Changed by Pankaj for AUS GPP
                dvData = new DataView(dsAuditScore.Tables[0]);
                dvData.RowFilter = "SiteId = '" + Val + "'";//Change

                if (dvData.ToTable().Rows.Count > 0)
                    dtYtdSummary.Rows[64][col] = dvData.ToTable().Rows[0]["Description"];
            }
            else if (col == 15)
            {
                //grid_Dash.Columns[11].Visible = true;

                int Val = -4;
                dvData = new DataView(dsAuditScore.Tables[0]);
                dvData.RowFilter = "SiteId = '" + Val + "'";//Change

                if (dvData.ToTable().Rows.Count > 0)
                    dtYtdSummary.Rows[64][col] = dvData.ToTable().Rows[0]["Description"];
            }
            else if (col == 19)
            {
                //grid_Dash.Columns[11].Visible = true;

                int Val = -7;
                dvData = new DataView(dsAuditScore.Tables[0]);
                dvData.RowFilter = "SiteId = '" + Val + "'";//Change

                if (dvData.ToTable().Rows.Count > 0)
                    dtYtdSummary.Rows[64][col] = dvData.ToTable().Rows[0]["Description"];
            }

        }
        #endregion

    }

    #region Cell Coloring Logic

    private bool GetComparisionResult(string Operator, string PlanValue, string valueToCompare)
    {
        bool retVal = false;
        //if (string.IsNullOrEmpty(Operator) && string.IsNullOrEmpty(PlanValue) && string.IsNullOrEmpty(valueToCompare))
        try
        {
            switch (Operator)
            {
                case ">":
                    {
                        if (decimal.Parse(valueToCompare) > decimal.Parse(PlanValue))
                        {
                            retVal = true;
                        }
                        break;
                    }
                case "<":
                    {
                        if (decimal.Parse(valueToCompare) < decimal.Parse(PlanValue))
                        {
                            retVal = true;
                        }
                        break;
                    }
                case "=":
                    {
                        if (decimal.Parse(valueToCompare) == decimal.Parse(PlanValue))
                        {
                            retVal = true;
                        }
                        break;
                    }
                default:
                    {
                        retVal = false;
                        break;
                    }
            }
        }
        catch (Exception ex)
        { }
        return retVal;
    }

    private string GetColorByValue(string kpi, string Value, string siteName)
    {
        string color = string.Empty;
        double delValue = 0.00;

        //Added By Bishwajit to Manage Session
        if (Session["dsPlan_S"] == null)        
        {
            int year = Convert.ToInt32(ddlYear.SelectedItem.Value);
            int month = Convert.ToInt32(ddlMonth.SelectedItem.Value);
            DataSet dsPlan = null;
            dsPlan = DataRepository.KpiProvider.Plan(year, month);
            Session["dsPlan_S"] = dsPlan;
        }
        //End Added By Bishwajit to Manage Session

        DataSet PlanScore = (DataSet)Session["dsPlan_S"];
        //if(PlanScore == null)
        //{
            
        //}
        //if (PlanScore.Tables[0].Rows.Count > 0)
        //{
        //PlanScore = null;
        try
        {
            if (PlanScore != null)
            {
                if (kpi.Contains("Lost Work Days Frequency Rate   LWDFR"))
                {

                    if (Value != "-" && Value != "n/a" && Value != "")
                    {
                        //delValue = Convert.ToDouble(Value);

                        string Op = PlanScore.Tables[0].Rows[0]["Operator"].ToString().Trim();
                        string PlanValue = PlanScore.Tables[0].Rows[0]["PlanValue"].ToString().Trim();

                        if (Op != "" && PlanValue != "")
                        {
                            bool CmpResult = GetComparisionResult(Op, PlanValue, Value);
                            if (CmpResult == true)
                            {
                                color = "Lime";
                            }
                            else
                            {
                                color = "Red";
                            }
                        }
                        else
                            color = "White";

                        //if (delValue == Convert.ToDouble(PlanScore.Tables[0].Rows[0]["PlanValue"].ToString().Trim()))
                        //{
                        //    color = "Lime";
                        //}
                        //else
                        //{
                        //    color = "Red";
                        //}
                    }
                    else
                        color = "White";
                }
                else if (kpi.Contains("Total Recordable Injuries Frequency Rate    TRIFR"))
                {
                    if (Value != "-" && Value != "n/a" && Value != "")
                    {
                        //delValue = Convert.ToDouble(Value);
                        string Op = PlanScore.Tables[0].Rows[1]["Operator"].ToString().Trim();
                        string PlanValue = PlanScore.Tables[0].Rows[1]["PlanValue"].ToString().Trim();
                        if (Op != "" && PlanValue != "")
                        {
                            bool CmpResult = GetComparisionResult(Op, PlanValue, Value);
                            if (CmpResult == true)
                            {
                                color = "Lime";
                            }
                            else
                            {
                                color = "Red";
                            }
                        }
                        else
                            color = "White";

                        //if (delValue < Convert.ToDouble(PlanScore.Tables[0].Rows[1]["PlanValue"].ToString()))
                        //{
                        //    color = "Lime";
                        //}
                        //else
                        //{
                        //    color = "Red";
                        //}
                    }
                    else
                        color = "White";
                }
                else if (kpi.Contains("All Injury Frequency Rate   AIFR"))
                {
                    if (Value != "-" && Value != "n/a" && Value != "")
                    {
                        //delValue = Convert.ToDouble(Value);


                        //if (delValue < Convert.ToDouble(PlanScore.Tables[0].Rows[2]["PlanValue"].ToString()))
                        //{
                        //    color = "Lime";
                        //}
                        //else
                        //{
                        //    color = "Red";
                        //}

                        string Op = PlanScore.Tables[0].Rows[2]["Operator"].ToString().Trim();
                        string PlanValue = PlanScore.Tables[0].Rows[2]["PlanValue"].ToString().Trim();

                        if (Op != "" && PlanValue != "")
                        {
                            bool CmpResult = GetComparisionResult(Op, PlanValue, Value);
                            if (CmpResult == true)
                            {
                                color = "Lime";
                            }
                            else
                            {
                                color = "Red";
                            }
                        }
                        else
                            color = "White";
                    }
                    else
                        color = "White";
                }
                else if (kpi.Contains("DART"))
                {
                    if (Value != "-" && Value != "n/a" && Value != "")
                    {
                        //delValue = Convert.ToDouble(Value);

                        //if (delValue == Convert.ToDouble(PlanScore.Tables[0].Rows[3]["PlanValue"].ToString()))
                        //{
                        //    color = "Lime";
                        //}
                        //else
                        //{
                        //    color = "Red";
                        //}

                        string Op = PlanScore.Tables[0].Rows[3]["Operator"].ToString().Trim();
                        string PlanValue = PlanScore.Tables[0].Rows[3]["PlanValue"].ToString().Trim();

                        if (Op != "" && PlanValue != "")
                        {
                            bool CmpResult = GetComparisionResult(Op, PlanValue, Value);
                            if (CmpResult == true)
                            {
                                color = "Lime";
                            }
                            else
                            {
                                color = "Red";
                            }
                        }
                        else
                            color = "White";
                    }
                    else
                        color = "White";
                }
                else if (kpi.Contains("Lost Work Day   LWD"))
                {
                    if (Value != "-" && Value != "n/a" && Value != "")
                    {
                        //delValue = Convert.ToDouble(Value);
                        //if (delValue == Convert.ToDouble(PlanScore.Tables[0].Rows[4]["PlanValue"].ToString()))
                        //{
                        //    color = "Lime";
                        //}
                        //else
                        //{
                        //    color = "Yellow";
                        //}
                        string Op = PlanScore.Tables[0].Rows[4]["Operator"].ToString().Trim();
                        string PlanValue = PlanScore.Tables[0].Rows[4]["PlanValue"].ToString().Trim();


                        if (Op != "" && PlanValue != "")
                        {
                            bool CmpResult = GetComparisionResult(Op, PlanValue, Value);
                            if (CmpResult == true)
                            {
                                color = "Lime";
                            }
                            else
                            {
                                color = "Yellow";
                            }
                        }
                        else
                            color = "White";
                    }
                    else
                        color = "White";
                }
                else if (kpi.Contains("Restricted Duty   RST"))
                {
                    if (Value != "-" && Value != "n/a" && Value != "")
                    {
                        //delValue = Convert.ToDouble(Value);
                        //if (delValue == Convert.ToDouble(PlanScore.Tables[0].Rows[5]["PlanValue"].ToString()))
                        //{
                        //    color = "Lime";
                        //}
                        //else
                        //{
                        //    color = "Yellow";
                        //}

                        string Op = PlanScore.Tables[0].Rows[5]["Operator"].ToString().Trim();
                        string PlanValue = PlanScore.Tables[0].Rows[5]["PlanValue"].ToString().Trim();

                        if (Op != "" && PlanValue != "")
                        {
                            bool CmpResult = GetComparisionResult(Op, PlanValue, Value);
                            if (CmpResult == true)
                            {
                                color = "Lime";
                            }
                            else
                            {
                                color = "Yellow";
                            }
                        }
                        else
                            color = "White";
                    }
                    else
                        color = "White";
                }
                else if (kpi.Contains("Medical Treatments   MT"))
                {
                    if (Value != "-" && Value != "n/a" && Value != "")
                    {
                        //delValue = Convert.ToDouble(Value);

                        //if (delValue == Convert.ToDouble(PlanScore.Tables[0].Rows[6]["PlanValue"].ToString()))
                        //{
                        //    color = "Lime";
                        //}
                        //else
                        //{
                        //    color = "Yellow";
                        //}

                        string Op = PlanScore.Tables[0].Rows[6]["Operator"].ToString().Trim();
                        string PlanValue = PlanScore.Tables[0].Rows[6]["PlanValue"].ToString().Trim();

                        if (Op != "" && PlanValue != "")
                        {
                            bool CmpResult = GetComparisionResult(Op, PlanValue, Value);
                            if (CmpResult == true)
                            {
                                color = "Lime";
                            }
                            else
                            {
                                color = "Yellow";
                            }
                        }
                        else
                            color = "White";
                    }
                    else
                        color = "White";
                }
                else if (kpi.Contains("First Aids   FA"))
                {
                    if (Value != "-" && Value != "n/a" && Value != "")
                    {
                        //delValue = Convert.ToDouble(Value);
                        //if (delValue == Convert.ToDouble(PlanScore.Tables[0].Rows[7]["PlanValue"].ToString()))
                        //{
                        //    color = "Lime";
                        //}
                        //else
                        //{
                        //    color = "Yellow";
                        //}

                        string Op = PlanScore.Tables[0].Rows[7]["Operator"].ToString().Trim();
                        string PlanValue = PlanScore.Tables[0].Rows[7]["PlanValue"].ToString().Trim();


                        if (Op != "" && PlanValue != "")
                        {
                            bool CmpResult = GetComparisionResult(Op, PlanValue, Value);
                            if (CmpResult == true)
                            {
                                color = "Lime";
                            }
                            else
                            {
                                color = "Yellow";
                            }
                        }
                        else
                            color = "White";
                    }
                    else
                        color = "White";
                }
                else if (kpi.Contains("Injury Free Events   IFE"))
                {
                    if (Value != "-" && Value != "n/a" && Value != "")
                    {
                        //delValue = Convert.ToDouble(Value);
                        //if (delValue == Convert.ToDouble(PlanScore.Tables[0].Rows[8]["PlanValue"].ToString()))
                        //{
                        //    color = "Lime";
                        //}
                        //else
                        //{
                        //    color = "Yellow";
                        //}

                        string Op = PlanScore.Tables[0].Rows[8]["Operator"].ToString().Trim();
                        string PlanValue = PlanScore.Tables[0].Rows[8]["PlanValue"].ToString().Trim();


                        if (Op != "" && PlanValue != "")
                        {
                            bool CmpResult = GetComparisionResult(Op, PlanValue, Value);
                            if (CmpResult == true)
                            {
                                color = "Lime";
                            }
                            else
                            {
                                color = "Yellow";
                            }
                        }
                        else
                            color = "White";
                    }
                    else
                        color = "White";

                }
                else if (kpi.Contains("Risk Notifications"))
                {
                    if (Value != "-" && Value != "n/a" && Value != "")
                    {
                        //delValue = Convert.ToDouble(Value);
                        //if (delValue == Convert.ToDouble(PlanScore.Tables[0].Rows[9]["PlanValue"].ToString()))
                        //{
                        //    color = "Lime";
                        //}
                        //else
                        //{
                        //    color = "Yellow";
                        //}

                        string Op = PlanScore.Tables[0].Rows[9]["Operator"].ToString().Trim();
                        string PlanValue = PlanScore.Tables[0].Rows[9]["PlanValue"].ToString().Trim();

                        if (Op != "" && PlanValue != "")
                        {
                            bool CmpResult = GetComparisionResult(Op, PlanValue, Value);
                            if (CmpResult == true)
                            {
                                color = "Lime";
                            }
                            else
                            {
                                color = "Yellow";
                            }
                        }
                        else
                            color = "White";
                    }
                    else
                        color = "White";
                }

                else if (kpi.Contains("IFE / Injury ratio"))
                {
                    if (Value != "-" && Value != "n/a" && Value != "")
                    {
                        //delValue = Convert.ToDouble(Value);
                        //if (delValue > Convert.ToDouble(PlanScore.Tables[0].Rows[10]["PlanValue"].ToString()))
                        //{
                        //    color = "Lime";
                        //}
                        //else
                        //{
                        //    color = "Yellow";
                        //}

                        string Op = PlanScore.Tables[0].Rows[10]["Operator"].ToString().Trim();
                        string PlanValue = PlanScore.Tables[0].Rows[10]["PlanValue"].ToString().Trim();

                        if (Op != "" && PlanValue != "")
                        {
                            bool CmpResult = GetComparisionResult(Op, PlanValue, Value);
                            if (CmpResult == true)
                            {
                                color = "Lime";
                            }
                            else
                            {
                                color = "Yellow";
                            }
                        }
                        else
                            color = "White";
                    }
                    else
                        color = "White";
                }
                else if (kpi.Contains("RN / Injury ratio"))
                {
                    if (Value != "-" && Value != "n/a" && Value != "")
                    {
                        //delValue = Convert.ToDouble(Value);
                        //if (delValue > Convert.ToDouble(PlanScore.Tables[0].Rows[11]["PlanValue"].ToString()))
                        //{
                        //    color = "Lime";
                        //}
                        //else
                        //{
                        //    color = "Yellow";
                        //}

                        string Op = PlanScore.Tables[0].Rows[11]["Operator"].ToString().Trim();
                        string PlanValue = PlanScore.Tables[0].Rows[11]["PlanValue"].ToString().Trim();

                        if (Op != "" && PlanValue != "")
                        {
                            bool CmpResult = GetComparisionResult(Op, PlanValue, Value);
                            if (CmpResult == true)
                            {
                                color = "Lime";
                            }
                            else
                            {
                                color = "Yellow";
                            }
                        }
                        else
                            color = "White";
                    }
                    else
                        color = "White";
                }
                else if (kpi.Contains("Expired - Currently With Procurement"))
                {
                    if (Value != "-" && Value != "n/a" && Value != "")
                    {
                        //delValue = Convert.ToDouble(Value);
                        //if (delValue == Convert.ToDouble(PlanScore.Tables[0].Rows[12]["PlanValue"].ToString()))
                        //{
                        //    color = "Lime";
                        //}
                        //else
                        //{
                        //    color = "Red";
                        //}

                        string Op = PlanScore.Tables[0].Rows[12]["Operator"].ToString().Trim();
                        string PlanValue = PlanScore.Tables[0].Rows[12]["PlanValue"].ToString().Trim();

                        if (Op != "" && PlanValue != "")
                        {
                            bool CmpResult = GetComparisionResult(Op, PlanValue, Value);
                            if (CmpResult == true)
                            {
                                color = "Lime";
                            }
                            else
                            {
                                color = "Red";
                            }
                        }
                        else
                            color = "White";
                    }
                    else
                        color = "White";
                }
                else if (kpi.Contains("Expired - Currently With Supplier"))
                {
                    if (Value != "-" && Value != "n/a" && Value != "")
                    {
                        //delValue = Convert.ToDouble(Value);
                        //if (delValue == Convert.ToDouble(PlanScore.Tables[0].Rows[13]["PlanValue"].ToString()))
                        //{
                        //    color = "Lime";
                        //}
                        //else
                        //{
                        //    color = "Red";
                        //}

                        string Op = PlanScore.Tables[0].Rows[13]["Operator"].ToString().Trim();
                        string PlanValue = PlanScore.Tables[0].Rows[13]["PlanValue"].ToString().Trim();

                        if (Op != "" && PlanValue != "")
                        {

                            bool CmpResult = GetComparisionResult(Op, PlanValue, Value);
                            if (CmpResult == true)
                            {
                                color = "Lime";
                            }
                            else
                            {
                                color = "Red";
                            }
                        }
                        else

                            color = "White";
                    }
                    else
                        color = "White";
                }
                else if (kpi.Contains("Expiring  > 7 days"))
                {
                    if (Value != "-" && Value != "n/a" && Value != "")
                    {
                        //delValue = Convert.ToDouble(Value);
                        //if (delValue == Convert.ToDouble(PlanScore.Tables[0].Rows[14]["PlanValue"].ToString()))
                        //{
                        //    color = "Lime";
                        //}
                        //else
                        //{
                        //    color = "Yellow";
                        //}

                        string Op = PlanScore.Tables[0].Rows[14]["Operator"].ToString().Trim();
                        string PlanValue = PlanScore.Tables[0].Rows[14]["PlanValue"].ToString().Trim();

                        if (Op != "" && PlanValue != "")
                        {
                            bool CmpResult = GetComparisionResult(Op, PlanValue, Value);
                            if (CmpResult == true)
                            {
                                color = "Lime";
                            }
                            else
                            {
                                color = "Yellow";
                            }
                        }
                        else

                            color = "White";
                    }
                    else
                        color = "White";

                }
                else if (kpi.Contains("Questionnaire  > 7 days"))
                {
                    if (Value != "-" && Value != "n/a" && Value != "")
                    {
                        //delValue = Convert.ToDouble(Value);
                        //if (delValue == Convert.ToDouble(PlanScore.Tables[0].Rows[15]["PlanValue"].ToString()))
                        //{
                        //    color = "Lime";
                        //}
                        //else
                        //{
                        //    color = "Yellow";
                        //}

                        string Op = PlanScore.Tables[0].Rows[15]["Operator"].ToString().Trim();
                        string PlanValue = PlanScore.Tables[0].Rows[15]["PlanValue"].ToString().Trim();

                        if (Op != "" && PlanValue != "")
                        {
                            bool CmpResult = GetComparisionResult(Op, PlanValue, Value);
                            if (CmpResult == true)
                            {
                                color = "Lime";
                            }
                            else
                            {
                                color = "Yellow";
                            }
                        }
                        else
                            color = "White";
                    }
                    else
                        color = "White";
                }

                else if (kpi.Contains("Assign Company Status   > 7 days"))
                {
                    if (Value != "-" && Value != "n/a" && Value != "")
                    {
                        //delValue = Convert.ToDouble(Value);
                        //if (delValue == Convert.ToDouble(PlanScore.Tables[0].Rows[16]["PlanValue"].ToString()))
                        //{
                        //    color = "Lime";
                        //}
                        //else
                        //{
                        //    color = "Yellow";
                        //}

                        string Op = PlanScore.Tables[0].Rows[16]["Operator"].ToString().Trim();
                        string PlanValue = PlanScore.Tables[0].Rows[16]["PlanValue"].ToString().Trim();
                        if (Op != "" && PlanValue != "")
                        {
                            bool CmpResult = GetComparisionResult(Op, PlanValue, Value);
                            if (CmpResult == true)
                            {
                                color = "Lime";
                            }
                            else
                            {
                                color = "Yellow";
                            }
                        }
                        else
                            color = "White";
                    }
                    else
                        color = "White";

                }
                else if (kpi.Contains("Questionnaire  > 28 Days"))
                {
                    if (Value != "-" && Value != "n/a" && Value != "")
                    {
                        //delValue = Convert.ToDouble(Value);
                        //if (delValue == Convert.ToDouble(PlanScore.Tables[0].Rows[17]["PlanValue"].ToString()))
                        //{
                        //    color = "Lime";
                        //}
                        //else
                        //{
                        //    color = "Yellow";
                        //}
                        string Op = PlanScore.Tables[0].Rows[17]["Operator"].ToString().Trim();
                        string PlanValue = PlanScore.Tables[0].Rows[17]["PlanValue"].ToString().Trim();

                        if (Op != "" && PlanValue != "")
                        {
                            bool CmpResult = GetComparisionResult(Op, PlanValue, Value);
                            if (CmpResult == true)
                            {
                                color = "Lime";
                            }
                            else
                            {
                                color = "Yellow";
                            }
                        }
                        else
                            color = "White";
                    }
                    else
                        color = "White";
                }
                else if (kpi.Contains("Questionnaire - Resubmitting"))
                {
                    if (Value != "-" && Value != "n/a" && Value != "")
                    {
                        //delValue = Convert.ToDouble(Value);
                        //if (delValue == Convert.ToDouble(PlanScore.Tables[0].Rows[18]["PlanValue"].ToString()))
                        //{
                        //    color = "Lime";
                        //}
                        //else
                        //{
                        //    color = "Yellow";
                        //}

                        string Op = PlanScore.Tables[0].Rows[18]["Operator"].ToString().Trim();
                        string PlanValue = PlanScore.Tables[0].Rows[18]["PlanValue"].ToString().Trim();

                        if (Op != "" && PlanValue != "")
                        {
                            bool CmpResult = GetComparisionResult(Op, PlanValue, Value);
                            if (CmpResult == true)
                            {
                                color = "Lime";
                            }
                            else
                            {
                                color = "Yellow";
                            }
                        }
                        else
                            color = "White";

                    }
                    else
                        color = "White";
                }
                else if (kpi.Contains("Access > 7 Days"))
                {
                    if (Value != "-" && Value != "n/a" && Value != "")
                    {
                        //delValue = Convert.ToDouble(Value);
                        //if (delValue == Convert.ToDouble(PlanScore.Tables[0].Rows[19]["PlanValue"].ToString()))
                        //{
                        //    color = "Lime";
                        //}
                        //else
                        //{
                        //    color = "Yellow";
                        //}
                        string Op = PlanScore.Tables[0].Rows[19]["Operator"].ToString().Trim();
                        string PlanValue = PlanScore.Tables[0].Rows[19]["PlanValue"].ToString().Trim();

                        if (Op != "" && PlanValue != "")
                        {
                            bool CmpResult = GetComparisionResult(Op, PlanValue, Value);
                            if (CmpResult == true)
                            {
                                color = "Lime";
                            }
                            else
                            {
                                color = "Yellow";
                            }
                        }
                        else
                            color = "White";
                    }
                    else
                        color = "White";
                }
                else if (kpi.Contains("Expired - Currently With H&S Assessor"))
                {
                    if (Value != "-" && Value != "n/a" && Value != "")
                    {
                        //delValue = Convert.ToDouble(Value);
                        //if (delValue == Convert.ToDouble(PlanScore.Tables[0].Rows[20]["PlanValue"].ToString()))
                        //{
                        //    color = "Lime";
                        //}
                        //else
                        //{
                        //    color = "Red";
                        //}

                        string Op = PlanScore.Tables[0].Rows[20]["Operator"].ToString().Trim();
                        string PlanValue = PlanScore.Tables[0].Rows[20]["PlanValue"].ToString().Trim();

                        if (Op != "" && PlanValue != "")
                        {
                            bool CmpResult = GetComparisionResult(Op, PlanValue, Value);
                            if (CmpResult == true)
                            {
                                color = "Lime";
                            }
                            else
                            {
                                color = "Red";
                            }
                        }
                        else
                            color = "White";
                    }
                    else
                        color = "White";
                }
                else if (kpi.Contains("Assessing Procurement Questionnaire > 7 days"))
                {
                    if (Value != "-" && Value != "n/a" && Value != "")
                    {
                        //delValue = Convert.ToDouble(Value);
                        //if (delValue == Convert.ToDouble(PlanScore.Tables[0].Rows[21]["PlanValue"].ToString()))
                        //{
                        //    color = "Lime";
                        //}
                        //else
                        //{
                        //    color = "Yellow";
                        //}
                        string Op = PlanScore.Tables[0].Rows[21]["Operator"].ToString().Trim();
                        string PlanValue = PlanScore.Tables[0].Rows[21]["PlanValue"].ToString().Trim();

                        if (Op != "" && PlanValue != "")
                        {
                            bool CmpResult = GetComparisionResult(Op, PlanValue, Value);
                            if (CmpResult == true)
                            {
                                color = "Lime";
                            }
                            else
                            {
                                color = "Yellow";
                            }
                        }
                        else
                            color = "White";
                    }
                    else
                        color = "White";

                }
                else if (kpi.Contains("Assessing Questionnaire > 7 days"))
                {
                    if (Value != "-" && Value != "n/a" && Value != "")
                    {
                        //delValue = Convert.ToDouble(Value);
                        //if (delValue == Convert.ToDouble(PlanScore.Tables[0].Rows[22]["PlanValue"].ToString()))
                        //{
                        //    color = "Lime";
                        //}
                        //else
                        //{
                        //    color = "Yellow";
                        //}

                        string Op = PlanScore.Tables[0].Rows[22]["Operator"].ToString().Trim();
                        string PlanValue = PlanScore.Tables[0].Rows[22]["PlanValue"].ToString().Trim();

                        if (Op != "" && PlanValue != "")
                        {
                            bool CmpResult = GetComparisionResult(Op, PlanValue, Value);
                            if (CmpResult == true)
                            {
                                color = "Lime";
                            }
                            else
                            {
                                color = "Yellow";
                            }
                        }
                        else
                            color = "White";
                    }
                    else
                        color = "White";
                }
                else if (kpi.Contains("SQ process total"))
                {
                    if (Value != "-" && Value != "n/a" && Value != "")
                    {
                        //delValue = Convert.ToDouble(Value);
                        //if (delValue == Convert.ToDouble(PlanScore.Tables[0].Rows[23]["PlanValue"].ToString()))
                        //{
                        //    color = "Lime";
                        //}
                        //else
                        //{
                        //    color = "Yellow";
                        //}
                        string Op = PlanScore.Tables[0].Rows[23]["Operator"].ToString().Trim();
                        string PlanValue = PlanScore.Tables[0].Rows[23]["PlanValue"].ToString().Trim();

                        if (Op != "" && PlanValue != "")
                        {
                            bool CmpResult = GetComparisionResult(Op, PlanValue, Value);
                            if (CmpResult == true)
                            {
                                color = "Lime";
                            }
                            else
                            {
                                color = "Yellow";
                            }
                        }
                        else
                            color = "White";
                    }
                    else
                        color = "White";
                }
                else if (kpi.Contains("2) Yes - SQ Current (expiring. in 60 days) / Site access granted"))
                {
                    if (Value != "-" && Value != "n/a" && Value != "" && Value != "$")
                    {
                        //delValue = Convert.ToDouble(Value);
                        //if (delValue == Convert.ToDouble(PlanScore.Tables[0].Rows[24]["PlanValue"].ToString()))
                        //{
                        //    color = "Lime";
                        //}
                        //else
                        //{
                        //    color = "Yellow";
                        //}

                        string Op = PlanScore.Tables[0].Rows[24]["Operator"].ToString().Trim();
                        string PlanValue = PlanScore.Tables[0].Rows[24]["PlanValue"].ToString().Trim();

                        if (Op != "" && PlanValue != "")
                        {
                            bool CmpResult = GetComparisionResult(Op, PlanValue, Value);
                            if (CmpResult == true)
                            {
                                color = "Lime";
                            }
                            else
                            {
                                color = "Yellow";
                            }
                        }
                        else
                            color = "White";
                    }
                    else if (siteName == "Bunbury Port" || siteName == "Willowdale (Mine)" || siteName == "FML" || siteName == "Anglesea" || siteName == "ARP Point Henry" || siteName == "Yennora")
                    {
                        color = "Orange";
                    }
                    else
                        color = "White";
                }
                else if (kpi.Contains("3) No - Current SQ not found / Access to Site Not Granted"))
                {
                    if (Value != "-" && Value != "n/a" && Value != "" && Value != "$")
                    {
                        //delValue = Convert.ToDouble(Value);
                        //if (delValue == Convert.ToDouble(PlanScore.Tables[0].Rows[25]["PlanValue"].ToString()))
                        //{
                        //    color = "Lime";
                        //}
                        //else
                        //{
                        //    color = "Red";
                        //}

                        string Op = PlanScore.Tables[0].Rows[25]["Operator"].ToString().Trim();
                        string PlanValue = PlanScore.Tables[0].Rows[25]["PlanValue"].ToString().Trim();

                        if (Op != "" && PlanValue != "")
                        {
                            bool CmpResult = GetComparisionResult(Op, PlanValue, Value);
                            if (CmpResult == true)
                            {
                                color = "Lime";
                            }
                            else
                            {
                                color = "Red";
                            }
                        }
                        else
                            color = "White";
                    }
                    else if (siteName == "Bunbury Port" || siteName == "Willowdale (Mine)" || siteName == "FML" || siteName == "Anglesea" || siteName == "ARP Point Henry" || siteName == "Yennora")
                    {
                        color = "Orange";
                    }
                    else
                        color = "White";
                }
                else if (kpi.Contains("4) No - SQ Expired / Access to Site Not Granted"))
                {
                    if (Value != "-" && Value != "n/a" && Value != "" && Value != "$")
                    {
                        //delValue = Convert.ToDouble(Value);
                        //if (delValue == Convert.ToDouble(PlanScore.Tables[0].Rows[26]["PlanValue"].ToString()))
                        //{
                        //    color = "Lime";
                        //}
                        //else
                        //{
                        //    color = "Red";
                        //}

                        string Op = PlanScore.Tables[0].Rows[26]["Operator"].ToString().Trim();
                        string PlanValue = PlanScore.Tables[0].Rows[26]["PlanValue"].ToString().Trim();

                        if (Op != "" && PlanValue != "")
                        {
                            bool CmpResult = GetComparisionResult(Op, PlanValue, Value);
                            if (CmpResult == true)
                            {
                                color = "Lime";
                            }
                            else
                            {
                                color = "Red";
                            }
                        }
                        else
                            color = "White";
                    }
                    else if (siteName == "Bunbury Port" || siteName == "Willowdale (Mine)" || siteName == "FML" || siteName == "Anglesea" || siteName == "ARP Point Henry" || siteName == "Yennora")
                    {
                        color = "Orange";
                    }
                    else
                        color = "White";
                }
                else if (kpi.Contains("5) No - Access to Site Not Granted"))
                {
                    if (Value != "-" && Value != "n/a" && Value != "" && Value != "$")
                    {
                        //delValue = Convert.ToDouble(Value);
                        //if (delValue == Convert.ToDouble(PlanScore.Tables[0].Rows[27]["PlanValue"].ToString()))
                        //{
                        //    color = "Lime";
                        //}
                        //else
                        //{
                        //    color = "Red";
                        //}

                        string Op = PlanScore.Tables[0].Rows[27]["Operator"].ToString().Trim();
                        string PlanValue = PlanScore.Tables[0].Rows[27]["PlanValue"].ToString().Trim();

                        if (Op != "" && PlanValue != "")
                        {
                            bool CmpResult = GetComparisionResult(Op, PlanValue, Value);
                            if (CmpResult == true)
                            {
                                color = "Lime";
                            }
                            else
                            {
                                color = "Red";
                            }
                        }
                        else
                            color = "White";
                    }
                    else if (siteName == "Bunbury Port" || siteName == "Willowdale (Mine)" || siteName == "FML" || siteName == "Anglesea" || siteName == "ARP Point Henry" || siteName == "Yennora")
                    {
                        color = "Orange";
                    }
                    else
                        color = "White";
                }


                else if (kpi.Contains("6) No - Safety Qualification not found"))
                {
                    if (Value != "-" && Value != "n/a" && Value != "" && Value != "$")
                    {
                        //delValue = Convert.ToDouble(Value);
                        //if (delValue == Convert.ToDouble(PlanScore.Tables[0].Rows[28]["PlanValue"].ToString()))
                        //{
                        //    color = "Lime";
                        //}
                        //else
                        //{
                        //    color = "Red";
                        //}

                        string Op = PlanScore.Tables[0].Rows[28]["Operator"].ToString().Trim();
                        string PlanValue = PlanScore.Tables[0].Rows[28]["PlanValue"].ToString().Trim();

                        if (Op != "" && PlanValue != "")
                        {
                            bool CmpResult = GetComparisionResult(Op, PlanValue, Value);
                            if (CmpResult == true)
                            {
                                color = "Lime";
                            }
                            else
                            {
                                color = "Red";
                            }
                        }
                        else
                            color = "White";
                    }
                    else if (siteName == "Bunbury Port" || siteName == "Willowdale (Mine)" || siteName == "FML" || siteName == "Anglesea" || siteName == "ARP Point Henry" || siteName == "Yennora")
                    {
                        color = "Orange";
                    }
                    else
                        color = "White";
                }
                else if (kpi.Contains("7) No - Information could not be found"))
                {
                    if (Value != "-" && Value != "n/a" && Value != "" && Value != "$")
                    {
                        //delValue = Convert.ToDouble(Value);
                        //if (delValue == Convert.ToDouble(PlanScore.Tables[0].Rows[29]["PlanValue"].ToString()))
                        //{
                        //    color = "Lime";
                        //}
                        //else
                        //{
                        //    color = "Red";
                        //}


                        string Op = PlanScore.Tables[0].Rows[29]["Operator"].ToString().Trim();
                        string PlanValue = PlanScore.Tables[0].Rows[29]["PlanValue"].ToString().Trim();

                        if (Op != "" && PlanValue != "")
                        {
                            bool CmpResult = GetComparisionResult(Op, PlanValue, Value);
                            if (CmpResult == true)
                            {
                                color = "Lime";
                            }
                            else
                            {
                                color = "Red";
                            }
                        }
                        else
                            color = "White";
                    }
                    else if (siteName == "Bunbury Port" || siteName == "Willowdale (Mine)" || siteName == "FML" || siteName == "Anglesea" || siteName == "ARP Point Henry" || siteName == "Yennora")
                    {
                        color = "Orange";
                    }
                    else
                        color = "White";
                }
                else if (kpi.Contains("8) Company has a valid SQ exemption"))
                {
                    if (Value != "-" && Value != "n/a" && Value != "" && Value != "$")
                    {
                        //delValue = Convert.ToDouble(Value);
                        //if (delValue == Convert.ToDouble(PlanScore.Tables[0].Rows[30]["PlanValue"].ToString()))
                        //{
                        //    color = "Lime";
                        //}
                        //else
                        //{
                        //    color = "Red";
                        //}

                        string Op = PlanScore.Tables[0].Rows[30]["Operator"].ToString().Trim();
                        string PlanValue = PlanScore.Tables[0].Rows[30]["PlanValue"].ToString().Trim();

                        if (Op != "" && PlanValue != "")
                        {
                            bool CmpResult = GetComparisionResult(Op, PlanValue, Value);
                            if (CmpResult == true)
                            {
                                color = "Lime";
                            }
                            else
                            {
                                color = "Red";
                            }
                        }
                        else
                            color = "White";
                    }
                    else if (siteName == "Bunbury Port" || siteName == "Willowdale (Mine)" || siteName == "FML" || siteName == "Anglesea" || siteName == "ARP Point Henry" || siteName == "Yennora")
                    {
                        color = "Orange";
                    }
                    else
                        color = "White";
                }
                else if (kpi.Contains("Required (# Companies)"))
                {
                    if (Value != "-" && Value != "n/a" && Value != "")
                    {
                        delValue = Convert.ToDouble(Value);
                        if (Value != "n/a")
                        {
                            color = "Yellow";
                        }
                    }
                    else
                        color = "White";
                }
                else if (kpi.Contains("Being Assessed (# Companies)"))
                {
                    if (Value != "-" && Value != "n/a" && Value != "")
                    {
                        delValue = Convert.ToDouble(Value);
                        if (Value != "n/a")
                        {
                            color = "Yellow";
                        }
                    }
                    else
                        color = "White";
                }
                else if (kpi.Contains("Awaiting Assignment (# Companies)"))
                {
                    if (Value != "-" && Value != "n/a" && Value != "")
                    {
                        delValue = Convert.ToDouble(Value);
                        if (Value != "n/a")
                        {
                            color = "Yellow";
                        }
                    }
                    else
                        color = "White";
                }
                //else if (kpi.Contains("Approved %"))
                else if (Regex.IsMatch(kpi, "Approved %"))
                {
                    if (Value != "-" && Value != "n/a" && Value != "")
                    {
                        //delValue = Convert.ToDouble(Value);
                        //if (delValue >= 100)
                        //{
                        //    color = "Lime";
                        //}
                        //else
                        //{
                        //    color = "Yellow";
                        //}

                        string Op = PlanScore.Tables[0].Rows[31]["Operator"].ToString().Trim();
                        string PlanValue = PlanScore.Tables[0].Rows[31]["PlanValue"].ToString().Trim();

                        if (Op != "" && PlanValue != "")
                        {
                            bool CmpResult = GetComparisionResult(Op, PlanValue, Value);
                            if (CmpResult == true)
                            {
                                color = "Lime";
                            }
                            else
                            {
                                color = "Yellow";
                            }
                        }
                        else
                            color = "White";
                    }
                    else
                        color = "White";
                }
                else if (kpi.Contains("Approved"))
                {
                    if (Value != "-" && Value != "n/a" && Value != "")
                    {
                        delValue = Convert.ToDouble(Value);
                        if (Value != "n/a")
                        {
                            color = "Yellow";
                        }
                    }
                    else
                        color = "White";
                }

                ////Rader/////
                else if (kpi.Contains("Contractor Services Audit - Self (Conducted)"))
                {
                    if (Value != "-" && Value != "n/a" && Value != "")
                    {
                        delValue = Convert.ToDouble(Value);
                        //if (delValue >= 100)
                        //{
                        //    color = "Lime";
                        //}
                        //else if (delValue > 80)
                        //{
                        //    color = "Yellow";
                        //}
                        //else
                        //{
                        //    color = "Red";
                        //}

                        string Op = PlanScore.Tables[0].Rows[32]["Operator"].ToString().Trim();
                        string PlanValue = PlanScore.Tables[0].Rows[32]["PlanValue"].ToString().Trim();

                        if (Op != "" && PlanValue != "")
                        {
                            bool CmpResult = GetComparisionResult(Op, PlanValue, Value);
                            if (CmpResult == true)
                            {
                                color = "Lime";
                            }
                            else
                            {

                                if (100 > delValue && delValue >= 80)
                                {
                                    color = "Yellow";
                                }
                                else
                                {
                                    color = "Red";
                                }
                            }
                        }
                        else
                            color = "White";
                    }
                    else
                        color = "White";
                }
                else if (kpi.Contains("Workplace Safety & Compliance"))
                {
                    if (Value != "-" && Value != "n/a" && Value != "")
                    {
                        delValue = Convert.ToDouble(Value);
                        //if (delValue >= 100)
                        //{
                        //    color = "Lime";
                        //}
                        //else if (delValue > 80)
                        //{
                        //    color = "Yellow";
                        //}
                        //else
                        //{
                        //    color = "Red";
                        //}

                        string Op = PlanScore.Tables[0].Rows[33]["Operator"].ToString().Trim();
                        string PlanValue = PlanScore.Tables[0].Rows[33]["PlanValue"].ToString().Trim();
                        if (Op != "" && PlanValue != "")
                        {
                            bool CmpResult = GetComparisionResult(Op, PlanValue, Value);
                            if (CmpResult == true)
                            {
                                color = "Lime";
                            }
                            else
                            {

                                if (100 > delValue && delValue >= 80)
                                {
                                    color = "Yellow";
                                }
                                else
                                {
                                    color = "Red";
                                }
                            }
                        }
                        else
                            color = "White";
                    }
                    else
                        color = "White";
                }
                //
                else if (kpi.Contains("Management Health Safety Work Contacts"))
                {
                    if (Value != "-" && Value != "n/a" && Value != "")
                    {
                        delValue = Convert.ToDouble(Value);
                        //if (delValue >= 100)
                        //{
                        //    color = "Lime";
                        //}
                        //else if (delValue > 80)
                        //{
                        //    color = "Yellow";
                        //}
                        //else
                        //{
                        //    color = "Red";
                        //}
                        string Op = PlanScore.Tables[0].Rows[34]["Operator"].ToString().Trim();
                        string PlanValue = PlanScore.Tables[0].Rows[34]["PlanValue"].ToString().Trim();

                        if (Op != "" && PlanValue != "")
                        {
                            bool CmpResult = GetComparisionResult(Op, PlanValue, Value);
                            if (CmpResult == true)
                            {
                                color = "Lime";
                            }
                            else
                            {

                                if (100 > delValue && delValue >= 80)
                                {
                                    color = "Yellow";
                                }
                                else
                                {
                                    color = "Red";
                                }
                            }
                        }
                        else
                            color = "White";
                    }
                    else
                        color = "White";
                }
                else if (kpi.Contains("Behavioural Observations"))
                {
                    if (Value != "-" && Value != "n/a" && Value != "")
                    {
                        delValue = Convert.ToDouble(Value);

                        //if (delValue >= 100)
                        //{
                        //    color = "Lime";
                        //}
                        //else if (delValue > 80)
                        //{
                        //    color = "Yellow";
                        //}
                        //else
                        //{
                        //    color = "Red";
                        //}

                        string Op = PlanScore.Tables[0].Rows[35]["Operator"].ToString().Trim();
                        string PlanValue = PlanScore.Tables[0].Rows[35]["PlanValue"].ToString().Trim();


                        if (Op != "" && PlanValue != "")
                        {
                            bool CmpResult = GetComparisionResult(Op, PlanValue, Value);
                            if (CmpResult == true)
                            {
                                color = "Lime";
                            }
                            else
                            {

                                if (100 > delValue && delValue >= 80)
                                {
                                    color = "Yellow";
                                }
                                else
                                {
                                    color = "Red";
                                }
                            }
                        }
                        else
                            color = "White";

                    }
                    else
                        color = "White";
                }
                //
                else if (kpi.Contains("Fatality Prevention Program"))
                {
                    if (Value != "-" && Value != "n/a" && Value != "")
                    {
                        delValue = Convert.ToDouble(Value);

                        //if (delValue >= 100)
                        //{
                        //    color = "Lime";
                        //}
                        //else if (delValue > 80)
                        //{
                        //    color = "Yellow";
                        //}
                        //else
                        //{
                        //    color = "Red";
                        //}

                        string Op = PlanScore.Tables[0].Rows[36]["Operator"].ToString().Trim();
                        string PlanValue = PlanScore.Tables[0].Rows[36]["PlanValue"].ToString().Trim();

                        if (Op != "" && PlanValue != "")
                        {
                            bool CmpResult = GetComparisionResult(Op, PlanValue, Value);
                            if (CmpResult == true)
                            {
                                color = "Lime";
                            }
                            else
                            {

                                if (100 > delValue && delValue >= 80)
                                {
                                    color = "Yellow";
                                }
                                else
                                {
                                    color = "Red";
                                }
                            }
                        }
                        else
                            color = "White";
                    }
                    else
                        color = "White";
                }
                else if (kpi.Contains("JSA Field Audit Verifications"))
                {
                    if (Value != "-" && Value != "n/a" && Value != "")
                    {
                        delValue = Convert.ToDouble(Value);

                        //if (delValue >= 100)
                        //{
                        //    color = "Lime";
                        //}
                        //else if (delValue > 80)
                        //{
                        //    color = "Yellow";
                        //}
                        //else
                        //{
                        //    color = "Red";
                        //}

                        string Op = PlanScore.Tables[0].Rows[37]["Operator"].ToString().Trim();
                        string PlanValue = PlanScore.Tables[0].Rows[37]["PlanValue"].ToString().Trim();

                        if (Op != "" && PlanValue != "")
                        {
                            bool CmpResult = GetComparisionResult(Op, PlanValue, Value);
                            if (CmpResult == true)
                            {
                                color = "Lime";
                            }
                            else
                            {

                                if (100 > delValue && delValue >= 80)
                                {
                                    color = "Yellow";
                                }
                                else
                                {
                                    color = "Red";
                                }
                            }
                        }
                        else
                            color = "White";
                    }
                    else
                        color = "White";
                }
                //
                else if (kpi.Contains("Toolbox Meetings"))
                {
                    if (Value != "-" && Value != "n/a" && Value != "")
                    {
                        delValue = Convert.ToDouble(Value);

                        //if (delValue >= 100)
                        //{
                        //    color = "Lime";
                        //}
                        //else if (delValue > 80)
                        //{
                        //    color = "Yellow";
                        //}
                        //else
                        //{
                        //    color = "Red";
                        //}

                        string Op = PlanScore.Tables[0].Rows[38]["Operator"].ToString().Trim();
                        string PlanValue = PlanScore.Tables[0].Rows[38]["PlanValue"].ToString().Trim();


                        if (Op != "" && PlanValue != "")
                        {
                            bool CmpResult = GetComparisionResult(Op, PlanValue, Value);
                            if (CmpResult == true)
                            {
                                color = "Lime";
                            }
                            else
                            {

                                if (100 > delValue && delValue >= 80)
                                {
                                    color = "Yellow";
                                }
                                else
                                {
                                    color = "Red";
                                }
                            }
                        }
                        else
                            color = "White";
                    }
                    else
                        color = "White";
                }
                else if (kpi.Contains("Alcoa Weekly Contractors Meeting"))
                {
                    if (Value != "-" && Value != "n/a" && Value != "")
                    {
                        delValue = Convert.ToDouble(Value);

                        //if (delValue >= 100)
                        //{
                        //    color = "Lime";
                        //}
                        //else if (delValue > 80)
                        //{
                        //    color = "Yellow";
                        //}
                        //else
                        //{
                        //    color = "Red";
                        //}

                        string Op = PlanScore.Tables[0].Rows[39]["Operator"].ToString().Trim();
                        string PlanValue = PlanScore.Tables[0].Rows[39]["PlanValue"].ToString().Trim();

                        if (Op != "" && PlanValue != "")
                        {
                            bool CmpResult = GetComparisionResult(Op, PlanValue, Value);
                            if (CmpResult == true)
                            {
                                color = "Lime";
                            }
                            else
                            {

                                if (100 > delValue && delValue >= 80)
                                {
                                    color = "Yellow";
                                }
                                else
                                {
                                    color = "Red";
                                }
                            }
                        }
                        else
                            color = "White";
                    }
                    else
                        color = "White";
                }
                //
                else if (kpi.Contains("Alcoa Monthly Contractors Meeting"))
                {
                    if (Value != "-" && Value != "n/a" && Value != "")
                    {
                        delValue = Convert.ToDouble(Value);

                        //if (delValue >= 100)
                        //{
                        //    color = "Lime";
                        //}
                        //else if (delValue > 80)
                        //{
                        //    color = "Yellow";
                        //}
                        //else
                        //{
                        //    color = "Red";
                        //}

                        string Op = PlanScore.Tables[0].Rows[40]["Operator"].ToString().Trim();
                        string PlanValue = PlanScore.Tables[0].Rows[40]["PlanValue"].ToString().Trim();

                        if (Op != "" && PlanValue != "")
                        {
                            bool CmpResult = GetComparisionResult(Op, PlanValue, Value);
                            if (CmpResult == true)
                            {
                                color = "Lime";
                            }
                            else
                            {

                                if (100 > delValue && delValue >= 80)
                                {
                                    color = "Yellow";
                                }
                                else
                                {
                                    color = "Red";
                                }
                            }
                        }
                        else
                            color = "White";
                    }
                    else
                        color = "White";
                }
                else if (kpi.Contains("Safety Plan(s) Submitted"))
                {
                    if (Value != "-" && Value != "n/a" && Value != "")
                    {
                        delValue = Convert.ToDouble(Value);

                        //if (delValue >= 100)
                        //{
                        //    color = "Lime";
                        //}
                        //else if (delValue > 80)
                        //{
                        //    color = "Yellow";
                        //}
                        //else
                        //{
                        //    color = "Red";
                        //}

                        string Op = PlanScore.Tables[0].Rows[41]["Operator"].ToString().Trim();
                        string PlanValue = PlanScore.Tables[0].Rows[41]["PlanValue"].ToString().Trim();

                        if (Op != "" && PlanValue != "")
                        {
                            bool CmpResult = GetComparisionResult(Op, PlanValue, Value);
                            if (CmpResult == true)
                            {
                                color = "Lime";
                            }
                            else
                            {

                                if (100 > delValue && delValue >= 80)
                                {
                                    color = "Yellow";
                                }
                                else
                                {
                                    color = "Red";
                                }
                            }
                        }
                        else
                            color = "White";
                    }

                    else
                        color = "White";

                }
                //
                else if (kpi.Contains("% Mandated Training"))
                {
                    if (Value != "-" && Value != "n/a" && Value != "")
                    {
                        delValue = Convert.ToDouble(Value);

                        //if (delValue >= 100)
                        //{
                        //    color = "Lime";
                        //}
                        //else if (delValue > 80)
                        //{
                        //    color = "Yellow";
                        //}
                        //else
                        //{
                        //    color = "Red";
                        //}

                        string Op = PlanScore.Tables[0].Rows[42]["Operator"].ToString().Trim();
                        string PlanValue = PlanScore.Tables[0].Rows[42]["PlanValue"].ToString().Trim();

                        if (Op != "" && PlanValue != "")
                        {
                            bool CmpResult = GetComparisionResult(Op, PlanValue, Value);
                            if (CmpResult == true)
                            {
                                color = "Lime";
                            }
                            else
                            {

                                if (100 > delValue && delValue >= 80)
                                {
                                    color = "Yellow";
                                }
                                else
                                {
                                    color = "Red";
                                }
                            }
                        }
                        else
                            color = "White";
                    }
                    else
                        color = "White";
                }
                else if (kpi.Contains("Embedded-Actual completed (Cumulative total)"))
                {
                    if (Value != "-" && Value != "n/a" && Value != "")
                    {
                        decimal cmpVal = Convert.ToDecimal(Value);
                        decimal cmpPrevVal = 0;
                        if (dtYtdSummary.Rows.Count > 0)
                        {
                            if (dtYtdSummary.Rows[59][siteName] != null && dtYtdSummary.Rows[59][siteName] != "" && dtYtdSummary.Rows[59][siteName] != "n/a")
                                cmpPrevVal = Convert.ToDecimal(dtYtdSummary.Rows[59][siteName]);
                            if (cmpVal >= cmpPrevVal)
                            {
                                color = "Lime";
                            }
                            else
                            {
                                color = "Red";
                            }
                        }


                    }

                    //    string Op = PlanScore.Tables[0].Rows[44]["Operator"].ToString().Trim();
                    //    string PlanValue = PlanScore.Tables[0].Rows[44]["PlanValue"].ToString().Trim();


                    //    if (Op != "" && PlanValue != "")
                    //    {
                    //        bool CmpResult = GetComparisionResult(Op, PlanValue, Value);
                    //        if (CmpResult == true)
                    //        {
                    //            color = "Lime";
                    //        }
                    //        else
                    //        {
                    //            color = "Red";
                    //        }
                    //    }
                    //    else
                    //        color = "White";
                    //}
                    //else
                    //    color = "White";

                }

                else if (kpi.Contains("Non Embedded 1-Actual completed (Cumulative total)"))
                {
                    if (Value != "-" && Value != "n/a" && Value != "")
                    {
                        decimal cmpVal = Convert.ToDecimal(Value);
                        decimal cmpPrevVal = 0;
                        if (dtYtdSummary.Rows.Count > 0)
                        {
                            if (dtYtdSummary.Rows[62][siteName] != null && dtYtdSummary.Rows[62][siteName] != "" && dtYtdSummary.Rows[62][siteName] != "n/a")
                                cmpPrevVal = Convert.ToDecimal(dtYtdSummary.Rows[62][siteName]);
                            if (cmpVal >= cmpPrevVal)
                            //if (cmpVal > (decimal)Session["NEReqToDate"])
                            {
                                color = "Lime";
                            }
                            else
                            {
                                color = "Red";
                            }
                        }



                    }
                }

                else if (kpi.Contains("Number of Companies"))
                {

                    color = "White";
                }
                else if (kpi.Contains("Required"))
                {

                    color = "White";
                }
                else if (kpi.Contains("Total required"))
                {
                    color = "White";
                }
            }
                return color;
            
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
       
    }

    #endregion

    #region Site-wise Column Range

    private void GetStartAndEndColumnIndexBySiteId(int siteId, out int startColumnIndex, out int endColumnIndex)
    {
        switch (siteId)
        {
            case -1:
                {
                    startColumnIndex = 6;
                    endColumnIndex = 15;
                    break;
                }
            case -2:
                {
                    startColumnIndex = 5;
                    endColumnIndex = 22;
                    break;
                }
            case -3:
                {
                    startColumnIndex = 11;
                    endColumnIndex = 13;
                    break;
                }
            case -4:
                {
                    startColumnIndex = 16;
                    endColumnIndex = 19;
                    break;
                }
            case -7:
                {
                    startColumnIndex = 20;
                    endColumnIndex = 22;
                    break;
                }
            //Added by Pankaj Dikshit for AUS GPP
            case -8:
                {
                    startColumnIndex = 5;
                    endColumnIndex = 19;
                    break;
                }
            case 13:
                {
                    startColumnIndex = 17;
                    endColumnIndex = 17;
                    break;
                }
            case 25:
                {
                    startColumnIndex = 21;
                    endColumnIndex = 21;
                    break;
                }
            case 8:
                {
                    startColumnIndex = 14;
                    endColumnIndex = 14;
                    break;
                }
            case 7:
                {
                    startColumnIndex = 10;
                    endColumnIndex = 10;
                    break;
                }
            case 6:
                {
                    startColumnIndex = 13;
                    endColumnIndex = 13;
                    break;
                }
            case 4:
                {
                    startColumnIndex = 11;
                    endColumnIndex = 11;
                    break;
                }
            case 1:
                {
                    startColumnIndex = 7;
                    endColumnIndex = 7;
                    break;
                }
            case 18:
                {
                    startColumnIndex = 15;
                    endColumnIndex = 15;
                    break;
                }
            case 2:
                {
                    startColumnIndex = 8;
                    endColumnIndex =8;
                    break;
                }
            case 14:
                {
                    startColumnIndex = 18;
                    endColumnIndex = 18;
                    break;
                }
            case 15:
                {
                    startColumnIndex = 19;
                    endColumnIndex = 19;
                    break;
                }
            case 3:
                {
                    startColumnIndex = 9;
                    endColumnIndex = 9;
                    break;
                }
            case 5:
                {
                    startColumnIndex = 12;
                    endColumnIndex = 12;
                    break;
                }
            case 24:
                {
                    startColumnIndex = 22;
                    endColumnIndex = 22;
                    break;
                }
            default:
                {
                    startColumnIndex = 1;
                    endColumnIndex = 3;
                    break;
                }
        }

    }

    //private void GetStartAndEndColumnIndexBySiteId(int siteId, out int startColumnIndex, out int endColumnIndex, out int startChildColumnIndex, out int endChildColumnIndex)
    //{
    //    switch (siteId)
    //    {
    //        case -1:
    //            {
    //                startColumnIndex = 5;
    //                endColumnIndex = 12;
    //                startChildColumnIndex = 1;
    //                endChildColumnIndex = 3;
    //                break;
    //            }
    //        case -2:
    //            {
    //                startColumnIndex = 4;
    //                endColumnIndex = 19;
    //                startChildColumnIndex = 1;
    //                endChildColumnIndex = 3;
    //                break;
    //            }
    //        case -3:
    //            {
    //                startColumnIndex = 10;
    //                endColumnIndex = 10;
    //                startChildColumnIndex = 1;
    //                endChildColumnIndex = 3;
    //                break;
    //            }
    //        case -4:
    //            {
    //                startColumnIndex = 13;
    //                endColumnIndex = 16;
    //                startChildColumnIndex = 0;
    //                endChildColumnIndex = 0;
    //                break;
    //            }
    //        case -7:
    //            {
    //                startColumnIndex = 17;
    //                endColumnIndex = 19;
    //                startChildColumnIndex = 0;
    //                endChildColumnIndex = 0;
    //                break;
    //            }
    //        //Added by Pankaj Dikshit for AUS GPP
    //        case -8:
    //            {
    //                startColumnIndex = 4;
    //                endColumnIndex = 16;
    //                startChildColumnIndex = 1;
    //                endChildColumnIndex = 3;
    //                break;
    //            }
    //        case 13:
    //            {
    //                startColumnIndex = 14;
    //                endColumnIndex = 14;
    //                startChildColumnIndex = 0;
    //                endChildColumnIndex = 0;
    //                break;
    //            }
    //        case 25:
    //            {
    //                startColumnIndex = 18;
    //                endColumnIndex = 18;
    //                startChildColumnIndex = 0;
    //                endChildColumnIndex = 0;
    //                break;
    //            }
    //        case 8:
    //            {
    //                startColumnIndex = 11;
    //                endColumnIndex = 11;
    //                startChildColumnIndex = 0;
    //                endChildColumnIndex = 0;
    //                break;
    //            }
    //        case 7:
    //            {
    //                startColumnIndex = 9;
    //                endColumnIndex = 9;
    //                startChildColumnIndex = 0;
    //                endChildColumnIndex = 0;
    //                break;
    //            }
    //        case 6:
    //            {
    //                startColumnIndex = 10;
    //                endColumnIndex = 10;
    //                startChildColumnIndex = 3;
    //                endChildColumnIndex = 3;
    //                break;
    //            }
    //        case 4:
    //            {
    //                startColumnIndex = 10;
    //                endColumnIndex = 10;
    //                startChildColumnIndex = 1;
    //                endChildColumnIndex = 1;
    //                break;
    //            }
    //        case 1:
    //            {
    //                startColumnIndex = 6;
    //                endColumnIndex = 6;
    //                startChildColumnIndex = 0;
    //                endChildColumnIndex = 0;
    //                break;
    //            }
    //        case 18:
    //            {
    //                startColumnIndex = 12;
    //                endColumnIndex = 12;
    //                startChildColumnIndex = 0;
    //                endChildColumnIndex = 0;
    //                break;
    //            }
    //        case 2:
    //            {
    //                startColumnIndex = 7;
    //                endColumnIndex = 7;
    //                startChildColumnIndex = 0;
    //                endChildColumnIndex = 0;
    //                break;
    //            }
    //        case 14:
    //            {
    //                startColumnIndex = 15;
    //                endColumnIndex = 15;
    //                startChildColumnIndex = 0;
    //                endChildColumnIndex = 0;
    //                break;
    //            }
    //        case 15:
    //            {
    //                startColumnIndex = 16;
    //                endColumnIndex = 16;
    //                startChildColumnIndex = 0;
    //                endChildColumnIndex = 0;
    //                break;
    //            }
    //        case 3:
    //            {
    //                startColumnIndex = 8;
    //                endColumnIndex = 8;
    //                startChildColumnIndex = 0;
    //                endChildColumnIndex = 0;
    //                break;
    //            }
    //        case 5:
    //            {
    //                startColumnIndex = 10;
    //                endColumnIndex = 10;
    //                startChildColumnIndex = 2;
    //                endChildColumnIndex = 2;
    //                break;
    //            }
    //        case 24:
    //            {
    //                startColumnIndex = 19;
    //                endColumnIndex = 19;
    //                startChildColumnIndex = 0;
    //                endChildColumnIndex = 0;
    //                break;
    //            }
    //        default:
    //            {
    //                startColumnIndex = 0;
    //                endColumnIndex = 2;
    //                startChildColumnIndex = 0;
    //                endChildColumnIndex = 0;
    //                break;
    //            }
    //    }

    //}

    #endregion

    protected void grid_Dash_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {
        if (e.RowType != DevExpress.Web.ASPxGridView.GridViewRowType.Data)
            return;

        try
        {
            if (cbRegionSite.SelectedItem.Text == "----------------")
            {
                throw new Exception("Please Select All Search Fields");
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }

        string kpiValue = (string)grid_Dash.GetRowValues(e.VisibleIndex, "FieldName");

        if ((e.VisibleIndex >= 0 && e.VisibleIndex <= 11) || (e.VisibleIndex > 23 && e.VisibleIndex <= 30) || (e.VisibleIndex >= 36 && e.VisibleIndex <= 57) || e.VisibleIndex == 64)
        {
            e.Row.Cells[2].Text = Convert.ToString(grid_Dash.GetRowValues(e.VisibleIndex, grid_Dash.Columns[3].Name));
            e.Row.Cells[2].ColumnSpan = 2;
            e.Row.Cells[3].Visible = false;
        }
        else if (e.VisibleIndex == 12)
        {
            e.Row.Cells[2].Text = "Procurement";
            e.Row.Cells[2].RowSpan = 7;
            e.Row.Cells[2].HorizontalAlign = HorizontalAlign.Center;
        }
        else if (e.VisibleIndex == 19)
        {
            e.Row.Cells[2].Text = "CSMS Admin";
            e.Row.Cells[2].HorizontalAlign = HorizontalAlign.Center;
        }
        else if (e.VisibleIndex == 20)
        {
            e.Row.Cells[2].Text = "H&S Assessor";
            e.Row.Cells[2].RowSpan = 3;
            e.Row.Cells[2].HorizontalAlign = HorizontalAlign.Center;
        }
        else if (e.VisibleIndex == 23)
        {
            e.Row.Cells[2].Text = Convert.ToString(grid_Dash.GetRowValues(e.VisibleIndex, grid_Dash.Columns[3].Name));
            e.Row.Cells[2].ColumnSpan = 2;
            e.Row.Cells[3].Visible = false;
            e.Row.Cells[2].HorizontalAlign = HorizontalAlign.Right;
        }
        else if (e.VisibleIndex == 31)
        {
            e.Row.Cells[2].Text = "HS Assessors";
            e.Row.Cells[2].RowSpan = 5;
            e.Row.Cells[2].HorizontalAlign = HorizontalAlign.Center;
        }
        else if (e.VisibleIndex == 58)
        {
            e.Row.Cells[2].Text = "Embedded";
            e.Row.Cells[2].RowSpan = 3;
            e.Row.Cells[2].HorizontalAlign = HorizontalAlign.Center;
        }
        else if (e.VisibleIndex == 61)
        {
            e.Row.Cells[2].Text = "Non Embedded 1";
            e.Row.Cells[2].RowSpan = 3;
            e.Row.Cells[2].HorizontalAlign = HorizontalAlign.Center;
        }
        else e.Row.Cells[2].Visible = false;

        if (e.VisibleIndex >= 12 && e.VisibleIndex <= 18)
            e.Row.Cells[3].Width = 200;

        for (int j = 0; j < 2; j++)
        {
            string Text = Convert.ToString(grid_Dash.GetRowValues(e.VisibleIndex, grid_Dash.Columns[j].Name));
            if (Text == "SAFETY Lagging Indicators - Compliance")
                e.Row.Cells[j].RowSpan = 36;
            if (Text == "SAFETY Leading Indicators - Compliance")
                e.Row.Cells[j].RowSpan = 29;
            if (Text == "Rates (YTD)")
                e.Row.Cells[j].RowSpan = 4;
            else if (Text == "Counts (YTD)")
                e.Row.Cells[j].RowSpan = 8;
            else if (Text == "Safety Qualification process")
                e.Row.Cells[j].RowSpan = 19;
            else if (Text == "Safety Management Plans ( E / NE1 )")
                e.Row.Cells[j].RowSpan = 5;
            else if (Text == "Embedded compliance (only) % of companies 100% compliant")
                e.Row.Cells[j].RowSpan = 12;
            else if (Text == "Non-Embedded 1 compliance (only) % of companies 100% compliant")
                e.Row.Cells[j].RowSpan = 10;
            else if (Text == "Alcoa Contractor Services Audits")
                e.Row.Cells[j].RowSpan = 6;

            else if (Text == "")
                e.Row.Cells[j].Visible = false;
        }


        int startColIndex = 0;
        int endColIndex = 0;
        int colHeaderCount = 0;

        GetStartAndEndColumnIndexBySiteId(Convert.ToInt32(cbRegionSite.SelectedItem.Value), out startColIndex, out endColIndex);
        
        int totColCount = (endColIndex - startColIndex) + 5;
        colHeaderCount = startColIndex - 1;
        for (int i = 5; i <= totColCount; i++)
        {
            string color = "";
            string Value = string.Empty;
            colHeaderCount++;

            if (colHeaderCount >= 23)
                break;

            //if (colHeaderCount > 12)
            //{
            //    colHeaderCount = colHeaderCount - 2;
            //}
            if (colHeaderCount <= 10)
            {
                Value = Convert.ToString(grid_Dash.GetRowValues(e.VisibleIndex, grid_Dash.Columns[colHeaderCount].Name));
                color = GetColorByValue(kpiValue, Value, grid_Dash.Columns[colHeaderCount].Name);
            }
            else if (colHeaderCount >= 11 && colHeaderCount <= 13)
            {
                GridViewBandColumn db = new GridViewBandColumn();
                db = (GridViewBandColumn)grid_Dash.Columns[11];
                switch (colHeaderCount)
                {
                    case 11:
                        {
                            Value = Convert.ToString(grid_Dash.GetRowValues(e.VisibleIndex, db.Columns[0].Name));
                            color = GetColorByValue(kpiValue, Value, db.Columns[0].Name);
                            break;
                        }
                    case 12:
                        {
                            Value = Convert.ToString(grid_Dash.GetRowValues(e.VisibleIndex, db.Columns[1].Name));
                            color = GetColorByValue(kpiValue, Value, db.Columns[1].Name);
                            break;
                        }
                    case 13:
                        {
                            Value = Convert.ToString(grid_Dash.GetRowValues(e.VisibleIndex, db.Columns[2].Name));
                            color = GetColorByValue(kpiValue, Value, db.Columns[2].Name);
                            break;
                        }
                }
            }
            else
            {
                Value = Convert.ToString(grid_Dash.GetRowValues(e.VisibleIndex, grid_Dash.Columns[colHeaderCount-2].Name));
                color = GetColorByValue(kpiValue, Value, grid_Dash.Columns[colHeaderCount-2].Name);
            }

            ////////
            if (!String.IsNullOrEmpty(color))
            {
                try
                {
                    e.Row.Cells[i].Style.Add("background-color", color);
                }
                catch (Exception ex)
                {
                    return;
                }
                //e.Row.Cells[i].Style.Add("border-color", "black");
                //e.Row.Cells[i].Style.Add("Width", "10px");

                if (kpiValue != "Lost Work Days Frequency Rate   LWDFR"
                    && kpiValue != "Total Recordable Injuries Frequency Rate    TRIFR"
                    && kpiValue != "All Injury Frequency Rate   AIFR"
                    && kpiValue != "Days Away, Restricted or Transferred Rate   DART"
                    && kpiValue != "Required (# Companies)"
                    && kpiValue != "Being Assessed (# Companies)"
                    && kpiValue != "Awaiting Assignment (# Companies)"
                    && kpiValue != "Approved %"
                    && kpiValue != "Approved"
                   )
                {
                    if (color == "Red" || color == "Yellow")
                    {
                        e.Row.Cells[i].ToolTip = "Click to see Non-Compliant Companies";
                        //e.Row.Cells[i].Attributes.Add("onClick", "javascript:popUp('PopUps/NonCompliantCompanies_Dashboard.aspx?Help=Default');");
                        int Dash_Year = 0;
                        int Dash_Month = 0;
                        if (ViewState["Dash_Year"] != null)
                        {
                            Dash_Year = Convert.ToInt32(ViewState["Dash_Year"]);
                        }
                        else
                        {
                            Dash_Year = Convert.ToInt32(ddlYear.SelectedItem.Value);
                        }
                        if (ViewState["Dash_Year"] != null)
                        {
                            Dash_Month = Convert.ToInt32(ViewState["Dash_Month"]);
                        }
                        else
                        {
                            Dash_Month = Convert.ToInt32(ddlMonth.SelectedItem.Value);
                        }

                        string strUrl = string.Empty;

                        if (colHeaderCount < 11)
                        {
                            strUrl = string.Format("PopUps/NonCompliantCompanies_Dashboard.aspx?kpi={0}&site={1}&Dash_Year={2}&Dash_Month={3}", e.VisibleIndex, GetSite(grid_Dash.Columns[colHeaderCount].Name), Dash_Year, Dash_Month);
                        }
                        else if (colHeaderCount >= 11 && colHeaderCount <= 13)
                        {
                            GridViewBandColumn db = new GridViewBandColumn();
                            db = (GridViewBandColumn)grid_Dash.Columns[11];
                            switch (colHeaderCount)
                            {
                                case 11:
                                    {
                                        strUrl = string.Format("PopUps/NonCompliantCompanies_Dashboard.aspx?kpi={0}&site={1}&Dash_Year={2}&Dash_Month={3}", e.VisibleIndex, GetSite(db.Columns[0].Name), Dash_Year, Dash_Month);
                                        break;
                                    }
                                case 12:
                                    {
                                        strUrl = string.Format("PopUps/NonCompliantCompanies_Dashboard.aspx?kpi={0}&site={1}&Dash_Year={2}&Dash_Month={3}", e.VisibleIndex, GetSite(db.Columns[1].Name), Dash_Year, Dash_Month);
                                        
                                        break;
                                    }
                                case 13:
                                    {
                                        strUrl = string.Format("PopUps/NonCompliantCompanies_Dashboard.aspx?kpi={0}&site={1}&Dash_Year={2}&Dash_Month={3}", e.VisibleIndex, GetSite(db.Columns[2].Name), Dash_Year, Dash_Month);
                                        
                                        break;
                                    }
                            }
                        }
                        else
                        {
                            strUrl = string.Format("PopUps/NonCompliantCompanies_Dashboard.aspx?kpi={0}&site={1}&Dash_Year={2}&Dash_Month={3}", e.VisibleIndex, GetSite(grid_Dash.Columns[colHeaderCount-2].Name), Dash_Year, Dash_Month);
                            
                        }
                        e.Row.Cells[i].Attributes.Add("onClick", String.Format("javascript:popUp('" + strUrl + "')"));
                        
                    }
                    else if (color == "Orange")
                    {
                        e.Row.Cells[i].ToolTip = "Electronic Security available but not linked to CSMS";
                    }
                }
            }

            //Added By Bishwajit to show values with %
            string sdValue = Convert.ToString(grid_Dash.GetRowValues(e.VisibleIndex, "Plan"));
            if (sdValue.Contains("%"))
            {
                double dOutput = 0;
                int iOutput = 0;
                if (Double.TryParse(Value, out dOutput))
                {
                    Double doubleVal = Convert.ToDouble(Value);
                    int intVal = Convert.ToInt32(doubleVal);
                    e.Row.Cells[i].Text = intVal.ToString() + "%";
                }
                else if (int.TryParse(Value, out iOutput))
                {
                    e.Row.Cells[i].Text = Value + "%";
                }

            }
            //END Added By Bishwajit to show values with %


        }
    }

    //protected void grid_Dash_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    //{
    //    if (e.RowType != DevExpress.Web.ASPxGridView.GridViewRowType.Data)
    //        return;

    //    try
    //    {
    //        if (cbRegionSite.SelectedItem.Text == "----------------")
    //        {
    //            throw new Exception("Please Select All Search Fields");
    //        }
    //    }
    //    catch (Exception ex)
    //    {
    //        throw new Exception(ex.Message);
    //    }

    //    string kpiValue = (string)grid_Dash.GetRowValues(e.VisibleIndex, "FieldName");

    //    for (int j = 0; j < 2; j++)
    //    {
    //        string Text = Convert.ToString(grid_Dash.GetRowValues(e.VisibleIndex, grid_Dash.Columns[j].Name));
    //        if (Text == "SAFETY Lagging Indicators - Compliance")
    //            e.Row.Cells[j].RowSpan = 36;
    //        if (Text == "SAFETY Leading Indicators - Compliance")
    //            e.Row.Cells[j].RowSpan = 29;
    //        if (Text == "Rates (YTD)")
    //            e.Row.Cells[j].RowSpan = 4;
    //        else if (Text == "Counts (YTD)")
    //            e.Row.Cells[j].RowSpan = 8;
    //        else if (Text == "Safety Qualification process")
    //            e.Row.Cells[j].RowSpan = 19;
    //        else if (Text == "Safety Management Plans ( E / NE1 )")
    //            e.Row.Cells[j].RowSpan = 5;
    //        else if (Text == "Embedded compliance (only) % of companies 100% compliant")
    //            e.Row.Cells[j].RowSpan = 12;
    //        else if (Text == "Non-Embedded 1 compliance (only) % of companies 100% compliant")
    //            e.Row.Cells[j].RowSpan = 10;
    //        else if (Text == "Alcoa Contractor Services Audits")
    //            e.Row.Cells[j].RowSpan = 6;

    //        else if (Text == "")
    //            e.Row.Cells[j].Visible = false;
    //    }
        
      
    //    int startColIndex = 0;
    //    int endColIndex = 0;
    //    int colHeaderCount = 0;
    //    int startChildColumnIndex = 0;
    //    int endChildColumnIndex = 0;
    //    int colChildHeaderCount = 0;

    //    GetStartAndEndColumnIndexBySiteId(Convert.ToInt32(cbRegionSite.SelectedItem.Value), out startColIndex, out endColIndex, out startChildColumnIndex, out endChildColumnIndex);
    //    int totColCount = (endColIndex - startColIndex) + 4;
    //    colHeaderCount = startColIndex - 1;
    //    for (int i = 4; i <= totColCount; i++)
    //    {
    //        string color = "";
    //        colHeaderCount++;

    //        if (colHeaderCount >= 20)
    //            break;
    //        string Value = string.Empty;
    //        if (i == 10)
    //        {
    //            string HeaderName = string.Empty;
    //            colChildHeaderCount = startChildColumnIndex - 1;

    //            GridViewBandColumn db = new GridViewBandColumn();
    //            db = (GridViewBandColumn)grid_Dash.Columns[10];
                
    //            for (int k = startChildColumnIndex - 1; k < endChildColumnIndex; k++)
    //            {
    //                HeaderName = db.Columns[k].Name;

    //                Value = Convert.ToString(grid_Dash.GetRowValues(e.VisibleIndex, HeaderName));
    //                color = GetColorByValue(kpiValue, Value, HeaderName);
    //                ////////
    //                if (!String.IsNullOrEmpty(color))
    //                {
    //                    try
    //                    {
    //                        e.Row.Cells[k].Style.Add("background-color", color);
    //                    }
    //                    catch (Exception ex)
    //                    {
    //                        return;
    //                    }
    //                    //e.Row.Cells[i].Style.Add("border-color", "black");
    //                    //e.Row.Cells[i].Style.Add("Width", "10px");

    //                    if (kpiValue != "Lost Work Days Frequency Rate   LWDFR"
    //                        && kpiValue != "Total Recordable Injuries Frequency Rate    TRIFR"
    //                        && kpiValue != "All Injury Frequency Rate   AIFR"
    //                        && kpiValue != "Days Away, Restricted or Transferred Rate   DART"
    //                        && kpiValue != "Required (# Companies)"
    //                        && kpiValue != "Being Assessed (# Companies)"
    //                        && kpiValue != "Awaiting Assignment (# Companies)"
    //                        && kpiValue != "Approved %"
    //                        && kpiValue != "Approved"
    //                       )
    //                    {
    //                        if (color == "Red" || color == "Yellow")
    //                        {
    //                            e.Row.Cells[i].ToolTip = "Click to see Non-Compliant Companies";
    //                            //e.Row.Cells[i].Attributes.Add("onClick", "javascript:popUp('PopUps/NonCompliantCompanies_Dashboard.aspx?Help=Default');");
    //                            int Dash_Year = 0;
    //                            int Dash_Month = 0;
    //                            if (ViewState["Dash_Year"] != null)
    //                            {
    //                                Dash_Year = Convert.ToInt32(ViewState["Dash_Year"]);
    //                            }
    //                            else
    //                            {
    //                                Dash_Year = Convert.ToInt32(ddlYear.SelectedItem.Value);
    //                            }
    //                            if (ViewState["Dash_Year"] != null)
    //                            {
    //                                Dash_Month = Convert.ToInt32(ViewState["Dash_Month"]);
    //                            }
    //                            else
    //                            {
    //                                Dash_Month = Convert.ToInt32(ddlMonth.SelectedItem.Value);
    //                            }

    //                            string strUrl = string.Format("PopUps/NonCompliantCompanies_Dashboard.aspx?kpi={0}&site={1}&Dash_Year={2}&Dash_Month={3}", e.VisibleIndex, GetSite(grid_Dash.Columns[colHeaderCount].Name), Dash_Year, Dash_Month);
    //                            e.Row.Cells[k].Attributes.Add("onClick", String.Format("javascript:popUp('" + strUrl + "')"));
    //                        }
    //                        else if (color == "Orange")
    //                        {
    //                            e.Row.Cells[k].ToolTip = "Electronic Security available but not linked to CSMS";
    //                        }
    //                    }
    //                }

    //                //Added By Bishwajit to show values with %
    //                string sdValue = Convert.ToString(grid_Dash.GetRowValues(e.VisibleIndex, "Plan"));
    //                if (sdValue.Contains("%"))
    //                {
    //                    double dOutput = 0;
    //                    int iOutput = 0;
    //                    if (Double.TryParse(Value, out dOutput))
    //                    {
    //                        Double doubleVal = Convert.ToDouble(Value);
    //                        int intVal = Convert.ToInt32(doubleVal);
    //                        e.Row.Cells[k].Text = intVal.ToString() + "%";
                            
    //                        //IEnumerator cl = db.Columns[k].Collection.GetEnumerator();

    //                        //for (int n = 0; n < cl.; n++)
    //                        //{
    //                        //    cl[n] = "";
    //                        //}
    //                    }
    //                    else if (int.TryParse(Value, out iOutput))
    //                    {
    //                        e.Row.Cells[k].Text = Value + "%";
    //                    }

    //                }
    //                //END Added By Bishwajit to show values with %
    //            }
    //        }
    //        else
    //        {
    //            Value = Convert.ToString(grid_Dash.GetRowValues(e.VisibleIndex, grid_Dash.Columns[colHeaderCount].Name));

    //            color = GetColorByValue(kpiValue, Value, grid_Dash.Columns[colHeaderCount].Name);
    //            ////////
    //            if (!String.IsNullOrEmpty(color))
    //            {
    //                try
    //                {
    //                    e.Row.Cells[i].Style.Add("background-color", color);
    //                }
    //                catch (Exception ex)
    //                {
    //                    return;
    //                }
    //                //e.Row.Cells[i].Style.Add("border-color", "black");
    //                //e.Row.Cells[i].Style.Add("Width", "10px");

    //                if (kpiValue != "Lost Work Days Frequency Rate   LWDFR"
    //                    && kpiValue != "Total Recordable Injuries Frequency Rate    TRIFR"
    //                    && kpiValue != "All Injury Frequency Rate   AIFR"
    //                    && kpiValue != "Days Away, Restricted or Transferred Rate   DART"
    //                    && kpiValue != "Required (# Companies)"
    //                    && kpiValue != "Being Assessed (# Companies)"
    //                    && kpiValue != "Awaiting Assignment (# Companies)"
    //                    && kpiValue != "Approved %"
    //                    && kpiValue != "Approved"
    //                   )
    //                {
    //                    if (color == "Red" || color == "Yellow")
    //                    {
    //                        e.Row.Cells[i].ToolTip = "Click to see Non-Compliant Companies";
    //                        //e.Row.Cells[i].Attributes.Add("onClick", "javascript:popUp('PopUps/NonCompliantCompanies_Dashboard.aspx?Help=Default');");
    //                        int Dash_Year = 0;
    //                        int Dash_Month = 0;
    //                        if (ViewState["Dash_Year"] != null)
    //                        {
    //                            Dash_Year = Convert.ToInt32(ViewState["Dash_Year"]);
    //                        }
    //                        else
    //                        {
    //                            Dash_Year = Convert.ToInt32(ddlYear.SelectedItem.Value);
    //                        }
    //                        if (ViewState["Dash_Year"] != null)
    //                        {
    //                            Dash_Month = Convert.ToInt32(ViewState["Dash_Month"]);
    //                        }
    //                        else
    //                        {
    //                            Dash_Month = Convert.ToInt32(ddlMonth.SelectedItem.Value);
    //                        }

    //                        string strUrl = string.Format("PopUps/NonCompliantCompanies_Dashboard.aspx?kpi={0}&site={1}&Dash_Year={2}&Dash_Month={3}", e.VisibleIndex, GetSite(grid_Dash.Columns[colHeaderCount].Name), Dash_Year, Dash_Month);
    //                        e.Row.Cells[i].Attributes.Add("onClick", String.Format("javascript:popUp('" + strUrl + "')"));
    //                    }
    //                    else if (color == "Orange")
    //                    {
    //                        e.Row.Cells[i].ToolTip = "Electronic Security available but not linked to CSMS";
    //                    }
    //                }
    //            }

    //            //Added By Bishwajit to show values with %
    //            string sdValue = Convert.ToString(grid_Dash.GetRowValues(e.VisibleIndex, "Plan"));
    //            if (sdValue.Contains("%"))
    //            {
    //                double dOutput = 0;
    //                int iOutput = 0;
    //                if (Double.TryParse(Value, out dOutput))
    //                {
    //                    Double doubleVal = Convert.ToDouble(Value);
    //                    int intVal = Convert.ToInt32(doubleVal);
    //                    e.Row.Cells[i].Text = intVal.ToString() + "%";
    //                }
    //                else if (int.TryParse(Value, out iOutput))
    //                {
    //                    e.Row.Cells[i].Text = Value + "%";
    //                }

    //            }
    //            //END Added By Bishwajit to show values with %
    //        }

 


    //    }
    //}


    public int GetSite(string col)
    {
        int SiteId;
        switch (col)
        {
            case "AUS GPP": SiteId = -8; //Changed by Pankaj for AUS GPP
                break;
            case "WAO": SiteId = -1;
                break;
            case "VIC OPS": SiteId = -4;
                break;
            case "ARP": SiteId = -7;
                break;
            case "Kwinana": SiteId = 1;
                break;
            case "Pinjara": SiteId = 2;
                break;
            case "Wagerup": SiteId = 3;
                break;
            case "Huntly (Mine)": SiteId = 4;
                break;
            case "Willowdale (Mine)": SiteId = 5;
                break;
            case "FML": SiteId = 6;
                break;
            case "Bunbury Port": SiteId = 7;
                break;
            case "BGN": SiteId = 8;
                break;
            case "Anglesea": SiteId = 13;
                break;
            case "Point Henry": SiteId = 14;
                break;
            case "Portland": SiteId = 15;
                break;
            case "Peel": SiteId = 18;
                break;
            case "Yennora": SiteId = 24;
                break;
            case "ARP Point Henry": SiteId = 25;
                break;

            default: SiteId = 100;
                break;
        }



        return SiteId;

    }

    protected void ddlYear_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToInt32(ddlYear.SelectedItem.Value) == 2012)
        {
            ddlMonth.Value = 12;
            ddlMonth.Enabled = false;
        }
        else
        {
            ddlMonth.Enabled = true;
        }
    }

    protected void ddlYear_Callback(object source, CallbackEventArgsBase e)
    {
        if (Convert.ToInt32(ddlYear.SelectedItem.Value) == 2012)
        {
            ddlMonth.Value = 12;
            ddlMonth.Enabled = false;
        }
        else
        {
            ddlMonth.Enabled = true;
        }
    }

    private DataTable ReturnDataTable(int month, int year)
    {
        DataTable dt = new DataTable();
        try
        {
            string sqldb = ConfigurationManager.ConnectionStrings["ALCOA_WAO_CSMWP_devConnectionString"].ConnectionString;

            using (SqlConnection cnDel = new SqlConnection(sqldb))
            {
                cnDel.Open();
                //Console.WriteLine("..Creating table: WorkFormPlanSummary" + "...");
                SqlCommand cmdDel = new SqlCommand("select * from DashBoard_SafetyQualificationProcess where [Month]=" + month + " and [Year]= " + year, cnDel);
                cmdDel.CommandTimeout = 7000;
                cmdDel.CommandType = CommandType.Text;
                SqlDataAdapter sqlda = new SqlDataAdapter(cmdDel);
                sqlda.Fill(dt);
                cnDel.Close();
            }
        }
        catch (Exception ex)
        {

        }
        return dt;
    }

    //public int GetSite(int col)
    //{
    //    int SiteId;
    //    switch (col)
    //    {
    //        case 5: SiteId = -1;
    //            break;

    //        case 4: SiteId = -2;
    //            break;
    //        case 15: SiteId = -4;
    //            break;
    //        case 19: SiteId = -7;
    //            break;
    //        case 16: SiteId = 13;
    //            break;
    //        case 20: SiteId = 25;
    //            break;
    //        case 13: SiteId = 8;
    //            break;
    //        case 9: SiteId = 7;
    //            break;
    //        case 12: SiteId = 6;
    //            break;
    //        case 10: SiteId = 4;
    //            break;
    //        case 6: SiteId = 1;
    //            break;
    //        case 14: SiteId = 18;
    //            break;
    //        case 7: SiteId = 2;
    //            break;
    //        case 17: SiteId = 14;
    //            break;
    //        case 18: SiteId = 15;
    //            break;
    //        case 8: SiteId = 3;
    //            break;
    //        case 11: SiteId = 5;
    //            break;
    //        case 21: SiteId = 24;
    //            break;
    //        default: SiteId = 100;
    //            break;
    //    }



    //    return SiteId;

    //}

}
