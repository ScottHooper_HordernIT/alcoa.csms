﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.FileDbMedicalTrainingProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(FileDbMedicalTrainingDataSourceDesigner))]
	public class FileDbMedicalTrainingDataSource : ProviderDataSource<FileDbMedicalTraining, FileDbMedicalTrainingKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FileDbMedicalTrainingDataSource class.
		/// </summary>
		public FileDbMedicalTrainingDataSource() : base(new FileDbMedicalTrainingService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the FileDbMedicalTrainingDataSourceView used by the FileDbMedicalTrainingDataSource.
		/// </summary>
		protected FileDbMedicalTrainingDataSourceView FileDbMedicalTrainingView
		{
			get { return ( View as FileDbMedicalTrainingDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the FileDbMedicalTrainingDataSource control invokes to retrieve data.
		/// </summary>
		public FileDbMedicalTrainingSelectMethod SelectMethod
		{
			get
			{
				FileDbMedicalTrainingSelectMethod selectMethod = FileDbMedicalTrainingSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (FileDbMedicalTrainingSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the FileDbMedicalTrainingDataSourceView class that is to be
		/// used by the FileDbMedicalTrainingDataSource.
		/// </summary>
		/// <returns>An instance of the FileDbMedicalTrainingDataSourceView class.</returns>
		protected override BaseDataSourceView<FileDbMedicalTraining, FileDbMedicalTrainingKey> GetNewDataSourceView()
		{
			return new FileDbMedicalTrainingDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the FileDbMedicalTrainingDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class FileDbMedicalTrainingDataSourceView : ProviderDataSourceView<FileDbMedicalTraining, FileDbMedicalTrainingKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FileDbMedicalTrainingDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the FileDbMedicalTrainingDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public FileDbMedicalTrainingDataSourceView(FileDbMedicalTrainingDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal FileDbMedicalTrainingDataSource FileDbMedicalTrainingOwner
		{
			get { return Owner as FileDbMedicalTrainingDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal FileDbMedicalTrainingSelectMethod SelectMethod
		{
			get { return FileDbMedicalTrainingOwner.SelectMethod; }
			set { FileDbMedicalTrainingOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal FileDbMedicalTrainingService FileDbMedicalTrainingProvider
		{
			get { return Provider as FileDbMedicalTrainingService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<FileDbMedicalTraining> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<FileDbMedicalTraining> results = null;
			FileDbMedicalTraining item;
			count = 0;
			
			System.Int32 _fileId;
			System.Int32 _companyId;
			System.Int32 _modifiedByUserId;
			System.Int32 _regionId;

			switch ( SelectMethod )
			{
				case FileDbMedicalTrainingSelectMethod.Get:
					FileDbMedicalTrainingKey entityKey  = new FileDbMedicalTrainingKey();
					entityKey.Load(values);
					item = FileDbMedicalTrainingProvider.Get(entityKey);
					results = new TList<FileDbMedicalTraining>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case FileDbMedicalTrainingSelectMethod.GetAll:
                    results = FileDbMedicalTrainingProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case FileDbMedicalTrainingSelectMethod.GetPaged:
					results = FileDbMedicalTrainingProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case FileDbMedicalTrainingSelectMethod.Find:
					if ( FilterParameters != null )
						results = FileDbMedicalTrainingProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = FileDbMedicalTrainingProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case FileDbMedicalTrainingSelectMethod.GetByFileId:
					_fileId = ( values["FileId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["FileId"], typeof(System.Int32)) : (int)0;
					item = FileDbMedicalTrainingProvider.GetByFileId(_fileId);
					results = new TList<FileDbMedicalTraining>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				// FK
				case FileDbMedicalTrainingSelectMethod.GetByCompanyId:
					_companyId = ( values["CompanyId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["CompanyId"], typeof(System.Int32)) : (int)0;
					results = FileDbMedicalTrainingProvider.GetByCompanyId(_companyId, this.StartIndex, this.PageSize, out count);
					break;
				case FileDbMedicalTrainingSelectMethod.GetByModifiedByUserId:
					_modifiedByUserId = ( values["ModifiedByUserId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["ModifiedByUserId"], typeof(System.Int32)) : (int)0;
					results = FileDbMedicalTrainingProvider.GetByModifiedByUserId(_modifiedByUserId, this.StartIndex, this.PageSize, out count);
					break;
				case FileDbMedicalTrainingSelectMethod.GetByRegionId:
					_regionId = ( values["RegionId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["RegionId"], typeof(System.Int32)) : (int)0;
					results = FileDbMedicalTrainingProvider.GetByRegionId(_regionId, this.StartIndex, this.PageSize, out count);
					break;
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == FileDbMedicalTrainingSelectMethod.Get || SelectMethod == FileDbMedicalTrainingSelectMethod.GetByFileId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				FileDbMedicalTraining entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					FileDbMedicalTrainingProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<FileDbMedicalTraining> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			FileDbMedicalTrainingProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region FileDbMedicalTrainingDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the FileDbMedicalTrainingDataSource class.
	/// </summary>
	public class FileDbMedicalTrainingDataSourceDesigner : ProviderDataSourceDesigner<FileDbMedicalTraining, FileDbMedicalTrainingKey>
	{
		/// <summary>
		/// Initializes a new instance of the FileDbMedicalTrainingDataSourceDesigner class.
		/// </summary>
		public FileDbMedicalTrainingDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public FileDbMedicalTrainingSelectMethod SelectMethod
		{
			get { return ((FileDbMedicalTrainingDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new FileDbMedicalTrainingDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region FileDbMedicalTrainingDataSourceActionList

	/// <summary>
	/// Supports the FileDbMedicalTrainingDataSourceDesigner class.
	/// </summary>
	internal class FileDbMedicalTrainingDataSourceActionList : DesignerActionList
	{
		private FileDbMedicalTrainingDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the FileDbMedicalTrainingDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public FileDbMedicalTrainingDataSourceActionList(FileDbMedicalTrainingDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public FileDbMedicalTrainingSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion FileDbMedicalTrainingDataSourceActionList
	
	#endregion FileDbMedicalTrainingDataSourceDesigner
	
	#region FileDbMedicalTrainingSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the FileDbMedicalTrainingDataSource.SelectMethod property.
	/// </summary>
	public enum FileDbMedicalTrainingSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByFileId method.
		/// </summary>
		GetByFileId,
		/// <summary>
		/// Represents the GetByCompanyId method.
		/// </summary>
		GetByCompanyId,
		/// <summary>
		/// Represents the GetByModifiedByUserId method.
		/// </summary>
		GetByModifiedByUserId,
		/// <summary>
		/// Represents the GetByRegionId method.
		/// </summary>
		GetByRegionId
	}
	
	#endregion FileDbMedicalTrainingSelectMethod

	#region FileDbMedicalTrainingFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="FileDbMedicalTraining"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FileDbMedicalTrainingFilter : SqlFilter<FileDbMedicalTrainingColumn>
	{
	}
	
	#endregion FileDbMedicalTrainingFilter

	#region FileDbMedicalTrainingExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="FileDbMedicalTraining"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FileDbMedicalTrainingExpressionBuilder : SqlExpressionBuilder<FileDbMedicalTrainingColumn>
	{
	}
	
	#endregion FileDbMedicalTrainingExpressionBuilder	

	#region FileDbMedicalTrainingProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;FileDbMedicalTrainingChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="FileDbMedicalTraining"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FileDbMedicalTrainingProperty : ChildEntityProperty<FileDbMedicalTrainingChildEntityTypes>
	{
	}
	
	#endregion FileDbMedicalTrainingProperty
}

