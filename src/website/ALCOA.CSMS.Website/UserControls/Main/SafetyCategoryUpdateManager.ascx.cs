﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using KaiZen.CSMS.Services;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxTabControl;
using DevExpress.Web.Data;
using DevExpress.Web.ASPxGridView;  

public partial class UserControls_Main_SafetyCategoryUpdateManager : System.Web.UI.UserControl
{
    Auth auth = new Auth();

    protected void Page_Load(object sender, EventArgs e) { Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); }

    private void moduleInit(bool postBack)
    {
        if (!postBack)
        { //first time load
            if (auth.RoleId == (int)RoleList.Administrator || Helper.General.isEhsConsultant(auth.UserId))
            {
                //grid.ExpandAll();
                //grid.SettingsPager.PageSize = 50;
                //grid.DataBind();
            }
            else
            {
                Response.Redirect("default.aspx");
            }

            string exportFileName = @"ALCOA CSMS - Safety Categories, Sponsors - Approvals"; //Chrome & IE is fine.
            String userAgent = Request.Headers.Get("User-Agent");
            if (
                (userAgent.Contains("Mozilla") && !userAgent.Contains("AppleWebKit") && !userAgent.Contains("MSIE")) || //Mozilla
                (userAgent.Contains("AppleWebKit") && !userAgent.Contains("Chrome"))      //Safari
                )
            {
                exportFileName = exportFileName.Replace(" ", "_");
            }
            Helper.ExportGrid.Settings(ucExportButtons, exportFileName, null, true);
        }
    }

    protected void gridStandard_CellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
    {
        if (e.Column.FieldName == "CompanySiteCategoryId" || e.Column.FieldName == "LocationSponsorUserId" || e.Column.FieldName == "Approved")
        {
            ASPxComboBox combo = (ASPxComboBox)e.Editor;
            combo.DataBindItems();

            string _text = string.Empty;
            if (e.Column.FieldName == "Approved") _text = "Tentative";

            ListEditItem item = new ListEditItem(_text, null);
            combo.Items.Insert(0, item);
        }
    }

    //protected void grid_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    //{
    //    try
    //    {
    //        int QuestionnaireId = Convert.ToInt32(Request.QueryString["q"].ToString());
    //        QuestionnaireService qService = new QuestionnaireService();
    //        Questionnaire q = qService.GetByQuestionnaireId(QuestionnaireId);

    //        //QuestionnaireWithLocationApprovalViewLatestService qwlavlService = new QuestionnaireWithLocationApprovalViewLatestService();
    //        //QuestionnaireWithLocationApprovalViewLatest qwlavl = qwlavlService.GetByC

    //        CompanySiteCategoryStandardService cscsService = new CompanySiteCategoryStandardService();
    //        CompanySiteCategoryStandard cscs = new CompanySiteCategoryStandard();
    //        cscs.CompanyId = q.CompanyId;
    //        cscs.SiteId = Convert.ToInt32(e.NewValues["SiteId"]);
    //        if (e.NewValues["LocationSponsorUserId"] != null)
    //        {
    //            cscs.LocationSponsorUserId = Convert.ToInt32(e.NewValues["LocationSponsorUserId"]);
    //        }
    //        if (e.NewValues["CompanySiteCategoryId"] != null)
    //        {
    //            cscs.CompanySiteCategoryId = Convert.ToInt32(e.NewValues["CompanySiteCategoryId"]);
    //        }
    //        cscs.ModifiedByUserId = auth.UserId;
    //        cscs.ModifiedDate = DateTime.Now;
    //        if (e.NewValues["Approved"] != null)
    //        {
    //            switch (e.NewValues["Approved"].ToString())
    //            {
    //                case "1":
    //                    cscs.Approved = true;
    //                    cscs.ApprovedByUserId = auth.UserId;
    //                    break;
    //                case "0":
    //                    cscs.Approved = false;
    //                    cscs.ApprovedByUserId = auth.UserId;
    //                    break;
    //                default:
    //                    break;
    //            }
    //        }

    //        cscsService.Save(cscs);

    //        e.Cancel = true;
    //        grid.CancelEdit();
    //        //BindLocationData(q.CompanyId);

    //        string SiteName = "-";
    //        SitesService sService = new SitesService();
    //        Sites s = sService.GetBySiteId(cscs.SiteId);
    //        if (s != null) SiteName = s.SiteName;

    //        string SafetyCategory = "-";
    //        if (cscs.CompanySiteCategoryId != null)
    //        {
    //            CompanySiteCategoryService cscService = new CompanySiteCategoryService();
    //            CompanySiteCategory csc = cscService.GetByCompanySiteCategoryId(Convert.ToInt32(cscs.CompanySiteCategoryId));
    //            if (cscs != null) SafetyCategory = csc.CategoryName;
    //        }

    //        string LocationSponsorUser = "-";
    //        if (cscs.LocationSponsorUserId != null)
    //        {
    //            UsersService uService = new UsersService();
    //            Users u = uService.GetByUserId(Convert.ToInt32(cscs.LocationSponsorUserId));
    //            if (u != null) LocationSponsorUser = u.FirstName + " " + u.LastName;
    //        }

    //        string Approval = "-";
    //        if (cscs.Approved != null)
    //        {
    //            if (cscs.Approved == true)
    //            {
    //                Approval = "Yes";
    //            }
    //            else
    //            {
    //                Approval = "No";
    //            }
    //        }

    //        if (Helper.Questionnaire.ActionLog.Add((int)QuestionnaireActionList.LocationApprovalAdded,
    //                                                q.QuestionnaireId,                         //QuestionnaireId
    //                                                auth.UserId,                                //UserId
    //                                                auth.FirstName,                             //FirstName
    //                                                auth.LastName,                              //LastName
    //                                                auth.RoleId,                                //RoleId
    //                                                auth.CompanyId,                             //CompanyId
    //                                                auth.CompanyName,                           //CompanyName
    //                                                null,                                       //Comments
    //                                                null,                                       //OldAssigned
    //                                                SiteName,                                   //NewAssigned
    //                                                SafetyCategory,                             //NewAssigned2
    //                                                LocationSponsorUser,                        //NewAssigned3
    //                                                Approval)                                   //NewAssigned4
    //                                                == false)
    //        {
    //            //for now we do nothing if action log fails.
    //        }
    //    }
    //    catch (Exception ex)
    //    {
    //        throw new Exception(ex.Message);
    //    }
    //}
    //protected void grid_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    //{
    //    try
    //    {
    //        string SiteName = "-";
    //        SitesService sService = new SitesService();
    //        Sites s = sService.GetBySiteId(cscs.SiteId);
    //        if (s != null) SiteName = s.SiteName;

    //        string SafetyCategory = "-";
    //        if (cscs.CompanySiteCategoryId != null)
    //        {
    //            CompanySiteCategoryService cscService = new CompanySiteCategoryService();
    //            CompanySiteCategory csc = cscService.GetByCompanySiteCategoryId(Convert.ToInt32(cscs.CompanySiteCategoryId));
    //            if (cscs != null) SafetyCategory = csc.CategoryName;
    //        }

    //        string LocationSponsorUser = "-";
    //        if (cscs.LocationSponsorUserId != null)
    //        {
    //            UsersService uService = new UsersService();
    //            Users u = uService.GetByUserId(Convert.ToInt32(cscs.LocationSponsorUserId));
    //            if (u != null) LocationSponsorUser = u.FirstName + " " + u.LastName;
    //        }

    //        string Approval = "-";
    //        if (cscs.Approved != null)
    //        {
    //            if (cscs.Approved == true)
    //            {
    //                Approval = "Yes";
    //            }
    //            else
    //            {
    //                Approval = "No";
    //            }
    //        }

    //        if (Helper.Questionnaire.ActionLog.Add((int)QuestionnaireActionList.LocationApprovalModified,
    //                                                q.QuestionnaireId,                         //QuestionnaireId
    //                                                auth.UserId,                                //UserId
    //                                                auth.FirstName,                             //FirstName
    //                                                auth.LastName,                              //LastName
    //                                                auth.RoleId,                                //RoleId
    //                                                auth.CompanyId,                             //CompanyId
    //                                                auth.CompanyName,                           //CompanyName
    //                                                null,                                       //Comments
    //                                                null,                                       //OldAssigned
    //                                                SiteName,                                   //NewAssigned
    //                                                SafetyCategory,                             //NewAssigned2
    //                                                LocationSponsorUser,                        //NewAssigned3
    //                                                Approval)                                   //NewAssigned4
    //                                                == false)
    //        {
    //            //for now we do nothing if action log fails.
    //        }

    //    }
    //    catch (Exception ex)
    //    {
    //        throw new Exception(ex.Message);
    //    }
    //}

    protected void grid_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        try
        {
            int CompanyId = Convert.ToInt32(e.NewValues["CompanyId"]);
            int SiteId = Convert.ToInt32(e.NewValues["SiteId"]);

            int count = 0;
            int QuestionnaireId = 0;

            QuestionnaireWithLocationApprovalViewLatestService qwlavlService = new QuestionnaireWithLocationApprovalViewLatestService();
            QuestionnaireWithLocationApprovalViewLatestFilters qwlavlFilters = new QuestionnaireWithLocationApprovalViewLatestFilters();
            qwlavlFilters.Append(QuestionnaireWithLocationApprovalViewLatestColumn.CompanyId, CompanyId.ToString());
            VList<QuestionnaireWithLocationApprovalViewLatest> qwlavlTlist = qwlavlService.GetPaged(qwlavlFilters.ToString(), "",0, 999, out count);

            if(count != 1) throw new Exception("Error Occured. Could not find Company.");

            QuestionnaireId = qwlavlTlist[0].QuestionnaireId;

            CompanySiteCategoryStandardService cscsService = new CompanySiteCategoryStandardService();
            CompanySiteCategoryStandard cscs = new CompanySiteCategoryStandard();
            cscs.CompanyId = CompanyId;
            cscs.SiteId = Convert.ToInt32(e.NewValues["SiteId"]);
            if (e.NewValues["LocationSponsorUserId"] != null)
            {
                cscs.LocationSponsorUserId = Convert.ToInt32(e.NewValues["LocationSponsorUserId"]);
            }
            if (e.NewValues["LocationSpaUserId"] != null)
            {
                cscs.LocationSpaUserId = Convert.ToInt32(e.NewValues["LocationSpaUserId"]);
            }
            if (e.NewValues["CompanySiteCategoryId"] != null)
            {
                cscs.CompanySiteCategoryId = Convert.ToInt32(e.NewValues["CompanySiteCategoryId"]);
            }
            if (e.NewValues["ArpUserId"] != null)
            {
                cscs.ArpUserId = Convert.ToInt32(e.NewValues["ArpUserId"]);
            }
            //if (e.NewValues["CrpUserId"] != null)
            //{
            //    cscs.CrpUserId = Convert.ToInt32(e.NewValues["CrpUserId"]);
            //}

            cscs.ModifiedByUserId = auth.UserId;
            cscs.ModifiedDate = DateTime.Now;
            if (e.NewValues["Approved"] != null)
            {
                switch (e.NewValues["Approved"].ToString())
                {
                    case "True":
                        cscs.Approved = true;
                        cscs.ApprovedByUserId = auth.UserId;
                        break;
                    case "False":
                        cscs.Approved = false;
                        cscs.ApprovedByUserId = auth.UserId;
                        break;
                    default:
                        break;
                }
            }

            cscsService.Save(cscs);

            e.Cancel = true;
            grid.CancelEdit();

            string SiteName = "-";
            SitesService sService = new SitesService();
            Sites s = sService.GetBySiteId(cscs.SiteId);
            if (s != null) SiteName = s.SiteName;

            string SafetyCategory = "-";
            if (cscs.CompanySiteCategoryId != null)
            {
                CompanySiteCategoryService cscService = new CompanySiteCategoryService();
                CompanySiteCategory csc = cscService.GetByCompanySiteCategoryId(Convert.ToInt32(cscs.CompanySiteCategoryId));
                if (cscs != null) SafetyCategory = csc.CategoryName;
            }

            string LocationSponsorUser = "-";
            if (cscs.LocationSponsorUserId != null)
            {
                UsersService uService = new UsersService();
                Users u = uService.GetByUserId(Convert.ToInt32(cscs.LocationSponsorUserId));
                if (u != null) LocationSponsorUser = u.FirstName + " " + u.LastName;
            }

            string Approval = "-";
            if (cscs.Approved != null)
            {
                if (cscs.Approved == true)
                {
                    Approval = "Yes";
                }
                else
                {
                    Approval = "No";
                }
            }

            if (Helper.Questionnaire.ActionLog.Add((int)QuestionnaireActionList.LocationApprovalAdded,
                                                    QuestionnaireId,                           //QuestionnaireId
                                                    auth.UserId,                                //UserId
                                                    auth.FirstName,                             //FirstName
                                                    auth.LastName,                              //LastName
                                                    auth.RoleId,                                //RoleId
                                                    auth.CompanyId,                             //CompanyId
                                                    auth.CompanyName,                           //CompanyName
                                                    "Edited in CMT Shop Front Tools section",   //Comments
                                                    null,                                       //OldAssigned
                                                    SiteName,                                   //NewAssigned
                                                    SafetyCategory,                             //NewAssigned2
                                                    LocationSponsorUser,                        //NewAssigned3
                                                    Approval)                                   //NewAssigned4
                                                    == false)
            {
                //for now we do nothing if action log fails.
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    protected void grid_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        try
        {
            int CompanyId = Convert.ToInt32(e.OldValues["CompanyId"]);
            int SiteId = Convert.ToInt32(e.OldValues["SiteId"]);

            int count = 0;
            int QuestionnaireId = 0;

            QuestionnaireWithLocationApprovalViewLatestService qwlavlService = new QuestionnaireWithLocationApprovalViewLatestService();
            QuestionnaireWithLocationApprovalViewLatestFilters qwlavlFilters = new QuestionnaireWithLocationApprovalViewLatestFilters();
            qwlavlFilters.Append(QuestionnaireWithLocationApprovalViewLatestColumn.CompanyId, CompanyId.ToString());
            VList<QuestionnaireWithLocationApprovalViewLatest> qwlavlTlist = qwlavlService.GetPaged(qwlavlFilters.ToString(), "",0, 999, out count);

            if(count != 1) throw new Exception("Error Occured. Could not find Company.");

            QuestionnaireId = qwlavlTlist[0].QuestionnaireId;

            CompanySiteCategoryStandardService cscsService = new CompanySiteCategoryStandardService();
            CompanySiteCategoryStandard cscs = cscsService.GetByCompanyIdSiteId(CompanyId, SiteId);
            if (cscs == null) throw new Exception("Unexpected Error. Please refresh (or re-open) this page.");
            cscs.CompanyId = CompanyId;
            cscs.SiteId = Convert.ToInt32(e.NewValues["SiteId"]);
            if (e.NewValues["LocationSponsorUserId"] != null)
            {
                cscs.LocationSponsorUserId = Convert.ToInt32(e.NewValues["LocationSponsorUserId"]);
            }
            else
            {
                cscs.LocationSponsorUserId = null;
            }
            if (e.NewValues["LocationSpaUserId"] != null)
            {
                cscs.LocationSpaUserId = Convert.ToInt32(e.NewValues["LocationSpaUserId"]);
            }
            else
            {
                cscs.LocationSpaUserId = null;
            }
            if (e.NewValues["CompanySiteCategoryId"] != null)
            {
                cscs.CompanySiteCategoryId = Convert.ToInt32(e.NewValues["CompanySiteCategoryId"]);
            }
            else
            {
                cscs.CompanySiteCategoryId = null;
            }
            if (e.NewValues["ArpUserId"] != null)
            {
                cscs.ArpUserId = Convert.ToInt32(e.NewValues["ArpUserId"]);
            }
            //if (e.NewValues["CrpUserId"] != null)
            //{
            //    cscs.CrpUserId = Convert.ToInt32(e.NewValues["CrpUserId"]);
            //}

            cscs.ModifiedByUserId = auth.UserId;
            cscs.ModifiedDate = DateTime.Now;
            if (e.NewValues["Approved"] != null)
            {
                switch (e.NewValues["Approved"].ToString())
                {
                    case "True":
                        cscs.Approved = true;
                        cscs.ApprovedByUserId = auth.UserId;
                        break;
                    case "False":
                        cscs.Approved = false;
                        cscs.ApprovedByUserId = auth.UserId;
                        break;
                    default:
                        break;
                }
            }

            cscsService.Save(cscs);

            e.Cancel = true;
            grid.CancelEdit();

            string SiteName = "-";
            SitesService sService = new SitesService();
            Sites s = sService.GetBySiteId(cscs.SiteId);
            if (s != null) SiteName = s.SiteName;

            string SafetyCategory = "-";
            if (cscs.CompanySiteCategoryId != null)
            {
                CompanySiteCategoryService cscService = new CompanySiteCategoryService();
                CompanySiteCategory csc = cscService.GetByCompanySiteCategoryId(Convert.ToInt32(cscs.CompanySiteCategoryId));
                if (cscs != null) SafetyCategory = csc.CategoryName;
            }

            string LocationSponsorUser = "-";
            if (cscs.LocationSponsorUserId != null)
            {
                UsersService uService = new UsersService();
                Users u = uService.GetByUserId(Convert.ToInt32(cscs.LocationSponsorUserId));
                if (u != null) LocationSponsorUser = u.FirstName + " " + u.LastName;
            }

            string Approval = "-";
            if (cscs.Approved != null)
            {
                if (cscs.Approved == true)
                {
                    Approval = "Yes";
                }
                else
                {
                    Approval = "No";
                }
            }

            if (Helper.Questionnaire.ActionLog.Add((int)QuestionnaireActionList.LocationApprovalModified,
                                                    QuestionnaireId,                            //QuestionnaireId
                                                    auth.UserId,                                //UserId
                                                    auth.FirstName,                             //FirstName
                                                    auth.LastName,                              //LastName
                                                    auth.RoleId,                                //RoleId
                                                    auth.CompanyId,                             //CompanyId
                                                    auth.CompanyName,                           //CompanyName
                                                    "Edited in CMT Shop Front Tools section",   //Comments
                                                    null,                                       //OldAssigned
                                                    SiteName,                                   //NewAssigned
                                                    SafetyCategory,                             //NewAssigned2
                                                    LocationSponsorUser,                        //NewAssigned3
                                                    Approval)                                   //NewAssigned4
                                                    == false)
            {
                //for now we do nothing if action log fails.
            }

        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    protected void grid_InitNewRow(object sender, ASPxDataInitNewRowEventArgs e)
    {
        //e.NewValues["ApprovedByUserId"] = auth.UserId;
        //e.NewValues["ApprovedDate"] = DateTime.Now;
        //e.NewValues["ModifiedByUserId"] = auth.UserId;
        //e.NewValues["ModifiedDate"] = DateTime.Now;

        //int QuestionnaireId = Convert.ToInt32(Request.QueryString["q"].ToString());
        //QuestionnaireService qService = new QuestionnaireService();
        //Questionnaire q = qService.GetByQuestionnaireId(QuestionnaireId);
        //e.NewValues["CompanyId"] = q.CompanyId;
    }
}
