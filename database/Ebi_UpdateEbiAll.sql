

/****** Object:  StoredProcedure [dbo].[Ebi_UpdateEbiAll]    Script Date: 3/08/2015 4:35:18 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Ebi_UpdateEbiAll]              
AS              
DECLARE @VendorNo VARCHAR(MAX)              
DECLARE @ClockId INT              
DECLARE @EbiId INT              
DECLARE @EbiCursor INT          
DECLARE @SwipeDateTime DateTime           
BEGIN TRY              
 SET @VendorNo = ''              
 SET @EbiId = 0      
 SELECT @EbiCursor=Max(EbiId) from Ebi            
    
 WHILE @EbiCursor >=0             
  BEGIN         
  SET @VendorNo = ''              
  SET @EbiId = 0  
  SET @ClockId=0   
  SET @SwipeDateTime=null  
      
   IF Exists(Select ClockId from Ebi where EbiId=@EbiCursor)          
   BEGIN          
    SET @EbiId=@EbiCursor;          
    SELECT @ClockId=ClockId,@SwipeDateTime=SwipeDateTime          
    FROM EBI WHERE EbiId=@EbiId;          
    IF @ClockId IS NOT NULL               
    BEGIN               
     SELECT TOP 1 @VendorNo=CWK_VENDOR_NO          
     FROM HR..XXHR_CWK_HISTORY_V            
     WHERE CWK_NBR=@ClockId AND EFFECTIVE_END_DATE>=@SwipeDateTime            
     ORDER BY EFFECTIVE_END_DATE ASC             
    
     -- Modify the vendor_number column              
     UPDATE              
     [dbo].[Ebi]              
     SET              
     [Vendor_Number] = @VendorNo              
     WHERE              
     [EbiId] = @EbiId              
    END          
   END          
   SET @EbiCursor=@EbiCursor-1           
  END             
END TRY              
BEGIN CATCH              
END CATCH 

------------------------------------------------------------------------------

GO


