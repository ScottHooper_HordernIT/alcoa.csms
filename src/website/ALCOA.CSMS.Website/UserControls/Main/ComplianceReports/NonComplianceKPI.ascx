﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="UserControls_NonComplianceKPI" Codebehind="NonComplianceKPI.ascx.cs" %>
<%@ Register Src="../../Other/ExportButtons.ascx" TagName="ExportButtons" TagPrefix="uc1" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
    <%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"  Namespace="System.Web.UI" TagPrefix="asp" %>

<table border="0" cellpadding="2" cellspacing="0" width="100%">
    <tr>
        <td class="pageName" colspan="3" style="height: 16px; width: 1170px;">
        <span class="bodycopy"><span class="title">Compliance Report</span><br />
                <span class="date">KPI</span><br />
            <img src="images/grfc_dottedline.gif" width="24" height="1"></span></td>
    </tr>
        <tr>
            <td style="height: 28px; width: 1170px;" colspan="3">
                <table style="width: 522px">
            <tr>
                <td style="width: 76px; text-align: right">
                    <strong>Operation:&nbsp;</strong></td>
                <td style="width: 201px; text-align: left">
                    <dxe:ASPxComboBox id="cbRegion" runat="server"
                         datasourceid="dsRegionsFilter" textfield="RegionName"
                         autopostback="True" valuefield="RegionId"
                         valuetype="System.Int32"
                         cssfilepath="~/App_Themes/Office2003Blue/{0}/styles.css"
                         csspostfix="Office2003Blue" width="200px"
                         onselectedindexchanged="cbRegion_SelectedIndexChanged" 
                        SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                        <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                        </LoadingPanelImage>
                        <ButtonStyle Width="13px">
                        </ButtonStyle>
                        <ValidationSettings
                         setfocusonerror="True" validationgroup="Filter" Display="Dynamic">
                      <RequiredField isrequired="True"></RequiredField></ValidationSettings></dxe:ASPxComboBox>
                </td>
                <td style="width: 38px; text-align: right">
                    <strong>Year:</strong></td>
                <td style="width: 71px; text-align: left">
                    <dxe:ASPxComboBox id="cmbYear" runat="server"
                     autopostback="True" clientinstancename="cmbYear"
                     cssfilepath="~/App_Themes/Office2003Blue/{0}/styles.css"
                     csspostfix="Office2003Blue" IncrementalFilteringMode="StartsWith"
                     onselectedindexchanged="cmbYear_SelectedIndexChanged"
                     valuetype="System.String" width="60px" 
                        SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                        <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                        </LoadingPanelImage>
                        <ButtonStyle Width="13px">
                        </ButtonStyle>
                        <ValidationSettings Display="Dynamic">
                        </ValidationSettings>
                    </dxe:ASPxComboBox>
                </td>
                <td align="right" style="width: 85px; height: 28px">
                    <button name="btnPrint" onclick="javascript:print();" style="font-weight: bold; width: 80px;
                        height: 25px" type="button">
                        Print</button>
                </td>
            </tr>
        </table>

            <dxwgv:ASPxGridView ID="gridKPIReport"
                ClientInstanceName="gridKPIReport"
                runat="server"
                AutoGenerateColumns="False"
                CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                CssPostfix="Office2003Blue"
                DataSourceID="sqldsComplianceKPI"
                Width="900px"
                OnHtmlRowCreated="gridKPIReport_RowCreated" KeyFieldName="CompanyId"
                OnAutoFilterCellEditorInitialize="gridKPIReport_AutoFilterCellEditorInitialize"

             >
                <Columns>
                    <dxwgv:GridViewDataComboBoxColumn Caption="Company Name" FieldName="CompanyId" Name="CompanyBox" SortIndex="2" SortOrder="Ascending"
                        VisibleIndex="0">
                        <PropertiesComboBox DataSourceID="sqldsCompaniesList" DropDownHeight="150px" TextField="CompanyName"
                            ValueField="CompanyId" ValueType="System.Int32" IncrementalFilteringMode="StartsWith">
                        </PropertiesComboBox>
                        <EditFormSettings Visible="True" />
                        <Settings SortMode="DisplayText" />
                    </dxwgv:GridViewDataComboBoxColumn>
                    
                    <dxwgv:GridViewDataComboBoxColumn FieldName="SiteId"
                        Visible="False" SortOrder="Ascending" SortIndex="1">
                        <PropertiesComboBox ValueType="System.String">
                        </PropertiesComboBox>
                    </dxwgv:GridViewDataComboBoxColumn>
                        
                    <dxwgv:GridViewDataComboBoxColumn Caption="Site Name" FieldName="SiteName" Name="SiteBox"
                        VisibleIndex="1" SortIndex="3" SortOrder="Ascending">
                        <PropertiesComboBox DataSourceID="dsSitesFilter" DropDownHeight="150px" TextField="SiteName"
                            ValueField="SiteName" ValueType="System.String" IncrementalFilteringMode="StartsWith">
                        </PropertiesComboBox>
                        <EditFormSettings Visible="True" />
                        <Settings SortMode="DisplayText" />
                         <DataItemTemplate> <%--changed by Debashis for Export issue--%>
                            <asp:Label ID="lblSiteName" runat="server" Text="" OnInit="lblSiteName_Init" ></asp:Label>
                        </DataItemTemplate>
                    </dxwgv:GridViewDataComboBoxColumn>

                    <dxwgv:GridViewDataComboBoxColumn Name="gcbResidentialCategory" 
                    Caption="Residential Category" FieldName="CompanySiteCategoryId"
                    SortIndex="0" SortOrder="Ascending" UnboundType="String" VisibleIndex="2" 
                    FixedStyle="Left" Width="145px">
                    <PropertiesComboBox DataSourceID="CompanySiteCategoryDataSource" DropDownHeight="150px" TextField="CategoryDesc"
                        ValueField="CompanySiteCategoryId" ValueType="System.Int32" IncrementalFilteringMode="StartsWith">
                    </PropertiesComboBox>
                    <Settings SortMode="DisplayText" />
                </dxwgv:GridViewDataComboBoxColumn>
                    <dxwgv:GridViewDataTextColumn FieldName="Jan" Name="Jan" ReadOnly="True" VisibleIndex="3">
                        <DataItemTemplate>
                     <asp:Image ID="changeImage1" runat="server" ImageUrl="~/Images/spacer.gif" Visible="true" GenerateEmptyAlternateText="True" />
                     <asp:Label ID="noKPISubmitted1" runat="server" Text="" ></asp:Label>
                        </DataItemTemplate>
                        <HeaderStyle HorizontalAlign="Center" />
                        <CellStyle HorizontalAlign="Center">
                        </CellStyle>
                        <Settings AllowHeaderFilter="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn FieldName="Feb" Name="Feb" ReadOnly="True" VisibleIndex="4">
                        <DataItemTemplate>
                     
                     <asp:Image ID="changeImage2" runat="server" ImageUrl="~/Images/spacer.gif" Visible="true" GenerateEmptyAlternateText="True" />
                     <asp:Label ID="noKPISubmitted2" runat="server" Text="" ></asp:Label>
                     
                        </DataItemTemplate>
                        <HeaderStyle HorizontalAlign="Center" />
                        <CellStyle HorizontalAlign="Center">
                        </CellStyle>
                        <Settings AllowHeaderFilter="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn FieldName="Mar" Name="Mar" ReadOnly="True" VisibleIndex="5">
                        <DataItemTemplate>
                     
                     <asp:Image ID="changeImage3" runat="server" ImageUrl="~/Images/spacer.gif" Visible="true" GenerateEmptyAlternateText="True" />
                     <asp:Label ID="noKPISubmitted3" runat="server" Text="" ></asp:Label>
                     
                        </DataItemTemplate>
                        <HeaderStyle HorizontalAlign="Center" />
                        <CellStyle HorizontalAlign="Center">
                        </CellStyle>
                        <Settings AllowHeaderFilter="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn FieldName="Apr" Name="Apr" ReadOnly="True" VisibleIndex="6">
                        <DataItemTemplate>
                     
                     <asp:Image ID="changeImage4" runat="server" ImageUrl="~/Images/spacer.gif" Visible="true" GenerateEmptyAlternateText="True" />
                     <asp:Label ID="noKPISubmitted4" runat="server" Text="" ></asp:Label>
                     
                        </DataItemTemplate>
                        <HeaderStyle HorizontalAlign="Center" />
                        <CellStyle HorizontalAlign="Center">
                        </CellStyle>
                        <Settings AllowHeaderFilter="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn FieldName="May" Name="May" ReadOnly="True" VisibleIndex="7">
                        <DataItemTemplate>
                     
                     <asp:Image ID="changeImage5" runat="server" ImageUrl="~/Images/spacer.gif" Visible="true" GenerateEmptyAlternateText="True" />
                     <asp:Label ID="noKPISubmitted5" runat="server" Text="" ></asp:Label>
                     
                        </DataItemTemplate>
                        <HeaderStyle HorizontalAlign="Center" />
                        <CellStyle HorizontalAlign="Center">
                        </CellStyle>
                        <Settings AllowHeaderFilter="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn FieldName="Jun" Name="Jun" ReadOnly="True" VisibleIndex="8">
                        <DataItemTemplate>
                     
                     <asp:Image ID="changeImage6" runat="server" ImageUrl="~/Images/spacer.gif" Visible="true" GenerateEmptyAlternateText="True" />
                     <asp:Label ID="noKPISubmitted6" runat="server" Text="" ></asp:Label>
                     
                        </DataItemTemplate>
                        <HeaderStyle HorizontalAlign="Center" />
                        <CellStyle HorizontalAlign="Center">
                        </CellStyle>
                        <Settings AllowHeaderFilter="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn FieldName="Jul" Name="Jul" ReadOnly="True" VisibleIndex="9">
                        <DataItemTemplate>
                     
                     <asp:Image ID="changeImage7" runat="server" ImageUrl="~/Images/spacer.gif" Visible="true" GenerateEmptyAlternateText="True" />
                     <asp:Label ID="noKPISubmitted7" runat="server" Text="" ></asp:Label>
                     
                        </DataItemTemplate>
                        <HeaderStyle HorizontalAlign="Center" />
                        <CellStyle HorizontalAlign="Center">
                        </CellStyle>
                        <Settings AllowHeaderFilter="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn FieldName="Aug" Name="Aug" ReadOnly="True" VisibleIndex="10">
                        <DataItemTemplate>
                     
                     <asp:Image ID="changeImage8" runat="server" ImageUrl="~/Images/spacer.gif" Visible="true" GenerateEmptyAlternateText="True" />
                     <asp:Label ID="noKPISubmitted8" runat="server" Text="" ></asp:Label>
                     
                        </DataItemTemplate>
                        <HeaderStyle HorizontalAlign="Center" />
                        <CellStyle HorizontalAlign="Center">
                        </CellStyle>
                        <Settings AllowHeaderFilter="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn FieldName="Sep" Name="Sep" ReadOnly="True" VisibleIndex="11">
                        <DataItemTemplate>
                     
                     <asp:Image ID="changeImage9" runat="server" ImageUrl="~/Images/spacer.gif" Visible="true" GenerateEmptyAlternateText="True" />
                     <asp:Label ID="noKPISubmitted9" runat="server" Text="" ></asp:Label>
                     
                        </DataItemTemplate>
                        <HeaderStyle HorizontalAlign="Center" />
                        <CellStyle HorizontalAlign="Center">
                        </CellStyle>
                        <Settings AllowHeaderFilter="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn FieldName="Oct" Name="Oct" ReadOnly="True" VisibleIndex="12">
                        <DataItemTemplate>
                     
                     <asp:Image ID="changeImage10" runat="server" ImageUrl="~/Images/spacer.gif" Visible="true" GenerateEmptyAlternateText="True" />
                     <asp:Label ID="noKPISubmitted10" runat="server" Text="" ></asp:Label>
                     
                        </DataItemTemplate>
                        <HeaderStyle HorizontalAlign="Center" />
                        <CellStyle HorizontalAlign="Center">
                        </CellStyle>
                        <Settings AllowHeaderFilter="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn FieldName="Nov" Name="Nov" ReadOnly="True" VisibleIndex="13">
                        <DataItemTemplate>
                     
                     <asp:Image ID="changeImage11" runat="server" ImageUrl="~/Images/spacer.gif" Visible="true" GenerateEmptyAlternateText="True" />
                     <asp:Label ID="noKPISubmitted11" runat="server" Text=""  ></asp:Label>
                     
                        </DataItemTemplate>
                        <HeaderStyle HorizontalAlign="Center" />
                        <CellStyle HorizontalAlign="Center">
                        </CellStyle>
                        <Settings AllowHeaderFilter="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn FieldName="Dec" Name="Dec" ReadOnly="True" VisibleIndex="14">
                        <DataItemTemplate>
                     
                     <asp:Image ID="changeImage12" runat="server" ImageUrl="~/Images/spacer.gif" Visible="true" GenerateEmptyAlternateText="True" />
                     <asp:Label ID="noKPISubmitted12" runat="server" Text="" ></asp:Label>
                     
                        </DataItemTemplate>
                        <HeaderStyle HorizontalAlign="Center" />
                        <CellStyle HorizontalAlign="Center">
                        </CellStyle>
                        <Settings AllowHeaderFilter="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn FieldName="Year" Name="Year" ReadOnly="True" VisibleIndex="15">
                        <DataItemTemplate>
                     
                     <asp:Label ID="noKPISubmitted13" runat="server" Text=""  OnInit="noKPISubmitted13_Init"></asp:Label>
                     
                        </DataItemTemplate>
                        <HeaderStyle HorizontalAlign="Center" />
                        <CellStyle HorizontalAlign="Center">
                        </CellStyle>
                        <Settings AllowHeaderFilter="False" />
                    </dxwgv:GridViewDataTextColumn>
                           
                </Columns>
                <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                    <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
                    </LoadingPanelOnStatusBar>
                    <LoadingPanel Url="~/App_Themes/Office2003BlueOld/GridView/Loading.gif"><%--Enhancement_023:change by debashis for testing platform upgradation--%>
                    </LoadingPanel>
                </Images>
                <Settings ShowGroupPanel="True" ShowFilterRow="True" />
                <SettingsPager PageSize="20" AlwaysShowPager="True">
                    <AllButton Visible="True">
                    </AllButton>
                    <NextPageButton >
                    </NextPageButton>
                    <PrevPageButton>
                    </PrevPageButton>
                </SettingsPager>
                <SettingsCustomizationWindow Enabled="True" />
                <ImagesFilterControl>
                    <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
                    </LoadingPanel>
                </ImagesFilterControl>
                <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
                    CssPostfix="Office2003Blue">
                    <Header ImageSpacing="5px" SortingImageSpacing="5px">
                    </Header>
                    <AlternatingRow Enabled="True">
                    </AlternatingRow>
                    <LoadingPanel ImageSpacing="10px">
                    </LoadingPanel>
                </Styles>
                <SettingsBehavior ConfirmDelete="True"/>
                <SettingsCookies CookiesID="NonComplianceKPI" Version="0.2" />
                <SettingsText CommandClearFilter="Clear" />
                <StylesEditors>
                    <ProgressBar Height="25px">
                    </ProgressBar>
                </StylesEditors>
            </dxwgv:ASPxGridView>
            <tr>
            <td>
                <a href="javascript:ShowHideCustomizationWindow()">
                        <span style="color: #0000ff; text-decoration: underline"></span>
                </a>
            </td>
        </tr>
</table>
<table width="900px">
<tr align="right">
        <td align="right" class="pageName" style="height: 30px; text-align: right; text-align:-moz-right; width: 900px;">
            <uc1:ExportButtons ID="ucExportKPIReport" runat="server" />
        </td>
</tr>
    <tr align="right">
        <td align="right" class="pageName" style="width: 900px; height: 30px; text-align: left">    
            <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td style="width: 100%; height: 13px; text-align: center">
    <table border="0" cellpadding="0" cellspacing="0" class="dxgvControl_Office2003_Blue"
                        style="width: 100%; border-collapse: collapse; empty-cells: show">
        <tbody>
            <tr>
                <td class="dxgvHeader_Office2003_Blue" colspan="2" style="border-top-width: 0px;
                                    font-weight: bold; border-left-width: 0px; white-space: normal; height: 13px; text-align: left;">
                    <span style="color: maroon">What does all these icons mean? (LEGEND)</span></td>
            </tr>
            <tr class="dxgvDataRow_Office2003_Blue">
                <td class="dxgv" style="width: 30px; height: 20px; text-align: center">
                    <img src="Images/greentick.gif" /></td>
                <td class="dxgv" style="width: 972px; height: 20px; text-align: left">
                    A KPI has been submitted against this Company, Site and Month.</td>
            </tr>
            <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                <td class="dxgv" style="width: 30px; height: 20px; text-align: center">
                    <img src="Images/greentick.gif" />*</td>
                <td class="dxgv" style="width: 972px; height: 20px; text-align: left">
                    A KPI has been submitted against this Company, Site and Month but was not known as being on-site based on EBI information.</td>
            </tr>
            <tr class="dxgvDataRow_Office2003_Blue">
                <td class="dxgv" style="width: 30px; height: 20px; text-align: center">
                    <img src="Images/TrafficLights/tlYellow.gif" /></td>
                <td class="dxgv" style="width: 972px; height: 20px; text-align: left">
                    A KPI has not been submitted or data has been entered incorrectly.</td>
            </tr>
            <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                <td class="dxgv" style="width: 30px; height: 20px; text-align: center">
                    <img src="Images/redcross.gif" /></td>
                <td class="dxgv" style="width: 972px; height: 20px; text-align: left">
                    A KPI for this specific Company, Site and Month was expected but does not exist.</td>
            </tr>
            <tr class="dxgvDataRow_Office2003_Blue">
                <td class="dxgv" style="width: 30px; height: 20px; text-align: center">
                  *</td>
                <td class="dxgv" style="width: 972px; height: 20px; text-align: left">
                    Shown against a Site Name, indicates that On-Site EBI information is not currently available for this location.</td>
            </tr>
        </tbody>
    </table>
            
        </td>
    </tr>
</table>
<br />
            <%--<asp:PlaceHolder ID="PlaceHolder1" runat="server"></asp:PlaceHolder>--%>
                 
                    
                <asp:SqlDataSource ID="sqldsComplianceKPI" runat="server"
                    ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
                    SelectCommand="_Kpi_GetReport_Compliance" SelectCommandType="StoredProcedure">
                    <SelectParameters>
                        <asp:SessionParameter
                            DefaultValue="2007" Name="sYear" SessionField="spVar_Year" Type="Int32" 
                        />
                        <asp:SessionParameter DefaultValue="1" Name="RegionId" SessionField="spVar_RegionId"
                            Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>
                
                <asp:SqlDataSource ID="sqldsKPIYear" runat="server"
                    ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
                    ProviderName="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString.ProviderName %>"
                    SelectCommand="SELECT DISTINCT(YEAR(kpiDateTime)) As [Year] FROM dbo.KPI ORDER BY [Year] ASC">
                </asp:SqlDataSource>
                
                <asp:SqlDataSource ID="sqldsCompaniesList" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="_Companies_GetCompanyNameList" SelectCommandType="StoredProcedure">
</asp:SqlDataSource>
                
                <data:CompanySiteCategoryDataSource ID="CompanySiteCategoryDataSource" runat="server" SelectMethod="GetPaged"
            EnablePaging="True" EnableSorting="True">
        </data:CompanySiteCategoryDataSource>
                <data:SitesDataSource ID="dsSitesFilter" runat="server" Filter="IsVisible = True" Sort="SiteName ASC"></data:SitesDataSource>    
                <data:RegionsDataSource ID="dsRegionsFilter" runat="server" Filter="IsVisible = True" Sort="Ordinal ASC"></data:RegionsDataSource>
