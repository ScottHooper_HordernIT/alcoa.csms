//Modifications

//DT2709 : Modifications done by Vikas Naidu for 3/07/2013


using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Services;
using KaiZen.CSMS.Data;
using System.Drawing;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.Data;
using DevExpress.Web.ASPxEditors;
using System.Collections.Generic;
using System.Security.Principal;
using KaiZen.Library;
using System.IO;
using DevExpress.Web.ASPxGridView.Export;
using model = Repo.CSMS.DAL.EntityModels;
using repo = Repo.CSMS.Service.Database;
using System.Linq;

public partial class UserControls_SqQuestionnaire_QuestionnaireModify : System.Web.UI.UserControl
{
    Auth auth = new Auth();
    bool requalify = false;
    bool panelRecommend_Visible = true;
    string qid = string.Empty;
    repo.IEmailLogService emailLogService = ALCOA.CSMS.Website.Global.GetInstance<repo.IEmailLogService>();
    repo.IEmailTemplateService emailTemplateService = ALCOA.CSMS.Website.Global.GetInstance<repo.IEmailTemplateService>();
    repo.ICompanyService companyService = ALCOA.CSMS.Website.Global.GetInstance<repo.ICompanyService>();
    repo.IQuestionnaireInitialResponseService qirService = ALCOA.CSMS.Website.Global.GetInstance<repo.IQuestionnaireInitialResponseService>();
    repo.IQuestionnaireService questionnaireService = ALCOA.CSMS.Website.Global.GetInstance<repo.IQuestionnaireService>();
    repo.ICompanySiteCategoryExceptionService companySiteCategoryExceptionService = ALCOA.CSMS.Website.Global.GetInstance<repo.ICompanySiteCategoryExceptionService>();
    repo.ICompanySiteCategoryStandardService companySiteCategoryStandardService = ALCOA.CSMS.Website.Global.GetInstance<repo.ICompanySiteCategoryStandardService>();
    repo.ICompanySiteCategoryService companySiteCategoryService = ALCOA.CSMS.Website.Global.GetInstance<repo.ICompanySiteCategoryService>();
    repo.IQuestionnaireInitialContactService questionnaireInitialContactService = ALCOA.CSMS.Website.Global.GetInstance<repo.IQuestionnaireInitialContactService>();
    protected void Page_Load(object sender, EventArgs e) { Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); }

    private void moduleInit(bool postBack)
    {
        ActionLog(postBack);
        if (!postBack)
        {
            switch (SessionHandler.spVar_Questionnaire_FrontPageLink)
            {
                case "QuestionnaireReport_Overview":
                    hlQuestionnaire.Text = "Australian Safety Qualification Information > Qualified Contractors";
                    hlQuestionnaire.ToolTip = "Go back to Qualified Contractors Page";
                    hlQuestionnaire.NavigateUrl = "~/Reports_SafetyPQOverview.aspx";
                    break;
                case "QuestionnaireReport_Expire":
                    hlQuestionnaire.Text = "Australian Safety Qualification Information > Expiry Date";
                    hlQuestionnaire.ToolTip = "Go back to SQ Expiry Date Page";
                    hlQuestionnaire.NavigateUrl = "~/Reports_SafetyPQ.aspx?q=expire";
                    break;
                case "QuestionnaireReport_Progress":
                    hlQuestionnaire.Text = "Australian Safety Qualification Information > Status";
                    hlQuestionnaire.ToolTip = "Go back to SQ Status Page";
                    hlQuestionnaire.NavigateUrl = "~/Reports_SafetyPQStatus.aspx";
                    break;
                case "EbiContractorsOnSite":
                    hlQuestionnaire.Text = "Reports / Charts > Compliance Reports > Contractors On-Site (EBI)";
                    hlQuestionnaire.ToolTip = "Go back to Contractors On-Site (EBI) Page";
                    hlQuestionnaire.NavigateUrl = "~/NonComplianceEbiOnSite.aspx";
                    break;
                default: //Questionnaire
                    hlQuestionnaire.Text = "Australian Safety Qualification Process";
                    hlQuestionnaire.ToolTip = "Go back to Safety Qualification Page";
                    hlQuestionnaire.NavigateUrl = "~/SafetyPQ_Questionnaire.aspx";
                    break;
            }

            //Initialisation
            bool print = false;
            string q = "";
            lblStatus.Visible = false;
            cbQuestionnaireStatus.ReadOnly = true;
            btnSave.Visible = false;

            //QuestionnaireService qsService = new QuestionnaireService();
            //Questionnaire _qs = new Questionnaire();
            //_qs = qsService.GetByQuestionnaireId(Convert.ToInt32(q));

            if (auth.RoleId == (int)RoleList.Contractor || auth.RoleId == (int)RoleList.PreQual || auth.RoleId == (int)RoleList.Disabled)
            {
                ASPxPageControl1.TabPages[1].Visible = false;
            }
            NoteAccessList curNoteAccess = Helper.Questionnaire.CompanyNoteEntry.IsEligibleToAddCompanyNote(auth.UserId, auth.RoleId, auth.CompanyId);
            // Uncommented by Swagato, moved to another place
            //if (!(curNoteAccess == NoteAccessList.Administrator || curNoteAccess == NoteAccessList.Ehs || curNoteAccess == NoteAccessList.Procurement))
            //{
            //    ASPxPageControl1.TabPages.FindByName("CompanyNotes").ClientVisible = false;
            //}
            //else
            //{
            //    odsActionNote.SelectParameters["_CreatedByCompanyId"] = new Parameter("_CreatedByCompanyId", DbType.Int32, ViewState["compID"].ToString());
            // //   odsActionNote.SelectParameters["_CreatedByCompanyId"] = new Parameter("_CreatedByCompanyId", DbType.Int32, _qs.CompanyId.ToString());
            //    odsActionNote.SelectParameters["_Visibility"] = new Parameter("_Visibility", DbType.String, Utility.GetEnumTextValue(curNoteAccess));
            //}

            //Add by By Tamal Patra
            ASPxGridViewExporter exCompanyNote = (ASPxGridViewExporter)ExportButtons1.FindControl("gridExporter");
            exCompanyNote.GridViewID = "gridCompanyNote";
            exCompanyNote.FileName = "Company Notes";

            
            //Required as Control.ClientID != Control.ID due to being inside roundpanel
            popupProcurementContact.PopupElementID = hlChangeProc.ClientID;
            popupFunctionalManager.PopupElementID = hlChangeFunctionalManager.ClientID;
            popupHSAssessor.PopupElementID = hlChangeHSAssessor.ClientID;

            try
            {
                if (Request.QueryString["q"] != null)
                {
                    q = Request.QueryString["q"];
                }
                else
                {
                    throw new Exception("Questionnaire Not Specified...");
                }

                if (Request.QueryString["printlayout"] != null)
                {
                    if (Request.QueryString["printlayout"] == "yes") print = true;
                }

                if (q != "New")
                {
                    lblMode.Text = "Assess/Review";

                    QuestionnaireService qService = new QuestionnaireService();
                    Questionnaire _q = new Questionnaire();
                    _q = qService.GetByQuestionnaireId(Convert.ToInt32(q));

                    LoadExistingCompanyStatusChangeRequestIfExists(_q);
                    CheckIfSafetyQualificationExemptionExists(_q);

                    if (auth.RoleId == (int)RoleList.Administrator || Helper.General.isEhsConsultant(auth.UserId))
                    {
                        hlNew.NavigateUrl = String.Format("javascript:popUp('../PopUps/EditSqExemption.aspx{0}');",
                                                       QueryStringModule.Encrypt(String.Format("c={0}",
                                                                    _q.CompanyId)));
                    }
                    else
                    {
                        //gvSqExemption.Columns[0].Visible = false;
                        hlNew.Visible = false;
                    }

                    BindLocationData(_q.CompanyId);

                    QuestionnairePresentlyWithActionService qpwaService = new QuestionnairePresentlyWithActionService();
                    if (_q.QuestionnairePresentlyWithActionId != null)
                    {
                        QuestionnairePresentlyWithAction qpwa = qpwaService.GetByQuestionnairePresentlyWithActionId(Convert.ToInt32(_q.QuestionnairePresentlyWithActionId));
                        if (qpwa != null)
                        {
                            //string dt = "";

                            lblPresentlyWith.Text = qpwa.ActionDescription;
                            if (qpwa.ActionDescription != "-" && _q.QuestionnairePresentlyWithSince != null)
                            {
                                lblPresentlyWith.Text += " at " + _q.QuestionnairePresentlyWithSince.ToString();
                            }
                        }
                    }
                    else
                    {
                        Exception ex = new Exception("SQ Modify Error: QuestionnairePresentlyWithActionId is null!");
                        Elmah.ErrorSignal.FromContext(Context).Raise(ex);
                    }

                    btnClose.PostBackUrl = "";

                    Configuration configuration = new Configuration();

                    //Check if user is authorised to view page
                    if (auth.RoleId == (int)RoleList.Contractor || auth.RoleId == (int)RoleList.PreQual)
                    {
                        int RsCount = 0;
                        QuestionnaireContractorRsFilters queryContractorRs = new QuestionnaireContractorRsFilters();
                        queryContractorRs.Append(QuestionnaireContractorRsColumn.ContractorCompanyId, auth.CompanyId.ToString());
                        queryContractorRs.Append(QuestionnaireContractorRsColumn.SubContractorCompanyId, _q.CompanyId.ToString());
                        TList<QuestionnaireContractorRs> listContractorRs = DataRepository.QuestionnaireContractorRsProvider.GetPaged(queryContractorRs.ToString(), null, 0, 100, out RsCount);

                        if (_q.CompanyId != auth.CompanyId && RsCount == 0)
                        {
                            throw new Exception("You are not authorised to view this page.");
                        }
                    }
                    
                    if (auth.RoleId == (int)RoleList.Administrator)
                    {
                        btnForceReQualify.Visible = true;  // Jolly - Enhancement Spec - 90
                        btnForceReQualify.Enabled = true;  // Jolly - Enhancement Spec - 90
                        btnForceReQualify3.Visible = true;
                        btnForceReQualify2.Visible = true;
                        if (_q.SubContractor != null)
                        {
                            if ((bool)_q.SubContractor)
                            {
                                btnForceReQualify2.Enabled = true;
                            }
                            else
                            {
                                btnForceReQualify3.Enabled = true;
                            }
                        }

                        btnDelete.Enabled = true;

                        if (_q.IsVerificationRequired == false)
                        {
                            btnForceRequireVerification.Enabled = true;
                        }
                        else
                        {
                            btnForceDeleteVerification.Enabled = true;
                        }
                    }
                    
                    if (_q.Status == (int)QuestionnaireStatusList.AssessmentComplete)
                    {
                        btnDelete.Enabled = false;

                        if (auth.RoleId != (int)RoleList.Contractor && auth.RoleId != (int)RoleList.PreQual)
                        {
                            CompaniesService _cService = new CompaniesService();
                            Companies _c = _cService.GetByCompanyId(_q.CompanyId);
                            if (_c.CompanyStatusId == (int)CompanyStatusList.Being_Assessed)
                            {
                                panelBox.Visible = true;
                                lblBox1.Text = "Awaiting Assignment";
                                lblBox2.Text = "This Company's Safety Qualification has been assessed as complete.<br /> Please change the Company Status accordingly from its current 'Being Assessed' status.<br /><br /><div align='left'>This can be done by:<br /><strong>1) </strong>Selecting the most appropriate option in the Company Status drop down list:<br /><strong>2) </strong>Clicking the change button next to it.</div>";
                                lblBox2_PopUp.Visible = false;
                            }
                        }

                        DataSet ds = qService.GetLatestAssessmentComplete_ByCompanyId(_q.CompanyId);
                        if (ds.Tables[0] == null) throw new Exception("Table Null.");
                        if (ds.Tables[0].Rows.Count != 1) throw new Exception("Table Null.");

                        if ((int)ds.Tables[0].Rows[0].ItemArray[0] == _q.QuestionnaireId)
                        {
                            DateTime dtNow = DateTime.Now;
                            DateTime? dtExpiry = _q.MainAssessmentValidTo;
                            if (dtExpiry != null)
                            {
                                TimeSpan ts = Convert.ToDateTime(dtExpiry) - dtNow;
                                if (ts.Days <= 60)
                                {
                                    try
                                    {
                                        //check requal
                                        QuestionnaireService qService0 = new QuestionnaireService();
                                        DataSet qDataSet = qService0.GetLastModifiedQuestionnaireId(_q.CompanyId);
                                        if (qDataSet.Tables[0] == null) throw new Exception("Table2 Null.");
                                        if (qDataSet.Tables[0].Rows.Count != 1) throw new Exception("Table2 Null.");

                                        string LatestQuestionnaireId = qDataSet.Tables[0].Rows[0].ItemArray[0].ToString();
                                        
                                        if (!String.IsNullOrEmpty(LatestQuestionnaireId))
                                        {
                                            if (_q.QuestionnaireId == Convert.ToInt32(LatestQuestionnaireId))
                                            {
                                                btnReQualify.Enabled = true;
                                                requalify = true;

                                                //show requal message (FOR CONTRACTOR)
                                                panelBox.Visible = true;
                                                if (ts.Days <= 0)
                                                {
                                                    lblBox1.Text = "Safety Qualification Expired";
                                                    if (auth.RoleId == (int)RoleList.Contractor || auth.RoleId == (int)RoleList.PreQual)
                                                    {
                                                        lblBox2.Text = "Your Company's Safety Qualification has Expired. If your company is required to do work on-site now (or in the future) you need to have your safety qualification re-qualified (up-to-date). Please Contact your Alcoa Contact.";
                                                    }
                                                    else
                                                    {
                                                        notifysupplier(Convert.ToInt32(q), ts.Days);

                                                        lblBox2.Text = "This Company's Safety Qualification has Expired. If you wish to utilise the services of this service provider, this company needs to be re-qualified prior to access to site being granted.<br /><br /><div align='left'>This can be done by:<br /><strong>1) </strong>Clicking the Re-Qualify button to initiate the re-qualification process.<br /><br />Alternatively to disable the re-qualification process for this company:<br /><strong>1) </strong>Change the company status to '<i>InActive - No Re-Qualification Required</i>' (if you have not done so already).</div>";
                                                    }
                                                }
                                                else
                                                {
                                                    lblBox1.Text = "Safety Qualification Due to Expire";
                                                    if (auth.RoleId == (int)RoleList.Contractor || auth.RoleId == (int)RoleList.PreQual)
                                                    {
                                                        lblBox2.Text = "Your Company's Safety Qualification is due to expire and needs to be Re-Validated prior to " + Convert.ToDateTime(dtExpiry).ToShortDateString() + " Please contact your Alcoa Contact.";
                                                    }
                                                    else
                                                    {
                                                        notifysupplier(Convert.ToInt32(q), ts.Days);
                                                        lblBox2.Text = "This Company's Safety Qualification is due to expire and needs to be Re-Validated prior to " + Convert.ToDateTime(dtExpiry).ToShortDateString() + ". If you wish to utilise the services of this service provider, this company needs to be re-qualified prior to access to site being granted.<br /><br /><div align='left'>This can be done by:<br /><strong>1)</strong> Clicking the Re-Qualify button to initiate the re-qualification process.<br /><br />Alternatively to disable the re-qualification process for this company:<br /><strong>1) </strong>Change the company status to '<i>InActive - No Re-Qualification Required</i>' (if you have not done so already).</div>";
                                                    }
                                                }
                                                lblBox2_PopUp.Visible = false;
                                            }
                                            else
                                            {
                                                panelBox.Visible = true;
                                                lblBox1.Text = "Old Questionnaire";
                                                lblBox2.Text = "You are viewing an old questionnaire which has been superseded.<br />As such this Questionnaire is READ-ONLY for historical viewing purposes only.<br /><br />";

                                                hlLatestSQ.NavigateUrl = "~/SafetyPQ_QuestionnaireModify.aspx" + QueryStringModule.Encrypt("q=" + LatestQuestionnaireId);
                                                hlLatestSQ.Visible = true;
                                                //Don't let users change stuff...
                                                hlChangeCompanyStatus.Enabled = false;
                                                btnChangeCompanyStatus.Enabled = false;
                                                cbChangeCompanyStatus.Enabled = false;
                                                cbChangeCompanyStatus.ReadOnly = true;
                                                hlChangeHSAssessor.Enabled = false;
                                                btnChangeHSAssessor.Enabled = false;
                                                popupHSAssessor.PopupElementID = "hlChangeHSAssessor_no";
                                            }
                                        }
                                        qService0 = null;
                                        qDataSet.Dispose();
                                    }
                                    catch (Exception ex)
                                    {
                                        Elmah.ErrorSignal.FromContext(Context).Raise(ex);
                                    }
                                }
                            }
                        }
                        else
                        {
                            QuestionnaireService qService0 = new QuestionnaireService();
                            DataSet qDataSet = qService0.GetLastModifiedQuestionnaireId(_q.CompanyId);
                            if (qDataSet.Tables[0] == null) throw new Exception("Table2 Null.");
                            if (qDataSet.Tables[0].Rows.Count != 1) throw new Exception("Table2 Null.");

                            string LatestQuestionnaireId = qDataSet.Tables[0].Rows[0].ItemArray[0].ToString();

                            if (!String.IsNullOrEmpty(LatestQuestionnaireId))
                            {
                                panelBox.Visible = true;
                                lblBox1.Text = "Old Questionnaire";
                                lblBox2.Text = "You are viewing an old questionnaire which has been superseded.<br />As such this Questionnaire is READ-ONLY for historical viewing purposes only.<br /><br />";

                                hlLatestSQ.NavigateUrl = "~/SafetyPQ_QuestionnaireModify.aspx" + QueryStringModule.Encrypt("q=" + LatestQuestionnaireId);
                                hlLatestSQ.Visible = true;
                                //Don't let users change stuff...
                                hlChangeCompanyStatus.Enabled = false;
                                btnChangeCompanyStatus.Enabled = false;
                                cbChangeCompanyStatus.Enabled = false;
                                cbChangeCompanyStatus.ReadOnly = true;
                                hlChangeHSAssessor.Enabled = false;
                                btnChangeHSAssessor.Enabled = false;
                                popupHSAssessor.PopupElementID = "hlChangeHSAssessor_no";
                            }
                        }
                    }

                    if (_q.SubContractor == true)
                    {
                        cbChangeCompanyStatus.DataBind();
                        //don't show:
                        cbChangeCompanyStatus.Items.RemoveAt(9); //Active - SQ Complete (will appear in database, Access to Site)

                        if (auth.CompanyStatus2Id == (int)CompanyStatus2List.SubContractor)
                        {
                            hlQuestionnaire.Text = "WA SubContractor Process";
                            hlQuestionnaire.ToolTip = "Go back to WA SubContractor Process Page";
                            hlQuestionnaire.NavigateUrl = "~/Default.aspx";
                        }
                    }
                    else
                    {
                        cbChangeCompanyStatus.DataBind();
                        //don't show:
                        cbChangeCompanyStatus.Items.RemoveAt(7); //SubContractor - SQ Complete, Not Recommended, No Access to Site (in Status Report)
                        cbChangeCompanyStatus.Items.RemoveAt(7); //SubContractor - SQ Complete, Recommended (will appear in database)

                    }

                    cbEhsConsultant.DataSourceID = "UsersEhsConsultantsDataSource2";
                    cbEhsConsultant.TextField = "UserFullName";
                    cbEhsConsultant.ValueField = "EhsConsultantId";
                    cbEhsConsultant.DataBind();
                    cbEhsConsultant.Items.Add("(Not Assigned)", null);

                    if (auth.RoleId == (int)RoleList.Administrator || auth.Email == configuration.GetValue(ConfigList.DefaultEhsConsultant_WAO)
                                                                   || auth.Email == configuration.GetValue(ConfigList.DefaultEhsConsultant_VICOPS))
                    {
                        cbEhsConsultant.ReadOnly = false;
                        cbHSAssessor_New.ReadOnly = false;

                        hlChangeHSAssessor.Visible = true;
                        btnChangeHSAssessor.Visible = true;
                    }


                    UsersProcurementService uProcurementService = new UsersProcurementService();
                    UsersProcurement uProcurement = uProcurementService.GetByUserId(auth.UserId);
                    if (auth.RoleId == (int)RoleList.Administrator)
                    {
                        cbChangeCompanyStatus.ReadOnly = false;

                        hlChangeCompanyStatus.Visible = true;
                        btnChangeCompanyStatus.Visible = true;

                        hlChangeProc.Visible = true;
                        hlChangeFunctionalManager.Visible = true;
                        btnChangeProc.Visible = true;
                        btnChangeFunctionalManager.Visible = true;
                        if (requalify)
                        {
                            try
                            {
                                //check requal
                                QuestionnaireService qService1 = new QuestionnaireService();
                                DataSet qDataSet = qService1.GetLastModifiedQuestionnaireId(auth.CompanyId);

                                string LatestQuestionnaireId = "";
                                foreach (DataRow dr in qDataSet.Tables[0].Rows)
                                {
                                    LatestQuestionnaireId = dr[0].ToString();
                                }

                                if (!String.IsNullOrEmpty(LatestQuestionnaireId))
                                {
                                    if (_q.QuestionnaireId == Convert.ToInt32(LatestQuestionnaireId))
                                    {
                                        btnReQualify.Enabled = true;
                                        requalify = true;

                                        DateTime? dtExpiry = _q.MainAssessmentValidTo;
                                        if (dtExpiry != null)
                                        {
                                            TimeSpan ts = Convert.ToDateTime(dtExpiry) - DateTime.Now;

                                            //show requal message (FOR CONTRACTOR)
                                            panelBox.Visible = true;
                                            if (ts.Days <= 0)
                                            {
                                                lblBox1.Text = "Safety Qualification Expired";
                                                if (auth.RoleId == (int)RoleList.Contractor || auth.RoleId == (int)RoleList.PreQual)
                                                {
                                                    lblBox2.Text = "Your Company's Safety Qualification has Expired. If your company is required to do work on-site now (or in the future) you need to have your safety qualification re-qualified (up-to-date). Please Contact your Alcoa Contact.";
                                                }
                                                else
                                                {
                                                    notifysupplier(Convert.ToInt32(q), ts.Days);
                                                    lblBox2.Text = "This Company's Safety Qualification has Expired. If you wish to utilise the services of this service provider, this company needs to be re-qualified prior to access to site being granted.<br /><br /><div align='left'>This can be done by:<br /><strong>1) </strong>Clicking the Re-Qualify button to initiate the re-qualification process.<br /><br />Alternatively to disable the re-qualification process for this company:<br /><strong>1) </strong>Change the company status to '<i>InActive - No Re-Qualification Required</i>' (if you have not done so already).</div>";
                                                }
                                            }
                                            else
                                            {
                                                lblBox1.Text = "Safety Qualification Due to Expire";
                                                if (auth.RoleId == (int)RoleList.Contractor || auth.RoleId == (int)RoleList.PreQual)
                                                {
                                                    lblBox2.Text = "Your Company's Safety Qualification is due to expire and needs to be Re-Validated prior to " + Convert.ToDateTime(dtExpiry).ToShortDateString() + " Please contact your Alcoa Contact.";
                                                }
                                                else
                                                {
                                                    notifysupplier(Convert.ToInt32(q), ts.Days);
                                                    lblBox2.Text = "This Company's Safety Qualification is due to expire and needs to be Re-Validated prior to " + Convert.ToDateTime(dtExpiry).ToShortDateString() + ". If you wish to utilise the services of this service provider, this company needs to be re-qualified prior to access to site being granted.<br /><br /><div align='left'>This can be done by:<br /><strong>1)</strong> Clicking the Re-Qualify button to initiate the re-qualification process.<br /><br />Alternatively to disable the re-qualification process for this company:<br /><strong>1) </strong>Change the company status to '<i>InActive - No Re-Qualification Required</i>' (if you have not done so already).</div>";
                                                }
                                            }
                                            lblBox2_PopUp.Visible = false;
                                        }
                                    }
                                    else
                                    {
                                        panelBox.Visible = true;
                                        lblBox1.Text = "Old Questionnaire";
                                        lblBox2.Text = "You are viewing an old questionnaire which has been superseded.<br />As such this Questionnaire is READ-ONLY for historical viewing purposes only.<br /><br />";

                                        hlLatestSQ.NavigateUrl = "~/SafetyPQ_QuestionnaireModify.aspx" + QueryStringModule.Encrypt("q=" + LatestQuestionnaireId);
                                        hlLatestSQ.Visible = true;
                                    }
                                }
                                qService1 = null;
                                qDataSet.Dispose();
                            }
                            catch (Exception ex)
                            {
                                Elmah.ErrorSignal.FromContext(Context).Raise(ex);
                            }
                        }
                    }
                    if (uProcurement != null)
                    {
                        if (uProcurement.Enabled == true)
                        {
                            btnChangeProc.Visible = true;
                            btnChangeFunctionalManager.Visible = true;

                            if (requalify)
                            {
                                try
                                {
                                    //check requal
                                    QuestionnaireService qService2 = new QuestionnaireService();
                                    DataSet qDataSet = qService2.GetLastModifiedQuestionnaireId(auth.CompanyId);

                                    string LatestQuestionnaireId = "";
                                    foreach (DataRow dr in qDataSet.Tables[0].Rows)
                                    {
                                        LatestQuestionnaireId = dr[0].ToString();
                                    }

                                    if (!String.IsNullOrEmpty(LatestQuestionnaireId))
                                    {
                                        if (_q.QuestionnaireId == Convert.ToInt32(LatestQuestionnaireId))
                                        {
                                            btnReQualify.Enabled = true;
                                            requalify = true;

                                            DateTime? dtExpiry = _q.MainAssessmentValidTo;
                                            if (dtExpiry != null)
                                            {
                                                TimeSpan ts = Convert.ToDateTime(dtExpiry) - DateTime.Now;

                                                //show requal message (FOR CONTRACTOR)
                                                panelBox.Visible = true;
                                                if (ts.Days <= 0)
                                                {
                                                    lblBox1.Text = "Safety Qualification Expired";
                                                    if (auth.RoleId == (int)RoleList.Contractor || auth.RoleId == (int)RoleList.PreQual)
                                                    {
                                                        lblBox2.Text = "Your Company's Safety Qualification has Expired. If your company is required to do work on-site now (or in the future) you need to have your safety qualification re-qualified (up-to-date). Please Contact your Alcoa Contact.";
                                                    }
                                                    else
                                                    {
                                                        notifysupplier(Convert.ToInt32(q), ts.Days);
                                                        lblBox2.Text = "This Company's Safety Qualification has Expired. If you wish to utilise the services of this service provider, this company needs to be re-qualified prior to access to site being granted.<br /><br /><div align='left'>This can be done by:<br /><strong>1) </strong>Clicking the Re-Qualify button to initiate the re-qualification process.<br /><br />Alternatively to disable the re-qualification process for this company:<br /><strong>1) </strong>Change the company status to '<i>InActive - No Re-Qualification Required</i>' (if you have not done so already).</div>";
                                                    }
                                                }
                                                else
                                                {
                                                    lblBox1.Text = "Safety Qualification Due to Expire";
                                                    if (auth.RoleId == (int)RoleList.Contractor || auth.RoleId == (int)RoleList.PreQual)
                                                    {
                                                        lblBox2.Text = "Your Company's Safety Qualification is due to expire and needs to be Re-Validated prior to " + Convert.ToDateTime(dtExpiry).ToShortDateString() + " Please contact your Alcoa Contact.";
                                                    }
                                                    else
                                                    {
                                                        notifysupplier(Convert.ToInt32(q), ts.Days);
                                                        lblBox2.Text = "This Company's Safety Qualification is due to expire and needs to be Re-Validated prior to " + Convert.ToDateTime(dtExpiry).ToShortDateString() + ". If you wish to utilise the services of this service provider, this company needs to be re-qualified prior to access to site being granted.<br /><br /><div align='left'>This can be done by:<br /><strong>1)</strong> Clicking the Re-Qualify button to initiate the re-qualification process.<br /><br />Alternatively to disable the re-qualification process for this company:<br /><strong>1) </strong>Change the company status to '<i>InActive - No Re-Qualification Required</i>' (if you have not done so already).</div>";
                                                    }
                                                }
                                                lblBox2_PopUp.Visible = false;
                                            }
                                        }
                                        else
                                        {
                                            panelBox.Visible = true;
                                            lblBox1.Text = "Old Questionnaire";
                                            lblBox2.Text = "You are viewing an old questionnaire which has been superseded.<br />As such this Questionnaire is READ-ONLY for historical viewing purposes only.<br /><br />";

                                            hlLatestSQ.NavigateUrl = "~/SafetyPQ_QuestionnaireModify.aspx" + QueryStringModule.Encrypt("q=" + LatestQuestionnaireId);
                                            hlLatestSQ.Visible = true;
                                        }
                                    }
                                    qService2 = null;
                                    qDataSet.Dispose();
                                }
                                catch (Exception ex)
                                {
                                    Elmah.ErrorSignal.FromContext(Context).Raise(ex);
                                }
                            }

                            if (_q.Status == (int)QuestionnaireStatusList.AssessmentComplete)
                            {
                                panelRecommend.Visible = true;
                                rbRecommended.Enabled = false;
                                cbDirect.Enabled = false;
                                cbSupervisionQuestionnaire.Enabled = false;
                                mRecommended.Enabled = false;
                                detailGrid.Columns[0].Visible = false;
                                detailGrid.Enabled = false;

                                cbChangeCompanyStatus.ReadOnly = false;

                                hlChangeCompanyStatus.Visible = true;
                                btnChangeCompanyStatus.Enabled = true;
                                btnChangeProc.Visible = true;
                                btnChangeFunctionalManager.Visible = true;

                            }
                            else
                            {
                                cbChangeCompanyStatus.ReadOnly = false;
                                btnChangeCompanyStatus.Enabled = true;
                                hlChangeCompanyStatus.Visible = true;
                            }
                        }
                    }

                    LoadEhs(Convert.ToInt32(q));

                    if (_q.IsReQualification == true)
                    {
                        cbSqType.Value = 1;
                    }
                    else
                    {
                        cbSqType.Value = 0;
                    }

                    string qsStatus, qsInitialStatus, qsMainStatus, qsVerificationStatus;
                    QuestionnaireStatus qs = new QuestionnaireStatus();
                    QuestionnaireStatusService qss = new QuestionnaireStatusService();
                    qs = qss.GetByQuestionnaireStatusId(_q.Status);
                    qsStatus = qs.QuestionnaireStatusDesc;
                    lblStatus.Text = qsStatus;
                    qs = qss.GetByQuestionnaireStatusId(_q.InitialStatus);
                    qsInitialStatus = qs.QuestionnaireStatusDesc;
                    qs = qss.GetByQuestionnaireStatusId(_q.MainStatus);
                    qsMainStatus = qs.QuestionnaireStatusDesc;
                    qs = qss.GetByQuestionnaireStatusId(_q.VerificationStatus);
                    qsVerificationStatus = qs.QuestionnaireStatusDesc;
                    ViewState["compID"] = _q.CompanyId;
                    CompaniesService companiesService = new CompaniesService();
                    Companies companiesEntity = companiesService.GetByCompanyId(_q.CompanyId);
                    lblCompanyName.Text = companiesEntity.CompanyName;
                    ViewState["CompName"] = companiesEntity.CompanyName;

                    // Added by Swagato - moved from top
                    if (!(curNoteAccess == NoteAccessList.Administrator || curNoteAccess == NoteAccessList.Ehs || curNoteAccess == NoteAccessList.Procurement))
                    {
                        ASPxPageControl1.TabPages.FindByName("CompanyNotes").ClientVisible = false;
                    }
                    else
                    {
                        odsActionNote.SelectParameters["_CreatedByCompanyId"] = new Parameter("_CreatedByCompanyId", DbType.Int32, ViewState["compID"].ToString());
                        //   odsActionNote.SelectParameters["_CreatedByCompanyId"] = new Parameter("_CreatedByCompanyId", DbType.Int32, _qs.CompanyId.ToString());
                        odsActionNote.SelectParameters["_Visibility"] = new Parameter("_Visibility", DbType.String, Utility.GetEnumTextValue(curNoteAccess));
                    }

                    if (_q.SubContractor == true)
                    {
                        lblSubcontractor.Text = "Sub Contractor";
                    }
                    else
                    {
                        lblSubcontractor.Text = "Contractor";
                    }

                    cbCompanyStatus.Value = companiesEntity.CompanyStatusId;
                    CompanyStatusService csService = new CompanyStatusService();
                    CompanyStatus cs = csService.GetByCompanyStatusId(companiesEntity.CompanyStatusId);
                    if (cs != null)
                    {
                        lblCompanyStatus.Text = cs.CompanyStatusDesc;
                    }

                    UsersService usersService = new UsersService();
                    Users userEntity;
                    userEntity = usersService.GetByUserId(_q.CreatedByUserId);
                    Companies companiesEntity2 = companiesService.GetByCompanyId(userEntity.CompanyId);
                    lblCreatedBy.Text = String.Format("{0}, {1} ({2}) at {3}", userEntity.LastName, userEntity.FirstName, companiesEntity2.CompanyName, _q.CreatedDate);
                    userEntity = usersService.GetByUserId(_q.ModifiedByUserId);
                    Companies companiesEntity3 = companiesService.GetByCompanyId(userEntity.CompanyId);
                    lblSubmittedBy.Text = String.Format("{0}, {1} ({2}) at {3}", userEntity.LastName, userEntity.FirstName, companiesEntity3.CompanyName, _q.ModifiedDate);
                    if (lblCreatedBy.Text == lblSubmittedBy.Text) lblSubmittedBy.Text = "-";
                    if (_q.Status == (int)QuestionnaireStatusList.Incomplete && _q.AssessedByUserId == null && _q.ApprovedByUserId == null)
                    { //If Incomplete & Not being Re-Submitted, Blank out lblSubmitted.
                        lblSubmittedBy.Text = "-";
                    }

                    //Show Latest Expiry Date
                    bool? expired = null;
                    if (_q.MainAssessmentValidTo != null)
                    {
                        lblExpiresAt.Text = _q.MainAssessmentValidTo.ToString();

                        if (_q.MainAssessmentValidTo <= DateTime.Now)
                            expired = true;
                        else
                            expired = false;
                    }
                    else
                    {
                        if (_q.IsReQualification)
                        {
                            int count = 0;
                            QuestionnaireFilters qFilters = new QuestionnaireFilters();
                            qFilters.Append(QuestionnaireColumn.CompanyId, _q.CompanyId.ToString());
                            qFilters.Append(QuestionnaireColumn.Status, ((int)QuestionnaireStatusList.AssessmentComplete).ToString());
                            qFilters.AppendLessThan(QuestionnaireColumn.CreatedDate, _q.CreatedDate.ToString("yyyy-MM-dd"));
                            TList<Questionnaire> qList =
                                DataRepository.QuestionnaireProvider.GetPaged(qFilters.ToString(), "CreatedDate DESC", 0, 100, out count);

                            if (count > 0)
                            {
                                Questionnaire qOld = (Questionnaire)qList[0];
                                if (qOld.MainAssessmentValidTo != null)
                                {
                                    lblExpiresAt.Text = qOld.MainAssessmentValidTo.ToString();
                                    hlPreviousQuestionnaire.NavigateUrl = "~/SafetyPQ_QuestionnaireModify.aspx" + QueryStringModule.Encrypt("q=" + qOld.QuestionnaireId);
                                    hlPreviousQuestionnaire.Visible = true;
                                    if (qOld.MainAssessmentValidTo <= DateTime.Now)
                                        expired = true;
                                    else
                                        expired = false;
                                }
                            }
                        }
                    }

                    if (expired == true)
                    {
                        lblExpiresAt.ForeColor = Color.Red;
                        lblExpiresAtText.Text = "Expired At:";
                    }
                    else if (expired == false)
                    {
                        lblExpiresAt.ForeColor = Color.Green;
                        lblExpiresAtText.Text = "Valid Until:";
                    }


                    if (_q.ApprovedByUserId != null)
                    {
                        int i = Convert.ToInt32(_q.ApprovedByUserId);
                        userEntity = usersService.GetByUserId(i);
                        Companies companiesEntity4 = companiesService.GetByCompanyId(userEntity.CompanyId);
                        lblAssessedBy.Text = String.Format("{0}, {1} ({2}) at {3}", userEntity.LastName, userEntity.FirstName, companiesEntity4.CompanyName, _q.ApprovedDate);
                    }
                    else
                    {
                        lblAssessedBy.Text = "-";
                        //check if this has been assessed by h&s assesors yet. if so, set name to lblassessedby.

                        if (_q.AssessedByUserId != null)
                        {
                            Users userAssessor = usersService.GetByUserId(Convert.ToInt32(_q.AssessedByUserId));
                            Companies companiesAssessor = companiesService.GetByCompanyId(userAssessor.CompanyId);
                            lblAssessedBy.Text = String.Format("{0}, {1} ({2}) at {3}", userAssessor.LastName, userAssessor.FirstName, companiesAssessor.CompanyName, _q.AssessedDate);
                        }
                        else
                        {
                            //old ones. no assessedbyuserid/assesseddate columns.
                            int QuestionnaireMainAssessmentCount = 0;
                            int QuestionnaireVerificationAssessmentCount = 0;
                            DateTime? QuestionnaireMainAssessmentDateTime = null;
                            DateTime? QuestionnaireVerificationAssessmentDateTime = null;

                            QuestionnaireMainAssessmentFilters queryQuestionnaireMainAssessment = new QuestionnaireMainAssessmentFilters();
                            queryQuestionnaireMainAssessment.Append(QuestionnaireMainAssessmentColumn.QuestionnaireId, _q.QuestionnaireId.ToString());

                            TList<QuestionnaireMainAssessment> QuestionnaireMainAssessmentList = DataRepository.QuestionnaireMainAssessmentProvider.GetPaged(queryQuestionnaireMainAssessment.ToString(), "ModifiedDate DESC, ResponseId DESC", 0, 99999, out QuestionnaireMainAssessmentCount);

                            if (QuestionnaireMainAssessmentCount > 0)
                            {
                                Users userAssessor = usersService.GetByUserId(QuestionnaireMainAssessmentList[0].ModifiedByUserId);
                                Companies companiesAssessor = companiesService.GetByCompanyId(userAssessor.CompanyId);
                                lblAssessedBy.Text = String.Format("{0}, {1} ({2}) at {3}", userAssessor.LastName, userAssessor.FirstName, companiesAssessor.CompanyName, QuestionnaireMainAssessmentList[0].ModifiedDate);
                                QuestionnaireMainAssessmentDateTime = QuestionnaireMainAssessmentList[0].ModifiedDate;
                            }

                            QuestionnaireVerificationAssessmentFilters queryQuestionnaireVerificationAssessment = new QuestionnaireVerificationAssessmentFilters();
                            queryQuestionnaireVerificationAssessment.Append(QuestionnaireVerificationAssessmentColumn.QuestionnaireId, _q.QuestionnaireId.ToString());

                            TList<QuestionnaireVerificationAssessment> QuestionnaireVerificationAssessmentList = DataRepository.QuestionnaireVerificationAssessmentProvider.GetPaged(queryQuestionnaireVerificationAssessment.ToString(), "ModifiedDate DESC, ResponseId DESC", 0, 99999, out QuestionnaireVerificationAssessmentCount);

                            if (QuestionnaireVerificationAssessmentCount > 0)
                            {
                                QuestionnaireVerificationAssessmentDateTime = QuestionnaireVerificationAssessmentList[0].ModifiedDate;

                                if (QuestionnaireMainAssessmentDateTime == null || QuestionnaireVerificationAssessmentDateTime > QuestionnaireMainAssessmentDateTime)
                                {
                                    Users userAssessor = usersService.GetByUserId(QuestionnaireVerificationAssessmentList[0].ModifiedByUserId);
                                    Companies companiesAssessor = companiesService.GetByCompanyId(userAssessor.CompanyId);
                                    lblAssessedBy.Text = String.Format("{0}, {1} ({2}) at {3}", userAssessor.LastName, userAssessor.FirstName, companiesAssessor.CompanyName, QuestionnaireVerificationAssessmentList[0].ModifiedDate);
                                }
                            }
                        }
                    }

                    //0. Set statusbar for mouseover for imgbuttons to blank.
                    img1Initial.Attributes.Add("onmouseover", String.Format("window.status='{0}'", " "));
                    img2Main.Attributes.Add("onmouseover", String.Format("window.status='{0}'", " "));
                    img3Submit.Attributes.Add("onmouseover", String.Format("window.status='{0}'", " "));
                    img3Verification.Attributes.Add("onmouseover", String.Format("window.status='{0}'", " "));
                    //DT 2953
                    lnk4Submit.Attributes.Add("onmouseover", String.Format("window.status='{0}'", " "));


                    //First Disable everything except Initial.
                    img2Main.ImageUrl = "~/Images/Questionnaire/d2.gif";
                    //DT 2953
                    //img3Submit.ImageUrl = "~/Images/Questionnaire/d3s.gif";
                    img3SubmitImg.Src = "~/Images/Questionnaire/d3s.gif";
                    //img4Submit.ImageUrl = "~/Images/Questionnaire/d4s.gif";
                    lnk4SubmitImg.Src = "~/Images/Questionnaire/d4s.gif";
                    img3Verification.ImageUrl = "~/Images/Questionnaire/d3.gif";
                    img2Main.PostBackUrl = "javascript:alert('Previous Questionnaire not completed.');void(0);";
                    img3Verification.PostBackUrl = "javascript:alert('Previous Questionnaire not completed.');void(0);";
                    img3Submit.PostBackUrl = "javascript:alert('You may not submit until all questionnaires have been completed.');void(0);";
                    lnk4Submit.PostBackUrl = "javascript:alert('You may not submit until all questionnaires have been completed.');void(0);";

                    lblStatus3Submit.Visible = false;
                    lblStatus4Submit.Visible = false;

                    //img1Initial.PostBackUrl = "~/SafetyPQ_QuestionnaireInitial.aspx?q=" + _q.QuestionnaireId;
                    img1Initial.PostBackUrl = "~/SafetyPQ_QuestionnaireInitial.aspx" + QueryStringModule.Encrypt("q=" + _q.QuestionnaireId);
                    //lol hardcode.
                    if (_q.Status == 1)
                    {
                        cbQuestionnaireStatus.Text = "Incomplete";
                    }
                    if (_q.Status == 2)
                    {
                        cbQuestionnaireStatus.Text = "Being Assessed";
                    }
                    if (_q.Status == 3)
                    {
                        cbQuestionnaireStatus.Text = "Assessment Complete";
                    }
                    //if (_q.Status == 4)
                    //{
                    //    cbQuestionnaireStatus.Text = "Re-Qualification";
                    //}

                    if (_q.Recommended.HasValue)
                    {
                        rbRecommended.Value = Convert.ToInt32(_q.Recommended);
                    }
                    mRecommended.Text = _q.RecommendedComments;

                    if (!String.IsNullOrEmpty(_q.LevelOfSupervision)) cbDirect.Text = _q.LevelOfSupervision;

                    QuestionnaireInitialResponseService qirService = new QuestionnaireInitialResponseService();
                    QuestionnaireInitialResponse qir = qirService.GetByQuestionnaireIdQuestionId(_q.QuestionnaireId, "2");
                    if (qir != null)
                    {
                        if (!String.IsNullOrEmpty(qir.AnswerText))
                        {
                            cbProcurementView.Value = Convert.ToInt32(qir.AnswerText);

                            try
                            {
                                UsersService uService = new UsersService();
                                Users u = uService.GetByUserId(Convert.ToInt32(qir.AnswerText));
                                if (u != null)
                                {
                                    lblProcContact.Text = u.LastName + ", " + u.FirstName;
                                }
                            }
                            catch (Exception ex)
                            {
                                Elmah.ErrorSignal.FromContext(Context).Raise(ex);
                            }
                            qir.Dispose();
                        }
                    }

                    qir = qirService.GetByQuestionnaireIdQuestionId(_q.QuestionnaireId, "2_1");
                    if (qir != null)
                    {
                        if (!String.IsNullOrEmpty(qir.AnswerText))
                        {
                            cbContract.Value = Convert.ToInt32(qir.AnswerText);

                            try
                            {
                                UsersService uService = new UsersService();
                                Users u = uService.GetByUserId(Convert.ToInt32(qir.AnswerText));
                                if (u != null)
                                {
                                    lblFunctionalManager_Current.Text = u.LastName + ", " + u.FirstName;
                                }
                            }
                            catch (Exception ex)
                            {
                                Elmah.ErrorSignal.FromContext(Context).Raise(ex);
                            }
                            qir.Dispose();

                        }
                    }
                    
                    UsersProcurementService upService = new UsersProcurementService();
                    UsersProcurement up_ = upService.GetByUserId(auth.UserId);
                    if (up_ != null)
                    {
                        if (up_.Enabled == true)
                        {
                            panelProcurement.Visible = true;
                            panelProcurement.Enabled = true;
                        }
                    }

                    if (auth.RoleId == (int)RoleList.Administrator)
                    {
                        panelProcurement.Visible = true;
                        panelProcurement.Enabled = true;
                    }

                    if (_q.InitialStatus == (int)QuestionnaireStatusList.BeingAssessed)
                    {
                        lblStatus1Initial.Text = "Awaiting Reviewal by Lead HS Assessors";
                    }

                    if (_q.InitialStatus == (int)QuestionnaireStatusList.AssessmentComplete)
                    {
                        //UsersProcurementService upService = new UsersProcurementService();
                        //UsersProcurement up = upService.GetByUserId(auth.UserId);
                        //if (auth.RoleId != (int)RoleList.Administrator && up != null)
                        ////if (up != null)
                        //{
                        //    if (up.Enabled == true)
                        //    {
                        //        //Response.Redirect("javascript:alert('Procurement Questionnaire successfully submitted. An email has been sent to the default EHS consultant and the Contractor Management Shared Mailbox.');window.location='SafetyPQ_Questionnaire.aspx';");
                        //    }
                        //}

                        img2Main.ImageUrl = "~/Images/Questionnaire/2.gif";
                        //img2Main.PostBackUrl = "~/SafetyPQ_QuestionnaireMain.aspx?q=" + _q.QuestionnaireId;
                        img2Main.PostBackUrl = "~/SafetyPQ_QuestionnaireMain.aspx" + QueryStringModule.Encrypt("q=" + _q.QuestionnaireId);
                        img1Initial.ImageUrl = "~/Images/Questionnaire/d1.gif";
                        imgArrowLeft1Initial.ImageUrl = "~/Images/greentick.gif";
                        lblStatus1Initial.Text = "Complete";
                        lblStatus1Initial.ForeColor = Color.Black;
                        if (_q.Status == (int)QuestionnaireStatusList.BeingAssessed) panelRecommend_Visible = true;

                        if (_q.IsMainRequired == true)
                        {

                            if (_q.IsVerificationRequired == true)
                            {
                                panel_2.Visible = false;
                                panel_3.Visible = true;

                                if (_q.MainStatus == (int)QuestionnaireStatusList.BeingAssessed)
                                {
                                    img3Verification.ImageUrl = "~/Images/Questionnaire/3.gif";
                                    //img3Verification.PostBackUrl = "~/SafetyPQ_QuestionnaireVerification.aspx?q=" + _q.QuestionnaireId;
                                    img3Verification.PostBackUrl = "~/SafetyPQ_QuestionnaireVerification.aspx" +
                                                                    QueryStringModule.Encrypt("q=" + _q.QuestionnaireId);
                                    img2Main.ImageUrl = "~/Images/Questionnaire/d2.gif";
                                    imgArrowLeft2Main.ImageUrl = "~/Images/greentick.gif";

                                    lblStatus2Main.Text = "Complete";

                                    if (_q.MainStatus == (int)QuestionnaireStatusList.BeingAssessed && _q.Status == (int)QuestionnaireStatusList.BeingAssessed)
                                    {
                                        QuestionnaireMainAssessmentService qmaService = new QuestionnaireMainAssessmentService();
                                        TList<QuestionnaireMainAssessment> qmaTlist_all;
                                        TList<QuestionnaireMainAssessment> qmaTlist_approved;
                                        qmaTlist_all = qmaService.GetByQuestionnaireId(_q.QuestionnaireId);
                                        qmaTlist_approved = qmaService.GetByQuestionnaireIdAssessorApproval(_q.QuestionnaireId, true);
                                        bool approvednull = false;
                                        bool notapproved = false;
                                        if (qmaTlist_all.Count != 22)
                                        {
                                            approvednull = true;
                                        }
                                        else
                                        {
                                            foreach (QuestionnaireMainAssessment qma in qmaTlist_all)
                                            {
                                                if (qma.AssessorApproval == null) approvednull = true;
                                                if (qma.AssessorApproval == false) notapproved = true;
                                            }
                                        }
                                        if (!notapproved && !approvednull)
                                        {
                                            lblStatus2Main.Text = "Assessed: Approved";
                                            imgArrowLeft2Main.ImageUrl = "~/Images/greentick.gif";
                                        }
                                        if (notapproved && !approvednull)
                                        {
                                            lblStatus2Main.Text = "Assessed: Not Approved";
                                            imgArrowLeft2Main.ImageUrl = "~/Images/TrafficLights/tlYellow.gif";
                                        }
                                        if (approvednull)
                                        {
                                            imgArrowLeft2Main.ImageUrl = "~/Images/TrafficLights/tlYellow.gif";
                                            lblStatus2Main.Text = "Being Assessed";
                                        }
                                        if (qmaTlist_approved.Count != 22) panelRecommend_Visible = false;
                                    }

                                    lblStatus2Main.ForeColor = Color.Black;

                                    if (_q.Status == (int)QuestionnaireStatusList.AssessmentComplete)
                                    {
                                        QuestionnaireMainAssessmentService qmaService = new QuestionnaireMainAssessmentService();
                                        TList<QuestionnaireMainAssessment> qmaTlist_approved;
                                        TList<QuestionnaireMainAssessment> qmaTlist_notapproved;
                                        qmaTlist_approved = qmaService.GetByQuestionnaireIdAssessorApproval(_q.QuestionnaireId, true);
                                        qmaTlist_notapproved = qmaService.GetByQuestionnaireIdAssessorApproval(_q.QuestionnaireId, false);
                                        if (qmaTlist_approved.Count + qmaTlist_notapproved.Count == 22)
                                        {
                                            if (qmaTlist_approved.Count == 22)
                                            {
                                                lblStatus2Main.Text = "Assessed: Approved";
                                                imgArrowLeft2Main.ImageUrl = "~/Images/greentick.gif";
                                            }
                                            else
                                            {
                                                lblStatus2Main.Text = "Assessed: Not Approved";
                                                imgArrowLeft2Main.ImageUrl = "~/Images/TrafficLights/tlYellow.gif";
                                            }
                                        }
                                    }


                                    if (_q.VerificationStatus == (int)QuestionnaireStatusList.BeingAssessed)
                                    {
                                        // Dt 2953
                                        //img4Submit.ImageUrl = "~/Images/Questionnaire/4s.gif";
                                        lnk4SubmitImg.Src = "~/Images/Questionnaire/4s.gif";
                                        lnk4Submit.PostBackUrl = "";
                                        img3Verification.ImageUrl = "~/Images/Questionnaire/d3.gif";
                                        imgArrowLeft3Verification.ImageUrl = "~/Images/greentick.gif";
                                        lblStatus3Verification.Text = "Complete";
                                        lblStatus3Verification.ForeColor = Color.Black;

                                        if (_q.Status == (int)QuestionnaireStatusList.BeingAssessed)
                                        {
                                            QuestionnaireVerificationAssessmentService qvaService = new QuestionnaireVerificationAssessmentService();
                                            TList<QuestionnaireVerificationAssessment> qvaTlist_all;
                                            TList<QuestionnaireVerificationAssessment> qvaTlist_approved;
                                            qvaTlist_all = qvaService.GetByQuestionnaireId(_q.QuestionnaireId);
                                            qvaTlist_approved = qvaService.GetByQuestionnaireIdAssessorApproval(_q.QuestionnaireId, true);
                                            bool approvednull = false;
                                            bool notapproved = false;
                                            if (qvaTlist_all.Count != 30)
                                            {
                                                approvednull = true;
                                            }
                                            else
                                            {
                                                foreach (QuestionnaireVerificationAssessment qva in qvaTlist_all)
                                                {
                                                    if (qva.AssessorApproval == null) approvednull = true;
                                                    if (qva.AssessorApproval == false) notapproved = true;
                                                }
                                            }
                                            if (!notapproved && !approvednull)
                                            {
                                                lblStatus3Verification.Text = "Assessed: Approved";
                                                imgArrowLeft3Verification.ImageUrl = "~/Images/greentick.gif";
                                            }
                                            if (notapproved && !approvednull)
                                            {
                                                lblStatus3Verification.Text = "Assessed: Not Approved";
                                                imgArrowLeft3Verification.ImageUrl = "~/Images/TrafficLights/tlYellow.gif";
                                            }
                                            if (approvednull)
                                            {
                                                imgArrowLeft3Verification.ImageUrl = "~/Images/TrafficLights/tlYellow.gif";
                                                lblStatus3Verification.Text = "Being Assessed";
                                            }
                                            if (qvaTlist_approved.Count != 30) panelRecommend_Visible = false;
                                        }

                                        if (_q.Status == (int)QuestionnaireStatusList.AssessmentComplete)
                                        {
                                            QuestionnaireVerificationAssessmentService qvaService = new QuestionnaireVerificationAssessmentService();
                                            TList<QuestionnaireVerificationAssessment> qvaTlist_approved;
                                            TList<QuestionnaireVerificationAssessment> qvaTlist_notapproved;
                                            qvaTlist_approved = qvaService.GetByQuestionnaireIdAssessorApproval(_q.QuestionnaireId, true);
                                            qvaTlist_notapproved = qvaService.GetByQuestionnaireIdAssessorApproval(_q.QuestionnaireId, false);
                                            if (qvaTlist_approved.Count + qvaTlist_notapproved.Count == 30)
                                            {
                                                if (qvaTlist_approved.Count == 30)
                                                {
                                                    lblStatus3Verification.Text = "Assessed: Approved";
                                                    imgArrowLeft2Main.ImageUrl = "~/Images/greentick.gif";
                                                }
                                                else
                                                {
                                                    lblStatus3Verification.Text = "Assessed: Not Approved";
                                                    imgArrowLeft3Verification.ImageUrl = "~/Images/TrafficLights/tlYellow.gif";
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        imgArrowLeft4Submit.Visible = false;
                                    }
                                }
                                else
                                {
                                    imgArrowLeft4Submit.Visible = false;
                                    lblStatus4Submit.Visible = false;
                                    imgArrowLeft3Verification.Visible = false;
                                    lblStatus3Verification.Visible = false;

                                    ////if user is admin or procurement, let them view the verification questionnaire.
                                    //bool authorised = false;
                                    //if (auth.RoleId == (int)RoleList.Administrator) authorised = true;
                                    //EhsConsultantService _eService = new EhsConsultantService();
                                    //EhsConsultant _ehsc = _eService.GetByUserId(auth.UserId);
                                    //if (_ehsc != null)
                                    //{
                                    //    if (_ehsc.Enabled) authorised = true;
                                    //}
                                    //CompaniesService _cService = new CompaniesService();
                                    //Companies _c = _cService.GetByCompanyId(_q.CompanyId);
                                    //if (_c != null)
                                    //{
                                    //    if (authorised)
                                    //    {
                                    img3Verification.ImageUrl = "~/Images/Questionnaire/3.gif";
                                    img3Verification.PostBackUrl = "~/SafetyPQ_QuestionnaireVerification.aspx" +
                                                                QueryStringModule.Encrypt("q=" + _q.QuestionnaireId);
                                    //    }
                                    //}

                                }
                            }
                            else
                            {
                                if (_q.MainStatus == (int)QuestionnaireStatusList.BeingAssessed)
                                {
                                    //DT 2953
                                    //img3Submit.ImageUrl = "~/Images/Questionnaire/3s.gif";
                                    img3SubmitImg.Src = "~/Images/Questionnaire/3s.gif";
                                    img3Submit.PostBackUrl = "";
                                    img2Main.ImageUrl = "~/Images/Questionnaire/d2.gif";
                                    imgArrowLeft2Main.ImageUrl = "~/Images/greentick.gif";
                                    lblStatus2Main.Text = "Complete";
                                    lblStatus2Main.ForeColor = Color.Black;

                                    if (_q.Status == (int)QuestionnaireStatusList.BeingAssessed)
                                    {
                                        QuestionnaireMainAssessmentService qmaService = new QuestionnaireMainAssessmentService();
                                        TList<QuestionnaireMainAssessment> qmaTlist_all;
                                        TList<QuestionnaireMainAssessment> qmaTlist_approved;
                                        qmaTlist_all = qmaService.GetByQuestionnaireId(_q.QuestionnaireId);
                                        qmaTlist_approved = qmaService.GetByQuestionnaireIdAssessorApproval(_q.QuestionnaireId, true);
                                        bool approvednull = false;
                                        bool notapproved = false;
                                        if (qmaTlist_all.Count != 22)
                                        {
                                            approvednull = true;
                                        }
                                        else
                                        {
                                            foreach (QuestionnaireMainAssessment qma in qmaTlist_all)
                                            {
                                                if (qma.AssessorApproval == null) approvednull = true;
                                                if (qma.AssessorApproval == false) notapproved = true;
                                            }
                                        }
                                        if (!notapproved && !approvednull)
                                        {
                                            lblStatus2Main.Text = "Assessed: Approved";
                                            imgArrowLeft2Main.ImageUrl = "~/Images/greentick.gif";
                                        }
                                        if (notapproved && !approvednull)
                                        {
                                            lblStatus2Main.Text = "Assessed: Not Approved";
                                            imgArrowLeft2Main.ImageUrl = "~/Images/TrafficLights/tlYellow.gif";
                                        }
                                        if (approvednull)
                                        {
                                            imgArrowLeft2Main.ImageUrl = "~/Images/TrafficLights/tlYellow.gif";
                                            lblStatus2Main.Text = "Being Assessed";
                                        }
                                        if (qmaTlist_approved.Count != 22) panelRecommend_Visible = false;
                                    }

                                    if (_q.Status == (int)QuestionnaireStatusList.AssessmentComplete)
                                    {
                                        QuestionnaireMainAssessmentService qmaService = new QuestionnaireMainAssessmentService();
                                        TList<QuestionnaireMainAssessment> qmaTlist_approved;
                                        TList<QuestionnaireMainAssessment> qmaTlist_notapproved;
                                        qmaTlist_approved = qmaService.GetByQuestionnaireIdAssessorApproval(_q.QuestionnaireId, true);
                                        qmaTlist_notapproved = qmaService.GetByQuestionnaireIdAssessorApproval(_q.QuestionnaireId, false);
                                        if (qmaTlist_approved.Count + qmaTlist_notapproved.Count == 22)
                                        {
                                            if (qmaTlist_approved.Count == 22)
                                            {
                                                lblStatus2Main.Text = "Assessed: Approved";
                                                imgArrowLeft2Main.ImageUrl = "~/Images/greentick.gif";
                                            }
                                            else
                                            {
                                                lblStatus2Main.Text = "Assessed: Not Approved";
                                                imgArrowLeft2Main.ImageUrl = "~/Images/TrafficLights/tlYellow.gif";
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    imgArrowLeft3Submit.Visible = false;
                                    lblStatus3Submit.Visible = false;
                                }
                            }
                        }
                        else
                        {
                            Panel1.Visible = false;
                            Panel2SubmitArrow.Visible = true;
                            //DT 2953
                            //img3Submit.ImageUrl = "~/Images/Questionnaire/2s.gif";
                            img3SubmitImg.Src = "~/Images/Questionnaire/2s.gif";
                            img3Submit.PostBackUrl = "";
                            imgArrowLeft3Submit.Visible = true;
                            lblStatus3Submit.Visible = true;
                            imgArrow2Submit.Visible = true;


                        }
                    }
                    else
                    {
                        if (auth.RoleId == (int)RoleList.Contractor || auth.RoleId == (int)RoleList.PreQual)
                        {
                            img1Initial.PostBackUrl = "javascript:alert('This section is to be answered by Procurement.');void(0);";
                        }
                        imgArrowLeft2Main.Visible = false;
                        lblStatus2Main.Visible = false;
                        imgArrowLeft3Submit.Visible = false;
                    }

                    if (_q.MainStatus == (int)QuestionnaireStatusList.BeingAssessed && _q.IsVerificationRequired == false)
                    {
                        lblStatus3Submit.Visible = true;
                        lblStatus4Submit.Visible = true;
                    }
                    if (_q.MainStatus == (int)QuestionnaireStatusList.BeingAssessed && _q.VerificationStatus == (int)QuestionnaireStatusList.BeingAssessed)
                    {
                        lblStatus3Submit.Visible = true;
                        lblStatus4Submit.Visible = true;
                    }

                    if (Helper.General.isEhsConsultant(auth.UserId) || auth.RoleId == (int)RoleList.Administrator)
                    {
                        if (_q.Status == (int)QuestionnaireStatusList.BeingAssessed)
                        {
                            if (_q.MainStatus == (int)QuestionnaireStatusList.BeingAssessed)
                            {
                                if (_q.IsMainRequired)
                                {
                                    if (_q.IsVerificationRequired && (_q.VerificationStatus == (int)QuestionnaireStatusList.BeingAssessed))
                                    {
                                        if ((lblStatus2Main.Text == "Assessed: Not Approved" && lblStatus3Verification.Text == "Assessed: Not Approved") ||
                                            (lblStatus2Main.Text == "Assessed: Approved" && lblStatus3Verification.Text == "Assessed: Not Approved") ||
                                            (lblStatus2Main.Text == "Assessed: Not Approved" && lblStatus3Verification.Text == "Assessed: Approved"))
                                        {
                                            panelBox.Visible = true;
                                            lblBox1.Text = "Assessed as Not Approved";
                                            //lblBox2.Text = "You now have to change the questionnaire status to 'incomplete' and save, in order for the questionnaire to be sent back to the supplier for completion.";
                                            lblBox2.Text = "Click the button below to send this questionnaire back to the supplier.";
                                            btnSendBackToSupplier_PopUp.Visible = true;
                                            lblBox1_PopUp.Text = lblBox1.Text;
                                            //lblBox2_PopUp.Text = lblBox2.Text;
                                            lblBox2_PopUp.Visible = false;
                                            pnlSendBackToSupplier.Visible = true;
                                            QuestionnaireInitialResponse qirSupplierEmail = qirService.GetByQuestionnaireIdQuestionId(_q.QuestionnaireId, "1_3");
                                            if (qirSupplierEmail != null)
                                            {
                                                tbSupplierEmail.Text = qirSupplierEmail.AnswerText;
                                            }
                                            ASPxPopupControl2.ShowOnPageLoad = true;
                                        }
                                        if (lblStatus2Main.Text == "Assessed: Approved" && lblStatus3Verification.Text == "Assessed: Approved")
                                        {
                                            panelBox.Visible = true;
                                            lblBox1.Text = "Assessed as Approved";
                                            lblBox2.Text = "You now have to complete the 'Recommendation & Comments' section below, change the questionnaire status to 'Assessment Complete' and save, in order for the questionnaire to be completed.";
                                            lblBox1_PopUp.Text = lblBox1.Text;
                                            lblBox2_PopUp.Text = lblBox2.Text;
                                            btnOk.Visible = true;
                                            ASPxPopupControl2.ShowOnPageLoad = true;
                                        }

                                    }
                                    else
                                    {
                                        if (lblStatus2Main.Text == "Assessed: Not Approved")
                                        {
                                            panelBox.Visible = true;
                                            lblBox1.Text = "Assessed as Not Approved";
                                            //lblBox2.Text = "You now have to change the questionnaire status to 'incomplete' and save, in order for the questionnaire to be sent back to the supplier for completion.";
                                            lblBox2.Text = "Click the button below to send this questionnaire back to the supplier.";
                                            btnSendBackToSupplier_PopUp.Visible = true;
                                            lblBox1_PopUp.Text = lblBox1.Text;
                                            pnlSendBackToSupplier.Visible = true;
                                            //lblBox2_PopUp.Text = lblBox2.Text;
                                            pnlSendBackToSupplier.Visible = true;
                                            QuestionnaireInitialResponse qirSupplierEmail = qirService.GetByQuestionnaireIdQuestionId(_q.QuestionnaireId, "1_3");
                                            if (qirSupplierEmail != null)
                                            {
                                                tbSupplierEmail.Text = qirSupplierEmail.AnswerText;
                                            }
                                            ASPxPopupControl2.ShowOnPageLoad = true;
                                        }
                                        if (lblStatus2Main.Text == "Assessed: Approved")
                                        {
                                            panelBox.Visible = true;
                                            lblBox1.Text = "Assessed as Approved";
                                            lblBox2.Text = "You now have to complete the 'Recommendation & Comments' section below, change the questionnaire status to 'Assessment Complete' and save, in order for the questionnaire to be completed.";
                                            lblBox1_PopUp.Text = lblBox1.Text;
                                            lblBox2_PopUp.Text = lblBox2.Text;
                                            btnOk.Visible = true;
                                            ASPxPopupControl2.ShowOnPageLoad = true;
                                        }
                                    }
                                }
                            }
                        }

                        if (_q.InitialStatus >= (int)QuestionnaireStatusList.BeingAssessed)
                        {
                            btnRiskSummary.Visible = true;
                        }
                    }

                    if (_q.Status == (int)QuestionnaireStatusList.BeingAssessed || _q.Status == (int)QuestionnaireStatusList.AssessmentComplete)
                    {
                        lblMode.Text = "View";
                        lblStatus3Submit.ForeColor = Color.Black;
                        lblStatus4Submit.ForeColor = Color.Black;
                        lblStatus3Submit.Text = "Submitted";
                        lblStatus4Submit.Text = "Submitted";
                        lblStatus3Submit.Visible = true;
                        lblStatus4Submit.Visible = true;
                        if (_q.IsMainRequired == true)
                        {
                            //DT 2953
                            //img3Submit.ImageUrl = "~/Images/Questionnaire/d3s.gif";
                            img3SubmitImg.Src = "~/Images/Questionnaire/d3s.gif";
                        }
                        else
                        {
                            //DT 2953
                            //img3Submit.ImageUrl = "~/Images/Questionnaire/d2s.gif";
                            img3SubmitImg.Src = "~/Images/Questionnaire/d2s.gif";
                        }
                        //Dt 2953
                        //img4submit.ImageUrl = "~/Images/Questionnaire/d4s.gif";
                        lnk4SubmitImg.Src = "~/Images/Questionnaire/d4s.gif";
                        //img3Submit.PostBackUrl = "javascript:alert('This questionnaire has already been submitted.');void(0);";
                        //img4Submit.PostBackUrl = "javascript:alert('This questionnaire has already been submitted.');void(0);";
                        img3Submit.PostBackUrl = null;
                        lnk4Submit.PostBackUrl = null;
                        img3Submit.OnClientClick = "javascript:alert('This questionnaire has already been submitted.');void(0);";
                        lnk4Submit.OnClientClick = "javascript:alert('This questionnaire has already been submitted.');void(0);";
                        imgArrowLeft3Submit.ImageUrl = "~/Images/greentick.gif";
                        imgArrowLeft4Submit.ImageUrl = "~/Images/greentick.gif";


                        if (Helper.General.isEhsConsultant(auth.UserId) || auth.RoleId == (int)RoleList.Administrator)
                        {
                            if (requalify)
                            {
                                try
                                {
                                    //check requal
                                    QuestionnaireService qService3 = new QuestionnaireService();
                                    DataSet qDataSet = qService3.GetLastModifiedQuestionnaireId(auth.CompanyId);

                                    string LatestQuestionnaireId = "";
                                    foreach (DataRow dr in qDataSet.Tables[0].Rows)
                                    {
                                        LatestQuestionnaireId = dr[0].ToString();
                                    }

                                    if (!String.IsNullOrEmpty(LatestQuestionnaireId))
                                    {
                                        if (_q.QuestionnaireId == Convert.ToInt32(LatestQuestionnaireId))
                                        {
                                            //CompaniesService cService = new CompaniesService();
                                            //Companies Companies = cService.GetByCompanyId(_q.CompanyId);
                                            //if (Companies.CompanyStatusId == (int)CompanyStatusList.Active)
                                            //{
                                            btnReQualify.Enabled = true;
                                            requalify = true;
                                            //}

                                            DateTime? dtExpiry = _q.MainAssessmentValidTo;
                                            if (dtExpiry != null)
                                            {
                                                TimeSpan ts = Convert.ToDateTime(dtExpiry) - DateTime.Now;

                                                //show requal message (FOR CONTRACTOR)
                                                panelBox.Visible = true;
                                                if (ts.Days <= 0)
                                                {
                                                    lblBox1.Text = "Safety Qualification Expired";
                                                    if (auth.RoleId == (int)RoleList.Contractor || auth.RoleId == (int)RoleList.PreQual)
                                                    {
                                                        lblBox2.Text = "Your Company's Safety Qualification has Expired. If your company is required to do work on-site now (or in the future) you need to have your safety qualification re-qualified (up-to-date). Please Contact your Alcoa Contact.";
                                                    }
                                                    else
                                                    {
                                                        notifysupplier(Convert.ToInt32(q), ts.Days);
                                                        lblBox2.Text = "This Company's Safety Qualification has Expired. If you wish to utilise the services of this service provider, this company needs to be re-qualified prior to access to site being granted.<br /><br /><div align='left'>This can be done by:<br /><strong>1) </strong>Clicking the Re-Qualify button to initiate the re-qualification process.<br /><br />Alternatively to disable the re-qualification process for this company:<br /><strong>1) </strong>Change the company status to '<i>InActive - No Re-Qualification Required</i>' (if you have not done so already).</div>";
                                                    }
                                                }
                                                else
                                                {

                                                    lblBox1.Text = "Safety Qualification Due to Expire";
                                                    if (auth.RoleId == (int)RoleList.Contractor || auth.RoleId == (int)RoleList.PreQual)
                                                    {
                                                        lblBox2.Text = "Your Company's Safety Qualification is due to expire and needs to be Re-Validated prior to " + Convert.ToDateTime(dtExpiry).ToShortDateString() + " Please contact your Alcoa Contact.";
                                                    }
                                                    else
                                                    {
                                                        notifysupplier(Convert.ToInt32(q), ts.Days);
                                                        lblBox2.Text = "This Company's Safety Qualification is due to expire and needs to be Re-Validated prior to " + Convert.ToDateTime(dtExpiry).ToShortDateString() + ". If you wish to utilise the services of this service provider, this company needs to be re-qualified prior to access to site being granted.<br /><br /><div align='left'>This can be done by:<br /><strong>1)</strong> Clicking the Re-Qualify button to initiate the re-qualification process.<br /><br />Alternatively to disable the re-qualification process for this company:<br /><strong>1) </strong>Change the company status to '<i>InActive - No Re-Qualification Required</i>' (if you have not done so already).</div>";
                                                    }
                                                }
                                                lblBox2_PopUp.Visible = false;
                                            }
                                        }
                                        else
                                        {
                                            panelBox.Visible = true;
                                            lblBox1.Text = "Old Questionnaire";
                                            lblBox2.Text = "You are viewing an old questionnaire which has been superseded.<br />As such this Questionnaire is READ-ONLY for historical viewing purposes only.<br /><br />";

                                            hlLatestSQ.NavigateUrl = "~/SafetyPQ_QuestionnaireModify.aspx" + QueryStringModule.Encrypt("q=" + LatestQuestionnaireId);
                                            hlLatestSQ.Visible = true;
                                        }
                                    }

                                    qService3 = null;
                                    qDataSet.Dispose();
                                }
                                catch (Exception ex)
                                {
                                    Elmah.ErrorSignal.FromContext(Context).Raise(ex);
                                }
                            }
                            btnRiskSummary.Visible = true;
                            btnScoreAnalysis.Visible = true;
                            //btnScoreAnalysis.ClientSideEvents.Click = String.Format("popUp('PopUps/SafetyPQ_AreaAnalysis.aspx{0}');", QueryStringModule.Encrypt("q=" + q));

                            GetDirectStatus(Convert.ToInt32(q));

                            if (panelRecommend_Visible)
                            {
                                panelRecommend.Enabled = true;
                                panelRecommend.Visible = true;
                            }
                            else
                            {
                                panelRecommend.Visible = true;
                                panelRecommend.Enabled = false;
                                lblReadOnly.Visible = true;
                            }
                            btnSave.Visible = true;
                            cbQuestionnaireStatus.Enabled = true;
                            cbQuestionnaireStatus.ReadOnly = false;
                            btnChangeProc.Visible = true;
                            btnChangeFunctionalManager.Visible = true;

                            if (_q.Status == (int)QuestionnaireStatusList.AssessmentComplete)
                            {
                                //can't change stuff no more!
                                cbQuestionnaireStatus.ReadOnly = true;
                                btnSave.Enabled = false;
                                mRecommended.ReadOnly = true;
                                rbRecommended.ReadOnly = true;
                                //mRecommended.Enabled = false;
                                rbRecommended.Enabled = false;
                                cbDirect.ReadOnly = true;
                                cbDirect.Enabled = false;
                            }
                        }
                    }

                    if (_q.SubContractor == true)
                    {
                        //img2Main.ImageUrl = "~/Images/Questionnaire/d2.gif";
                        //img2Main.PostBackUrl = "javascript:alert('This section to be completed by the SubContractor.'); void(0);";
                    }

                    if (Request.QueryString["s"] != null)
                    {
                        if (Request.QueryString["s"] == "1") lblSave.Text = "Saved Successfully.";
                    }
                    if (Request.QueryString["submit"] != null)
                    {
                        if (Request.QueryString["submit"] == "1") panelBox.Visible = true;
                        lblBox1.Text = "Submitted Successfully";
                        lblBox2.Text = "Your questionnaire will now be assessed and procurement will be in contact with you regarding your submission.";
                    }

                    compareEnable(Convert.ToInt32(q));

                    if (_q.Status == (int)QuestionnaireStatusList.AssessmentComplete)
                    {
                        if (auth.RoleId == (int)RoleList.Administrator || auth.Email == configuration.GetValue(ConfigList.DefaultEhsConsultant_WAO)
                                                                           || auth.Email == configuration.GetValue(ConfigList.DefaultEhsConsultant_VICOPS))
                        {
                            cbDirect.Enabled = true;
                            cbDirect.ReadOnly = false;
                            btnChangeLos.Visible = true;
                            btnChangeLos.Enabled = true;
                        }
                    }

                    //deactivated?
                    CompaniesService _c2Service = new CompaniesService();
                    Companies _c2 = _c2Service.GetByCompanyId(_q.CompanyId);
                    if (_c2.Deactivated == true
                            || _c2.CompanyStatusId == (int)CompanyStatusList.InActive_SQ_Incomplete
                            || _c2.CompanyStatusId == (int)CompanyStatusList.InActive_No_Requal_Required)
                    {
                        panelBox.Visible = true;

                        if (_c2.Deactivated == true)
                        {
                            lblBox1.Text = "Company De-Activated";
                            lblBox2.Text = "This company has been de-activated, Please contact the <a href='mailto:AUAAlcoaContractorServices@alcoa.com.au'>CSMS Team</a> for further assistance such as re-activating this company.";
                            btnChangeCompanyStatus.Enabled = false;
                        }

                        if (_c2.CompanyStatusId == (int)CompanyStatusList.InActive_SQ_Incomplete)
                        {
                            lblBox1.Text = "InActive - SQ Incomplete, No Access to Site (in Status Report)";
                            lblBox2.Text = "<div align='left'>This Company has failed to complete the questionnaire by either opting to not complete or failing to complete, as such no access to site is granted.<br /><br />If circumstances change, to re-initiate the questionnaire process, change the company status to 'Being Assessed' or 'Re-Qualification Incomplete (in Progress)' (whichever applicable).</div>";
                        }
                        else if (_c2.CompanyStatusId == (int)CompanyStatusList.InActive_No_Requal_Required)
                        {
                            lblBox1.Text = "InActive - No Re-Qualification Required.";
                            lblBox2.Text = "<div align='left'>This Company has no contract and no Re-Qualification is required.<br /><br />If circumstances change, to re-initiate the questionnaire process, change the company status to 'Being Assessed' or 'Re-Qualification Incomplete (in Progress)' (whichever applicable)</div>";
                        }


                        lblBox2_PopUp.Visible = false;

                        if (_c2.Deactivated == true)
                        {
                            hlChangeHSAssessor.Enabled = false;
                        }
                        
                        btnSave.Enabled = false;
                        //hlChangeProc.Enabled = false;
                        //hlChangeFunctionalManager.Enabled = false;
                        //btnChangeProc.Enabled = false;
                        //btnChangeFunctionalManager.Enabled = false;
                        //popupProcurementContact.PopupElementID = popupProcurementContact.PopupElementID + "_no";
                        //popupFunctionalManager.PopupElementID = popupProcurementContact.PopupElementID + "_no";

                        //btnChangeCompanyStatus.Enabled = false;
                        btnChangeLos.Enabled = false;
                        //btnChangeProc.Enabled = false;
                        btnReQualify.Enabled = false;
                        btnSendBackToSupplier.Enabled = false;
                        btnSendBackToSupplier_PopUp.Enabled = false;
                        ASPxPopupControl2.ShowOnPageLoad = false;
                        ASPxPopupControl1.ShowOnPageLoad = false;
                        detailGrid.Enabled = false;
                        detailGrid.Columns[0].Visible = false;

                        lblStatus1Initial.Visible = false;
                        lblStatus2Main.Visible = false;
                        lblStatus3Submit.Visible = false;
                        lblStatus3Verification.Visible = false;
                        lblStatus4Submit.Visible = false;
                        //img1Initial.Enabled = false;
                        //img2Main.Enabled = false;
                        //img3Verification.Enabled = false;
                        img3Submit.Enabled = false;
                        lnk4Submit.Enabled = false;
                    }
                }
                else //New
                {
                    ASPxPageControl1.Visible = false;//should be false
                    Label5.Text = "No Safety Qualification Questionnaire exists,<br />Please contact Alcoa.";
                    Label5.Visible = true;
                    lblMiddle.Visible = false;
                    lblMode.Visible = false;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromContext(Context).Raise(ex);
                //Response.Redirect(String.Format("AccessDenied.aspx{0}",
                Server.Transfer(String.Format("AccessDenied.aspx{0}",
                QueryStringModule.Encrypt(String.Format("error={0}", Server.UrlEncode(ex.Message)))));
            }


            if (print)
            {
                ASPxHyperLink1.Enabled = false;
                hlQuestionnaire.Enabled = false;
                btnPrint.Enabled = false;

                hlChangeCompanyStatus.Enabled = false;
                btnChangeCompanyStatus.Enabled = false;

                hlChangeHSAssessor.Enabled = false;
                hlChangeFunctionalManager.Enabled = false;

                btnChangeHSAssessor.Enabled = false;
                btnChangeFunctionalManager.Enabled = false;
                btnChangeLos.Enabled = false;
                btnChangeProc.Enabled = false;
                btnCompare.Enabled = false;
                btnDelete.Enabled = false;
                btnForceDeleteVerification.Enabled = false;
                btnForceReQualify.Enabled = false;
                btnForceReQualify2.Enabled = false;
                btnForceReQualify3.Enabled = false;
                btnOk.Enabled = false;
                btnPrint.Enabled = false;
                btnReQualify.Enabled = false;
                btnSendBackToSupplier.Enabled = false;
                btnSendBackToSupplier_PopUp.Enabled = false;
                btnSave.Enabled = false;
                btnClose.Enabled = false;
                btnScoreAnalysis.Enabled = false;
                btnRiskSummary.Enabled = false;
                detailGrid.Columns[0].Visible = false;
                detailGrid.Enabled = false;
                hlAdmin.Enabled = false;
                img1Initial.Enabled = false;
                img2Main.Enabled = false;
                img3Submit.Enabled = false;
                img3Verification.Enabled = false;
                lnk4Submit.Enabled = false;
            }
        }
        else
        {
            BindLocationData();
        }
        
        switch (auth.RoleId)
        {
            case ((int)RoleList.Reader):
                break;
            case ((int)RoleList.Contractor):
                btnReQualify.Visible = false;
                btnCompare.Visible = false;
                break;
            case ((int)RoleList.Administrator):
                hlAdmin.Visible = true;
                break;
            case ((int)RoleList.PreQual):
                btnReQualify.Visible = false;
                btnCompare.Visible = false;
                break;
            default:
                //Response.Redirect(String.Format("AccessDenied.aspx{0}",
                Server.Transfer(String.Format("AccessDenied.aspx{0}",
                                    QueryStringModule.Encrypt("error=" + Server.UrlEncode("Unrecognised User."))));
                break;
        }
    }

    protected void ActionLog(bool postBack)
    {
        //if (postBack)
        //{

        //}
        //else
        //{
        //    int? q = 0;
        //    if(Request.QueryString["q"] != null) q = Convert.ToInt32(Request.QueryString["q"]);
        //    QuestionnaireActionLogFriendlyService aclfService = new QuestionnaireActionLogFriendlyService();
        //    DataSet aclfDataSet = aclfService.GetByQuestionnaireId(q);


        //}
    }
    protected void grid_StartRowEditing(object sender, DevExpress.Web.Data.ASPxStartRowEditingEventArgs e)
    {
        detailGrid.Columns["CompanySiteCategoryId"].Visible = true;
        detailGrid.Columns["CompanySiteCategoryDesc"].Visible = false;
    }
    protected void grid_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
    {
        if (e.DataColumn.FieldName == "CompanySiteCategoryDesc")
        {
            if (e.CellValue != null)
            {
                e.Cell.ToolTip = detailGrid.GetRowValues(e.VisibleIndex, "Comments").ToString();
            }
        }


    }
    protected void BindLocationData(int CompanyId)
    {
        //model.CompanySiteCategoryStandard c = new model.CompanySiteCategoryStandard();
        DataTable dt = new DataTable();
        dt.Columns.Add("CompanySiteCategoryStandardId",typeof(int));
        dt.Columns.Add("SiteId", typeof(int));
        dt.Columns.Add("CompanySiteCategoryId", typeof(int));
        dt.Columns.Add("CompanyId", typeof(int));
        dt.Columns.Add("LocationSponsorUserId", typeof(int));
        dt.Columns.Add("Approved", typeof(string));
        dt.Columns.Add("CompanySiteCategoryDesc", typeof(string));
        dt.Columns.Add("ApprovedByUserId", typeof(int));
        dt.Columns.Add("ApprovedDate", typeof(DateTime));
        dt.Columns.Add("Comments", typeof(string));
        List<model.CompanySiteCategoryStandard> cscsList = companySiteCategoryStandardService.GetMany(null, cs => cs.CompanyId == CompanyId, null,new List<System.Linq.Expressions.Expression<Func<model.CompanySiteCategoryStandard,object>>>(){l=>l.CompanySiteCategory});
        string category;
        string comments = "";
        int year = DateTime.Now.Year;
        DateTime firstDayOfMonth = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
        foreach (model.CompanySiteCategoryStandard cscs in cscsList)
        {
            comments = "";
            List<model.CompanySiteCategoryException> csceList = companySiteCategoryExceptionService.GetMany(null, i => i.CompanyId == CompanyId && i.SiteId == cscs.SiteId && year >= i.MonthYearFrom.Year && year <= i.MonthYearTo.Year, o => o.OrderBy(m => m.MonthYearFrom), null);
            String suffix = (csceList.Count > 0) ? "*" : "";
            if (cscs.CompanySiteCategoryId == null)
                category = "";
            else
            {
                model.CompanySiteCategory csc = companySiteCategoryService.Get(ct => ct.CompanySiteCategoryId == cscs.CompanySiteCategoryId, null);
                category = csc.CategoryDesc;
                comments = csc.CategoryDesc + " Default\n";
            }
            foreach (model.CompanySiteCategoryException csce in csceList)
            {
                if (firstDayOfMonth >= csce.MonthYearFrom && firstDayOfMonth <= csce.MonthYearTo)
                {
                    category = csce.CompanySiteCategory.CategoryDesc;
                }
                comments += String.Format("{0} {1}-{2}{3}", csce.CompanySiteCategory.CategoryDesc, csce.MonthYearFrom.ToString("MMM yyyy"), csce.MonthYearTo.ToString("MMM yyyy"), "\n");
            }
            category += suffix;
            string ap;
            if (cscs.Approved.HasValue && cscs.Approved == true)
                ap = "Yes";
            else if (cscs.Approved.HasValue && cscs.Approved == false)
                ap = "No";
            else
                ap = null;


            dt.Rows.Add(cscs.CompanySiteCategoryStandardId, cscs.SiteId, cscs.CompanySiteCategoryId, cscs.CompanyId, cscs.LocationSponsorUserId, ap, category,cscs.ApprovedByUserId,cscs.ApprovedDate,comments);
        }


        

        //CompanySiteCategoryStandardService cscsService = new CompanySiteCategoryStandardService();
        
        //TList<CompanySiteCategoryStandard> cscsTlist = cscsService.GetByCompanyId(CompanyId);
        //DataSet ds = cscsTlist.ToDataSet(false);
        //if (ds.Tables[0] != null)
        if (dt != null)
        {
            //Session["dtLocationSq"] = ds.Tables[0];
            //Session["dtLocationSq"] = cscsList;
            Session["dtLocationSq"] = dt;
            BindLocationData();
        }
        else
        {
            Session["dtLocationSq"] = null;
        }
    }
    protected void BindLocationData()
    {
        if (Session["dtLocationSq"] != null)
        {
            //detailGrid.DataSource = (DataTable)Session["dtLocationSq"];          
            detailGrid.DataSource = Session["dtLocationSq"];
            detailGrid.Columns["CompanySiteCategoryId"].Visible = false;
            detailGrid.Columns["CompanySiteCategoryDesc"].Visible = true;
            detailGrid.DataBind();
        }
    }
    protected void grid_CellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
    {
        if (e.Column.FieldName == "CompanySiteCategoryId" || e.Column.FieldName == "LocationSponsorUserId" || e.Column.FieldName == "Approved")
        {
            ASPxComboBox combo = (ASPxComboBox)e.Editor;
            combo.DataBindItems();

            string _text = string.Empty;
            if (e.Column.FieldName == "Approved") _text = "Tentative";

            ListEditItem item = new ListEditItem(_text, null);
            combo.Items.Insert(0, item);
        }
    }

    protected void Submit_Click(object sender, EventArgs e)
    {
        try
        {
            QuestionnaireService _qs = new QuestionnaireService();
            Questionnaire _q = new Questionnaire();
            string q = Request.QueryString["q"];
            //DT 2953
            int qnum;
            bool result = int.TryParse(q,out qnum);
            if (result)
            {
                qid = q;
                _q = _qs.GetByQuestionnaireId(Convert.ToInt32(q.Trim()));

                //throw new Exception();
                if (_q != null)
                {
                    _q.ModifiedByUserId = auth.UserId;
                    _q.ModifiedDate = DateTime.Now;
                    if (_q.Status == (int)QuestionnaireStatusList.Incomplete)
                    {
                        bool _good = false;
                        if (_q.InitialStatus == (int)QuestionnaireStatusList.AssessmentComplete && _q.MainStatus == (int)QuestionnaireStatusList.BeingAssessed) _good = true;
                        if (_q.InitialStatus == (int)QuestionnaireStatusList.AssessmentComplete && _q.IsMainRequired == false) _good = true;

                        //if (_q.Status != (int)QuestionnaireStatusList.Re_Qualification)
                        //{
                        //    if (_q.Status != (int)QuestionnaireStatusList.Incomplete) _good = false;
                        //}

                        if (_q.Status != (int)QuestionnaireStatusList.AssessmentComplete)
                        {
                            if (_good)
                            {
                                bool good = true;
                                if (_q.IsVerificationRequired == true && _q.VerificationStatus != (int)QuestionnaireStatusList.BeingAssessed)
                                {
                                    good = false;
                                }
                                if (_q.IsMainRequired == false) good = true;
                                if (good)
                                {
                                    _q.Status = (int)QuestionnaireStatusList.BeingAssessed;
                                    _q.ModifiedByUserId = auth.UserId;
                                    _q.ModifiedDate = DateTime.Now;
                                    _q.QuestionnairePresentlyWithActionId = (int)QuestionnairePresentlyWithActionList.HSAssessorQuestionnaire;

                                    _q.QuestionnairePresentlyWithSince = DateTime.Now;
                                    _qs.Save(_q);

                                    UsersService usersService = new UsersService();
                                    Users userEntity;
                                    userEntity = usersService.GetByUserId(_q.ModifiedByUserId);

                                    CompaniesService companiesService = new CompaniesService();
                                    Companies companiesEntity = companiesService.GetByCompanyId(_q.CompanyId);
                                    Companies companiesEntity2 = companiesService.GetByCompanyId(auth.CompanyId);

                                    //Email Time
                                    string PreReSq = "Contractor Safety Pre-Qualification";
                                    if (_q.IsReQualification) PreReSq = "Contractor Safety Re-Qualification";
                                    if (_q.SubContractor == true) PreReSq = "Sub " + PreReSq;

                                    //Modified by Ashley Goldstraw to use new email template and email sender.  11/9/2015
                                    model.EmailTemplate eMailTemplate = emailTemplateService.Get(i => i.EmailTemplateName == "Questionnaire Submitted for Assessment � Sent to EHS Consultant", null);
                                    byte[] bodyByte = eMailTemplate.EmailBody;
                                    string mailbody = System.Text.Encoding.ASCII.GetString(bodyByte);
                                    string subject = eMailTemplate.EmailTemplateSubject;
                                    subject = subject.Replace("{CompanyName}", companiesEntity.CompanyName);
                                    subject = subject.Replace("{PreOrReQualification}", PreReSq);
                                    mailbody = mailbody.Replace("{CompanyName}", companiesEntity.CompanyName);
                                    mailbody = mailbody.Replace("{SubmittedByLastName}", userEntity.LastName);
                                    mailbody = mailbody.Replace("{SubmittedByFirstName}", userEntity.FirstName);
                                    mailbody = mailbody.Replace("{SubmittedByCompanyName}", companiesEntity2.CompanyName);
                                    mailbody = mailbody.Replace("{SubmittedDateTime}", _q.ModifiedDate.ToString());



                                    /*string subject = String.Format("{0}: 2 - Questionnaire submitted for {1}", PreReSq, companiesEntity.CompanyName);
                                    string body = "A Questionnaire has been submitted for assessment as follows:\n\n" +
                                                  "Company Name: " + companiesEntity.CompanyName + "\n" +
                                                  "\n\n" +
                                                  "Questionnaire Submitted by: " + userEntity.LastName + ", " + userEntity.FirstName + " (" + companiesEntity2.CompanyName + ") at " + _q.ModifiedDate.ToString() + "\n" +
                                                  "\n\n" +
                                                  "THIS SAFETY QUALIFICATION QUESTIONNAIRE WILL NOW BE ASSESSED BY EHS / SAFETY ASSESSOR WHO WILL ASSESS THE QUESTIONNAIRE AND MAKE A RECOMMENDATION.";
                                     */
                                    Configuration configuration = new Configuration();
                                    //string[] cc = { configuration.GetValue(ConfigList.ContactEmail) };
                                    //cc - AUACSMS Team cc Removed 9.Mar.11
                                    //Modified by Ashley Goldstraw 11/9/2015 to use new email sender
                                    string[] to = { Helper.General.getEhsConsultantEmailByCompany(_q.CompanyId, true) };
                                    emailLogService.Insert(to, null, null, subject, mailbody, "Questionnaire", true, null, null);
                                    //Helper.Email.notifyEHSConsultant(_q.CompanyId, null, null, subject, body, null);

                                    string redirPage = "SafetyPQ_Questionnaire.aspx";
                                    string msgBoxText = "Your questionnaire will now be assessed and procurement will be in contact with you regarding your submission. Click on OK, then click on Close to exit.";

                                    if (_q.SubContractor == true)
                                    {
                                        //redirPage = "SafetyPQ_Questionnaire.aspx?sc=1";
                                        msgBoxText = "Your questionnaire will now be assessed. Click on OK, then click on Close to exit.";
                                    }


                                    int intSubmitResubmit = (int)QuestionnaireActionList.SafetyQuestionnaireSubmitted;
                                    QuestionnaireMainAssessmentService qmaService = new QuestionnaireMainAssessmentService();
                                    TList<QuestionnaireMainAssessment> qmaTlist_all;
                                    qmaTlist_all = qmaService.GetByQuestionnaireId(_q.QuestionnaireId);
                                    if (qmaTlist_all.Count > 0) intSubmitResubmit = (int)QuestionnaireActionList.SafetyQuestionnaireReSubmitted;

                                    if (Helper.Questionnaire.ActionLog.Add(intSubmitResubmit,
                                                        _q.QuestionnaireId,                         //QuestionnaireId
                                                        auth.UserId,                                //UserId
                                                        auth.FirstName,                             //FirstName
                                                        auth.LastName,                              //LastName
                                                        auth.RoleId,                                //RoleId
                                                        auth.CompanyId,                             //CompanyId
                                                        auth.CompanyName,                           //CompanyName
                                                        null,                                       //Comments
                                                        null,                                       //OldAssigned
                                                        null,                                       //NewAssigned
                                                        null,                                       //NewAssigned2
                                                        null,                                       //NewAssigned3
                                                        null)                                       //NewAssigned4
                                                        == false)
                                    {
                                        //for now we do nothing if action log fails.
                                    }


                                    //Response.Redirect("javascript:alert('" + msgBoxText + "'); window.location='" + redirPage + "';");
                                    Helper.General.Redirect(Page, msgBoxText, redirPage);

                                }
                                else
                                {
                                    lblSave.Text = "Hmm... You shouldn't be seeing this. Something went wrong! Contact Support (Help)";
                                }
                            }
                            else
                            {
                                lblSave.Text = "Hmm... You shouldn't be seeing this. Something went wrong! Contact Support (Help)";
                            }
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            System.Web.HttpBrowserCapabilities browser = Request.Browser;

            Elmah.ErrorSignal.FromContext(Context).Raise(ex);
            lblSave.Text = String.Format("Error: {0}", ex.Message);
            LogTheError(ex,browser,qid);
            //throw new Exception("Message : " + ex.Message + "\n" + "Stack Trace :" + ex.StackTrace + "\n Source:" + ex.Source + "\n qid='" + qid + "', length=" + qid.Length + "\n Browser information:- Type= " + browser.Type + " Name=" +browser.Browser+ " Platform="+browser.Platform + " IsWin32="+browser.Win32);
            
        }

    }
    protected void LogTheError(Exception ex,System.Web.HttpBrowserCapabilities browser,String qid)
    { 
            WindowsImpersonationContext impContext = null;
            try
            {
                impContext = NetworkSecurity.Impersonation.ImpersonateUser(
                                        System.Configuration.ConfigurationManager.AppSettings["WAOCSM_domain"].ToString(),
                                        System.Configuration.ConfigurationManager.AppSettings["WAOCSM_user"].ToString(),
                                        System.Configuration.ConfigurationManager.AppSettings["WAOCSM_pass"].ToString());
            }
            catch (ApplicationException ex1)
            {
                Elmah.ErrorSignal.FromContext(Context).Raise(ex1);
            }
            try
            {
                if (null != impContext)
                {
                    using (impContext)
                    {
                        string saveFolder = "";
                        string fileName = "";
                        ConfigService cfService = new ConfigService();
                        Config cng = cfService.GetByKey("AIContractorListFilePath");
                        saveFolder = cng.Value;



                        fileName = String.Format("Questionnaire Submission Error Log.csv");


                        if (String.IsNullOrEmpty(saveFolder) || String.IsNullOrEmpty(fileName))
                        {
                            throw new Exception("Expected folder name or filename");
                        }

                        //string csvHeaders = "Message,Stack Trace,Source,Query String Id,Length,Browser Type,Browser Name,Browser Platform,Browser IsWin32?";

                        //if (File.Exists(saveFolder + fileName)) throw new Exception("File Already Exists?!: " + saveFolder + fileName);
                        TextWriter tw = new StreamWriter(saveFolder + fileName, true);
                        
                        //tw.WriteLine(ex.Message,ex.StackTrace,ex.Source,qid,qid.Length,browser.Type,browser.Browser,browser.Platform,browser.Win32);
                        tw.WriteLine("\"" + ex.Message + "\",\"" + ex.StackTrace + "\",\"" + ex.Source + "\",\"" + qid + "\",\"" + qid.Length + "\",\"" + browser.Type + "\",\"" + browser.Browser + "\",\"" + browser.Platform + "\",\"" + browser.Win32 + "\",\"" + DateTime.Now + "\"");
                        tw.Close();
                    }
                }
            }
            catch (Exception ex2)
            {
                Elmah.ErrorSignal.FromContext(Context).Raise(ex2);
            }
            finally
            {
                if (impContext != null) impContext.Undo();

            }


    
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        //System.Threading.Thread.Sleep(1000);
        try
        {
            string q = Request.QueryString["q"].ToString();
            if (q != "New")
            {
                QuestionnaireService _qs = new QuestionnaireService();
                Questionnaire _q = new Questionnaire();
                _q = _qs.GetByQuestionnaireId(Convert.ToInt32(q));

                switch (cbQuestionnaireStatus.Value.ToString())
                {
                    case "Incomplete":
                        _q.Status = (int)QuestionnaireStatusList.Incomplete;
                        break;
                    case "Being Assessed":
                        _q.Status = (int)QuestionnaireStatusList.BeingAssessed;
                        break;
                    case "Assessment Complete":
                        _q.Status = (int)QuestionnaireStatusList.AssessmentComplete;
                        break;

                    //this should be the only option. TODO: Investigate why above may be chosen...
                    default:
                        _q.Status = Convert.ToInt32(cbQuestionnaireStatus.Value.ToString());
                        break;
                }

                bool? rec = null;
                if (Convert.ToInt32(rbRecommended.Value) == 1) rec = true;
                if (Convert.ToInt32(rbRecommended.Value) == 0) rec = false;
                if (rbRecommended.Value == null) rec = null;

                if (_q.Status == (int)QuestionnaireStatusList.Incomplete)
                {
                    //check if all assessment done.
                    bool qmaAllAssessed = false;
                    bool qvaAllAssessed = false;
                    bool qmaNotApproved = false;
                    bool qvaNotApproved = false;
                    if (_q.IsMainRequired)
                    {
                        QuestionnaireMainAssessmentService qmaService = new QuestionnaireMainAssessmentService();
                        TList<QuestionnaireMainAssessment> qmaTlist = qmaService.GetByQuestionnaireId(_q.QuestionnaireId);
                        if (qmaTlist.Count == 22) qmaAllAssessed = true;
                        foreach (QuestionnaireMainAssessment qma in qmaTlist)
                        {
                            if (qma.AssessorApproval == null) qmaAllAssessed = false;
                            if (qma.AssessorApproval == false) qmaNotApproved = true;
                        }
                    }
                    else
                    {
                        qmaAllAssessed = true;
                    }

                    if (_q.IsVerificationRequired)
                    {
                        QuestionnaireVerificationAssessmentService qvaService = new QuestionnaireVerificationAssessmentService();
                        TList<QuestionnaireVerificationAssessment> qvaTlist = qvaService.GetByQuestionnaireId(_q.QuestionnaireId);
                        if (qvaTlist.Count == 30) qvaAllAssessed = true;
                        foreach (QuestionnaireVerificationAssessment qva in qvaTlist)
                        {
                            if (qva.AssessorApproval == null) qvaAllAssessed = false;
                            if (qva.AssessorApproval == false) qvaNotApproved = true;
                        }
                    }
                    else
                    {
                        qvaAllAssessed = true;
                    }

                    if (!qmaAllAssessed && !qvaAllAssessed)
                    {
                        throw new Exception("Cannot change questionnaire status to [Incomplete].\nBoth the  Supplier & Verification Questionnaire questions have not been completely assessed.");
                    }
                    else
                    {
                        if (!qmaAllAssessed) throw new Exception("Cannot change questionnaire status to [Incomplete].\nSupplier Questionnaire must have all questions completely assessed.");
                        if (!qvaAllAssessed) throw new Exception("Cannot change questionnaire status to [Incomplete].\nVerification Questionnaire must have all questions completely assessed.");
                    }
                    if (!qvaNotApproved && !qmaNotApproved)
                    {
                        throw new Exception("Cannot change questionnaire status to [Incomplete].\nSupplier and Verification Questionnaire have been assessed as approved.");
                    }
                    if (qvaNotApproved) _q.VerificationStatus = (int)QuestionnaireStatusList.Incomplete;
                    if (qmaNotApproved)
                    {
                        _q.MainStatus = (int)QuestionnaireStatusList.Incomplete;
                    }
                    _q.QuestionnairePresentlyWithActionId = (int)QuestionnairePresentlyWithActionList.SupplierQuestionnaireIncomplete;
                    _q.QuestionnairePresentlyWithSince = DateTime.Now;
                    _qs.Save(_q);
                    //Response.Redirect("javascript:alert('Questionnaire Status set back to [Incomplete]. Please contact the Supplier to correct the questionnaire based upon the assessment approval/comments.');window.location='SafetyPQ_QuestionnaireModify.aspx" + QueryStringModule.Encrypt("q=" + q) + "'");

                    if (Helper.Questionnaire.ActionLog.Add((int)QuestionnaireActionList.SafetyQuestionnaireNotApproved,
                                                    _q.QuestionnaireId,                         //QuestionnaireId
                                                    auth.UserId,                                //UserId
                                                    auth.FirstName,                             //FirstName
                                                    auth.LastName,                              //LastName
                                                    auth.RoleId,                                //RoleId
                                                    auth.CompanyId,                             //CompanyId
                                                    auth.CompanyName,                           //CompanyName
                                                    null,                                       //Comments
                                                    null,                                       //OldAssigned
                                                    "(n/a)",                                    //NewAssigned
                                                    null,                                       //NewAssigned2
                                                    null,                                       //NewAssigned3
                                                    null)                                       //NewAssigned4
                                                    == false)
                    {
                        //for now we do nothing if action log fails.
                    }

                    Helper.General.Redirect(Page, "Questionnaire Status set back to [Incomplete]. Please contact the Supplier to correct the questionnaire based upon the assessment approval/comments.", "SafetyPQ_QuestionnaireModify.aspx" + QueryStringModule.Encrypt("q=" + q));

                }
                int? EhsConsultantId = null;
                if (cbEhsConsultant.Value != null) EhsConsultantId = Convert.ToInt32(cbEhsConsultant.Value);
                CompaniesService cService = new CompaniesService();
                Companies c = cService.GetByCompanyId(_q.CompanyId);

                if (c != null)
                {
                    c.EhsConsultantId = EhsConsultantId;
                    cService.Save(c);
                }

                if (_q.Status == (int)QuestionnaireStatusList.BeingAssessed)
                {
                    //_q.QuestionnairePresentlyWithActionId = (int)QuestionnairePresentlyWithActionList.HSAssessorQuestionnaire;
                    //_q.QuestionnairePresentlyWithSince = DateTime.Now;
                    if (rec != null)
                    {
                        _q.Recommended = rec;
                        _q.RecommendedComments = mRecommended.Text;
                        _q.LevelOfSupervision = cbDirect.Value.ToString();
                        _qs.Save(_q);
                    }

                    if (rec == false)
                    {
                        UsersService usersService = new UsersService();
                        Users userEntity;
                        userEntity = usersService.GetByUserId(auth.UserId);

                        CompaniesService companiesService = new CompaniesService();
                        Companies companiesEntity = companiesService.GetByCompanyId(_q.CompanyId);
                        Companies companiesEntity2 = companiesService.GetByCompanyId(auth.CompanyId);

                        string recommended = "";
                        if (rec == true) recommended = "Yes";
                        if (rec == false) recommended = "No";

                        string riskRating = _q.MainAssessmentRiskRating;
                        if (Helper.Questionnaire.getRiskLevel(_q.InitialRiskAssessment) > Helper.Questionnaire.getRiskLevel(_q.MainAssessmentRiskRating))
                        {
                            riskRating = _q.InitialRiskAssessment;
                        }

                        string PreReSq = "Contractor Safety Pre-Qualification";
                        if (_q.IsReQualification) PreReSq = "Contractor Safety Re-Qualification";
                        if (_q.SubContractor == true) PreReSq = "Sub " + PreReSq;

                        model.EmailTemplate eMailTemplate = emailTemplateService.Get(i => i.EmailTemplateName == "Contractor Safety Pre-Qualification � Not Recommended for Qualification",null);
                        byte[] bodyByte = eMailTemplate.EmailBody;
                        string mailbody = System.Text.Encoding.ASCII.GetString(bodyByte);
                        string subject = eMailTemplate.EmailTemplateSubject;
                        subject = subject.Replace("{CompanyName}", companiesEntity.CompanyName);
                        subject = subject.Replace("{PreOrReQualification}", PreReSq);
                        mailbody = mailbody.Replace("{CompanyName}", companiesEntity.CompanyName);
                        mailbody = mailbody.Replace("{Recommended}", recommended);
                        mailbody = mailbody.Replace("{RiskRating}", riskRating);
                        mailbody = mailbody.Replace("{AssessedByLastName}", userEntity.LastName);
                        mailbody = mailbody.Replace("{AssessedByFirstName}", userEntity.FirstName);
                        mailbody = mailbody.Replace("{AssessedByCompanyName}", companiesEntity2.CompanyName);
                        mailbody = mailbody.Replace("{DateAssessed}", _q.ApprovedDate.ToString());


                        //string subject = String.Format("{0}: 3 - Questionnaire assessed as NOT RECOMMENDED for {1}", PreReSq, companiesEntity.CompanyName);
                        /*string body = "A Questionnaire has been assessed as follows:\n\n" +
                                      "Company Name: " + companiesEntity.CompanyName + "\n" +
                                        "Recommended: " + recommended + "\n" +
                                        "Final Risk Rating: " + riskRating +
                                        "\n\n" +
                                        "Questionnaire assessed by: " + userEntity.LastName + ", " + userEntity.FirstName + " (" + companiesEntity2.CompanyName + ") at " + _q.ApprovedDate.ToString() + "\n" +
                                        "\n\n" +
                                        "THIS QUESTIONNAIRE HAS BEEN ASSESSED BY EHS / SAFETY ASSESSOR.\n\n" +
                                        "CSMS Team Please review this not recommended status.";*/
                        Configuration configuration = new Configuration();

                        //To - AUACSMS Team To Removed 9.Mar.11, Replaced with Default EHS Consultant
                        ConfigService _cService = new ConfigService();
                        Config _c = _cService.GetByKey("DefaultEhsConsultant_WAO");
                        if (!String.IsNullOrEmpty(_c.Value))
                        {
                            string[] defaultehs = { _c.Value };
                            //Helper.Email.sendEmail(defaultehs, null, null, subject, body, null);
                            //Helper.Email.logEmail(null, null, defaultehs, null, null, subject, body, EmailLogTypeList.Questionnaire);
                            emailLogService.Insert(defaultehs, null, null, subject, mailbody, "Questionnaire", true, null,null);
                            
                        }
                        else
                        {
                            string[] csmteam = { configuration.GetValue(ConfigList.ContactEmail) };
                            //Helper.Email.sendEmail(csmteam, null, null, subject, body, null);
                            //Helper.Email.logEmail(null, null, csmteam, null, null, subject, body, EmailLogTypeList.Questionnaire);
                            emailLogService.Insert(csmteam, null, null, subject, mailbody, "Questionnaire", true, null, null);
                        }
                        //Response.Redirect("javascript:alert('Questionnaire Status set to [Being Assessed] with an Not Recommended Status. The Contractor Services Management Team will now review your questionnaire assessment.');window.location='SafetyPQ_QuestionnaireModify.aspx" + QueryStringModule.Encrypt("q=" + q) + "'");
                        Helper.General.Redirect(Page, "Questionnaire Status set to [Being Assessed] with an Not Recommended Status. The Contractor Services Management Team will now review your questionnaire assessment.", "SafetyPQ_QuestionnaireModify.aspx" + QueryStringModule.Encrypt("q=" + q));
                    }
                }

                if (_q.Status == (int)QuestionnaireStatusList.AssessmentComplete)
                {

                    if (_q.IsMainRequired)
                    {
                        //check all assessed.
                        bool qmaAllApproved = false;
                        bool qvaAllApproved = false;
                        QuestionnaireMainAssessmentService qmaService = new QuestionnaireMainAssessmentService();
                        TList<QuestionnaireMainAssessment> qmaTlist = qmaService.GetByQuestionnaireIdAssessorApproval(_q.QuestionnaireId, true);
                        if (qmaTlist.Count == 22) qmaAllApproved = true;

                        if (_q.IsVerificationRequired)
                        {
                            QuestionnaireVerificationAssessmentService qvaService = new QuestionnaireVerificationAssessmentService();
                            TList<QuestionnaireVerificationAssessment> qvaTlist = qvaService.GetByQuestionnaireIdAssessorApproval(_q.QuestionnaireId, true);
                            if (qvaTlist.Count == 30) qvaAllApproved = true;
                        }
                        else
                        {
                            qvaAllApproved = true;
                        }

                        if (!qmaAllApproved && !qvaAllApproved)
                        {
                            throw new Exception("Cannot change questionnaire status to [Assessment Complete].\nBoth the Supplier & Verification Questionnaire must be completely assessed and approved.");
                        }
                        else
                        {
                            if (!qmaAllApproved) throw new Exception("Cannot change questionnaire status to [Assessment Complete].\nSupplier Questionnaire must be completely assessed and approved.");
                            if (!qvaAllApproved) throw new Exception("Cannot change questionnaire status to [Assessment Complete].\nVerification Questionnaire must be completely assessed and approved.");
                        }

                        if (auth.RoleId != (int)RoleList.Administrator)
                        {
                            if (rec == false) throw new Exception("Cannot set company questionnaire as [Not Recommended]. You can choose to save this as [Not Recommended] only if the [Being Assessed] questionnaire status is chosen and an automatic e-mail will then be generated notifying the CSMS team of your selection for reviewal purposes.");
                        }
                    }

                    _q.Recommended = rec;
                    _q.RecommendedComments = mRecommended.Text;
                    _q.LevelOfSupervision = cbDirect.Value.ToString();
                    _q.ApprovedByUserId = auth.UserId;
                    _q.ApprovedDate = DateTime.Now;
                    _q.MainAssessmentDate = DateTime.Now;

                    //try
                    //{
                    if(Convert.ToInt32(cbSqType.SelectedItem.Value)==1)
                    {
                        //Changes done for DT2709 on 3/7/2013
                        //DT 3191,3192
                        if (!String.IsNullOrEmpty(lblExpiresAt.Text) && lblExpiresAt.Text.ToString() !="-")
                        {
                            DateTime dtPrevious = Convert.ToDateTime(lblExpiresAt.Text);
                            if (dtPrevious < DateTime.Now)
                                _q.MainAssessmentValidTo = DateTime.Now.AddYears(1);
                            else
                                _q.MainAssessmentValidTo = dtPrevious.AddYears(1);

                        }
                        else
                        {
                            _q.MainAssessmentValidTo = DateTime.Now.AddYears(1);
                        }
                        //End
                     }
                    else
                        _q.MainAssessmentValidTo = DateTime.Now.AddYears(1);

                    //}
                    //catch(Exception ex)
                    //{
                    //  _q.MainAssessmentValidTo=DateTime.Now.AddYears(1);
                    //}

                    if (c.CompanyStatusId == (int)CompanyStatusList.Being_Assessed || c.CompanyStatusId == (int)CompanyStatusList.Requal_Incomplete)
                    {
                        _q.QuestionnairePresentlyWithActionId = (int)QuestionnairePresentlyWithActionList.ProcurementAssignCompanyStatus;
                        _q.QuestionnairePresentlyWithSince = DateTime.Now;
                    }
                    _qs.Save(_q);



                    UsersService usersService = new UsersService();
                    Users userEntity;
                    userEntity = usersService.GetByUserId(auth.UserId);

                    CompaniesService companiesService = new CompaniesService();
                    Companies companiesEntity = companiesService.GetByCompanyId(_q.CompanyId);
                    Companies companiesEntity2 = companiesService.GetByCompanyId(auth.CompanyId);

                    string recommended = "";
                    if (rec == true) recommended = "Yes";
                    if (rec == false) recommended = "No";

                    string riskRating = _q.MainAssessmentRiskRating;

                    try
                    {
                        if (Helper.Questionnaire.getRiskLevel(_q.InitialRiskAssessment) > Helper.Questionnaire.getRiskLevel(_q.MainAssessmentRiskRating))
                        {
                            riskRating = _q.InitialRiskAssessment;
                        }
                    }
                    catch (Exception)
                    {
                    }

                    //Email Time
                    string PreReSq = "Contractor Safety Pre-Qualification";
                    if (_q.IsReQualification) PreReSq = "Contractor Safety Re-Qualification";
                    if (_q.SubContractor == true) PreReSq = "Sub " + PreReSq;

                    //Modified by Ashley Goldstraw 11/9/15
                    model.EmailTemplate eMailTemplate = emailTemplateService.Get(i => i.EmailTemplateName == "Questionnaire Assessed � Sent To EHS Consultant", null);
                    byte[] bodyByte = eMailTemplate.EmailBody;
                    string mailbody = System.Text.Encoding.ASCII.GetString(bodyByte);
                    string subject = eMailTemplate.EmailTemplateSubject;
                    subject = subject.Replace("{CompanyName}", companiesEntity.CompanyName);
                    subject = subject.Replace("{PreOrReQualification}", PreReSq);
                    mailbody = mailbody.Replace("{CompanyName}", companiesEntity.CompanyName);
                    mailbody = mailbody.Replace("{Recommended}", recommended);
                    mailbody = mailbody.Replace("{RiskRating}", riskRating);
                    mailbody = mailbody.Replace("{AssessedByLastName}", userEntity.LastName);
                    mailbody = mailbody.Replace("{AssessedByFirstName}", userEntity.FirstName);
                    mailbody = mailbody.Replace("{AssessedByCompanyName}", companiesEntity2.CompanyName);
                    mailbody = mailbody.Replace("{AssessedDateTime}", _q.ApprovedDate.ToString());



                    /*string subject = String.Format("{0}: 3 - Questionnaire assessed for {1}", PreReSq, companiesEntity.CompanyName);
                    string body = "A Questionnaire has been assessed as follows:\n\n" +
                                  "Company Name: " + companiesEntity.CompanyName + "\n" +
                                    "Recommended: " + recommended + "\n" +
                                    "Final Risk Rating: " + riskRating +
                                    "\n\n" +
                                    "Questionnaire assessed by: " + userEntity.LastName + ", " + userEntity.FirstName + " (" + companiesEntity2.CompanyName + ") at " + _q.ApprovedDate.ToString() + "\n" +
                                    "\n\n" +
                                    "THIS QUESTIONNAIRE HAS BEEN ASSESSED BY EHS / SAFETY ASSESSOR.\n\n" +
                                    "PROCUREMENT (INITIATOR) NOW NEED TO REVIEW THE ASSESSMENT AND UPDATE THE COMPANY STATUS TO REFLECT THE CONTRACTOR STATUS.";
                    */


                    userEntity = usersService.GetByUserId(_q.CreatedByUserId);

                    QuestionnaireInitialResponseService qirService = new QuestionnaireInitialResponseService();
                    QuestionnaireInitialResponse qir = qirService.GetByQuestionnaireIdQuestionId(_q.QuestionnaireId, "2");
                    if (qir != null)
                    {
                        if (!String.IsNullOrEmpty(qir.AnswerText))
                        {
                            try
                            {
                                userEntity = usersService.GetByUserId(Convert.ToInt32(qir.AnswerText));
                            }
                            catch (Exception ex)
                            {
                                Elmah.ErrorSignal.FromContext(Context).Raise(ex);
                            }
                            qir.Dispose();
                        }
                    }

                    Configuration configuration = new Configuration();
                    //listCC.Add(userEntity.Email);
                    //listCC.Add(configuration.GetValue(ConfigList.ContactEmail).ToString());
                    //string[] cc = listCC.ToArray();
                    string[] cc = { userEntity.Email }; //configuration.GetValue(ConfigList.ContactEmail) - AUACSMS Team To Removed 9.Mar.11

                    //Changed by Ashley Goldstraw to use new email sender.
                    string[] to = { Helper.General.getEhsConsultantEmailByCompany(_q.CompanyId, true) };
                    emailLogService.Insert(to, cc, null, subject, mailbody, "Questionnaire", true, null, null);

                    //Helper.Email.notifyEHSConsultant(_q.CompanyId, cc, null, subject, body, null);

                    if (Helper.Questionnaire.ActionLog.Add((int)QuestionnaireActionList.SafetyQuestionnaireApprovedAssessmentComplete,
                                                    _q.QuestionnaireId,                         //QuestionnaireId
                                                    auth.UserId,                                //UserId
                                                    auth.FirstName,                             //FirstName
                                                    auth.LastName,                              //LastName
                                                    auth.RoleId,                                //RoleId
                                                    auth.CompanyId,                             //CompanyId
                                                    auth.CompanyName,                           //CompanyName
                                                    _q.RecommendedComments,                     //Comments
                                                    null,                                       //OldAssigned
                                                    recommended,                                //NewAssigned
                                                    _q.LevelOfSupervision,                      //NewAssigned2
                                                    null,                                       //NewAssigned3
                                                    null)                                       //NewAssigned4
                                                    == false)
                    {
                        //for now we do nothing if action log fails.
                    }
                }
                Helper.General.Redirect(Page, String.Format("SafetyPQ_QuestionnaireModify.aspx{0}", QueryStringModule.Encrypt("q=" + q)));
            }
        }
        catch (Exception ex)
        {
            Elmah.ErrorSignal.FromContext(Context).Raise(ex);
            throw new Exception(String.Format("Error: {0}", ex.Message));
        }
    }
    protected void grid_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        try
        {
            int QuestionnaireId = Convert.ToInt32(Request.QueryString["q"].ToString());
            QuestionnaireService qService = new QuestionnaireService();
            Questionnaire q = qService.GetByQuestionnaireId(QuestionnaireId);

            //CompanySiteCategoryStandardService cscsService = new CompanySiteCategoryStandardService();
            //CompanySiteCategoryStandard cscs = new CompanySiteCategoryStandard();
            //Modified by Ashley Goldstraw to use new repo. 16/9/15
            model.CompanySiteCategoryStandard cscs = new model.CompanySiteCategoryStandard();
            cscs.CompanyId = q.CompanyId;
            cscs.SiteId = Convert.ToInt32(e.NewValues["SiteId"]);
            if (e.NewValues["LocationSponsorUserId"] != null)
            {
                cscs.LocationSponsorUserId = Convert.ToInt32(e.NewValues["LocationSponsorUserId"]);
            }
            if (e.NewValues["CompanySiteCategoryId"] != null)
            {
                cscs.CompanySiteCategoryId = Convert.ToInt32(e.NewValues["CompanySiteCategoryId"]);
            }
            cscs.ModifiedByUserId = auth.UserId;
            cscs.ModifiedDate = DateTime.Now;
            if (e.NewValues["Approved"] != null)
            {
                switch (e.NewValues["Approved"].ToString())
                {
                    case "Yes":
                        cscs.Approved = true;
                        cscs.ApprovedByUserId = auth.UserId;
                        break;
                    case "No":
                        cscs.Approved = false;
                        cscs.ApprovedByUserId = auth.UserId;
                        break;
                    default:
                        break;
                }
            }
            else
            {
                cscs.Approved = null;
                cscs.ApprovedByUserId = null;
            }

            //cscsService.Save(cscs);
            companySiteCategoryStandardService.Insert(cscs);
            e.Cancel = true;
            detailGrid.CancelEdit();
            BindLocationData(q.CompanyId);

            string SiteName = "-";
            SitesService sService = new SitesService();
            Sites s = sService.GetBySiteId(cscs.SiteId);
            if (s != null) SiteName = s.SiteName;

            string SafetyCategory = "-";
            if (cscs.CompanySiteCategoryId != null)
            {
                CompanySiteCategoryService cscService = new CompanySiteCategoryService();
                CompanySiteCategory csc = cscService.GetByCompanySiteCategoryId(Convert.ToInt32(cscs.CompanySiteCategoryId));
                if (cscs != null) SafetyCategory = csc.CategoryName;
            }

            string LocationSponsorUser = "-";
            if (cscs.LocationSponsorUserId != null)
            {
                UsersService uService = new UsersService();
                Users u = uService.GetByUserId(Convert.ToInt32(cscs.LocationSponsorUserId));
                if (u != null) LocationSponsorUser = u.FirstName + " " + u.LastName;
            }

            string Approval = "-";
            if (cscs.Approved != null)
            {
                if (cscs.Approved == true)
                {
                    Approval = "Yes";
                }
                else
                {
                    Approval = "No";
                }
            }

            if (Helper.Questionnaire.ActionLog.Add((int)QuestionnaireActionList.LocationApprovalAdded,
                                                    q.QuestionnaireId,                         //QuestionnaireId
                                                    auth.UserId,                                //UserId
                                                    auth.FirstName,                             //FirstName
                                                    auth.LastName,                              //LastName
                                                    auth.RoleId,                                //RoleId
                                                    auth.CompanyId,                             //CompanyId
                                                    auth.CompanyName,                           //CompanyName
                                                    null,                                       //Comments
                                                    null,                                       //OldAssigned
                                                    SiteName,                                   //NewAssigned
                                                    SafetyCategory,                             //NewAssigned2
                                                    LocationSponsorUser,                        //NewAssigned3
                                                    Approval)                                   //NewAssigned4
                                                    == false)
            {
                //for now we do nothing if action log fails.
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    protected void grid_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        try
        {
            int QuestionnaireId = Convert.ToInt32(Request.QueryString["q"].ToString());
            QuestionnaireService qService = new QuestionnaireService();
            Questionnaire q = qService.GetByQuestionnaireId(QuestionnaireId);

            int CompanyId = q.CompanyId;
            int SiteId = Convert.ToInt32(e.OldValues["SiteId"]);

            //CompanySiteCategoryStandardService cscsService = new CompanySiteCategoryStandardService();
        
            //CompanySiteCategoryStandard cscs = cscsService.GetByCompanyIdSiteId(CompanyId, SiteId);
            //Modified by Ashley Goldstraw to use new repo as conflict was being caused with old repo. 16/9/2015
            model.CompanySiteCategoryStandard cscs = companySiteCategoryStandardService.Get(i => i.CompanyId == CompanyId && i.SiteId == SiteId, null);
            if (cscs == null) throw new Exception("Unexpected Error. Please refresh (or re-open) this page.");
            cscs.CompanyId = q.CompanyId;
            cscs.SiteId = Convert.ToInt32(e.NewValues["SiteId"]);
            if (e.NewValues["LocationSponsorUserId"] != null)
            {
                cscs.LocationSponsorUserId = Convert.ToInt32(e.NewValues["LocationSponsorUserId"]);
            }
            else
            {
                cscs.LocationSponsorUserId = null;
            }
            if (e.NewValues["CompanySiteCategoryId"] != null)
            {
                cscs.CompanySiteCategoryId = Convert.ToInt32(e.NewValues["CompanySiteCategoryId"]);
            }
            else
            {
                cscs.CompanySiteCategoryId = null;
            }
            cscs.ModifiedByUserId = auth.UserId;
            cscs.ModifiedDate = DateTime.Now;
            if (e.NewValues["Approved"] != null)
            {
                switch (e.NewValues["Approved"].ToString())
                {
                    case "Yes":
                        cscs.Approved = true;
                        cscs.ApprovedByUserId = auth.UserId;
                        break;
                    case "No":
                        cscs.Approved = false;
                        cscs.ApprovedByUserId = auth.UserId;
                        break;
                    default:

                        break;
                }

            }
            else
            {
                cscs.Approved = null;
                cscs.ApprovedByUserId = null;
            }

            //cscsService.Save(cscs);
            companySiteCategoryStandardService.Update(cscs);
            e.Cancel = true;
            detailGrid.CancelEdit();
            BindLocationData(q.CompanyId);

            string SiteName = "-";
            SitesService sService = new SitesService();
            Sites s = sService.GetBySiteId(cscs.SiteId);
            if (s != null) SiteName = s.SiteName;

            string SafetyCategory = "-";
            if (cscs.CompanySiteCategoryId != null)
            {
                CompanySiteCategoryService cscService = new CompanySiteCategoryService();
                CompanySiteCategory csc = cscService.GetByCompanySiteCategoryId(Convert.ToInt32(cscs.CompanySiteCategoryId));
                if (cscs != null) SafetyCategory = csc.CategoryName;
            }

            string LocationSponsorUser = "-";
            if (cscs.LocationSponsorUserId != null)
            {
                UsersService uService = new UsersService();
                Users u = uService.GetByUserId(Convert.ToInt32(cscs.LocationSponsorUserId));
                if (u != null) LocationSponsorUser = u.FirstName + " " + u.LastName;
            }

            string Approval = "-";
            if (cscs.Approved != null)
            {
                if (cscs.Approved == true)
                {
                    Approval = "Yes";
                }
                else
                {
                    Approval = "No";
                }
            }

            if (Helper.Questionnaire.ActionLog.Add((int)QuestionnaireActionList.LocationApprovalModified,
                                                    q.QuestionnaireId,                         //QuestionnaireId
                                                    auth.UserId,                                //UserId
                                                    auth.FirstName,                             //FirstName
                                                    auth.LastName,                              //LastName
                                                    auth.RoleId,                                //RoleId
                                                    auth.CompanyId,                             //CompanyId
                                                    auth.CompanyName,                           //CompanyName
                                                    null,                                       //Comments
                                                    null,                                       //OldAssigned
                                                    SiteName,                                   //NewAssigned
                                                    SafetyCategory,                             //NewAssigned2
                                                    LocationSponsorUser,                        //NewAssigned3
                                                    Approval)                                   //NewAssigned4
                                                    == false)
            {
                //for now we do nothing if action log fails.
            }

        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    protected void grid_InitNewRow(object sender, ASPxDataInitNewRowEventArgs e)
    {
        detailGrid.Columns["CompanySiteCategoryId"].Visible = true;
        detailGrid.Columns["CompanySiteCategoryDesc"].Visible = false;
    }

    protected void btnClose_Click(object sender, EventArgs e)
    {
        //System.Threading.Thread.Sleep(1000);
        string page;
        if (auth.RoleId == (int)RoleList.Contractor || auth.RoleId == (int)RoleList.PreQual)
        {
            page = "Default.aspx";
        }
        else
        {
            page = "SafetyPQ_Questionnaire.aspx";

            //try
            //{
            //    string q = Request.QueryString["q"].ToString();
            //    if (!String.IsNullOrEmpty(q))
            //    {
            //        QuestionnaireService _qs = new QuestionnaireService();
            //        Questionnaire _q = new Questionnaire();
            //        _q = _qs.GetByQuestionnaireId(Convert.ToInt32(q));
            //        if (_q.SubContractor == true) page = "SafetyPQ_Questionnaire_SubC.aspx?sc=1";
            //    }
            //}
            //catch (Exception ex) { }

        }
        Helper.General.Redirect(Page, page);
    }

    protected void GetDirectStatus(int q)
    {
        string answer = "Direct"; //Default: Direct
        QuestionnaireInitialResponseService qirService = new QuestionnaireInitialResponseService();
        QuestionnaireInitialResponse qir = qirService.GetByQuestionnaireIdQuestionId(q, "7");
        if (qir != null && !string.IsNullOrEmpty(qir.AnswerText))
        {
            //if (qir.AnswerText == "SubContractor")
            //{
            //    answer = qir.AnswerText;
            //}
            //else
            //{
            QuestionnaireService qService = new QuestionnaireService();
            Questionnaire questionnaire = qService.GetByQuestionnaireId(q);
            if (questionnaire != null)
            {
                if (questionnaire.IsVerificationRequired == true)
                { //if "No" selected for any Verification Questionnaire, Level of Supervision Status = "Direct", else "InDirect".
                    int count = 0;
                    QuestionnaireVerificationResponseFilters qvrFilters = new QuestionnaireVerificationResponseFilters();
                    qvrFilters.Append(QuestionnaireVerificationResponseColumn.AnswerText, "No");
                    qvrFilters.Append(QuestionnaireVerificationResponseColumn.QuestionnaireId, q.ToString());
                    TList<QuestionnaireVerificationResponse> qvrList = DataRepository.QuestionnaireVerificationResponseProvider.GetPaged(qvrFilters.ToString(), null, 0, 100, out count);
                    if (count > 0)
                    {
                        qir.AnswerText = "Direct";
                    }
                    else
                    {
                        qir.AnswerText = "InDirect";
                    }

                    qirService.Save(qir);
                    answer = qir.AnswerText;
                }
            }
            //}
        }
        cbSupervisionQuestionnaire.SelectedItem.Value = answer;
        cbSupervisionQuestionnaire.Value = answer;
    }
    protected void btnScoreAnalysis_Click(object sender, EventArgs e)
    {
        //System.Threading.Thread.Sleep(1000);
        string q = Request.QueryString["q"].ToString();
        Response.Redirect(String.Format("javascript:popUp('PopUps/SafetyPQ_AreaAnalysis.aspx{0}'); window.location='SafetyPQ_QuestionnaireModify.aspx{0}';", QueryStringModule.Encrypt("q=" + q)), false);
    }

    protected void btnRiskSummary_Click(object sender, EventArgs e)
    {
        //System.Threading.Thread.Sleep(1000);
        string q = Request.QueryString["q"].ToString();
        Response.Redirect(String.Format("javascript:popUp('PopUps/SafetyPQ_QuestionnaireReport.aspx{0}'); window.location='SafetyPQ_QuestionnaireModify.aspx{1}';", QueryStringModule.Encrypt("ID=" + q), QueryStringModule.Encrypt("q=" + q)), false);
    }

    protected void LoadEhs(int q)
    {
        QuestionnaireService questionnaireService = new QuestionnaireService();
        Questionnaire questionnaireEntity = questionnaireService.GetByQuestionnaireId(q);

        CompaniesService companiesService = new CompaniesService();
        Companies companiesEntity = companiesService.GetByCompanyId(questionnaireEntity.CompanyId);

        //save ehs
        if (companiesEntity.EhsConsultantId.HasValue)
        {
            cbEhsConsultant.Value = companiesEntity.EhsConsultantId;

            if (companiesEntity.EhsConsultantId != null)
            {
                EhsConsultantService eService = new EhsConsultantService();
                EhsConsultant ehs = eService.GetByEhsConsultantId(Convert.ToInt32(companiesEntity.EhsConsultantId));

                UsersService uService = new UsersService();
                Users u = uService.GetByUserId(ehs.UserId);

                lblHSAssessor_Current.Text = u.LastName + ", " + u.FirstName;
            }
        }
    }


    protected void ReQualify(int _q, bool endResponse, bool button, bool changeToDirectContractor, bool changeToSubContractor, string comments)
    {
        QuestionnaireService qService = new QuestionnaireService();
        Questionnaire q_original = qService.GetByQuestionnaireId(_q);
        int newQuestionnaireId = -1;
        if (q_original != null)
        {
            CompaniesService cService = new CompaniesService();
            Companies company = cService.GetByCompanyId(q_original.CompanyId);

            //08-Feb-2010 Change Company Status to 'Re-Qualification Incomplete (In Progress)' instead of 'Being Assessed'
            company.CompanyStatusId = (int)CompanyStatusList.Requal_Incomplete;

            cService.Save(company);

            Questionnaire q_new = q_original.Copy();
            q_new.EntityState = EntityState.Added;
            //Start:Enhancement_008 by Debashis
            //if (q_original.Status == 3 && q_original.IsReQualification == false) // QuestionnaireStatusList.AssesmentComplete=3 and Prequalify
            //{
            //    q_new.IsReQualification = true;
            //}
            //else
            //{
            //    q_new.IsReQualification = false;
            //}           

            //q_new.Status = (int)QuestionnaireStatusList.Incomplete;

           // q_new.IsReQualification = true;
            //End: Enhancement_008 by Debashis

            //Modified enhancement
            if (q_original.Status == 3 || q_original.Status ==1) // QuestionnaireStatusList.AssesmentComplete=3 and Prequalify
            {
                q_new.IsReQualification = true;
            }
            else
            {
                q_new.IsReQualification = false;
            }

            q_new.Status = (int)QuestionnaireStatusList.Incomplete;

            // q_new.IsReQualification = true;
             //End: Modified

            q_new.InitialStatus = (int)QuestionnaireStatusList.Incomplete;
            q_new.MainStatus = (int)QuestionnaireStatusList.Incomplete;
            q_new.VerificationStatus = (int)QuestionnaireStatusList.Incomplete;
            q_new.IsMainRequired = false;
            q_new.IsVerificationRequired = false;
            q_new.InitialRiskAssessment = null;
            q_new.InitialCategoryHigh = null;
            q_new.InitialModifiedByUserId = null;
            q_new.InitialModifiedDate = null;
            q_new.MainAssessmentStatus = null;
            q_new.MainAssessmentRiskRating = null;
            q_new.VerificationRiskRating = null;
            q_new.MainScoreExpectations = null;
            q_new.MainScoreOverall = null;
            q_new.MainScorePexpectations = null;
            q_new.MainScorePoverall = null;
            q_new.MainScorePresults = null;
            q_new.MainScorePstaffing = null;
            q_new.MainScorePsystems = null;
            q_new.MainScoreResults = null;
            q_new.MainScoreStaffing = null;
            q_new.MainScoreSystems = null;
            q_new.MainAssessmentDate = null;
            q_new.MainAssessmentValidTo = null;
            q_new.MainModifiedByUserId = null;
            q_new.MainModifiedDate = null;
            q_new.VerificationModifiedByUserId = null;
            q_new.VerificationModifiedDate = null;
            q_new.LevelOfSupervision = null;
            q_new.ProcurementModified = false;
            q_new.ApprovedByUserId = null;
            q_new.ApprovedDate = null;
            q_new.Recommended = null;
            q_new.RecommendedComments = null;
            q_new.CreatedByUserId = auth.UserId;
            q_new.CreatedDate = DateTime.Now;
            q_new.ModifiedByUserId = auth.UserId;
            q_new.ModifiedDate = DateTime.Now;
            q_new.QuestionnairePresentlyWithActionId = (int)QuestionnairePresentlyWithActionList.ProcurementQuestionnaire;
            q_new.QuestionnairePresentlyWithSince = DateTime.Now;
            if (changeToDirectContractor)
            {
                q_new.SubContractor = false;
                Companies c = cService.GetByCompanyId(q_original.CompanyId);
                c.CompanyStatus2Id = (int)CompanyStatus2List.Contractor;
                q_new.SubContractor = false;
                cService.Update(c);
            }

            if (changeToSubContractor)
            {
                q_new.SubContractor = true;
                Companies c = cService.GetByCompanyId(q_original.CompanyId);
                c.CompanyStatus2Id = (int)CompanyStatus2List.SubContractor;
                q_new.SubContractor = true;
                cService.Update(c);
            }

            qService.Save(q_new);
            q_original.QuestionnairePresentlyWithActionId = (int)QuestionnairePresentlyWithActionList.NoActionRequired;
            q_original.QuestionnairePresentlyWithSince = null;
            qService.Save(q_original);

            newQuestionnaireId = q_new.QuestionnaireId;

            //1. Procurement
            QuestionnaireInitialResponseService qirService = new QuestionnaireInitialResponseService();
            TList<QuestionnaireInitialResponse> qir_original_list = qirService.GetByQuestionnaireId(_q);
            if (qir_original_list != null)
            {
                foreach (QuestionnaireInitialResponse qir_original in qir_original_list)
                {
                    QuestionnaireInitialResponse qir_new = qir_original.Copy();
                    qir_new.EntityState = EntityState.Added;
                    qir_new.QuestionnaireId = newQuestionnaireId; //change QuestionnaireId to new one
                    qirService.Save(qir_new);
                    qir_new.Dispose();
                }
                try
                {
                    qir_original_list.Dispose();
                }
                catch (Exception ex) { Elmah.ErrorSignal.FromContext(Context).Raise(ex); }
            }

            if (!changeToSubContractor)
            {
                //Services Selected
                QuestionnaireServicesSelectedService qssService = new QuestionnaireServicesSelectedService();
                TList<QuestionnaireServicesSelected> qss_original_list = qssService.GetByQuestionnaireId(_q);
                if (qss_original_list != null)
                {
                    foreach (QuestionnaireServicesSelected qss_original in qss_original_list)
                    {
                        QuestionnaireServicesSelected qss_new = qss_original.Copy();
                        qss_new.EntityState = EntityState.Added;
                        qss_new.QuestionnaireId = newQuestionnaireId; //change QuestionnaireId to new one
                        qssService.Save(qss_new);
                        qss_new.Dispose();
                    }
                    try
                    {
                        qss_original_list.Dispose();
                    }
                    catch (Exception ex) { Elmah.ErrorSignal.FromContext(Context).Raise(ex); }
                }
            }

            //2. Supplier
            QuestionnaireMainResponseService qmrService = new QuestionnaireMainResponseService();
            TList<QuestionnaireMainResponse> qmr_original_list = qmrService.GetByQuestionnaireId(_q);
            if (qmr_original_list != null)
            {
                foreach (QuestionnaireMainResponse qmr_original in qmr_original_list)
                {
                    QuestionnaireMainResponse qmr_new = qmr_original.Copy();
                    qmr_new.EntityState = EntityState.Added;
                    qmr_new.QuestionnaireId = newQuestionnaireId; //change QuestionnaireId to new one
                    if (qmr_new.AnswerId == 0) qmr_new.AnswerText = DateTime.Now.ToString("dd/MM/yyyy");
                    qmrService.Save(qmr_new);
                    qmr_new.Dispose();
                }
                try
                {
                    qmr_original_list.Dispose();
                }
                catch (Exception ex) { Elmah.ErrorSignal.FromContext(Context).Raise(ex); }
            }


            //3. Supplier Attachments
            QuestionnaireMainAttachmentService qmaService = new QuestionnaireMainAttachmentService();
            TList<QuestionnaireMainAttachment> qma_original_list = qmaService.GetByQuestionnaireId(_q);
            if (qma_original_list != null)
            {
                foreach (QuestionnaireMainAttachment qma_original in qma_original_list)
                {
                    QuestionnaireMainAttachment qma_new = qma_original.Copy();
                    qma_new.EntityState = EntityState.Added;
                    qma_new.QuestionnaireId = newQuestionnaireId; //change QuestionnaireId to new one
                    qmaService.Save(qma_new);
                    qma_new.Dispose();
                }
                try
                {
                    qma_original_list.Dispose();
                }
                catch (Exception ex) { Elmah.ErrorSignal.FromContext(Context).Raise(ex); }
            }


            if (!changeToSubContractor)
            {
                //3. Verification Section
                QuestionnaireVerificationSectionService qvsService = new QuestionnaireVerificationSectionService();
                QuestionnaireVerificationSection qvs_original = qvsService.GetByQuestionnaireId(_q);
                if (qvs_original != null)
                {
                    QuestionnaireVerificationSection qvs_new = qvs_original.Copy();
                    qvs_new.EntityState = EntityState.Added;
                    qvs_new.QuestionnaireId = newQuestionnaireId; //change QuestionnaireId to new one
                    qvsService.Save(qvs_new);
                    qvs_new.Dispose();
                    try
                    {
                        qvs_original.Dispose();
                    }
                    catch (Exception ex) { Elmah.ErrorSignal.FromContext(Context).Raise(ex); }
                }


                //3. Verification Response
                QuestionnaireVerificationResponseService qvrService = new QuestionnaireVerificationResponseService();
                TList<QuestionnaireVerificationResponse> qvr_original_list = qvrService.GetByQuestionnaireId(_q);
                if (qvr_original_list != null)
                {
                    foreach (QuestionnaireVerificationResponse qvr_original in qvr_original_list)
                    {
                        if (qvr_original.SectionId == 8 && qvr_original.QuestionId == 3)
                        {
                            //there should be none of these anymore.. but put this here just in case.
                        }
                        else
                        {
                            QuestionnaireVerificationResponse qvr_new = qvr_original.Copy();
                            qvr_new.EntityState = EntityState.Added;
                            qvr_new.QuestionnaireId = newQuestionnaireId; //change QuestionnaireId to new one
                            qvrService.Save(qvr_new);
                            qvr_new.Dispose();
                        }
                    }
                    try
                    {
                        qvr_original_list.Dispose();
                    }
                    catch (Exception ex) { Elmah.ErrorSignal.FromContext(Context).Raise(ex); }
                }


                //3. Verification - Attachments
                QuestionnaireVerificationAttachmentService qvaService = new QuestionnaireVerificationAttachmentService();
                TList<QuestionnaireVerificationAttachment> qva_original_list = qvaService.GetByQuestionnaireId(_q);
                if (qva_original_list != null)
                {
                    foreach (QuestionnaireVerificationAttachment qva_original in qva_original_list)
                    {
                        if (qva_original.SectionId == 8 && qva_original.QuestionId == 3)
                        {
                            //there should be none of these anymore.. but put this here just in case.
                        }
                        else
                        {
                            QuestionnaireVerificationAttachment qva_new = qva_original.Copy();
                            qva_new.EntityState = EntityState.Added;
                            qva_new.QuestionnaireId = newQuestionnaireId; //change QuestionnaireId to new one
                            qvaService.Save(qva_new);
                            qva_new.Dispose();
                        }
                    }
                    try
                    {
                        qva_original_list.Dispose();
                    }
                    catch (Exception ex) { Elmah.ErrorSignal.FromContext(Context).Raise(ex); }
                }
            }

            if (Helper.Questionnaire.ActionLog.Add((int)QuestionnaireActionList.ProcurementQuestionnaireCreated,
                                                        q_new.QuestionnaireId,                      //QuestionnaireId
                                                        auth.UserId,                                //UserId
                                                        auth.FirstName,                             //FirstName
                                                        auth.LastName,                              //LastName
                                                        auth.RoleId,                                //RoleId
                                                        auth.CompanyId,                             //CompanyId
                                                        auth.CompanyName,                           //CompanyName
                                                        comments,                                   //Comments
                                                        null,                                       //OldAssigned
                                                        null,                                       //NewAssigned
                                                        null,                                       //NewAssigned2
                                                        null,                                       //NewAssigned3
                                                        null)                                       //NewAssigned4
                                                        == false)
            {
                //for now we do nothing if action log fails.
            }

        }
        if (button)
        {
            //Response.Redirect("javascript:alert('Safety Re-Qualification process initiated.');window.location='SafetyPQ_QuestionnaireInitial.aspx" +
            //QueryStringModule.Encrypt("q=" + newQuestionnaireId.ToString()) + "';", endResponse);
            ASPxPopupControl1.CloseAction = DevExpress.Web.ASPxClasses.CloseAction.MouseOut; //debashis for resizing popup issue of DevExpress
            Helper.General.Redirect(Page, "Safety Re-Qualification process initiated.", "SafetyPQ_QuestionnaireInitial.aspx" + QueryStringModule.Encrypt("q=" + newQuestionnaireId.ToString()));
            
        }
        else
        {
            //Response.Redirect("SafetyPQ_QuestionnaireInitial.aspx" + QueryStringModule.Encrypt("q=" + newQuestionnaireId.ToString()), endResponse);
            Helper.General.Redirect(Page, "SafetyPQ_QuestionnaireInitial.aspx" + QueryStringModule.Encrypt("q=" + newQuestionnaireId.ToString()));
        }
        //ASPxPopupControl1.CloseAction = DevExpress.Web.ASPxClasses.CloseAction.MouseOut; //debashis for resizing popup issue of DevExpress
    }
    protected void btnReQualify_Click(object sender, EventArgs e)
    {
        ReQualify(Convert.ToInt32(Request.QueryString["q"]), true, true, false, false, null);
    }
    protected void btnForciblyRequireRequalify_Click(object sender, EventArgs e) //DT374 Ashley Goldstraw 6/1/2015
    {
        string comments = mbForciblyRequireRequalify.Text;
        ReQualify(Convert.ToInt32(Request.QueryString["q"]), true, true, false, false,comments);
    }
    protected void btnReQualify2_Click(object sender, EventArgs e)
    {
        ReQualify(Convert.ToInt32(Request.QueryString["q"]), true, true, true, false, null);
    }
    protected void btnReQualify3_Click(object sender, EventArgs e)
    {
        ReQualify(Convert.ToInt32(Request.QueryString["q"]), true, true, false, true, null);
    }
    protected void btnDelete_Click(object sender, EventArgs e)
    {
        QuestionnaireService qService = new QuestionnaireService();
        qService.DeleteByQuestionnaireId(Convert.ToInt32(Request.QueryString["q"]));
        Helper.General.Redirect(Page, "Questionnaire Deleted.", "SafetyPQ_Questionnaire.aspx");

    }
    protected void btnCompare_Click(object sender, EventArgs e)
    {
        //System.Threading.Thread.Sleep(1000);
        string q = Request.QueryString["q"].ToString();
        Response.Redirect(String.Format("javascript:popUp('PopUps/SafetyPQ_Compare.aspx{0}'); window.location='SafetyPQ_QuestionnaireModify.aspx{0}';", QueryStringModule.Encrypt("q=" + q)), false);

    }

    protected void compareEnable(int qNo)
    {
        btnCompare.Enabled = false;
        QuestionnaireService qService = new QuestionnaireService();
        CompaniesService cService = new CompaniesService();
        UsersService uService = new UsersService();

        Questionnaire qNew = qService.GetByQuestionnaireId(qNo);

        //find previous questionnaire to compare against
        int count = 0;
        QuestionnaireFilters qFilters = new QuestionnaireFilters();
        qFilters.Append(QuestionnaireColumn.CompanyId, qNew.CompanyId.ToString());
        qFilters.Append(QuestionnaireColumn.Status, ((int)QuestionnaireStatusList.AssessmentComplete).ToString());
        qFilters.AppendLessThan(QuestionnaireColumn.CreatedDate, qNew.CreatedDate.ToString("yyyy-MM-dd"));
        TList<Questionnaire> qList = DataRepository.QuestionnaireProvider.GetPaged(qFilters.ToString(), "CreatedDate DESC", 0, 100, out count);
        if (count > 0)
        {
            if (qNew.Status != (int)QuestionnaireStatusList.Incomplete) btnCompare.Enabled = true;
        }

        if (qNew.Status == (int)QuestionnaireStatusList.Incomplete)
        {
            btnCompare.Text = "Compare";
            btnCompare.Enabled = false;
        }
        if (qNew.Status == (int)QuestionnaireStatusList.BeingAssessed) btnCompare.Text = "Compare & Assess";
        if (qNew.Status == (int)QuestionnaireStatusList.AssessmentComplete) btnCompare.Text = "Compare";
    }

    protected void btnChangeLos_Click(object sender, EventArgs e)
    {
        //System.Threading.Thread.Sleep(1000);
        try
        {
            string q = Request.QueryString["q"];
            if (q != "New")
            {
                QuestionnaireService _qs = new QuestionnaireService();
                Questionnaire _q = new Questionnaire();
                _q = _qs.GetByQuestionnaireId(Convert.ToInt32(q));

                string los_old = _q.LevelOfSupervision;
                string comments_old = _q.MainAssessmentComments;

                _q.LevelOfSupervision = cbDirect.Value.ToString();
                _q.MainAssessmentComments = mRecommended.Text;

                string los_new = _q.LevelOfSupervision;
                string comments_new = _q.MainAssessmentComments;

                if (los_old != los_new)
                {
                    if (Helper.Questionnaire.ActionLog.Add((int)QuestionnaireActionList.LevelOfSupervisionChanged,
                                                        _q.QuestionnaireId,                      //QuestionnaireId
                                                        auth.UserId,                                //UserId
                                                        auth.FirstName,                             //FirstName
                                                        auth.LastName,                              //LastName
                                                        auth.RoleId,                                //RoleId
                                                        auth.CompanyId,                             //CompanyId
                                                        auth.CompanyName,                           //CompanyName
                                                        null,                                       //Comments
                                                        los_old,                                    //OldAssigned
                                                        los_new,                                    //NewAssigned
                                                        null,                                       //NewAssigned2
                                                        null,                                       //NewAssigned3
                                                        null)                                       //NewAssigned4
                                                        == false)
                    {
                        //empty.
                    }
                }
                if (comments_old != comments_new)
                {
                    if (Helper.Questionnaire.ActionLog.Add((int)QuestionnaireActionList.RecommendationCommentsChanged,
                                                        _q.QuestionnaireId,                      //QuestionnaireId
                                                        auth.UserId,                                //UserId
                                                        auth.FirstName,                             //FirstName
                                                        auth.LastName,                              //LastName
                                                        auth.RoleId,                                //RoleId
                                                        auth.CompanyId,                             //CompanyId
                                                        auth.CompanyName,                           //CompanyName
                                                        null,                                       //Comments
                                                        comments_old,                               //OldAssigned
                                                        comments_new,                               //NewAssigned
                                                        null,                                       //NewAssigned2
                                                        null,                                       //NewAssigned3
                                                        null)                                       //NewAssigned4
                                                        == false)
                    {
                        //empty
                    }
                }

                _qs.Save(_q);
                Helper.General.Redirect(Page, String.Format("SafetyPQ_QuestionnaireModify.aspx{0}", QueryStringModule.Encrypt("q=" + q)));
            }
        }
        catch (Exception ex)
        {
            Elmah.ErrorSignal.FromContext(Context).Raise(ex);
            Response.Write(String.Format("Error: {0}", ex.Message));
        }
    }

    protected void btnForceRequireVerification_Click(object sender, EventArgs e)
    {
        try
        {
            string q = Request.QueryString["q"];
            if (q != "New")
            {
                QuestionnaireService _qs = new QuestionnaireService();
                Questionnaire _q = new Questionnaire();
                _q = _qs.GetByQuestionnaireId(Convert.ToInt32(q));
                _q.IsVerificationRequired = true;
                _q.Status = (int)QuestionnaireStatusList.Incomplete;

                if (_q.InitialStatus == (int)QuestionnaireStatusList.Incomplete)
                {
                    _q.QuestionnairePresentlyWithActionId = (int)QuestionnairePresentlyWithActionList.ProcurementQuestionnaire;
                }
                else if (_q.InitialStatus == (int)QuestionnaireStatusList.BeingAssessed)
                {
                    _q.QuestionnairePresentlyWithActionId = (int)QuestionnairePresentlyWithActionList.HSAssessorQuestionnaireProcurement;
                }
                else
                {
                    _q.QuestionnairePresentlyWithActionId = (int)QuestionnairePresentlyWithActionList.SupplierQuestionnaireIncomplete;
                }

                _qs.Save(_q);
                Helper.General.Redirect(Page, String.Format("SafetyPQ_QuestionnaireModify.aspx{0}", QueryStringModule.Encrypt("q=" + q)));
            }
        }
        catch (Exception ex)
        {
            Elmah.ErrorSignal.FromContext(Context).Raise(ex);
            Response.Write(String.Format("Error: {0}", ex.Message));
        }
    }
    protected void btnForceDeleteVerification_Click(object sender, EventArgs e)
    {
        try
        {
            string q = Request.QueryString["q"];
            if (q != "New")
            {
                QuestionnaireService _qs = new QuestionnaireService();
                Questionnaire _q = new Questionnaire();
                _q = _qs.GetByQuestionnaireId(Convert.ToInt32(q));
                _q.IsVerificationRequired = false;
                _qs.Save(_q);
                _qs.DeleteByQuestionnaireId_VerificationOnly(Convert.ToInt32(q));

                Helper.General.Redirect(Page, String.Format("SafetyPQ_QuestionnaireModify.aspx{0}", QueryStringModule.Encrypt("q=" + q)));
            }
        }
        catch (Exception ex)
        {
            Elmah.ErrorSignal.FromContext(Context).Raise(ex);
            Response.Write(String.Format("Error: {0}", ex.Message));
        }
    }
    protected void btnSendBackToSupplier_Click(object sender, EventArgs e)
    {
        try
        {
            string q = Request.QueryString["q"];
            if (q != "New")
            {
                QuestionnaireService _qs = new QuestionnaireService();
                Questionnaire _q = _qs.GetByQuestionnaireId(Convert.ToInt32(q));

                //check if all assessment done.
                bool qmaAllAssessed = false;
                bool qvaAllAssessed = false;
                bool qmaNotApproved = false;
                bool qvaNotApproved = false;
                if (_q.IsMainRequired)
                {
                    QuestionnaireMainAssessmentService qmaService = new QuestionnaireMainAssessmentService();
                    TList<QuestionnaireMainAssessment> qmaTlist = qmaService.GetByQuestionnaireId(_q.QuestionnaireId);
                    if (qmaTlist.Count == 22) qmaAllAssessed = true;
                    foreach (QuestionnaireMainAssessment qma in qmaTlist)
                    {
                        if (qma.AssessorApproval == null) qmaAllAssessed = false;
                        if (qma.AssessorApproval == false) qmaNotApproved = true;
                    }
                }
                else
                {
                    qmaAllAssessed = true;
                }

                if (_q.IsVerificationRequired)
                {
                    QuestionnaireVerificationAssessmentService qvaService = new QuestionnaireVerificationAssessmentService();
                    TList<QuestionnaireVerificationAssessment> qvaTlist = qvaService.GetByQuestionnaireId(_q.QuestionnaireId);
                    if (qvaTlist.Count == 30) qvaAllAssessed = true;
                    foreach (QuestionnaireVerificationAssessment qva in qvaTlist)
                    {
                        if (qva.AssessorApproval == null) qvaAllAssessed = false;
                        if (qva.AssessorApproval == false) qvaNotApproved = true;
                    }
                }
                else
                {
                    qvaAllAssessed = true;
                }

                if (!qmaAllAssessed && !qvaAllAssessed)
                {
                    throw new Exception("Cannot change questionnaire status to [Incomplete].\nBoth the  Supplier & Verification Questionnaire questions have not been completely assessed.");
                }
                else
                {
                    if (!qmaAllAssessed) throw new Exception("Cannot change questionnaire status to [Incomplete].\nSupplier Questionnaire must have all questions completely assessed.");
                    if (!qvaAllAssessed) throw new Exception("Cannot change questionnaire status to [Incomplete].\nVerification Questionnaire must have all questions completely assessed.");
                }
                if (!qvaNotApproved && !qmaNotApproved)
                {
                    throw new Exception("Cannot change questionnaire status to [Incomplete].\nSupplier and Verification Questionnaire have been assessed as approved.");
                }
                if (qvaNotApproved) _q.VerificationStatus = (int)QuestionnaireStatusList.Incomplete;
                if (qmaNotApproved)
                {
                    _q.MainStatus = (int)QuestionnaireStatusList.Incomplete;
                }
                _q.AssessedByUserId = auth.UserId;
                _q.AssessedDate = DateTime.Now;
                _q.Status = (int)QuestionnaireStatusList.Incomplete;
                _q.QuestionnairePresentlyWithActionId = (int)QuestionnairePresentlyWithActionList.SupplierQuestionnaireIncomplete;
                _q.QuestionnairePresentlyWithSince = DateTime.Now;
                _qs.Save(_q);

                UsersService usersService = new UsersService();
                Users userEntity;
                userEntity = usersService.GetByUserId(auth.UserId);

                CompaniesService companiesService = new CompaniesService();
                Companies companiesEntity = companiesService.GetByCompanyId(_q.CompanyId);
                Companies companiesEntity2 = companiesService.GetByCompanyId(auth.CompanyId);

                //string HSAssessorPhone = String.Format("Your H&S Assessor is '{0}', {1} and is contactable via e-mail ({2}) or PHONE ({3})\n", userEntity.LastName, userEntity.FirstName, userEntity.Email, userEntity.PhoneNo);


                string PreReSq = "Contractor Safety Pre-Qualification";
                if (_q.IsReQualification) PreReSq = "Contractor Safety Re-Qualification";
                if (_q.SubContractor == true) PreReSq = "Sub " + PreReSq;

                //Modified by Ashley Goldstraw 8/9/2015 to allow for admins to edit emails.
                model.EmailTemplate eMailTemplate = emailTemplateService.Get(i => i.EmailTemplateName == "Contractor Safety Pre-Qualification � Questionnaire Assessed as Incomplete", null);
                string subject1 = eMailTemplate.EmailTemplateSubject;
                subject1 = subject1.Replace("{PreOrReQualification}", PreReSq);
                subject1 = subject1.Replace("{CompanyName}", companiesEntity.CompanyName);
                
                byte[] bodyByte = eMailTemplate.EmailBody;
                string mailbody = System.Text.Encoding.ASCII.GetString(bodyByte);
                mailbody = mailbody.Replace("{CompanyName}", companiesEntity.CompanyName);
                mailbody = mailbody.Replace("{AssessorLastName}", userEntity.LastName);
                mailbody = mailbody.Replace("{AssessorFirstName}", userEntity.FirstName);
                mailbody = mailbody.Replace("{AssessorEmail}", userEntity.Email);
                mailbody = mailbody.Replace("{AssessorPhone}", userEntity.PhoneNo);
                //Removed by Ashley Goldstraw 9/9/2015
                /*string subject = String.Format("{0}: Questionnaire assessed as INCOMPLETE for {1}", PreReSq, companiesEntity.CompanyName);
                string body = "Company Name: " + companiesEntity.CompanyName + "\n" +
                                "Your Safety Qualification has been assessed and found to be incomplete.\n" +
                                "You may now go back into the Alcoa Contractor Services Management System and alter/supply the relevant information" +
                                "\n\n" +
                                HSAssessorPhone;*/
                Configuration configuration = new Configuration();
                string tempTo = tbSupplierEmail.Text;
                if (tempTo == "")
                    tempTo = userEntity.Email;
                string[] to = { tempTo };
                string[] cc = { userEntity.Email }; //configuration.GetValue(ConfigList.ContactEmail) - AUACSMS Team CC Removed 9.Mar.11
                if (to == null) to = cc;
                //Helper.Email.sendEmail(to, cc, null, subject, body, null); //Modified by AG 9/9/2015
                //Helper.Email.logEmail(null, null, to, cc, null, subject, body, EmailLogTypeList.Questionnaire);
                emailLogService.Insert(to, cc, null, subject1, mailbody, "Questionnaire", true, null, null);


                //Response.Redirect("javascript:alert('Questionnaire Status set to [Incomplete] and an e-mail has been sent to the supplier to correct their questionnaire.');window.location='SafetyPQ_QuestionnaireModify.aspx" + QueryStringModule.Encrypt("q=" + q) + "'");



                if (Helper.Questionnaire.ActionLog.Add((int)QuestionnaireActionList.SafetyQuestionnaireNotApproved,
                                                    _q.QuestionnaireId,                         //QuestionnaireId
                                                    auth.UserId,                                //UserId
                                                    auth.FirstName,                             //FirstName
                                                    auth.LastName,                              //LastName
                                                    auth.RoleId,                                //RoleId
                                                    auth.CompanyId,                             //CompanyId
                                                    auth.CompanyName,                           //CompanyName
                                                    null,                                       //Comments
                                                    null,                                       //OldAssigned
                                                    tbSupplierEmail.Text,       //NewAssigned
                                                    null,                                       //NewAssigned2
                                                    null,                                       //NewAssigned3
                                                    null)                                       //NewAssigned4
                                                    == false)
                {
                    //for now we do nothing if action log fails.
                }


                Helper.General.Redirect(Page, "Questionnaire Status set to [Incomplete] and an e-mail has been sent to the supplier to correct their questionnaire.", "SafetyPQ_QuestionnaireModify.aspx" + QueryStringModule.Encrypt("q=" + q));

            }
        }
        catch (Exception ex)
        {
            Elmah.ErrorSignal.FromContext(Context).Raise(ex);
            throw new Exception(String.Format("Error: {0}", ex.Message));
        }

    }

    protected void btnPrint_Click(object sender, EventArgs e)
    {
        Helper.General.Redirect(Page, String.Format("printSQ.aspx{0}",
                                            QueryStringModule.Encrypt("q=" + Request.QueryString["q"] + "&printlayout=yes")));
    }

    protected void btnChangeProc_Click(object sender, EventArgs e)
    {
        string q = Request.QueryString["q"];
        //QuestionnaireInitialResponseService qirService = new QuestionnaireInitialResponseService();
        //QuestionnaireInitialResponse qir = qirService.GetByQuestionnaireIdQuestionId(Convert.ToInt32(q), "2");
        //Modified by Ashley Goldstraw 10/9/2015 to use new repo as old repo was haning database.
        int qid = Convert.ToInt32(q);
        model.QuestionnaireInitialResponse qir = qirService.Get(qi => qi.QuestionnaireId == qid && qi.QuestionId == "2", null);
        if (qir == null)
        {
            //qir.Dispose();
            qir = new model.QuestionnaireInitialResponse();
            qir.QuestionnaireId = Convert.ToInt32(q);
            qir.QuestionId = "2";
            qir.ModifiedByUserId = auth.UserId;
            qir.ModifiedDate = DateTime.Now;
            qir.AnswerText = cbProcurement.Value.ToString();
            qirService.Insert(qir);
        }
        else
        {
            qir.AnswerText = cbProcurement.Value.ToString();
            qirService.Update(qir);
        }
        string message = "Procurement Contact Changed.";
        string Comments = mbProcComments.Text;

        string NewProcContact = "";
        string AdditionalComments = "";

        UsersProcurementListService uplService = new UsersProcurementListService();
        DataSet dsUpl = uplService.GetByUserId(Convert.ToInt32(cbProcurement.Value.ToString()));
        if (dsUpl != null)
        {
            if (dsUpl.Tables[0] != null)
            {
                if (dsUpl.Tables[0].Rows[0].ItemArray[2] != DBNull.Value)
                {
                    NewProcContact = dsUpl.Tables[0].Rows[0].ItemArray[2].ToString();
                }
            }
        }
        if (String.IsNullOrEmpty(NewProcContact)) throw new Exception("Cannot Get User Full Name from Procurement List.");

        if (cbProcEmail.Checked) //send email
        {
            message = "Procurement Contact Changed & New Procurement Contact notified via E-Mail.";

            try
            {
                UsersService uService = new UsersService();
                Users u = uService.GetByUserId(Convert.ToInt32(cbProcurement.Value.ToString()));
                if (u == null) throw new Exception("User entity not found.");
                if (String.IsNullOrEmpty(u.Email)) throw new Exception("User Email is empty.");

                QuestionnaireService qService = new QuestionnaireService();
                Questionnaire _q = qService.GetByQuestionnaireId(Convert.ToInt32(q));
                CompaniesService cService = new CompaniesService();
                Companies companiesEntity = cService.GetByCompanyId(_q.CompanyId);

                Users userEntity = uService.GetByUserId(auth.UserId);
                Companies companiesEntity2 = cService.GetByCompanyId(auth.CompanyId);

                //Changed by Ashley Goldstraw 9/9/2015
                string PreReSq = "Contractor Safety Pre-Qualification";
                if (_q.IsReQualification) PreReSq = "Contractor Safety Re-Qualification";
                if (_q.SubContractor == true) PreReSq = "Sub " + PreReSq;
                if (!String.IsNullOrEmpty(mbProcComments.Text))
                {
                    AdditionalComments = String.Format("<br>Additional Comments from Submitter:<br>{0}<br>", mbProcComments.Text);
                }

                //string subject = String.Format("{0}: 0 - Procurement Contact of Safety Questionnaire for {1} re-assigned to you", PreReSq, companiesEntity.CompanyName);
                model.EmailTemplate eMailTemplate = emailTemplateService.Get(i => i.EmailTemplateName == "Questionnaire Procurement Contact Re-Assigned", null);
                byte[] bodyByte = eMailTemplate.EmailBody;
                string mailbody = System.Text.Encoding.ASCII.GetString(bodyByte);
                string subject = eMailTemplate.EmailTemplateSubject;
                subject = subject.Replace("{CompanyName}", companiesEntity.CompanyName);
                subject = subject.Replace("{PreOrReQualification}", PreReSq);
                mailbody = mailbody.Replace("{CompanyName}", companiesEntity.CompanyName);
                mailbody = mailbody.Replace("{PrevProcurementContact}", lblProcContact.Text);
                mailbody = mailbody.Replace("{NewProcurementContact}", NewProcContact);
                mailbody = mailbody.Replace("{AdditionalComments}", AdditionalComments);
                mailbody = mailbody.Replace("{ChangedByLastName}", userEntity.LastName);
                mailbody = mailbody.Replace("{ChangedByFirstName}", userEntity.FirstName);
                mailbody = mailbody.Replace("{ChangedByCompanyName}", companiesEntity2.CompanyName);
                mailbody = mailbody.Replace("{DateChanged}", _q.ModifiedDate.ToString());



               //Commented by Ashley Goldstraw 9/9/15.  Replaced with template email
                /*string body = "A Questionnaire's Procurement Contact has been re-assigned to you as follows:\n\n" +
                              "Company Name: " + companiesEntity.CompanyName + "\n" +
                              "Previous Procurement Contact: " + lblProcContact.Text + "\n" +
                              "New Procurement Contact: " + NewProcContact + "\n" +
                              AdditionalComments +
                              "\n\n" +
                              "Change submitted by: " + userEntity.LastName + ", " + userEntity.FirstName + " (" + companiesEntity2.CompanyName + ") at " + _q.ModifiedDate.ToString() + "\n";*/
                Configuration configuration = new Configuration();
                string[] to = { u.Email };
                string[] cc = { configuration.GetValue(ConfigList.ContactEmail) };
                //cc - AUACSMS Team CC Removed 9.Mar.11
                //Helper.Email.sendEmail(to, null, null, subject, body, null);  //Commented by AG
                //Helper.Email.logEmail(null, null, to, null, null, subject, body, EmailLogTypeList.Questionnaire); //Commented by AG

                emailLogService.Insert(to, null, null, subject, mailbody, "Questionnaire", true, null, null);

                if (cbProcEmail.Checked)
                {
                    Comments += " (E-Mail Sent)";
                }
                else
                {
                    Comments += " (No E-Mail Sent)";
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromContext(Context).Raise(ex);
            }
        }

        if (Helper.Questionnaire.ActionLog.Add((int)QuestionnaireActionList.ProcurementContactAssigned,
                                                       Convert.ToInt32(q),                         //QuestionnaireId
                                                       auth.UserId,                                //UserId
                                                       auth.FirstName,                             //FirstName
                                                       auth.LastName,                              //LastName
                                                       auth.RoleId,                                //RoleId
                                                       auth.CompanyId,                             //CompanyId
                                                       auth.CompanyName,                           //CompanyName
                                                       Comments,                                   //Comments
                                                       lblProcContact.Text,                        //OldAssigned
                                                       NewProcContact,                             //NewAssigned
                                                       null,                                       //NewAssigned2
                                                       null,                                       //NewAssigned3
                                                       null)                                       //NewAssigned4
                                                       == false)
        {
            //for now we do nothing if action log fails.
        }

        Helper.General.Redirect(Page, message, "SafetyPQ_QuestionnaireModify.aspx" + QueryStringModule.Encrypt(String.Format("q={0}", q)));
    }
    protected void btnChangeFunctionalManager_Click(object sender, EventArgs e)
    {
        string q = Request.QueryString["q"];
        //Modified by AG to use new repo
        //QuestionnaireInitialResponseService qirService = new QuestionnaireInitialResponseService();
        //QuestionnaireInitialResponse qir = qirService.GetByQuestionnaireIdQuestionId(Convert.ToInt32(q), "2_1");
        int qid = Convert.ToInt32(q);
        model.QuestionnaireInitialResponse qir = qirService.Get(qi => qi.QuestionnaireId == qid && qi.QuestionId == "2_1",null);

        if (qir == null)
        {
            //qir.Dispose();
            qir = new model.QuestionnaireInitialResponse();
            qir.QuestionnaireId = Convert.ToInt32(q);
            qir.QuestionId = "2_1";
            qir.ModifiedByUserId = auth.UserId;
            qir.ModifiedDate = DateTime.Now;
            qir.AnswerText = cbFunctionalManager_New.Value.ToString();
            qirService.Insert(qir);
        }
        else
        {
            qir.AnswerText = cbFunctionalManager_New.Value.ToString();
            qirService.Update(qir);
        }
        string message = "Procurement Functional Supervisor.";
        string Comments = mbFunctionalManager_AdditionalComments.Text;

        string NewProcContact = "";
        string AdditionalComments = "";

        UsersProcurementListService uplService = new UsersProcurementListService();
        DataSet dsUpl = uplService.GetByUserId(Convert.ToInt32(cbFunctionalManager_New.Value.ToString()));
        if (dsUpl != null)
        {
            if (dsUpl.Tables[0] != null)
            {
                if (dsUpl.Tables[0].Rows[0].ItemArray[2] != DBNull.Value)
                {
                    NewProcContact = dsUpl.Tables[0].Rows[0].ItemArray[2].ToString();
                }
            }
        }
        if (String.IsNullOrEmpty(NewProcContact)) throw new Exception("Cannot Get User Full Name from Procurement List.");

        if (cbFunctionalManager_Notify.Checked) //send email
        {
            message = "Procurement Functional Supervisor Changed & New Procurement Functional Supervisor notified via E-Mail.";

            try
            {
                UsersService uService = new UsersService();
                Users u = uService.GetByUserId(Convert.ToInt32(cbFunctionalManager_New.Value.ToString()));
                if (u == null) throw new Exception("User entity not found.");
                if (String.IsNullOrEmpty(u.Email)) throw new Exception("User Email is empty.");

                QuestionnaireService qService = new QuestionnaireService();
                Questionnaire _q = qService.GetByQuestionnaireId(Convert.ToInt32(q));
                //CompaniesService cService = new CompaniesService();//Changed by AG
                model.Company companiesEntity = companyService.Get(cx => cx.CompanyId == _q.CompanyId, null);
                //Companies companiesEntity = cService.GetByCompanyId(_q.CompanyId);

                Users userEntity = uService.GetByUserId(auth.UserId);
                //Companies companiesEntity2 = cService.GetByCompanyId(auth.CompanyId);
                model.Company companiesEntity2 = companyService.Get(cx => cx.CompanyId == auth.CompanyId, null);
                string PreReSq = "Contractor Safety Pre-Qualification";
                if (_q.IsReQualification) PreReSq = "Contractor Safety Re-Qualification";
                if (_q.SubContractor == true) PreReSq = "Sub " + PreReSq;
                //string subject = String.Format("{PreOrReQualification}: 0 - Procurement Functional Supervisor of Safety Questionnaire for {CompanyName} re-assigned to you", PreReSq, companiesEntity.CompanyName);
                if (!String.IsNullOrEmpty(mbFunctionalManager_AdditionalComments.Text))
                {
                    AdditionalComments = String.Format("<br>Additional Comments from Submitter:<br>{0}<br>", mbFunctionalManager_AdditionalComments.Text);
                }
                //Modified by Ashley Goldstraw 9/9/15 DT 254
                model.EmailTemplate eMailTemplate = emailTemplateService.Get(i => i.EmailTemplateName == "Questionnaire Procurement Functional Supervisor Re-Assigned", null);
                byte[] bodyByte = eMailTemplate.EmailBody;
                string mailbody = System.Text.Encoding.ASCII.GetString(bodyByte);
                string subject = eMailTemplate.EmailTemplateSubject;
                subject = subject.Replace("{CompanyName}", companiesEntity.CompanyName);
                subject = subject.Replace("{PreOrReQualification}", PreReSq);
                mailbody = mailbody.Replace("{CompanyName}", companiesEntity.CompanyName);
                mailbody = mailbody.Replace("{PrevProcurementContact}", lblFunctionalManager_Current.Text);
                mailbody = mailbody.Replace("{NewProcurementContact}", NewProcContact);
                mailbody = mailbody.Replace("{AdditionalComments}", AdditionalComments);
                mailbody = mailbody.Replace("{ChangedByLastName}", userEntity.LastName);
                mailbody = mailbody.Replace("{ChangedByFirstName}", userEntity.FirstName);
                mailbody = mailbody.Replace("{ChangedByCompanyName}", companiesEntity2.CompanyName);
                mailbody = mailbody.Replace("{DateChanged}", _q.ModifiedDate.ToString());

                //Modified by Ashley Goldstraw 9/9/15 DT 254
                /*string body = "A Questionnaire's Procurement Functional Supervisor has been re-assigned to you as follows:\n\n" +
                              "Company Name: " + companiesEntity.CompanyName + "\n" +
                              "Previous Procurement Functional Supervisor: " + lblFunctionalManager_Current.Text + "\n" +
                              "New Procurement Functional Supervisor: " + NewProcContact + "\n" +
                              AdditionalComments +
                              "\n\n" +
                              "Change submitted by: " + userEntity.LastName + ", " + userEntity.FirstName + " (" + companiesEntity2.CompanyName + ") at " + _q.ModifiedDate.ToString() + "\n";
                */


                Configuration configuration = new Configuration();
                string[] to = { u.Email };
                string[] cc = { configuration.GetValue(ConfigList.ContactEmail) };
                //cc - AUACSMS Team CC Removed 9.Mar.11
                //Modified by Ashley Goldstraw 9/9/15 DT 254
                //Helper.Email.sendEmail(to, null, null, subject, body, null);
                //Helper.Email.logEmail(null, null, to, null, null, subject, body, EmailLogTypeList.Questionnaire);
                emailLogService.Insert(to, null, null, subject, mailbody, "Questionnaire", true, null, null);

                if (cbFunctionalManager_Notify.Checked)
                {
                    Comments += " (E-Mail Sent)";
                }
                else
                {
                    Comments += " (No E-Mail Sent)";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromContext(Context).Raise(ex);
            }
        }

        if (Helper.Questionnaire.ActionLog.Add((int)QuestionnaireActionList.ProcurementFunctionalManagerAssigned,
                                                       Convert.ToInt32(q),                         //QuestionnaireId
                                                       auth.UserId,                                //UserId
                                                       auth.FirstName,                             //FirstName
                                                       auth.LastName,                              //LastName
                                                       auth.RoleId,                                //RoleId
                                                       auth.CompanyId,                             //CompanyId
                                                       auth.CompanyName,                           //CompanyName
                                                       Comments,                                   //Comments
                                                       lblFunctionalManager_Current.Text,          //OldAssigned
                                                       NewProcContact,                             //NewAssigned
                                                       null,                                       //NewAssigned2
                                                       null,                                       //NewAssigned3
                                                       null)                                       //NewAssigned4
                                                       == false)
        {
            //for now we do nothing if action log fails.
        }

        Helper.General.Redirect(Page, message, "SafetyPQ_QuestionnaireModify.aspx" + QueryStringModule.Encrypt(String.Format("q={0}", q)));

    }
    protected void btnChangeHSAssessor_Click(object sender, EventArgs e)
    {
        string q = Request.QueryString["q"];
        string AdditionalComments = "";
        QuestionnaireService _qs = new QuestionnaireService();
        Questionnaire _q = new Questionnaire();
        _q = _qs.GetByQuestionnaireId(Convert.ToInt32(q));

        //Ashley Goldstraw changed the services to the repo services
       

        //CompaniesService cService = new CompaniesService();
        //Companies c = cService.GetByCompanyId(_q.CompanyId);
        model.Company c = companyService.Get(cx => cx.CompanyId == _q.CompanyId, null);
        int? ehsconsultantid = null;
        if (cbHSAssessor_New.Value != null) ehsconsultantid = Convert.ToInt32(cbHSAssessor_New.Value);

        int? ehsconsultantid_o = c.EHSConsultantId;

        c.EHSConsultantId = ehsconsultantid;
        c.ModifiedbyUserId = auth.UserId;
        companyService.Update(c);

        string message = "Company Safety Assessor Changed.";

        message = "Company Safety Assessor Changed & New Company Safety Assessor notified via E-Mail.";
        string ehsconsultant = "n/a";
        string ehsconsultant_o = "n/a";
        string ehsconsultant_email = "";

        UsersService uService = new UsersService();
        Users userEntity = uService.GetByUserId(auth.UserId);
        model.Company companiesEntity2 = companyService.Get(cy=>cy.CompanyId == auth.CompanyId,null);

        if (ehsconsultantid != null)
        {
            EhsConsultantService ehscService = new EhsConsultantService();
            EhsConsultant ehsc = ehscService.GetByEhsConsultantId((int)ehsconsultantid);
            Users u = uService.GetByUserId(ehsc.UserId);
            ehsconsultant = String.Format("{0}, {1}", u.LastName, u.FirstName);
            ehsconsultant_email = u.Email;
        }

        if (ehsconsultantid_o != null)
        {
            EhsConsultantService ehscService = new EhsConsultantService();
            EhsConsultant ehsc = ehscService.GetByEhsConsultantId((int)ehsconsultantid_o);
            Users u = uService.GetByUserId(ehsc.UserId);
            ehsconsultant_o = String.Format("{0}, {1}", u.LastName, u.FirstName);
        }

        //email them!
        string Comments = mbHSAssessor_AdditionalComments.Text; ;
        if (cbHSAssessor_Notify.Checked)
        {
            string PreReSq;
            if (!String.IsNullOrEmpty(mbHSAssessor_AdditionalComments.Text))
            {
                AdditionalComments = String.Format("\nAdditional Comments from Submitter:\n{0}\n", mbHSAssessor_AdditionalComments.Text);
            }

            if (_q.IsReQualification)
                PreReSq = "Contractor Safety Re-Qualification";
            else
                PreReSq = "Contractor Safety Pre-Qualification";
            if (_q.SubContractor == true) PreReSq = "Sub " + PreReSq;
            //Changes made by Ashley Goldstraw DT254 9/9/2015
            model.EmailTemplate eMailTemplate = emailTemplateService.Get(i => i.EmailTemplateName == "Safety Assessor Changed - Email to New Safety Assessor", null);
            byte[] bodyByte = eMailTemplate.EmailBody;
            string mailbody = System.Text.Encoding.ASCII.GetString(bodyByte);
            string subject = eMailTemplate.EmailTemplateSubject;
            subject = subject.Replace("{CompanyName}", c.CompanyName);
            subject = subject.Replace("{PreOrReQualification}", PreReSq);
            mailbody = mailbody.Replace("{CompanyName}", c.CompanyName);
            mailbody = mailbody.Replace("{PreviousSafetyAssessor}", ehsconsultant_o);
            mailbody = mailbody.Replace("{NewSafetyAssessor}", ehsconsultant);
            mailbody = mailbody.Replace("{AdditionalComments}", AdditionalComments);
            mailbody = mailbody.Replace("{ChangedByLastName}", userEntity.LastName);
            mailbody = mailbody.Replace("{ChangedByFirstName}", userEntity.FirstName);
            mailbody = mailbody.Replace("{ChangedByCompanyName}", companiesEntity2.CompanyName);
            mailbody = mailbody.Replace("{DateChanged}", DateTime.Now.ToString());

            //string subject = String.Format("{0}: 0 - {1}: Safety Assessor Changed", PreReSq, c.CompanyName);
            
            /*string body = "A Questionnaire's Company Safety Assessor has been re-assigned to you as follows:\n\n" +
                          "Company Name: " + c.CompanyName + "\n" +
                          "Previous Company Assessor: " + ehsconsultant_o + "\n" +
                          "New Company Assessor: " + ehsconsultant + "\n" +
                          AdditionalComments +
                          "\n\n" +
                          "Change submitted by: " + userEntity.LastName + ", " + userEntity.FirstName + " (" + companiesEntity2.CompanyName + ") at " + DateTime.Now.ToString() + "\n";
            */
            Configuration configuration = new Configuration();
            string[] to = { ehsconsultant_email };
            string[] csmteam = { configuration.GetValue(ConfigList.ContactEmail) };

            if (!String.IsNullOrEmpty(ehsconsultant_email))
            {
                //csmteam - AUACSMS Team CC Removed 9.Mar.11
                //Modified by AG 9/9/2015 DT254
                //Helper.Email.sendEmail(to, null, null, subject, body, null);
                //Helper.Email.logEmail(null, null, to, null, null, subject, body, EmailLogTypeList.Questionnaire);
                emailLogService.Insert(to, null, null, subject, mailbody, "Questionnaire", true, null, null);
            }
            else
            { // no safety assessor selected?! send to default ehs consultant - AUACSMS Team TO Removed 9.Mar.11
                ConfigService _cService = new ConfigService();
                Config _c = _cService.GetByKey("DefaultEhsConsultant_WAO");
                if (!String.IsNullOrEmpty(_c.Value))
                {
                    string[] defaultehs = { _c.Value };
                    //Helper.Email.sendEmail(defaultehs, null, null, subject, body, null);
                    //Helper.Email.logEmail(null, null, defaultehs, null, null, subject, body, EmailLogTypeList.Questionnaire);
                    emailLogService.Insert(defaultehs, null, null, subject, mailbody, "Questionnaire", true, null, null);
                }
                else
                {
                    //Helper.Email.sendEmail(csmteam, null, null, subject, body, null);
                    //Helper.Email.logEmail(null, null, csmteam, null, null, subject, body, EmailLogTypeList.Questionnaire);
                    emailLogService.Insert(csmteam, null, null, subject, mailbody, "Questionnaire", true, null, null);
                }
            }
        }

        if (cbHSAssessor_Notify.Checked)
        {
            Comments += " (E-Mail Sent)";
        }
        else
        {
            Comments += " (No E-Mail Sent)";
        }

        if (Helper.Questionnaire.ActionLog.Add((int)QuestionnaireActionList.SafetyAssessorAssigned,
                                                       Convert.ToInt32(q),                         //QuestionnaireId
                                                       auth.UserId,                                //UserId
                                                       auth.FirstName,                             //FirstName
                                                       auth.LastName,                              //LastName
                                                       auth.RoleId,                                //RoleId
                                                       auth.CompanyId,                             //CompanyId
                                                       auth.CompanyName,                           //CompanyName
                                                       Comments,                                   //Comments
                                                       ehsconsultant_o,                            //OldAssigned
                                                       ehsconsultant,                              //NewAssigned
                                                       null,                                       //NewAssigned2
                                                       null,                                       //NewAssigned3
                                                       null)                                       //NewAssigned4
                                                       == false)
        {
            //for now we do nothing if action log fails.
        }
        
        Helper.General.Redirect(Page, message, "SafetyPQ_QuestionnaireModify.aspx" + QueryStringModule.Encrypt("q=" + q));
        //Helper.General.Redirect(Page, message, "SafetyPQ_QuestionnaireModify.aspx?show=" + q);
    }
    protected void btnCAddInternalComment_Click(object sender, EventArgs e)
    {
        lblAddInternalComment_Error.Text = "";

        try
        {
            Helper.Questionnaire.ActionLog.Add((int)QuestionnaireActionList.InternalCommentAdded, Convert.ToInt32(Request.QueryString["q"]),
                auth.UserId,
                auth.FirstName,
                auth.LastName,
                auth.RoleId,
                auth.CompanyId,
                auth.CompanyName,
                mbAddInternalComment.Text,
                null, null, null, null, null);

            Helper.General.Redirect(Page, "Comment Added", "SafetyPQ_QuestionnaireModify.aspx" + QueryStringModule.Encrypt("q=" + Request.QueryString["q"]));
        }
        catch (Exception ex)
        {
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }

    }

    /// <summary>
    /// DT224 - add company note - new tab
    /// Some work done by TCS, alterations by Cindi Thornton 15/9/2015
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnCAddInternalNote_Click(object sender, EventArgs e)
    {
        lblAddInternalNote_Error.Text = "";
        int compID = Convert.ToInt32(ViewState["compID"]);
        string compName = ViewState["CompName"].ToString();
        try
        {
            //Only show the Note Added popup if CompanyNoteEntry.Add returned true
            if (Helper.Questionnaire.CompanyNoteEntry.Add((int)QuestionnaireActionList.CompanyNoteAdded,
                auth.UserId,
                auth.FirstName,
                auth.LastName,
                auth.RoleId,
                compID,
                compName,
                mbAddInternalNote.Text))
            {
                Helper.General.Redirect(Page, "Note Added", "SafetyPQ_QuestionnaireModify.aspx" + QueryStringModule.Encrypt("q=" + Request.QueryString["q"]));
            }
            else
            {
                Helper.General.Redirect(Page, "There was a problem - the note was not Added", "SafetyPQ_QuestionnaireModify.aspx" + QueryStringModule.Encrypt("q=" + Request.QueryString["q"]));
            }

            
        }
        catch (Exception ex)
        {
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }

    }

    protected void btnSendNotificationEmail_Click(object sender, EventArgs e)
    {
        try
        {
            string q = Request.QueryString["q"];
            QuestionnaireService qs = new QuestionnaireService();
            Questionnaire _q = qs.GetByQuestionnaireId(Convert.ToInt32(q));
            //log email
            QuestionnaireInitialContactService qicService = new QuestionnaireInitialContactService();
            QuestionnaireInitialContact qic = qicService.GetByQuestionnaireId(Convert.ToInt32(q));

            QuestionnaireInitialContactEmailService qiceService = new QuestionnaireInitialContactEmailService();
            QuestionnaireInitialContactEmail qice = new QuestionnaireInitialContactEmail();

            qice.QuestionnaireId = Convert.ToInt32(q);
            qice.EmailTypeId = (int)QuestionnaireInitialContactEmailTypeList.ExpiryReminder;
            qice.EmailSentDateTime = DateTime.Now;
            qice.ContactId = qic.QuestionnaireInitialContactId;
            qice.ContactEmail = qic.ContactEmail;
            qice.Subject = tbNotifySubject.Text;
            qice.Message = mbNotifyMessage.Text;
            qice.SentByUserId = auth.UserId;

            qiceService.Save(qice);

            //send email
            string[] to = { qic.ContactEmail };
            string[] cc = { auth.Email };
            //Helper.Email.sendEmail(to, cc, null, tbNotifySubject.Text, mbNotifyMessage.Text, null);
            //Helper.Email.logEmail(null, null, to, cc, null, tbNotifySubject.Text, mbNotifyMessage.Text, EmailLogTypeList.Questionnaire);
            emailLogService.Insert(to, cc, null, tbNotifySubject.Text, mbNotifyMessage.Text, "Questionnaire", true, null, null);
            Helper.General.Redirect(Page, "Contact Notification E-Mail Sent!", String.Format("SafetyPQ_QuestionnaireModify.aspx?q={0}", q));
        }
        catch (Exception ex)
        {
            Elmah.ErrorSignal.FromContext(Context).Raise(ex);
            throw new Exception("Error Occurred, Could not Send E-Mail. Please Contact your Administrator.");
        }
    }

    public void notifysupplier(int q, int DaysTillExpiry)
    {
        //btnNotifySupplierContact.Visible = true;
        //try
        //{
        //    QuestionnaireInitialContactService qicService = new QuestionnaireInitialContactService();
        //    QuestionnaireInitialContact qic = qicService.GetByQuestionnaireId(q);
        //    if (qic == null)
        //    {
        //        qic = new QuestionnaireInitialContact();

        //        QuestionnaireInitialResponseService qirService = new QuestionnaireInitialResponseService();
        //        QuestionnaireInitialResponse qir = null;
        //        qir = qirService.GetByQuestionnaireIdQuestionId(q, "1_1");
        //        qic.ContactFirstName = qir.AnswerText;

        //        qir = null;
        //        qir = qirService.GetByQuestionnaireIdQuestionId(q, "1_2");
        //        qic.ContactLastName = qir.AnswerText;

        //        qir = null;
        //        qir = qirService.GetByQuestionnaireIdQuestionId(q, "1_3");
        //        qic.ContactEmail = qir.AnswerText;

        //        qir = null;
        //        qir = qirService.GetByQuestionnaireIdQuestionId(q, "1_4");
        //        qic.ContactTitle = qir.AnswerText;

        //        qir = null;
        //        qir = qirService.GetByQuestionnaireIdQuestionId(q, "1_5");
        //        qic.ContactPhone = qir.AnswerText;

        //        qic.CreatedByUserId = 0;
        //        qic.ModifiedByUserId = auth.UserId;
        //        qic.ModifiedDate = DateTime.Now;
        //        qic.QuestionnaireId = q;

        //        qic.ContactStatusId = (int)QuestionnaireInitialContactStatusList.NotVerified;

        //        qicService.Save(qic);
        //    }

        //    Configuration configuration = new Configuration();
        //    tbNotifyFrom.Text = String.Format("AUA Alcoa Contractor Services [{0}]", configuration.GetValue(ConfigList.ContactEmail));

        //    if (!String.IsNullOrEmpty(qic.ContactFirstName) && !String.IsNullOrEmpty(qic.ContactLastName) && !String.IsNullOrEmpty(qic.ContactEmail))
        //    {
        //        tbNotifyTo.Text = String.Format("{0}, {1} [{2}]", qic.ContactFirstName, qic.ContactLastName, qic.ContactEmail);
        //    }
        //    else
        //    {
        //        throw new Exception("Contact First Name, Last Name or E-Mail is missing. Please correct in the Procurement Questionnaire.");
        //    }
        //    tbNotifyCC.Text = String.Format("{0}, {1} [{2}]", auth.FirstName, auth.LastName, auth.Email);

        //    string PreReSq = "Contractor Safety Pre-Qualification";
        //    QuestionnaireService qService = new QuestionnaireService();
        //    Questionnaire _questionnaire = qService.GetByQuestionnaireId(q);
        //    if (_questionnaire.IsReQualification) PreReSq = "Contractor Safety Re-Qualification";
        //    if (_questionnaire.SubContractor == true) PreReSq = "Sub " + PreReSq;
        //    CompaniesService cService = new CompaniesService();
        //    Companies _companies = cService.GetByCompanyId(_questionnaire.CompanyId);
        //    tbNotifySubject.Text = String.Format("{0}: 0 - Safety Qualification Expiring {1}", PreReSq, _companies.CompanyName);

        //    if(DaysTillExpiry <= 0)
        //    {
        //        tbNotifySubject.Text = tbNotifySubject.Text.Replace("Expiring", "Expired");
        //        mbNotifyMessage.Text = "Hi " + qic.ContactFirstName + " " + qic.ContactLastName + ",\n" +
        //                            "\n" +
        //                            "Your current Alcoa Contractor Safety Qualification has expired.\n" +
        //                            "\n\n" +
        //                            "To ensure that your organisation complies with both Government and Alcoa Corporate requirements it is requested that your organisation now update the Alcoa Safety Re-qualification process. \n\n" +
        //                            "If you presently do not have a current contract with Alcoa completing the Safety Re-qualification will allow your organisation to comply with one of Alcoa procurements requirements if the opportunity arises to create a new contract. If you decide not to complete the Safety Re-qualification process your approval to access site will continue to be denied." +
        //                            "\n\n" +
        //                            "Thanks,\n" +
        //                            auth.FirstName + " " + auth.LastName;
        //    }
        //    else
        //    {
        //    mbNotifyMessage.Text = "Hi " + qic.ContactFirstName + " " + qic.ContactLastName + ",\n" +
        //                            "\n" +
        //                            "Your current Alcoa Contractor Safety Qualification is due to expire in " + DaysTillExpiry.ToString() + " days.\n" +
        //                            "\n\n" +
        //                            "To ensure that your organisation complies with both Government and Alcoa Corporate requirements it is requested that your organisation now update the Alcoa Safety Re-qualification process. \n\n" +
        //                            "If you presently do not have a current contract with Alcoa completing the Safety Re-qualification will allow your organisation to comply with one of Alcoa procurements requirements if the opportunity arises to create a new contract. If you decide not to complete the Safety Re-qualification process your approval to access site will expire in " + DaysTillExpiry.ToString() + " days." +
        //                            "\n\n" +
        //                            "Thanks,\n" +
        //                            auth.FirstName + " " + auth.LastName;
        //    }

        //    lblNotifyNotes0.Text = "0";
        //    int countNoEmails = 0;
        //    QuestionnaireInitialContactEmailService qiceService = new QuestionnaireInitialContactEmailService();
        //    QuestionnaireInitialContactEmailFilters qiceFilters = new QuestionnaireInitialContactEmailFilters();
        //    qiceFilters.Append(QuestionnaireInitialContactEmailColumn.EmailTypeId,
        //                            ((int)QuestionnaireInitialContactEmailTypeList.ExpiryReminder).ToString());
        //    qiceFilters.Append(QuestionnaireInitialContactEmailColumn.ContactId, qic.QuestionnaireInitialContactId.ToString());
        //    qiceFilters.Append(QuestionnaireInitialContactEmailColumn.ContactEmail, qic.ContactEmail);
        //    TList<QuestionnaireInitialContactEmail> qiceList =
        //        DataRepository.QuestionnaireInitialContactEmailProvider.GetPaged(
        //        qiceFilters.ToString(),
        //        "EmailSentDateTime DESC",
        //        0,
        //        99999,
        //        out countNoEmails);

        //    if (countNoEmails > 0)
        //    {
        //        lblNotifyNotes0.Text = countNoEmails.ToString();
        //        lblNotifyNotes1.Text = "(Last E-Mail Sent: " + qiceList[0].EmailSentDateTime.ToString() + ")";
        //    }

        //    if (qic.ContactStatusId == (int)QuestionnaireInitialContactStatusList.Verified)
        //    {
        //        lblNotifyNotes.ForeColor = Color.Green;
        //        lblNotifyNotes.Visible = true;
        //        if (qic.VerifiedDate != null)
        //        {
        //            lblNotifyNotes.Text = "Contact Verified on " + qic.VerifiedDate.ToString();
        //        }
        //    }
        //    else
        //    {
        //        lblNotifyNotes.Visible = true;
        //        lblNotifyNotes.Text = "Warning: This Contact's details have not been verified. If the details below are incorrect or you would like to verify this contact, please do this in Question 1 of the Procurement Questionnaire.";
        //    }
        //    btnSendNotificationEmail.Enabled = true;
        //}
        //catch(Exception ex)
        //{
        //    Elmah.ErrorSignal.FromContext(Context).Raise(ex);
        //    btnSendNotificationEmail.Enabled = false;
        //    lblNotifyNotes.Visible = true;
        //    lblNotifyNotes.Text = "Error Occurred: " + ex.Message;
        //}
    }

    protected void gridHistoryLog_RowCreated(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewTableRowEventArgs e)
    {
        if (e.RowType != DevExpress.Web.ASPxGridView.GridViewRowType.Data) return;
        Label label = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "lblActionDetailed") as Label;
        string ActionDetailed = (string)grid.GetRowValues(e.VisibleIndex, "HistoryLog");
        if (label != null)
        {
            label.Text = ActionDetailed;
        }
    }

    protected void gridCompanyNote_RowCreated(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewTableRowEventArgs e)
    {
        if (e.RowType != DevExpress.Web.ASPxGridView.GridViewRowType.Data) return;
        Label label = gridCompanyNote.FindRowCellTemplateControl(e.VisibleIndex, null, "lblActionNote") as Label;
        string ActionDetailed = (string)gridCompanyNote.GetRowValues(e.VisibleIndex, "ActionNote");
        if (label != null)
        {
            label.Text = ActionDetailed;
        }
    }


    protected bool CompanyStatusChangeRequiresApproval(Questionnaire q, int newCompanyStatusId, string comments)
    {
        /* TODO: Write up this Method properly based upon the business rules below instead of using the current messy one.
         * 
         * ==================================================================
         * ========================= BUSINESS RULES =========================
         * ==================================================================
         * Questionnaire Status: Incomplete/Being Assessed
         * Questionnaire Type: Re-Qualification
         * Available Company Status changes:
         *      Requal Incomplete
         *      InActive - No Requal Required
         *      InActive - SQ Incomplete
         ********************************************************************      
         * Questionnaire Status: Incomplete/Being Assessed
         * Questionnaire Type: Pre-Qualification
         * Available Company Status changes:
         *      Being Assessed
         *      InActive - No Requal Required
         *      InActive - SQ Incomplete
         ********************************************************************      
         * Questionnaire Status: Assessment Complete
         * Questionnaire Required: Yes
         * Company Type: Contractor
         * Validity: True (Expiry Date > DateTime.Now())
         * Available Company Status changes:
         *      InActive
         *      InActive - No Requal Required
         *      Active
         *      Acceptable
         ********************************************************************
         * Questionnaire Status: Assessment Complete
         * Questionnaire Required: Yes
         * Company Type: Contractor
         * Validity: False (Expiry Date < DateTime.Now())
         * Available Company Status changes:
         *      InActive - No Requal Required
         ********************************************************************      
         * Questionnaire Status: Assessment Complete
         * Questionnaire Required: Yes
         * Company Type: Sub Contractor
         * Validity: True (Expiry Date > DateTime.Now())
         * Available Company Status changes:
         *      InActive - No Requal Required
         *      SubContractor
         *      SubContractor - Not Recommended
         ********************************************************************      
         * Questionnaire Status: Assessment Complete
         * Questionnaire Required: Yes
         * Company Type: Sub Contractor
         * Validity: False (Expiry Date < DateTime.Now())
         * Available Company Status changes:
         *      InActive - No Requal Required
         ********************************************************************      
         * Questionnaire Status: Assessment Complete
         * Questionnaire Required: No
         * Available Company Status changes:
         *      InActive
         *      Active - No PQ Required
         *      Active - No PQ Required - Restricted Access to Site
         ********************************************************************
         * Re-Activating:
         * Questionnaire Status: Incomplete / Being Assessed
         * Current Company Status:
         *      InActive - No Requal Required
         *      InActive - SQ Incomplete
         * Action:
         *      Reset to Procurement Stage, after changing company status
         ********************************************************************
         *  InActive - No Requal Required
            InActive - SQ Incomplete
            InActive
            Acceptable
            SubContractor
            Active
            Active - No PQ Required
            Active - No PQ Required - Restricted Access to Site
            SubContractor - Not Recommended
	            _q.QuestionnairePresentlyWithActionId = (int)QuestionnairePresentlyWithActionList.NoActionRequired;
	            _q.QuestionnairePresentlyWithSince = DateTime.Now;

            Questionnaire Incomplete
	            Procurement Questionnaire Incomplete
		            _q.QuestionnairePresentlyWithActionId = (int)QuestionnairePresentlyWithActionList.ProcurementQuestionnaire;
		            _q.QuestionnairePresentlyWithSince = DateTime.Now;
	            Procurement Questionnaire Being Assessed
		            _q.QuestionnairePresentlyWithActionId = (int)QuestionnairePresentlyWithActionList.HSAssessorQuestionnaireProcurement;
		            _q.QuestionnairePresentlyWithSince = DateTime.Now;
	            Procurement Questionnaire Assessment Complete
		            _q.QuestionnairePresentlyWithActionId = (int)QuestionnairePresentlyWithActionList.SupplierQuestionnaireSupplier;
		            _q.QuestionnairePresentlyWithSince = DateTime.Now;

            Questionnaire Being Assessed
	            _q.QuestionnairePresentlyWithActionId = (int)QuestionnairePresentlyWithActionList.HSAssessorQuestionnaire;
	            _q.QuestionnairePresentlyWithSince = DateTime.Now;

            Questionnaire Assessment Complete
	            Company Status - Being Assessed || Company Status - Requal Incomplete
		            _q.QuestionnairePresentlyWithActionId = (int)QuestionnairePresentlyWithActionList.ProcurementAssignCompanyStatus;
		            _q.QuestionnairePresentlyWithSince = DateTime.Now;

            Active
	            Send Email to H&S Assessors to please authorise for use on your location as applicable
	            Update All Company users of role 'PreQual' to 'Contractor'

         * 
         * 

            ---
            Log();
            Msg User();
         */

        try
        {
            bool RequiresApproval = false;
            bool CheckApprovalToChange = true;
            if (auth.RoleId == (int)RoleList.Administrator) //Admin allowed to change without going through approval process
                CheckApprovalToChange = false; 

            Configuration configuration = new Configuration();
            if (auth.Email == configuration.GetValue(ConfigList.ProcurementRegionalManager).ToString())
                CheckApprovalToChange = false;

            CompanyStatusService csService = new CompanyStatusService();
            CompanyStatus companyStatus = csService.GetByCompanyStatusId(newCompanyStatusId);
            if (companyStatus.ChangeRequiresApproval)
            {
                if (CheckApprovalToChange)
                {
                    RequiresApproval = true;

                    CompaniesService cService = new CompaniesService();
                    Companies companiesEntity = cService.GetByCompanyId(q.CompanyId);
                    CompanyStatus csCurrent = csService.GetByCompanyStatusId((int)companiesEntity.CompanyStatusId);
                    CompanyStatus csRequested = csService.GetByCompanyStatusId(newCompanyStatusId);
                    UsersService uService = new UsersService();
                    Users uRequested = uService.GetByUserId(auth.UserId);
                    Companies companiesEntity2 = cService.GetByCompanyId(uRequested.CompanyId);

                    CompanyStatusChangeApprovalService cscaService = new CompanyStatusChangeApprovalService();
                    CompanyStatusChangeApproval csca = new CompanyStatusChangeApproval();
                    csca.RequestedDate = DateTime.Now;
                    csca.RequestedByUserId = auth.UserId;
                    csca.CompanyId = q.CompanyId;
                    csca.CurrentCompanyStatusId = companiesEntity.CompanyStatusId;
                    csca.RequestedCompanyStatusId = newCompanyStatusId;
                    csca.Comments = comments;
                    csca.QuestionnaireId = q.QuestionnaireId;
                    cscaService.Save(csca);

                    string emailAddress = configuration.GetValue(ConfigList.ContactEmail);
                    string name = "CSMS Team";

                    string PreReSq = "Contractor Safety Pre-Qualification";
                    if (q.IsReQualification) PreReSq = "Contractor Safety Re-Qualification";
                    if (q.SubContractor == true) PreReSq = "Sub " + PreReSq;
                    //string subject = String.Format("Company Status Change Request - {0}", companiesEntity.CompanyName);
                    //Modified by Ashley Goldstraw 10/9/2015 to use email templates
                    model.EmailTemplate eMailTemplate = emailTemplateService.Get(r => r.EmailTemplateName == "Company Status Change Requested", null);
                    byte[] bodyByte = eMailTemplate.EmailBody;
                    string mailbody = System.Text.Encoding.ASCII.GetString(bodyByte);
                    string subject = eMailTemplate.EmailTemplateSubject;
                    subject = subject.Replace("{CompanyName}", companiesEntity.CompanyName);
                    subject = subject.Replace("{PreOrReQualification}", PreReSq);
                    mailbody = mailbody.Replace("{CompanyName}", companiesEntity.CompanyName);
                    mailbody = mailbody.Replace("{CurrentCompanyStatus}", csCurrent.CompanyStatusDesc);
                    mailbody = mailbody.Replace("{RequestedCompanyStatus}", csRequested.CompanyStatusDesc);
                    mailbody = mailbody.Replace("{Comments}", comments);
                    mailbody = mailbody.Replace("{RequestedByLastName}", uRequested.LastName);
                    mailbody = mailbody.Replace("{RequestedByFirstName}", uRequested.FirstName);
                    mailbody = mailbody.Replace("{RequestedByCompanyName}", companiesEntity2.CompanyName);
                    mailbody = mailbody.Replace("{CurrentDateTime}", DateTime.Now.ToString());
                    string qUrl = String.Format("<a href='http://csm.aua.alcoa.com/SafetyPQ_QuestionnaireModify.aspx{0}'>{1} - Safety Qualification Questionnaire</a>", QueryStringModule.Encrypt("q=" + q.QuestionnaireId.ToString()), companiesEntity.CompanyName);
                    mailbody = mailbody.Replace("{QuestionnaireLink}", qUrl);
                    
                    /*string qUrl = "http://csm.aua.alcoa.com/SafetyPQ_QuestionnaireModify.aspx" + QueryStringModule.Encrypt("q=" + q.QuestionnaireId.ToString());
                    string body = "A Companies status change has been requested but <strong>requires approval</strong> as follows:<br /><br />" +
                                    "<strong>Company Name</strong>: " + companiesEntity.CompanyName + "<br />" +
                                    "<strong>Company Status - Current</strong>: " + csCurrent.CompanyStatusDesc + "<br />" +
                                    "<strong>Company Status - Requested</strong>: " + csRequested.CompanyStatusDesc + "<br />" +
                                    "<strong>Comments</strong>: " + comments + "<br />" +
                                    "<br /><br />" +
                                    "<strong>Requested by</strong>: " + uRequested.LastName + ", " + uRequested.FirstName + " (" + companiesEntity2.CompanyName + ") at " + DateTime.Now.ToString() + "<br /><br />" +
                                    "<strong>Quick Links</strong>: <br />1) <a href='" + qUrl + "'>" + companiesEntity.CompanyName + " - Safety Qualification Questionnaire</a><br />" +
                                                  "2) <a href='http://csm.aua.alcoa.com/CompanyChangeApprovalList.aspx'>Manager - Procurement Operations : Approvals List</a><br />";
                    */


                    if (configuration.GetValue(ConfigList.ProcurementRegionalManager).ToString() != null)
                    {
                        //Email Site Lead for approval
                        emailAddress = configuration.GetValue(ConfigList.ProcurementRegionalManager).ToString();
                    }

                    string[] to = { emailAddress };
                    string[] cc = { auth.Email };
                    //modified by Ashley Goldstraw 10/9/2015
                    //Helper.Email.sendEmail(to, cc, null, subject, body, null, true);
                    //Helper.Email.logEmail(null, null, to, cc, null, subject, body, EmailLogTypeList.General);
                    emailLogService.Insert(to, cc, null, subject, mailbody, "General", true, null, null);
                    if (Helper.Questionnaire.ActionLog.Add((int)QuestionnaireActionList.CompanyStatusChangeApprovalPending,
                                                                q.QuestionnaireId,                         //QuestionnaireId
                                                                auth.UserId,                                //UserId
                                                                auth.FirstName,                             //FirstName
                                                                auth.LastName,                              //LastName
                                                                auth.RoleId,                                //RoleId
                                                                auth.CompanyId,                             //CompanyId
                                                                auth.CompanyName,                           //CompanyName
                                                                comments,                                   //Comments
                                                                csCurrent.CompanyStatusDesc,                //OldAssigned
                                                                csRequested.CompanyStatusDesc,              //NewAssigned
                                                                null,                                       //NewAssigned2
                                                                null,                                       //NewAssigned3
                                                                null)                                       //NewAssigned4
                                                                == false)
                    {
                        //for now we do nothing if action log fails.
                    }
                }
            }
            else
            {
                RequiresApproval = false;
            }

            return RequiresApproval;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    protected void AssessCompanyStatusChangeRequest(Questionnaire q, bool approved, string comments)
    {
        try
        {
            int count = 0;
            CompanyStatusChangeApprovalFilters cscaFilters = new CompanyStatusChangeApprovalFilters();
            cscaFilters.Append(CompanyStatusChangeApprovalColumn.CompanyId, q.CompanyId.ToString());
            cscaFilters.AppendIsNull(CompanyStatusChangeApprovalColumn.AssessedByUserId);
            using (TList<CompanyStatusChangeApproval> cscaList =
                            DataRepository.CompanyStatusChangeApprovalProvider.GetPaged(cscaFilters.ToString(), "CompanyStatusChangeApprovalId DESC", 0, 1, out count))
            {
                cscaList[0].AssessedByUserId = auth.UserId;
                cscaList[0].AssessedDate = DateTime.Now;
                cscaList[0].Approved = approved;
                cscaList[0].AssessorComments = comments;
                CompanyStatusChangeApprovalService cscaService = new CompanyStatusChangeApprovalService();
                cscaService.Save(cscaList[0]);

                CompanyStatusService csService = new CompanyStatusService();
                CompaniesService cService = new CompaniesService();
                Companies companiesEntity = cService.GetByCompanyId(q.CompanyId);
                CompanyStatus csCurrent = csService.GetByCompanyStatusId((int)companiesEntity.CompanyStatusId);
                CompanyStatus csRequested = csService.GetByCompanyStatusId(cscaList[0].RequestedCompanyStatusId);
                int action = (int)QuestionnaireActionList.CompanyStatusChangeNotApproved;
                model.EmailTemplate eMailTemplate = new model.EmailTemplate(); ;
                if (approved)
                {
                     eMailTemplate = emailTemplateService.Get(tp => tp.EmailTemplateName == "Company Status Change Request - Approved", null);
                }
                else
                {
                     eMailTemplate = emailTemplateService.Get(tp => tp.EmailTemplateName == "Company Status Change Request - Not Approved", null);
                }

                //string qUrl = "http://csm.aua.alcoa.com/SafetyPQ_QuestionnaireModify.aspx" + QueryStringModule.Encrypt("q=" + q.QuestionnaireId.ToString());
                //string qUrl = String.Format("<a href='http://csm.aua.alcoa.com/SafetyPQ_QuestionnaireModify.aspx{0}'>{1} - Safety Qualification Questionnaire</a>", QueryStringModule.Encrypt("q=" + q.QuestionnaireId.ToString()), companiesEntity.CompanyName);
                //string subject = String.Format("Company Status Change Request - {0} - Not Approved", companiesEntity.CompanyName);
                /*string body = "The below Company Status Change Request has been [<strong>NOT APPROVED</strong>] and as such this company needs to be re-qualified.<br /><br />" +
                                "<strong>Company Name</strong>: " + companiesEntity.CompanyName + "<br />" +
                                "<strong>Original Company Status</strong>: " + csCurrent.CompanyStatusDesc + "<br />" +
                                "<strong>Requested Company Status</strong>: " + csRequested.CompanyStatusDesc + "<br />" +
                                "<strong>Requester Comments</strong>: " + cscaList[0].Comments + "<br />" +
                                "<strong>Assessor Comments</strong>: " + cscaList[0].AssessorComments + "<br />" +
                                "<br /><br />" +
                                "<strong>Assessed by</strong>: " + auth.LastName + ", " + auth.FirstName + " (" + auth.CompanyName + ") at " + DateTime.Now.ToString() + "<br /><br />" +
                                "<strong>Quick Links</strong>: <br />1) <a href='" + qUrl + "'>" + companiesEntity.CompanyName + " - Safety Qualification Questionnaire</a><br />";
                */
                byte[] bodyByte = eMailTemplate.EmailBody;
                string mailbody = System.Text.Encoding.ASCII.GetString(bodyByte);
                string subject = eMailTemplate.EmailTemplateSubject;
                subject = subject.Replace("{CompanyName}", companiesEntity.CompanyName);
                mailbody = mailbody.Replace("{CompanyName}", companiesEntity.CompanyName);
                mailbody = mailbody.Replace("{OriginalCompanyStatus}", csCurrent.CompanyStatusDesc);
                mailbody = mailbody.Replace("{RequestedCompanyStatus}", csRequested.CompanyStatusDesc);
                mailbody = mailbody.Replace("{Comments}", cscaList[0].Comments);
                mailbody = mailbody.Replace("{AssessorComments}", cscaList[0].AssessorComments);
                mailbody = mailbody.Replace("{AssessedByLastName}", auth.LastName);
                mailbody = mailbody.Replace("{AssessedByFirstName}", auth.FirstName);
                mailbody = mailbody.Replace("{AssessedByCompanyName}", auth.CompanyName);
                mailbody = mailbody.Replace("{CurrentDateTime}", DateTime.Now.ToString());
                string qUrl = String.Format("<a href='http://csm.aua.alcoa.com/SafetyPQ_QuestionnaireModify.aspx{0}'>{1} - Safety Qualification Questionnaire</a>", QueryStringModule.Encrypt("q=" + q.QuestionnaireId.ToString()), companiesEntity.CompanyName);
                mailbody = mailbody.Replace("{QuestionnaireLink}", qUrl);


                if (approved)
                {
                    companiesEntity.CompanyStatusId = cscaList[0].RequestedCompanyStatusId;
                    cService.Save(companiesEntity);
                    action = (int)QuestionnaireActionList.CompanyStatusChangeApproved;

                    /*subject = subject.Replace("Not Approved", "Approved");
                    body = body.Replace("The below Company Status Change Request has been [<strong>NOT APPROVED</strong>] and as such this company needs to be re-qualified.",
                                        "The below Company Status Change Request has been [<strong>APPROVED</strong>] as follows:");*/
                }

                //email
                UsersService uService = new UsersService();
                Users uRequested = uService.GetByUserId(cscaList[0].RequestedByUserId);
                string[] to = { uRequested.Email };
                string[] cc = { auth.Email };
                //modified by Ashley Goldstraw 10/9/15
                //Helper.Email.sendEmail(to, cc, null, subject, body, null, true);
                emailLogService.Insert(to, cc, null, subject, mailbody, "General", true, null, null);
                if (Helper.Questionnaire.ActionLog.Add(action,
                                                                q.QuestionnaireId,                          //QuestionnaireId
                                                                auth.UserId,                                //UserId
                                                                auth.FirstName,                             //FirstName
                                                                auth.LastName,                              //LastName
                                                                auth.RoleId,                                //RoleId
                                                                auth.CompanyId,                             //CompanyId
                                                                auth.CompanyName,                           //CompanyName
                                                                comments,                                   //Comments
                                                                csCurrent.CompanyStatusDesc,                //OldAssigned
                                                                csRequested.CompanyStatusDesc,              //NewAssigned
                                                                null,                                       //NewAssigned2
                                                                null,                                       //NewAssigned3
                                                                null)                                       //NewAssigned4
                                                                == false)
                {
                    //for now we do nothing if action log fails.
                }

                //Helper.General.Redirect(Page, "Changed Company Status Successfully!", "SafetyPQ_QuestionnaireModify.aspx" + QueryStringModule.Encrypt("q=" + q));
            }
        }
        catch (Exception ex)
        {
            lblCscpChangeCompanyStatusError.Text = "Error Occured: " + ex.Message;
        }
    }

    protected void LoadExistingCompanyStatusChangeRequestIfExists(Questionnaire q)
    {
        panelBox2.Visible = false;

        btnCscpApprove.Enabled = false; //Should be false
        //btnCscpApprove.Enabled = true;
        btnCscpNotApprove.Enabled = false; // Should be false
        //btnCscpNotApprove.Enabled = true;
        mbCscpCompanyStatus.Enabled = false;

        int count = 0;
        CompanyStatusChangeApprovalFilters cscaFilters = new CompanyStatusChangeApprovalFilters();
        cscaFilters.Append(CompanyStatusChangeApprovalColumn.CompanyId, q.CompanyId.ToString());
        cscaFilters.AppendIsNull(CompanyStatusChangeApprovalColumn.AssessedByUserId);
        using (TList<CompanyStatusChangeApproval> cscaList =
                        DataRepository.CompanyStatusChangeApprovalProvider.GetPaged(cscaFilters.ToString(), "CompanyStatusChangeApprovalId DESC", 0, 1, out count))
        {
            if (count > 0)
            { //Load Information
                UsersService uService = new UsersService();
                Users uRequestedBy = uService.GetByUserId(cscaList[0].RequestedByUserId);
                lblCscpRequestedBy.Text = String.Format("{0} {1} at {2}", uRequestedBy.FirstName, uRequestedBy.LastName, cscaList[0].RequestedDate);

                CompaniesService cService = new CompaniesService();
                CompanyStatusService csService = new CompanyStatusService();
                Companies c = cService.GetByCompanyId(q.CompanyId);
                CompanyStatus csCurrent = csService.GetByCompanyStatusId((int)c.CompanyStatusId);
                CompanyStatus csRequested = csService.GetByCompanyStatusId((int)cscaList[0].RequestedCompanyStatusId);

                lblCscpCompanyStatusCurrent.Text = csCurrent.CompanyStatusDesc;
                lblCscpCompanyStatusNew.Text = csRequested.CompanyStatusDesc;
                lblCscpComments.Text = cscaList[0].Comments;

                lblCscpCompanyStatusCurrent_Top.Text = lblCscpCompanyStatusCurrent.Text;
                lblCscpCompanyStatusNew_Top.Text = lblCscpCompanyStatusNew.Text;
                lblCscpComments_Top.Text = lblCscpComments.Text;
                lblCscpRequestedBy_Top.Text = lblCscpRequestedBy.Text;
                panelBox2.Visible = true;
               
                if (auth.RoleId == (int)RoleList.Administrator || Helper.General.isRegionalManager(auth.UserId))
                {
                    btnCscpApprove.Enabled = true;
                    btnCscpNotApprove.Enabled = true;
                    mbCscpCompanyStatus.Enabled = true;
                }


                panelChangeCompanyStatus.Visible = false;
                panelChangeCompanyStatusPending.Visible = true;
            }
            else
            {
                panelChangeCompanyStatus.Visible = true;
                panelChangeCompanyStatusPending.Visible = false;//Should be false
                //panelChangeCompanyStatusPending.Visible = true;
            }
        }
    }

    protected void btnCscpApprove_Click(object sender, EventArgs e)
    {
        string q = Request.QueryString["q"];
        QuestionnaireService _qs = new QuestionnaireService();
        Questionnaire _q = _qs.GetByQuestionnaireId(Convert.ToInt32(q));
        AssessCompanyStatusChangeRequest(_q, true, mbCscpCompanyStatus.Text);
        Helper.General.Redirect(Page, "Approved", "SafetyPQ_QuestionnaireModify.aspx" + QueryStringModule.Encrypt("q=" + q));
    }
    protected void btnCscpNotApprove_Click(object sender, EventArgs e)
    {
        string q = Request.QueryString["q"];
        QuestionnaireService _qs = new QuestionnaireService();
        Questionnaire _q = _qs.GetByQuestionnaireId(Convert.ToInt32(q));
        AssessCompanyStatusChangeRequest(_q, false, mbCscpCompanyStatus.Text);
        Helper.General.Redirect(Page, "Not Approved", "SafetyPQ_QuestionnaireModify.aspx" + QueryStringModule.Encrypt("q=" + q));
    }

    protected void btnChangeCompanyStatus_Click(object sender, EventArgs e)
    {
        string q = Request.QueryString["q"];

        bool reactivating = false;
        string p = " & Sent back to Procurement Stage.";

        QuestionnaireService _qs = new QuestionnaireService();
        Questionnaire _q = new Questionnaire();
        _q = _qs.GetByQuestionnaireId(Convert.ToInt32(q));
        _q.ProcurementModified = true;

        int optionChosen = Convert.ToInt32(cbChangeCompanyStatus.Value.ToString());

        if (_q.Status != (int)QuestionnaireStatusList.AssessmentComplete)
        {
            if (_q.IsReQualification)
            {
                if (optionChosen != 7 && optionChosen != 5 && optionChosen != 6)
                {
                    throw new Exception("Company Status NOT changed. You can only choose:\n 'Re-Qualification Incomplete (in Progress)',\n 'InActive - No Re-Qualification Required',\n 'InActive - SQ Incomplete, No Access to Site (in Status Report)'\nif this is a re-qualification and the questionnaire is still incomplete/being assessed (SQ Not Complete).");
                }
            }
            else
            {
                if (optionChosen != 2 && optionChosen != 5 && optionChosen != 6)
                {
                    throw new Exception("Company Status NOT changed. You can only choose:\n 'Being Assessed', 'InActive - No Re-Qualification Required',\n 'InActive - SQ Incomplete, No Access to Site (in Status Report)'\nif the questionnaire is still incomplete/being assessed (SQ Not Complete).");
                }
            }
        }


        if (_q.IsReQualification)
        {
            if (optionChosen == (int)CompanyStatusList.Being_Assessed)
            {
                throw new Exception("Company Status NOT changed. You cannot choose 'Being Assessed' as this is a Re-Qualification. Instead, please choose 'Re-Qualification Incomplete (in Progress)'");
            }
        }
        else
        {
            if (optionChosen == (int)CompanyStatusList.Requal_Incomplete)
            {
                throw new Exception("Company Status NOT changed. You cannot choose 'Re-Qualification Incomplete (in Progress)' as this is a Pre-Qualification. Instead, please choose 'Being Assessed'");
            }
        }

        if (_q.InitialStatus == (int)QuestionnaireStatusList.AssessmentComplete)
        {
            if (_q.IsMainRequired)
            {
                if (optionChosen == (int)CompanyStatusList.Active_No_PQ_Required || optionChosen == (int)CompanyStatusList.Active_No_PQ_Required_Restricted_Access_to_Site)
                {
                    throw new Exception("Company Status NOT changed. You cannot choose 'Active - No PQ Required' as Pre-Qualification is required according to the procurement questionnaire");
                }
            }
            else
            {
                //Changed by Pankaj Dikshit for <Issue# 2441, 09-July-2012> - Start
                bool SelectedAvailableOption = false;
                if (optionChosen == (int)CompanyStatusList.Active_No_PQ_Required || optionChosen == (int)CompanyStatusList.Active_No_PQ_Required_Restricted_Access_to_Site ||
                    optionChosen == (int)CompanyStatusList.Being_Assessed || optionChosen == (int)CompanyStatusList.Requal_Incomplete ||
                    optionChosen == (int)CompanyStatusList.InActive_No_Requal_Required || optionChosen == (int)CompanyStatusList.InActive ||
                    optionChosen == (int)CompanyStatusList.SubContractor_Not_Recommended)
                {
                    SelectedAvailableOption = true;
                }
                if (!SelectedAvailableOption)
                {
                    throw new Exception("Company Status NOT changed. You can only choose 'Active - No PQ Required' as Pre-Qualification is not required according to the procurement questionnaire");
                }
                //Changed by Pankaj Dikshit for <Issue# 2441, 09-July-2012> - End
            }
        }

        CompaniesService cService = new CompaniesService();
        Companies c = cService.GetByCompanyId(_q.CompanyId);

        if (_q.Status != (int)QuestionnaireStatusList.AssessmentComplete)
        {
            if (c.CompanyStatusId == (int)CompanyStatusList.InActive_No_Requal_Required || c.CompanyStatusId == (int)CompanyStatusList.InActive_SQ_Incomplete)
            {
                if (optionChosen == (int)CompanyStatusList.Being_Assessed || optionChosen == (int)CompanyStatusList.Requal_Incomplete)
                {
                    reactivating = true;
                }
            }
        }

        QuestionnaireService qService = new QuestionnaireService();
        DataSet ds = qService.GetLatestAssessmentComplete_ByCompanyId(_q.CompanyId);

        if (ds.Tables[0] != null)
        {
            if (ds.Tables[0].Rows.Count == 1)
            {
                if ((int)ds.Tables[0].Rows[0].ItemArray[0] == _q.QuestionnaireId)
                {
                    if (_q.MainAssessmentValidTo != null)
                    {
                        if (_q.MainAssessmentValidTo < DateTime.Now)
                        {
                            if (optionChosen != (int)CompanyStatusList.InActive_No_Requal_Required)
                            {
                                if (c.CompanyStatusId == (int)CompanyStatusList.InActive_No_Requal_Required)
                                {
                                    if (optionChosen == (int)CompanyStatusList.Being_Assessed || optionChosen == (int)CompanyStatusList.Requal_Incomplete)
                                    {
                                    }
                                    else
                                    {
                                        throw new Exception("Company Status NOT changed. You may only choose 'Being Assessed - SQ Incomplete (in Progress)' as this company's questionnaire has expired.");
                                    }
                                }
                                else
                                {
                                    if (optionChosen == (int)CompanyStatusList.Being_Assessed || optionChosen == (int)CompanyStatusList.Requal_Incomplete)
                                    {
                                    }
                                    else
                                    {
                                        throw new Exception("Company Status NOT changed. You may only choose 'InActive - No Re-Qualification Required' as this company's questionnaire has expired.");
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    Questionnaire q2 = qService.GetByQuestionnaireId((int)ds.Tables[0].Rows[0].ItemArray[0]);
                    if (q2 != null)
                    {
                        if (q2.MainAssessmentValidTo != null)
                        {
                            if (q2.MainAssessmentValidTo < DateTime.Now)
                            {
                                if (optionChosen != (int)CompanyStatusList.InActive_No_Requal_Required)
                                {
                                    if (c.CompanyStatusId == (int)CompanyStatusList.InActive_No_Requal_Required)
                                    {
                                        if (optionChosen == (int)CompanyStatusList.Being_Assessed || optionChosen == (int)CompanyStatusList.Requal_Incomplete)
                                        {
                                        }
                                        else
                                        {
                                            throw new Exception("Company Status NOT changed. You may only choose 'Being Assessed - SQ Incomplete (in Progress)' as this company's questionnaire has expired.");
                                        }
                                    }
                                    else
                                    {
                                        if (optionChosen == (int)CompanyStatusList.Being_Assessed || optionChosen == (int)CompanyStatusList.Requal_Incomplete)
                                        {
                                        }
                                        else
                                        {
                                            throw new Exception("Company Status NOT changed. You may only choose 'InActive - No Re-Qualification Required' as this company's questionnaire has expired.");
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        CompanyStatusService csService = new CompanyStatusService();

        string msg = "Company status Changed.";

        if (!CompanyStatusChangeRequiresApproval(_q, optionChosen, mbCompanyStatus.Text))
        {
            //Changing by Pankaj Dikshit for <DT 2488, 30-July-2012> - Start
            CompanyStatus cs = csService.GetByCompanyStatusId((int)optionChosen);
            if (cs.CompanyStatusId == (int)CompanyStatusList.Acceptable || cs.CompanyStatusId == (int)CompanyStatusList.Active)
            //Changing by Pankaj Dikshit for <DT 2488, 30-July-2012> - End
            {
                UsersService usersService = new UsersService();
                Users userEntity;
                userEntity = usersService.GetByUserId(auth.UserId);

                CompaniesService companiesService = new CompaniesService();
                Companies companiesEntity = companiesService.GetByCompanyId(_q.CompanyId);
                Companies companiesEntity2 = companiesService.GetByCompanyId(auth.CompanyId);


                string PreReSq = "Contractor Safety Pre-Qualification";
                if (_q.IsReQualification) PreReSq = "Contractor Safety Re-Qualification";
                if (_q.SubContractor == true) PreReSq = "Sub " + PreReSq;
                //string subject = String.Format("{0}: 4 - {1}: Company Status Changed", PreReSq, companiesEntity.CompanyName);
                /*string body = "A Companies status has been changed as follows:\n\n" +
                                "Company Name: " + companiesEntity.CompanyName + "\n" +
                                "Company Status: " + cs.CompanyStatusDesc + "\n" +
                                "\n\n" +
                                "Changed by: " + userEntity.LastName + ", " + userEntity.FirstName + " (" + companiesEntity2.CompanyName + ") at " + DateTime.Now.ToString() + "\n";
                 */
                //Modified by Ashley Goldstraw 10/9/2015 to use email template.
                model.EmailTemplate eMailTemplate = emailTemplateService.Get(et => et.EmailTemplateName == "Company Status Changed", null);
                byte[] bodyByte = eMailTemplate.EmailBody;
                string mailbody = System.Text.Encoding.ASCII.GetString(bodyByte);
                string subject = eMailTemplate.EmailTemplateSubject;
                subject = subject.Replace("{CompanyName}", companiesEntity.CompanyName);
                subject = subject.Replace("{PreOrReQualification}", PreReSq);
                mailbody = mailbody.Replace("{CompanyName}", companiesEntity.CompanyName);
                mailbody = mailbody.Replace("{CompanyStatus}", cs.CompanyStatusDesc);
                mailbody = mailbody.Replace("{ChangedByLastName}", userEntity.LastName);
                mailbody = mailbody.Replace("{ChangedByFirstName}", userEntity.FirstName);
                mailbody = mailbody.Replace("{ChangedByCompanyName}", companiesEntity2.CompanyName);
                mailbody = mailbody.Replace("{CurrentDateTime}", DateTime.Now.ToString());

                Configuration configuration = new Configuration();
                //string[] to = { configuration.GetValue(ConfigList.ContactEmail) };
                //to - AUACSMS Team To Removed 9.Mar.11
                string[] to = { userEntity.Email };

                if (cs.CompanyStatusId == (int)CompanyStatusList.Acceptable || cs.CompanyStatusId == (int)CompanyStatusList.Active)
                {
                    List<string> listCC = new List<string>();
                    ConfigSa812Service c812Service = new ConfigSa812Service();
                    EhsConsultantService eService = new EhsConsultantService();
                    CompanySiteCategoryStandardService cscsService = new CompanySiteCategoryStandardService();


                    TList<ConfigSa812> c812List = c812Service.GetAll();

                    foreach (ConfigSa812 c812 in c812List)
                    {
                        if (c812 != null)
                        {
                            if (!String.IsNullOrEmpty(c812.EhsConsultantId.ToString()))
                            {
                                EhsConsultant ehs = eService.GetByEhsConsultantId((int)c812.EhsConsultantId);
                                if (ehs != null)
                                {
                                    Users u = usersService.GetByUserId(ehs.UserId);
                                    if (u != null)
                                    {
                                        if (!listCC.Contains(u.Email)) listCC.Add(u.Email);
                                    }
                                }
                            }
                        }
                    }

                    mailbody += "<br/>H&S ASSESSORS PLEASE AUTHORISE FOR USE ON YOUR LOCATION AS APPLICABLE.";

                    //listCC.Add(userEntity.Email);
                    //listCC.Add(configuration.GetValue(ConfigList.ContactEmail).ToString());
                    //cc - AUACSMS Team cc Removed 9.Mar.11
                    string[] cc = listCC.ToArray();

                    //Helper.Email.sendEmail(cc, to, null, subject, body, null);
                    //Helper.Email.logEmail(null, null, cc, to, null, subject, body, EmailLogTypeList.Questionnaire);
                    emailLogService.Insert(cc, to, null, subject, mailbody, "Questionnaire", true, null, null);
                    //Change Users from 'Prequal' to 'Contractor' if users of 'prequal' exist.
                    if (cs.CompanyStatusId == (int)CompanyStatusList.Active)
                    {
                        UsersService uService = new UsersService();
                        TList<Users> uCompany = uService.GetByCompanyId(c.CompanyId);
                        int iCount = 0;
                        while (iCount + 1 <= uCompany.Count)
                        {
                            if (uCompany[iCount].RoleId == (int)RoleList.PreQual)
                            {
                                Users u = uService.GetByUserId(uCompany[iCount].UserId);
                                u.RoleId = (int)RoleList.Contractor;
                                uService.Save(u);
                            }
                            iCount++;
                        }
                    }
                }
                else
                {
                    //Helper.Email.sendEmail(to, null, null, subject, body, null);
                    //Helper.Email.logEmail(null, null, to, null, null, subject, body, EmailLogTypeList.Questionnaire);
                    emailLogService.Insert(to, null, null, subject, mailbody, "Questionnaire", true, null, null);
                }
            }

            if (cbChangeCompanyStatus.Value == null) { throw new Exception("Error Occured."); };

            CompanyStatus csOld = csService.GetByCompanyStatusId(c.CompanyStatusId);
            string CompanyStatusOld = "-";
            if (csOld != null) CompanyStatusOld = csOld.CompanyStatusDesc;

            c.CompanyStatusId = Convert.ToInt32(cbChangeCompanyStatus.Value.ToString());
            c.ModifiedbyUserId = auth.UserId;
            cService.Save(c);


            string Comments = mbCompanyStatus.Text;
            CompanyStatus csNew = csService.GetByCompanyStatusId((int)optionChosen);
            string CompanyStatusNew = "-";
            if (csNew != null) CompanyStatusNew = csNew.CompanyStatusDesc;
            if (Helper.Questionnaire.ActionLog.Add((int)QuestionnaireActionList.CompanyStatusChanged,
                                                        Convert.ToInt32(q),                         //QuestionnaireId
                                                        auth.UserId,                                //UserId
                                                        auth.FirstName,                             //FirstName
                                                        auth.LastName,                              //LastName
                                                        auth.RoleId,                                //RoleId
                                                        auth.CompanyId,                             //CompanyId
                                                        auth.CompanyName,                           //CompanyName
                                                        Comments,                                   //Comments
                                                        CompanyStatusOld,                           //OldAssigned
                                                        CompanyStatusNew,                           //NewAssigned
                                                        null,                                       //NewAssigned2
                                                        null,                                       //NewAssigned3
                                                        null)                                       //NewAssigned4
                                                        == false)
            {
                //for now we do nothing if action log fails.
            }


            if (reactivating)
            {
                ResetToProcurementStage(_q.QuestionnaireId, true);
                msg = "Company Status Changed and reset to Procurement Stage due to re-activation of company.";
            }
        }
        else
        {
            msg = "Company Status Change - Request for Approval sent!";
            //msg = "The system has stipulated that all requests to change a company status to 'Inactive' be approved by the Manager - Procurement Operations (or CSMS Team), they have been notified of this request to change company status and will approve/not approve your request.";
        }


        bool inactive = false;

        if (optionChosen == (int)CompanyStatusList.InActive) inactive = true;
        if (optionChosen == (int)CompanyStatusList.InActive_No_Requal_Required) inactive = true;
        if (optionChosen == (int)CompanyStatusList.InActive_SQ_Incomplete) inactive = true;

        if (inactive)
        {
            _q.QuestionnairePresentlyWithActionId = (int)QuestionnairePresentlyWithActionList.NoActionRequired;
            _q.QuestionnairePresentlyWithSince = DateTime.Now;
        }
        else
        {
            if (_q.QuestionnairePresentlyWithActionId == (int)QuestionnairePresentlyWithActionList.NoActionRequired ||
                    c.CompanyStatusId == (int)CompanyStatusList.Being_Assessed ||
                c.CompanyStatusId == (int)CompanyStatusList.Requal_Incomplete)
            {
                switch (_q.Status)
                {
                    case (int)QuestionnaireStatusList.Incomplete:
                        switch (_q.InitialStatus)
                        {
                            case (int)QuestionnaireStatusList.Incomplete:
                                _q.QuestionnairePresentlyWithActionId = (int)QuestionnairePresentlyWithActionList.ProcurementQuestionnaire;
                                break;
                            case (int)QuestionnaireStatusList.BeingAssessed:
                                _q.QuestionnairePresentlyWithActionId = (int)QuestionnairePresentlyWithActionList.HSAssessorQuestionnaireProcurement;
                                break;
                            case (int)QuestionnaireStatusList.AssessmentComplete:
                                if (_q.MainStatus == (int)QuestionnaireStatusList.BeingAssessed ||
                                    _q.VerificationStatus == (int)QuestionnaireStatusList.BeingAssessed)
                                {
                                    _q.QuestionnairePresentlyWithActionId = (int)QuestionnairePresentlyWithActionList.HSAssessorQuestionnaire;
                                }
                                if (_q.MainStatus == (int)QuestionnaireStatusList.Incomplete || _q.VerificationStatus == (int)QuestionnaireStatusList.Incomplete)
                                {
                                    //TODO:check if any assessment done. if so set to supplierincomplete, else supplier
                                    _q.QuestionnairePresentlyWithActionId = (int)QuestionnairePresentlyWithActionList.SupplierQuestionnaireSupplier;
                                }
                                break;
                        }
                        break;
                    case (int)QuestionnaireStatusList.BeingAssessed:
                        _q.QuestionnairePresentlyWithActionId = (int)QuestionnairePresentlyWithActionList.HSAssessorQuestionnaire;
                        break;
                    case (int)QuestionnaireStatusList.AssessmentComplete:

                        if (optionChosen == (int)CompanyStatusList.Being_Assessed || optionChosen == (int)CompanyStatusList.Requal_Incomplete)
                        {
                            _q.QuestionnairePresentlyWithActionId = (int)QuestionnairePresentlyWithActionList.ProcurementAssignCompanyStatus;
                        }

                        //TODO:Assuming this is the latest questionnaire, follow expiry rules...

                        break;
                    default:
                        break;
                }
            }
        }


        

        if (c.CompanyStatusId == (int)CompanyStatusList.Active || c.CompanyStatusId == (int)CompanyStatusList.Acceptable ||
            c.CompanyStatusId == (int)CompanyStatusList.Active_No_PQ_Required ||
            //Condition added by Pankaj Dikshit for <Issue# 2441, 09-July-2012>
            c.CompanyStatusId == (int)CompanyStatusList.Active_No_PQ_Required_Restricted_Access_to_Site ||
            c.CompanyStatusId == (int)CompanyStatusList.SubContractor || c.CompanyStatusId == (int)CompanyStatusList.SubContractor_Not_Recommended
            )
        {
            if (_q.QuestionnairePresentlyWithActionId == (int)QuestionnairePresentlyWithActionList.ProcurementAssignCompanyStatus)
            {
                _q.QuestionnairePresentlyWithActionId = (int)QuestionnairePresentlyWithActionList.NoActionRequired;
                _q.QuestionnairePresentlyWithSince = DateTime.Now;
            }
        }

        if (reactivating)
        {
            _q.InitialStatus = (int)QuestionnaireStatusList.Incomplete;
            _q.QuestionnairePresentlyWithActionId = (int)QuestionnairePresentlyWithActionList.ProcurementQuestionnaire;
        }

        _qs.Save(_q);

        Helper.General.Redirect(Page, msg, "SafetyPQ_QuestionnaireModify.aspx" + QueryStringModule.Encrypt("q=" + q));
    }

    protected void btnSendBackToProcurementStage_Click(object sender, EventArgs e)
    {
        try
        {
            string q = Request.QueryString["q"];
            if (q != "New")
            {
                ResetToProcurementStage(Convert.ToInt32(Request.QueryString["q"]), false);

                Helper.General.Redirect(Page, "Questionnaire reset to Procurement Stage", String.Format("SafetyPQ_QuestionnaireModify.aspx{0}", QueryStringModule.Encrypt("q=" + q)));
            }
        }
        catch (Exception ex)
        {
            Elmah.ErrorSignal.FromContext(Context).Raise(ex);
            Response.Write(String.Format("Error: {0}", ex.Message));
        }
    }

    protected void ResetToProcurementStage(int q, bool reactivating)
    {
        string msg = "";
        if (reactivating) msg = "Questionnaire reset to Procurement Stage due to re-activation of company.";
        QuestionnaireService qService = new QuestionnaireService();
        qService.SendBackToProcurementStage(q);

        Helper.Questionnaire.ActionLog.Add((int)QuestionnaireActionList.InternalCommentAdded, q,
        auth.UserId,
        auth.FirstName,
        auth.LastName,
        auth.RoleId,
        auth.CompanyId,
        auth.CompanyName,
        msg,
        null, null, null, null, null);
    }

    protected void gvSqExemption_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {
        if (e.RowType != GridViewRowType.Data) return;

        ASPxHyperLink hlEdit = gvSqExemption.FindRowCellTemplateControl(e.VisibleIndex, null, "hlEdit") as ASPxHyperLink;
        if (hlEdit != null)
        {
            hlEdit.NavigateUrl = String.Format("javascript:popUp('../PopUps/EditSqExemption.aspx{0}');",
                                           QueryStringModule.Encrypt(String.Format("q={0}&c={1}",
                                                        (int)gvSqExemption.GetRowValues(e.VisibleIndex, "SqExemptionId"),
                                                        (int)gvSqExemption.GetRowValues(e.VisibleIndex, "CompanyId"))));

            if (auth.RoleId == (int)RoleList.Administrator)
                hlEdit.Text = "Edit";
            else
                hlEdit.Text = "View";
        }
    }

    protected void CheckIfSafetyQualificationExemptionExists(Questionnaire q)
    {
        try
        {
            SqExemptionService seService = new SqExemptionService();
            DataSet seDataSet = seService.GetValidByCompanyId(q.CompanyId);
            if (seDataSet != null)
            {
                if (seDataSet.Tables[0].Rows.Count > 0)
                {

                    if (q.QuestionnairePresentlyWithActionId == (int)QuestionnairePresentlyWithActionList.ProcurementQuestionnaire)
                    {
                        panelBoxSqExemptionFound.Visible = true;
                        panelBoxSqExemptionFoundProcurement.Visible = true;
                        panelBoxSqExemptionFoundNotice.Visible = false;
                    }
                    else
                    {
                        panelBoxSqExemptionFound.Visible = true;
                        panelBoxSqExemptionFoundProcurement.Visible = false;
                        panelBoxSqExemptionFoundNotice.Visible = true;
                    }
                }
            }
        }
        catch (Exception ex)
        {
            Elmah.ErrorSignal.FromContext(this.Context).Raise(ex);
        }
    }
    protected void btnForciblyRequireVerification_Click(object sender, EventArgs e)
    {
        
        try
        {

            //Written by Ashley Goldstraw 18/9/2015.  DevTrack item 223
            string q = Request.QueryString["q"]; //Get the questionnaire Id
            int qid = Int32.Parse(q);  //Turn the Id into an integer
            model.Questionnaire questionnaire = questionnaireService.Get(x => x.QuestionnaireId == qid, null);  //Retreive the questionnaire from teh database

            questionnaire.IsVerificationRequired = true;
            questionnaire.Status = (int)QuestionnaireStatusList.Incomplete;

            if (questionnaire.InitialStatus == (int)QuestionnaireStatusList.Incomplete)
            {
                questionnaire.QuestionnairePresentlyWithActionId = (int)QuestionnairePresentlyWithActionList.ProcurementQuestionnaire;
            }
            else if (questionnaire.InitialStatus == (int)QuestionnaireStatusList.BeingAssessed)
            {
                questionnaire.QuestionnairePresentlyWithActionId = (int)QuestionnairePresentlyWithActionList.HSAssessorQuestionnaireProcurement;
            }
            else
            {
                questionnaire.QuestionnairePresentlyWithActionId = (int)QuestionnairePresentlyWithActionList.SupplierQuestionnaireIncomplete;
            }

            questionnaireService.Update(questionnaire);


            model.Company companiesEntity = companyService.Get(c => c.CompanyId == questionnaire.CompanyId, null); //Retrieve the company from the database using the company id from the Questionnaire
            model.QuestionnaireInitialContact qic = questionnaireInitialContactService.Get(qi => qi.QuestionnaireId == qid, null); //Get the initial Contact for the questionnaire from the database
            model.EmailTemplate eMailTemplate = emailTemplateService.Get(i => i.EmailTemplateName == "Forciby Require Verification Questionnaire and Set Questionnaire Status to [INCOMPLETE]", null); //Retrive the correct email template from the database
            byte[] bodyByte = eMailTemplate.EmailBody; //Get the email body as a byte array
            string mailbody = System.Text.Encoding.ASCII.GetString(bodyByte); //turn the body to a string so that it can be editted
            string subject = eMailTemplate.EmailTemplateSubject; 
            string[] cc = {auth.Email};
            string[] to = { qic.ContactEmail };
            //Replace the variables in the template with the values required
            subject = subject.Replace("{CompanyName}", companiesEntity.CompanyName);
            mailbody = mailbody.Replace("{CompanyName}", companiesEntity.CompanyName);
            mailbody = mailbody.Replace("{Reason}", mbForciblyRequireVerification.Text); //Replace the reason with the value from the text box
            emailLogService.Insert(to,cc, null, subject, mailbody, "Questionnaire", true, null, null); //Insert the email into the email log.

            //Log the action
            if (Helper.Questionnaire.ActionLog.Add((int)QuestionnaireActionList.VerificationQuestionnaireForciblyRequired,
                                                           Convert.ToInt32(q),                         //QuestionnaireId
                                                           auth.UserId,                                //UserId
                                                           auth.FirstName,                             //FirstName
                                                           auth.LastName,                              //LastName
                                                           auth.RoleId,                                //RoleId
                                                           auth.CompanyId,                             //CompanyId
                                                           auth.CompanyName,                           //CompanyName
                                                           mbForciblyRequireVerification.Text,                                   //Comments
                                                           null,                           //OldAssigned
                                                           null,                           //NewAssigned
                                                           null,                                       //NewAssigned2
                                                           null,                                       //NewAssigned3
                                                           null)                                       //NewAssigned4
                                                           == false)
            {
                // do nothing if log fails
            }

            Helper.General.Redirect(Page, String.Format("SafetyPQ_QuestionnaireModify.aspx{0}", QueryStringModule.Encrypt("q=" + q)));
            //Helper.General.Redirect(Page, "Require Verification Questionnaire email sent.", "SafetyPQ_QuestionnaireModify.aspx" + QueryStringModule.Encrypt("q=" + q));
        }
        catch (Exception ex)
        {
            Elmah.ErrorSignal.FromContext(Context).Raise(ex);
            Response.Write(String.Format("Error: {0}", ex.Message));
        }
    }
}