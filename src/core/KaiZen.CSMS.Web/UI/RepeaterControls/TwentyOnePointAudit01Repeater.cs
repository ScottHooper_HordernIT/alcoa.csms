﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.ComponentModel;
using System.Web.UI.Design.WebControls;
using System.Web.UI.Design;
using System.Web.UI.WebControls;

namespace KaiZen.CSMS.Web.UI
{
    /// <summary>
    /// A designer class for a strongly typed repeater <c>TwentyOnePointAudit01Repeater</c>
    /// </summary>
	public class TwentyOnePointAudit01RepeaterDesigner : System.Web.UI.Design.ControlDesigner
	{
	    /// <summary>
        /// Initializes a new instance of the <see cref="T:TwentyOnePointAudit01RepeaterDesigner"/> class.
        /// </summary>
		public TwentyOnePointAudit01RepeaterDesigner()
		{
		}

        /// <summary>
        /// Initializes the control designer and loads the specified component.
        /// </summary>
        /// <param name="component">The control being designed.</param>
		public override void Initialize(IComponent component)
		{
			if (!(component is TwentyOnePointAudit01Repeater))
			{ 
				throw new ArgumentException("Component is not a TwentyOnePointAudit01Repeater."); 
			} 
			base.Initialize(component); 
			base.SetViewFlags(ViewFlags.TemplateEditing, true); 
		}


		/// <summary>
		/// Generate HTML for the designer
		/// </summary>
		/// <returns>a string of design time HTML</returns>
		public override string GetDesignTimeHtml()
		{

			// Get the instance this designer applies to
			//
			TwentyOnePointAudit01Repeater z = (TwentyOnePointAudit01Repeater)Component;
			z.DataBind();

			return base.GetDesignTimeHtml();

			//return z.RenderAtDesignTime();

			//	ControlCollection c = z.Controls;
			//Totem z = (Totem) Component;
			//Totem z = (Totem) Component;
			//return ("<div style='border: 1px gray dotted; background-color: lightgray'><b>TagStat :</b> zae |  qsdds</div>");

		}
	}

    /// <summary>
    /// A strongly typed repeater control for the <see cref="TwentyOnePointAudit01Repeater"/> Type.
    /// </summary>
	[Designer(typeof(TwentyOnePointAudit01RepeaterDesigner))]
	[ParseChildren(true)]
	[ToolboxData("<{0}:TwentyOnePointAudit01Repeater runat=\"server\"></{0}:TwentyOnePointAudit01Repeater>")]
	public class TwentyOnePointAudit01Repeater : CompositeDataBoundControl, System.Web.UI.INamingContainer
	{
	    /// <summary>
        /// Initializes a new instance of the <see cref="T:TwentyOnePointAudit01Repeater"/> class.
        /// </summary>
		public TwentyOnePointAudit01Repeater()
		{
		}

		/// <summary>
        /// Gets a <see cref="T:System.Web.UI.ControlCollection"></see> object that represents the child controls for a specified server control in the UI hierarchy.
        /// </summary>
        /// <value></value>
        /// <returns>The collection of child controls for the specified server control.</returns>
		public override ControlCollection Controls
		{
			get
			{
				this.EnsureChildControls();
				return base.Controls;
			}
		}

		private ITemplate m_headerTemplate;
		/// <summary>
        /// Gets or sets the header template.
        /// </summary>
        /// <value>The header template.</value>
		[Browsable(false)]
		[TemplateContainer(typeof(TwentyOnePointAudit01Item))]
		[PersistenceMode(PersistenceMode.InnerDefaultProperty)]
		public ITemplate HeaderTemplate
		{
			get { return m_headerTemplate; }
			set { m_headerTemplate = value; }
		}

		private ITemplate m_itemTemplate;
		/// <summary>
        /// Gets or sets the item template.
        /// </summary>
        /// <value>The item template.</value>
		[Browsable(false)]
		[TemplateContainer(typeof(TwentyOnePointAudit01Item))]
		[PersistenceMode(PersistenceMode.InnerDefaultProperty)]
		public ITemplate ItemTemplate
		{
			get { return m_itemTemplate; }
			set { m_itemTemplate = value; }
		}

		private ITemplate m_seperatorTemplate;
        /// <summary>
        /// Gets or sets the Seperator Template
        /// </summary>
        [Browsable(false)]
        [TemplateContainer(typeof(TwentyOnePointAudit01Item))]
        [PersistenceMode(PersistenceMode.InnerDefaultProperty)]
        public ITemplate SeperatorTemplate
        {
            get { return m_seperatorTemplate; }
            set { m_seperatorTemplate = value; }
        }
			
		private ITemplate m_altenateItemTemplate;
        /// <summary>
        /// Gets or sets the alternating item template.
        /// </summary>
        /// <value>The alternating item template.</value>
		[Browsable(false)]
		[TemplateContainer(typeof(TwentyOnePointAudit01Item))]
		[PersistenceMode(PersistenceMode.InnerDefaultProperty)]
		public ITemplate AlternatingItemTemplate
		{
			get { return m_altenateItemTemplate; }
			set { m_altenateItemTemplate = value; }
		}

		private ITemplate m_footerTemplate;
        /// <summary>
        /// Gets or sets the footer template.
        /// </summary>
        /// <value>The footer template.</value>
		[Browsable(false)]
		[TemplateContainer(typeof(TwentyOnePointAudit01Item))]
		[PersistenceMode(PersistenceMode.InnerDefaultProperty)]
		public ITemplate FooterTemplate
		{
			get { return m_footerTemplate; }
			set { m_footerTemplate = value; }
		}

//      /// <summary>
//      /// Called by the ASP.NET page framework to notify server controls that use composition-based implementation to create any child controls they contain in preparation for posting back or rendering.
//      /// </summary>
//		protected override void CreateChildControls()
//      {
//         if (ChildControlsCreated)
//         {
//            return;
//         }

//         Controls.Clear();

//         //Instantiate the Header template (if exists)
//         if (m_headerTemplate != null)
//         {
//            Control headerItem = new Control();
//            m_headerTemplate.InstantiateIn(headerItem);
//            Controls.Add(headerItem);
//         }

//         //Instantiate the Footer template (if exists)
//         if (m_footerTemplate != null)
//         {
//            Control footerItem = new Control();
//            m_footerTemplate.InstantiateIn(footerItem);
//            Controls.Add(footerItem);
//         }
//
//         ChildControlsCreated = true;
//      }
	
		/// <summary>
        /// Overridden and Empty so that span tags are not written
        /// </summary>
        /// <param name="writer"></param>
        public override void RenderBeginTag(HtmlTextWriter writer)
        {
            
        }

        /// <summary>
        /// Overridden and Empty so that span tags are not written
        /// </summary>
        /// <param name="writer"></param>
        public override void RenderEndTag(HtmlTextWriter writer)
        {
                
        }		
		
		/// <summary>
      	/// Called by the ASP.NET page framework to notify server controls that use composition-based implementation to create any child controls they contain in preparation for posting back or rendering.
      	/// </summary>
		protected override int CreateChildControls(System.Collections.IEnumerable dataSource, bool dataBinding)
      	{
         int pos = 0;

         if (dataBinding)
         {
            //Instantiate the Header template (if exists)
            if (m_headerTemplate != null)
            {
                Control headerItem = new Control();
                m_headerTemplate.InstantiateIn(headerItem);
                Controls.Add(headerItem);
            }
			if (dataSource != null)
			{
				foreach (object o in dataSource)
				{
						KaiZen.CSMS.Entities.TwentyOnePointAudit01 entity = o as KaiZen.CSMS.Entities.TwentyOnePointAudit01;
						TwentyOnePointAudit01Item container = new TwentyOnePointAudit01Item(entity);
	
						if (m_itemTemplate != null && (pos % 2) == 0)
						{
							m_itemTemplate.InstantiateIn(container);
							
							if (m_seperatorTemplate != null)
							{
								m_seperatorTemplate.InstantiateIn(container);
							}
						}
						else
						{
							if (m_altenateItemTemplate != null)
							{
								m_altenateItemTemplate.InstantiateIn(container);
								
								if (m_seperatorTemplate != null)
								{
									m_seperatorTemplate.InstantiateIn(container);
								}
								
							}
							else if (m_itemTemplate != null)
							{
								m_itemTemplate.InstantiateIn(container);
								
								if (m_seperatorTemplate != null)
								{
									m_seperatorTemplate.InstantiateIn(container);
								}
							}
							else
							{
								// no template !!!
							}
						}
						Controls.Add(container);
						
						container.DataBind();
						
						pos++;
				}
			}
            //Instantiate the Footer template (if exists)
            if (m_footerTemplate != null)
            {
                Control footerItem = new Control();
                m_footerTemplate.InstantiateIn(footerItem);
                Controls.Add(footerItem);
            }

		}
			
			return pos;
		}

        /// <summary>
        /// Raises the <see cref="E:System.Web.UI.Control.PreRender"></see> event.
        /// </summary>
        /// <param name="e">An <see cref="T:System.EventArgs"></see> object that contains the event data.</param>
		protected override void OnPreRender(EventArgs e)
		{
			base.DataBind();
		}

		#region Design time
        /// <summary>
        /// Renders at design time.
        /// </summary>
        /// <returns>a  string of the Designed HTML</returns>
		internal string RenderAtDesignTime()
		{			
			return "Designer currently not implemented"; 
		}

		#endregion
	}

    /// <summary>
    /// A wrapper type for the entity
    /// </summary>
	[System.ComponentModel.ToolboxItem(false)]
	public class TwentyOnePointAudit01Item : System.Web.UI.Control, System.Web.UI.INamingContainer
	{
		private KaiZen.CSMS.Entities.TwentyOnePointAudit01 _entity;

        /// <summary>
        /// Initializes a new instance of the <see cref="T:TwentyOnePointAudit01Item"/> class.
        /// </summary>
		public TwentyOnePointAudit01Item()
			: base()
		{ }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:TwentyOnePointAudit01Item"/> class.
        /// </summary>
		public TwentyOnePointAudit01Item(KaiZen.CSMS.Entities.TwentyOnePointAudit01 entity)
			: base()
		{
			_entity = entity;
		}
		
        /// <summary>
        /// Gets the Id
        /// </summary>
        /// <value>The Id.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32 Id
		{
			get { return _entity.Id; }
		}
        /// <summary>
        /// Gets the Achieved1a
        /// </summary>
        /// <value>The Achieved1a.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved1a
		{
			get { return _entity.Achieved1a; }
		}
        /// <summary>
        /// Gets the Achieved1b
        /// </summary>
        /// <value>The Achieved1b.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved1b
		{
			get { return _entity.Achieved1b; }
		}
        /// <summary>
        /// Gets the Achieved1c
        /// </summary>
        /// <value>The Achieved1c.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved1c
		{
			get { return _entity.Achieved1c; }
		}
        /// <summary>
        /// Gets the Achieved1c1
        /// </summary>
        /// <value>The Achieved1c1.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved1c1
		{
			get { return _entity.Achieved1c1; }
		}
        /// <summary>
        /// Gets the Achieved1c2
        /// </summary>
        /// <value>The Achieved1c2.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved1c2
		{
			get { return _entity.Achieved1c2; }
		}
        /// <summary>
        /// Gets the Achieved1c3
        /// </summary>
        /// <value>The Achieved1c3.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved1c3
		{
			get { return _entity.Achieved1c3; }
		}
        /// <summary>
        /// Gets the Achieved1c4
        /// </summary>
        /// <value>The Achieved1c4.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved1c4
		{
			get { return _entity.Achieved1c4; }
		}
        /// <summary>
        /// Gets the Achieved1c5
        /// </summary>
        /// <value>The Achieved1c5.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved1c5
		{
			get { return _entity.Achieved1c5; }
		}
        /// <summary>
        /// Gets the Achieved1c6
        /// </summary>
        /// <value>The Achieved1c6.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved1c6
		{
			get { return _entity.Achieved1c6; }
		}
        /// <summary>
        /// Gets the Achieved1c7
        /// </summary>
        /// <value>The Achieved1c7.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved1c7
		{
			get { return _entity.Achieved1c7; }
		}
        /// <summary>
        /// Gets the Achieved1c8
        /// </summary>
        /// <value>The Achieved1c8.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved1c8
		{
			get { return _entity.Achieved1c8; }
		}
        /// <summary>
        /// Gets the Achieved1c9
        /// </summary>
        /// <value>The Achieved1c9.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved1c9
		{
			get { return _entity.Achieved1c9; }
		}
        /// <summary>
        /// Gets the Achieved1c10
        /// </summary>
        /// <value>The Achieved1c10.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved1c10
		{
			get { return _entity.Achieved1c10; }
		}
        /// <summary>
        /// Gets the Achieved1c11
        /// </summary>
        /// <value>The Achieved1c11.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved1c11
		{
			get { return _entity.Achieved1c11; }
		}
        /// <summary>
        /// Gets the Achieved1d
        /// </summary>
        /// <value>The Achieved1d.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved1d
		{
			get { return _entity.Achieved1d; }
		}
        /// <summary>
        /// Gets the Achieved1e
        /// </summary>
        /// <value>The Achieved1e.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved1e
		{
			get { return _entity.Achieved1e; }
		}
        /// <summary>
        /// Gets the Achieved1f
        /// </summary>
        /// <value>The Achieved1f.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved1f
		{
			get { return _entity.Achieved1f; }
		}
        /// <summary>
        /// Gets the Achieved1g
        /// </summary>
        /// <value>The Achieved1g.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved1g
		{
			get { return _entity.Achieved1g; }
		}
        /// <summary>
        /// Gets the Achieved1h
        /// </summary>
        /// <value>The Achieved1h.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved1h
		{
			get { return _entity.Achieved1h; }
		}
        /// <summary>
        /// Gets the Achieved1i
        /// </summary>
        /// <value>The Achieved1i.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved1i
		{
			get { return _entity.Achieved1i; }
		}
        /// <summary>
        /// Gets the Achieved1j
        /// </summary>
        /// <value>The Achieved1j.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved1j
		{
			get { return _entity.Achieved1j; }
		}
        /// <summary>
        /// Gets the Achieved1k
        /// </summary>
        /// <value>The Achieved1k.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved1k
		{
			get { return _entity.Achieved1k; }
		}
        /// <summary>
        /// Gets the Achieved1l
        /// </summary>
        /// <value>The Achieved1l.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved1l
		{
			get { return _entity.Achieved1l; }
		}
        /// <summary>
        /// Gets the Achieved1m
        /// </summary>
        /// <value>The Achieved1m.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved1m
		{
			get { return _entity.Achieved1m; }
		}
        /// <summary>
        /// Gets the Achieved1n
        /// </summary>
        /// <value>The Achieved1n.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved1n
		{
			get { return _entity.Achieved1n; }
		}
        /// <summary>
        /// Gets the Achieved1o
        /// </summary>
        /// <value>The Achieved1o.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved1o
		{
			get { return _entity.Achieved1o; }
		}
        /// <summary>
        /// Gets the Achieved1p
        /// </summary>
        /// <value>The Achieved1p.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved1p
		{
			get { return _entity.Achieved1p; }
		}
        /// <summary>
        /// Gets the Achieved1q
        /// </summary>
        /// <value>The Achieved1q.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved1q
		{
			get { return _entity.Achieved1q; }
		}
        /// <summary>
        /// Gets the Achieved1r
        /// </summary>
        /// <value>The Achieved1r.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved1r
		{
			get { return _entity.Achieved1r; }
		}
        /// <summary>
        /// Gets the Achieved1s
        /// </summary>
        /// <value>The Achieved1s.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved1s
		{
			get { return _entity.Achieved1s; }
		}
        /// <summary>
        /// Gets the Achieved1t
        /// </summary>
        /// <value>The Achieved1t.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved1t
		{
			get { return _entity.Achieved1t; }
		}
        /// <summary>
        /// Gets the Achieved1u
        /// </summary>
        /// <value>The Achieved1u.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved1u
		{
			get { return _entity.Achieved1u; }
		}
        /// <summary>
        /// Gets the Achieved1v
        /// </summary>
        /// <value>The Achieved1v.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved1v
		{
			get { return _entity.Achieved1v; }
		}
        /// <summary>
        /// Gets the Achieved1w
        /// </summary>
        /// <value>The Achieved1w.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved1w
		{
			get { return _entity.Achieved1w; }
		}
        /// <summary>
        /// Gets the Achieved1x
        /// </summary>
        /// <value>The Achieved1x.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved1x
		{
			get { return _entity.Achieved1x; }
		}
        /// <summary>
        /// Gets the Achieved1y
        /// </summary>
        /// <value>The Achieved1y.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved1y
		{
			get { return _entity.Achieved1y; }
		}
        /// <summary>
        /// Gets the Achieved1z
        /// </summary>
        /// <value>The Achieved1z.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved1z
		{
			get { return _entity.Achieved1z; }
		}
        /// <summary>
        /// Gets the Observation1a
        /// </summary>
        /// <value>The Observation1a.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation1a
		{
			get { return _entity.Observation1a; }
		}
        /// <summary>
        /// Gets the Observation1b
        /// </summary>
        /// <value>The Observation1b.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation1b
		{
			get { return _entity.Observation1b; }
		}
        /// <summary>
        /// Gets the Observation1c
        /// </summary>
        /// <value>The Observation1c.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation1c
		{
			get { return _entity.Observation1c; }
		}
        /// <summary>
        /// Gets the Observation1c1
        /// </summary>
        /// <value>The Observation1c1.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation1c1
		{
			get { return _entity.Observation1c1; }
		}
        /// <summary>
        /// Gets the Observation1c2
        /// </summary>
        /// <value>The Observation1c2.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation1c2
		{
			get { return _entity.Observation1c2; }
		}
        /// <summary>
        /// Gets the Observation1c3
        /// </summary>
        /// <value>The Observation1c3.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation1c3
		{
			get { return _entity.Observation1c3; }
		}
        /// <summary>
        /// Gets the Observation1c4
        /// </summary>
        /// <value>The Observation1c4.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation1c4
		{
			get { return _entity.Observation1c4; }
		}
        /// <summary>
        /// Gets the Observation1c5
        /// </summary>
        /// <value>The Observation1c5.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation1c5
		{
			get { return _entity.Observation1c5; }
		}
        /// <summary>
        /// Gets the Observation1c6
        /// </summary>
        /// <value>The Observation1c6.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation1c6
		{
			get { return _entity.Observation1c6; }
		}
        /// <summary>
        /// Gets the Observation1c7
        /// </summary>
        /// <value>The Observation1c7.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation1c7
		{
			get { return _entity.Observation1c7; }
		}
        /// <summary>
        /// Gets the Observation1c8
        /// </summary>
        /// <value>The Observation1c8.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation1c8
		{
			get { return _entity.Observation1c8; }
		}
        /// <summary>
        /// Gets the Observation1c9
        /// </summary>
        /// <value>The Observation1c9.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation1c9
		{
			get { return _entity.Observation1c9; }
		}
        /// <summary>
        /// Gets the Observation1c10
        /// </summary>
        /// <value>The Observation1c10.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation1c10
		{
			get { return _entity.Observation1c10; }
		}
        /// <summary>
        /// Gets the Observation1c11
        /// </summary>
        /// <value>The Observation1c11.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation1c11
		{
			get { return _entity.Observation1c11; }
		}
        /// <summary>
        /// Gets the Observation1d
        /// </summary>
        /// <value>The Observation1d.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation1d
		{
			get { return _entity.Observation1d; }
		}
        /// <summary>
        /// Gets the Observation1e
        /// </summary>
        /// <value>The Observation1e.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation1e
		{
			get { return _entity.Observation1e; }
		}
        /// <summary>
        /// Gets the Observation1f
        /// </summary>
        /// <value>The Observation1f.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation1f
		{
			get { return _entity.Observation1f; }
		}
        /// <summary>
        /// Gets the Observation1g
        /// </summary>
        /// <value>The Observation1g.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation1g
		{
			get { return _entity.Observation1g; }
		}
        /// <summary>
        /// Gets the Observation1h
        /// </summary>
        /// <value>The Observation1h.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation1h
		{
			get { return _entity.Observation1h; }
		}
        /// <summary>
        /// Gets the Observation1i
        /// </summary>
        /// <value>The Observation1i.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation1i
		{
			get { return _entity.Observation1i; }
		}
        /// <summary>
        /// Gets the Observation1j
        /// </summary>
        /// <value>The Observation1j.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation1j
		{
			get { return _entity.Observation1j; }
		}
        /// <summary>
        /// Gets the Observation1k
        /// </summary>
        /// <value>The Observation1k.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation1k
		{
			get { return _entity.Observation1k; }
		}
        /// <summary>
        /// Gets the Observation1l
        /// </summary>
        /// <value>The Observation1l.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation1l
		{
			get { return _entity.Observation1l; }
		}
        /// <summary>
        /// Gets the Observation1m
        /// </summary>
        /// <value>The Observation1m.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation1m
		{
			get { return _entity.Observation1m; }
		}
        /// <summary>
        /// Gets the Observation1n
        /// </summary>
        /// <value>The Observation1n.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation1n
		{
			get { return _entity.Observation1n; }
		}
        /// <summary>
        /// Gets the Observation1o
        /// </summary>
        /// <value>The Observation1o.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation1o
		{
			get { return _entity.Observation1o; }
		}
        /// <summary>
        /// Gets the Observation1p
        /// </summary>
        /// <value>The Observation1p.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation1p
		{
			get { return _entity.Observation1p; }
		}
        /// <summary>
        /// Gets the Observation1q
        /// </summary>
        /// <value>The Observation1q.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation1q
		{
			get { return _entity.Observation1q; }
		}
        /// <summary>
        /// Gets the Observation1r
        /// </summary>
        /// <value>The Observation1r.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation1r
		{
			get { return _entity.Observation1r; }
		}
        /// <summary>
        /// Gets the Observation1s
        /// </summary>
        /// <value>The Observation1s.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation1s
		{
			get { return _entity.Observation1s; }
		}
        /// <summary>
        /// Gets the Observation1t
        /// </summary>
        /// <value>The Observation1t.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation1t
		{
			get { return _entity.Observation1t; }
		}
        /// <summary>
        /// Gets the Observation1u
        /// </summary>
        /// <value>The Observation1u.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation1u
		{
			get { return _entity.Observation1u; }
		}
        /// <summary>
        /// Gets the Observation1v
        /// </summary>
        /// <value>The Observation1v.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation1v
		{
			get { return _entity.Observation1v; }
		}
        /// <summary>
        /// Gets the Observation1w
        /// </summary>
        /// <value>The Observation1w.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation1w
		{
			get { return _entity.Observation1w; }
		}
        /// <summary>
        /// Gets the Observation1x
        /// </summary>
        /// <value>The Observation1x.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation1x
		{
			get { return _entity.Observation1x; }
		}
        /// <summary>
        /// Gets the Observation1y
        /// </summary>
        /// <value>The Observation1y.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation1y
		{
			get { return _entity.Observation1y; }
		}
        /// <summary>
        /// Gets the Observation1z
        /// </summary>
        /// <value>The Observation1z.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation1z
		{
			get { return _entity.Observation1z; }
		}
        /// <summary>
        /// Gets the TotalScore
        /// </summary>
        /// <value>The TotalScore.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? TotalScore
		{
			get { return _entity.TotalScore; }
		}

        /// <summary>
        /// Gets a <see cref="T:KaiZen.CSMS.Entities.TwentyOnePointAudit01"></see> object
        /// </summary>
        /// <value></value>
        [System.ComponentModel.Bindable(true)]
        public KaiZen.CSMS.Entities.TwentyOnePointAudit01 Entity
        {
            get { return _entity; }
        }
	}
}
