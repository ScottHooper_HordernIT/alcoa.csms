using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Services;
using System.Text;

public partial class UserControls_SafetyPlans_SelfAssessment_Print : System.Web.UI.UserControl
{
    Auth auth = new Auth();

    protected void Page_Load(object sender, EventArgs e) {  Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); }

    private void moduleInit(bool postBack)
    {
        if (!postBack)
        {
            FileDbService fdbs = new FileDbService();
            int r = Convert.ToInt32(Request.QueryString["r"]);
            DataSet ds_ = fdbs.GetCompanyIdByQuestionnaireResponseId(r);
            string CompanyName = "";
            foreach (DataRow dr in ds_.Tables[0].Rows)
            {
                CompanyName = dr[0].ToString();
            }


            Response.Write(String.Format("<table border='0' cellpadding='0' cellspacing='0' style='width: 700px'><tr><td style='width: 700px; height: 13px; text-align: center'><span style='font-size: 14pt'><strong>Safety Plan</strong></span></td></tr><tr><td align='center'><strong>Questionnaire Response</strong></td></tr><tr><td style='width: 700px; height: 13px; text-align: center'><table style='width: 700px' cellspacing='0' cellpadding='0'><tr><td style='width: 566px'>Company Name: {0}</td><td style='width: 18px'><input ID='btnClose' type='submit' value='Close' OnClick='window.close();' /></td><td>&nbsp; &nbsp;</td><td><input ID='btnPrint' type='submit' value='Print' OnClick='window.print();' /></td></tr></table></td></tr></table><br />", CompanyName));
            SafetyPlansSeAnswersService seas = new SafetyPlansSeAnswersService();
            
            DataSet ds = seas.Printable_GetByResponseId(r);

            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                Response.Write("<table border='0' cellpadding='1' cellspacing='1' style='width:700px'>\n");
                string QuestionId = dr[0].ToString();
                string Question = dr[1].ToString();
                string Answer = dr[2].ToString();
                string  Comment = Encoding.ASCII.GetString((byte[])dr[3]);
                if (Answer == "True") Answer = "Yes";
                if (Answer == "False") Answer = "No";
                Response.Write("<tr>\n");
                Response.Write(String.Format("<td style='width: 50px; text-align: right'>\n<span style='font-family: Tahoma; font-size:12pt'><strong>{0} &nbsp;</strong></span></td>\n", QuestionId));
                Response.Write(String.Format("<td style='width: 600px;'><span style='font-family: Tahoma; font-size:11pt'><strong>{0}</strong></span></td>\n", Question));
                Response.Write("<td style='width: 50px; text-align: right'>");
                if (Answer == "Yes")
                {
                    Response.Write("<span style='COLOR: Green; font-family: Tahoma; font-size:12pt'>");
                }
                else
                {
                    Response.Write("<span style='COLOR: Red; font-family: Tahoma; font-size:12pt'>");
                }
                Response.Write(Answer + "</td>\n");
                Response.Write("</tr>\n");
                Response.Write("<tr><td style='width: 50px'>&nbsp;</td>");
                Response.Write(String.Format("<td>{0}</td><td style='width: 50px'>&nbsp;</td></tr></table>", Comment));
                Response.Write("<br />\n");
            }
        }
    }
}
