using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using KaiZen.CSMS.Services;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.Library;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxTabControl;
using DevExpress.Web.Data;
using DevExpress.Web.ASPxGridView;
using model = Repo.CSMS.DAL.EntityModels;
using repo = Repo.CSMS.Service.Database;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;

public partial class Adminv2_UserControls_Companies_Admin : System.Web.UI.UserControl
{
    Auth auth = new Auth();
    repo.IEmailTemplateService emailTemplateService = ALCOA.CSMS.Website.Global.GetInstance<repo.IEmailTemplateService>();
    repo.IEmailLogService emailLogService = ALCOA.CSMS.Website.Global.GetInstance<repo.IEmailLogService>();
    repo.ISiteService siteService = ALCOA.CSMS.Website.Global.GetInstance<repo.ISiteService>();
    repo.IEHSConsultantService ehsConsultantService = ALCOA.CSMS.Website.Global.GetInstance<repo.IEHSConsultantService>();
    //repo.ICompanyService companyService = ALCOA.CSMS.Website.Global.GetInstance<repo.ICompanyService>();
    repo.IQuestionnaireService questionnaireService = ALCOA.CSMS.Website.Global.GetInstance<repo.IQuestionnaireService>();
    repo.IQuestionnaireInitialResponseService questionnaireInitialResponseService = ALCOA.CSMS.Website.Global.GetInstance<repo.IQuestionnaireInitialResponseService>();
    repo.IUserService userService = ALCOA.CSMS.Website.Global.GetInstance<repo.IUserService>();
    protected void Page_Load(object sender, EventArgs e) {  Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); }

    private void moduleInit(bool postBack)
    {
        if (!postBack)
        {
            if (auth.RoleId != (int)RoleList.Administrator) Response.Redirect("default.aspx");

            // Export
            String exportFileName = @"ALCOA CSMS - Admin - Companies"; //Chrome & IE is fine.
            String userAgent = Request.Headers.Get("User-Agent");
            if (
                (userAgent.Contains("Mozilla") && !userAgent.Contains("AppleWebKit") && !userAgent.Contains("MSIE")) || //Mozilla
                (userAgent.Contains("AppleWebKit") && !userAgent.Contains("Chrome"))      //Safari
                )
            {
                exportFileName = exportFileName.Replace(" ", "_");
            }
            Helper.ExportGrid.Settings(ucExportButtons, exportFileName,"grid");
        }
    }

    protected void grid_onEdit(object sender, ASPxGridViewEditorEventArgs e)
    {
        //if (e.Column.FieldName == "EhsConsultantId")
        //{
        //    ASPxComboBox comboBox = e.Editor as ASPxComboBox;
        //    comboBox.ClientSideEvents.Init = "function(s, e) {s.InsertItem('Null', '(No One)', '');}";
        //}
    }
    protected void grid_RowValidating(object sender, DevExpress.Web.Data.ASPxDataValidationEventArgs e)
    {
        if (e.NewValues["CompanyAbn"] == null)
        {
            //e.Errors[ASPxGridView1.Columns["CompanyAbn"]] = "Value can't be null.";
        }
        else
        {
            if (e.NewValues["CompanyAbn"].ToString() != "00000000000")
            {
                if (!ValidationUtilities.CheckValidity.Abn(e.NewValues["CompanyAbn"].ToString()))
                {
                    e.Errors[grid.Columns["CompanyAbn"]] = "Invalid ABN";
                }
            }
        }
    }

    

    protected void grid_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        e.NewValues["ModifiedByUserId"] = auth.UserId;
    }
    protected void grid_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        e.NewValues["ModifiedByUserId"] = auth.UserId;
    }
    protected void grid_InitNewRow(object sender, ASPxDataInitNewRowEventArgs e) //Default Values 
    {
        if (e.NewValues["ModifiedByUserId"] == null) { e.NewValues["ModifiedByUserId"] = auth.UserId; }
    }

    protected void grid_RowUpdated(object sender, ASPxDataUpdatedEventArgs e)
    {
         //Start of DT265.
        if (Convert.ToBoolean(e.NewValues["Deactivated"]) == true && Convert.ToBoolean(e.OldValues["Deactivated"]) == false)
        {
            model.EmailTemplate emailTemplate = emailTemplateService.Get(i => i.EmailTemplateName == "Company Deactivated", null);
            int companyId = Convert.ToInt32(e.Keys[0]);
           
            String subject = emailTemplate.EmailTemplateSubject;
            string companyName = e.NewValues["CompanyName"].ToString();
            subject = subject.Replace("{CompanyName}", companyName);

            string mailbody;
            byte[] bodyByte = emailTemplate.EmailBody;
            mailbody = System.Text.Encoding.ASCII.GetString(bodyByte);
            mailbody = mailbody.Replace("{CompanyName}", companyName);

            List<string> addressList = new List<string>();
           
            List<model.Site> sites = siteService.GetMany(null,null,null,null);
            foreach (model.Site site in sites)
            {
                if (site.EhsConsultantId != null)
                {
                    model.User user = userService.Get(u => u.UserId == site.EHSConsultant.UserId, null);
                    if (user != null)
                        addressList.Add(user.Email);
                }
            }

            //get rid of any duplicate emails in the list.
            List<string> finalAddressList = addressList.Distinct().ToList();

            //Get the most recent questionnaire to get the procurement contact
            List<model.Questionnaire> questionnaireList = questionnaireService.GetMany(null, c => c.CompanyId == companyId, null, null);
            questionnaireList.OrderByDescending(s => s.MainAssessmentValidTo);
            model.Questionnaire q = questionnaireList[0];
            int questionnaireId = q.QuestionnaireId;
            //find out the procurement contact for this company from the questionnaire and put them in a list.
            model.QuestionnaireInitialResponse qir = questionnaireInitialResponseService.Get(qr => qr.QuestionnaireId == questionnaireId && qr.QuestionId == "2", null);
            model.User procUser = userService.Get(u => u.UserId.ToString() == qir.AnswerText, null);
            List<String> procAddressList = new List<String>();
            procAddressList.Add(procUser.Email);
            //turn the lists to array.
            String[] addressArray = finalAddressList.ToArray();
            String[] procAddressArray = procAddressList.ToArray(); 
            //Send the email.
            emailLogService.Insert(addressArray, procAddressArray, null, subject, mailbody, "General", true, null, null);
        }
    }
}
