﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="ConfigText2ProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class ConfigText2ProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.ConfigText2, KaiZen.CSMS.Entities.ConfigText2Key>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.ConfigText2Key key)
		{
			return Delete(transactionManager, key.ConfigText2Id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_configText2Id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _configText2Id)
		{
			return Delete(null, _configText2Id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_configText2Id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _configText2Id);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ConfigText2_ConfigTextType key.
		///		FK_ConfigText2_ConfigTextType Description: 
		/// </summary>
		/// <param name="_configTextTypeId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.ConfigText2 objects.</returns>
		public TList<ConfigText2> GetByConfigTextTypeId(System.Int32 _configTextTypeId)
		{
			int count = -1;
			return GetByConfigTextTypeId(_configTextTypeId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ConfigText2_ConfigTextType key.
		///		FK_ConfigText2_ConfigTextType Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_configTextTypeId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.ConfigText2 objects.</returns>
		/// <remarks></remarks>
		public TList<ConfigText2> GetByConfigTextTypeId(TransactionManager transactionManager, System.Int32 _configTextTypeId)
		{
			int count = -1;
			return GetByConfigTextTypeId(transactionManager, _configTextTypeId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_ConfigText2_ConfigTextType key.
		///		FK_ConfigText2_ConfigTextType Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_configTextTypeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.ConfigText2 objects.</returns>
		public TList<ConfigText2> GetByConfigTextTypeId(TransactionManager transactionManager, System.Int32 _configTextTypeId, int start, int pageLength)
		{
			int count = -1;
			return GetByConfigTextTypeId(transactionManager, _configTextTypeId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ConfigText2_ConfigTextType key.
		///		fkConfigText2ConfigTextType Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_configTextTypeId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.ConfigText2 objects.</returns>
		public TList<ConfigText2> GetByConfigTextTypeId(System.Int32 _configTextTypeId, int start, int pageLength)
		{
			int count =  -1;
			return GetByConfigTextTypeId(null, _configTextTypeId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ConfigText2_ConfigTextType key.
		///		fkConfigText2ConfigTextType Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_configTextTypeId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.ConfigText2 objects.</returns>
		public TList<ConfigText2> GetByConfigTextTypeId(System.Int32 _configTextTypeId, int start, int pageLength,out int count)
		{
			return GetByConfigTextTypeId(null, _configTextTypeId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ConfigText2_ConfigTextType key.
		///		FK_ConfigText2_ConfigTextType Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_configTextTypeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.ConfigText2 objects.</returns>
		public abstract TList<ConfigText2> GetByConfigTextTypeId(TransactionManager transactionManager, System.Int32 _configTextTypeId, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.ConfigText2 Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.ConfigText2Key key, int start, int pageLength)
		{
			return GetByConfigText2Id(transactionManager, key.ConfigText2Id, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_ConfigText2 index.
		/// </summary>
		/// <param name="_configText2Id"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.ConfigText2"/> class.</returns>
		public KaiZen.CSMS.Entities.ConfigText2 GetByConfigText2Id(System.Int32 _configText2Id)
		{
			int count = -1;
			return GetByConfigText2Id(null,_configText2Id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ConfigText2 index.
		/// </summary>
		/// <param name="_configText2Id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.ConfigText2"/> class.</returns>
		public KaiZen.CSMS.Entities.ConfigText2 GetByConfigText2Id(System.Int32 _configText2Id, int start, int pageLength)
		{
			int count = -1;
			return GetByConfigText2Id(null, _configText2Id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ConfigText2 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_configText2Id"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.ConfigText2"/> class.</returns>
		public KaiZen.CSMS.Entities.ConfigText2 GetByConfigText2Id(TransactionManager transactionManager, System.Int32 _configText2Id)
		{
			int count = -1;
			return GetByConfigText2Id(transactionManager, _configText2Id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ConfigText2 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_configText2Id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.ConfigText2"/> class.</returns>
		public KaiZen.CSMS.Entities.ConfigText2 GetByConfigText2Id(TransactionManager transactionManager, System.Int32 _configText2Id, int start, int pageLength)
		{
			int count = -1;
			return GetByConfigText2Id(transactionManager, _configText2Id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ConfigText2 index.
		/// </summary>
		/// <param name="_configText2Id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.ConfigText2"/> class.</returns>
		public KaiZen.CSMS.Entities.ConfigText2 GetByConfigText2Id(System.Int32 _configText2Id, int start, int pageLength, out int count)
		{
			return GetByConfigText2Id(null, _configText2Id, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ConfigText2 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_configText2Id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.ConfigText2"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.ConfigText2 GetByConfigText2Id(TransactionManager transactionManager, System.Int32 _configText2Id, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;ConfigText2&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;ConfigText2&gt;"/></returns>
		public static TList<ConfigText2> Fill(IDataReader reader, TList<ConfigText2> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.ConfigText2 c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("ConfigText2")
					.Append("|").Append((System.Int32)reader[((int)ConfigText2Column.ConfigText2Id - 1)]).ToString();
					c = EntityManager.LocateOrCreate<ConfigText2>(
					key.ToString(), // EntityTrackingKey
					"ConfigText2",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.ConfigText2();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.ConfigText2Id = (System.Int32)reader[((int)ConfigText2Column.ConfigText2Id - 1)];
					c.ConfigTextTypeId = (System.Int32)reader[((int)ConfigText2Column.ConfigTextTypeId - 1)];
					c.ConfigText = (reader.IsDBNull(((int)ConfigText2Column.ConfigText - 1)))?null:(System.Byte[])reader[((int)ConfigText2Column.ConfigText - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.ConfigText2"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.ConfigText2"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.ConfigText2 entity)
		{
			if (!reader.Read()) return;
			
			entity.ConfigText2Id = (System.Int32)reader[((int)ConfigText2Column.ConfigText2Id - 1)];
			entity.ConfigTextTypeId = (System.Int32)reader[((int)ConfigText2Column.ConfigTextTypeId - 1)];
			entity.ConfigText = (reader.IsDBNull(((int)ConfigText2Column.ConfigText - 1)))?null:(System.Byte[])reader[((int)ConfigText2Column.ConfigText - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.ConfigText2"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.ConfigText2"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.ConfigText2 entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.ConfigText2Id = (System.Int32)dataRow["ConfigText2Id"];
			entity.ConfigTextTypeId = (System.Int32)dataRow["ConfigTextTypeId"];
			entity.ConfigText = Convert.IsDBNull(dataRow["ConfigText"]) ? null : (System.Byte[])dataRow["ConfigText"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.ConfigText2"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.ConfigText2 Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.ConfigText2 entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region ConfigTextTypeIdSource	
			if (CanDeepLoad(entity, "ConfigTextType|ConfigTextTypeIdSource", deepLoadType, innerList) 
				&& entity.ConfigTextTypeIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.ConfigTextTypeId;
				ConfigTextType tmpEntity = EntityManager.LocateEntity<ConfigTextType>(EntityLocator.ConstructKeyFromPkItems(typeof(ConfigTextType), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.ConfigTextTypeIdSource = tmpEntity;
				else
					entity.ConfigTextTypeIdSource = DataRepository.ConfigTextTypeProvider.GetByConfigTextTypeId(transactionManager, entity.ConfigTextTypeId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ConfigTextTypeIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.ConfigTextTypeIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.ConfigTextTypeProvider.DeepLoad(transactionManager, entity.ConfigTextTypeIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion ConfigTextTypeIdSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.ConfigText2 object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.ConfigText2 instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.ConfigText2 Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.ConfigText2 entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region ConfigTextTypeIdSource
			if (CanDeepSave(entity, "ConfigTextType|ConfigTextTypeIdSource", deepSaveType, innerList) 
				&& entity.ConfigTextTypeIdSource != null)
			{
				DataRepository.ConfigTextTypeProvider.Save(transactionManager, entity.ConfigTextTypeIdSource);
				entity.ConfigTextTypeId = entity.ConfigTextTypeIdSource.ConfigTextTypeId;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region ConfigText2ChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.ConfigText2</c>
	///</summary>
	public enum ConfigText2ChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>ConfigTextType</c> at ConfigTextTypeIdSource
		///</summary>
		[ChildEntityType(typeof(ConfigTextType))]
		ConfigTextType,
		}
	
	#endregion ConfigText2ChildEntityTypes
	
	#region ConfigText2FilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;ConfigText2Column&gt;"/> class
	/// that is used exclusively with a <see cref="ConfigText2"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ConfigText2FilterBuilder : SqlFilterBuilder<ConfigText2Column>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ConfigText2FilterBuilder class.
		/// </summary>
		public ConfigText2FilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ConfigText2FilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ConfigText2FilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ConfigText2FilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ConfigText2FilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ConfigText2FilterBuilder
	
	#region ConfigText2ParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;ConfigText2Column&gt;"/> class
	/// that is used exclusively with a <see cref="ConfigText2"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ConfigText2ParameterBuilder : ParameterizedSqlFilterBuilder<ConfigText2Column>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ConfigText2ParameterBuilder class.
		/// </summary>
		public ConfigText2ParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ConfigText2ParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ConfigText2ParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ConfigText2ParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ConfigText2ParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ConfigText2ParameterBuilder
	
	#region ConfigText2SortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;ConfigText2Column&gt;"/> class
	/// that is used exclusively with a <see cref="ConfigText2"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class ConfigText2SortBuilder : SqlSortBuilder<ConfigText2Column>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ConfigText2SqlSortBuilder class.
		/// </summary>
		public ConfigText2SortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion ConfigText2SortBuilder
	
} // end namespace
