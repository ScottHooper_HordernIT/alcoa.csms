﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface IDashboardCompaniesNonCompliantService
    {
        Dashboard_Companies_NonCompliant Get(Expression<Func<Dashboard_Companies_NonCompliant, bool>> where, List<Expression<Func<Dashboard_Companies_NonCompliant, object>>> includes);
        List<Dashboard_Companies_NonCompliant> GetMany(int? top, Expression<Func<Dashboard_Companies_NonCompliant, bool>> where, Func<IQueryable<Dashboard_Companies_NonCompliant>, IOrderedQueryable<Dashboard_Companies_NonCompliant>> orderBy, List<Expression<Func<Dashboard_Companies_NonCompliant, object>>> includes);
        List<Dashboard_Companies_NonCompliant> GetManyPaged(int pageNumber, int pageSize, Expression<Func<Dashboard_Companies_NonCompliant, bool>> where, Func<IQueryable<Dashboard_Companies_NonCompliant>, IOrderedQueryable<Dashboard_Companies_NonCompliant>> orderBy, List<Expression<Func<Dashboard_Companies_NonCompliant, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<Dashboard_Companies_NonCompliant, bool>> where, List<Expression<Func<Dashboard_Companies_NonCompliant, object>>> includes);
        void Insert(Dashboard_Companies_NonCompliant item, bool saveChanges = true);
        void Update(Dashboard_Companies_NonCompliant item, bool saveChanges = true);
        void Delete(Dashboard_Companies_NonCompliant item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class DashboardCompaniesNonCompliantService : IDashboardCompaniesNonCompliantService
    {
        private readonly IRepository<Dashboard_Companies_NonCompliant> _repository;

        public DashboardCompaniesNonCompliantService(IRepository<Dashboard_Companies_NonCompliant> repository)
        {
            this._repository = repository;
        }

        public Dashboard_Companies_NonCompliant Get(Expression<Func<Dashboard_Companies_NonCompliant, bool>> where, List<Expression<Func<Dashboard_Companies_NonCompliant, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<Dashboard_Companies_NonCompliant> GetMany(int? top, Expression<Func<Dashboard_Companies_NonCompliant, bool>> where, Func<IQueryable<Dashboard_Companies_NonCompliant>, IOrderedQueryable<Dashboard_Companies_NonCompliant>> orderBy, List<Expression<Func<Dashboard_Companies_NonCompliant, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<Dashboard_Companies_NonCompliant> GetManyPaged(int pageNumber, int pageSize, Expression<Func<Dashboard_Companies_NonCompliant, bool>> where, Func<IQueryable<Dashboard_Companies_NonCompliant>, IOrderedQueryable<Dashboard_Companies_NonCompliant>> orderBy, List<Expression<Func<Dashboard_Companies_NonCompliant, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<Dashboard_Companies_NonCompliant, bool>> where, List<Expression<Func<Dashboard_Companies_NonCompliant, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(Dashboard_Companies_NonCompliant item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("Dashboard_Companies_NonCompliant");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(Dashboard_Companies_NonCompliant item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("Dashboard_Companies_NonCompliant");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(Dashboard_Companies_NonCompliant item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("Dashboard_Companies_NonCompliant");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
