USE [AuA_CSMS_Dev]
GO

/****** Object:  StoredProcedure [dbo].[Procurement_CurrentlyWithProcurement]    Script Date: 31/07/2015 10:00:56 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Procurement_CurrentlyWithProcurement]                                  
(                                  
 @SiteId int,                                  
 @Month int,                                  
 @Year int                                  
)                                  
AS                        
DECLARE @Companies VARCHAR(MAX)                    
IF (@SiteId > 0)                                 
BEGIN      
  SELECT @Companies = COALESCE(@Companies + ',', '') + X.CompanyName FROM       
  (SELECT DISTINCT F.CompanyName           
  FROM dbo.Questionnaire AS A                   
  LEFT OUTER JOIN QuestionnaireReportExpiry B            
  ON A.QuestionnaireId = B.QuestionnaireId             
  LEFT OUTER JOIN dbo.QuestionnairePresentlyWithAction AS C            
  ON B.QuestionnairePresentlyWithActionId = C.QuestionnairePresentlyWithActionId               
  LEFT OUTER JOIN dbo.CompanySiteCategoryStandard D             
  ON D.CompanyId = A.CompanyId          
  LEFT OUTER JOIN dbo.Companies F                        
  ON F.CompanyId = A.CompanyId             
  WHERE C.ActionName = 'ProcurementExpired'            
  AND D.SiteId = @SiteId  
  AND YEAR(GETDATE()) = @Year AND MONTH(GETDATE()) = @Month ) X                
  --AND YEAR(B.MainAssessmentValidTo) = @Year                
  --AND MONTH(B.MainAssessmentValidTo) = @Month    
END                          
ELSE                          
BEGIN       
  SELECT @Companies = COALESCE(@Companies + ',', '') + X.CompanyName FROM                     
  (SELECT DISTINCT F.CompanyName           
  FROM dbo.Questionnaire AS A                   
  LEFT OUTER JOIN QuestionnaireReportExpiry B            
  ON A.QuestionnaireId = B.QuestionnaireId             
  LEFT OUTER JOIN dbo.QuestionnairePresentlyWithAction AS C            
  ON B.QuestionnairePresentlyWithActionId = C.QuestionnairePresentlyWithActionId               
  LEFT OUTER JOIN dbo.CompanySiteCategoryStandard D             
  ON D.CompanyId = A.CompanyId            
  LEFT OUTER JOIN dbo.Companies F                        
  ON F.CompanyId = A.CompanyId           
  WHERE C.ActionName = 'ProcurementExpired'            
  AND D.SiteId IN (SELECT SiteId FROM RegionsSites WHERE (-1 * dbo.RegionsSites.RegionId) = @SiteId)  
  AND YEAR(GETDATE()) = @Year AND MONTH(GETDATE()) = @Month ) X            
  --AND YEAR(B.MainAssessmentValidTo) = @Year                
  --AND MONTH(B.MainAssessmentValidTo) = @Month   
END       
      
IF EXISTS(SELECT * FROM dbo.Dashboard_Companies_NonCompliant WHERE SiteId = @SiteId AND [Year] = @Year         
 AND [Month] = @Month AND [ItemName] = 'Expired - Currently With Procurement') 
 UPDATE [dbo].[Dashboard_Companies_NonCompliant]      
 SET [NonCompliantCompanies] = @Companies      
 WHERE [SiteId] = @SiteId      
   AND [Month] = @Month      
   AND [Year] = @Year      
   AND [ItemName] = 'Expired - Currently With Procurement'      
ELSE      
 INSERT INTO [dbo].[Dashboard_Companies_NonCompliant]      
           ([SiteId]      
           ,[Month]      
           ,[Year]      
           ,[ItemName]      
           ,[NonCompliantCompanies])      
     VALUES      
           (@SiteId      
           ,@Month      
           ,@Year      
           ,'Expired - Currently With Procurement'      
           ,@Companies)      
----------------------------------------

GO


