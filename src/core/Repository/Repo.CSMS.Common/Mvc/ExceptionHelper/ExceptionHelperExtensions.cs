﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Repo.CSMS.Common.Mvc.ExceptionHelper
{
    public static class ExceptionHelperExtensions
    {
        #region Exception

        public static IEnumerable<Exception> AllExceptions(this Exception ex)
        {
            if (ex == null)
            {
                throw new ArgumentNullException("ex");
            }

            var innerException = ex;
            do
            {
                yield return innerException;
                innerException = innerException.InnerException;
            }
            while (innerException != null);
        }


        #endregion
    }
}
