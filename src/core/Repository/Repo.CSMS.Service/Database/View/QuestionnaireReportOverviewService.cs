﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database.View
{
    #region Interface

    public interface IQuestionnaireReportOverviewService
    {
        QuestionnaireReportOverview Get(Expression<Func<QuestionnaireReportOverview, bool>> where, List<Expression<Func<QuestionnaireReportOverview, object>>> includes);
        List<QuestionnaireReportOverview> GetMany(int? top, Expression<Func<QuestionnaireReportOverview, bool>> where, Func<IQueryable<QuestionnaireReportOverview>, IOrderedQueryable<QuestionnaireReportOverview>> orderBy, List<Expression<Func<QuestionnaireReportOverview, object>>> includes);
        List<QuestionnaireReportOverview> GetManyPaged(int pageNumber, int pageSize, Expression<Func<QuestionnaireReportOverview, bool>> where, Func<IQueryable<QuestionnaireReportOverview>, IOrderedQueryable<QuestionnaireReportOverview>> orderBy, List<Expression<Func<QuestionnaireReportOverview, object>>> includes, out int pageCount, out int totalRecords);
        List<QuestionnaireReportOverview> GetMany(int companyId, string type, int quesionaireServiceCategoryId, string companyAbn);
        int? GetCount(Expression<Func<QuestionnaireReportOverview, bool>> where, List<Expression<Func<QuestionnaireReportOverview, object>>> includes);
        void Insert(QuestionnaireReportOverview item, bool saveChanges = true);
        void Update(QuestionnaireReportOverview item, bool saveChanges = true);
        void Delete(QuestionnaireReportOverview item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class QuestionnaireReportOverviewService : IQuestionnaireReportOverviewService
    {
        private readonly IRepository<QuestionnaireReportOverview> _repository;

        public QuestionnaireReportOverviewService(IRepository<QuestionnaireReportOverview> repository)
        {
            this._repository = repository;
        }

        public QuestionnaireReportOverview Get(Expression<Func<QuestionnaireReportOverview, bool>> where, List<Expression<Func<QuestionnaireReportOverview, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<QuestionnaireReportOverview> GetMany(int? top, Expression<Func<QuestionnaireReportOverview, bool>> where, Func<IQueryable<QuestionnaireReportOverview>, IOrderedQueryable<QuestionnaireReportOverview>> orderBy, List<Expression<Func<QuestionnaireReportOverview, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

       
        public List<QuestionnaireReportOverview> GetManyPaged(int pageNumber, int pageSize, Expression<Func<QuestionnaireReportOverview, bool>> where, Func<IQueryable<QuestionnaireReportOverview>, IOrderedQueryable<QuestionnaireReportOverview>> orderBy, List<Expression<Func<QuestionnaireReportOverview, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<QuestionnaireReportOverview, bool>> where, List<Expression<Func<QuestionnaireReportOverview, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(QuestionnaireReportOverview item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("QuestionnaireReportOverview");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(QuestionnaireReportOverview item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("QuestionnaireReportOverview");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(QuestionnaireReportOverview item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("QuestionnaireReportOverview");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }


        public List<QuestionnaireReportOverview> GetMany(int companyId, string type, int quesionaireServiceCategoryId, string companyAbn)
        {
            return _repository.GetMany(c => (companyId <= 0 || c.CompanyId == companyId) 
                                && (type == "0" || type == "-1" || c.Type == type)
                                && (quesionaireServiceCategoryId <= 0 || c.TypeOfService == quesionaireServiceCategoryId)
                                && (companyAbn == "0" || companyAbn == "-1" || c.CompanyAbn == companyAbn)
                                ).ToList();
        }
    }

    #endregion
}
