<%@ Control Language="C#" AutoEventWireup="true" Inherits="UserControls_Tiny_ComplianceReports_ComplianceReport_SQ" Codebehind="ComplianceReport_SQ.ascx.cs" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<dxwgv:ASPxGridView ID="grid" runat="server" AutoGenerateColumns="False"
    CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
    CssPostfix="Office2003Blue" ClientInstanceName="grid" 
    KeyFieldName="QuestionnaireId" OnHtmlRowCreated="grid_RowCreated"
    DataSourceID="QuestionnaireWithLocationApprovalViewLatestDataSource1" 
    Enabled="False" Width="100%">
    <ImagesFilterControl>
        <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
        </LoadingPanel>
    </ImagesFilterControl>
    <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
        CssPostfix="Office2003Blue">
        <Header ImageSpacing="5px" SortingImageSpacing="5px">
        </Header>
        <LoadingPanel ImageSpacing="10px">
        </LoadingPanel>
    </Styles>
    <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
        <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
        </LoadingPanelOnStatusBar>
        <LoadingPanel Url="~/App_Themes/Office2003Blue/GridView/Loading.gif">
        </LoadingPanel>
    </Images>
    <Columns>
        <dxwgv:GridViewDataTextColumn FieldName="QuestionnaireId" Visible="False" VisibleIndex="0">
        </dxwgv:GridViewDataTextColumn>
        <dxwgv:GridViewDataComboBoxColumn Caption="Company" FieldName="CompanyId" SortIndex="0"
            SortOrder="Ascending" VisibleIndex="0" Visible="False">
            <PropertiesComboBox DataSourceID="sqldsCompaniesList" TextField="CompanyName"
                ValueField="CompanyId" ValueType="System.Int32">
            </PropertiesComboBox>
            <Settings SortMode="DisplayText" />
        </dxwgv:GridViewDataComboBoxColumn>
        <dxwgv:GridViewDataDateColumn FieldName="CreatedDate" VisibleIndex="0">
        </dxwgv:GridViewDataDateColumn>
        <dxwgv:GridViewDataDateColumn FieldName="ModifiedDate" VisibleIndex="1" SortIndex="1" SortOrder="Descending">
        </dxwgv:GridViewDataDateColumn>
        <dxwgv:GridViewDataComboBoxColumn FieldName="Status" UnboundType="String" Name="Status" Width="130px" VisibleIndex="2">
<PropertiesComboBox DataSourceID="dsQuestionnaireStatus" TextField="QuestionnaireStatusDesc" ValueField="QuestionnaireStatusId" ValueType="System.String" DropDownHeight="150px"></PropertiesComboBox>
            <Settings SortMode="DisplayText" />
</dxwgv:GridViewDataComboBoxColumn>
        <dxwgv:GridViewDataTextColumn FieldName="InitialRiskAssessment" VisibleIndex="5" Visible="False">
        </dxwgv:GridViewDataTextColumn>
        <dxwgv:GridViewDataTextColumn FieldName="MainAssessmentByUserId" VisibleIndex="6" Visible="False">
        </dxwgv:GridViewDataTextColumn>
        <dxwgv:GridViewDataDateColumn FieldName="MainAssessmentDate" VisibleIndex="7" Visible="False">
        </dxwgv:GridViewDataDateColumn>
        <dxwgv:GridViewDataTextColumn FieldName="MainAssessmentRiskRating" VisibleIndex="5" Visible="False">
        </dxwgv:GridViewDataTextColumn>
        <dxwgv:GridViewDataDateColumn FieldName="MainAssessmentValidTo" VisibleIndex="4" Caption="Assessment Valid Till">
            <DataItemTemplate>
                <table><tr><td>
                <asp:Label ID="lblExpiresAt" runat="server" Text=""></asp:Label>
                </td></tr></table>      
            </DataItemTemplate>
        </dxwgv:GridViewDataDateColumn>
        <dxwgv:GridViewDataComboBoxColumn FieldName="Recommended" Width="105px" VisibleIndex="5">
<PropertiesComboBox ValueType="System.Int32"><Items>
<dxe:ListEditItem Text="Yes" Value="1"></dxe:ListEditItem>
<dxe:ListEditItem Text="No" Value="0"></dxe:ListEditItem>
</Items>

<ItemStyle Font-Bold="True"></ItemStyle>
</PropertiesComboBox>

<Settings SortMode="DisplayText"></Settings>

<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<CellStyle HorizontalAlign="Center" VerticalAlign="Middle"></CellStyle>
</dxwgv:GridViewDataComboBoxColumn>
        <dxwgv:GridViewDataTextColumn Name="FinalRiskRating" Width="100px" Caption="Final Risk Rating" VisibleIndex="3">
            <DataItemTemplate>
                <table><tr><td>
                <asp:Label ID="FinalRiskLevel" runat="server" Text=""></asp:Label>
                </td></tr></table>      
            </DataItemTemplate>
        <HeaderStyle HorizontalAlign="Center" Wrap="True"></HeaderStyle>
        </dxwgv:GridViewDataTextColumn>
        <dxwgv:GridViewDataTextColumn Caption="Company Status" FieldName="CompanyStatusDesc" VisibleIndex="10" Visible="True">
        </dxwgv:GridViewDataTextColumn>
        <dxwgv:GridViewDataCheckColumn FieldName="SubContractor" VisibleIndex="10" Visible="False">
        </dxwgv:GridViewDataCheckColumn>
    </Columns>
    <StylesEditors>
        <ProgressBar Height="25px">
        </ProgressBar>
    </StylesEditors>
</dxwgv:ASPxGridView>
<data:QuestionnaireWithLocationApprovalViewLatestDataSource ID="QuestionnaireWithLocationApprovalViewLatestDataSource1"
    runat="server">
</data:QuestionnaireWithLocationApprovalViewLatestDataSource>
<data:QuestionnaireStatusDataSource ID="dsQuestionnaireStatus" runat="server">
</data:QuestionnaireStatusDataSource>
<asp:SqlDataSource ID="sqldsCompaniesList" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="_Companies_GetCompanyNameList" SelectCommandType="StoredProcedure">
</asp:SqlDataSource>