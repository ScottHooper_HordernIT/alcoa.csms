<%@ Control Language="C#" AutoEventWireup="true" Inherits="Adminv2_UserControls_CompaniesEhs" Codebehind="companiesEHS.ascx.cs" %>

<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
        
<table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
    <tr>
        <td style="width: 900px; text-align: left">
            EHSIMS<br />
    
<dxwgv:ASPxGridView ID="ASPxGridView1"
            runat="server" AutoGenerateColumns="False" ClientInstanceName="ASPxGridView1"
            CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
            CssPostfix="Office2003Blue" DataSourceID="CompaniesEhsimsMapDataSource" 
                KeyFieldName="EhssimsMapId" Width="900px"
            OnInitNewRow="grid_InitNewRow"
            >
            <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
                </LoadingPanelOnStatusBar>
                <CollapsedButton Height="12px"
                    Width="11px" />
                <ExpandedButton Height="12px"
                    Width="11px" />
                <DetailCollapsedButton Height="12px"
                    Width="11px" />
                <DetailExpandedButton Height="12px"
                    Width="11px" />
                <LoadingPanel Url="~/App_Themes/Office2003Blue/GridView/Loading.gif">
                </LoadingPanel>
            </Images>
            <ImagesFilterControl>
                <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
                </LoadingPanel>
            </ImagesFilterControl>
            <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
                CssPostfix="Office2003Blue">
                <Header ImageSpacing="5px" SortingImageSpacing="5px">
                </Header>
                <LoadingPanel ImageSpacing="10px">
                </LoadingPanel>
            </Styles>
            <Columns>
             <dxwgv:GridViewCommandColumn AllowDragDrop="False" Caption="Action" VisibleIndex="0" Width="80px">
                    <EditButton Visible="True" />
                    <NewButton Visible="True" />
                    <ClearFilterButton Visible="True" />
                </dxwgv:GridViewCommandColumn>
                <dxwgv:GridViewDataTextColumn FieldName="EhssimsMapId" ReadOnly="True" Visible="False"
                    VisibleIndex="0">
                    <EditFormSettings Visible="False" />
                </dxwgv:GridViewDataTextColumn>
              <dxwgv:GridViewDataComboBoxColumn
                    Caption="Company Name"
                    FieldName="CompanyId"
                    unboundType="String"  SortIndex="0" Settings-SortMode="DisplayText" SortOrder="Ascending"
                    VisibleIndex="1"
                    Width="370px">
                    <PropertiesComboBox
                        DataSourceID="sqldsCompaniesList"
                        DropDownHeight="150px"
                        TextField="CompanyName" 
                        ValueField="CompanyId" IncrementalFilteringMode="StartsWith"
                        ValueType="System.Int32">
                    </PropertiesComboBox>

<Settings SortMode="DisplayText"></Settings>
                </dxwgv:GridViewDataComboBoxColumn>

                <dxwgv:GridViewDataComboBoxColumn
                    Caption="Location Code"
                    FieldName="LocCode"
                    unboundType="String"
                    VisibleIndex="1" SortIndex="1" Settings-SortMode="DisplayText"
                    Width="100px">
                    <PropertiesComboBox
                        DataSourceID="sqldsEHS"
                        DropDownHeight="150px"
                        TextField="Name"
                        ValueField="Id" IncrementalFilteringMode="StartsWith"
                        ValueType="System.String">
                    </PropertiesComboBox>

<Settings SortMode="DisplayText"></Settings>
                </dxwgv:GridViewDataComboBoxColumn>
                
                <dxwgv:GridViewDataTextColumn Caption="Contractor Company Code" FieldName="ContCompanyCode" VisibleIndex="2">
                </dxwgv:GridViewDataTextColumn>
                
            </Columns>
            <Settings ShowFilterRow="True" ShowGroupPanel="True" />
            <SettingsBehavior ColumnResizeMode="NextColumn" ConfirmDelete="True"></SettingsBehavior>
            <SettingsPager>
                <AllButton Visible="True">
                </AllButton>
            </SettingsPager>
            <SettingsEditing Mode="Inline" />
        </dxwgv:ASPxGridView>
            <br />
            
        </td>
    </tr>
    <tr>
        <td style="width: 900px; text-align: right">
            Number of Rows to show per page:<asp:DropDownList ID="DropDownList1" runat="server"
                AutoPostBack="True" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged">
                <asp:ListItem>10</asp:ListItem>
                <asp:ListItem>20</asp:ListItem>
                <asp:ListItem>50</asp:ListItem>
                <asp:ListItem>All</asp:ListItem>
            </asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td style="width: 900px; text-align: left; height: 85px;">

<data:CompaniesEhsimsMapDataSource
    ID="CompaniesEhsimsMapDataSource"
    runat="server"
	SelectMethod="GetPaged"
	EnablePaging="True"
	EnableSorting="True">
</data:CompaniesEhsimsMapDataSource>

<asp:SqlDataSource ID="sqldsCompaniesList" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="_Companies_GetCompanyNameList" SelectCommandType="StoredProcedure">
</asp:SqlDataSource>

            &nbsp;
            <asp:SqlDataSource ID="sqldsEHS" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
                SelectCommand="SELECT DISTINCT(LocCode) As Id, LocCode As Name FROM Sites WHERE LocCode NOT LIKE ''">
            </asp:SqlDataSource>
        </td>
    </tr>
</table>
&nbsp;&nbsp;