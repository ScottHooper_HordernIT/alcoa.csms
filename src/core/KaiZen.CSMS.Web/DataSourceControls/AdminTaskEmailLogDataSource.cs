﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.AdminTaskEmailLogProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(AdminTaskEmailLogDataSourceDesigner))]
	public class AdminTaskEmailLogDataSource : ProviderDataSource<AdminTaskEmailLog, AdminTaskEmailLogKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailLogDataSource class.
		/// </summary>
		public AdminTaskEmailLogDataSource() : base(new AdminTaskEmailLogService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the AdminTaskEmailLogDataSourceView used by the AdminTaskEmailLogDataSource.
		/// </summary>
		protected AdminTaskEmailLogDataSourceView AdminTaskEmailLogView
		{
			get { return ( View as AdminTaskEmailLogDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the AdminTaskEmailLogDataSource control invokes to retrieve data.
		/// </summary>
		public AdminTaskEmailLogSelectMethod SelectMethod
		{
			get
			{
				AdminTaskEmailLogSelectMethod selectMethod = AdminTaskEmailLogSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (AdminTaskEmailLogSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the AdminTaskEmailLogDataSourceView class that is to be
		/// used by the AdminTaskEmailLogDataSource.
		/// </summary>
		/// <returns>An instance of the AdminTaskEmailLogDataSourceView class.</returns>
		protected override BaseDataSourceView<AdminTaskEmailLog, AdminTaskEmailLogKey> GetNewDataSourceView()
		{
			return new AdminTaskEmailLogDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the AdminTaskEmailLogDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class AdminTaskEmailLogDataSourceView : ProviderDataSourceView<AdminTaskEmailLog, AdminTaskEmailLogKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailLogDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the AdminTaskEmailLogDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public AdminTaskEmailLogDataSourceView(AdminTaskEmailLogDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal AdminTaskEmailLogDataSource AdminTaskEmailLogOwner
		{
			get { return Owner as AdminTaskEmailLogDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal AdminTaskEmailLogSelectMethod SelectMethod
		{
			get { return AdminTaskEmailLogOwner.SelectMethod; }
			set { AdminTaskEmailLogOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal AdminTaskEmailLogService AdminTaskEmailLogProvider
		{
			get { return Provider as AdminTaskEmailLogService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<AdminTaskEmailLog> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<AdminTaskEmailLog> results = null;
			AdminTaskEmailLog item;
			count = 0;
			
			System.Int32 _adminTaskEmailId;
			System.Int32 _adminTaskId;
			System.Int32 _csmsEmailLogId;

			switch ( SelectMethod )
			{
				case AdminTaskEmailLogSelectMethod.Get:
					AdminTaskEmailLogKey entityKey  = new AdminTaskEmailLogKey();
					entityKey.Load(values);
					item = AdminTaskEmailLogProvider.Get(entityKey);
					results = new TList<AdminTaskEmailLog>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case AdminTaskEmailLogSelectMethod.GetAll:
                    results = AdminTaskEmailLogProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case AdminTaskEmailLogSelectMethod.GetPaged:
					results = AdminTaskEmailLogProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case AdminTaskEmailLogSelectMethod.Find:
					if ( FilterParameters != null )
						results = AdminTaskEmailLogProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = AdminTaskEmailLogProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case AdminTaskEmailLogSelectMethod.GetByAdminTaskEmailId:
					_adminTaskEmailId = ( values["AdminTaskEmailId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["AdminTaskEmailId"], typeof(System.Int32)) : (int)0;
					item = AdminTaskEmailLogProvider.GetByAdminTaskEmailId(_adminTaskEmailId);
					results = new TList<AdminTaskEmailLog>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				// FK
				case AdminTaskEmailLogSelectMethod.GetByAdminTaskId:
					_adminTaskId = ( values["AdminTaskId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["AdminTaskId"], typeof(System.Int32)) : (int)0;
					results = AdminTaskEmailLogProvider.GetByAdminTaskId(_adminTaskId, this.StartIndex, this.PageSize, out count);
					break;
				case AdminTaskEmailLogSelectMethod.GetByCsmsEmailLogId:
					_csmsEmailLogId = ( values["CsmsEmailLogId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["CsmsEmailLogId"], typeof(System.Int32)) : (int)0;
					results = AdminTaskEmailLogProvider.GetByCsmsEmailLogId(_csmsEmailLogId, this.StartIndex, this.PageSize, out count);
					break;
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == AdminTaskEmailLogSelectMethod.Get || SelectMethod == AdminTaskEmailLogSelectMethod.GetByAdminTaskEmailId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				AdminTaskEmailLog entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					AdminTaskEmailLogProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<AdminTaskEmailLog> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			AdminTaskEmailLogProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region AdminTaskEmailLogDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the AdminTaskEmailLogDataSource class.
	/// </summary>
	public class AdminTaskEmailLogDataSourceDesigner : ProviderDataSourceDesigner<AdminTaskEmailLog, AdminTaskEmailLogKey>
	{
		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailLogDataSourceDesigner class.
		/// </summary>
		public AdminTaskEmailLogDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public AdminTaskEmailLogSelectMethod SelectMethod
		{
			get { return ((AdminTaskEmailLogDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new AdminTaskEmailLogDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region AdminTaskEmailLogDataSourceActionList

	/// <summary>
	/// Supports the AdminTaskEmailLogDataSourceDesigner class.
	/// </summary>
	internal class AdminTaskEmailLogDataSourceActionList : DesignerActionList
	{
		private AdminTaskEmailLogDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailLogDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public AdminTaskEmailLogDataSourceActionList(AdminTaskEmailLogDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public AdminTaskEmailLogSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion AdminTaskEmailLogDataSourceActionList
	
	#endregion AdminTaskEmailLogDataSourceDesigner
	
	#region AdminTaskEmailLogSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the AdminTaskEmailLogDataSource.SelectMethod property.
	/// </summary>
	public enum AdminTaskEmailLogSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByAdminTaskEmailId method.
		/// </summary>
		GetByAdminTaskEmailId,
		/// <summary>
		/// Represents the GetByAdminTaskId method.
		/// </summary>
		GetByAdminTaskId,
		/// <summary>
		/// Represents the GetByCsmsEmailLogId method.
		/// </summary>
		GetByCsmsEmailLogId
	}
	
	#endregion AdminTaskEmailLogSelectMethod

	#region AdminTaskEmailLogFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AdminTaskEmailLog"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AdminTaskEmailLogFilter : SqlFilter<AdminTaskEmailLogColumn>
	{
	}
	
	#endregion AdminTaskEmailLogFilter

	#region AdminTaskEmailLogExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AdminTaskEmailLog"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AdminTaskEmailLogExpressionBuilder : SqlExpressionBuilder<AdminTaskEmailLogColumn>
	{
	}
	
	#endregion AdminTaskEmailLogExpressionBuilder	

	#region AdminTaskEmailLogProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;AdminTaskEmailLogChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="AdminTaskEmailLog"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AdminTaskEmailLogProperty : ChildEntityProperty<AdminTaskEmailLogChildEntityTypes>
	{
	}
	
	#endregion AdminTaskEmailLogProperty
}

