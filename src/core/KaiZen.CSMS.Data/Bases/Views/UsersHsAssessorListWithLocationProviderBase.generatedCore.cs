﻿#region Using directives

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="UsersHsAssessorListWithLocationProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract class UsersHsAssessorListWithLocationProviderBaseCore : EntityViewProviderBase<UsersHsAssessorListWithLocation>
	{
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions
		
		/*
		///<summary>
		/// Fill an VList&lt;UsersHsAssessorListWithLocation&gt; From a DataSet
		///</summary>
		/// <param name="dataSet">the DataSet</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList&lt;UsersHsAssessorListWithLocation&gt;"/></returns>
		protected static VList&lt;UsersHsAssessorListWithLocation&gt; Fill(DataSet dataSet, VList<UsersHsAssessorListWithLocation> rows, int start, int pagelen)
		{
			if (dataSet.Tables.Count == 1)
			{
				return Fill(dataSet.Tables[0], rows, start, pagelen);
			}
			else
			{
				return new VList<UsersHsAssessorListWithLocation>();
			}	
		}
		
		
		///<summary>
		/// Fill an VList&lt;UsersHsAssessorListWithLocation&gt; From a DataTable
		///</summary>
		/// <param name="dataTable">the DataTable that hold the data.</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList<UsersHsAssessorListWithLocation>"/></returns>
		protected static VList&lt;UsersHsAssessorListWithLocation&gt; Fill(DataTable dataTable, VList<UsersHsAssessorListWithLocation> rows, int start, int pagelen)
		{
			int recordnum = 0;
			
			System.Collections.IEnumerator dataRows =  dataTable.Rows.GetEnumerator();
			
			while (dataRows.MoveNext() && (pagelen != 0))
			{
				if(recordnum >= start)
				{
					DataRow row = (DataRow)dataRows.Current;
				
					UsersHsAssessorListWithLocation c = new UsersHsAssessorListWithLocation();
					c.EhsConsultantId = (Convert.IsDBNull(row["EHSConsultantId"]))?(int)0:(System.Int32)row["EHSConsultantId"];
					c.UserId = (Convert.IsDBNull(row["UserId"]))?(int)0:(System.Int32)row["UserId"];
					c.UserFullName = (Convert.IsDBNull(row["UserFullName"]))?string.Empty:(System.String)row["UserFullName"];
					c.UserFullNameLogon = (Convert.IsDBNull(row["UserFullNameLogon"]))?string.Empty:(System.String)row["UserFullNameLogon"];
					c.Enabled = (Convert.IsDBNull(row["Enabled"]))?false:(System.Boolean)row["Enabled"];
					c.SafeNameDefault = (Convert.IsDBNull(row["Default"]))?false:(System.Boolean?)row["Default"];
					c.Email = (Convert.IsDBNull(row["Email"]))?string.Empty:(System.String)row["Email"];
					c.SiteId = (Convert.IsDBNull(row["SiteId"]))?(int)0:(System.Int32?)row["SiteId"];
					c.SiteName = (Convert.IsDBNull(row["SiteName"]))?string.Empty:(System.String)row["SiteName"];
					c.RegionId = (Convert.IsDBNull(row["RegionId"]))?(int)0:(System.Int32?)row["RegionId"];
					c.RegionName = (Convert.IsDBNull(row["RegionName"]))?string.Empty:(System.String)row["RegionName"];
					c.RegionNameAbbrev = (Convert.IsDBNull(row["RegionNameAbbrev"]))?string.Empty:(System.String)row["RegionNameAbbrev"];
					c.AcceptChanges();
					rows.Add(c);
					pagelen -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		*/	
						
		///<summary>
		/// Fill an <see cref="VList&lt;UsersHsAssessorListWithLocation&gt;"/> From a DataReader.
		///</summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pageLength">number of row.</param>
		///<returns>a <see cref="VList&lt;UsersHsAssessorListWithLocation&gt;"/></returns>
		protected VList<UsersHsAssessorListWithLocation> Fill(IDataReader reader, VList<UsersHsAssessorListWithLocation> rows, int start, int pageLength)
		{
			int recordnum = 0;
			while (reader.Read() && (pageLength != 0))
			{
				if(recordnum >= start)
				{
					UsersHsAssessorListWithLocation entity = null;
					if (DataRepository.Provider.UseEntityFactory)
					{
						entity = EntityManager.CreateViewEntity<UsersHsAssessorListWithLocation>("UsersHsAssessorListWithLocation",  DataRepository.Provider.EntityCreationalFactoryType); 
					}
					else
					{
						entity = new UsersHsAssessorListWithLocation();
					}
					
					entity.SuppressEntityEvents = true;

					entity.EhsConsultantId = (System.Int32)reader[((int)UsersHsAssessorListWithLocationColumn.EhsConsultantId)];
					//entity.EhsConsultantId = (Convert.IsDBNull(reader["EHSConsultantId"]))?(int)0:(System.Int32)reader["EHSConsultantId"];
					entity.UserId = (System.Int32)reader[((int)UsersHsAssessorListWithLocationColumn.UserId)];
					//entity.UserId = (Convert.IsDBNull(reader["UserId"]))?(int)0:(System.Int32)reader["UserId"];
					entity.UserFullName = (System.String)reader[((int)UsersHsAssessorListWithLocationColumn.UserFullName)];
					//entity.UserFullName = (Convert.IsDBNull(reader["UserFullName"]))?string.Empty:(System.String)reader["UserFullName"];
					entity.UserFullNameLogon = (System.String)reader[((int)UsersHsAssessorListWithLocationColumn.UserFullNameLogon)];
					//entity.UserFullNameLogon = (Convert.IsDBNull(reader["UserFullNameLogon"]))?string.Empty:(System.String)reader["UserFullNameLogon"];
					entity.Enabled = (System.Boolean)reader[((int)UsersHsAssessorListWithLocationColumn.Enabled)];
					//entity.Enabled = (Convert.IsDBNull(reader["Enabled"]))?false:(System.Boolean)reader["Enabled"];
					entity.SafeNameDefault = (reader.IsDBNull(((int)UsersHsAssessorListWithLocationColumn.SafeNameDefault)))?null:(System.Boolean?)reader[((int)UsersHsAssessorListWithLocationColumn.SafeNameDefault)];
					//entity.SafeNameDefault = (Convert.IsDBNull(reader["Default"]))?false:(System.Boolean?)reader["Default"];
					entity.Email = (System.String)reader[((int)UsersHsAssessorListWithLocationColumn.Email)];
					//entity.Email = (Convert.IsDBNull(reader["Email"]))?string.Empty:(System.String)reader["Email"];
					entity.SiteId = (reader.IsDBNull(((int)UsersHsAssessorListWithLocationColumn.SiteId)))?null:(System.Int32?)reader[((int)UsersHsAssessorListWithLocationColumn.SiteId)];
					//entity.SiteId = (Convert.IsDBNull(reader["SiteId"]))?(int)0:(System.Int32?)reader["SiteId"];
					entity.SiteName = (reader.IsDBNull(((int)UsersHsAssessorListWithLocationColumn.SiteName)))?null:(System.String)reader[((int)UsersHsAssessorListWithLocationColumn.SiteName)];
					//entity.SiteName = (Convert.IsDBNull(reader["SiteName"]))?string.Empty:(System.String)reader["SiteName"];
					entity.RegionId = (reader.IsDBNull(((int)UsersHsAssessorListWithLocationColumn.RegionId)))?null:(System.Int32?)reader[((int)UsersHsAssessorListWithLocationColumn.RegionId)];
					//entity.RegionId = (Convert.IsDBNull(reader["RegionId"]))?(int)0:(System.Int32?)reader["RegionId"];
					entity.RegionName = (reader.IsDBNull(((int)UsersHsAssessorListWithLocationColumn.RegionName)))?null:(System.String)reader[((int)UsersHsAssessorListWithLocationColumn.RegionName)];
					//entity.RegionName = (Convert.IsDBNull(reader["RegionName"]))?string.Empty:(System.String)reader["RegionName"];
					entity.RegionNameAbbrev = (reader.IsDBNull(((int)UsersHsAssessorListWithLocationColumn.RegionNameAbbrev)))?null:(System.String)reader[((int)UsersHsAssessorListWithLocationColumn.RegionNameAbbrev)];
					//entity.RegionNameAbbrev = (Convert.IsDBNull(reader["RegionNameAbbrev"]))?string.Empty:(System.String)reader["RegionNameAbbrev"];
					entity.AcceptChanges();
					entity.SuppressEntityEvents = false;
					
					rows.Add(entity);
					pageLength -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		
		
		/// <summary>
		/// Refreshes the <see cref="UsersHsAssessorListWithLocation"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="UsersHsAssessorListWithLocation"/> object to refresh.</param>
		protected void RefreshEntity(IDataReader reader, UsersHsAssessorListWithLocation entity)
		{
			reader.Read();
			entity.EhsConsultantId = (System.Int32)reader[((int)UsersHsAssessorListWithLocationColumn.EhsConsultantId)];
			//entity.EhsConsultantId = (Convert.IsDBNull(reader["EHSConsultantId"]))?(int)0:(System.Int32)reader["EHSConsultantId"];
			entity.UserId = (System.Int32)reader[((int)UsersHsAssessorListWithLocationColumn.UserId)];
			//entity.UserId = (Convert.IsDBNull(reader["UserId"]))?(int)0:(System.Int32)reader["UserId"];
			entity.UserFullName = (System.String)reader[((int)UsersHsAssessorListWithLocationColumn.UserFullName)];
			//entity.UserFullName = (Convert.IsDBNull(reader["UserFullName"]))?string.Empty:(System.String)reader["UserFullName"];
			entity.UserFullNameLogon = (System.String)reader[((int)UsersHsAssessorListWithLocationColumn.UserFullNameLogon)];
			//entity.UserFullNameLogon = (Convert.IsDBNull(reader["UserFullNameLogon"]))?string.Empty:(System.String)reader["UserFullNameLogon"];
			entity.Enabled = (System.Boolean)reader[((int)UsersHsAssessorListWithLocationColumn.Enabled)];
			//entity.Enabled = (Convert.IsDBNull(reader["Enabled"]))?false:(System.Boolean)reader["Enabled"];
			entity.SafeNameDefault = (reader.IsDBNull(((int)UsersHsAssessorListWithLocationColumn.SafeNameDefault)))?null:(System.Boolean?)reader[((int)UsersHsAssessorListWithLocationColumn.SafeNameDefault)];
			//entity.SafeNameDefault = (Convert.IsDBNull(reader["Default"]))?false:(System.Boolean?)reader["Default"];
			entity.Email = (System.String)reader[((int)UsersHsAssessorListWithLocationColumn.Email)];
			//entity.Email = (Convert.IsDBNull(reader["Email"]))?string.Empty:(System.String)reader["Email"];
			entity.SiteId = (reader.IsDBNull(((int)UsersHsAssessorListWithLocationColumn.SiteId)))?null:(System.Int32?)reader[((int)UsersHsAssessorListWithLocationColumn.SiteId)];
			//entity.SiteId = (Convert.IsDBNull(reader["SiteId"]))?(int)0:(System.Int32?)reader["SiteId"];
			entity.SiteName = (reader.IsDBNull(((int)UsersHsAssessorListWithLocationColumn.SiteName)))?null:(System.String)reader[((int)UsersHsAssessorListWithLocationColumn.SiteName)];
			//entity.SiteName = (Convert.IsDBNull(reader["SiteName"]))?string.Empty:(System.String)reader["SiteName"];
			entity.RegionId = (reader.IsDBNull(((int)UsersHsAssessorListWithLocationColumn.RegionId)))?null:(System.Int32?)reader[((int)UsersHsAssessorListWithLocationColumn.RegionId)];
			//entity.RegionId = (Convert.IsDBNull(reader["RegionId"]))?(int)0:(System.Int32?)reader["RegionId"];
			entity.RegionName = (reader.IsDBNull(((int)UsersHsAssessorListWithLocationColumn.RegionName)))?null:(System.String)reader[((int)UsersHsAssessorListWithLocationColumn.RegionName)];
			//entity.RegionName = (Convert.IsDBNull(reader["RegionName"]))?string.Empty:(System.String)reader["RegionName"];
			entity.RegionNameAbbrev = (reader.IsDBNull(((int)UsersHsAssessorListWithLocationColumn.RegionNameAbbrev)))?null:(System.String)reader[((int)UsersHsAssessorListWithLocationColumn.RegionNameAbbrev)];
			//entity.RegionNameAbbrev = (Convert.IsDBNull(reader["RegionNameAbbrev"]))?string.Empty:(System.String)reader["RegionNameAbbrev"];
			reader.Close();
	
			entity.AcceptChanges();
		}
		
		/*
		/// <summary>
		/// Refreshes the <see cref="UsersHsAssessorListWithLocation"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="UsersHsAssessorListWithLocation"/> object.</param>
		protected static void RefreshEntity(DataSet dataSet, UsersHsAssessorListWithLocation entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.EhsConsultantId = (Convert.IsDBNull(dataRow["EHSConsultantId"]))?(int)0:(System.Int32)dataRow["EHSConsultantId"];
			entity.UserId = (Convert.IsDBNull(dataRow["UserId"]))?(int)0:(System.Int32)dataRow["UserId"];
			entity.UserFullName = (Convert.IsDBNull(dataRow["UserFullName"]))?string.Empty:(System.String)dataRow["UserFullName"];
			entity.UserFullNameLogon = (Convert.IsDBNull(dataRow["UserFullNameLogon"]))?string.Empty:(System.String)dataRow["UserFullNameLogon"];
			entity.Enabled = (Convert.IsDBNull(dataRow["Enabled"]))?false:(System.Boolean)dataRow["Enabled"];
			entity.SafeNameDefault = (Convert.IsDBNull(dataRow["Default"]))?false:(System.Boolean?)dataRow["Default"];
			entity.Email = (Convert.IsDBNull(dataRow["Email"]))?string.Empty:(System.String)dataRow["Email"];
			entity.SiteId = (Convert.IsDBNull(dataRow["SiteId"]))?(int)0:(System.Int32?)dataRow["SiteId"];
			entity.SiteName = (Convert.IsDBNull(dataRow["SiteName"]))?string.Empty:(System.String)dataRow["SiteName"];
			entity.RegionId = (Convert.IsDBNull(dataRow["RegionId"]))?(int)0:(System.Int32?)dataRow["RegionId"];
			entity.RegionName = (Convert.IsDBNull(dataRow["RegionName"]))?string.Empty:(System.String)dataRow["RegionName"];
			entity.RegionNameAbbrev = (Convert.IsDBNull(dataRow["RegionNameAbbrev"]))?string.Empty:(System.String)dataRow["RegionNameAbbrev"];
			entity.AcceptChanges();
		}
		*/
			
		#endregion Helper Functions
	}//end class

	#region UsersHsAssessorListWithLocationFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="UsersHsAssessorListWithLocation"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UsersHsAssessorListWithLocationFilterBuilder : SqlFilterBuilder<UsersHsAssessorListWithLocationColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UsersHsAssessorListWithLocationFilterBuilder class.
		/// </summary>
		public UsersHsAssessorListWithLocationFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the UsersHsAssessorListWithLocationFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public UsersHsAssessorListWithLocationFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the UsersHsAssessorListWithLocationFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public UsersHsAssessorListWithLocationFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion UsersHsAssessorListWithLocationFilterBuilder

	#region UsersHsAssessorListWithLocationParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="UsersHsAssessorListWithLocation"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UsersHsAssessorListWithLocationParameterBuilder : ParameterizedSqlFilterBuilder<UsersHsAssessorListWithLocationColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UsersHsAssessorListWithLocationParameterBuilder class.
		/// </summary>
		public UsersHsAssessorListWithLocationParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the UsersHsAssessorListWithLocationParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public UsersHsAssessorListWithLocationParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the UsersHsAssessorListWithLocationParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public UsersHsAssessorListWithLocationParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion UsersHsAssessorListWithLocationParameterBuilder
	
	#region UsersHsAssessorListWithLocationSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="UsersHsAssessorListWithLocation"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class UsersHsAssessorListWithLocationSortBuilder : SqlSortBuilder<UsersHsAssessorListWithLocationColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UsersHsAssessorListWithLocationSqlSortBuilder class.
		/// </summary>
		public UsersHsAssessorListWithLocationSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion UsersHsAssessorListWithLocationSortBuilder

} // end namespace
