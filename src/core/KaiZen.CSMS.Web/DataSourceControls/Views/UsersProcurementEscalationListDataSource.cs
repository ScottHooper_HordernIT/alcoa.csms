﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.UsersProcurementEscalationListProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[CLSCompliant(true)]
	[Designer(typeof(UsersProcurementEscalationListDataSourceDesigner))]
	public class UsersProcurementEscalationListDataSource : ReadOnlyDataSource<UsersProcurementEscalationList>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UsersProcurementEscalationListDataSource class.
		/// </summary>
		public UsersProcurementEscalationListDataSource() : base(new UsersProcurementEscalationListService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the UsersProcurementEscalationListDataSourceView used by the UsersProcurementEscalationListDataSource.
		/// </summary>
		protected UsersProcurementEscalationListDataSourceView UsersProcurementEscalationListView
		{
			get { return ( View as UsersProcurementEscalationListDataSourceView ); }
		}
		
		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the UsersProcurementEscalationListDataSourceView class that is to be
		/// used by the UsersProcurementEscalationListDataSource.
		/// </summary>
		/// <returns>An instance of the UsersProcurementEscalationListDataSourceView class.</returns>
		protected override BaseDataSourceView<UsersProcurementEscalationList, Object> GetNewDataSourceView()
		{
			return new UsersProcurementEscalationListDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the UsersProcurementEscalationListDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class UsersProcurementEscalationListDataSourceView : ReadOnlyDataSourceView<UsersProcurementEscalationList>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UsersProcurementEscalationListDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the UsersProcurementEscalationListDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public UsersProcurementEscalationListDataSourceView(UsersProcurementEscalationListDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal UsersProcurementEscalationListDataSource UsersProcurementEscalationListOwner
		{
			get { return Owner as UsersProcurementEscalationListDataSource; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal UsersProcurementEscalationListService UsersProcurementEscalationListProvider
		{
			get { return Provider as UsersProcurementEscalationListService; }
		}

		#endregion Properties
		
		#region Methods
		
		#endregion Methods
	}

	#region UsersProcurementEscalationListDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the UsersProcurementEscalationListDataSource class.
	/// </summary>
	public class UsersProcurementEscalationListDataSourceDesigner : ReadOnlyDataSourceDesigner<UsersProcurementEscalationList>
	{
	}

	#endregion UsersProcurementEscalationListDataSourceDesigner

	#region UsersProcurementEscalationListFilter

	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="UsersProcurementEscalationList"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UsersProcurementEscalationListFilter : SqlFilter<UsersProcurementEscalationListColumn>
	{
	}

	#endregion UsersProcurementEscalationListFilter

	#region UsersProcurementEscalationListExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="UsersProcurementEscalationList"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UsersProcurementEscalationListExpressionBuilder : SqlExpressionBuilder<UsersProcurementEscalationListColumn>
	{
	}
	
	#endregion UsersProcurementEscalationListExpressionBuilder		
}

