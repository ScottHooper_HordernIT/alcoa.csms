﻿using System;
using System.ComponentModel;

namespace KaiZen.CSMS.Entities
{
	/// <summary>
	///		The data structure representation of the 'Questionnaire' table via interface.
	/// </summary>
	/// <remarks>
	/// 	This struct is generated by a tool and should never be modified.
	/// </remarks>
	public interface IQuestionnaire 
	{
		/// <summary>			
		/// QuestionnaireId : 
		/// </summary>
		/// <remarks>Member of the primary key of the underlying table "Questionnaire"</remarks>
		System.Int32 QuestionnaireId { get; set; }
				
		
		
		/// <summary>
		/// CompanyId : 
		/// </summary>
		System.Int32  CompanyId  { get; set; }
		
		/// <summary>
		/// CreatedByUserId : 
		/// </summary>
		System.Int32  CreatedByUserId  { get; set; }
		
		/// <summary>
		/// CreatedDate : 
		/// </summary>
		System.DateTime  CreatedDate  { get; set; }
		
		/// <summary>
		/// ModifiedByUserId : 
		/// </summary>
		System.Int32  ModifiedByUserId  { get; set; }
		
		/// <summary>
		/// ModifiedDate : 
		/// </summary>
		System.DateTime  ModifiedDate  { get; set; }
		
		/// <summary>
		/// Status : 
		/// </summary>
		System.Int32  Status  { get; set; }
		
		/// <summary>
		/// IsReQualification : 
		/// </summary>
		System.Boolean  IsReQualification  { get; set; }
		
		/// <summary>
		/// IsMainRequired : 
		/// </summary>
		System.Boolean  IsMainRequired  { get; set; }
		
		/// <summary>
		/// IsVerificationRequired : 
		/// </summary>
		System.Boolean  IsVerificationRequired  { get; set; }
		
		/// <summary>
		/// InitialCategoryHigh : 
		/// </summary>
		System.Boolean?  InitialCategoryHigh  { get; set; }
		
		/// <summary>
		/// InitialCreatedByUserId : 
		/// </summary>
		System.Int32?  InitialCreatedByUserId  { get; set; }
		
		/// <summary>
		/// InitialCreatedDate : 
		/// </summary>
		System.DateTime?  InitialCreatedDate  { get; set; }
		
		/// <summary>
		/// InitialSubmittedByUserId : 
		/// </summary>
		System.Int32?  InitialSubmittedByUserId  { get; set; }
		
		/// <summary>
		/// InitialSubmittedDate : 
		/// </summary>
		System.DateTime?  InitialSubmittedDate  { get; set; }
		
		/// <summary>
		/// InitialModifiedByUserId : 
		/// </summary>
		System.Int32?  InitialModifiedByUserId  { get; set; }
		
		/// <summary>
		/// InitialModifiedDate : 
		/// </summary>
		System.DateTime?  InitialModifiedDate  { get; set; }
		
		/// <summary>
		/// InitialStatus : 
		/// </summary>
		System.Int32  InitialStatus  { get; set; }
		
		/// <summary>
		/// InitialRiskAssessment : 
		/// </summary>
		System.String  InitialRiskAssessment  { get; set; }
		
		/// <summary>
		/// MainCreatedByUserId : 
		/// </summary>
		System.Int32?  MainCreatedByUserId  { get; set; }
		
		/// <summary>
		/// MainCreatedDate : 
		/// </summary>
		System.DateTime?  MainCreatedDate  { get; set; }
		
		/// <summary>
		/// MainModifiedByUserId : 
		/// </summary>
		System.Int32?  MainModifiedByUserId  { get; set; }
		
		/// <summary>
		/// MainModifiedDate : 
		/// </summary>
		System.DateTime?  MainModifiedDate  { get; set; }
		
		/// <summary>
		/// MainScoreExpectations : 
		/// </summary>
		System.Int32?  MainScoreExpectations  { get; set; }
		
		/// <summary>
		/// MainScoreOverall : 
		/// </summary>
		System.Int32?  MainScoreOverall  { get; set; }
		
		/// <summary>
		/// MainScoreResults : 
		/// </summary>
		System.Int32?  MainScoreResults  { get; set; }
		
		/// <summary>
		/// MainScoreStaffing : 
		/// </summary>
		System.Int32?  MainScoreStaffing  { get; set; }
		
		/// <summary>
		/// MainScoreSystems : 
		/// </summary>
		System.Int32?  MainScoreSystems  { get; set; }
		
		/// <summary>
		/// MainStatus : 
		/// </summary>
		System.Int32  MainStatus  { get; set; }
		
		/// <summary>
		/// MainAssessmentByUserId : 
		/// </summary>
		System.Int32?  MainAssessmentByUserId  { get; set; }
		
		/// <summary>
		/// MainAssessmentComments : 
		/// </summary>
		System.String  MainAssessmentComments  { get; set; }
		
		/// <summary>
		/// MainAssessmentDate : 
		/// </summary>
		System.DateTime?  MainAssessmentDate  { get; set; }
		
		/// <summary>
		/// MainAssessmentRiskRating : 
		/// </summary>
		System.String  MainAssessmentRiskRating  { get; set; }
		
		/// <summary>
		/// MainAssessmentStatus : 
		/// </summary>
		System.String  MainAssessmentStatus  { get; set; }
		
		/// <summary>
		/// MainAssessmentValidTo : 
		/// </summary>
		System.DateTime?  MainAssessmentValidTo  { get; set; }
		
		/// <summary>
		/// MainScorePExpectations : 
		/// </summary>
		System.Int32?  MainScorePexpectations  { get; set; }
		
		/// <summary>
		/// MainScorePOverall : 
		/// </summary>
		System.Int32?  MainScorePoverall  { get; set; }
		
		/// <summary>
		/// MainScorePResults : 
		/// </summary>
		System.Int32?  MainScorePresults  { get; set; }
		
		/// <summary>
		/// MainScorePStaffing : 
		/// </summary>
		System.Int32?  MainScorePstaffing  { get; set; }
		
		/// <summary>
		/// MainScorePSystems : 
		/// </summary>
		System.Int32?  MainScorePsystems  { get; set; }
		
		/// <summary>
		/// VerificationCreatedByUserId : 
		/// </summary>
		System.Int32?  VerificationCreatedByUserId  { get; set; }
		
		/// <summary>
		/// VerificationCreatedDate : 
		/// </summary>
		System.DateTime?  VerificationCreatedDate  { get; set; }
		
		/// <summary>
		/// VerificationModifiedByUserId : 
		/// </summary>
		System.Int32?  VerificationModifiedByUserId  { get; set; }
		
		/// <summary>
		/// VerificationModifiedDate : 
		/// </summary>
		System.DateTime?  VerificationModifiedDate  { get; set; }
		
		/// <summary>
		/// VerificationRiskRating : 
		/// </summary>
		System.String  VerificationRiskRating  { get; set; }
		
		/// <summary>
		/// VerificationStatus : 
		/// </summary>
		System.Int32  VerificationStatus  { get; set; }
		
		/// <summary>
		/// ApprovedByUserId : 
		/// </summary>
		System.Int32?  ApprovedByUserId  { get; set; }
		
		/// <summary>
		/// ApprovedDate : 
		/// </summary>
		System.DateTime?  ApprovedDate  { get; set; }
		
		/// <summary>
		/// Recommended : 
		/// </summary>
		System.Boolean?  Recommended  { get; set; }
		
		/// <summary>
		/// RecommendedComments : 
		/// </summary>
		System.String  RecommendedComments  { get; set; }
		
		/// <summary>
		/// LevelOfSupervision : 
		/// </summary>
		System.String  LevelOfSupervision  { get; set; }
		
		/// <summary>
		/// ProcurementModified : 
		/// </summary>
		System.Boolean  ProcurementModified  { get; set; }
		
		/// <summary>
		/// SubContractor : 
		/// </summary>
		System.Boolean?  SubContractor  { get; set; }
		
		/// <summary>
		/// AssessedByUserId : 
		/// </summary>
		System.Int32?  AssessedByUserId  { get; set; }
		
		/// <summary>
		/// AssessedDate : 
		/// </summary>
		System.DateTime?  AssessedDate  { get; set; }
		
		/// <summary>
		/// LastReminderEmailSentOn : 
		/// </summary>
		System.DateTime?  LastReminderEmailSentOn  { get; set; }
		
		/// <summary>
		/// NoReminderEmailsSent : 
		/// </summary>
		System.Int32?  NoReminderEmailsSent  { get; set; }
		
		/// <summary>
		/// SupplierContactVerifiedOn : 
		/// </summary>
		System.DateTime?  SupplierContactVerifiedOn  { get; set; }
		
		/// <summary>
		/// QuestionnairePresentlyWithActionId : 
		/// </summary>
		System.Int32?  QuestionnairePresentlyWithActionId  { get; set; }
		
		/// <summary>
		/// QuestionnairePresentlyWithSince : 
		/// </summary>
		System.DateTime?  QuestionnairePresentlyWithSince  { get; set; }
			
		/// <summary>
		/// Creates a new object that is a copy of the current instance.
		/// </summary>
		/// <returns>A new object that is a copy of this instance.</returns>
		System.Object Clone();
		
		#region Data Properties


		/// <summary>
		///	Holds a collection of entity objects
		///	which are related to this object through the relation _questionnaireActionLogQuestionnaireId
		/// </summary>	
		TList<QuestionnaireActionLog> QuestionnaireActionLogCollection {  get;  set;}	


		/// <summary>
		///	Holds a collection of entity objects
		///	which are related to this object through the relation _questionnaireVerificationAssessmentQuestionnaireId
		/// </summary>	
		TList<QuestionnaireVerificationAssessment> QuestionnaireVerificationAssessmentCollection {  get;  set;}	


		/// <summary>
		///	Holds a collection of entity objects
		///	which are related to this object through the relation _questionnaireInitialContactEmailQuestionnaireId
		/// </summary>	
		TList<QuestionnaireInitialContactEmail> QuestionnaireInitialContactEmailCollection {  get;  set;}	


		/// <summary>
		///	Holds a collection of entity objects
		///	which are related to this object through the relation _questionnaireVerificationAttachmentQuestionnaireId
		/// </summary>	
		TList<QuestionnaireVerificationAttachment> QuestionnaireVerificationAttachmentCollection {  get;  set;}	


		/// <summary>
		///	Holds a collection of entity objects
		///	which are related to this object through the relation _questionnaireMainAssessmentQuestionnaireId
		/// </summary>	
		TList<QuestionnaireMainAssessment> QuestionnaireMainAssessmentCollection {  get;  set;}	


		/// <summary>
		///	Holds a collection of entity objects
		///	which are related to this object through the relation _questionnaireMainResponseQuestionnaireId
		/// </summary>	
		TList<QuestionnaireMainResponse> QuestionnaireMainResponseCollection {  get;  set;}	


		/// <summary>
		///	Holds a collection of entity objects
		///	which are related to this object through the relation _questionnaireInitialLocationQuestionnaireId
		/// </summary>	
		TList<QuestionnaireInitialLocation> QuestionnaireInitialLocationCollection {  get;  set;}	
	

		/// <summary>
		///	Holds a  Questionnaire entity object
		///	which is related to this object through the relation _questionnaireQuestionnaireId
		/// </summary>
		Questionnaire Questionnaire { get; set; }


		/// <summary>
		///	Holds a collection of entity objects
		///	which are related to this object through the relation _questionnaireVerificationResponseQuestionnaireId
		/// </summary>	
		TList<QuestionnaireVerificationResponse> QuestionnaireVerificationResponseCollection {  get;  set;}	


		/// <summary>
		///	Holds a collection of entity objects
		///	which are related to this object through the relation _questionnaireInitialResponseQuestionnaireId
		/// </summary>	
		TList<QuestionnaireInitialResponse> QuestionnaireInitialResponseCollection {  get;  set;}	


		/// <summary>
		///	Holds a collection of entity objects
		///	which are related to this object through the relation _questionnaireCommentsQuestionnaireId
		/// </summary>	
		TList<QuestionnaireComments> QuestionnaireCommentsCollection {  get;  set;}	


		/// <summary>
		///	Holds a collection of entity objects
		///	which are related to this object through the relation _questionnaireServicesSelectedQuestionnaireId
		/// </summary>	
		TList<QuestionnaireServicesSelected> QuestionnaireServicesSelectedCollection {  get;  set;}	


		/// <summary>
		///	Holds a collection of entity objects
		///	which are related to this object through the relation _companyStatusChangeApprovalQuestionnaireId
		/// </summary>	
		TList<CompanyStatusChangeApproval> CompanyStatusChangeApprovalCollection {  get;  set;}	
	

		/// <summary>
		///	Holds a  QuestionnaireVerificationSection entity object
		///	which is related to this object through the relation _questionnaireVerificationSectionQuestionnaireId
		/// </summary>
		QuestionnaireVerificationSection QuestionnaireVerificationSection { get; set; }
	

		/// <summary>
		///	Holds a  QuestionnaireInitialContact entity object
		///	which is related to this object through the relation _questionnaireInitialContactQuestionnaireId
		/// </summary>
		QuestionnaireInitialContact QuestionnaireInitialContact { get; set; }

		#endregion Data Properties

	}
}


