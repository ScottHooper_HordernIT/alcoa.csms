﻿
#region Using Directives
using System;
using System.ComponentModel;
using System.Collections;
using System.Xml.Serialization;
using System.Data;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Entities.Validation;

using KaiZen.CSMS.Data;
using Microsoft.Practices.EnterpriseLibrary.Logging;

#endregion

namespace KaiZen.CSMS.Services
{		
	
	///<summary>
	/// An component type implementation of the '_Current_Smp_EhsConsultantId' table.
	///</summary>
	/// <remarks>
	/// All custom implementations should be done here.
	/// </remarks>
	[CLSCompliant(true)]
	public partial class CurrentSmpEhsConsultantIdService : KaiZen.CSMS.Services.CurrentSmpEhsConsultantIdServiceBase
	{
		/// <summary>
		/// Initializes a new instance of the CurrentSmpEhsConsultantIdService class.
		/// </summary>
		public CurrentSmpEhsConsultantIdService() : base()
		{
		}
		
	}//End Class


} // end namespace
