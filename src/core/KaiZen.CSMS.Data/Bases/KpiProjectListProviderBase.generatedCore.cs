﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="KpiProjectListProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class KpiProjectListProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.KpiProjectList, KaiZen.CSMS.Entities.KpiProjectListKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.KpiProjectListKey key)
		{
			return Delete(transactionManager, key.KpiProjectListId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_kpiProjectListId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _kpiProjectListId)
		{
			return Delete(null, _kpiProjectListId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_kpiProjectListId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _kpiProjectListId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.KpiProjectList Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.KpiProjectListKey key, int start, int pageLength)
		{
			return GetByKpiProjectListId(transactionManager, key.KpiProjectListId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_KpiProjectList index.
		/// </summary>
		/// <param name="_kpiProjectListId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.KpiProjectList"/> class.</returns>
		public KaiZen.CSMS.Entities.KpiProjectList GetByKpiProjectListId(System.Int32 _kpiProjectListId)
		{
			int count = -1;
			return GetByKpiProjectListId(null,_kpiProjectListId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_KpiProjectList index.
		/// </summary>
		/// <param name="_kpiProjectListId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.KpiProjectList"/> class.</returns>
		public KaiZen.CSMS.Entities.KpiProjectList GetByKpiProjectListId(System.Int32 _kpiProjectListId, int start, int pageLength)
		{
			int count = -1;
			return GetByKpiProjectListId(null, _kpiProjectListId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_KpiProjectList index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_kpiProjectListId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.KpiProjectList"/> class.</returns>
		public KaiZen.CSMS.Entities.KpiProjectList GetByKpiProjectListId(TransactionManager transactionManager, System.Int32 _kpiProjectListId)
		{
			int count = -1;
			return GetByKpiProjectListId(transactionManager, _kpiProjectListId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_KpiProjectList index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_kpiProjectListId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.KpiProjectList"/> class.</returns>
		public KaiZen.CSMS.Entities.KpiProjectList GetByKpiProjectListId(TransactionManager transactionManager, System.Int32 _kpiProjectListId, int start, int pageLength)
		{
			int count = -1;
			return GetByKpiProjectListId(transactionManager, _kpiProjectListId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_KpiProjectList index.
		/// </summary>
		/// <param name="_kpiProjectListId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.KpiProjectList"/> class.</returns>
		public KaiZen.CSMS.Entities.KpiProjectList GetByKpiProjectListId(System.Int32 _kpiProjectListId, int start, int pageLength, out int count)
		{
			return GetByKpiProjectListId(null, _kpiProjectListId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_KpiProjectList index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_kpiProjectListId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.KpiProjectList"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.KpiProjectList GetByKpiProjectListId(TransactionManager transactionManager, System.Int32 _kpiProjectListId, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_KpiProjectList index.
		/// </summary>
		/// <param name="_projectNumber"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.KpiProjectList"/> class.</returns>
		public KaiZen.CSMS.Entities.KpiProjectList GetByProjectNumber(System.String _projectNumber)
		{
			int count = -1;
			return GetByProjectNumber(null,_projectNumber, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_KpiProjectList index.
		/// </summary>
		/// <param name="_projectNumber"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.KpiProjectList"/> class.</returns>
		public KaiZen.CSMS.Entities.KpiProjectList GetByProjectNumber(System.String _projectNumber, int start, int pageLength)
		{
			int count = -1;
			return GetByProjectNumber(null, _projectNumber, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_KpiProjectList index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_projectNumber"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.KpiProjectList"/> class.</returns>
		public KaiZen.CSMS.Entities.KpiProjectList GetByProjectNumber(TransactionManager transactionManager, System.String _projectNumber)
		{
			int count = -1;
			return GetByProjectNumber(transactionManager, _projectNumber, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_KpiProjectList index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_projectNumber"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.KpiProjectList"/> class.</returns>
		public KaiZen.CSMS.Entities.KpiProjectList GetByProjectNumber(TransactionManager transactionManager, System.String _projectNumber, int start, int pageLength)
		{
			int count = -1;
			return GetByProjectNumber(transactionManager, _projectNumber, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_KpiProjectList index.
		/// </summary>
		/// <param name="_projectNumber"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.KpiProjectList"/> class.</returns>
		public KaiZen.CSMS.Entities.KpiProjectList GetByProjectNumber(System.String _projectNumber, int start, int pageLength, out int count)
		{
			return GetByProjectNumber(null, _projectNumber, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_KpiProjectList index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_projectNumber"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.KpiProjectList"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.KpiProjectList GetByProjectNumber(TransactionManager transactionManager, System.String _projectNumber, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_KpiProjectList_1 index.
		/// </summary>
		/// <param name="_projectDescription"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.KpiProjectList"/> class.</returns>
		public KaiZen.CSMS.Entities.KpiProjectList GetByProjectDescription(System.String _projectDescription)
		{
			int count = -1;
			return GetByProjectDescription(null,_projectDescription, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_KpiProjectList_1 index.
		/// </summary>
		/// <param name="_projectDescription"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.KpiProjectList"/> class.</returns>
		public KaiZen.CSMS.Entities.KpiProjectList GetByProjectDescription(System.String _projectDescription, int start, int pageLength)
		{
			int count = -1;
			return GetByProjectDescription(null, _projectDescription, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_KpiProjectList_1 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_projectDescription"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.KpiProjectList"/> class.</returns>
		public KaiZen.CSMS.Entities.KpiProjectList GetByProjectDescription(TransactionManager transactionManager, System.String _projectDescription)
		{
			int count = -1;
			return GetByProjectDescription(transactionManager, _projectDescription, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_KpiProjectList_1 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_projectDescription"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.KpiProjectList"/> class.</returns>
		public KaiZen.CSMS.Entities.KpiProjectList GetByProjectDescription(TransactionManager transactionManager, System.String _projectDescription, int start, int pageLength)
		{
			int count = -1;
			return GetByProjectDescription(transactionManager, _projectDescription, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_KpiProjectList_1 index.
		/// </summary>
		/// <param name="_projectDescription"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.KpiProjectList"/> class.</returns>
		public KaiZen.CSMS.Entities.KpiProjectList GetByProjectDescription(System.String _projectDescription, int start, int pageLength, out int count)
		{
			return GetByProjectDescription(null, _projectDescription, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_KpiProjectList_1 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_projectDescription"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.KpiProjectList"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.KpiProjectList GetByProjectDescription(TransactionManager transactionManager, System.String _projectDescription, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#region _KpiProjectList_Reseed 
		
		/// <summary>
		///	This method wrap the '_KpiProjectList_Reseed' stored procedure. 
		/// </summary>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Reseed()
		{
			return Reseed(null, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_KpiProjectList_Reseed' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Reseed(int start, int pageLength)
		{
			return Reseed(null, start, pageLength );
		}
				
		/// <summary>
		///	This method wrap the '_KpiProjectList_Reseed' stored procedure. 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Reseed(TransactionManager transactionManager)
		{
			return Reseed(transactionManager, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_KpiProjectList_Reseed' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet Reseed(TransactionManager transactionManager, int start, int pageLength );
		
		#endregion
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;KpiProjectList&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;KpiProjectList&gt;"/></returns>
		public static TList<KpiProjectList> Fill(IDataReader reader, TList<KpiProjectList> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.KpiProjectList c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("KpiProjectList")
					.Append("|").Append((System.Int32)reader[((int)KpiProjectListColumn.KpiProjectListId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<KpiProjectList>(
					key.ToString(), // EntityTrackingKey
					"KpiProjectList",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.KpiProjectList();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.KpiProjectListId = (System.Int32)reader[((int)KpiProjectListColumn.KpiProjectListId - 1)];
					c.ProjectNumber = (System.String)reader[((int)KpiProjectListColumn.ProjectNumber - 1)];
					c.ProjectDescription = (System.String)reader[((int)KpiProjectListColumn.ProjectDescription - 1)];
					c.ProjectCode = (System.String)reader[((int)KpiProjectListColumn.ProjectCode - 1)];
					c.Area = (System.String)reader[((int)KpiProjectListColumn.Area - 1)];
					c.ProjectStatus = (System.String)reader[((int)KpiProjectListColumn.ProjectStatus - 1)];
					c.CurrentProjectManager = (System.String)reader[((int)KpiProjectListColumn.CurrentProjectManager - 1)];
					c.Phase = (System.String)reader[((int)KpiProjectListColumn.Phase - 1)];
					c.OriginalCompletionDate = (reader.IsDBNull(((int)KpiProjectListColumn.OriginalCompletionDate - 1)))?null:(System.DateTime?)reader[((int)KpiProjectListColumn.OriginalCompletionDate - 1)];
					c.AuthorisedDate = (reader.IsDBNull(((int)KpiProjectListColumn.AuthorisedDate - 1)))?null:(System.DateTime?)reader[((int)KpiProjectListColumn.AuthorisedDate - 1)];
					c.ConceptApprovalNumber = (reader.IsDBNull(((int)KpiProjectListColumn.ConceptApprovalNumber - 1)))?null:(System.String)reader[((int)KpiProjectListColumn.ConceptApprovalNumber - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.KpiProjectList"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.KpiProjectList"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.KpiProjectList entity)
		{
			if (!reader.Read()) return;
			
			entity.KpiProjectListId = (System.Int32)reader[((int)KpiProjectListColumn.KpiProjectListId - 1)];
			entity.ProjectNumber = (System.String)reader[((int)KpiProjectListColumn.ProjectNumber - 1)];
			entity.ProjectDescription = (System.String)reader[((int)KpiProjectListColumn.ProjectDescription - 1)];
			entity.ProjectCode = (System.String)reader[((int)KpiProjectListColumn.ProjectCode - 1)];
			entity.Area = (System.String)reader[((int)KpiProjectListColumn.Area - 1)];
			entity.ProjectStatus = (System.String)reader[((int)KpiProjectListColumn.ProjectStatus - 1)];
			entity.CurrentProjectManager = (System.String)reader[((int)KpiProjectListColumn.CurrentProjectManager - 1)];
			entity.Phase = (System.String)reader[((int)KpiProjectListColumn.Phase - 1)];
			entity.OriginalCompletionDate = (reader.IsDBNull(((int)KpiProjectListColumn.OriginalCompletionDate - 1)))?null:(System.DateTime?)reader[((int)KpiProjectListColumn.OriginalCompletionDate - 1)];
			entity.AuthorisedDate = (reader.IsDBNull(((int)KpiProjectListColumn.AuthorisedDate - 1)))?null:(System.DateTime?)reader[((int)KpiProjectListColumn.AuthorisedDate - 1)];
			entity.ConceptApprovalNumber = (reader.IsDBNull(((int)KpiProjectListColumn.ConceptApprovalNumber - 1)))?null:(System.String)reader[((int)KpiProjectListColumn.ConceptApprovalNumber - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.KpiProjectList"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.KpiProjectList"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.KpiProjectList entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.KpiProjectListId = (System.Int32)dataRow["KpiProjectListId"];
			entity.ProjectNumber = (System.String)dataRow["ProjectNumber"];
			entity.ProjectDescription = (System.String)dataRow["ProjectDescription"];
			entity.ProjectCode = (System.String)dataRow["ProjectCode"];
			entity.Area = (System.String)dataRow["Area"];
			entity.ProjectStatus = (System.String)dataRow["ProjectStatus"];
			entity.CurrentProjectManager = (System.String)dataRow["CurrentProjectManager"];
			entity.Phase = (System.String)dataRow["Phase"];
			entity.OriginalCompletionDate = Convert.IsDBNull(dataRow["OriginalCompletionDate"]) ? null : (System.DateTime?)dataRow["OriginalCompletionDate"];
			entity.AuthorisedDate = Convert.IsDBNull(dataRow["AuthorisedDate"]) ? null : (System.DateTime?)dataRow["AuthorisedDate"];
			entity.ConceptApprovalNumber = Convert.IsDBNull(dataRow["ConceptApprovalNumber"]) ? null : (System.String)dataRow["ConceptApprovalNumber"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.KpiProjectList"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.KpiProjectList Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.KpiProjectList entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.KpiProjectList object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.KpiProjectList instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.KpiProjectList Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.KpiProjectList entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region KpiProjectListChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.KpiProjectList</c>
	///</summary>
	public enum KpiProjectListChildEntityTypes
	{
	}
	
	#endregion KpiProjectListChildEntityTypes
	
	#region KpiProjectListFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;KpiProjectListColumn&gt;"/> class
	/// that is used exclusively with a <see cref="KpiProjectList"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class KpiProjectListFilterBuilder : SqlFilterBuilder<KpiProjectListColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the KpiProjectListFilterBuilder class.
		/// </summary>
		public KpiProjectListFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the KpiProjectListFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public KpiProjectListFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the KpiProjectListFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public KpiProjectListFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion KpiProjectListFilterBuilder
	
	#region KpiProjectListParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;KpiProjectListColumn&gt;"/> class
	/// that is used exclusively with a <see cref="KpiProjectList"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class KpiProjectListParameterBuilder : ParameterizedSqlFilterBuilder<KpiProjectListColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the KpiProjectListParameterBuilder class.
		/// </summary>
		public KpiProjectListParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the KpiProjectListParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public KpiProjectListParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the KpiProjectListParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public KpiProjectListParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion KpiProjectListParameterBuilder
	
	#region KpiProjectListSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;KpiProjectListColumn&gt;"/> class
	/// that is used exclusively with a <see cref="KpiProjectList"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class KpiProjectListSortBuilder : SqlSortBuilder<KpiProjectListColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the KpiProjectListSqlSortBuilder class.
		/// </summary>
		public KpiProjectListSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion KpiProjectListSortBuilder
	
} // end namespace
