<%@ Control Language="C#" AutoEventWireup="true"
    Inherits="UserControls_SqQuestionnaire_Questionnaire1Procurement" CodeBehind="Questionnaire1Procurement.ascx.cs" %>
<%@ Register Assembly="DevExpress.Web.ASPxHtmlEditor.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxHtmlEditor" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallback" TagPrefix="dxcb" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxNavBar" TagPrefix="dxnb" %>
<%@ Register Assembly="DevExpress.Web.ASPxGauges.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGauges" TagPrefix="dxg" %>
<%@ Register Assembly="DevExpress.Web.ASPxGauges.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGauges.Gauges" TagPrefix="dxg" %>
<%@ Register Assembly="DevExpress.Web.ASPxGauges.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGauges.Gauges.Linear" TagPrefix="dxg" %>
<%@ Register Assembly="DevExpress.Web.ASPxGauges.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGauges.Gauges.Circular" TagPrefix="dxg" %>
<%@ Register Assembly="DevExpress.Web.ASPxGauges.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGauges.Gauges.State" TagPrefix="dxg" %>
<%@ Register Assembly="DevExpress.Web.ASPxGauges.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGauges.Gauges.Digital" TagPrefix="dxg" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallbackPanel" TagPrefix="dxcp" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxRoundPanel" TagPrefix="dxrp" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dxp" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>

<%@ Register assembly="DevExpress.Web.ASPxSpellChecker.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxSpellChecker" tagprefix="dx" %>

<style type="text/css">
    .auto-style1 {
        width: 269px;
        height: 19px;
    }

    .auto-style2 {
        width: 386px;
        height: 19px;
    }
</style>

<%: System.Web.Optimization.Scripts.Render("~/bundles/jquery_scripts") %>

<script type="text/javascript">
    $(document).ready(function () {
        var companyName = cmbCompanies.GetValue();
        pnlHealthCheck.PerformCallback(companyName);
    });
</script>

<table border="0" cellpadding="2" cellspacing="0" width="100%">
    <tr>
        <td class="pageName" colspan="3" style="height: 17px">
            <span class="bodycopy"><span class="title">Safety Qualification</span><br />
                <span class="date">
                    <dxe:ASPxHyperLink ID="hlQuestionnaire" runat="server" Text="Questionnaire" ToolTip="Go back to Safety Qualification Page"
                        NavigateUrl="~/SafetyPQ_Questionnaire.aspx">
                    </dxe:ASPxHyperLink>
                    >
                    <asp:LinkButton ID="LinkButton1" runat="server">Assess/Review</asp:LinkButton></span>&nbsp;
                &gt; Procurement Questionnaire<br />
                <img height="1" src="images/grfc_dottedline.gif" width="24" />&nbsp;</span>
        </td>
    </tr>
</table>
<br />
<table border="0" cellpadding="0" cellspacing="0" style="width: 900px">
    <tr>
        <td style="width: 900px; height: 46px; text-align: center">
            <div align="center">
                <table border="0" cellpadding="1" cellspacing="1">
                    <tr>
                        <td>
                            <dxe:ASPxButton ID="btnHome" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                CssPostfix="Office2003Blue" OnClick="btnHome_Click" Text="Home" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" UseSubmitBehavior="false">
                            </dxe:ASPxButton>
                        </td>
                        <td>
                            <dxe:ASPxButton ID="btnSave" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                CssPostfix="Office2003Blue" OnClick="btnSave_Click" Text="Save " SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" UseSubmitBehavior="false">
                            </dxe:ASPxButton>
                        </td>
                        <td>
                            <dxe:ASPxButton ID="btnSubmitTop" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                CssPostfix="Office2003Blue" OnClick="btnSubmit_Click" Text="Submit" ValidationGroup="Submit"
                                SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" Enabled="False" UseSubmitBehavior="false">
                            </dxe:ASPxButton>
                        </td>
                        <td>
                            <%--btnApproveTop, btnApproveBottom, btnNotApproveTop and btnNotApproveBottom added by ashley Goldstraw 11/1/2016 DT222--%>
                            <dxe:ASPxButton ID="btnApproveTop" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                CssPostfix="Office2003Blue" OnClick="btnSubmit_Click" Text="Approve" Visible="false"
                                SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" UseSubmitBehavior="false">
                                <ClientSideEvents Click="function (s, e) {e.processOnServer = confirm('Are you sure you want to approve this procurement questionnaire?');}" />
                            </dxe:ASPxButton>
                        </td>
                        <td>
                            <dxe:ASPxButton ID="btnNotApproveTop" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                CssPostfix="Office2003Blue" Text="Not Approve" 
                                SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" Visible="False" UseSubmitBehavior="false" AutoPostBack="false">
                            </dxe:ASPxButton>
                        </td>
                        <td>
                            <asp:Panel ID="pnlPrint" runat="server">
                                <input id="btnPrint" onclick="window.print();" type="Button" value="Print" style="width: 50px; height: 29px;" />
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
            </div>
            <br />
            <asp:Label ID="lblSave" runat="server" Font-Italic="True" ForeColor="Red"></asp:Label>
            <br />
            <br />
            <dxe:ASPxLabel ID="lblSubmit2" runat="server" ForeColor="Red" Text="Note: You can not submit this procurement questionnaire until the supplier contact has been verified.">
            </dxe:ASPxLabel>
        </td>
    </tr>
    <tr>
        <td style="width: 900px; height: 46px; text-align: right">
            <table border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 197px; height: 25px; text-align: right">
                        <strong><span><span style="font-family: Verdana">Company Name:</span> &nbsp;</span></strong>
                    </td>
                    <td style="width: 326px; height: 25px; text-align: left">
                        <asp:Label ID="lblCompanyName" runat="server" Font-Size="12pt" Width="291px"></asp:Label>
                    </td>
                    <td style="width: 418px; height: 25px; text-align: right">
                        <strong><span style="font-family: Verdana">Status:</span>&nbsp; </strong>
                    </td>
                    <td style="width: 200px; height: 25px; text-align: left">
                        <dxe:ASPxLabel ID="lblStatus" runat="server" Font-Bold="False" ForeColor="#C04000"
                            Text="Not Saved" Font-Size="12pt" Width="100%">
                        </dxe:ASPxLabel>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" style="width: 900px">
    <tr>
        <td style="width: 900px; height: 363px">
            <dxrp:ASPxRoundPanel ID="Aspxroundpanel1" runat="server" BackColor="#DDECFE" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                CssPostfix="Office2003Blue" EnableDefaultAppearance="False" HeaderText="1" Width="900px"
                SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                <TopEdge>
                    <BackgroundImage HorizontalPosition="left" ImageUrl="~/App_Themes/Office2003Blue/Web/rpTopEdge.png"
                        Repeat="RepeatX" VerticalPosition="top" />
                </TopEdge>
                <BottomRightCorner Height="9px" Width="9px" />
                <ContentPaddings Padding="2px" PaddingBottom="10px" PaddingTop="10px" />
                <NoHeaderTopRightCorner Height="9px" Width="9px" />
                <HeaderRightEdge>
                    <BackgroundImage HorizontalPosition="left" ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png"
                        Repeat="RepeatX" VerticalPosition="top" />
                </HeaderRightEdge>
                <Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px" />
                <HeaderLeftEdge>
                    <BackgroundImage HorizontalPosition="left" ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png"
                        Repeat="RepeatX" VerticalPosition="top" />
                </HeaderLeftEdge>
                <HeaderStyle BackColor="#7BA4E0">
                    <Paddings Padding="0px" PaddingBottom="7px" PaddingLeft="2px" PaddingRight="2px" />
                    <BorderBottom BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px" />
                </HeaderStyle>
                <TopRightCorner Height="9px" Width="9px" />
                <HeaderContent>
                    <BackgroundImage HorizontalPosition="left" ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png"
                        Repeat="RepeatX" VerticalPosition="top" />
                </HeaderContent>
                <DisabledStyle ForeColor="Gray">
                </DisabledStyle>
                <NoHeaderTopEdge BackColor="#DDECFE">
                </NoHeaderTopEdge>
                <NoHeaderTopLeftCorner Height="9px" Width="9px" />
                <PanelCollection>
                    <dxp:PanelContent runat="server">
                        <table style="width: 100%" cellspacing="0" cellpadding="0" border="0">
                            <tbody>
                                <tr>
                                    <td style="height: 21px">Who will be the person "completing" the Safety Qualification Questionnaire:
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height: 21px; text-align: center">
                                        <div align="center">
                                            <span style="font-size: 10pt; color: red">
                                                <dxe:ASPxLabel ID="lblInfo" runat="server" Text="Please ensure that the name and email address details entered below are correct.">
                                                </dxe:ASPxLabel>
                                            </span>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height: 156px">
                                        <table border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="width: 269px; height: 19px; text-align: right; padding-right: 3px;">First Name:
                                                </td>
                                                <td style="width: 386px; height: 19px">
                                                    <dxe:ASPxTextBox ID="qAnswer1_1_" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                        CssPostfix="Office2003Blue" Width="287px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                        Enabled="False" NullText="Please enter details by clicking the [Edit] button.."
                                                        ReadOnly="True">
                                                        <ValidationSettings ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True" ValidationGroup="Submit"
                                                            RequiredField-ErrorText="Question 1 - First Name is Required.">
                                                            <RequiredField IsRequired="True"></RequiredField>
                                                        </ValidationSettings>
                                                    </dxe:ASPxTextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: right; padding-right: 3px;" class="auto-style1">Last Name:
                                                </td>
                                                <td class="auto-style2">
                                                    <dxe:ASPxTextBox ID="qAnswer1_2_" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                        CssPostfix="Office2003Blue" Width="287px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                        Enabled="False" NullText="Please enter details by clicking the [Edit] button.."
                                                        ReadOnly="True">
                                                        <ValidationSettings ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True" ValidationGroup="Submit"
                                                            RequiredField-ErrorText="Question 1 - Last Name is Required.">
                                                            <RequiredField IsRequired="True"></RequiredField>
                                                        </ValidationSettings>
                                                    </dxe:ASPxTextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 269px; text-align: right; padding-right: 3px;">E-Mail Address:
                                                </td>
                                                <td style="width: 386px">
                                                    <dxe:ASPxTextBox ID="qAnswer1_3_" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                        CssPostfix="Office2003Blue" Width="287px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                        Enabled="False" NullText="Please enter details by clicking the [Edit] button.."
                                                        ReadOnly="True">
                                                        <ValidationSettings ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True" ValidationGroup="Submit"
                                                            RequiredField-ErrorText="Question 1 - E-Mail Address is Required.">
                                                            <RequiredField IsRequired="True"></RequiredField>
                                                        </ValidationSettings>
                                                    </dxe:ASPxTextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 269px; text-align: right; padding-right: 3px;">Job Title:
                                                </td>
                                                <td style="width: 386px">
                                                    <dxe:ASPxTextBox ID="qAnswer1_4_" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                        CssPostfix="Office2003Blue" Width="287px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                        Enabled="False" NullText="Please enter details by clicking the [Edit] button.."
                                                        ReadOnly="True">
                                                        <ValidationSettings ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True" ValidationGroup="Submit"
                                                            RequiredField-ErrorText="Question 1 - Job Title is Required.">
                                                            <RequiredField IsRequired="True"></RequiredField>
                                                        </ValidationSettings>
                                                    </dxe:ASPxTextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 269px; text-align: right; padding-right: 3px;">Telephone:
                                                </td>
                                                <td style="width: 386px">
                                                    <dxe:ASPxTextBox ID="qAnswer1_5_" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                        CssPostfix="Office2003Blue" Width="287px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                        Enabled="False" NullText="Please enter details by clicking the [Edit] button.."
                                                        ReadOnly="True">
                                                        <ValidationSettings ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True" ValidationGroup="Submit"
                                                            RequiredField-ErrorText="Question 1 - Telephone is Required.">
                                                            <RequiredField IsRequired="True"></RequiredField>
                                                        </ValidationSettings>
                                                    </dxe:ASPxTextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 269px; text-align: right; padding-right: 3px;">Company Name:
                                                </td>
                                                <td style="width: 386px">
                                                    <dxe:ASPxTextBox ID="qAnswer1_6_" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                        CssPostfix="Office2003Blue" Width="287px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                        Enabled="False" NullText="Please enter details by clicking the [Edit] button.."
                                                        ReadOnly="True">
                                                        <ValidationSettings ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True" ValidationGroup="Submit"
                                                            RequiredField-ErrorText="Question 1 - Company Name is Required.">
                                                            <RequiredField IsRequired="True"></RequiredField>
                                                        </ValidationSettings>
                                                    </dxe:ASPxTextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 269px; text-align: right; padding-right: 3px;">ABN:
                                                </td>
                                                <td style="width: 386px">
                                                    <dxe:ASPxTextBox ID="qAnswer1_7_" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                        CssPostfix="Office2003Blue" Width="287px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                        Enabled="False" NullText="Please enter details by clicking the [Edit] button.."
                                                        ReadOnly="True">
                                                        <ValidationSettings ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True" ValidationGroup="Submit"
                                                            RequiredField-ErrorText="Question 1 - ABN is Required.">
                                                            <RequiredField IsRequired="True"></RequiredField>
                                                        </ValidationSettings>
                                                    </dxe:ASPxTextBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                            <tr>
                                <td style="height: 21px; text-align: center">
                                    <div align="center" style="text-align: left">
                                        <b>Contact E-Mail Verification Status:</b>
                                        <dxe:ASPxLabel ID="lblVerify" runat="server" Text="-" ForeColor="Red">
                                        </dxe:ASPxLabel>
                                        <br />
                                        <table cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td>1)&nbsp;
                                                </td>
                                                <td>
                                                    <dxe:ASPxButton ID="btnEdit" runat="server" AutoPostBack="False" ClientInstanceName="btnEdit"
                                                        CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue"
                                                        SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" Text="Enter and Edit the Contact Details Above"
                                                        UseSubmitBehavior="False">
                                                    </dxe:ASPxButton>
                                                </td>
                                                <td>&nbsp;
                                                </td>
                                            </tr>
                                        </table>
                                        <table cellpadding="0" cellspacing="0" style="padding-top: 2px">
                                            <tr>
                                                <td>2)&nbsp;
                                                </td>
                                                <td>
                                                    <dxe:ASPxButton ID="btnVerify" runat="server" AutoPostBack="False" ClientInstanceName="btnVerify"
                                                        CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue"
                                                        Enabled="False" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                        Text="Create/Send Verification E-Mail" UseSubmitBehavior="False">
                                                    </dxe:ASPxButton>
                                                </td>
                                                <td>&nbsp; to Supplier Contact listed Above.&nbsp;[&nbsp;Number of Emails Sent:
                                                    <dxe:ASPxLabel ID="lblVerifyNotes0" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                        CssPostfix="Office2003Blue" ForeColor="Red" Text="n/a">
                                                    </dxe:ASPxLabel>
                                                    <dxe:ASPxLabel ID="lblVerifyNotes1" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                        CssPostfix="Office2003Blue" ForeColor="Red">
                                                    </dxe:ASPxLabel>
                                                    &nbsp;]
                                                </td>
                                            </tr>
                                        </table>
                                        <table cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td>3) Once you have received confirmation, you can:&nbsp;
                                                </td>
                                                <td>
                                                    <dxe:ASPxButton ID="btnVerifyConfirm" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                        CssPostfix="Office2003Blue" OnClick="btnVerifyConfirm_Click" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                        Text="Confirm the Supplier Contact Details are Correct" Visible="True" Enabled="false">
                                                        <ClientSideEvents Click="function (s, e) {e.processOnServer = confirm('Are you sure you want to confirm this supplier contact detail is correct?');}" />
                                                    </dxe:ASPxButton>
                                                </td>
                                                <td>&nbsp;<dxe:ASPxLabel ID="lblConfirmTime" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                    CssPostfix="Office2003Blue" ForeColor="Red">
                                                </dxe:ASPxLabel>
                                                </td>
                                            </tr>
                                        </table>
                                        &nbsp;
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <dx:ASPxPopupControl ID="ASPxPopupControl1" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                            CssPostfix="Office2003Blue" EnableHotTrack="False" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                            Modal="True" Width="420px" PopupElementID="btnEdit" HeaderText="Edit/Update Contact Details">
                            <HeaderStyle>
                                <Paddings PaddingRight="6px" />
                            </HeaderStyle>
                            <ContentCollection>
                                <dx:PopupControlContentControl runat="server">
                                    <table border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td style="width: 269px; height: 19px; text-align: right; padding-right: 3px;">First Name:
                                            </td>
                                            <td style="width: 386px; height: 19px">
                                                <dxe:ASPxTextBox ID="qAnswer1_1" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                    CssPostfix="Office2003Blue" Width="287px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                                    <ValidationSettings ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True" ValidationGroup="Edit">
                                                        <RequiredField IsRequired="True"></RequiredField>
                                                    </ValidationSettings>
                                                </dxe:ASPxTextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 269px; text-align: right; padding-right: 3px;">Last Name:
                                            </td>
                                            <td style="width: 386px">
                                                <dxe:ASPxTextBox ID="qAnswer1_2" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                    CssPostfix="Office2003Blue" Width="287px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                                    <ValidationSettings ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True" ValidationGroup="Edit">
                                                        <RequiredField IsRequired="True"></RequiredField>
                                                    </ValidationSettings>
                                                </dxe:ASPxTextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 269px; text-align: right; padding-right: 3px;">E-Mail Address:
                                            </td>
                                            <td style="width: 386px">
                                                <dxe:ASPxTextBox ID="qAnswer1_3" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                    CssPostfix="Office2003Blue" Width="287px"
                                                    SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                    Style="height: 19px; width: 287px">
                                                    <ValidationSettings ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True" ValidationGroup="Edit">
                                                        <RegularExpression ErrorText="A Valid Email address is required. (Check if any spaces or invalid characters in the email)"
                                                            ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" />
                                                        <RequiredField IsRequired="True"></RequiredField>
                                                    </ValidationSettings>
                                                </dxe:ASPxTextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 269px; text-align: right; padding-right: 3px;">Job Title:
                                            </td>
                                            <td style="width: 386px">
                                                <dxe:ASPxTextBox ID="qAnswer1_4" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                    CssPostfix="Office2003Blue" Width="287px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                                    <ValidationSettings ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True" ValidationGroup="Edit">
                                                        <RequiredField IsRequired="True"></RequiredField>
                                                    </ValidationSettings>
                                                </dxe:ASPxTextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 269px; text-align: right; padding-right: 3px;">Telephone:
                                            </td>
                                            <td style="width: 386px">
                                                <dxe:ASPxTextBox ID="qAnswer1_5" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                    CssPostfix="Office2003Blue" Width="287px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                                    <ValidationSettings ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True" ValidationGroup="Edit">
                                                        <RequiredField IsRequired="True"></RequiredField>
                                                    </ValidationSettings>
                                                </dxe:ASPxTextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 269px; text-align: right; padding-right: 3px;">Company Name:
                                            </td>
                                            <td style="width: 386px">
                                                <dxe:ASPxTextBox ID="qAnswer1_6" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                    CssPostfix="Office2003Blue" Width="287px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                                    <ValidationSettings ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True" ValidationGroup="Edit">
                                                        <RequiredField IsRequired="True"></RequiredField>
                                                    </ValidationSettings>
                                                </dxe:ASPxTextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 269px; text-align: right; padding-right: 3px;">ABN:
                                            </td>
                                            <td style="width: 386px">
                                                <dxe:ASPxTextBox ID="qAnswer1_7" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                    CssPostfix="Office2003Blue" Width="287px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                                    <ValidationSettings ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True" ValidationGroup="Edit">
                                                        <RequiredField IsRequired="True"></RequiredField>
                                                    </ValidationSettings>
                                                </dxe:ASPxTextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: center; padding-right: 3px;" colspan="2">
                                                <div align="center">
                                                    <span style="font-size: 10pt; color: red">
                                                        <dxe:ASPxLabel ID="lblEditSaveWarning" runat="server" Text="Warning: This supplier contact is currently verified. If you change the e-mail address above and save/update, the listed supplier contact will have to be verified again."
                                                            Visible="false">
                                                        </dxe:ASPxLabel>
                                                    </span>
                                                </div>
                                                <br />
                                                <div align="center">
                                                    <dxe:ASPxButton ID="ASPxButton1" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                        CssPostfix="Office2003Blue" OnClick="ASPxButton1_Click" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                        Text="Save/Update" ValidationGroup="Edit">
                                                        <ClientSideEvents Click="function (s, e) {e.processOnServer = confirm('Are you sure you want to save/update?');}" />
                                                    </dxe:ASPxButton>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </dx:PopupControlContentControl>
                            </ContentCollection>
                        </dx:ASPxPopupControl>
                        <dx:ASPxPopupControl ID="ASPxPopupControl2" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                            CssPostfix="Office2003Blue" EnableHotTrack="False" PopupElementID="btnVerify"
                            SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" Modal="True" Width="420px"
                            PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" HeaderText="Send Verification E-Mail to Supplier Contact">
                            <HeaderStyle>
                                <Paddings PaddingRight="6px" />
                            </HeaderStyle>
                            <ContentCollection>
                                <dx:PopupControlContentControl runat="server">
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td colspan="2">
                                                <dxe:ASPxLabel ID="lblVerifyNotes" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                    CssPostfix="Office2003Blue" ForeColor="Red">
                                                </dxe:ASPxLabel>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>From:
                                            </td>
                                            <td>
                                                <dxe:ASPxTextBox ID="tbVerifyFrom" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                    CssPostfix="Office2003Blue" Enabled="False" ReadOnly="True" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                    Width="500px">
                                                </dxe:ASPxTextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>To:
                                            </td>
                                            <td>
                                                <dxe:ASPxTextBox ID="tbVerifyTo" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                    CssPostfix="Office2003Blue" Enabled="False" ReadOnly="True" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                    Width="500px">
                                                </dxe:ASPxTextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>CC:
                                            </td>
                                            <td>
                                                <dxe:ASPxTextBox ID="tbVerifyCC" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                    CssPostfix="Office2003Blue" Enabled="False" ReadOnly="True" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                    Width="500px">
                                                </dxe:ASPxTextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Subject:
                                            </td>
                                            <td>
                                                <dxe:ASPxTextBox ID="tbVerifySubject" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                    CssPostfix="Office2003Blue" Enabled="False" ReadOnly="True" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                    Width="500px">
                                                </dxe:ASPxTextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>
                                                <%--<dxe:ASPxMemo ID="mbVerifyMessageOld" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                    CssPostfix="Office2003Blue" Height="150px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                    Width="500px">
                                                </dxe:ASPxMemo>--%>

                                                <dx:ASPxHtmlEditor ID="hteVerifyMessage" runat="server" Width="500px">
                                                    <Settings AllowHtmlView="false" AllowPreview="false" />
                                                    <Toolbars>
                                                        <dx:htmleditortoolbar>
                                                        <Items>
                                                            <dx:ToolbarParagraphFormattingEdit Width="120px">
                                                                <Items>
                                                                    <dx:ToolbarListEditItem Text="Normal" Value="p" />
                                                                    <dx:ToolbarListEditItem Text="Heading 1" Value="h1" />
                                                                    <dx:ToolbarListEditItem Text="Heading 2" Value="h2" />
                                                                    <dx:ToolbarListEditItem Text="Heading 3" Value="h3" />
                                                                    <dx:ToolbarListEditItem Text="Heading 4" Value="h4" />
                                                                    <dx:ToolbarListEditItem Text="Heading 5" Value="h5" />
                                                                    <dx:ToolbarListEditItem Text="Heading 6" Value="h6" />
                                                                    <dx:ToolbarListEditItem Text="Address" Value="address" />
                                                                    <dx:ToolbarListEditItem Text="Normal (DIV)" Value="div" />
                                                                </Items>
                                                            </dx:ToolbarParagraphFormattingEdit>
                                                            <dx:ToolbarFontNameEdit>
                                                                <Items>
                                                                    <dx:ToolbarListEditItem Text="Times New Roman" Value="Times New Roman" />
                                                                    <dx:ToolbarListEditItem Text="Tahoma" Value="Tahoma" />
                                                                    <dx:ToolbarListEditItem Text="Verdana" Value="Verdana" />
                                                                    <dx:ToolbarListEditItem Text="Arial" Value="Arial" />
                                                                    <dx:ToolbarListEditItem Text="MS Sans Serif" Value="MS Sans Serif" />
                                                                    <dx:ToolbarListEditItem Text="Courier" Value="Courier" />
                                                                </Items>
                                                            </dx:ToolbarFontNameEdit>
                                                            <dx:ToolbarFontSizeEdit>
                                                                <Items>
                                                                    <dx:ToolbarListEditItem Text="1 (8pt)" Value="1" />
                                                                    <dx:ToolbarListEditItem Text="2 (10pt)" Value="2" />
                                                                    <dx:ToolbarListEditItem Text="3 (12pt)" Value="3" />
                                                                    <dx:ToolbarListEditItem Text="4 (14pt)" Value="4" />
                                                                    <dx:ToolbarListEditItem Text="5 (18pt)" Value="5" />
                                                                    <dx:ToolbarListEditItem Text="6 (24pt)" Value="6" />
                                                                    <dx:ToolbarListEditItem Text="7 (36pt)" Value="7" />
                                                                </Items>
                                                            </dx:ToolbarFontSizeEdit>
                                                            <dx:ToolbarBoldButton BeginGroup="True">
                                                            </dx:ToolbarBoldButton>
                                                            <dx:ToolbarItalicButton>
                                                            </dx:ToolbarItalicButton>
                                                            <dx:ToolbarUnderlineButton>
                                                            </dx:ToolbarUnderlineButton>
                                                            <dx:ToolbarStrikethroughButton>
                                                            </dx:ToolbarStrikethroughButton>
                                                            <dx:ToolbarJustifyLeftButton BeginGroup="True">
                                                            </dx:ToolbarJustifyLeftButton>
                                                            <dx:ToolbarJustifyCenterButton>
                                                            </dx:ToolbarJustifyCenterButton>
                                                            <dx:ToolbarJustifyRightButton>
                                                            </dx:ToolbarJustifyRightButton>
                                                            <dx:ToolbarJustifyFullButton>
                                                            </dx:ToolbarJustifyFullButton>
                                                            <dx:ToolbarBackColorButton BeginGroup="True">
                                                            </dx:ToolbarBackColorButton>
                                                            <dx:ToolbarFontColorButton>
                                                            </dx:ToolbarFontColorButton>
                                                        </Items>
                                                    </dx:htmleditortoolbar>

                                                    </Toolbars>
                                                </dx:ASPxHtmlEditor>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <div align="center">
                                                    <dxe:ASPxButton ID="btnVerifyEmail" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                        CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                        Text="Send Verification E-Mail" Width="224px" OnClick="btnVerifyEmail_Click">
                                                        <ClientSideEvents Click="function (s, e) {e.processOnServer = confirm('Are you sure you want to send the contact verification e-mail?');}" />
                                                    </dxe:ASPxButton>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;
                                            </td>
                                            <td>&nbsp;
                                            </td>
                                        </tr>
                                    </table>
                                </dx:PopupControlContentControl>
                            </ContentCollection>
                        </dx:ASPxPopupControl>
                    </dxp:PanelContent>
                </PanelCollection>

                <TopLeftCorner Height="9px" Width="9px" />
                <BottomLeftCorner Height="9px" Width="9px" />
            </dxrp:ASPxRoundPanel>
            <asp:Label ID="rpbr2" runat="server" Text="<br />"></asp:Label>
            <dxrp:ASPxRoundPanel ID="ASPxRoundPanel2" runat="server" BackColor="#DDECFE" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                CssPostfix="Office2003Blue" EnableDefaultAppearance="False" HeaderText="2" Width="900px"
                SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                <TopEdge>
                    <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpTopEdge.png" Repeat="RepeatX"
                        HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
                </TopEdge>
                <BottomRightCorner Height="9px" Width="9px">
                </BottomRightCorner>
                <ContentPaddings Padding="2px" PaddingTop="10px" PaddingBottom="10px"></ContentPaddings>
                <NoHeaderTopRightCorner Height="9px" Width="9px">
                </NoHeaderTopRightCorner>
                <HeaderRightEdge>
                    <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" Repeat="RepeatX"
                        HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
                </HeaderRightEdge>
                <Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px"></Border>
                <HeaderLeftEdge>
                    <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" Repeat="RepeatX"
                        HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
                </HeaderLeftEdge>
                <HeaderStyle BackColor="#7BA4E0">
                    <Paddings Padding="0px" PaddingLeft="2px" PaddingRight="2px" PaddingBottom="7px"></Paddings>
                    <BorderBottom BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px"></BorderBottom>
                </HeaderStyle>
                <TopRightCorner Height="9px" Width="9px">
                </TopRightCorner>
                <HeaderContent>
                    <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" Repeat="RepeatX"
                        HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
                </HeaderContent>
                <DisabledStyle ForeColor="Gray">
                </DisabledStyle>
                <NoHeaderTopEdge BackColor="#DDECFE">
                </NoHeaderTopEdge>
                <NoHeaderTopLeftCorner Height="9px" Width="9px">
                </NoHeaderTopLeftCorner>
                <PanelCollection>
                    <dxp:PanelContent runat="server">
                        <table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td style="width: 269px; text-align: right; padding-right: 3px;">Safety Assessor:
                                </td>
                                <td style="width: 100px">
                                    <dxe:ASPxComboBox runat="server" Width="287px" CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                        ID="qAnswer2_0" ValueType="System.Int32" IncrementalFilteringMode="StartsWith" ReadOnly="true"
                                        Enabled="false" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                        <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                                        </LoadingPanelImage>
                                        <ButtonStyle Width="13px">
                                        </ButtonStyle>
                                    </dxe:ASPxComboBox>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 269px; text-align: right; padding-right: 3px;">Procurement Contact:
                                </td>
                                <td style="width: 100px">
                                    <dxe:ASPxComboBox runat="server" Width="287px" CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                        ID="qAnswer2" DataSourceID="UsersProcurementListDataSource2" TextField="UserFullName"
                                        ValueField="UserId" ValueType="System.Int32" IncrementalFilteringMode="StartsWith"
                                        SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                        <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                                        </LoadingPanelImage>
                                        <ButtonStyle Width="13px">
                                        </ButtonStyle>
                                        <ValidationSettings ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True" ValidationGroup="Submit"
                                            RequiredField-ErrorText="Question 2 - Procurement Contact is Required.">
                                            <RequiredField IsRequired="True" />
                                        </ValidationSettings>
                                    </dxe:ASPxComboBox>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 269px; text-align: right; padding-right: 3px;">Procurement Functional Supervisor:
                                </td>
                                <td style="width: 100px">
                                    <dxe:ASPxComboBox runat="server" Width="287px" CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                        ID="qAnswer2_1" DataSourceID="UsersProcurementListDataSource2" TextField="UserFullName"
                                        ValueField="UserId" ValueType="System.Int32" IncrementalFilteringMode="StartsWith"
                                        SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                        <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                                        </LoadingPanelImage>
                                        <ButtonStyle Width="13px">
                                        </ButtonStyle>
                                        <ValidationSettings ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True" ValidationGroup="Submit"
                                            RequiredField-ErrorText="Question 2 - Procurement Functional Supervisor is Required.">
                                            <RequiredField IsRequired="True" />
                                        </ValidationSettings>
                                    </dxe:ASPxComboBox>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 269px; text-align: right; padding-right: 3px;">Original Requestor Name:
                                </td>
                                <td style="width: 100px">
                                    <dxe:ASPxTextBox ID="qAnswer2_2" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                        CssPostfix="Office2003Blue" Width="287px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                        <ValidationSettings ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True" ValidationGroup="Submit"
                                            RequiredField-ErrorText="Question 2 - Original Requester Name is Required.">
                                            <RequiredField IsRequired="True" />
                                        </ValidationSettings>
                                    </dxe:ASPxTextBox>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 269px; text-align: right; padding-right: 3px;">Original Requester Telephone:
                                </td>
                                <td style="width: 100px">
                                    <dxe:ASPxTextBox ID="qAnswer2_3" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                        CssPostfix="Office2003Blue" Width="287px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                        <ValidationSettings ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True" ValidationGroup="Submit"
                                            RequiredField-ErrorText="Question 2 - Original Requester Telephone is Required.">
                                            <RequiredField IsRequired="True" />
                                        </ValidationSettings>
                                    </dxe:ASPxTextBox>
                                </td>
                            </tr>
                        </table>
                    </dxp:PanelContent>
                </PanelCollection>
                <TopLeftCorner Height="9px" Width="9px">
                </TopLeftCorner>
                <BottomLeftCorner Height="9px" Width="9px">
                </BottomLeftCorner>
            </dxrp:ASPxRoundPanel>
            <asp:Label ID="rpbr3" runat="server" Text="<br />"></asp:Label>
            <dxrp:ASPxRoundPanel ID="ASPxRoundPanel3" runat="server" BackColor="#DDECFE" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                CssPostfix="Office2003Blue" EnableDefaultAppearance="False" HeaderText="3" Width="900px"
                SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                <TopEdge>
                    <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpTopEdge.png" Repeat="RepeatX"
                        HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
                </TopEdge>
                <BottomRightCorner Height="9px" Width="9px">
                </BottomRightCorner>
                <ContentPaddings Padding="2px" PaddingTop="10px" PaddingBottom="10px"></ContentPaddings>
                <NoHeaderTopRightCorner Height="9px" Width="9px">
                </NoHeaderTopRightCorner>
                <HeaderRightEdge>
                    <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" Repeat="RepeatX"
                        HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
                </HeaderRightEdge>
                <Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px"></Border>
                <HeaderLeftEdge>
                    <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" Repeat="RepeatX"
                        HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
                </HeaderLeftEdge>
                <HeaderStyle BackColor="#7BA4E0">
                    <Paddings Padding="0px" PaddingLeft="2px" PaddingRight="2px" PaddingBottom="7px"></Paddings>
                    <BorderBottom BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px"></BorderBottom>
                </HeaderStyle>
                <TopRightCorner Height="9px" Width="9px">
                </TopRightCorner>
                <HeaderContent>
                    <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" Repeat="RepeatX"
                        HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
                </HeaderContent>
                <DisabledStyle ForeColor="Gray">
                </DisabledStyle>
                <NoHeaderTopEdge BackColor="#DDECFE">
                </NoHeaderTopEdge>
                <NoHeaderTopLeftCorner Height="9px" Width="9px">
                </NoHeaderTopLeftCorner>
                <PanelCollection>
                    <dxp:PanelContent runat="server">
                        <table style="width: 100%" cellspacing="0" cellpadding="0" border="0">
                            <tbody>
                                <tr>
                                    <td style="height: 21px">Brief description of work:
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height: 14px"></td>
                                </tr>
                                <tr>
                                    <td>
                                        <dxe:ASPxMemo runat="server" Height="85px" Width="100%" ID="qAnswer3" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                            CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                            <ValidationSettings ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True" ValidationGroup="Submit"
                                                RequiredField-ErrorText="Question 3 - Brief Description of Work is Required.">
                                                <RequiredField IsRequired="True" />
                                                <RequiredField IsRequired="True"></RequiredField>
                                            </ValidationSettings>
                                        </dxe:ASPxMemo>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </dxp:PanelContent>
                </PanelCollection>
                <TopLeftCorner Height="9px" Width="9px">
                </TopLeftCorner>
                <BottomLeftCorner Height="9px" Width="9px">
                </BottomLeftCorner>
            </dxrp:ASPxRoundPanel>
            <asp:Label ID="rpbr4" runat="server" Text="<br />"></asp:Label>
            <dxrp:ASPxRoundPanel ID="ASPxRoundPanel4" runat="server" BackColor="#DDECFE" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                CssPostfix="Office2003Blue" EnableDefaultAppearance="False" HeaderText="4" Width="900px"
                SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                <TopEdge>
                    <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpTopEdge.png" Repeat="RepeatX"
                        HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
                </TopEdge>
                <BottomRightCorner Height="9px" Width="9px">
                </BottomRightCorner>
                <ContentPaddings Padding="2px" PaddingTop="10px" PaddingBottom="10px"></ContentPaddings>
                <NoHeaderTopRightCorner Height="9px" Width="9px">
                </NoHeaderTopRightCorner>
                <HeaderRightEdge>
                    <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" Repeat="RepeatX"
                        HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
                </HeaderRightEdge>
                <Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px"></Border>
                <HeaderLeftEdge>
                    <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" Repeat="RepeatX"
                        HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
                </HeaderLeftEdge>
                <HeaderStyle BackColor="#7BA4E0">
                    <Paddings Padding="0px" PaddingLeft="2px" PaddingRight="2px" PaddingBottom="7px"></Paddings>
                    <BorderBottom BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px"></BorderBottom>
                </HeaderStyle>
                <TopRightCorner Height="9px" Width="9px">
                </TopRightCorner>
                <HeaderContent>
                    <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" Repeat="RepeatX"
                        HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
                </HeaderContent>
                <DisabledStyle ForeColor="Gray">
                </DisabledStyle>
                <NoHeaderTopEdge BackColor="#DDECFE">
                </NoHeaderTopEdge>
                <NoHeaderTopLeftCorner Height="9px" Width="9px">
                </NoHeaderTopLeftCorner>
                <PanelCollection>
                    <dxp:PanelContent runat="server">
                        <table style="width: 100%" cellspacing="0" cellpadding="0" border="0">
                            <tbody>
                                <tr>
                                    <td style="height: 21px">Primary Service currently being offered by
                                        <asp:Label ID="lblSub1" runat="server"
                                            Text=""></asp:Label>Contractor:
                                    </td>
                                </tr>
                                <tr>
                                    <td>&nbsp;
                                        <dxe:ASPxComboBox ID="qAnswer4" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                            CssPostfix="Office2003Blue" DataSourceID="QuestionnaireServicesCategoryDataSource"
                                            IncrementalFilteringMode="StartsWith" ValueType="System.Int32" TextField="CategoryText"
                                            ValueField="CategoryId" Width="355px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                            <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                                            </LoadingPanelImage>
                                            <ValidationSettings ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True" ValidationGroup="Submit"
                                                RequiredField-ErrorText="Question 4 - Primary Service currently offered by Supplier required.">
                                                <RequiredField IsRequired="True"></RequiredField>
                                            </ValidationSettings>
                                        </dxe:ASPxComboBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>Other: If the service does not appear in the above list, please describe the service
                                        below:
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:TextBox ID="qAnswer4_1" runat="server" Width="99%"></asp:TextBox>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </dxp:PanelContent>
                </PanelCollection>
                <TopLeftCorner Height="9px" Width="9px" />
                <BottomLeftCorner Height="9px" Width="9px" />
            </dxrp:ASPxRoundPanel>
            <asp:Label ID="rpbr5" runat="server" Text="<br />"></asp:Label>
            <dxrp:ASPxRoundPanel ID="Aspxroundpanel5" runat="server" BackColor="#DDECFE" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                CssPostfix="Office2003Blue" EnableDefaultAppearance="False" HeaderText="5" Width="900px"
                SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                <TopEdge>
                    <BackgroundImage HorizontalPosition="left" ImageUrl="~/App_Themes/Office2003Blue/Web/rpTopEdge.png"
                        Repeat="RepeatX" VerticalPosition="top" />
                </TopEdge>
                <BottomRightCorner Height="9px" Width="9px" />
                <ContentPaddings Padding="2px" PaddingBottom="10px" PaddingTop="10px" />
                <NoHeaderTopRightCorner Height="9px" Width="9px" />
                <HeaderRightEdge>
                    <BackgroundImage HorizontalPosition="left" ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png"
                        Repeat="RepeatX" VerticalPosition="top" />
                </HeaderRightEdge>
                <Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px" />
                <HeaderLeftEdge>
                    <BackgroundImage HorizontalPosition="left" ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png"
                        Repeat="RepeatX" VerticalPosition="top" />
                </HeaderLeftEdge>
                <HeaderStyle BackColor="#7BA4E0">
                    <Paddings Padding="0px" PaddingBottom="7px" PaddingLeft="2px" PaddingRight="2px" />
                    <BorderBottom BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px" />
                </HeaderStyle>
                <TopRightCorner Height="9px" Width="9px" />
                <HeaderContent>
                    <BackgroundImage HorizontalPosition="left" ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png"
                        Repeat="RepeatX" VerticalPosition="top" />
                </HeaderContent>
                <DisabledStyle ForeColor="Gray">
                </DisabledStyle>
                <NoHeaderTopEdge BackColor="#DDECFE">
                </NoHeaderTopEdge>
                <NoHeaderTopLeftCorner Height="9px" Width="9px" />
                <PanelCollection>
                    <dxp:PanelContent runat="server">
                        <table style="width: 100%" cellspacing="0" cellpadding="0" border="0">
                            <tbody>
                                <tr>
                                    <td style="height: 19px">Number of people expected to work on site:<br />
                                        <em>This information is required to determine on boarding resources availability and
                                            training needs.</em>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height: 14px"></td>
                                </tr>
                                <tr>
                                    <td style="height: 25px">
                                        <dxe:ASPxTextBox ID="qAnswer5" runat="server" Width="170px" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                            CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                            <ValidationSettings ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True" ValidationGroup="Submit"
                                                RequiredField-ErrorText="Question 5 - Number of People Expected To Work On Site is Required.">
                                                <RegularExpression ErrorText="Question 5 - Number of People Expected To Work On Site must be in numeric form (0-9) only"
                                                    ValidationExpression="^\d+$" />
                                                <RequiredField IsRequired="True" />
                                            </ValidationSettings>
                                        </dxe:ASPxTextBox>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </dxp:PanelContent>
                </PanelCollection>
                <TopLeftCorner Height="9px" Width="9px" />
                <BottomLeftCorner Height="9px" Width="9px" />
            </dxrp:ASPxRoundPanel>
            <asp:Label ID="rpbr6" runat="server" Text="<br />"></asp:Label>
            <dxrp:ASPxRoundPanel ID="Aspxroundpanel6" runat="server" BackColor="#DDECFE" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                CssPostfix="Office2003Blue" EnableDefaultAppearance="False" HeaderText="6" Width="900px"
                SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                <TopEdge>
                    <BackgroundImage HorizontalPosition="left" ImageUrl="~/App_Themes/Office2003Blue/Web/rpTopEdge.png"
                        Repeat="RepeatX" VerticalPosition="top" />
                </TopEdge>
                <BottomRightCorner Height="9px" Width="9px" />
                <ContentPaddings Padding="2px" PaddingBottom="10px" PaddingTop="10px" />
                <NoHeaderTopRightCorner Height="9px" Width="9px" />
                <HeaderRightEdge>
                    <BackgroundImage HorizontalPosition="left" ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png"
                        Repeat="RepeatX" VerticalPosition="top" />
                </HeaderRightEdge>
                <Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px" />
                <HeaderLeftEdge>
                    <BackgroundImage HorizontalPosition="left" ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png"
                        Repeat="RepeatX" VerticalPosition="top" />
                </HeaderLeftEdge>
                <HeaderStyle BackColor="#7BA4E0">
                    <Paddings Padding="0px" PaddingBottom="7px" PaddingLeft="2px" PaddingRight="2px" />
                    <BorderBottom BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px" />
                </HeaderStyle>
                <TopRightCorner Height="9px" Width="9px" />
                <HeaderContent>
                    <BackgroundImage HorizontalPosition="left" ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png"
                        Repeat="RepeatX" VerticalPosition="top" />
                </HeaderContent>
                <DisabledStyle ForeColor="Gray">
                </DisabledStyle>
                <NoHeaderTopEdge BackColor="#DDECFE">
                </NoHeaderTopEdge>
                <NoHeaderTopLeftCorner Height="9px" Width="9px" />
                <PanelCollection>
                    <dxp:PanelContent runat="server">
                        <table style="width: 100%" cellspacing="0" cellpadding="0" border="0">
                            <tbody>
                                <tr>
                                    <td style="height: 19px" colspan="2">
                                        <asp:Label ID="lblLocationText" runat="server" Text="Location(s) Requesting Service:"></asp:Label><br />
                                        nb. Click on 'Update' to save your selection.
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="height: 14px"></td>
                                </tr>
                                <tr>
                                    <td style="height: 25px" colspan="2">
                                        <dxwgv:ASPxGridView runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                            ID="gridLocations" CssPostfix="Office2003Blue" KeyFieldName="CompanySiteCategoryStandardId"
                                            AutoGenerateColumns="False" Width="100%" OnRowUpdating="gridLocations_RowUpdating"
                                            OnRowInserting="gridLocations_RowInserting" OnRowDeleting="gridLocations_RowDeleting">
                                            <Columns>
                                                <dxwgv:GridViewCommandColumn ShowInCustomizationForm="True" Width="150px" VisibleIndex="0">
                                                    <EditButton Visible="True">
                                                    </EditButton>
                                                    <NewButton Visible="True">
                                                    </NewButton>
                                                    <DeleteButton Visible="True">
                                                    </DeleteButton>
                                                    <ClearFilterButton Visible="True">
                                                    </ClearFilterButton>
                                                </dxwgv:GridViewCommandColumn>
                                                <dxwgv:GridViewDataComboBoxColumn Name="Site" Caption="Site" FieldName="SiteId" UnboundType="String"
                                                    VisibleIndex="1" SortOrder="Ascending">
                                                    <PropertiesComboBox DataSourceID="SitesDataSource2" DropDownHeight="150px" TextField="SiteName"
                                                        IncrementalFilteringMode="StartsWith" ValueField="SiteId" ValueType="System.Int32">
                                                    </PropertiesComboBox>
                                                </dxwgv:GridViewDataComboBoxColumn>
                                                <dxwgv:GridViewDataComboBoxColumn Caption="Residential Category" FieldName="CompanySiteCategoryId"
                                                    VisibleIndex="2" Visible="false">
                                                    <PropertiesComboBox DataSourceID="CompanySiteCategoryDataSource" TextField="CategoryDesc"
                                                        ValueField="CompanySiteCategoryId" ValueType="System.Int32" IncrementalFilteringMode="StartsWith">
                                                    </PropertiesComboBox>
                                                    <Settings SortMode="DisplayText" />
                                                </dxwgv:GridViewDataComboBoxColumn>
                                                <dxwgv:GridViewDataTextColumn FieldName="SiteId" VisibleIndex="2" ReadOnly="True"
                                                    Visible="False">
                                                </dxwgv:GridViewDataTextColumn>
                                                <dxwgv:GridViewDataComboBoxColumn Caption="Location Sponsor" FieldName="LocationSponsorUserId"
                                                    VisibleIndex="3">
                                                    <PropertiesComboBox DataSourceID="UsersAlcoanDataSource2" DropDownHeight="150px"
                                                        TextField="UserFullName" IncrementalFilteringMode="StartsWith" ValueField="UserId"
                                                        ValueType="System.Int32">
                                                    </PropertiesComboBox>
                                                </dxwgv:GridViewDataComboBoxColumn>
                                                <dxwgv:GridViewDataComboBoxColumn FieldName="Area"
                                                    VisibleIndex="3" Visible="false">
                                                    <PropertiesComboBox DropDownHeight="150px"
                                                        IncrementalFilteringMode="StartsWith"
                                                        ValueType="System.String">
                                                        <Items>
                                                            <dxe:ListEditItem Text="Mining" Value="Mining" />
                                                            <dxe:ListEditItem Text="Refining" Value="Refining" />
                                                            <dxe:ListEditItem Text="Residue" Value="Residue" />
                                                            <dxe:ListEditItem Text="(n/a)" Value="(n/a)" />
                                                        </Items>
                                                        <ValidationSettings CausesValidation="true" RequiredField-IsRequired="true" />
                                                    </PropertiesComboBox>
                                                </dxwgv:GridViewDataComboBoxColumn>
                                                <dxwgv:GridViewDataComboBoxColumn FieldName="Approved" VisibleIndex="4" Visible="false">
                                                    <PropertiesComboBox ValueType="System.Int32" NullDisplayText="Tentative">
                                                        <Items>
                                                            <dxe:ListEditItem Text="Yes" Value="1"></dxe:ListEditItem>
                                                            <dxe:ListEditItem Text="No" Value="0"></dxe:ListEditItem>
                                                            <dxe:ListEditItem Text="Tentative" Value=""></dxe:ListEditItem>
                                                        </Items>
                                                    </PropertiesComboBox>
                                                </dxwgv:GridViewDataComboBoxColumn>
                                                <dxwgv:GridViewDataTextColumn FieldName="ApprovedByUserId" Visible="false">
                                                </dxwgv:GridViewDataTextColumn>
                                                <dxwgv:GridViewDataTextColumn FieldName="ApprovedDate" Visible="false">
                                                </dxwgv:GridViewDataTextColumn>
                                            </Columns>
                                            <SettingsPager Mode="ShowAllRecords" Visible="False">
                                            </SettingsPager>
                                            <SettingsEditing Mode="Inline"></SettingsEditing>
                                            <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                                <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
                                                </LoadingPanelOnStatusBar>
                                                <LoadingPanel Url="~/App_Themes/Office2003Blue/GridView/Loading.gif">
                                                </LoadingPanel>
                                            </Images>
                                            <ImagesFilterControl>
                                                <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
                                                </LoadingPanel>
                                            </ImagesFilterControl>
                                            <Styles CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css">
                                                <Header SortingImageSpacing="5px" ImageSpacing="5px">
                                                </Header>
                                                <LoadingPanel ImageSpacing="10px">
                                                </LoadingPanel>
                                            </Styles>
                                            <StylesEditors>
                                                <ProgressBar Height="25px">
                                                </ProgressBar>
                                            </StylesEditors>
                                            <SettingsBehavior ConfirmDelete="true" />
                                        </dxwgv:ASPxGridView>
                                        <data:CompanySiteCategoryStandardDataSource ID="dsCompanySiteCategoryStandard" runat="server"
                                            EnablePaging="True" EnableSorting="True" SelectMethod="GetByCompanyId" EnableCaching="False"
                                            InsertMethod="Insert" UpdateMethod="Update">
                                            <DeepLoadProperties Method="IncludeChildren" Recursive="False">
                                            </DeepLoadProperties>
                                            <Parameters>
                                                <asp:QueryStringParameter DefaultValue="0" Name="CompanyId" QueryStringField="c" />
                                            </Parameters>
                                        </data:CompanySiteCategoryStandardDataSource>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </dxp:PanelContent>
                </PanelCollection>
                <TopLeftCorner Height="9px" Width="9px" />
                <BottomLeftCorner Height="9px" Width="9px" />
            </dxrp:ASPxRoundPanel>
            <asp:Label ID="rpbr7" runat="server" Text="<br />"></asp:Label>
            <dxrp:ASPxRoundPanel ID="Aspxroundpanel7" runat="server" BackColor="#DDECFE" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                CssPostfix="Office2003Blue" EnableDefaultAppearance="False" HeaderText="7 - [old] - not shown"
                Width="900px" Visible="False" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                <TopEdge>
                    <BackgroundImage HorizontalPosition="left" ImageUrl="~/App_Themes/Office2003Blue/Web/rpTopEdge.png"
                        Repeat="RepeatX" VerticalPosition="top" />
                </TopEdge>
                <BottomRightCorner Height="9px" Width="9px" />
                <ContentPaddings Padding="2px" PaddingBottom="10px" PaddingTop="10px" />
                <NoHeaderTopRightCorner Height="9px" Width="9px" />
                <HeaderRightEdge>
                    <BackgroundImage HorizontalPosition="left" ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png"
                        Repeat="RepeatX" VerticalPosition="top" />
                </HeaderRightEdge>
                <Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px" />
                <HeaderLeftEdge>
                    <BackgroundImage HorizontalPosition="left" ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png"
                        Repeat="RepeatX" VerticalPosition="top" />
                </HeaderLeftEdge>
                <HeaderStyle BackColor="#7BA4E0">
                    <Paddings Padding="0px" PaddingBottom="7px" PaddingLeft="2px" PaddingRight="2px" />
                    <BorderBottom BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px" />
                </HeaderStyle>
                <TopRightCorner Height="9px" Width="9px" />
                <HeaderContent>
                    <BackgroundImage HorizontalPosition="left" ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png"
                        Repeat="RepeatX" VerticalPosition="top" />
                </HeaderContent>
                <DisabledStyle ForeColor="Gray">
                </DisabledStyle>
                <NoHeaderTopEdge BackColor="#DDECFE">
                </NoHeaderTopEdge>
                <NoHeaderTopLeftCorner Height="9px" Width="9px" />
                <PanelCollection>
                    <dxp:PanelContent runat="server">
                        <table style="width: 100%" cellspacing="0" cellpadding="0" border="0">
                            <tbody>
                                <tr>
                                    <td style="height: 19px">Level of Supervision from Alcoa (Direct or Indirect):
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height: 14px">
                                        <em>This determines the level of supervision required to manage the contractor and has
                                            a direct impact on the types of questions they will need to answer as part of the
                                            pre qualification process.</em>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height: 14px"></td>
                                </tr>
                                <tr>
                                    <td style="height: 25px">
                                        <dxe:ASPxComboBox ID="qAnswer7" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                            CssPostfix="Office2003Blue" IncrementalFilteringMode="StartsWith" ValueType="System.String"
                                            ClientInstanceName="qAnswer7" SelectedIndex="0" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                            <Items>
                                                <dxe:ListEditItem Text="Direct" Value="Direct" />
                                                <dxe:ListEditItem Text="InDirect" Value="InDirect" />
                                            </Items>
                                            <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                                            </LoadingPanelImage>
                                            <ButtonStyle Width="13px">
                                            </ButtonStyle>
                                            <ValidationSettings ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True" ValidationGroup="Submit">
                                                <RequiredField IsRequired="True" />
                                                <RequiredField IsRequired="True"></RequiredField>
                                            </ValidationSettings>
                                        </dxe:ASPxComboBox>
                                        &nbsp;
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </dxp:PanelContent>
                </PanelCollection>
                <TopLeftCorner Height="9px" Width="9px" />
                <BottomLeftCorner Height="9px" Width="9px" />
            </dxrp:ASPxRoundPanel>
            <asp:Label ID="rpbr8" runat="server" Text="<br />"></asp:Label>
            <dxrp:ASPxRoundPanel ID="Aspxroundpanel8" runat="server" BackColor="#DDECFE" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                CssPostfix="Office2003Blue" EnableDefaultAppearance="False" HeaderText="7" Width="900px"
                SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                <TopEdge>
                    <BackgroundImage HorizontalPosition="left" ImageUrl="~/App_Themes/Office2003Blue/Web/rpTopEdge.png"
                        Repeat="RepeatX" VerticalPosition="top" />
                </TopEdge>
                <BottomRightCorner Height="9px" Width="9px" />
                <ContentPaddings Padding="2px" PaddingBottom="10px" PaddingTop="10px" />
                <NoHeaderTopRightCorner Height="9px" Width="9px" />
                <HeaderRightEdge>
                    <BackgroundImage HorizontalPosition="left" ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png"
                        Repeat="RepeatX" VerticalPosition="top" />
                </HeaderRightEdge>
                <Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px" />
                <HeaderLeftEdge>
                    <BackgroundImage HorizontalPosition="left" ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png"
                        Repeat="RepeatX" VerticalPosition="top" />
                </HeaderLeftEdge>
                <HeaderStyle BackColor="#7BA4E0">
                    <Paddings Padding="0px" PaddingBottom="7px" PaddingLeft="2px" PaddingRight="2px" />
                    <BorderBottom BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px" />
                </HeaderStyle>
                <TopRightCorner Height="9px" Width="9px" />
                <HeaderContent>
                    <BackgroundImage HorizontalPosition="left" ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png"
                        Repeat="RepeatX" VerticalPosition="top" />
                </HeaderContent>
                <DisabledStyle ForeColor="Gray">
                </DisabledStyle>
                <NoHeaderTopEdge BackColor="#DDECFE">
                </NoHeaderTopEdge>
                <NoHeaderTopLeftCorner Height="9px" Width="9px" />
                <PanelCollection>
                    <dxp:PanelContent runat="server">
                        <table style="width: 100%" cellspacing="0" cellpadding="0" border="0">
                            <tbody>
                                <tr>
                                    <td style="height: 19px">When is Service required?
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height: 14px">
                                        <em>Determines when pre qualification needs to be completed by and is critical in maintaining
                                            maintenance and production schedules.</em>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height: 14px"></td>
                                </tr>
                                <tr>
                                    <td style="height: 25px">
                                        <dxe:ASPxDateEdit ID="qAnswer8" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                            CssPostfix="Office2003Blue" EditFormat="Custom" EditFormatString="dd/MM/yyyy"
                                            SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                            <ButtonStyle Width="13px">
                                            </ButtonStyle>
                                            <ValidationSettings ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True" ValidationGroup="Submit"
                                                RequiredField-ErrorText="Question 7 - Date of Service Requirement is required">
                                                <RequiredField IsRequired="True" />
                                                <RequiredField IsRequired="True"></RequiredField>
                                            </ValidationSettings>
                                        </dxe:ASPxDateEdit>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </dxp:PanelContent>
                </PanelCollection>
                <TopLeftCorner Height="9px" Width="9px" />
                <BottomLeftCorner Height="9px" Width="9px" />
            </dxrp:ASPxRoundPanel>
            <asp:Label ID="rpbr9" runat="server" Text="<br />"></asp:Label>
            <dxrp:ASPxRoundPanel ID="Aspxroundpanel9" runat="server" BackColor="#DDECFE" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                CssPostfix="Office2003Blue" EnableDefaultAppearance="False" HeaderText="8" Visible="False"
                Width="900px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                <TopEdge>
                    <BackgroundImage HorizontalPosition="left" ImageUrl="~/App_Themes/Office2003Blue/Web/rpTopEdge.png"
                        Repeat="RepeatX" VerticalPosition="top" />
                </TopEdge>
                <BottomRightCorner Height="9px" Width="9px" />
                <ContentPaddings Padding="2px" PaddingBottom="10px" PaddingTop="10px" />
                <NoHeaderTopRightCorner Height="9px" Width="9px" />
                <HeaderRightEdge>
                    <BackgroundImage HorizontalPosition="left" ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png"
                        Repeat="RepeatX" VerticalPosition="top" />
                </HeaderRightEdge>
                <Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px" />
                <HeaderLeftEdge>
                    <BackgroundImage HorizontalPosition="left" ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png"
                        Repeat="RepeatX" VerticalPosition="top" />
                </HeaderLeftEdge>
                <HeaderStyle BackColor="#7BA4E0">
                    <Paddings Padding="0px" PaddingBottom="7px" PaddingLeft="2px" PaddingRight="2px" />
                    <BorderBottom BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px" />
                </HeaderStyle>
                <TopRightCorner Height="9px" Width="9px" />
                <HeaderContent>
                    <BackgroundImage HorizontalPosition="left" ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png"
                        Repeat="RepeatX" VerticalPosition="top" />
                </HeaderContent>
                <DisabledStyle ForeColor="Gray">
                </DisabledStyle>
                <NoHeaderTopEdge BackColor="#DDECFE">
                </NoHeaderTopEdge>
                <NoHeaderTopLeftCorner Height="9px" Width="9px" />
                <PanelCollection>
                    <dxp:PanelContent runat="server">
                        <table style="width: 100%" cellspacing="0" cellpadding="0" border="0">
                            <tbody>
                                <tr>
                                    <td style="height: 19px">Direct Contract or Sub-contract?
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height: 14px">
                                        <em>Determines who has responsibility for the contractor.</em>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height: 14px"></td>
                                </tr>
                                <tr>
                                    <td style="height: 25px">
                                        <dxe:ASPxComboBox ID="qAnswer9" ClientInstanceName="qAnswer9" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                            CssPostfix="Office2003Blue" IncrementalFilteringMode="StartsWith" ValueType="System.String"
                                            EnableCallbackMode="True" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                            <Items>
                                                <dxe:ListEditItem Text="Direct Contract" Value="Direct Contract" />
                                                <dxe:ListEditItem Text="Sub-Contract" Value="Sub-Contract" />
                                            </Items>
                                            <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                                            </LoadingPanelImage>
                                            <ButtonStyle Width="13px">
                                            </ButtonStyle>
                                            <ValidationSettings ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True" ValidationGroup="Submit">
                                                <RequiredField IsRequired="True" />
                                                <RequiredField IsRequired="True"></RequiredField>
                                            </ValidationSettings>
                                            <ClientSideEvents SelectedIndexChanged="function(s, e) {
 	if(s.GetValue() == 'Sub-Contract')
 	{
 		qAnswer9_1.SetEnabled(true);
  		qAnswer9_1.SetFocus();
 	}
  	else
 	{
 		qAnswer9_1.SetEnabled(false);
		qAnswer9_1.SetIsValid(true);
 	}
}"></ClientSideEvents>
                                        </dxe:ASPxComboBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height: 25px">
                                        <br />
                                        <br />
                                        <dxe:ASPxLabel ID="ASPxLabel1" runat="server" Text="Primary Contractor (only required if you select 'Sub-Contract'):">
                                        </dxe:ASPxLabel>
                                        <br />
                                        <dxe:ASPxComboBox ID="qAnswer9_1" ClientInstanceName="qAnswer9_1" runat="server"
                                            CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue"
                                            IncrementalFilteringMode="StartsWith" ValueType="System.Int32" Width="213px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                            <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                                            </LoadingPanelImage>
                                            <ButtonStyle Width="13px">
                                            </ButtonStyle>
                                            <ValidationSettings ErrorDisplayMode="ImageWithTooltip" ValidationGroup="Submit"
                                                ValidateOnLeave="False">
                                            </ValidationSettings>
                                        </dxe:ASPxComboBox>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </dxp:PanelContent>
                </PanelCollection>
                <TopLeftCorner Height="9px" Width="9px" />
                <BottomLeftCorner Height="9px" Width="9px" />
            </dxrp:ASPxRoundPanel>
            <asp:Label ID="rpbr11" runat="server" Text="<br />"></asp:Label>
            <dxrp:ASPxRoundPanel ID="Aspxroundpanel11" runat="server" BackColor="#DDECFE" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                CssPostfix="Office2003Blue" EnableDefaultAppearance="False" HeaderText="8" Width="900px"
                SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                <TopEdge>
                    <BackgroundImage HorizontalPosition="left" ImageUrl="~/App_Themes/Office2003Blue/Web/rpTopEdge.png"
                        Repeat="RepeatX" VerticalPosition="top" />
                </TopEdge>
                <BottomRightCorner Height="9px" Width="9px" />
                <ContentPaddings Padding="2px" PaddingBottom="10px" PaddingTop="10px" />
                <NoHeaderTopRightCorner Height="9px" Width="9px" />
                <HeaderRightEdge>
                    <BackgroundImage HorizontalPosition="left" ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png"
                        Repeat="RepeatX" VerticalPosition="top" />
                </HeaderRightEdge>
                <Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px" />
                <HeaderLeftEdge>
                    <BackgroundImage HorizontalPosition="left" ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png"
                        Repeat="RepeatX" VerticalPosition="top" />
                </HeaderLeftEdge>
                <HeaderStyle BackColor="#7BA4E0">
                    <Paddings Padding="0px" PaddingBottom="7px" PaddingLeft="2px" PaddingRight="2px" />
                    <BorderBottom BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px" />
                </HeaderStyle>
                <TopRightCorner Height="9px" Width="9px" />
                <HeaderContent>
                    <BackgroundImage HorizontalPosition="left" ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png"
                        Repeat="RepeatX" VerticalPosition="top" />
                </HeaderContent>
                <DisabledStyle ForeColor="Gray">
                </DisabledStyle>
                <NoHeaderTopEdge BackColor="#DDECFE">
                </NoHeaderTopEdge>
                <NoHeaderTopLeftCorner Height="9px" Width="9px" />
                <PanelCollection>
                    <dxp:PanelContent runat="server">
                        <span style="font-size: 11pt; font-family: Arial">
                            <table border="0" cellpadding="0" cellspacing="0" style="font-size: 11pt; font-family: Arial">
                                <tr>
                                    <td bgcolor="#660000">
                                        <table border="0" cellpadding="4" cellspacing="1">
                                            <tr>
                                                <td bgcolor="#fffff0" colspan="3" style="height: 14px">
                                                    <span style="background-color: #fffff0"><strong>Specific Contractor Category </strong>
                                                        (Determines if Pre-Qualification is Required):</span>
                                                </td>
                                                <td bgcolor="#fffff0" style="width: 257px; height: 14px; background-color: #fffff0">
                                                    <span style="background-color: #fffff0"><strong>Examples of "Yes" Response</strong></span>
                                                </td>
                                            </tr>
                                            <tr style="background-color: #fffff0">
                                                <td bgcolor="#fffff0" style="width: 142px">1) Visitor
                                                </td>
                                                <td bgcolor="#fffff0" style="width: 88px; text-align: center">
                                                    <dxe:ASPxRadioButtonList ID="rbl2_7" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                        CssPostfix="Office2003Blue" Font-Bold="True" ItemSpacing="10px" RepeatDirection="Horizontal"
                                                        Width="100px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                        TextSpacing="10px">
                                                        <ClientSideEvents SelectedIndexChanged="function(s, e) {
                                                                if(s.GetValue() == 'Yes' && MoreThanOneCategorySelected()) {
                                                                    alert('Selection Cancelled.\nOnly One Category can be selected As [Yes].');
                                                                    ctl00_body_Questionnaire1Procurement1_Aspxroundpanel11_rbl2_7.SetValue(null);
                                                                }
                                                                if(s.GetValue() == 'No' && AllCategoriesNo()) {
                                                                    alert('Selection Cancelled.\nOne Category must be selected As [Yes].');
                                                                    ctl00_body_Questionnaire1Procurement1_Aspxroundpanel11_rbl2_7.SetValue(null);
                                                                }
                                                                CalculateIfPqRequired();
                                                        }" />
                                                        <Items>
                                                            <dxe:ListEditItem Text="Yes" Value="Yes" />
                                                            <dxe:ListEditItem Text="No" Value="No" />
                                                        </Items>
                                                        <ValidationSettings ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True" ValidationGroup="Submit"
                                                            RequiredField-ErrorText="Question 8.7 - answer is required">
                                                            <RequiredField IsRequired="True" />
                                                        </ValidationSettings>
                                                    </dxe:ASPxRadioButtonList>
                                                </td>
                                                <td bgcolor="#fffff0" style="width: 126px; text-align: center">
                                                    <dxe:ASPxLabel ID="lbl2_7" runat="server" Font-Bold="True" Text="-" Width="102px">
                                                    </dxe:ASPxLabel>
                                                </td>
                                                <td bgcolor="#fffff0" rowspan="1" style="width: 397px">Person(s) not contracted for work at Alcoa Operations. The person is not performing work and is escorted 100% of the time by a fully inducted person.
                                                </td>
                                            </tr>
                                            <tr>
                                                <td bgcolor="#fffff0" style="width: 142px">2) Delivery Driver 1
                                                </td>
                                                <td bgcolor="#fffff0" style="width: 88px; text-align: center">
                                                    <dxe:ASPxRadioButtonList ID="rbl2_8" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                        CssPostfix="Office2003Blue" Font-Bold="True" ItemSpacing="10px" RepeatDirection="Horizontal"
                                                        Width="100px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                        TextSpacing="10px">
                                                        <ClientSideEvents SelectedIndexChanged="function(s, e) {
                                                                if(s.GetValue() == 'Yes' && MoreThanOneCategorySelected()) {
                                                                    alert('Selection Cancelled.\nOnly One Category can be selected As [Yes].');
                                                                    ctl00_body_Questionnaire1Procurement1_Aspxroundpanel11_rbl2_8.SetValue(null);
                                                                }
                                                                if(s.GetValue() == 'No' && AllCategoriesNo()) {
                                                                    alert('Selection Cancelled.\nOne Category must be selected As [Yes].');
                                                                    ctl00_body_Questionnaire1Procurement1_Aspxroundpanel11_rbl2_8.SetValue(null);
                                                                }
                                                                CalculateIfPqRequired();
                                                        }" />
                                                        <Items>
                                                            <dxe:ListEditItem Text="Yes" Value="Yes" />
                                                            <dxe:ListEditItem Text="No" Value="No" />
                                                        </Items>
                                                        <ValidationSettings ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True" ValidationGroup="Submit"
                                                            RequiredField-ErrorText="Question 8.8 - answer is required">
                                                            <RequiredField IsRequired="True" />
                                                        </ValidationSettings>
                                                    </dxe:ASPxRadioButtonList>
                                                </td>
                                                <td bgcolor="#fffff0" style="width: 126px; text-align: center">
                                                    <dxe:ASPxLabel ID="lbl2_8" runat="server" Font-Bold="True" Text="-" Width="102px">
                                                    </dxe:ASPxLabel>
                                                </td>
                                                <td bgcolor="#fffff0" rowspan="1" style="width: 397px">A Delivery Driver whose visits are frequent/ infrequent does not have access to the process areas and is not involved in working whilst unloading/loading the material.<br />
                                                    <br />
                                                    Exempts suppliers that only supply goods and deliver to location nominated store location only. (This covers delivery couriers delivering to stores).
                                                    <br />
                                                    <br />
                                                    For delivery of over size loads directly to a location of installation, must be 100% escorted by an individual with current site induction.
                                                </td>
                                            </tr>
                                            <tr>
                                                <td bgcolor="#fffff0" style="width: 142px">3) Delivery Driver 2
                                                </td>
                                                <td bgcolor="#fffff0" style="width: 88px; text-align: center">
                                                    <dxe:ASPxRadioButtonList ID="rbl2_9" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                        CssPostfix="Office2003Blue" Font-Bold="True" ItemSpacing="10px" RepeatDirection="Horizontal"
                                                        SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" TextSpacing="10px">
                                                        <ClientSideEvents SelectedIndexChanged="function(s, e) {
                                                                if(s.GetValue() == 'Yes' && MoreThanOneCategorySelected()) {
                                                                    alert('Selection Cancelled.\nOnly One Category can be selected As [Yes].');
                                                                    ctl00_body_Questionnaire1Procurement1_Aspxroundpanel11_rbl2_9.SetValue(null);
                                                                }
                                                                if(s.GetValue() == 'No' && AllCategoriesNo()) {
                                                                    alert('Selection Cancelled.\nOne Category must be selected As [Yes].');
                                                                    ctl00_body_Questionnaire1Procurement1_Aspxroundpanel11_rbl2_9.SetValue(null);
                                                                }
                                                                CalculateIfPqRequired();
                                                        }" />
                                                        <Items>
                                                            <dxe:ListEditItem Text="Yes" Value="Yes" />
                                                            <dxe:ListEditItem Text="No" Value="No" />
                                                        </Items>
                                                        <ValidationSettings ErrorDisplayMode="ImageWithTooltip" ValidationGroup="Submit"
                                                            RequiredField-ErrorText="Question 8.9 - answer is required">
                                                            <RequiredField IsRequired="True" />
                                                        </ValidationSettings>
                                                    </dxe:ASPxRadioButtonList>
                                                </td>
                                                <td bgcolor="#fffff0" style="width: 126px; text-align: center">
                                                    <dxe:ASPxLabel ID="lbl2_9" runat="server" Font-Bold="True" Text="-" Width="102px">
                                                    </dxe:ASPxLabel>
                                                </td>
                                                <td bgcolor="#fffff0" rowspan="1" style="width: 397px">Will you be required to access an area on an Alcoa location other than the nominated location stores delivery point , canteen or office area unescorted without traversing through the location. Will you be required to exit vehicle to release, remove or restrain loads other than in a nominated location stores.
                                                </td>
                                            </tr>
                                            <tr>
                                                <td bgcolor="#fffff0" style="width: 142px">4) Technical Professional (Office Work Only)
                                                </td>
                                                <td bgcolor="#fffff0" style="width: 88px; text-align: center">
                                                    <dxe:ASPxRadioButtonList ID="rbl2_10" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                        CssPostfix="Office2003Blue" Font-Bold="True" ItemSpacing="10px" RepeatDirection="Horizontal"
                                                        SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" TextSpacing="10px">
                                                        <ClientSideEvents SelectedIndexChanged="function(s, e) {
                                                                if(s.GetValue() == 'Yes' && MoreThanOneCategorySelected()) {
                                                                    alert('Selection Cancelled.\nOnly One Category can be selected As [Yes].');
                                                                    ctl00_body_Questionnaire1Procurement1_Aspxroundpanel11_rbl2_10.SetValue(null);
                                                                }
                                                                if(s.GetValue() == 'No' && AllCategoriesNo()) {
                                                                    alert('Selection Cancelled.\nOne Category must be selected As [Yes].');
                                                                    ctl00_body_Questionnaire1Procurement1_Aspxroundpanel11_rbl2_10.SetValue(null);
                                                                }
                                                                CalculateIfPqRequired();
                                                        }" />
                                                        <Items>
                                                            <dxe:ListEditItem Text="Yes" Value="Yes" />
                                                            <dxe:ListEditItem Text="No" Value="No" />
                                                        </Items>
                                                        <ValidationSettings ErrorDisplayMode="ImageWithTooltip" ValidationGroup="Submit"
                                                            RequiredField-ErrorText="Question 8.10 - answer is required">
                                                            <RequiredField IsRequired="True" />
                                                        </ValidationSettings>
                                                    </dxe:ASPxRadioButtonList>
                                                </td>
                                                <td bgcolor="#fffff0" style="width: 126px; text-align: center">
                                                    <dxe:ASPxLabel ID="lbl2_10" runat="server" Font-Bold="True" Text="-" Width="102px">
                                                    </dxe:ASPxLabel>
                                                </td>
                                                <td bgcolor="#fffff0" rowspan="1" style="width: 397px">Office Only (Generally meets the requirements of directly supervised contracted services.).
                                                    <br />
                                                    Person(s) contracted for office work. Reaching the work site may require unescorted presence in the Operations but work is basically office tasks.
                                                    <br />
                                                    <br />
                                                    Includes commissioning in an controlled environment with 100% supervision by Alcoa technical specialist.
                                                </td>
                                            </tr>
                                            <tr>
                                                <td bgcolor="#fffff0" style="width: 142px">5) Technical Consultant (Site Work)
                                                </td>
                                                <td bgcolor="#fffff0" style="width: 88px; text-align: center">
                                                    <dxe:ASPxRadioButtonList ID="rbl2_11" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                        CssPostfix="Office2003Blue" Font-Bold="True" ItemSpacing="10px" RepeatDirection="Horizontal"
                                                        SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" TextSpacing="10px">
                                                        <ClientSideEvents SelectedIndexChanged="function(s, e) {
                                                                if(s.GetValue() == 'Yes' && MoreThanOneCategorySelected()) {
                                                                    alert('Selection Cancelled.\nOnly One Category can be selected As [Yes].');
                                                                    ctl00_body_Questionnaire1Procurement1_Aspxroundpanel11_rbl2_11.SetValue(null);
                                                                }
                                                                if(s.GetValue() == 'No' && AllCategoriesNo()) {
                                                                    alert('Selection Cancelled.\nOne Category must be selected As [Yes].');
                                                                    ctl00_body_Questionnaire1Procurement1_Aspxroundpanel11_rbl2_11.SetValue(null);
                                                                }
                                                                CalculateIfPqRequired();
                                                        }" />
                                                        <Items>
                                                            <dxe:ListEditItem Text="Yes" Value="Yes" />
                                                            <dxe:ListEditItem Text="No" Value="No" />
                                                        </Items>
                                                        <ValidationSettings ErrorDisplayMode="ImageWithTooltip" ValidationGroup="Submit"
                                                            RequiredField-ErrorText="Question 8.11 - answer is required">
                                                            <RequiredField IsRequired="True" />
                                                        </ValidationSettings>
                                                    </dxe:ASPxRadioButtonList>
                                                </td>
                                                <td bgcolor="#fffff0" style="width: 126px; text-align: center">
                                                    <dxe:ASPxLabel ID="lbl2_11" runat="server" Font-Bold="True" Text="-" Width="102px">
                                                    </dxe:ASPxLabel>
                                                </td>
                                                <td bgcolor="#fffff0" rowspan="1" style="width: 397px">(Some meet the requirements of directly supervised contracted services.).
                                                    <br />
                                                    Person(s) contracted for work in or on the production or maintenance processes and/or technical person contracted to perform work. Exposure to Operations elements is expected and/or routine. Minor hand tool usage.
                                                </td>
                                            </tr>
                                            <tr>
                                                <td bgcolor="#fffff0" style="width: 142px">6) Other
                                                </td>
                                                <td bgcolor="#fffff0" style="width: 88px; text-align: center">
                                                    <dxe:ASPxRadioButtonList ID="rbl2_12" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                        CssPostfix="Office2003Blue" Font-Bold="True" ItemSpacing="10px" RepeatDirection="Horizontal"
                                                        SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" TextSpacing="10px">
                                                        <ClientSideEvents SelectedIndexChanged="function(s, e) {
                                                                if(s.GetValue() == 'Yes' && MoreThanOneCategorySelected()) {
                                                                    alert('Selection Cancelled.\nOnly One Category can be selected As [Yes].');
                                                                    ctl00_body_Questionnaire1Procurement1_Aspxroundpanel11_rbl2_12.SetValue(null);
                                                                }
                                                                if(s.GetValue() == 'No' && AllCategoriesNo()) {
                                                                    alert('Selection Cancelled.\nOne Category must be selected As [Yes].');
                                                                    ctl00_body_Questionnaire1Procurement1_Aspxroundpanel11_rbl2_12.SetValue(null);
                                                                }
                                                                CalculateIfPqRequired();
                                                        }" />
                                                        <Items>
                                                            <dxe:ListEditItem Text="Yes" Value="Yes" />
                                                            <dxe:ListEditItem Text="No" Value="No" />
                                                        </Items>
                                                        <ValidationSettings ErrorDisplayMode="ImageWithTooltip" ValidationGroup="Submit"
                                                            RequiredField-ErrorText="Question 8.12 - answer is required">
                                                            <RequiredField IsRequired="True" />
                                                        </ValidationSettings>
                                                    </dxe:ASPxRadioButtonList>
                                                </td>
                                                <td bgcolor="#fffff0" style="width: 126px; text-align: center">
                                                    <dxe:ASPxLabel ID="lbl2_12" runat="server" Font-Bold="True" Text="n/a" Width="102px">
                                                    </dxe:ASPxLabel>
                                                </td>
                                                <td bgcolor="#fffff0" rowspan="1" style="width: 397px">Contractor falls into a category other than the ones specified above.
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                            <br />
                            <table border="0" id="tblPreQualRequirement" runat="server" cellpadding="0" cellspacing="0" style="font-size: 11pt; font-family: Arial">
                                <tr>
                                    <td bgcolor="#660000">
                                        <table border="0" cellpadding="4" cellspacing="1">
                                            <tr>
                                                <td bgcolor="#fffff0" colspan="3" style="height: 14px">
                                                    <span style="background-color: #fffff0"><strong>Pre-Qualification Requirement:</strong></span>
                                                </td>
                                                <td bgcolor="#fffff0" style="width: 257px; height: 14px; background-color: #fffff0">
                                                    <span style="background-color: #fffff0"><strong>Examples of "Yes" Response</strong></span>
                                                </td>
                                            </tr>
                                            <tr style="background-color: #fffff0">
                                                <td bgcolor="#fffff0" style="width: 282px">Will all work completed by the contractor/subcontractor be completed off-site?
                                                </td>
                                                <td bgcolor="#fffff0" style="width: 88px; text-align: center">
                                                    <dxe:ASPxRadioButtonList ID="rbl2_1" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                        CssPostfix="Office2003Blue" Font-Bold="True" ItemSpacing="10px" RepeatDirection="Horizontal"
                                                        Width="100px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                        TextSpacing="10px">
                                                        <ClientSideEvents SelectedIndexChanged="function(s, e) {
                                                                CalculateIfPqRequired();
                                                        }" />
                                                        <Items>
                                                            <dxe:ListEditItem Text="Yes" Value="Yes" />
                                                            <dxe:ListEditItem Text="No" Value="No" />
                                                        </Items>
                                                        <ValidationSettings ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True" ValidationGroup="Submit"
                                                            RequiredField-ErrorText="Question 8.1 - answer is required">
                                                            <RequiredField IsRequired="True" />
                                                        </ValidationSettings>
                                                    </dxe:ASPxRadioButtonList>
                                                </td>
                                                <td bgcolor="#fffff0" style="width: 126px; text-align: center">
                                                    <dxe:ASPxLabel ID="lbl2_1" runat="server" Font-Bold="True" Text="-" Width="102px">
                                                    </dxe:ASPxLabel>
                                                </td>
                                                <td bgcolor="#fffff0" rowspan="1" style="width: 257px">Off-site fabrication, repair or servicing workshops.
                                                </td>
                                            </tr>
                                            <tr>
                                                <td bgcolor="#fffff0" style="width: 282px">Is the work completed within a defined and restricted area or travelling along a
                                                    defined and restricted route?
                                                </td>
                                                <td bgcolor="#fffff0" style="width: 88px; text-align: center">
                                                    <dxe:ASPxRadioButtonList ID="rbl2_2" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                        CssPostfix="Office2003Blue" Font-Bold="True" ItemSpacing="10px" RepeatDirection="Horizontal"
                                                        Width="100px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                        TextSpacing="10px">
                                                        <ClientSideEvents SelectedIndexChanged="function(s, e) {
                                                                CalculateIfPqRequired();
                                                        }" />
                                                        <Items>
                                                            <dxe:ListEditItem Text="Yes" Value="Yes" />
                                                            <dxe:ListEditItem Text="No" Value="No" />
                                                        </Items>
                                                        <ValidationSettings ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True" ValidationGroup="Submit"
                                                            RequiredField-ErrorText="Question 8.2 - answer is required">
                                                            <RequiredField IsRequired="True" />
                                                        </ValidationSettings>
                                                    </dxe:ASPxRadioButtonList>
                                                </td>
                                                <td bgcolor="#fffff0" style="width: 126px; text-align: center">
                                                    <dxe:ASPxLabel ID="lbl2_2" runat="server" Font-Bold="True" Text="-" Width="102px">
                                                    </dxe:ASPxLabel>
                                                </td>
                                                <td bgcolor="#fffff0" rowspan="1" style="width: 257px">Delivers to set points, mini-store restocking. Office based administrative work.
                                                    Professional support.
                                                </td>
                                            </tr>
                                            <tr>
                                                <td bgcolor="#fffff0" style="width: 282px">Does the work use, generate or expose employees to hazardous materials or chemicals?
                                                </td>
                                                <td bgcolor="#fffff0" style="width: 88px; text-align: center">
                                                    <dxe:ASPxRadioButtonList ID="rbl2_3" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                        CssPostfix="Office2003Blue" Font-Bold="True" ItemSpacing="10px" RepeatDirection="Horizontal"
                                                        SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" TextSpacing="10px">
                                                        <ClientSideEvents SelectedIndexChanged="function(s, e) {
                                                                CalculateIfPqRequired();
                                                        }" />
                                                        <Items>
                                                            <dxe:ListEditItem Text="Yes" Value="Yes" />
                                                            <dxe:ListEditItem Text="No" Value="No" />
                                                        </Items>
                                                        <ValidationSettings ErrorDisplayMode="ImageWithTooltip" ValidationGroup="Submit"
                                                            RequiredField-ErrorText="Question 8.3 - answer is required">
                                                            <RequiredField IsRequired="True" />
                                                        </ValidationSettings>
                                                    </dxe:ASPxRadioButtonList>
                                                </td>
                                                <td bgcolor="#fffff0" style="width: 126px; text-align: center">
                                                    <dxe:ASPxLabel ID="lbl2_3" runat="server" Font-Bold="True" Text="-" Width="102px">
                                                    </dxe:ASPxLabel>
                                                </td>
                                                <td bgcolor="#fffff0" rowspan="1" style="width: 257px">Handling of hazardous materials such as flammable, corrosive or biological agents.
                                                </td>
                                            </tr>
                                            <tr>
                                                <td bgcolor="#fffff0" style="width: 282px">Does the work use tools and equipment which could create a significant hazard to
                                                    themselves or others?
                                                </td>
                                                <td bgcolor="#fffff0" style="width: 88px; text-align: center">
                                                    <dxe:ASPxRadioButtonList ID="rbl2_4" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                        CssPostfix="Office2003Blue" Font-Bold="True" ItemSpacing="10px" RepeatDirection="Horizontal"
                                                        SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" TextSpacing="10px">
                                                        <ClientSideEvents SelectedIndexChanged="function(s, e) {
                                                                CalculateIfPqRequired();
                                                        }" />
                                                        <Items>
                                                            <dxe:ListEditItem Text="Yes" Value="Yes" />
                                                            <dxe:ListEditItem Text="No" Value="No" />
                                                        </Items>
                                                        <ValidationSettings ErrorDisplayMode="ImageWithTooltip" ValidationGroup="Submit"
                                                            RequiredField-ErrorText="Question 8.4 - answer is required">
                                                            <RequiredField IsRequired="True" />
                                                        </ValidationSettings>
                                                    </dxe:ASPxRadioButtonList>
                                                </td>
                                                <td bgcolor="#fffff0" style="width: 126px; text-align: center">
                                                    <dxe:ASPxLabel ID="lbl2_4" runat="server" Font-Bold="True" Text="-" Width="102px">
                                                    </dxe:ASPxLabel>
                                                </td>
                                                <td bgcolor="#fffff0" rowspan="1" style="width: 257px">Use of high powered tools, angle grinders, hydraulic rams.
                                                </td>
                                            </tr>
                                            <tr>
                                                <td bgcolor="#fffff0" style="width: 282px">Does the work require advanced layers of protection as safeguards from potential
                                                    safety and health hazards?
                                                </td>
                                                <td bgcolor="#fffff0" style="width: 88px; text-align: center">
                                                    <dxe:ASPxRadioButtonList ID="rbl2_5" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                        CssPostfix="Office2003Blue" Font-Bold="True" ItemSpacing="10px" RepeatDirection="Horizontal"
                                                        SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" TextSpacing="10px">
                                                        <ClientSideEvents SelectedIndexChanged="function(s, e) {
                                                                CalculateIfPqRequired();
                                                        }" />
                                                        <Items>
                                                            <dxe:ListEditItem Text="Yes" Value="Yes" />
                                                            <dxe:ListEditItem Text="No" Value="No" />
                                                        </Items>
                                                        <ValidationSettings ErrorDisplayMode="ImageWithTooltip" ValidationGroup="Submit"
                                                            RequiredField-ErrorText="Question 8.5 - answer is required">
                                                            <RequiredField IsRequired="True" />
                                                        </ValidationSettings>
                                                    </dxe:ASPxRadioButtonList>
                                                </td>
                                                <td bgcolor="#fffff0" style="width: 126px; text-align: center">
                                                    <dxe:ASPxLabel ID="lbl2_5" runat="server" Font-Bold="True" Text="-" Width="102px">
                                                    </dxe:ASPxLabel>
                                                </td>
                                                <td bgcolor="#fffff0" rowspan="1" style="width: 257px">Requires use of LOTO, Working at Heights, Confined Space Entry, HV Work. Exposure
                                                    to Chemical SEGs.
                                                </td>
                                            </tr>
                                            <tr>
                                                <td bgcolor="#fffff0" style="width: 282px">Works on-site within defined/restricted routes with no exposure to significant hazards
                                                    and does not require advanced layers of protection?
                                                </td>
                                                <td bgcolor="#fffff0" style="width: 88px; text-align: center">
                                                    <dxe:ASPxRadioButtonList ID="rbl2_6" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                        CssPostfix="Office2003Blue" Font-Bold="True" ItemSpacing="10px" RepeatDirection="Horizontal"
                                                        SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" TextSpacing="10px">
                                                        <ClientSideEvents SelectedIndexChanged="function(s, e) {
                                                                CalculateIfPqRequired();
                                                        }" />
                                                        <Items>
                                                            <dxe:ListEditItem Text="Yes" Value="Yes" />
                                                            <dxe:ListEditItem Text="No" Value="No" />
                                                        </Items>
                                                        <ValidationSettings ErrorDisplayMode="ImageWithTooltip" ValidationGroup="Submit"
                                                            RequiredField-ErrorText="Question 8.6 - answer is required">
                                                            <RequiredField IsRequired="True" />
                                                        </ValidationSettings>
                                                    </dxe:ASPxRadioButtonList>
                                                </td>
                                                <td bgcolor="#fffff0" style="width: 126px; text-align: center">
                                                    <dxe:ASPxLabel ID="lbl2_6" runat="server" Font-Bold="True" Text="-" Width="102px">
                                                    </dxe:ASPxLabel>
                                                </td>
                                                <td bgcolor="#fffff0" rowspan="1" style="width: 257px">On-site work, within defined routes and not exposed to hazards or requiring advanced
                                                    layers of protection.
                                                </td>
                                            </tr>
                                            <tr>
                                                <td bgcolor="#fffff0" style="width: 282px">Will this organisation be directly supervising Alcoa employees?
                                                </td>
                                                <td bgcolor="#fffff0" style="width: 88px; text-align: center">
                                                    <dxe:ASPxRadioButtonList ID="rbl2_13" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                        CssPostfix="Office2003Blue" Font-Bold="True" ItemSpacing="10px" RepeatDirection="Horizontal"
                                                        SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" TextSpacing="10px">
                                                        <ClientSideEvents SelectedIndexChanged="function(s, e) {
                                                                CalculateIfPqRequired();
                                                        }" />
                                                        <Items>
                                                            <dxe:ListEditItem Text="Yes" Value="Yes" />
                                                            <dxe:ListEditItem Text="No" Value="No" />
                                                        </Items>
                                                        <ValidationSettings ErrorDisplayMode="ImageWithTooltip" ValidationGroup="Submit"
                                                            RequiredField-ErrorText="Question 8.13 - answer is required">
                                                            <RequiredField IsRequired="True" />
                                                        </ValidationSettings>
                                                    </dxe:ASPxRadioButtonList>
                                                </td>
                                                <td bgcolor="#fffff0" style="width: 126px; text-align: center">
                                                    <dxe:ASPxLabel ID="lbl2_13" runat="server" Font-Bold="True" Text="-" Width="102px">
                                                    </dxe:ASPxLabel>
                                                </td>
                                                <td bgcolor="#fffff0" rowspan="1" style="width: 257px">Work by apprentices at an external Contractor�s premises.
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                            <br />
                            <table border="0" id="tblIsPreQualRequired" runat="server" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="width: 278px; text-align: right; height: 38px;">
                                        <strong><span style="font-size: 12pt">Is Pre-Qualification Required? </span>&nbsp;</strong>
                                    </td>
                                    <td style="width: 425px; height: 38px;">
                                        <dxe:ASPxLabel ID="lbl2_Overall" runat="server" Font-Bold="True" Text="-" Width="407px"
                                            Font-Size="12pt">
                                        </dxe:ASPxLabel>
                                    </td>
                                </tr>
                            </table>
                        </span><span style="font-size: 11pt; font-family: Arial"></span>
                    </dxp:PanelContent>
                </PanelCollection>
                <TopLeftCorner Height="9px" Width="9px" />
                <BottomLeftCorner Height="9px" Width="9px" />
            </dxrp:ASPxRoundPanel>
            <asp:Label ID="rpbr10" runat="server" Text="<br />"></asp:Label>
            <dxrp:ASPxRoundPanel ID="Aspxroundpanel10" runat="server" BackColor="#DDECFE" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                CssPostfix="Office2003Blue" EnableDefaultAppearance="False" HeaderText="9" Width="900px"
                SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                <TopEdge>
                    <BackgroundImage HorizontalPosition="left" ImageUrl="~/App_Themes/Office2003Blue/Web/rpTopEdge.png"
                        Repeat="RepeatX" VerticalPosition="top" />
                </TopEdge>
                <BottomRightCorner Height="9px" Width="9px" />
                <ContentPaddings Padding="2px" PaddingBottom="10px" PaddingTop="10px" />
                <NoHeaderTopRightCorner Height="9px" Width="9px" />
                <HeaderRightEdge>
                    <BackgroundImage HorizontalPosition="left" ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png"
                        Repeat="RepeatX" VerticalPosition="top" />
                </HeaderRightEdge>
                <Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px" />
                <HeaderLeftEdge>
                    <BackgroundImage HorizontalPosition="left" ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png"
                        Repeat="RepeatX" VerticalPosition="top" />
                </HeaderLeftEdge>
                <HeaderStyle BackColor="#7BA4E0">
                    <Paddings Padding="0px" PaddingBottom="7px" PaddingLeft="2px" PaddingRight="2px" />
                    <BorderBottom BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px" />
                </HeaderStyle>
                <TopRightCorner Height="9px" Width="9px" />
                <HeaderContent>
                    <BackgroundImage HorizontalPosition="left" ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png"
                        Repeat="RepeatX" VerticalPosition="top" />
                </HeaderContent>
                <DisabledStyle ForeColor="Gray">
                </DisabledStyle>
                <NoHeaderTopEdge BackColor="#DDECFE">
                </NoHeaderTopEdge>
                <NoHeaderTopLeftCorner Height="9px" Width="9px" />
                <PanelCollection>
                    <dxp:PanelContent runat="server">
                        <table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td style="width: 197px; text-align: right">
                                    <strong><span style="font-size: 11pt; font-family: Arial">Initial Risk Assessment:</span>
                                        &nbsp;</strong>
                                </td>
                                <td style="width: 100px">
                                    <dxe:ASPxLabel ID="lblOverall" runat="server" Font-Bold="True" Text="Low Risk" Width="172px"
                                        ForeColor="DarkGreen">
                                    </dxe:ASPxLabel>

                                </td>
                            </tr>
                        </table>
                        <br />
                        <table border="0" cellspacing="0" cellpadding="0" style="font-size: 11pt; font-family: Arial">
                            <tr>
                                <td bgcolor="#660000">
                                    <table border="0" cellpadding="4" cellspacing="1">
                                        <tr>
                                            <td bgcolor="#fffff0" colspan="3" style="height: 14px">
                                                <span style="background-color: #fffff0"><strong>Contractor/Sub-Contractor Risk Levels:</strong></span>
                                            </td>
                                            <td bgcolor="#fffff0" style="width: 257px; height: 14px; background-color: #fffff0">
                                                <span style="background-color: #fffff0"><strong>Examples</strong></span>
                                            </td>
                                        </tr>
                                        <tr style="background-color: #fffff0">
                                            <td bgcolor="#fffff0" style="width: 282px">Is the contractor/supplier fully (100%) supervised or fully escorted when on site?
                                            </td>
                                            <td bgcolor="#fffff0" style="width: 88px; text-align: center">
                                                <dxe:ASPxRadioButtonList ID="rbl1" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                    CssPostfix="Office2003Blue" Font-Bold="True" ItemSpacing="10px" RepeatDirection="Horizontal"
                                                    Width="100px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                    TextSpacing="10px">
                                                    <ClientSideEvents SelectedIndexChanged="function(s, e) {
		                                                        var test = document.getElementById('ctl00_body_Questionnaire1Procurement1_Aspxroundpanel10_lbl1');
                                                                if(s.GetValue() == 'Yes')
                                                                {
                                                                    test.innerHTML = 'Low Risk';
                                                                    test.style.color = 'Green';
                                                                }
                                                                else
                                                                {
                                                                    test.innerHTML = '-';
                                                                    test.style.color = 'Black';
                                                                }
                                                                setRiskLevel();
                                                        }" />
                                                    <Items>
                                                        <dxe:ListEditItem Text="Yes" Value="Yes" />
                                                        <dxe:ListEditItem Text="No" Value="No" />
                                                    </Items>
                                                    <ValidationSettings ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True" ValidationGroup="Submit"
                                                        RequiredField-ErrorText="Question 9.1 - answer is required">
                                                        <RequiredField IsRequired="True" />
                                                    </ValidationSettings>
                                                </dxe:ASPxRadioButtonList>
                                            </td>
                                            <td bgcolor="#fffff0" style="width: 126px; text-align: center">
                                                <dxe:ASPxLabel runat="server" Text="-" Width="102px" Font-Bold="True" ID="lbl1">
                                                </dxe:ASPxLabel>
                                            </td>
                                            <td bgcolor="#fffff0" rowspan="3" style="width: 257px">Contracted services such as vending machine operators, engineering firms not working
                                                in operating areas, office administration, or office equipment repair services not
                                                requiring isolations.
                                            </td>
                                        </tr>
                                        <tr>
                                            <td bgcolor="#fffff0" style="width: 282px; height: 13px">Does the contractor/supplier have minimal exposure to production/maintenance areas?
                                            </td>
                                            <td bgcolor="#fffff0" style="width: 88px; height: 13px; text-align: center">
                                                <dxe:ASPxRadioButtonList ID="rbl2" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                    CssPostfix="Office2003Blue" Font-Bold="True" ItemSpacing="10px" RepeatDirection="Horizontal"
                                                    Width="100px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                    TextSpacing="10px">
                                                    <ClientSideEvents SelectedIndexChanged="function(s, e) {
		                                                        var test = document.getElementById('ctl00_body_Questionnaire1Procurement1_Aspxroundpanel10_lbl2');
                                                                if(s.GetValue() == 'Yes')
                                                                {
                                                                    test.innerHTML = 'Low Risk';
                                                                    test.style.color = 'Green';
                                                                }
                                                                else
                                                                {
                                                                    test.innerHTML = '-';
                                                                    test.style.color = 'Black';
                                                                }
                                                                setRiskLevel();
                                                        }" />
                                                    <Items>
                                                        <dxe:ListEditItem Text="Yes" Value="Yes" />
                                                        <dxe:ListEditItem Text="No" Value="No" />
                                                    </Items>
                                                    <ValidationSettings ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True" ValidationGroup="Submit"
                                                        RequiredField-ErrorText="Question 9.2 - answer is required">
                                                        <RequiredField IsRequired="True" />
                                                    </ValidationSettings>
                                                </dxe:ASPxRadioButtonList>
                                            </td>
                                            <td bgcolor="#fffff0" style="width: 126px; height: 13px; text-align: center">
                                                <dxe:ASPxLabel runat="server" Text="-" Width="102px" Font-Bold="True" ID="lbl2">
                                                </dxe:ASPxLabel>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td bgcolor="#fffff0" style="width: 282px; height: 13px">Does the contractor/supplier have minimal exposure to hazardous risks when working
                                                on site?
                                            </td>
                                            <td bgcolor="#fffff0" style="width: 88px; height: 13px; text-align: center">
                                                <dxe:ASPxRadioButtonList ID="rbl3" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                    CssPostfix="Office2003Blue" Font-Bold="True" ItemSpacing="10px" RepeatDirection="Horizontal"
                                                    SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" TextSpacing="10px">
                                                    <ClientSideEvents SelectedIndexChanged="function(s, e) {
		                                                        var test = document.getElementById('ctl00_body_Questionnaire1Procurement1_Aspxroundpanel10_lbl3');
                                                                if(s.GetValue() == 'Yes')
                                                                {
                                                                    test.innerHTML = 'Low Risk';
                                                                    test.style.color = 'Green';
                                                                }
                                                                else
                                                                {
                                                                    test.innerHTML = '-';
                                                                    test.style.color = 'Black';
                                                                }
                                                                setRiskLevel();
                                                        }" />
                                                    <Items>
                                                        <dxe:ListEditItem Text="Yes" Value="Yes" />
                                                        <dxe:ListEditItem Text="No" Value="No" />
                                                    </Items>
                                                    <ValidationSettings ErrorDisplayMode="ImageWithTooltip" ValidationGroup="Submit"
                                                        RequiredField-ErrorText="Question 9.3 - answer is required">
                                                        <RequiredField IsRequired="True" />
                                                    </ValidationSettings>
                                                </dxe:ASPxRadioButtonList>
                                            </td>
                                            <td bgcolor="#fffff0" style="width: 126px; height: 13px; text-align: center">
                                                <dxe:ASPxLabel runat="server" Text="-" Width="102px" Font-Bold="True" ID="lbl3">
                                                </dxe:ASPxLabel>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td bgcolor="#fffff0" style="width: 282px; height: 12px">Is the contractor on site more frequently and working with partial Alcoa supervision
                                                in production/maintenance areas?
                                            </td>
                                            <td bgcolor="#fffff0" style="width: 88px; height: 12px; text-align: center">
                                                <dxe:ASPxRadioButtonList ID="rbl4" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                    CssPostfix="Office2003Blue" Font-Bold="True" ItemSpacing="10px" RepeatDirection="Horizontal"
                                                    SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" TextSpacing="10px">
                                                    <ClientSideEvents SelectedIndexChanged="function(s, e) {
		                                                        var test = document.getElementById('ctl00_body_Questionnaire1Procurement1_Aspxroundpanel10_lbl4');
                                                                if(s.GetValue() == 'Yes')
                                                                {
                                                                    test.innerHTML = 'Medium Risk';
                                                                    test.style.color = 'Gold';
                                                                }
                                                                else
                                                                {
                                                                    test.innerHTML = '-';
                                                                    test.style.color = 'Black';
                                                                }
                                                                setRiskLevel();
                                                        }" />
                                                    <Items>
                                                        <dxe:ListEditItem Text="Yes" Value="Yes" />
                                                        <dxe:ListEditItem Text="No" Value="No" />
                                                    </Items>
                                                    <ValidationSettings ErrorDisplayMode="ImageWithTooltip" ValidationGroup="Submit"
                                                        RequiredField-ErrorText="Question 9.4 - answer is required">
                                                        <RequiredField IsRequired="True" />
                                                    </ValidationSettings>
                                                </dxe:ASPxRadioButtonList>
                                            </td>
                                            <td bgcolor="#fffff0" style="width: 126px; height: 12px; text-align: center">
                                                <dxe:ASPxLabel runat="server" Text="-" Width="102px" Font-Bold="True" ID="lbl4">
                                                </dxe:ASPxLabel>
                                            </td>
                                            <td bgcolor="#fffff0" rowspan="2" style="width: 257px">Condition monitoring services, boiling maintenance servicing, compressor servicing,
                                                other specialist servicing.
                                            </td>
                                        </tr>
                                        <tr>
                                            <td bgcolor="#fffff0" style="width: 282px; height: 13px">Is the contractor on site more frequently and working with partial Alcoa supervision
                                                and exposed to medium levels of risk?
                                            </td>
                                            <td bgcolor="#fffff0" style="width: 88px; height: 13px; text-align: center">
                                                <dxe:ASPxRadioButtonList ID="rbl5" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                    CssPostfix="Office2003Blue" Font-Bold="True" ItemSpacing="10px" RepeatDirection="Horizontal"
                                                    SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" TextSpacing="10px">
                                                    <ClientSideEvents SelectedIndexChanged="function(s, e) {
		                                                        var test = document.getElementById('ctl00_body_Questionnaire1Procurement1_Aspxroundpanel10_lbl5');
                                                                if(s.GetValue() == 'Yes')
                                                                {
                                                                    test.innerHTML = 'Medium Risk';
                                                                    test.style.color = 'Gold';
                                                                }
                                                                else
                                                                {
                                                                    test.innerHTML = '-';
                                                                    test.style.color = 'Black';
                                                                }
                                                                setRiskLevel();
                                                        }" />
                                                    <Items>
                                                        <dxe:ListEditItem Text="Yes" Value="Yes" />
                                                        <dxe:ListEditItem Text="No" Value="No" />
                                                    </Items>
                                                    <ValidationSettings ErrorDisplayMode="ImageWithTooltip" ValidationGroup="Submit"
                                                        RequiredField-ErrorText="Question 9.5 - answer is required">
                                                        <RequiredField IsRequired="True" />
                                                    </ValidationSettings>
                                                </dxe:ASPxRadioButtonList>
                                            </td>
                                            <td bgcolor="#fffff0" style="width: 126px; height: 13px; text-align: center">
                                                <dxe:ASPxLabel runat="server" Text="-" Width="102px" Font-Bold="True" ID="lbl5">
                                                </dxe:ASPxLabel>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td bgcolor="#fffff0" style="width: 282px; height: 13px">Is the contractor on site frequently and working under limited Alcoa supervision
                                                in production/maintenance areas?
                                            </td>
                                            <td bgcolor="#fffff0" style="width: 88px; height: 13px; text-align: center">
                                                <dxe:ASPxRadioButtonList ID="rbl6" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                    CssPostfix="Office2003Blue" Font-Bold="True" ItemSpacing="10px" RepeatDirection="Horizontal"
                                                    SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" TextSpacing="10px">
                                                    <ClientSideEvents SelectedIndexChanged="function(s, e) {
		                                                        var test = document.getElementById('ctl00_body_Questionnaire1Procurement1_Aspxroundpanel10_lbl6');
                                                                if(s.GetValue() == 'Yes')
                                                                {
                                                                    test.innerHTML = 'High Risk';
                                                                    test.style.color = 'Orange';
                                                                }
                                                                else
                                                                {
                                                                    test.innerHTML = '-';
                                                                    test.style.color = 'Black';
                                                                }
                                                                setRiskLevel();
                                                        }" />
                                                    <Items>
                                                        <dxe:ListEditItem Text="Yes" Value="Yes" />
                                                        <dxe:ListEditItem Text="No" Value="No" />
                                                    </Items>
                                                    <ValidationSettings ErrorDisplayMode="ImageWithTooltip" ValidationGroup="Submit"
                                                        RequiredField-ErrorText="Question 9.6 - answer is required">
                                                        <RequiredField IsRequired="True" />
                                                    </ValidationSettings>
                                                </dxe:ASPxRadioButtonList>
                                            </td>
                                            <td bgcolor="#fffff0" style="width: 126px; height: 13px; text-align: center">
                                                <dxe:ASPxLabel runat="server" Text="-" Width="102px" Font-Bold="True" ID="lbl6">
                                                </dxe:ASPxLabel>
                                            </td>
                                            <td bgcolor="#fffff0" rowspan="2" style="width: 257px">Maintenance or capital works companies, civil and earth works contractors, roofing
                                                repairs, crane maintenance.
                                            </td>
                                        </tr>
                                        <tr>
                                            <td bgcolor="#fffff0" style="width: 282px; height: 13px">Is the contractor on site frequently and working under limited Alcoa supervision
                                                and exposed to high levels of risk?
                                            </td>
                                            <td bgcolor="#fffff0" style="width: 88px; height: 13px; text-align: center">
                                                <dxe:ASPxRadioButtonList ID="rbl7" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                    CssPostfix="Office2003Blue" Font-Bold="True" ItemSpacing="10px" RepeatDirection="Horizontal"
                                                    SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" TextSpacing="10px">
                                                    <ClientSideEvents SelectedIndexChanged="function(s, e) {
		                                                        var test = document.getElementById('ctl00_body_Questionnaire1Procurement1_Aspxroundpanel10_lbl7');
                                                                if(s.GetValue() == 'Yes')
                                                                {
                                                                    test.innerHTML = 'High Risk';
                                                                    test.style.color = 'Orange';
                                                                }
                                                                else
                                                                {
                                                                    test.innerHTML = '-';
                                                                    test.style.color = 'Black';
                                                                }
                                                                setRiskLevel();
                                                        }" />
                                                    <Items>
                                                        <dxe:ListEditItem Text="Yes" Value="Yes" />
                                                        <dxe:ListEditItem Text="No" Value="No" />
                                                    </Items>
                                                    <ValidationSettings ErrorDisplayMode="ImageWithTooltip" ValidationGroup="Submit"
                                                        RequiredField-ErrorText="Question 9.7 - answer is required">
                                                        <RequiredField IsRequired="True" />
                                                    </ValidationSettings>
                                                </dxe:ASPxRadioButtonList>
                                            </td>
                                            <td bgcolor="#fffff0" style="width: 126px; height: 13px; text-align: center">
                                                <dxe:ASPxLabel runat="server" Text="-" Width="102px" Font-Bold="True" ID="lbl7">
                                                </dxe:ASPxLabel>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td bgcolor="#fffff0" style="width: 282px; height: 13px">Is the contractor bidding for major maintenance or capital contract and mobilising
                                                a large (&gt;100 people) workforce on site?
                                            </td>
                                            <td bgcolor="#fffff0" style="width: 88px; height: 13px; text-align: center">
                                                <dxe:ASPxRadioButtonList ID="rbl8" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                    CssPostfix="Office2003Blue" Font-Bold="True" ItemSpacing="10px" RepeatDirection="Horizontal"
                                                    SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" TextSpacing="10px">
                                                    <ClientSideEvents SelectedIndexChanged="function(s, e) {
		                                                        var test = document.getElementById('ctl00_body_Questionnaire1Procurement1_Aspxroundpanel10_lbl8');
                                                                if(s.GetValue() == 'Yes')
                                                                {
                                                                    test.innerHTML = 'Forced High Risk';
                                                                    test.style.color = 'Red';
                                                                }
                                                                else
                                                                {
                                                                    test.innerHTML = '-';
                                                                    test.style.color = 'Black';
                                                                }
                                                                setRiskLevel();
                                                        }" />
                                                    <Items>
                                                        <dxe:ListEditItem Text="Yes" Value="Yes" />
                                                        <dxe:ListEditItem Text="No" Value="No" />
                                                    </Items>
                                                    <ValidationSettings ErrorDisplayMode="ImageWithTooltip" ValidationGroup="Submit"
                                                        RequiredField-ErrorText="Question 9.8 - answer is required">
                                                        <RequiredField IsRequired="True" />
                                                    </ValidationSettings>
                                                </dxe:ASPxRadioButtonList>
                                            </td>
                                            <td bgcolor="#fffff0" style="width: 126px; height: 13px; text-align: center">
                                                <dxe:ASPxLabel runat="server" Text="-" Width="102px" Font-Bold="True" ID="lbl8">
                                                </dxe:ASPxLabel>
                                            </td>
                                            <td bgcolor="#fffff0" rowspan="2" style="width: 257px">Major capital project installation contractors, on-site embedded maintenance or
                                                capital works companies, or outsourced operations managed fully on site.
                                            </td>
                                        </tr>
                                        <tr>
                                            <td bgcolor="#fffff0" style="width: 282px; height: 13px">Is the contractor seeking "embedded" status and will set up operations on Alcoa
                                                property?
                                            </td>
                                            <td bgcolor="#fffff0" style="width: 88px; height: 13px; text-align: center">
                                                <dxe:ASPxRadioButtonList ID="rbl9" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                    CssPostfix="Office2003Blue" Font-Bold="True" ItemSpacing="10px" RepeatDirection="Horizontal"
                                                    SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" TextSpacing="10px">
                                                    <ClientSideEvents SelectedIndexChanged="function(s, e) {
		                                                        var test = document.getElementById('ctl00_body_Questionnaire1Procurement1_Aspxroundpanel10_lbl9');
                                                                if(s.GetValue() == 'Yes')
                                                                {
                                                                    test.innerHTML = 'Forced High Risk';
                                                                    test.style.color = 'Red';
                                                                }
                                                                else
                                                                {
                                                                    test.innerHTML = '-';
                                                                    test.style.color = 'Black';
                                                                }
                                                                setRiskLevel();
                                                        }" />
                                                    <Items>
                                                        <dxe:ListEditItem Text="Yes" Value="Yes" />
                                                        <dxe:ListEditItem Text="No" Value="No" />
                                                    </Items>
                                                    <ValidationSettings ErrorDisplayMode="ImageWithTooltip" ValidationGroup="Submit"
                                                        RequiredField-ErrorText="Question 9.9 - answer is required">
                                                        <RequiredField IsRequired="True" />
                                                    </ValidationSettings>
                                                </dxe:ASPxRadioButtonList>
                                            </td>
                                            <td bgcolor="#fffff0" style="width: 126px; height: 13px; text-align: center">
                                                <dxe:ASPxLabel runat="server" Text="-" Width="102px" Font-Bold="True" ID="lbl9">
                                                </dxe:ASPxLabel>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td bgcolor="#fffff0" style="width: 282px; height: 13px">Has the contractor had a serious or disabling injury, fatality to any employee or
                                                contractor within the last 5 years?
                                            </td>
                                            <td bgcolor="#fffff0" style="width: 88px; height: 13px; text-align: center">
                                                <dxe:ASPxRadioButtonList ID="rbl10" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                    CssPostfix="Office2003Blue" Font-Bold="True" ItemSpacing="10px" RepeatDirection="Horizontal"
                                                    SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" TextSpacing="10px">
                                                    <ClientSideEvents SelectedIndexChanged="function(s, e) {
		                                                        var test = document.getElementById('ctl00_body_Questionnaire1Procurement1_Aspxroundpanel10_lbl10');
                                                                if(s.GetValue() == 'Yes')
                                                                {
                                                                    test.innerHTML = 'Forced High Risk';
                                                                    test.style.color = 'Red';
                                                                }
                                                                else
                                                                {
                                                                    test.innerHTML = '-';
                                                                    test.style.color = 'Black';
                                                                }
                                                                setRiskLevel();
                                                        }" />
                                                    <Items>
                                                        <dxe:ListEditItem Text="Yes" Value="Yes" />
                                                        <dxe:ListEditItem Text="No" Value="No" />
                                                    </Items>
                                                    <ValidationSettings ErrorDisplayMode="ImageWithTooltip" ValidationGroup="Submit"
                                                        RequiredField-ErrorText="Question 9.10 - answer is required">
                                                        <RequiredField IsRequired="True" />
                                                    </ValidationSettings>
                                                </dxe:ASPxRadioButtonList>
                                            </td>
                                            <td bgcolor="#fffff0" style="width: 126px; height: 13px; text-align: center">
                                                <dxe:ASPxLabel runat="server" Text="-" Width="102px" Font-Bold="True" ID="lbl10">
                                                </dxe:ASPxLabel>
                                            </td>
                                            <td bgcolor="#fffff0" rowspan="2" style="width: 257px">Companies which have had either a fatality or other triggers including regulatory
                                                action taken against the company due to injuries or fatality at any location the
                                                contractors work at. Includes non-Alcoan locations.
                                            </td>
                                        </tr>
                                        <tr>
                                            <td bgcolor="#fffff0" style="width: 282px; height: 13px">Has the contractor received any citation notices from any government agency as a
                                                result of an injury or fatality within the last 3 years?
                                            </td>
                                            <td bgcolor="#fffff0" style="width: 88px; height: 13px; text-align: center">
                                                <dxe:ASPxRadioButtonList ID="rbl11" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                    CssPostfix="Office2003Blue" Font-Bold="True" ItemSpacing="10px" RepeatDirection="Horizontal"
                                                    SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" TextSpacing="10px">
                                                    <ClientSideEvents SelectedIndexChanged="function(s, e) {
		                                                        var test = document.getElementById('ctl00_body_Questionnaire1Procurement1_Aspxroundpanel10_lbl11');
                                                                if(s.GetValue() == 'Yes')
                                                                {
                                                                    test.innerHTML = 'Forced High Risk';
                                                                    test.style.color = 'Red';
                                                                }
                                                                else
                                                                {
                                                                    test.innerHTML = '-';
                                                                    test.style.color = 'Black';
                                                                }
                                                                setRiskLevel();
                                                        }" />
                                                    <Items>
                                                        <dxe:ListEditItem Text="Yes" Value="Yes" />
                                                        <dxe:ListEditItem Text="No" Value="No" />
                                                    </Items>
                                                    <ValidationSettings ErrorDisplayMode="ImageWithTooltip" ValidationGroup="Submit"
                                                        RequiredField-ErrorText="Question 9.11 - answer is required">
                                                        <RequiredField IsRequired="True" />
                                                    </ValidationSettings>
                                                </dxe:ASPxRadioButtonList>
                                            </td>
                                            <td bgcolor="#fffff0" style="width: 126px; height: 13px; text-align: center">
                                                <dxe:ASPxLabel runat="server" Text="-" Width="102px" Font-Bold="True" ID="lbl11">
                                                </dxe:ASPxLabel>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </dxp:PanelContent>
                </PanelCollection>
                <TopLeftCorner Height="9px" Width="9px" />
                <BottomLeftCorner Height="9px" Width="9px" />
            </dxrp:ASPxRoundPanel>
            <asp:Label ID="rpbr12" runat="server" Text="<br />"></asp:Label>
            <dxrp:ASPxRoundPanel ID="ASPxRoundPanel12" runat="server" BackColor="#DDECFE" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                CssPostfix="Office2003Blue" EnableDefaultAppearance="False" HeaderText="10" Width="900px"
                SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" Visible="True">
                <TopEdge>
                    <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpTopEdge.png" Repeat="RepeatX"
                        HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
                </TopEdge>
                <BottomRightCorner Height="9px" Width="9px">
                </BottomRightCorner>
                <ContentPaddings Padding="2px" PaddingTop="10px" PaddingBottom="10px"></ContentPaddings>
                <NoHeaderTopRightCorner Height="9px" Width="9px">
                </NoHeaderTopRightCorner>
                <HeaderRightEdge>
                    <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" Repeat="RepeatX"
                        HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
                </HeaderRightEdge>
                <Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px"></Border>
                <HeaderLeftEdge>
                    <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" Repeat="RepeatX"
                        HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
                </HeaderLeftEdge>
                <HeaderStyle BackColor="#7BA4E0">
                    <Paddings Padding="0px" PaddingLeft="2px" PaddingRight="2px" PaddingBottom="7px"></Paddings>
                    <BorderBottom BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px"></BorderBottom>
                </HeaderStyle>
                <TopRightCorner Height="9px" Width="9px">
                </TopRightCorner>
                <HeaderContent>
                    <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" Repeat="RepeatX"
                        HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
                </HeaderContent>
                <DisabledStyle ForeColor="Gray">
                </DisabledStyle>
                <NoHeaderTopEdge BackColor="#DDECFE">
                </NoHeaderTopEdge>
                <NoHeaderTopLeftCorner Height="9px" Width="9px">
                </NoHeaderTopLeftCorner>
                <PanelCollection>
                    <dxp:PanelContent ID="PanelContent1" runat="server">
                        <table style="width: 100%" cellspacing="0" cellpadding="0" border="0">
                            <tbody>
                                <tr>
                                    <td style="height: 21px">Will this company be engaging Subcontractors (now or in the future)?<br />
                                        If Yes, the supplier will be required to complete the Verification Questionnaire
                                        section.
                                    </td>
                                </tr>
                                <tr>
                                    <td>&nbsp;
                                        <dxe:ASPxComboBox ID="qAnswer12_1" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                            CssPostfix="Office2003Blue" IncrementalFilteringMode="StartsWith" ValueType="System.String"
                                            Width="355px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                            <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                                            </LoadingPanelImage>
                                            <Items>
                                                <dxe:ListEditItem Text="Yes" Value="Yes" />
                                                <dxe:ListEditItem Text="No" Value="No" />
                                            </Items>
                                            <ValidationSettings ErrorDisplayMode="ImageWithTooltip" ValidationGroup="Submit"
                                                RequiredField-ErrorText="Question 12 - answer is required">
                                                <RequiredField IsRequired="True" />
                                            </ValidationSettings>
                                        </dxe:ASPxComboBox>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </dxp:PanelContent>
                </PanelCollection>
                <TopLeftCorner Height="9px" Width="9px" />
                <BottomLeftCorner Height="9px" Width="9px" />
            </dxrp:ASPxRoundPanel>
            <asp:Label ID="rpbr13" runat="server" Text="<br />"></asp:Label>
            <dxrp:ASPxRoundPanel ID="ASPxRoundPanel13" runat="server" BackColor="#DDECFE" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                CssPostfix="Office2003Blue" EnableDefaultAppearance="False" HeaderText="11" Width="900px"
                SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" Visible="True">
                <TopEdge>
                    <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpTopEdge.png" Repeat="RepeatX"
                        HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
                </TopEdge>
                <BottomRightCorner Height="9px" Width="9px">
                </BottomRightCorner>
                <ContentPaddings Padding="2px" PaddingTop="10px" PaddingBottom="10px"></ContentPaddings>
                <NoHeaderTopRightCorner Height="9px" Width="9px">
                </NoHeaderTopRightCorner>
                <HeaderRightEdge>
                    <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" Repeat="RepeatX"
                        HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
                </HeaderRightEdge>
                <Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px"></Border>
                <HeaderLeftEdge>
                    <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" Repeat="RepeatX"
                        HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
                </HeaderLeftEdge>
                <HeaderStyle BackColor="#7BA4E0">
                    <Paddings Padding="0px" PaddingLeft="2px" PaddingRight="2px" PaddingBottom="7px"></Paddings>
                    <BorderBottom BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px"></BorderBottom>
                </HeaderStyle>
                <TopRightCorner Height="9px" Width="9px">
                </TopRightCorner>
                <HeaderContent>
                    <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" Repeat="RepeatX"
                        HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
                </HeaderContent>
                <DisabledStyle ForeColor="Gray">
                </DisabledStyle>
                <NoHeaderTopEdge BackColor="#DDECFE">
                </NoHeaderTopEdge>
                <NoHeaderTopLeftCorner Height="9px" Width="9px">
                </NoHeaderTopLeftCorner>
                <PanelCollection>
                    <dxp:PanelContent ID="PanelContent2" runat="server">
                        <table style="width: 100%" cellspacing="0" cellpadding="0" border="0">
                            <tbody>
                                <tr>
                                    <td style="height: 21px">Please comment on the reason this supplier is required.
                                    </td>
                                </tr>
                                <tr>
                                    <td>&nbsp;
                                        <dxe:ASPxMemo runat="server" Height="85px" Width="100%" ID="qAnswer13" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                            CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                            <ValidationSettings ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True" ValidationGroup="Submit"
                                                RequiredField-ErrorText="Question 11 - Comment is required on the reason this supplier is required.">
                                                <RequiredField IsRequired="True"></RequiredField>
                                            </ValidationSettings>
                                        </dxe:ASPxMemo>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </dxp:PanelContent>
                </PanelCollection>
                <TopLeftCorner Height="9px" Width="9px" />
                <BottomLeftCorner Height="9px" Width="9px" />
            </dxrp:ASPxRoundPanel>
            <br />
            <dxrp:ASPxRoundPanel ID="ASPxRoundPanel14" runat="server" BackColor="#DDECFE" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                CssPostfix="Office2003Blue" EnableDefaultAppearance="False" HeaderText="8" Width="900px"
                SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                Visible="False">
                <TopEdge>
                    <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpTopEdge.png" Repeat="RepeatX"
                        HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
                </TopEdge>
                <BottomRightCorner Height="9px" Width="9px">
                </BottomRightCorner>
                <ContentPaddings Padding="2px" PaddingTop="10px" PaddingBottom="10px"></ContentPaddings>
                <NoHeaderTopRightCorner Height="9px" Width="9px">
                </NoHeaderTopRightCorner>
                <HeaderRightEdge>
                    <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" Repeat="RepeatX"
                        HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
                </HeaderRightEdge>
                <Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px"></Border>
                <HeaderLeftEdge>
                    <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" Repeat="RepeatX"
                        HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
                </HeaderLeftEdge>
                <HeaderStyle BackColor="#7BA4E0">
                    <Paddings Padding="0px" PaddingLeft="2px" PaddingRight="2px" PaddingBottom="7px"></Paddings>
                    <BorderBottom BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px"></BorderBottom>
                </HeaderStyle>
                <TopRightCorner Height="9px" Width="9px">
                </TopRightCorner>
                <HeaderContent>
                    <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" Repeat="RepeatX"
                        HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
                </HeaderContent>
                <DisabledStyle ForeColor="Gray">
                </DisabledStyle>
                <NoHeaderTopEdge BackColor="#DDECFE">
                </NoHeaderTopEdge>
                <NoHeaderTopLeftCorner Height="9px" Width="9px">
                </NoHeaderTopLeftCorner>
                <PanelCollection>
                    <dxp:PanelContent ID="PanelContent3" runat="server">
                        <table style="width: 100%" cellspacing="0" cellpadding="0" border="0">
                            <tbody>
                                <tr>
                                    <td style="height: 21px">Please indicate who is the direct company requesting this Subcontractor.</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;
                                        <table>
                                            <tr>
                                                <td>
                                                    <dxe:ASPxComboBox ID="qAnswer14" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                        CssPostfix="Office2003Blue" IncrementalFilteringMode="StartsWith" ValueType="System.Int32"
                                                        Width="250px"
                                                        SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                        ClientInstanceName="cmbCompanies"
                                                        DataSourceID="dsDirectCompanyQualifiedDatabase" TextField="CompanyName"
                                                        ValueField="CompanyId">
                                                        <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                                                        </LoadingPanelImage>
                                                        <ButtonStyle Width="13px">
                                                        </ButtonStyle>
                                                        <ValidationSettings ErrorDisplayMode="ImageWithTooltip" ValidationGroup="Submit"
                                                            RequiredField-ErrorText="Question 8 - Please select a company."
                                                            CausesValidation="True" SetFocusOnError="True">
                                                            <RequiredField IsRequired="True"
                                                                ErrorText="Question 8 - Please select a company." />
                                                        </ValidationSettings>
                                                        <ClientSideEvents SelectedIndexChanged="function(s, e) {
	                                                                    pnlHealthCheck.PerformCallback(s.GetValue());
                                                                    }" />
                                                    </dxe:ASPxComboBox>

                                                </td>
                                                <td>
                                                    <dxcp:ASPxCallbackPanel ID="pnlHealthCheck" runat="server" ClientInstanceName="pnlHealthCheck" OnCallback="pnlHealthCheck_Callback">
                                                        <PanelCollection>
                                                            <dxp:PanelContent ID="PanelContentHealthCheck" runat="server">
                                                                <table id="tblHealthCheck" runat="server">
                                                                    <tr>
                                                                        <td>
                                                                            <dxe:ASPxHyperLink ID="lbHealthCheck" runat="server" Text="Health Check"
                                                                                NavigateUrl="javascript:popUp('PopUps/HealthCheckPopup.aspx?CompanyId=' + cmbCompanies.GetValue() +
                                                                                '&CompanyName=' + cmbCompanies.GetText());"
                                                                                CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue" />
                                                                        </td>
                                                                        <td>
                                                                            <dxg:ASPxGaugeControl ID="gcTL_SC" runat="server" BackColor="#DDECFE" Height="25px"
                                                                                ImageType="Png" Value="3" Width="25px">
                                                                                <Gauges>
                                                                                    <dxg:StateIndicatorGauge Bounds="0, 0, 20, 20" Name="Gauge0">
                                                                                        <indicators>
                                                                                <dxg:StateIndicatorComponent Center="124, 124" Name="stateIndicatorComponent1" 
                                                                                    StateIndex="0">
                                                                                    <states>
                                                                                        <dxg:IndicatorStateWeb Name="State1" ShapeType="ElectricLight1" />
                                                                                        <dxg:IndicatorStateWeb Name="State2" ShapeType="ElectricLight2" />
                                                                                        <dxg:IndicatorStateWeb Name="State3" ShapeType="ElectricLight3" />
                                                                                        <dxg:IndicatorStateWeb Name="State4" ShapeType="ElectricLight4" />
                                                                                    </states>
                                                                                </dxg:StateIndicatorComponent>
                                                                            </indicators>
                                                                                    </dxg:StateIndicatorGauge>
                                                                                </Gauges>

                                                                                <LayoutPadding All="0" Left="0" Top="0" Right="0" Bottom="0"></LayoutPadding>
                                                                            </dxg:ASPxGaugeControl>
                                                                        </td>
                                                                        <td>
                                                                            <dxe:ASPxLabel ID="lblTL_SC" runat="server" Text="Approval to engage Sub Contractors"></dxe:ASPxLabel>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </dxp:PanelContent>
                                                        </PanelCollection>
                                                    </dxcp:ASPxCallbackPanel>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </dxp:PanelContent>
                </PanelCollection>
                <TopLeftCorner Height="9px" Width="9px" />
                <BottomLeftCorner Height="9px" Width="9px" />
            </dxrp:ASPxRoundPanel>
            <br />
        </td>
    </tr>
    <tr>
        <td style="width: 900px; text-align: left">
            <dxe:ASPxValidationSummary ID="ASPxValidationSummary1" runat="server" HeaderText="Validation Errors - You must correct these before being able to submit:"
                ShowErrorsInEditors="True" ValidationGroup="Submit">
            </dxe:ASPxValidationSummary>
        </td>
    </tr>
    <tr>
        <td style="width: 900px; text-align: center">
            <dxe:ASPxLabel ID="lblSubmit" runat="server" ForeColor="Red" Text="Note: You can not submit this procurement questionnaire until the supplier contact has been verified.">
            </dxe:ASPxLabel>
        </td>
    </tr>
    <tr>
        <td style="width: 900px; text-align: center">
            <div align="center">
                <table border="0" cellpadding="1" cellspacing="1">
                    <tr>
                        <td style="height: 48px">
                            <dxe:ASPxButton ID="btnHome2" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                CssPostfix="Office2003Blue" OnClick="btnHome_Click" Text="Home" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" UseSubmitBehavior="false">
                            </dxe:ASPxButton>
                        </td>
                        <td style="height: 48px">
                            <dxe:ASPxButton ID="btnSave2" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                CssPostfix="Office2003Blue" OnClick="btnSave_Click" Text="Save " SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" UseSubmitBehavior="false">
                            </dxe:ASPxButton>
                        </td>
                        <td>
                            <dxe:ASPxButton ID="btnSubmit" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                CssPostfix="Office2003Blue" OnClick="btnSubmit_Click" Text="Submit" ValidationGroup="Submit"
                                SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" Enabled="False" UseSubmitBehavior="false">
                            </dxe:ASPxButton>
                        </td>
                        <td>
                            <dxe:ASPxButton ID="btnApproveBottom" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                CssPostfix="Office2003Blue" OnClick="btnSubmit_Click" Text="Approve" Visible="false"
                                SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" UseSubmitBehavior="false">
                                <ClientSideEvents Click="function (s, e) {e.processOnServer = confirm('Are you sure you want to approve this procurement questionnaire?');}" />
                            </dxe:ASPxButton>
                        </td>
                        <td>
                            <dxe:ASPxButton ID="btnNotApproveBottom" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                CssPostfix="Office2003Blue" Text="Not Approve"
                                SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" Visible="False" UseSubmitBehavior="false" AutoPostBack="false">
                            </dxe:ASPxButton>
                        </td>
                    </tr>
                </table>
            </div>
        </td>
    </tr>
</table>


<%--DT222--%>

<dx:ASPxPopupControl ID="popupForciblyRequireVerificationTop" runat="server" AllowDragging="True"
    CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue"
    HeaderText="Forcibly Require Verification Questionnaire" Modal="True" PopupElementID="btnNotApproveTop"
    Width="200px" Font-Bold="False" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
    ShowHeader="False">
    <ClientSideEvents PopUp="function(s){s.UpdatePosition()}" />
    <SizeGripImage Height="16px" Width="16px" />
    <ContentCollection>
        <dx:PopupControlContentControl ID="btnNotApproveTopContent" runat="server" BackColor="#DDECFE">

            <dxrp:ASPxRoundPanel ID="ASPxRoundPanel15" runat="server" BackColor="#DDECFE"
                CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue"
                ShowHeader="false" EnableDefaultAppearance="False" HeaderText="Procurement Questionnaire - Not approve"
                SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" Width="490px">
                <TopEdge>
                    <BackgroundImage HorizontalPosition="left" ImageUrl="~/App_Themes/Office2003Blue/Web/rpTopEdge.png"
                        Repeat="RepeatX" VerticalPosition="top" />
                </TopEdge>
                <ContentPaddings Padding="2px" PaddingBottom="10px" PaddingTop="10px" />
                <HeaderRightEdge>
                    <BackgroundImage HorizontalPosition="left" ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png"
                        Repeat="RepeatX" VerticalPosition="top" />
                </HeaderRightEdge>
                <Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px" />
                <HeaderLeftEdge>
                    <BackgroundImage HorizontalPosition="left" ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png"
                        Repeat="RepeatX" VerticalPosition="top" />
                </HeaderLeftEdge>
                <HeaderStyle BackColor="#7BA4E0">
                    <Paddings Padding="0px" PaddingBottom="7px" PaddingLeft="2px" PaddingRight="2px" />
                    <BorderBottom BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px" />
                </HeaderStyle>
                <HeaderContent>
                    <BackgroundImage HorizontalPosition="left" ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png"
                        Repeat="RepeatX" VerticalPosition="top" />
                </HeaderContent>
                <DisabledStyle ForeColor="Gray">
                </DisabledStyle>
                <NoHeaderTopEdge BackColor="#DDECFE">
                </NoHeaderTopEdge>
                <PanelCollection>
                    <dxp:PanelContent ID="PanelContent7" runat="server">
                        <div align="center">
                            <span style="color: black; font-weight: bold; text-align: center">Reason for not approving Procurement Questionnaire
                                <br />
                                (Recorded in History Log and Sent to Supplier)
                            </span>
                            <dxe:ASPxMemo ID="mbReasonForNotApprovingTop" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                CssPostfix="Office2003Blue" Height="53px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                Width="100%">
                                <ValidationSettings Display="Dynamic" ValidationGroup="ForciblyRequireVerification">
                                    <RequiredField IsRequired="True" />
                                </ValidationSettings>

                            </dxe:ASPxMemo>
                            <br />
                            <dxe:ASPxButton ID="btnNotApproveOnPopupTop" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                Text="Send Back to Procuement Contact" Width="236px"
                                ValidationGroup="ForciblyRequireVerification" OnClick="btnNotApproveOnPopup_click">
                            </dxe:ASPxButton>
                            <br />
                            <span style="font-size: 10pt; color: red">
                                <dxe:ASPxLabel runat="server" Text="Note: by not approving this Procurement Questionnaire, it will be sent back to the Procument Contact to revise (an email will be sent to them with the reasons above)."></dxe:ASPxLabel>
                            </span>
                        </div>
                    </dxp:PanelContent>
                </PanelCollection>
            </dxrp:ASPxRoundPanel>
        </dx:PopupControlContentControl>
    </ContentCollection>
    <CloseButtonImage Height="12px" Width="13px" />
    <HeaderStyle>
        <Paddings PaddingRight="6px" />
    </HeaderStyle>
</dx:ASPxPopupControl>


<dx:ASPxPopupControl ID="popupForciblyRequireVerificationBottom" runat="server" AllowDragging="True"
    CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue"
    HeaderText="Forcibly Require Verification Questionnaire" Modal="True" PopupElementID="btnNotApproveBottom"
    Width="200px" Font-Bold="False" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
    ShowHeader="False">
    <ClientSideEvents PopUp="function(s){s.UpdatePosition()}" />
    <SizeGripImage Height="16px" Width="16px" />
    <ContentCollection>
        <dx:PopupControlContentControl ID="PopupControlContentControl1" runat="server" BackColor="#DDECFE">

            <dxrp:ASPxRoundPanel ID="ASPxRoundPanel16" runat="server" BackColor="#DDECFE"
                CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue"
                ShowHeader="false" EnableDefaultAppearance="False" HeaderText="Procurement Questionnaire - Not approve"
                SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" Width="490px">
                <TopEdge>
                    <BackgroundImage HorizontalPosition="left" ImageUrl="~/App_Themes/Office2003Blue/Web/rpTopEdge.png"
                        Repeat="RepeatX" VerticalPosition="top" />
                </TopEdge>
                <ContentPaddings Padding="2px" PaddingBottom="10px" PaddingTop="10px" />
                <HeaderRightEdge>
                    <BackgroundImage HorizontalPosition="left" ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png"
                        Repeat="RepeatX" VerticalPosition="top" />
                </HeaderRightEdge>
                <Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px" />
                <HeaderLeftEdge>
                    <BackgroundImage HorizontalPosition="left" ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png"
                        Repeat="RepeatX" VerticalPosition="top" />
                </HeaderLeftEdge>
                <HeaderStyle BackColor="#7BA4E0">
                    <Paddings Padding="0px" PaddingBottom="7px" PaddingLeft="2px" PaddingRight="2px" />
                    <BorderBottom BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px" />
                </HeaderStyle>
                <HeaderContent>
                    <BackgroundImage HorizontalPosition="left" ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png"
                        Repeat="RepeatX" VerticalPosition="top" />
                </HeaderContent>
                <DisabledStyle ForeColor="Gray">
                </DisabledStyle>
                <NoHeaderTopEdge BackColor="#DDECFE">
                </NoHeaderTopEdge>
                <PanelCollection>
                    <dxp:PanelContent ID="PanelContent4" runat="server">
                        <div align="center">
                            <span style="color: black; font-weight: bold; text-align: center">Reason for not approving Procurement Questionnaire
                                <br />
                                (Recorded in History Log and Sent to Supplier)
                            </span>
                            <dxe:ASPxMemo ID="mbReasonForNotApprovingBottom" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                CssPostfix="Office2003Blue" Height="53px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                Width="100%">
                                <ValidationSettings Display="Dynamic" ValidationGroup="RejectProcurementQuestionnaireBottom">
                                    <RequiredField IsRequired="True" />
                                </ValidationSettings>

                            </dxe:ASPxMemo>
                            <br />
                            <dxe:ASPxButton ID="btnNotApproveOnPopupBottom" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                Text="Send Back to Procuement Contact" Width="236px"
                                ValidationGroup="RejectProcurementQuestionnaireBottom" OnClick="btnNotApproveOnPopup_click">
                            </dxe:ASPxButton>
                            <br />
                            <span style="font-size: 10pt; color: red">
                                <dxe:ASPxLabel ID="ASPxLabel2" runat="server" Text="Note: by not approving this Procurement Questionnaire, it will be sent back to the Procument Contact to revise (an email will be sent to them with the reasons above)."></dxe:ASPxLabel>
                            </span>
                        </div>
                    </dxp:PanelContent>
                </PanelCollection>
            </dxrp:ASPxRoundPanel>
        </dx:PopupControlContentControl>
    </ContentCollection>
    <CloseButtonImage Height="12px" Width="13px" />
    <HeaderStyle>
        <Paddings PaddingRight="6px" />
    </HeaderStyle>
</dx:ASPxPopupControl>
<%--End DT222--%>









<br />
<table width="900">
    <tbody>
        <tr width="900px">
            <td style="text-align: right" class="pageName">
                <a href="#top">Scroll to top of page</a>
                <br />
            </td>
        </tr>
    </tbody>
</table>
<br />
<data:QuestionnaireServicesCategoryDataSource ID="QuestionnaireServicesCategoryDataSource"
    runat="server" EnableSorting="True" SelectMethod="GetPaged">
    <Parameters>
        <data:CustomParameter Name="OrderBy" DefaultValue="CategoryText" />
    </Parameters>
</data:QuestionnaireServicesCategoryDataSource>
<data:QuestionnaireServicesSelectedDataSource ID="QuestionnaireServicesSelectedDataSource"
    runat="server" EnablePaging="True" EnableSorting="True" SelectMethod="GetByQuestionnaireIdQuestionnaireTypeId"
    EnableCaching="False" InsertMethod="Insert" UpdateMethod="Update">
    <DeepLoadProperties Method="IncludeChildren" Recursive="False">
    </DeepLoadProperties>
    <Parameters>
        <asp:QueryStringParameter DefaultValue="0" Name="QuestionnaireId" QueryStringField="q" />
        <data:SqlParameter DefaultValue="1" Name="QuestionnaireTypeId" />
    </Parameters>
</data:QuestionnaireServicesSelectedDataSource>
<data:QuestionnaireServicesSelectedDataSource ID="QuestionnaireServicesSelectedDataSource_2"
    runat="server" EnablePaging="True" EnableSorting="True" SelectMethod="GetByQuestionnaireIdQuestionnaireTypeId"
    EnableCaching="False" InsertMethod="Insert" UpdateMethod="Update">
    <DeepLoadProperties Method="IncludeChildren" Recursive="False">
    </DeepLoadProperties>
    <Parameters>
        <asp:QueryStringParameter DefaultValue="0" Name="QuestionnaireId" QueryStringField="q" />
        <data:SqlParameter DefaultValue="2" Name="QuestionnaireTypeId" />
    </Parameters>
</data:QuestionnaireServicesSelectedDataSource>
<data:EntityDataSource ID="SitesDataSource2" runat="server" ProviderName="SitesProvider"
    EntityTypeName="KaiZen.CSMS.Sites, KaiZen.CSMS" EntityKeyTypeName="System.Int32"
    SelectMethod="GetAll" Sort="SiteName ASC" Filter="IsVisible = True" />
<data:CompaniesDataSource ID="CompaniesDataSource" runat="server" SelectMethod="GetPaged"
    Sort="">
    <DeepLoadProperties Method="IncludeChildren" Recursive="False">
    </DeepLoadProperties>
</data:CompaniesDataSource>
<data:EntityDataSource ID="UsersAlcoanDataSource2" runat="server" ProviderName="UsersAlcoanProvider"
    EntityTypeName="KaiZen.CSMS.UsersAlcoan, KaiZen.CSMS" EntityKeyTypeName="System.Int32"
    SelectMethod="GetAll" Sort="UserFullNameLogon ASC" />
<data:EntityDataSource ID="UsersProcurementListDataSource2" runat="server" ProviderName="UsersProcurementListProvider"
    EntityTypeName="KaiZen.CSMS.UsersProcurementList, KaiZen.CSMS" EntityKeyTypeName="System.Int32"
    SelectMethod="GetAll" Sort="UserFullNameLogon ASC" Filter="Enabled = True" />
<data:EntityDataSource ID="UsersEhsConsultantsDataSource2" runat="server" ProviderName="UsersEhsConsultantsProvider"
    EntityTypeName="KaiZen.CSMS.UsersEhsConsultants, KaiZen.CSMS" EntityKeyTypeName="System.Int32"
    SelectMethod="GetAll" Sort="UserFullNameLogon ASC" Filter="Enabled = True" />
<data:CompanySiteCategoryDataSource ID="CompanySiteCategoryDataSource" runat="server"
    SelectMethod="GetPaged" EnablePaging="True" EnableSorting="True">
</data:CompanySiteCategoryDataSource>
<asp:SqlDataSource ID="dsDirectCompanyQualifiedDatabase" runat="server"
    ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="_QuestionnaireWithLocationApprovalView_Companies"
    SelectCommandType="StoredProcedure"></asp:SqlDataSource>

