﻿
using System;

namespace KaiZen.CSMS.Entities
{
	/// <summary>
	/// An enum representation of the 'AdminTaskEmailRecipient' table. [No description found in the database]
	/// </summary>
	/// <remark>this enumeration contains the items contained in the table AdminTaskEmailRecipient</remark>
	[Serializable]
	public enum AdminTaskEmailRecipientList
	{
		/// <summary> 
		/// Admin
		/// </summary>
		[EnumTextValue(@"Admin")]
		Admin = 1, 

		/// <summary> 
		/// Procurement Contact
		/// </summary>
		[EnumTextValue(@"Procurement Contact")]
		Procurement_Contact = 2, 

		/// <summary> 
		/// Procurement Functional Supervisor
		/// </summary>
		[EnumTextValue(@"Procurement Functional Supervisor")]
		Procurement_Functional_Supervisor = 3, 

		/// <summary> 
		/// Requester
		/// </summary>
		[EnumTextValue(@"Requester")]
		Requester = 4, 

		/// <summary> 
		/// H&S Assessor
		/// </summary>
		[EnumTextValue(@"H&S Assessor")]
		H_S_Assessor = 5, 

		/// <summary> 
		/// Lead H&S Assessor
		/// </summary>
		[EnumTextValue(@"Lead H&S Assessor")]
		Lead_H_S_Assessor = 6

	}
}
