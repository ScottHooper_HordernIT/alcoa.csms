﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="ALCOA.CSMS.Website.UserControls.Admin.HsAssessorEscalation"
    CodeBehind="UsersHsAssessorEscalation.ascx.cs" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Src="../Other/ExportButtons.ascx" TagName="ExportButtons" TagPrefix="uc1" %>

<dx:ASPxGridView ID="gridHsAssessorsNew" runat="server" AutoGenerateColumns="False" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
    OnInitNewRow="grid_InitNewRow" 
    CssPostfix="Office2003Blue" DataSourceID="EHSConsultantsDataSourceNew" KeyFieldName="EhsConsultantId" Width="900px">
    <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
        <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
        </LoadingPanelOnStatusBar>
        <CollapsedButton Height="12px" Width="11px" />
        <ExpandedButton Height="12px" Width="11px" />
        <DetailCollapsedButton Height="12px" Width="11px" />
        <DetailExpandedButton Height="12px" Width="11px" />
        <LoadingPanel Url="~/App_Themes/Office2003Blue/GridView/Loading.gif">
        </LoadingPanel>
    </Images>
    <ImagesFilterControl>
        <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
        </LoadingPanel>
    </ImagesFilterControl>
    <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue">
        <Header ImageSpacing="5px" SortingImageSpacing="5px" />
        <LoadingPanel ImageSpacing="10px" />
    </Styles>
    <Columns>
        <dx:GridViewCommandColumn Caption="Action" VisibleIndex="0" Width="120px" FixedStyle="Left" ShowDeleteButton="True" ShowEditButton="True" ShowNewButtonInHeader="True">
            <EditButton Visible="True">
            </EditButton>
            <NewButton Visible="True">
            </NewButton>
            <DeleteButton Visible="True">
            </DeleteButton>
            <ClearFilterButton Visible="True">
            </ClearFilterButton>
            <CellStyle HorizontalAlign="Center" Wrap="False">
            </CellStyle>
        </dx:GridViewCommandColumn>
        <dx:GridViewDataTextColumn FieldName="EhsConsultantId" ReadOnly="True" Visible="False"
            VisibleIndex="1" Width="0px">
            <EditFormSettings Visible="False" />
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataCheckColumn Caption="Active" FieldName="Enabled" VisibleIndex="3"
            Width="64px">
        </dx:GridViewDataCheckColumn>
        <dx:GridViewDataComboBoxColumn Caption="Name" FieldName="UserId" UnboundType="String"
            VisibleIndex="2" SortIndex="0" SortOrder="Ascending" FixedStyle="Left" Width="126px">
            <PropertiesComboBox DataSourceID="UsersDataSource" DropDownHeight="150px" IncrementalFilteringMode="StartsWith"
                TextField="UserDetails" ValueField="UserId" ValueType="System.Int32">
            </PropertiesComboBox>
            <Settings SortMode="DisplayText"/>
        </dx:GridViewDataComboBoxColumn>
        <dx:GridViewDataComboBoxColumn Caption="Region" FieldName="RegionId" UnboundType="String"
            VisibleIndex="4" Width="75px">
            <PropertiesComboBox DropDownHeight="150px" ValueType="System.Int32" IncrementalFilteringMode="StartsWith">
                <Items>
                    <dx:ListEditItem Text="WAO" Value="1" />
                    <dx:ListEditItem Text="VICOPS" Value="4" />
                    <dx:ListEditItem Text="ARP" Value="7" />
                </Items>
            </PropertiesComboBox>
            <Settings SortMode="DisplayText"/>
        </dx:GridViewDataComboBoxColumn>
        <dx:GridViewDataComboBoxColumn Caption="Site" FieldName="SiteId" UnboundType="String"
            VisibleIndex="5" Width="100px">
            <PropertiesComboBox DataSourceID="SitesDataSource1" DropDownHeight="150px" IncrementalFilteringMode="StartsWith"
                TextField="SiteName" ValueField="SiteId" ValueType="System.Int32">
            </PropertiesComboBox>
            <Settings SortMode="DisplayText" />
        </dx:GridViewDataComboBoxColumn>
        <dx:GridViewDataTextColumn Caption="# Companies Assigned to" FieldName="NoCompanies" ReadOnly="True" VisibleIndex="7" Width="90px">
            <Settings AllowAutoFilter="False" />
            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
            <CellStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
            </CellStyle>
        </dx:GridViewDataTextColumn>
    </Columns>
    <SettingsPager NumericButtonCount="5" PageSize="5" AlwaysShowPager="True">
        <AllButton Visible="True">
        </AllButton>
    </SettingsPager>
    <Settings  ShowHorizontalScrollBar="True" ShowFilterRow="true"/>
    <SettingsEditing Mode="Inline" />
    <SettingsBehavior ColumnResizeMode="NextColumn" ConfirmDelete="True"></SettingsBehavior>
</dx:ASPxGridView>
nb. # Companies Assigned is calculated when this page is loaded/refreshed.
<br />
<table border="0" cellpadding="0" cellspacing="0" width="900">
    <tr>
        <td style="height: 10px; text-align: right">
            <uc1:ExportButtons ID="ucExportButtons" runat="server" />
        </td>
    </tr>
</table>
<br />

<asp:SqlDataSource ID="UsersDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>">
</asp:SqlDataSource>
<asp:SqlDataSource ID="EHSConsultantsDataSourceNew" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="EHSConsultantsCompanyCount"
    SelectCommandType="StoredProcedure"
    UpdateCommand="Update dbo.EHSConsultant Set UserId = @UserId, Enabled = @Enabled, SiteId = @SiteId, RegionId = @RegionId where EHSConsultantId = @EhsConsultantId"
    DeleteCommand="Delete from dbo.EHSConsultant where EHSConsultantId = @EhsConsultantId"
    InsertCommand="Insert into dbo.EHSConsultant(UserId,Enabled,SiteId,RegionId) Values(@UserId, @Enabled, @SiteId, @RegionId)">
    <DeleteParameters>
        <asp:Parameter Name="EhsConsultantId" />
    </DeleteParameters>
    <InsertParameters>
        <asp:Parameter Name="UserId" />
        <asp:Parameter Name="Enabled" />
        <asp:Parameter Name="SiteId" />
        <asp:Parameter Name="RegionId" />
    </InsertParameters>
    <UpdateParameters>
        <asp:Parameter Name="UserId" />
        <asp:Parameter Name="Enabled" />
        <asp:Parameter Name="SiteId" />
        <asp:Parameter Name="RegionId" />
        <asp:Parameter Name="EhsConsultantId" />
    </UpdateParameters>
</asp:SqlDataSource>
<data:SitesDataSource ID="SitesDataSource1" runat="server" Sort="SiteName ASC" Filter="SiteName != 'Australia GPP'"></data:SitesDataSource>
