﻿#region Using directives

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="EbiLastTimeOnSiteDistinctProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract class EbiLastTimeOnSiteDistinctProviderBaseCore : EntityViewProviderBase<EbiLastTimeOnSiteDistinct>
	{
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions
		
		/*
		///<summary>
		/// Fill an VList&lt;EbiLastTimeOnSiteDistinct&gt; From a DataSet
		///</summary>
		/// <param name="dataSet">the DataSet</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList&lt;EbiLastTimeOnSiteDistinct&gt;"/></returns>
		protected static VList&lt;EbiLastTimeOnSiteDistinct&gt; Fill(DataSet dataSet, VList<EbiLastTimeOnSiteDistinct> rows, int start, int pagelen)
		{
			if (dataSet.Tables.Count == 1)
			{
				return Fill(dataSet.Tables[0], rows, start, pagelen);
			}
			else
			{
				return new VList<EbiLastTimeOnSiteDistinct>();
			}	
		}
		
		
		///<summary>
		/// Fill an VList&lt;EbiLastTimeOnSiteDistinct&gt; From a DataTable
		///</summary>
		/// <param name="dataTable">the DataTable that hold the data.</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList<EbiLastTimeOnSiteDistinct>"/></returns>
		protected static VList&lt;EbiLastTimeOnSiteDistinct&gt; Fill(DataTable dataTable, VList<EbiLastTimeOnSiteDistinct> rows, int start, int pagelen)
		{
			int recordnum = 0;
			
			System.Collections.IEnumerator dataRows =  dataTable.Rows.GetEnumerator();
			
			while (dataRows.MoveNext() && (pagelen != 0))
			{
				if(recordnum >= start)
				{
					DataRow row = (DataRow)dataRows.Current;
				
					EbiLastTimeOnSiteDistinct c = new EbiLastTimeOnSiteDistinct();
					c.CompanyName = (Convert.IsDBNull(row["CompanyName"]))?string.Empty:(System.String)row["CompanyName"];
					c.SwipeSite = (Convert.IsDBNull(row["SwipeSite"]))?string.Empty:(System.String)row["SwipeSite"];
					c.SwipeDateTime = (Convert.IsDBNull(row["SwipeDateTime"]))?DateTime.MinValue:(System.DateTime)row["SwipeDateTime"];
					c.AcceptChanges();
					rows.Add(c);
					pagelen -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		*/	
						
		///<summary>
		/// Fill an <see cref="VList&lt;EbiLastTimeOnSiteDistinct&gt;"/> From a DataReader.
		///</summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pageLength">number of row.</param>
		///<returns>a <see cref="VList&lt;EbiLastTimeOnSiteDistinct&gt;"/></returns>
		protected VList<EbiLastTimeOnSiteDistinct> Fill(IDataReader reader, VList<EbiLastTimeOnSiteDistinct> rows, int start, int pageLength)
		{
			int recordnum = 0;
			while (reader.Read() && (pageLength != 0))
			{
				if(recordnum >= start)
				{
					EbiLastTimeOnSiteDistinct entity = null;
					if (DataRepository.Provider.UseEntityFactory)
					{
						entity = EntityManager.CreateViewEntity<EbiLastTimeOnSiteDistinct>("EbiLastTimeOnSiteDistinct",  DataRepository.Provider.EntityCreationalFactoryType); 
					}
					else
					{
						entity = new EbiLastTimeOnSiteDistinct();
					}
					
					entity.SuppressEntityEvents = true;

					entity.CompanyName = (reader.IsDBNull(((int)EbiLastTimeOnSiteDistinctColumn.CompanyName)))?null:(System.String)reader[((int)EbiLastTimeOnSiteDistinctColumn.CompanyName)];
					//entity.CompanyName = (Convert.IsDBNull(reader["CompanyName"]))?string.Empty:(System.String)reader["CompanyName"];
					entity.SwipeSite = (System.String)reader[((int)EbiLastTimeOnSiteDistinctColumn.SwipeSite)];
					//entity.SwipeSite = (Convert.IsDBNull(reader["SwipeSite"]))?string.Empty:(System.String)reader["SwipeSite"];
					entity.SwipeDateTime = (System.DateTime)reader[((int)EbiLastTimeOnSiteDistinctColumn.SwipeDateTime)];
					//entity.SwipeDateTime = (Convert.IsDBNull(reader["SwipeDateTime"]))?DateTime.MinValue:(System.DateTime)reader["SwipeDateTime"];
					entity.AcceptChanges();
					entity.SuppressEntityEvents = false;
					
					rows.Add(entity);
					pageLength -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		
		
		/// <summary>
		/// Refreshes the <see cref="EbiLastTimeOnSiteDistinct"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="EbiLastTimeOnSiteDistinct"/> object to refresh.</param>
		protected void RefreshEntity(IDataReader reader, EbiLastTimeOnSiteDistinct entity)
		{
			reader.Read();
			entity.CompanyName = (reader.IsDBNull(((int)EbiLastTimeOnSiteDistinctColumn.CompanyName)))?null:(System.String)reader[((int)EbiLastTimeOnSiteDistinctColumn.CompanyName)];
			//entity.CompanyName = (Convert.IsDBNull(reader["CompanyName"]))?string.Empty:(System.String)reader["CompanyName"];
			entity.SwipeSite = (System.String)reader[((int)EbiLastTimeOnSiteDistinctColumn.SwipeSite)];
			//entity.SwipeSite = (Convert.IsDBNull(reader["SwipeSite"]))?string.Empty:(System.String)reader["SwipeSite"];
			entity.SwipeDateTime = (System.DateTime)reader[((int)EbiLastTimeOnSiteDistinctColumn.SwipeDateTime)];
			//entity.SwipeDateTime = (Convert.IsDBNull(reader["SwipeDateTime"]))?DateTime.MinValue:(System.DateTime)reader["SwipeDateTime"];
			reader.Close();
	
			entity.AcceptChanges();
		}
		
		/*
		/// <summary>
		/// Refreshes the <see cref="EbiLastTimeOnSiteDistinct"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="EbiLastTimeOnSiteDistinct"/> object.</param>
		protected static void RefreshEntity(DataSet dataSet, EbiLastTimeOnSiteDistinct entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.CompanyName = (Convert.IsDBNull(dataRow["CompanyName"]))?string.Empty:(System.String)dataRow["CompanyName"];
			entity.SwipeSite = (Convert.IsDBNull(dataRow["SwipeSite"]))?string.Empty:(System.String)dataRow["SwipeSite"];
			entity.SwipeDateTime = (Convert.IsDBNull(dataRow["SwipeDateTime"]))?DateTime.MinValue:(System.DateTime)dataRow["SwipeDateTime"];
			entity.AcceptChanges();
		}
		*/
			
		#endregion Helper Functions
	}//end class

	#region EbiLastTimeOnSiteDistinctFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EbiLastTimeOnSiteDistinct"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EbiLastTimeOnSiteDistinctFilterBuilder : SqlFilterBuilder<EbiLastTimeOnSiteDistinctColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EbiLastTimeOnSiteDistinctFilterBuilder class.
		/// </summary>
		public EbiLastTimeOnSiteDistinctFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the EbiLastTimeOnSiteDistinctFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public EbiLastTimeOnSiteDistinctFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the EbiLastTimeOnSiteDistinctFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public EbiLastTimeOnSiteDistinctFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion EbiLastTimeOnSiteDistinctFilterBuilder

	#region EbiLastTimeOnSiteDistinctParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EbiLastTimeOnSiteDistinct"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EbiLastTimeOnSiteDistinctParameterBuilder : ParameterizedSqlFilterBuilder<EbiLastTimeOnSiteDistinctColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EbiLastTimeOnSiteDistinctParameterBuilder class.
		/// </summary>
		public EbiLastTimeOnSiteDistinctParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the EbiLastTimeOnSiteDistinctParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public EbiLastTimeOnSiteDistinctParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the EbiLastTimeOnSiteDistinctParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public EbiLastTimeOnSiteDistinctParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion EbiLastTimeOnSiteDistinctParameterBuilder
	
	#region EbiLastTimeOnSiteDistinctSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EbiLastTimeOnSiteDistinct"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class EbiLastTimeOnSiteDistinctSortBuilder : SqlSortBuilder<EbiLastTimeOnSiteDistinctColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EbiLastTimeOnSiteDistinctSqlSortBuilder class.
		/// </summary>
		public EbiLastTimeOnSiteDistinctSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion EbiLastTimeOnSiteDistinctSortBuilder

} // end namespace
