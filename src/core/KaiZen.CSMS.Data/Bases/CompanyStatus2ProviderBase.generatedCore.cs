﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="CompanyStatus2ProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class CompanyStatus2ProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.CompanyStatus2, KaiZen.CSMS.Entities.CompanyStatus2Key>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.CompanyStatus2Key key)
		{
			return Delete(transactionManager, key.CompanyStatusId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_companyStatusId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _companyStatusId)
		{
			return Delete(null, _companyStatusId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyStatusId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _companyStatusId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.CompanyStatus2 Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.CompanyStatus2Key key, int start, int pageLength)
		{
			return GetByCompanyStatusId(transactionManager, key.CompanyStatusId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_CompanyStatus2 index.
		/// </summary>
		/// <param name="_companyStatusId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompanyStatus2"/> class.</returns>
		public KaiZen.CSMS.Entities.CompanyStatus2 GetByCompanyStatusId(System.Int32 _companyStatusId)
		{
			int count = -1;
			return GetByCompanyStatusId(null,_companyStatusId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CompanyStatus2 index.
		/// </summary>
		/// <param name="_companyStatusId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompanyStatus2"/> class.</returns>
		public KaiZen.CSMS.Entities.CompanyStatus2 GetByCompanyStatusId(System.Int32 _companyStatusId, int start, int pageLength)
		{
			int count = -1;
			return GetByCompanyStatusId(null, _companyStatusId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CompanyStatus2 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyStatusId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompanyStatus2"/> class.</returns>
		public KaiZen.CSMS.Entities.CompanyStatus2 GetByCompanyStatusId(TransactionManager transactionManager, System.Int32 _companyStatusId)
		{
			int count = -1;
			return GetByCompanyStatusId(transactionManager, _companyStatusId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CompanyStatus2 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyStatusId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompanyStatus2"/> class.</returns>
		public KaiZen.CSMS.Entities.CompanyStatus2 GetByCompanyStatusId(TransactionManager transactionManager, System.Int32 _companyStatusId, int start, int pageLength)
		{
			int count = -1;
			return GetByCompanyStatusId(transactionManager, _companyStatusId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CompanyStatus2 index.
		/// </summary>
		/// <param name="_companyStatusId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompanyStatus2"/> class.</returns>
		public KaiZen.CSMS.Entities.CompanyStatus2 GetByCompanyStatusId(System.Int32 _companyStatusId, int start, int pageLength, out int count)
		{
			return GetByCompanyStatusId(null, _companyStatusId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CompanyStatus2 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyStatusId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompanyStatus2"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.CompanyStatus2 GetByCompanyStatusId(TransactionManager transactionManager, System.Int32 _companyStatusId, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_CompanyStatus2 index.
		/// </summary>
		/// <param name="_companyStatusName"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompanyStatus2"/> class.</returns>
		public KaiZen.CSMS.Entities.CompanyStatus2 GetByCompanyStatusName(System.String _companyStatusName)
		{
			int count = -1;
			return GetByCompanyStatusName(null,_companyStatusName, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_CompanyStatus2 index.
		/// </summary>
		/// <param name="_companyStatusName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompanyStatus2"/> class.</returns>
		public KaiZen.CSMS.Entities.CompanyStatus2 GetByCompanyStatusName(System.String _companyStatusName, int start, int pageLength)
		{
			int count = -1;
			return GetByCompanyStatusName(null, _companyStatusName, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_CompanyStatus2 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyStatusName"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompanyStatus2"/> class.</returns>
		public KaiZen.CSMS.Entities.CompanyStatus2 GetByCompanyStatusName(TransactionManager transactionManager, System.String _companyStatusName)
		{
			int count = -1;
			return GetByCompanyStatusName(transactionManager, _companyStatusName, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_CompanyStatus2 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyStatusName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompanyStatus2"/> class.</returns>
		public KaiZen.CSMS.Entities.CompanyStatus2 GetByCompanyStatusName(TransactionManager transactionManager, System.String _companyStatusName, int start, int pageLength)
		{
			int count = -1;
			return GetByCompanyStatusName(transactionManager, _companyStatusName, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_CompanyStatus2 index.
		/// </summary>
		/// <param name="_companyStatusName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompanyStatus2"/> class.</returns>
		public KaiZen.CSMS.Entities.CompanyStatus2 GetByCompanyStatusName(System.String _companyStatusName, int start, int pageLength, out int count)
		{
			return GetByCompanyStatusName(null, _companyStatusName, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_CompanyStatus2 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyStatusName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompanyStatus2"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.CompanyStatus2 GetByCompanyStatusName(TransactionManager transactionManager, System.String _companyStatusName, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;CompanyStatus2&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;CompanyStatus2&gt;"/></returns>
		public static TList<CompanyStatus2> Fill(IDataReader reader, TList<CompanyStatus2> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.CompanyStatus2 c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("CompanyStatus2")
					.Append("|").Append((System.Int32)reader[((int)CompanyStatus2Column.CompanyStatusId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<CompanyStatus2>(
					key.ToString(), // EntityTrackingKey
					"CompanyStatus2",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.CompanyStatus2();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.CompanyStatusId = (System.Int32)reader[((int)CompanyStatus2Column.CompanyStatusId - 1)];
					c.CompanyStatusName = (System.String)reader[((int)CompanyStatus2Column.CompanyStatusName - 1)];
					c.CompanyStatusDesc = (reader.IsDBNull(((int)CompanyStatus2Column.CompanyStatusDesc - 1)))?null:(System.String)reader[((int)CompanyStatus2Column.CompanyStatusDesc - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.CompanyStatus2"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.CompanyStatus2"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.CompanyStatus2 entity)
		{
			if (!reader.Read()) return;
			
			entity.CompanyStatusId = (System.Int32)reader[((int)CompanyStatus2Column.CompanyStatusId - 1)];
			entity.CompanyStatusName = (System.String)reader[((int)CompanyStatus2Column.CompanyStatusName - 1)];
			entity.CompanyStatusDesc = (reader.IsDBNull(((int)CompanyStatus2Column.CompanyStatusDesc - 1)))?null:(System.String)reader[((int)CompanyStatus2Column.CompanyStatusDesc - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.CompanyStatus2"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.CompanyStatus2"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.CompanyStatus2 entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.CompanyStatusId = (System.Int32)dataRow["CompanyStatusId"];
			entity.CompanyStatusName = (System.String)dataRow["CompanyStatusName"];
			entity.CompanyStatusDesc = Convert.IsDBNull(dataRow["CompanyStatusDesc"]) ? null : (System.String)dataRow["CompanyStatusDesc"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.CompanyStatus2"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.CompanyStatus2 Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.CompanyStatus2 entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetByCompanyStatusId methods when available
			
			#region CompaniesCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<Companies>|CompaniesCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CompaniesCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.CompaniesCollection = DataRepository.CompaniesProvider.GetByCompanyStatus2Id(transactionManager, entity.CompanyStatusId);

				if (deep && entity.CompaniesCollection.Count > 0)
				{
					deepHandles.Add("CompaniesCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<Companies>) DataRepository.CompaniesProvider.DeepLoad,
						new object[] { transactionManager, entity.CompaniesCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region SqExemptionCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<SqExemption>|SqExemptionCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'SqExemptionCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.SqExemptionCollection = DataRepository.SqExemptionProvider.GetByCompanyStatus2Id(transactionManager, entity.CompanyStatusId);

				if (deep && entity.SqExemptionCollection.Count > 0)
				{
					deepHandles.Add("SqExemptionCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<SqExemption>) DataRepository.SqExemptionProvider.DeepLoad,
						new object[] { transactionManager, entity.SqExemptionCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.CompanyStatus2 object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.CompanyStatus2 instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.CompanyStatus2 Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.CompanyStatus2 entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<Companies>
				if (CanDeepSave(entity.CompaniesCollection, "List<Companies>|CompaniesCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(Companies child in entity.CompaniesCollection)
					{
						if(child.CompanyStatus2IdSource != null)
						{
							child.CompanyStatus2Id = child.CompanyStatus2IdSource.CompanyStatusId;
						}
						else
						{
							child.CompanyStatus2Id = entity.CompanyStatusId;
						}

					}

					if (entity.CompaniesCollection.Count > 0 || entity.CompaniesCollection.DeletedItems.Count > 0)
					{
						//DataRepository.CompaniesProvider.Save(transactionManager, entity.CompaniesCollection);
						
						deepHandles.Add("CompaniesCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< Companies >) DataRepository.CompaniesProvider.DeepSave,
							new object[] { transactionManager, entity.CompaniesCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<SqExemption>
				if (CanDeepSave(entity.SqExemptionCollection, "List<SqExemption>|SqExemptionCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(SqExemption child in entity.SqExemptionCollection)
					{
						if(child.CompanyStatus2IdSource != null)
						{
							child.CompanyStatus2Id = child.CompanyStatus2IdSource.CompanyStatusId;
						}
						else
						{
							child.CompanyStatus2Id = entity.CompanyStatusId;
						}

					}

					if (entity.SqExemptionCollection.Count > 0 || entity.SqExemptionCollection.DeletedItems.Count > 0)
					{
						//DataRepository.SqExemptionProvider.Save(transactionManager, entity.SqExemptionCollection);
						
						deepHandles.Add("SqExemptionCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< SqExemption >) DataRepository.SqExemptionProvider.DeepSave,
							new object[] { transactionManager, entity.SqExemptionCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region CompanyStatus2ChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.CompanyStatus2</c>
	///</summary>
	public enum CompanyStatus2ChildEntityTypes
	{

		///<summary>
		/// Collection of <c>CompanyStatus2</c> as OneToMany for CompaniesCollection
		///</summary>
		[ChildEntityType(typeof(TList<Companies>))]
		CompaniesCollection,

		///<summary>
		/// Collection of <c>CompanyStatus2</c> as OneToMany for SqExemptionCollection
		///</summary>
		[ChildEntityType(typeof(TList<SqExemption>))]
		SqExemptionCollection,
	}
	
	#endregion CompanyStatus2ChildEntityTypes
	
	#region CompanyStatus2FilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;CompanyStatus2Column&gt;"/> class
	/// that is used exclusively with a <see cref="CompanyStatus2"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompanyStatus2FilterBuilder : SqlFilterBuilder<CompanyStatus2Column>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompanyStatus2FilterBuilder class.
		/// </summary>
		public CompanyStatus2FilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the CompanyStatus2FilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CompanyStatus2FilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CompanyStatus2FilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CompanyStatus2FilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CompanyStatus2FilterBuilder
	
	#region CompanyStatus2ParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;CompanyStatus2Column&gt;"/> class
	/// that is used exclusively with a <see cref="CompanyStatus2"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompanyStatus2ParameterBuilder : ParameterizedSqlFilterBuilder<CompanyStatus2Column>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompanyStatus2ParameterBuilder class.
		/// </summary>
		public CompanyStatus2ParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the CompanyStatus2ParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CompanyStatus2ParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CompanyStatus2ParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CompanyStatus2ParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CompanyStatus2ParameterBuilder
	
	#region CompanyStatus2SortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;CompanyStatus2Column&gt;"/> class
	/// that is used exclusively with a <see cref="CompanyStatus2"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class CompanyStatus2SortBuilder : SqlSortBuilder<CompanyStatus2Column>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompanyStatus2SqlSortBuilder class.
		/// </summary>
		public CompanyStatus2SortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion CompanyStatus2SortBuilder
	
} // end namespace
