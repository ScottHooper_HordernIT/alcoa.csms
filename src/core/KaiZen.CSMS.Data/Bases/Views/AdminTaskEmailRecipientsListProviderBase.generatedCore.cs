﻿#region Using directives

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="AdminTaskEmailRecipientsListProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract class AdminTaskEmailRecipientsListProviderBaseCore : EntityViewProviderBase<AdminTaskEmailRecipientsList>
	{
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions
		
		/*
		///<summary>
		/// Fill an VList&lt;AdminTaskEmailRecipientsList&gt; From a DataSet
		///</summary>
		/// <param name="dataSet">the DataSet</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList&lt;AdminTaskEmailRecipientsList&gt;"/></returns>
		protected static VList&lt;AdminTaskEmailRecipientsList&gt; Fill(DataSet dataSet, VList<AdminTaskEmailRecipientsList> rows, int start, int pagelen)
		{
			if (dataSet.Tables.Count == 1)
			{
				return Fill(dataSet.Tables[0], rows, start, pagelen);
			}
			else
			{
				return new VList<AdminTaskEmailRecipientsList>();
			}	
		}
		
		
		///<summary>
		/// Fill an VList&lt;AdminTaskEmailRecipientsList&gt; From a DataTable
		///</summary>
		/// <param name="dataTable">the DataTable that hold the data.</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList<AdminTaskEmailRecipientsList>"/></returns>
		protected static VList&lt;AdminTaskEmailRecipientsList&gt; Fill(DataTable dataTable, VList<AdminTaskEmailRecipientsList> rows, int start, int pagelen)
		{
			int recordnum = 0;
			
			System.Collections.IEnumerator dataRows =  dataTable.Rows.GetEnumerator();
			
			while (dataRows.MoveNext() && (pagelen != 0))
			{
				if(recordnum >= start)
				{
					DataRow row = (DataRow)dataRows.Current;
				
					AdminTaskEmailRecipientsList c = new AdminTaskEmailRecipientsList();
					c.AdminTaskTypeName = (Convert.IsDBNull(row["AdminTaskTypeName"]))?string.Empty:(System.String)row["AdminTaskTypeName"];
					c.TypeDesc = (Convert.IsDBNull(row["TypeDesc"]))?string.Empty:(System.String)row["TypeDesc"];
					c.RecipientDesc = (Convert.IsDBNull(row["RecipientDesc"]))?string.Empty:(System.String)row["RecipientDesc"];
					c.AcceptChanges();
					rows.Add(c);
					pagelen -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		*/	
						
		///<summary>
		/// Fill an <see cref="VList&lt;AdminTaskEmailRecipientsList&gt;"/> From a DataReader.
		///</summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pageLength">number of row.</param>
		///<returns>a <see cref="VList&lt;AdminTaskEmailRecipientsList&gt;"/></returns>
		protected VList<AdminTaskEmailRecipientsList> Fill(IDataReader reader, VList<AdminTaskEmailRecipientsList> rows, int start, int pageLength)
		{
			int recordnum = 0;
			while (reader.Read() && (pageLength != 0))
			{
				if(recordnum >= start)
				{
					AdminTaskEmailRecipientsList entity = null;
					if (DataRepository.Provider.UseEntityFactory)
					{
						entity = EntityManager.CreateViewEntity<AdminTaskEmailRecipientsList>("AdminTaskEmailRecipientsList",  DataRepository.Provider.EntityCreationalFactoryType); 
					}
					else
					{
						entity = new AdminTaskEmailRecipientsList();
					}
					
					entity.SuppressEntityEvents = true;

					entity.AdminTaskTypeName = (System.String)reader[((int)AdminTaskEmailRecipientsListColumn.AdminTaskTypeName)];
					//entity.AdminTaskTypeName = (Convert.IsDBNull(reader["AdminTaskTypeName"]))?string.Empty:(System.String)reader["AdminTaskTypeName"];
					entity.TypeDesc = (System.String)reader[((int)AdminTaskEmailRecipientsListColumn.TypeDesc)];
					//entity.TypeDesc = (Convert.IsDBNull(reader["TypeDesc"]))?string.Empty:(System.String)reader["TypeDesc"];
					entity.RecipientDesc = (System.String)reader[((int)AdminTaskEmailRecipientsListColumn.RecipientDesc)];
					//entity.RecipientDesc = (Convert.IsDBNull(reader["RecipientDesc"]))?string.Empty:(System.String)reader["RecipientDesc"];
					entity.AcceptChanges();
					entity.SuppressEntityEvents = false;
					
					rows.Add(entity);
					pageLength -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		
		
		/// <summary>
		/// Refreshes the <see cref="AdminTaskEmailRecipientsList"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="AdminTaskEmailRecipientsList"/> object to refresh.</param>
		protected void RefreshEntity(IDataReader reader, AdminTaskEmailRecipientsList entity)
		{
			reader.Read();
			entity.AdminTaskTypeName = (System.String)reader[((int)AdminTaskEmailRecipientsListColumn.AdminTaskTypeName)];
			//entity.AdminTaskTypeName = (Convert.IsDBNull(reader["AdminTaskTypeName"]))?string.Empty:(System.String)reader["AdminTaskTypeName"];
			entity.TypeDesc = (System.String)reader[((int)AdminTaskEmailRecipientsListColumn.TypeDesc)];
			//entity.TypeDesc = (Convert.IsDBNull(reader["TypeDesc"]))?string.Empty:(System.String)reader["TypeDesc"];
			entity.RecipientDesc = (System.String)reader[((int)AdminTaskEmailRecipientsListColumn.RecipientDesc)];
			//entity.RecipientDesc = (Convert.IsDBNull(reader["RecipientDesc"]))?string.Empty:(System.String)reader["RecipientDesc"];
			reader.Close();
	
			entity.AcceptChanges();
		}
		
		/*
		/// <summary>
		/// Refreshes the <see cref="AdminTaskEmailRecipientsList"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="AdminTaskEmailRecipientsList"/> object.</param>
		protected static void RefreshEntity(DataSet dataSet, AdminTaskEmailRecipientsList entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.AdminTaskTypeName = (Convert.IsDBNull(dataRow["AdminTaskTypeName"]))?string.Empty:(System.String)dataRow["AdminTaskTypeName"];
			entity.TypeDesc = (Convert.IsDBNull(dataRow["TypeDesc"]))?string.Empty:(System.String)dataRow["TypeDesc"];
			entity.RecipientDesc = (Convert.IsDBNull(dataRow["RecipientDesc"]))?string.Empty:(System.String)dataRow["RecipientDesc"];
			entity.AcceptChanges();
		}
		*/
			
		#endregion Helper Functions
	}//end class

	#region AdminTaskEmailRecipientsListFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AdminTaskEmailRecipientsList"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AdminTaskEmailRecipientsListFilterBuilder : SqlFilterBuilder<AdminTaskEmailRecipientsListColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailRecipientsListFilterBuilder class.
		/// </summary>
		public AdminTaskEmailRecipientsListFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailRecipientsListFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AdminTaskEmailRecipientsListFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailRecipientsListFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AdminTaskEmailRecipientsListFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AdminTaskEmailRecipientsListFilterBuilder

	#region AdminTaskEmailRecipientsListParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AdminTaskEmailRecipientsList"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AdminTaskEmailRecipientsListParameterBuilder : ParameterizedSqlFilterBuilder<AdminTaskEmailRecipientsListColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailRecipientsListParameterBuilder class.
		/// </summary>
		public AdminTaskEmailRecipientsListParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailRecipientsListParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AdminTaskEmailRecipientsListParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailRecipientsListParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AdminTaskEmailRecipientsListParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AdminTaskEmailRecipientsListParameterBuilder
	
	#region AdminTaskEmailRecipientsListSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AdminTaskEmailRecipientsList"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class AdminTaskEmailRecipientsListSortBuilder : SqlSortBuilder<AdminTaskEmailRecipientsListColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailRecipientsListSqlSortBuilder class.
		/// </summary>
		public AdminTaskEmailRecipientsListSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion AdminTaskEmailRecipientsListSortBuilder

} // end namespace
