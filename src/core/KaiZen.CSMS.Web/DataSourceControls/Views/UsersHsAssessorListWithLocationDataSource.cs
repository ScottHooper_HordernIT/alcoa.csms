﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.UsersHsAssessorListWithLocationProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[CLSCompliant(true)]
	[Designer(typeof(UsersHsAssessorListWithLocationDataSourceDesigner))]
	public class UsersHsAssessorListWithLocationDataSource : ReadOnlyDataSource<UsersHsAssessorListWithLocation>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UsersHsAssessorListWithLocationDataSource class.
		/// </summary>
		public UsersHsAssessorListWithLocationDataSource() : base(new UsersHsAssessorListWithLocationService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the UsersHsAssessorListWithLocationDataSourceView used by the UsersHsAssessorListWithLocationDataSource.
		/// </summary>
		protected UsersHsAssessorListWithLocationDataSourceView UsersHsAssessorListWithLocationView
		{
			get { return ( View as UsersHsAssessorListWithLocationDataSourceView ); }
		}
		
		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the UsersHsAssessorListWithLocationDataSourceView class that is to be
		/// used by the UsersHsAssessorListWithLocationDataSource.
		/// </summary>
		/// <returns>An instance of the UsersHsAssessorListWithLocationDataSourceView class.</returns>
		protected override BaseDataSourceView<UsersHsAssessorListWithLocation, Object> GetNewDataSourceView()
		{
			return new UsersHsAssessorListWithLocationDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the UsersHsAssessorListWithLocationDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class UsersHsAssessorListWithLocationDataSourceView : ReadOnlyDataSourceView<UsersHsAssessorListWithLocation>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UsersHsAssessorListWithLocationDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the UsersHsAssessorListWithLocationDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public UsersHsAssessorListWithLocationDataSourceView(UsersHsAssessorListWithLocationDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal UsersHsAssessorListWithLocationDataSource UsersHsAssessorListWithLocationOwner
		{
			get { return Owner as UsersHsAssessorListWithLocationDataSource; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal UsersHsAssessorListWithLocationService UsersHsAssessorListWithLocationProvider
		{
			get { return Provider as UsersHsAssessorListWithLocationService; }
		}

		#endregion Properties
		
		#region Methods
		
		#endregion Methods
	}

	#region UsersHsAssessorListWithLocationDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the UsersHsAssessorListWithLocationDataSource class.
	/// </summary>
	public class UsersHsAssessorListWithLocationDataSourceDesigner : ReadOnlyDataSourceDesigner<UsersHsAssessorListWithLocation>
	{
	}

	#endregion UsersHsAssessorListWithLocationDataSourceDesigner

	#region UsersHsAssessorListWithLocationFilter

	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="UsersHsAssessorListWithLocation"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UsersHsAssessorListWithLocationFilter : SqlFilter<UsersHsAssessorListWithLocationColumn>
	{
	}

	#endregion UsersHsAssessorListWithLocationFilter

	#region UsersHsAssessorListWithLocationExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="UsersHsAssessorListWithLocation"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UsersHsAssessorListWithLocationExpressionBuilder : SqlExpressionBuilder<UsersHsAssessorListWithLocationColumn>
	{
	}
	
	#endregion UsersHsAssessorListWithLocationExpressionBuilder		
}

